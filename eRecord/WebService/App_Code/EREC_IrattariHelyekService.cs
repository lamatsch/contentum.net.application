﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contentum.eBusinessDocuments;
using System.Web.Services;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

/// <summary>
/// Summary description for EREC_IrattariHelyekService
/// </summary>
/// 
[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
public partial class EREC_IrattariHelyekService : System.Web.Services.WebService
{
    /// <summary>
    /// Update(ExecParam ExecParam, EREC_IrattariHelyek Record)
    /// Az EREC_IrattariHelyek tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param>
    /// <param name="Record">A módosított adatokat a Record paraméter tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result Update(ExecParam ExecParam, EREC_IrattariHelyek Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);            

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            result = this.RenameIrattariHelyekIrattariHely(ExecParam, ExecParam.Record_Id);
            if (!String.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);
                        
            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_IrattariHelyek", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
            
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(ExecParam, result);
        return result;
    }
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyekSearch))]
    public Result GetAllRoots(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllRoots(ExecParam, _EREC_IrattariHelyekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyekSearch))]
    public Result GetAllWithPartnerKapcsolatok(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithPartnerKapcsolatok(ExecParam, _EREC_IrattariHelyekSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result GetAllLeafs(ExecParam ExecParam)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllLeafs(ExecParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result RenameIrattariHelyekIrattariHely(ExecParam ExecParam,string AlairokId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            if (AlairokId == null)
                throw new ResultException(50405);

            result = sp.IrattariHelyekRename(ExecParam, AlairokId);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result IrattariHelyStrukturaGetByRoots(ExecParam ExecParam, string RootId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.IrattariHelyStrukturaGetByRoots(ExecParam, RootId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyekSearch))]
    public Result GetReadOnlyIds(ExecParam ExecParam)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetReadOnlyIds(ExecParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result GetByVonalkod(ExecParam ExecParam, string Vonalkod)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetByVonalkod(ExecParam, Vonalkod);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result IrattarRendezes(ExecParam ExecParam, string UgyiratIds, string IrattarId, string Ertek)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.IrattarRendezes(ExecParam, DateTime.Now, UgyiratIds, IrattarId, Ertek);

            //migrált főszámok áthelyezése
            try
            {
                EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam xpm = ExecParam.Clone();
                Result resGetAllRegiSzerelt = ugyiratokService.GetAllRegiSzereltAdat(xpm, UgyiratIds.Split(','));

                if (resGetAllRegiSzerelt.IsError)
                {
                    Logger.Error("GetAllRegiSzerelt hiba", xpm, resGetAllRegiSzerelt);
                    throw new ResultException(resGetAllRegiSzerelt);
                }

                DataTable table = resGetAllRegiSzerelt.Ds.Tables[0];
                Logger.Debug(String.Format("GetAllRegiSzerelt eredmenyszam: {0}", table.Rows.Count));

                if (table.Rows.Count > 0)
                {
                    string[] regiRendszerIktatoszamIdArray = new String[table.Rows.Count];

                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        regiRendszerIktatoszamIdArray[i] = table.Rows[i]["RegiRendszerIktatoszamId"].ToString();
                    }

                    Logger.Debug("MIG_IrattariHelyekService.IrattarRendezes kezdete");
                    Contentum.eMigration.Service.MIG_IrattariHelyekService irattariHelyekService = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_IrattariHelyekService();
                    Result resIrattarRendezes = irattariHelyekService.IrattarRendezes(xpm, String.Join(",", regiRendszerIktatoszamIdArray), IrattarId, Ertek);
                    if (resIrattarRendezes.IsError)
                    {
                        Logger.Error("MIG_IrattariHelyekService.IrattarRendezes hiba", xpm, resIrattarRendezes);
                        throw new ResultException(resIrattarRendezes);
                    }


                    Logger.Debug("UpdateRegiSzereltUgyiratok vege");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Migrált szerelt adatok hiba!", ex);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_IrattariHelyek Record)
    /// Egy rekord felvétele a EREC_IrattariHelyek táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyek))]
    public Result Insert(ExecParam ExecParam, EREC_IrattariHelyek Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            using (KRT_BarkodokService service = new KRT_BarkodokService(this.dataContext))
            {

                #region Volt-e már ilyen vonalkod ellenőrzés

                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                Result resBarCode = srvBarcode.CheckBarcode(ExecParam, Record.Vonalkod);
                if (!String.IsNullOrEmpty(resBarCode.ErrorCode))
                {
                    throw new ResultException(resBarCode);
                }

                #endregion

                //Mehet az irattári hely létrehozása
                result = sp.Insert(Constants.Insert, ExecParam, Record);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }

                #region A vonalkod az irattári hely rekordhoz kapcsolása.
                if (!string.IsNullOrEmpty(resBarCode.Uid) && !string.IsNullOrEmpty(result.Uid))
                {
                    string barkodId = resBarCode.Uid;
                    string barkodVer = resBarCode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                    string irattariHelyId = result.Uid;

                    resBarCode = new Result();
                    resBarCode = service.BarkodBindToObject(ExecParam.Clone(), barkodId, irattariHelyId, "EREC_IrattariHelyek", barkodVer);

                    if (!String.IsNullOrEmpty(resBarCode.ErrorCode))
                    {
                        throw new ResultException(resBarCode);
                    }
                }
                else
                {
                    Logger.Error("KRT_BARCODE ID NULL!");
                    throw new ResultException("A vonalkód létrehozása nem sikerült!");
                }
                #endregion


            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_IrattariHelyek", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// GetAll(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
    /// A(z) EREC_IrattariHelyek táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szûrési feltételeket paraméter tartalmazza (*Search). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemenõ paraméter. Az ExecParam.Record_Id nem használt, további adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).</param>
    /// <param name="_EREC_IrattariHelyekSearch">Bemenõ paraméter, a keresési feltételeket tartalmazza. </param>
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott táblára vonatkozó keresés eredményhalmazának elkérése. A táblára vonatkozó szûrési feltételeket paraméter tartalmazza (*Search). Az ExecParam.Record_Id nem használt, a további adatok a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariHelyekSearch))]
    public Result GetAllForTerkep(ExecParam ExecParam, EREC_IrattariHelyekSearch _EREC_IrattariHelyekSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            var irattarozasHelyFelhasznaloAltal = Rendszerparameterek.GetBoolean(ExecParam, "IRATTAROZAS_HELY_FELH_ALTAL", true); // BUG_4785
            bool kozpontiIrattar = !String.IsNullOrEmpty(_EREC_IrattariHelyekSearch.Felelos_Csoport_Id.Value) 
                && _EREC_IrattariHelyekSearch.Felelos_Csoport_Id.Value.Contains(KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id);
            if (!irattarozasHelyFelhasznaloAltal && kozpontiIrattar)
            {
                var irattariHelyek = GetAllJogosultIrattariHelyList(ExecParam);
                if (irattariHelyek != null && irattariHelyek.Count() > 0)
                {
                    _EREC_IrattariHelyekSearch.Id.In(irattariHelyek);
                }
                else
                {
                    _EREC_IrattariHelyekSearch.Id.Filter(Guid.Empty.ToString());
                }
            }

            result = sp.GetAllForTerkep(ExecParam, _EREC_IrattariHelyekSearch);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private KRT_Barkodok getBarCode(ExecParam execParam, EREC_IrattariHelyek erec_IrattariHelyek, string Obj_id, KRT_Barkodok krt_Barkod)
    {
        krt_Barkod.Updated.SetValueAll(true);
        krt_Barkod.Kod = erec_IrattariHelyek.Vonalkod;
        krt_Barkod.Obj_Id = Obj_id;
        krt_Barkod.Obj_type = "EREC_IrattariHelyek";
        krt_Barkod.Allapot = "F";
        return krt_Barkod;
    }

    [WebMethod]
    public Result GetAllJogosultIrattariHely(ExecParam execParam)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllJogosultIrattariHely(execParam);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private List<string> GetAllJogosultIrattariHelyList(ExecParam execParam)
    {
        var irattariHelyek = new List<string>();
        var res = GetAllJogosultIrattariHely(execParam);
        if (!res.IsError)
        {
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                irattariHelyek.Add(row["Id"].ToString());
            }
        }
        return irattariHelyek;
    }
}