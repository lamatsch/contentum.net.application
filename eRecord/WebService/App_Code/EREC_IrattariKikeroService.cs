using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using System.Collections.Generic;
using Contentum.eAdmin.Service;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IrattariKikeroService : System.Web.Services.WebService
{
    // m�g nem haszn�lt, de k�s�bb az egys�ges j�v�hagy�s kezel�s miatt j� lenne az iratt�rba/skontr�ba ad�shoz hasonl�an kezelni
    private static bool NeedKolcsonzesJovahagyas(ExecParam p_xpm, out string vezetoID)
    {
        vezetoID = String.Empty;

        // itt egyel�re nem szab�lyozzuk rendszerparam�terrel
        //if (!IsKolcsonzesJovahagyasEnabled(p_xpm))
        //    return false;


        //// ha van KolcsonzesJovahagyasNelkul joga, akkor sem kell j�v�hagyni
        //if (HasFunctionRight(p_xpm, Contentum.eUtility.Constants.Funkcio.KolcsonzesJovahagyasNelkul))
        //{
        //    return false;
        //}

        vezetoID = Csoportok.GetFirstRightedLeader_ID(p_xpm, true, Contentum.eUtility.Constants.Funkcio.UgyiratKolcsonzesJovahagyas);
        if (vezetoID == p_xpm.Felhasznalo_Id)
            return false;
        else
            return true; // TODO: hibakezel�s!!!

    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariKikeroSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_IrattariKikeroSearch _EREC_IrattariKikeroSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_IrattariKikeroSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// GetByUgyiratId(ExecParam ExecParam)  
    /// A EREC_IrattariKikero rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(Description = "Egy rekord elk�r�se az adott t�bl�b�l a t�telazonos�t� alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariKikero))]
    public Result GetByUgyiratId(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_IrattariKikeroSearch search = new EREC_IrattariKikeroSearch();
            search.UgyUgyirat_Id.Value = ExecParam.Record_Id;
            search.UgyUgyirat_Id.Operator = Query.Operators.equals;

            search.Allapot.Value = KodTarak.IRATTARIKIKEROALLAPOT.Nyitott + "," + KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto + "," +
                KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett + "," + KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            search.Allapot.Operator = Query.Operators.inner;

            result = sp.GetAll(ExecParam, search);

            ResultError.CreateNoRowResultError(result);

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                ExecParam.Record_Id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                result = sp.Get(ExecParam);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }



    public Result GetAllErvenyesKikeroByUgyiratIds(ExecParam ExecParam, string[] UgyiratIds)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_IrattariKikeroSearch search = new EREC_IrattariKikeroSearch();
            search.UgyUgyirat_Id.Value = Search.GetSqlInnerString(UgyiratIds);
            search.UgyUgyirat_Id.Operator = Query.Operators.inner;

            search.Allapot.Value = KodTarak.IRATTARIKIKEROALLAPOT.Nyitott + "," + KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto + "," +
                KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett + "," + KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            search.Allapot.Operator = Query.Operators.inner;

            result = sp.GetAll(ExecParam, search);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }



    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariKikero))]
    public Result Insert_Tomeges(ExecParam ExecParam, string[] UgyUgyirat_Ids, EREC_IrattariKikero Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert_Tomeges(Constants.Insert, ExecParam, null, null, UgyUgyirat_Ids, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //#region Esem�nynapl�z�s
            //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, Search.GetSqlInnerString(UgyUgyirat_Ids), "IrattariKikeroNew");
            //if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //    Logger.Debug("[ERROR]EREC_IrattariKikero::IrattariKikeroNew: ", ExecParam, eventLogResult);
            //#endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariKikero))]
    public Result Update_Tomeges(ExecParam ExecParam, string[] Ids, string[] Vers, EREC_IrattariKikero Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert_Tomeges(Constants.Update, ExecParam, Ids, Vers, null, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //#region Esem�nynapl�z�s
            //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, Search.GetSqlInnerString(UgyUgyirat_Ids), "IrattariKikeroModify");
            //if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //    Logger.Debug("[ERROR]EREC_IrattariKikero::IrattariKikeroModify: ", ExecParam, eventLogResult);
            //#endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region �gyirat Kolcsonzese
    // BUG_10365: Kik�r�s/k�lcs�nz�s sor�n vizsg�ljuk meg, hogy mely szervezethez tartozik az �gyirat (Szervezeti egys�g) �s csak azon
    // szervezeti egys�g vezet�j�n�l legyen automatikusan Enged�lyezett kik�r�n l�v� az �gyirat (saj�t mag�t�l nem kell elk�rnie).
    private bool IsSajatSzervezet(ExecParam execParam, string vezetoID)
    {
        return !String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            && execParam.Felhasznalo_Id.Equals(vezetoID, StringComparison.InvariantCultureIgnoreCase);
    }

    /// <summary>
    /// �gyirat Kolcsonzese
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Kolcsonzes(ExecParam execParam, EREC_IrattariKikero erec_IrattariKikero, bool withoutLeader)
    {
        return Kolcsonzes(execParam, erec_IrattariKikero, false, withoutLeader);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IrattariKikero"></param>
    /// <param name="automatikusJovahagyasKell">ha true: mindenk�ppen j�v� is hagy�dik a k�lcs�nz�s; 
    /// ha false, akkor eredeti m�k�d�s, teh�t att�l m�g a j�v�hagy�s is megt�rt�nhet, ha pl. az �gyint�z� k�lcs�nzi ki az �gyiratot</param>
    /// <returns></returns>
    public Result Kolcsonzes(ExecParam execParam, EREC_IrattariKikero erec_IrattariKikero, bool automatikusJovahagyasKell, bool withoutLeader)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // �gyirat lek�r�se:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            //execParam_UgyiratGet.Record_Id = execParam.Record_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52204);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atveheto = execParam.Clone();

                //kolcsonozheto-e?
                // (K�zponti iratt�rban van, vagy k�zponti iratt�rba k�ld�tt (iktat�skor ilyenkor is kell k�lcs�nz�st ind�tani))
                if (Contentum.eRecord.BaseUtility.Ugyiratok.KozpontiIrattarbanOrzott(ugyiratStatusz, execParam_atveheto) == false
                    && (Contentum.eRecord.BaseUtility.Ugyiratok.KozpontiIrattarbaKuldott(ugyiratStatusz, execParam_atveheto) == false))
                {
                    // nem az irattarban van az ugyirat:
                    throw new ResultException(52401);
                }

                // Ha az �gyint�z� megegyezik a kik�r�vel, akkor r�gt�n j�v� is hagyjuk a kik�r�st
                //bool automatikusJovahagyasKell = false;

                // ha ez true, nem ind�tunk j�v�hagy�st (iratt�rba k�ld�tt �gyiratra nem kell ind�tani)
                bool disableJovahagyas = false;

                bool jovahagyoUgyfelelos = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTAROZAS_JOVAHAGYO_UGYFELELOS, false);
                // j�v�hagy� mez�be be�rjuk azt, aki j�v�hagyhatja
                string vezetoID = "";
                //LZS - BUG_6386
                //LZS - BUG_11549
                if (!withoutLeader)
                {
                    if (jovahagyoUgyfelelos)
                    {
                        vezetoID = Csoportok.GetCsoportVezetoID(execParam, erec_UgyUgyirat.Csoport_Id_Ugyfelelos);
                    }
                    else
                    {
                        vezetoID = Csoportok.GetFirstRightedLeader_ID(execParam.Clone(), true, Contentum.eUtility.Constants.Funkcio.UgyiratKolcsonzesJovahagyas);
                    }
                }//LZS - BUG_11549
                else
                {
                    vezetoID = erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy;
                }
                // Ha az �gyirat Iratt�rba k�ld�tt, nem kell �ll�tani az �llapotot. (Ilyenkor majd az iratt�rba v�telkor kell ezt be�ll�tani.)
                // Ha Iratt�rban �rz�tt az �llapot, akkor Iratt�rb�l elk�rt-re �ll�tjuk
                if (erec_UgyUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                {

                    // �GYIRAT UPDATE:
                    // mez�k m�dos�that�s�g�nak letilt�sa:
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    // M�dos�tani kell: az allapotot, 

                    /// �llapot be�ll�t�sa:
                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    // EB 2011.01.24:
                    // CR#2602: K�lcs�nz�s j�v�hagy�j�t bejegyezz�k a k�vetkez� felel�sbe is,
                    // hogy az �gyiratokra a list�kon sz�rni lehessen
                    // - pl. feladataim k�lcs�nz�sre j�v�hagyand�k link
                    erec_UgyUgyirat.Kovetkezo_Felelos_Id = vezetoID;
                    erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_UgyiratUpdate);
                    }

                    // ha a kik�r� az �gyirat �gyint�z�je, r�gt�n j�v� is hagyjuk a kik�r�st
                    //if (erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero == erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez
                    //    && !string.IsNullOrEmpty(erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez))
                    //{
                    //    automatikusJovahagyasKell = true;
                    //}
                }
                else
                {
                    // ha iratt�rba k�ld�tt az �gyirat:
                    disableJovahagyas = true;
                }

                // ha van funkci�joga a kik�r�nek, automatikus j�vahagy�s
                if (!automatikusJovahagyasKell)
                {
                    if (CanFelhasznaloJovahagy(execParam, erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero, false, erec_UgyUgyirat))
                    {
                        // BUG_11347
                        //automatikusJovahagyasKell = IsSajatSzervezet(execParam, vezetoID);
                        if (jovahagyoUgyfelelos)
                        {
                            automatikusJovahagyasKell = IsSajatSzervezet(execParam, vezetoID);
                        }
                        else
                        {
                            automatikusJovahagyasKell = true;
                        }
                    }
                }

                Result IrattariKikeroResult = new Result();
                ExecParam IrattariKikeroExecParam = execParam.Clone();
                IrattariKikeroExecParam.Record_Id = "";

                // Elso allapot:
                erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto;
                erec_IrattariKikero.Updated.Allapot = true;

                // Iratt�r ID-j�nak be�ll�tani az �gyirat felel�s�t (vagyis az iratt�rat)
                erec_IrattariKikero.Irattar_Id = erec_UgyUgyirat.Csoport_Id_Felelos;
                erec_IrattariKikero.Updated.Irattar_Id = true;

                // j�v�hagy� mez�be be�rjuk azt, aki j�v�hagyhatja
                //KRT_CsoportokService csoportokService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                //Result vezetoGetResult = csoportokService.GetLeader(execParam.Clone(), execParam.FelhasznaloSzervezet_Id);
                //if (!string.IsNullOrEmpty(vezetoGetResult.ErrorCode))
                //{
                //    throw new ResultException(vezetoGetResult);
                //}
                //erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = (vezetoGetResult.Record as KRT_Csoportok).Id;

                // j�v�hagy� mez�be be�rjuk azt, aki j�v�hagyhatja
                //string vezetoID = Csoportok.GetFirstRightedLeader_ID(execParam.Clone(), true, Contentum.eUtility.Constants.Funkcio.UgyiratKolcsonzesJovahagyas);
                if (String.IsNullOrEmpty(vezetoID))
                {
                    throw new ResultException(53202); // Nem tal�lhat� az aktu�lis felhaszn�l� f�l� rendelt, adott jogosults�ggal rendelkez� vezet� a szervezeti hierarchi�ban!
                }
                erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = vezetoID;
                erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;

                IrattariKikeroResult = this.Insert(IrattariKikeroExecParam, erec_IrattariKikero);
                if (!String.IsNullOrEmpty(IrattariKikeroResult.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    throw new ResultException(IrattariKikeroResult);
                }

                result = IrattariKikeroResult;

                if (automatikusJovahagyasKell && !disableJovahagyas)
                {
                    #region K�lcs�nz�s j�v�hagy�sa

                    Logger.Info("Automatikus k�lcs�nz�s j�v�hagy�s...", execParam);

                    ExecParam execParam_jovahagyas = execParam.Clone();
                    // RecordId-ba tessz�k az �gyirat Id-t:
                    execParam_jovahagyas.Record_Id = erec_UgyUgyirat.Id;

                    Result result_jovahagyas = this.KolcsonzesJovahagyas(execParam_jovahagyas, false);
                    if (result_jovahagyas.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_jovahagyas);
                    }

                    #endregion
                }

            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "KozpontiIrattarKolcsonzes").Record;

            eventLogRecord.Base.Note = erec_IrattariKikero.Keres_note;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public const string funkcioKolcsonzesJovahagyas = Contentum.eUtility.Constants.Funkcio.UgyiratKolcsonzesJovahagyas; //"KolcsonzesJovahagyas";

    private bool CanFelhasznaloJovahagy(ExecParam p_ExecParam, string p_FelhasznaloID, EREC_UgyUgyiratok ugyirat)
    {
        return CanFelhasznaloJovahagy(p_ExecParam, p_FelhasznaloID, true, ugyirat);
    }

    private bool CanFelhasznaloJovahagy(ExecParam p_ExecParam, string p_FelhasznaloID, bool WithRightCheck, EREC_UgyUgyiratok ugyirat)
    {
        Logger.Debug("CanFelhasznaloJovahagy start");
        if (String.IsNullOrEmpty(p_FelhasznaloID))
        {
            Logger.Debug("CanFelhasznaloJovahagy p_FelhasznaloID is null or empty");
            return false;
        }

        // az aktu�lis felhaszn�l�t n�zz�k, amikor k�zi j�v�hagy�s van, �s a kik�r�t, ha automatikus
        string User_Id_ToCheck = (WithRightCheck ? p_ExecParam.Felhasznalo_Id : p_FelhasznaloID);

        // a k�lcs�nz�st csak a kik�r� vezet�je hagyhatja j�v�, kiv�ve, ha kik�r�skor automatikusan j�v� is hagyjuk
        if (WithRightCheck)
        {
            Result kikeroGetResult = this.GetByUgyiratId(p_ExecParam);
            if (!string.IsNullOrEmpty(kikeroGetResult.ErrorCode))
            {
                Logger.Error("CanFelhasznaloJovahagy hiba", p_ExecParam, kikeroGetResult);
                throw new ResultException(kikeroGetResult);
            }

            EREC_IrattariKikero kikero = (EREC_IrattariKikero)kikeroGetResult.Record;

            // a k�lcs�nz�st csak a kik�r� vezet�je hagyhatja j�v�
            //if (kikero.FelhasznaloCsoport_Id_Jovahagy != p_FelhasznaloID)
            if (kikero.FelhasznaloCsoport_Id_Jovahagy != User_Id_ToCheck)
            {
                return false;
            }
        }

        #region Ha az �gyirat �gyint�z�je k�rte ki az �gyiratot, akkor j�v�hagyhat�, funkci�jogt�l f�ggetlen�l

        //EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        //ExecParam ugyiratGetExecParam = p_ExecParam.Clone();
        //Result ugyiratGetResult = ugyiratokService.Get(ugyiratGetExecParam);

        if (IsUgyintezoAutomatikusJovahagyasEnabled(p_ExecParam.Clone()) &&
            ugyirat != null && ugyirat.FelhasznaloCsoport_Id_Ugyintez == p_FelhasznaloID)
        {
            return true;
        }


        #endregion Ha az �gyirat �gyint�z�je k�rte ki az �gyiratot, akkor j�v�hagyhat�, funkci�jogt�l f�ggetlen�l


        if (p_ExecParam.Felhasznalo_Id.Equals(p_FelhasznaloID, StringComparison.InvariantCultureIgnoreCase))
        {
            return FunctionRights.HasFunctionRight(p_ExecParam, funkcioKolcsonzesJovahagyas);
        }
        else
        {
            KRT_FunkciokService svcFunkciok = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            KRT_Felhasznalok krtFelhasznalok = new KRT_Felhasznalok();
            //krtFelhasznalok.Id = p_FelhasznaloID;
            krtFelhasznalok.Id = User_Id_ToCheck;
            Result res = svcFunkciok.GetAllByFelhasznalo(p_ExecParam, krtFelhasznalok, null);

            if (res.IsError)
            {
                Logger.Error("CanFelhasznaloJovahagy hiba", p_ExecParam, res);
                throw new ResultException(res);
            }
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string szerepkorKod = row["Kod"].ToString();
                if (String.Compare(szerepkorKod, funkcioKolcsonzesJovahagyas, true) == 0)
                {
                    Logger.Debug("CanFelhasznaloJovahagy return true");
                    return true;
                }
            }
        }
        Logger.Debug("CanFelhasznaloJovahagy end");
        return false;
    }

    private bool IsUgyintezoAutomatikusJovahagyasEnabled(ExecParam execParam)
    {
        bool ret = Rendszerparameterek.GetBoolean(execParam, "KOLCSONZES_JOVAHAGYAS_BY_UGYINTEZO_DISABLED", false);
        return !ret;
    }

    #endregion

    #region �gyirat Kolcsonzes Modositasa
    /// <summary>
    /// �gyirat Kolcsonzes Modositasa
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result KolcsonzesUpdate(ExecParam execParam, EREC_IrattariKikero erec_IrattariKikero)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (String.IsNullOrEmpty(erec_IrattariKikero.UgyUgyirat_Id) && String.IsNullOrEmpty(erec_IrattariKikero.Tertiveveny_Id))
            {
                // hiba, nincsen hozzarendelve semmilyen objektum!
                //Result result = ResultError.CreateNewResultWithErrorCode(52402);
                //log.WsEnd(execParam, result);
                //return result;

                throw new ResultException(52402);
            }

            // TODO: ide kene egy irattarosi szerepkor ellenorzes, mert csak azok modosithatjak a jovahagyot!

            Result IrattariKikeroResult = new Result();
            ExecParam IrattariKikeroExecParam = execParam.Clone();

            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Updated.Ver = true;

            //if (execParam.Felhasznalo_Id == erec_IrattariKikero.Kikero_Id) // Ide is kene egy irattarosi szerepkor figyeles
            //{
            erec_IrattariKikero.Updated.FelhasznalasiCel = true;
            erec_IrattariKikero.Updated.KikerKezd = true;
            erec_IrattariKikero.Updated.KikerVege = true;
            erec_IrattariKikero.Updated.Keres_note = true;
            //}
            //else
            //{
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;
            //}

            // Ha megadtak a jovahagyot, akkor az allapotot enfedelyezettre kell allitani nyitottrol
            if (!String.IsNullOrEmpty(erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy) || erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy == true)
            {
                if (erec_IrattariKikero.Allapot == KodTarak.IRATTARIKIKEROALLAPOT.Nyitott)
                {
                    erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto;
                    erec_IrattariKikero.Updated.Allapot = true;
                }
            }

            IrattariKikeroResult = this.Update(IrattariKikeroExecParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(IrattariKikeroResult.ErrorCode))
            {
                // hiba:
                //ContextUtil.SetAbort();

                throw new ResultException(IrattariKikeroResult);
            }

            result = IrattariKikeroResult;

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(IrattariKikeroExecParam, dataContext.Tranz_Id, IrattariKikeroExecParam.Record_Id, "EREC_IrattariKikero", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(IrattariKikeroExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }
    #endregion

    #region �gyirat Kolcsonzes Jovahagyasa
    /// <summary>
    /// �gyirat Kolcsonzes Jovahagyasa
    /// 2008.09.04: A felel�st (kezel�t) nem itt �ll�tjuk, hanem majd a KolcsonzesKiadas-n�l
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KolcsonzesJovahagyas(ExecParam execParam)
    {
        return KolcsonzesJovahagyas(execParam, true);
    }

    private Result KolcsonzesJovahagyas(ExecParam execParam, bool WithRightCheck)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // m�s nev�re val� kik�r�sn�l elt�r� jogai lehetnek a felhaszn�l�nak �s a kik�r�nek
            //#region RightCheck

            //if (!this.CanFelhasznaloJovahagy(execParam.Clone(), execParam.Felhasznalo_Id, WithRightCheck))
            //{
            //    throw new ResultException(52411);
            //}

            //#endregion RightCheck

            //�gyirat lek�r�se
            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam ugyiratGetExecParam = execParam.Clone();
            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratService.Get(ugyiratGetExecParam).Record;

            #region iratt�ri kik�r� t�tel record m�dos�t�sa

            string ugyiratId = execParam.Record_Id;
            Result _irattariTetelGetResult = this.GetByUgyiratId(execParam);

            if (!String.IsNullOrEmpty(_irattariTetelGetResult.ErrorCode) || _irattariTetelGetResult.Record == null)
            {
                // hiba
                throw new ResultException(52403);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)_irattariTetelGetResult.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            #region RightCheck
            // itt az eredeti execParamban m�r a kik�r� Id-ja van, nem az �gyirat�
            ExecParam execParam_jovahagy = execParam.Clone();
            execParam_jovahagy.Record_Id = ugyiratId;
            // ha van jogvizsg�lat, akkor az aktu�lis felhaszn�l�t n�zz�k, ha nincs, a kik�r�t
            if (!this.CanFelhasznaloJovahagy(execParam_jovahagy, erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero, WithRightCheck, ugyirat))
            {
                throw new ResultException(52411);
            }

            #endregion RightCheck

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Jovahagyhato(statusz, execParam))
            {
                // hiba nem jovahagyhato
                throw new ResultException(52404);
            }

            EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            // jovahagyott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett;
            erec_IrattariKikero.Updated.Allapot = true;

            // jovahagyo felhasznalo es ido beallitasa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = (WithRightCheck ? execParam.Felhasznalo_Id : erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero); //execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;

            erec_IrattariKikero.JovagyasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.JovagyasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }
            #endregion

            #region �gyirat m�dos�t�sa

            // �llapotot m�dos�tjuk, felel�st nem, az a KolcsonzesKiadas-n�l v�ltozik


            // Ha az �gyirat Iratt�rba k�ld�tt, nem kell �ll�tani az �llapotot.
            // (Akkor lehet ilyen, ha iratt�rba k�ld�tt �gyiratba iktattak, �s indult egy k�lcs�nz�si folyamat (de m�g nincs k�zponti iratt�rba v�ve))    
            if (ugyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
            {
                ugyirat.Updated.SetValueAll(false);
                ugyirat.Base.Updated.SetValueAll(false);
                ugyirat.Base.Updated.Ver = true;
                //ugyirat.Csoport_Id_Felelos = erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero;
                //ugyirat.Updated.Csoport_Id_Felelos = true;

                ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo;
                ugyirat.Updated.Allapot = true;

                // EB 2011.01.24:
                // CR#2602: K�lcs�nz�s j�v�hagy�j�t bejegyezz�k a k�vetkez� felel�sbe is,
                // hogy az �gyiratokra a list�kon sz�rni lehessen, ez�rt itt t�r�lj�k
                // - pl. feladataim k�lcs�nz�sre j�v�hagyand�k link
                ugyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
                ugyirat.Updated.Kovetkezo_Felelos_Id = true;

                Result ugyiratUpdateResult = ugyiratService.Update(ugyiratGetExecParam, ugyirat);
                if (!string.IsNullOrEmpty(ugyiratUpdateResult.ErrorCode))
                {
                    throw new ResultException(ugyiratUpdateResult);
                }
            }

            #endregion

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyiratGetExecParam.Record_Id, "EREC_UgyUgyiratok", "KolcsonzesJovahagyas").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion



    #region �gyirat Kolcsonzes Visszautasitasa
    /// <summary>
    /// �gyirat Kolcsonzes Visszautasitas
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result KolcsonzesVisszautasitas(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52400);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //�gyirat lek�r�se
            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam ugyiratGetExecParam = execParam.Clone();
            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratService.Get(ugyiratGetExecParam).Record;

            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52403);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52403);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Visszautasithato(statusz, execParam))
            {
                // hiba nem visszautasithato
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52230);
            }

            ExecParam kikeroExecParam = execParam.Clone();
            kikeroExecParam.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;

            if (!CanFelhasznaloJovahagy(kikeroExecParam, kikeroExecParam.Felhasznalo_Id, ugyirat))
            {
                throw new ResultException(52412);
            }

            EREC_IrattariKikero erec_IrattariKikero = erec_IrattariKikeroStatusz;
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.Ver = true;

            // visszautas�tott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Visszautasitott;
            erec_IrattariKikero.Updated.Allapot = true;

            // visszautasitas felhasznalo es ido beallitasa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;

            erec_IrattariKikero.VisszaadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.VisszaadasDatuma = true;



            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                //ContextUtil.SetAbort();

                throw new ResultException(result);
            }

            #region �gyirat �llapot�nak m�dos�t�sa iratt�rban �rz�ttre

            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);
            ugyirat.Base.Updated.Ver = true;

            ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
            ugyirat.Updated.Allapot = true;

            Result ugyiratUpdateResult = ugyiratService.Update(ugyiratGetExecParam, ugyirat);
            if (!string.IsNullOrEmpty(ugyiratUpdateResult.ErrorCode))
            {
                throw new ResultException(ugyiratUpdateResult);
            }

            #endregion

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IrattariKikero.UgyUgyirat_Id, "EREC_UgyUgyiratok", "AtmenetiIrattarKolcsonzesElutasitasa").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion

    #region �gyirat Kolcsonzes Stornozasa
    /// <summary>
    /// �gyirat Kolcsonzes Stornozasa
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result KolcsonzesSztorno(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52403);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52403);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);
            ErrorDetails errorDetail = null;

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Sztornozhato(statusz, execParam, out errorDetail))
            {
                // hiba nem sztornozhato
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52406);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52406, errorDetail);
            }

            erec_IrattariKikeroStatusz.Updated.SetValueAll(false);
            erec_IrattariKikeroStatusz.Base.Updated.SetValueAll(false);

            erec_IrattariKikeroStatusz.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikeroStatusz.Base.Updated.Ver = true;

            // sztornozott allapot beallitasa:
            erec_IrattariKikeroStatusz.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Sztornozott;
            erec_IrattariKikeroStatusz.Updated.Allapot = true;

            erec_IrattariKikeroStatusz.VisszaadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikeroStatusz.Updated.VisszaadasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikeroStatusz);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                //ContextUtil.SetAbort();

                throw new ResultException(result);
            }

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            execParam.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;
            result = ugyiratService.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result.Record;

            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);
            ugyirat.Base.Updated.Ver = true;

            ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
            ugyirat.Updated.Allapot = true;

            execParam.Record_Id = ugyirat.Id;
            result = ugyiratService.Update(execParam, ugyirat);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "KolcsonzesVisszavonas").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }
    #endregion

    #region �gyirat Kolcsonzes Kiadasa
    /// <summary>
    /// �gyirat Kolcsonzes Kiadasa
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KolcsonzesKiadas(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba
                throw new ResultException(52403);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Kiadhato(statusz, execParam))
            {
                // hiba nem kiadhato
                throw new ResultException(52407);
            }

            // Ugyirat allapotat es feleloset allitani kell

            // �gyirat lek�r�se:
            if (!String.IsNullOrEmpty(erec_IrattariKikeroStatusz.UgyUgyirat_Id))
            {
                EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam execParam_UgyiratGet = execParam.Clone();
                execParam_UgyiratGet.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;

                Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratGet);
                }
                else
                {
                    if (result_ugyiratGet.Record == null)
                    {
                        // hiba
                        throw new ResultException(52408);
                    }

                    EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                    Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                        Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                    ExecParam execParam_atveheto = execParam.Clone();

                    //kiadhato-e?
                    ErrorDetails errorDetail;
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.KiadhatoKozpontiIrattarbol(ugyiratStatusz, out errorDetail) == false)
                    {
                        // nem kiadhat�:
                        throw new ResultException(52407, errorDetail);
                    }

                    // �GYIRAT UPDATE:
                    // mez�k m�dos�that�s�g�nak letilt�sa:
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    // M�dos�tani kell: az allapotot, felel�st

                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                    erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                    erec_UgyUgyirat.Csoport_Id_Felelos = erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_UgyiratUpdate);
                    }


                    #region K�zbes�t�si t�tel l�trehoz�sa

                    EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                    ExecParam execParam_KezbesitesiTetelInsert = execParam.Clone();

                    string ugyirat_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;

                    /// ha van valami�rt m�r k�zbes�t�si t�tel a rekordra (�tad�sra kijel�lt, vagy �tadott �llapot�),
                    /// azt �rv�nytelen�teni m�g az �j l�trehoz�sa el�tt
                    /// 
                    Result result_inv =
                        service_KezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_KezbesitesiTetelInsert, ugyirat_Id);
                    if (!String.IsNullOrEmpty(result_inv.ErrorCode))
                    {
                        throw new ResultException(result_inv);
                    }

                    EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();


                    kezbesitesiTetel.Obj_Id = ugyirat_Id;
                    kezbesitesiTetel.Updated.Obj_Id = true;

                    // TODO: ObjTip_Id -t is k�ne majd �ll�tani valamire
                    // helyette: Obj_type a t�bla neve lesz (EREC_UgyUgyiratok)

                    kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                    kezbesitesiTetel.Updated.Obj_type = true;

                    // �tad�b�l k�t mez� is van; nem tom melyik mire j�, �gyhogy t�lt�m mind a kett�t

                    kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = execParam.Felhasznalo_Id;
                    kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                    kezbesitesiTetel.Felhasznalo_Id_Atado_USER = execParam.Felhasznalo_Id;
                    kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                    kezbesitesiTetel.Csoport_Id_Cel = erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero;
                    kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                    // �llapot �ll�t�sa:
                    kezbesitesiTetel.Allapot = Contentum.eUtility.Constants.KezbesitesiTetel_Allapot.Atadasra_kijelolt;
                    kezbesitesiTetel.Updated.Allapot = true;


                    Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);

                    if (result_kezbesitesiTetel_Insert.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_kezbesitesiTetel_Insert);
                    }

                    #endregion


                }
            }

            EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            // jovahagyott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            erec_IrattariKikero.Updated.Allapot = true;

            // jovahagyo felhasznalo es ido beallitasa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Kiado = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Kiado = true;

            erec_IrattariKikero.KiadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.KiadasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IrattariKikeroStatusz.UgyUgyirat_Id, "EREC_UgyUgyiratok", "IrattarKolcsonzesKiadasa").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }
    #endregion

    #region �gyirat Kolcsonzes �tv�tel
    /// <summary>
    /// �gyirat Kolcsonzes �tv�te
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result KolcsonzesAtvetel(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52403);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52403);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            // TODO: Ugyirat allapotat es feleloset allitani kell,  !!!! FUGG a felhasznalasi celtol!!!!

            // �gyirat lek�r�se:
            if (!String.IsNullOrEmpty(erec_IrattariKikeroStatusz.UgyUgyirat_Id))
            {
                EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam execParam_UgyiratGet = execParam.Clone();
                execParam_UgyiratGet.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;

                Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    // hiba:
                    //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                    //return result_ugyiratGet;

                    throw new ResultException(result_ugyiratGet);
                }
                else
                {
                    if (result_ugyiratGet.Record == null)
                    {
                        // hiba
                        //Result result1 = ResultError.CreateNewResultWithErrorCode(52408);
                        //log.WsEnd(execParam_UgyiratGet, result1);
                        //return result1;

                        throw new ResultException(52408);
                    }

                    EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                    Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                        Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                    ExecParam execParam_atveheto = execParam.Clone();


                    // �GYIRAT UPDATE:
                    // mez�k m�dos�that�s�g�nak letilt�sa:
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    // M�dos�tani kell: az allapotot, 

                    /// �llapot be�ll�t�sa:
                    /// Ha betekintesre
                    if (erec_IrattariKikeroStatusz.FelhasznalasiCel == "B")
                    {
                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott;
                        erec_UgyUgyirat.Updated.Allapot = true;
                    }
                    // ha ugyintezesre
                    if (erec_IrattariKikeroStatusz.FelhasznalasiCel == "U")
                    {
                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                        erec_UgyUgyirat.Updated.Allapot = true;
                    }

                    erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = execParam.Felhasznalo_Id;
                    erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                    erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                    {
                        // hiba:
                        //ContextUtil.SetAbort();
                        //log.WsEnd(execParam_ugyiratUpdate, result_UgyiratUpdate);
                        //return result_UgyiratUpdate;

                        throw new ResultException(result_UgyiratUpdate);
                    }

                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }
    #endregion

    #region �gyirat Kolcsonzes Visszaadasa
    /// <summary>
    /// �gyirat Kolcsonzes Visszaadasa
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result KolcsonzesVissza(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52403);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52403);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Visszaadhato(statusz, execParam))
            {
                // hiba nem visszaadhato
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52408);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52408);
            }

            // TODO: Ugyirat allapotat es feleloset allitani kell,  !!!! FUGG a felhasznalasi celtol!!!!


            // �gyirat lek�r�se:
            if (!String.IsNullOrEmpty(erec_IrattariKikeroStatusz.UgyUgyirat_Id))
            {
                EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam execParam_UgyiratGet = execParam.Clone();
                execParam_UgyiratGet.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;

                Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    // hiba:
                    //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                    //return result_ugyiratGet;

                    throw new ResultException(result_ugyiratGet);
                }
                else
                {
                    if (result_ugyiratGet.Record == null)
                    {
                        // hiba
                        //Result result1 = ResultError.CreateNewResultWithErrorCode(52410);
                        //log.WsEnd(execParam_UgyiratGet, result1);
                        //return result1;

                        throw new ResultException(52410);
                    }

                    EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                    Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                        Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                    ExecParam execParam_atveheto = execParam.Clone();

                    // �tmemeniti irattarbol elkert-e?
                    //if (Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbolElkert(
                    //        ugyiratStatusz, execParam_atveheto) == false && Contentum.eRecord.BaseUtility.Ugyiratok.KozpontiIrattarbolElkert(
                    //        ugyiratStatusz, execParam_atveheto) == false)
                    //{
                    //    // hiba, nem elkert az irattarbol: 
                    //    //Result result1 = ResultError.CreateNewResultWithErrorCode(52411);
                    //    //log.WsEnd(execParam_atveheto, result1);
                    //    //return result1;

                    //    throw new ResultException(52411);
                    //}

                    // �GYIRAT UPDATE:
                    // mez�k m�dos�that�s�g�nak letilt�sa:
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    // M�dos�tani kell: az allapotot, 

                    /// �llapot be�ll�t�sa:
                    /// Ha betekintesre
                    if (erec_IrattariKikeroStatusz.FelhasznalasiCel == "B")
                    {
                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
                        erec_UgyUgyirat.Updated.Allapot = true;
                    }
                    else
                    {
                        // hiba, mert nem adhato vissza az olyan ugyirat, ami nem betekintesre lett kolcsonozve
                        //Result result1 = ResultError.CreateNewResultWithErrorCode(52409);
                        //log.WsEnd(execParam_atveheto, result1);
                        //return result1;

                        throw new ResultException(52409);
                    }

                    // �gyirat felel�s�nek iratt�rat kell be�ll�tani
                    erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;
                    erec_UgyUgyirat.Csoport_Id_Felelos = erec_IrattariKikeroStatusz.Irattar_Id;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;
                    //iratt�rba k�ld�s d�tum�nak be�ll�t�sa (CR 2114)
                    erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
                    erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                    {
                        // hiba:
                        //ContextUtil.SetAbort();
                        //log.WsEnd(execParam_ugyiratUpdate, result_UgyiratUpdate);
                        //return result_UgyiratUpdate;

                        throw new ResultException(result_UgyiratUpdate);
                    }

                }
            }

            // iratt�ri kik�r� mez�inek m�dos�t�sa (az �llapotot �s visszavev�t �tv�teln�l kell �ll�tani)
            EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            //// jovahagyott allapot beallitasa:
            //erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott;
            //erec_IrattariKikero.Updated.Allapot = true;

            //// jovahagyo felhasznalo es ido beallitasa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Visszaad = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Visszaad = true;

            erec_IrattariKikero.VisszaadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.VisszaadasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                //ContextUtil.SetAbort();

                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IrattariKikeroStatusz.UgyUgyirat_Id, "EREC_UgyUgyiratok", "KozpontiIrattarKolcsonzesVisszavetel").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }
    #endregion

    #region �gyirat kik�r�s �tmeneti iratt�rb�l

    /// <summary>
    /// �gyirat �tmeneti iratt�rb�l kik�r�se
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KikeresAtmenetiIrattarbol(ExecParam execParam, string ugyiratId, EREC_IrattariKikero erec_IrattariKikero, bool WithoutLeaderCheck)
    {
        return KikeresAtmenetiIrattarbol(execParam, ugyiratId, erec_IrattariKikero, false, WithoutLeaderCheck);
    }

    public Result KikeresAtmenetiIrattarbol(ExecParam execParam, string ugyiratId, EREC_IrattariKikero erec_IrattariKikero, bool automatikusJovahagyasKell, bool WithoutLeaderCheck)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ugyiratId))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52710);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // �gyirat lek�r�se:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = ugyiratId;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ErrorDetails errorDetail = null;

            //Kik�rhet�-e?
            if (Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbolKikerheto(ugyiratStatusz, execParam.Clone(), out errorDetail) == false)
            {
                // Az �gyiratra nem ind�that� kik�r�s
                throw new ResultException(52712, errorDetail);
            }

            //j�v�hagy�s
            bool kikeresJovahagyassal = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTAROZAS_ATMENETI_AZONOS_KEZELES, false);
            bool jovahagyoUgyfelelos = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTAROZAS_JOVAHAGYO_UGYFELELOS, false);
            string vezetoID = String.Empty;
            // ha ez true, nem ind�tunk j�v�hagy�st (iratt�rba k�ld�tt �gyiratra nem kell ind�tani)
            bool disableJovahagyas = false;

            if (kikeresJovahagyassal)
            {
                //LZS - BUG_11549
                if (!WithoutLeaderCheck)
                {
                    ExecParam xpmVezeto = execParam.Clone();
                    //BLG_6556: �gyfelel�s szervezeti egys�g vezet�je hagyja j�v� a kik�r�st. 
                    if (jovahagyoUgyfelelos && !String.IsNullOrEmpty(erec_UgyUgyirat.Csoport_Id_Ugyfelelos))
                    {
                        xpmVezeto.FelhasznaloSzervezet_Id = erec_UgyUgyirat.Csoport_Id_Ugyfelelos;
                    }
                    vezetoID = Csoportok.GetFirstRightedLeader_ID(xpmVezeto, true, Contentum.eUtility.Constants.Funkcio.UgyiratAtmenetiIrattarKikeresJovahagyas);

                    if (String.IsNullOrEmpty(vezetoID))
                    {
                        throw new ResultException(53202); // Nem tal�lhat� az aktu�lis felhaszn�l� f�l� rendelt, adott jogosults�ggal rendelkez� vezet� a szervezeti hierarchi�ban!
                    }
                }//LZS - BUG_11549
                else
                {
                    vezetoID = erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy;
                }

                if (!automatikusJovahagyasKell)
                {
                    if (CanFelhasznaloJovahagyKikeres(execParam, erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero, false, erec_UgyUgyirat))
                    {
                        if (jovahagyoUgyfelelos)
                        {
                            automatikusJovahagyasKell = IsSajatSzervezet(execParam, vezetoID);
                        }
                        else
                        {
                            automatikusJovahagyasKell = true;
                        }
                    }
                }
            }

            // Ha az �gyirat Iratt�rba k�ld�tt, nem kell �ll�tani az �llapotot. (Ilyenkor majd az iratt�rba v�telkor kell ezt be�ll�tani.)
            // Ha Iratt�rban �rz�tt az �llapot, akkor Iratt�rb�l elk�rt-re �ll�tjuk
            if (erec_UgyUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
            {
                // �GYIRAT UPDATE:
                // mez�k m�dos�that�s�g�nak letilt�sa:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // M�dos�tani kell: az allapotot, 

                /// �llapot be�ll�t�sa:
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert;
                erec_UgyUgyirat.Updated.Allapot = true;

                if (kikeresJovahagyassal)
                {
                    // Kik�r�s j�v�hagy�j�t bejegyezz�k a k�vetkez� felel�sbe is
                    erec_UgyUgyirat.Kovetkezo_Felelos_Id = vezetoID;
                    erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;
                }

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_UgyiratUpdate);
                }
            }
            else
            {
                disableJovahagyas = true;
            }

            ExecParam IrattariKikeroExecParam = execParam.Clone();
            IrattariKikeroExecParam.Record_Id = "";

            // Elso allapot:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto;
            erec_IrattariKikero.Updated.Allapot = true;

            // Iratt�r ID-j�nak be�ll�tani az �gyirat felel�s�t (vagyis az iratt�rat)
            erec_IrattariKikero.Irattar_Id = erec_UgyUgyirat.Csoport_Id_Felelos;
            erec_IrattariKikero.Updated.Irattar_Id = true;

            // �gyirathoz k�t�s:
            erec_IrattariKikero.UgyUgyirat_Id = ugyiratId;
            erec_IrattariKikero.Updated.UgyUgyirat_Id = true;

            // K�lcs�nz�si d�tumokat nem �ll�tjuk:
            erec_IrattariKikero.Updated.KikerKezd = false;
            erec_IrattariKikero.Updated.KikerVege = false;

            if (kikeresJovahagyassal)
            {
                erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = vezetoID;
                erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;
            }

            Result IrattariKikeroResult = this.Insert(IrattariKikeroExecParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(IrattariKikeroResult.ErrorCode))
            {
                // hiba:
                throw new ResultException(IrattariKikeroResult);
            }

            result = IrattariKikeroResult;

            if (kikeresJovahagyassal)
            {
                if (automatikusJovahagyasKell && !disableJovahagyas)
                {
                    #region K�lcs�nz�s j�v�hagy�sa

                    Logger.Info("Automatikus kik�r�s j�v�hagy�s...", execParam);

                    ExecParam execParam_jovahagyas = execParam.Clone();
                    // RecordId-ba tessz�k az �gyirat Id-t:
                    execParam_jovahagyas.Record_Id = erec_UgyUgyirat.Id;

                    Result result_jovahagyas = this.KikeresJovahagyas(execParam_jovahagyas, false);
                    if (result_jovahagyas.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_jovahagyas);
                    }

                    #endregion
                }
            }


            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "AtmenetiIrattarKolcsonzes").Record;

            eventLogRecord.Base.Note = erec_IrattariKikero.Keres_note;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// �gyiratok �tmeneti iratt�rb�l kik�r�se
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KikeresAtmenetiIrattarbol_Tomeges(ExecParam execParam, string[] UgyiratIdArray, EREC_IrattariKikero erec_IrattariKikero, bool WithoutLeaderCheck)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Param�terek ellen�rz�se:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || (UgyiratIdArray == null || UgyiratIdArray.Length == 0))
            {
                // hiba
                throw new ResultException(52710);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            bool kikeresJovahagyassal = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTAROZAS_ATMENETI_AZONOS_KEZELES, false);

            if (kikeresJovahagyassal)
            {
                foreach (string ugyiratId in UgyiratIdArray)
                {
                    result = KikeresAtmenetiIrattarbol(execParam, ugyiratId, erec_IrattariKikero, WithoutLeaderCheck);

                    if (result.IsError)
                    {
                        throw new ResultException(result);
                    }
                }
            }
            else
            {

                // �gyirat lek�r�se:
                EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam xpmUgyiratokGetAll = execParam.Clone();

                EREC_UgyUgyiratokSearch searchUgyiratokGetAll = new EREC_UgyUgyiratokSearch();
                searchUgyiratokGetAll.Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
                searchUgyiratokGetAll.Id.Operator = Query.Operators.inner;

                Result resUgyiratokGetAll = erec_UgyUgyiratokService.GetAll(xpmUgyiratokGetAll, searchUgyiratokGetAll);
                if (resUgyiratokGetAll.IsError)
                {
                    // hiba:
                    throw new ResultException(resUgyiratokGetAll);
                }

                System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), resUgyiratokGetAll.Ds);
                if (statusz.Count == 0)
                    throw new ResultException(52710);

                ErrorDetails errorDetail = null;

                string ugyiratIds = string.Empty;
                List<string> lstUgyiratIds = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);
                string selectedVers = string.Empty;
                List<string> lstVers = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);

                string ugyiratCsoport_Id_Felelos = String.Empty;
                foreach (System.Data.DataRow r in resUgyiratokGetAll.Ds.Tables[0].Rows)
                {
                    ugyiratIds += ",'" + r["Id"].ToString() + "'";
                    lstUgyiratIds.Add(r["Id"].ToString());
                    selectedVers += "," + r["Ver"].ToString();
                    lstVers.Add(r["Ver"].ToString());
                    ugyiratCsoport_Id_Felelos = r["Csoport_Id_Felelos"].ToString();

                    //Kik�rhet�-e?
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbolKikerheto((statusz[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)
                        , execParam.Clone(), out errorDetail) == false)
                    {
                        // Az �gyiratra nem ind�that� kik�r�s
                        throw new ResultException(52712, errorDetail);
                    }
                }

                ugyiratIds = ugyiratIds.TrimStart(',');
                selectedVers = selectedVers.TrimStart(',');


                // �GYIRAT UPDATE:
                // mez�k m�dos�that�s�g�nak letilt�sa:
                EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // M�dos�tani kell: az allapotot, 

                /// �llapot be�ll�t�sa:
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert;
                erec_UgyUgyirat.Updated.Allapot = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();

                Result result_UgyiratUpdate =
                    erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, resUgyiratokGetAll.Ds.Tables[0], lstUgyiratIds.ToArray(), lstVers.ToArray(), erec_UgyUgyirat, DateTime.Now);
                if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_UgyiratUpdate);
                }

                ExecParam IrattariKikeroExecParam = execParam.Clone();
                IrattariKikeroExecParam.Record_Id = "";

                // Elso allapot:
                erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto;
                erec_IrattariKikero.Updated.Allapot = true;

                // Iratt�r ID-j�nak be�ll�tani az �gyirat felel�s�t (vagyis az iratt�rat)
                erec_IrattariKikero.Irattar_Id = ugyiratCsoport_Id_Felelos;
                erec_IrattariKikero.Updated.Irattar_Id = true;

                // �gyirathoz k�t�s:
                //erec_IrattariKikero.UgyUgyirat_Id = ugyiratId;
                //erec_IrattariKikero.Updated.UgyUgyirat_Id = true;

                // K�lcs�nz�si d�tumokat nem �ll�tjuk:
                erec_IrattariKikero.Updated.KikerKezd = false;
                erec_IrattariKikero.Updated.KikerVege = false;

                Result IrattariKikeroResult = this.Insert_Tomeges(IrattariKikeroExecParam, UgyiratIdArray, erec_IrattariKikero);
                if (!String.IsNullOrEmpty(IrattariKikeroResult.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(IrattariKikeroResult);
                }

                result = IrattariKikeroResult;

                #region Esem�nynapl�z�s
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ugyiratIds, "AtmenetiIrattarKolcsonzes", erec_IrattariKikero.Keres_note);
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_IrattariKikero::AtmenetiIrattarKolcsonzes: ", execParam, eventLogResult);
                #endregion
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private bool CanFelhasznaloJovahagyKikeres(ExecParam p_ExecParam, string p_FelhasznaloID, bool WithRightCheck, EREC_UgyUgyiratok ugyirat)
    {
        Logger.Debug("CanFelhasznaloJovahagyKikeres start");
        if (String.IsNullOrEmpty(p_FelhasznaloID))
        {
            Logger.Debug("CanFelhasznaloJovahagyKikeres p_FelhasznaloID is null or empty");
            return false;
        }

        // az aktu�lis felhaszn�l�t n�zz�k, amikor k�zi j�v�hagy�s van, �s a kik�r�t, ha automatikus
        string User_Id_ToCheck = (WithRightCheck ? p_ExecParam.Felhasznalo_Id : p_FelhasznaloID);

        // a k�lcs�nz�st csak a kik�r� vezet�je hagyhatja j�v�, kiv�ve, ha kik�r�skor automatikusan j�v� is hagyjuk
        if (WithRightCheck)
        {
            Result kikeroGetResult = this.GetByUgyiratId(p_ExecParam);
            if (!string.IsNullOrEmpty(kikeroGetResult.ErrorCode))
            {
                Logger.Error("CanFelhasznaloJovahagy hiba", p_ExecParam, kikeroGetResult);
                throw new ResultException(kikeroGetResult);
            }

            EREC_IrattariKikero kikero = (EREC_IrattariKikero)kikeroGetResult.Record;

            // a k�lcs�nz�st csak a kik�r� vezet�je hagyhatja j�v�
            //if (kikero.FelhasznaloCsoport_Id_Jovahagy != p_FelhasznaloID)
            if (kikero.FelhasznaloCsoport_Id_Jovahagy != User_Id_ToCheck)
            {
                return false;
            }
        }

        #region Ha az �gyirat �gyint�z�je k�rte ki az �gyiratot, akkor j�v�hagyhat�, funkci�jogt�l f�ggetlen�l

        if (IsUgyintezoAutomatikusJovahagyasEnabled(p_ExecParam.Clone()) &&
            ugyirat != null && ugyirat.FelhasznaloCsoport_Id_Ugyintez == p_FelhasznaloID)
        {
            return true;
        }


        #endregion Ha az �gyirat �gyint�z�je k�rte ki az �gyiratot, akkor j�v�hagyhat�, funkci�jogt�l f�ggetlen�l


        if (p_ExecParam.Felhasznalo_Id.Equals(p_FelhasznaloID, StringComparison.InvariantCultureIgnoreCase))
        {
            return FunctionRights.HasFunctionRight(p_ExecParam, Contentum.eUtility.Constants.Funkcio.UgyiratAtmenetiIrattarKikeresJovahagyas);
        }
        else
        {
            KRT_FunkciokService svcFunkciok = eAdminService.ServiceFactory.GetKRT_FunkciokService();
            KRT_Felhasznalok krtFelhasznalok = new KRT_Felhasznalok();
            //krtFelhasznalok.Id = p_FelhasznaloID;
            krtFelhasznalok.Id = User_Id_ToCheck;
            Result res = svcFunkciok.GetAllByFelhasznalo(p_ExecParam, krtFelhasznalok, null);

            if (res.IsError)
            {
                Logger.Error("CanFelhasznaloJovahagyKikeres hiba", p_ExecParam, res);
                throw new ResultException(res);
            }
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string szerepkorKod = row["Kod"].ToString();
                if (String.Compare(szerepkorKod, Contentum.eUtility.Constants.Funkcio.UgyiratAtmenetiIrattarKikeresJovahagyas, true) == 0)
                {
                    Logger.Debug("CanFelhasznaloJovahagyKikeres return true");
                    return true;
                }
            }
        }
        Logger.Debug("CanFelhasznaloJovahagyKikeres end");
        return false;
    }

    #endregion

    #region �gyirat �tmeneti iratt�rb�l kik�r�s J�v�hagy�sa

    /// <summary>
    /// �tmeneti iratt�rb�l kik�rt �gyiratok j�v�hagy�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="UgyiratIdArray"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KikeresJovahagyas_Tomeges(ExecParam execParam, string[] UgyiratIdArray)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Param�terek ellen�rz�se:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || (UgyiratIdArray == null || UgyiratIdArray.Length == 0))
            {
                // hiba
                throw new ResultException(52717);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string ugyiratId in UgyiratIdArray)
            {
                ExecParam xpm = execParam.Clone();
                xpm.Record_Id = ugyiratId;

                result = KikeresJovahagyas(xpm);

                if (result.IsError)
                {
                    throw new ResultException(result);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// �gyirat Kolcsonzes Jovahagyasa
    /// 2008.09.04: A felel�st (kezel�t) nem itt �ll�tjuk, hanem majd a KolcsonzesKiadas-n�l
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    public Result KikeresJovahagyas(ExecParam execParam)
    {
        return KikeresJovahagyas(execParam, true);
    }

    private Result KikeresJovahagyas(ExecParam execParam, bool WithRightCheck)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52400);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //�gyirat lek�r�se
            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam ugyiratGetExecParam = execParam.Clone();
            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratService.Get(ugyiratGetExecParam).Record;

            #region iratt�ri kik�r� t�tel record m�dos�t�sa

            string ugyiratId = execParam.Record_Id;
            Result _irattariTetelGetResult = this.GetByUgyiratId(execParam);

            if (!String.IsNullOrEmpty(_irattariTetelGetResult.ErrorCode) || _irattariTetelGetResult.Record == null)
            {
                // hiba
                throw new ResultException(52721);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)_irattariTetelGetResult.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            #region RightCheck
            // itt az eredeti execParamban m�r a kik�r� Id-ja van, nem az �gyirat�
            ExecParam execParam_jovahagy = execParam.Clone();
            execParam_jovahagy.Record_Id = ugyiratId;
            // ha van jogvizsg�lat, akkor az aktu�lis felhaszn�l�t n�zz�k, ha nincs, a kik�r�t
            if (!this.CanFelhasznaloJovahagyKikeres(execParam_jovahagy, erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero, WithRightCheck, ugyirat))
            {
                throw new ResultException(52713);
            }

            #endregion RightCheck

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Jovahagyhato(statusz, execParam))
            {
                // hiba nem jovahagyhato
                throw new ResultException(52715);
            }

            EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            // jovahagyott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett;
            erec_IrattariKikero.Updated.Allapot = true;

            // jovahagyo felhasznalo es ido beallitasa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = (WithRightCheck ? execParam.Felhasznalo_Id : erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero); //execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;

            erec_IrattariKikero.JovagyasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.JovagyasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }
            #endregion

            #region �gyirat m�dos�t�sa

            // �llapotot m�dos�tjuk, felel�st nem, az a KolcsonzesKiadas-n�l v�ltozik


            // Ha az �gyirat Iratt�rba k�ld�tt, nem kell �ll�tani az �llapotot.
            // (Akkor lehet ilyen, ha iratt�rba k�ld�tt �gyiratba iktattak, �s indult egy k�lcs�nz�si folyamat (de m�g nincs k�zponti iratt�rba v�ve))    
            if (ugyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
            {
                ugyirat.Updated.SetValueAll(false);
                ugyirat.Base.Updated.SetValueAll(false);
                ugyirat.Base.Updated.Ver = true;
                //ugyirat.Csoport_Id_Felelos = erec_IrattariKikeroStatusz.FelhasznaloCsoport_Id_Kikero;
                //ugyirat.Updated.Csoport_Id_Felelos = true;

                ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo;
                ugyirat.Updated.Allapot = true;

                // EB 2011.01.24:
                // CR#2602: Kik�r� j�v�hagy�j�t bejegyezz�k a k�vetkez� felel�sbe is,
                // hogy az �gyiratokra a list�kon sz�rni lehessen, ez�rt itt t�r�lj�k
                // - pl. feladataim k�lcs�nz�sre j�v�hagyand�k link
                ugyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
                ugyirat.Updated.Kovetkezo_Felelos_Id = true;

                Result ugyiratUpdateResult = ugyiratService.Update(ugyiratGetExecParam, ugyirat);
                if (!string.IsNullOrEmpty(ugyiratUpdateResult.ErrorCode))
                {
                    throw new ResultException(ugyiratUpdateResult);
                }
            }

            #endregion

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyiratGetExecParam.Record_Id, "EREC_UgyUgyiratok", "AtmenetiIrattarKikeresJovahagyas").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion

    #region �gyirat �tmeneti iratt�rb�l kik�r�s visszautas�t�sa

    /// <summary>
    /// �tmeneti iratt�rb�l kik�rt �gyiratok visszautas�t�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="UgyiratIdArray"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KikeresVisszautasitas_Tomeges(ExecParam execParam, string[] UgyiratIdArray)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Param�terek ellen�rz�se:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || (UgyiratIdArray == null || UgyiratIdArray.Length == 0))
            {
                // hiba
                throw new ResultException(52718);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string ugyiratId in UgyiratIdArray)
            {
                ExecParam xpm = execParam.Clone();
                xpm.Record_Id = ugyiratId;

                result = KikeresVisszautasitas(xpm);

                if (result.IsError)
                {
                    throw new ResultException(result);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// �gyirat Kolcsonzes Visszautasitas
    /// </summary>
    /// <param name="execParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id a amit atvesz</param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    public Result KikeresVisszautasitas(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            return ResultError.CreateNewResultWithErrorCode(52400);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //�gyirat lek�r�se
            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam ugyiratGetExecParam = execParam.Clone();
            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratService.Get(ugyiratGetExecParam).Record;

            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52403);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52721);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Visszautasithato(statusz, execParam))
            {
                // hiba nem visszautasithato
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52716);
            }

            ExecParam kikeroExecParam = execParam.Clone();
            kikeroExecParam.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;

            if (!CanFelhasznaloJovahagy(kikeroExecParam, kikeroExecParam.Felhasznalo_Id, ugyirat))
            {
                throw new ResultException(52714);
            }

            EREC_IrattariKikero erec_IrattariKikero = erec_IrattariKikeroStatusz;
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.Ver = true;

            // visszautas�tott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Visszautasitott;
            erec_IrattariKikero.Updated.Allapot = true;

            // visszautasitas felhasznalo es ido beallitasa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = true;

            erec_IrattariKikero.VisszaadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.VisszaadasDatuma = true;



            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                //ContextUtil.SetAbort();

                throw new ResultException(result);
            }

            #region �gyirat �llapot�nak m�dos�t�sa iratt�rban �rz�ttre

            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);
            ugyirat.Base.Updated.Ver = true;

            ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
            ugyirat.Updated.Allapot = true;

            Result ugyiratUpdateResult = ugyiratService.Update(ugyiratGetExecParam, ugyirat);
            if (!string.IsNullOrEmpty(ugyiratUpdateResult.ErrorCode))
            {
                throw new ResultException(ugyiratUpdateResult);
            }

            #endregion

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_IrattariKikero.UgyUgyirat_Id, "EREC_UgyUgyiratok", "AtmenetiIrattarKolcsonzesElutasitasa").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion

    #region �tmeneti iratt�rb�l kik�r�s sztorn�
    /// <summary>
    /// �gyirat �tmeneti iratt�rb�l val� kik�r�s�nek visszavon�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KikeresSztorno(ExecParam execParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ugyiratId))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52720);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            execParam.Record_Id = ugyiratId;

            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            if (result.Record == null)
            {
                // hiba
                throw new ResultException(52721);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);
            ErrorDetails errorDetail = null;

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Sztornozhato(statusz, execParam, out errorDetail))
            {
                // hiba nem sztornozhato
                throw new ResultException(52722, errorDetail);
            }

            erec_IrattariKikeroStatusz.Updated.SetValueAll(false);
            erec_IrattariKikeroStatusz.Base.Updated.SetValueAll(false);

            erec_IrattariKikeroStatusz.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikeroStatusz.Base.Updated.Ver = true;

            // sztornozott allapot beallitasa:
            erec_IrattariKikeroStatusz.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Sztornozott;
            erec_IrattariKikeroStatusz.Updated.Allapot = true;

            erec_IrattariKikeroStatusz.VisszaadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikeroStatusz.Updated.VisszaadasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikeroStatusz);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            execParam.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;
            result = ugyiratService.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result.Record;

            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);
            ugyirat.Base.Updated.Ver = true;

            ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
            ugyirat.Updated.Allapot = true;

            execParam.Record_Id = ugyirat.Id;
            result = ugyiratService.Update(execParam, ugyirat);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyiratId, "EREC_UgyUgyiratok", "AtmenetiIrattarKikeresVisszavonas").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    #endregion

    #region KiadasAtmenetiIrattarbol

    /// <summary>
    /// Kik�rt �gyirat kiad�sa �tmeneti iratt�rb�l
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KiadasAtmenetiIrattarbol(ExecParam execParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ugyiratId))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52730);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iratt�ri kik�r� t�tel record m�dos�t�sa

            execParam.Record_Id = ugyiratId;
            Result _irattariTetelGetResult = this.GetByUgyiratId(execParam);

            if (!String.IsNullOrEmpty(_irattariTetelGetResult.ErrorCode) || _irattariTetelGetResult.Record == null)
            {
                // hiba
                throw new ResultException(52731);
            }

            EREC_IrattariKikero erec_IrattariKikero = (EREC_IrattariKikero)_irattariTetelGetResult.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikero);

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.KiadhatoAtmenetiIrattarbol(statusz, execParam))
            {
                // hiba nem adhat� ki
                throw new ResultException(52732);
            }

            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikero.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            // Kiadott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            erec_IrattariKikero.Updated.Allapot = true;

            // kiad� szem�ly �s kiad�si id�pont �ll�t�sa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Kiado = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Kiado = true;

            erec_IrattariKikero.KiadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.KiadasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            #endregion



            // Ugyirat allapotat es feleloset allitani kell

            // �gyirat lek�r�se:
            if (!String.IsNullOrEmpty(erec_IrattariKikero.UgyUgyirat_Id))
            {
                EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
                ExecParam execParam_UgyiratGet = execParam.Clone();
                execParam_UgyiratGet.Record_Id = erec_IrattariKikero.UgyUgyirat_Id;

                Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratGet);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);

                //kiadhato-e?
                if (Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbolElkert(ugyiratStatusz, execParam.Clone()) == false)
                {
                    // nem az irattarban van az ugyirat:
                    throw new ResultException(52732);
                }

                // �GYIRAT UPDATE:
                // mez�k m�dos�that�s�g�nak letilt�sa:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // M�dos�tani kell: az �llapotot, kezel�t

                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                erec_UgyUgyirat.Updated.Allapot = true;

                if (erec_IrattariKikero.FelhasznalasiCel == "U")
                {
                    erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                }
                else
                {
                    erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                }

                erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                erec_UgyUgyirat.Csoport_Id_Felelos = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
                erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_UgyiratUpdate);
                }
            }


            #region K�zbes�t�si t�tel l�trehoz�sa

            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_KezbesitesiTetelInsert = execParam.Clone();

            String ugyirat_Id = erec_IrattariKikero.UgyUgyirat_Id;

            /// ha van valami�rt m�r k�zbes�t�si t�tel a rekordra (�tad�sra kijel�lt, vagy �tadott �llapot�),
            /// azt �rv�nytelen�teni m�g az �j l�trehoz�sa el�tt
            /// 
            Result result_inv =
                service_KezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_KezbesitesiTetelInsert, ugyirat_Id);
            if (!String.IsNullOrEmpty(result_inv.ErrorCode))
            {
                throw new ResultException(result_inv);
            }

            EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

            kezbesitesiTetel.Obj_Id = ugyirat_Id;
            kezbesitesiTetel.Updated.Obj_Id = true;

            // TODO: ObjTip_Id -t is k�ne majd �ll�tani valamire
            // helyette: Obj_type a t�bla neve lesz (EREC_UgyUgyiratok)

            kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
            kezbesitesiTetel.Updated.Obj_type = true;

            kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = execParam.Felhasznalo_Id;
            kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

            kezbesitesiTetel.Felhasznalo_Id_Atado_USER = execParam.Felhasznalo_Id;
            kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

            kezbesitesiTetel.Csoport_Id_Cel = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
            kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

            // �llapot �ll�t�sa:
            kezbesitesiTetel.Allapot = Contentum.eUtility.Constants.KezbesitesiTetel_Allapot.Atadasra_kijelolt;
            kezbesitesiTetel.Updated.Allapot = true;

            Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);

            if (!String.IsNullOrEmpty(result_kezbesitesiTetel_Insert.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kezbesitesiTetel_Insert);
            }

            #endregion

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyiratId, "EREC_UgyUgyiratok", "AtmenetiIrattarKolcsonzesEngedelyezese").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Kik�rt �gyiratok kiad�sa �tmeneti iratt�rb�l
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KiadasAtmenetiIrattarbol_Tomeges(ExecParam execParam, string[] UgyiratIdArray)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Param�terek ellen�rz�se:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || (UgyiratIdArray == null || UgyiratIdArray.Length == 0))
            {
                // hiba
                throw new ResultException(52730);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iratt�ri kik�r� t�tel record m�dos�t�sa
            ExecParam xpmIrattariKikeroGetAll = execParam.Clone();
            EREC_IrattariKikeroSearch schIrattariKikero = new EREC_IrattariKikeroSearch();
            schIrattariKikero.UgyUgyirat_Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
            schIrattariKikero.UgyUgyirat_Id.Operator = Query.Operators.inner;
            Contentum.eRecord.BaseUtility.IrattariKikero.SetSearchFilterKiadhatoAtmenetiIrattarbol(schIrattariKikero);
            Result resIrattariKikeroGetAll = this.GetAll(xpmIrattariKikeroGetAll, schIrattariKikero);

            if (resIrattariKikeroGetAll.IsError)
            {
                // hiba
                throw new ResultException(52731);
            }

            System.Collections.Hashtable statuszKikero = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByDataSet(execParam.Clone(), resIrattariKikeroGetAll.Ds);
            if (statuszKikero.Count == 0)
            {
                Logger.Error("statuszKikero.Count = 0", execParam);
                throw new ResultException(52730);
            }

            List<string> KikeroIdList = new List<string>();
            List<string> KikeroVerList = new List<string>();

            List<string> UgyiratokUgyintezesre = new List<string>();
            List<string> UgyiratokBetekintesre = new List<string>();

            Dictionary<string, string> FelhasznaloCsoport_Id_Kikero_Dictionary = new Dictionary<string, string>();
            foreach (System.Data.DataRow r in resIrattariKikeroGetAll.Ds.Tables[0].Rows)
            {
                KikeroIdList.Add(r["Id"].ToString());
                KikeroVerList.Add(r["Ver"].ToString());
                string ugyiratId = r["UgyUgyirat_Id"].ToString();
                if (!FelhasznaloCsoport_Id_Kikero_Dictionary.ContainsKey(ugyiratId))
                {
                    FelhasznaloCsoport_Id_Kikero_Dictionary.Add(ugyiratId, r["FelhasznaloCsoport_Id_Kikero"].ToString());
                }

                // statusz ellenorzesek:
                if (!Contentum.eRecord.BaseUtility.IrattariKikero.KiadhatoAtmenetiIrattarbol((statuszKikero[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.IrattariKikero.Statusz), execParam.Clone()))
                {
                    // hiba nem adhat� ki
                    throw new ResultException(52732);
                }

                // sz�tv�logatni melyik �gyiratokat k�rt�k �gyint�z�sre �s melyikeket betekint�sre
                if (r["FelhasznalasiCel"].ToString() == "U")
                    UgyiratokUgyintezesre.Add(ugyiratId);
                else
                    UgyiratokBetekintesre.Add(ugyiratId);
            }



            EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikero.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            // Kiadott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            erec_IrattariKikero.Updated.Allapot = true;

            // kiad� szem�ly �s kiad�si id�pont �ll�t�sa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Kiado = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Kiado = true;

            erec_IrattariKikero.KiadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.KiadasDatuma = true;

            result = this.Update_Tomeges(execParam, KikeroIdList.ToArray(), KikeroVerList.ToArray(), erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            #endregion

            if (UgyiratokUgyintezesre.Count != 0)
            {
                result = this.KiadasAtmenetiIrattarbol_Ugyintezesre_Tomeges(execParam.Clone(), UgyiratokUgyintezesre.ToArray(), FelhasznaloCsoport_Id_Kikero_Dictionary, resIrattariKikeroGetAll);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }

            if (UgyiratokBetekintesre.Count != 0)
            {
                result = this.KiadasAtmenetiIrattarbol_Betekintesre_Tomeges(execParam.Clone(), UgyiratokBetekintesre.ToArray(), FelhasznaloCsoport_Id_Kikero_Dictionary, resIrattariKikeroGetAll);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, Search.GetSqlInnerString(UgyiratIdArray), "AtmenetiIrattarKolcsonzesEngedelyezese");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_IrattariKikero::AtmenetiIrattarbolKiadas: ", execParam, eventLogResult);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private Result KiadasAtmenetiIrattarbol_Ugyintezesre_Tomeges(ExecParam execParam, string[] UgyiratIdArray, Dictionary<string, string> FelhasznaloCsoport_Id_Kikero_Dictionary, Result resIrattariKikeroGetAll)
    {
        // Ugyirat allapotat es feleloset allitani kell

        // �gyirat lek�r�se:
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execParam_UgyiratGetAll = execParam.Clone();

        EREC_UgyUgyiratokSearch schUgyiratok = new EREC_UgyUgyiratokSearch();
        schUgyiratok.Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
        schUgyiratok.Id.Operator = Query.Operators.inner;

        Result resUgyiratokGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, schUgyiratok);
        if (resUgyiratokGetAll.IsError)
        {
            // hiba:
            throw new ResultException(resUgyiratokGetAll);
        }


        System.Collections.Hashtable statuszUgyirat = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), resUgyiratokGetAll.Ds);
        if (statuszUgyirat.Count == 0)
        {
            Logger.Error("statuszUgyirat.Count = 0", execParam);
            throw new ResultException(52730);
        }

        string ugyiratIds = string.Empty;
        List<string> lstUgyiratIds = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);
        string ugyiratVers = string.Empty;
        List<string> lstVers = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);

        List<string> Csoport_Id_Felelos_List = new List<string>();
        foreach (System.Data.DataRow r in resUgyiratokGetAll.Ds.Tables[0].Rows)
        {
            string id = r["Id"].ToString();
            ugyiratIds += ",'" + id + "'";
            lstUgyiratIds.Add(r["Id"].ToString());
            ugyiratVers += "," + r["Ver"].ToString();
            lstVers.Add(r["Ver"].ToString());

            string Csoport_Id_Felelos = String.Empty;
            if (FelhasznaloCsoport_Id_Kikero_Dictionary.ContainsKey(id))
            {
                Csoport_Id_Felelos = FelhasznaloCsoport_Id_Kikero_Dictionary[id];
            }
            Csoport_Id_Felelos_List.Add(Csoport_Id_Felelos);

            //kiadhato-e?
            if (Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbolElkert((statuszUgyirat[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz), execParam.Clone()) == false)
            {
                // nem az irattarban van az ugyirat:
                throw new ResultException(52732);
            }
        }

        ugyiratIds = ugyiratIds.TrimStart(',');
        ugyiratVers = ugyiratVers.TrimStart(',');

        // �GYIRAT UPDATE:
        // mez�k m�dos�that�s�g�nak letilt�sa:
        EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
        erec_UgyUgyirat.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.Ver = true;

        // M�dos�tani kell: az �llapotot, kezel�t

        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
        erec_UgyUgyirat.Updated.Allapot = true;

        erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert;
        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

        //erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
        //erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

        //erec_UgyUgyirat.Csoport_Id_Felelos = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
        //erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

        ExecParam execParam_ugyiratUpdate = execParam.Clone();

        return erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, resIrattariKikeroGetAll.Ds.Tables[0], lstUgyiratIds.ToArray(), lstVers.ToArray(), Csoport_Id_Felelos_List.ToArray(), erec_UgyUgyirat, DateTime.Now);
    }

    private Result KiadasAtmenetiIrattarbol_Betekintesre_Tomeges(ExecParam execParam, string[] UgyiratIdArray, Dictionary<string, string> FelhasznaloCsoport_Id_Kikero_Dictionary, Result resIrattariKikeroGetAll)
    {
        // Ugyirat allapotat es feleloset allitani kell

        // �gyirat lek�r�se:
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execParam_UgyiratGetAll = execParam.Clone();

        EREC_UgyUgyiratokSearch schUgyiratok = new EREC_UgyUgyiratokSearch();
        schUgyiratok.Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
        schUgyiratok.Id.Operator = Query.Operators.inner;

        Result resUgyiratokGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, schUgyiratok);
        if (resUgyiratokGetAll.IsError)
        {
            // hiba:
            throw new ResultException(resUgyiratokGetAll);
        }


        System.Collections.Hashtable statuszUgyirat = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), resUgyiratokGetAll.Ds);
        if (statuszUgyirat.Count == 0)
        {
            Logger.Error("statuszUgyirat.Count = 0", execParam);
            throw new ResultException(52730);
        }

        string ugyiratIds = string.Empty;
        List<string> lstUgyiratIds = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);
        string ugyiratVers = string.Empty;
        List<string> Csoport_Id_Felelos_List = new List<string>();
        List<string> lstVers = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);

        foreach (System.Data.DataRow r in resUgyiratokGetAll.Ds.Tables[0].Rows)
        {
            string id = r["Id"].ToString();
            ugyiratIds += ",'" + id + "'";
            lstUgyiratIds.Add(r["Id"].ToString());
            ugyiratVers += "," + r["Ver"].ToString();
            lstVers.Add(r["Ver"].ToString());

            string Csoport_Id_Felelos = String.Empty;
            if (FelhasznaloCsoport_Id_Kikero_Dictionary.ContainsKey(id))
            {
                Csoport_Id_Felelos = FelhasznaloCsoport_Id_Kikero_Dictionary[id];
            }
            Csoport_Id_Felelos_List.Add(Csoport_Id_Felelos);

            //kiadhato-e?
            if (Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbolElkert((statuszUgyirat[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz), execParam.Clone()) == false)
            {
                // nem az irattarban van az ugyirat:
                throw new ResultException(52732);
            }
        }

        ugyiratIds = ugyiratIds.TrimStart(',');
        ugyiratVers = ugyiratVers.TrimStart(',');

        // �GYIRAT UPDATE:
        // mez�k m�dos�that�s�g�nak letilt�sa:
        EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
        erec_UgyUgyirat.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.Ver = true;

        // M�dos�tani kell: az �llapotot, kezel�t

        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
        erec_UgyUgyirat.Updated.Allapot = true;

        erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott;
        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

        //erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
        //erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

        //erec_UgyUgyirat.Csoport_Id_Felelos = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
        //erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

        ExecParam execParam_ugyiratUpdate = execParam.Clone();

        return erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, resIrattariKikeroGetAll.Ds.Tables[0], lstUgyiratIds.ToArray(), lstVers.ToArray(), Csoport_Id_Felelos_List.ToArray(), erec_UgyUgyirat, DateTime.Now);
    }

    #endregion


    #region �gyirat kik�r�s skontr� iratt�rb�l

    /// <summary>
    /// �gyiratok skontr� iratt�rb�l kik�r�se
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="erec_IrattariKikero"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KikeresSkontroIrattarbol_Tomeges(ExecParam execParam, string[] UgyiratIdArray, EREC_IrattariKikero erec_IrattariKikero)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Param�terek ellen�rz�se:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || (UgyiratIdArray == null || UgyiratIdArray.Length == 0))
            {
                // hiba
                throw new ResultException(52710);
            }

            // r�gi kik�r�k �t�ll�t�sa visszaadottra
            result = this.SetKikerokToVisszaadott_ByUgyiratId(execParam.Clone(), UgyiratIdArray);
            if (!string.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            // �gyirat lek�r�se:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam xpmUgyiratokGetAll = execParam.Clone();

            EREC_UgyUgyiratokSearch searchUgyiratokGetAll = new EREC_UgyUgyiratokSearch();
            searchUgyiratokGetAll.Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
            searchUgyiratokGetAll.Id.Operator = Query.Operators.inner;

            Result resUgyiratokGetAll = erec_UgyUgyiratokService.GetAll(xpmUgyiratokGetAll, searchUgyiratokGetAll);
            if (resUgyiratokGetAll.IsError)
            {
                // hiba:
                throw new ResultException(resUgyiratokGetAll);
            }

            System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), resUgyiratokGetAll.Ds);
            if (statusz.Count == 0)
                throw new ResultException(52710);
            ErrorDetails errorDetail = null;

            string ugyiratIds = string.Empty;
            List<string> lstUgyiratIds = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);
            string selectedVers = string.Empty;
            List<string> lstVers = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);

            string ugyiratCsoport_Id_Felelos = String.Empty;
            foreach (System.Data.DataRow r in resUgyiratokGetAll.Ds.Tables[0].Rows)
            {
                ugyiratIds += ",'" + r["Id"].ToString() + "'";
                lstUgyiratIds.Add(r["Id"].ToString());
                selectedVers += "," + r["Ver"].ToString();
                lstVers.Add(r["Ver"].ToString());
                ugyiratCsoport_Id_Felelos = r["Csoport_Id_Felelos"].ToString();

                //Kik�rhet�-e?
                if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontroIrattarbolKikerheto((statusz[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)
                    , execParam.Clone(), out errorDetail) == false)
                {
                    // Az �gyiratra nem ind�that� kik�r�s
                    throw new ResultException(52712, errorDetail);
                }
            }

            ugyiratIds = ugyiratIds.TrimStart(',');
            selectedVers = selectedVers.TrimStart(',');


            // �GYIRAT UPDATE:
            // mez�k m�dos�that�s�g�nak letilt�sa:
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            // M�dos�tani kell: az allapotot, 

            /// �llapot be�ll�t�sa:
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert;
            erec_UgyUgyirat.Updated.Allapot = true;

            ExecParam execParam_ugyiratUpdate = execParam.Clone();

            Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, resUgyiratokGetAll.Ds.Tables[0], lstUgyiratIds.ToArray(), lstVers.ToArray(), erec_UgyUgyirat, DateTime.Now);
            if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_UgyiratUpdate);
            }

            ExecParam IrattariKikeroExecParam = execParam.Clone();
            IrattariKikeroExecParam.Record_Id = "";

            // Elso allapot:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto;
            erec_IrattariKikero.Updated.Allapot = true;

            // Iratt�r ID-j�nak be�ll�tani az �gyirat felel�s�t (vagyis az iratt�rat)
            erec_IrattariKikero.Irattar_Id = ugyiratCsoport_Id_Felelos;
            erec_IrattariKikero.Updated.Irattar_Id = true;

            // K�lcs�nz�si d�tumokat nem �ll�tjuk:
            erec_IrattariKikero.Updated.KikerKezd = false;
            erec_IrattariKikero.Updated.KikerVege = false;

            Result IrattariKikeroResult = this.Insert_Tomeges(IrattariKikeroExecParam, UgyiratIdArray, erec_IrattariKikero);
            if (!String.IsNullOrEmpty(IrattariKikeroResult.ErrorCode))
            {
                // hiba:
                throw new ResultException(IrattariKikeroResult);
            }

            result = IrattariKikeroResult;


            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ugyiratIds, "KikeresSkontroIrattarbol", erec_IrattariKikero.Keres_note);
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_IrattariKikero::KikeresSkontroIrattarbol: ", execParam, eventLogResult);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private Result SetKikerokToVisszaadott_ByUgyiratId(ExecParam execParam, string[] UgyiratIdArray)
    {
        EREC_IrattariKikeroSearch search = new EREC_IrattariKikeroSearch();

        search.UgyUgyirat_Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
        search.UgyUgyirat_Id.Operator = Query.Operators.inner;

        search.Allapot.Value = KodTarak.IRATTARIKIKEROALLAPOT.Nyitott
                        + "," + KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto
                        + "," + KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett
                        + "," + KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;

        search.Allapot.Operator = Query.Operators.inner;

        search.Irattar_Id.Value = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam).Obj_Id;
        search.Irattar_Id.Operator = Query.Operators.equals;

        Result result = this.GetAll(execParam, search);

        // nincs �tv�telre �ll�tand� kik�r� => return
        if (result.Ds.Tables[0].Rows.Count == 0)
            return result;

        List<string> idList = new List<string>();
        List<string> verList = new List<string>();

        foreach (DataRow r in result.Ds.Tables[0].Rows)
        {
            idList.Add(r["Id"].ToString());
            verList.Add(r["Ver"].ToString());
        }

        EREC_IrattariKikero kikero = new EREC_IrattariKikero();
        kikero.Updated.SetValueAll(false);
        kikero.Base.Updated.SetValueAll(false);
        kikero.Base.Updated.Ver = true;

        kikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott;
        kikero.Updated.Allapot = true;

        return this.Update_Tomeges(execParam, idList.ToArray(), verList.ToArray(), kikero);
    }

    #endregion �gyirat kik�r�s skontr� iratt�rb�l

    #region Skontr� iratt�rb�l kik�r�s sztorn�
    /// <summary>
    /// �gyirat Skontr� iratt�rb�l val� kik�r�s�nek visszavon�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SkontroKikeresSztorno(ExecParam execParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ugyiratId))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52720);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            execParam.Record_Id = ugyiratId;

            result = this.GetByUgyiratId(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            if (result.Record == null)
            {
                // hiba
                throw new ResultException(52721);
            }

            EREC_IrattariKikero erec_IrattariKikeroStatusz = (EREC_IrattariKikero)result.Record;
            Contentum.eRecord.BaseUtility.IrattariKikero.Statusz statusz = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByBusinessDocument(erec_IrattariKikeroStatusz);
            ErrorDetails errorDetail = null;

            // statusz ellenorzesek:
            if (!Contentum.eRecord.BaseUtility.IrattariKikero.Sztornozhato(statusz, execParam, out errorDetail))
            {
                // hiba nem sztornozhato
                throw new ResultException(52722, errorDetail);
            }

            erec_IrattariKikeroStatusz.Updated.SetValueAll(false);
            erec_IrattariKikeroStatusz.Base.Updated.SetValueAll(false);

            erec_IrattariKikeroStatusz.Base.Ver = erec_IrattariKikeroStatusz.Base.Ver;
            erec_IrattariKikeroStatusz.Base.Updated.Ver = true;

            // sztornozott allapot beallitasa:
            erec_IrattariKikeroStatusz.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Sztornozott;
            erec_IrattariKikeroStatusz.Updated.Allapot = true;

            erec_IrattariKikeroStatusz.VisszaadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikeroStatusz.Updated.VisszaadasDatuma = true;

            result = this.Update(execParam, erec_IrattariKikeroStatusz);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            EREC_UgyUgyiratokService ugyiratService = new EREC_UgyUgyiratokService(this.dataContext);

            execParam.Record_Id = erec_IrattariKikeroStatusz.UgyUgyirat_Id;
            result = ugyiratService.Get(execParam);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result.Record;

            ugyirat.Updated.SetValueAll(false);
            ugyirat.Base.Updated.SetValueAll(false);
            ugyirat.Base.Updated.Ver = true;

            ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Skontroban;
            ugyirat.Updated.Allapot = true;

            execParam.Record_Id = ugyirat.Id;
            result = ugyiratService.Update(execParam, ugyirat);
            if (!string.IsNullOrEmpty(result.ErrorCode))
                throw new ResultException(result);

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyiratId, "EREC_UgyUgyiratok", "KikeresSkontroIrattarbolVisszavonas").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    #endregion

    #region KiadasSkontroIrattarbol

    /// <summary>
    /// Kik�rt �gyiratok kiad�sa skontr� iratt�rb�l
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KiadasSkontroIrattarbol_Tomeges(ExecParam execParam, string[] UgyiratIdArray, string SkontroVege)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            // Param�terek ellen�rz�se:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || (UgyiratIdArray == null || UgyiratIdArray.Length == 0))
            {
                // hiba
                throw new ResultException(52730);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iratt�ri kik�r� t�tel record m�dos�t�sa

            ExecParam xpmIrattariKikeroGetAll = execParam.Clone();
            EREC_IrattariKikeroSearch schIrattariKikero = new EREC_IrattariKikeroSearch();
            schIrattariKikero.UgyUgyirat_Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
            schIrattariKikero.UgyUgyirat_Id.Operator = Query.Operators.inner;
            Contentum.eRecord.BaseUtility.IrattariKikero.SetSearchFilterKiadhatoSkontroIrattarbol(schIrattariKikero);
            Result resIrattariKikeroGetAll = this.GetAll(xpmIrattariKikeroGetAll, schIrattariKikero);

            if (resIrattariKikeroGetAll.IsError)
            {
                // hiba
                throw new ResultException(52731);
            }

            System.Collections.Hashtable statuszKikero = Contentum.eRecord.BaseUtility.IrattariKikero.GetAllapotByDataSet(execParam.Clone(), resIrattariKikeroGetAll.Ds);
            if (statuszKikero.Count == 0)
            {
                Logger.Error("statuszKikero.Count = 0", execParam);
                throw new ResultException(52730);
            }

            List<string> KikeroIdList = new List<string>();
            List<string> KikeroVerList = new List<string>();

            List<string> UgyiratokUgyintezesre = new List<string>();
            List<string> UgyiratokBetekintesre = new List<string>();

            Dictionary<string, string> FelhasznaloCsoport_Id_Kikero_Dictionary = new Dictionary<string, string>();
            foreach (System.Data.DataRow r in resIrattariKikeroGetAll.Ds.Tables[0].Rows)
            {
                KikeroIdList.Add(r["Id"].ToString());
                KikeroVerList.Add(r["Ver"].ToString());
                string ugyiratId = r["UgyUgyirat_Id"].ToString();
                if (!FelhasznaloCsoport_Id_Kikero_Dictionary.ContainsKey(ugyiratId))
                {
                    FelhasznaloCsoport_Id_Kikero_Dictionary.Add(ugyiratId, r["FelhasznaloCsoport_Id_Kikero"].ToString());
                }

                // statusz ellenorzesek:
                if (!Contentum.eRecord.BaseUtility.IrattariKikero.KiadhatoSkontroIrattarbol((statuszKikero[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.IrattariKikero.Statusz), execParam.Clone()))
                {
                    // hiba nem adhat� ki
                    throw new ResultException(52732);
                }

                // sz�tv�logatni melyik �gyiratokat k�rt�k �gyint�z�sre �s melyikeket betekint�sre
                if (r["FelhasznalasiCel"].ToString() == "U")
                    UgyiratokUgyintezesre.Add(ugyiratId);
                else
                    UgyiratokBetekintesre.Add(ugyiratId);
            }



            EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
            erec_IrattariKikero.Updated.SetValueAll(false);
            erec_IrattariKikero.Base.Updated.SetValueAll(false);

            erec_IrattariKikero.Base.Ver = erec_IrattariKikero.Base.Ver;
            erec_IrattariKikero.Base.Updated.Ver = true;

            // Kiadott allapot beallitasa:
            erec_IrattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Kiadott;
            erec_IrattariKikero.Updated.Allapot = true;

            // kiad� szem�ly �s kiad�si id�pont �ll�t�sa:
            erec_IrattariKikero.FelhasznaloCsoport_Id_Kiado = execParam.Felhasznalo_Id;
            erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Kiado = true;

            erec_IrattariKikero.KiadasDatuma = DateTime.Now.ToString();
            erec_IrattariKikero.Updated.KiadasDatuma = true;

            result = this.Update_Tomeges(execParam, KikeroIdList.ToArray(), KikeroVerList.ToArray(), erec_IrattariKikero);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }

            #endregion

            if (UgyiratokUgyintezesre.Count != 0)
            {
                result = this.KiadasSkontroIrattarbol_Ugyintezesre_Tomeges(execParam.Clone(), UgyiratokUgyintezesre.ToArray(), SkontroVege, FelhasznaloCsoport_Id_Kikero_Dictionary, resIrattariKikeroGetAll);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }

            if (UgyiratokBetekintesre.Count != 0)
            {
                result = this.KiadasSkontroIrattarbol_Betekintesre_Tomeges(execParam.Clone(), UgyiratokBetekintesre.ToArray(), FelhasznaloCsoport_Id_Kikero_Dictionary, resIrattariKikeroGetAll);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }


            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, Search.GetSqlInnerString(UgyiratIdArray), "KiadasSkontroIrattarbol");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_IrattariKikero::KiadasSkontroIrattarbol: ", execParam, eventLogResult);
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private Result KiadasSkontroIrattarbol_Ugyintezesre_Tomeges(ExecParam execParam, string[] UgyiratIdArray, string SkontroVege, Dictionary<string, string> FelhasznaloCsoport_Id_Kikero_Dictionary, Result resIrattariKikeroGetAll)
    {
        // skontr�b�l kiv�tel
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

        Result result = erec_UgyUgyiratokService.SkontrobolKivetel_Tomeges(execParam.Clone(), UgyiratIdArray, SkontroVege);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        // Ugyirat allapotat es feleloset allitani kell

        // �gyirat lek�r�se:
        ExecParam execParam_UgyiratGetAll = execParam.Clone();

        EREC_UgyUgyiratokSearch schUgyiratok = new EREC_UgyUgyiratokSearch();
        schUgyiratok.Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
        schUgyiratok.Id.Operator = Query.Operators.inner;

        Result resUgyiratokGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, schUgyiratok);
        if (resUgyiratokGetAll.IsError)
        {
            // hiba:
            throw new ResultException(resUgyiratokGetAll);
        }


        System.Collections.Hashtable statuszUgyirat = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), resUgyiratokGetAll.Ds);
        if (statuszUgyirat.Count == 0)
        {
            Logger.Error("statuszUgyirat.Count = 0", execParam);
            throw new ResultException(52730);
        }

        string ugyiratIds = string.Empty;
        List<string> lstUgyiratIds = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);
        string ugyiratVers = string.Empty;
        List<string> lstVers = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);

        List<string> Csoport_Id_Felelos_List = new List<string>();
        foreach (System.Data.DataRow r in resUgyiratokGetAll.Ds.Tables[0].Rows)
        {
            string id = r["Id"].ToString();
            ugyiratIds += ",'" + id + "'";
            lstUgyiratIds.Add(r["Id"].ToString());
            ugyiratVers += "," + r["Ver"].ToString();
            lstVers.Add(r["Ver"].ToString());

            string Csoport_Id_Felelos = String.Empty;
            if (FelhasznaloCsoport_Id_Kikero_Dictionary.ContainsKey(id))
            {
                Csoport_Id_Felelos = FelhasznaloCsoport_Id_Kikero_Dictionary[id];
            }
            Csoport_Id_Felelos_List.Add(Csoport_Id_Felelos);
        }

        ugyiratIds = ugyiratIds.TrimStart(',');
        ugyiratVers = ugyiratVers.TrimStart(',');

        // �GYIRAT UPDATE:
        // mez�k m�dos�that�s�g�nak letilt�sa:
        EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
        erec_UgyUgyirat.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.Ver = true;

        // M�dos�tani kell: az �llapotot, kezel�t

        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
        erec_UgyUgyirat.Updated.Allapot = true;

        erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;


        ExecParam execParam_ugyiratUpdate = execParam.Clone();

        return erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, resIrattariKikeroGetAll.Ds.Tables[0], lstUgyiratIds.ToArray(), lstVers.ToArray(), Csoport_Id_Felelos_List.ToArray(), erec_UgyUgyirat, DateTime.Now);
    }

    private Result KiadasSkontroIrattarbol_Betekintesre_Tomeges(ExecParam execParam, string[] UgyiratIdArray, Dictionary<string, string> FelhasznaloCsoport_Id_Kikero_Dictionary, Result resIrattariKikeroGetAll)
    {
        // Ugyirat allapotat es feleloset allitani kell
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

        // �gyirat lek�r�se:
        ExecParam execParam_UgyiratGetAll = execParam.Clone();

        EREC_UgyUgyiratokSearch schUgyiratok = new EREC_UgyUgyiratokSearch();
        schUgyiratok.Id.Value = Search.GetSqlInnerString(UgyiratIdArray);
        schUgyiratok.Id.Operator = Query.Operators.inner;

        Result resUgyiratokGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, schUgyiratok);
        if (resUgyiratokGetAll.IsError)
        {
            // hiba:
            throw new ResultException(resUgyiratokGetAll);
        }


        System.Collections.Hashtable statuszUgyirat = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), resUgyiratokGetAll.Ds);
        if (statuszUgyirat.Count == 0)
        {
            Logger.Error("statuszUgyirat.Count = 0", execParam);
            throw new ResultException(52730);
        }

        string ugyiratIds = string.Empty;
        List<string> lstUgyiratIds = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);
        string ugyiratVers = string.Empty;
        List<string> lstVers = new List<string>(resUgyiratokGetAll.Ds.Tables[0].Rows.Count);

        List<string> Csoport_Id_Felelos_List = new List<string>();
        foreach (System.Data.DataRow r in resUgyiratokGetAll.Ds.Tables[0].Rows)
        {
            string id = r["Id"].ToString();
            ugyiratIds += ",'" + id + "'";
            lstUgyiratIds.Add(r["Id"].ToString());
            ugyiratVers += "," + r["Ver"].ToString();
            lstVers.Add(r["Ver"].ToString());

            string Csoport_Id_Felelos = String.Empty;
            if (FelhasznaloCsoport_Id_Kikero_Dictionary.ContainsKey(id))
            {
                Csoport_Id_Felelos = FelhasznaloCsoport_Id_Kikero_Dictionary[id];
            }
            Csoport_Id_Felelos_List.Add(Csoport_Id_Felelos);
        }

        ugyiratIds = ugyiratIds.TrimStart(',');
        ugyiratVers = ugyiratVers.TrimStart(',');

        // �GYIRAT UPDATE:
        // mez�k m�dos�that�s�g�nak letilt�sa:
        EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
        erec_UgyUgyirat.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.SetValueAll(false);
        erec_UgyUgyirat.Base.Updated.Ver = true;

        // M�dos�tani kell: az �llapotot, kezel�t

        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
        erec_UgyUgyirat.Updated.Allapot = true;

        erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Skontroban;
        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

        ExecParam execParam_ugyiratUpdate = execParam.Clone();

        return erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, resIrattariKikeroGetAll.Ds.Tables[0], lstUgyiratIds.ToArray(), lstVers.ToArray(), Csoport_Id_Felelos_List.ToArray(), erec_UgyUgyirat, DateTime.Now);
    }

    #endregion

}