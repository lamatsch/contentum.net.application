using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_IrattariTetel_IktatokonyvService : System.Web.Services.WebService
{
    #region BLG_2967
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariTetel_IktatokonyvSearch))]
    public String CreateIktatokonyvIrattariTetelDexXml(ExecParam ExecParam, EREC_IrattariTetel_IktatokonyvSearch _EREC_IrattariTetel_IktatokonyvSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        String result = String.Empty;
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.CreateIktatokonyvIrattariTetelDexXml(ExecParam, _EREC_IrattariTetel_IktatokonyvSearch);

        }
        catch (Exception e)
        {
            result = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam);
        return result;
    }
    #endregion

    #region BLG_4977
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariTetel_IktatokonyvSearch))]
    public String CreateErkeztetokonyvContentumXml(ExecParam ExecParam, EREC_IrattariTetel_IktatokonyvSearch _EREC_IrattariTetel_IktatokonyvSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        String result = String.Empty;
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.CreateErkeztetokonyvContentumXml(ExecParam, _EREC_IrattariTetel_IktatokonyvSearch);

        }
        catch (Exception e)
        {
            result = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam);
        return result;
    }
    #endregion

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_IrattariTetel_IktatokonyvSearch))]
    public Result GetAllByIktatoKonyv(ExecParam ExecParam
        , EREC_IrattariTetel_IktatokonyvSearch _EREC_IrattariTetel_IktatokonyvSearch
        , String Iktatokonyv_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByIktatoKonyv(ExecParam, _EREC_IrattariTetel_IktatokonyvSearch, Iktatokonyv_Id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result InsertAllByIktatokonyv(ExecParam execParam, string Iktatokonyv_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            Arguments args = new Arguments();
            args.Add(new Argument("Iktatókönyv", Iktatokonyv_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.InsertAllByIktatokonyv(execParam, Iktatokonyv_Id);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                System.Collections.Generic.List<string> resultIds = new System.Collections.Generic.List<string>();

                foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                {
                    resultIds.Add(row["Id"].ToString());
                }

                string objIds = Search.GetSqlInnerString(resultIds.ToArray());
                
                Result eventLogResult = eventLogService.InsertTomeges(execParam, objIds, "EREC_IrattariTetel_Iktatokonyv", "New");
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

}
