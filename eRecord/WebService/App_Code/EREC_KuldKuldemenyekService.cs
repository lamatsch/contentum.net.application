﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
//using Contentum.eRecord.BaseUtility;
using Contentum.eDocument.Service;
using Contentum.eQuery;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using Contentum.eAdmin.Service;


[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Xml.Serialization.XmlInclude(typeof(ErkeztetesIktatasResult))]
public partial class EREC_KuldKuldemenyekService : System.Web.Services.WebService
{

    private string GetOrgKod(ExecParam execParam)
    {
        var service_org = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_OrgokService();
        ExecParam execParam_orgGet = execParam.Clone();
        execParam_orgGet.Record_Id = execParam.Org_Id;

        Result result_orgGet = service_org.Get(execParam_orgGet);
        result_orgGet.CheckError();

        KRT_Orgok krt_Orgok = (KRT_Orgok)result_orgGet.Record;
        return krt_Orgok.Kod;
    }
    /// <summary>
    /// Egy küldemény keresése megadott vonalkód alapján.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="BarCode"></param>
    /// <returns></returns>
    [WebMethod(Description = "Egy küldemény keresése megadott vonalkód alapján.")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result GetByBarCode(ExecParam execParam, string BarCode)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetByBarCode(execParam, BarCode);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra).
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_KuldKuldemenyekSearch"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch)
    {
        return GetAllWithExtensionAndJogosultak(ExecParam, _EREC_KuldKuldemenyekSearch, false);
    }


    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra).
    /// Jogosultság alapján is szûr
    /// </summary>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_KuldKuldemenyekSearch, Jogosultak);

            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_KuldKuldemenyekSearch, new EREC_KuldKuldemenyekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_KuldKuldemenyek", "KuldemenyList").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch);

                    #region where feltétel összeállítása
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_KuldKuldemenyekSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_KuldKuldemenyekSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Kimenõ küldemények lekérésére (más tárolt eljárást hív, mint a sima GetAllWithExtension)
    /// Szûrési feltételek megadása a _EREC_KuldKuldemenyekSearch objektumban történik
    /// </summary>    
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result KimenoKuldGetAllWithExtension(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch)
    {
        return KimenoKuldGetAllWithExtensionAndJogosultak(ExecParam, _EREC_KuldKuldemenyekSearch, false);
    }

    /// <summary>
    /// Kimenõ küldemények lekérése jogosultságra szûréssel. Szûrési feltételek megadása a _EREC_KuldKuldemenyekSearch objektumban történik
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_KuldKuldemenyekSearch"></param>
    /// <param name="Jogosultak"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result KimenoKuldGetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.KimenoKuldGetAllWithExtension(ExecParam, _EREC_KuldKuldemenyekSearch, Jogosultak);

            #region Eseménynaplózás
            if (!Search.IsIdentical(_EREC_KuldKuldemenyekSearch, new EREC_KuldKuldemenyekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_KuldKuldemenyek", "KuldemenyList").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch);

                    #region where feltétel összeállítása
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_KuldKuldemenyekSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_KuldKuldemenyekSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemények lekérésére (más tárolt eljárást hív, mint a sima GetAllWithExtension)
    /// Szûrési feltételek megadása a _EREC_KuldKuldemenyekSearch objektumban történik
    /// </summary>    
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result KimenoKuldGetAllWithExtensionForEFeladoJegyzek(ExecParam ExecParam, EPostazasiParameterek ePostazasiParameterek)
    {
        return KimenoKuldGetAllWithExtensionAndJogosultakForEFeladoJegyzek(ExecParam, ePostazasiParameterek, false);
    }

    /// <summary>
    /// Kimenõ küldemények lekérése jogosultságra szûréssel. Szûrési feltételek megadása a _EREC_KuldKuldemenyekSearch objektumban történik
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_KuldKuldemenyekSearch"></param>
    /// <param name="Jogosultak"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result KimenoKuldGetAllWithExtensionAndJogosultakForEFeladoJegyzek(ExecParam ExecParam, EPostazasiParameterek ePostazasiParameterek, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.KimenoKuldGetAllWithExtensionForEFeladoJegyzek(ExecParam, ePostazasiParameterek, Jogosultak);

            // táblák elnevezése
            if (!result.IsError)
            {
                // megjegyzés: az elsõ visszaadott tábla tartalmazza a lapozási információkat
                // a további táblanevek megfelelnek az XSD-ben a 3. mélységi szintnek,
                // azaz tomeges_adatok/..._tetelek/..._tetel, amin belül már a a rekord mezõnevek adják a megnevezéseket (pl. sorszam, azonosito)...
                // pl. tomeges_adatok/kozonseges_tetelek/kozonseges_tetel
                // BLG_1938
                //if (result.Ds.Tables.Count > 0)
                //{
                //    result.Ds.Tables[0].TableName = "paging";

                //    if (result.Ds.Tables.Count > 1)
                //    {
                //        if (!ePostazasiParameterek.IgnoreKozonsegesTetelek)
                //        {
                //            result.Ds.Tables[1].TableName = "kozonseges_tetelek";
                //        }
                //        else if (!ePostazasiParameterek.IgnoreNyilvantartottTetelek)
                //        {

                //        }
                //    }
                //}
                // BLG_1938
                //string[] TableNames = new string[] { "paging", "kozonseges_tetel", "nyilvantartott_tetel" };
                //bool[] AddTable = new bool[] { true, !ePostazasiParameterek.IgnoreKozonsegesTetelek, !ePostazasiParameterek.IgnoreNyilvantartottTetelek };
                string[] TableNames = new string[] { "paging",
                                                     Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_tetel,
                                                     Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_tetel,
                                                     Contentum.eUtility.EFeladoJegyzek.Constants.kozonseges_azon_tetel,
                                                     Contentum.eUtility.EFeladoJegyzek.Constants.nyilvantartott_tetel,
                                                     Contentum.eUtility.EFeladoJegyzek.Constants.nemz_kozonseges_azon_tetel,
                                                     Contentum.eUtility.EFeladoJegyzek.Constants.nemz_nyilvantartott_tetel,
                                                     };
                //bool[] AddTable = new bool[] { true,
                //                              !ePostazasiParameterek.IgnoreKozonsegesTetelek,
                //                              !ePostazasiParameterek.IgnoreKozonsegesTetelek,
                //                              !ePostazasiParameterek.IgnoreKozonsegesTetelek,
                //                              !ePostazasiParameterek.IgnoreNyilvantartottTetelek,
                //                              !ePostazasiParameterek.IgnoreKozonsegesTetelek,
                //                              !ePostazasiParameterek.IgnoreNyilvantartottTetelek};

                int tn = 0;
                // BLG_1938
                //for (int i = 0; i < result.Ds.Tables.Count && tn < AddTable.Length; i++)
                for (int i = 0; i < result.Ds.Tables.Count; i++)
                {
                    // BLG_1938
                    //while (tn < AddTable.Length && !AddTable[tn])
                    //{
                    //    tn++;
                    //}
                    //if (tn < AddTable.Length)
                    //{
                    result.Ds.Tables[i].TableName = TableNames[tn];
                    tn++;
                    //}

                }
                //for (int i = 0; i < result.Ds.Tables.Count && i < TableNames.Length; i++)
                //{
                //    result.Ds.Tables[i].TableName = TableNames[i];
                //}
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_KuldKuldemenyek", "Postazas").Record;

            if (eventLogRecord != null)
            {
                eventLogRecord.KeresesiFeltetel = ePostazasiParameterek.ToString();
                if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                {

                    if (result.Ds.Tables.Count > 0)
                    {
                        // a 0. tábla tartalmazza a lapozási információkat!
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[0].Rows[0]["RecordNumber"].ToString();
                    }

                    //// nincs lapozás
                    //int talalatokSzama = 0;
                    //foreach (DataTable table in result.Ds.Tables)
                    //{
                    //    talalatokSzama += table.Rows.Count;
                    //}

                    //eventLogRecord.TalalatokSzama = talalatokSzama.ToString();
                }

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemények összesítõjének lekérése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_KuldKuldemenyekSearch"></param>
    /// <param name="KezdDat"></param>
    /// <param name="VegeDat"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result KimenoKuldOsszesitoje(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch, string KezdDat, string VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.KimenoKuldOsszesitoje(ExecParam, _EREC_KuldKuldemenyekSearch, KezdDat, VegeDat);

            #region Eseménynaplózás
            if (!Search.IsIdentical(_EREC_KuldKuldemenyekSearch, new EREC_KuldKuldemenyekSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_KuldKuldemenyek", "KuldemenyList").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_KuldKuldemenyekSearch);

                    #region where feltétel összeállítása
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_KuldKuldemenyekSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_KuldKuldemenyekSearch.WhereByManual;
                    }


                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Egy küldemény rekord lekérése jogosultságellenõrzéssel
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Jogszint"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_KuldKuldemenyek", "KuldemenyView").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Egy küldemény rekord lekérése jogosultságellenõrzéssel
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Jogszint"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result GetWithRightCheckWithoutEventLogging(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Küldemény rekord létrehozása az adatbázisban
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result Insert(ExecParam ExecParam, EREC_KuldKuldemenyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            /// Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
            /// Kézbesítési tételt kell létrehozni, ha be van állítva a felelõs, és az nem egyezik a felhasználó egyszemélyes csoportjával
            /// 

            bool kellKezbesitesiTetel = false;

            // ki van-e töltve a felelõs mezõ?
            if (Record.Updated.Csoport_Id_Felelos == true && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos))
            {
                // ha nem a felhasználó saját csoportja:
                if (Csoportok.GetFelhasznaloSajatCsoportId(ExecParam) != Record.Csoport_Id_Felelos)
                {
                    kellKezbesitesiTetel = true;
                }
            }

            //Vonalkód ellenõrzése
            KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
            Result resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
            if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
            {
                throw new ResultException(resBarcode);
            }

            // ha kézbesítési tételt is kell csinálni, akkor továbbítás alatti állapotot kell a küldeménynek beállítani
            if (kellKezbesitesiTetel)
            {
                Record.TovabbitasAlattAllapot = Record.Allapot;
                Record.Updated.TovabbitasAlattAllapot = true;

                Record.Allapot = KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt;
                Record.Updated.Allapot = true;

                Record.Csoport_Id_Felelos_Elozo = Record.Csoport_Id_Felelos;
                Record.Updated.Csoport_Id_Felelos_Elozo = true;
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //Vonalkód update és kötés if a vonalkódkezelés azonosítós
            //Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
            ExecParam ExecParam_RendszerParameter = ExecParam.Clone();
            if (Rendszerparameterek.Get(ExecParam_RendszerParameter, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))//CR 3058
            {
                //ExecParam_RendszerParameter.Record_Id = "96472D53-55BC-E611-80BB-00155D020D34"; //VonalkodPrefix
                //Result_KRTParam = service_parameterek.Get(ExecParam_RendszerParameter);
                //string barcode = ((KRT_Parameterek)Result_KRTParam.Record).Ertek.Trim()+"-";
                string barcode = Rendszerparameterek.Get(ExecParam_RendszerParameter, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_PREFIX).Trim() + "-";
                var execParam_Kuldemeny = ExecParam.Clone();
                execParam_Kuldemeny.Record_Id = result.Uid;
                Result KRT_Kuldemeny = sp.Get(execParam_Kuldemeny);

                var ExecParamBarCode = ExecParam.Clone();


                DateTime beerkezes = Convert.ToDateTime(((EREC_KuldKuldemenyek)KRT_Kuldemeny.Record).BeerkezesIdeje);
                string erkeztetoszam = "0000000" + ((EREC_KuldKuldemenyek)KRT_Kuldemeny.Record).Erkezteto_Szam;
                //BLG_2167 - szkennelés során kell-e elektronikusan hiteles másolat
                barcode += beerkezes.Year.ToString() + beerkezes.Month.ToString().PadLeft(2, '0') + beerkezes.Day.ToString().PadLeft(2, '0') + "-" + beerkezes.Hour.ToString().PadLeft(2, '0') + beerkezes.Minute.ToString().PadLeft(2, '0') + "-" +
                        erkeztetoszam.Substring(erkeztetoszam.Length - 6) + "-" + Rendszerparameterek.Get(ExecParam_RendszerParameter, Contentum.eUtility.Rendszerparameterek.SCAN_EHITELES_MASOLAT) + "-00"; //oldalszám és aláírás??
                barcode = barcode.Replace(" ", "");
                KRT_Barkodok barcodeRecord = new KRT_Barkodok();
                barcodeRecord.ErvKezd = beerkezes.ToString();
                barcodeRecord.Kod = barcode;
                barcodeRecord.Tranz_id = this.dataContext.Tranz_Id;
                // ???barcodeRecord.KodType = 

                var bid = srvBarcode.Insert(ExecParamBarCode, barcodeRecord);
                if (!String.IsNullOrEmpty(bid.ErrorCode))
                {
                    // hiba
                    Logger.Error("Hiba: Barkod rekordhoz  Insert", ExecParamBarCode);
                    throw new ResultException(bid);
                }
                var result_barcodebind = srvBarcode.BarkodBindToKuldemeny(ExecParamBarCode, bid.Uid, result.Uid);
                if (!String.IsNullOrEmpty(result_barcodebind.ErrorCode))
                {
                    // hiba
                    Logger.Error("Hiba: Barkod rekordhoz  hozzákötése", ExecParamBarCode);
                    throw new ResultException(result_barcodebind);
                }
                KRT_Kuldemeny = sp.Get(execParam_Kuldemeny);
                ((EREC_KuldKuldemenyek)KRT_Kuldemeny.Record).BarCode = barcode;
                sp.Insert(Constants.Update, execParam_Kuldemeny, ((EREC_KuldKuldemenyek)KRT_Kuldemeny.Record));
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }
            }


            //vonalkód kötése, ha meg volt adva
            //CR 3058
            if (!String.IsNullOrEmpty(resBarcode.Uid) && !Rendszerparameterek.Get(ExecParam_RendszerParameter, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
            {
                string barkodId = resBarcode.Uid;
                string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                string kuldemenyId = result.Uid;
                resBarcode = new Result();
                resBarcode = srvBarcode.BarkodBindToKuldemeny(ExecParam, barkodId, kuldemenyId, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }


            // Kézbesítési tétel létrehozása, ha szükséges:
            if (kellKezbesitesiTetel == true && String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_KezbesitesiTetelInsert = ExecParam.Clone();

                EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

                //String kuldemeny_Id = ExecParam.Record_Id;
                String kuldemeny_Id = result.Uid;

                kezbesitesiTetel.Obj_Id = kuldemeny_Id;
                kezbesitesiTetel.Updated.Obj_Id = true;

                // TODO: ObjTip_Id -t is kéne majd állítani valamire
                // helyette: Obj_type a tábla neve lesz (EREC_KuldKuldemenyek)

                kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                kezbesitesiTetel.Updated.Obj_type = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_USER = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                kezbesitesiTetel.Csoport_Id_Cel = Record.Csoport_Id_Felelos;
                kezbesitesiTetel.Updated.Csoport_Id_Cel = true;


                // Állapot állítása:
                Contentum.eAdmin.Service.KRT_CsoportokService csoportokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                KRT_CsoportokSearch csoportokSearch = new KRT_CsoportokSearch();
                csoportokSearch.Id.Filter(Record.Csoport_Id_Felelos);

                Result subCsoportResult = csoportokService.GetAllSubCsoport(ExecParam.Clone(), ExecParam.FelhasznaloSzervezet_Id, true, csoportokSearch);
                if (string.IsNullOrEmpty(subCsoportResult.ErrorCode) && (subCsoportResult.Ds.Tables[0].Rows.Count != 0 || Record.Csoport_Id_Felelos == ExecParam.FelhasznaloSzervezet_Id))
                {
                    kezbesitesiTetel.AtadasDat = DateTime.Now.ToString();
                    kezbesitesiTetel.Updated.AtadasDat = true;

                    kezbesitesiTetel.Allapot = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
                    kezbesitesiTetel.Updated.Allapot = true;

                    EREC_IraKezbesitesiFejekService kezbesitesiFejekService = new EREC_IraKezbesitesiFejekService(this.dataContext);
                    EREC_IraKezbesitesiFejek kezbesitesiFej = new EREC_IraKezbesitesiFejek();

                    kezbesitesiFej.CsomagAzonosito = "TomegesAtadas";
                    kezbesitesiFej.Updated.CsomagAzonosito = true;

                    kezbesitesiFej.Tipus = "A";
                    kezbesitesiFej.Updated.Tipus = true;

                    Result kezbesitesiFejInsertResult = kezbesitesiFejekService.Insert(execParam_KezbesitesiTetelInsert.Clone(), kezbesitesiFej);
                    kezbesitesiFejInsertResult.CheckError();

                    if (string.IsNullOrEmpty(kezbesitesiFejInsertResult.Uid))
                        throw new ResultException(0);

                    kezbesitesiTetel.KezbesitesFej_Id = kezbesitesiFejInsertResult.Uid;
                    kezbesitesiTetel.Updated.KezbesitesFej_Id = true;
                }
                else
                {
                    kezbesitesiTetel.Allapot = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
                    kezbesitesiTetel.Updated.Allapot = true;
                }

                Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);
                result_kezbesitesiTetel_Insert.CheckError();
            }

            #region ACL kezelés

            if (Rendszerparameterek.GetBoolean(ExecParam, "TULAJDONOS_JOGOSULTSAG_ROGZITES", true))
            {
                Result aclResult = result;
                RightsService rs = new RightsService(this.dataContext);
                aclResult = rs.AddScopeIdToJogtargyWithJogszint(ExecParam, result.Uid, "EREC_KuldKuldemenyek", Record.IraIktatokonyv_Id, 'S', ExecParam.Felhasznalo_Id, 'O');
                if (!string.IsNullOrEmpty(aclResult.ErrorCode))
                {
                    throw new ResultException(aclResult);
                }
            }

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Küldemény módosítása
    /// Felelõs változásának figyelése: ha változik a felelõs, kézbesítési tétel generálás
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result Update(ExecParam ExecParam, EREC_KuldKuldemenyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Általában: Nem lehet erkezteto szamot es erkezeto konyv_id-t modositani!
            // Kimenõ küldeménynél viszont be kell írni a postakönyv azonosítót, ezért
            // a régi küldemény lekérése után vizsgáljuk, hogy a IraIktatokonyv_Id módosítást
            // le kell-e tiltani
            Record.Updated.Erkezteto_Szam = false;
            //Record.Updated.IraIktatokonyv_Id = false;

            /// Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
            /// Ha változtatjuk, Kézbesítési tételt kell létrehozni:
            /// 

            bool kellKezbesitesiTetel = false;

            EREC_KuldKuldemenyek kuldemeny_regi = null;
            string kuldemenyOrzoElozo = "";
            string kuldemenyFelelosElozo = "";
            bool kuldemenyMozog = false;

            #region régi küldemény lekérése DB-bõl
            /// Meg kell vizsgálni, hogy ténylegesen változott-e a felelõs
            /// (Ehhez le kell kérni a rekordot)
            /// 
            ExecParam execParam_kuldemenyGet = ExecParam.Clone();
            execParam_kuldemenyGet.Record_Id = ExecParam.Record_Id;

            Result result_kuldemenyGet = this.Get(execParam_kuldemenyGet);
            if (!String.IsNullOrEmpty(result_kuldemenyGet.ErrorCode))
            {
                // hiba
                throw new ResultException(result_kuldemenyGet);
            }

            kuldemeny_regi = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

            kuldemenyOrzoElozo = (result_kuldemenyGet.Record as EREC_KuldKuldemenyek).FelhasznaloCsoport_Id_Orzo;
            kuldemenyFelelosElozo = (result_kuldemenyGet.Record as EREC_KuldKuldemenyek).Csoport_Id_Felelos;

            #endregion

            #region Felelõs változásának vizsgálata:
            if (Record.Updated.Csoport_Id_Felelos == true && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos))
            {
                if (kuldemeny_regi != null)
                {
                    if (kuldemeny_regi.Csoport_Id_Felelos != Record.Csoport_Id_Felelos)
                    {
                        kellKezbesitesiTetel = true;
                    }
                }
            }
            #endregion

            #region Vonalkód változásának vizsgálata

            bool vanUjVonalkod = false;
            Result resBarcode = null;

            // ha változott a vonalkód:
            if (Record.Updated.BarCode == true && !String.IsNullOrEmpty(Record.BarCode) && Record.BarCode != kuldemeny_regi.BarCode)
            {
                vanUjVonalkod = true;

                //Vonalkód ellenõrzése
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }

            #endregion

            #region Küldemény mozgásának vizsgálata

            if (Record.Updated.Csoport_Id_Felelos == true && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos) && Record.Csoport_Id_Felelos != kuldemeny_regi.Csoport_Id_Felelos
                && Record.Updated.FelhasznaloCsoport_Id_Orzo == true && !String.IsNullOrEmpty(Record.FelhasznaloCsoport_Id_Orzo) && Record.FelhasznaloCsoport_Id_Orzo != kuldemeny_regi.FelhasznaloCsoport_Id_Orzo)
            {
                kuldemenyMozog = true;
            }

            // Benne van-e fizikai dossziéban?
            if (kuldemenyMozog && this.IsKuldemenyInFizikaiDosszie(ExecParam, ExecParam.Record_Id))
                throw new ResultException(64016);

            #endregion Küldemény mozgásának vizsgálata

            #region Ragszámhoz vizsgálata

            if (kuldemeny_regi.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                string val = Rendszerparameterek.Get(ExecParam.Clone(), Rendszerparameterek.TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA);
                string elektronikusPostakonyvId = GetElektronikusPostakonyvId(ExecParam.Clone());
                bool elektronikus = (Record.IraIktatokonyv_Id == elektronikusPostakonyvId);

                if (!elektronikus && !String.IsNullOrEmpty(Record.RagSzam) && (val == "A" || val == "M" || val == "T"))
                {
                    if (!string.IsNullOrEmpty(Record.IraIktatokonyv_Id) && !string.IsNullOrEmpty(Record.RagSzam))
                    {
                        KRT_RagSzamokService ragszamokkService = new KRT_RagSzamokService(this.dataContext);
                        ExecParam execParam_ragszam = ExecParam.Clone();
                        KRT_RagSzamokSearch ragSzamokSearch = new KRT_RagSzamokSearch();
                        ragSzamokSearch.Kod.Filter(Record.RagSzam);

                        Result ragszamResult = ragszamokkService.GetAll(execParam_ragszam, ragSzamokSearch);
                        ragszamResult.CheckError();

                        string uId;
                        //Ha nem létező ragszám akkor hibát dobunk, ha létező, de még nem volt foglalózva, akkor lefoglaljuk
                        if (ragszamResult.GetCount < 1)
                        {
                            Logger.Error("A megadott ragszám nem létezik.");
                            throw new ResultException("66666", "A megadott ragszám nem létezik: " + Record.RagSzam);
                        }
                        else
                        {
                            var ragszamRow = ragszamResult.Ds.Tables[0].Rows[0];
                            uId = ragszamRow["Id"].ToString();
                            string postakonyvId = ragszamRow["Postakonyv_Id"].ToString();

                            if (postakonyvId.ToLower().Trim() != Record.IraIktatokonyv_Id.ToLower().Trim())
                            {
                                Logger.Error("A megadott ragszám nem tartozik az adott postakönyvhöz: " + Record.RagSzam);
                                throw new ResultException("66666", "A megadott ragszám nem tartozik az adott postakönyvhöz: " + Record.RagSzam);
                            }
                        }
                    }
                }
            }

            #endregion

            // Ha nem kimenõ küldemény, az érkeztetõkönyv már nem változtatható
            if (kuldemeny_regi.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno || kuldemeny_regi.Allapot != KodTarak.KULDEMENY_ALLAPOT.Expedialt)
            {
                Record.Updated.IraIktatokonyv_Id = false;
            }

            Result result_kuldemenyUpdate = sp.Insert(Constants.Update, ExecParam, Record);
            result_kuldemenyUpdate.CheckError();

            //ha mozog a küldemény (átadás vagy átvétel)
            //akkor mozgatjuk vele az iratpéldányokat, és elsõdleges iratpéldányokkal az iratot is
            if (IsKimenoKuldemenyMoved(Record))
            {
                Result resMoved = this.KimenoKuldemenyek_Moved(ExecParam);
                resMoved.CheckError();
            }

            #region Vonalkód kötése, ha kell:

            if (vanUjVonalkod == true && resBarcode != null)
            {
                string barkodId = resBarcode.Uid;
                string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                string kuldemenyId = ExecParam.Record_Id;
                resBarcode = new Result();
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                ExecParam execParam_barkod = ExecParam.Clone();
                resBarcode = srvBarcode.BarkodBindToKuldemeny(execParam_barkod, barkodId, kuldemenyId, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }

                if (String.IsNullOrEmpty(kuldemeny_regi.BarCode))
                {
                    //Vonalkód átmásolása a mellékletekhez
                    Record.Id = ExecParam.Record_Id;
                    this.SaveBarcode(ExecParam, Record);
                }
            }
            #endregion

            // Kézbesítési tétel létrehozása, ha szükséges:
            if (kellKezbesitesiTetel)
            {
                #region Kézbesítési tétel létrehozása

                EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_KezbesitesiTetelInsert = ExecParam.Clone();

                EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

                String kuldemeny_Id = ExecParam.Record_Id;

                kezbesitesiTetel.Obj_Id = kuldemeny_Id;
                kezbesitesiTetel.Updated.Obj_Id = true;

                // TODO: ObjTip_Id -t is kéne majd állítani valamire
                // helyette: Obj_type a tábla neve lesz (EREC_KuldKuldemenyek)

                kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                kezbesitesiTetel.Updated.Obj_type = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_USER = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                kezbesitesiTetel.Csoport_Id_Cel = Record.Csoport_Id_Felelos;
                kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                // Állapot állítása:
                kezbesitesiTetel.Allapot = Contentum.eUtility.KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
                kezbesitesiTetel.Updated.Allapot = true;

                Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);

                if (!String.IsNullOrEmpty(result_kezbesitesiTetel_Insert.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kezbesitesiTetel_Insert);
                }

                #endregion
            }

            #region Borító típus változásának vizsgálata

            if (Record.Updated.BoritoTipus == true && !String.IsNullOrEmpty(Record.BoritoTipus)
                && kuldemeny_regi != null && kuldemeny_regi.BoritoTipus != Record.BoritoTipus)
            {

                // Kell-e módosítani a mellékletet? (iratról borítékra, vagy fordítva, vagy fel kell venni mellékletet, vagy törölni kell)

                if (kuldemeny_regi.BoritoTipus != KodTarak.BoritoTipus.Boritek && kuldemeny_regi.BoritoTipus != KodTarak.BoritoTipus.Irat
                    && (Record.BoritoTipus == KodTarak.BoritoTipus.Irat || Record.BoritoTipus == KodTarak.BoritoTipus.Boritek))
                {
                    // Melléklet felvétele (Vonalkód átmásolása a mellékletekhez)   
                    Record.Id = ExecParam.Record_Id;
                    this.SaveBarcode(ExecParam.Clone(), Record);
                }
                else if ((kuldemeny_regi.BoritoTipus == KodTarak.BoritoTipus.Boritek || kuldemeny_regi.BoritoTipus == KodTarak.BoritoTipus.Irat)
                    && (Record.BoritoTipus == KodTarak.BoritoTipus.Boritek || Record.BoritoTipus == KodTarak.BoritoTipus.Irat))
                {
                    #region Melléklet típusának megváltoztatása:

                    // melléklet lekérése:
                    EREC_MellekletekService service_Mellekletek = new EREC_MellekletekService(this.dataContext);

                    EREC_MellekletekSearch search_melleklet = new EREC_MellekletekSearch();
                    search_melleklet.KuldKuldemeny_Id.Filter(ExecParam.Record_Id);

                    if (kuldemeny_regi.BoritoTipus == KodTarak.BoritoTipus.Irat)
                    {
                        search_melleklet.AdathordozoTipus.Filter(KodTarak.AdathordozoTipus.PapirAlapu);
                    }
                    else
                    {
                        search_melleklet.AdathordozoTipus.Filter(KodTarak.AdathordozoTipus.Boritek);
                    }

                    //LZS - BUG_2923
                    if (!string.IsNullOrEmpty(kuldemeny_regi.BarCode))
                    {
                        search_melleklet.BarCode.Filter(kuldemeny_regi.BarCode);
                    }

                    Result result_mellekletGetAll = service_Mellekletek.GetAll(ExecParam.Clone(), search_melleklet);
                    result_mellekletGetAll.CheckError();

                    // Egy ilyen mellékletnek kell lennie elvileg:
                    if (result_mellekletGetAll.GetCount == 1)
                    {
                        DataRow row = result_mellekletGetAll.Ds.Tables[0].Rows[0];
                        string row_Id = row["Id"].ToString();
                        string row_Ver = row["Ver"].ToString();

                        EREC_Mellekletek mellekletAdatok = new EREC_Mellekletek();
                        mellekletAdatok.Updated.SetValueAll(false);
                        mellekletAdatok.Base.Updated.SetValueAll(false);

                        // Adathordozó típusának megváltoztatása:
                        if (Record.BoritoTipus == KodTarak.BoritoTipus.Irat)
                        {
                            mellekletAdatok.AdathordozoTipus = KodTarak.AdathordozoTipus.PapirAlapu;
                        }
                        else
                        {
                            mellekletAdatok.AdathordozoTipus = KodTarak.AdathordozoTipus.Boritek;
                        }
                        mellekletAdatok.Updated.AdathordozoTipus = true;

                        mellekletAdatok.Base.Ver = row_Ver;
                        mellekletAdatok.Base.Updated.Ver = true;

                        ExecParam execParam_MellekletUpdate = ExecParam.Clone();
                        execParam_MellekletUpdate.Record_Id = row_Id;

                        Result result_mellekletUpdate = service_Mellekletek.Update(execParam_MellekletUpdate, mellekletAdatok);
                        result_mellekletUpdate.CheckError();
                    }

                    #endregion
                }
                else if ((kuldemeny_regi.BoritoTipus == KodTarak.BoritoTipus.Boritek || kuldemeny_regi.BoritoTipus == KodTarak.BoritoTipus.Irat)
                    && Record.BoritoTipus == KodTarak.BoritoTipus.EldobhatoBoritek)
                {
                    #region Melléklet érvénytelenítése

                    // melléklet lekérése:
                    EREC_MellekletekService service_Mellekletek = new EREC_MellekletekService(this.dataContext);

                    EREC_MellekletekSearch search_melleklet = new EREC_MellekletekSearch();
                    search_melleklet.KuldKuldemeny_Id.Filter(ExecParam.Record_Id);

                    if (kuldemeny_regi.BoritoTipus == KodTarak.BoritoTipus.Irat)
                    {
                        search_melleklet.AdathordozoTipus.Filter(KodTarak.AdathordozoTipus.PapirAlapu);
                    }
                    else
                    {
                        search_melleklet.AdathordozoTipus.Filter(KodTarak.AdathordozoTipus.Boritek);
                    }

                    search_melleklet.BarCode.Filter(kuldemeny_regi.BarCode);

                    Result result_mellekletGetAll = service_Mellekletek.GetAll(ExecParam.Clone(), search_melleklet);
                    result_mellekletGetAll.CheckError();

                    // Egy ilyen mellékletnek kell lennie elvileg:
                    if (result_mellekletGetAll.GetCount == 1)
                    {
                        DataRow row = result_mellekletGetAll.Ds.Tables[0].Rows[0];
                        string row_Id = row["Id"].ToString();

                        // Melléklet érvénytelenítése:
                        ExecParam execParam_mellekletInvalidate = ExecParam.Clone();
                        execParam_mellekletInvalidate.Record_Id = row_Id;

                        Result result_mellekletInvalidate = service_Mellekletek.Invalidate(execParam_mellekletInvalidate);
                        result_mellekletInvalidate.CheckError();
                    }

                    #endregion
                }

            }

            #endregion

            result = result_kuldemenyUpdate;

            #region Partner cím
            Result result_bindCim = BindPartnerCim(ExecParam.Clone(), Record);
            result_bindCim.CheckError();
            #endregion Partner cím

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_KuldKuldemenyek", "KuldemenyModify").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private Result BindPartnerCim(ExecParam execParam, EREC_KuldKuldemenyek _EREC_KuldKuldemenyek)
    {
        Result result = new Result();

        if (_EREC_KuldKuldemenyek.Updated.Cim_Id && _EREC_KuldKuldemenyek.Updated.Partner_Id_Bekuldo
            && !String.IsNullOrEmpty(_EREC_KuldKuldemenyek.Cim_Id) && !String.IsNullOrEmpty(_EREC_KuldKuldemenyek.Partner_Id_Bekuldo))
        {
            Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            execParam.Record_Id = _EREC_KuldKuldemenyek.Partner_Id_Bekuldo;

            result = service_partner.BindCim(execParam, _EREC_KuldKuldemenyek.Cim_Id);
        }

        return result;
    }

    /// <summary>
    /// Küldemény módosítása + partner adószám és cím insert/update, ha kell
    /// Felelõs változásának figyelése: ha változik a felelõs, kézbesítési tétel generálás
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <param name="_EREC_Szamlak">Kiegészítõ számla adatok az EDOK-ban való tárolásra</param>
    /// <param name="_SzamlaErkeztetesParameterek">Egyéb kiegészítõ számla adatok, pl. partner frissítésére, PIR-EDOK átvitelhez</param>

    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result SzamlaUpdate(ExecParam ExecParam, EREC_KuldKuldemenyek Record, EREC_Szamlak _EREC_Szamlak, SzamlaErkeztetesParameterek _SzamlaErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Partner cím
            //if (_SzamlaErkeztetesParameterek.BindPartnerCim && !String.IsNullOrEmpty(Record.Cim_Id))
            //{
            //    if (!_EREC_Szamlak.Partner_Id_Szallito.Equals(Record.Partner_Id_Bekuldo, StringComparison.InvariantCultureIgnoreCase)
            //        || !_EREC_Szamlak.Cim_Id_Szallito.Equals(Record.Cim_Id, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            //        ExecParam execParam_bindCim = ExecParam.Clone();
            //        execParam_bindCim.Record_Id = Record.Partner_Id_Bekuldo;

            //        Result result_bindCim = service_partner.BindCim(execParam_bindCim, Record.Cim_Id);

            //        if (result_bindCim.IsError)
            //        {
            //            throw new ResultException(result_bindCim);
            //        }
            //    }
            //}

            if (_SzamlaErkeztetesParameterek.BindPartnerCim)
            {
                if (Record.Updated.Partner_Id_Bekuldo && Record.Updated.Cim_Id
                    && _EREC_Szamlak.Partner_Id_Szallito.Equals(Record.Partner_Id_Bekuldo, StringComparison.InvariantCultureIgnoreCase)
                    && _EREC_Szamlak.Cim_Id_Szallito.Equals(Record.Cim_Id, StringComparison.InvariantCultureIgnoreCase))
                {
                    // már az update-nél mentõdik, nem kapcsoljuk össze még egyszer
                    _SzamlaErkeztetesParameterek.BindPartnerCim = false;
                }
            }
            #endregion Partner cím

            // visszaküldött számla küldemény megfelelõje lezárt állapotba kerül!
            if (!String.IsNullOrEmpty(_EREC_Szamlak.VisszakuldesDatuma) && _EREC_Szamlak.Updated.VisszakuldesDatuma)
            {
                Record.Allapot = KodTarak.KULDEMENY_ALLAPOT.Lezarva;
                Record.Updated.Allapot = true;

                _EREC_Szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Elutasitott;
                _EREC_Szamlak.Updated.PIRAllapot = true;
            }
            else
            {
                _EREC_Szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Fogadott;
                _EREC_Szamlak.Updated.PIRAllapot = true;
            }

            Result result_update = this.Update(ExecParam, Record);
            result_update.CheckError();

            EREC_SzamlakService service_szamla = new EREC_SzamlakService(this.dataContext);
            ExecParam execParam_szamlaUpdate = ExecParam.Clone();
            execParam_szamlaUpdate.Record_Id = _EREC_Szamlak.Id;

            _SzamlaErkeztetesParameterek.Vonalkod = Record.BarCode;
            _SzamlaErkeztetesParameterek.BeerkezesIdeje = Record.BeerkezesIdeje;

            Result result_szamlaUpdate = service_szamla.SzamlaModositas(execParam_szamlaUpdate, _EREC_Szamlak, _SzamlaErkeztetesParameterek);
            result_szamlaUpdate.CheckError();

            result = result_update;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_Szamlak", "SzamlaModify").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Küldemény érkeztetése + partner adószám és cím insert/update, ha kell
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="Record">A létrehozandó küldemény adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthetõ mezõket veszi figyelembe.</param>
    /// <param name="_EREC_Szamlak">Kiegészítõ számla adatok az EDOK-ban való tárolásra</param>
    /// <param name="_SzamlaErkeztetesParameterek">Egyéb kiegészítõ számla adatok, pl. partner frissítésére, PIR-EDOK átvitelhez</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result SzamlaErkeztetes(ExecParam ExecParam, EREC_KuldKuldemenyek Record, EREC_Szamlak _EREC_Szamlak, EREC_HataridosFeladatok _EREC_HataridosFeladatok, SzamlaErkeztetesParameterek _SzamlaErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        // Érkeztetõkönyv meglétének ellenõrzése:
        if (String.IsNullOrEmpty(Record.IraIktatokonyv_Id))
        {
            // Nincs megadva érkeztetõkönyv
            return ResultError.CreateNewResultWithErrorCode(52458);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Partner cím
            //if (_SzamlaErkeztetesParameterek.BindPartnerCim && !String.IsNullOrEmpty(Record.Cim_Id))
            //{
            //    if (!_EREC_Szamlak.Partner_Id_Szallito.Equals(Record.Partner_Id_Bekuldo, StringComparison.InvariantCultureIgnoreCase)
            //        || !_EREC_Szamlak.Cim_Id_Szallito.Equals(Record.Cim_Id, StringComparison.InvariantCultureIgnoreCase))
            //    {
            //        Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            //        ExecParam execParam_bindCim = ExecParam.Clone();
            //        execParam_bindCim.Record_Id = Record.Partner_Id_Bekuldo;

            //        Result result_bindCim = service_partner.BindCim(execParam_bindCim, Record.Cim_Id);

            //        if (result_bindCim.IsError)
            //        {
            //            throw new ResultException(result_bindCim);
            //        }
            //    }
            //}

            if (_SzamlaErkeztetesParameterek.BindPartnerCim)
            {
                if (Record.Updated.Partner_Id_Bekuldo && Record.Updated.Cim_Id
                    && _EREC_Szamlak.Partner_Id_Szallito.Equals(Record.Partner_Id_Bekuldo, StringComparison.InvariantCultureIgnoreCase)
                    && _EREC_Szamlak.Cim_Id_Szallito.Equals(Record.Cim_Id, StringComparison.InvariantCultureIgnoreCase))
                {
                    // már az érkeztetésnél mentõdik, nem kapcsoljuk össze még egyszer
                    _SzamlaErkeztetesParameterek.BindPartnerCim = false;
                }
            }
            #endregion Partner cím

            // visszaküldött számla küldemény megfelelõje lezárt állapotba kerül!
            if (!String.IsNullOrEmpty(_EREC_Szamlak.VisszakuldesDatuma) && _EREC_Szamlak.Updated.VisszakuldesDatuma)
            {
                Record.Allapot = KodTarak.KULDEMENY_ALLAPOT.Lezarva;
                Record.Updated.Allapot = true;

                _EREC_Szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Elutasitott;
                _EREC_Szamlak.Updated.PIRAllapot = true;
            }
            else
            {
                _EREC_Szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Fogadott;
                _EREC_Szamlak.Updated.PIRAllapot = true;
            }

            Result result_erkeztetes = this.Erkeztetes(ExecParam, Record, _EREC_HataridosFeladatok, _SzamlaErkeztetesParameterek);
            result_erkeztetes.CheckError();

            result = result_erkeztetes;
            EREC_SzamlakService service_szamla = new EREC_SzamlakService(this.dataContext);
            ExecParam execParam_szamlaInsert = ExecParam.Clone();

            _EREC_Szamlak.KuldKuldemeny_Id = result_erkeztetes.Uid;
            _EREC_Szamlak.Updated.KuldKuldemeny_Id = true;

            _SzamlaErkeztetesParameterek.Vonalkod = Record.BarCode;
            _SzamlaErkeztetesParameterek.BeerkezesIdeje = Record.BeerkezesIdeje;

            Result result_szamlaInsert = service_szamla.SzamlaFelvitel(execParam_szamlaInsert, _EREC_Szamlak, _SzamlaErkeztetesParameterek);

            if (result_szamlaInsert.IsError)
            {
                // CR3359 Számla átadásnál (PIR interface) új csoportosító mezõ (ettõl függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
                // PIR Interface által visszaadott hiba
                if (result_szamlaInsert.ErrorCode.Substring(0, 3) == "PIR")
                {
                    #region objektumfüggõ tárgyszavak mentése a küldeményhez
                    //if (_SzamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray != null && _SzamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray.Length > 0)
                    //{
                    //    ExecParam execparam_ot = ExecParam.Clone();
                    //    EREC_ObjektumTargyszavaiService service_ot = new EREC_ObjektumTargyszavaiService(this.dataContext);
                    //    Result result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                    //                , _SzamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray
                    //                , result_erkeztetes.Uid
                    //                , null
                    //                , Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek
                    //                , Contentum.eUtility.Constants.ColumnNames.EREC_KuldKuldemenyek.Tipus
                    //                , null
                    //                , false
                    //                , null
                    //                , false
                    //                , true
                    //                );

                    //    if (result_ot.IsError)
                    //    {
                    //        throw new ResultException(result_ot);
                    //    }
                    //}
                    #endregion objektumfüggõ tárgyszavak mentése a küldeményhez


                    // CR3359 Számla átadásnál (PIR interface) új csoportosító mezõ (ettõl függ hogy BOPMH-ban melyik webservice-re adja át a számlát)
                    result.ErrorCode = result_szamlaInsert.ErrorCode;
                    result.ErrorMessage = result_szamlaInsert.ErrorMessage;

                }
                else throw new ResultException(result_szamlaInsert); //1000132
            }

            //#region objektumfüggõ tárgyszavak mentése a küldeményhez
            ////if (_SzamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray != null && _SzamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray.Length > 0)
            ////{
            ////    ExecParam execparam_ot = ExecParam.Clone();
            ////    EREC_ObjektumTargyszavaiService service_ot = new EREC_ObjektumTargyszavaiService(this.dataContext);
            ////    Result result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
            ////                , _SzamlaErkeztetesParameterek.EREC_ObjektumTargyszavaiArray
            ////                , result_erkeztetes.Uid
            ////                , null
            ////                , Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek
            ////                , Contentum.eUtility.Constants.ColumnNames.EREC_KuldKuldemenyek.Tipus
            ////                , null
            ////                , false
            ////                , null
            ////                , false
            ////                , true
            ////                );

            ////    if (result_ot.IsError)
            ////    {
            ////        throw new ResultException(result_ot);
            ////    }
            ////}
            //#endregion objektumfüggõ tárgyszavak mentése a küldeményhez

            //result = result_erkeztetes;
            //// CR3359
            //if (result_szamlaInsert.ErrorCode.Substring(0,3) == "PIR") //1000132
            //{
            //    result.ErrorCode = result_szamlaInsert.ErrorCode;
            //    result.ErrorMessage = result_szamlaInsert.ErrorMessage;
            //}

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_Szamlak", "SzamlaNew").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }


    private int erkeztetesRepeatNumber = 0;

    /// <summary>
    /// Küldemény érkeztetése
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="Record">A létrehozandó küldemény adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthetõ mezõket veszi figyelembe.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result Erkeztetes(ExecParam ExecParam, EREC_KuldKuldemenyek Record, EREC_HataridosFeladatok _EREC_HataridosFeladatok, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        // Érkeztetõkönyv meglétének ellenõrzése:
        if (String.IsNullOrEmpty(Record.IraIktatokonyv_Id))
        {
            // Nincs megadva érkeztetõkönyv
            return ResultError.CreateNewResultWithErrorCode(52458);
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (_ErkeztetesParameterek == null) _ErkeztetesParameterek = new ErkeztetesParameterek();

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Érkeztetõkönyv GET
            EREC_IraIktatoKonyvekService service = new EREC_IraIktatoKonyvekService(this.dataContext);
            ExecParam IraIktatoKonyvekExecParam = ExecParam.Clone();

            IraIktatoKonyvekExecParam.Record_Id = Record.IraIktatokonyv_Id;

            Result IraIktatoKonyvekResult = service.Get(IraIktatoKonyvekExecParam);
            if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
            {
                throw new ResultException(IraIktatoKonyvekResult);
            }

            if (IraIktatoKonyvekResult.Record == null)
            {
                throw new ResultException(52450);
            }

            EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (IraIktatoKonyvekResult.Record as EREC_IraIktatoKonyvek);

            // lezárt érkeztetõkönyvbe nem lehet érkeztetni, kivéve migráció esetén
            if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek) == true && System.Web.Configuration.WebConfigurationManager.AppSettings["MODE"].ToUpper() != "MIGRATION")
            {
                throw new ResultException(52449);
            }

            #endregion

            Int32 erkeztetoszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoFoszam) + 1;

            if (!_ErkeztetesParameterek.TomegesErkeztetesVegrehajtasa)
            {
                #region Érkeztetõkönyv UPDATE

                erec_IraIktatoKonyvek.Updated.SetValueAll(false);
                erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);

                erec_IraIktatoKonyvek.UtolsoFoszam = erkeztetoszam.ToString();
                erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;

                erec_IraIktatoKonyvek.Base.Updated.Ver = true;

                IraIktatoKonyvekResult = service.Update(IraIktatoKonyvekExecParam, erec_IraIktatoKonyvek);
                if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
                {
                    // hiba:

                    // Ha verzióütközés van:
                    int errorNumber = ResultError.GetErrorNumberFromResult(IraIktatoKonyvekResult);
                    if (errorNumber == 50402 || errorNumber == 50401)
                    {
                        Logger.Error("Egyidejû érkeztetés: már módosult az érkeztetõkönyv verziója", ExecParam, IraIktatoKonyvekResult);
                        // Nem egyezik a verzió --> valami normális hibaüzenet kell a felhasználónak
                        throw new ResultException(52459);
                    }

                    throw new ResultException(IraIktatoKonyvekResult);
                }

                #endregion
            }



            #region Küldemény INSERT

            if (!_ErkeztetesParameterek.TomegesErkeztetesVegrehajtasa)
            {
                Record.Erkezteto_Szam = erkeztetoszam.ToString();
                Record.Updated.Erkezteto_Szam = true;
            }
            else
            {
                Record.Updated.Erkezteto_Szam = false;
            }

            // Állapot és FelhasznaloCsoport_Id_Orzo
            // Beállítás átvéve a felületrõl, "Érkeztetett" állapotba kerül, kivéve ha a Kezelõ nem a futtató user (Insert()-ben figyeljük)
            // vagy a küldemény számla típusú és eleve lezárt állapotban kell létrehozni (visszaküldött számla)
            if (Record.Tipus != KodTarak.KULDEMENY_TIPUS.Szamla
                || String.IsNullOrEmpty(Record.Allapot) || Record.Updated.Allapot == false)
            {
                Record.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
                Record.Updated.Allapot = true;
            }

            Record.FelhasznaloCsoport_Id_Orzo = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam.Felhasznalo_Id);
            Record.Updated.FelhasznaloCsoport_Id_Orzo = true;

            Record.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
            Record.Updated.PostazasIranya = true;

            // BUG_14099
            bool isIktatniKellEmpty = string.IsNullOrEmpty(Record.IktatniKell);
            bool isIktatastNemIgenyelEmpty = string.IsNullOrEmpty(Record.IktatastNemIgenyel);

            if (isIktatniKellEmpty && !isIktatastNemIgenyelEmpty)
            {
                Record.IktatniKell = Record.IktatastNemIgenyel;
                Record.Updated.IktatniKell = true;
            }
            if (!isIktatniKellEmpty && isIktatastNemIgenyelEmpty)
            {

                Record.IktatastNemIgenyel = Record.IktatniKell;
                Record.Updated.IktatastNemIgenyel = true;
            }

            Result res = new Result();
            if (!_ErkeztetesParameterek.TomegesErkeztetesVegrehajtasa)
            {
                /// Azonosító (Érkeztetõszám) összeállítása:
                // BLG_292
                //Record.Azonosito = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullErkeztetoSzam(erec_IraIktatoKonyvek, Record);
                Record.Azonosito = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullErkeztetoSzam(ExecParam, erec_IraIktatoKonyvek, Record);

                Record.Updated.Azonosito = true;

                res = this.Insert(ExecParam, Record);

                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    throw new ResultException(res);
                }
            }
            else
            {
                Record.Updated.Azonosito = false;
                ExecParam exec_tomegesIktatasUpdate = ExecParam.Clone();
                exec_tomegesIktatasUpdate.Record_Id = Record.Id;
                res = this.Update(exec_tomegesIktatasUpdate, Record);
                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    throw new ResultException(res);
                }
                res.Uid = Record.Id;
            }

            #endregion
            // BUG_8080
            //Record.Id = res.Uid;

            var execParam_Kuldemeny = ExecParam.Clone();
            execParam_Kuldemeny.Record_Id = res.Uid;
            Result KRT_Kuldemeny = sp.Get(execParam_Kuldemeny);
            Record = (KRT_Kuldemeny.Record as EREC_KuldKuldemenyek);


            //Vonalkód átmásolása a mellékletekhez
            this.SaveBarcode(ExecParam, Record);


            #region Visszatérési infomációk
            // BLG_292
            //string FullErkeztetoSzam = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullErkeztetoSzam(erec_IraIktatoKonyvek, Record);
            string FullErkeztetoSzam = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullErkeztetoSzam(ExecParam, erec_IraIktatoKonyvek, Record);

            ErkeztetesIktatasResult eiResult = new ErkeztetesIktatasResult();
            eiResult.KuldemenyId = res.Uid;
            eiResult.KuldemenyAzonosito = FullErkeztetoSzam;
            res.Record = eiResult;
            #endregion

            result = res;

            #region Partner cím
            Result result_bindCim = BindPartnerCim(ExecParam.Clone(), Record);
            result_bindCim.CheckError();
            #endregion Partner cím

            #region Feljegyzés

            // Kezelési feljegyzések felvétel, ha meg van adva
            if (_EREC_HataridosFeladatok != null)
            {
                // Irat kezelési feljegyzés INSERT
                EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                ExecParam execParam_kezfeljegyzesek = ExecParam.Clone();

                _EREC_HataridosFeladatok.Obj_Id = eiResult.KuldemenyId;
                _EREC_HataridosFeladatok.Updated.Obj_Id = true;

                _EREC_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                _EREC_HataridosFeladatok.Updated.Obj_type = true;

                if (!String.IsNullOrEmpty(eiResult.KuldemenyAzonosito))
                {
                    _EREC_HataridosFeladatok.Azonosito = eiResult.KuldemenyAzonosito;
                    _EREC_HataridosFeladatok.Updated.Azonosito = true;
                }

                Result result_kezfeljegyzes = erec_HataridosFeladatokService.Insert(execParam_kezfeljegyzesek, _EREC_HataridosFeladatok);
                if (!String.IsNullOrEmpty(result_kezfeljegyzes.ErrorCode))
                {
                    Logger.Error("Kezelési feljegyzés létrehozásánál hiba", execParam_kezfeljegyzesek, result_kezfeljegyzes);

                    // hiba
                    throw new ResultException(result_kezfeljegyzes);
                }
            }

            #endregion

            #region mellékletek
            if (_ErkeztetesParameterek != null && _ErkeztetesParameterek.Mellekletek != null)
            {
                if (_ErkeztetesParameterek.Mellekletek.Count > 0)
                {
                    Logger.Debug(String.Format("Mellékletek száma: {0}", _ErkeztetesParameterek.Mellekletek.Count));

                    EREC_MellekletekService svcMellekletek = new EREC_MellekletekService(this.dataContext);
                    ExecParam xpmMellekletek = ExecParam.Clone();
                    Result resMellekletek;

                    foreach (EREC_Mellekletek erec_Mellekletek in _ErkeztetesParameterek.Mellekletek)
                    {
                        erec_Mellekletek.KuldKuldemeny_Id = eiResult.KuldemenyId;
                        resMellekletek = svcMellekletek.Insert(xpmMellekletek, erec_Mellekletek);
                        resMellekletek.CheckError();
                    }
                }
            }
            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string kuldemenyId = res.Uid;

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id
                    , kuldemenyId, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);

            #region Egyidejû érkeztetés figyelése

            int errorNumber = ResultError.GetErrorNumberFromResult(result);

            // Ha egyidejû érkeztetés volt, kis késleltetés után még max. kétszer próbálkozunk:
            if (errorNumber == 52459 && erkeztetesRepeatNumber < 2)
            {
                Logger.Warn("Egyidejû érkeztetés -> érkeztetést még egyszer meghívjuk", ExecParam);

                // számláló növelése (hogy ne próbálkozzon a végtelenségig)
                erkeztetesRepeatNumber++;

                // Késleltetés: (300 és 1000 msec közti véletlen érték)
                Random random = new Random();
                int milliSec = random.Next(300, 1000);
                Logger.Debug("Késleltetési idõ: " + milliSec.ToString() + " msec", ExecParam);

                System.Threading.Thread.Sleep(milliSec);

                // Érkeztetés újbóli meghívása:
                result = this.Erkeztetes(ExecParam.Clone(), Record, _EREC_HataridosFeladatok, _ErkeztetesParameterek);

            }

            #endregion
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Küldemény érkeztetése email-bõl
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="Record">A létrehozandó küldemény adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthetõ mezõket veszi figyelembe.</param>
    /// <param name="erec_eMailBoritekok">A kiválasztott email adatait tartalmazó objektum.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result ErkeztetesEmailbol(ExecParam ExecParam, EREC_KuldKuldemenyek Record, EREC_eMailBoritekok erec_eMailBoritekok, EREC_HataridosFeladatok _EREC_HataridosFeladatok, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            String Kuldemeny_Id = "";
            Result res = new Result();
            if (erec_eMailBoritekok == null)
            {
                throw new ResultException(52450);
            }

            #region Vonalkód generálása, ha nincs megadva:

            if (String.IsNullOrEmpty(Record.BarCode))
            {
                Logger.Debug("Vonalkód generálása küldeménynek...", ExecParam);

                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);

                Result result_barkodGen = service_barkodok.BarkodGeneralas(ExecParam.Clone());
                if (result_barkodGen.IsError)
                {
                    // hiba:
                    Logger.Error("Vonalkód generálás a küldeménynek sikertelen", ExecParam);
                    throw new ResultException(result_barkodGen);
                }

                string generaltBarkod = (string)result_barkodGen.Record;
                Record.BarCode = generaltBarkod;
                Record.Updated.BarCode = true;
            }

            #endregion
            // BUG_4809
            if (String.IsNullOrEmpty(Record.CimzesTipusa))
            {
                Record.CimzesTipusa = KodTarak.KULD_CIMZES_TIPUS.nevre_szolo;
                Record.Updated.CimzesTipusa = true;
            }
            res = Erkeztetes(ExecParam, Record, _EREC_HataridosFeladatok, _ErkeztetesParameterek);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new ResultException(res);
            }

            Kuldemeny_Id = res.Uid;

            EREC_eMailBoritekokService service = new EREC_eMailBoritekokService(this.dataContext);
            erec_eMailBoritekok.Updated.SetValueAll(false);
            erec_eMailBoritekok.Base.Updated.SetValueAll(false);

            erec_eMailBoritekok.KuldKuldemeny_Id = res.Uid;
            erec_eMailBoritekok.Updated.KuldKuldemeny_Id = true;
            erec_eMailBoritekok.FeldolgozasIdo = DateTime.Now.ToString();
            erec_eMailBoritekok.Updated.FeldolgozasIdo = true;

            erec_eMailBoritekok.Base.Updated.Ver = true;

            ExecParam.Record_Id = erec_eMailBoritekok.Id;
            Result _res = service.Update(ExecParam, erec_eMailBoritekok);
            if (!String.IsNullOrEmpty(_res.ErrorCode))
            {
                throw new ResultException(_res);
            }

            #region Csatolmány átmásolása:

            EREC_eMailBoritekCsatolmanyokService erec_eMailBoritekCsatolmanyokService = new EREC_eMailBoritekCsatolmanyokService(this.dataContext);
            EREC_eMailBoritekCsatolmanyokSearch erec_eMailBoritekCsatolmanyokSearch = new EREC_eMailBoritekCsatolmanyokSearch();
            erec_eMailBoritekCsatolmanyokSearch.eMailBoritek_Id.Filter(erec_eMailBoritekok.Id);
            ExecParam.Record_Id = "";
            _res = erec_eMailBoritekCsatolmanyokService.GetAll(ExecParam, erec_eMailBoritekCsatolmanyokSearch);
            _res.CheckError();

            // TODO: nem jon at a futtato neve!!! ezert van egy ContentumSPUser felhasznalo, aki beteszi a dolgokat!!!
            DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

            //"EredetiOutlookUzenet.msg";
            int cntOriginalEmailMessage = (_res.Ds.Tables[0].Select(String.Format("Nev='{0}'", Contentum.eUtility.Constants.EMail.EredetiUzenetFileName))).Length;
            Logger.Info(String.Format("{0} nevû azonosított csatolmányok száma: {1} (összes talált csatolmány: {2})", Contentum.eUtility.Constants.EMail.EredetiUzenetFileName
                , cntOriginalEmailMessage, _res.Ds.Tables[0].Rows.Count));

            if (_res.Ds.Tables[0].Rows.Count > 0 && cntOriginalEmailMessage != 1)
            {
                Logger.Warn("Érkeztetés emailbõl: Az eredeti email üzenet nem azonosítható, ezért nem lesz beállítva dokumentum szerep (fõdokumentum, dokuemntum melléklet)!");
            }

            //System.Data.DataSet ds = _res.Ds;
            foreach (DataRow row in _res.Ds.Tables[0].Rows)
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //ExecParam.Record_Id = ds.Tables[0].Rows[i]["Dokumentum_Id"].ToString();

                //_res = documentService.CopyFromeRecord(ExecParam
                //    , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.KULDEMENY_CSATOLMANY_DOKUMENTUMTARTIPUS)
                //    , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.KULDEMENY_CSATOLMANY_DOKUMENTUMTARTERULET)
                //    , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.KULDEMENY_CSATOLMANY_DOKUMENTUMKONYVTAR)
                //    , Kuldemeny_Id);

                //if (!String.IsNullOrEmpty(_res.ErrorCode))
                //{
                //    Logger.Error(_res.ErrorMessage, ExecParam);
                //    throw new ResultException(_res);
                //}
                //else
                //{
                EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
                EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
                erec_Csatolmanyok.Dokumentum_Id = row["Dokumentum_Id"].ToString(); //ds.Tables[0].Rows[i]["Dokumentum_Id"].ToString(); //_res.Uid;
                erec_Csatolmanyok.Updated.Dokumentum_Id = true;
                erec_Csatolmanyok.KuldKuldemeny_Id = Kuldemeny_Id;
                erec_Csatolmanyok.Updated.KuldKuldemeny_Id = true;
                // TODO:
                erec_Csatolmanyok.Lapszam = "1";
                erec_Csatolmanyok.Updated.Lapszam = true;

                if (cntOriginalEmailMessage == 1)
                {
                    // DokumentumSzerep
                    if (row["Nev"].ToString() == Contentum.eUtility.Constants.EMail.EredetiUzenetFileName) //"EredetiOutlookUzenet.msg";
                    {
                        erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                        erec_Csatolmanyok.Updated.DokumentumSzerep = true;
                    }
                    else
                    {
                        erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
                        erec_Csatolmanyok.Updated.DokumentumSzerep = true;
                    }
                }

                _res = erec_CsatolmanyokService.Insert(ExecParam.Clone(), erec_Csatolmanyok);

                if (!String.IsNullOrEmpty(_res.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(_res);
                }
                //}
            }

            #endregion

            res.Uid = Kuldemeny_Id;

            result = res;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id
                    , Kuldemeny_Id, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    private string CimFelvetel(ExecParam execParam, Contentum.eUtility.EKF.Models.EKF_Form_XML_Model modelFormXml, string nev)
    {
        var krt_Cimek = new KRT_Cimek
        {
            Nev = nev,
            IRSZ = modelFormXml.Type == 1 ? modelFormXml.FormCompanyAddressZip : modelFormXml.CpfBenyujtoLakcimIrsz,
            TelepulesNev = modelFormXml.Type == 1 ? modelFormXml.FormCompanyAddressCity : modelFormXml.CpfBenyujtoLakcimTelepules,
            KozteruletNev = modelFormXml.Type == 1 ? modelFormXml.FormCompanyAddressId : modelFormXml.CpfBenyujtoLakcim,
            Tipus = KodTarak.Cim_Tipus.Postai,
            Kategoria = "K"
        };

        krt_Cimek.Updated.SetValueAll(true);
        krt_Cimek.Base.Updated.SetValueAll(true);

        using (var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService())
        {
            var xp = execParam.Clone();
            var result = cimekService.Insert(xp, krt_Cimek);
            if (!result.IsError)
            {
                return result.Uid;
            }

            Logger.Error("Cím felvétele nem sikerült", xp, result);
            return "";
        }
    }

    private string FormatCpfCim(Contentum.eUtility.EKF.Models.EKF_Form_XML_Model modelFormXml)
    {
        return String.Format("{0} {1} {2}", modelFormXml.CpfBenyujtoLakcimIrsz, modelFormXml.CpfBenyujtoLakcimTelepules, modelFormXml.CpfBenyujtoLakcim);
    }

    private KRT_Partnerek GetPartner(ExecParam execParam, KRT_PartnerekSearch partnerSearch)
    {
        if (partnerSearch == null)
        {
            return null;
        }

        partnerSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
        partnerSearch.ErvKezd.AndGroup("100");

        partnerSearch.ErvVege.GreaterOrEqual(Query.SQLFunction.getdate);
        partnerSearch.ErvVege.AndGroup("100");

        partnerSearch.OrderBy = " LetrehozasIdo DESC";
        partnerSearch.TopRow = 1;

        using (var partnerService = eAdminService.ServiceFactory.GetKRT_PartnerekService())
        {
            var result = partnerService.GetAll(execParam.Clone(), partnerSearch);
            result.CheckError();

            if (result.GetCount > 0)
            {
                var res = new KRT_Partnerek();
                Utility.LoadBusinessDocumentFromDataRow(res, result.Ds.Tables[0].Rows[0]);
                return res;
            }
        }
        return null;
    }

    // BUG_11574
    private void BekuldoEllenorzes(ExecParam execParam, EREC_KuldKuldemenyek kuldemeny, EREC_eBeadvanyok eBeadvany)
    {
        if (kuldemeny == null)
        {
            throw new ArgumentNullException("kuldemeny");
        }

        var eBeadvanyFormXmlHelper = new Contentum.eUtility.EKF.EBeadvanyFormXmlHelper();
        var modelFormXml = eBeadvanyFormXmlHelper.ExtractDataFromEBeadvanyFormXml(new Guid(eBeadvany.Id), execParam);
        if (modelFormXml == null)
        {
            return;
        }

        // beküldő létrehozása típus alapján
        EREC_KuldBekuldok bekuldo = null;
        var partnerSearch = new KRT_PartnerekSearch();
        switch (modelFormXml.Type)
        {
            case 1:
                {
                    var azonosito = "";
                    if (!String.IsNullOrEmpty(modelFormXml.FormCompanyId) && !String.IsNullOrEmpty(modelFormXml.FormHirId))
                    {
                        azonosito = modelFormXml.FormHirId + "," + modelFormXml.FormCompanyId; // NOTE: eIntegrator\WebService\App_Code\AdatKapu\PartnerSync\AdatkapuDataBasePartnerManager.cs GenerateMainPartner
                    }
                    else if (!String.IsNullOrEmpty(modelFormXml.FormHirId))
                    {
                        azonosito = modelFormXml.FormHirId + ",NULL";
                    }
                    else if (!string.IsNullOrEmpty(modelFormXml.FormCompanyId))
                    {
                        azonosito = "NULL," + modelFormXml.FormCompanyId;
                    }

                    // beküldő létrehozása
                    bekuldo = new EREC_KuldBekuldok
                    {
                        NevSTR = modelFormXml.FormCompanyName,
                        CimSTR = String.Format("{0} {1} {2}", modelFormXml.FormCompanyAddressZip, modelFormXml.FormCompanyAddressCity, modelFormXml.FormCompanyAddressId)
                    };

                    // partner keresése
                    if (!String.IsNullOrEmpty(azonosito))
                    {
                        partnerSearch.KulsoAzonositok.Filter(azonosito);
                        var partner = GetPartner(execParam.Clone(), partnerSearch);
                        if (partner == null && modelFormXml.FormHirId != null && modelFormXml.FormHirId.Length > 1) // utolsó esély, új keresés: LIKE FormHirId
                        {
                            partnerSearch = new KRT_PartnerekSearch();
                            partnerSearch.KulsoAzonositok.Like('%' + modelFormXml.FormHirId + '%');
                            partner = GetPartner(execParam.Clone(), partnerSearch);
                        }

                        if (partner != null)
                        {
                            bekuldo.Partner_Id_Bekuldo = partner.Id;
                        }
                    }
                    break;
                }

            case 2:
                {
                    if (!String.IsNullOrEmpty(modelFormXml.CpfSzervezet))
                    {
                        bekuldo = new EREC_KuldBekuldok
                        {
                            NevSTR = modelFormXml.CpfSzervezet,
                            CimSTR = FormatCpfCim(modelFormXml)
                        };

                        partnerSearch.Nev.Filter(modelFormXml.CpfSzervezet);
                        var partner = GetPartner(execParam.Clone(), partnerSearch);
                        if (partner != null)
                        {
                            bekuldo.Partner_Id_Bekuldo = partner.Id;
                        }
                    }
                    break;
                }

            case 3:
                {
                    if (!String.IsNullOrEmpty(modelFormXml.CpfCsaladiNev) && !String.IsNullOrEmpty(modelFormXml.CpfUtonev1))
                    {
                        bekuldo = new EREC_KuldBekuldok
                        {
                            NevSTR = modelFormXml.CpfCsaladiNev + " " + modelFormXml.CpfUtonev1 + (String.IsNullOrEmpty(modelFormXml.CpfUtonev2) ? "" : " " + modelFormXml.CpfUtonev2),
                            CimSTR = FormatCpfCim(modelFormXml)
                        };

                        partnerSearch.SzemelyekSearch.UjCsaladiNev.Filter(modelFormXml.CpfCsaladiNev);
                        partnerSearch.SzemelyekSearch.UjUtonev.Filter(modelFormXml.CpfUtonev1);
                        partnerSearch.SzemelyekSearch.UjTovabbiUtonev.FilterIfNotEmpty(modelFormXml.CpfUtonev2);

                        var partner = GetPartner(execParam.Clone(), partnerSearch);
                        if (partner != null)
                        {
                            bekuldo.Partner_Id_Bekuldo = partner.Id;
                        }
                    }
                    break;
                }

            default:
                {
                    break;
                }
        }

        // beküldő felvétele
        if (bekuldo != null)
        {
            bekuldo.Updated.SetValueAll(false);
            bekuldo.Base.Updated.SetValueAll(false);

            // cím keresése
            var cimId = CimUtility.GetCimIdByStr(execParam.Clone(), bekuldo.CimSTR);
            if (String.IsNullOrEmpty(cimId))
            {
                // nincs ilyen cím, fel kell venni
                cimId = CimFelvetel(execParam.Clone(), modelFormXml, bekuldo.NevSTR);
            }

            if (!String.IsNullOrEmpty(cimId))
            {
                bekuldo.PartnerCim_Id_Bekuldo = cimId;
                bekuldo.Updated.PartnerCim_Id_Bekuldo = true;
            }

            bekuldo.Updated.Partner_Id_Bekuldo = true;
            bekuldo.Updated.NevSTR = true;
            bekuldo.Updated.CimSTR = true;

            bekuldo.KuldKuldemeny_Id = kuldemeny.Id;
            bekuldo.Updated.KuldKuldemeny_Id = true;

            using (var kuldBekuldokService = new EREC_KuldBekuldokService(this.dataContext))
            {
                var resultBekuldo = kuldBekuldokService.Insert(execParam.Clone(), bekuldo);
                resultBekuldo.CheckError();
            }

            // küldemény Partner_Id_Bekuldo és Cim_Id módosítása
            if (bekuldo != null && (kuldemeny.Partner_Id_Bekuldo != bekuldo.Partner_Id_Bekuldo || kuldemeny.Cim_Id != bekuldo.PartnerCim_Id_Bekuldo))
            {
                kuldemeny.Updated.SetValueAll(false);
                kuldemeny.Base.Updated.SetValueAll(false);

                kuldemeny.Partner_Id_Bekuldo = bekuldo.Partner_Id_Bekuldo;
                kuldemeny.Updated.Partner_Id_Bekuldo = true;

                kuldemeny.Cim_Id = bekuldo.PartnerCim_Id_Bekuldo;
                kuldemeny.Updated.Cim_Id = true;

                kuldemeny.Base.Ver = "1";
                kuldemeny.Base.Updated.Ver = true;

                var kuldemenyService = new EREC_KuldKuldemenyekService(dataContext);
                var xp = execParam.Clone();
                xp.Record_Id = kuldemeny.Id;
                var kuldemenyResult = kuldemenyService.Update(xp, kuldemeny);
                kuldemenyResult.CheckError();
            }
        }
    }

    /// <summary>
    /// Küldemény érkeztetése hivatali kapus üzenetbõl
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="Record">A létrehozandó küldemény adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthetõ mezõket veszi figyelembe.</param>
    /// <param name="erec_eMailBoritekok">A kiválasztott hivatali kapus üzenet adatait tartalmazó objektum.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result ErkeztetesHivataliKapubol(ExecParam ExecParam, EREC_KuldKuldemenyek Record, string eBeadvany_Id, EREC_HataridosFeladatok _EREC_HataridosFeladatok, ErkeztetesParameterek _ErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res = new Result();

            #region Vonalkód generálása, ha nincs megadva:

            if (String.IsNullOrEmpty(Record.BarCode))
            {
                Logger.Debug("Vonalkód generálása küldeménynek...", ExecParam);

                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);

                Result result_barkodGen = service_barkodok.BarkodGeneralas(ExecParam.Clone());
                if (result_barkodGen.IsError)
                {
                    // hiba:
                    Logger.Error("Vonalkód generálás a küldeménynek sikertelen", ExecParam);
                    throw new ResultException(result_barkodGen);
                }

                string generaltBarkod = (string)result_barkodGen.Record;
                Record.BarCode = generaltBarkod;
                Record.Updated.BarCode = true;
            }

            #endregion

            res = Erkeztetes(ExecParam, Record, _EREC_HataridosFeladatok, _ErkeztetesParameterek);
            res.CheckError();
            Record.Id = res.Uid;

            // eBeadvány lekérdezése
            var eBeadvanyService = new EREC_eBeadvanyokService(this.dataContext);

            ExecParam xpmGet = ExecParam.Clone();
            xpmGet.Record_Id = eBeadvany_Id;
            Result resGet = eBeadvanyService.Get(xpmGet);
            resGet.CheckError();

            EREC_eBeadvanyok erec_eBeadvanyok = resGet.Record as EREC_eBeadvanyok;

            BekuldoEllenorzes(ExecParam.Clone(), Record, erec_eBeadvanyok); // BUG_11574

            erec_eBeadvanyok.Updated.SetValueAll(false);
            erec_eBeadvanyok.Base.Updated.SetValueAll(false);

            erec_eBeadvanyok.KuldKuldemeny_Id = res.Uid;
            erec_eBeadvanyok.Updated.KuldKuldemeny_Id = true;
            //erec_eBeadvanyok.FeldolgozasIdo = DateTime.Now.ToString();
            //erec_eBeadvanyok.Updated.FeldolgozasIdo = true;

            erec_eBeadvanyok.Base.Updated.Ver = true;

            ExecParam.Record_Id = erec_eBeadvanyok.Id;
            Result _res = eBeadvanyService.Update(ExecParam, erec_eBeadvanyok);
            _res.CheckError();

            #region Csatolmány átmásolása

            EREC_eBeadvanyCsatolmanyokService erec_eBeadvanyCsatolmanyokService = new EREC_eBeadvanyCsatolmanyokService(this.dataContext);
            EREC_eBeadvanyCsatolmanyokSearch erec_eBeadvanyCsatolmanyokSearch = new EREC_eBeadvanyCsatolmanyokSearch();
            erec_eBeadvanyCsatolmanyokSearch.eBeadvany_Id.Filter(erec_eBeadvanyok.Id);
            ExecParam.Record_Id = String.Empty;
            _res = erec_eBeadvanyCsatolmanyokService.GetAll(ExecParam, erec_eBeadvanyCsatolmanyokSearch);
            _res.CheckError();

            EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
            foreach (DataRow row in _res.Ds.Tables[0].Rows)
            {
                EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
                erec_Csatolmanyok.Dokumentum_Id = row["Dokumentum_Id"].ToString();
                erec_Csatolmanyok.Updated.Dokumentum_Id = true;
                erec_Csatolmanyok.KuldKuldemeny_Id = Record.Id;
                erec_Csatolmanyok.Updated.KuldKuldemeny_Id = true;

                // DokumentumSzerep
                if (row["Nev"].ToString() == erec_eBeadvanyok.KR_FileNev)
                {
                    erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
                    erec_Csatolmanyok.Updated.DokumentumSzerep = true;
                }
                else
                {
                    erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
                    erec_Csatolmanyok.Updated.DokumentumSzerep = true;
                }

                _res = erec_CsatolmanyokService.Insert(ExecParam.Clone(), erec_Csatolmanyok);
                _res.CheckError();
            }

            #endregion

            res.Uid = Record.Id;

            result = res;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id
                    , Record.Id, "EREC_KuldKuldemenyek", "KuldemenyNew").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Küldemény sztornózása
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza, Record_Id tartalmazza a sztornózandó küldemény azonosítóját.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result Sztorno(ExecParam ExecParam, String SztornozasIndoka)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            EREC_KuldKuldemenyekService service = new EREC_KuldKuldemenyekService(this.dataContext);
            Result _res = service.Get(ExecParam);
            if (!String.IsNullOrEmpty(_res.ErrorCode))
            {
                //log.WsEnd(ExecParam, _res);
                //return _res;

                throw new ResultException(_res);
            }

            EREC_KuldKuldemenyek Record = (EREC_KuldKuldemenyek)_res.Record;

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(Record);

            if (Record.FelhasznaloCsoport_Id_Orzo != ExecParam.Felhasznalo_Id)
            {
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52457);
                //log.WsEnd(ExecParam, result1);
                //return result1;       

                throw new ResultException(52457);
            }

            ErrorDetails errorDetail = null;
            if (!Contentum.eRecord.BaseUtility.Kuldemenyek.Sztornozhato(ExecParam, statusz, out errorDetail))
            {
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52452);
                //log.WsEnd(ExecParam, result1);
                //return result1;

                throw new ResultException(52452, errorDetail);
            }

            #region SzamlaRontas, ha kell
            if (Record.Tipus == KodTarak.KULDEMENY_TIPUS.Szamla)
            {
                EREC_SzamlakService service_szamla = new EREC_SzamlakService(this.dataContext);
                Result result_szamla = service_szamla.GetByKuldemenyId(ExecParam);
                result_szamla.CheckError();

                EREC_Szamlak erec_Szamlak = (EREC_Szamlak)result_szamla.Record;

                if (erec_Szamlak != null)
                {
                    ExecParam execParam_szamlasztorno = ExecParam.Clone();
                    execParam_szamlasztorno.Record_Id = erec_Szamlak.Id;

                    Result result_szamlasztorno = service_szamla.SzamlaSztorno(execParam_szamlasztorno, Record.BarCode, SztornozasIndoka);
                    result_szamlasztorno.CheckError();
                }
            }
            #endregion SzamlaRontas, ha kell

            EREC_KuldKuldemenyek UpdateRecord = new EREC_KuldKuldemenyek();
            UpdateRecord.Updated.SetValueAll(false);
            UpdateRecord.Base.Updated.SetValueAll(false);

            UpdateRecord.Base.Ver = Record.Base.Ver;
            UpdateRecord.Base.Updated.Ver = true;

            UpdateRecord.SztornirozasDat = DateTime.Now.ToString();
            UpdateRecord.Updated.SztornirozasDat = true;

            UpdateRecord.Allapot = Contentum.eUtility.KodTarak.KULDEMENY_ALLAPOT.Sztorno;
            UpdateRecord.Updated.Allapot = true;

            result = sp.Insert(Constants.Update, ExecParam, UpdateRecord);
            result.CheckError();

            // Az esetleges kézbesítési tételek törlése:
            EREC_IraKezbesitesiTetelekService service_kezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbTetelInvalidate = ExecParam.Clone();

            Result result_kezbTetelInvalidate =
                service_kezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_kezbTetelInvalidate, Record.Id);
            result_kezbTetelInvalidate.CheckError();

            #region Vonalkód felszabadítása

            if (!string.IsNullOrEmpty(Record.BarCode))
            {
                KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                Result barkodResult = barkodService.FreeBarcode(ExecParam.Clone(), Record.BarCode);
                if (!string.IsNullOrEmpty(barkodResult.ErrorCode))
                    throw new ResultException(barkodResult);
            }

            #endregion


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_KuldKuldemenyek", "KuldemenySztorno").Record;

                if (!String.IsNullOrEmpty(SztornozasIndoka))
                {
                    eventLogRecord.Base.Note = SztornozasIndoka;
                }

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }



    /// <summary>
    /// Kimenõ küldemény sztornózása
    /// </summary>    
    [WebMethod(false)]
    public Result KimenoKuldemenySztorno(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.KimenoKuldemenySztorno(execParam, true, null);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_KuldKuldemenyek", "KimenoKuldemenySztorno").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemény sztornózása - példányok visszaállítása nélkül
    /// (akkor hívjuk, ha a kimenõ küldemény a példányok sztornózása miatt kiürül,
    /// ekkor eleve csak sztornózott példányok vannak benne, nem kell visszaállítás)
    /// </summary>    
    public Result KimenoKuldemenySztornoWithoutPeldanyCheck(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.KimenoKuldemenySztorno(execParam, false, "PLD_SZTORNO");

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_KuldKuldemenyek", "KimenoKuldemenySztorno").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemény sztornózása - példányainak visszaállításával együtt vagy sem
    /// (ha úgyis a példányok sztornó felõl hívtuk, felesleges még egyszer vizsgálni a példányokat)
    /// , ill. megjegyzés is megadható, ami a felhasználói megjegyzés elé kerül szögletes zárójelek ([...]) közé
    /// - pl. [PLD_SZTORNO]
    /// </summary>    
    private Result KimenoKuldemenySztorno(ExecParam execParam, bool doPeldanyokCheck, string megjegyzes)
    {
        Logger.Info(String.Format("KimenoKuldemenySztorno START: Példány vizsgálat={0}; Megjegyzés={1}", doPeldanyokCheck ? "IGEN" : "NEM", megjegyzes ?? "<null>"));

        Result result = new Result();
        #region KodTarak Get
        KRT_KodTarakService svc_kodtar = eAdminService.ServiceFactory.GetKRT_KodTarakService();
        Result res_kodtarak = svc_kodtar.GetByKodcsoportKod(execParam.Clone(), "IRATPELDANY_ALLAPOT", KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben);
        if (res_kodtarak.IsError || res_kodtarak.Record == null)
        {
            throw new ResultException("Nem sikerült a KodTarak kodcsoportjának beazonosítása!");
        }
        KRT_KodTarak kimeno_kuld = res_kodtarak.Record as KRT_KodTarak;
        string kimeno_kuldemenyben = kimeno_kuld.Nev;
        #endregion

        #region Küldemény GET

        Result result_Get = this.Get(execParam);
        if (result_Get.IsError)
        {
            // hiba:
            throw new ResultException(result_Get);
        }

        EREC_KuldKuldemenyek kimenoKuldemeny = (EREC_KuldKuldemenyek)result_Get.Record;

        #endregion

        Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(kimenoKuldemeny);

        ErrorDetails errorDetail = null;
        if (!Contentum.eRecord.BaseUtility.Kuldemenyek.KimenoKuldemenySztornozhato(statusz, out errorDetail))
        {
            // hiba: A kimenõ küldemény nem sztornózható!
            throw new ResultException(54201, errorDetail);
        }

        #region Küldemény UPDATE

        kimenoKuldemeny.Updated.SetValueAll(false);
        kimenoKuldemeny.Base.Updated.SetValueAll(false);
        kimenoKuldemeny.Base.Updated.Ver = true;

        // Módosítani kell: sztornózás dátuma, állapot

        kimenoKuldemeny.SztornirozasDat = DateTime.Now.ToString();
        kimenoKuldemeny.Updated.SztornirozasDat = true;

        kimenoKuldemeny.Allapot = Contentum.eUtility.KodTarak.KULDEMENY_ALLAPOT.Sztorno;
        kimenoKuldemeny.Updated.Allapot = true;

        kimenoKuldemeny.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
        kimenoKuldemeny.Updated.TovabbitasAlattAllapot = true;

        if (!String.IsNullOrEmpty(megjegyzes))
        {
            kimenoKuldemeny.Base.Note = String.Format("[{0}]{1}", megjegyzes, kimenoKuldemeny.Base.Note);
            kimenoKuldemeny.Base.Updated.Note = true;
        }

        ExecParam execParam_update = execParam.Clone();
        execParam_update.Record_Id = kimenoKuldemeny.Id;

        result = this.Update(execParam_update, kimenoKuldemeny);
        if (result.IsError)
        {
            // hiba:
            throw new ResultException(result);
        }
        #endregion


        if (doPeldanyokCheck)
        {
            #region Kimenõ küldeményhez tartozó iratpéldányok módosítása:

            #region Iratpéldányok lekérése:
            EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch(true);
            ExecParam execParam_PldGetAll = execParam.Clone();

            Result result_PldGetAll = service_Pld.GetAllWithExtensionByKimenoKuldemeny(execParam_PldGetAll, search, kimenoKuldemeny.Id);
            if (!String.IsNullOrEmpty(result_PldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_PldGetAll);
            }
            #endregion

            if (result_PldGetAll.GetCount > 0)
            {
                ExecParam execParam_PldUpdate = execParam.Clone();
                foreach (System.Data.DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
                {
                    string pld_Id = row["Id"].ToString();
                    string pld_Ver = row["Ver"].ToString();
                    string pld_Allapot = row["Allapot"].ToString();
                    string pld_TovabbitasAlattAllapot = row["TovabbitasAlattAllapot"].ToString();

                    // TODO: Kell valamilyen állapotellenõrzés?
                    //// Állapot ellenõrzés: (csak postázott vagy kimneõ küldeményben állapotú lehet)
                    //if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Postazott
                    //    && pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben
                    //    && pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                    //{
                    //    // Nem sztornózható
                    //    Logger.Error("Az iratpéldány nem postázott: " + pld_Id, execParam);
                    //    throw new ResultException("Hiba a kimenõ küldemény sztornózásakor: az iratpéldány nem postázott vagy kimenõ küldeményben állapotú!"); //(52533);
                    //}

                    #region Pld update

                    Logger.Info("Kimenõ küldemény sztornó - Iratpéldány update", execParam);

                    if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                    {

                        EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
                        iratPeldany.Updated.SetValueAll(false);
                        iratPeldany.Base.Updated.SetValueAll(false);

                        iratPeldany.Base.Ver = pld_Ver;
                        iratPeldany.Base.Updated.Ver = true;

                        // Állapot visszaállítása az elõzõre, ha lehetséges:
                        if (!String.IsNullOrEmpty(pld_TovabbitasAlattAllapot))
                        {
                            iratPeldany.Allapot = pld_TovabbitasAlattAllapot;
                            iratPeldany.Updated.Allapot = true;

                            iratPeldany.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; //"";
                            iratPeldany.Updated.TovabbitasAlattAllapot = true;
                        }
                        else
                        {
                            #region Historyból lekérdezni, hogy milyen állapóta volt.
                            string OldState = string.Empty;
                            int maxVer = -1;

                            ExecParam ep = execParam_PldGetAll.Clone();
                            ep.Record_Id = pld_Id;

                            var svc_history = eAdminService.ServiceFactory.GetRecordHistoryService();

                            var result_hist = svc_history.GetAllByRecord(ep, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok);

                            if (result_hist.IsError && result_hist.Ds != null || result_hist.Ds.Tables[0].Rows.Count == 0)
                            {
                                OldState = KodTarak.IRAT_ALLAPOT.Iktatott;
                            }
                            else
                            {
                                foreach (DataRow dr in result_hist.Ds.Tables[0].Rows)
                                {
                                    if (dr["ColumnName"].ToString() == "Allapot" && dr["NewValue"].ToString() == kimeno_kuldemenyben)
                                    {
                                        if (int.Parse(dr["Ver"].ToString()) > maxVer)

                                            OldState = dr["OldValue"].ToString();
                                        maxVer = int.Parse(dr["Ver"].ToString());
                                    }
                                }
                            }
                            #endregion
                            if (!string.IsNullOrEmpty(OldState))
                            {

                                KRT_KodTarakSearch src_kodtarak = new KRT_KodTarakSearch();
                                src_kodtarak.Nev.Filter(OldState);
                                src_kodtarak.WhereByManual = string.Format(" AND KRT_KODCSOPORTOK.KOD = 'IRATPELDANY_ALLAPOT' ");
                                Result res = svc_kodtar.GetAllWithKodcsoport(ep.Clone(), src_kodtarak);

                                if (res.IsError || res.GetCount == 0)
                                {
                                    throw new ResultException("A kód visszaolvasása nem sikerült.");
                                }
                                string kod = res.Ds.Tables[0].Rows[0]["Kod"].ToString();

                                iratPeldany.Allapot = (kod != null) ? kod : KodTarak.IRATPELDANY_ALLAPOT.Iktatott;

                            }
                            else
                            {
                                iratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Iktatott;
                            }
                            iratPeldany.Updated.Allapot = true;
                        }

                        // Postázás dátumának törlése:
                        iratPeldany.PostazasDatuma = Contentum.eUtility.Constants.BusinessDocument.nullString;
                        iratPeldany.Updated.PostazasDatuma = true;

                        execParam_PldUpdate.Record_Id = pld_Id;

                        Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, iratPeldany);
                        result_PldUpdate.CheckError();
                    }
                    else
                    {
                        // Sztornózott példány
                        Logger.Error("Az iratpéldány sztornózott: " + pld_Id, execParam);
                    }

                    #endregion

                }
            }
            #endregion Kimenõ küldeményhez tartozó iratpéldányok módosítása
        }

        #region Tértivevény érvénytelenítés, ha szükséges

        bool isTertivevenyes = IsKimenoKuldemenyTertivevenyes(kimenoKuldemeny, false);

        // ha a küldés módja tértivevényes:
        //if (kimenoKuldemeny.Tertiveveny == "1")
        if (isTertivevenyes)
        {
            Logger.Info("Tértivevény lekérése Start");

            #region Tértivevények létezésének ellenõrzése
            EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);

            ExecParam execParam_tertiveveny = execParam.Clone();

            EREC_KuldTertivevenyekSearch search_tertiveveny = new EREC_KuldTertivevenyekSearch();
            search_tertiveveny.Kuldemeny_Id.Filter(kimenoKuldemeny.Id);

            Result result_tertivevenyGetAll = service_tertiveveny.GetAll(execParam_tertiveveny, search_tertiveveny);
            result_tertivevenyGetAll.CheckError();
            #endregion Tértivevények létezésének ellenõrzése

            foreach (DataRow row in result_tertivevenyGetAll.Ds.Tables[0].Rows)
            {
                EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                Utility.LoadBusinessDocumentFromDataRow(tertiveveny, row);

                execParam_tertiveveny.Record_Id = row["Id"].ToString();

                Result result_tertivevenyInvalidate = service_tertiveveny.Invalidate(execParam_tertiveveny);
                result_tertivevenyInvalidate.CheckError();

                // Tértivevény típusú melléklet érvénytelenítése
                Result result_mellekletekInvalidate = this.RemoveTertivevenyMellekletFromKimenoKuldemeny(execParam.Clone(), tertiveveny);
                result_mellekletekInvalidate.CheckError();
            }
        }

        #endregion Tértivevény érvénytelenítés, ha szükséges

        #region Vonalkód felszabadítása

        if (!string.IsNullOrEmpty(kimenoKuldemeny.BarCode))
        {
            KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
            Result barkodResult = barkodService.FreeBarcode(execParam.Clone(), kimenoKuldemeny.BarCode);
            if (barkodResult.IsError)
            {
                // hiba
                throw new ResultException(barkodResult);
            }
        }

        #endregion

        Logger.Info("KimenoKuldemenySztorno END");
        return result;
    }

    /// <summary>
    /// Küldemény lezárása
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza, Record_Id tartalmazza a lezárandó küldemény azonosítóját.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>
    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result Lezaras(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_KuldKuldemenyekService service = new EREC_KuldKuldemenyekService(this.dataContext);
            Result _res = service.Get(ExecParam);
            _res.CheckError();

            EREC_KuldKuldemenyek Record = (EREC_KuldKuldemenyek)_res.Record;

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(Record);
            ErrorDetails errorDetail = null;

            if (!Contentum.eRecord.BaseUtility.Kuldemenyek.Lezarhato(ExecParam, statusz, out errorDetail))
            {
                // nem zárható le:
                throw new ResultException(52453, errorDetail);
            }

            EREC_KuldKuldemenyek UpdateRecord = new EREC_KuldKuldemenyek();
            UpdateRecord.Updated.SetValueAll(false);
            UpdateRecord.Base.Updated.SetValueAll(false);

            UpdateRecord.Base.Ver = Record.Base.Ver;
            UpdateRecord.Base.Updated.Ver = true;

            UpdateRecord.Allapot = Contentum.eUtility.KodTarak.KULDEMENY_ALLAPOT.Lezarva;
            UpdateRecord.Updated.Allapot = true;

            result = sp.Insert(Constants.Update, ExecParam, UpdateRecord);
            result.CheckError();

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_KuldKuldemenyek", "KuldemenyLezaras").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result LezarasVisszavonasa(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Küldemény GET:
            Result result_Get = this.Get(execParam);
            result_Get.CheckError();

            EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)result_Get.Record;

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(kuldemenyObj);
            ErrorDetails errorDetail = null;

            if (!Contentum.eRecord.BaseUtility.Kuldemenyek.LezarasVisszavonhato(execParam, statusz, out errorDetail))
            {
                // A lezárás nem vonható vissza
                throw new ResultException(52781, errorDetail);
            }

            #region Küldemény UPDATE

            kuldemenyObj.Updated.SetValueAll(false);
            kuldemenyObj.Base.Updated.SetValueAll(false);
            kuldemenyObj.Base.Updated.Ver = true;

            // Állapot vissza érkeztetettre:
            kuldemenyObj.Allapot = Contentum.eUtility.KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
            kuldemenyObj.Updated.Allapot = true;

            ExecParam execParam_update = execParam.Clone();
            execParam_update.Record_Id = kuldemenyObj.Id;

            result = this.Update(execParam_update, kuldemenyObj);
            result.CheckError();
            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, kuldemenyObj.Id, "EREC_KuldKuldemenyek", "KuldemenyLezarasVisszavonas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Küldemény bontása
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza, Record_Id tartalmazza a bontandó küldemény azonosítóját.</param>
    /// <param name="NewRecord">A bontás során keletkezett új küldemény adatait tartalmazza. KuldKuldemeny_Id_Szulo = a bontott küldémény azonosítója; Csoport_Id_Felelos = az új küldemény felelõse</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.
    /// Result.Uid tartalmazza a létrehozott irat Id-ját</returns>    
    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result Bontas(ExecParam ExecParam, EREC_KuldKuldemenyek NewRecord)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result _res = new Result();

            if (String.IsNullOrEmpty(NewRecord.KuldKuldemeny_Id_Szulo) || String.IsNullOrEmpty(NewRecord.Csoport_Id_Felelos))
            {
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52454);
                //log.WsEnd(ExecParam, result1);
                //return result1;

                throw new ResultException(52454);
            }

            EREC_KuldKuldemenyekService service = new EREC_KuldKuldemenyekService(this.dataContext);
            _res = service.Get(ExecParam);

            if (!String.IsNullOrEmpty(_res.ErrorCode))
            {
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52455);
                //log.WsEnd(ExecParam, result1);
                //return result1;

                throw new ResultException(52455);
            }

            EREC_KuldKuldemenyek Record = (EREC_KuldKuldemenyek)_res.Record;

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(Record);

            if (!Contentum.eRecord.BaseUtility.Kuldemenyek.Bonthato(ExecParam, statusz))
            {
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52456);
                //log.WsEnd(ExecParam, result1);
                //return result1;

                throw new ResultException(52456);
            }

            // összes mezõ update-elhetõségét kezdetben letiltani:
            NewRecord.Updated.SetValueAll(false);
            NewRecord.Base.Updated.SetValueAll(false);

            // Az erteket mar megkatuk parameterkent:
            NewRecord.Updated.KuldKuldemeny_Id_Szulo = true;

            NewRecord.IraIktatokonyv_Id = Record.IraIktatokonyv_Id;
            NewRecord.Updated.IraIktatokonyv_Id = true;

            NewRecord.FelhasznaloCsoport_Id_Orzo = Record.FelhasznaloCsoport_Id_Orzo;
            NewRecord.Updated.FelhasznaloCsoport_Id_Orzo = true;

            NewRecord.Allapot = Record.Allapot;
            NewRecord.Updated.Allapot = true;

            NewRecord.IktatniKell = Record.IktatniKell;
            NewRecord.Updated.IktatniKell = true;

            NewRecord.Partner_Id_Bekuldo = Record.Partner_Id_Bekuldo;
            NewRecord.Updated.Partner_Id_Bekuldo = true;

            NewRecord.Cim_Id = Record.Cim_Id;
            NewRecord.Updated.Cim_Id = true;

            NewRecord.KuldesMod = Record.KuldesMod;
            NewRecord.Updated.KuldesMod = true;

            NewRecord.BeerkezesIdeje = Record.BeerkezesIdeje;
            NewRecord.Updated.BeerkezesIdeje = true;

            NewRecord.Tovabbito = Record.Tovabbito;
            NewRecord.Updated.Tovabbito = true;

            NewRecord.FelhasznaloCsoport_Id_Bonto = Record.FelhasznaloCsoport_Id_Bonto;
            NewRecord.Updated.FelhasznaloCsoport_Id_Bonto = true;

            // TODO: nincsen felbontasi ido!
            //FelbontasiIdoCalendarControl2.Text = erec_KuldKuldemenyek.

            NewRecord.BelyegzoDatuma = Record.BelyegzoDatuma;
            NewRecord.Updated.BelyegzoDatuma = true;

            NewRecord.HivatkozasiSzam = Record.HivatkozasiSzam;
            NewRecord.Updated.HivatkozasiSzam = true;

            NewRecord.UgyintezesModja = Record.UgyintezesModja;
            NewRecord.Updated.UgyintezesModja = true;

            NewRecord.RagSzam = Record.RagSzam;
            NewRecord.Updated.RagSzam = true;

            NewRecord.Csoport_Id_Cimzett = Record.Csoport_Id_Cimzett;
            NewRecord.Updated.Csoport_Id_Cimzett = true;

            NewRecord.Surgosseg = Record.Surgosseg;
            NewRecord.Updated.Surgosseg = true;

            // felelos-t mar a bejovo parameterkent kapjuk nem masoljuk a regi recordrol!
            //NewRecord.Csoport_Id_Felelos = FelelosCsoportTextBox1.Id_HiddenField;
            NewRecord.Updated.Csoport_Id_Felelos = true;

            NewRecord.Targy = Record.Targy;
            NewRecord.Updated.Targy = true;

            // TODO: nem tudjuk miert kellenek, de kotelezoek
            NewRecord.PeldanySzam = Record.PeldanySzam;
            NewRecord.Updated.PeldanySzam = true;
            // TODOD: a postazas iranya bejovo kuldmeny v. kimeno
            NewRecord.PostazasIranya = Record.PostazasIranya;
            NewRecord.Updated.PostazasIranya = true;

            NewRecord.Erkeztetes_Ev = Record.Erkeztetes_Ev;
            NewRecord.Updated.Erkeztetes_Ev = true;

            #region LZS - BUG_481
            NewRecord.NevSTR_Bekuldo = Record.NevSTR_Bekuldo;
            NewRecord.Updated.NevSTR_Bekuldo = true;

            NewRecord.CimSTR_Bekuldo = Record.CimSTR_Bekuldo;
            NewRecord.Updated.CimSTR_Bekuldo = true;

            NewRecord.BoritoTipus = Record.BoritoTipus;
            NewRecord.Updated.BoritoTipus = true;

            NewRecord.AdathordozoTipusa = Record.AdathordozoTipusa;
            NewRecord.Updated.AdathordozoTipusa = true;

            NewRecord.FelbontasDatuma = Record.FelbontasDatuma;
            NewRecord.Updated.FelbontasDatuma = true;

            NewRecord.ElsodlegesAdathordozoTipusa = Record.ElsodlegesAdathordozoTipusa;
            NewRecord.Updated.ElsodlegesAdathordozoTipusa = true;

            NewRecord.KezbesitesModja = Record.KezbesitesModja;
            NewRecord.Updated.KezbesitesModja = true;

            NewRecord.CimzesTipusa = Record.CimzesTipusa;
            NewRecord.Updated.CimzesTipusa = true;
            #endregion

            //EREC_KuldKuldemenyek UpdateRecord = new EREC_KuldKuldemenyek();
            //UpdateRecord.Updated.SetValueAll(false);
            //UpdateRecord.Base.Updated.SetValueAll(false);

            //UpdateRecord.Base.Ver = Record.Base.Ver;
            //UpdateRecord.Base.Updated.Ver = true;

            //UpdateRecord.Allapot = Contentum.eUtility.KodTarak.KULDEMENY_ALLAPOT.Lezarva;
            //UpdateRecord.Updated.Allapot = true;

            result = Erkeztetes(ExecParam, NewRecord, null, null);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, NewRecord.Id, "EREC_KuldKuldemenyek", "KuldemenyBontas").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// A megadott csatolmány feltöltése a küldeményhez
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <param name="csatolmany"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result CsatolmanyUpload(ExecParam ExecParam, EREC_Csatolmanyok Record, Csatolmany csatolmany)
    {
        return CsatolmanyUpload(ExecParam, Record, csatolmany, false);
    }

    public Result CsatolmanyUpload(ExecParam ExecParam, EREC_Csatolmanyok Record, Csatolmany csatolmany, bool ujKuldemeny)
    {
        Logger.Debug(String.Format("Erec_kuldkuldemenyekService.CsatolmanyUpload indul. Record.Id: {0} - csatolmany.Nev: {1}", Record.Id, csatolmany.Nev));

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

            // Upload webservicenek átadandó adatok aláírás esetén xml-ben:
            string uploadXmlStrParams_alairasResz = "";
            if (csatolmany.AlairasAdatok.CsatolmanyAlairando)
            {
                uploadXmlStrParams_alairasResz = csatolmany.AlairasAdatok.GetInnerXml();
            }

            // Vagy a fájl elérési útvonalát adjuk át, vagy magát a fájlt
            if (!String.IsNullOrEmpty(csatolmany.SourceSharePath))
            {
                #region Ha SourceSharePath meg van adva...

                string kivalasztottKuldemenyId = Record.KuldKuldemeny_Id;

                string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv></iktatokonyv>" +
                                                        "<sourceSharePath>{5}</sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<kivalasztottKuldemenyId>{7}</kivalasztottKuldemenyId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{6}</tartalomSHA1>" +
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , ExecParam.Record_Id
                                                    , String.Empty
                                                    , "NEM"
                                                    , ujKuldemeny ? "IGEN" : "NEM"
                                                    , "NEM"
                                                    , csatolmany.SourceSharePath
                                                    , csatolmany.TartalomHash
                                                    , ujKuldemeny ? String.Empty : kivalasztottKuldemenyId
                                                    );

                result = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , null
                       , null
                       , uploadXmlStrParams
                    );
                #endregion
            }
            else
            {
                // KRT_Dokumentumok objektum létrehozása
                KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();

                #region KRT_Dokumentumok mezõinek állítása

                if (csatolmany.Megnyithato != null)
                {
                    krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
                }
                if (csatolmany.Olvashato != null)
                {
                    krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
                }
                if (csatolmany.ElektronikusAlairas != null)
                {
                    krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
                }
                if (!String.IsNullOrEmpty(csatolmany.Titkositas))
                {
                    krt_Dokumentumok.Titkositas = csatolmany.Titkositas;
                }
                if (csatolmany.OCRAdatok.OCRAllapot == "1")
                {
                    krt_Dokumentumok.OCRAllapot = csatolmany.OCRAdatok.OCRAllapot;
                    krt_Dokumentumok.OCRPrioritas = csatolmany.OCRAdatok.OCRPrioritas;
                }


                #endregion

                string kivalasztottKuldemenyId = Record.KuldKuldemeny_Id;

                string uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv></iktatokonyv>" +
                                                        "<sourceSharePath></sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<kivalasztottKuldemenyId>{6}</kivalasztottKuldemenyId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{5}</tartalomSHA1>" +
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , ExecParam.Record_Id
                                                    , String.Empty
                                                    , "NEM"
                                                    , ujKuldemeny ? "IGEN" : "NEM"
                                                    , "NEM"
                                                    , csatolmany.TartalomHash
                                                    , ujKuldemeny ? String.Empty : kivalasztottKuldemenyId
                                                    );

                Logger.Debug(String.Format("Erec_kuldkuldemenyekService.CsatolmanyUpload documentService.UploadFromeRecordWithCTTCheckin elott. - uploadXmlStrParams: {0}", uploadXmlStrParams));

                result = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , csatolmany.Tartalom
                       , krt_Dokumentumok
                       , uploadXmlStrParams
                    );

                Logger.Debug(String.Format("Erec_kuldkuldemenyekService.CsatolmanyUpload documentService.UploadFromeRecordWithCTTCheckin vege."));

            }

            if (result.IsError)
            {
                // hiba:
                log.WsEnd(ExecParam, result);
                throw new ResultException(result);
            }

            #region Eseménynaplózás

            Logger.Debug("Eseménynaplózás 1 indul.");

            try
            {
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_Dokumentumok", "DokumentumokNew").Record;

                    //
                    //  nem egeszen vilagos ezt miert is nem adja at nehany helyen....
                    //  az emailnal pl ha ezt hozzaadtuk, akkor mukodott rendesen
                    //  itt is hozzaadjuk a biztosag kedveert

                    if (String.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
                    {
                        Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo, ezert nem lesz bejegyezve a DokumentumokNew esemeny!");
                    }
                    else
                    {
                        eventLogRecord.Felhasznalo_Id_Login = (ExecParam.Helyettesites_Id != null) ? ExecParam.LoginUser_Id : ExecParam.Felhasznalo_Id;
                    }

                    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);

                    if (!String.IsNullOrEmpty(eventLogResult.ErrorCode))
                    {
                        Logger.Error(String.Format("Hibaval tert vissza az esemeny insert! - {0}\n{1}", eventLogResult.ErrorCode, eventLogResult.ErrorMessage));
                    }
                }

                Logger.Debug("Eseménynaplózás 1 vége.");

            }
            catch (Exception ex1)
            {
                Logger.Error(String.Format("Hiba az eseménynaplózásban: {0}\nStack: {1}\nSource: {2}", ex1.Message, ex1.StackTrace, ex1.Source));
            }


            #endregion


            #region Eredmény feldolgozása

            // result.Record -ban jön vissza néhány dolog, pl. hogy kell-e új csatolmány bejegyzés, vagy csak update kell
            // (ha volt már egy ugyanilyen nevû csatolmány, akkor nem jött létre új KRT_Dokumentumok bejegyzés, így nem kell
            // új EREC_Csatolmanyok bejegyzés sem)

            /// A visszaadott xml string: 
            /// <result>
            /// <dokumentumFullUrl>XXXXXXXXXXXX</dokumentumFullUrl>
            /// <tortentUjKrtDokBejegyzes>XXXXXXXXXXXXX</tortentUjKrtDokBejegyzes>
            /// <csatolmanyId>XXXXXXXXXXXX</csatolmanyId>
            /// <csatolmanyVerzio>XXXXXXXXXXXX</csatolmanyVerzio>
            /// </result>

            Logger.Debug("Erec_kuldkuldemenyekService.CsatolmanyUpload feltoltes ok.");

            bool csatolmanyUpdateKell = false;
            string csatolmanyIdToUpdate = String.Empty;
            string csatolmanyVerToUpdate = String.Empty;
            string dokumentumId = result.Uid;

            if (result.Record != null)
            {
                string resultXml = result.Record.ToString();
                if (!string.IsNullOrEmpty(resultXml))
                {
                    Logger.Debug(String.Format("feltoltesbol kapott resultXml: {0}", resultXml));

                    XmlDocument resultXmlDoc = new XmlDocument();
                    resultXmlDoc.LoadXml(resultXml);

                    string tortentUjKrtDokBejegyzes = GetParam("tortentUjKrtDokBejegyzes", resultXmlDoc);
                    csatolmanyIdToUpdate = GetParam("csatolmanyId", resultXmlDoc);
                    csatolmanyVerToUpdate = GetParam("csatolmanyVerzio", resultXmlDoc);

                    if (string.IsNullOrEmpty(tortentUjKrtDokBejegyzes)
                         && !string.IsNullOrEmpty(csatolmanyIdToUpdate))
                    {
                        csatolmanyUpdateKell = true;
                    }
                }
            }

            #endregion

            Logger.Debug(String.Format("csatolmanyUpdateKell: {0}", csatolmanyUpdateKell));

            EREC_CsatolmanyokService erec_CsatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);

            if (csatolmanyUpdateKell)
            {
                #region update elotti csatolmany objektum lekeres, ha document_id ures

                if (String.IsNullOrEmpty(Record.Dokumentum_Id))
                {
                    Logger.Debug("update elotti csatolmany objektum lekeres, ha document_id ures");

                    ExecParam exPar = ExecParam.Clone();
                    exPar.Record_Id = csatolmanyIdToUpdate;
                    Result resultCsatSelect = erec_CsatolmanyokService.Get(exPar);

                    if (!String.IsNullOrEmpty(resultCsatSelect.ErrorCode))
                    {
                        Logger.Error(String.Format("csatolmany lekeres soran hiba: {0}\nErrorMessage: {1}\nErrorDetail: {2}", resultCsatSelect.ErrorCode, resultCsatSelect.ErrorMessage, resultCsatSelect.ErrorDetail));
                        throw new ResultException(resultCsatSelect);
                    }

                    if (resultCsatSelect.Record == null)
                    {
                        Logger.Error(String.Format("csatolmany lekeres soran hiba: null a csatolmany select resultjaban visszakapott Record mezo!"));
                        resultCsatSelect = new Result();
                        resultCsatSelect.ErrorCode = "CSAUPCSASZE001";
                        resultCsatSelect.ErrorMessage = "Hiba a feltöltött csatolmány bejegyzés visszaellenõrzésekor! Az eredményobjektumban visszakapott Record mezõ értéke null.";
                        throw new ResultException(resultCsatSelect);
                    }

                    Record = (EREC_Csatolmanyok)resultCsatSelect.Record;
                }

                #endregion

                #region EREC_Csatolmanyok UPDATE

                Logger.Debug(String.Format("erec csat update"));

                Record.Base.Ver = csatolmanyVerToUpdate;
                Record.Base.Updated.Ver = true;

                ExecParam execParam_update = ExecParam.Clone();
                execParam_update.Record_Id = csatolmanyIdToUpdate;

                result = erec_CsatolmanyokService.Update(execParam_update, Record);
                result.CheckError();
                result.Uid = csatolmanyIdToUpdate;

                #endregion
            }
            else
            {
                Logger.Debug(String.Format("erec csat insert"));

                Record.Dokumentum_Id = result.Uid;
                Record.Updated.Dokumentum_Id = true;

                #region Fõdokumentum meglétének ellenõrzése
                if (String.IsNullOrEmpty(Record.DokumentumSzerep))
                {
                    Logger.Debug(String.Format("Fõdokumentum keresés indítás"));
                    Result result_fodoku_search = null;
                    EREC_CsatolmanyokSearch foduko_search = new EREC_CsatolmanyokSearch();
                    foduko_search.DokumentumSzerep.Filter(KodTarak.DOKUMENTUM_SZEREP.Fodokumentum);
                    foduko_search.TopRow = 1;
                    foduko_search.KuldKuldemeny_Id.Filter(Record.KuldKuldemeny_Id);
                    ExecParam execParam_fodoku_search = ExecParam.Clone();
                    result_fodoku_search = erec_CsatolmanyokService.GetAllWithExtension(execParam_fodoku_search, foduko_search);

                    if (result_fodoku_search.IsError)
                    {
                        Logger.Error(String.Format("Fõdokumentum keresés hibára futott"));
                    }
                    else if (result_fodoku_search.Ds.Tables[0].Rows.Count > 0)
                    {
                        Logger.Debug(String.Format("Van már fõdokumentum"));
                    }
                    else
                    {
                        Logger.Debug(String.Format("Nincs még fõdokumentum, az új fájlt beállítjuk Fõdokumentumnak"));
                        Record.DokumentumSzerep = "01";
                        Record.Updated.DokumentumSzerep = true;
                    }

                }
                #endregion

                result = erec_CsatolmanyokService.Insert(ExecParam, Record);
                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }

                #region Eseménynaplózás
                if (isTransactionBeginHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_Csatolmanyok", "CsatolmanyokNew").Record;

                    Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
                }
                #endregion
            }

            //dokumentumId visszaadása
            result.Record = dokumentumId;

            Logger.Debug("Kuld.CsatolmanyUpload commit elott.");

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Az xmlStrParambol visszaadja a megadott nevu parameter erteket.
    /// </summary>
    /// <param name="pname">A parameter neve.</param>
    /// <param name="xdoc">Az xmlStrParams XmlDockent.</param>
    /// <returns></returns>
    private string GetParam(string pname, XmlDocument xdoc)
    {
        string _retval = String.Empty;

        if (xdoc == null) return _retval;

        string xpath = String.Format("//{0}", pname);

        if (xdoc.SelectNodes(xpath).Count == 1)
        {
            try
            {
                XmlNode xn = xdoc.SelectSingleNode(xpath);
                if (xn != null)
                {
                    _retval = Convert.ToString(xn.InnerText);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("DocumentService.GetParam hiba!");
                Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace));
                Logger.Error(String.Format("panem: {0}\nxmlDoc: {1}", pname, xdoc.OuterXml.ToString()));
            }
        }

        return _retval;
    }



    #region oldAtadasraKijeloles
    ///// <summary>
    ///// Tömeges átadásra kijelölés
    ///// </summary>
    ///// <param name="execParam"></param>
    ///// <param name="erec_KuldKuldemenyek_Id_Array"></param>
    ///// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    ///// <param name="erec_HataridosFeladatok"></param>
    ///// <returns></returns>
    //[WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    //[System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    //public Result AtadasraKijeloles_Tomeges(ExecParam execParam, String[] erec_KuldKuldemenyek_Id_Array
    //    , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


    //        if (erec_KuldKuldemenyek_Id_Array == null || erec_KuldKuldemenyek_Id_Array.Length == 0)
    //        {
    //            // hiba
    //            //Result result1 = ResultError.CreateNewResultWithErrorCode(52380);
    //            //log.WsEnd(execParam, result1);
    //            //return result1;

    //            throw new ResultException(52380);
    //        }
    //        else
    //        {
    //            result = new Result();

    //            // az összes küldemény lekérése
    //            string kuldemenyIds = "'" + erec_KuldKuldemenyek_Id_Array[0] + "'";

    //            for (int i = 1; i < erec_KuldKuldemenyek_Id_Array.Length; i++)
    //            {
    //                kuldemenyIds += ",'" + erec_KuldKuldemenyek_Id_Array[i] + "'";
    //            }

    //            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
    //            search.Id.Value = kuldemenyIds;
    //            search.Id.Operator = Query.Operators.inner;

    //            Result getAllResult = GetAll(execParam.Clone(), search);
    //            if (!string.IsNullOrEmpty(getAllResult.ErrorCode))
    //                throw new ResultException(getAllResult);

    //            System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByDataSet(execParam.Clone(), getAllResult.Ds);
    //            if (statusz.Count == 0)
    //                throw new ResultException(52380);

    //            foreach (System.Data.DataRow r in getAllResult.Ds.Tables[0].Rows)
    //            {
    //                if (!Contentum.eRecord.BaseUtility.Kuldemenyek.AtadasraKijelolheto(execParam.Clone(), (statusz[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz)))
    //                    throw new ResultException(52381);

    //                EREC_KuldKuldemenyek kuldemeny = new EREC_KuldKuldemenyek();
    //                kuldemeny.Updated.SetValueAll(false);
    //                kuldemeny.Base.Updated.SetValueAll(false);

    //                kuldemeny.Base.Ver = r["Ver"].ToString();
    //                kuldemeny.Id = r["Id"].ToString();
    //                kuldemeny.Base.Updated.Ver = true;

    //                // Elõzõ felelõs állítása
    //                kuldemeny.Csoport_Id_Felelos_Elozo = r["Csoport_Id_Felelos"].ToString();
    //                kuldemeny.Updated.Csoport_Id_Felelos_Elozo = true;

    //                // új felelõs:
    //                kuldemeny.Csoport_Id_Felelos = csoport_Id_Felelos_Kovetkezo;
    //                kuldemeny.Updated.Csoport_Id_Felelos = true;

    //                // Továbbítás alatti állapot beállítása:
    //                if (r["Allapot"].ToString() != KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt
    //                    && r["Allapot"].ToString() != KodTarak.KULDEMENY_ALLAPOT.Szignalva)
    //                {
    //                    kuldemeny.TovabbitasAlattAllapot = r["Allapot"].ToString();
    //                    kuldemeny.Updated.TovabbitasAlattAllapot = true;
    //                }

    //                // Állapot:
    //                kuldemeny.Allapot = KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt;
    //                kuldemeny.Updated.Allapot = true;

    //                // Update:
    //                ExecParam updateExecParam = execParam.Clone();
    //                updateExecParam.Record_Id = r["Id"].ToString();
    //                Result updateResult = Update(updateExecParam, kuldemeny);
    //                if (!string.IsNullOrEmpty(updateResult.ErrorCode))
    //                    throw new ResultException(updateResult);

    //                // Kezelési feljegyzés INSERT, ha megadták
    //                if (erec_HataridosFeladatok != null &&
    //                    (!String.IsNullOrEmpty(erec_HataridosFeladatok.KezelesTipus)
    //                     || !String.IsNullOrEmpty(erec_HataridosFeladatok.Leiras))
    //                    )
    //                {
    //                    EREC_HataridosFeladatokService service_kuldKezFelj = new EREC_HataridosFeladatokService(this.dataContext);
    //                    ExecParam execParam_kezFeljInsert = execParam.Clone();

    //                    // Küldeményhez kötés
    //                    erec_HataridosFeladatok.KuldKuldemeny_Id = r["Id"].ToString();
    //                    erec_HataridosFeladatok.Updated.KuldKuldemeny_Id = true;

    //                    Result result_kezFeljInsert = service_kuldKezFelj.Insert(
    //                        execParam_kezFeljInsert, erec_HataridosFeladatok);
    //                    if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
    //                    {
    //                        // hiba:
    //                        //ContextUtil.SetAbort();
    //                        //log.WsEnd(execParam_kezFeljInsert, result_kezFeljInsert);
    //                        //return result_kezFeljInsert;

    //                        throw new ResultException(result_kezFeljInsert);
    //                    }
    //                }
    //            }
    //        }

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}
    #endregion

    /// <summary>
    /// Küldemények tömeges átadásra kijelölése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id_Array">Az átadandó küldemények id-jait tartalmazza</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">Következõ kezelõ</param>
    /// <param name="erec_HataridosFeladatok">Kezelési feljegyzés (opcionális)</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result AtadasraKijeloles_Tomeges(ExecParam execParam, String[] erec_KuldKuldemenyek_Id_Array
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_KuldKuldemenyek_Id_Array == null || erec_KuldKuldemenyek_Id_Array.Length == 0)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52380);
                //log.WsEnd(execParam, result1);
                //return result1;

                throw new ResultException(52380);
            }
            else
            {
                result = new Result();

                // az összes küldemény lekérése
                //string kuldemenyIds = "'" + erec_KuldKuldemenyek_Id_Array[0] + "'";
                string kuldemenyVers = string.Empty;

                //for (int i = 1; i < erec_KuldKuldemenyek_Id_Array.Length; i++)
                //{
                //    kuldemenyIds += ",'" + erec_KuldKuldemenyek_Id_Array[i] + "'";
                //}

                EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
                search.Id.In(erec_KuldKuldemenyek_Id_Array);

                Result getAllResult = GetAll(execParam.Clone(), search);
                getAllResult.CheckError();

                System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByDataSet(execParam.Clone(), getAllResult.Ds);
                if (statusz.Count == 0)
                    throw new ResultException(52380);

                String kuldemenyIds = string.Empty;
                ErrorDetails errorDetail = null;
                foreach (System.Data.DataRow r in getAllResult.Ds.Tables[0].Rows)
                {
                    if (!Contentum.eRecord.BaseUtility.Kuldemenyek.AtadasraKijelolheto(execParam.Clone(), (statusz[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz)
                        , out errorDetail))
                    {
                        throw new ResultException(52381, errorDetail);
                    }

                    kuldemenyIds += ",'" + r["Id"].ToString() + "'";
                    kuldemenyVers += "," + r["Ver"].ToString();
                }

                kuldemenyIds = kuldemenyIds.TrimStart(',');
                kuldemenyVers = kuldemenyVers.TrimStart(',');

                Result kuldemenyUpdateResult = sp.AtadasraKijeloles_Tomeges(execParam,
                                kuldemenyIds,
                                kuldemenyVers,
                                csoport_Id_Felelos_Kovetkezo,
                                null,
                                null
                                );
                kuldemenyUpdateResult.CheckError();

                #region Határidõs Feladat

                if (erec_HataridosFeladatok != null)
                {
                    EREC_HataridosFeladatokService svcHataridosFeladat = new EREC_HataridosFeladatokService(dataContext);
                    svcHataridosFeladat.SimpleInsertTomeges(execParam.Clone(), erec_HataridosFeladatok, Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek, erec_KuldKuldemenyek_Id_Array);
                }

                #endregion

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, kuldemenyIds, "KuldemenyAtadas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_KuldKuldemenyek::AtadasTomeges: ", execParam, eventLogResult);
                #endregion
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    /// <summary>
    /// Küldemények tömeges átadásra kijelölése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id_Array">Az átadandó küldemények id-jait tartalmazza</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">Következõ kezelõ</param>
    /// <param name="erec_HataridosFeladatok">Kezelési feljegyzés (opcionális)</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Atadas_Tomeges(ExecParam execParam, String[] erec_KuldKuldemenyek_Id_Array
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.AtadasraKijeloles_Tomeges(execParam, erec_KuldKuldemenyek_Id_Array,
                csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
            result.CheckError();

            EREC_IraKezbesitesiTetelekService _EREC_IraKezbesitesiTetelekService = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            EREC_IraKezbesitesiTetelekSearch _EREC_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();
            _EREC_IraKezbesitesiTetelekSearch.Obj_Id.In(erec_KuldKuldemenyek_Id_Array);
            _EREC_IraKezbesitesiTetelekSearch.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt);
            result = _EREC_IraKezbesitesiTetelekService.GetAll(execParam, _EREC_IraKezbesitesiTetelekSearch);
            result.CheckError();

            List<string> kezbesitesiTetelIds = new List<string>();
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                kezbesitesiTetelIds.Add(row["Id"].ToString());
            }

            result = _EREC_IraKezbesitesiTetelekService.Atadas_Tomeges(execParam, kezbesitesiTetelIds.ToArray());
            result.CheckError();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }
    /// <summary>
    /// Küldemények tömeges átvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemeny_Id_Array">Az átveendõ küldemények Id-jai</param>
    /// <param name="kovetkezoOrzoId">Következõ õrzõ (az átvevõ felhasználó)</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atvetel_Tomeges(ExecParam execParam, string[] kuldemeny_Id_Array, string kovetkezoOrzoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_IraKezbesitesiTetelekService kezbTetelekService = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            EREC_IraKezbesitesiTetelekSearch kezbTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

            //string ids = string.Empty;
            //foreach (string s in kuldemeny_Id_Array)
            //{
            //    ids += "'" + s + "',";
            //}
            //ids = ids.TrimEnd(',');

            kezbTetelekSearch.Obj_Id.In(kuldemeny_Id_Array);

            kezbTetelekSearch.Allapot.In(new string[] { KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott });
            kezbTetelekSearch.Csoport_Id_Cel.In(new string[] { execParam.Felhasznalo_Id, execParam.FelhasznaloSzervezet_Id });

            Result kezbTetelekGetAllResult = kezbTetelekService.GetAll(execParam.Clone(), kezbTetelekSearch);
            kezbTetelekGetAllResult.CheckError();

            string[] kezbTetelIds = new string[kezbTetelekGetAllResult.GetCount];
            // ha a kézbesítési tételek száma nem egyezik az átveendõ küldemények számával -> hiba (kézbesítési tétel lekérése sikertelen)
            if (kezbTetelIds.Length != kuldemeny_Id_Array.Length)
                throw new ResultException(52224);

            for (int i = 0; i < kezbTetelIds.Length; i++)
            {
                kezbTetelIds[i] = kezbTetelekGetAllResult.Ds.Tables[0].Rows[i]["Id"].ToString();
            }

            result = kezbTetelekService.Atvetel_Tomeges(execParam.Clone(), kezbTetelIds);

            #region Eseménynaplózás
            // naplózás a kezbTetelekService.Atvetel_Tomeges által visszahívott másik, nem WebMethod Atvetel_Tomeges metódusban
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //    Result eventLogResult = eventLogService.InsertTomeges(execParam, ids, "EREC_KuldKuldemenyek", "Atvetel");
            //    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //        Logger.Debug("[ERROR]EREC_KuldKuldemenyek::AtvetelTomeges: ", execParam, eventLogResult);
            //}
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public Result Atvetel_Tomeges(ExecParam ExecParam, string Ids, string Vers, string KovetkezoOrzoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Atvetel_Tomeges(ExecParam, Ids, Vers, KovetkezoOrzoId);

            #region Eseménynaplózás
            //if (isTransactionBeginHere)
            //{
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, Ids, "KuldemenyAtvetel");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_KuldKuldemenyek::AtvetelTomeges: ", ExecParam, eventLogResult);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Küldemény átadásra kijelölése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id">Az átadandó küldemény Id-ja</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">Következõ kezelõ</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result AtadasraKijeloles(ExecParam execParam, String erec_KuldKuldemenyek_Id
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_KuldKuldemenyek_Id)
            || String.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52380);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Küldemény lekérése:
            EREC_KuldKuldemenyekService service_kuldemenyek = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParam_kuldemenyGet = execParam.Clone();
            execParam_kuldemenyGet.Record_Id = erec_KuldKuldemenyek_Id;

            Result result_kuldemenyGet = service_kuldemenyek.Get(execParam_kuldemenyGet);
            if (!String.IsNullOrEmpty(result_kuldemenyGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_kuldemenyGet, result_kuldemenyGet);
                //return result_kuldemenyGet;

                throw new ResultException(result_kuldemenyGet);
            }
            else
            {
                if (result_kuldemenyGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52382);
                    //log.WsEnd(execParam_kuldemenyGet, result1);
                    //return result1;

                    throw new ResultException(52382);
                }

                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

                Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz =
                    Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                ExecParam execParam_atadasraKijelolheto = execParam.Clone();

                // Átadásra kijelölhetõ?
                ErrorDetails errorDetail = null;
                if (Contentum.eRecord.BaseUtility.Kuldemenyek.AtadasraKijelolheto(
                    execParam_atadasraKijelolheto, kuldemenyStatusz, out errorDetail) == false)
                {
                    // Nem jelölhetõ ki átadásra
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52381);
                    //log.WsEnd(execParam_atadasraKijelolheto, result1);
                    //return result1;

                    throw new ResultException(52381, errorDetail);
                }

                // Küldemény UPDATE
                // mezõk módosíthatóságának letiltása:
                erec_KuldKuldemenyek.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.Ver = true;

                // Módosítani kell: Elõzõ felelõs, Felelõs, TovabbitasAlattAllapot, Állapot

                // Elõzõ felelõs állítása
                erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo = erec_KuldKuldemenyek.Csoport_Id_Felelos;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos_Elozo = true;

                // új felelõs:
                erec_KuldKuldemenyek.Csoport_Id_Felelos = csoport_Id_Felelos_Kovetkezo;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

                // Továbbítás alatti állapot beállítása:
                if (erec_KuldKuldemenyek.Allapot != KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt
                    && erec_KuldKuldemenyek.Allapot != KodTarak.KULDEMENY_ALLAPOT.Szignalva)
                {
                    erec_KuldKuldemenyek.TovabbitasAlattAllapot = erec_KuldKuldemenyek.Allapot;
                    erec_KuldKuldemenyek.Updated.TovabbitasAlattAllapot = true;
                }

                // Állapot:
                erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt;
                erec_KuldKuldemenyek.Updated.Allapot = true;

                ExecParam execParam_kuldUpdate = execParam.Clone();
                execParam_kuldUpdate.Record_Id = erec_KuldKuldemenyek.Id;

                Result result_kuldUpdate =
                    service_kuldemenyek.Update(execParam_kuldUpdate, erec_KuldKuldemenyek);
                if (!String.IsNullOrEmpty(result_kuldUpdate.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(execParam_kuldUpdate, result_kuldUpdate);
                    //return result_kuldUpdate;

                    throw new ResultException(result_kuldUpdate);
                }
                else
                {
                    // Kezelési feljegyzés INSERT, ha megadták
                    if (erec_HataridosFeladatok != null)
                    {
                        EREC_HataridosFeladatokService service_kuldKezFelj = new EREC_HataridosFeladatokService(this.dataContext);
                        ExecParam execParam_kezFeljInsert = execParam.Clone();

                        // Küldeményhez kötés
                        erec_HataridosFeladatok.Obj_Id = erec_KuldKuldemenyek.Id;
                        erec_HataridosFeladatok.Updated.Obj_Id = true;

                        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                        erec_HataridosFeladatok.Updated.Obj_type = true;

                        if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.Azonosito))
                        {
                            erec_HataridosFeladatok.Azonosito = erec_KuldKuldemenyek.Azonosito;
                            erec_HataridosFeladatok.Updated.Azonosito = true;
                        }

                        Result result_kezFeljInsert = service_kuldKezFelj.Insert(
                            execParam_kezFeljInsert, erec_HataridosFeladatok);
                        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                        {
                            // hiba:
                            //ContextUtil.SetAbort();
                            //log.WsEnd(execParam_kezFeljInsert, result_kezFeljInsert);
                            //return result_kezFeljInsert;

                            throw new ResultException(result_kezFeljInsert);
                        }
                    }

                    result = result_kuldUpdate;
                }
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek_Id, "EREC_KuldKuldemenyek", "KuldemenyAtadas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Küldemény átvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id">Az átveendõ küldemény</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atvetel(ExecParam execParam, String erec_KuldKuldemenyek_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_KuldKuldemenyek_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52390);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // A tömeges átvétel fv. meghívva:
            string[] kuldemenyIds = new string[1];
            kuldemenyIds[0] = erec_KuldKuldemenyek_Id;

            result = this.Atvetel_Tomeges(execParam, kuldemenyIds, execParam.Felhasznalo_Id);
            result.CheckError();

            #region Eseménynaplózás
            // naplózás a tömeges átvételben
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek_Id, "EREC_KuldKuldemenyek", "KuldemenyAtvetel").Record;

            //    eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public void AddKezelesiFeljegyzes(ExecParam p_ExecParam, string p_KuldemenyID, string p_Leiras, string p_Tipus)
    {
        Logger.Debug("HataridosFeladatokService Insert start", p_ExecParam);
        Logger.Debug("Leiras: " + p_Leiras ?? "NULL");
        Logger.Debug("KuldemenyID: " + p_KuldemenyID ?? "NULL");
        Logger.Debug("Tipus: " + p_Tipus ?? "NULL");

        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
        ExecParam execParam_kezFeljInsert = p_ExecParam.Clone();

        EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
        erec_HataridosFeladatok.Updated.SetValueAll(false);
        erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

        // Küldeményhez kötés
        erec_HataridosFeladatok.Obj_Id = p_KuldemenyID;
        erec_HataridosFeladatok.Updated.Obj_Id = true;

        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
        erec_HataridosFeladatok.Updated.Obj_type = true;

        erec_HataridosFeladatok.Leiras = p_Leiras;
        erec_HataridosFeladatok.Updated.Leiras = true;

        erec_HataridosFeladatok.Altipus = p_Tipus;
        erec_HataridosFeladatok.Updated.Altipus = true;

        erec_HataridosFeladatok.Tipus = KodTarak.FELADAT_TIPUS.Megjegyzes;
        erec_HataridosFeladatok.Updated.Tipus = true;

        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(execParam_kezFeljInsert, erec_HataridosFeladatok);
        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
        {
            // hiba:
            Logger.Error("HataridosFeladatokService Insert hiba", execParam_kezFeljInsert, result_kezFeljInsert);
            throw new ResultException(result_kezFeljInsert);
        }
        Logger.Debug("HataridosFeladatokService Insert end", p_ExecParam);
    }

    #region Visszaküldés
    /// <summary>
    /// Küldemény átvételének visszautasítása, küldemény visszaküldése
    /// A kézbesítési tételt a hívó szervíznek kell módosítania a sikeres futás után!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraKezbesitesiTetelek"></param>
    /// <returns></returns>
    public Result Visszakuldes(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_Id) || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_type))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Kézbesítési tétel ellenõrzés
            Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldheto(execParam, erec_IraKezbesitesiTetelek);
            if (result_kezbesitesiTetelCheck.IsError)
            {
                throw new ResultException(result_kezbesitesiTetelCheck);
            }

            if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek)
            {
                Logger.Error(String.Format("Kézbesítési tételbõl kapott objektum típus: {0}; Várt objektum típus: {1}"
                    , erec_IraKezbesitesiTetelek.Obj_type
                    , Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek));

                // Hiba a visszaküldés során: a kézbesítési tétel objektum típusa nem megfelelõ!
                throw new ResultException(53707);
            }
            #endregion Kézbesítési tétel ellenõrzés

            string kuldemeny_Id = erec_IraKezbesitesiTetelek.Obj_Id;
            string visszakuldesIndoka = erec_IraKezbesitesiTetelek.Base.Note;

            #region Küldemény objektum lekérése
            ExecParam execParam_KuldemenyGet = execParam.Clone();
            execParam_KuldemenyGet.Record_Id = kuldemeny_Id;

            Result result_KuldemenyGet = this.Get(execParam_KuldemenyGet);
            if (result_KuldemenyGet.IsError)
            {
                // hiba
                throw new ResultException(result_KuldemenyGet);
            }

            EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_KuldemenyGet.Record;

            #endregion Küldemény objektum lekérése

            #region Küldemény UPDATE
            erec_KuldKuldemenyek.Updated.SetValueAll(false);
            erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);
            erec_KuldKuldemenyek.Base.Updated.Ver = true;

            // Visszaküldés: a következõ felelõs az elõzõ felelõs (és még jelenlegi õrzõ) lesz
            string felelos_Id = erec_KuldKuldemenyek.Csoport_Id_Felelos;

            if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo))
            {
                // Felelõs visszaállítása az elõzõ felelõsre:
                erec_KuldKuldemenyek.Csoport_Id_Felelos = erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos_Elozo = true;
            }
            else
            {
                erec_KuldKuldemenyek.Csoport_Id_Felelos = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos_Elozo = true;
            }

            ExecParam execParam_kuldemenyUpdate = execParam.Clone();
            execParam_kuldemenyUpdate.Record_Id = kuldemeny_Id;
            // a StoredProcedure szintet hívjuk, hogy ne keletkezzen új kézbesítési tétel
            result = sp.Insert(Constants.Update, execParam_kuldemenyUpdate, erec_KuldKuldemenyek);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #endregion Küldemény UPDATE

            #region Kezelési feljegyzés
            // Kezelési feljegyzés létrehozása, ha van megjegyzés:
            if (!String.IsNullOrEmpty(visszakuldesIndoka.Trim()))
            {
                Logger.Debug("Visszakuldes indoka:" + visszakuldesIndoka.Trim());
                this.AddKezelesiFeljegyzes(execParam, kuldemeny_Id, visszakuldesIndoka.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }
            #endregion Kezelési feljegyzés

            #region Eseménynaplózás
            // naplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, kuldemeny_Id, Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek, "KuldemenyVisszakuldes").Record;
                if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
                {
                    eventLogRecord.Base.Note = visszakuldesIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }
    #endregion Visszaküldés


    /// <summary>
    /// Átadásra kijelölés sztornózása (visszavonása)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result AtadasraKijelolesSztorno(ExecParam execParam, String erec_KuldKuldemenyek_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_KuldKuldemenyek_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52460);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Küldemény lekérése:
            EREC_KuldKuldemenyekService service_kuldemenyek = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParam_kuldemenyGet = execParam.Clone();
            execParam_kuldemenyGet.Record_Id = erec_KuldKuldemenyek_Id;

            Result result_kuldemenyGet = service_kuldemenyek.Get(execParam_kuldemenyGet);
            if (!String.IsNullOrEmpty(result_kuldemenyGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_kuldemenyGet, result_kuldemenyGet);
                //return result_kuldemenyGet;

                throw new ResultException(result_kuldemenyGet);
            }
            else
            {
                if (result_kuldemenyGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52461);
                    //log.WsEnd(execParam_kuldemenyGet, result1);
                    //return result1;

                    throw new ResultException(52461);
                }

                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

                Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz =
                    Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                ExecParam execParam_visszavonhato = execParam.Clone();

                if (Contentum.eRecord.BaseUtility.Kuldemenyek.AtadasraKijelolesVisszavonhato(kuldemenyStatusz, execParam_visszavonhato) == false)
                {
                    // Nem vonható vissza
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52462);
                    //log.WsEnd(execParam_visszavonhato, result1);
                    //return result1;

                    throw new ResultException(52462);
                }


                // KÜLDEMÉNY UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_KuldKuldemenyek.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.Ver = true;

                // Módosítani kell: Felelõs, Állapot (Továbbításalattiállapot)

                string felelos_Id = erec_KuldKuldemenyek.Csoport_Id_Felelos;

                // Felelõs visszaállítása az elõzõ felelõsre:
                erec_KuldKuldemenyek.Csoport_Id_Felelos = erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos_Elozo = true;

                // Állapot állítása: (vissza a továbbítás alatti állapotra, ha az be van állítva)
                if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.TovabbitasAlattAllapot))
                {
                    // BUG_4505
                    // Ha Érkeztetett az elõzõ állapot és iratId ki van töltve, akkor Szignálásból jött-> irat összerendelést meg kell szüntetni                   
                    if (erec_KuldKuldemenyek.TovabbitasAlattAllapot == KodTarak.KULDEMENY_ALLAPOT.Erkeztetve)
                    {
                        if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIratok_Id))
                        {
                            ExecParam execParam_iratGet = execParam.Clone();
                            execParam_iratGet.Record_Id = erec_KuldKuldemenyek.IraIratok_Id;

                            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);


                            Result result_iratGet = service_iratok.Invalidate(execParam_iratGet);
                            if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
                            {
                                // hiba:
                                throw new ResultException(result_iratGet);
                            }
                            erec_KuldKuldemenyek.IraIratok_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
                            erec_KuldKuldemenyek.Updated.IraIratok_Id = true;
                        }
                    }
                    erec_KuldKuldemenyek.Allapot = erec_KuldKuldemenyek.TovabbitasAlattAllapot;
                    erec_KuldKuldemenyek.Updated.Allapot = true;

                    erec_KuldKuldemenyek.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString; //"";
                    erec_KuldKuldemenyek.Updated.TovabbitasAlattAllapot = true;
                }

                ExecParam execParam_kuldUpdate = execParam.Clone();
                execParam_kuldUpdate.Record_Id = erec_KuldKuldemenyek.Id;
                // Közvetlenül sp objektumnak hívjuk az update-jét, hogy ne csináljon megint kézbesítési tételt
                // a felelõs megváltozása miatt:
                Result result_kuldUpdate = sp.Insert(Constants.Update, execParam_kuldUpdate, erec_KuldKuldemenyek);
                if (!String.IsNullOrEmpty(result_kuldUpdate.ErrorCode))
                {
                    // hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(execParam_kuldUpdate, result_kuldUpdate);
                    //return result_kuldUpdate;

                    throw new ResultException(result_kuldUpdate);
                }
                else
                {
                    //ha mozog a küldemény (átadás vagy átvétel)
                    //akkor mozgatjuk vele az iratpéldányokat, és elsõdleges iratpéldányokkal az iratot is
                    if (IsKimenoKuldemenyMoved(erec_KuldKuldemenyek))
                    {
                        ExecParam execParam_moved = execParam.Clone();
                        execParam_moved.Record_Id = erec_KuldKuldemenyek_Id;

                        Result resMoved = this.KimenoKuldemenyek_Moved(execParam_moved);

                        if (resMoved.IsError)
                        {
                            throw new ResultException(resMoved);
                        }
                    }

                    /// Kézbesítési tételek vizsgálata:
                    /// ha volt a rekordra kézbesítési tétel (átadásra kijelölt), kézb. tétel érvénytelenítése:
                    /// 

                    EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                    ExecParam execParam_kezbTetel = execParam.Clone();

                    Result result_kezbTetelUpdate =
                        service_KezbesitesiTetelek.AtadasraKijeloltekInvalidate(execParam_kezbTetel, erec_KuldKuldemenyek.Id);

                    if (!String.IsNullOrEmpty(result_kezbTetelUpdate.ErrorCode))
                    {
                        // hiba:
                        //ContextUtil.SetAbort();
                        //log.WsEnd(execParam_kezbTetel, result_kezbTetelUpdate);
                        //return result_kezbTetelUpdate;

                        throw new ResultException(result_kezbTetelUpdate);
                    }

                    // ha minden OK:

                    result = result_kuldUpdate;
                }
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek_Id, "EREC_KuldKuldemenyek", "KuldemenyAtadasSztorno").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public Result SaveBarcode(ExecParam execParam, EREC_KuldKuldemenyek Record)
    {
        //meg kell õrizni
        Result result = new Result();
        if (Record.MegorzesJelzo == Contentum.eUtility.Constants.Database.Yes || Record.BoritoTipus == Contentum.eUtility.KodTarak.BoritoTipus.Irat)
        {
            if (!String.IsNullOrEmpty(Record.BarCode) && !String.IsNullOrEmpty(Record.BoritoTipus))
            {
                EREC_Mellekletek melleklet = new EREC_Mellekletek();
                melleklet.Updated.SetValueAll(false);
                melleklet.Base.Updated.SetValueAll(true);
                melleklet.Base = Record.Base;

                melleklet.KuldKuldemeny_Id = Record.Id;
                melleklet.Updated.KuldKuldemeny_Id = true;

                melleklet.BarCode = Record.BarCode;
                melleklet.Updated.BarCode = true;

                melleklet.Mennyiseg = "1";
                melleklet.Updated.Mennyiseg = true;

                melleklet.ErvKezd = Record.ErvKezd;
                melleklet.Updated.ErvKezd = true;

                melleklet.ErvVege = Record.ErvVege;
                melleklet.Updated.ErvVege = true;

                bool insertMelleklet = false;
                //boríték
                if (Record.BoritoTipus == Contentum.eUtility.KodTarak.BoritoTipus.Boritek)
                {
                    insertMelleklet = true;
                    melleklet.AdathordozoTipus = Contentum.eUtility.KodTarak.AdathordozoTipus.Boritek;
                    melleklet.Updated.AdathordozoTipus = true;
                    melleklet.MennyisegiEgyseg = Contentum.eUtility.KodTarak.MennyisegiEgyseg.Darab;
                    melleklet.Updated.MennyisegiEgyseg = true;
                }
                //irat
                if (Record.BoritoTipus == Contentum.eUtility.KodTarak.BoritoTipus.Irat)
                {
                    insertMelleklet = true;
                    melleklet.AdathordozoTipus = Contentum.eUtility.KodTarak.AdathordozoTipus.PapirAlapu;
                    melleklet.Updated.AdathordozoTipus = true;
                    melleklet.MennyisegiEgyseg = Contentum.eUtility.KodTarak.MennyisegiEgyseg.Lap;
                    melleklet.Updated.MennyisegiEgyseg = true;
                }

                if (insertMelleklet)
                {
                    EREC_MellekletekService service = new EREC_MellekletekService(this.dataContext);
                    result = service.Insert(execParam, melleklet, false);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        throw new ResultException(result);
                    }
                }
            }
        }

        return result;
    }



    /// <summary>
    /// Küldemény szervezetre szignálása, ezáltal meghatározza a (küldeményhez kapcsolódó, elõkészített állapotban lévõ) irat ügyfelelõsét
    /// (Az ügyfelelõs a kijelölt szervezet vezetõje lesz)
    /// A küldemény a megadott szervezet vezetõjéhez továbbítódik
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id"></param>
    /// <param name="CsoportId_Szervezet"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SzignalasSzervezetre(ExecParam execParam, String erec_KuldKuldemenyek_Id, String CsoportId_Szervezet
            , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_KuldKuldemenyek_Id)
            || String.IsNullOrEmpty(CsoportId_Szervezet))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52580);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Küldemény lekérése:
            EREC_KuldKuldemenyekService service_kuldemenyek = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParam_kuldemenyGet = execParam.Clone();
            execParam_kuldemenyGet.Record_Id = erec_KuldKuldemenyek_Id;

            Result result_kuldemenyGet = service_kuldemenyek.Get(execParam_kuldemenyGet);
            if (!String.IsNullOrEmpty(result_kuldemenyGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kuldemenyGet);
            }
            else
            {
                if (result_kuldemenyGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52581);
                }

                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

                Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz =
                    Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                ExecParam execParam_szignalhato = execParam.Clone();
                ErrorDetails errorDetail = null;

                if (Contentum.eRecord.BaseUtility.Kuldemenyek.Szignalhato(kuldemenyStatusz, execParam_szignalhato, out errorDetail) == false)
                {
                    // Nem szignálható
                    throw new ResultException(52582, errorDetail);
                }

                // Megadott szervezet vezetõjének lekérdezése:
                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
                    Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                ExecParam execParam_getLeader = execParam.Clone();

                Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, CsoportId_Szervezet);
                if (!String.IsNullOrEmpty(result_GetLeader.ErrorCode)
                    || result_GetLeader.Record == null)
                {

                    // hiba: Nincs meg a szervezet vezetõje

                    Logger.Error("Küldemény szignálása: Nem határozható meg a csoport vezetõje: " + CsoportId_Szervezet, execParam);
                    throw new ResultException(52583);
                }

                KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;

                if (String.IsNullOrEmpty(csoportVezeto.Id))
                {
                    // hiba: Nincs meg a szervezet vezetõje

                    Logger.Error("Küldemény szignálása: Nem határozható meg a csoport vezetõje: " + CsoportId_Szervezet, execParam);
                    throw new ResultException(52583);
                }

                // Küldemény UPDATE:
                erec_KuldKuldemenyek.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.Ver = true;


                // Küldeményhez kapcsolódik-e már irat?
                // ha igen, lekérjük; ha nem, hozzáteszünk egyet

                EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

                EREC_IraIratok erec_IraIrat = null;

                if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIratok_Id))
                {
                    #region Irat UPDATE:

                    ExecParam execParam_iratGet = execParam.Clone();
                    execParam_iratGet.Record_Id = erec_KuldKuldemenyek.IraIratok_Id;

                    Result result_iratGet = service_iratok.Get(execParam_iratGet);
                    if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iratGet);
                    }
                    else

                        erec_IraIrat = (EREC_IraIratok)result_iratGet.Record;

                    erec_IraIrat.Updated.SetValueAll(false);
                    erec_IraIrat.Base.Updated.SetValueAll(false);
                    erec_IraIrat.Base.Updated.Ver = true;

                    //// Ügyfelelõs beállítása (Vezetõre):
                    //erec_IraIrat.Csoport_Id_Ugyfelelos = csoportVezeto.Id;
                    // CR#2241 2009.12.17: mivel ügyfelelõs csak szervezetcsoport lehet, a szervezetre állítjuk
                    erec_IraIrat.Csoport_Id_Ugyfelelos = CsoportId_Szervezet;
                    erec_IraIrat.Updated.Csoport_Id_Ugyfelelos = true;

                    // CR#2241 2009.12.17: Szervezet vezetõjének kell beállítani
                    // Felelõs állítása (Kezelõ)
                    erec_IraIrat.Csoport_Id_Felelos = csoportVezeto.Id;
                    erec_IraIrat.Updated.Csoport_Id_Felelos = true;

                    ExecParam execParam_iratUpdate = execParam.Clone();
                    execParam_iratUpdate.Record_Id = erec_IraIrat.Id;

                    Result result_iratUpdate = service_iratok.Update(execParam_iratUpdate, erec_IraIrat);
                    if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iratUpdate);
                    }

                    #endregion
                }
                else
                {
                    #region Irat létrehozása
                    // Ügyfelelõs beállítása (vezetõre)

                    erec_IraIrat = new EREC_IraIratok();

                    //// ügyfelelõs a vezetõ lesz
                    //erec_IraIrat.Csoport_Id_Ugyfelelos = csoportVezeto.Id;
                    // CR#2241 2009.12.17: mivel ügyfelelõs csak szervezetcsoport lehet, a szervezetre állítjuk
                    erec_IraIrat.Csoport_Id_Ugyfelelos = CsoportId_Szervezet;
                    erec_IraIrat.Updated.Csoport_Id_Ugyfelelos = true;

                    // CR#2241 2009.12.17: Szervezet vezetõjének kell beállítani
                    // Felelõs állítása (Kezelõ)
                    erec_IraIrat.Csoport_Id_Felelos = csoportVezeto.Id;
                    erec_IraIrat.Updated.Csoport_Id_Felelos = true;

                    // küldemény hozzákapcsolása:
                    erec_IraIrat.KuldKuldemenyek_Id = erec_KuldKuldemenyek.Id;
                    erec_IraIrat.Updated.KuldKuldemenyek_Id = true;

                    // iktatót kötelezõ megadni, de igazából itt nem kéne
                    erec_IraIrat.FelhasznaloCsoport_Id_Iktato = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
                    erec_IraIrat.Updated.FelhasznaloCsoport_Id_Iktato = true;

                    // Tárgy is kötelezõ, átvesszük a küldeményból, ha van:
                    if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.Targy))
                    {
                        erec_IraIrat.Targy = erec_KuldKuldemenyek.Targy;
                    }
                    else
                    {
                        // csak hogy legyen valami:
                        erec_IraIrat.Targy = ".";
                    }
                    erec_IraIrat.Updated.Targy = true;


                    ExecParam execParam_IratInsert = execParam.Clone();

                    Result result_iratInsert = service_iratok.Insert(execParam_IratInsert, erec_IraIrat);
                    if (!String.IsNullOrEmpty(result_iratInsert.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iratInsert);
                    }

                    string newIratId = result_iratInsert.Uid;

                    erec_KuldKuldemenyek.IraIratok_Id = newIratId;
                    erec_KuldKuldemenyek.Updated.IraIratok_Id = true;

                    #endregion
                }

                // Szervezet vezetõjének kell beállítani
                // Felelõs állítása (Kezelõ)
                erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo = erec_KuldKuldemenyek.Csoport_Id_Felelos;
                erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos_Elozo = true;

                // ha 2-es típusú szignálási folyamat van, a küldeményt az iktatónak kell továbbítani,
                // egyébként pedig a kijelölt szervezeti egység vezetõjéhez
                if (erec_KuldKuldemenyek.Iktathato == "S")
                {
                    erec_KuldKuldemenyek.Csoport_Id_Felelos = erec_IraIrat.FelhasznaloCsoport_Id_Iktato;
                    erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;
                }
                else
                {
                    erec_KuldKuldemenyek.Csoport_Id_Felelos = csoportVezeto.Id;
                    erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;
                }

                // Állapot: Szignált
                // BUG_4505
                // Elõzõ állapot elmentése Továbbítás alatti állapotba
                erec_KuldKuldemenyek.TovabbitasAlattAllapot = erec_KuldKuldemenyek.Allapot;
                erec_KuldKuldemenyek.Updated.TovabbitasAlattAllapot = true;

                erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Szignalva;
                erec_KuldKuldemenyek.Updated.Allapot = true;

                ExecParam execParam_kuldUpdate = execParam.Clone();
                execParam_kuldUpdate.Record_Id = erec_KuldKuldemenyek.Id;

                Result result_kuldUpdate = this.Update(execParam_kuldUpdate, erec_KuldKuldemenyek);
                result_kuldUpdate.CheckError();

                #region Kézbesítési tétel átadottra állítása:

                EREC_IraKezbesitesiTetelekService service_kezbTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);

                string[] kuld_array = new string[1];
                kuld_array[0] = erec_KuldKuldemenyek.Id;

                Result result_kezbTetel = service_kezbTetelek.SetKezbesitesiTetelToAtadott_Tomeges(execParam, kuld_array);
                result_kezbTetel.CheckError();

                #endregion


                #region Kezelési feljegyzés INSERT, ha megadták
                if (erec_HataridosFeladatok != null)
                {
                    Logger.Debug("Kezelési feljegyzés Insert", execParam);

                    EREC_HataridosFeladatokService service_kuldKezFelj = new EREC_HataridosFeladatokService(this.dataContext);
                    ExecParam execParam_kezFeljInsert = execParam.Clone();

                    // Küldeményhez kötés
                    erec_HataridosFeladatok.Obj_Id = erec_KuldKuldemenyek.Id;
                    erec_HataridosFeladatok.Updated.Obj_Id = true;

                    erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                    erec_HataridosFeladatok.Updated.Obj_type = true;

                    if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.Azonosito))
                    {
                        erec_HataridosFeladatok.Azonosito = erec_KuldKuldemenyek.Azonosito;
                        erec_HataridosFeladatok.Updated.Azonosito = true;
                    }

                    Result result_kezFeljInsert = service_kuldKezFelj.Insert(
                        execParam_kezFeljInsert, erec_HataridosFeladatok);
                    if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_kezFeljInsert);
                    }
                }
                #endregion

                result = result_kuldUpdate;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek_Id, "EREC_KuldKuldemenyek", "KuldemenySzignalas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }



    /// <summary>
    /// Küldemény szignálása ügyintézõre
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek_Id">Szignálandó küldemény Id-ja</param>
    /// <param name="CsoportId_Ugyintezo">Ügyintézõ Id-ja</param>
    /// <param name="CsoportId_Iktato">Iktató Id-ja</param>
    /// <returns></returns>
    [WebMethod()]
    public Result SzignalasUgyintezore(ExecParam execParam, String erec_KuldKuldemenyek_Id, String CsoportId_Ugyintezo
            , EREC_HataridosFeladatok erec_HataridosFeladatok, string CsoportId_Iktato)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_KuldKuldemenyek_Id)
            || String.IsNullOrEmpty(CsoportId_Ugyintezo))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52580);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Küldemény lekérése:
            EREC_KuldKuldemenyekService service_kuldemenyek = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam execParam_kuldemenyGet = execParam.Clone();
            execParam_kuldemenyGet.Record_Id = erec_KuldKuldemenyek_Id;

            Result result_kuldemenyGet = service_kuldemenyek.Get(execParam_kuldemenyGet);
            if (!String.IsNullOrEmpty(result_kuldemenyGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kuldemenyGet);
            }
            else
            {
                if (result_kuldemenyGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52581);
                }

                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

                Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldemenyStatusz =
                    Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                ExecParam execParam_szignalhato = execParam.Clone();
                ErrorDetails errorDetail = null;

                if (Contentum.eRecord.BaseUtility.Kuldemenyek.Szignalhato(kuldemenyStatusz, execParam_szignalhato, out errorDetail) == false)
                {
                    // Nem szignálható
                    throw new ResultException(52582, errorDetail);
                }

                // Küldemény UPDATE:
                erec_KuldKuldemenyek.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);
                erec_KuldKuldemenyek.Base.Updated.Ver = true;

                // Küldeményhez kapcsolódik-e már irat?
                // ha igen, lekérjük; ha nem, hozzáteszünk egyet

                EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

                EREC_IraIratok erec_IraIrat = null;

                if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIratok_Id))
                {
                    #region Irat UPDATE:

                    ExecParam execParam_iratGet = execParam.Clone();
                    execParam_iratGet.Record_Id = erec_KuldKuldemenyek.IraIratok_Id;

                    Result result_iratGet = service_iratok.Get(execParam_iratGet);
                    result_iratGet.CheckError();

                    erec_IraIrat = (EREC_IraIratok)result_iratGet.Record;

                    erec_IraIrat.Updated.SetValueAll(false);
                    erec_IraIrat.Base.Updated.SetValueAll(false);
                    erec_IraIrat.Base.Updated.Ver = true;

                    // Ügyintézõ beállítása:
                    //erec_IraIrat.FelhasznaloCsoport_Id_Ugyintez = CsoportId_Ugyintezo;
                    //erec_IraIrat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                    erec_IraIrat.Base.Note = Contentum.eRecord.BaseUtility.Iratok.SetBusinessObjectFieldXMLForKuldemenyElokeszites(erec_IraIrat.Base.Note
                        , Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez", CsoportId_Ugyintezo);
                    erec_IraIrat.Base.Updated.Note = true;

                    ExecParam execParam_iratUpdate = execParam.Clone();
                    execParam_iratUpdate.Record_Id = erec_IraIrat.Id;

                    Result result_iratUpdate = service_iratok.Update(execParam_iratUpdate, erec_IraIrat);
                    result_iratUpdate.CheckError();

                    #endregion
                }
                else
                {
                    #region Irat létrehozása
                    // Ügyintézõ beállítása

                    erec_IraIrat = new EREC_IraIratok();

                    // ügyintézõ
                    //erec_IraIrat.FelhasznaloCsoport_Id_Ugyintez = CsoportId_Ugyintezo;
                    //erec_IraIrat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                    erec_IraIrat.Base.Note = Contentum.eRecord.BaseUtility.Iratok.SetBusinessObjectFieldXMLForKuldemenyElokeszites(erec_IraIrat.Base.Note
                        , Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, "FelhasznaloCsoport_Id_Ugyintez", CsoportId_Ugyintezo);
                    erec_IraIrat.Base.Updated.Note = true;

                    // küldemény hozzákapcsolása:
                    erec_IraIrat.KuldKuldemenyek_Id = erec_KuldKuldemenyek.Id;
                    erec_IraIrat.Updated.KuldKuldemenyek_Id = true;

                    // iktatót kötelezõ megadni, de igazából itt nem kéne
                    erec_IraIrat.FelhasznaloCsoport_Id_Iktato = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
                    erec_IraIrat.Updated.FelhasznaloCsoport_Id_Iktato = true;

                    // Tárgy is kötelezõ, átvesszük a küldeményból, ha van:
                    if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.Targy))
                    {
                        erec_IraIrat.Targy = erec_KuldKuldemenyek.Targy;
                    }
                    else
                    {
                        // csak hogy legyen valami:
                        erec_IraIrat.Targy = ".";
                    }
                    erec_IraIrat.Updated.Targy = true;

                    ExecParam execParam_IratInsert = execParam.Clone();

                    Result result_iratInsert = service_iratok.Insert(execParam_IratInsert, erec_IraIrat);
                    result_iratInsert.CheckError();

                    string newIratId = result_iratInsert.Uid;

                    erec_KuldKuldemenyek.IraIratok_Id = newIratId;
                    erec_KuldKuldemenyek.Updated.IraIratok_Id = true;

                    #endregion
                }



                // Küldeményt vissza kell küldeni az iktatóhelyre:


                if (!String.IsNullOrEmpty(erec_IraIrat.FelhasznaloCsoport_Id_Iktato))
                {
                    // Felelõs beállítása az irat iktatójára:

                    erec_KuldKuldemenyek.Csoport_Id_Felelos_Elozo = erec_KuldKuldemenyek.Csoport_Id_Felelos;
                    erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

                    if (!String.IsNullOrEmpty(CsoportId_Iktato))
                    {
                        erec_KuldKuldemenyek.Csoport_Id_Felelos = CsoportId_Iktato;
                        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;
                    }
                    else
                    {
                        // Ha a szignálás típusa be van állítva (1-es vagy 2-es), az iktatónak küldjük vissza a küldeményt
                        if (erec_KuldKuldemenyek.Iktathato == "S" || erec_KuldKuldemenyek.Iktathato == "U")
                        {
                            erec_KuldKuldemenyek.Csoport_Id_Felelos = erec_IraIrat.FelhasznaloCsoport_Id_Iktato;
                            erec_IraIrat.Updated.Csoport_Id_Felelos = true;
                        }
                        else
                        {
                            erec_KuldKuldemenyek.Csoport_Id_Felelos = CsoportId_Ugyintezo;
                            erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;
                        }
                    }

                    //// Ideiglenes:
                    //// iktató szervezetének meghatározása:
                    //Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportTagok =
                    //    eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

                    //// Iktatóra külön execparam:
                    //ExecParam execParam_iktato = new ExecParam();
                    //execParam_iktato.LoginUser_Id = erec_IraIrat.FelhasznaloCsoport_Id_Iktato;
                    //execParam_iktato.Felhasznalo_Id = execParam_iktato.LoginUser_Id;

                    //Result result_IktatoSzervezet = service_csoportTagok.GetSzervezet(execParam_iktato);

                    //if (!String.IsNullOrEmpty(result_IktatoSzervezet.ErrorCode))
                    //{
                    //    // hiba:
                    //    Logger.Error("Hiba a szervezet lekérésekor", execParam);
                    //    throw new ResultException(52595);
                    //}
                    //else
                    //{
                    //    string szervezet_Id = result_IktatoSzervezet.Uid;

                    //    if (!String.IsNullOrEmpty(szervezet_Id))
                    //    {
                    //        //erec_KuldKuldemenyek.Csoport_Id_Felelos = erec_IraIrat.FelhasznaloCsoport_Id_Iktato;
                    //        erec_KuldKuldemenyek.Csoport_Id_Felelos = szervezet_Id;
                    //        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;
                    //    }
                    //    else
                    //    {
                    //        // hiba:
                    //        Logger.Error("Nem határozható meg az iktató szervezete!", execParam);
                    //        throw new ResultException(52595);
                    //    }
                    //}
                }

                // Állapot: Szignált
                erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Szignalva;
                erec_KuldKuldemenyek.Updated.Allapot = true;

                ExecParam execParam_kuldUpdate = execParam.Clone();
                execParam_kuldUpdate.Record_Id = erec_KuldKuldemenyek.Id;

                Result result_kuldUpdate = this.Update(execParam_kuldUpdate, erec_KuldKuldemenyek);
                result_kuldUpdate.CheckError();

                #region Kézbesítési tétel átadottra állítása:

                EREC_IraKezbesitesiTetelekService service_kezbTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);

                string[] kuld_array = new string[1];
                kuld_array[0] = erec_KuldKuldemenyek.Id;

                Result result_kezbTetel = service_kezbTetelek.SetKezbesitesiTetelToAtadott_Tomeges(execParam, kuld_array);
                result_kezbTetel.CheckError();

                #endregion


                #region Kezelési feljegyzés INSERT, ha megadták
                if (erec_HataridosFeladatok != null)
                {
                    Logger.Debug("Kezelési feljegyzés Insert", execParam);

                    EREC_HataridosFeladatokService service_kuldKezFelj = new EREC_HataridosFeladatokService(this.dataContext);
                    ExecParam execParam_kezFeljInsert = execParam.Clone();

                    // Küldeményhez kötés
                    erec_HataridosFeladatok.Obj_Id = erec_KuldKuldemenyek.Id;
                    erec_HataridosFeladatok.Updated.Obj_Id = true;

                    erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;
                    erec_HataridosFeladatok.Updated.Obj_type = true;

                    if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.Azonosito))
                    {
                        erec_HataridosFeladatok.Azonosito = erec_KuldKuldemenyek.Azonosito;
                        erec_HataridosFeladatok.Updated.Azonosito = true;
                    }

                    Result result_kezFeljInsert = service_kuldKezFelj.Insert(
                        execParam_kezFeljInsert, erec_HataridosFeladatok);
                    result_kezFeljInsert.CheckError();
                }
                #endregion


                result = result_kuldUpdate;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek_Id, "EREC_KuldKuldemenyek", "KuldemenySzignalas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private bool IsKimenoKuldemenyTertivevenyes(EREC_KuldKuldemenyek erec_KuldKuldemenyek, bool onlyUpdated)
    {
        bool isTertivevenyes = false;
        if (erec_KuldKuldemenyek.Tertiveveny == "1")
        {
            if (!onlyUpdated || erec_KuldKuldemenyek.Updated.Tertiveveny)
            {
                isTertivevenyes = true;
            }
        }
        else if (erec_KuldKuldemenyek.KimenoKuldemenyFajta == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat
            || erec_KuldKuldemenyek.KimenoKuldemenyFajta == KodTarak.KIMENO_KULDEMENY_FAJTA.Hivatalos_irat_sajat_kezbe
            || erec_KuldKuldemenyek.KimenoKuldemenyFajta == "08"
            || erec_KuldKuldemenyek.KimenoKuldemenyFajta == "09"
            || erec_KuldKuldemenyek.KimenoKuldemenyFajta == "10"
            || erec_KuldKuldemenyek.KimenoKuldemenyFajta == "11")
        {
            if (!onlyUpdated || erec_KuldKuldemenyek.Updated.KimenoKuldemenyFajta)
            {
                isTertivevenyes = true;
            }
        }
        else if (erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
        {
            isTertivevenyes = true;
        }

        return isTertivevenyes;
    }


    /// <summary>
    /// Küldemény módosítása
    /// Felelõs változásának figyelése a tárolt eljárásban: ha változik a felelõs, kézbesítési tétel generálás
    /// Jelenleg csak a kimenõ küldemények tömeges postázásánál használt,
    /// ezért nem tartalmaz minden lépést a küldemény UPDATE metódusból
    /// TODO:
    /// Vonalkód változások kezelése - ellenõrzés, kötés
    /// Iktatókönyv módosíthatóságának letiltása, kivéve postakönyv
    /// Borító típus változásának vizsgálata: mellékletek módosítása, felvétele, érvénytelenítése szükség szerint
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Ids"></param>
    /// <param name="Vers"></param>
    /// <param name="BarCode_Array"></param>
    /// <param name="RagSzam_Array"></param>
    /// <param name="Record"></param>
    /// <param name="ExecutionTime"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result UpdateTomeges(ExecParam ExecParam, List<string> Ids, List<string> Vers, List<string> BarCode_List, List<string> Ragszam_List, EREC_KuldKuldemenyek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            #region Paraméterek ellenõrzése
            if (ExecParam == null || String.IsNullOrEmpty(ExecParam.Felhasznalo_Id)
                || Ids == null || Vers == null
                || Ids.Count == 0 || Vers.Count == 0)
            {
                // hiba
                // Küldemények: hiányzó paraméter(ek)!
                throw new ResultException(53840);
            }
            #endregion Paraméterek ellenõrzése

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Általában: Nem lehet erkezteto szamot es erkezeto konyv_id-t modositani!
            // Kimenõ küldeménynél viszont be kell írni a postakönyv azonosítót, ezért
            // a régi küldemény lekérése után vizsgáljuk, hogy a IraIktatokonyv_Id módosítást
            // le kell-e tiltani
            Record.Updated.Erkezteto_Szam = false;
            //Record.Updated.IraIktatokonyv_Id = false;

            #region Vonalkód változásának vizsgálata
            // TODO: tárolt eljárás bõvítése
            #endregion


            Result result_kuldemenyUpdate = sp.UpdateTomeges(ExecParam, Ids, Vers, BarCode_List, Ragszam_List, Record, ExecutionTime);
            result_kuldemenyUpdate.CheckError();

            #region Vonalkód kötése, ha kell:
            // TODO: tárolt eljárás bõvítése
            #endregion


            #region Borító típus változásának vizsgálata
            // TODO: tárolt eljárás bõvítése
            #endregion

            result = result_kuldemenyUpdate;

            #region Partner cím
            Result result_bindCim = BindPartnerCim(ExecParam.Clone(), Record);
            result_bindCim.CheckError();
            #endregion Partner cím

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, Search.GetSqlInnerString(Ids.ToArray()), "KuldemenyModify");
                if (eventLogResult.IsError)
                    Logger.Debug("[ERROR]EREC_KuldKuldemenyek::UpdateTomeges: ", ExecParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private string GetKuldesModFromKimenoKuldemeny(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        /*BLG_2941*/
        string kuldesMod = String.IsNullOrEmpty(erec_KuldKuldemenyek.KuldesMod) ? KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima : erec_KuldKuldemenyek.KuldesMod;// != KodTarak.KULDEMENY_KULDES_MODJA.E_mail ? KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima : KodTarak.KULDEMENY_KULDES_MODJA.E_mail; // default

        //hivatali kapu
        // BLG_1721
        // TUK Futárszolgálat, TUK Személyes ézbesítés
        //if (erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu
        //    || erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz)

        if (erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu //Ezt a feltételt ne vedd ki mert IsKimenoKuldemenyTertivevenyes hívás true-t fog visszaadni hivatali kapura és lentebb akkor felül fogja írni a kuldésmodot....(Side effect rulez)
              || erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz
              || erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Futarszolgalat
              || erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Szemelyes_kezbesites
              // BLG_237
              || erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.E_mail)
        {
            kuldesMod = erec_KuldKuldemenyek.KuldesMod;
        }
        else
        {
            if (kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Postai_tertivevenyes 
                || kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Postai_ajanlott
                || kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Postai_elsobbsegi)
            {
                kuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima;
            }

            if (IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, false))
            {
                // Tértivevény
                kuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_tertivevenyes;
            }
            else if (erec_KuldKuldemenyek.Ajanlott == "1")
            {
                // Ajánlott
                kuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_ajanlott;
            }
            else if (erec_KuldKuldemenyek.Elsobbsegi == "1")
            {
                // Elsõbbségi
                kuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_elsobbsegi;
            }

            if (erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
            {
                kuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
            }

            if (erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz)
            {
                kuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Elhisz;
            }
        }

        return kuldesMod;
    }

    /// <summary>
    /// Figyelem: nincs ellenõrzés egy esetleges tértivevény melléklet létezésére!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="tertiveveny"></param>
    /// <returns></returns>
    private Result AddTertivevenyMellekletToKimenoKuldemeny(ExecParam execParam, EREC_KuldTertivevenyek tertiveveny)
    {
        if (String.IsNullOrEmpty(tertiveveny.Kuldemeny_Id))
        {
            throw new ResultException("Tértivevény melléklet felvétele: Nincs megadva minden paraméter!");
        }

        // Tértivevény típusú melléklet létrehozása a kimenõ küldeményhez
        EREC_MellekletekService service_mellekletek = new EREC_MellekletekService(this.dataContext);
        EREC_Mellekletek erec_Mellekletek = new EREC_Mellekletek();
        erec_Mellekletek.Updated.SetValueAll(false);
        erec_Mellekletek.Base.Updated.SetValueAll(false);

        erec_Mellekletek.KuldKuldemeny_Id = tertiveveny.Kuldemeny_Id;
        erec_Mellekletek.Updated.KuldKuldemeny_Id = true;

        erec_Mellekletek.BarCode = tertiveveny.BarCode;
        erec_Mellekletek.Updated.BarCode = true;

        erec_Mellekletek.AdathordozoTipus = KodTarak.AdathordozoTipus.Tertiveveny;
        erec_Mellekletek.Updated.AdathordozoTipus = true;

        erec_Mellekletek.MennyisegiEgyseg = KodTarak.MennyisegiEgyseg.Darab;
        erec_Mellekletek.Updated.MennyisegiEgyseg = true;

        erec_Mellekletek.Mennyiseg = "1";
        erec_Mellekletek.Updated.Mennyiseg = true;

        Result result = service_mellekletek.Insert(execParam.Clone(), erec_Mellekletek, false);

        return result;
    }

    /// <summary>
    /// Figyelem: nincs ellenõrzés egy esetleges tértivevény melléklet létezésére!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="tertiveveny"></param>
    /// <returns></returns>
    private Result AddTertivevenyMellekletToKimenoKuldemeny_Tomeges(ExecParam execParam, List<string> Kuldemeny_Id_List, List<string> BarCode_List)
    {
        if (Kuldemeny_Id_List == null || Kuldemeny_Id_List.Count == 0 || BarCode_List == null || BarCode_List.Count == 0)
        {
            // Hiba a küldemények tértivevevény típusú mellékleteinek tömeges létrehozásánál: hiányzó paraméter(ek)!
            throw new ResultException(53830);
        }

        // Tértivevény típusú melléklet létrehozása a kimenõ küldeményhez
        EREC_MellekletekService service_mellekletek = new EREC_MellekletekService(this.dataContext);
        EREC_Mellekletek erec_Mellekletek = new EREC_Mellekletek();
        erec_Mellekletek.Updated.SetValueAll(false);
        erec_Mellekletek.Base.Updated.SetValueAll(false);

        // tárolt eljárás alapértelmezetten is állítja - de kötelezõ paraméter az eredeti insertben, ezért kitöltjük:
        erec_Mellekletek.AdathordozoTipus = KodTarak.AdathordozoTipus.Tertiveveny;
        erec_Mellekletek.Updated.AdathordozoTipus = true;

        erec_Mellekletek.MennyisegiEgyseg = KodTarak.MennyisegiEgyseg.Darab;
        erec_Mellekletek.Updated.MennyisegiEgyseg = true;

        erec_Mellekletek.Mennyiseg = "1";
        erec_Mellekletek.Updated.Mennyiseg = true;

        Result result = service_mellekletek.InsertTertivevenyTomeges(execParam.Clone(), Kuldemeny_Id_List, BarCode_List, erec_Mellekletek, DateTime.Now);

        return result;
    }

    private Result UpdateTertivevenyMellekletBarCode(ExecParam execParam, EREC_KuldTertivevenyek tertiveveny)
    {
        if (String.IsNullOrEmpty(tertiveveny.Kuldemeny_Id))
        {
            // Tértivevény melléklet vonalkód módosítás: Nincs megadva minden paraméter!
            throw new ResultException(53861);
        }

        // Tértivevény típusú kimenõ küldemény melléklet módosítása
        EREC_MellekletekService service_mellekletek = new EREC_MellekletekService(this.dataContext);
        EREC_MellekletekSearch search = new EREC_MellekletekSearch();

        search.KuldKuldemeny_Id.Filter(tertiveveny.Kuldemeny_Id);
        search.AdathordozoTipus.Filter(KodTarak.AdathordozoTipus.Tertiveveny);

        Result result_getAll = service_mellekletek.GetAll(execParam, search);
        result_getAll.CheckError();

        Result result = new Result();
        ExecParam execParam_update = execParam.Clone();
        // elvileg csak egy lehet
        foreach (DataRow row in result_getAll.Ds.Tables[0].Rows)
        {
            execParam_update.Record_Id = row["Id"].ToString();

            string barcode = row["BarCode"].ToString();
            if (barcode != tertiveveny.BarCode)
            {
                Result result_update = service_mellekletek.UpdateBarCodeWithoutBinding(execParam_update, tertiveveny.BarCode);
                result_update.CheckError();

                result = result_update;
            }
        }

        return result;
    }

    private Result RemoveTertivevenyMellekletFromKimenoKuldemeny(ExecParam execParam, EREC_KuldTertivevenyek tertiveveny)
    {
        if (String.IsNullOrEmpty(tertiveveny.Kuldemeny_Id))
        {
            // Tértivevény melléklet érvénytelenítés: Nincs megadva minden paraméter!
            throw new ResultException(53862);
        }

        // Tértivevény típusú kimenõ küldemény melléklet érvénytelenítése
        EREC_MellekletekService service_mellekletek = new EREC_MellekletekService(this.dataContext);
        EREC_MellekletekSearch search = new EREC_MellekletekSearch();

        search.KuldKuldemeny_Id.Filter(tertiveveny.Kuldemeny_Id);
        search.AdathordozoTipus.Filter(KodTarak.AdathordozoTipus.Tertiveveny);

        Result result_getAll = service_mellekletek.GetAll(execParam, search);
        result_getAll.CheckError();

        Result result = new Result();
        ExecParam execParam_invalidate = execParam.Clone();
        // elvileg csak egy lehet
        if (result_getAll.GetCount > 1)
        {
            Logger.Warn(String.Format("Tértivevény melléklet érvénytelenítés: több tértivevény típusú melléklet is tartozik a kimenõ küldeményhez:({0} db)!", result_getAll.Ds.Tables[0].Rows.Count));
        }

        foreach (DataRow row in result_getAll.Ds.Tables[0].Rows)
        {
            execParam_invalidate.Record_Id = row["Id"].ToString();

            string barcode = row["BarCode"].ToString();
            if (barcode != tertiveveny.BarCode)
            {
                // TODO: hogyan kezeljük? Hiba? Most érvénytelenítjük ezt is
                Logger.Warn(String.Format("Tértivevény melléklet érvénytelenítés: eltér a talált melléklet vonalkódja ({0}) a tértivevény vonalkódjától ({1})!", barcode, tertiveveny.BarCode));
            }

            Result result_invalidate = service_mellekletek.Invalidate(execParam_invalidate);
            result_invalidate.CheckError();

            result = result_invalidate;
        }

        return result;
    }

    /// <summary>
    /// Kimenõ küldemény postázása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public Result Postazas(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string tertivevenyVonalkod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény Postázása Start", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || erec_KuldKuldemenyek == null || String.IsNullOrEmpty(erec_KuldKuldemenyek.Id))
        {
            // hiba a postázás során
            Result result1 = ResultError.CreateNewResultWithErrorCode(52530);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // BLG_237
            bool isTertivevenyes = false;
            if (erec_KuldKuldemenyek.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.E_mail)
            {
                // BUG_13250
                //if (Rendszerparameterek.GetBoolean(execParam, Contentum.eUtility.Rendszerparameterek.EMAIL_KIMENO_TERTIVEVENY, false))
                if ((Rendszerparameterek.GetBoolean(execParam, Contentum.eUtility.Rendszerparameterek.EMAIL_KIMENO_TERTIVEVENY, false))
                    && !(String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam)) // ha addInból jön, van ragszám kitöltve 
                   )

                {
                    // KimenoEmailTertivevenyesBeallitas(execParam, erec_KuldKuldemenyek, tertivevenyVonalkod);
                    isTertivevenyes = true;
                }
            }
            else isTertivevenyes = IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, false);

            bool isKulfoldi = IsKimenoKuldemenyKulfoldi(erec_KuldKuldemenyek);

            #region Ellenõrzés, postázható-e

            #region Tértivevény vonalkód/ajánlott ragszám ellenõrzés
            //if (erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes)
            {
                if (!isKulfoldi)
                {
                    // CR 3088 :  BOPMH-nál, CSBO-nál nem kötelezõ a tértivevény kitöltése (csak FPH-nál)
                    // ORG függõ megjelenítés
                    // BLG_591
                    //if ((String.IsNullOrEmpty(tertivevenyVonalkod)) && (Session != null && FelhasznaloProfil.OrgKod(Session) == Contentum.eUtility.Constants.OrgKod.FPH))
                    if ((String.IsNullOrEmpty(tertivevenyVonalkod)) && (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TERTIVONALKOD_KELL, false)))

                    //     if (String.IsNullOrEmpty(tertivevenyVonalkod))
                    {
                        //hivatali kapu esetén nem kell vonalkód
                        if (erec_KuldKuldemenyek.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu
                            && erec_KuldKuldemenyek.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.Elhisz
                            && erec_KuldKuldemenyek.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.HAIR
                            )
                        {
                            //"Hiba a postázásnál: a tértivevényes küldemény tértivevény vonalkódja nincs megadva!"
                            throw new ResultException(52536);
                        }
                    }
                }
                if (String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    //"Hiba a postázásnál: a tértivevényes küldemény ragszáma nincs megadva!"
                    throw new ResultException(52537);
                }
            }
            else if (erec_KuldKuldemenyek.Ajanlott == "1")
            {
                if (String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    // "Hiba a postázásnál: az ajánlott küldemény ragszáma nincs megadva!"
                    throw new ResultException(52538);
                }

            }
            #endregion Tértivevény vonalkód/ajánlott ragszám ellenõrzés

            ExecParam execParam_kuldGet = execParam.Clone();
            execParam_kuldGet.Record_Id = erec_KuldKuldemenyek.Id;

            Result result_kuldGet = this.Get(execParam_kuldGet);
            result_kuldGet.CheckError();

            EREC_KuldKuldemenyek obj_kuldemeny = (EREC_KuldKuldemenyek)result_kuldGet.Record;

            Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldStatusz =
                Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByBusinessDocument(obj_kuldemeny);
            ErrorDetails errorDetail = null;

            if (Contentum.eRecord.BaseUtility.Kuldemenyek.Postazhato(kuldStatusz, execParam, out errorDetail) == false
                || obj_kuldemeny.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                Logger.Error("A kimenõ küldemény nem postázható", execParam);
                throw new ResultException(52535, errorDetail);
            }

            #region elektronikus postakönyv

            string elektronikusPostakonyvId = GetElektronikusPostakonyvId(execParam);

            if ((String.IsNullOrEmpty(erec_KuldKuldemenyek.IraIktatokonyv_Id)))
            {
                erec_KuldKuldemenyek.IraIktatokonyv_Id = elektronikusPostakonyvId;
                erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = true;
            }

            #endregion

            #region Ragszámhoz küldemény rendelése ha még nem történt meg

            string val = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA);
            bool elektronikus = (erec_KuldKuldemenyek.IraIktatokonyv_Id == elektronikusPostakonyvId);

            if (!elektronikus && !String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam) && (val == "A" || val == "M" || val == "T"))
            {

                KRT_RagSzamokService ragszamokkService = new KRT_RagSzamokService(this.dataContext);
                ExecParam execParam_ragszam = execParam.Clone();
                KRT_RagSzamokSearch ragSzamokSearch = new KRT_RagSzamokSearch();
                ragSzamokSearch.Kod.Filter(erec_KuldKuldemenyek.RagSzam);

                Result ragszamResult = ragszamokkService.GetAll(execParam_ragszam, ragSzamokSearch);
                ragszamResult.CheckError();

                Result res;
                string uId;
                //Ha nem létező ragszám akkor hibát dobunk, ha létező, de még nem volt foglalózva, akkor lefoglaljuk
                if (ragszamResult.GetCount < 1)
                {
                    Logger.Error("A megadott ragszám nem létezik.");
                    throw new ResultException("66666", "A megadott ragszám nem létezik: " + erec_KuldKuldemenyek.RagSzam);
                }
                else
                {
                    var ragszamRow = ragszamResult.Ds.Tables[0].Rows[0];
                    uId = ragszamRow["Id"].ToString();
                    string postakonyvId = ragszamRow["Postakonyv_Id"].ToString();

                    if (postakonyvId.ToLower().Trim() != erec_KuldKuldemenyek.IraIktatokonyv_Id.ToLower().Trim())
                    {
                        Logger.Error("A megadott ragszám nem tartozik az adott postakönyvhöz: " + erec_KuldKuldemenyek.RagSzam);
                        throw new ResultException("66666", "A megadott ragszám nem tartozik az adott postakönyvhöz: " + erec_KuldKuldemenyek.RagSzam);
                    }

                    if (ragszamRow["Allapot"].ToString() != "F")
                    {
                        float? ragszamNum = GetRagszamNumber(erec_KuldKuldemenyek.RagSzam);
                        res = RagszamFoglalasManual(execParam, erec_KuldKuldemenyek.RagSzam, ragszamNum.Value);

                        if (!String.IsNullOrEmpty(res.ErrorCode))
                            throw new ResultException(res);

                        uId = res.Uid;
                    }
                }

                Result resultRagszamFind = FindRagszamById(execParam, uId);
                if (!String.IsNullOrEmpty(resultRagszamFind.ErrorCode))
                    throw new ResultException(resultRagszamFind);

                KRT_RagSzamok ragszamFound = (KRT_RagSzamok)resultRagszamFind.Record;

                if (ragszamFound.Obj_Id != erec_KuldKuldemenyek.Id)
                {
                    ragszamFound.Updated.SetValueAll(false);
                    ragszamFound.Base.Updated.SetValueAll(false);

                    ragszamFound.Obj_Id = erec_KuldKuldemenyek.Id;
                    ragszamFound.Updated.Obj_Id = true;
                    ragszamFound.Obj_type = "EREC_KuldKuldemenyek";
                    ragszamFound.Updated.Obj_type = true;
                    ragszamFound.Base.Updated.Ver = true;

                    KRT_RagSzamokService service = new KRT_RagSzamokService(this.dataContext);
                    ExecParam execPm = execParam.Clone();
                    execPm.Record_Id = ragszamFound.Id;
                    Result ragszamUpdateResult = service.Update(execPm, ragszamFound);
                    ragszamUpdateResult.CheckError();
                }
            }

            #endregion

            #endregion


            #region Küldemény UPDATE

            // erec_KuldKuldemenyek-ben megadottakon kívül: Állapot módosítása

            erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Postazott;
            erec_KuldKuldemenyek.Updated.Allapot = true;

            erec_KuldKuldemenyek.Base.Ver = obj_kuldemeny.Base.Ver;
            erec_KuldKuldemenyek.Base.Updated.Ver = true;


            #region Küldésmód meghatározása:
            string kuldesMod = GetKuldesModFromKimenoKuldemeny(erec_KuldKuldemenyek);
            //if (String.IsNullOrEmpty(obj_kuldemeny.KuldesMod))
            //{
            erec_KuldKuldemenyek.KuldesMod = kuldesMod;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;
            //}
            #endregion

            // CR3306 kapcsán a kiküldõ és a postázás dátuma nem került kitöltésre 
            //Kiküldõ:
            if (String.IsNullOrEmpty(erec_KuldKuldemenyek.Csoport_Id_Cimzett)) erec_KuldKuldemenyek.Csoport_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Felelos;
            erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = true;

            if (String.IsNullOrEmpty(erec_KuldKuldemenyek.BelyegzoDatuma)) erec_KuldKuldemenyek.BelyegzoDatuma = DateTime.Now.ToString();
            erec_KuldKuldemenyek.Updated.BelyegzoDatuma = true;

            #region érkezteõszám

            EREC_IraIktatoKonyvekService iktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);
            ExecParam execParamIktatoKonyvek = execParam.Clone();

            execParamIktatoKonyvek.Record_Id = erec_KuldKuldemenyek.IraIktatokonyv_Id;
            Result iktatoKonyvekResult = iktatoKonyvekService.Get(execParamIktatoKonyvek);

            if (iktatoKonyvekResult.IsError)
            {
                throw new ResultException(iktatoKonyvekResult);
            }

            EREC_IraIktatoKonyvek iktatoKonyv = iktatoKonyvekResult.Record as EREC_IraIktatoKonyvek;
            int newFoszam = int.Parse(iktatoKonyv.UtolsoFoszam) + 1;

            iktatoKonyv.UtolsoFoszam = newFoszam.ToString();


            if (String.IsNullOrEmpty(iktatoKonyvekResult.ErrorCode))
            {
                erec_KuldKuldemenyek.Erkezteto_Szam = newFoszam.ToString();
            }


            erec_KuldKuldemenyek.Azonosito = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullKuldemenyAzonosito(execParam, (EREC_IraIktatoKonyvek)iktatoKonyvekResult.Record, erec_KuldKuldemenyek);
            erec_KuldKuldemenyek.Updated.Azonosito = true;
            erec_KuldKuldemenyek.Updated.Erkezteto_Szam = true;

            iktatoKonyv.Updated.UtolsoFoszam = true;
            Result updateResult = iktatoKonyvekService.Update(execParamIktatoKonyvek, iktatoKonyv);

            #endregion


            ExecParam execParam_kuldUpdate = execParam.Clone();
            execParam_kuldUpdate.Record_Id = erec_KuldKuldemenyek.Id;

            result = this.Update(execParam_kuldUpdate, erec_KuldKuldemenyek);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }
            #endregion          

            result.Uid = erec_KuldKuldemenyek.Id;
            #region Kimenõ küldeményhez tartozó iratpéldányok módosítása:

            #region Iratpéldányok lekérése:
            EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch(true);
            ExecParam execParam_PldGetAll = execParam.Clone();

            Result result_PldGetAll = service_Pld.GetAllWithExtensionByKimenoKuldemeny(execParam_PldGetAll, search, erec_KuldKuldemenyek.Id);
            if (!String.IsNullOrEmpty(result_PldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_PldGetAll);
            }
            #endregion
            List<string> postazottIrat = new List<string>();
            foreach (System.Data.DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
            {
                string pld_Id = row["Id"].ToString();
                string pld_Ver = row["Ver"].ToString();
                string pld_Allapot = row["Allapot"].ToString();

                // Állapot ellenõrzés: (csak expediált postázható)
                if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Expedialt)
                {
                    // Nem postázható
                    Logger.Error("Az iratpéldány nem postázható: " + pld_Id, execParam);
                    throw new ResultException(52533);
                }

                #region Pld update

                Logger.Info("Postázás - Iratpéldány update", execParam);

                EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
                iratPeldany.Updated.SetValueAll(false);
                iratPeldany.Base.Updated.SetValueAll(false);

                iratPeldany.Base.Ver = pld_Ver;
                iratPeldany.Base.Updated.Ver = true;

                //Állapot Postázottra:
                iratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Postazott;
                iratPeldany.Updated.Allapot = true;

                // Postázás dátuma a kuldeménynél megadott lesz:
                iratPeldany.PostazasDatuma = erec_KuldKuldemenyek.BelyegzoDatuma;
                iratPeldany.Updated.PostazasDatuma = true;

                ExecParam execParam_PldUpdate = execParam.Clone();
                execParam_PldUpdate.Record_Id = pld_Id;

                Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, iratPeldany);
                if (!String.IsNullOrEmpty(result_PldUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_PldUpdate);
                }

                #endregion
                string IraIrat_Id = row["IraIrat_Id"].ToString();
                if (!postazottIrat.Contains(IraIrat_Id))
                    postazottIrat.Add(IraIrat_Id);
            }
            #endregion Kimenõ küldeményhez tartozó iratpéldányok módosítása

            #region BLG_1354
            this.StopElteltIdoPostazasSoranByIratIds(execParam.Clone(), postazottIrat);
            #endregion BLG_1354

            #region UgyintezesKezdoDatuma + UgyintezesiIdoUjraSzamolas
            if (postazottIrat != null && postazottIrat.Count > 0)
            {
                Dictionary<string, string> ugyiratIds = new Dictionary<string, string>();

                EREC_IraIratok irat = null;
                foreach (string iratId in postazottIrat)
                {
                    try
                    {
                        irat = GetIrat(execParam.Clone(), iratId);
                        irat.UgyintezesKezdoDatuma = erec_KuldKuldemenyek.BelyegzoDatuma;
                        irat.Updated.UgyintezesKezdoDatuma = true;
                        UpdateIrat(execParam.Clone(), irat);

                        if (!ugyiratIds.ContainsKey(irat.Ugyirat_Id))
                            ugyiratIds.Add(irat.Ugyirat_Id, irat.Ugyirat_Id);
                    }
                    catch (Exception exc)
                    {
                        Logger.Error("Irat UgyintezesKezdoDatuma Hiba:", exc);
                    }
                }

                #region UgyintezesiIdoUjraSzamolas
                //if (ugyiratIds != null && ugyiratIds.Count > 0)
                //{
                //    SakkoraService sakkSvc = new SakkoraService(this.dataContext);
                //    foreach (KeyValuePair<string, string> entry in ugyiratIds)
                //    {
                //        try
                //        {
                //            sakkSvc.SakkoraUgyintezesiIdoUjraSzamolas(execParam.Clone(), entry.Key);
                //            Logger.Debug("SakkoraUgyintezesiIdoUjraSzamolas");
                //        }
                //        catch (Exception exc)
                //        {
                //            Logger.Error("SakkoraUgyintezesiIdoUjraSzamolas Hiba:", exc);
                //        }
                //    }
                //}
                #endregion
            }
            #endregion UgyintezesKezdoDatuma


            #region Tértivevény kezelés (insert, update, invalidate)

            // EB 2011.01.06: Tértivevényt már nem hozunk létre expediáláskor,
            // ezért itt elég létrehozni, nem kell vizsgálni a létezõket

            // ha a küldés módja tértivevényes:
            //if (erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes)
            {
                EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);
                ExecParam execParam_tertiveveny = execParam.Clone();

                EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                tertiveveny.Updated.SetValueAll(false);
                tertiveveny.Base.Updated.SetValueAll(false);

                // INSERT
                tertiveveny.Kuldemeny_Id = obj_kuldemeny.Id;
                tertiveveny.Updated.Kuldemeny_Id = true;

                tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
                tertiveveny.Updated.Allapot = true;

                tertiveveny.BarCode = tertivevenyVonalkod;
                tertiveveny.Updated.BarCode = true;

                tertiveveny.Ragszam = erec_KuldKuldemenyek.RagSzam;
                tertiveveny.Updated.Ragszam = true;

                if (!KuldemenyTertivevenyLetezik(execParam, tertiveveny.Kuldemeny_Id, tertiveveny.Ragszam))
                {
                    Result result_tertInsert = service_tertiveveny.Insert(execParam_tertiveveny, tertiveveny);
                    result_tertInsert.CheckError();
                }
                else
                {
                    Logger.Debug("A kimenõ küldemény tértivevény már létezik, nem hozzuk létre.", execParam);
                }
                // Tértivevény típusú melléklet létrehozása a kimenõ küldeményhez
                Result result_mellekletekInsert = this.AddTertivevenyMellekletToKimenoKuldemeny(execParam.Clone(), tertiveveny);
                result_mellekletekInsert.CheckError();
            }
            #endregion Tértivevény kezelés (insert, update, invalidate)

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek.Id, "EREC_KuldKuldemenyek", "Postazas").Record;
                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    // BUG_4345
    // BLG_237
    private String GetElektronikusPostakonyvId(ExecParam _execParam)
    {
        Result result = new Result();
        String epId = String.Empty;
        ExecParam execParam = _execParam.Clone();
        string ep_azonosito = Rendszerparameterek.Get(execParam, Rendszerparameterek.ELEKTRONIKUS_POSTAKONYV_AZONOSITO);
        if (String.IsNullOrEmpty(ep_azonosito))
        {
            throw new Exception("Nincs beállítva Elektronikus Postakönyv!");
        }
        EREC_IraIktatoKonyvekService iktatoKonyvekService = new EREC_IraIktatoKonyvekService(this.dataContext);
        EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
        erec_IraIktatoKonyvekSearch.Azonosito.Filter(ep_azonosito);
        erec_IraIktatoKonyvekSearch.IktatoErkezteto.Filter(Contentum.eUtility.Constants.IktatoErkezteto.Postakonyv);
        result = iktatoKonyvekService.GetAll(execParam, erec_IraIktatoKonyvekSearch);
        result.CheckError();

        ResultError.CreateNoRowResultError(result);
        epId = result.Ds.Tables[0].Rows[0]["Id"].ToString();

        return epId;
    }

    /// <summary>
    /// Kimenõ küldemény postázása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public Result Postazas_Tomeges(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string[] VonalkodArray, string[] RagszamArray, string[] TertivevenyVonalkodArray)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemények tömeges postázása Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        List<string> vonalkodokHibasTetelek = null;

        try
        {
            // Paraméterek ellenõrzése:
            if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
                || erec_KuldKuldemenyek == null || VonalkodArray == null || VonalkodArray.Length == 0)
            {
                // hiba a postázás során
                throw new ResultException(52530);
            }

            if (Array.Exists(VonalkodArray, delegate (string item) { return String.IsNullOrEmpty(item); }))
            {
                // Hiba a tömeges postázásnál: Nincs megadva minden vonalkód!
                throw new ResultException(53850);
            }

            // hibás tételek vonalkódjainak tárolására - pl. felületen való megjelöléshez
            vonalkodokHibasTetelek = new List<string>(VonalkodArray.Length);

            bool isTertivevenyes = IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, false);
            bool isAjanlott = isTertivevenyes || erec_KuldKuldemenyek.Ajanlott == "1";
            bool isKulfoldi = IsKimenoKuldemenyKulfoldi(erec_KuldKuldemenyek);

            #region Ellenõrzés, postázható-e

            #region Tértivevény vonalkód/ajánlott ragszám ellenõrzés
            //if (erec_KuldKuldemenyek.Ajanlott == "1" || erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes || isAjanlott)
            {
                if (RagszamArray == null)
                { // Hiba a tömeges postázásnál: Ajánlott és tértivevényes küldeményeknél a ragszámok megadása kötelezõ!"
                    throw new ResultException(53801);
                }
                else if (RagszamArray.Length != VonalkodArray.Length)
                {
                    // Hiba a tömeges postázásnál: Nem egyértelmû az összerendelés a vonalkódok és ragszámok között! A vonalkódok és ragszámok darabszámának meg kell egyeznie.
                    throw new ResultException(53802);
                }
                else if (Array.Exists(RagszamArray, delegate (string item) { return String.IsNullOrEmpty(item); }))
                {
                    // Hiba a tömeges postázásnál: Nincs minden küldeményhez megadva a ragszám!
                    for (int i = 0; i < RagszamArray.Length; i++)
                    {
                        if (String.IsNullOrEmpty(RagszamArray[i]))
                        {
                            vonalkodokHibasTetelek.Add(VonalkodArray[i]);
                        }
                    }

                    throw new ResultException(53806);
                }

                //if (erec_KuldKuldemenyek.Tertiveveny == "1")
                if (isTertivevenyes)
                {
                    if (!isKulfoldi)
                    {
                        // CR 3088 :  BOPMH-nál, CSBO-nál nem kötelezõ a tértivevény kitöltése (csak FPH-nál)
                        // ORG függõ megjelenítés
                        // BLG_591
                        //if (FelhasznaloProfil.OrgKod(Session) == Contentum.eUtility.Constants.OrgKod.FPH)
                        if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TERTIVONALKOD_KELL, false))
                        {
                            if (TertivevenyVonalkodArray == null)
                            {
                                // Hiba a tömeges postázásnál: Tértivevényes küldeményeknél a tértivevény ragszámok megadása kötelezõ!
                                throw new ResultException(53803);
                            }
                            else if (TertivevenyVonalkodArray.Length != VonalkodArray.Length)
                            {
                                // Hiba a tömeges postázásnál: Nem egyértelmû az összerendelés a vonalkódok és a tértivevény ragszámok között! A vonalkódok és a tértivevény ragszámok darabszámának meg kell egyeznie.
                                throw new ResultException(53804);
                            }
                            else if (Array.Exists(TertivevenyVonalkodArray, delegate (string item) { return String.IsNullOrEmpty(item); }))
                            {
                                // Hiba a tömeges postázásnál: Nincs minden tértivevényes küldeményhez megadva a tértivevény ragszám!
                                for (int i = 0; i < TertivevenyVonalkodArray.Length; i++)
                                {
                                    if (String.IsNullOrEmpty(TertivevenyVonalkodArray[i]))
                                    {
                                        vonalkodokHibasTetelek.Add(VonalkodArray[i]);
                                    }
                                }
                                throw new ResultException(53807);
                            }
                        }
                    }
                }
            }

            #endregion Tértivevény vonalkód/ajánlott ragszám ellenõrzés

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // kimenõ küldemények lekérése vonalkódok alapján - ami nem postázható állapotú vagy nem kimenõ, le sem kérjük
            ExecParam execParam_kuldGetAll = execParam.Clone();
            EREC_KuldKuldemenyekSearch search_kuld = new EREC_KuldKuldemenyekSearch();
            search_kuld.BarCode.In(VonalkodArray);
            search_kuld.PostazasIranya.Filter(KodTarak.POSTAZAS_IRANYA.Kimeno);
            search_kuld.Allapot.In(Contentum.eRecord.BaseUtility.Kuldemenyek.GetPostazhatoAllapotok());

            Result result_kuldGetAll = this.GetAll(execParam_kuldGetAll, search_kuld);
            result_kuldGetAll.CheckError();

            if (result_kuldGetAll.GetCount != VonalkodArray.Length)
            {
                foreach (string vonalkod in VonalkodArray)
                {
                    if (result_kuldGetAll.Ds.Tables[0].Select(String.Format("BarCode='{0}'", vonalkod)).Length == 0)
                    {
                        vonalkodokHibasTetelek.Add(vonalkod);
                    }
                }
                // Hiba a tömeges postázásnál: A küldemények egy része nem található, esetleg nem kimenõ küldemény, vagy nem postázható állapotú!
                throw new ResultException(53805);
            }

            System.Collections.Hashtable statuszTable =
                Contentum.eRecord.BaseUtility.Kuldemenyek.GetAllapotByDataSet(execParam_kuldGetAll, result_kuldGetAll.Ds);
            ErrorDetails errorDetail = null;

            #region Ellenõrzés - státusz alapján, postázható-e
            foreach (DataRow row in result_kuldGetAll.Ds.Tables[0].Rows)
            {
                Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz kuldStatusz = (Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz)statuszTable[row["Id"].ToString()];
                if (Contentum.eRecord.BaseUtility.Kuldemenyek.Postazhato(kuldStatusz, execParam, out errorDetail) == false)
                {
                    Logger.Error("A kimenõ küldemény nem postázható", execParam);
                    vonalkodokHibasTetelek.Add(row["BarCode"].ToString());
                    throw new ResultException(52535, errorDetail);
                }
            }


            #endregion Ellenõrzés - státusz alapján, postázható-e

            #endregion Ellenõrzés, postázható-e

            #region Küldemény UPDATE

            #region Küldésmód meghatározása:
            // küldésmód meghatározása
            string kuldesMod = GetKuldesModFromKimenoKuldemeny(erec_KuldKuldemenyek);
            //if (String.IsNullOrEmpty(erec_KuldKuldemenyek.KuldesMod))
            //{
            erec_KuldKuldemenyek.KuldesMod = kuldesMod;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;
            //}
            #endregion Küldésmód meghatározása

            // erec_KuldKuldemenyek-ben megadottakon kívül: Állapot módosítása

            erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Postazott;
            erec_KuldKuldemenyek.Updated.Allapot = true;

            ExecParam execParam_kuldUpdate = execParam.Clone();

            System.Collections.Generic.List<string> lstKimenoKuldemenyIds = new System.Collections.Generic.List<string>();

            // kimenõ küldeménynek megfelelõ sorrendben
            //System.Collections.Generic.List<string> lstVonalkodok = new System.Collections.Generic.List<string>();
            System.Collections.Generic.List<string> lstRagSzamok = (isAjanlott ? new System.Collections.Generic.List<string>() : null);
            System.Collections.Generic.List<string> lstVers = new System.Collections.Generic.List<string>();
            foreach (DataRow row in result_kuldGetAll.Ds.Tables[0].Rows)
            {
                lstKimenoKuldemenyIds.Add(row["Id"].ToString());
                if (isAjanlott)
                {
                    lstRagSzamok.Add(RagszamArray[Array.IndexOf(VonalkodArray, row["BarCode"].ToString())]);
                }
                lstVers.Add(row["Ver"].ToString());

                //erec_KuldKuldemenyek.RagSzam = RagszamArray[Array.IndexOf(VonalkodArray, row["BarCode"].ToString())];
                //erec_KuldKuldemenyek.Updated.RagSzam = true;

                //erec_KuldKuldemenyek.Base.Ver = row["Ver"].ToString();
                //erec_KuldKuldemenyek.Base.Updated.Ver = true;

                //execParam_kuldUpdate.Record_Id = row["Id"].ToString();

                //// összegyûjtjük a kimenõ küldemények azonosítóit a késõbbi példánykereséshez
                //lstKimenoKuldemenyIds.Add(execParam_kuldUpdate.Record_Id);

                //Result result_kuldUpdate = this.Update(execParam_kuldUpdate, erec_KuldKuldemenyek);
                //if (result_kuldUpdate.IsError)
                //{
                //    vonalkodokHibasTetelek.Add(row["BarCode"].ToString());
                //    // hiba:
                //    throw new ResultException(result_kuldUpdate);
                //}
            }

            // UPDATE tömeges
            Result result_kuldUpdate = this.UpdateTomeges(execParam_kuldUpdate, lstKimenoKuldemenyIds, lstVers, null, lstRagSzamok, erec_KuldKuldemenyek, DateTime.Now);
            result_kuldUpdate.CheckError();
            #endregion

            #region Kimenõ küldeményhez tartozó iratpéldányok módosítása:

            #region Iratpéldányok lekérése:
            EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch(true);
            ExecParam execParam_PldGetAll = execParam.Clone();


            string Filter_KimenoKuldemenyIds = Search.GetSqlInnerString(lstKimenoKuldemenyIds.ToArray());

            Result result_PldGetAll = service_Pld.GetAllWithExtensionByKimenoKuldemeny(execParam_PldGetAll, search, Filter_KimenoKuldemenyIds);
            if (!String.IsNullOrEmpty(result_PldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_PldGetAll);
            }
            #endregion

            List<string> pldIds = new List<string>(result_PldGetAll.Ds.Tables[0].Rows.Count);
            List<string> pldVers = new List<string>(result_PldGetAll.Ds.Tables[0].Rows.Count);

            //foreach (System.Data.DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
            //{
            //    string pld_Id = row["Id"].ToString();
            //    string pld_Ver = row["Ver"].ToString();
            //    string pld_Allapot = row["Allapot"].ToString();

            //    // Állapot ellenõrzés: (csak expediált postázható)
            //    if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Expedialt)
            //    {
            //        // Nem postázható
            //        Logger.Error("Az iratpéldány nem postázható: " + pld_Id, execParam);
            //        throw new ResultException(52533);
            //    }

            //    #region Pld update

            //    Logger.Info("Postázás - Iratpéldány update", execParam);

            //    EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
            //    iratPeldany.Updated.SetValueAll(false);
            //    iratPeldany.Base.Updated.SetValueAll(false);

            //    iratPeldany.Base.Ver = pld_Ver;
            //    iratPeldany.Base.Updated.Ver = true;

            //    //Állapot Postázottra:
            //    iratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Postazott;
            //    iratPeldany.Updated.Allapot = true;

            //    // Postázás dátuma a kuldeménynél megadott lesz:
            //    iratPeldany.PostazasDatuma = erec_KuldKuldemenyek.BelyegzoDatuma;
            //    iratPeldany.Updated.PostazasDatuma = true;

            //    ExecParam execParam_PldUpdate = execParam.Clone();
            //    execParam_PldUpdate.Record_Id = pld_Id;

            //    Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, iratPeldany);
            //    if (!String.IsNullOrEmpty(result_PldUpdate.ErrorCode))
            //    {
            //        // hiba:
            //        throw new ResultException(result_PldUpdate);
            //    }

            //    #endregion

            //}

            foreach (System.Data.DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
            {
                string pld_Id = row["Id"].ToString();
                string pld_Ver = row["Ver"].ToString();
                string pld_Allapot = row["Allapot"].ToString();

                // Állapot ellenõrzés: (csak expediált postázható)
                if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Expedialt)
                {
                    // Nem postázható
                    Logger.Error("Az iratpéldány nem postázható: " + pld_Id, execParam);
                    throw new ResultException(52533);
                }

                pldIds.Add(pld_Id);
                pldVers.Add(pld_Ver);
            }

            #region Pld update tömeges

            if (pldIds.Count > 0)
            {
                Logger.Info("Postázás - Iratpéldány update", execParam);

                EREC_PldIratPeldanyok iratPeldany = new EREC_PldIratPeldanyok();
                iratPeldany.Updated.SetValueAll(false);
                iratPeldany.Base.Updated.SetValueAll(false);

                //Állapot Postázottra:
                iratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Postazott;
                iratPeldany.Updated.Allapot = true;

                // Postázás dátuma a kuldeménynél megadott lesz:
                iratPeldany.PostazasDatuma = erec_KuldKuldemenyek.BelyegzoDatuma;
                iratPeldany.Updated.PostazasDatuma = true;

                ExecParam execParam_PldUpdate = execParam.Clone();

                Result result_PldUpdate = service_Pld.UpdateTomeges(execParam_PldUpdate, pldIds, pldVers, iratPeldany, DateTime.Now);
                if (!String.IsNullOrEmpty(result_PldUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_PldUpdate);
                }
            }
            #endregion Pld update tömeges

            #endregion

            #region BLG_1354
            Sakkora.StopElteltIdoPostazasSoranByIratPlds(execParam.Clone(), pldIds);
            #endregion BLG_1354


            #region Tértivevény kezelés (insert)

            // ha a küldés módja tértivevényes:
            //if (erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes)
            {
                Logger.Info("Tértivevények létrehozása/módosítása Start");

                EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);

                EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                tertiveveny.Updated.SetValueAll(false);
                tertiveveny.Base.Updated.SetValueAll(false);
                ExecParam execParam_tertiveveny = execParam.Clone();

                List<string> lstBarCodes = new List<string>(lstKimenoKuldemenyIds.Count);
                List<string> lstRagszamok = new List<string>(lstKimenoKuldemenyIds.Count);

                foreach (string kuldemeny_Id in lstKimenoKuldemenyIds)
                {
                    // ragszám és vonalkód megkeresése - küldemény id alapján
                    DataRow[] rows_kuldemeny = result_kuldGetAll.Ds.Tables[0].Select(String.Format("Id='{0}'", kuldemeny_Id));
                    string kuldemeny_vonalkod = null;
                    string tertivevenyVonalkod = null;
                    string ragszam = null;
                    if (rows_kuldemeny.Length > 0)
                    {
                        kuldemeny_vonalkod = rows_kuldemeny[0]["BarCode"].ToString();
                        int index = Array.IndexOf(VonalkodArray, kuldemeny_vonalkod);
                        if (index >= 0)
                        {
                            ragszam = RagszamArray[index];
                            tertivevenyVonalkod = TertivevenyVonalkodArray[index];
                        }
                        else
                        {
                            Logger.Warn(String.Format("Tömeges postázás: Nem található a vonalkód: {0} - Küldemény id: {1}", kuldemeny_vonalkod ?? "<null>", kuldemeny_Id));
                        }
                    }
                    else
                    {
                        Logger.Warn(String.Format("Tömeges postázás: Nem található a küldemény: {0}", kuldemeny_Id));
                    }

                    lstBarCodes.Add(tertivevenyVonalkod);
                    lstRagszamok.Add(ragszam);

                    //// INSERT
                    //tertiveveny.Kuldemeny_Id = kuldemeny_Id;
                    //tertiveveny.Updated.Kuldemeny_Id = true;

                    //tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
                    //tertiveveny.Updated.Allapot = true;

                    //tertiveveny.BarCode = tertivevenyVonalkod;
                    //tertiveveny.Updated.BarCode = true;

                    //tertiveveny.Ragszam = ragszam;
                    //tertiveveny.Updated.Ragszam = true;

                    //Result result_tertInsert = service_tertiveveny.Insert(execParam_tertiveveny, tertiveveny);
                    //if (result_tertInsert.IsError)
                    //{
                    //    if (!String.IsNullOrEmpty(kuldemeny_vonalkod))
                    //    {
                    //        vonalkodokHibasTetelek.Add(kuldemeny_vonalkod);
                    //    }
                    //    // hiba:
                    //    throw new ResultException(result_tertInsert);
                    //}

                    //// Tértivevény típusú melléklet létrehozása a kimenõ küldeményhez
                    //Result result_mellekletekInsert = this.AddTertivevenyMellekletToKimenoKuldemeny(execParam.Clone(), tertiveveny);
                    //if (result_mellekletekInsert.IsError)
                    //{
                    //    if (!String.IsNullOrEmpty(kuldemeny_vonalkod))
                    //    {
                    //        vonalkodokHibasTetelek.Add(kuldemeny_vonalkod);
                    //    }
                    //    // hiba:
                    //    throw new ResultException(result_mellekletekInsert);
                    //}
                }

                // TÖMEGES INSERT
                tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
                tertiveveny.Updated.Allapot = true;

                Result result_tertInsert = service_tertiveveny.InsertTomeges(execParam_tertiveveny
                    , lstKimenoKuldemenyIds, lstBarCodes, lstRagszamok
                    , tertiveveny, DateTime.Now);

                if (result_tertInsert.IsError)
                {
                    if (result_tertInsert.Ds != null && result_tertInsert.Ds.Tables.Count > 0)
                    {
                        if (result_tertInsert.Ds.Tables[0].Columns.Contains("BarCode"))
                        {
                            foreach (DataRow row in result_tertInsert.Ds.Tables[0].Rows)
                            {
                                vonalkodokHibasTetelek.Add(row["BarCode"].ToString());
                            }
                        }
                    }
                    // hiba:
                    throw new ResultException(result_tertInsert);
                }

                // Tértivevény típusú melléklet létrehozása a kimenõ küldeményhez
                Result result_mellekletekInsert = this.AddTertivevenyMellekletToKimenoKuldemeny_Tomeges(execParam.Clone()
                    , lstKimenoKuldemenyIds, lstBarCodes);
                if (result_mellekletekInsert.IsError)
                {
                    //if (!String.IsNullOrEmpty(kuldemeny_vonalkod))
                    //{
                    //    vonalkodokHibasTetelek.Add(kuldemeny_vonalkod);
                    //}
                    //// hiba:
                    throw new ResultException(result_mellekletekInsert);
                }


            }

            #endregion Tértivevény kezelés (insert)


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, Search.GetSqlInnerString(lstKimenoKuldemenyIds.ToArray()), "Postazas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_KuldKuldemenyek::Postazas_Tomeges: ", execParam, eventLogResult);

            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            if (vonalkodokHibasTetelek != null)
            {
                result.Record = vonalkodokHibasTetelek.ToArray();
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemény létrehozása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public Result KimenoKuldemenyFelvitel(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string tertivevenyVonalkod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény felvitele Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Küldésmód meghatározása:

            string kuldesMod = GetKuldesModFromKimenoKuldemeny(erec_KuldKuldemenyek);
            bool isTertivevenyes = IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, false);
            bool isKulfoldi = IsKimenoKuldemenyKulfoldi(erec_KuldKuldemenyek);

            #endregion

            #region Tértivevény vonalkód/ajánlott ragszám ellenõrzés
            //if (erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes)
            {
                if (String.IsNullOrEmpty(tertivevenyVonalkod))
                {
                    // CR 3088 :  BOPMH - nál, CSBO - nál nem kötelezõ a tértivevény kitöltése(csak FPH - nál)
                    // ORG függõ megjelenítés
                    // BLG_591
                    //if ((!isKulfoldi) && (FelhasznaloProfil.OrgKod(Session) == Contentum.eUtility.Constants.OrgKod.FPH))
                    if ((!isKulfoldi) && (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TERTIVONALKOD_KELL, false)))


                    //   if (!isKulfoldi)
                    {
                        //"Hiba a kimenõ küldemény felvitelnél: a tértivevényes küldemény tértivevény vonalkódja nincs megadva!"
                        throw new ResultException(52512);
                    }
                }
                else if (erec_KuldKuldemenyek.Updated.RagSzam == false || String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    //"Hiba a kimenõ küldemény felvitelnél: a tértivevényes küldemény ragszáma nincs megadva!"
                    throw new ResultException(52513);
                }
            }
            else if (erec_KuldKuldemenyek.Ajanlott == "1")
            {
                if (erec_KuldKuldemenyek.Updated.RagSzam == false || String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    //"Hiba a kimenõ küldemény felvitelnél: az ajánlott küldemény ragszáma nincs megadva!"
                    throw new ResultException(52514);
                }
            }
            #endregion Tértivevény vonalkód/ajánlott ragszám ellenõrzés

            // Kimenõ küldemény
            erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
            erec_KuldKuldemenyek.Updated.PostazasIranya = true;

            // Állapot: Postázott
            erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Postazott;
            erec_KuldKuldemenyek.Updated.Allapot = true;

            erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando;
            erec_KuldKuldemenyek.Updated.IktatniKell = true;

            erec_KuldKuldemenyek.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

            erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Orzo = true;

            #region Küldésmód meghatározása:

            erec_KuldKuldemenyek.KuldesMod = kuldesMod;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;

            #endregion

            // kötelezõ kitölteni...
            erec_KuldKuldemenyek.BeerkezesIdeje = DateTime.Now.ToString();
            erec_KuldKuldemenyek.Updated.BeerkezesIdeje = true;

            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            result = this.Insert(execParam, erec_KuldKuldemenyek);
            result.CheckError();

            erec_KuldKuldemenyek.Id = result.Uid;

            #region Tértivevény vonalkód bejegyzés, ha kell
            // ha a küldés módja tértivevényes:
            //if (erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes)
            {
                Logger.Info("Tértivevény létrehozása Start");

                EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);
                ExecParam execParam_tertiveveny = execParam.Clone();

                EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                tertiveveny.Updated.SetValueAll(false);
                tertiveveny.Base.Updated.SetValueAll(false);

                tertiveveny.Kuldemeny_Id = result.Uid;
                tertiveveny.Updated.Kuldemeny_Id = true;

                tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
                tertiveveny.Updated.Allapot = true;

                tertiveveny.BarCode = tertivevenyVonalkod;
                tertiveveny.Updated.BarCode = true;

                tertiveveny.Ragszam = erec_KuldKuldemenyek.RagSzam;
                tertiveveny.Updated.Ragszam = true;

                Result result_tertInsert = service_tertiveveny.Insert(execParam_tertiveveny, tertiveveny);
                result_tertInsert.CheckError();

                Result result_mellekletekInsert = this.AddTertivevenyMellekletToKimenoKuldemeny(execParam.Clone(), tertiveveny);
                result_mellekletekInsert.CheckError();
            }

            #endregion Tértivevény vonalkód bejegyzés, ha kell

            #region Partner cím
            Result result_bindCim = BindPartnerCim(execParam.Clone(), erec_KuldKuldemenyek);

            if (result_bindCim.IsError)
            {
                throw new ResultException(result_bindCim);
            }
            #endregion Partner cím

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek.Id, "EREC_KuldKuldemenyek", "KimenoKuldemenyFelvitel").Record;
                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Kimenõ küldemény létrehozása az Iratpéldányok expediálásával
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek"></param>
    /// <param name="tertivevenyVonalkod"></param>
    /// <param name="iratPld_Array"> Irtpéldányok azonosítói</param>
    /// <returns></returns>
    [WebMethod()]
    public Result KimenoKuldemenyFelvitelWithExpedialas(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string tertivevenyVonalkod, String[] iratPld_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény felvitele Expediálással Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        string kuldemenyId = null;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            bool isExpedialt = false;
            // van(nak) hozzá iratpéldány(ok), akkor expediálás
            if ((iratPld_Array != null) && (iratPld_Array.Length > 0))
            {
                EREC_PldIratPeldanyokService servicePld = new EREC_PldIratPeldanyokService(this.dataContext);

                ExecParam xpm = execParam.Clone();
                xpm.Record_Id = iratPld_Array[0];

                //Result res = servicePld.Get(xpm);

                //if (res.IsError)
                //{
                //    Logger.Error("Iratpeldany lekerese hiba", xpm, res);
                //    throw new ResultException(res.ErrorCode);
                //    //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                //    //return
                //}

                //EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)res.Record;
                EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
                erec_PldIratPeldanyok.Updated.SetValueAll(false);
                erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

                erec_PldIratPeldanyok.KuldesMod = GetKuldesModFromKimenoKuldemeny(erec_KuldKuldemenyek);
                erec_PldIratPeldanyok.Updated.KuldesMod = true;

                erec_PldIratPeldanyok.Partner_Id_Cimzett = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
                erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = true;

                erec_PldIratPeldanyok.NevSTR_Cimzett = erec_KuldKuldemenyek.NevSTR_Bekuldo;
                erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

                erec_PldIratPeldanyok.Cim_id_Cimzett = erec_KuldKuldemenyek.Cim_Id;
                erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = true;

                erec_PldIratPeldanyok.CimSTR_Cimzett = erec_KuldKuldemenyek.CimSTR_Bekuldo;
                erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;

                //erec_PldIratPeldanyok.Base.Ver = FormHeader1.Record_Ver;
                //erec_PldIratPeldanyok.Base.Updated.Ver = true;

                Result resultExp;
                //TODO dokumentumok
                resultExp = servicePld.Expedialas(xpm, iratPld_Array, erec_KuldKuldemenyek.BarCode, erec_PldIratPeldanyok, null);
                resultExp.CheckError();

                //     erec_KuldKuldemenyek.Id = kuldemenyId;
                isExpedialt = true;
                kuldemenyId = resultExp.Uid;
            }

            #region Küldésmód meghatározása:

            string kuldesMod = GetKuldesModFromKimenoKuldemeny(erec_KuldKuldemenyek);
            bool isTertivevenyes = IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, false);
            bool isKulfoldi = IsKimenoKuldemenyKulfoldi(erec_KuldKuldemenyek);

            #endregion

            #region Tértivevény vonalkód/ajánlott ragszám ellenõrzés
            //if (erec_KuldKuldemenyek.Tertiveveny == "1")
            if (isTertivevenyes)
            {
                if (String.IsNullOrEmpty(tertivevenyVonalkod))
                {
                    if (!isKulfoldi)
                    {
                        // BLG_591
                        //string orgKod = GetOrgKod(execParam);
                        //if (orgKod == Contentum.eUtility.Constants.OrgKod.FPH)
                        if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TERTIVONALKOD_KELL, false))
                        {
                            //"Hiba a kimenõ küldemény felvitelnél: a tértivevényes küldemény tértivevény vonalkódja nincs megadva!"
                            throw new ResultException(52512);
                        }
                    }
                }
                else if (erec_KuldKuldemenyek.Updated.RagSzam == false || String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    //"Hiba a kimenõ küldemény felvitelnél: a tértivevényes küldemény ragszáma nincs megadva!"
                    throw new ResultException(52513);
                }
            }
            else if (erec_KuldKuldemenyek.Ajanlott == "1")
            {
                if (erec_KuldKuldemenyek.Updated.RagSzam == false || String.IsNullOrEmpty(erec_KuldKuldemenyek.RagSzam))
                {
                    //"Hiba a kimenõ küldemény felvitelnél: az ajánlott küldemény ragszáma nincs megadva!"
                    throw new ResultException(52514);
                }
            }
            #endregion Tértivevény vonalkód/ajánlott ragszám ellenõrzés

            // Kimenõ küldemény
            erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
            erec_KuldKuldemenyek.Updated.PostazasIranya = true;

            // Állapot: Postázott
            erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Postazott;
            erec_KuldKuldemenyek.Updated.Allapot = true;

            erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando;
            erec_KuldKuldemenyek.Updated.IktatniKell = true;

            erec_KuldKuldemenyek.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

            erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Orzo = true;

            #region Küldésmód meghatározása:

            erec_KuldKuldemenyek.KuldesMod = kuldesMod;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;

            #endregion

            // kötelezõ kitölteni...
            erec_KuldKuldemenyek.BeerkezesIdeje = DateTime.Now.ToString();
            erec_KuldKuldemenyek.Updated.BeerkezesIdeje = true;

            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            if (isExpedialt)
            {
                erec_KuldKuldemenyek.Id = kuldemenyId;

                result = this.Postazas(execParam, erec_KuldKuldemenyek, tertivevenyVonalkod);
                result.CheckError();
            }
            else
            {
                result = this.Insert(execParam, erec_KuldKuldemenyek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    #region Tértivevény vonalkód bejegyzés, ha kell
                    // ha a küldés módja tértivevényes:
                    //if (erec_KuldKuldemenyek.Tertiveveny == "1")
                    if (isTertivevenyes)
                    {
                        Logger.Info("Tértivevény létrehozása Start");

                        EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);
                        ExecParam execParam_tertiveveny = execParam.Clone();

                        EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                        tertiveveny.Updated.SetValueAll(false);
                        tertiveveny.Base.Updated.SetValueAll(false);

                        tertiveveny.Kuldemeny_Id = result.Uid;
                        tertiveveny.Updated.Kuldemeny_Id = true;

                        tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
                        tertiveveny.Updated.Allapot = true;

                        tertiveveny.BarCode = tertivevenyVonalkod;
                        tertiveveny.Updated.BarCode = true;

                        tertiveveny.Ragszam = erec_KuldKuldemenyek.RagSzam;
                        tertiveveny.Updated.Ragszam = true;

                        Result result_tertInsert = service_tertiveveny.Insert(execParam_tertiveveny, tertiveveny);
                        result_tertInsert.CheckError();

                        Result result_mellekletekInsert = this.AddTertivevenyMellekletToKimenoKuldemeny(execParam.Clone(), tertiveveny);
                        result_mellekletekInsert.CheckError();
                    }

                    #endregion Tértivevény vonalkód bejegyzés, ha kell
                }
            }

            result.CheckError();

            #region Partner cím
            Result result_bindCim = BindPartnerCim(execParam.Clone(), erec_KuldKuldemenyek);
            result_bindCim.CheckError();
            #endregion Partner cím

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek.Id, "EREC_KuldKuldemenyek", "KimenoKuldemenyFelvitel").Record;
                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemény módosítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KimenoKuldemenyModositas(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string tertivevenyVonalkod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény módosítása Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // eredeti küldemény lekérése, ha szükséges
            #region Küldemény Get
            EREC_KuldKuldemenyek erec_KuldKuldemenyekOriginal = null;

            // ha változott a tértivevény beállítás vagy a ragszám:
            if (erec_KuldKuldemenyek.Updated.Tertiveveny || erec_KuldKuldemenyek.Updated.KimenoKuldemenyFajta
                || erec_KuldKuldemenyek.Updated.RagSzam)
            {
                ExecParam execParam_kuldemenyGet = execParam.Clone();
                Result result_Get = this.Get(execParam_kuldemenyGet);
                result_Get.CheckError();

                erec_KuldKuldemenyekOriginal = (EREC_KuldKuldemenyek)result_Get.Record;
            }
            #endregion Küldemény Get

            #region Küldésmód meghatározása:

            string kuldesMod = GetKuldesModFromKimenoKuldemeny(erec_KuldKuldemenyek);
            bool isTertivevenyes = IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyek, true);
            bool isKulfoldi = IsKimenoKuldemenyKulfoldi(erec_KuldKuldemenyek);

            #endregion

            //// Kimenõ küldemény
            //erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
            //erec_KuldKuldemenyek.Updated.PostazasIranya = true;

            //// Állapot: Postázott
            //erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Postazott;
            //erec_KuldKuldemenyek.Updated.Allapot = true;

            //erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando;
            //erec_KuldKuldemenyek.Updated.IktatniKell = true;

            //erec_KuldKuldemenyek.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            //erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

            //erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            //erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Orzo = true;

            #region Küldésmód meghatározása:

            erec_KuldKuldemenyek.KuldesMod = kuldesMod;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;

            #endregion

            // kötelezõ kitölteni...
            erec_KuldKuldemenyek.BeerkezesIdeje = DateTime.Now.ToString();
            erec_KuldKuldemenyek.Updated.BeerkezesIdeje = true;

            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            result = this.Update(execParam, erec_KuldKuldemenyek);
            result.CheckError();

            #region Tértivevény módosítás vagy érvénytelenítés, ha szükséges
            // ha változott a tértivevény beállítás vagy a ragszám:
            bool isChangedToTertivevenyes = false;
            bool isChangedFromTertivevenyes = false;
            bool isChangedRagszam = false;
            bool isChangedTertivevenyVonalkod = false;
            if (erec_KuldKuldemenyekOriginal != null)
            {
                bool isTertivevenyesOriginal = IsKimenoKuldemenyTertivevenyes(erec_KuldKuldemenyekOriginal, false);

                //if (erec_KuldKuldemenyek.Updated.Tertiveveny
                //    && erec_KuldKuldemenyek.Tertiveveny != erec_KuldKuldemenyekOriginal.Tertiveveny)
                if (isTertivevenyes != isTertivevenyesOriginal)
                {
                    //if (erec_KuldKuldemenyekOriginal.Tertiveveny == "1")
                    if (isTertivevenyesOriginal)
                    {
                        isChangedFromTertivevenyes = true;
                    }
                    //else if (erec_KuldKuldemenyek.Tertiveveny == "1")
                    else if (isTertivevenyes)
                    {
                        isChangedToTertivevenyes = true;
                    }
                }
                else
                {
                    //if (erec_KuldKuldemenyek.Updated.RagSzam
                    //&& erec_KuldKuldemenyekOriginal.Tertiveveny == "1"
                    //&& erec_KuldKuldemenyek.RagSzam != erec_KuldKuldemenyekOriginal.RagSzam)
                    if (erec_KuldKuldemenyek.Updated.RagSzam
                    && isTertivevenyesOriginal
                    && erec_KuldKuldemenyek.RagSzam != erec_KuldKuldemenyekOriginal.RagSzam)
                    {
                        isChangedRagszam = true;
                    }
                    if (tertivevenyVonalkod != null)
                    {
                        isChangedTertivevenyVonalkod = true;
                    }
                }
            }
            if (isChangedFromTertivevenyes || isChangedRagszam || isChangedTertivevenyVonalkod)
            {
                Logger.Info("Tértivevény lekérése Start");

                #region Tértivevények létezésének ellenõrzése
                EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);

                ExecParam execParam_tertiveveny = execParam.Clone();

                EREC_KuldTertivevenyekSearch search_tertiveveny = new EREC_KuldTertivevenyekSearch();
                search_tertiveveny.Kuldemeny_Id.Filter(execParam.Record_Id);

                Result result_tertivevenyGetAll = service_tertiveveny.GetAll(execParam_tertiveveny, search_tertiveveny);
                result_tertivevenyGetAll.CheckError();
                #endregion Tértivevények létezésének ellenõrzése

                if (isChangedFromTertivevenyes) // érvénytelenítés
                {
                    foreach (DataRow row in result_tertivevenyGetAll.Ds.Tables[0].Rows)
                    {
                        EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                        Utility.LoadBusinessDocumentFromDataRow(tertiveveny, row);

                        execParam_tertiveveny.Record_Id = tertiveveny.Id;//row["Id"].ToString();

                        Result result_tertivevenyInvalidate = service_tertiveveny.Invalidate(execParam_tertiveveny);
                        result_tertivevenyInvalidate.CheckError();

                        // Tértivevény típusú melléklet érvénytelenítése
                        Result result_mellekletekInvalidate = this.RemoveTertivevenyMellekletFromKimenoKuldemeny(execParam.Clone(), tertiveveny);
                        result_mellekletekInvalidate.CheckError();
                    }
                }
                else if (isChangedRagszam || isChangedTertivevenyVonalkod) // update
                {
                    foreach (DataRow row in result_tertivevenyGetAll.Ds.Tables[0].Rows)
                    {

                        Logger.Info("Tértivevény módosítása Start");

                        EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                        Utility.LoadBusinessDocumentFromDataRow(tertiveveny, row);

                        execParam_tertiveveny.Record_Id = tertiveveny.Id;//row["Id"].ToString();

                        //EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                        tertiveveny.Updated.SetValueAll(false);
                        tertiveveny.Base.Updated.SetValueAll(false);

                        //if (isChangedRagszam)
                        //{
                        tertiveveny.Ragszam = erec_KuldKuldemenyek.RagSzam;
                        tertiveveny.Updated.Ragszam = true;
                        //}

                        //if (isChangedTertivevenyVonalkod)
                        //{
                        tertiveveny.BarCode = tertivevenyVonalkod;
                        tertiveveny.Updated.BarCode = true;
                        //}

                        tertiveveny.Base.Ver = row["Ver"].ToString();
                        tertiveveny.Base.Updated.Ver = true;

                        Result result_tertUpdate = service_tertiveveny.Update(execParam_tertiveveny, tertiveveny);
                        result_tertUpdate.CheckError();

                        // tertivevenyVonalkod changed...
                        // Tértivevény típusú melléklet érvénytelenítése
                        Result result_mellekletekUpdate = this.UpdateTertivevenyMellekletBarCode(execParam.Clone(), tertiveveny);
                        result_mellekletekUpdate.CheckError();
                    }
                }
            }
            else if (isChangedToTertivevenyes) // létrehozás
            {
                Logger.Info("Tértivevény létrehozása Start");

                EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);
                ExecParam execParam_tertiveveny = execParam.Clone();

                EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
                tertiveveny.Updated.SetValueAll(false);
                tertiveveny.Base.Updated.SetValueAll(false);

                tertiveveny.Kuldemeny_Id = execParam.Record_Id;
                tertiveveny.Updated.Kuldemeny_Id = true;

                tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
                tertiveveny.Updated.Allapot = true;

                tertiveveny.Ragszam = erec_KuldKuldemenyek.RagSzam;
                tertiveveny.Updated.Ragszam = true;

                tertiveveny.BarCode = tertivevenyVonalkod;
                tertiveveny.Updated.BarCode = true;

                Result result_tertInsert = service_tertiveveny.Insert(execParam_tertiveveny, tertiveveny);
                result_tertInsert.CheckError();

                // Tértivevény típusú melléklet létrehozása kimenõ küldeményhez
                Result result_mellekletekInsert = this.AddTertivevenyMellekletToKimenoKuldemeny(execParam.Clone(), tertiveveny);
                result_mellekletekInsert.CheckError();
            }

            #endregion Tértivevény módosítás vagy érvénytelenítés, ha szükséges

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek.Id, "EREC_KuldKuldemenyek", "KuldemenyModify").Record;
                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private bool IsKuldemenyInFizikaiDosszie(ExecParam execParam, string Kuldemeny_Id)
    {
        KRT_MappaTartalmakService service = new KRT_MappaTartalmakService(this.dataContext);
        KRT_MappaTartalmakSearch search = new KRT_MappaTartalmakSearch();

        search.Obj_Id.Filter(Kuldemeny_Id);

        Result result = service.GetAllWithExtension(execParam, search);

        if (result.IsError)
            return true;

        foreach (DataRow r in result.Ds.Tables[0].Rows)
        {
            if (r["Mappa_Tipus"].ToString() == KodTarak.MAPPA_TIPUS.Fizikai)
                return true;
        }

        return false;

    }

    private bool IsKimenoKuldemenyMoved(EREC_KuldKuldemenyek Record)
    {
        if (Record.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            if (Record.Updated.Csoport_Id_Felelos || Record.Updated.FelhasznaloCsoport_Id_Orzo)
            {
                return true;
            }
        }
        return false;
    }
    private Result KimenoKuldemenyek_Moved(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            Arguments args = new Arguments();
            args.Add(new Argument("Record_Id", execParam.Record_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.KimenoKuldemenyek_Moved(execParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result KimenoKuldemenyekFeladoJegyzek(ExecParam ExecParam, Guid? PostaKonyv_Id, DateTime? KezdDat, DateTime? VegeDat)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.KimenoKuldemenyekFeladoJegyzek(ExecParam, PostaKonyv_Id, KezdDat, VegeDat);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private bool IsKimenoKuldemenyKulfoldi(EREC_KuldKuldemenyek kuldemeny)
    {
        bool isKulfoldi = kuldemeny.KimenoKuldemenyFajta.StartsWith(Contentum.eUtility.Constants.OrszagViszonylatKod.Europai)
                    || kuldemeny.KimenoKuldemenyFajta.StartsWith(Contentum.eUtility.Constants.OrszagViszonylatKod.EgyebKulfoldi);

        return isKulfoldi;
    }

    #region POSTAI RAGSZAM KIOSZTAS
    [WebMethod(EnableSession = true)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result PostaiRagszamEllenorzes(ExecParam execParam, string mode, string postaKonyvId, string kuldesModja, string ragSzamSavId, string ragSzam)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Info("Postai ragszám kiosztás, és küldeményhez rendelés", execParam);

        Result resultMain = new Result();

        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            Result result1 = new Result();
            result1.ErrorMessage = MessageRagszamParametersIncorrect;
            log.WsEnd(execParam, result1);
            return result1;
        }

        #region RAGSZAM CHECK
        string outCheckErrorMessage = null;
        if (!CheckPostaiRagszamIgenyles(execParam, mode, postaKonyvId, kuldesModja, ragSzam, ragSzamSavId, out outCheckErrorMessage))
        {
            Result result = new Result();
            result.ErrorMessage = outCheckErrorMessage;
            return result;
        }
        return resultMain;
        #endregion
    }

    [WebMethod(EnableSession = true)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result PostaiRagszamKiosztas(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string mode, string postaKonyvId, string kuldesModja, string ragSzamSavId, string ragSzam)
    {
        //Mode = "T" - tömeges kiosztás - BUG2128 tömgeges ragszámosztásnál nem változtatunk küldésmódot, egyébként "A" - ág szerint mûködik
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Info("Postai ragszám kiosztás, és küldeményhez rendelés", execParam);

        Result resultMain = null;

        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || erec_KuldKuldemenyek == null || String.IsNullOrEmpty(erec_KuldKuldemenyek.Id))
        {
            Result result1 = ResultError.CreateNewResultWithErrorCode(52530);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region RAGSZAM CHECK
            string outCheckErrorMessage = null;
            if (!CheckPostaiRagszamIgenyles(execParam, mode, postaKonyvId, kuldesModja, ragSzam, ragSzamSavId, out outCheckErrorMessage))
            {
                Result result1 = new Result();
                result.ErrorMessage = outCheckErrorMessage;

                return result;
            }
            #endregion

            #region RAGSZAM FOGLALAS
            Result res = null;
            switch (mode)
            {
                case "M":
                    float? ragszamNum = GetRagszamNumber(ragSzam);
                    res = RagszamFoglalasManual(execParam, ragSzam, ragszamNum.Value);
                    break;
                case "T":
                case "A":
                    res = RagszamFoglalasAuto(execParam, ragSzamSavId, postaKonyvId, kuldesModja);
                    break;
                default:
                    break;
            }

            #endregion

            if (!String.IsNullOrEmpty(res.ErrorCode))
                throw new ResultException(res);

            Result resultRagszamFind = FindRagszamById(execParam, res.Uid);
            if (!String.IsNullOrEmpty(resultRagszamFind.ErrorCode))
                throw new ResultException(resultRagszamFind);

            KRT_RagSzamok ragszamFound = (KRT_RagSzamok)resultRagszamFind.Record;
            ragSzam = ragszamFound.Kod;

            ragszamFound.Updated.SetValueAll(false);
            ragszamFound.Base.Updated.SetValueAll(false);

            ragszamFound.Obj_Id = erec_KuldKuldemenyek.Id;
            ragszamFound.Updated.Obj_Id = true;
            ragszamFound.Obj_type = "EREC_KuldKuldemenyek";
            ragszamFound.Updated.Obj_type = true;
            ragszamFound.Base.Updated.Ver = true;

            KRT_RagSzamokService service = new KRT_RagSzamokService(this.dataContext);
            ExecParam execPm = execParam.Clone();
            execPm.Record_Id = ragszamFound.Id;
            Result updateResult = service.Update(execPm, ragszamFound);

            if (!String.IsNullOrEmpty(updateResult.ErrorCode))
                throw new ResultException(updateResult);


            //#region GENERATE BARCODE 4 KULD
            //string kuldBarCode = null;
            //KRT_Barkodok item_KuldBarCode = null;
            //Result result_KuldBarCode = GenerateAndSaveNewBarcode(execParam, out kuldBarCode, out item_KuldBarCode);
            //if (result_KuldBarCode.IsError)
            //{
            //    log.WsEnd(execParam, result_KuldBarCode);
            //    throw new ResultException(result_KuldBarCode);
            //}
            //#endregion

            #region GENERATE BARCODE 4 TERTI
            string tertiBarCode = null;
            KRT_Barkodok item_tertiBarCode = null;
            Result result_TertiBarCode = GenerateAndSaveNewBarcode(execParam, out tertiBarCode, out item_tertiBarCode);
            if (result_TertiBarCode.IsError)
            {
                log.WsEnd(execParam, result_TertiBarCode);
                throw new ResultException(result_TertiBarCode);
            }
            #endregion

            #region GET BARCODE 4 TERTI
            KRT_Barkodok item_barcodeGet = null;
            Result result_BarcodeGet = GetBarcode(execParam, item_tertiBarCode.Id, out item_barcodeGet);
            item_barcodeGet = (KRT_Barkodok)result_BarcodeGet.Record;
            #endregion

            #region KULDTERTI LETREHOZAS
            EREC_KuldKuldemenyek erec_KuldKuldemeny = erec_KuldKuldemenyek;
            ExecParam execParamMod = execParam.Clone();
            execParamMod.Record_Id = erec_KuldKuldemenyek.Id;

            if (!"T".Equals(mode))
            {
                erec_KuldKuldemeny.Tertiveveny = "1";
                erec_KuldKuldemeny.Updated.Tertiveveny = true;
            }

            erec_KuldKuldemeny.RagSzam = ragSzam;
            erec_KuldKuldemeny.Updated.RagSzam = true;

            if (string.IsNullOrEmpty(erec_KuldKuldemeny.IraIktatokonyv_Id))
            {
                erec_KuldKuldemeny.IraIktatokonyv_Id = postaKonyvId;
                erec_KuldKuldemeny.Updated.IraIktatokonyv_Id = true;
            }

            //erec_KuldKuldemeny.BarCode = kuldBarCode;
            //erec_KuldKuldemeny.Updated.BarCode = true;
            Result resu = null;
            if ("T".Equals(mode))
            {
                resu = KimenoKuldemenyModositasTomeges(execParamMod, erec_KuldKuldemeny, item_barcodeGet.Kod);
            }
            else
            {
                resu = KimenoKuldemenyModositas(execParamMod, erec_KuldKuldemeny, item_barcodeGet.Kod);
            }
            if (resu != null && resu.IsError)
            {
                log.WsEnd(execParam, resu);
                throw new ResultException(resu);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            resultMain = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        return resultMain;
    }

    //BUG2128 - tömeges ragszám generáláskor más módon mûködjön a küldemény módosítása
    private Result KimenoKuldemenyModositasTomeges(ExecParam execParam, EREC_KuldKuldemenyek erec_KuldKuldemenyek, string tertivevenyVonalkod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény módosítása Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // eredeti küldemény lekérése, ha szükséges
            #region Küldemény Get
            EREC_KuldKuldemenyek erec_KuldKuldemenyekOriginal = null;

            // ha változott a ragszám:
            if (erec_KuldKuldemenyek.Updated.RagSzam)
            {
                ExecParam execParam_kuldemenyGet = execParam.Clone();
                Result result_Get = this.Get(execParam_kuldemenyGet);

                if (result_Get.IsError)
                {
                    throw new ResultException(result_Get);
                }

                erec_KuldKuldemenyekOriginal = (EREC_KuldKuldemenyek)result_Get.Record;
            }
            #endregion Küldemény Get

            bool isKulfoldi = IsKimenoKuldemenyKulfoldi(erec_KuldKuldemenyek);

            // kötelezõ kitölteni...
            erec_KuldKuldemenyek.BeerkezesIdeje = DateTime.Now.ToString();
            erec_KuldKuldemenyek.Updated.BeerkezesIdeje = true;

            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            result = this.Update(execParam, erec_KuldKuldemenyek);

            if (result.IsError)
            {
                // hiba:
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_KuldKuldemenyek.Id, "EREC_KuldKuldemenyek", "KuldemenyModify").Record;
                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    #region RAGSZAM HELPERS

    #region CONSTANST

    private const string MessageRagszamSavNotFound = "Nem található megfelelõ RagszámSáv !";
    private const string MessageRagszamSavMoreThanOne = "Egynél több megfelelõ RagszámSáv található !";
    private const string MessageRagszamExist = "Nem található megfelelõ Ragszám !";
    private const string MessageRagszamEmpty = "Nem megfelelõ Ragszám !";
    private const string MessageRagszamIncorrect = "A megadott ragszám nem része a ragszámsávnak!";
    private const string MessageRagszamCheckOk = "Sikeres a ragszám ellenõrzés";
    private const string MessageRagszamParametersIncorrect = "Nem megfelelõ paraméterek!";
    private const string MessageRagszamValidationError = "Ragszám validációs hiba !";

    private const string MessageKuldTertivevenyExistingRagszamExist = "Már létezik ilyen küldemény tértivevény !";
    #endregion

    #region VALIDATORS
    private bool CheckPostaiRagszamIgenyles(ExecParam execParam, string mode, string postakonyv, string kuldesModja, string ragszam, string ragSzamSav, out string outErrorMessage)
    {
        outErrorMessage = null;
        if (mode == "M")
        {
            string outError = null;
            string outPostakonyv = null;
            string outKuldesModja = null;
            if (!CheckRagszamModeManual(execParam, ragszam, out outPostakonyv, out outKuldesModja, out outError))
            {
                outErrorMessage = outError;
                return false;
            }
        }
        else if (mode == "A")
        {
            string outError = null;
            if (!CheckRagszamModeAuto(execParam, postakonyv, kuldesModja, out outError))
            {
                outErrorMessage = outError;
                return false;
            }
        }

        return true;
    }
    private bool CheckRagszamModeAuto(ExecParam execParam, string postakonyv, string kuldesModja, out string outError)
    {
        outError = null;
        string errorString = null;
        string ragSzamSavId = null;
        DataRow ragszamSavData = null;
        bool ret = CheckRagszamSavInService(execParam, postakonyv, kuldesModja, out errorString, out ragSzamSavId, out ragszamSavData);

        if (!ret)
        {
            outError = errorString;
            return false;
        }

        return true;
    }
    private bool CheckRagszamModeManual(ExecParam execParam, string ragszamValue, out string postakonyv, out string kuldesModja, out string outError)
    {
        outError = null;
        postakonyv = null;
        kuldesModja = null;
        if (string.IsNullOrEmpty(ragszamValue))
        {
            outError = MessageRagszamEmpty;
            return false;
        }

        string errorStringRagszamExist = null;
        if (!CheckmanualRagszamExistingInService(execParam, ragszamValue, out errorStringRagszamExist))
        {
            outError = errorStringRagszamExist;
            return false;
        }

        return true;
    }
    private bool CheckRagszamSavInService(ExecParam execParam, string postakonyv, string kuldesModja, out string errorString, out string outRagSzamSavId, out DataRow outRagSzamSav)
    {
        outRagSzamSavId = null;
        errorString = null;
        outRagSzamSav = null;
        Result result = GetAllRagszamSavFilteredByPkKf(execParam, postakonyv, kuldesModja);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds == null || result.Ds.Tables.Count < 1 || result.Ds.Tables[0].Rows.Count < 1)
        {
            errorString = MessageRagszamSavNotFound;
            return false;
        }

        //if (result.Ds.Tables[0].Rows.Count > 1)
        //{
        //    errorString = MessageRagszamSavMoreThanOne;
        //    return false;
        //}

        outRagSzamSavId = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        outRagSzamSav = result.Ds.Tables[0].Rows[0];
        return true;
    }
    private bool CheckRagszamExistingInService(ExecParam execParam, string ragszam, string postakonyv, string ragSzamSavId, out string errorString)
    {
        float? ragszamNum = GetRagszamNumber(ragszam);
        errorString = null;
        Result result = FindRagszamByCode(execParam, ragszam, ragszamNum.ToString(), postakonyv, ragSzamSavId);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count >= 1)
        {
            errorString = MessageRagszamExist;
            return false;
        }
        return true;
    }
    private bool CheckmanualRagszamExistingInService(ExecParam execParam, string ragszam, out string errorString)
    {
        float? ragszamNum = GetRagszamNumber(ragszam);
        errorString = null;
        Result result = FindRagszamByCode(execParam, ragszam, ragszamNum.ToString());
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count < 1)
        {
            errorString = "Nem található ragszám";
            return false;
        }
        return true;
    }

    private bool CheckKuldTertivevenyExisting(ExecParam execParam, string kuldemenyId, string ragSzam, out string errorString)
    {
        errorString = null;
        Result result = GetKuldTertiveveny(execParam, kuldemenyId, ragSzam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count >= 0)
        {
            errorString = MessageKuldTertivevenyExistingRagszamExist;
            return false;
        }
        return true;
    }
    #endregion

    #region SERVICE CALL

    /// <summary>
    /// Get all allokalt ragszamsav by postakonyv and kuldemeny fajta.
    /// </summary>
    /// <param name="postakonyv"></param>
    /// <param name="kuldesModja"></param>
    /// <returns></returns>
    private Result GetAllRagszamSavFilteredByPkKf(ExecParam execParam, string postakonyv, string kuldesModja)
    {
        KRT_RagszamSavokService service = new KRT_RagszamSavokService(this.dataContext);
        ExecParam execPm = execParam.Clone();
        KRT_RagszamSavokSearch search = new KRT_RagszamSavokSearch();
        search.Postakonyv_Id.Filter(postakonyv);

        search.SavType.Filter(kuldesModja);

        search.SavAllapot.Filter("H");

        return service.GetAll(execPm, search);
    }
    private Result GetKuldTertiveveny(ExecParam execParam, string kuldemenyId, string ragSzam)
    {
        EREC_KuldTertivevenyekService service = new EREC_KuldTertivevenyekService(this.dataContext);
        ExecParam execPm = execParam.Clone();
        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        search.Kuldemeny_Id.Filter(kuldemenyId);
        search.Ragszam.Filter(ragSzam);

        return service.GetAll(execPm, search);
    }
    private Result FindRagszamByCode(ExecParam execParam, string ragszam, string ragszamNum, string postakonyv, string ragszamSavId)
    {
        KRT_RagSzamokService service = new KRT_RagSzamokService(this.dataContext);
        ExecParam execPm = execParam.Clone();
        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Filter(ragszam);
        search.KodNum.Filter(ragszamNum);
        search.Postakonyv_Id.Filter(postakonyv);
        search.KodType.Filter("A");
        search.RagszamSav_Id.Filter(ragszamSavId);

        return service.GetAll(execPm, search);
    }
    private Result FindRagszamByCode(ExecParam execParam, string ragszam, string ragszamNum)
    {
        KRT_RagSzamokService service = new KRT_RagSzamokService(this.dataContext);
        ExecParam execPm = execParam.Clone();
        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Filter(ragszam);

        //search.KodNum.Value = ragszamNum;
        //search.KodNum.Operator = Query.Operators.equals;

        search.Allapot.Filter("H");

        return service.GetAll(execPm, search);
    }

    private Result FindRagszamById(ExecParam execParam, string id)
    {
        KRT_RagSzamokService service = new KRT_RagSzamokService(this.dataContext);
        ExecParam execPm = execParam.Clone();
        execPm.Record_Id = id;
        return service.Get(execPm);
    }

    private Result RagszamFoglalasAuto(ExecParam execParam, string ragSzamSavId, string postaKonyvId, string kuldesModja)
    {
        KRT_RagszamSavokService service = new KRT_RagszamSavokService();
        ExecParam execPm = execParam.Clone();
        execParam.Record_Id = ragSzamSavId;

        Result result = service.RagszamSav_Foglalas(
            execPm,
            new Guid(ragSzamSavId),
            new Guid(postaKonyvId),
            1,
            "H"
            );
        return result;
    }
    private Result RagszamFoglalasManual(ExecParam execParam, string ragSzam, float ragSzamNum)
    {
        KRT_RagszamSavokService service = new KRT_RagszamSavokService();
        ExecParam execPm = execParam.Clone();
        Result result = service.RagszamSav_FoglalasManual(
            execPm,
            ragSzam,
            ragSzamNum,
            null,
            null
            );
        return result;

    }

    private Result GenerateAndSaveNewBarcode(ExecParam execParam, out string barCode, out KRT_Barkodok barcodeFind)
    {
        barCode = null;
        KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
        ExecParam execParam_barkod = execParam.Clone();

        Result result_barkod_pld = service_barkodok.BarkodGeneralas(execParam_barkod);
        if (!String.IsNullOrEmpty(result_barkod_pld.ErrorCode))
        {
            Logger.Error("Vonalkód generálás az iratpéldánynak: Sikertelen", execParam);
            throw new ResultException(result_barkod_pld);
        }

        barCode = (String)result_barkod_pld.Record;

        KRT_Barkodok barcodeRecord = new KRT_Barkodok();
        barcodeRecord.Kod = barCode;
        barcodeRecord.Updated.Kod = true;

        barcodeRecord.KodType = "N";
        barcodeRecord.Updated.KodType = true;

        barcodeRecord.Allapot = "S";
        barcodeRecord.Updated.Allapot = true;

        Result resultBarCode = service_barkodok.Insert(execParam_barkod, barcodeRecord);
        if (!String.IsNullOrEmpty(resultBarCode.ErrorCode))
        {
            Logger.Error("Hiba: Barkod rekordhoz  Insert", execParam_barkod);
            throw new ResultException(resultBarCode);
        }

        ExecParam execParam_barkodFind = execParam.Clone();
        execParam_barkodFind.Record_Id = resultBarCode.Uid;
        Result resultBarcodeFind = service_barkodok.Get(execParam_barkodFind);
        if (!String.IsNullOrEmpty(resultBarcodeFind.ErrorCode))
        {
            Logger.Error("Hiba: Barkod rekordhoz  Get", execParam_barkodFind);
            throw new ResultException(resultBarcodeFind);
        }
        barcodeFind = (KRT_Barkodok)resultBarcodeFind.Record;

        return resultBarcodeFind;
    }
    private Result GetBarcode(ExecParam execParam, string id, out KRT_Barkodok barcodeFind)
    {
        barcodeFind = null;
        KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
        ExecParam execParam_barkod = execParam.Clone();
        execParam_barkod.Record_Id = id;

        Result result_barkod = service_barkodok.Get(execParam_barkod);
        if (!String.IsNullOrEmpty(result_barkod.ErrorCode))
        {
            Logger.Error("Vonalkód lekérdezs: Sikertelen", execParam);
            throw new ResultException(result_barkod);
        }
        barcodeFind = (KRT_Barkodok)result_barkod.Record;
        return result_barkod;
    }

    private Result FindKuldTertiveveny(ExecParam execParam, string kuldemenyId, string ragszam, string barcode)
    {
        EREC_KuldTertivevenyekService service = new EREC_KuldTertivevenyekService(this.dataContext);
        ExecParam execParam_tertiveveny = execParam.Clone();

        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        search.Kuldemeny_Id.Filter(kuldemenyId);
        search.Ragszam.Filter(ragszam);
        search.BarCode.FilterIfNotEmpty(barcode);

        return service.GetAll(execParam_tertiveveny, search);
    }
    #endregion

    #region HELPER
    public float? GetRagszamNumber(string value)
    {
        int index = int.MinValue;
        for (int i = 0; i < value.Length; i++)
        {
            byte val;
            if (byte.TryParse(value[i].ToString(), out val))
            {
                index = i;
                break;
            }
        }
        if (index != int.MinValue)
        {
            string numString = value.Substring(index);
            float ret = float.MinValue;
            if (float.TryParse(numString, out ret))
            {
                return ret;
            }
        }
        return null;
    }
    #endregion

    #endregion
    #endregion

    #region TOMEGES_POSTAZAS
    /// <summary>
    /// Kimenõ küldemény postázása tömegesen
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_KuldKuldemenyek"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public Result Postazas_Tomegesen(ExecParam execParam, EREC_KuldKuldemenyek postazasiAdatok, string[] erec_KuldKuldemenyIds, string tertivevenyVonalkod_notUsed)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény Tömeges Postázása Start", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || erec_KuldKuldemenyIds == null || erec_KuldKuldemenyIds.Length < 1)
        {
            // hiba a postázás során
            Result result1 = ResultError.CreateNewResultWithErrorCode(52530);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            ExecParam execParam_kuldGet = null;
            Result result_kuldGet = null;
            List<EREC_KuldKuldemenyek> kuldemenyekList = new List<EREC_KuldKuldemenyek>();
            EREC_KuldKuldemenyekService service_Kuld = new EREC_KuldKuldemenyekService(this.dataContext);
            string kuldesMod = null;
            for (int i = 0; i < erec_KuldKuldemenyIds.Length; i++)
            {
                execParam_kuldGet = execParam.Clone();
                execParam_kuldGet.Record_Id = erec_KuldKuldemenyIds[i];

                result_kuldGet = service_Kuld.Get(execParam_kuldGet);
                result_kuldGet.CheckError();

                EREC_KuldKuldemenyek kuld = (EREC_KuldKuldemenyek)result_kuldGet.Record;
                kuldemenyekList.Add(kuld);

                if (string.IsNullOrEmpty(kuldesMod))
                    kuldesMod = kuld.KuldesMod;
                else if (kuldesMod != kuld.KuldesMod)
                    throw new ResultException(80412);
            }
            Result resultPostaz = null;
            foreach (EREC_KuldKuldemenyek kuld in kuldemenyekList)
            {
                // Címzett adatok ne íródjanak felül, az eredeti címzett kell: (BUG#4759)
                bool updateCimzettAdatok = false;
                EREC_KuldKuldemenyek kuldemeny = SetKuldemenyAdatokFromPostazasiAdatok(kuld, postazasiAdatok, updateCimzettAdatok);
                resultPostaz = this.Postazas(execParam.Clone(), kuldemeny, kuldemeny.BarCode);
                resultPostaz.CheckError();
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, Search.GetSqlInnerString(erec_KuldKuldemenyIds), "EREC_KuldKuldemenyek", "PostazasTomeges").Record;
                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    /// <summary>
    /// 'kuldemeny' objektum adatainak felülírása a felületrõl jövõ 'postazasiAdatok' objektummal
    /// </summary>
    /// <param name="kuldemeny"></param>
    /// <param name="postazasiAdatok"></param>
    /// <param name="updateCimzettAdatok">A címzett adatok felülíródjanak-e (NevSTR_Bekuldo, Partner_Id_Bekuldo, CimSTR_Bekuldo, Cim_Id)</param>
    private EREC_KuldKuldemenyek SetKuldemenyAdatokFromPostazasiAdatok(EREC_KuldKuldemenyek kuldemeny, EREC_KuldKuldemenyek postazasiAdatok
            , bool updateCimzettAdatok)
    {
        kuldemeny.BelyegzoDatuma = postazasiAdatok.BelyegzoDatuma;
        kuldemeny.Updated.BelyegzoDatuma = true;

        // Kiküldõ:
        kuldemeny.Csoport_Id_Cimzett = postazasiAdatok.Csoport_Id_Cimzett;
        kuldemeny.Updated.Csoport_Id_Cimzett = true;

        // Címzett adatok felülíródjanak-e:
        if (updateCimzettAdatok)
        {
            kuldemeny.NevSTR_Bekuldo = postazasiAdatok.NevSTR_Bekuldo;
            kuldemeny.Updated.NevSTR_Bekuldo = true;

            kuldemeny.Partner_Id_Bekuldo = postazasiAdatok.Partner_Id_Bekuldo;
            kuldemeny.Updated.Partner_Id_Bekuldo = true;

            kuldemeny.CimSTR_Bekuldo = postazasiAdatok.CimSTR_Bekuldo;
            kuldemeny.Updated.CimSTR_Bekuldo = true;

            kuldemeny.Cim_Id = postazasiAdatok.Cim_Id;
            kuldemeny.Updated.Cim_Id = true;
        }

        //kuldemeny.PeldanySzam = "1";
        //kuldemeny.Updated.PeldanySzam = true;

        kuldemeny.IraIktatokonyv_Id = postazasiAdatok.IraIktatokonyv_Id;
        kuldemeny.Updated.IraIktatokonyv_Id = true;

        if (string.IsNullOrEmpty(kuldemeny.BarCode))
        {
            kuldemeny.BarCode = postazasiAdatok.BarCode;
            kuldemeny.Updated.BarCode = true;
        }

        //kuldemeny.RagSzam = postazasiAdatok.RagSzam;
        //kuldemeny.Updated.RagSzam = true;

        kuldemeny.KimenoKuldemenyFajta = postazasiAdatok.KimenoKuldemenyFajta;
        kuldemeny.Updated.KimenoKuldemenyFajta = true;

        kuldemeny.Elsobbsegi = postazasiAdatok.Elsobbsegi;
        kuldemeny.Updated.Elsobbsegi = true;

        kuldemeny.Ajanlott = postazasiAdatok.Ajanlott;
        kuldemeny.Updated.Ajanlott = true;

        kuldemeny.Tertiveveny = postazasiAdatok.Tertiveveny;
        kuldemeny.Updated.Tertiveveny = true;

        kuldemeny.SajatKezbe = postazasiAdatok.SajatKezbe;
        kuldemeny.Updated.SajatKezbe = true;

        kuldemeny.E_ertesites = postazasiAdatok.E_ertesites;
        kuldemeny.Updated.E_ertesites = true;

        kuldemeny.E_elorejelzes = postazasiAdatok.E_elorejelzes;
        kuldemeny.Updated.E_elorejelzes = true;

        kuldemeny.PostaiLezaroSzolgalat = postazasiAdatok.PostaiLezaroSzolgalat;
        kuldemeny.Updated.PostaiLezaroSzolgalat = true;

        kuldemeny.Ar = postazasiAdatok.Ar;
        kuldemeny.Updated.Ar = true;

        kuldemeny.Base.Note = postazasiAdatok.Base.Note;
        kuldemeny.Base.Updated.Note = true;

        return kuldemeny;
    }
    /// <summary>
    /// KuldemenyTertivevenyLetezik
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kuldemenyId"></param>
    /// <param name="tertiveveny"></param>
    /// <returns></returns>
    private bool KuldemenyTertivevenyLetezik(ExecParam execParam, string kuldemenyId, string ragszam)
    {
        EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);
        ExecParam execParam_tertiveveny = execParam.Clone();
        EREC_KuldTertivevenyekSearch src = new EREC_KuldTertivevenyekSearch();

        src.Kuldemeny_Id.Filter(kuldemenyId);
        src.Ragszam.Filter(ragszam);

        Result result = service_tertiveveny.GetAll(execParam, src);
        result.CheckError();

        if (result == null || result.GetCount < 1)
            return false;
        return true;
    }
    #endregion TOMEGES_POSTAZAS}

    #region SAKKORA

    /// <summary>
    /// GetIrat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public EREC_IraIratok GetIrat(ExecParam execParam, string id)
    {
        #region GET IRAT
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);

        execParam.Record_Id = id;
        Result iratResultGet = service.Get(execParam);

        if (!String.IsNullOrEmpty(iratResultGet.ErrorCode))
            return null;
        return (EREC_IraIratok)iratResultGet.Record;
        #endregion  GET IRAT
    }

    /// <summary>
    /// Get ugyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    public EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string ugyiratId)
    {
        #region GET UGY
        EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);

        execParam.Record_Id = ugyiratId;
        Result iratResultGet = service.Get(execParam);

        if (!String.IsNullOrEmpty(iratResultGet.ErrorCode))
            return null;
        return (EREC_UgyUgyiratok)iratResultGet.Record;
        #endregion  GET UGY
    }
    /// <summary>
    /// UpdateIrat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    /// <returns></returns>
    private bool UpdateIrat(ExecParam execParam, EREC_IraIratok irat)
    {
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        execParam.Record_Id = irat.Id;
        Result result = service.Update(execParam, irat);

        if (!String.IsNullOrEmpty(result.ErrorCode))
            return false;

        return true;
    }
    /// <summary>
    /// UpdateUgyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyirat"></param>
    /// <returns></returns>
    private bool UpdateUgyirat(ExecParam execParam, EREC_UgyUgyiratok ugyirat)
    {
        EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
        execParam.Record_Id = ugyirat.Id;
        Result result = service.Update(execParam, ugyirat);

        if (!String.IsNullOrEmpty(result.ErrorCode))
            return false;

        return true;
    }
    public void StopElteltIdoPostazasSoranByIratIds(ExecParam execParam, List<string> iratIds)
    {
        try
        {
            if (iratIds == null || iratIds.Count < 1)
                return;

            foreach (string iratId in iratIds)
            {
                SetElteltIdoPostazasSoran(execParam, iratId);
            }
        }
        catch (Exception exc)
        {
            Logger.Error("sakkora.StopElteltIdoPostazasSoranByIrat ", exc);
        }
    }
    /// <summary>
    /// Eltelt ido leállítása postázás során
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    public void SetElteltIdoPostazasSoran(ExecParam execParam, string iratId)
    {
        try
        {
            if (string.IsNullOrEmpty(iratId))
                return;

            EREC_IraIratok irat = GetIrat(execParam, iratId);
            if (irat == null)
                return;

            EREC_UgyUgyiratok ugy = GetUgyirat(execParam, irat.Ugyirat_Id);
            if (ugy == null)
                return;

            ugy.ElteltidoAllapot = KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.STOP;
            ugy.Updated.ElteltidoAllapot = true;

            UpdateUgyirat(execParam, ugy);
        }
        catch (Exception exc)
        {
            Logger.Error("sakkora.StopElteltIdoPostazasSoran ", exc);
        }
    }

    #endregion
}
