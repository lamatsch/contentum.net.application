using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_KuldMellekletekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldMellekletekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_KuldMellekletekSearch _EREC_KuldMellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_KuldMellekletekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result GetAllByKuldKuldemenyek(ExecParam ExecParam, EREC_KuldKuldemenyek _EREC_KuldKuldemenyek
        , EREC_KuldMellekletekSearch _EREC_KuldMellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByKuldKuldemenyek(ExecParam, _EREC_KuldKuldemenyek
               , _EREC_KuldMellekletekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_KuldMellekletek Record)
    /// Egy rekord felv�tele a EREC_KuldMellekletek t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldMellekletek))]
    public Result Insert(ExecParam ExecParam, EREC_KuldMellekletek Record)
    {
        return this.Insert(ExecParam, Record,true);
    }

    public Result Insert(ExecParam ExecParam, EREC_KuldMellekletek Record, bool checkBarcode)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //Vonalk�d ellen�rz�se
            KRT_BarkodokService srvBarcode = new KRT_BarkodokService();
            Result resBarcode = new Result();
            if (checkBarcode)
            {
                srvBarcode = new KRT_BarkodokService(this.dataContext);
                resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }

            // F�irat ellen�rz�se
            if (Record.AdathordozoTipus == KodTarak.AdathordozoTipus.PapirAlapu)
            {
                EREC_KuldMellekletekSearch search = new EREC_KuldMellekletekSearch();
                
                search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.PapirAlapu;
                search.AdathordozoTipus.Operator = Contentum.eQuery.Query.Operators.equals;

                search.KuldKuldemeny_Id.Value = Record.KuldKuldemeny_Id;
                search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                //search.ErvKezd.Value = DateTime.Now.ToString();
                //search.ErvKezd.Operator = Contentum.eQuery.Query.Operators.less;

                //search.ErvVege.Value = DateTime.Now.ToString();
                //search.ErvVege.Operator = Contentum.eQuery.Query.Operators.greater;

                Result getAllResult = this.GetAll(ExecParam.Clone(), search);
                if (!(string.IsNullOrEmpty(getAllResult.ErrorCode) && getAllResult.Ds.Tables[0].Rows.Count == 0))
                    throw new ResultException(52624);
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //vonalk�d k�t�se, ha meg volt adva
            if (checkBarcode)
            {
                if (!String.IsNullOrEmpty(resBarcode.Uid))
                {
                    string barkodId = resBarcode.Uid;
                    string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                    string mellekletId = result.Uid;
                    resBarcode = new Result();
                    resBarcode = srvBarcode.BarkodBindToKuldMelleklet(ExecParam, barkodId, mellekletId, barkodVer);
                    if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                    {
                        throw new ResultException(resBarcode);
                    }
                }
            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_KuldMellekletek", "New").Record;

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, EREC_KuldMellekletek Record)
    /// Az EREC_KuldMellekletek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldMellekletek))]
    public Result Update(ExecParam ExecParam, EREC_KuldMellekletek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resBarcodeUpdate = new Result();
            if (Record.Updated.BarCode == true)
            {
                resBarcodeUpdate = UpdateKuldMellekletBarcode(ExecParam, Record.BarCode);
            }

            if (!String.IsNullOrEmpty(resBarcodeUpdate.ErrorCode))
            {
                throw new ResultException(resBarcodeUpdate);
            }

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_KuldMellekletek", "Modify").Record;

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private Result UpdateKuldMellekletBarcode(ExecParam ExecParam, string newBarcode)
    {
        //el�z� vonalk�d beolvas�sa
        Result resGet = this.Get(ExecParam);
        if (!String.IsNullOrEmpty(resGet.ErrorCode))
        {
            throw new ResultException(resGet);
        }

        string oldBarcode = ((EREC_KuldMellekletek)resGet.Record).BarCode;

        if (oldBarcode == newBarcode)
        {
            return new Result();
        }
        //Vonalk�d ellen�rz�se
        KRT_BarkodokService svcBarcode = new KRT_BarkodokService(this.dataContext);
        Result resBarcode = svcBarcode.CheckBarcode(ExecParam, newBarcode);
        if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
        {
            throw new ResultException(resBarcode);
        }
        string obj_type = String.Empty;
        string obj_id = ExecParam.Record_Id;

        //r�gi vonalk�d lek�r�se
        if (!String.IsNullOrEmpty(oldBarcode))
        {
            ExecParam xpm = ExecParam.Clone();
            xpm.Record_Id = String.Empty;
            Result resBarCodeGet = svcBarcode.GetBarcodeByValue(xpm, oldBarcode);
            if (resBarCodeGet.ErrorCode != "52481")
            {
                if (!String.IsNullOrEmpty(resBarCodeGet.ErrorCode))
                {
                    throw new ResultException(resBarCodeGet);
                }

                obj_type = ((KRT_Barkodok)resBarCodeGet.Record).Obj_type;
                obj_id = ((KRT_Barkodok)resBarCodeGet.Record).Obj_Id;
                //el�z� vonalk�d felszabad�t�sa
                Result resBarcodeFree = svcBarcode.FreeBarcode(xpm, oldBarcode);
                if (!String.IsNullOrEmpty(resBarcodeFree.ErrorCode))
                {
                    throw new ResultException(resBarcodeFree);
                }
            }

        }

        ExecParam execParam = ExecParam.Clone();
        if (!String.IsNullOrEmpty(resBarcode.Uid))
        {
            string barkodId = resBarcode.Uid;
            string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();

            resBarcode = new Result();
            //vonalk�d k�t�se k�ldem�nyhez vagy mell�klethez
            if (obj_type == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek)
            {
                resBarcode = svcBarcode.BarkodBindToKuldemeny(execParam, barkodId, obj_id, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }

                //kuldem�ny update-el�se
                EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
                ExecParam xpm = ExecParam.Clone();
                xpm.Record_Id = obj_id;
                Result resKuld = svcKuld.Get(xpm);
                if (!String.IsNullOrEmpty(resKuld.ErrorCode))
                {
                    throw new ResultException(resKuld);
                }

                EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)resKuld.Record;
                kuldemeny.Updated.SetValueAll(false);
                kuldemeny.Base.Updated.SetValueAll(false);
                kuldemeny.Base.Updated.Ver = true;

                kuldemeny.BarCode = newBarcode;
                kuldemeny.Updated.BarCode = true;

                Result resInsert = svcKuld.Update(xpm, kuldemeny);

                if (!String.IsNullOrEmpty(resInsert.ErrorCode))
                {
                    throw new ResultException(resInsert);
                }

            }
            else
            {

                resBarcode = svcBarcode.BarkodBindToKuldMelleklet(execParam, barkodId, obj_id, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }
        }

        return resBarcode;

    }


}