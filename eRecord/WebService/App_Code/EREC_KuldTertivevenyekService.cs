using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;
using Contentum.eQuery;
using System.Linq;
using System.Data;
using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_KuldTertivevenyekService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvett
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_KuldTertivevenyek Record)
    /// Egy rekord felv�tele a EREC_KuldTertivevenyek t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyek))]
    public Result Insert(ExecParam ExecParam, EREC_KuldTertivevenyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //Vonalk�d ellen�rz�se
            KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
            Result resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
            if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
            {
                throw new ResultException(resBarcode);
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //vonalk�d k�t�se, ha meg volt adva
            if (!String.IsNullOrEmpty(resBarcode.Uid))
            {
                string barkodId = resBarcode.Uid;
                string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                string tertivevenyId = result.Uid;
                resBarcode = new Result();
                resBarcode = srvBarcode.BarkodBindToKuldTertiveveny(ExecParam, barkodId, tertivevenyId, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id
                    , Contentum.eUtility.Constants.TableNames.EREC_KuldTertivevenyek, "KuldTertivevenyNew").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Update(ExecParam ExecParam, EREC_KuldTertivevenyek Record)
    /// Az EREC_KuldTertivevenyek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyek))]
    public Result Update(ExecParam ExecParam, EREC_KuldTertivevenyek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_KuldTertivevenyek erec_KuldTertivevenyek_regi = null;
            #region r�gi t�rtivev�ny lek�r�se DB-b�l
            /// Meg kell vizsg�lni, hogy t�nylegesen v�ltozott-e a vonalk�d
            ExecParam execParam_tertivevenyGet = ExecParam.Clone();
            execParam_tertivevenyGet.Record_Id = ExecParam.Record_Id;

            Result result_tertivevenyGet = this.Get(execParam_tertivevenyGet);
            if (!String.IsNullOrEmpty(result_tertivevenyGet.ErrorCode))
            {
                // hiba
                throw new ResultException(result_tertivevenyGet);
            }

            erec_KuldTertivevenyek_regi = (EREC_KuldTertivevenyek)result_tertivevenyGet.Record;
            #endregion r�gi t�rtivev�ny lek�r�se DB-b�l

            #region Vonalk�d v�ltoz�s�nak vizsg�lata

            bool vanUjVonalkod = false;
            Result resBarcode = null;
            KRT_BarkodokService srvBarcode = null;
            // ha v�ltozott a vonalk�d:
            if (Record.Updated.BarCode == true && !String.IsNullOrEmpty(Record.BarCode) && Record.BarCode != erec_KuldTertivevenyek_regi.BarCode)
            {
                vanUjVonalkod = true;
                srvBarcode = new KRT_BarkodokService(this.dataContext);

                #region Vonalk�d felszabad�t�sa
                if (!String.IsNullOrEmpty(erec_KuldTertivevenyek_regi.BarCode))
                {
                    Result resultFreeBarcode = srvBarcode.FreeBarcode(ExecParam.Clone(), erec_KuldTertivevenyek_regi.BarCode);
                    if (resultFreeBarcode.IsError)
                    {
                        throw new ResultException(resultFreeBarcode);
                    }
                }
                #endregion

                //Vonalk�d ellen�rz�se
                resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }

            #endregion Vonalk�d v�ltoz�s�nak vizsg�lata

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Vonalk�d k�t�se, ha kell:

            if (vanUjVonalkod == true && resBarcode != null)
            {
                string barkodId = resBarcode.Uid;
                string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                string tertivevenyId = ExecParam.Record_Id;
                resBarcode = new Result();

                ExecParam execParam_barkod = ExecParam.Clone();
                Result resBarcodeBind = srvBarcode.BarkodBindToKuldTertiveveny(execParam_barkod, barkodId, tertivevenyId, barkodVer);
                if (resBarcodeBind.IsError)
                {
                    throw new ResultException(resBarcodeBind);
                }
            }
            #endregion

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id
                    , Contentum.eUtility.Constants.TableNames.EREC_KuldTertivevenyek, "KuldTertivevenyModify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_KuldTertivevenyek t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyek))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_KuldTertivevenyek erec_KuldTertivevenyek = null;
            #region r�gi t�rtivev�ny lek�r�se DB-b�l
            ExecParam execParam_tertivevenyGet = ExecParam.Clone();
            execParam_tertivevenyGet.Record_Id = ExecParam.Record_Id;

            Result result_tertivevenyGet = this.Get(execParam_tertivevenyGet);
            if (!String.IsNullOrEmpty(result_tertivevenyGet.ErrorCode))
            {
                // hiba
                throw new ResultException(result_tertivevenyGet);
            }

            erec_KuldTertivevenyek = (EREC_KuldTertivevenyek)result_tertivevenyGet.Record;
            #endregion r�gi t�rtivev�ny lek�r�se DB-b�l

            #region Vonalk�d felszabad�t�sa

            if (!string.IsNullOrEmpty(erec_KuldTertivevenyek.BarCode))
            {
                KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                Result barkodResult = barkodService.FreeBarcode(ExecParam.Clone(), erec_KuldTertivevenyek.BarCode);
                if (!string.IsNullOrEmpty(barkodResult.ErrorCode))
                    throw new ResultException(barkodResult);
            }

            #endregion

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id
                    , Contentum.eUtility.Constants.TableNames.EREC_KuldTertivevenyek, "KuldTertivevenyInvalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion Gener�ltb�l �tvett


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_KuldTertivevenyekSearch _EREC_KuldTertivevenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_KuldTertivevenyekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod(Description = "A t�rtivev�nyek �rkeztet�s�hez lek�rdezi az �sszes k�ldem�nyt amihez t�rtivev�nyt lehet rendelni.")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyekSearch))]
    public Result GetTertivevenyKuldemenyeit(ExecParam ExecParam, EREC_KuldKuldemenyekSearch _EREC_KuldKuldemenyekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetTertivevenyKuldemenyeit(ExecParam, _EREC_KuldKuldemenyekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    // BUG_6935
    // BUG kapcs�n kirakva Kodtarak.cs-be
    //private string GetKodTarId(ExecParam execParam, string kod, string where)
    //{
    //    Contentum.eAdmin.Service.KRT_KodTarakService kodTarakService = eAdminService.ServiceFactory.GetKRT_KodTarakService();
    //    KRT_KodTarakSearch kodTarakSearch = new KRT_KodTarakSearch();
    //    kodTarakSearch.Kod.Value = kod;
    //    kodTarakSearch.Kod.Operator = Query.Operators.equals;
    //    kodTarakSearch.WhereByManual = where;        

    //    Result kodTarakResult = kodTarakService.GetAllWithKodcsoport(execParam, kodTarakSearch);
    //    string result = "";
    //    if (!kodTarakResult.IsError && kodTarakResult.Ds.Tables[0].Rows.Count > 0)
    //    {
    //        result = kodTarakResult.Ds.Tables[0].AsEnumerable().FirstOrDefault(x => x["Kod"].ToString() == kod)["Id"].ToString();
    //    }
    //    return result;
    //}

    private bool Atvette(ExecParam execParam, string tertivisszaKod)
    {
        bool atvette = false;
        string atvetteGuid = "";
        string selectedTertvisszaGuid = "";

        atvetteGuid = Contentum.eRecord.BaseUtility.KodTarak.GetKodTarId(execParam, "40", " and KRT_KodCsoportok.Kod='IRATPELDANY_ALLAPOT' ");
        selectedTertvisszaGuid = Contentum.eRecord.BaseUtility.KodTarak.GetKodTarId(execParam, tertivisszaKod, " and KRT_KodCsoportok.Kod='TERTIVEVENY_VISSZA_KOD' ");

        Contentum.eAdmin.Service.KRT_KodtarFuggosegService kodservice = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();
        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "TERTIVEVENY_VISSZA_KOD");
        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", "IRATPELDANY_ALLAPOT");
        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

        Result res = kodservice.GetAll(execParam, search);

        if (!res.IsError)
        {
            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                List<string> fuggoKodtarak = new List<string>();

                var item = fuggosegek.Items.FirstOrDefault(x => (x.VezerloKodTarId == selectedTertvisszaGuid && (x.FuggoKodtarId == atvetteGuid)));

                if (item != null)
                {
                    atvette = true;
                }
            }
        }
        return atvette;
    }

    [WebMethod()]
    public Result TertivevenyErkeztetes(ExecParam execParam, string erec_KuldTertivevenyek_Id, EREC_KuldTertivevenyek erec_KuldTertivevenyek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Info("T�rtivev�ny �rkeztet�s - Start", execParam);

        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_KuldTertivevenyek_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52640);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            string kezbesitesEredmenye = erec_KuldTertivevenyek.TertivisszaKod;
            //if (kezbesitesEredmenye != KodTarak.TERTIVEVENY_VISSZA_KOD.Cimzett_atvette
            //    && kezbesitesEredmenye != KodTarak.TERTIVEVENY_VISSZA_KOD.Ismeretlen_cimzett
            //    && kezbesitesEredmenye != KodTarak.TERTIVEVENY_VISSZA_KOD.Mas_atvette
            //    && kezbesitesEredmenye != KodTarak.TERTIVEVENY_VISSZA_KOD.Nem_vette_at)
            //{
            //    Logger.Error("A k�zbes�t�s eredm�nye nem hat�rozhat� meg", execParam);
            //    throw new ResultException(52642);
            //}

            #region T�rtivev�ny �llapot ellen�rz�s:

            ExecParam execParam_TertivGet = execParam.Clone();
            execParam_TertivGet.Record_Id = erec_KuldTertivevenyek_Id;

            Result result_TertivGet = this.Get(execParam_TertivGet);
            if (!String.IsNullOrEmpty(result_TertivGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_TertivGet);
            }

            EREC_KuldTertivevenyek tertivevenyObj = (EREC_KuldTertivevenyek)result_TertivGet.Record;

            // �llapot ellen�rz�s:
            if (tertivevenyObj.Allapot != "" && tertivevenyObj.Allapot != KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett
                && tertivevenyObj.Allapot != KodTarak.TERTIVEVENY_ALLAPOT.Feltoltott)
            {
                Logger.Warn("A t�rtivev�ny adatok nem m�dos�that�k", execParam);
                throw new ResultException(52641);
            }

            #endregion

            #region T�rtivev�ny UPDATE:

            tertivevenyObj.Updated.SetValueAll(false);
            tertivevenyObj.Base.Updated.SetValueAll(false);
            tertivevenyObj.Base.Updated.Ver = true;

            // Param�terb�l kapott adatok �tv�tele:

            tertivevenyObj.TertivisszaDat = erec_KuldTertivevenyek.TertivisszaDat;
            tertivevenyObj.Updated.TertivisszaDat = true;

            tertivevenyObj.TertivisszaKod = erec_KuldTertivevenyek.TertivisszaKod;
            tertivevenyObj.Updated.TertivisszaKod = true;

            tertivevenyObj.AtvevoSzemely = erec_KuldTertivevenyek.AtvevoSzemely;
            tertivevenyObj.Updated.AtvevoSzemely = erec_KuldTertivevenyek.Updated.AtvevoSzemely;

            tertivevenyObj.AtvetelDat = erec_KuldTertivevenyek.AtvetelDat;
            tertivevenyObj.Updated.AtvetelDat = erec_KuldTertivevenyek.Updated.AtvetelDat;

            // A Note mez�ben jelenleg az �tv�tel jogc�m�t tartjuk nyilv�n
            tertivevenyObj.Base.Note = erec_KuldTertivevenyek.Base.Note;
            tertivevenyObj.Base.Updated.Note = true;

            tertivevenyObj.KezbVelelemDatuma = erec_KuldTertivevenyek.KezbVelelemDatuma;
            tertivevenyObj.Updated.KezbVelelemDatuma = erec_KuldTertivevenyek.Updated.KezbVelelemDatuma;

            tertivevenyObj.KezbVelelemBeallta = erec_KuldTertivevenyek.KezbVelelemBeallta;
            tertivevenyObj.Updated.KezbVelelemBeallta = erec_KuldTertivevenyek.Updated.KezbVelelemBeallta;


            // �llapot m�dos�t�s:
            if (tertivevenyObj.Allapot == KodTarak.TERTIVEVENY_ALLAPOT.Feltoltott)
            {
                tertivevenyObj.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Feldolgozott;
            }
            else
            {
                tertivevenyObj.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Regisztralt;
            }
            tertivevenyObj.Updated.Allapot = true;

            ExecParam execParam_tertivUpdate = execParam.Clone();
            execParam_tertivUpdate.Record_Id = tertivevenyObj.Id;

            Result result_tertivUpdate = this.Update(execParam_tertivUpdate, tertivevenyObj);
            if (!String.IsNullOrEmpty(result_tertivUpdate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_tertivUpdate);
            }

            #endregion

            result = result_tertivUpdate;

            // TODO: Kimen� k�ldem�nyt kell m�dos�tani? (�llapot �gy most Post�zott marad)

            #region K�ldem�ny iratp�ld�nyainak lek�r�se �s m�dos�t�sa

            EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

            ExecParam execParam_PldGetAll = execParam.Clone();
            EREC_PldIratPeldanyokSearch search_Pld = new EREC_PldIratPeldanyokSearch();

            Result result_PldGetAll = service_Pld.GetAllWithExtensionByKimenoKuldemeny(execParam_PldGetAll, search_Pld, tertivevenyObj.Kuldemeny_Id);
            if (!String.IsNullOrEmpty(result_PldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_PldGetAll);
            }

            foreach (System.Data.DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
            {
                string pld_Id = row["Id"].ToString();
                string pld_Ver = row["Ver"].ToString();
                string pld_Allapot = row["Allapot"].ToString();
                string pld_PostazasAllapot = row["PostazasAllapot"].ToString();

                #region �llapot ellen�rz�s:

                // Sztorn�zottal m�r nem foglalkozunk:
                if (pld_Allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                {
                    continue;
                }

                // Iratp�ld�nynak post�zottnak kell lennie:
                if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Postazott)
                {
                    Logger.Warn("Az iratp�ld�ny �llapota nem post�zott", execParam);
                    throw new ResultException(52643);
                }

                #endregion

                EREC_PldIratPeldanyok iratPeldany_UpdateObj = new EREC_PldIratPeldanyok();
                iratPeldany_UpdateObj.Updated.SetValueAll(false);
                iratPeldany_UpdateObj.Base.Updated.SetValueAll(false);
                iratPeldany_UpdateObj.Base.Ver = pld_Ver;
                iratPeldany_UpdateObj.Base.Updated.Ver = true;

                if (!Atvette(execParam, kezbesitesEredmenye))
                {
                    // Sikertelen k�zbes�t�s, iratp�ld�ny �jrak�ldend�, ha ez az els� kik�ld�s volt, egy�bk�nt c�mzett �tvette:

                    // ha m�r a m�sodik post�z�s volt --> c�mzett �tvette �llapot
                    //if (pld_PostazasAllapot == KodTarak.IRATPELDANY_POSTAZAS_ALLAPOT.Ujrapostazas)
                    //{
                    //    iratPeldany_UpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette;
                    //    iratPeldany_UpdateObj.Updated.Allapot = true;
                    //}
                    //else
                    //{
                    //    iratPeldany_UpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo;
                    //    iratPeldany_UpdateObj.Updated.Allapot = true;

                    //    iratPeldany_UpdateObj.PostazasAllapot = KodTarak.IRATPELDANY_POSTAZAS_ALLAPOT.Ujrapostazas;
                    //    iratPeldany_UpdateObj.Updated.PostazasAllapot = true;
                    //}

                    //nincs m�r �jrak�ld�s
                    iratPeldany_UpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at;
                    iratPeldany_UpdateObj.Updated.Allapot = true;
                }
                else
                {
                    // Sikeres k�zbes�t�s: (C�mzett �tvette)
                    iratPeldany_UpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette;
                    iratPeldany_UpdateObj.Updated.Allapot = true;

                    //LZS - BUG_12607
                    iratPeldany_UpdateObj.AtvetelDatuma = erec_KuldTertivevenyek.AtvetelDat;
                    iratPeldany_UpdateObj.Updated.AtvetelDatuma = true;
                }

                ExecParam execParam_PldUpdate = execParam.Clone();
                execParam_PldUpdate.Record_Id = pld_Id;

                Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, iratPeldany_UpdateObj);
                if (!String.IsNullOrEmpty(result_PldUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_PldUpdate);
                }
            }

            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result ETertivevenyErkeztetes(ExecParam execParam, string ragszam, string tertiVisszaKod, string kezbVelelemDatuma, DateTime atvetelDatuma, string atvevo, string atvetelJogcim, string moreData)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Info("ETertivevenyErkeztetes - Start", execParam);

        Result result = new Result();
        if (execParam == null
            || string.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || string.IsNullOrEmpty(ragszam))
        {
            // hiba
            result = ResultError.CreateNewResultWithErrorCode(52640);
            log.WsEnd(execParam, result);
            return result;
        }

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region FIND TERTI
            EREC_KuldTertivevenyek erec_KuldTertivevenyek = GetTertiveveny(execParam, ragszam);
            #endregion

            #region CHECK TERTI ALLAPOT
            if (erec_KuldTertivevenyek.Allapot != ""
                && erec_KuldTertivevenyek.Allapot != KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett
                && erec_KuldTertivevenyek.Allapot != KodTarak.TERTIVEVENY_ALLAPOT.Feltoltott
                && erec_KuldTertivevenyek.Allapot != KodTarak.TERTIVEVENY_ALLAPOT.Regisztralt
                )
            {
                Logger.Warn("A t�rtivev�ny adatok nem m�dos�that�k", execParam);
                throw new ResultException(52641);
            }
            #endregion

            #region SET TERTI ALLAPOT

            erec_KuldTertivevenyek.Updated.SetValueAll(false);
            erec_KuldTertivevenyek.Base.Updated.SetValueAll(false);
            erec_KuldTertivevenyek.Base.Updated.Ver = true;

            if (!string.IsNullOrEmpty(tertiVisszaKod))
            {
                erec_KuldTertivevenyek.TertivisszaKod = tertiVisszaKod;
                erec_KuldTertivevenyek.Updated.TertivisszaKod = true;
            }

            erec_KuldTertivevenyek.TertivisszaDat = atvetelDatuma.ToString();
            erec_KuldTertivevenyek.Updated.TertivisszaDat = true;

            erec_KuldTertivevenyek.AtvetelDat = atvetelDatuma.ToString();
            erec_KuldTertivevenyek.Updated.AtvetelDat = true;

            if (!string.IsNullOrEmpty(atvevo))
            {
                erec_KuldTertivevenyek.AtvevoSzemely = atvevo;
                erec_KuldTertivevenyek.Updated.AtvevoSzemely = true;
            }

            if (!string.IsNullOrEmpty(kezbVelelemDatuma))
            {
                erec_KuldTertivevenyek.KezbVelelemDatuma = kezbVelelemDatuma;
                erec_KuldTertivevenyek.Updated.KezbVelelemDatuma = true;
            }
            if (!string.IsNullOrEmpty(atvetelJogcim))
            {
                erec_KuldTertivevenyek.Base.Note = atvetelJogcim;
                erec_KuldTertivevenyek.Base.Updated.Note = true;
            }
            erec_KuldTertivevenyek.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Regisztralt;
            erec_KuldTertivevenyek.Updated.Allapot = true;

            ExecParam execParamUpdate = execParam.Clone();
            execParamUpdate.Record_Id = erec_KuldTertivevenyek.Id;
            Result resultUpdate = this.Update(execParamUpdate, erec_KuldTertivevenyek);
            resultUpdate.CheckError();

            #endregion

            #region K�ldem�ny iratp�ld�nyainak lek�r�se �s m�dos�t�sa

            EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

            ExecParam execParam_PldGetAll = execParam.Clone();
            EREC_PldIratPeldanyokSearch search_Pld = new EREC_PldIratPeldanyokSearch();

            Result result_PldGetAll = service_Pld.GetAllWithExtensionByKimenoKuldemeny(execParam_PldGetAll, search_Pld, erec_KuldTertivevenyek.Kuldemeny_Id);
            if (!String.IsNullOrEmpty(result_PldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_PldGetAll);
            }

            foreach (DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
            {
                string pld_Id = row["Id"].ToString();
                string pld_Ver = row["Ver"].ToString();
                string pld_Allapot = row["Allapot"].ToString();
                string pld_PostazasAllapot = row["PostazasAllapot"].ToString();

                #region �llapot ellen�rz�s:

                // Sztorn�zottal m�r nem foglalkozunk:
                if (pld_Allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                {
                    continue;
                }

                // Iratp�ld�nynak post�zottnak kell lennie:
                if (pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Postazott 
                    && pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette 
                    && pld_Allapot != KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at)
                {
                    Logger.Warn("Az iratp�ld�ny �llapota nem post�zott", execParam);
                    throw new ResultException(52643);
                }

                #endregion

                EREC_PldIratPeldanyok iratPeldany_UpdateObj = new EREC_PldIratPeldanyok();
                iratPeldany_UpdateObj.Updated.SetValueAll(false);
                iratPeldany_UpdateObj.Base.Updated.SetValueAll(false);
                iratPeldany_UpdateObj.Base.Ver = pld_Ver;
                iratPeldany_UpdateObj.Base.Updated.Ver = true;

                if (!Atvette(execParam, tertiVisszaKod))
                {
                    //nincs m�r �jrak�ld�s
                    iratPeldany_UpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at;
                    iratPeldany_UpdateObj.Updated.Allapot = true;
                }
                else
                {
                    // Sikeres k�zbes�t�s: (C�mzett �tvette)
                    iratPeldany_UpdateObj.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette;
                    iratPeldany_UpdateObj.Updated.Allapot = true;
                }

                ExecParam execParam_PldUpdate = execParam.Clone();
                execParam_PldUpdate.Record_Id = pld_Id;

                Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, iratPeldany_UpdateObj);
                if (!string.IsNullOrEmpty(result_PldUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_PldUpdate);
                }
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    [WebMethod()]
    // BLG_237
    public Result TertivevenyErkeztetesEmail(ExecParam execParam, string ragszam, String tertiVisszaKod, DateTime atvetelDatuma, String atvevo)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Info("T�rtivev�ny �rkeztet�s Emailb�l- Start", execParam);

        Result resultTerti = new Result();
        // Param�terek ellen�rz�se:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ragszam) || String.IsNullOrEmpty(tertiVisszaKod))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52640);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_KuldTertivevenyek erec_KuldTertivevenyek = GetTertiveveny(execParam, ragszam);

            if (erec_KuldTertivevenyek.Allapot == KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett)
            {
                erec_KuldTertivevenyek.Updated.SetValueAll(false);
                erec_KuldTertivevenyek.Base.Updated.SetValueAll(false);
                erec_KuldTertivevenyek.Base.Updated.Ver = true;


                erec_KuldTertivevenyek.TertivisszaKod = tertiVisszaKod;
                erec_KuldTertivevenyek.Updated.TertivisszaKod = true;

                erec_KuldTertivevenyek.TertivisszaDat = DateTime.Now.ToString();
                erec_KuldTertivevenyek.Updated.TertivisszaDat = true;

                erec_KuldTertivevenyek.AtvetelDat = atvetelDatuma.ToString();
                erec_KuldTertivevenyek.Updated.AtvetelDat = true;

                erec_KuldTertivevenyek.AtvevoSzemely = atvevo;
                erec_KuldTertivevenyek.Updated.AtvevoSzemely = true;

                resultTerti = this.TertivevenyErkeztetes(execParam, erec_KuldTertivevenyek.Id, erec_KuldTertivevenyek);

                if (resultTerti.IsError)
                {
                    throw new ResultException(resultTerti);
                }

                //AttacheBeadVanyCsatolmanyToTertiveveny(execParam, erec_eBeadvanyok, tertiveveny);

                //attached = true;
            }
            else
            {
                Logger.Debug("A t�rtivev�ny m�r �rkeztetve van!");
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return resultTerti;
    }

    private EREC_KuldTertivevenyek GetTertiveveny(ExecParam execParam, string hivatkozasiSzam)
    {
        Logger.Info(String.Format("GetTertiveveny kezdete: {0}", hivatkozasiSzam));

        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        search.Ragszam.Value = hivatkozasiSzam;
        search.Ragszam.Operator = Query.Operators.equals;

        EREC_KuldTertivevenyekService service = new EREC_KuldTertivevenyekService(this.dataContext);

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Warn(String.Format("Nem tal�lhat� a t�rtivev�ny: {0}", hivatkozasiSzam));
            return null;
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            throw new Exception(String.Format("T�bb t�rtivev�ny tal�lhat�: {0}", hivatkozasiSzam));            
        }

        EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
        Utility.LoadBusinessDocumentFromDataRow(tertiveveny, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GetTertiveveny vege: {0}", hivatkozasiSzam));

        return tertiveveny;
    }

    /// <summary>
    /// InsertTomeges(ExecParam ExecParam, string[] Kuldemeny_Id_Array, string[] BarCode_Array, string[] Ragszam_Array, EREC_KuldTertivevenyek Record, DateTime ExecutionTime)
    /// T�bb rekord egyidej� felv�tele az EREC_KuldTertivevenyek t�bl�ba. 
    /// A rekord adatait a Record parameter �s a t�telenk�nt egyedi param�tereket k�l�n�ll� t�mb�k tartalmazz�k. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "T�bb rekord egyidej� felv�tele az EREC_KuldTertivevenyek t�bl�ba. A rekord adatait a Record parameter �s a t�telenk�nt egyedi param�tereket k�l�n�ll� t�mb�k tartalmazz�k. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyek))]
    public Result InsertTomeges(ExecParam ExecParam, List<string> Kuldemeny_Id_List, List<string> BarCode_List, List<string> Ragszam_List, EREC_KuldTertivevenyek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {

            #region Param�terek ellen�rz�se
            if (ExecParam == null || String.IsNullOrEmpty(ExecParam.Felhasznalo_Id)
                || Kuldemeny_Id_List == null || BarCode_List == null
                || Kuldemeny_Id_List.Count == 0 || BarCode_List.Count == 0)
            {
                // hiba
                // T�rtivev�nyek: hi�nyz� param�ter(ek)!
                throw new ResultException(53810);
            }

            if (Kuldemeny_Id_List.Count != BarCode_List.Count)
            {
                // Hiba a t�rtivevev�nyek t�meges l�trehoz�s�n�l: Nem egy�rtelm� az �sszerendel�s a k�ldem�nyek �s a t�rtivev�ny ragsz�mok k�z�tt! A k�ldem�nyek �s a t�rtivev�ny ragsz�mok darabsz�m�nak meg kell egyeznie.
                throw new ResultException(53811);
            }

            if (Ragszam_List != null && Kuldemeny_Id_List.Count != Ragszam_List.Count)
            {
                // Hiba a t�rtivevev�nyek t�meges l�trehoz�s�n�l: Nem egy�rtelm� az �sszerendel�s a k�ldem�nyek �s k�ldem�ny ragsz�mok k�z�tt! A k�ldem�nyek �s k�ldem�ny ragsz�mok darabsz�m�nak meg kell egyeznie.
                throw new ResultException(53812);
            }

            if (BarCode_List.Exists(delegate (string item) { return String.IsNullOrEmpty(item); }))
            {
                // Hiba a t�rtivev�nyek t�meges l�trehoz�s�n�l: nincs megadva minden k�ldem�nyhez a t�rtivev�ny ragsz�m!
                throw new ResultException(53813);
            }
            #endregion Param�terek ellen�rz�se

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //Vonalk�dok ellen�rz�se
            //  -> sp_EREC_KuldtertivevenyekInsertTomeges t�rolt elj�r�s v�gzi

            result = sp.InsertTomeges(ExecParam, Kuldemeny_Id_List, BarCode_List, Ragszam_List, Record, ExecutionTime);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            //vonalk�d k�t�se, ha meg volt adva
            //  -> sp_EREC_KuldtertivevenyekInsertTomeges t�rolt elj�r�s v�gzi


            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                #region Id-k visszaolvas�sa esem�nynapl�hoz
                string ids = null;
                if (result.Ds != null)
                {
                    System.Collections.Generic.List<string> lstIds = new System.Collections.Generic.List<string>(result.Ds.Tables[0].Rows.Count);
                    foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        if (lstIds.Contains(id) == false)
                        {
                            lstIds.Add(id);
                        }
                    }

                    ids = Search.GetSqlInnerString(lstIds.ToArray());
                }
                #endregion Id-k visszaolvas�sa esem�nynapl�hoz

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, ids, "KuldTertivevenyNew");
                if (eventLogResult.IsError)
                    Logger.Debug("[ERROR]EREC_KuldTertivevenyek::InsertTomeges: ", ExecParam, eventLogResult);

            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);

            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    /// <summary>
    /// Get Kuld Tertivevenyek With Dokumentum
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [WebMethod(Description = "A t�rtivev�nyek lek�rdez�se, t�rtivev�ny dokumentumm id-val b�v�tve .")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyek))]
    public Result GetWithDokumentum(ExecParam ExecParam, string id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.KuldTertivevenyekGetWithDokumentum(ExecParam, id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}