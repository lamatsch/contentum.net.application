﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_MellekletekService : System.Web.Services.WebService
{
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_MellekletekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_MellekletekSearch _EREC_MellekletekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_MellekletekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod(Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Mellekletek))]
    public Result Insert(ExecParam ExecParam, EREC_Mellekletek Record)
    {
        return this.Insert(ExecParam, Record, true);
    }

    public Result Insert(ExecParam ExecParam, EREC_Mellekletek Record, bool checkBarcode)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (String.IsNullOrEmpty(Record.BarCode))
            {
                checkBarcode = false;
            }

            //Vonalkód ellenőrzése
            KRT_BarkodokService srvBarcode = new KRT_BarkodokService();
            Result resBarcode = new Result();
            if (checkBarcode)
            {
                srvBarcode = new KRT_BarkodokService(this.dataContext);
                resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }

            // Főirat ellenőrzése (küldemény mellékletnél)
            if (Record.AdathordozoTipus == KodTarak.AdathordozoTipus.PapirAlapu && !String.IsNullOrEmpty(Record.KuldKuldemeny_Id))
            {
                EREC_MellekletekSearch search = new EREC_MellekletekSearch();

                search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.PapirAlapu;
                search.AdathordozoTipus.Operator = Contentum.eQuery.Query.Operators.equals;

                search.KuldKuldemeny_Id.Value = Record.KuldKuldemeny_Id;
                search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                Result getAllResult = this.GetAll(ExecParam.Clone(), search);
                if (!(string.IsNullOrEmpty(getAllResult.ErrorCode) && getAllResult.Ds.Tables[0].Rows.Count == 0))
                    throw new ResultException(52624);
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //vonalkód kötése, ha meg volt adva
            if (checkBarcode)
            {
                if (!String.IsNullOrEmpty(resBarcode.Uid))
                {
                    string barkodId = resBarcode.Uid;
                    string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                    string mellekletId = result.Uid;
                    resBarcode = new Result();

                    // vagy iratmelléklethez, vagy küldemény melléklethez kötjük:
                    if (!string.IsNullOrEmpty(Record.IraIrat_Id))
                    {
                        resBarcode = srvBarcode.BarkodBindToObject(ExecParam, barkodId, mellekletId, 
                            Contentum.eUtility.Constants.TableNames.EREC_IratMellekletek, barkodVer);
                    }
                    else if (!String.IsNullOrEmpty(Record.KuldKuldemeny_Id))
                    {
                        resBarcode = srvBarcode.BarkodBindToObject(ExecParam, barkodId, mellekletId,
                            Contentum.eUtility.Constants.TableNames.EREC_KuldMellekletek, barkodVer);
                    }
                
                    if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                    {
                        throw new ResultException(resBarcode);
                    }
                }
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            string funkcioKod = !string.IsNullOrEmpty(Record.IraIrat_Id) ? "IraIratMellekletNew" : "KuldemenyMellekletNew";

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_Mellekletek", funkcioKod).Record;

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. A vonalkódra, ha meg van adva nem történik ellenőrzés és az nem lesz az objektumhoz kötve. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Mellekletek))]
    public Result UpdateBarCodeWithoutBinding(ExecParam ExecParam, string barCode)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result result_get = Get(ExecParam);
            if (result_get.IsError)
            {
                throw new ResultException(result_get);
            }

            EREC_Mellekletek erec_Mellekletek = (EREC_Mellekletek)result_get.Record;

            if (erec_Mellekletek != null && erec_Mellekletek.BarCode != barCode)
            {
                erec_Mellekletek.Updated.SetValueAll(false);
                erec_Mellekletek.Base.Updated.SetValueAll(false);

                erec_Mellekletek.BarCode = barCode;
                erec_Mellekletek.Updated.BarCode = true;

                erec_Mellekletek.Base.Updated.Ver = true;

                Result result_update = sp.Insert(Constants.Update, ExecParam, erec_Mellekletek);

                if (result_update.IsError)
                {
                    throw new ResultException(result_update);
                }

                result = result_update;
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_KuldMellekletek", "Modify").Record;

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Mellekletek))]
    public Result Update(ExecParam ExecParam, EREC_Mellekletek Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result resBarcodeUpdate = new Result();
            if (Record.Updated.BarCode == true)
            {
                resBarcodeUpdate = UpdateKuldMellekletBarcode(ExecParam, Record.BarCode);
            }

            if (!String.IsNullOrEmpty(resBarcodeUpdate.ErrorCode))
            {
                throw new ResultException(resBarcodeUpdate);
            }

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_KuldMellekletek", "Modify").Record;

            Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private Result UpdateKuldMellekletBarcode(ExecParam ExecParam, string newBarcode)
    {
        //előző vonalkód beolvasása
        Result resGet = this.Get(ExecParam);
        if (!String.IsNullOrEmpty(resGet.ErrorCode))
        {
            throw new ResultException(resGet);
        }

        EREC_Mellekletek erec_Mellekletek = (EREC_Mellekletek)resGet.Record;

        // Csak a Küldeményre kell lefutnia (ahol KuldemenyId meg van adva, de az iratId nincs)
        if (string.IsNullOrEmpty(erec_Mellekletek.KuldKuldemeny_Id) || !String.IsNullOrEmpty(erec_Mellekletek.IraIrat_Id))
        {
            return new Result();
        }

        string oldBarcode = erec_Mellekletek.BarCode;

        if (oldBarcode == newBarcode)
        {
            return new Result();
        }
        //Vonalkód ellenőrzése
        KRT_BarkodokService svcBarcode = new KRT_BarkodokService(this.dataContext);
        Result resBarcode = svcBarcode.CheckBarcode(ExecParam, newBarcode);
        if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
        {
            throw new ResultException(resBarcode);
        }
        string obj_type = String.Empty;
        string obj_id = ExecParam.Record_Id;

        //régi vonalkód lekérése
        if (!String.IsNullOrEmpty(oldBarcode))
        {
            ExecParam xpm = ExecParam.Clone();
            xpm.Record_Id = String.Empty;
            Result resBarCodeGet = svcBarcode.GetBarcodeByValue(xpm, oldBarcode);
            if (resBarCodeGet.ErrorCode != "52481")
            {
                if (!String.IsNullOrEmpty(resBarCodeGet.ErrorCode))
                {
                    throw new ResultException(resBarCodeGet);
                }

                obj_type = ((KRT_Barkodok)resBarCodeGet.Record).Obj_type;
                obj_id = ((KRT_Barkodok)resBarCodeGet.Record).Obj_Id;
                //előző vonalkód felszabadítása
                Result resBarcodeFree = svcBarcode.FreeBarcode(xpm, oldBarcode);
                if (!String.IsNullOrEmpty(resBarcodeFree.ErrorCode))
                {
                    throw new ResultException(resBarcodeFree);
                }
            }

        }

        ExecParam execParam = ExecParam.Clone();
        if (!String.IsNullOrEmpty(resBarcode.Uid))
        {
            string barkodId = resBarcode.Uid;
            string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();

            resBarcode = new Result();
            //vonalkód kötése küldeményhez vagy melléklethez
            if (obj_type == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek)
            {
                resBarcode = svcBarcode.BarkodBindToKuldemeny(execParam, barkodId, obj_id, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }

                //kuldemény update-elése
                EREC_KuldKuldemenyekService svcKuld = new EREC_KuldKuldemenyekService(this.dataContext);
                ExecParam xpm = ExecParam.Clone();
                xpm.Record_Id = obj_id;
                Result resKuld = svcKuld.Get(xpm);
                if (!String.IsNullOrEmpty(resKuld.ErrorCode))
                {
                    throw new ResultException(resKuld);
                }

                EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)resKuld.Record;
                kuldemeny.Updated.SetValueAll(false);
                kuldemeny.Base.Updated.SetValueAll(false);
                kuldemeny.Base.Updated.Ver = true;

                kuldemeny.BarCode = newBarcode;
                kuldemeny.Updated.BarCode = true;

                Result resInsert = svcKuld.Update(xpm, kuldemeny);

                if (!String.IsNullOrEmpty(resInsert.ErrorCode))
                {
                    throw new ResultException(resInsert);
                }

            }
            else
            {

                resBarcode = svcBarcode.BarkodBindToKuldMelleklet(execParam, barkodId, obj_id, barkodVer);
                if (!String.IsNullOrEmpty(resBarcode.ErrorCode))
                {
                    throw new ResultException(resBarcode);
                }
            }
        }

        return resBarcode;

    }

    /// <summary>
    /// InsertTomeges(ExecParam ExecParam, string[] Kuldemeny_Id_Array, string[] BarCode_Array, string[] Ragszam_Array, EREC_Mellekletek Record, DateTime ExecutionTime)
    /// Több rekord egyidejű felvétele az EREC_Mellekletek táblába. Speciálisan tértivevény típusú melléklethez.
    /// A rekord adatait a Record parameter és a tételenként egyedi paramétereket különálló tömbök tartalmazzák. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Több rekord egyidejű felvétele az EREC_Mellekletek táblába. Speciálisan tértivevény típusú melléklethez. A rekord adatait a Record parameter és a tételenként egyedi paramétereket különálló tömbök tartalmazzák. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldTertivevenyek))]
    public Result InsertTertivevenyTomeges(ExecParam ExecParam, List<string> Kuldemeny_Id_List, List<string> BarCode_List, EREC_Mellekletek Record, DateTime ExecutionTime)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            #region Paraméterek ellenőrzése
            if (ExecParam == null || String.IsNullOrEmpty(ExecParam.Felhasznalo_Id)
                || Kuldemeny_Id_List == null || BarCode_List == null
                || Kuldemeny_Id_List.Count == 0 || BarCode_List.Count == 0)
            {
                // hiba
                // Hiba a küldemények tértivevevény típusú mellékleteinek tömeges létrehozásánál: hiányzó paraméter(ek)!
                throw new ResultException(53823);
            }

            if (Kuldemeny_Id_List.Count != BarCode_List.Count)
            {
                // Hiba a küldemények tértivevevény típusú mellékleteinek tömeges létrehozásánál: Nem egyértelmű az összerendelés a küldemények és a tértivevény ragszámok között! A küldemények és a tértivevény ragszámok darabszámának meg kell egyeznie.
                throw new ResultException(53821);
            }

            if (BarCode_List.Exists(delegate(string item) { return String.IsNullOrEmpty(item); }))
            {
                // Hiba a küldemények tértivevevény típusú mellékleteinek tömeges létrehozásánál: nincs megadva minden küldeményhez a tértivevény ragszám!
                throw new ResultException(53813);
            }
            #endregion Paraméterek ellenőrzése

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //Vonalkódok ellenőrzése
            //  -> a tértivevényből származik, nem kötjük, és nem vizsgáljuk, csak másoljuk

            result = sp.InsertTertivevenyTomeges(ExecParam, Kuldemeny_Id_List, BarCode_List, Record, ExecutionTime);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            //vonalkód kötése, ha meg volt adva
            //  -> a tértivevényből származik, nem kötjük, és nem vizsgáljuk, csak másoljuk


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                #region Id-k visszaolvasása eseménynaplóhoz
                string ids = null;
                if (result.Ds != null)
                {
                    System.Collections.Generic.List<string> lstIds = new System.Collections.Generic.List<string>(result.Ds.Tables[0].Rows.Count);
                    foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        if (lstIds.Contains(id) == false)
                        {
                            lstIds.Add(id);
                        }
                    }

                    ids = Search.GetSqlInnerString(lstIds.ToArray());
                }
                #endregion Id-k visszaolvasása eseménynaplóhoz

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, ids, "KuldemenyMellekletNew");
                if (eventLogResult.IsError)
                    Logger.Debug("[ERROR]EREC_Mellekletek::InsertTertivevenyTomeges: ", ExecParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}
