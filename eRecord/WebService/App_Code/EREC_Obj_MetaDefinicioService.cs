using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Web.Script.Services;


[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_Obj_MetaDefinicioService : System.Web.Services.WebService
{
    /// <summary>
    /// Adott felt�teleknek megfelel� rekordok lek�rdez�se a t�bl�b�l.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_Obj_MetaDefinicioSearch"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Obj_MetaDefinicioSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Obj_MetaDefinicioSearch _EREC_Obj_MetaDefinicioSearch)
    {
        return GetAllWithExtensionNoLoop(ExecParam, _EREC_Obj_MetaDefinicioSearch, null, null);
    }

    /// <summary>
    /// Adott felt�teleknek megfelel� rekordok lek�rdez�se a t�bl�b�l. K�l�n param�terrel kiz�rhat�k a felettesk�nt k�rt okoz� rekordok.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_Obj_MetaDefinicioSearch"></param>
    /// <param name="NoAncestorLoopWithId">Ha meg van adva, az ehhez a rekordhoz felettesk�nt nem rendelhet� rekordok (�nmaga �s lesz�rmazottai) nem ker�lnek az eredm�nyhalmazba.</param>
    /// <param name="NoSuccessorLoopWithId">Ha meg van adva, az ehhez a rekordhoz al�rendeltk�nt nem rendelhet� rekordok (�nmaga �s felettesei) nem ker�lnek az eredm�nyhalmazba.</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Obj_MetaDefinicioSearch))]
    public Result GetAllWithExtensionNoLoop(ExecParam ExecParam, EREC_Obj_MetaDefinicioSearch _EREC_Obj_MetaDefinicioSearch, string NoAncestorLoopWithId, string NoSuccessorLoopWithId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_Obj_MetaDefinicioSearch, NoAncestorLoopWithId, NoSuccessorLoopWithId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// IratMetaDefinicioTerkephez meghat�rozza alulr�l felfel� hierarchikusan
    /// azon objektum Id-ket, melyek megfelelenek az �tadott keres�si felt�teleknek
    /// Ha nincs megadva felt�tel, �res t�bl�t ad vissza az �sszes Id helyett,
    /// az als�bb szinteken megfogalmazott felt�telekb�l sz�rmaz� sz�r�si
    /// t�bl�kkal akkor is megsz�ri a magasabb szinteket, ha azokra nincs egy�b
    /// felt�tel
    /// Nyilv�ntartja, ha egy szinten a felt�tel mellett m�r nem volt tal�lat,
    /// �gy megk�l�nb�ztethet� a felt�tel n�lk�li �s a felt�tel miatt �res
    /// sz�r�t�bla, �s nincs tov�bbi felesleges lek�r�s
    /// Visszaadott �rt�kek:
    ///    Table[0]: IsFilteredWithoutHit: '1', ha valamely felt�tel miatt nem volt tal�lat, 1 egy�bk�nt
    ///    Sz�r�t�bl�k (�res vagy Id-k, rendezve)
    ///    Table[1]: EREC_AgazatiJelek
    ///    Table[2]: EREC_IrattariTetelek
    ///    Table[3]: EREC_IratMetaDefinicio (�sszes t�nylegesen illeszked� rekord - a sz�l�k nem: Szint = 0)
    ///    Table[4]: EREC_IratMetaDefinicio (Irattipus, EljarasiSzakasz: null; Ugytipus: not null)
    ///    Table[5]: EREC_IratMetaDefinicio (Irattipus: null; EljarasiSzakasz: not null)
    ///    Table[6]: EREC_IratMetaDefinicio (Irattipus: not null)
    ///    Table[7]: EREC_ObjMetaDefinicio
    ///    Table[8]: EREC_ObjMetaAdatai
    ///    Table[9]: EREC_TargySzavak
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_MetaAdatHierarchiaSearch">Keres�si objektum, mely az EREC_AgazatiJelek, EREC_IrattariTetelek,
    ///    EREC_IratMetaDefinicio, EREC_ObjMetaDefinicio, EREC_ObjMetaAdatai, EREC_TargySzavak t�bl�kra vonatkoz� keres�si
    ///    objektumokat fogja egybe
    /// </param>
    /// <param name="bCsakSajatSzint">Ha �rt�ke True, a felettes objektum metadefin�ci�k (A0 defin�ci�t�pus) nem ker�lnek figyelembev�telre.
    /// Ha �rt�ke False, azon C2 t�pus� metadefin�ci�k is beker�lnek a sz�r�be, melyek feletteseihez vannak a felt�teleket teljes�t�
    /// t�rgyszavak, objektum metadatok k�tve</param>
    /// <returns>Hiba vagy a Record �rt�ke true, ha a sz�r�si felt�telek miatt nem volt tal�lat, egy�bk�nt false, a DS pedig
    /// a sz�r�s eredm�nyek�pp kapott Id-kb�l �ll� t�bl�kat tartalmazza (�resek, he nem volt felt�tel)</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(MetaAdatHierarchiaSearch))]
    public Result GetAllMetaAdatokHierarchiaFilter(ExecParam ExecParam, MetaAdatHierarchiaSearch _MetaAdatHierarchiaSearch, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllMetaAdatokHierarchiaFilter(ExecParam, _MetaAdatHierarchiaSearch, bCsakSajatSzint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// SetMetaFromFelettes(ExecParam ExecParam)
    /// Az ExecParam Record_Id alapj�n kijel�lt objektum metadefin�ci� minden hierarchikusan f�l�rendelt
    /// objektum metadefin�ci�j�b�l �tm�solja az objektum metaadatokat, majd t�rli a felettes hozz�rendel�s�t.
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az ExecParam Record_Id alapj�n kijel�lt objektum metadefin�ci� minden hierarchikusan f�l�rendelt objektum metadefin�ci�j�b�l �tm�solja az objektum metaadatokat, majd t�rli a felettes hozz�rendel�s�t. ")]
    public Result SetMetaFromFelettes(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result result_get = sp.Get(ExecParam);

            if (!String.IsNullOrEmpty(result_get.ErrorCode))
            {
                throw new ResultException(result_get);
            }

            EREC_Obj_MetaDefinicio erec_Obj_MetaDefinicio = (EREC_Obj_MetaDefinicio)result_get.Record;

            if (erec_Obj_MetaDefinicio != null)
            {
                String RecordId = ExecParam.Record_Id;
                String Felettes_Obj_Meta = erec_Obj_MetaDefinicio.Felettes_Obj_Meta;

                if (!String.IsNullOrEmpty(Felettes_Obj_Meta))
                {
                    EREC_Obj_MetaAdataiService service_oma = new EREC_Obj_MetaAdataiService(dataContext);
                    ExecParam execParam_omd_felettes = ExecParam.Clone();
                    ExecParam execParam_oma = ExecParam.Clone();

                    while (!String.IsNullOrEmpty(Felettes_Obj_Meta))
                    {
                        execParam_omd_felettes.Record_Id = Felettes_Obj_Meta;
                        result_get = sp.Get(execParam_omd_felettes);

                        if (!String.IsNullOrEmpty(result_get.ErrorCode))
                        {
                            throw new ResultException(result_get);
                        }

                        EREC_Obj_MetaDefinicio omd_Felettes = (EREC_Obj_MetaDefinicio)result_get.Record;

                        if (omd_Felettes != null)
                        {
                            // minden olyan objektum metaadatai rekord, ami a feletteshez van k�tve
                            EREC_Obj_MetaAdataiSearch search_oma = new EREC_Obj_MetaAdataiSearch();
                            search_oma.Obj_MetaDefinicio_Id.Value = Felettes_Obj_Meta;
                            search_oma.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;

                            Result result_getall_oma = service_oma.GetAll(execParam_oma, search_oma);

                            if (!String.IsNullOrEmpty(result_getall_oma.ErrorCode))
                            {
                                throw new ResultException(result_getall_oma);
                            }

                            // minden objektum metaadatai rekordot lem�solunk �s az aktu�lis objektum metadefin�ci�hoz kapcsoljuk
                            foreach (System.Data.DataRow row in result_getall_oma.Ds.Tables[0].Rows)
                            {
                                String oma_id = row["Id"].ToString();

                                if (!String.IsNullOrEmpty(oma_id))
                                {

                                    EREC_Obj_MetaAdatai erec_Obj_MetaAdatai = new EREC_Obj_MetaAdatai();
                                    Utility.LoadBusinessDocumentFromDataRow(erec_Obj_MetaAdatai, row);

                                    if (erec_Obj_MetaAdatai != null)
                                    {
                                        // ellen�rizz�k, hogy nincs-e m�r ilyen
                                        EREC_Obj_MetaAdataiSearch search_oma_same = new EREC_Obj_MetaAdataiSearch();
                                        search_oma_same.Obj_MetaDefinicio_Id.Value = RecordId;
                                        search_oma_same.Obj_MetaDefinicio_Id.Operator = Query.Operators.equals;

                                        search_oma_same.Targyszavak_Id.Value = erec_Obj_MetaAdatai.Targyszavak_Id;
                                        search_oma_same.Targyszavak_Id.Operator = Query.Operators.equals;

                                        search_oma_same.Funkcio.Value = erec_Obj_MetaAdatai.Funkcio;
                                        search_oma_same.Funkcio.Operator = Query.Operators.equals;

                                        search_oma_same.Opcionalis.Value = erec_Obj_MetaAdatai.Opcionalis;
                                        search_oma_same.Opcionalis.Operator = Query.Operators.equals;

                                        search_oma_same.Ismetlodo.Value = erec_Obj_MetaAdatai.Ismetlodo;
                                        search_oma_same.Ismetlodo.Operator = Query.Operators.equals;

                                        Result result_oma_same_getall = service_oma.GetAll(execParam_oma, search_oma_same);
                                        if (!String.IsNullOrEmpty(result_oma_same_getall.ErrorCode))
                                        {
                                            throw new ResultException(result_oma_same_getall);
                                        }

                                        // csak akkor mentj�k, ha nem volt m�g ilyen
                                        if (result_oma_same_getall.Ds.Tables[0].Rows.Count == 0)
                                        {
                                            erec_Obj_MetaAdatai.Id = "";
                                            erec_Obj_MetaAdatai.Updated.Id = true;

                                            erec_Obj_MetaAdatai.Obj_MetaDefinicio_Id = RecordId;
                                            erec_Obj_MetaAdatai.Updated.Obj_MetaDefinicio_Id = true;

                                            Result result_insert_oma = service_oma.Insert(execParam_oma, erec_Obj_MetaAdatai);

                                            if (!String.IsNullOrEmpty(result_insert_oma.ErrorCode))
                                            {
                                                throw new ResultException(result_insert_oma);
                                            }
                                        }
                                    }
                                }
                            }

                            Felettes_Obj_Meta = omd_Felettes.Felettes_Obj_Meta;
                        }

                    }

                    // felettes kapcsolat t�rl�se, ha volt
                    erec_Obj_MetaDefinicio.Felettes_Obj_Meta = "";
                    erec_Obj_MetaDefinicio.Updated.Felettes_Obj_Meta = true;

                    result = sp.Insert(Constants.Update, ExecParam, erec_Obj_MetaDefinicio);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetContentTypeList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("GetContentTypeList webservice elindul");
        Logger.Debug(String.Format("Param�terek: prefixText {0}, count: {1}, contextKey {2}", prefixText, count, contextKey));

        String userId = contextKey.Split(';')[0];

        Logger.Debug("Felhaszn�l� id: " + userId);

        String definicioTipusFilter = contextKey.Split(';')[1];
        String noAncestorLoopWithId = contextKey.Split(';')[2];
        String noSuccessorLoopWithId = contextKey.Split(';')[3];

        if (!String.IsNullOrEmpty(definicioTipusFilter))
        {
            Logger.Debug("Defi�ci� t�pus filter: " + definicioTipusFilter);
        }

        if (!String.IsNullOrEmpty(noAncestorLoopWithId))
        {
            Logger.Debug("Id a k�rt okoz� felettesek kiz�r�s�hoz: " + noAncestorLoopWithId);
        }
        
        if (!String.IsNullOrEmpty(noSuccessorLoopWithId))
        {        
            Logger.Debug("Id a k�rt okoz� al�rendeltek kiz�r�s�hoz: " + noSuccessorLoopWithId);
        }

        if (!String.IsNullOrEmpty(prefixText))
        {
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            EREC_Obj_MetaDefinicioSearch search = new EREC_Obj_MetaDefinicioSearch();

            if (!String.IsNullOrEmpty(definicioTipusFilter))
            {
                search.DefinicioTipus.Value = definicioTipusFilter;
                search.DefinicioTipus.Operator = Query.Operators.equals;
            }

            search.OrderBy = " ContentType " + Utility.customCollation + " ASC";
            search.ContentType.Value = prefixText + '%';
            search.ContentType.Operator = Query.Operators.like;
            search.TopRow = count;

            Result res = this.GetAllWithExtensionNoLoop(execparam, search, noAncestorLoopWithId, noSuccessorLoopWithId);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Debug("GetContentTypeList webservice v�ge");
                Logger.Debug("A visszat�rt elemek sz�ma: " + res.Ds.Tables[0].Rows.Count);
                Logger.DebugEnd();
                return ContentTypeDataSourceToStringPairArray(res);
            }
            else
            {
                Logger.Error(String.Format("GetAllWithExtension webservice hiba: Hibakod: {0}, Hibauzenet: {1} ", res.ErrorCode, res.ErrorMessage));
            }
        }

        Logger.Debug("GetContentTypeList webservice v�ge null visszat�r�si �rt�kkel");
        Logger.DebugEnd();
        return null;

    }

    private static string[] ContentTypeDataSourceToStringPairArray(Result res)
    {
        const string delimeter = ";";
        if (res.Ds != null)
        {
            string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
            string value = "";
            string text = "";
            for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
            {
                value = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                value += delimeter + res.Ds.Tables[0].Rows[i]["DefinicioTipus"].ToString();
                value += delimeter + res.Ds.Tables[0].Rows[i]["SPSSzinkronizalt"].ToString();
                text = res.Ds.Tables[0].Rows[i]["ContentType"].ToString();
                string item = Utility.CreateAutoCompleteItem(text, value);
                StringArray.SetValue(item, i);
            }
            return StringArray;
        }
        else
        {
            return null;
        }
    }


}