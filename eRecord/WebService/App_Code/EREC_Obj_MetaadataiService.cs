using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_Obj_MetaAdataiService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Obj_MetaAdataiSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_Obj_MetaAdataiSearch _EREC_Obj_MetaAdataiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_Obj_MetaAdataiSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Obj_MetaAdataiSearch))]
    public Result GetAllWithExtensionByObjMetaDefinicio(ExecParam ExecParam, EREC_Obj_MetaAdataiSearch _EREC_Obj_MetaAdataiSearch, String Obj_MetaDefinicio_Id, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (String.IsNullOrEmpty(Obj_MetaDefinicio_Id))
            {
                result.ErrorCode = "[56200]";
                throw new ResultException(result);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionByObjMetaDefinicio(ExecParam, _EREC_Obj_MetaAdataiSearch, Obj_MetaDefinicio_Id, bCsakSajatSzint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}