using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

using log4net;
using log4net.Config;

using System.Collections.Generic;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_ObjektumTargyszavaiService : System.Web.Services.WebService
{
	
	private static readonly ILog log = LogManager.GetLogger(typeof(Logger));

    // B1 t�pus� metadefin�ci�k alapj�n t�rt�n� lek�rdez�sek eredm�ny�nek cache-el�s�hez
    String cache_key_allMetaByDefinicioTipus = "AllMetaByDefinicioTipusDictionary";

    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_ObjektumTargyszavaiSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetAllWithExtension(ExecParam, _EREC_ObjektumTargyszavaiSearch);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_ObjektumTargyszavaiSearch))]
    public Result GetAllWithExtensionIratEsUgyirat(ExecParam ExecParam, string Irat_Id, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region Irat hierarchia lek�r�s

            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
            ExecParam execParam_GetIratHierarchia = ExecParam.Clone();
            execParam_GetIratHierarchia.Record_Id = Irat_Id;

            Result result_GetIratHierarchia = service_iratok.GetIratHierarchia(execParam_GetIratHierarchia);
            if (!String.IsNullOrEmpty(result_GetIratHierarchia.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_GetIratHierarchia);
            }

            IratHierarchia iratHierarchia = (IratHierarchia)result_GetIratHierarchia.Record;
            string ugyiratdarab_Id = iratHierarchia.UgyiratDarabObj.Id;
            string ugyirat_Id = iratHierarchia.UgyiratObj.Id;
            
            #endregion Irat hierarchia lek�r�s

            Result result_irat = sp.GetAllMetaByDefinicioTipus(ExecParam
                , _EREC_ObjektumTargyszavaiSearch
                , Irat_Id
                , null
                , "EREC_IraIratok"
                , null
                , false
                , false
                );
            
            if (!String.IsNullOrEmpty(result_irat.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_irat);
            }

            // �r�k�lt metaadatok k�z�l csak a t�nyleges hozz�rendel�seket k�rj�k le
            // ehhez egy "priv�t" search objektumot haszn�lunk
            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
            search.OrderBy = _EREC_ObjektumTargyszavaiSearch.OrderBy;
            search.WhereByManual = " Id is not null";

            Result result_ugyiratdarab = sp.GetAllMetaByDefinicioTipus(ExecParam
                , search
                , ugyiratdarab_Id
                , null
                , "EREC_UgyUgyiratdarabok"
                , null
                , false
                , false
                );

            if (!String.IsNullOrEmpty(result_ugyiratdarab.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratdarab);
            }

            Result result_ugyirat = sp.GetAllMetaByDefinicioTipus(ExecParam
                , search
                , ugyirat_Id
                , null
                , "EREC_UgyUgyiratok"
                , null
                , false
                , false
                );

            if (!String.IsNullOrEmpty(result_ugyirat.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyirat);
            }

            result.Ds = new DataSet();
            result_irat.Ds.Tables[0].TableName = "EREC_IraIratok";
            result_ugyiratdarab.Ds.Tables[0].TableName = "EREC_UgyUgyiratdarabok";
            result_ugyirat.Ds.Tables[0].TableName = "EREC_UgyUgyiratok";

            result.Ds.Tables.Add(result_irat.Ds.Tables[0].Copy());
            result.Ds.Tables.Add(result_ugyiratdarab.Ds.Tables[0].Copy());
            result.Ds.Tables.Add(result_ugyirat.Ds.Tables[0].Copy());

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    // besz�rja a rekordokat az eredm�nyt�bla sorai alapj�n
    private Result InsertByGetAllMetaDataTable(ExecParam ExecParam, string Obj_Id, DataTable DataTable_AllMeta)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (DataTable_AllMeta != null && DataTable_AllMeta.Rows.Count > 0)
            {
                foreach (DataRow row in DataTable_AllMeta.Rows)
                {
                    EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                    erec_ObjektumTargyszavai.Updated.SetValueAll(false);
                    erec_ObjektumTargyszavai.Base.Updated.SetValueAll(false);

                    erec_ObjektumTargyszavai.Obj_Id = Obj_Id;
                    erec_ObjektumTargyszavai.Updated.Obj_Id = true;

                    erec_ObjektumTargyszavai.ObjTip_Id = row["ObjTip_Id"].ToString();
                    erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                    erec_ObjektumTargyszavai.Obj_Metaadatai_Id = row["Obj_Metaadatai_Id"].ToString();
                    erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                    erec_ObjektumTargyszavai.Targyszo_Id = row["Targyszo_Id"].ToString();
                    erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                    erec_ObjektumTargyszavai.Targyszo = row["Targyszo"].ToString();
                    erec_ObjektumTargyszavai.Updated.Targyszo = true;

                    erec_ObjektumTargyszavai.Forras = row["Forras"].ToString();
                    erec_ObjektumTargyszavai.Updated.Forras = true;

                    erec_ObjektumTargyszavai.Sorszam = row["Sorszam"].ToString();
                    erec_ObjektumTargyszavai.Updated.Sorszam = true;

                    erec_ObjektumTargyszavai.ErvKezd = System.DateTime.Now.ToString();
                    erec_ObjektumTargyszavai.Updated.ErvKezd = true;

                    erec_ObjektumTargyszavai.Base.Note = row["Note"].ToString();
                    erec_ObjektumTargyszavai.Base.Updated.Note = true;

                    Result result_insert = sp.Insert(Constants.Insert, ExecParam, erec_ObjektumTargyszavai);

                    if (!String.IsNullOrEmpty(result_insert.ErrorCode))
                    {
                        throw new ResultException(result_insert);
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;    
    }


    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AssignByDefinicioTipus(ExecParam ExecParam, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String DefinicioTipus, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        //bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            //isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // az objektum azon t�rgyszavai, melyek objektum metadefin�ci� alapj�n
            // hozz�rendelend�k, �s nem tartozik �rt�k hozz�juk
            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
            search.OrderBy = "Sorszam";
            search.WhereByManual = "Tipus = '0' and Id is null";

            Result result_GetAllMetaByDefinicioTipus = GetAllMetaByDefinicioTipus(ExecParam
                , search, Obj_Id, ObjTip_Id, ObjTip_Kod, DefinicioTipus, bCsakSajatSzint, true);

            if (!String.IsNullOrEmpty(result_GetAllMetaByDefinicioTipus.ErrorCode))
            {
                throw new ResultException(result_GetAllMetaByDefinicioTipus);
            }

            result = InsertByGetAllMetaDataTable(ExecParam, Obj_Id, result_GetAllMetaByDefinicioTipus.Ds.Tables[0]);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            //// COMMIT
            //dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            //dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// 1.) EREC_Obj_MetaDefinicio t�bla defin�ci�i alapj�n egy adott t�pus� (pl.irat, �gyirat, dokumentum)
    /// objektum k�telez� metaadatainak hozz�rendel�se
    /// 2.) Ha a @CsakSajatSzint �rt�ke '1', csak a saj�t szinten t�rt�nik hozz�rendel�s,
    /// ha pedig az �rt�k '0', minden szinten, a Felettes_Obj_Meta k�vet�s�vel
    /// Csak azokat a hozz�rendel�seket hajtja v�gre, melyek m�g nem l�teznek
    /// </summary>
    /// <param name="ExecParam">Felhaszn�l� Id</param>
    /// <param name="Obj_Id">Az objektum Id-ja, amelyhez at t�rgyszavakat/metaadatokat rendelj�k. K�telez�.</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez a t�rgyszavakat/metaadatokat rendelj�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez a t�rgyszavakat/metaadatokat rendelj�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="ContentType">Egy�rtelm�en azonos�t egy metadefin�ci�t. K�telez�.</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek hozz�rendel�sre.</param>
    /// <returns>Az �rv�nytelen�tett rekordok halmaza vagy hibak�d.</returns>
    /// <returns></returns>
    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AssignByContentType(ExecParam ExecParam, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String ContentType, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        //bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            //isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // az objektum azon t�rgyszavai, melyek objektum metadefin�ci� alapj�n
            // hozz�rendelend�k, �s nem tartozik �rt�k hozz�juk
            EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
            search.OrderBy = "Sorszam";
            search.WhereByManual = "Tipus = '0' and Id is null";

            Result result_GetAllMetaByContentType = GetAllMetaByContentType(ExecParam, search, Obj_Id, ObjTip_Id, ObjTip_Kod, ContentType, bCsakSajatSzint, true);

            if (!String.IsNullOrEmpty(result_GetAllMetaByContentType.ErrorCode))
            {
                throw new ResultException(result_GetAllMetaByContentType);
            }

            result = InsertByGetAllMetaDataTable(ExecParam, Obj_Id, result_GetAllMetaByContentType.Ds.Tables[0]);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //// COMMIT
            //dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            //dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Standard t�rgysz� �rt�kek hozz�rendel�se objektumhoz list�ban �tadott �rt�kek alapj�n.
    /// A (f�)rekordot az Id, vagy Obj_Id, ObjTip_Id �s Obj_MetaAdatai_Id alapj�n azonos�tja.
    /// Csak automatikus hozz�rendel�seket vesz figyelembe.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_ObjektumTargyszavai">A kit�ltend� �rt�keket tartalmaz� objektumlista. A rekordoknak azonos�that�aknak kell lenni�k Id vagy Obj_MetaAdatai_Id alapj�n.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <returns></returns>
    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    public Result SetValuesFromListAllMetaByDefinicioTipus(ExecParam ExecParam, EREC_ObjektumTargyszavai[] _EREC_ObjektumTargyszavai, string Obj_Id, string ObjTip_Id, string ObjTip_Kod, string DefinicioTipus, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (String.IsNullOrEmpty(Obj_Id))
            {
                throw new ResultException(63102); // Hiba a t�rgyszavak k�t�s�n�l: Hi�nyz� param�ter! Az objektum azonos�t� �rt�ke null.
            }

            if (String.IsNullOrEmpty(ObjTip_Id) && String.IsNullOrEmpty(ObjTip_Kod))
            {
                throw new ResultException(63103); // Hiba a t�rgyszavak k�t�s�n�l: Hi�nyz� param�ter! Az objektum t�pus nem azonos�that�.
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (_EREC_ObjektumTargyszavai != null)
            {
                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
                search.OrderBy = "Sorszam";
                ExecParam execparam_GetAllMetaByDefinicioTipus = ExecParam.Clone();

                Result result_GetAllMetaByDefinicioTipus = GetAllMetaByDefinicioTipus(ExecParam
                    , search
                    , Obj_Id
                    , ObjTip_Id
                    , ObjTip_Kod
                    , DefinicioTipus
                    , bCsakSajatSzint
                    , true
                    );

                if (!String.IsNullOrEmpty(result_GetAllMetaByDefinicioTipus.ErrorCode))
                {
                    throw new ResultException(result_GetAllMetaByDefinicioTipus);
                }

                DataTable tableMeta = result_GetAllMetaByDefinicioTipus.Ds.Tables[0];

                if (tableMeta.Rows.Count > 0)// ha nincs k�t�s, nincs mit kit�lteni...
                {

                    foreach (EREC_ObjektumTargyszavai item in _EREC_ObjektumTargyszavai)
                    {
                        // megkeress�k a metaadat hozz�rendel�s�nek sor�t
                        //int i = 0;
                        //if (!String.IsNullOrEmpty(item.Id))
                        //{
                        //    while (i < tableMeta.Rows.Count && tableMeta.Rows[i]["Id"].ToString() != item.Id)
                        //        i++;
                        //}
                        //else if (!String.IsNullOrEmpty(item.Obj_Metaadatai_Id))
                        //{
                        //    while (i < tableMeta.Rows.Count && tableMeta.Rows[i]["Obj_MetaAdatai_Id"].ToString() != item.Obj_Metaadatai_Id)
                        //        i++;
                        //}

                        DataRow[] rows = null;
                        if (!String.IsNullOrEmpty(item.Id))
                        {
                            rows = tableMeta.Select("Id='" + item.Id + "'");
                        }
                        else if (!String.IsNullOrEmpty(item.Obj_Metaadatai_Id))
                        {
                            rows = tableMeta.Select("Obj_MetaAdatai_Id='" + item.Obj_Metaadatai_Id + "'");
                        }

                        if (rows == null || rows.Length == 0)
                        //if (i > tableMeta.Rows.Count) // helyesen if (i >= tableMeta.Rows.Count)
                        {
                            if (!String.IsNullOrEmpty(item.Id))
                            {
                                throw new ResultException(63104); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! Nem l�tezik a megadott azonos�t�j� hozz�rendel�s.
                            }
                            else
                            {
                                throw new ResultException(63105); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! Nem l�tezik hozz�rendel�s a megadott objektum metadat alapj�n.
                            }

                        }
                        else if (rows.Length > 1)
                        {
                            if (!String.IsNullOrEmpty(item.Id))
                            {
                                throw new ResultException(63106); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! T�bb hozz�rendel�s is l�tezik a megadott azonos�t�val.
                            }
                            else
                            {
                                throw new ResultException(63107); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! T�bb hozz�rendel�s is l�tezik a megadott objektum metadat alapj�n.
                            }
                        }

                        DataRow row = rows[0];

                        if (row["Ismetlodo"].ToString() != "1" && String.IsNullOrEmpty(item.Id) && !String.IsNullOrEmpty(row["Id"].ToString()))
                        {
                            //throw new Exception(String.Format("A t�rgysz� ({0}) m�r hozz� van rendelve az objekzumhoz!", row["Targyszo"]));
                            item.Id = row["Id"].ToString();
                        }

                        // csak a m�dos�that� �rt�keket m�soljuk
                        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = item;

                        erec_ObjektumTargyszavai.Obj_Id = Obj_Id;
                        erec_ObjektumTargyszavai.Updated.Obj_Id = true;

                        erec_ObjektumTargyszavai.ObjTip_Id = row["ObjTip_Id"].ToString(); //tableMeta.Rows[i]["ObjTip_Id"].ToString();
                        erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                        Result result_item = null;

                        if (!String.IsNullOrEmpty(item.Id))
                        {
                            ExecParam.Record_Id = item.Id;

                            erec_ObjektumTargyszavai.Base.Ver = row["Ver"].ToString(); //tableMeta.Rows[i]["Ver"].ToString();
                            erec_ObjektumTargyszavai.Base.Updated.Ver = true;

                            result_item = sp.Insert(Constants.Update, ExecParam, erec_ObjektumTargyszavai);
                            result_item.CheckError();
                        }
                        else
                        {
                            // �res �rt�ket nem l�tez� hozz�rendel�shez nem visz�nk fel, ha az �rt�k k�telez�
                            // if (!String.IsNullOrEmpty(item.Ertek) || tableMeta.Rows[i]["Tipus"] == "0")
                            if (!String.IsNullOrEmpty(item.Ertek) || row["Tipus"] == "0")
                            {
                                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = row["Obj_Metaadatai_Id"].ToString(); //tableMeta.Rows[i]["Obj_Metaadatai_Id"].ToString();
                                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                                erec_ObjektumTargyszavai.Targyszo_Id = row["Targyszo_Id"].ToString(); //tableMeta.Rows[i]["Targyszo_Id"].ToString();
                                erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                                erec_ObjektumTargyszavai.Targyszo = row["Targyszo"].ToString(); //tableMeta.Rows[i]["Targyszo"].ToString();
                                erec_ObjektumTargyszavai.Updated.Targyszo = true;

                                if (item.Updated.Sorszam == false)
                                {
                                    erec_ObjektumTargyszavai.Sorszam = row["Sorszam"].ToString(); //tableMeta.Rows[i]["Sorszam"].ToString();
                                    erec_ObjektumTargyszavai.Updated.Sorszam = true;
                                }

                                if (item.Updated.ErvKezd == false)
                                {
                                    erec_ObjektumTargyszavai.ErvKezd = row["ErvKezd"].ToString(); //tableMeta.Rows[i]["ErvKezd"].ToString();
                                    erec_ObjektumTargyszavai.Updated.ErvKezd = true;
                                }

                                if (item.Updated.ErvVege == false)
                                {
                                    erec_ObjektumTargyszavai.ErvVege = row["ErvVege"].ToString(); //tableMeta.Rows[i]["ErvVege"].ToString();
                                    erec_ObjektumTargyszavai.Updated.ErvVege = true;
                                }

                                if (item.Base.Updated.LetrehozasIdo == false)
                                {
                                    erec_ObjektumTargyszavai.Base.LetrehozasIdo = System.DateTime.Now.ToString();
                                    erec_ObjektumTargyszavai.Base.Updated.LetrehozasIdo = true;
                                }

                                if (item.Base.Updated.Letrehozo_id == false)
                                {
                                    erec_ObjektumTargyszavai.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                                    erec_ObjektumTargyszavai.Base.Updated.Letrehozo_id = true;
                                }

                                erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;  // Automatikus
                                erec_ObjektumTargyszavai.Updated.Forras = true;

                                result_item = sp.Insert(Constants.Insert, ExecParam, erec_ObjektumTargyszavai);

                                if (!String.IsNullOrEmpty(result_item.ErrorCode))
                                {
                                    throw new ResultException(result_item);
                                }
                            }

                        }

                    }
                } // foreach
        
                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
            else
            {
                throw new ResultException(63101); //Hiba a t�rgyszavak k�t�s�n�l: Hi�nyz� param�ter! A lista objektum �rt�ke null.
            }
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Standard t�rgysz� �rt�kek hozz�rendel�se objektumhoz list�ban �tadott �rt�kek alapj�n.
    /// A (f�)rekordot az Id, vagy Obj_Id, ObjTip_Id �s Obj_MetaAdatai_Id alapj�n azonos�tja.
    /// Bemen� param�terek az objektum metadefin�ci� meghat�roz�s�hoz:
    /// Objektum metadefin�ci� Id VAGY
    /// a) t�blan�v (t�bl�hoz k�t�tt objektum metadefin�ci�: B1 t�pus) vagy "*" (minden t�bla),
    /// b) t�blan�v + oszlopn�v (oszlop�rt�khez k�t�tt objektum metadefin�ci�k B2 vagy C2, mind) vagy "*" (ha a t�blan�v nem �res, akkor minden oszlop)
    /// c) t�blan�v + oszlopn�v + oszlop�rt�k (oszlop�rt�khez k�t�tt objektum metadefin�ci�, konkr�t)
    /// d) defin�ci� t�pus (Figyelmen k�v�l hagyva, ha az Objektum metadefin�ci� Id van megadva! A sz�r�s a nem automatikus elemekre nem vonatkozik!)
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavai">A kit�ltend� �rt�keket tartalmaz� objektumlista. A rekordoknak azonos�that�aknak kell lenni�k Id vagy Obj_MetaAdatai_Id alapj�n.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlopokat jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    /// <returns></returns>
    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    public Result InsertOrUpdateValuesByObjMetaDefinicio(ExecParam ExecParam
        , EREC_ObjektumTargyszavai[] _EREC_ObjektumTargyszavai, string Obj_Id, String ObjMetaDefinicio_Id
        , String TableName, String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
        , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (String.IsNullOrEmpty(Obj_Id))
            {
                throw new ResultException(63102); // Hiba a t�rgyszavak k�t�s�n�l: Hi�nyz� param�ter! Az objektum azonos�t� �rt�ke null.
            }

            if (String.IsNullOrEmpty(ObjMetaDefinicio_Id) && String.IsNullOrEmpty(TableName))
            {
                throw new ResultException(63103); // Hiba a t�rgyszavak k�t�s�n�l: Hi�nyz� param�ter! Az objektum t�pus nem azonos�that�.
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (_EREC_ObjektumTargyszavai != null)
            {
                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
                search.OrderBy = "Sorszam";
                ExecParam execparam_GetAllMetaByObjMetaDefinicio = ExecParam.Clone();

                Result result_GetAllMetaByObjMetaDefinicio = GetAllMetaByObjMetaDefinicio(ExecParam
                    , search
                    , Obj_Id
                    , ObjMetaDefinicio_Id
                    , TableName
                    , ColumnName
                    , ColumnValues
                    , bColumnValuesExclude
                    , DefinicioTipus
                    , bCsakSajatSzint
                    , bCsakAutomatikus
                    );

                if (!String.IsNullOrEmpty(result_GetAllMetaByObjMetaDefinicio.ErrorCode))
                {
                    throw new ResultException(result_GetAllMetaByObjMetaDefinicio);
                }

                DataTable tableMeta = result_GetAllMetaByObjMetaDefinicio.Ds.Tables[0];

                if (tableMeta.Rows.Count > 0)// ha nincs k�t�s, nincs mit kit�lteni...
                {

                    foreach (EREC_ObjektumTargyszavai item in _EREC_ObjektumTargyszavai)
                    {
                        // megkeress�k a metaadat hozz�rendel�s�nek sor�t
                        DataRow[] rows = null;
                        if (!String.IsNullOrEmpty(item.Id))
                        {
                            rows = tableMeta.Select("Id='" + item.Id + "'");
                        }
                        else if (!String.IsNullOrEmpty(item.Obj_Metaadatai_Id))
                        {
                            rows = tableMeta.Select("Obj_MetaAdatai_Id='" + item.Obj_Metaadatai_Id + "'");
                        }

                        if (rows == null || rows.Length == 0)
                        {
                            if (!String.IsNullOrEmpty(item.Id))
                            {
                                throw new ResultException(63104); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! Nem l�tezik a megadott azonos�t�j� hozz�rendel�s.
                            }
                            else
                            {
                                throw new ResultException(63105); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! Nem l�tezik hozz�rendel�s a megadott objektum metadat alapj�n.
                            }

                        }
                        else if (rows.Length > 1)
                        {
                            if (!String.IsNullOrEmpty(item.Id))
                            {
                                throw new ResultException(63106); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! T�bb hozz�rendel�s is l�tezik a megadott azonos�t�val.
                            }
                            else
                            {
                                throw new ResultException(63107); // Hiba a t�rgyszavak k�t�s�n�l: A rekord nem azonos�that�! T�bb hozz�rendel�s is l�tezik a megadott objektum metadat alapj�n.
                            }
                        }

                        DataRow row = rows[0];

                        // csak a m�dos�that� �rt�keket m�soljuk
                        EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = item;

                        erec_ObjektumTargyszavai.Obj_Id = Obj_Id;
                        erec_ObjektumTargyszavai.Updated.Obj_Id = true;

                        erec_ObjektumTargyszavai.ObjTip_Id = row["ObjTip_Id"].ToString();
                        erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                        Result result_item = null;

                        if (!String.IsNullOrEmpty(item.Id))
                        {
                            ExecParam.Record_Id = item.Id;

                            erec_ObjektumTargyszavai.Base.Ver = row["Ver"].ToString();
                            erec_ObjektumTargyszavai.Base.Updated.Ver = true;

                            result_item = sp.Insert(Constants.Update, ExecParam, erec_ObjektumTargyszavai);

                            if (!String.IsNullOrEmpty(result_item.ErrorCode))
                            {
                                throw new ResultException(result_item);
                            }

                        }
                        else
                        {
                            // �res �rt�ket nem l�tez� hozz�rendel�shez nem visz�nk fel, ha az �rt�k k�telez�
                            if (!String.IsNullOrEmpty(item.Ertek) || row["Tipus"] == "0")
                            {
                                erec_ObjektumTargyszavai.Obj_Metaadatai_Id = row["Obj_Metaadatai_Id"].ToString();
                                erec_ObjektumTargyszavai.Updated.Obj_Metaadatai_Id = true;

                                erec_ObjektumTargyszavai.Targyszo_Id = row["Targyszo_Id"].ToString();
                                erec_ObjektumTargyszavai.Updated.Targyszo_Id = true;

                                erec_ObjektumTargyszavai.Targyszo = row["Targyszo"].ToString();
                                erec_ObjektumTargyszavai.Updated.Targyszo = true;

                                if (item.Updated.Sorszam == false)
                                {
                                    erec_ObjektumTargyszavai.Sorszam = row["Sorszam"].ToString();
                                    erec_ObjektumTargyszavai.Updated.Sorszam = true;
                                }

                                if (item.Updated.ErvKezd == false)
                                {
                                    erec_ObjektumTargyszavai.ErvKezd = row["ErvKezd"].ToString();
                                    erec_ObjektumTargyszavai.Updated.ErvKezd = true;
                                }

                                if (item.Updated.ErvVege == false)
                                {
                                    erec_ObjektumTargyszavai.ErvVege = row["ErvVege"].ToString();
                                    erec_ObjektumTargyszavai.Updated.ErvVege = true;
                                }

                                if (item.Base.Updated.LetrehozasIdo == false)
                                {
                                    erec_ObjektumTargyszavai.Base.LetrehozasIdo = System.DateTime.Now.ToString();
                                    erec_ObjektumTargyszavai.Base.Updated.LetrehozasIdo = true;
                                }

                                if (item.Base.Updated.Letrehozo_id == false)
                                {
                                    erec_ObjektumTargyszavai.Base.Letrehozo_id = ExecParam.Felhasznalo_Id;
                                    erec_ObjektumTargyszavai.Base.Updated.Letrehozo_id = true;
                                }

                                if (item.Updated.Forras == false || String.IsNullOrEmpty(item.Forras))
                                {
                                    erec_ObjektumTargyszavai.Forras = KodTarak.TARGYSZO_FORRAS.Automatikus;  // Automatikus
                                    erec_ObjektumTargyszavai.Updated.Forras = true;
                                }

                                result_item = sp.Insert(Constants.Insert, ExecParam, erec_ObjektumTargyszavai);

                                if (!String.IsNullOrEmpty(result_item.ErrorCode))
                                {
                                    throw new ResultException(result_item);
                                }
                            }

                        }

                    }
                } // foreach

                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
            else
            {
                throw new ResultException(63101); //Hiba a t�rgyszavak k�t�s�n�l: Hi�nyz� param�ter! A lista objektum �rt�ke null.
            }
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// 1.)
    /// EREC_Obj_MetaDefinicio t�bla defin�ci�i alapj�n egy adott t�pus� (pl.irat vagy �gyirat)
    /// objektum k�telez� metaadatainak �rv�nytelen�t�se
    /// 2.)
    /// Ha a @CsakSajatSzint �rt�ke '1', csak a saj�t szinten t�rt�nik �rv�nytelen�t�s,
    /// ha pedig az �rt�k '0', minden szinten, a Felettes_Obj_Meta k�vet�s�vel
    /// 3.)
    /// Ha a @CsakErvenytelen �rt�ke '0', a metadefin�ci� alapj�n minden minden hozz�rendel�st �rv�nytelen�t (m�dos�t�s el�tt c�lszer�)
    /// ha pedig az �rt�k '1', megkeresi azon objektum metaadatokat, melyeket a jelenlegi �llapotban nem rendeln�nk hozz� (m�dos�t�s ut�n c�lszer�)
    /// </summary>
    /// <param name="ExecParam">Felhaszn�l� Id</param>
    /// <param name="Obj_Id">Az objektum Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. K�telez�.</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g, mely �rv�nytelen�t�seket kell elv�gezni - kcs:OBJMETADEFINICIO_TIPUS</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek �rv�nytelen�t�sre.</param>
    /// <param name="bCsakErvenytelen">Ha true, a metadefin�ci� alapj�n minden minden hozz�rendel�st �rv�nytelen�t (m�dos�t�s el�tt c�lszer�)
    /// ha pedig false, megkeresi azon objektum metaadatokat, melyeket a jelenlegi �llapotban nem rendeln�nk hozz� (m�dos�t�s ut�n c�lszer�)</param>
    /// <returns>Az �rv�nytelen�tett rekordok halmaza vagy hibak�d.</returns>
    public Result InvalidateByDefinicioTipus(ExecParam ExecParam, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String DefinicioTipus, bool bCsakSajatSzint, bool bCsakErvenytelen)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.InvalidateByDefinicioTipus(ExecParam, Obj_Id, ObjTip_Id, ObjTip_Kod, DefinicioTipus, bCsakSajatSzint, bCsakErvenytelen);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// 1.)
    /// EREC_Obj_MetaDefinicio t�bla defin�ci�i alapj�n egy adott t�pus� (pl.irat vagy �gyirat)
    /// objektum k�telez� metaadatainak �rv�nytelen�t�se
    /// 2.)
    /// Ha a @CsakSajatSzint �rt�ke '1', csak a saj�t szinten t�rt�nik �rv�nytelen�t�s,
    /// ha pedig az �rt�k '0', minden szinten, a Felettes_Obj_Meta k�vet�s�vel
    /// </summary>
    /// <param name="ExecParam">Felhaszn�l� Id</param>
    /// <param name="Obj_Id">Az objektum Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. K�telez�.</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat �rv�nytelen�tj�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="ContentType">Egy�rtelm�en azonos�t egy metadefin�ci�t. K�telez�.</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek �rv�nytelen�t�sre.</param>
    /// <returns>Az �rv�nytelen�tett rekordok halmaza vagy hibak�d.</returns>
    public Result InvalidateByContentType(ExecParam ExecParam, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String ContentType, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.InvalidateByContentType(ExecParam, Obj_Id, ObjTip_Id, ObjTip_Kod, ContentType, bCsakSajatSzint);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// T�rli az objektum t�pus f�gg� metaadatokat tartalmaz� cache-t az aktu�lis orgra
    /// (objektum metadefin�ci�, objektum metaadat m�dosul�sakor c�lszer� h�vni)
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "T�rli az objektum t�pus f�gg� metaadatokat tartalmaz� cache-t (objektum metadefin�ci�, objektum metaadat m�dosul�sakor c�lszer� h�vni)")]
    public void ClearObjectMetaCache(ExecParam execParam)
    {
        Context.Cache.Remove(cache_key_allMetaByDefinicioTipus + (execParam.Org_Id ?? ""));
    }

    /// <summary>
    /// Az EREC_ObjektumTargyszavaihoz hasonl� elnevez�ssel adja vissza az oszlopokat
    /// amelyek egy adott objektumt�pushoz k�telez�en tartoznak
    /// c�lja, hogy m�r akkor le lehessen k�rdezni a hozz�rendelend� t�rgyszavakat,
    /// amikor m�g nem ismert a konkr�t objektum Id, csak a t�pus
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_ObjektumTargyszavaiSearch))]
    public Result GetAllMetaByDefinicioTipus(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            if (DefinicioTipus != KodTarak.OBJMETADEFINICIO_TIPUS.B1 || !String.IsNullOrEmpty(Obj_Id))
            {
                // nem objektum szint� hozz�rendel�s, hagyom�nyos m�k�d�s, nincs cache
                result = sp.GetAllMetaByDefinicioTipus(ExecParam
                , _EREC_ObjektumTargyszavaiSearch
                , Obj_Id
                , ObjTip_Id
                , ObjTip_Kod
                , DefinicioTipus
                , bCsakSajatSzint
                , bCsakAutomatikus
                );
            }
            else
            {
                // B1 t�pus� defin�ci�k cache-el�se
                

                Dictionary<string, DataSet> AllMetaByDefinicioTipusDictionary = null;

                string keyCache = cache_key_allMetaByDefinicioTipus + (ExecParam.Org_Id ?? "");

                // key meghat�roz�sa a Dictionary sz�m�ra
                String ObjTip_Key = "";
                if (!String.IsNullOrEmpty(ObjTip_Kod))
                {
                    ObjTip_Key = ObjTip_Kod;
                }
                else
                {
                    ObjTip_Key = ObjTip_Id;
                }
                ObjTip_Key += (bCsakSajatSzint ? "_1" : "_0");
                ObjTip_Key += (bCsakAutomatikus ? "_1" : "_0");

                if (!String.IsNullOrEmpty(_EREC_ObjektumTargyszavaiSearch.OrderBy))
                {
                    ObjTip_Key += "_" + _EREC_ObjektumTargyszavaiSearch.OrderBy;
                }

                if (Context.Cache[keyCache] == null)
                {

                    AllMetaByDefinicioTipusDictionary = new Dictionary<string, DataSet>();

                    int AllMetaCache_RefreshPeriod_Minute =
                        Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.AllMetaCache_RefreshPeriod_Minute);
                    if (AllMetaCache_RefreshPeriod_Minute == 0)
                    {
                        AllMetaCache_RefreshPeriod_Minute = 600;
                    }

                    // Dictionary felv�tele a Cache-be:
                    Context.Cache.Insert(keyCache, AllMetaByDefinicioTipusDictionary, null
                            , DateTime.Now.AddMinutes(AllMetaCache_RefreshPeriod_Minute), System.Web.Caching.Cache.NoSlidingExpiration
                            , System.Web.Caching.CacheItemPriority.NotRemovable, null);

                    result = sp.GetAllMetaByDefinicioTipus(ExecParam
                        , _EREC_ObjektumTargyszavaiSearch
                        , Obj_Id
                        , ObjTip_Id
                        , ObjTip_Kod
                        , DefinicioTipus
                        , bCsakSajatSzint
                        , bCsakAutomatikus
                        );

                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result);
                    }
                    else
                    {
                        // Dictionary felt�lt�se:
                        try
                        {
                            if (!AllMetaByDefinicioTipusDictionary.ContainsKey(ObjTip_Key))
                            {
                                AllMetaByDefinicioTipusDictionary.Add(ObjTip_Key, result.Ds);
                            }

                        }
                        catch
                        {
                            // hiba:
                            result.ErrorCode = "Hiba a cache-el�sn�l (felv�tel)!";
                            throw new ResultException(result);
                        }

                        if (AllMetaByDefinicioTipusDictionary.ContainsKey(ObjTip_Key))
                        {
                            result.Ds = AllMetaByDefinicioTipusDictionary[ObjTip_Key];
                        }
                        else
                        {
                            result.ErrorCode = "Hiba a cache-el�sn�l (kiolvas�s)!";
                            throw new ResultException(result);
                        }
                    }
                }
                else
                {
                    // �rt�kek a Dictionary-b�l
                    AllMetaByDefinicioTipusDictionary = (Dictionary<string, DataSet>)Context.Cache[keyCache];

                    if (AllMetaByDefinicioTipusDictionary.ContainsKey(ObjTip_Key))
                    {
                        result.Ds = AllMetaByDefinicioTipusDictionary[ObjTip_Key];
                    }
                    else
                    {
                        // Nincs bent a Dictionary-ben, le kell k�rni az adatb�zisb�l:
                        result = sp.GetAllMetaByDefinicioTipus(ExecParam
                            , _EREC_ObjektumTargyszavaiSearch
                            , Obj_Id
                            , ObjTip_Id
                            , ObjTip_Kod
                            , DefinicioTipus
                            , bCsakSajatSzint
                            , bCsakAutomatikus
                            );

                        if (result != null && String.IsNullOrEmpty(result.ErrorCode))
                        {
                            AllMetaByDefinicioTipusDictionary.Add(ObjTip_Key, result.Ds);
                        }
                    }
                }
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Az EREC_ObjektumTargyszavaihoz hasonl� elnevez�ssel adja vissza az oszlopokat
    /// amelyek egy adott objektumt�pushoz k�telez�en tartoznak
    /// c�lja, hogy m�r akkor le lehessen k�rdezni a hozz�rendelend� t�rgyszavakat,
    /// amikor m�g nem ismert a konkr�t objektum Id, csak a t�pus
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjTip_Id">Az objektum t�pus�nak Id-ja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Kod, akkor k�telez�.</param>
    /// <param name="ObjTip_Kod">Az objektum t�pus�nak k�dja, amelyhez rendelt t�rgyszavakat/metaadatokat keress�k. Ha nincs megadva az ObjTip_Id, akkor k�telez�.</param>
    /// <param name="ContentType">Egy�rtelm�en azonos�t egy metadefin�ci�t. K�telez�.</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_ObjektumTargyszavaiSearch))]
    public Result GetAllMetaByContentType(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch, String Obj_Id, String ObjTip_Id, String ObjTip_Kod, String ContentType, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllMetaByContentType(ExecParam
                , _EREC_ObjektumTargyszavaiSearch
                , Obj_Id
                , ObjTip_Id
                , ObjTip_Kod
                , ContentType
                , bCsakSajatSzint
                , bCsakAutomatikus
                );

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region GetAllMetaByObjMetaDefinicio

    /// <summary>
    /// Az adott objektum elvben hozz�rendelend� metaadatait v�logatja le,
    /// �s ha m�r l�tezik a t�nyleges hozz�rendel�s, kieg�sz�ti annak adataival
    /// (az elvben hozz�rendelend� metaadatokat �sszef�s�li a t�nylegesen hozz�rendeltekkel)
    /// Az EREC_ObjektumTargyszavaihoz hasonl� elnevez�ssel adja vissza az oszlopokat.
    /// C�lja, hogy m�r akkor le lehessen k�rdezni a hozz�rendelend� t�rgyszavakat,
    /// amikor m�g nem ismert a konkr�t objektum Id, csak a t�pus
    /// Bemen� param�terek az objektum metadefin�ci� meghat�roz�s�hoz:
    /// Objektum metadefin�ci� Id VAGY
    /// a) t�blan�v (t�bl�hoz k�t�tt objektum metadefin�ci�: B1 t�pus),
    /// b) t�blan�v + oszlopn�v (oszlop�rt�khez k�t�tt objektum metadefin�ci�k B2 vagy C2, mind)
    /// c) t�blan�v + oszlopn�v + oszlop�rt�k (oszlop�rt�khez k�t�tt objektum metadefin�ci�, konkr�t)
    /// d) defin�ci� t�pus (Figyelmen k�v�l hagyva, ha az Objektum metadefin�ci� Id van megadva! A sz�r�s a nem automatikus elemekre nem vonatkozik!)
    /// A param�terek �ll�t�s�t�l f�gg�en a t�nylegesen (nem automatikusan) hozz�rendelt t�rgyszavak/metaadatok is lek�rhet�k
    /// </summary>
    /// <param name="ExecParam">A napl�z�shoz sz�ks�ges inform�ci�kat tartalmazza.</param>
    /// <param name="_EREC_ObjektumTargyszavaiSearch">Keres�si objektum, tov�bbi sz�r�si felt�telek megad�s�ra a kapcsolt t�rgyszavakra/metaadatokra vonatkoz�an.</param>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendelend�/hozz�rendelt t�rgyszavakat/metaadatokat keress�k.
    /// Ha nincs megadva, oszlop�rt�kvizsg�lat �s t�nyleges hozz�rendel�s lek�rdez�s nem v�gezhet�!</param>
    /// <param name="ObjMetaDefinicio_Id">Az objektum metadefin�ci� azonos�t�ja, amelyhez a kapcsolt t�rgyszavakat/metaadatokat keress�k.</param>
    /// <param name="TableName">Az objektum t�pus azonos�t�shoz a t�blan�v. Ha nincs megadva sem ObjMetaDefinicio_Id, sem DefinicioTipus, akkor k�telez�.</param>
    /// <param name="ColumnName">Ha a keresett objektum t�pus oszlop, akkor az oszlop neve. Csak megadott t�blan�vvel egy�tt haszn�lhat�!</param>
    /// <param param name="ColumnValues">Az adott oszlophoz kapcsolt objektum metadefin�ci�k k�r�nek sz�k�t�s�hez haszn�lt oszlop�rt�k(ek).</param>
    /// <param name="bColumnValuesExclude">A megadott oszlop�rt�k sz�r�s, ha van, a tartalmazott (false �rt�kn�l) vagy a kiz�rt (true �rt�kn�l) oszlopokat jelzik</param>
    /// <param name="DefinicioTipus">Sz�r�si lehet�s�g - kcs:OBJMETADEFINICIO_TIPUS (A0, B1, B2 vagy C2 lehet)</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten (A0, �r�k�lt) defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <param name="bCsakAutomatikus">Ha true, a nem automatikus (k�zi, list�s) t�nyleges hozz�rendel�sek lek�rdez�se nem t�rt�nik meg.</param>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Adott t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A t�bl�ra vonatkoz� sz�r�si felt�teleket param�ter tartalmazza (*Search). Az ExecParam.Record_Id nem haszn�lt, a tov�bbi adatok a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalmaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_ObjektumTargyszavaiSearch))]
    public Result GetAllMetaByObjMetaDefinicio(ExecParam ExecParam, EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch
        , String Obj_Id, String ObjMetaDefinicio_Id
        , String TableName, String ColumnName, String[] ColumnValues, bool bColumnValuesExclude
        , String DefinicioTipus, bool bCsakSajatSzint, bool bCsakAutomatikus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // nem objektum szint� hozz�rendel�s, hagyom�nyos m�k�d�s, nincs cache
            result = sp.GetAllMetaByObjMetaDefinicio(ExecParam
            , _EREC_ObjektumTargyszavaiSearch
            , Obj_Id
            , ObjMetaDefinicio_Id
            , TableName
            , ColumnName
            , ColumnValues
            , bColumnValuesExclude
            , DefinicioTipus
            , bCsakSajatSzint
            , bCsakAutomatikus
            );
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion GetAllMetaByObjMetaDefinicio


    //-----------------------------------------------------------------------------------
    // Teszt - SPS kit�lt�tt metaadatok �tv�tele Word XML-b�l
    //-----------------------------------------------------------------------------------
    #region Utils

    // throws ResultException...
    // KRT_Dokumentumok, EREC_UgyUgyiratok, EREC_UgyUgyiratDarabok, EREC_IraIratok
    private Dictionary<string, string> GetObjTipIds(ExecParam execParam, string[] ObjTip_KodArray)
    {
        Dictionary<string, string> ObjTipIds = new Dictionary<string, string>();
        
        string Kodok = "";
        if (ObjTip_KodArray != null && ObjTip_KodArray.Length > 0)
        {
            Kodok = String.Format("'{0}'", String.Join("','", ObjTip_KodArray));
        }
        else
        {
            return ObjTipIds;
        }

        // TODO: cache-b�l?
        // objektum Id-k lek�r�se
        Contentum.eAdmin.Service.KRT_ObjTipusokService service_ot = eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
        KRT_ObjTipusokSearch search_ot = new KRT_ObjTipusokSearch();
        ExecParam execParam_ot = execParam.Clone();

        search_ot.Kod.Value = Kodok;
        search_ot.Kod.Operator = Query.Operators.inner;
        Result result_ot = service_ot.GetAllWithExtension(execParam_ot, search_ot, Contentum.eUtility.Constants.FilterType.ObjTipusok.Tabla);

        if (String.IsNullOrEmpty(result_ot.ErrorCode))
        {
            foreach (System.Data.DataRow row in result_ot.Ds.Tables[0].Rows)
            {
                String Kod = row["Kod"].ToString();

                String Id = row["Id"].ToString();
                if (!String.IsNullOrEmpty(Id) && !ObjTipIds.ContainsKey(Kod))
                {
                    ObjTipIds.Add(Kod, Id);
                }
            }
        }
        else
        {
            throw new ResultException(result_ot);
        }

        return ObjTipIds;
    }
    #endregion Utils

    /// <summary>
    /// A Word fel�l XML-ben kapott ctt_w_... megjel�l�s� (Field.Name = BelsoAzonosito) t�rgyszavak k�t�se a ContentType alapj�n
    /// </summary>
    /// <param param name="Obj_Id">Az aktu�lis objektum azonos�t�ja, melyhez a hozz�rendel�st v�gezz�k.
    /// Ha irat v. �gyiratdarab, a feletteshez (pl. �gyirat) is rendelhet�nk, azt a metaadatok szintje szabja meg.</param>
    /// <param name="ObjTip_Kod">Pl. EREC_IraIratok (ekkor �gyirathierarchi�t n�z�nk) vagy KRT_Dokumentumok</param>
    /// <param name="WordMetaXML">A Word dokumentumban mentett metaadatok:
    ///     <metaadatok>
    ///         <metaadat azonosito="static_NAME1" nev="display1" ertek="1234XCVBNM" />
    ///         <metaadat azonosito="static_NAME2" nev="display2" ertek="kjhgKJH" />
    ///         ...
    ///     </metaadatok>
    ///     SPS sz�r�s: EREC_ObjektumTargyszavai t�bl�ba azon met�k ker�lnek, melyek XML-beli azonosito prefixe 'ctt_w_', de nem
    ///     tartalmazz�k az '_w_alairo' stringet
    ///     EDOK azonos�t�s: nev = EREC_TargySzavak.TargySzavak (egyedi, visszaadva GetAllMetaByContentType �ltal 'Targyszo' n�ven)
    ///     SPS azonos�t�s: azonosito = EREC_TargySzavak.BelsoAzonosito
    /// </param>
    /// <param name="ContentType">Egy�rtelm�en azonos�t egy metadefin�ci�t. K�telez�.</param>
    /// <param name="bCsakSajatSzint">Ha true, a magasabb szinten defini�lt metaadatok nem ker�lnek kiv�laszt�sra.</param>
    /// <returns>Hiba, vagy a Result Ds-ben 2 t�bl�zat: a Wordben/EDOK szerinti metadefin�ci�ban egyez� t�telek �s m�veleteik ('ResultTable'),
    /// az SPS-b�l kapott, EDOK-ban nem defini�lt met�k ('DifferenceTable')</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "A Word fel�l XML-ben kapott ctt_w_... megjel�l�s�, de nem _w_alairo... jel�l�s� (Field.DisplayName = TargySzavak ill. Field.Name = BelsoAzonosito) t�rgyszavak k�t�se a ContentType alapj�n. Vissza: Hiba, vagy a Result Ds-ben 2 t�bl�zat: a Wordben/EDOK szerinti metadefin�ci�ban egyez� t�telek �s m�veleteik ('ResultTable'), az SPS-b�l kapott, EDOK-ban nem defini�lt met�k ('DifferenceTable')")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_ObjektumTargyszavaiSearch))]
    public Result MergeWordMetaByContentType(ExecParam ExecParam, System.Xml.XmlDocument WordMetaXML, String Obj_Id, string ObjTip_Kod, String ContentType, bool bCsakSajatSzint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        //// Word fel�l iktat�skor csak iratr�l lehet sz�
        //string ObjTip_Kod = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;

        //// TODO: KRT_ObjTipusok lek�rdez�s, cache
        //const String EREC_IraIratok_Id = "AA5E7BBA-96A0-4C17-8709-06A6D297E107";
        //const String EREC_UgyUgyiratdarabok_Id = "EDB1D028-5317-43C9-8378-9C6F811EE716";
        //const String EREC_UgyUgyiratok_Id = "A04417A6-54AC-4AF1-9B63-5BABF0203D42";

        try
        {
            //Dictionary<string, string> UgyiratHierarchiaObjTipIds = GetUgyiratHierarchiaObjTipusIds(ExecParam);
            Dictionary<string, string> UgyiratHierarchiaObjTipIds;
            if (bCsakSajatSzint == false && ObjTip_Kod == Contentum.eUtility.Constants.TableNames.EREC_IraIratok)
            {
                string[] Kodok = {Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok
                             , Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratdarabok
                             , Contentum.eUtility.Constants.TableNames.EREC_IraIratok};
                UgyiratHierarchiaObjTipIds = GetObjTipIds(ExecParam, Kodok);
            }
            else
            {
                string[] Kodok = {ObjTip_Kod};
                UgyiratHierarchiaObjTipIds = GetObjTipIds(ExecParam, Kodok);
            }

            String EREC_IraIratok_Id = "";
            String EREC_UgyUgyiratdarabok_Id = "";
            String EREC_UgyUgyiratok_Id = "";

            String InputObjTip_Id = "";

            foreach (String key in UgyiratHierarchiaObjTipIds.Keys)
            {
                switch (key)
                {
                    case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok:
                        UgyiratHierarchiaObjTipIds.TryGetValue(key, out EREC_UgyUgyiratok_Id);
                        break;
                    case Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratdarabok:
                        UgyiratHierarchiaObjTipIds.TryGetValue(key, out EREC_UgyUgyiratdarabok_Id);
                        break;
                    case Contentum.eUtility.Constants.TableNames.EREC_IraIratok:
                        UgyiratHierarchiaObjTipIds.TryGetValue(key, out EREC_IraIratok_Id);
                        break;
                    default:
                        UgyiratHierarchiaObjTipIds.TryGetValue(key, out InputObjTip_Id);
                        break;
                }
            }

            // TODO: konstansok
            const String OperationUpdate = "UPDATE";
            const String OperationInsert = "INSERT";
            const String OperationInvalidate = "INVALIDATE";
            const String OperationNothing = "(nothing)";

            EREC_ObjektumTargyszavaiSearch _EREC_ObjektumTargyszavaiSearch = new EREC_ObjektumTargyszavaiSearch();

            System.Xml.XmlNamespaceManager xmlns = new System.Xml.XmlNamespaceManager(WordMetaXML.NameTable);
            System.Xml.XmlNodeList nodeList = WordMetaXML.SelectNodes("//metaadatok/metaadat[contains(@azonosito, 'ctt_w_') and not(contains(@azonosito, '_w_alairo'))]");

            #region debug log
            // a Word fel�l kapott node-ok sz�m�nak (�sszesen, ebb�l a mint�ra illeszked�) visszaad�s�ra
            Logger.Debug("EREC_ObjektumTargyszavai.MergeWordMetaByContentType Node Statistics", ExecParam);
            Logger.Debug("Pattern: '//metaadatok/metaadat' Count: " + WordMetaXML.SelectNodes("//metaadatok/metaadat").Count.ToString());
            Logger.Debug("Pattern: '//metaadatok/metaadat[contains(@azonosito, 'ctt_w_')]' Count: " + WordMetaXML.SelectNodes("//metaadatok/metaadat[contains(@azonosito, 'ctt_w_')]").Count.ToString());
            Logger.Debug("Pattern: '//metaadatok/metaadat[contains(@azonosito, 'ctt_w_') and not(contains(@azonosito, '_w_alairo'))]' Count: " + nodeList.Count.ToString());
            #endregion debug log

            #region Result DS t�bladefin�ci�k
            // a tal�lt egyez� sorok �s a rajtuk elv�gzett m�veletek visszaad�s�ra
            DataTable resultTable = new DataTable("ResultTable");
            resultTable.Columns.Add("Id");
            resultTable.Columns.Add("BelsoAzonosito");
            resultTable.Columns.Add("Targyszo");
            resultTable.Columns.Add("Ertek");
            resultTable.Columns.Add("Operation");

            // a defin�ci�k k�z�tti elt�r�sek (nem tal�lhat� SPS fel�li node-ok) visszaad�s�ra
            DataTable differenceTable = new DataTable("DifferenceTable");
            differenceTable.Columns.Add("Name");
            differenceTable.Columns.Add("Value");
            differenceTable.Columns.Add("Count");
            #endregion Result DS t�bladefin�ci�k

            // ha van �tadott "haszn�lhat�" meta
            if (nodeList.Count > 0)
            {
                // TODO: DisplayName meghat�roz�sa (= Funkcio)
                // ha az objtip_id nem irat, akkor irat hierarchia lek�r�se, �s a megfelel� objektumhoz k�t�s

                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

                result = sp.GetAllMetaByContentType(ExecParam
                    , _EREC_ObjektumTargyszavaiSearch
                    , Obj_Id
                    , ""
                    , ObjTip_Kod
                    , ContentType
                    , bCsakSajatSzint
                    , true
                    );

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    throw new ResultException(result);
                }

                int rowCount = result.Ds.Tables[0].Rows.Count;

                if (rowCount > 0)
                {
                    EREC_UgyUgyiratok erec_UgyUgyiratok = null;
                    EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = null;

                    if (bCsakSajatSzint == false && ObjTip_Kod == Contentum.eUtility.Constants.TableNames.EREC_IraIratok)
                    {
                        DataRow[] dataRows = result.Ds.Tables[0].Select("ObjTip_Id <> '" + EREC_IraIratok_Id + "'");
                        if (dataRows != null && dataRows.Length > 0)
                        {
                            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
                            ExecParam execParam_irat = ExecParam.Clone();
                            execParam_irat.Record_Id = Obj_Id;
                            Result result_irathierarchia = service_iratok.GetIratHierarchia(execParam_irat);

                            if (!String.IsNullOrEmpty(result_irathierarchia.ErrorCode))
                            {
                                throw new ResultException(result_irathierarchia);
                            }

                            IratHierarchia iratHierarchia = (IratHierarchia)result_irathierarchia.Record;

                            if (iratHierarchia != null)
                            {
                                erec_UgyUgyiratok = iratHierarchia.UgyiratObj;
                                erec_UgyUgyiratdarabok = iratHierarchia.UgyiratDarabObj;
                            }
                            else
                            {
                                throw new ResultException(55501); // "Hiba az irat hierarchia meghat�roz�sa sor�n!")
                            }
                        }
                    }

                    foreach (System.Xml.XmlNode xmlNode in nodeList)
                    {
                        DataRow[] dataRows_TargySzavak = result.Ds.Tables[0].Select("Targyszo = '" + xmlNode.Attributes["nev"].Value + "' and BelsoAzonosito='" + xmlNode.Attributes["azonosito"].Value + "'");
                        
                        if (dataRows_TargySzavak.Length == 0)
                        {
                            // TODO: valamilyen form�ban az eredm�nyben (DifferenceTable) is visszaadni
                            Logger.Debug(String.Format("Nincs ilyen t�rgysz�/azonos�t� p�r a tal�lati list�n! (t�rgysz�: {0}; azonos�t�: {1})", xmlNode.Attributes["nev"].Value, xmlNode.Attributes["azonosito"].Value));
                            dataRows_TargySzavak = result.Ds.Tables[0].Select("Targyszo = '" + xmlNode.Attributes["nev"].Value + "'");
                            Logger.Debug(String.Format("�j sz�r�s csak t�rgysz� alapj�n, tal�latok sz�ma: {0}", dataRows_TargySzavak.Length.ToString()));
                        }

                        if (dataRows_TargySzavak.Length > 0)
                        {
                            DataRow dataRow_TargySzavak = dataRows_TargySzavak[0];
                            // eredm�ny "loggol�s"
                            DataRow resultRow = resultTable.NewRow();

                            EREC_ObjektumTargyszavai erec_ObjektumTargyszavai = new EREC_ObjektumTargyszavai();
                            Utility.LoadBusinessDocumentFromDataRow(erec_ObjektumTargyszavai, dataRow_TargySzavak);

                            //// TODO: megn�zz�k a t�pust (�rt�k csak az 1 t�pus� t�rgysz� mellett �llhat)
                            String Record_Id = dataRow_TargySzavak["Id"].ToString();

                            // meg kell hat�rozni a metaadat �rt�k�t az XML-b�l
                            String NewValue = xmlNode.Attributes["ertek"].Value;

                            resultRow["Id"] = Record_Id;
                            resultRow["BelsoAzonosito"] = dataRow_TargySzavak["BelsoAzonosito"].ToString();
                            resultRow["Targyszo"] = dataRow_TargySzavak["Targyszo"].ToString();
                            resultRow["Ertek"] = NewValue;
                            resultRow["Operation"] = OperationNothing;

                            // ha van megadva �rt�k, insert vagy update
                            if (!String.IsNullOrEmpty(NewValue))
                            {
                                if (bCsakSajatSzint == false && ObjTip_Kod == Contentum.eUtility.Constants.TableNames.EREC_IraIratok)
                                {
                                    // meg kell hat�rozni az objektum t�pust, amihez k�tni kell a met�t:
                                    // ha �gyirat hierarchia, akkor lehet az irat �gyiratdarabja, �gyirata is
                                    String ObjTip_Id = dataRow_TargySzavak["ObjTip_Id"].ToString(); ;

                                    erec_ObjektumTargyszavai.ObjTip_Id = ObjTip_Id;
                                    erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                                    if (ObjTip_Id.Equals(EREC_IraIratok_Id, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        erec_ObjektumTargyszavai.Obj_Id = Obj_Id;
                                        erec_ObjektumTargyszavai.Updated.Obj_Id = true;
                                    }
                                    else if (ObjTip_Id.Equals(EREC_UgyUgyiratdarabok_Id, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        erec_ObjektumTargyszavai.Obj_Id = erec_UgyUgyiratdarabok.Id;
                                        erec_ObjektumTargyszavai.Updated.Obj_Id = true;
                                    }
                                    else if (ObjTip_Id.Equals(EREC_UgyUgyiratok_Id, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        erec_ObjektumTargyszavai.Obj_Id = erec_UgyUgyiratok.Id;
                                        erec_ObjektumTargyszavai.Updated.Obj_Id = true;
                                    }
                                }
                                else
                                {
                                    // meg kell hat�rozni az objektum t�pust, amihez k�tni kell a met�t:
                                    // ha nem irat, akkor saj�t mag�hoz k�t�nk
                                    String ObjTip_Id = InputObjTip_Id;
                                    erec_ObjektumTargyszavai.ObjTip_Id = ObjTip_Id;
                                    erec_ObjektumTargyszavai.Updated.ObjTip_Id = true;

                                    erec_ObjektumTargyszavai.Obj_Id = Obj_Id;
                                    erec_ObjektumTargyszavai.Updated.Obj_Id = true;
                                }

                                erec_ObjektumTargyszavai.Ertek = NewValue;
                                erec_ObjektumTargyszavai.Updated.Ertek = true;

                                erec_ObjektumTargyszavai.SPSSzinkronizalt = "1";
                                erec_ObjektumTargyszavai.Updated.SPSSzinkronizalt = true;

                                if (String.IsNullOrEmpty(Record_Id))
                                {
                                    ExecParam execParam_insert = ExecParam.Clone();
                                    Result result_insert = sp.Insert(Constants.Insert, execParam_insert, erec_ObjektumTargyszavai);

                                    if (!String.IsNullOrEmpty(result_insert.ErrorCode))
                                    {
                                        throw new ResultException(result_insert);
                                    }

                                    resultRow["Id"] = result_insert.Uid;
                                    resultRow["Operation"] = OperationInsert;
                                }
                                else
                                {
                                    String OldValue = dataRow_TargySzavak["Ertek"].ToString();
                                    String SPSSzinkronizalt = dataRow_TargySzavak["SPSSzinkronizalt"].ToString();

                                    if (OldValue != NewValue || SPSSzinkronizalt != "1")
                                    {
                                        erec_ObjektumTargyszavai.Base.Updated.Letrehozo_id = false;
                                        erec_ObjektumTargyszavai.Base.Updated.LetrehozasIdo = false;

                                        ExecParam execParam_update = ExecParam.Clone();
                                        execParam_update.Record_Id = Record_Id;
                                        Result result_update = sp.Insert(Constants.Update, execParam_update, erec_ObjektumTargyszavai);

                                        if (!String.IsNullOrEmpty(result_update.ErrorCode))
                                        {
                                            throw new ResultException(result_update);
                                        }

                                        resultRow["Id"] = Record_Id;
                                        resultRow["Operation"] = OperationUpdate;
                                    }
                                }
                            }
                            //else if (!String.IsNullOrEmpty(Record_Id) && ObjTip_Id == EREC_IraIratok_Id)
                            else if (!String.IsNullOrEmpty(Record_Id))
                            {
                                // ha nincs megadva �rt�k, de m�r volt ilyen k�t�s: �rv�nytelen�t�s, de csak az adott objektumn�l?
                                ExecParam execParam_invalidate = ExecParam.Clone();
                                execParam_invalidate.Record_Id = Record_Id;
                                Result result_invalidate = sp.Invalidate(execParam_invalidate);

                                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                                {
                                    throw new ResultException(result_invalidate);
                                }

                                resultRow["Id"] = Record_Id;
                                resultRow["Operation"] = OperationInvalidate;
                            }

                            resultTable.Rows.Add(resultRow);
                        }

                        if (dataRows_TargySzavak.Length != 1)
                        {
                            // 0 vagy 1-n�l t�bb eredm�nysor az xmlNode nev attrib�tum�val azonos Targyszo �rt�kkel:
                            // eredm�ny "loggol�s"
                            DataRow differenceRow = differenceTable.NewRow();
                            differenceRow["Name"] = xmlNode.Attributes["nev"].Value;
                            differenceRow["Value"] = xmlNode.Attributes["ertek"].Value;
                            differenceRow["Count"] = dataRows_TargySzavak.Length;

                            differenceTable.Rows.Add(differenceRow);
                        }
                    }
                }

            }


            #region Result DS t�bl�k hozz�ad�sa
            result.Ds.Tables.RemoveAt(0);

            result.Ds.Tables.Add(resultTable.Copy());
            result.Ds.Tables.Add(differenceTable.Copy());
            #endregion Result DS t�bl�k hozz�ad�sa
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

}
