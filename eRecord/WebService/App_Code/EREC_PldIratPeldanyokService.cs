﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eBusinessDocuments.PH;
using Contentum.eIntegrator.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.KRX;
using Contentum.eUtility.KRX.KULDEMENY_META;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Services;
using System.Xml;
using System.Xml.Linq;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_PldIratPeldanyokService : System.Web.Services.WebService
{
    #region Generáltból átvettek (Insert, Update)


    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyok))]
    public Result Insert(ExecParam ExecParam, EREC_PldIratPeldanyok Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // BUG_11716
            // Csak akkor lesz másolat, ha az "Ügyirat példány szükséges" checkbox ki van jelölve (ez nem látszik, ha TOBBES_IRATPELDANY_IKTATASKOR=1, de ettől még ki lesz jelölve)
            // vagy UGYIRATI_PELDANY_SZUKSEGES=1 (iktatásnál IktatasiParameterek.UgyiratPeldanySzukseges==true lesz)
            var masolat = Record.Eredet == KodTarak.IRAT_FAJTA.Masolat;
            var ugyiratiPeldanyAutoAtadas = !masolat ||
                Rendszerparameterek.GetBoolean(ExecParam, "UGYIRATI_PELDANY_AUTO_ATADAS", true); // PROD-nál true, NMHH-nál false

            /// Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
            /// Kézbesítési tételt kell létrehozni, ha be van állítva a felelõs, és az nem egyezik a felhasználó egyszemélyes csoportjával
            /// 

            bool kellKezbesitesiTetel = false;
            bool orzoUpdatedAndFilled = Record.Updated.Csoport_Id_Felelos && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos);
            bool orzoEsFelelosKulonbozik = Record.Csoport_Id_Felelos != Record.FelhasznaloCsoport_Id_Orzo;

            // ki van-e töltve a felelõs mezõ?
            if (ugyiratiPeldanyAutoAtadas && orzoUpdatedAndFilled && orzoEsFelelosKulonbozik)
            {
                // ha nem a felhasználó saját csoportja:
                if (Csoportok.GetFelhasznaloSajatCsoportId(ExecParam) != Record.Csoport_Id_Felelos)
                {
                    kellKezbesitesiTetel = true;
                }
            }
            // ha kézbesítési tételt is kell csinálni, akkor továbbítás alatti állapotot kell az iratpéldánynak beállítani
            if (kellKezbesitesiTetel)
            {
                Record.TovabbitasAlattAllapot = Record.Allapot;
                Record.Updated.TovabbitasAlattAllapot = true;

                Record.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt;
                Record.Updated.Allapot = true;

                Record.Csoport_Id_Felelos_Elozo = Record.Csoport_Id_Felelos;
                Record.Updated.Csoport_Id_Felelos_Elozo = true;
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);
            result.CheckError();

            // Kézbesítési tétel létrehozása, ha szükséges:
            if (kellKezbesitesiTetel && Record.Sorszam != "1")
            {
                var service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(dataContext);
                var execParam_KezbesitesiTetelInsert = ExecParam.Clone();
                var kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

                //String kuldemeny_Id = ExecParam.Record_Id;
                String ugyirat_Id = result.Uid;

                kezbesitesiTetel.Obj_Id = ugyirat_Id;
                kezbesitesiTetel.Updated.Obj_Id = true;

                // TODO: ObjTip_Id -t is kéne majd állítani valamire
                // helyette: Obj_type a tábla neve lesz (EREC_KuldKuldemenyek)

                kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok;
                kezbesitesiTetel.Updated.Obj_type = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_USER = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                kezbesitesiTetel.Csoport_Id_Cel = Record.Csoport_Id_Felelos;
                kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                // Állapot állítása:
                var csoportokService = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                var csoportokSearch = new KRT_CsoportokSearch();
                csoportokSearch.Id.Filter(Record.Csoport_Id_Felelos);

                Result subCsoportResult = csoportokService.GetAllSubCsoport(ExecParam.Clone(), ExecParam.FelhasznaloSzervezet_Id, true, csoportokSearch);
                if (!subCsoportResult.IsError && (subCsoportResult.GetCount != 0 || Record.Csoport_Id_Felelos == ExecParam.FelhasznaloSzervezet_Id))
                {
                    kezbesitesiTetel.AtadasDat = DateTime.Now.ToString();
                    kezbesitesiTetel.Updated.AtadasDat = true;

                    kezbesitesiTetel.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
                    kezbesitesiTetel.Updated.Allapot = true;

                    EREC_IraKezbesitesiFejekService kezbesitesiFejekService = new EREC_IraKezbesitesiFejekService(this.dataContext);
                    EREC_IraKezbesitesiFejek kezbesitesiFej = new EREC_IraKezbesitesiFejek();

                    kezbesitesiFej.CsomagAzonosito = "TomegesAtadas";
                    kezbesitesiFej.Updated.CsomagAzonosito = true;

                    kezbesitesiFej.Tipus = "A";
                    kezbesitesiFej.Updated.Tipus = true;

                    Result kezbesitesiFejInsertResult = kezbesitesiFejekService.Insert(execParam_KezbesitesiTetelInsert.Clone(), kezbesitesiFej);
                    kezbesitesiFejInsertResult.CheckError();

                    if (string.IsNullOrEmpty(kezbesitesiFejInsertResult.Uid))
                        throw new ResultException(0);

                    kezbesitesiTetel.KezbesitesFej_Id = kezbesitesiFejInsertResult.Uid;
                    kezbesitesiTetel.Updated.KezbesitesFej_Id = true;
                }
                else
                {
                    kezbesitesiTetel.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
                    kezbesitesiTetel.Updated.Allapot = true;
                }

                Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);
                result_kezbesitesiTetel_Insert.CheckError();
            }

            /// TODO: Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
            /// Ha beállítjuk, Kézbesítési tételt kell létrehozni:
            /// 

            #region Irat AdathordozoTipusa mező frissítése

            /// BLG#3451: Vegyes típusú irat kezelése:
            /// Irat AdathordozoTipusa mezőjének beállítása az iratpéldányok UgyintezesModja alapján
            /// 
            new EREC_IraIratokService(this.dataContext).RefreshAdathordozoTipusaByIratPld(ExecParam, Record.IraIrat_Id);

            #endregion

            #region Partner cím
            Result result_bindCim = BindPartnerCim(ExecParam.Clone(), Record);
            result_bindCim.CheckError();
            #endregion Partner cím

            #region ACL kezelés

            Result aclResult = result;
            RightsService rs = new RightsService(this.dataContext);
            aclResult = rs.AddScopeIdToJogtargy(ExecParam, result.Uid, "EREC_PldIratpeldanyok", Record.IraIrat_Id, 'S', null);
            aclResult.CheckError();

            #region UCM

            SetUCMJogosultsag(ExecParam, Record.IraIrat_Id);

            #endregion

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_PldIratPeldanyok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Iratpéldány módosítása
    /// Felelõs változásának figyelése: ha változik a felelõs, kézbesítési tétel generálás
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyok))]
    public Result Update(ExecParam ExecParam, EREC_PldIratPeldanyok Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());
        /// Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
        /// Ha változtatjuk, Kézbesítési tételt kell létrehozni:
        /// 

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            bool kellKezbesitesiTetel = false;
            EREC_PldIratPeldanyok iratPeldany_regi = null;
            bool iratPeldanyMozog = false;

            #region régi iratPéldány lekérése DB-bõl
            string iratpeldanyFelelosElozo = "";
            string iratpeldanyOrzoElozo = "";

            ExecParam execParam_iratPeldanyGet = ExecParam.Clone();
            execParam_iratPeldanyGet.Record_Id = ExecParam.Record_Id;
            Result result_iratPeldanyGet = this.Get(execParam_iratPeldanyGet);

            EREC_PldIratPeldanyok record_elozoVerzio = result_iratPeldanyGet.Record as EREC_PldIratPeldanyok;
            iratpeldanyFelelosElozo = record_elozoVerzio.Csoport_Id_Felelos;
            iratpeldanyOrzoElozo = record_elozoVerzio.FelhasznaloCsoport_Id_Orzo;
            #endregion

            // Felelõs változásának vizsgálata:
            if (Record.Updated.Csoport_Id_Felelos && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos))
            {
                /// Meg kell vizsgálni, hogy ténylegesen változott-e a felelõs
                /// (Ehhez le kell kérni a rekordot)
                /// 

                if (!String.IsNullOrEmpty(result_iratPeldanyGet.ErrorCode))
                {
                    // hiba
                    //log.WsEnd(execParam_iratPeldanyGet, result_iratPeldanyGet);
                    //return result_iratPeldanyGet;

                    throw new ResultException(result_iratPeldanyGet);
                }

                if (result_iratPeldanyGet.Record != null)
                {
                    iratPeldany_regi = (EREC_PldIratPeldanyok)result_iratPeldanyGet.Record;

                    if (iratPeldany_regi.Csoport_Id_Felelos != Record.Csoport_Id_Felelos)
                    {
                        kellKezbesitesiTetel = true;
                    }
                }
            }


            #region Vonalkód változásának vizsgálata, és ellenõrzés

            bool vanUjVonalkod = false;
            Result resBarcode = null;

            // ha megadtak vonalkódot, és az elõzõ verzióban még nem volt vonalkód:
            if (!String.IsNullOrEmpty(Record.BarCode) && Record.Updated.BarCode
                && String.IsNullOrEmpty(record_elozoVerzio.BarCode))
            {
                //Vonalkód ellenõrzése
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                resBarcode = srvBarcode.CheckBarcode(ExecParam, Record.BarCode);
                resBarcode.CheckError();
                vanUjVonalkod = true;
            }

            #endregion

            #region IratPéldány mozgásának vizsgálata

            if (Record.Updated.Csoport_Id_Felelos && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos) && Record.Csoport_Id_Felelos != iratPeldany_regi.Csoport_Id_Felelos
                && Record.Updated.FelhasznaloCsoport_Id_Orzo && !String.IsNullOrEmpty(Record.FelhasznaloCsoport_Id_Orzo) && Record.FelhasznaloCsoport_Id_Orzo != iratPeldany_regi.FelhasznaloCsoport_Id_Orzo)
            {
                iratPeldanyMozog = true;
            }

            // Benne van-e fizikai dossziéban?
            if (iratPeldanyMozog && this.IsIratPeldanyInFizikaiDosszie(ExecParam, ExecParam.Record_Id))
                throw new ResultException(64026); // Az iratpéldány csak dossziéval együtt mozgatható!

            #endregion IratPéldány mozgásának vizsgálata

            Result result_IratPeldanyUpdate = BaseUpdate(ExecParam, Record);
            result_IratPeldanyUpdate.CheckError();

            #region Vonalkód kötése, ha kell:

            if (vanUjVonalkod && resBarcode != null)
            {
                string barkodId = resBarcode.Uid;
                //string barkodVer = resBarcode.Ds.Tables[0].Rows[0]["Ver"].ToString();
                string pld_Id = ExecParam.Record_Id;
                resBarcode = new Result();
                KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
                ExecParam execParam_barkod = ExecParam.Clone();
                resBarcode = srvBarcode.BarkodBindToIratPeldany(execParam_barkod, barkodId, pld_Id);
                resBarcode.CheckError();
            }

            #endregion

            // Kézbesítési tétel létrehozása, ha szükséges:
            if (kellKezbesitesiTetel)
            {
                EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_KezbesitesiTetelInsert = ExecParam.Clone();

                EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

                String iratPeldany_Id = ExecParam.Record_Id;

                kezbesitesiTetel.Obj_Id = iratPeldany_Id;
                kezbesitesiTetel.Updated.Obj_Id = true;

                // TODO: ObjTip_Id -t is kéne majd állítani valamire
                // helyette: Obj_type a tábla neve lesz (EREC_UgyUgyiratok)

                kezbesitesiTetel.Obj_type = Constants.TableNames.EREC_PldIratPeldanyok;
                kezbesitesiTetel.Updated.Obj_type = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_USER = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                kezbesitesiTetel.Csoport_Id_Cel = Record.Csoport_Id_Felelos;
                kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                // Állapot állítása:
                kezbesitesiTetel.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
                kezbesitesiTetel.Updated.Allapot = true;

                // TODO: Azonosító állítása (Iktatókönyv megküljel. / Fõszám (Év)) a listában való megjelenítéshez

                Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);
                result_kezbesitesiTetel_Insert.CheckError();

                //a keletkezett kézbesítési tétel id-jának visszaadása az Uid változóban
                result_IratPeldanyUpdate.Uid = result_kezbesitesiTetel_Insert.Uid;
            }

            #region UgyintezesModja mezõ változásának figyelése

            // Ha megváltozott az UgyintezesModja mezõ az 1-es iratpéldánynál, akkor meg kell update-elni az iratot
            if (Record.Updated.UgyintezesModja && record_elozoVerzio.Sorszam == "1"
                && Record.UgyintezesModja != record_elozoVerzio.UgyintezesModja && !string.IsNullOrEmpty(Record.UgyintezesModja))
            {
                // irat ügyintézés módja mezõjét update-eljük:
                // Irat GET:
                EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
                ExecParam execParam_iratGet = ExecParam.Clone();
                execParam_iratGet.Record_Id = record_elozoVerzio.IraIrat_Id;

                Result result_iratGet = service_iratok.Get(execParam_iratGet);
                result_iratGet.CheckError();

                EREC_IraIratok irat = (EREC_IraIratok)result_iratGet.Record;

                // BUG_2051
                //if (Record.UgyintezesModja != irat.UgyintezesAlapja)
                if (Record.UgyintezesModja != irat.AdathordozoTipusa)
                {
                    // Irat UPDATE:
                    irat.Updated.SetValueAll(false);
                    irat.Base.Updated.SetValueAll(false);
                    irat.Base.Updated.Ver = true;

                    // BUG_2051                
                    //irat.UgyintezesAlapja = Record.UgyintezesModja;
                    //irat.Updated.UgyintezesAlapja = true;
                    irat.AdathordozoTipusa = Record.UgyintezesModja;
                    irat.Updated.AdathordozoTipusa = true;

                    ExecParam execParam_iratUpdate = ExecParam.Clone();
                    execParam_iratUpdate.Record_Id = irat.Id;

                    Result result_iratUpdate = service_iratok.Update(execParam_iratUpdate, irat);
                    result_iratUpdate.CheckError();
                }
            }

            /// BLG#3451: Vegyes típusú irat kezelése:
            /// Irat AdathordozoTipusa mezőjének beállítása az iratpéldányok UgyintezesModja alapján
            /// 
            if ((Record.Updated.UgyintezesModja && Record.UgyintezesModja != record_elozoVerzio.UgyintezesModja)
                // Vagy ha az IraIrat_Id mező változott meg (Átiktatás esetén):
                || (Record.Updated.IraIrat_Id && Record.IraIrat_Id != record_elozoVerzio.IraIrat_Id))
            {
                EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

                string iratId = record_elozoVerzio.IraIrat_Id;
                // Átiktatás esetén az új iratra kell a frissítés:
                if (Record.Updated.IraIrat_Id
                     && Record.IraIrat_Id != record_elozoVerzio.IraIrat_Id && !String.IsNullOrEmpty(Record.IraIrat_Id))
                {
                    iratId = Record.IraIrat_Id;
                }

                // AdathordozoTipusa mező frissítés:
                service_iratok.RefreshAdathordozoTipusaByIratPld(ExecParam, iratId);
            }

            #endregion

            result = result_IratPeldanyUpdate;

            #region ACL kezelés
            RightsService rs = new RightsService(this.dataContext);
            Result aclResult = new Result();

            if (Record.Updated.FelhasznaloCsoport_Id_Orzo && !string.IsNullOrEmpty(iratpeldanyOrzoElozo))
            {
                // Megkeresés esetén a felelõsnek adni kell kézi írási jogot az ügyirathoz,
                // továbbá bon jogot kell adni az iratpéldány címzettjének az irathoz, feltéve, hogy még nincs
                // (csak átvétel esetén kell jogot adni, nem véletlenül van ebben a blokkban)
                if (Record.Visszavarolag == KodTarak.VISSZAVAROLAG.Megkereses)
                {
                    EREC_IraIratokService iis = new EREC_IraIratokService(this.dataContext);
                    EREC_UgyUgyiratdarabokService uuds = new EREC_UgyUgyiratdarabokService(this.dataContext);

                    ExecParam ep = new ExecParam();

                    ep.Record_Id = Record.IraIrat_Id;
                    ep.Record_Id = (iis.Get(ep).Record as EREC_IraIratok).UgyUgyIratDarab_Id;
                    string ugyUgyiratId = (uuds.Get(ep).Record as EREC_UgyUgyiratdarabok).UgyUgyirat_Id;

                    rs.AddCsoportToJogtargyWithJogszint(ExecParam, ugyUgyiratId, Record.Csoport_Id_Felelos, 'I', 'I', 'M');

                    // címzettnek az irathoz
                    string pldCimzettId = string.Empty;
                    if (string.IsNullOrEmpty(Record.Partner_Id_Cimzett))
                    {
                        if (!string.IsNullOrEmpty(record_elozoVerzio.Partner_Id_Cimzett))
                            pldCimzettId = record_elozoVerzio.Partner_Id_Cimzett;
                    }
                    else
                    {
                        pldCimzettId = Record.Partner_Id_Cimzett;
                    }

                    if (!string.IsNullOrEmpty(pldCimzettId))
                    {
                        KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

                        KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();
                        felhasznalokSearch.Partner_id.Filter(pldCimzettId);

                        Result felhasznalokGetAllResult = felhasznalokService.GetAll(ExecParam, felhasznalokSearch);
                        if (string.IsNullOrEmpty(felhasznalokGetAllResult.ErrorCode) && felhasznalokGetAllResult.Ds.Tables[0].Rows.Count > 0)
                        {
                            string pldCimzettIdAsFelhasznaloId = felhasznalokGetAllResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                            // Ha hibát dob, már létezik a kapcsolat => nem kell csinálni semmit
                            rs.AddCsoportToJogtargyWithJogszint(ExecParam, record_elozoVerzio.IraIrat_Id, pldCimzettIdAsFelhasznaloId, 'I', 'I', 'B');
                        }
                    }
                }

            }

            #endregion

            #region UCM

            SetUCMJogosultsag(ExecParam, Record.IraIrat_Id);

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_PldIratpeldanyok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public Result BaseUpdate(ExecParam ExecParam, EREC_PldIratPeldanyok Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);
            result.CheckError();

            //ha mozog az elsõdeleges iratpéldány, átadás vagy átvétel
            //akkor mozgatjuk vele a küldeményt és iratot
            if (IsMoved(Record))
            {
                Result resMoved = this.Moved(ExecParam);
                resMoved.CheckError();
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_PldIratpeldanyok", "IratPeldanyModify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion


    public Result UpdateTomeges(ExecParam ExecParam, List<string> Ids, List<string> Vers, EREC_PldIratPeldanyok Record, DateTime ExecutionTime)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        try
        {
            result = sp.UpdateTomeges(ExecParam, Ids, Vers, Record, ExecutionTime);
            result.CheckError();
        }
        catch (ResultException re)
        {
            result = re.GetResult();
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra). 
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_PldIratPeldanyokSearch">A keresési feltételeket tartalmazó objektum</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_PldIratPeldanyokSearch _EREC_PldIratPeldanyokSearch)
    {
        return GetAllWithExtensionAndJogosultak(ExecParam, _EREC_PldIratPeldanyokSearch, false);
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyokSearch))]
    public Result GetAllWithExtensionByKimenoKuldemeny(ExecParam execParam, EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch, string kimenoKuldemenyId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch, false, kimenoKuldemenyId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra). 
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_PldIratPeldanyokSearch">A keresési feltételeket tartalmazó objektum</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyokSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_PldIratPeldanyokSearch _EREC_PldIratPeldanyokSearch, bool Jogosultak)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_PldIratPeldanyokSearch, Jogosultak);

            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_PldIratPeldanyokSearch, new EREC_PldIratPeldanyokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_PldIratPeldanyok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_PldIratPeldanyokSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_EREC_PldIratPeldanyokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_PldIratPeldanyokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }

                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Iratpéldány rekord lekérése jogosultság-ellenõrzéssel
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Jogszint"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyokSearch))]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_PldIratPeldanyok", "View").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Iratpéldány rekord lekérése jogosultság-ellenõrzéssel
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Jogszint"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyokSearch))]
    public Result GetWithRightCheckWithoutEventLogging(ExecParam execParam, char Jogszint)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Új iratpéldány létrehozása a megadott irathoz
    /// Iratpéldány felelõsének és õrzõjének beállítása a létrehozó felhasználó csoportjára
    /// </summary>
    /// <param name="_ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_PldIratPeldanyok">A létrehozandó iratpéldány adatait tartalmazó objektum.
    /// Csak a felhasználó által a felületen is kitölthetõ mezõket veszi figyelembe, mint pl. Címzett, Címzett címe, stb...</param>
    /// <param name="_IraIrat_Id">Az irat Id-ja, amihez az új iratpéldányt készítjük.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyok))]
    public Result AddNewPldIratPeldanyToIraIrat(ExecParam _ExecParam,
        EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, string _IraIrat_Id)
    {
        Log log = Log.WsStart(_ExecParam, Context, GetType());
        // paraméterek meglétének ellenõrzése
        if (String.IsNullOrEmpty(_IraIrat_Id)
            || _EREC_PldIratPeldanyok == null
            || _ExecParam == null || String.IsNullOrEmpty(_ExecParam.Felhasznalo_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52111);
            log.WsEnd(_ExecParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = AddNewPldIratPeldanyToIraIrat_segedFv(_ExecParam, _EREC_PldIratPeldanyok, _IraIrat_Id, null);
            result.CheckError();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(_ExecParam, result);
        return result;
    }


    [WebMethod()]
    public Result AddNewPldIratPeldanyToIraIrat_ByIratPeldany(ExecParam execParam,
        EREC_PldIratPeldanyok erec_PldIratPeldanyok, string iratPeldany_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // paraméterek meglétének ellenõrzése
        if (String.IsNullOrEmpty(iratPeldany_Id)
            || erec_PldIratPeldanyok == null
            || execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52111);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = AddNewPldIratPeldanyToIraIrat_segedFv(execParam, erec_PldIratPeldanyok, null, iratPeldany_Id);
            result.CheckError();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ha meg van adva az iratpéldány id, akkor abból határozzuk meg az irat id-t, és ilyenkor az iratpéldány módosíthatóságára ellenõrzünk;
    /// ha iratId van megadva, akkor az irat módosíthatóságára ellenõrzünk
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_PldIratPeldanyok"></param>
    /// <param name="_IraIrat_Id"></param>
    /// <param name="_pld_IratPeldany_Id"></param>
    /// <returns></returns>
    private Result AddNewPldIratPeldanyToIraIrat_segedFv(ExecParam _ExecParam,
        EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, string _IraIrat_Id, string _pld_IratPeldany_Id)
    {
        #region Iratpéldány lekérése, ha meg volt adva

        Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz refIratPeldanyStatusz = null;

        bool bFromIratPeldany = !String.IsNullOrEmpty(_pld_IratPeldany_Id);

        if (bFromIratPeldany)
        {
            ExecParam execParam_pldGet = _ExecParam.Clone();
            execParam_pldGet.Record_Id = _pld_IratPeldany_Id;

            Result result_pldGet = this.Get(execParam_pldGet);
            result_pldGet.CheckError();

            EREC_PldIratPeldanyok refIratPeldanyObj = (EREC_PldIratPeldanyok)result_pldGet.Record;

            _IraIrat_Id = refIratPeldanyObj.IraIrat_Id;

            refIratPeldanyStatusz = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(refIratPeldanyObj);
        }

        #endregion

        #region Irat hierarchia lekérése (kell az azonosító összeállításához)

        EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
        ExecParam execParam_IratGet = _ExecParam.Clone();
        execParam_IratGet.Record_Id = _IraIrat_Id;

        Result result_IratHier = erec_IraIratokService.GetIratHierarchia(execParam_IratGet);
        result_IratHier.CheckError();

        if (result_IratHier.Record == null)
        {
            // hiba
            throw new ResultException(52111);
        }

        IratHierarchia iratHier = (IratHierarchia)result_IratHier.Record;
        //EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result_IratGet.Record;

        EREC_IraIratok erec_IraIratok = iratHier.IratObj;
        EREC_UgyUgyiratok erec_UgyUgyiratok = iratHier.UgyiratObj;

        #endregion

        #region Státusz ellenõrzés, hogy lehet-e új iratpéldányt létrehozni?

        // Elsõ iratpéldány lekérése az iratstátuszhoz
        Result result_elsoIratPeldanyGet = this.GetElsoIratPeldanyByIraIrat(_ExecParam.Clone(), _IraIrat_Id);
        if (result_elsoIratPeldanyGet.IsError)
        {
            // hiba:
            Logger.Error("Hiba az elsõ iratpéldány lekérésekor", _ExecParam);
            throw new ResultException(result_elsoIratPeldanyGet);
        }

        EREC_PldIratPeldanyok elsoIratPeldany = (EREC_PldIratPeldanyok)result_elsoIratPeldanyGet.Record;

        Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz =
            Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(erec_IraIratok, elsoIratPeldany);
        Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz ugyiratDarabStatusz =
            Contentum.eRecord.BaseUtility.UgyiratDarabok.GetAllapotByBusinessDocument(iratHier.UgyiratDarabObj);
        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
            Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(iratHier.UgyiratObj);

        ExecParam execParam_check = _ExecParam.Clone();
        ErrorDetails errorDetail;

        /// ha a referencia iratpéldány id meg volt adva, akkor az iratpéldány módosíthatóságára ellenõrzünk,
        /// egyébként az irat módosíthatóságára
        /// 
        if (bFromIratPeldany && refIratPeldanyStatusz != null)
        {

            // iratpéldány módosíthatóságára ellenõrzés:
            if (Contentum.eRecord.BaseUtility.IratPeldanyok.Modosithato(refIratPeldanyStatusz, iratStatusz,
                    ugyiratStatusz, execParam_check, out errorDetail) == false)
            {
                // Nem lehet az irathoz új iratpéldányt létrehozni
                throw new ResultException(52113, errorDetail);
            }
        }
        else
        {
            // CR 3107 : Postázásnál Postazas funkciokod-ot is néz hogy enged-e új iratpéldányt létrehozni
            // Modosithato függvénynél megadja, hogy új iratpéldány létrehozásakor hívták
            // irat módosíthatóságára ellenõrzés:
            if (Contentum.eRecord.BaseUtility.Iratok.Modosithato(iratStatusz, ugyiratStatusz
                    , execParam_check, out errorDetail, true) == false)
            {
                // Nem lehet az irathoz új iratpéldányt létrehozni
                throw new ResultException(52113, errorDetail);
            }
        }

        #endregion

        #region Iktatókönyv lekérése

        EREC_IraIktatoKonyvekService service_Iktatokonyv = new EREC_IraIktatoKonyvekService(this.dataContext);

        ExecParam execParam_iktKonyvGet = _ExecParam.Clone();
        execParam_iktKonyvGet.Record_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;

        Result result_iktKonyvGet = service_Iktatokonyv.Get(execParam_iktKonyvGet);
        result_iktKonyvGet.CheckError();

        EREC_IraIktatoKonyvek erec_IraIktatokonyvek = (EREC_IraIktatoKonyvek)result_iktKonyvGet.Record;

        #endregion


        int utolsoSorszam = 1;

        try
        {
            utolsoSorszam = Int32.Parse(erec_IraIratok.UtolsoSorszam);
        }
        catch
        {
            // hiba
            throw new ResultException(52112);
        }

        string barkod = "";
        string barkod_Id = "";

        //Vonalkódkezelés azonosítós
        Contentum.eAdmin.Service.KRT_ParameterekService service_parameterek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        ExecParam execParam_KRT_Param = _ExecParam.Clone();
        //CR 3058
        // modified by nekrisz
        string vonalkodkezeles = Rendszerparameterek.Get(execParam_KRT_Param, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim();
        //execParam_KRT_Param.Record_Id = "F7D555E4-54BC-E611-80BB-00155D020D34"; //VonalkodKezeles, fajtája, hova tegyem a constansokat??
        //Result Result_KRTParam = service_parameterek.Get(execParam_KRT_Param);

        //LZS - BUG_5369
        //A feltételből kivettem ezt: && _EREC_PldIratPeldanyok.UgyintezesModja == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir
        //ugyanis NMHH-ban papírra is és elektronikus-ra is kell generálni, ha üresen hagyjuk.
        if (String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode))
        {
            #region Barkod generálása (csak ha nincs megadva, és ügyintézés módja elektronikus)

            try
            {
                KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                ExecParam execParam_barkod = _ExecParam.Clone();

                Result result_barkod_pld = service_barkodok.BarkodGeneralas(execParam_barkod);
                if (!String.IsNullOrEmpty(result_barkod_pld.ErrorCode))
                {
                    // TODO: egyelõre nem dobunk hibát, késõbb majd kellene (ha már minden stabilan megy)
                    Logger.Error("Vonalkód generálás az ügyiratnak: Sikertelen", _ExecParam);
                }
                else
                {
                    barkod_Id = result_barkod_pld.Uid;
                    barkod = (String)result_barkod_pld.Record;
                }

            }
            catch (Exception e)
            {
                // Elõfordulhatnak még hibák a vonalkód generálásnál; lenyeljük a hibát egyelõre
                Logger.Error("Hiba a vonalkód generálás során", _ExecParam);
                Logger.Error("", e);
            }
            #endregion
        }
        else if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode))
        {
            #region BarCode ellenõrzés

            //Vonalkód ellenõrzése 
            KRT_BarkodokService srvBarcode = new KRT_BarkodokService(this.dataContext);
            ExecParam execParam_barkod = _ExecParam.Clone();
            Result resBarcode = srvBarcode.CheckBarcode(execParam_barkod, _EREC_PldIratPeldanyok.BarCode);
            resBarcode.CheckError();

            barkod = _EREC_PldIratPeldanyok.BarCode;
            barkod_Id = resBarcode.Uid;

            #endregion
        }
        else if (!bFromIratPeldany && !(vonalkodkezeles.ToUpper().Trim().Equals("AZONOSITO"))) //MP - Azonosítós vonalkód kezelés esetén nem dobunk hibát -- modified by nekrisz
        {
            // ha nem elektronikus az ügyintézés és nem iratpéldányból hozzuk létre, akkor kötelezõ a vonalkód
            throw new ResultException(52140); //"Nincs megadva vonalkód!"
        }

        // Iratra hivatkozás beállítása:
        _EREC_PldIratPeldanyok.IraIrat_Id = erec_IraIratok.Id;
        _EREC_PldIratPeldanyok.Updated.IraIrat_Id = true;

        // Felelõs és Õrzõ beállítása a Felhasználó saját csoportjára            
        _EREC_PldIratPeldanyok.Csoport_Id_Felelos = FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
        _EREC_PldIratPeldanyok.Updated.Csoport_Id_Felelos = true;

        _EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo =
            FelhasznaloUtil.GetFelhasznaloCsoport(_ExecParam);
        _EREC_PldIratPeldanyok.Updated.FelhasznaloCsoport_Id_Orzo = true;

        // Sorszám beállítása:
        utolsoSorszam++;
        _EREC_PldIratPeldanyok.Sorszam = utolsoSorszam.ToString();
        _EREC_PldIratPeldanyok.Updated.Sorszam = true;

        // Állapot beállítása:
        // Ha az irat munkaanyag, akkor munkaállapotba tesszük az iratpéldányt is:
        if (Contentum.eRecord.BaseUtility.Iratok.Munkaanyag(erec_IraIratok))
        {
            _EREC_PldIratPeldanyok.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban;
            _EREC_PldIratPeldanyok.Updated.Allapot = true;
        }
        else
        {
            _EREC_PldIratPeldanyok.Allapot = (_EREC_PldIratPeldanyok.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott) ? KodTarak.IRATPELDANY_ALLAPOT.Iktatott : KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott;
            _EREC_PldIratPeldanyok.Updated.Allapot = true;
        }

        // BARCODE:
        _EREC_PldIratPeldanyok.BarCode = barkod;
        _EREC_PldIratPeldanyok.Updated.BarCode = true;

        /// Azonosító (Iktatószám) megadása
        /// 
        // BLG_292
        //_EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(erec_IraIktatokonyvek, erec_UgyUgyiratok
        //    , erec_IraIratok, _EREC_PldIratPeldanyok);
        _EREC_PldIratPeldanyok.Azonosito = Contentum.eRecord.BaseUtility.IratPeldanyok.GetFullIktatoszam(_ExecParam, erec_IraIktatokonyvek, erec_UgyUgyiratok
            , erec_IraIratok, _EREC_PldIratPeldanyok);
        _EREC_PldIratPeldanyok.Updated.Azonosito = true;


        // IratPéldány INSERT:
        EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);

        Result result_iratPeldanyInsert = erec_PldIratPeldanyokService.Insert(_ExecParam, _EREC_PldIratPeldanyok);
        result_iratPeldanyInsert.CheckError();

        string newId = result_iratPeldanyInsert.Uid;


        #region BARCODE UPDATE
        if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.BarCode))
        {
            KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
            ExecParam execParam_barcodeUpdate = _ExecParam.Clone();

            Result result_barcodeUpdate = service_barkodok.BarkodBindToIratPeldany(
                    execParam_barcodeUpdate, barkod_Id, newId);

            if (!String.IsNullOrEmpty(result_barcodeUpdate.ErrorCode))
            {
                // hiba, de egyelõre lenyeljük, míg a vonalkódos móka stabilan nem mûködik
                Logger.Error("Hiba: Barkod rekordhoz iratpéldány hozzákötése", _ExecParam);
            }
        }
        #endregion

        // Az Irat verziószáma változhatott (iratpéldány insertnél lehet irat update is...), ezért le kell kérni újra:
        erec_IraIratok.Base.Ver = ((EREC_IraIratok)erec_IraIratokService.Get(execParam_IratGet).Record).Base.Ver;

        // IRAT UPDATE (utolsó sorszám módosítása)
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.Ver = true;

        // utolsó sorszám módosítása:
        erec_IraIratok.UtolsoSorszam = utolsoSorszam.ToString();
        erec_IraIratok.Updated.UtolsoSorszam = true;

        ExecParam execParam_iratUpdate = _ExecParam.Clone();
        execParam_iratUpdate.Record_Id = erec_IraIratok.Id;

        Result result_IratUpdate = erec_IraIratokService.Update(execParam_iratUpdate, erec_IraIratok);
        result_IratUpdate.CheckError();

        Result result = result_iratPeldanyInsert;
        // visszaadjuk az iratpéldány objektumot (iktatószám kiíratásához)
        result.Record = _EREC_PldIratPeldanyok;

        return result;
    }

    /// <summary>
    /// A megadott irat elsõ iratpéldányának lekérése
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="IraIrat_Id">Az irat Id-ja, aminek elsõ iratpéldányát le szeretnénk kérni.</param>
    /// <returns>Result.Record tartalmazza az iratpéldányt reprezentáló objektumot.</returns>
    [WebMethod()]
    public Result GetElsoIratPeldanyByIraIrat(ExecParam ExecParam, String IraIrat_Id)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            var erec_PldIratPeldanyokService = new EREC_PldIratPeldanyokService(this.dataContext);

            var search = new EREC_PldIratPeldanyokSearch();
            search.IraIrat_Id.Filter(IraIrat_Id);
            search.Sorszam.Filter("1");

            result = sp.GetAll(ExecParam, search);
            result.CheckError();

            System.Data.DataSet ds = result.Ds;
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 1)
                {
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(51103);
                    //log.WsEnd(ExecParam, result1);
                    //return result1;

                    throw new ResultException(51103);
                }
                else
                {
                    String Id = ds.Tables[0].Rows[0]["Id"].ToString();

                    // objektum lekérése az Id alapján:
                    ExecParam execParam_Get = ExecParam.Clone();
                    execParam_Get.Record_Id = Id;

                    result = erec_PldIratPeldanyokService.Get(execParam_Get);
                }
            }
            else
            {
                // Nem volt hiba, viszont nincs találat (Result.Record == null lesz)

                result = new Result();
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Iratpéldányok tömeges átadásra kijelölése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id_Array">Az átadandó iratpéldányok Id-jai</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">Következõ kezelõ</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result AtadasraKijeloles_Tomeges(ExecParam execParam, String[] erec_PldIratPeldanyok_Id_Array
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length == 0)
            {
                throw new ResultException(52280);
            }
            else
            {
                Result result_atadasraKijeloles = new Result();

                // az összes iratpéldány lekérése
                var search = new EREC_PldIratPeldanyokSearch();
                search.Id.In(erec_PldIratPeldanyok_Id_Array);

                Result getAllResult = this.GetAllWithExtension(execParam.Clone(), search);
                getAllResult.CheckError();

                //System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByDataSet(execParam.Clone(), getAllResult.Ds);
                Hashtable iratPeldanyStatuszok;
                Hashtable iratStatuszok;
                Hashtable ugyiratStatuszok;

                // statuszok feltöltése:
                Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotHierarchiaByDataSet(execParam.Clone(), getAllResult.Ds
                    , out iratPeldanyStatuszok, out iratStatuszok, out ugyiratStatuszok);

                if (iratPeldanyStatuszok == null || iratPeldanyStatuszok.Count == 0
                    || iratStatuszok == null || iratStatuszok.Count == 0
                    || ugyiratStatuszok == null || ugyiratStatuszok.Count == 0)
                {
                    throw new ResultException(52380);
                }

                EREC_PldIratPeldanyok erec_PldIratPeldany = new EREC_PldIratPeldanyok();

                var iratpeldanyIds = string.Empty;
                string iratpeldanyVers = string.Empty;
                foreach (System.Data.DataRow r in getAllResult.Ds.Tables[0].Rows)
                {
                    //if (!Contentum.eRecord.BaseUtility.IratPeldanyok.AtadasraKijelolheto((statusz[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz), execParam.Clone()))
                    string pld_Id = r["Id"].ToString();

                    ErrorDetails errorDetail = null;
                    if (!Contentum.eRecord.BaseUtility.IratPeldanyok.AtadasraKijelolheto(
                        iratPeldanyStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz
                        , iratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Iratok.Statusz
                        , ugyiratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz
                        , execParam.Clone(), out errorDetail))
                    {
                        throw new ResultException(52201, errorDetail);
                    }

                    iratpeldanyIds += ",'" + r["Id"].ToString() + "'";
                    iratpeldanyVers += "," + r["Ver"].ToString();
                }

                iratpeldanyIds = iratpeldanyIds.TrimStart(',');
                iratpeldanyVers = iratpeldanyVers.TrimStart(',');

                result = sp.AtadasraKijeloles_Tomeges(execParam.Clone(), iratpeldanyIds, iratpeldanyVers, csoport_Id_Felelos_Kovetkezo, null, null);
                result.CheckError();

                #region Határidõs Feladat

                if (erec_HataridosFeladatok != null)
                {
                    EREC_HataridosFeladatokService svcHataridosFeladat = new EREC_HataridosFeladatokService(dataContext);
                    svcHataridosFeladat.SimpleInsertTomeges(execParam.Clone(), erec_HataridosFeladatok, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok, erec_PldIratPeldanyok_Id_Array);
                }

                #endregion

                #region ucm
                SetUCMJogosultsagPeldanyok(execParam, iratpeldanyIds);
                #endregion

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, iratpeldanyIds, "IratPeldanyAtadas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_PldIratPeldanyok::AtadasTomeges: ", execParam, eventLogResult);
                #endregion
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Iratpéldány átadásra kijelölése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id">Az átadandó iratpéldány Id-ja</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result AtadasraKijeloles(ExecParam execParam, String erec_PldIratPeldanyok_Id
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_PldIratPeldanyok_Id)
            || String.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52280);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // IratPéldány lekérése:
            EREC_PldIratPeldanyokService service_iratPeldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
            ExecParam execParam_IratPeldanyGet = execParam.Clone();
            execParam_IratPeldanyGet.Record_Id = erec_PldIratPeldanyok_Id;

            Result result_IratPeldanyGet = service_iratPeldanyok.Get(execParam_IratPeldanyGet);
            result_IratPeldanyGet.CheckError();

            if (result_IratPeldanyGet.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52282);
                //log.WsEnd(execParam_IratPeldanyGet, result1);
                //return result1;
                throw new ResultException(52282);
            }

            EREC_PldIratPeldanyok erec_PldIratPeldany = (EREC_PldIratPeldanyok)result_IratPeldanyGet.Record;

            var iratPeldanyStatusz = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(erec_PldIratPeldany);

            EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);
            ExecParam xpmIratok = execParam.Clone();
            xpmIratok.Record_Id = erec_PldIratPeldany.IraIrat_Id;

            Result resIratok = iratokService.Get(xpmIratok);
            resIratok.CheckError();

            EREC_IraIratok irat = resIratok.Record as EREC_IraIratok;

            Contentum.eRecord.BaseUtility.Iratok.Statusz iratStatusz =
                Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(irat);

            EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam xpmUgyiratok = execParam.Clone();
            xpmUgyiratok.Record_Id = irat.Ugyirat_Id;

            Result resUgyiratok = ugyiratokService.Get(xpmUgyiratok);
            resUgyiratok.CheckError();

            EREC_UgyUgyiratok ugyirat = resUgyiratok.Record as EREC_UgyUgyiratok;

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);

            // Átadásra kijelölhetõ?
            ExecParam execParam_atadasraKijelolheto = execParam.Clone();
            ErrorDetails errorDetail = null;
            if (!Contentum.eRecord.BaseUtility.IratPeldanyok.AtadasraKijelolheto(
                iratPeldanyStatusz, iratStatusz, ugyiratStatusz, execParam_atadasraKijelolheto, out errorDetail))
            {
                // Nem jelölhetõ ki átadásra az ügyirat:
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52281);
                //log.WsEnd(execParam_atadasraKijelolheto, result1);
                //return result1;

                throw new ResultException(52281, errorDetail);
            }

            // IratPéldány UPDATE
            // mezõk módosíthatóságának letiltása:
            erec_PldIratPeldany.Updated.SetValueAll(false);
            erec_PldIratPeldany.Base.Updated.SetValueAll(false);
            erec_PldIratPeldany.Base.Updated.Ver = true;

            // Módosítani kell: Elõzõ felelõs, Felelõs, TovabbitasAlattAllapot, Állapot

            // elõzõ felelõs
            erec_PldIratPeldany.Csoport_Id_Felelos_Elozo = erec_PldIratPeldany.Csoport_Id_Felelos;
            erec_PldIratPeldany.Updated.Csoport_Id_Felelos_Elozo = true;

            // új felelõs:
            erec_PldIratPeldany.Csoport_Id_Felelos = csoport_Id_Felelos_Kovetkezo;
            erec_PldIratPeldany.Updated.Csoport_Id_Felelos = true;

            // Továbbítás alatti állapot beállítása:
            if (erec_PldIratPeldany.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
            {
                erec_PldIratPeldany.TovabbitasAlattAllapot = erec_PldIratPeldany.Allapot;
                erec_PldIratPeldany.Updated.TovabbitasAlattAllapot = true;
            }

            // Állapot:
            erec_PldIratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt;
            erec_PldIratPeldany.Updated.Allapot = true;

            ExecParam execParam_iratPeldanyUpdate = execParam.Clone();
            execParam_iratPeldanyUpdate.Record_Id = erec_PldIratPeldany.Id;

            Result result_iratPeldanyUpdate =
                service_iratPeldanyok.Update(execParam_iratPeldanyUpdate, erec_PldIratPeldany);
            result_iratPeldanyUpdate.CheckError();

            // Kezelési feljegyzés INSERT, ha megadták
            if (erec_HataridosFeladatok != null)
            {
                EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                ExecParam execParam_kezFeljInsert = execParam.Clone();

                // Irathoz kötés
                erec_HataridosFeladatok.Obj_Id = erec_PldIratPeldany.Id;
                erec_HataridosFeladatok.Updated.Obj_Id = true;

                erec_HataridosFeladatok.Obj_type = Constants.TableNames.EREC_PldIratPeldanyok;
                erec_HataridosFeladatok.Updated.Obj_type = true;

                if (!String.IsNullOrEmpty(erec_PldIratPeldany.Azonosito))
                {
                    erec_HataridosFeladatok.Azonosito = erec_PldIratPeldany.Azonosito;
                    erec_HataridosFeladatok.Updated.Azonosito = true;
                }

                Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                    execParam_kezFeljInsert, erec_HataridosFeladatok);
                result_kezFeljInsert.CheckError();
            }

            result = result_iratPeldanyUpdate;

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_PldIratPeldanyok_Id, "EREC_PldIratPeldanyok", "IratPeldanyAtadas").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Iratpéldány átadása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id">>Az átadandó iratpéldány Id-ja</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A következõ kezelõ Id-ja</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atadas(ExecParam execParam, String erec_PldIratPeldanyok_Id
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.AtadasraKijeloles(execParam, erec_PldIratPeldanyok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
            result.CheckError();

            //átadott állapotba kerülés
            if (String.IsNullOrEmpty(result.Uid))
            {
                Logger.Debug("Nincs kézbesítési tétel");
            }
            else
            {
                //Kézbesítési tétel átadása
                Logger.Debug("Kézbesítési tétel átadása");
                string kezbesitesiTetelId = result.Uid;
                ExecParam xpmAtadas = execParam.Clone();
                EREC_IraKezbesitesiTetelekService svcKezbesitesiTeletelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                Result resAtadas = svcKezbesitesiTeletelek.Atadas(xpmAtadas, kezbesitesiTetelId);
                resAtadas.CheckError();
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Átadásra kijelölése sztornózása (visszavonása)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result AtadasraKijelolesSztorno(ExecParam execParam, String erec_PldIratPeldanyok_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_PldIratPeldanyok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52360);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_PldIratPeldanyok erec_PldIratPeldany;


            // IratPéldány lekérése:
            EREC_PldIratPeldanyokService service_iratPeldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
            ExecParam execParam_IratPeldanyGet = execParam.Clone();
            execParam_IratPeldanyGet.Record_Id = erec_PldIratPeldanyok_Id;

            Result result_IratPeldanyGet = service_iratPeldanyok.Get(execParam_IratPeldanyGet);
            result_IratPeldanyGet.CheckError();

            if (result_IratPeldanyGet.Record == null)
            {
                // hiba
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52361);
                //log.WsEnd(execParam_IratPeldanyGet, result1);
                //return result1;

                throw new ResultException(52361);
            }

            erec_PldIratPeldany = (EREC_PldIratPeldanyok)result_IratPeldanyGet.Record;

            Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz iratPeldanyStatusz =
                Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(erec_PldIratPeldany);
            ExecParam execParam_visszavonhato = execParam.Clone();

            if (!Contentum.eRecord.BaseUtility.IratPeldanyok.AtadasraKijelolesVisszavonhato(iratPeldanyStatusz, execParam_visszavonhato))
            {
                // Nem vonható vissza
                //Result result1 = ResultError.CreateNewResultWithErrorCode(52362);
                //log.WsEnd(execParam_visszavonhato, result1);
                //return result1;

                throw new ResultException(52362);
            }

            // IRATPÉLDÁNY UPDATE:
            // mezõk módosíthatóságának letiltása:
            erec_PldIratPeldany.Updated.SetValueAll(false);
            erec_PldIratPeldany.Base.Updated.SetValueAll(false);
            erec_PldIratPeldany.Base.Updated.Ver = true;

            // Módosítani kell: Felelõs, Állapot (Továbbításalattiállapot)

            string felelos_Id = erec_PldIratPeldany.Csoport_Id_Felelos;

            // Felelõs visszaállítása az elõzõ felelõsre:
            erec_PldIratPeldany.Csoport_Id_Felelos = erec_PldIratPeldany.Csoport_Id_Felelos_Elozo;
            erec_PldIratPeldany.Updated.Csoport_Id_Felelos = true;

            // Elõzõ felelõs állítása a mostanira:
            erec_PldIratPeldany.Csoport_Id_Felelos_Elozo = felelos_Id;
            erec_PldIratPeldany.Updated.Csoport_Id_Felelos_Elozo = true;

            // Állapot állítása:
            if (erec_PldIratPeldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt
                && !String.IsNullOrEmpty(erec_PldIratPeldany.TovabbitasAlattAllapot))
            {
                erec_PldIratPeldany.Allapot = erec_PldIratPeldany.TovabbitasAlattAllapot;
                erec_PldIratPeldany.Updated.Allapot = true;

                erec_PldIratPeldany.TovabbitasAlattAllapot = Constants.BusinessDocument.nullString;
                erec_PldIratPeldany.Updated.TovabbitasAlattAllapot = true;
            }

            ExecParam execParam_PldUpdate = execParam.Clone();
            execParam_PldUpdate.Record_Id = erec_PldIratPeldany.Id;

            // Közvetlenül sp objektumnak hívjuk az update-jét, hogy ne csináljon megint kézbesítési tételt
            // a felelõs megváltozása miatt:
            //Result result_PldUpdate = sp.Insert(Constants.Update, execParam_PldUpdate, erec_PldIratPeldany);
            Result result_PldUpdate = BaseUpdate(execParam_PldUpdate, erec_PldIratPeldany);
            result_PldUpdate.CheckError();

            /// Kézbesítési tételek vizsgálata:
            /// ha volt a rekordra kézbesítési tétel (átadásra kijelölt), kézb. tétel érvénytelenítése:
            /// 

            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbTetel = execParam.Clone();

            Result result_kezbTetelUpdate =
                service_KezbesitesiTetelek.AtadasraKijeloltekInvalidate(execParam_kezbTetel, erec_PldIratPeldany.Id);
            result_kezbTetelUpdate.CheckError();

            // ha minden OK:
            result = result_PldUpdate;

            #region ucm

            SetUCMJogosultsag(execParam, erec_PldIratPeldany.IraIrat_Id);

            #endregion

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_PldIratPeldanyok_Id, "EREC_PldIratpeldanyok", "IratPeldanyAtadasSztorno").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// Iratpéldány átvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id">Átveendõ iratpéldány</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atvetel(ExecParam execParam, String erec_PldIratPeldanyok_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_PldIratPeldanyok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // A tömeges átvétel fv. meghívva:
            string[] iratpeldanyIds = new string[1];
            iratpeldanyIds[0] = erec_PldIratPeldanyok_Id;

            result = this.Atvetel_Tomeges(execParam, iratpeldanyIds, execParam.Felhasznalo_Id);
            result.CheckError();

            // naplózás a tömeges átvételben            

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Iratpéldányok tömeges átvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratpeldanyok_Id_Array">Az átveendõ iratpéldányok Id-jai</param>
    /// <param name="kovetkezoOrzoId">Következõ õrzõ (átvevõ felhasználó)</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atvetel_Tomeges(ExecParam execParam, string[] iratpeldanyok_Id_Array, string kovetkezoOrzoId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_IraKezbesitesiTetelekService kezbTetelekService = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            EREC_IraKezbesitesiTetelekSearch kezbTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

            kezbTetelekSearch.Obj_Id.In(iratpeldanyok_Id_Array);

            kezbTetelekSearch.Allapot.In(new string[] {
                KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott,
                KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott
            });

            kezbTetelekSearch.Csoport_Id_Cel.In(new string[] { execParam.Felhasznalo_Id, execParam.FelhasznaloSzervezet_Id });

            Result kezbTetelekGetAllResult = kezbTetelekService.GetAll(execParam.Clone(), kezbTetelekSearch);
            kezbTetelekGetAllResult.CheckError();

            string[] kezbTetelIds = new string[kezbTetelekGetAllResult.Ds.Tables[0].Rows.Count];
            // ha a kézbesítési tételek száma nem egyezik az átveendõ küldemények számával -> hiba (kézbesítési tétel lekérése sikertelen)
            if (kezbTetelIds.Length != iratpeldanyok_Id_Array.Length)
                throw new ResultException(52224);

            for (int i = 0; i < kezbTetelIds.Length; i++)
            {
                kezbTetelIds[i] = kezbTetelekGetAllResult.Ds.Tables[0].Rows[i]["Id"].ToString();
            }

            result = kezbTetelekService.Atvetel_Tomeges(execParam.Clone(), kezbTetelIds);

            //naplózás a kezbTetelekService.Atvetel_Tomeges által visszahívott másik, nem WebMethod AtvetelTomeges metódusban            

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public Result AtvetelTomeges(ExecParam execParam, string Ids, string Vers, string KovetkezoOrzoId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Atvetel_Tomeges(execParam, Ids, Vers, KovetkezoOrzoId);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, Ids, "IratPeldanyAtvetel");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_PldIratPeldanyok::AtvetelTomeges: ", execParam, eventLogResult);
            #endregion

            #region ucm
            SetUCMJogosultsagPeldanyok(execParam, Ids);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public void AddKezelesiFeljegyzes(ExecParam p_ExecParam, string p_IratPeldanyID, string p_Leiras, string p_Tipus)
    {
        Logger.Debug("HataridosFeladatokService Insert start", p_ExecParam);
        Logger.Debug("Leiras: " + p_Leiras ?? "NULL");
        Logger.Debug("IratPeldanyID: " + p_IratPeldanyID ?? "NULL");
        Logger.Debug("Tipus: " + p_Tipus ?? "NULL");

        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
        ExecParam execParam_kezFeljInsert = p_ExecParam.Clone();

        EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
        erec_HataridosFeladatok.Updated.SetValueAll(false);
        erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

        // Iratpéldányhoz kötés
        erec_HataridosFeladatok.Obj_Id = p_IratPeldanyID;
        erec_HataridosFeladatok.Updated.Obj_Id = true;

        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok;
        erec_HataridosFeladatok.Updated.Obj_type = true;

        erec_HataridosFeladatok.Leiras = p_Leiras;
        erec_HataridosFeladatok.Updated.Leiras = true;

        erec_HataridosFeladatok.Altipus = p_Tipus;
        erec_HataridosFeladatok.Updated.Altipus = true;

        erec_HataridosFeladatok.Tipus = KodTarak.FELADAT_TIPUS.Megjegyzes;
        erec_HataridosFeladatok.Updated.Tipus = true;

        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(execParam_kezFeljInsert, erec_HataridosFeladatok);
        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
        {
            // hiba:
            Logger.Error("HataridosFeladatokService Insert hiba", execParam_kezFeljInsert, result_kezFeljInsert);
            throw new ResultException(result_kezFeljInsert);
        }
        Logger.Debug("HataridosFeladatokService Insert end", p_ExecParam);
    }

    #region Visszaküldés
    /// <summary>
    /// Iratpéldány átvételének visszautasítása, küldemény visszaküldése
    /// A kézbesítési tételt a hívó szervíznek kell módosítania a sikeres futás után!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraKezbesitesiTetelek"></param>
    /// <returns></returns>
    public Result Visszakuldes(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_Id) || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_type))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Kézbesítési tétel ellenõrzés
            Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldheto(execParam, erec_IraKezbesitesiTetelek);
            result_kezbesitesiTetelCheck.CheckError();

            if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok)
            {
                Logger.Error(String.Format("Kézbesítési tételbõl kapott objektum típus: {0}; Várt objektum típus: {1}"
                    , erec_IraKezbesitesiTetelek.Obj_type
                    , Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok));

                // Hiba a visszaküldés során: a kézbesítési tétel objektum típusa nem megfelelõ!
                throw new ResultException(53707);
            }
            #endregion Kézbesítési tétel ellenõrzés

            string iratPeldany_Id = erec_IraKezbesitesiTetelek.Obj_Id;
            string visszakuldesIndoka = erec_IraKezbesitesiTetelek.Base.Note;

            #region Iratpéldány objektum lekérése
            ExecParam execParam_IratPeldanyGet = execParam.Clone();
            execParam_IratPeldanyGet.Record_Id = iratPeldany_Id;

            Result result_IratPeldanyGet = this.Get(execParam_IratPeldanyGet);
            result_IratPeldanyGet.CheckError();

            EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_IratPeldanyGet.Record;

            #endregion Iratpéldány objektum lekérése

            #region Iratpéldány UPDATE
            erec_PldIratPeldanyok.Updated.SetValueAll(false);
            erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);
            erec_PldIratPeldanyok.Base.Updated.Ver = true;

            // Visszaküldés: a következõ felelõs az elõzõ felelõs (és még jelenlegi õrzõ) lesz
            string felelos_Id = erec_PldIratPeldanyok.Csoport_Id_Felelos;

            if (!String.IsNullOrEmpty(erec_PldIratPeldanyok.Csoport_Id_Felelos_Elozo))
            {
                // Felelõs visszaállítása az elõzõ felelõsre:
                erec_PldIratPeldanyok.Csoport_Id_Felelos = erec_PldIratPeldanyok.Csoport_Id_Felelos_Elozo;
                erec_PldIratPeldanyok.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_PldIratPeldanyok.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_PldIratPeldanyok.Updated.Csoport_Id_Felelos_Elozo = true;
            }
            else
            {
                erec_PldIratPeldanyok.Csoport_Id_Felelos = erec_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo;
                erec_PldIratPeldanyok.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_PldIratPeldanyok.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_PldIratPeldanyok.Updated.Csoport_Id_Felelos_Elozo = true;
            }

            ExecParam execParam_iratPeldanyUpdate = execParam.Clone();
            execParam_iratPeldanyUpdate.Record_Id = iratPeldany_Id;
            // a StoredProcedure szintet hívjuk, hogy ne keletkezzen új kézbesítési tétel
            result = sp.Insert(Constants.Update, execParam_iratPeldanyUpdate, erec_PldIratPeldanyok);
            result.CheckError();

            #endregion Iratpéldány UPDATE

            #region Kezelési feljegyzés
            // Kezelési feljegyzés létrehozása, ha van megjegyzés:
            if (!String.IsNullOrEmpty(visszakuldesIndoka.Trim()))
            {
                Logger.Debug("Visszakuldes indoka:" + visszakuldesIndoka.Trim());
                this.AddKezelesiFeljegyzes(execParam, iratPeldany_Id, visszakuldesIndoka.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }
            #endregion Kezelési feljegyzés

            #region Eseménynaplózás
            // naplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, iratPeldany_Id, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok, "IratPeldanyVisszakuldes").Record;
                if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
                {
                    eventLogRecord.Base.Note = visszakuldesIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }
    #endregion Visszaküldés

    /// <summary>
    /// Iratpéldány sztornózása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id">A sztornózandó iratpéldány Id-ja</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Sztornozas(ExecParam execParam, String erec_PldIratPeldanyok_Id, String SztornozasIndoka, bool? SztornoMegsemmisitve, string SztornoMegsemmisitesDatuma)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_PldIratPeldanyok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52300);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // IratPéldány hierarchia lekérése:
            EREC_PldIratPeldanyokService service_iratPeldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
            ExecParam execParam_IratPeldanyHierGet = execParam.Clone();
            execParam_IratPeldanyHierGet.Record_Id = erec_PldIratPeldanyok_Id;

            Result result_IratPeldanyHierGet = service_iratPeldanyok.GetIratPeldanyHierarchia(execParam_IratPeldanyHierGet);
            result_IratPeldanyHierGet.CheckError();

            if (result_IratPeldanyHierGet.Record == null)
            {
                // hiba
                throw new ResultException(52302);
            }

            IratPeldanyHierarchia iratPeldanyHierarchia = (IratPeldanyHierarchia)result_IratPeldanyHierGet.Record;

            EREC_PldIratPeldanyok erec_PldIratPeldany = iratPeldanyHierarchia.IratPeldanyObj;

            Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz iratPeldanyStatusz =
                Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(erec_PldIratPeldany);

            EREC_IraIratokService service_IraIratok = new EREC_IraIratokService(this.dataContext);

            EREC_IraIratok erec_IraIrat = iratPeldanyHierarchia.IratObj;

            #region Sztornózható
            ExecParam execParam_sztornozhato = execParam.Clone();

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(iratPeldanyHierarchia.UgyiratObj);

            ErrorDetails errorDetail;
            if (Contentum.eRecord.BaseUtility.IratPeldanyok.Sztornozhato(iratPeldanyStatusz, ugyiratStatusz, execParam_sztornozhato, out errorDetail) == false)
            {
                // hiba:
                throw new ResultException(52301);
            }

            // Irat ellenõrzés:

            bool ugyiratInditoIrat = EREC_IraIratokService.UgyiratInditoIrat(erec_IraIrat);
            if (ugyiratInditoIrat)
            {
                // TODO: Ügyirat indító irat csak akkor sztornózható, ha a többi irat már sztornózva van (?)
            }

            // Elsõdleges iratpéldány csak akkor sztornózható, ha nincs már rajta kívül másik nem sztornózott iratpéldány
            if (erec_PldIratPeldany.Sorszam == "1")
            {
                // Ellenõrzés, van-e még másik iratpéldány is:
                EREC_PldIratPeldanyokSearch search_pld = new EREC_PldIratPeldanyokSearch();

                search_pld.IraIrat_Id.Filter(erec_PldIratPeldany.IraIrat_Id);

                // NEM 1-es sorszámú:
                search_pld.Sorszam.NotEquals("1");

                // NEM Sztornózott, és NEM felszabadított
                search_pld.Allapot.NotIn(new string[] {
                    KodTarak.IRATPELDANY_ALLAPOT.Sztornozott,
                    KodTarak.IRATPELDANY_ALLAPOT.Felszabaditott
                });

                Result result_pld = this.GetAll(execParam.Clone(), search_pld);
                result_pld.CheckError();

                // ha van találat --> van még iratpéldány az iratban --> hiba:
                if (result_pld.GetCount > 0)
                {
                    // hiba: Az elsõdleges iratpéldány nem sztornózható, mert még több iratpéldány is létezik az irathoz.
                    throw new ResultException(52305);
                }
            }

            #endregion

            #region Kimenõ küldeménybõl kivétel, ha szükséges
            if (erec_PldIratPeldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben
                || erec_PldIratPeldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Postazott)
            {
                // kiválasztott iratpéldány-hoz van-e már kimenõ küldemény (Állapota: Kimenõ, összeállítás alatt; és csak 1 lehet):
                EREC_Kuldemeny_IratPeldanyaiService service_kuldIratPeldanyai = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
                EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();

                search.Peldany_Id.Value = erec_PldIratPeldany.Id;
                search.Peldany_Id.Operator = Query.Operators.equals;

                Result result_kuldIratPeldanyaiGetAll = service_kuldIratPeldanyai.GetAllWithExtension(execParam.Clone(), search);
                result_kuldIratPeldanyaiGetAll.CheckError();

                if (result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows.Count > 0)
                {
                    ExecParam execParam_remove = execParam.Clone();

                    foreach (DataRow row in result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows)
                    {
                        // elvileg csak egy sor lehet, ahol nem sztornózott a küldemény
                        if (row["Kuldemeny_Allapot"].ToString() != KodTarak.KULDEMENY_ALLAPOT.Sztorno)
                        {
                            //string kimenoKuldemenyId = row["KuldKuldemeny_Id"].ToString();
                            // Nem ezt hívjuk, mert felesleges állítani a példány állapotát,
                            // ezért csak a hozzárendelést töröljük
                            //RemoveIratPeldanyFromKimenoKuldemeny(execParam_remove, erec_PldIratPeldany.Id, kimenoKuldemenyId);

                            // Kimenõ küldemény Id megjegyzése
                            string kimenoKuldemenyId = row["KuldKuldemeny_Id"].ToString();

                            // EREC_Kuldemeny_IratPeldanyai sor törlése a táblából:
                            execParam_remove.Record_Id = row["Id"].ToString();

                            Result result_kuldPldInv = service_kuldIratPeldanyai.Invalidate(execParam_remove);
                            result_kuldPldInv.CheckError();

                            #region Kimenõ küldemény sztornó, ha kiürült

                            #region Kimenõ küldeményben lévõ iratpéldányok lekérése
                            EREC_PldIratPeldanyokSearch search_kimenoKuldemenyPeldanyai = new EREC_PldIratPeldanyokSearch();

                            search_kimenoKuldemenyPeldanyai.Allapot.Value = KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
                            search_kimenoKuldemenyPeldanyai.Allapot.Operator = Query.Operators.notequals;

                            ExecParam execParam_kimenoKuldemenyPeldanyai = execParam.Clone();
                            Result result_kimenoKuldemenyPeldanyai = GetAllWithExtensionByKimenoKuldemeny(execParam_kimenoKuldemenyPeldanyai
                                , search_kimenoKuldemenyPeldanyai, kimenoKuldemenyId);
                            result_kimenoKuldemenyPeldanyai.CheckError();

                            #endregion Kimenõ küldeményben lévõ iratpéldányok lekérése

                            // üres a kimenõ küldemény
                            if (result_kimenoKuldemenyPeldanyai.Ds.Tables[0].Rows.Count == 0)
                            {
                                EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);
                                ExecParam execParam_kuldSztorno = execParam.Clone();
                                execParam_kuldSztorno.Record_Id = kimenoKuldemenyId;

                                Result result_kuldSztorno = service_kuld.KimenoKuldemenySztornoWithoutPeldanyCheck(execParam_kuldSztorno);
                                result_kuldSztorno.CheckError();
                            }
                            #endregion Kimenõ küldemény sztornó, ha kiürült
                        }
                    }
                }
            }
            #endregion Kimenõ küldeménybõl kivétel, ha szükséges

            // IratPéldány UPDATE
            // mezõk módosíthatóságának letiltása:
            erec_PldIratPeldany.Updated.SetValueAll(false);
            erec_PldIratPeldany.Base.Updated.SetValueAll(false);
            erec_PldIratPeldany.Base.Updated.Ver = true;

            // Módosítani kell: Állapot (Továbbításalattiállapot), SztornirozasDat

            // SztornirozasDat
            erec_PldIratPeldany.SztornirozasDat = DateTime.Now.ToString();
            erec_PldIratPeldany.Updated.SztornirozasDat = true;

            // Allapot
            erec_PldIratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
            erec_PldIratPeldany.Updated.Allapot = true;

            // TovabbitasAlattAllapot
            erec_PldIratPeldany.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;//"";
            erec_PldIratPeldany.Updated.TovabbitasAlattAllapot = true;

            #region BLG_2948
            //LZS - a papíralapú példányoknál lehetséges property-k business object-be töltése.
            if (SztornoMegsemmisitve != null)
            {
                erec_PldIratPeldany.IratPeldanyMegsemmisitve = SztornoMegsemmisitve.Value ? "1" : "0";
                erec_PldIratPeldany.Updated.IratPeldanyMegsemmisitve = true;
            }

            if (!string.IsNullOrEmpty(SztornoMegsemmisitesDatuma))
            {
                erec_PldIratPeldany.IratPeldanyMegsemmisitesDatuma = SztornoMegsemmisitesDatuma;
                erec_PldIratPeldany.Updated.IratPeldanyMegsemmisitesDatuma = true;
            }
            else
                erec_PldIratPeldany.Updated.IratPeldanyMegsemmisitesDatuma = false;
            #endregion

            ExecParam execParam_iratPeldanyUpdate = execParam.Clone();
            execParam_iratPeldanyUpdate.Record_Id = erec_PldIratPeldany.Id;

            Result result_iratPeldanyUpdate = service_iratPeldanyok.Update(execParam_iratPeldanyUpdate, erec_PldIratPeldany);
            result_iratPeldanyUpdate.CheckError();

            // Ha az összes iratpéldány sztornózott lett ezzel az iratban, akkor az iratot is sztornózottá tesszük

            // Iratpéldányok GETALL
            ExecParam execParam_PldGetAll = execParam.Clone();

            var iratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
            iratPeldanyokSearch.IraIrat_Id.Filter(erec_PldIratPeldany.IraIrat_Id);

            Result result_PldGetAll = service_iratPeldanyok.GetAll(execParam_PldGetAll, iratPeldanyokSearch);
            result_PldGetAll.CheckError();

            int sztornozottIratPeldanyokSzama = 0;

            try
            {
                foreach (DataRow row in result_PldGetAll.Ds.Tables[0].Rows)
                {
                    String allapot = row["Allapot"].ToString();
                    if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                    {
                        sztornozottIratPeldanyokSzama++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch
            {
                // hiba:
                throw new ResultException(52303);
            }

            if (sztornozottIratPeldanyokSzama > 0
                && sztornozottIratPeldanyokSzama == result_PldGetAll.Ds.Tables[0].Rows.Count)
            {
                // Irat sztornózása:

                ExecParam xpmIratSztornozas = execParam.Clone();
                service_IraIratok.Sztornozas(xpmIratSztornozas, erec_IraIrat, iratPeldanyHierarchia.UgyiratObj.Id);

                // Ha ez az irat az ügyiratDarabot indító irat --> ügyiratDarab lezárás
                if (ugyiratInditoIrat)
                {
                    // TODO: 
                    // ÜgyiratDarab lezárás:

                }
            }

            // AdathordozoTipusa mező frissítése az iratnál:
            service_IraIratok.RefreshAdathordozoTipusaByIratPld(execParam, erec_IraIrat.Id);

            // Az esetleges kézbesítési tételek törlése:
            EREC_IraKezbesitesiTetelekService service_kezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbTetelInvalidate = execParam.Clone();

            Result result_kezbTetelInvalidate =
                service_kezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_kezbTetelInvalidate, erec_PldIratPeldany.Id);
            result_kezbTetelInvalidate.CheckError();

            #region Vonalkód felszabadítása

            if (!string.IsNullOrEmpty(erec_PldIratPeldany.BarCode))
            {
                var barkodService = new KRT_BarkodokService(dataContext);
                Result barkodResult = barkodService.FreeBarcode(execParam.Clone(), erec_PldIratPeldany.BarCode);
                barkodResult.CheckError();
            }

            #endregion

            result = result_iratPeldanyUpdate;

            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_PldIratPeldanyok_Id, "EREC_PldIratpeldanyok", "IratpeldanySztorno").Record;

                if (!String.IsNullOrEmpty(SztornozasIndoka))
                {
                    eventLogRecord.Base.Note = SztornozasIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Kimenõ küldemény összeállítása (vagy mentése) a megadott iratpéldányokból
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id_Array"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result KimenoKuldemeny_Save(ExecParam execParam, String erec_PldIratPeldany_kivalasztott, String[] erec_PldIratPeldanyok_Id_Array, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, string kimenoKuldemenyVonalkod, String[] dokumentum_Id_Array)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Info("Kimenõ küldemény összeállítása/mentése", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_PldIratPeldany_kivalasztott)
            || erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length <= 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52510);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // kiválasztott iratpéldány-hoz van-e már kimenõ küldemény (Állapota: Kimenõ, összeállítás alatt; és csak 1 lehet):
            EREC_Kuldemeny_IratPeldanyaiService service_kuldIratPeldanyai = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
            EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();

            search.Peldany_Id.Filter(erec_PldIratPeldany_kivalasztott);

            Result result_kuldIratPeldanyaiGetAll = service_kuldIratPeldanyai.GetAll(execParam, search);
            result_kuldIratPeldanyaiGetAll.CheckError();

            // az iratpéldányokhoz tartozó kimenõ küldemények száma (ahol az állapot Kimenõ, összeállítás alatt)
            int kuldCount = 0;
            string kimenoKuldemeny_Id = "";
            //string kuldId = "";
            string originalBarCodeKuld = null;
            bool isChangedBarCode = false;

            if (result_kuldIratPeldanyaiGetAll.GetCount > 0)
            {
                List<string> kimenoKuldemenyek = new List<string>();

                foreach (DataRow row in result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows)
                {
                    string kuldId = row["KuldKuldemeny_Id"].ToString();

                    if (!kimenoKuldemenyek.Contains(kuldId))
                    {
                        kimenoKuldemenyek.Add(kuldId);
                    }
                }
                // küldemény állapotának lekérése:
                EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);
                ExecParam execParam_kuldGet = execParam.Clone();

                var search_kuld = new EREC_KuldKuldemenyekSearch();
                search_kuld.Id.In(kimenoKuldemenyek);

                Result result_kuldGetAll = service_kuld.GetAll(execParam_kuldGet, search_kuld);
                result_kuldGetAll.CheckError();

                // TODO: kell állapotellenõrzés? vagy bármilyen állapotú egyéb kimenõ küldemény esetén hiba
                DataRow[] rows_kimenoben = result_kuldGetAll.Ds.Tables[0].Select(String.Format("Allapot='{0}'", KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt));
                //EREC_KuldKuldemenyek kimenoKuld = (EREC_KuldKuldemenyek)result_kuldGet.Record;

                //if (kimenoKuld.Allapot == KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt)
                //{
                //    kimenoKuldemeny_Id = kuldId;
                //    kuldCount++;
                //}

                kuldCount = rows_kimenoben.Length;
                //}

                // ha több kimenõ küldemény is van, hiba:
                if (kuldCount > 1)
                {
                    // hiba:
                    Logger.Error("Több kimenõ küldemény is tartozik az iratpéldányhoz", execParam);
                    throw new ResultException(52511);
                }
                else if (kuldCount == 1)
                {
                    kimenoKuldemeny_Id = rows_kimenoben[0]["Id"].ToString();

                    originalBarCodeKuld = rows_kimenoben[0]["BarCode"].ToString(); // result_kuldGetAll.Ds.Tables[0].Rows[0]["BarCode"].ToString();
                    //if (result_kuldGetAll.Ds.Tables[0].Rows[0]["BarCode"] != kimenoKuldemenyVonalkod)
                    if (originalBarCodeKuld != kimenoKuldemenyVonalkod)
                    {
                        isChangedBarCode = true;
                    }
                }
            }

            #region Ablakos boríték ellenõrzés

            // "ablakos boríték" akkor lehet, ha pontosan 1 iratpéldányt expediálunk,
            // és a megadott vonalkód megegyezik a kiválaszott példány vonalkódjával
            // ilyenkor a vonalkódot átkötjük a kimenõ küldeményre
            // (a példány táblában meghagyjuk, csak a KRT_BarKodok-ban módosul)
            bool bAblakosBoritek = false;
            bool isOldAblakosBoritek = false;
            if (kuldCount == 1)
            {
                // IratPéldány vonalkód ellenõrzése:
                // 1. Ha a régi kimenõ küldeményben ugyanaz a vonalkód, mint a kiválasztott példányé
                // akkor a régi kimenõ küldemény ablakos boríték volt (másképp nem lehetnek azonos vonalkódok)
                // 2. Ha az új kimenõ küldemény vonalkód ugyanaz, mint a kiválasztott példányé
                // akkor a kimenõ küldemény ablakos boríték lesz (másképp nem lehetnek azonos vonalkódok)
                ExecParam execParam_pld = execParam.Clone();
                execParam_pld.Record_Id = erec_PldIratPeldany_kivalasztott;

                Result resPldGet = sp.Get(execParam_pld);
                resPldGet.CheckError();

                EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)resPldGet.Record;
                string originalBarCodePld = erec_PldIratPeldanyok.BarCode;

                isOldAblakosBoritek = (!String.IsNullOrEmpty(originalBarCodePld) && originalBarCodePld == originalBarCodeKuld);
                bAblakosBoritek = (erec_PldIratPeldanyok_Id_Array.Length == 1 && !String.IsNullOrEmpty(kimenoKuldemenyVonalkod) && originalBarCodePld == kimenoKuldemenyVonalkod);

                if (!bAblakosBoritek && (originalBarCodePld == kimenoKuldemenyVonalkod))
                {
                    //"Hiba a kimenõ küldemény mentése során: A vonalkód megegyezik a kiválasztott példány vonalkódjával! (nem ablakos boríték esetén önálló vonalkódot kell adni a küldeménynek)"
                    throw new ResultException(52517);
                }
            }
            else if (kuldCount == 0)
            {
                // itt, mivel a régi küldeményt nem kell vizsgálni,
                // csak akkor kérjük le a példányt, ha egy van megadva, másképp eleve nem lehet ablakos boríték
                if (erec_PldIratPeldanyok_Id_Array.Length == 1)
                {
                    // IratPéldány vonalkód ellenõrzése:
                    // 1. Ha az új kimenõ küldemény vonalkód ugyanaz, mint a kiválasztott példányé
                    // akkor a kimenõ küldemény ablakos boríték lesz (másképp nem lehetnek azonos vonalkódok)
                    ExecParam execParam_pld = execParam.Clone();
                    execParam_pld.Record_Id = erec_PldIratPeldany_kivalasztott;

                    Result resPldGet = sp.Get(execParam_pld);
                    resPldGet.CheckError();

                    EREC_PldIratPeldanyok erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)resPldGet.Record;
                    string originalBarCodePld = erec_PldIratPeldanyok.BarCode;

                    bAblakosBoritek = (!String.IsNullOrEmpty(kimenoKuldemenyVonalkod) && originalBarCodePld == kimenoKuldemenyVonalkod);
                }

            }
            #endregion Ablakos boríték ellenõrzés

            if (kuldCount == 1)
            {
                #region Vonalkód felszabadítása
                if (isChangedBarCode)
                {
                    if (isOldAblakosBoritek && !bAblakosBoritek)
                    {
                        // ablakos -> nem ablakos
                        // régi kimenõ küldemény vonalkódot vissza kell kötni a példányra
                        KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                        Result resultBarCodeGetByValue = barkodService.GetBarcodeByValue(execParam.Clone(), originalBarCodeKuld);
                        resultBarCodeGetByValue.CheckError();
                        KRT_Barkodok krt_Barkodok = (KRT_Barkodok)resultBarCodeGetByValue.Record;

                        Result barkodResult = barkodService.BarkodBindToIratPeldany(execParam.Clone(), krt_Barkodok.Id, erec_PldIratPeldany_kivalasztott, krt_Barkodok.Base.Ver);
                        barkodResult.CheckError();
                    }
                    else if (!isOldAblakosBoritek)
                    {
                        // nem ablakos -> nem ablakos
                        // régi kimenõ küldemény vonalkódot fel kell szabadítani
                        // nem ablakos -> ablakos
                        // régi kimenõ küldemény vonalkódot fel kell szabadítani, példány vonalkódot fel kell szabadítani, hogy át lehessen kötni kimenõ küldeményre
                        if (!String.IsNullOrEmpty(originalBarCodeKuld))
                        {
                            KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                            Result barkodResult = barkodService.FreeBarcode(execParam.Clone(), originalBarCodeKuld);
                            barkodResult.CheckError();
                        }

                        if (bAblakosBoritek)
                        {
                            if (!String.IsNullOrEmpty(kimenoKuldemenyVonalkod))
                            {
                                KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                                Result barkodResult = barkodService.FreeBarcode(execParam.Clone(), kimenoKuldemenyVonalkod);
                                barkodResult.CheckError();
                            }
                        }
                    }


                    //if (!String.IsNullOrEmpty(originalBarCode))
                    //{
                    //    KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                    //    Result barkodResult = barkodService.FreeBarcode(execParam.Clone(), originalBarCode);
                    //    if (!string.IsNullOrEmpty(barkodResult.ErrorCode))
                    //        throw new ResultException(barkodResult);
                    //}
                }
                #endregion

                result = SaveKimenoKuldemeny(execParam, kimenoKuldemeny_Id, erec_PldIratPeldanyok_Id_Array);
            }
            else if (kuldCount == 0)
            {
                if (bAblakosBoritek)
                {
                    // nincs -> ablakos
                    // példány vonalkódot fel kell szabadítani, hogy át lehessen kötni kimenõ küldeményre
                    if (!String.IsNullOrEmpty(kimenoKuldemenyVonalkod))
                    {
                        KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                        Result barkodResult = barkodService.FreeBarcode(execParam.Clone(), kimenoKuldemenyVonalkod);
                        barkodResult.CheckError();
                    }
                }

                result = CreateKimenoKuldemeny(execParam, erec_PldIratPeldanyok_Id_Array, kimenoKuldemenyVonalkod);
            }

            string kuldemenyId = result.Uid;
            Logger.Debug("Kimeno kuldemeny id: " + kuldemenyId);

            Logger.Debug("Iratpeldany cimzesi adatok update start");
            Logger.Debug("Iratpeldany lekerese start: " + erec_PldIratPeldany_kivalasztott);
            ExecParam xpm = execParam.Clone();
            xpm.Record_Id = erec_PldIratPeldany_kivalasztott;

            Result res = this.Get(xpm);

            if (res.IsError)
            {
                Logger.Error("Iratpeldany lekerese hiba", xpm, res);
                throw new ResultException(res);
            }

            EREC_PldIratPeldanyok peldany = (EREC_PldIratPeldanyok)res.Record;
            Logger.Debug("Iratpeldany lekerese end");

            //TODO: nem elég a kiválasztott iratpéldányt nézni, hogy változott-e a cím (iratpéldány irányból csak ugyanolyan címû iratok lehettek, de postázás irányból különbözõek is)
            bool needUpdate = isChangedBarCode || Utils.IsNeedUpdateBusinessDocument(peldany, _EREC_PldIratPeldanyok);

            if (needUpdate)
            {
                Logger.Debug("Kell update");
                Logger.Debug("Kimeno kuldemeny update start");
                xpm.Record_Id = kuldemenyId;
                EREC_KuldKuldemenyekService svc = new EREC_KuldKuldemenyekService(this.dataContext);
                res = svc.Get(xpm);
                if (res.IsError)
                {
                    Logger.Error("Kimeno kuldemeny lekerese hiba", xpm, res);
                    throw new ResultException(res);
                }
                EREC_KuldKuldemenyek kimenoKuldemeny = (EREC_KuldKuldemenyek)res.Record;
                kimenoKuldemeny.Updated.SetValueAll(false);
                kimenoKuldemeny.Base.Updated.SetValueAll(false);
                kimenoKuldemeny.Base.Updated.Ver = true;

                #region Küldemény Címzett adatainak kitöltése
                if (!String.IsNullOrEmpty(_EREC_PldIratPeldanyok.KuldesMod))
                {
                    kimenoKuldemeny.KuldesMod = _EREC_PldIratPeldanyok.KuldesMod;
                    kimenoKuldemeny.Updated.KuldesMod = true;
                }

                // A címzett adatok a bekuldo(partner) mezõibe kerülnek kimenõ küldeménynél:

                kimenoKuldemeny.NevSTR_Bekuldo = _EREC_PldIratPeldanyok.NevSTR_Cimzett;
                kimenoKuldemeny.Updated.NevSTR_Bekuldo = true;

                kimenoKuldemeny.Partner_Id_Bekuldo = _EREC_PldIratPeldanyok.Partner_Id_Cimzett;
                kimenoKuldemeny.Updated.Partner_Id_Bekuldo = true;

				// BUG_13213
                kimenoKuldemeny.Partner_Id_BekuldoKapcsolt = _EREC_PldIratPeldanyok.Partner_Id_CimzettKapcsolt;
                kimenoKuldemeny.Updated.Partner_Id_BekuldoKapcsolt = true;

                kimenoKuldemeny.CimSTR_Bekuldo = _EREC_PldIratPeldanyok.CimSTR_Cimzett;
                kimenoKuldemeny.Updated.CimSTR_Bekuldo = true;

                kimenoKuldemeny.Cim_Id = _EREC_PldIratPeldanyok.Cim_id_Cimzett;
                kimenoKuldemeny.Updated.Cim_Id = true;

                if (isChangedBarCode)
                {
                    kimenoKuldemeny.BarCode = String.IsNullOrEmpty(kimenoKuldemenyVonalkod) ? Contentum.eUtility.Constants.BusinessDocument.nullString : kimenoKuldemenyVonalkod;
                    kimenoKuldemeny.Updated.BarCode = true;
                }

                res = svc.Update(xpm, kimenoKuldemeny);

                if (res.IsError)
                {
                    Logger.Error("Kimeno kuldemeny update hiba", xpm, res);
                    throw new ResultException(res);
                }

                Logger.Debug("Kimeno kuldemeny update end");

                #endregion

                Logger.Debug("Kimeno kuldemeny update end");
                Logger.Debug("IratPeldanyGetAll start");
                xpm.Record_Id = String.Empty;
                EREC_PldIratPeldanyokSearch searchPeldany = new EREC_PldIratPeldanyokSearch();
                searchPeldany.Id.Value = Contentum.eUtility.Search.GetSqlInnerString(erec_PldIratPeldanyok_Id_Array);
                searchPeldany.Id.Operator = Query.Operators.inner;
                res = this.GetAll(xpm, searchPeldany);
                if (res.IsError)
                {
                    Logger.Error("IratPeldanyokGetAll hiba", xpm, res);
                    throw new ResultException(res);
                }
                foreach (DataRow row in res.Ds.Tables[0].Rows)   //**************
                {
                    string id = row["Id"].ToString();
                    string ver = row["Ver"].ToString();
                    Logger.Debug("Iratpeldany update: " + id);
                    xpm.Record_Id = id;
                    _EREC_PldIratPeldanyok.Base.Ver = ver;
                    _EREC_PldIratPeldanyok.Base.Updated.Ver = true;
                    res = this.BaseUpdate(xpm, _EREC_PldIratPeldanyok);
                    if (res.IsError)
                    {
                        Logger.Error("Iratpeldany update hiba", xpm, res);
                        throw new ResultException(res);
                    }
                }
                Logger.Debug("IratPeldanyGetAll end");
            }
            else
            {
                Logger.Debug("Nem kell update");
            }

            Logger.Debug("Iratpeldany cimzesi adatok update end");

            //dokumentumok hozzákapcsolása a kimenõ küldeményhez
            AddDokumentumokToKimenoKuldemeny(execParam, kuldemenyId, dokumentum_Id_Array, erec_PldIratPeldanyok_Id_Array);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region Segédeljárások kimenõ küldemény összeállításához


    /// <summary>
    /// 
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id_Array"></param>
    /// <returns>Result.Uid a létrehozott kimenõ küldemény id-ja</returns>
    private Result CreateKimenoKuldemeny(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array, string kimenoKuldemenyVonalkod)
    {
        Logger.Info("Kimenõ küldemény létrehozása", execParam);

        // kimenõ küldemény létrehozása:
        EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);

        EREC_KuldKuldemenyek erec_KuldKuldemeny = new EREC_KuldKuldemenyek();
        erec_KuldKuldemeny.Updated.SetValueAll(false);
        erec_KuldKuldemeny.Base.Updated.SetValueAll(false);

        erec_KuldKuldemeny.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Kimeno;
        erec_KuldKuldemeny.Updated.PostazasIranya = true;

        erec_KuldKuldemeny.Allapot = KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt;
        erec_KuldKuldemeny.Updated.Allapot = true;

        erec_KuldKuldemeny.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando;
        erec_KuldKuldemeny.Updated.IktatniKell = true;

        erec_KuldKuldemeny.Csoport_Id_Felelos = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
        erec_KuldKuldemeny.Updated.Csoport_Id_Felelos = true;

        erec_KuldKuldemeny.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
        erec_KuldKuldemeny.Updated.FelhasznaloCsoport_Id_Orzo = true;

        erec_KuldKuldemeny.BeerkezesIdeje = DateTime.Now.ToString();
        erec_KuldKuldemeny.Updated.BeerkezesIdeje = true;

        // Küldésmód: Az iratpéldányból kell venni (pl. Postai tértivevényes)
        #region IratPéldány GET

        EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);
        ExecParam execParam_pld = execParam.Clone();
        execParam_pld.Record_Id = erec_PldIratPeldanyok_Id_Array[0];

        Result result_pldGet = service_pld.Get(execParam_pld);
        result_pldGet.CheckError();

        EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)result_pldGet.Record;

        #endregion

        erec_KuldKuldemeny.Updated.KuldesMod = true;

        erec_KuldKuldemeny.Surgosseg = KodTarak.SURGOSSEG.Normal;
        erec_KuldKuldemeny.Updated.Surgosseg = true;

        erec_KuldKuldemeny.PeldanySzam = "1";
        erec_KuldKuldemeny.Updated.PeldanySzam = true;

        #region Küldemény Címzett adatainak kitöltése 

        // A címzett adatok a bekuldo(partner) mezõibe kerülnek kimenõ küldeménynél:
        if (!String.IsNullOrEmpty(iratPeldany.KuldesMod))
        {
            erec_KuldKuldemeny.KuldesMod = iratPeldany.KuldesMod;
        }
        else
        {
            erec_KuldKuldemeny.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima; // valami kellett neki, mert kötelezõ
        }
        // BLG_237
        // BUG_4345
        if (String.IsNullOrEmpty(erec_KuldKuldemeny.AdathordozoTipusa))
        {
            erec_KuldKuldemeny.AdathordozoTipusa = iratPeldany.UgyintezesModja;
            erec_KuldKuldemeny.Updated.AdathordozoTipusa = true;
        }

        // TODO Kuldemeny.adathordozoTipusa = iratpld.Ugyintezesmodja
        erec_KuldKuldemeny.NevSTR_Bekuldo = iratPeldany.NevSTR_Cimzett;
        erec_KuldKuldemeny.Updated.NevSTR_Bekuldo = true;

        erec_KuldKuldemeny.Partner_Id_Bekuldo = iratPeldany.Partner_Id_Cimzett;
        erec_KuldKuldemeny.Updated.Partner_Id_Bekuldo = true;

		// BUG_13213
        erec_KuldKuldemeny.Partner_Id_BekuldoKapcsolt = iratPeldany.Partner_Id_CimzettKapcsolt;
        erec_KuldKuldemeny.Updated.Partner_Id_BekuldoKapcsolt = true;

        erec_KuldKuldemeny.CimSTR_Bekuldo = iratPeldany.CimSTR_Cimzett;
        erec_KuldKuldemeny.Updated.CimSTR_Bekuldo = true;

        erec_KuldKuldemeny.Cim_Id = iratPeldany.Cim_id_Cimzett;
        erec_KuldKuldemeny.Updated.Cim_Id = true;

        if (!String.IsNullOrEmpty(kimenoKuldemenyVonalkod))
        {
            erec_KuldKuldemeny.BarCode = kimenoKuldemenyVonalkod;
            erec_KuldKuldemeny.Updated.BarCode = true;
        }

        #endregion

        Result result_kuldInsert = service_kuld.Insert(execParam, erec_KuldKuldemeny);
        result_kuldInsert.CheckError();

        string kuldemeny_Id_kimeno = result_kuldInsert.Uid;

        // iratpéldányok hozzáadása
        for (int i = 0; i < erec_PldIratPeldanyok_Id_Array.Length; i++)
        {
            string iratpeldany_Id = erec_PldIratPeldanyok_Id_Array[i];

            AddIratPeldanyToKimenoKuldemeny(execParam, iratpeldany_Id, kuldemeny_Id_kimeno);
        }

        return result_kuldInsert;
    }


    /// <summary>
    /// Kimenõ küldeményhez tartozó iratpéldányok beállítása (törlés/hozzáadás)
    /// </summary> 
    /// <returns>Result.Uid a kimenõ küldemény id-ja (== Kuldemeny_Id_kimeno param.)</returns>
    private Result SaveKimenoKuldemeny(ExecParam execParam, string Kuldemeny_Id_kimeno, string[] erec_PldIratPeldanyok_Id_Array)
    {
        Logger.Info("Kimenõ küldeményhez tartozó iratpéldányok beállítása", execParam);

        // küldemény iratpéldányainak lekérése:
        // ami nincs a tömbben, töröljük, ami hiányzik, felvesszük

        if (erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length == 0
            || String.IsNullOrEmpty(Kuldemeny_Id_kimeno))
        {
            throw new ResultException(52510);
        }

        var pld_list_ujak = new List<string>();
        for (int i = 0; i < erec_PldIratPeldanyok_Id_Array.Length; i++)
        {
            pld_list_ujak.Add(erec_PldIratPeldanyok_Id_Array[i]);
        }

        var service_kuldPld = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
        var search_kuldPldGetAll = new EREC_Kuldemeny_IratPeldanyaiSearch();
        search_kuldPldGetAll.KuldKuldemeny_Id.Filter(Kuldemeny_Id_kimeno);

        Result result_kuldPldGetAll = service_kuldPld.GetAll(execParam, search_kuldPldGetAll);
        result_kuldPldGetAll.CheckError();

        // A küldeményhez tartozó régi EREC_Kuldemeny_IratPeldanyai.Peldany_Id - EREC_Kuldemeny_IratPeldanyai.Id párok
        Dictionary<string, string> kuldPld_list_regiek = new Dictionary<string, string>();

        foreach (DataRow row in result_kuldPldGetAll.Ds.Tables[0].Rows)
        {
            string id = row["Id"].ToString();
            string pld_Id = row["Peldany_Id"].ToString();

            kuldPld_list_regiek.Add(pld_Id, id);
        }

        // régiek törlése, amelyek nincsenek bent az újban:
        foreach (KeyValuePair<string, string> kvp in kuldPld_list_regiek)
        {
            string old_kuldPld_PeldanyId = kvp.Key;
            string old_kuldPld_Id = kvp.Value;

            // amik nincsenek bent az újban:
            if (pld_list_ujak.Contains(old_kuldPld_PeldanyId) == false)
            {
                // az iratpéldány nincs benne az újak között, törölni kell:

                RemoveIratPeldanyFromKimenoKuldemeny(execParam, old_kuldPld_PeldanyId, old_kuldPld_Id);
            }
        }

        // újak hozzáadása, amik a régiben nincsenek benne:
        foreach (string pld_Id_uj in pld_list_ujak)
        {
            // amik nincsenek bent a régiben:
            if (kuldPld_list_regiek.ContainsKey(pld_Id_uj) == false)
            {
                // az iratpéldány nincs a régiek között, hozzá kell adni a küldeményhez:

                AddIratPeldanyToKimenoKuldemeny(execParam, pld_Id_uj, Kuldemeny_Id_kimeno);
            }
        }

        Result result = new Result();
        result.Uid = Kuldemeny_Id_kimeno;
        return result;
    }

    /// <summary>
    /// Iratpéldány hozzákapcsolása a kimenõ küldeményhez, és állapotának beállítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="IratPeldany_Id"></param>
    /// <param name="Kuldemeny_Id_kimeno"></param>
    private void AddIratPeldanyToKimenoKuldemeny(ExecParam execParam, string IratPeldany_Id, string Kuldemeny_Id_kimeno)
    {
        Logger.Info("Iratpéldány hozzákapcsolása a kimenõ küldeményhez, és állapotának beállítása", execParam);

        // kapcsolat létrehozása:
        EREC_Kuldemeny_IratPeldanyaiService service_kuldIratPeldanyai = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
        EREC_Kuldemeny_IratPeldanyai erec_Kuldemeny_IratPeldanyai = new EREC_Kuldemeny_IratPeldanyai();

        erec_Kuldemeny_IratPeldanyai.KuldKuldemeny_Id = Kuldemeny_Id_kimeno;

        erec_Kuldemeny_IratPeldanyai.Peldany_Id = IratPeldany_Id;

        Result result_kuldIratPld_Insert = service_kuldIratPeldanyai.Insert(execParam, erec_Kuldemeny_IratPeldanyai);
        result_kuldIratPld_Insert.CheckError();

        #region Iratpéldány új állapotának beállítása:

        // iratpéldány GET:
        EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);
        ExecParam execParam_PldGet = execParam.Clone();
        execParam_PldGet.Record_Id = IratPeldany_Id;

        Result result_PldGet = service_Pld.Get(execParam_PldGet);
        result_PldGet.CheckError();

        EREC_PldIratPeldanyok erec_PldIratPeldany = (EREC_PldIratPeldanyok)result_PldGet.Record;

        // iratpéldány UPDATE:
        erec_PldIratPeldany.Updated.SetValueAll(false);
        erec_PldIratPeldany.Base.Updated.SetValueAll(false);
        erec_PldIratPeldany.Base.Updated.Ver = true;

        // jelenlegi állapot elmentése a TovabbitasAlattAllapot mezõbe,
        // új állapot: Kimeno_kuldemenyben

        erec_PldIratPeldany.TovabbitasAlattAllapot = erec_PldIratPeldany.Allapot;
        erec_PldIratPeldany.Updated.TovabbitasAlattAllapot = true;

        erec_PldIratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben;
        erec_PldIratPeldany.Updated.Allapot = true;

        ExecParam execParam_PldUpdate = execParam.Clone();
        execParam_PldUpdate.Record_Id = erec_PldIratPeldany.Id;

        Result result_PldUpdate = service_Pld.Update(execParam_PldUpdate, erec_PldIratPeldany);
        result_PldUpdate.CheckError();

        #endregion
    }

    /// <summary>
    /// Iratpéldány eltávolítása a kimenõ küldeményrõl, állapotának visszaállítása
    /// </summary>    
    private void RemoveIratPeldanyFromKimenoKuldemeny(ExecParam execParam, string iratPeldany_Id, string erec_Kuldemeny_IratPeldanyai_Id)
    {
        Logger.Info("Iratpéldány eltávolítása a kimenõ küldeményrõl, állapotának visszaállítása", execParam);

        // IratPéldány állapotának visszaállítása, illetve erec_Kuldemeny_IratPeldanyai_Id rekord törlése

        //iratpéldány lekérése:
        var service_pld = new EREC_PldIratPeldanyokService(this.dataContext);
        ExecParam execParam_PldGet = execParam.Clone();
        execParam_PldGet.Record_Id = iratPeldany_Id;

        Result result_PldGet = service_pld.Get(execParam_PldGet);
        result_PldGet.CheckError();

        var iratPeldany = (EREC_PldIratPeldanyok)result_PldGet.Record;

        // IratPéldány UPDATE:
        iratPeldany.Updated.SetValueAll(false);
        iratPeldany.Base.Updated.SetValueAll(false);
        iratPeldany.Base.Updated.Ver = true;

        // Állapot visszaállítása az elõzõre, ha lehetséges:
        if (!String.IsNullOrEmpty(iratPeldany.TovabbitasAlattAllapot))
        {
            iratPeldany.Allapot = iratPeldany.TovabbitasAlattAllapot;
            iratPeldany.Updated.Allapot = true;

            iratPeldany.TovabbitasAlattAllapot = Constants.BusinessDocument.nullString;
            iratPeldany.Updated.TovabbitasAlattAllapot = true;
        }
        else
        {
            // defaultból kiadmányozottra állítjuk vissza:
            iratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott;
            iratPeldany.Updated.Allapot = true;
        }

        ExecParam execParam_PldUpdate = execParam.Clone();
        execParam_PldUpdate.Record_Id = iratPeldany.Id;

        Result result_pldUpdate = service_pld.Update(execParam_PldUpdate, iratPeldany);
        result_pldUpdate.CheckError();

        Logger.Debug("Iratpéldány visszaállított állapota: " + iratPeldany.Allapot, execParam);

        // erec_Kuldemeny_IratPeldanyai_Id sor törlése a táblából:
        var service_kuldPld = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);

        ExecParam execParam_kuldPldInvalidate = execParam.Clone();
        execParam_kuldPldInvalidate.Record_Id = erec_Kuldemeny_IratPeldanyai_Id;

        Result result_kuldPldInv = service_kuldPld.Invalidate(execParam_kuldPldInvalidate);
        result_kuldPldInv.CheckError();
    }

    void AddDokumentumokToKimenoKuldemeny(ExecParam execParam, string kimenoKuldemenyId, string[] dokumentum_Id_Array, string[] peldanyok_Id_Array)
    {
        //összes csatolmány kiküldése
        if (dokumentum_Id_Array == null)
        {
            if (peldanyok_Id_Array != null && peldanyok_Id_Array.Length > 0)
            {
                dokumentum_Id_Array = GetIratpeldanyokDokumentumai(execParam, peldanyok_Id_Array);
            }
        }

        if (dokumentum_Id_Array != null)
        {
            //küldemény dokumentumainak lekérése
            var csatService = new EREC_CsatolmanyokService(dataContext);
            var csatSearch = new EREC_CsatolmanyokSearch();
            csatSearch.KuldKuldemeny_Id.Filter(kimenoKuldemenyId);

            Result resGetAll = csatService.GetAll(execParam, csatSearch);
            resGetAll.CheckError();

            List<string> addedDokumentumok = new List<string>();

            foreach (DataRow row in resGetAll.Ds.Tables[0].Rows)
            {
                string dokumentumId = row["Dokumentum_Id"].ToString();
                addedDokumentumok.Add(dokumentumId);
            }

            if (HKPFileSettings != null && HKPFileSettings.PackFiles)
            {
                PackKuldemenyDokumentumai(execParam, kimenoKuldemenyId, dokumentum_Id_Array);
                dokumentum_Id_Array = new string[] { };
            }
            else
            {
                //új dokumentumok hozzáadása
                foreach (string dokumentumId in dokumentum_Id_Array)
                {
                    if (!addedDokumentumok.Contains(dokumentumId))
                    {
                        EREC_Csatolmanyok csat = new EREC_Csatolmanyok();
                        csat.KuldKuldemeny_Id = kimenoKuldemenyId;
                        csat.Dokumentum_Id = dokumentumId;

                        Result resInsert = csatService.Insert(execParam, csat);
                        resInsert.CheckError();
                    }
                }

                //régi dokumentumok törlése
                foreach (DataRow row in resGetAll.Ds.Tables[0].Rows)
                {
                    string csatolmanyId = row["Id"].ToString();
                    string dokumentumId = row["Dokumentum_Id"].ToString();

                    if (!dokumentum_Id_Array.Contains(dokumentumId))
                    {
                        ExecParam invalidateExecParam = execParam.Clone();
                        invalidateExecParam.Record_Id = csatolmanyId;

                        Result resInvalidate = csatService.Invalidate(invalidateExecParam);
                        resInvalidate.CheckError();
                    }
                }
            }
        }
    }

    string[] GetIratpeldanyokDokumentumai(ExecParam execParam, string[] peldanyok_Id_Array)
    {
        EREC_CsatolmanyokService csatService = new EREC_CsatolmanyokService(this.dataContext);
        EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();

        csatSearch.IraIrat_Id.Value = String.Format("select IraIrat_Id from EREC_PldIratPeldanyok where Id in ({0})", Search.GetSqlInnerString(peldanyok_Id_Array));
        csatSearch.IraIrat_Id.Operator = Query.Operators.inner;

        Result resGetAll = csatService.GetAll(execParam, csatSearch);
        resGetAll.CheckError();

        List<string> dokumentumIdList = new List<string>();

        foreach (DataRow row in resGetAll.Ds.Tables[0].Rows)
        {
            string dokumentumId = row["Dokumentum_Id"].ToString();
            dokumentumIdList.Add(dokumentumId);
        }

        return dokumentumIdList.ToArray();
    }

    private HivataliKapuFileSettings HKPFileSettings { get; set; }

    private Dictionary<string, EREC_Csatolmanyok> PackedFiles = new Dictionary<string, EREC_Csatolmanyok>();
    private EREC_eBeadvanyok HKPBeadvany { get; set; }
    public void PackKuldemenyDokumentumai(ExecParam execParam, string kuldemenyId, string[] dokumentum_Id_Array)
    {
        Logger.Info("PackKuldemenyDokumentumai kezdete");
        //létre lett-e már hozva a csomagolt file
        string key = String.Join("", dokumentum_Id_Array);
        if (PackedFiles.ContainsKey(key))
        {
            EREC_Csatolmanyok erec_Csatolmanyok = PackedFiles[key];
            erec_Csatolmanyok.KuldKuldemeny_Id = kuldemenyId;
            Logger.Debug("A zip file már létre lett hozva");

            EREC_CsatolmanyokService csatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);

            Logger.Debug("EREC_CsatolmanyokService.Insert kezdete");
            Result resInsert = csatolmanyokService.Insert(execParam, erec_Csatolmanyok);
            Logger.Debug("EREC_CsatolmanyokService.Insert vege");

            resInsert.CheckError();
        }
        else
        {
            //csomagolt fájl létrehozása
            Logger.Debug("GetDokumentumok kezdete");
            List<Csatolmany> csatolmanyok = GetDokumentumok(execParam, dokumentum_Id_Array);
            Logger.Debug("GetDokumentumok vege");
            Csatolmany packedFile = new Csatolmany();
            Logger.Debug("CreatePackedFile kezdete");
            packedFile.Tartalom = CreatePackedFile(execParam, kuldemenyId, csatolmanyok);
            Logger.Debug("CreatePackedFile vege");
            Logger.Debug("GetKimenoKuldemenyAzonosito kezdete");
            string hivatal = GetDokTipusHivatal(execParam);
            Logger.Debug("GetKimenoKuldemenyAzonosito vege");
            packedFile.Nev = GetPackedFileName(hivatal);

            //feltöltés
            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
            erec_Csatolmanyok.KuldKuldemeny_Id = kuldemenyId;
            erec_Csatolmanyok.Leiras = String.Join(", ", csatolmanyok.Select(cs => cs.Nev).ToArray());
            erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
            EREC_KuldKuldemenyekService service = new EREC_KuldKuldemenyekService(this.dataContext);
            ExecParam xpmUpload = execParam.Clone();
            xpmUpload.Record_Id = kuldemenyId;
            Logger.Debug("CsatolmanyUpload kezdete");
            Result resUpload = service.CsatolmanyUpload(xpmUpload, erec_Csatolmanyok, packedFile, true);
            Logger.Debug("CsatolmanyUpload vege");

            resUpload.CheckError();

            PackedFiles.Add(key, erec_Csatolmanyok);
        }

        Logger.Info("PackKuldemenyDokumentumai vege");
    }

    List<Csatolmany> GetDokumentumok(ExecParam execParam, string[] dokumentum_Id_Array)
    {
        var csatolmanyok = new List<Csatolmany>();

        var dokService = eAdminService.ServiceFactory.GetKRT_DokumentumokService();
        var dokSearch = new KRT_DokumentumokSearch();
        dokSearch.Id.In(dokumentum_Id_Array);

        Result csatResult = dokService.GetAll(execParam, dokSearch);
        csatResult.CheckError();

        foreach (DataRow row in csatResult.Ds.Tables[0].Rows)
        {
            string dokumentumId = row["Id"].ToString();
            string externalLink = row["External_Link"].ToString();
            string fajlNev = row["FajlNev"].ToString();

            Csatolmany csat = new Csatolmany();
            csat.Nev = fajlNev;
            csat.Tartalom = GetDokTartalom(externalLink);

            csatolmanyok.Add(csat);
        }

        return csatolmanyok;
    }

    private string GetKimenoKuldemenyAzonosito(ExecParam execParam, string kuldemenyId)
    {
        List<string> peldanyIdList = new List<string>();

        EREC_PldIratPeldanyokSearch pldSearch = new EREC_PldIratPeldanyokSearch();
        pldSearch.Id.Value = String.Format("select kp.Peldany_Id from EREC_Kuldemeny_IratPeldanyai kp where kp.KuldKuldemeny_Id='{0}' and getdate() between kp.ErvKezd and kp.ErvVege", kuldemenyId);
        pldSearch.Id.Operator = Query.Operators.inner;

        Result pldResult = GetAll(execParam, pldSearch);
        pldResult.CheckError();

        var peldanyIktatoszamList = new List<string>();

        foreach (DataRow row in pldResult.Ds.Tables[0].Rows)
        {
            string id = row["Id"].ToString();
            string iktatoszam = row["Azonosito"].ToString();

            peldanyIktatoszamList.Add(iktatoszam);
        }

        return String.Join(",", peldanyIktatoszamList.ToArray());
    }

    private string GetKimenoKuldemenyTargy(ExecParam execParam, string kuldemenyId)
    {
        EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);

        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
        iratSearch.Id.Value = String.Format("select p.IraIrat_Id from EREC_Kuldemeny_IratPeldanyai kp join EREC_PldIratPeldanyok p on kp.Peldany_Id = p.Id where kp.KuldKuldemeny_Id='{0}' and getdate() between kp.ErvKezd and kp.ErvVege", kuldemenyId);
        iratSearch.Id.Operator = Query.Operators.inner;

        Result iratResult = iratokService.GetAll(execParam, iratSearch);
        iratResult.CheckError();

        List<string> targyList = new List<string>();

        foreach (DataRow row in iratResult.Ds.Tables[0].Rows)
        {
            string targy = row["Targy"].ToString();

            targyList.Add(targy);
        }

        return String.Join(",", targyList.ToArray());
    }

    byte[] CreateZipFile(List<Csatolmany> csatolmanyok)
    {
        MemoryStream outputStream = new MemoryStream();
        using (ZipOutputStream zipStream = new ZipOutputStream(outputStream))
        {
            foreach (Csatolmany csatolmany in csatolmanyok)
            {
                ZipEntry entry = new ZipEntry(ZipEntry.CleanName(csatolmany.Nev));
                zipStream.PutNextEntry(entry);

                zipStream.Write(csatolmany.Tartalom, 0, csatolmany.Tartalom.Length);
                zipStream.CloseEntry();
            }
        }

        return outputStream.ToArray();
    }

    private string GetValidFileName(string fileName)
    {
        foreach (char c in System.IO.Path.GetInvalidFileNameChars())
        {
            fileName = fileName.Replace(c, '_');
        }

        return fileName;
    }

    byte[] CreatePackedFile(ExecParam execParam, string kuldemenyId, List<Csatolmany> csatolmanyok)
    {
        if (HKPFileSettings != null && HKPFileSettings.FileFormat == HivataliKapuFileSettings.HivataliKapuFileFormat.Krx)
        {
            return CreateKrxFile(execParam, kuldemenyId, csatolmanyok);
        }
        else
        {
            return CreateZipFile(csatolmanyok);
        }
    }

    string GetPackedFileName(string hivatal)
    {
        string packedFileNev;
        string ext = "zip";

        if (HKPFileSettings != null && HKPFileSettings.FileFormat == HivataliKapuFileSettings.HivataliKapuFileFormat.Krx)
        {
            ext = "krx";
        }

        if (HKPFileSettings != null && !String.IsNullOrEmpty(HKPFileSettings.PackedFileName))
        {
            packedFileNev = String.Format("{0}.{1}", HKPFileSettings.PackedFileName, ext);
        }
        else
        {
            packedFileNev = String.Format("{0}_{1:yyyyMMhhmmss}.{2}", hivatal, DateTime.Now, ext);
        }

        return packedFileNev;

    }

    byte[] CreateKrxFile(ExecParam execParam, string kuldemenyId, List<Csatolmany> csatolmanyok)
    {
        string kuldemenyAzonosito = GetKimenoKuldemenyAzonosito(execParam, kuldemenyId);
        string targy = GetKimenoKuldemenyTargy(execParam, kuldemenyId);
        ExecParam xpmKuldemeny = execParam.Clone();
        xpmKuldemeny.Record_Id = kuldemenyId;

        EREC_KuldKuldemenyekService kuldemenyService = new EREC_KuldKuldemenyekService(this.dataContext);
        Result resKuldemeny = kuldemenyService.Get(xpmKuldemeny);
        resKuldemeny.CheckError();

        string fiok = HKPBeadvany != null ? HKPBeadvany.KR_Fiok : GetDokTipusHivatal(execParam);

        EREC_KuldKuldemenyek erec_kuldemeny = resKuldemeny.Record as EREC_KuldKuldemenyek;

        KULDEMENY kuldemeny = KRXUtility.CreateKuldemeny();
        kuldemeny.FEJRESZ.KULDEMENY_AZONOSITO = kuldemenyAzonosito;
        kuldemeny.FEJRESZ.KULDEMENY_LETREHOZASANAK_IDEJE = DateTime.Now;

        kuldemeny.EXPEDIALASOK.EXPEDIALAS.EXPEDIALASI_AZONOSITO = kuldemenyAzonosito;
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.CIMZETT_NEV = erec_kuldemeny.NevSTR_Bekuldo;
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.CIMZETT_CIM = erec_kuldemeny.CimSTR_Bekuldo;
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_NEV = fiok;
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.UGYKEZELES = new KULDEMENYEXPEDIALASOKEXPEDIALASUGYKEZELES();
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.UGYKEZELES.IKTATOSZAM = kuldemenyAzonosito;
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.TARGY = targy;
        kuldemeny.EXPEDIALASOK.EXPEDIALAS.EXPEDIALAS_DATUM = DateTime.Now;

        //Posta hibrid
        List<Csatolmany> metaFiles = new List<Csatolmany>();
        if (this.PH_DeliveryInstructions != null)
        {
            this.PH_DeliveryInstructions.consignment.RequestId = kuldemenyAzonosito;
            this.PH_DeliveryInstructions.consignment.ConsignmentGuid = kuldemenyId;
            PostaHibridHelper.ComputeCsatolmanyokHash(this.PH_DeliveryInstructions, csatolmanyok);
            PostaHibridHelper.SetKiadmanyozo(execParam, this.dataContext, kuldemenyId, this.PH_DeliveryInstructions);

            Csatolmany metaFile = PostaHibridHelper.GetDeliveryInstructionsFile(this.PH_DeliveryInstructions);
            metaFiles.Add(metaFile);
        }

        KRXFileManager krx = new KRXFileManager();
        byte[] krxContent = krx.CreateKRXFile(kuldemeny, csatolmanyok, metaFiles);
        return krxContent;
    }
    #endregion

    /// <summary>
    /// Iratpéldányok ügyiratba helyezése (õrzõ átállítása az ügyiratéra)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id_Array">Ügyiratok Id-jai</param>
    /// <returns></returns>
    [WebMethod()]
    public Result UgyiratbaHelyezes_Tomeges(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Info("Iratpéldány(ok) ügyiratba helyezése", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length <= 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52740);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Iratpéldányok lekérése:

            string iratpeldanyIds = Search.GetSqlInnerString(erec_PldIratPeldanyok_Id_Array);

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();

            search.Id.Value = iratpeldanyIds;
            search.Id.Operator = Query.Operators.inner;

            Result result_getAllWithExt = this.GetAllWithExtension(execParam.Clone(), search);
            result_getAllWithExt.CheckError();

            // Ellenõrzés, annyi rekord jött-e, mint amennyi id-t megadtunk:
            if (result_getAllWithExt.Ds == null || result_getAllWithExt.Ds.Tables[0].Rows.Count != erec_PldIratPeldanyok_Id_Array.Length)
            {
                // Hiba az iratpéldányok lekérdezése során!
                throw new ResultException(52741);
            }

            // Ellenõrzés, ügyiratba helyezhetõk-e az iratpéldányok:
            // + iratpéldányok verzióinak kigyûjtése stringbe, amit majd a tárolt eljárásnak átadunk (id-k és verziók sorrendjére figyelni kell)
            iratpeldanyIds = string.Empty;
            string iratpeldanyVers = string.Empty;

            Hashtable iratPeldanyStatuszok;
            Hashtable iratStatuszok;
            Hashtable ugyiratStatuszok;

            // statuszok feltöltése:
            Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotHierarchiaByDataSet(execParam.Clone(), result_getAllWithExt.Ds
                , out iratPeldanyStatuszok, out iratStatuszok, out ugyiratStatuszok);

            if (iratPeldanyStatuszok == null || iratPeldanyStatuszok.Count == 0
                || iratStatuszok == null || iratStatuszok.Count == 0
                || ugyiratStatuszok == null || ugyiratStatuszok.Count == 0)
            {
                throw new ResultException(52742);
            }

            foreach (DataRow row in result_getAllWithExt.Ds.Tables[0].Rows)
            {
                string pld_Id = row["Id"].ToString();

                ErrorDetails errorDetail;
                if (!Contentum.eRecord.BaseUtility.IratPeldanyok.UgyiratbaHelyezheto(
                        iratPeldanyStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz
                        , iratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Iratok.Statusz
                        , ugyiratStatuszok[pld_Id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz
                        , execParam.Clone(), out errorDetail))
                {
                    throw new ResultException(52743);
                }

                if (string.IsNullOrEmpty(iratpeldanyIds))
                {
                    iratpeldanyIds += "'" + row["Id"].ToString() + "'";
                }
                else
                {
                    iratpeldanyIds += ",'" + row["Id"].ToString() + "'";
                }

                if (string.IsNullOrEmpty(iratpeldanyVers))
                {
                    iratpeldanyVers += row["Ver"].ToString();
                }
                else
                {
                    iratpeldanyVers += "," + row["Ver"].ToString();
                }
            }

            #endregion

            result = sp.UgyiratbaHelyezes_Tomeges(execParam.Clone(), iratpeldanyIds, iratpeldanyVers);
            result.CheckError();

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region CR3152 - Expediálás folyamat tömegesítése
    /// <summary>
    /// Iratpéldányok tömeges expediálása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id_Array">Az expediálandó iratpéldányok Id-jai</param>
    /// <param name="expedialasModja">Az expediálás módja.</param>
    /// <returns></returns>
    [WebMethod()]
    public Result TomegesExpedialasWithExpedialasMod(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array, string expedialasModja)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Result res = new Result();
        //tömeges update
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            if (erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length == 0)
            {
                throw new ResultException(52280);
            }
            else
            {
                // az összes iratpéldány lekérése
                var search = new EREC_PldIratPeldanyokSearch();
                search.Id.In(erec_PldIratPeldanyok_Id_Array);

                Result getAllResult = this.GetAllWithExtension(execParam.Clone(), search);
                getAllResult.CheckError();

                EREC_PldIratPeldanyok erec_PldIratPeldany = new EREC_PldIratPeldanyok();
                erec_PldIratPeldany.Updated.SetValueAll(false);
                erec_PldIratPeldany.Base.Updated.SetValueAll(false);
                var iratpeldanyIds = string.Empty;
                string iratpeldanyVers = string.Empty;
                foreach (DataRow r in getAllResult.Ds.Tables[0].Rows)
                {
                    iratpeldanyIds += "," + r["Id"].ToString();
                    iratpeldanyVers += "," + r["Ver"].ToString();
                }
                char[] separator = { ',' };
                List<string> ids = new List<string>();
                ids.AddRange(iratpeldanyIds.TrimStart(',').Split(separator));
                List<string> vers = new List<string>();
                vers.AddRange(iratpeldanyVers.TrimStart(',').Split(separator));
                erec_PldIratPeldany.KuldesMod = expedialasModja;
                erec_PldIratPeldany.Updated.KuldesMod = true;

                var result = UpdateTomeges(execParam.Clone(), ids, vers, erec_PldIratPeldany, DateTime.Now);
                result.CheckError();

                foreach (DataRow r in getAllResult.Ds.Tables[0].Rows)
                {
                    string[] iratpeldanyArray = { r["Id"].ToString() };
                    var iratPeldany = new EREC_PldIratPeldanyok()
                    {
                        Id = r["Id"].ToString(),
                        KuldesMod = r["KuldesMod"].ToString(),
                        NevSTR_Cimzett = r["NevSTR_Cimzett"].ToString(),
                        Partner_Id_Cimzett = r["Partner_Id_Cimzett"].ToString(),
                        CimSTR_Cimzett = r["CimSTR_Cimzett"].ToString(),
                        Cim_id_Cimzett = r["Cim_id_Cimzett"].ToString(),
                    };
                    iratPeldany.Updated.SetValueAll(false);
                    iratPeldany.Base.Updated.SetValueAll(false);
                    //TODO
                    res = Expedialas(execParam.Clone(), iratpeldanyArray, "", iratPeldany, null);
                    res.CheckError();
                }

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, iratpeldanyIds, "IratPeldanyExpedialas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_PldIratPeldanyok::ExpedialasTomeges: ", execParam, eventLogResult);
                #endregion
                //foreach (var iratPeldanyId in erec_PldIratPeldanyok_Id_Array)
                //{
                //    ExecParam execGetIratPeldany = execParam.Clone();
                //    execGetIratPeldany.Record_Id = iratPeldanyId;
                //    var ResIratPeldany = Get(execGetIratPeldany);
                //    EREC_PldIratPeldanyok IratPeldany = null;
                //    if (!ResIratPeldany.IsError)
                //    {
                //        IratPeldany = (EREC_PldIratPeldanyok)ResIratPeldany.Record;
                //        IratPeldany.KuldesMod = expedialasModja;
                //        IratPeldany.Updated.KuldesMod = true;
                //    }
                //    else
                //    {
                //        return ResIratPeldany;
                //    }
                //    Result ResUpdateIratPeldany = null;
                //    if(IratPeldany != null)
                //        ResUpdateIratPeldany = Update(execGetIratPeldany, IratPeldany);
                //    if (ResUpdateIratPeldany.IsError)
                //    {
                //        return ResUpdateIratPeldany;
                //    }
                //    string[] iratpeldanyArray = { iratPeldanyId };
                //    res = Expedialas(execParam.Clone(), iratpeldanyArray, "", IratPeldany); 
                //}
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            res = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, res);

        return res;
    }

    [WebMethod()]
    public Result TomegesExpedialas(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array, String[] dokumentum_Id_Array, HivataliKapuFileSettings fileSettings, EREC_eBeadvanyok _EREC_eBeadvanyok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Result res = new Result();
        //tömeges update
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            if (erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length == 0)
            {
                throw new ResultException(52280);
            }
            else
            {
                // az összes iratpéldány lekérése
                string iratpeldanyIds = Search.GetSqlInnerString(erec_PldIratPeldanyok_Id_Array);

                var search = new EREC_PldIratPeldanyokSearch();
                search.Id.In(erec_PldIratPeldanyok_Id_Array);

                Result getAllResult = this.GetAllWithExtension(execParam.Clone(), search);
                getAllResult.CheckError();

                DataTable hibaTable = new DataTable();
                hibaTable.Columns.Add("Id", typeof(String));
                hibaTable.Columns.Add("Azonosito", typeof(String));
                hibaTable.Columns.Add("ErrorCode", typeof(String));
                hibaTable.Columns.Add("ErrorMessage", typeof(String));

                DataSet hibaDs = new DataSet();
                hibaDs.Tables.Add(hibaTable);

                foreach (DataRow r in getAllResult.Ds.Tables[0].Rows)
                {
                    EREC_PldIratPeldanyokService newContext = new EREC_PldIratPeldanyokService();
                    Result expResult = new Result();
                    String[] _dokumentum_Id_Array = dokumentum_Id_Array;

                    try
                    {
                        string[] iratpeldanyArray = { r["Id"].ToString() };
                        var iratPeldany = new EREC_PldIratPeldanyok()
                        {
                            Id = r["Id"].ToString(),
                            KuldesMod = r["KuldesMod"].ToString(),
                            NevSTR_Cimzett = r["NevSTR_Cimzett"].ToString(),
                            Partner_Id_Cimzett = r["Partner_Id_Cimzett"].ToString(),
							 // BUG_13213
                            Partner_Id_CimzettKapcsolt = r["Partner_Id_CimzettKapcsolt"].ToString(),
                            CimSTR_Cimzett = r["CimSTR_Cimzett"].ToString(),
                            Cim_id_Cimzett = r["Cim_id_Cimzett"].ToString(),
                        };
                        iratPeldany.Updated.SetValueAll(false);
                        iratPeldany.Base.Updated.SetValueAll(false);

                        if (iratPeldany.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
                        {

                            EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
                            eBeadvany.Updated.SetValueAll(false);
                            eBeadvany.Base.Updated.SetValueAll(false);

                            if (iratPeldany.CimSTR_Cimzett.Length == 32)
                            {
                                eBeadvany.PartnerKapcsolatiKod = iratPeldany.CimSTR_Cimzett;
                                eBeadvany.Updated.PartnerKapcsolatiKod = true;
                                eBeadvany.PartnerNev = iratPeldany.NevSTR_Cimzett;
                                eBeadvany.Updated.PartnerNev = true;
                            }
                            else
                            {
                                eBeadvany.PartnerKRID = iratPeldany.CimSTR_Cimzett;
                                eBeadvany.Updated.PartnerKRID = true;
                                eBeadvany.PartnerNev = iratPeldany.NevSTR_Cimzett;
                                eBeadvany.Updated.PartnerNev = true;
                            }

                            eBeadvany.KR_ETertiveveny = "1";
                            eBeadvany.Updated.KR_ETertiveveny = true;

                            eBeadvany.KR_Fiok = _EREC_eBeadvanyok.KR_Fiok;
                            eBeadvany.Updated.KR_Fiok = true;

                            if (_dokumentum_Id_Array == null)
                            {

                                string dokumentumId = GetKikuldendoCsatolmany(execParam, r["Id"].ToString());
                                if (!String.IsNullOrEmpty(dokumentumId))
                                {
                                    _dokumentum_Id_Array = new string[] { dokumentumId };
                                }
                            }

                            expResult = newContext.Expedialas_HivataliKapus(execParam.Clone(), iratpeldanyArray, "", iratPeldany, eBeadvany, _dokumentum_Id_Array, fileSettings);
                        }
                        else
                        {

                            expResult = newContext.Expedialas(execParam.Clone(), iratpeldanyArray, "", iratPeldany, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        expResult = ResultException.GetResultFromException(ex);
                    }

                    if (expResult.IsError)
                    {
                        Logger.Error(String.Format("Expedialas ({0}) hiba: ", r["Id"].ToString()), execParam, expResult);

                        DataRow hibaRow = hibaTable.NewRow();
                        hibaRow["Id"] = r["Id"].ToString();
                        hibaRow["Azonosito"] = r["IktatoSzam_Merge"].ToString();
                        hibaRow["ErrorCode"] = expResult.ErrorCode;
                        hibaRow["ErrorMessage"] = expResult.ErrorMessage;
                        hibaTable.Rows.Add(hibaRow);
                    }
                }

                if (hibaTable.Rows.Count > 0)
                {
                    res = ResultError.CreateNewResultWithErrorCode(52520);
                    res.Ds = hibaDs;
                }

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, iratpeldanyIds, "IratPeldanyExpedialas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_PldIratPeldanyok::ExpedialasTomeges: ", execParam, eventLogResult);
                #endregion
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            res = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, res);

        return res;
    }

    private string GetKikuldendoCsatolmany(ExecParam execParam, string peldanyId)
    {
        string dokumentumId = String.Empty;

        EREC_CsatolmanyokService csatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
        EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();
        csatolmanyokSearch.IraIrat_Id.Value = String.Format("select IraIrat_Id from EREC_PldIratPeldanyok where Id = '{0}'", peldanyId);
        csatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.inner;

        Result csatolmanyokResult = csatolmanyokService.GetAllWithExtension(execParam, csatolmanyokSearch);

        csatolmanyokResult.CheckError();

        int csatolmanyokSzama = csatolmanyokResult.GetCount;

        int hitelesCsatolmanyokSzama = 0;

        if (csatolmanyokSzama == 1)
        {
            dokumentumId = csatolmanyokResult.Ds.Tables[0].Rows[0]["Dokumentum_Id"].ToString();
        }
        else if (csatolmanyokSzama > 1)
        {
            foreach (DataRow row in csatolmanyokResult.Ds.Tables[0].Rows)
            {
                string elektronikusAlairas = row["ElektronikusAlairas"].ToString();

                if (!String.IsNullOrEmpty(elektronikusAlairas) && elektronikusAlairas != "0")
                {
                    dokumentumId = row["Dokumentum_Id"].ToString();
                    hitelesCsatolmanyokSzama++;
                }
            }

            if (hitelesCsatolmanyokSzama > 1)
                dokumentumId = String.Empty;
        }

        if (String.IsNullOrEmpty(dokumentumId))
        {
            throw new Exception("A kiküldendő csatolmány nem határozható meg!");
        }

        return dokumentumId;
    }

    #endregion

    /// <summary>
    /// Iratpéldányok expediálása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_PldIratPeldanyok_Id_Array">Az expediálandó iratpéldányok Id-jai</param>
    /// <param name="kimenoKuldemenyVonalkod">A kimenõ küldemény, ill. a tértivevény azonosítását szolgáló vonalkód</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Expedialas(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array, string kimenoKuldemenyVonalkod, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, String[] dokumentum_Id_Array)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Info("Iratpéldány(ok) expediálása", execParam);
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || erec_PldIratPeldanyok_Id_Array == null || erec_PldIratPeldanyok_Id_Array.Length <= 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52520);
            log.WsEnd(execParam, result1);
            return result1;
        }
        EREC_PldIratPeldanyok elsoIratPeldanyCimListaban = null;
        EREC_IraElosztoivek existingElosztoIv = null;
        bool cimzettListasExpedialas = IsCimzettListasExpedialas(execParam.Clone(), erec_PldIratPeldanyok_Id_Array[0], out elsoIratPeldanyCimListaban, out existingElosztoIv);
        if (!cimzettListasExpedialas)
            //Cím szegmentáltság ellenörzés
            if (Rendszerparameterek.GetBoolean(execParam, Contentum.eUtility.Rendszerparameterek.CIM_SZEGMENTALTSAG_ELLENORZES, false))
            {
                if (_EREC_PldIratPeldanyok.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.E_mail
                    && _EREC_PldIratPeldanyok.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu
                    && _EREC_PldIratPeldanyok.KuldesMod != KodTarak.KULDEMENY_KULDES_MODJA.HAIR)
                {
                    if (String.IsNullOrEmpty(_EREC_PldIratPeldanyok.Cim_id_Cimzett))
                    {
                        //A postázási cím nem struktúrált!
                        Result resultError = ResultError.CreateNewResultWithErrorCode(52799);
                        log.WsEnd(execParam, resultError);
                        return resultError;
                    }
                }
            }

        // CR3306 Kimenõ email esetén automatikus postazas javítás
        // Címadatok összeállítása a Küldemény számára (ha felületen változtattak, akkor az, egyébként az iratpéldányból jövõ)
        #region Címadatok küldeményhez
        _EREC_PldIratPeldanyok = CimAdatokKuldemenyhez(_EREC_PldIratPeldanyok);
        #endregion

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            List<string> cimzettListaPldIratIds = new List<string>();
            #region CIMZETTLISTA ALAPJAN PLD IRATOK LÉTREHOZÁSA
            if (cimzettListasExpedialas)
                try
                {
                    EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
                    cimzettListaPldIratIds = service.IratPeldanyokLetrehozasasCimzettListaAlapjan(execParam.Clone(), elsoIratPeldanyCimListaban, _EREC_PldIratPeldanyok);
                    if (cimzettListaPldIratIds != null && cimzettListaPldIratIds.Count > 0)
                    {
                        List<string> merged = new List<string>();
                        merged.AddRange(erec_PldIratPeldanyok_Id_Array.ToList());
                        merged.AddRange(cimzettListaPldIratIds);

                        erec_PldIratPeldanyok_Id_Array = merged.ToArray();
                    }
                }
                catch (Exception exc)
                {
                    Logger.Error("EREC_PldIratPeldanyokService.Expedialas.IratPeldanyokLetrehozasasCimzettListaAlapjan ", exc);
                }
            #endregion CIMZETTLISTA ALAPJAN PLD IRATOK LÉTREHOZÁSA

            // CR3229: Az EXPEDIALAS_UJ_IRATPELDANNYAL paraméter értékétõl függõen új iratpéldány jön létre
            // CR 3048: Expediálhatóság ellenõrzés, hogy szükség esetén új iratpéldány létrehozhassunk 
            for (int i = 0; i < erec_PldIratPeldanyok_Id_Array.Length; i++)
            {
                string pld_Id = erec_PldIratPeldanyok_Id_Array[i];

                // IratPéldány ellenõrzése:

                EREC_PldIratPeldanyok IratPeldany = null;
                EREC_IraIratok obj_irat = null;
                EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
                EREC_UgyUgyiratok obj_ugyirat = null;

                this.GetObjectsHierarchyByIratpeldany(execParam, pld_Id
                    , out IratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat);

                Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz statusz_pld =
                    Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(IratPeldany);
                Contentum.eRecord.BaseUtility.Iratok.Statusz statusz_irat =
                    Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(obj_irat, IratPeldany);
                Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz statusz_ugyiratDarab =
                    Contentum.eRecord.BaseUtility.UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz statusz_ugyirat =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);

                // Expediálhatóság ellenõrzése:
                bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1");
                ErrorDetails errorDetail = null;
                bool isExpedialhato = false;
                if (Contentum.eRecord.BaseUtility.IratPeldanyok.Expedialhato(
                        statusz_pld, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail))
                {
                    isExpedialhato = true;
                }
                else
                {
                    // CR 3048: Nem az iratpéldány örzõje-> új iratpéldányt hozunk létre
                    if (errorDetail.Code == 80011)  //ErrorDetailMessages.IratpeldanynakNemOnAzOrzoje)
                    {
                        isExpedialhato = true;
                    }
                }
                // CR3229: Az EXPEDIALAS_UJ_IRATPELDANNYAL paraméter értékétõl függõen új iratpéldány jön létre
                ExecParam execParam_Param = execParam.Clone();
                // BLG_237 Kimenõ emailnál nem jön létre második jav FPH
                // if (isExpedialhato && Rendszerparameterek.GetBoolean(execParam_Param, Rendszerparameterek.EXPEDIALAS_UJ_IRATPELDANNYAL))
                if (isExpedialhato && Rendszerparameterek.GetBoolean(execParam_Param, Rendszerparameterek.EXPEDIALAS_UJ_IRATPELDANNYAL)
                    && !(obj_irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso && obj_irat.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet))

                {
                    EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);
                    ExecParam execParam_newPld = execParam.Clone();
                    IratPeldany.Id = "";
                    IratPeldany.FelhasznaloCsoport_Id_Orzo = Csoportok.GetFelhasznaloSajatCsoportId(execParam_newPld);
                    // Barcode generálás az új iratpéldányhoz
                    KRT_BarkodokService service_barkodok = new KRT_BarkodokService(this.dataContext);
                    ExecParam execParam_barkod = execParam.Clone();

                    Result result_barkod_pld = service_barkodok.BarkodGeneralas(execParam_barkod);
                    if (!String.IsNullOrEmpty(result_barkod_pld.ErrorCode))
                    {
                        // TODO: egyelõre nem dobunk hibát, késõbb majd kellene (ha már minden stabilan megy)
                        Logger.Error("Vonalkód generálás az iratpéldánynak: Sikertelen", execParam);
                    }
                    else
                    {
                        IratPeldany.BarCode = (String)result_barkod_pld.Record;
                    }

                    Result result_insert = AddNewPldIratPeldanyToIraIrat(execParam_newPld, IratPeldany, obj_irat.Id);
                    if (!String.IsNullOrEmpty(result_insert.ErrorCode))
                    {
                        Logger.Error("Hiba az új iratpéldány létrehozásánál!", execParam_newPld);

                        throw new ResultException(result_insert);
                    }
                    else
                    {
                        pld_Id = result_insert.Uid;
                        erec_PldIratPeldanyok_Id_Array[i] = pld_Id;
                    }
                }
            }

            // Kimenõ küldemény összeállítása, majd a kimenõ küldemény állapotának beállítása, illetve az iratpéldányok állapotának beállítása:
            string kivalasztott_Pld = erec_PldIratPeldanyok_Id_Array[0];

            List<string> tomegesKimenoKuldemenyIdLista = new List<string>();
            Result result_kimenoKuldSave = null;
            if (cimzettListasExpedialas)
            {
                #region CIMZETTLISTA -> KIMENO KULDEMENY
                EREC_PldIratPeldanyok pldIratKimenoKuldemenyhez = new EREC_PldIratPeldanyok();
                foreach (string iratPeldanyId in erec_PldIratPeldanyok_Id_Array)
                {
                    EREC_PldIratPeldanyok pldIratFind = GetIratPeldany(execParam.Clone(), iratPeldanyId);
                    if (pldIratFind == null)
                        continue;

                    pldIratKimenoKuldemenyhez = CimAdatokKimenoKuldemenyhez(_EREC_PldIratPeldanyok, pldIratFind);
                    result_kimenoKuldSave = this.KimenoKuldemeny_Save(execParam, iratPeldanyId, new string[] { iratPeldanyId }, pldIratKimenoKuldemenyhez, kimenoKuldemenyVonalkod, dokumentum_Id_Array);
                    if (!string.IsNullOrEmpty(result_kimenoKuldSave.ErrorCode))
                        throw new ResultException(result_kimenoKuldSave);

                    if (!tomegesKimenoKuldemenyIdLista.Contains(result_kimenoKuldSave.Uid))
                        tomegesKimenoKuldemenyIdLista.Add(result_kimenoKuldSave.Uid);
                }
                #endregion
            }
            else
            {
                result_kimenoKuldSave = this.KimenoKuldemeny_Save(execParam, kivalasztott_Pld, erec_PldIratPeldanyok_Id_Array, _EREC_PldIratPeldanyok, kimenoKuldemenyVonalkod, dokumentum_Id_Array);
                result_kimenoKuldSave.CheckError();
                tomegesKimenoKuldemenyIdLista.Add(result_kimenoKuldSave.Uid);
            }

            // string kimenoKuld_Id = result_kimenoKuldSave.Uid;

            #region Kimenõ küldemény expediálása
            List<EREC_KuldKuldemenyek> tomegesKuldemenyObjektumLista = new List<EREC_KuldKuldemenyek>();

            EREC_KuldKuldemenyekService service_kuldemeny = new EREC_KuldKuldemenyekService(this.dataContext);
            EREC_KuldKuldemenyek kimenoKuldemenyInstance;
            Result result_KuldemenyUpdate = null;
            if (tomegesKimenoKuldemenyIdLista.Count > 0)
                foreach (string kimenoKudemenyId in tomegesKimenoKuldemenyIdLista)
                {
                    KimenoKuldemenyExpedialas(execParam, kimenoKuldemenyVonalkod, kimenoKudemenyId, out kimenoKuldemenyInstance, out result_KuldemenyUpdate);
                    tomegesKuldemenyObjektumLista.Add(kimenoKuldemenyInstance);
                }


            #endregion

            #region Iratpéldányok expediálása

            for (int i = 0; i < erec_PldIratPeldanyok_Id_Array.Length; i++)
            {
                string pld_Id = erec_PldIratPeldanyok_Id_Array[i];

                // IratPéldány ellenõrzése:

                EREC_PldIratPeldanyok IratPeldany = null;
                EREC_IraIratok obj_irat = null;
                EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
                EREC_UgyUgyiratok obj_ugyirat = null;

                this.GetObjectsHierarchyByIratpeldany(execParam, pld_Id
                    , out IratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat);

                Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz statusz_pld =
                    Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByBusinessDocument(IratPeldany);
                Contentum.eRecord.BaseUtility.Iratok.Statusz statusz_irat =
                    Contentum.eRecord.BaseUtility.Iratok.GetAllapotByBusinessDocument(obj_irat, IratPeldany);
                Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz statusz_ugyiratDarab =
                    Contentum.eRecord.BaseUtility.UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz statusz_ugyirat =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);

                // Expediálhatóság ellenõrzése:
                bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1");
                ErrorDetails errorDetail = null;
                if (Contentum.eRecord.BaseUtility.IratPeldanyok.Expedialhato(
                        statusz_pld, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail) == false)
                {
                    // Nem expediálható:
                    throw new ResultException(52522, errorDetail);
                }

                // Iratpéldány UPDATE:
                IratPeldany.Updated.SetValueAll(false);
                IratPeldany.Base.Updated.SetValueAll(false);
                IratPeldany.Base.Updated.Ver = true;

                // Állapot beállítása expediáltra:
                IratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Expedialt;
                IratPeldany.Updated.Allapot = true;

                ExecParam execParam_PldUpdate = execParam.Clone();
                execParam_PldUpdate.Record_Id = IratPeldany.Id;

                Result result_PldUpdate = this.Update(execParam_PldUpdate, IratPeldany);
                result_PldUpdate.CheckError();
            }

            #endregion

            #region Ha Kimenõ email küldemény akkor postázzuk is.
            // BLG_237  Postázás kiszedve, külön kerül meghívásra
            //EREC_PldIratPeldanyok Pld = null;
            //EREC_IraIratok Irat = null;
            //EREC_UgyUgyiratdarabok UgyiratDarab = null;
            //EREC_UgyUgyiratok Ugyirat = null;

            //// CR3306 Kimenõ email esetén automatikus postazas javítás
            ////if (!string.IsNullOrEmpty(_EREC_PldIratPeldanyok.Id ))
            //if (!string.IsNullOrEmpty(kivalasztott_Pld))
            //{
            //    this.GetObjectsHierarchyByIratpeldany(execParam, kivalasztott_Pld
            //        , out Pld, out Irat, out UgyiratDarab, out Ugyirat);
            //    // CR3306 Ha bejövõ email kipostázható, akkor ez a vizsgálat nem elég itt TODO Nézni kell hogy kimenõ-e
            //    if (Irat.Jelleg == KodTarak.IRAT_JELLEG.E_mail_uzenet)
            //    {
            //        foreach (EREC_KuldKuldemenyek itemKuldemeny in tomegesKuldemenyObjektumLista)
            //        {
            //            // BLG_237
            //            EREC_KuldKuldemenyek kuldemenyTerti = itemKuldemeny;
            //            string x = Rendszerparameterek.Get(execParam, Rendszerparameterek.EMAIL_KIMENO_TERTIVEVENY);

            //            if (Rendszerparameterek.GetBoolean(execParam, Contentum.eUtility.Rendszerparameterek.EMAIL_KIMENO_TERTIVEVENY, false))
            //            {
            //                kuldemenyTerti = KimenoEmailTertivevenyBeallitas(execParam, itemKuldemeny, Irat.Id);
            //                //Result result_kimenokuldpostazas = service_kuldemeny.Postazas(execParam.Clone(), itemKuldemeny, "");
            //            }
            //                Result result_kimenokuldpostazas = service_kuldemeny.Postazas(execParam.Clone(), kuldemenyTerti, "");
            //                if (result_kimenokuldpostazas.IsError)
            //                throw new ResultException(result_kimenokuldpostazas);
            //        }

            //    }
            //}

            #endregion

            // EB 2011.01.06: Majd postázáskor hozzuk létre:
            #region Tértivevény létrehozása, ha szükséges
            //// ha a küldés módja Postai,tértivevényes:
            //if (kimenoKuldemeny.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Postai_tertivevenyes)
            //{
            //    Logger.Info("Tértivevény létrehozás Start");

            //    EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();

            //    tertiveveny.Kuldemeny_Id = kimenoKuldemeny.Id;
            //    tertiveveny.Updated.Kuldemeny_Id = true;

            //    tertiveveny.Allapot = KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett;
            //    tertiveveny.Updated.Allapot = true;

            //    //tertiveveny.BarCode = kimenoKuldemeny.BarCode;
            //    //tertiveveny.Updated.BarCode = true;

            //    tertiveveny.Ragszam = kimenoKuldemeny.RagSzam;
            //    tertiveveny.Updated.Ragszam = true;

            //    EREC_KuldTertivevenyekService service_tertiveveny = new EREC_KuldTertivevenyekService(this.dataContext);
            //    ExecParam execParam_tertInsert = execParam.Clone();

            //    Result result_tertInsert = service_tertiveveny.Insert(execParam_tertInsert, tertiveveny);
            //    if (!String.IsNullOrEmpty(result_tertInsert.ErrorCode))
            //    {
            //        // hiba:
            //        throw new ResultException(result_tertInsert);
            //    }
            //}

            #endregion
            result = result_KuldemenyUpdate;
            // BLG_11881
            if (tomegesKimenoKuldemenyIdLista.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");

                foreach (string kimenoKudemenyId in tomegesKimenoKuldemenyIdLista)
                {
                    DataRow row = dt.NewRow();
                    row["ID"] = kimenoKudemenyId;
                    dt.Rows.Add(row);
                }
                result.Ds = new DataSet();
                result.Ds.Tables.Add(dt);
            }

            #region CIZETTLISTA INVALIDÁLÁS
            if (existingElosztoIv != null)
                InvalidateElosztoIv(execParam.Clone(), existingElosztoIv.Id);
            #endregion CIZETTLISTA INVALIDÁLÁS

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                string obj_ids = Search.GetSqlInnerString(erec_PldIratPeldanyok_Id_Array);

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, obj_ids, "Expedialas");
            }
            #endregion

            //test only
            //dataContext.RollbackIfRequired(isTransactionBeginHere);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private EREC_PldIratPeldanyok GetIratPeldany(ExecParam execParam, string id)
    {
        EREC_PldIratPeldanyokService pldIratService = new EREC_PldIratPeldanyokService(this.dataContext);
        ExecParam xParamPldIratGet = execParam.Clone();
        xParamPldIratGet.Record_Id = id;
        Result pldResult = this.Get(xParamPldIratGet);
        if (string.IsNullOrEmpty(pldResult.ErrorCode))
        {
            return (EREC_PldIratPeldanyok)pldResult.Record;
        }
        return null;
    }
    private static EREC_PldIratPeldanyok CimAdatokKuldemenyhez(EREC_PldIratPeldanyok _EREC_PldIratPeldanyok)
    {
        EREC_PldIratPeldanyok cimAdatokForKuldemeny = new EREC_PldIratPeldanyok();
        cimAdatokForKuldemeny.Updated.SetValueAll(false);
        cimAdatokForKuldemeny.Base.Updated.SetValueAll(false);

        cimAdatokForKuldemeny.KuldesMod = _EREC_PldIratPeldanyok.KuldesMod;
        cimAdatokForKuldemeny.Updated.KuldesMod = true;

        cimAdatokForKuldemeny.Partner_Id_Cimzett = _EREC_PldIratPeldanyok.Partner_Id_Cimzett;
        cimAdatokForKuldemeny.Updated.Partner_Id_Cimzett = true;

		// BUG_13213
        cimAdatokForKuldemeny.Partner_Id_CimzettKapcsolt = _EREC_PldIratPeldanyok.Partner_Id_CimzettKapcsolt;
        cimAdatokForKuldemeny.Updated.Partner_Id_CimzettKapcsolt = true;

        cimAdatokForKuldemeny.NevSTR_Cimzett = _EREC_PldIratPeldanyok.NevSTR_Cimzett;
        cimAdatokForKuldemeny.Updated.NevSTR_Cimzett = true;

        cimAdatokForKuldemeny.Cim_id_Cimzett = _EREC_PldIratPeldanyok.Cim_id_Cimzett;
        cimAdatokForKuldemeny.Updated.Cim_id_Cimzett = true;

        cimAdatokForKuldemeny.CimSTR_Cimzett = _EREC_PldIratPeldanyok.CimSTR_Cimzett;
        cimAdatokForKuldemeny.Updated.CimSTR_Cimzett = true;

        return cimAdatokForKuldemeny;
    }

    private static EREC_PldIratPeldanyok CimAdatokKuldemenyhez(EREC_PldIratPeldanyok FoPldIratPeldany, EREC_PldIratPeldanyok elsoIratPeldany)
    {
        EREC_PldIratPeldanyok cimAdatokForKuldemeny = new EREC_PldIratPeldanyok();
        cimAdatokForKuldemeny.Updated.SetValueAll(false);
        cimAdatokForKuldemeny.Base.Updated.SetValueAll(false);

        cimAdatokForKuldemeny.KuldesMod = FoPldIratPeldany.KuldesMod;
        cimAdatokForKuldemeny.Updated.KuldesMod = true;

        cimAdatokForKuldemeny.Partner_Id_Cimzett = elsoIratPeldany.Partner_Id_Cimzett;
        cimAdatokForKuldemeny.Updated.Partner_Id_Cimzett = true;

        cimAdatokForKuldemeny.NevSTR_Cimzett = elsoIratPeldany.NevSTR_Cimzett;
        cimAdatokForKuldemeny.Updated.NevSTR_Cimzett = true;

        cimAdatokForKuldemeny.Cim_id_Cimzett = elsoIratPeldany.Cim_id_Cimzett;
        cimAdatokForKuldemeny.Updated.Cim_id_Cimzett = true;

        cimAdatokForKuldemeny.CimSTR_Cimzett = elsoIratPeldany.CimSTR_Cimzett;
        cimAdatokForKuldemeny.Updated.CimSTR_Cimzett = true;

        return cimAdatokForKuldemeny;
    }
    private static EREC_PldIratPeldanyok CimAdatokKimenoKuldemenyhez(EREC_PldIratPeldanyok FoPldIratPeldany, EREC_PldIratPeldanyok elsoIratPeldany)
    {
        EREC_PldIratPeldanyok cimAdatokForKuldemeny = new EREC_PldIratPeldanyok();
        cimAdatokForKuldemeny.Updated.SetValueAll(false);
        cimAdatokForKuldemeny.Base.Updated.SetValueAll(false);

        cimAdatokForKuldemeny.KuldesMod = elsoIratPeldany.KuldesMod;
        cimAdatokForKuldemeny.Updated.KuldesMod = true;

        cimAdatokForKuldemeny.Partner_Id_Cimzett = elsoIratPeldany.Partner_Id_Cimzett;
        cimAdatokForKuldemeny.Updated.Partner_Id_Cimzett = true;

        cimAdatokForKuldemeny.NevSTR_Cimzett = elsoIratPeldany.NevSTR_Cimzett;
        cimAdatokForKuldemeny.Updated.NevSTR_Cimzett = true;

        cimAdatokForKuldemeny.Cim_id_Cimzett = elsoIratPeldany.Cim_id_Cimzett;
        cimAdatokForKuldemeny.Updated.Cim_id_Cimzett = true;

        cimAdatokForKuldemeny.CimSTR_Cimzett = elsoIratPeldany.CimSTR_Cimzett;
        cimAdatokForKuldemeny.Updated.CimSTR_Cimzett = true;

        return cimAdatokForKuldemeny;
    }

    private void KimenoKuldemenyExpedialas(ExecParam execParam, string kimenoKuldemenyVonalkod, string kimenoKuld_Id, out EREC_KuldKuldemenyek kimenoKuldemeny, out Result result_kuldUpdate)
    {
        EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);
        ExecParam execParam_kuldGet = execParam.Clone();
        execParam_kuldGet.Record_Id = kimenoKuld_Id;

        Result result_kuldGet = service_kuld.Get(execParam_kuldGet);
        result_kuldGet.CheckError();

        kimenoKuldemeny = (EREC_KuldKuldemenyek)result_kuldGet.Record;

        // Ellenõrzés:
        if (kimenoKuldemeny.Allapot != KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt
            || kimenoKuldemeny.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            // hiba:
            Logger.Error("Az összeállított kimenõ küldemény nem expediálható: " + kimenoKuldemeny.Id, execParam);
            throw new ResultException(52521);
        }

        // Küldemény UPDATE:
        kimenoKuldemeny.Updated.SetValueAll(false);
        kimenoKuldemeny.Base.Updated.SetValueAll(false);
        kimenoKuldemeny.Base.Updated.Ver = true;

        // Állapot átállítása expediáltra:
        kimenoKuldemeny.Allapot = KodTarak.KULDEMENY_ALLAPOT.Expedialt;
        kimenoKuldemeny.Updated.Allapot = true;

        // vonalkód mezõ kitöltése:
        kimenoKuldemeny.BarCode = kimenoKuldemenyVonalkod;
        kimenoKuldemeny.Updated.BarCode = true;

        // expediáló:
        kimenoKuldemeny.FelhasznaloCsoport_Id_Expedial = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
        kimenoKuldemeny.Updated.FelhasznaloCsoport_Id_Expedial = true;

        // expediálás ideje:
        kimenoKuldemeny.ExpedialasIdeje = DateTime.Now.ToString();
        kimenoKuldemeny.Updated.ExpedialasIdeje = true;

        ExecParam execParam_kuldUpdate = execParam.Clone();
        execParam_kuldUpdate.Record_Id = kimenoKuldemeny.Id;

        result_kuldUpdate = service_kuld.Update(execParam_kuldUpdate, kimenoKuldemeny);
        result_kuldUpdate.Uid = kimenoKuldemeny.Id;

        result_kuldUpdate.CheckError();
    }

    #region HELPER
    public bool IsCimzettListasExpedialas(ExecParam execParam,
           string pldIratId,
           out EREC_PldIratPeldanyok existingPldIrat,
           out EREC_IraElosztoivek existingElosztoIv)
    {
        existingPldIrat = null;
        existingElosztoIv = null;

        Result resultPldIrat = GetPldIrat(execParam, pldIratId);
        if (!string.IsNullOrEmpty(resultPldIrat.ErrorCode))
            return false;
        existingPldIrat = (EREC_PldIratPeldanyok)resultPldIrat.Record;

        Result resultEIvAll = GetElosztoIvByIrat(execParam, existingPldIrat.IraIrat_Id);
        if (!string.IsNullOrEmpty(resultEIvAll.ErrorCode) || resultEIvAll.Ds.Tables[0].Rows.Count < 1)
            return false;

        Result resultEIv = GetElosztoIv(execParam, resultEIvAll.Ds.Tables[0].Rows[0]["Id"].ToString());
        existingElosztoIv = (EREC_IraElosztoivek)resultEIv.Record;
        return true;
    }
    private Result GetElosztoIv(ExecParam execParam, string id)
    {
        EREC_IraElosztoivekService service = new EREC_IraElosztoivekService(this.dataContext);
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        return result;
    }
    private Result GetPldIrat(ExecParam execParam, string id)
    {
        EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        return result;
    }
    private Result GetElosztoIvByIrat(ExecParam execParam, string iratId)
    {
        if (string.IsNullOrEmpty(iratId))
            return null;
        EREC_IraElosztoivekService service = new EREC_IraElosztoivekService(this.dataContext);
        EREC_IraElosztoivekSearch src = new EREC_IraElosztoivekSearch();
        src.WhereByManual = " and EREC_IraElosztoivek.Note = '" + iratId + "'";
        Result result = service.GetAll(execParam, src);

        return result;
    }
    private Result InvalidateElosztoIv(ExecParam execParam, string id)
    {
        EREC_IraElosztoivekService service = new EREC_IraElosztoivekService(this.dataContext);
        execParam.Record_Id = id;
        Result result = service.Invalidate(execParam);
        return result;
    }
    #endregion

    /// <summary>
    /// Visszaadja a Result.Record-ban az iratpéldányt, a felette lévõ iratot, ügyiratdarabot, illetve ügyiratot egy 
    /// IratPeldanyHierarchia típusú objektum formájában    
    /// </summary>
    /// <param name="execParam">execParam.Record_Id -ban kell megadni az irat Id-ját</param>
    /// <returns></returns>
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(IratPeldanyHierarchia))]
    public Result GetIratPeldanyHierarchia(ExecParam execParam)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            IratPeldanyHierarchia iratPeldanyHier = new IratPeldanyHierarchia();

            EREC_PldIratPeldanyok pldObj = null;
            EREC_IraIratok iratObj = null;
            EREC_UgyUgyiratdarabok ugyiratDarabObj = null;
            EREC_UgyUgyiratok ugyiratObj = null;

            result = this.GetObjectsHierarchyByIratpeldany(execParam, execParam.Record_Id, out pldObj, out iratObj
                    , out ugyiratDarabObj, out ugyiratObj);

            iratPeldanyHier.IratPeldanyObj = pldObj;
            iratPeldanyHier.IratObj = iratObj;
            iratPeldanyHier.UgyiratDarabObj = ugyiratDarabObj;
            iratPeldanyHier.UgyiratObj = ugyiratObj;

            result.Record = iratPeldanyHier;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    ///  Visszaadja a Result.Record-ban az iratpéldányt, a felette lévõ iratot, ügyiratdarabot, illetve ügyiratot egy 
    /// IratPeldanyHierarchia típusú objektum formájában 
    /// Jogosultság ellenõrzést végez
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="jogszint"></param>
    /// <returns></returns>
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(IratPeldanyHierarchia))]
    public Result GetIratPeldanyHierarchiaWithRightCheck(ExecParam execParam, char jogszint)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            IratPeldanyHierarchia iratPeldanyHier = new IratPeldanyHierarchia();

            EREC_PldIratPeldanyok pldObj = null;
            EREC_IraIratok iratObj = null;
            EREC_UgyUgyiratdarabok ugyiratDarabObj = null;
            EREC_UgyUgyiratok ugyiratObj = null;

            result = this.GetObjectsHierarchyByIratpeldany(execParam, execParam.Record_Id, true, jogszint
                , out pldObj, out iratObj, out ugyiratDarabObj, out ugyiratObj);

            iratPeldanyHier.IratPeldanyObj = pldObj;
            iratPeldanyHier.IratObj = iratObj;
            iratPeldanyHier.UgyiratDarabObj = ugyiratDarabObj;
            iratPeldanyHier.UgyiratObj = ugyiratObj;

            result.Record = iratPeldanyHier;

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Segédeljárás: iratpéldány id-ja alapján visszaadja a hierarchiában szereplõ
    /// négy rekord (iratpéldány, irat, ügyiratdarab, ügyirat) üzleti objektumát
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratPeldany_Id"></param>
    /// <param name="erec_PldIratPeldanyok"></param>
    /// <param name="erec_IraIratok"></param>
    /// <param name="erec_UgyUgyiratdarabok"></param>
    /// <param name="erec_UgyUgyiratok"></param>
    private Result GetObjectsHierarchyByIratpeldany(ExecParam execParam, string iratPeldany_Id
            , out EREC_PldIratPeldanyok erec_PldIratPeldanyok, out EREC_IraIratok erec_IraIratok
            , out EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, out EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        return this.GetObjectsHierarchyByIratpeldany(execParam, iratPeldany_Id, false, ' '
                , out erec_PldIratPeldanyok, out erec_IraIratok, out erec_UgyUgyiratdarabok, out erec_UgyUgyiratok);
    }


    private Result GetObjectsHierarchyByIratpeldany(ExecParam execParam, string iratPeldany_Id, bool withRightCheck, char jogszint
            , out EREC_PldIratPeldanyok erec_PldIratPeldanyok, out EREC_IraIratok erec_IraIratok
            , out EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, out EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        erec_PldIratPeldanyok = null;
        erec_IraIratok = null;
        erec_UgyUgyiratdarabok = null;
        erec_UgyUgyiratok = null;

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region Iratpéldány Get
            ExecParam execParam_pld = execParam.Clone();
            execParam_pld.Record_Id = iratPeldany_Id;

            Result result_pld = (withRightCheck) ?
                this.GetWithRightCheck(execParam, jogszint)
                : this.Get(execParam_pld);

            result_pld.CheckError();

            erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_pld.Record;
            #endregion


            #region Irat Get
            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
            ExecParam execParam_irat = execParam.Clone();
            execParam_irat.Record_Id = erec_PldIratPeldanyok.IraIrat_Id;

            Result result_irat = service_iratok.Get(execParam_irat);
            result_irat.CheckError();

            erec_IraIratok = (EREC_IraIratok)result_irat.Record;
            #endregion

            #region Ügyiratdarab Get
            EREC_UgyUgyiratdarabokService service_ugyiratdarab = new EREC_UgyUgyiratdarabokService(this.dataContext);
            ExecParam execParam_ugyiratDarab = execParam.Clone();
            execParam_ugyiratDarab.Record_Id = erec_IraIratok.UgyUgyIratDarab_Id;

            Result result_ugyiratDarab = service_ugyiratdarab.Get(execParam_ugyiratDarab);
            result_ugyiratDarab.CheckError();

            erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result_ugyiratDarab.Record;
            #endregion

            #region Ügyirat Get
            EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_ugyirat = execParam.Clone();
            execParam_ugyirat.Record_Id = erec_UgyUgyiratdarabok.UgyUgyirat_Id;

            Result result_ugyirat = service_ugyiratok.Get(execParam_ugyirat);
            result_ugyirat.CheckError();

            erec_UgyUgyiratok = (EREC_UgyUgyiratok)result_ugyirat.Record;
            #endregion

            result = result_pld;
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        result.CheckError();

        return result;
    }

    /// <summary>
    /// Iratpéldányok lekérése, szûrési feltételek megadása az iratpéldányokra és a felettes ügyiratokra is lehetséges
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="_EREC_UgyUgyiratokSearch"></param>
    /// <param name="_EREC_PldIratPeldanyokSearch"></param>
    /// <param name="_PeldanyJoinFilter"></param>
    /// <returns></returns>
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_PldIratPeldanyokSearch))]
    public Result GetAllByUgyirat(ExecParam execParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, EREC_PldIratPeldanyokSearch _EREC_PldIratPeldanyokSearch, EREC_PldIratPeldanyokSearch _PeldanyJoinFilter)
    {
        Log log = Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllByUgyirat(execParam, _EREC_UgyUgyiratokSearch, _EREC_PldIratPeldanyokSearch, _PeldanyJoinFilter);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    private bool IsIratPeldanyInFizikaiDosszie(ExecParam execParam, string IratPeldany_Id)
    {
        KRT_MappaTartalmakService service = new KRT_MappaTartalmakService(this.dataContext);
        KRT_MappaTartalmakSearch search = new KRT_MappaTartalmakSearch();

        search.Obj_Id.Value = IratPeldany_Id;
        search.Obj_Id.Operator = Query.Operators.equals;

        Result result = service.GetAllWithExtension(execParam, search);

        if (result.IsError)
            return true;

        foreach (DataRow r in result.Ds.Tables[0].Rows)
        {
            if (r["Mappa_Tipus"].ToString() == KodTarak.MAPPA_TIPUS.Fizikai)
                return true;
        }

        return false;

    }

    private bool IsMoved(EREC_PldIratPeldanyok Record)
    {
        //csak az elsõdleges iratpéldányok mozgása érdekel
        if (Record.Sorszam == "1")
        {
            if (Record.Updated.Csoport_Id_Felelos || Record.Updated.FelhasznaloCsoport_Id_Orzo)
            {
                return true;
            }
        }

        return false;
    }
    private Result Moved(ExecParam execParam)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            Arguments args = new Arguments();
            args.Add(new Argument("Record_Id", execParam.Record_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Moved(execParam);


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region Hivatali kapu

    [WebMethod()]
    public Result Expedialas_HivataliKapus(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array, string kimenoKuldemenyVonalkod, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, EREC_eBeadvanyok _EREC_eBeadvanyok, String[] dokumentum_Id_Array, HivataliKapuFileSettings fileSettings)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            this.HKPFileSettings = fileSettings;
            this.HKPBeadvany = _EREC_eBeadvanyok;
            //sima expediálás
            result = this.Expedialas(execParam, erec_PldIratPeldanyok_Id_Array, kimenoKuldemenyVonalkod, _EREC_PldIratPeldanyok, dokumentum_Id_Array);
            this.HKPFileSettings = null;
            this.HKPBeadvany = null;

            result.CheckError();

            string kuldemenyId = result.Uid;

            #region titkosítás

            if (fileSettings != null && fileSettings.EncryptFiles)
            {
                EncryptKuldemenyCsatolmanyai(execParam, kuldemenyId, _EREC_eBeadvanyok, fileSettings);
            }

            #endregion

            #region hivatali kapu

            FeltoltesSeged feltoltesSeged = GetCsoportosDokumentumFeltoltesAdatok(execParam, kuldemenyId);

            if (feltoltesSeged.Csatolmanyok.Count == 0)
            {
                throw new Exception(String.Format("A kimenõ küldeménynek nincs csatolmánya!"));
            }

            List<EREC_eBeadvanyok> eBeadvanyok = new List<EREC_eBeadvanyok>();
            List<Csatolmany> csatolmanyok = new List<Csatolmany>();


            foreach (DokumentumCsatolmany dokCsat in feltoltesSeged.Csatolmanyok)
            {
                EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
                eBeadvany.Updated.SetValueAll(false);
                eBeadvany.Base.Updated.SetValueAll(false);

                eBeadvany.Irany = "1"; //Kimenõ
                eBeadvany.Updated.Irany = true;
                eBeadvany.Allapot = "11"; //Hivatali kapunak elküldött
                eBeadvany.Updated.Allapot = true;
                //felülketrõl bekérjük ezeket az adatokat
                if (String.IsNullOrEmpty(_EREC_eBeadvanyok.KR_DokTipusHivatal))
                {
                    eBeadvany.KR_DokTipusHivatal = GetDokTipusHivatal(execParam);
                    eBeadvany.Updated.KR_DokTipusHivatal = true;
                }
                else
                {
                    eBeadvany.KR_DokTipusHivatal = _EREC_eBeadvanyok.KR_DokTipusHivatal;
                    eBeadvany.Updated.KR_DokTipusHivatal = true;
                }
                if (String.IsNullOrEmpty(_EREC_eBeadvanyok.KR_DokTipusAzonosito))
                {
                    eBeadvany.KR_DokTipusAzonosito = feltoltesSeged.Azonosito;
                    eBeadvany.Updated.KR_DokTipusAzonosito = true;
                }
                else
                {
                    eBeadvany.KR_DokTipusAzonosito = _EREC_eBeadvanyok.KR_DokTipusAzonosito;
                    eBeadvany.Updated.KR_DokTipusAzonosito = true;
                }
                if (String.IsNullOrEmpty(_EREC_eBeadvanyok.KR_DokTipusLeiras))
                {
                    eBeadvany.KR_DokTipusLeiras = feltoltesSeged.Leiras;
                    eBeadvany.Updated.KR_DokTipusLeiras = true;
                }
                else
                {
                    eBeadvany.KR_DokTipusLeiras = _EREC_eBeadvanyok.KR_DokTipusLeiras;
                    eBeadvany.Updated.KR_DokTipusLeiras = true;
                }

                eBeadvany.KR_FileNev = dokCsat.Csatolmany.Nev;
                eBeadvany.Updated.KR_FileNev = true;
                //eBeadvany.Dokumentum_Id = dokCsat.DokumentumId;
                //eBeadvany.Updated.Dokumentum_Id = true;
                eBeadvany.Base.Note = dokCsat.DokumentumId;
                eBeadvany.KuldKuldemeny_Id = feltoltesSeged.KuldemenyId;
                eBeadvany.Updated.KuldKuldemeny_Id = true;
                eBeadvany.KR_Valaszutvonal = "0"; // Közmû
                eBeadvany.Updated.KR_Valaszutvonal = true;
                eBeadvany.KR_Valasztitkositas = "0"; //nem
                eBeadvany.Updated.KR_Valasztitkositas = true;
                eBeadvany.KR_Rendszeruzenet = "0"; //nem
                eBeadvany.Updated.KR_Rendszeruzenet = true;

                #region címzett adatok

                eBeadvany.PartnerKapcsolatiKod = _EREC_eBeadvanyok.PartnerKapcsolatiKod;
                eBeadvany.Updated.PartnerKapcsolatiKod = true;
                eBeadvany.PartnerNev = _EREC_eBeadvanyok.PartnerNev;
                eBeadvany.Updated.PartnerNev = true;
                eBeadvany.PartnerEmail = _EREC_eBeadvanyok.PartnerEmail;
                eBeadvany.Updated.PartnerEmail = true;

                eBeadvany.PartnerKRID = _EREC_eBeadvanyok.PartnerKRID;
                eBeadvany.Updated.PartnerKRID = true;
                eBeadvany.PartnerRovidNev = _EREC_eBeadvanyok.PartnerRovidNev;
                eBeadvany.Updated.PartnerRovidNev = true;
                eBeadvany.PartnerMAKKod = _EREC_eBeadvanyok.PartnerMAKKod;
                eBeadvany.Updated.PartnerMAKKod = true;

                #endregion

                eBeadvany.KR_ETertiveveny = _EREC_eBeadvanyok.KR_ETertiveveny;
                eBeadvany.Updated.KR_ETertiveveny = true;

                eBeadvany.KR_Megjegyzes = _EREC_eBeadvanyok.KR_Megjegyzes;
                eBeadvany.Updated.KR_Megjegyzes = true;

                #region kr file

                string ext = Path.GetExtension(dokCsat.Csatolmany.Nev);

                if (".kr".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
                {
                    LoadKRFileMetaAdatok(eBeadvany, dokCsat.Csatolmany.Tartalom);
                }

                #endregion

                eBeadvany.KR_Fiok = _EREC_eBeadvanyok.KR_Fiok;
                eBeadvany.Updated.KR_Fiok = true;

                eBeadvany.Cel = KodTarak.eBeadvany_CelRendszer.WEB;
                eBeadvany.Updated.Cel = true;

                eBeadvanyok.Add(eBeadvany);
                csatolmanyok.Add(dokCsat.Csatolmany);
            }

            // BLG_1643
            EREC_eBeadvanyokService eBeadvanyokServcie = new EREC_eBeadvanyokService(this.dataContext);
            eBeadvanyokServcie.SendHKPAutomataValasz(_EREC_eBeadvanyok.KR_Fiok, eBeadvanyok, csatolmanyok, execParam);

            #endregion hivatali kapu
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            result.Record = null;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private class FeltoltesSeged
    {
        public string KuldemenyId { get; set; }

        public string Azonosito { get; set; }

        public string Leiras { get; set; }

        public List<DokumentumCsatolmany> Csatolmanyok = new List<DokumentumCsatolmany>();
    }

    private class DokumentumCsatolmany
    {
        public string DokumentumId;
        public Csatolmany Csatolmany { get; set; }
    }

    private byte[] GetDokTartalom(string externalLink)
    {
        Logger.Debug(String.Format("GetDokTartalom: {0}", externalLink));
        using (WebClient client = new WebClient())
        {
            client.Credentials = new NetworkCredential(UI.GetAppSetting("eSharePointBusinessServiceUserName"), UI.GetAppSetting("eSharePointBusinessServicePassword"), UI.GetAppSetting("eSharePointBusinessServiceUserDomain"));
            return client.DownloadData(externalLink);
        }
    }

    FeltoltesSeged GetCsoportosDokumentumFeltoltesAdatok(ExecParam execParam, string kuldemenyId)
    {
        ExecParam xpm = execParam;
        var peldanyIdList = new List<string>();

        #region példányok lekérése

        var kuldPeldService = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
        var kuldPeldSearch = new EREC_Kuldemeny_IratPeldanyaiSearch();
        kuldPeldSearch.KuldKuldemeny_Id.Filter(kuldemenyId);

        Result kuldPeldRes = kuldPeldService.GetAll(execParam, kuldPeldSearch);
        kuldPeldRes.CheckError();

        foreach (DataRow row in kuldPeldRes.Ds.Tables[0].Rows)
        {
            string peldanyId = row["Peldany_Id"].ToString();
            peldanyIdList.Add(peldanyId);
        }

        #endregion

        #region irat id-k lekérése

        var pldSearch = new EREC_PldIratPeldanyokSearch();
        pldSearch.Id.In(peldanyIdList);

        Result pldResult = this.GetAll(xpm, pldSearch);
        pldResult.CheckError();

        List<string> iratIdList = new List<string>();
        List<string> peldanyIktatoszamList = new List<string>();

        foreach (DataRow row in pldResult.Ds.Tables[0].Rows)
        {
            string id = row["Id"].ToString();
            string iratId = row["IraIrat_Id"].ToString();
            string iktatoszam = row["Azonosito"].ToString();

            iratIdList.Add(iratId);
            peldanyIktatoszamList.Add(iktatoszam);
        }

        #endregion

        #region iratok lekérése

        var iratService = new EREC_IraIratokService(dataContext);
        var iratSearch = new EREC_IraIratokSearch();
        iratSearch.Id.In(iratIdList);

        Result iratResult = iratService.GetAll(xpm, iratSearch);
        iratResult.CheckError();

        List<string> iratTargyList = new List<string>();
        foreach (DataRow row in iratResult.Ds.Tables[0].Rows)
        {
            string iratId = row["Id"].ToString();
            string targy = row["Targy"].ToString();
            iratTargyList.Add(targy);
        }

        #endregion

        #region csatolmányok lekérése

        EREC_CsatolmanyokService csatService = new EREC_CsatolmanyokService(this.dataContext);
        EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();

        csatSearch.KuldKuldemeny_Id.Value = kuldemenyId;
        csatSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        Result csatResult = csatService.GetAllWithExtension(xpm, csatSearch);

        if (csatResult.IsError)
            throw new ResultException(csatResult);

        #endregion

        FeltoltesSeged feltoltesSeged = new FeltoltesSeged();
        feltoltesSeged.KuldemenyId = kuldemenyId;
        feltoltesSeged.Azonosito = String.Join(", ", peldanyIktatoszamList.ToArray());
        feltoltesSeged.Leiras = String.Join(", ", iratTargyList.ToArray());

        foreach (DataRow row in csatResult.Ds.Tables[0].Rows)
        {
            string dokumentumId = row["Dokumentum_Id"].ToString();
            string iratId = row["IraIrat_Id"].ToString();
            string externalLink = row["External_Link"].ToString();
            string fajlNev = row["FajlNev"].ToString();

            Csatolmany csat = new Csatolmany();
            csat.Nev = fajlNev;
            csat.Tartalom = GetDokTartalom(externalLink);
            DokumentumCsatolmany dokCsat = new DokumentumCsatolmany();

            dokCsat.DokumentumId = dokumentumId;
            dokCsat.Csatolmany = csat;
            feltoltesSeged.Csatolmanyok.Add(dokCsat);
        }

        return feltoltesSeged;
    }

    EREC_eBeadvanyok GetKRFileMataAdatok(byte[] fileContent)
    {
        EREC_eBeadvanyok adatok = new EREC_eBeadvanyok();

        try
        {
            MemoryStream ms = new MemoryStream(fileContent);
            XmlReader reader = XmlReader.Create(ms);
            XDocument doc = XDocument.Load(reader);

            var fejlec = from element in doc.Root.Elements()
                         where element.Name.LocalName == "Fejlec"
                         select element;

            adatok.PartnerRovidNev = (from element in fejlec.Elements()
                                      where element.Name.LocalName == "Cimzett"
                                      select element.Value).FirstOrDefault();

            adatok.KR_DokTipusAzonosito = (from element in fejlec.Elements()
                                           where element.Name.LocalName == "DokTipusAzonosito"
                                           select element.Value).FirstOrDefault();

            adatok.KR_DokTipusLeiras = (from element in fejlec.Elements()
                                        where element.Name.LocalName == "DokTipusLeiras"
                                        select element.Value).FirstOrDefault();

            adatok.KR_FileNev = (from element in fejlec.Elements()
                                 where element.Name.LocalName == "FileNev"
                                 select element.Value).FirstOrDefault();

            adatok.KR_Megjegyzes = (from element in fejlec.Elements()
                                    where element.Name.LocalName == "Megjegyzes"
                                    select element.Value).FirstOrDefault();
        }
        catch (Exception ex)
        {
            Logger.Error("GetKRFileMataAdatok.Error", ex);
            return null;
        }

        return adatok;
    }

    void LoadKRFileMetaAdatok(EREC_eBeadvanyok dok, byte[] fileContent)
    {
        EREC_eBeadvanyok metaAdatok = GetKRFileMataAdatok(fileContent);

        if (metaAdatok != null)
        {
            dok.KR_DokTipusAzonosito = metaAdatok.KR_DokTipusAzonosito;
            dok.KR_DokTipusLeiras = metaAdatok.KR_DokTipusLeiras;
            dok.KR_Megjegyzes = metaAdatok.KR_Megjegyzes;
        }
    }


    string GetDokTipusHivatal(ExecParam execParam)
    {
        string hivatal = "FPH";

        string orgId = execParam.Org_Id;

        if (String.IsNullOrEmpty(orgId))
            orgId = "450B510A-7CAA-46B0-83E3-18445C0C53A9"; //default org

        KRT_OrgokService service = eAdminService.ServiceFactory.GetKRT_OrgokService();
        ExecParam execParamGet = execParam.Clone();
        execParamGet.Record_Id = orgId;
        Result res = service.Get(execParamGet);

        if (!res.IsError)
        {
            KRT_Orgok org = res.Record as KRT_Orgok;
            hivatal = org.Kod;
        }

        return hivatal;
    }

    void EncryptKuldemenyCsatolmanyai(ExecParam execParam, string kuldemenyId, EREC_eBeadvanyok eBeadvany, HivataliKapuFileSettings fileSettings)
    {
        string publicKey = GetPublikusKulcs(execParam, eBeadvany);

        if (String.IsNullOrEmpty(publicKey))
        {
            throw new ResultException("Nincs megadva publikus kulcs!");
        }

        var csatolmanyokService = new EREC_CsatolmanyokService(this.dataContext);
        var csatolmanyokSearch = new EREC_CsatolmanyokSearch();
        csatolmanyokSearch.KuldKuldemeny_Id.Filter(kuldemenyId);

        Result csatolmanyokResult = csatolmanyokService.GetAllWithExtensionInternal(execParam, csatolmanyokSearch, false);
        csatolmanyokResult.CheckError();

        foreach (DataRow row in csatolmanyokResult.Ds.Tables[0].Rows)
        {
            EREC_Csatolmanyok erec_Csatolmany = new EREC_Csatolmanyok();
            Utility.LoadBusinessDocumentFromDataRow(erec_Csatolmany, row);
            string csatolmanyId = erec_Csatolmany.Id;

            string fajlNev = row["FajlNev"].ToString();
            string externalLink = row["External_Link"].ToString();

            EncryptCsatolmany(execParam, fajlNev, externalLink, publicKey, fileSettings, erec_Csatolmany);

            ExecParam csatolmanyInvalidateExec = execParam.Clone();
            csatolmanyInvalidateExec.Record_Id = csatolmanyId;
            Result resultInvalidate = csatolmanyokService.Invalidate(csatolmanyInvalidateExec);
            resultInvalidate.CheckError();
        }
    }

    string GetPublikusKulcs(ExecParam execParam, EREC_eBeadvanyok eBeadvany)
    {
        var svc = eIntegratorService.ServiceFactory.geteUzenetService();
        Result result = svc.PublikusKulcsLekerdezese(eBeadvany.KR_Fiok, eBeadvany.PartnerKapcsolatiKod, eBeadvany.PartnerKRID);

        if (result.IsError)
        {
            result.ErrorDetail = new ErrorDetails { Message = result.ErrorMessage };
            result.ErrorCode = "81001";
            result.ErrorMessage = "Hiba a publikus kulcs lekérdezése során!";
            throw new ResultException(result);
        }

        return result.Record as string;
    }

    void EncryptCsatolmany(ExecParam execParam, string fajlNev, string externalLink, string publicKey, HivataliKapuFileSettings fileSettings, EREC_Csatolmanyok erec_Csatolmany)
    {
        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Nev = fajlNev;
        csatolmany.Tartalom = GetDokTartalom(externalLink);

        Csatolmany encryptedCsatolmany = new Csatolmany();
        encryptedCsatolmany.Nev = GetEncryptedFileName(fajlNev, fileSettings);
        encryptedCsatolmany.Tartalom = EncryptCsatolmany(execParam, csatolmany, publicKey);

        var service = new EREC_KuldKuldemenyekService(this.dataContext);
        ExecParam xpmUpload = execParam.Clone();
        xpmUpload.Record_Id = erec_Csatolmany.KuldKuldemeny_Id;
        erec_Csatolmany.Id = String.Empty;
        Logger.Debug("CsatolmanyUpload kezdete");
        Result resUpload = service.CsatolmanyUpload(xpmUpload, erec_Csatolmany, encryptedCsatolmany, true);
        Logger.Debug("CsatolmanyUpload vege");

        resUpload.CheckError();
    }

    byte[] EncryptCsatolmany(ExecParam execParam, Csatolmany csatolmany, string publicKey)
    {
        GPGEncryptFileManager efm = new GPGEncryptFileManager(execParam);
        string error;
        byte[] result = efm.EncryptFile(csatolmany.Nev, csatolmany.Tartalom, publicKey, out error);

        if (!String.IsNullOrEmpty(error))
        {
            Result errorResult = new Result();
            errorResult.ErrorDetail = new ErrorDetails { Message = error };
            errorResult.ErrorCode = "81002";
            errorResult.ErrorMessage = String.Format("Hiba a fájl tikosítása során során: {0}!", csatolmany.Nev);
            throw new ResultException(errorResult);
        }
        return result;
    }

    string GetEncryptedFileName(string fajlNev, HivataliKapuFileSettings fileSettings)
    {
        return String.Format("{0}.enc", fajlNev);
    }

    #endregion

    #region partnercim

    private Result BindPartnerCim(ExecParam execParam, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok)
    {
        Result result = new Result();

        if (_EREC_PldIratPeldanyok.Updated.Cim_id_Cimzett && _EREC_PldIratPeldanyok.Updated.Partner_Id_Cimzett
            && !String.IsNullOrEmpty(_EREC_PldIratPeldanyok.Cim_id_Cimzett) && !String.IsNullOrEmpty(_EREC_PldIratPeldanyok.Partner_Id_Cimzett))
        {
            Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            execParam.Record_Id = _EREC_PldIratPeldanyok.Partner_Id_Cimzett;

            result = service_partner.BindCim(execParam, _EREC_PldIratPeldanyok.Cim_id_Cimzett);
        }

        return result;
    }

    #endregion

    public void SetUCMJogosultsag(ExecParam execParam, string iratId)
    {
        Logger.Info(String.Format("EREC_PldIratPeldanyokService.SetUCMJogosultsag: {0}", iratId));
        try
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
                service.SetUCMJogosultsag(execParam, iratId);
            }
        }
        catch (Exception ex)
        {
            Logger.Error("EREC_PldIratPeldanyokService.SetUCMJogosultsag hiba", ex);
        }
    }

    public void SetUCMJogosultsagPeldanyok(ExecParam execParam, string ids)
    {
        Logger.Info(String.Format("EREC_PldIratPeldanyokService.SetUCMJogosultsagPeldanyok: {0}", ids));
        try
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                EREC_PldIratPeldanyokSearch peldanyokSearch = new EREC_PldIratPeldanyokSearch();
                peldanyokSearch.Id.Value = ids;
                peldanyokSearch.Id.Operator = Query.Operators.inner;

                Result peldanyokResult = this.GetAll(execParam, peldanyokSearch);
                peldanyokResult.CheckError();

                var iratIds = new List<string>();
                foreach (DataRow row in peldanyokResult.Ds.Tables[0].Rows)
                {
                    string iratId = row["IraIrat_Id"].ToString();

                    if (!iratIds.Contains(iratId))
                        iratIds.Add(iratId);
                }

                EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);

                foreach (string iratId in iratIds)
                {
                    service.SetUCMJogosultsag(execParam, iratId);
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("EREC_PldIratPeldanyokService.SetUCMJogosultsagPeldanyok hiba", ex);
        }
    }

    #region PostaHibrid

    deliveryInstructions PH_DeliveryInstructions;

    [WebMethod()]
    public Result Expedialas_PostaHibrid(ExecParam execParam, string[] erec_PldIratPeldanyok_Id_Array,
        string kimenoKuldemenyVonalkod, EREC_PldIratPeldanyok _EREC_PldIratPeldanyok, EREC_eBeadvanyok _EREC_eBeadvanyok,
        String[] dokumentum_Id_Array, HivataliKapuFileSettings fileSettings, deliveryInstructions deliveryInstructions)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            fileSettings.PackFiles = true;
            fileSettings.PackedFileName = String.Format("{0}_{1:yyyyMMddHHmmss}", GetDokTipusHivatal(execParam), DateTime.Now);
            fileSettings.FileFormat = HivataliKapuFileSettings.HivataliKapuFileFormat.Krx;

            //Magyar Posta Zrt.
            //_EREC_eBeadvanyok.PartnerKRID = "506341775";
            //_EREC_eBeadvanyok.Updated.PartnerKRID = true;
            //_EREC_eBeadvanyok.PartnerNev = "Magyar Posta Zrt.";
            //_EREC_eBeadvanyok.Updated.PartnerNev = true;
            //_EREC_eBeadvanyok.PartnerRovidNev = "MPZRT";
            //_EREC_eBeadvanyok.Updated.PartnerRovidNev = true;
            PostaHibridHelper.SetPostaCimzett(execParam, _EREC_eBeadvanyok);

            this.PH_DeliveryInstructions = deliveryInstructions;
            result = this.Expedialas_HivataliKapus(execParam, erec_PldIratPeldanyok_Id_Array, kimenoKuldemenyVonalkod, _EREC_PldIratPeldanyok, _EREC_eBeadvanyok, dokumentum_Id_Array, fileSettings);
            this.PH_DeliveryInstructions = null;

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            result.Record = null;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    #endregion


    /// <summary>
    /// Irathoz tartozo minden olyan iratpeldany Kezeloje legyen az Intezkedo ugyintezoben megadott felhasznalo, 
    /// ami annal a szemelynel van, aki szignal, kieéve az ugyiratban marado peldanyet, 
    /// mert az az ugyiratba tart (kezeloje a Felelos ugyintézo)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <returns></returns>
    internal Result IratPeldanySzignalasInternal(ExecParam execParam, string iratId, string csoport_Id_Felelos_Kovetkezo)
    {
        Result result = new Result();
        Logger.Debug("IratPeldanySzignalasInternal start:");

        #region CHECKS
        if (string.IsNullOrEmpty(iratId))
            throw new Exception("Nem kitoltott az irat azonosito !");

        if (string.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
            throw new Exception("Nem kitoltott a felelos mezo !");
        #endregion

        #region Iratpéldányok lekérése
        EREC_PldIratPeldanyokService pldService = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_PldIratPeldanyokSearch pldSearch = new EREC_PldIratPeldanyokSearch();

        pldSearch.IraIrat_Id.Filter(iratId);

        ////szignalo szemelynel van
        pldSearch.FelhasznaloCsoport_Id_Orzo.Filter(Csoportok.GetFelhasznaloSajatCsoportId(execParam));

        pldSearch.Allapot.NotEquals(KodTarak.IRATPELDANY_ALLAPOT.Sztornozott);

        ////Ugyiratban marado nem kell
        pldSearch.Eredet.NotEquals(KodTarak.IRAT_FAJTA.Masolat);

        var resultPldGetAll = pldService.GetAll(execParam.Clone(), pldSearch);
        resultPldGetAll.CheckError();

        if (resultPldGetAll.GetCount < 1)
        {
            Logger.Debug("IratPeldanySzignalasInternal: Nincsenek szignalando iratpeldanyok !");
            return result;
        }
        #endregion

        string tmpIratPldId = null;

        foreach (DataRow pldRow in resultPldGetAll.Ds.Tables[0].Rows)
        {
            #region ATADAS
            tmpIratPldId = pldRow["Id"].ToString();
            string csoport_Id_Felelos = pldRow["Csoport_Id_Felelos"].ToString();
            if (csoport_Id_Felelos != csoport_Id_Felelos_Kovetkezo)
            {
                Logger.Debug("Iratpeldany atadas start: " + tmpIratPldId);
                Result resultAtadas = Atadas(execParam, tmpIratPldId, csoport_Id_Felelos_Kovetkezo, null);
                resultAtadas.CheckError();
                Logger.Debug("Iratpeldany atadas stop: " + tmpIratPldId);
            }
            #endregion
        }
        Logger.Debug("IratPeldanySzignalasInternal stop:");
        return result;
    }

    /// <summary>
    /// Le kell válogatni azokat az iratpéldányokat, ahol a Státusz: Újraküldendő, 
    /// az Expediálás módja: Hivatali Kapu, és a kimenő küldemény csatolmányai között van meghiusulási igazolás:
    /// Az adott iratpéldányok esetén a státuszt át kell állítani "Címzett nem vette át"-ra.
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="felhasznaloSzervezetId"></param>
    /// <returns></returns>
    [WebMethod(Description = "Meghiúsulási igazolás - újrapostázás")]
    public Result IratPeldanyMeghiusulasiIgazolassalUjraPostazas(string felhasznaloId, string felhasznaloSzervezetId)
    {
        if (string.IsNullOrEmpty(felhasznaloId))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "";
            errorResult.ErrorMessage = "Nincs megadva a Felhasznalo_Id!";
            Logger.Error("Nincs megadva a Felhasznalo_Id!");
            return errorResult;
        }

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;
        if (!string.IsNullOrEmpty(felhasznaloSzervezetId))
            execParam.FelhasznaloSzervezet_Id = felhasznaloSzervezetId;

        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("START IratPeldanyMeghiusulasiIgazolassalUjraPostazas");

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.GetAllIratPeldanyMeghiusulasiIgazolassalUjraPostazashoz(execParam);
            result.CheckError();

            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                ExecParam execParamFind = execParam.Clone();
                ExecParam execParamUpdate = execParam.Clone();
                Result resultFind = null;
                Result resultUpdate = null;
                EREC_PldIratPeldanyok iratPeldany = null;

                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    #region FIND
                    execParamFind.Record_Id = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    execParamUpdate.Record_Id = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    resultFind = Get(execParamFind);
                    resultFind.CheckError();

                    if (resultFind.Record == null)
                        continue;

                    iratPeldany = (EREC_PldIratPeldanyok)resultFind.Record;
                    #endregion

                    #region SET
                    iratPeldany.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at;
                    iratPeldany.Updated.Allapot = true;
                    #endregion

                    #region UPDATE
                    resultUpdate = Update(execParamUpdate, iratPeldany);
                    resultUpdate.CheckError();
                    #endregion
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            Logger.Debug(e.Message + e.StackTrace);
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            result.Record = null;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public void CeckIratPeldanyaiCimzes(ExecParam execParam, EREC_IraIratok irat)
    {
        if (!Rendszerparameterek.GetBoolean(execParam, "CIM_TIPUS_FILTER_ENABLED", false)
            && !Rendszerparameterek.GetBoolean(execParam, "UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA_FILTER_ENABLED", false))
        {
            return;
        }

        if (irat.Allapot != KodTarak.IRAT_ALLAPOT.Atiktatott
                && irat.Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott
                && irat.Allapot != KodTarak.IRAT_ALLAPOT.Felszabaditva
                && irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
        {
            EREC_PldIratPeldanyokSearch peldanyokSearch = new EREC_PldIratPeldanyokSearch();
            peldanyokSearch.IraIrat_Id.Value = irat.Id;
            peldanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

            Result result = this.GetAll(execParam, peldanyokSearch);

            result.CheckError();

            StringBuilder errors = new StringBuilder();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string error = CheckPeldanyCimzes(execParam, id);

                if (!String.IsNullOrEmpty(error))
                {
                    errors.Append(error);
                }
            }

            if (errors.Length > 0)
            {
                throw new Exception(errors.ToString());
            }
        }
    }

    public string CheckPeldanyCimzes(ExecParam execParam, string peldanyId)
    {
        if (!Rendszerparameterek.GetBoolean(execParam, "CIM_TIPUS_FILTER_ENABLED", false)
            && !Rendszerparameterek.GetBoolean(execParam, "UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA_FILTER_ENABLED", false))
        {
            return null;
        }

        StringBuilder errors = new StringBuilder();

        ExecParam getExecParam = execParam.Clone();
        getExecParam.Record_Id = peldanyId;
        Result getResult = this.Get(getExecParam);

        getResult.CheckError();

        EREC_PldIratPeldanyok peldany = getResult.Record as EREC_PldIratPeldanyok;

        //Expediálás módjának ellenőrzése
        if (Rendszerparameterek.GetBoolean(execParam, "UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA_FILTER_ENABLED", false))
        {
            string vezerloKodcsoportKod = "UGYINTEZES_ALAPJA";
            string fuggoKodcsoportKod = "KULDEMENY_KULDES_MODJA";
            string ugyintezesModja = peldany.UgyintezesModja;
            string kuldesMod = peldany.KuldesMod;

            bool hasNincsItem = false;

            List<string> engedelyettKuldesModok = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, vezerloKodcsoportKod, fuggoKodcsoportKod, ugyintezesModja, out hasNincsItem);

            if (engedelyettKuldesModok != null && engedelyettKuldesModok.Count > 0)
            {
                if (!engedelyettKuldesModok.Contains(kuldesMod, StringComparer.CurrentCultureIgnoreCase))
                {
                    errors.Append(String.Format("A példány ({0}) expediálás módja nem megfelelő! Küldés módja nem engedélyezett, vezérlő kódcsoport: {1}, ügyintézés módja: {2}, függő kódcsoport: {3}, küldés módja: {4}", peldany.Azonosito, vezerloKodcsoportKod, ugyintezesModja, fuggoKodcsoportKod, peldany.KuldesMod));
                }
            }
        }

        //Cím típusának ellenőrzése
        if (Rendszerparameterek.GetBoolean(execParam, "CIM_TIPUS_FILTER_ENABLED", false))
        {
            string cimId = peldany.Cim_id_Cimzett;
            string cimText = peldany.CimSTR_Cimzett;

            string cimTipus = String.Empty;

            EREC_PldIratPeldanyok elsoIratPeldanyCimListaban = null;
            EREC_IraElosztoivek existingElosztoIv = null;
            bool cimzettListasExpedialas = IsCimzettListasExpedialas(execParam.Clone(), peldany.Id, out elsoIratPeldanyCimListaban, out existingElosztoIv);

            if (!String.IsNullOrEmpty(cimId))
            {
                KRT_CimekService cimekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CimekService();
                ExecParam cimGetExecParam = execParam.Clone();
                cimGetExecParam.Record_Id = cimId;

                Result cimGetResult = cimekService.Get(cimGetExecParam);

                cimGetResult.CheckError();

                KRT_Cimek cim = cimGetResult.Record as KRT_Cimek;
                cimTipus = cim.Tipus;
            }

            string vezerloKodcsoportKod = "KULDEMENY_KULDES_MODJA";
            string fuggoKodcsoportKod = "CIM_TIPUS";
            string kuldesMod = peldany.KuldesMod;

            bool hasNincsItem = false;

            List<string> engedelyettCimTipusok = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarakKodList(execParam, vezerloKodcsoportKod, fuggoKodcsoportKod, kuldesMod, out hasNincsItem);

            if (!cimzettListasExpedialas)
            {
                if (engedelyettCimTipusok != null && engedelyettCimTipusok.Count > 0)
                {
                    if (String.IsNullOrEmpty(cimTipus))
                    {
                        if (!hasNincsItem)
                        {
                            errors.Append(String.Format("A példány ({0}) címének kitöltése kötelező! ", peldany.Azonosito));
                        }
                    }
                    else
                    {
                        if (!engedelyettCimTipusok.Contains(cimTipus, StringComparer.CurrentCultureIgnoreCase))
                        {
                            errors.Append(String.Format("A példány ({0}) címének típusa nem megfelelő! Cím típusa nem engedélyezett, vezérlő kódcsoport: {1}, küldés módja: {2}, függő kódcsoport: {3}, cím típusa: {4} ", peldany.Azonosito, vezerloKodcsoportKod, kuldesMod, fuggoKodcsoportKod, cimTipus));
                        }
                    }
                }
            }
        }

        if (errors.Length > 0)
        {
            return errors.ToString();
        }
        else
        {
            return null;
        }
    }
}