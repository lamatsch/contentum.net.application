using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;
using Contentum.eIntegrator;


[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_SzamlakService : System.Web.Services.WebService
{
    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra).
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_KuldKuldemenyekSearch"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_SzamlakSearch _EREC_SzamlakSearch)
    {
        return GetAllWithExtensionAndJogosultak(ExecParam, _EREC_SzamlakSearch, false);
    }

    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra).
    /// Jogosults�g alapj�n is sz�r
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_SzamlakSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_SzamlakSearch _EREC_SzamlakSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_SzamlakSearch, Jogosultak);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    // Seg�dmet�dus: Az �tvett keres�si objektum seg�ts�g�vel megkeresi az els� illeszked� t�telt
    // �s tal�lat eset�n �zleti objektumk�nt adja vissza az a Result objektum Recordj�ban (egy�bk�nt hiba).
    private Result SearchSzamla(ExecParam execParam, EREC_SzamlakSearch search)
    {
        // csak az els� t�telt k�rj�k le - ha van
        search.TopRow = 1;

        Result result_szamlaGetAll = sp.GetAll(execParam, search);

        if (result_szamlaGetAll.IsError)
        {
            throw new ResultException(result_szamlaGetAll);
        }

        if (result_szamlaGetAll.Ds.Tables[0].Rows.Count == 0)
        {
            // TODO:
            throw new ResultException(100101); //"Sz�ml�k: A t�tel nem tal�lhat�!"
        }

        EREC_Szamlak erec_Szamlak = new EREC_Szamlak();
        Utility.LoadBusinessDocumentFromDataRow(erec_Szamlak, result_szamlaGetAll.Ds.Tables[0].Rows[0]);

        Result result = new Result();
        result.Record = erec_Szamlak;

        return result;
    }

    /// <summary>
    /// GetByKuldemenyId(ExecParam ExecParam)  
    /// A EREC_Szamlak rekord elk�r�se az adott t�bl�b�l a KuldKuldemeny_Id alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a KuldKuldemeny_Id alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Szamlak))]
    public Result GetByKuldemenyId(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                // TODO:
                throw new ResultException(100102); //"Sz�ml�k: Nincs megadva a k�ldem�ny azonos�t�ja!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_SzamlakSearch search = new EREC_SzamlakSearch();
            search.KuldKuldemeny_Id.Value = ExecParam.Record_Id;
            search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

            result = this.SearchSzamla(ExecParam.Clone(), search);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// GetByDokumentumId(ExecParam ExecParam)  
    /// A EREC_Szamlak rekord elk�r�se az adott t�bl�b�l a Dokumentum_Id alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a Dokumentum_Id alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Szamlak))]
    public Result GetByDokumentumId(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                // TODO:
                throw new ResultException(100103); //"Sz�ml�k: Nincs megadva a dokumentum azonos�t�ja!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_SzamlakSearch search = new EREC_SzamlakSearch();
            search.Dokumentum_Id.Value = ExecParam.Record_Id;
            search.Dokumentum_Id.Operator = Query.Operators.equals;

            result = this.SearchSzamla(ExecParam.Clone(), search);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// GetByObjId(ExecParam ExecParam)  
    /// A EREC_Szamlak rekord elk�r�se az adott t�bl�b�l a KuldKuldemeny_Id vagy a Dokumentum_Id alapj�n (ExecParam.Record_Id parameter). 
    /// </summary>   
    /// <param name="ExecParam">Az ExecParam bemen� param�ter tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elk�r�se az adott t�bl�b�l a KuldKuldemeny_Id vagy a Dokumentum_Id alapj�n (ExecParam.Record_Id parameter). Az ExecParam tov�bbi adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Szamlak))]
    public Result GetByObjId(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (String.IsNullOrEmpty(ExecParam.Record_Id))
            {
                // TODO:
                throw new ResultException(100104); //"Sz�ml�k: Nincs megadva a k�ldem�ny vagy dokumentum azonos�t�ja!"
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_SzamlakSearch search = new EREC_SzamlakSearch();
            search.KuldKuldemeny_Id.Group = "222";
            search.KuldKuldemeny_Id.GroupOperator = Query.Operators.or;
            search.KuldKuldemeny_Id.Value = ExecParam.Record_Id;
            search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

            search.Dokumentum_Id.Group = "222";
            search.Dokumentum_Id.GroupOperator = Query.Operators.or;
            search.Dokumentum_Id.Value = ExecParam.Record_Id;
            search.Dokumentum_Id.Operator = Query.Operators.equals;

            result = this.SearchSzamla(ExecParam.Clone(), search);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region FillPirEdokSzamla
    private void FillPartnerAdatokForPIR(ExecParam ExecParam, EREC_Szamlak _EREC_Szamlak, PirEdokSzamla pirEdokSzamla)
    {
        if (pirEdokSzamla == null || String.IsNullOrEmpty(_EREC_Szamlak.Partner_Id_Szallito)) return;

        #region Partner adatok lek�r�se
        Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam execParam_partnerGetAll = ExecParam.Clone();

        KRT_PartnerekSearch search_partner = new KRT_PartnerekSearch();

        search_partner.Id.Value = _EREC_Szamlak.Partner_Id_Szallito;
        search_partner.Id.Operator = Query.Operators.equals;

        search_partner.TopRow = 1;

        Result result_partnerGetAll = service_partner.GetAllForSzamla(execParam_partnerGetAll, search_partner, _EREC_Szamlak.Cim_Id_Szallito, _EREC_Szamlak.Bankszamlaszam_Id);
        if (result_partnerGetAll.IsError)
        {
            throw new ResultException(result_partnerGetAll);
        }

        if (result_partnerGetAll.Ds.Tables[0].Rows.Count == 0)
        {
            throw new ResultException(53100); // A partner nem tal�lhat�.
        }

        DataRow partnerRow = result_partnerGetAll.Ds.Tables[0].Rows[0];

        #endregion Partner adatok lek�r�se

        string IRSZ = partnerRow["IRSZ"].ToString();
        string TelepulesNev = partnerRow["TelepulesNev"].ToString();

        if (String.IsNullOrEmpty(IRSZ) || String.IsNullOrEmpty(TelepulesNev))
        {
            // c�m k�telez� elemeinek kiolvas�sa
            string CimText = partnerRow["CimTobbi"].ToString();

            if (!String.IsNullOrEmpty(CimText))
            {
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^\s*(\d+)\s+([-a-zA-Z]+)\s*");

                System.Text.RegularExpressions.Match match = regex.Match(CimText);

                System.Text.RegularExpressions.MatchCollection matches = regex.Matches(CimText);

                if (match != null && match.Groups.Count > 0)
                {
                    if (String.IsNullOrEmpty(IRSZ))
                    {
                        IRSZ = match.Groups[1].Value;
                    }

                    if (match.Groups.Count > 1)
                    {
                        if (String.IsNullOrEmpty(TelepulesNev))
                        {
                            TelepulesNev = match.Groups[2].Value;
                        }
                    }
                }
            }
        }

        pirEdokSzamla.EDOKPartnerKod = partnerRow["Id"].ToString();
        pirEdokSzamla.EDOKPartnerNev = partnerRow["Nev"].ToString();
        pirEdokSzamla.EDOKPartnerIranyitoszam = IRSZ;
        pirEdokSzamla.EDOKPartnerHelyseg = TelepulesNev;
        pirEdokSzamla.EDOKPartnerCim = partnerRow["CimNev"].ToString();
        pirEdokSzamla.BelfoldiAdoszam = partnerRow["BelfoldiAdoszam"].ToString();
        pirEdokSzamla.KozossegiAdoszam = partnerRow["KozossegiAdoszam"].ToString();
        pirEdokSzamla.MaganszemelyAdoazonositoJel = partnerRow["AdoazonositoJel"].ToString();
        pirEdokSzamla.HelyrajziSzam = partnerRow["HRSZ"].ToString();
        pirEdokSzamla.Bankszamlaszam1 = partnerRow["Bankszamlaszam1"].ToString();
        pirEdokSzamla.Bankszamlaszam2 = partnerRow["Bankszamlaszam2"].ToString();
        pirEdokSzamla.Bankszamlaszam3 = partnerRow["Bankszamlaszam3"].ToString();
        pirEdokSzamla.Telefonszam = partnerRow["Telefonszam"].ToString();
        pirEdokSzamla.Fax = partnerRow["Fax"].ToString();
        pirEdokSzamla.EmailCim = partnerRow["EmailCim"].ToString();
    }

    private void FillSzamlaAdatokForPIR(EREC_Szamlak erec_Szamlak, PirEdokSzamla pirEdokSzamla)
    {
        if (erec_Szamlak == null || pirEdokSzamla == null) return;

        pirEdokSzamla.Szamlaszam = erec_Szamlak.SzamlaSorszam;
        pirEdokSzamla.FizetesiMod = erec_Szamlak.FizetesiMod;
        pirEdokSzamla.TeljesitesDatuma = erec_Szamlak.TeljesitesDatuma;
        pirEdokSzamla.BizonylatDatuma = erec_Szamlak.BizonylatDatuma;
        pirEdokSzamla.FizetesiHatarido = erec_Szamlak.FizetesiHatarido;
        pirEdokSzamla.DevizaKod = erec_Szamlak.DevizaKod;
        pirEdokSzamla.VisszakuldesDatuma = erec_Szamlak.VisszakuldesDatuma;
        pirEdokSzamla.EllenorzoOsszeg = erec_Szamlak.EllenorzoOsszeg;
        pirEdokSzamla.Megjegyzes = erec_Szamlak.Base.Note;

        pirEdokSzamla.PIRAllapot = erec_Szamlak.PIRAllapot;

        // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
        pirEdokSzamla.VevoBesorolas = erec_Szamlak.VevoBesorolas;
    }

    private void FillErkeztetesAdatokForPIR(SzamlaErkeztetesParameterek _SzamlaErkeztetesParameterek, PirEdokSzamla pirEdokSzamla)
    {
        pirEdokSzamla.Vonalkod = _SzamlaErkeztetesParameterek.Vonalkod;
        pirEdokSzamla.ErkeztetesDatuma = _SzamlaErkeztetesParameterek.BeerkezesIdeje;
        pirEdokSzamla.SzervezetiEgysegKod = _SzamlaErkeztetesParameterek.Csoport_Id_SzervezetiEgyseg;
        pirEdokSzamla.SzervezetiEgysegNev = _SzamlaErkeztetesParameterek.SzervezetiEgyseg;
    }
    #endregion FillPirEdokSzamla

    #region PartnerData
    private Result UpdatePartnerDataIfNecessary(ExecParam ExecParam, EREC_Szamlak _EREC_Szamlak, SzamlaErkeztetesParameterek _SzamlaErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (_SzamlaErkeztetesParameterek != null)
            {

                if (_SzamlaErkeztetesParameterek.BindPartnerCim || _SzamlaErkeztetesParameterek.Adoszam != null)
                {
                    Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

                    if (_SzamlaErkeztetesParameterek.Adoszam != null)
                    {
                        ExecParam execParam_updateAdoszam = ExecParam.Clone();
                        execParam_updateAdoszam.Record_Id = _EREC_Szamlak.Partner_Id_Szallito;

                        Result result_updateAdoszam = service_partner.UpdateAdoszam(execParam_updateAdoszam, _SzamlaErkeztetesParameterek.Adoszam, _SzamlaErkeztetesParameterek.IsKulfoldiAdoszam, _SzamlaErkeztetesParameterek.InsertSzemelyOrVallakozasToPartnerIfMissing);

                        if (result_updateAdoszam.IsError)
                        {
                            throw new ResultException(result_updateAdoszam);
                        }
                    }

                    if (_SzamlaErkeztetesParameterek.BindPartnerCim && !String.IsNullOrEmpty(_EREC_Szamlak.Cim_Id_Szallito))
                    {
                        ExecParam execParam_bindCim = ExecParam.Clone();
                        execParam_bindCim.Record_Id = _EREC_Szamlak.Partner_Id_Szallito;

                        Result result_bindCim = service_partner.BindCim(execParam_bindCim, _EREC_Szamlak.Cim_Id_Szallito);

                        if (result_bindCim.IsError)
                        {
                            throw new ResultException(result_bindCim);
                        }
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #endregion PartnerData

    /// <summary>
    /// Sz�mla felvitele + partner ad�sz�m �s c�m insert/update, ha kell
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_SzamlaErkeztetesParameterek">A l�trehozand� sz�mla �s kapcsol�d� partner kieg�sz�t� adatait tartalmaz� objektum.
    /// Csak a felhaszn�l� �ltal a fel�leten is kit�lthet� mez�ket veszi figyelembe.</param>
    /// <returns>A m�velet sikeress�g�t jelz� Result objektum.
    /// Result.Uid tartalmazza a l�trehozott irat Id-j�t</returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result SzamlaFelvitel(ExecParam ExecParam, EREC_Szamlak _EREC_Szamlak, SzamlaErkeztetesParameterek _SzamlaErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        string insertedSzamlaId = String.Empty;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
            //isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            isTransactionBeginHere = true;

            Result result_updatePartnerData = UpdatePartnerDataIfNecessary(ExecParam.Clone(), _EREC_Szamlak, _SzamlaErkeztetesParameterek);

            if (result_updatePartnerData.IsError)
            {
                throw new ResultException(result_updatePartnerData);
            }

            Result result_szamlaInsert = sp.Insert(Constants.Insert, ExecParam, _EREC_Szamlak);

            if (result_szamlaInsert.IsError)
            {
                throw new ResultException(result_szamlaInsert);
            }

            insertedSzamlaId = result_szamlaInsert.Uid;

            result = result_szamlaInsert;

            bool isPIREnabled = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);


            if (isPIREnabled)
            {
                #region PirEdok

                PirEdokSzamla pirEdokSzamla = new PirEdokSzamla();
                FillPartnerAdatokForPIR(ExecParam.Clone(), _EREC_Szamlak, pirEdokSzamla);
                FillSzamlaAdatokForPIR(_EREC_Szamlak, pirEdokSzamla);
                FillErkeztetesAdatokForPIR(_SzamlaErkeztetesParameterek, pirEdokSzamla);

                // CR3234 P�nz�gyi interface
                string PIR_Rendszer = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.PIR_RENDSZER);
                Result result_piredok = null;

                if (PIR_Rendszer == "PIR")
                {

                    PirEdokInterfaceService service_piredok = new PirEdokInterfaceService();
                    ExecParam execParam_piredok = ExecParam.Clone();
                    result_piredok = service_piredok.PirEdokSzamlaFejFelvitel(execParam_piredok, pirEdokSzamla);

                    // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                    // CR kapcs�n hibakezel�s m�dos�t�sa
                    if (result_piredok.IsError)
                    {
                        // EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
                        throw new ResultException(result_piredok);
                    }
                }
                else
                {
                    // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                    // CR kapcs�n hibakezel�s m�dos�t�sa
                    try
                    {

                        // CR3234: PIR interface miatt (mivel az k�l�n webservicen fut, a tranzakci� nem vihet� �t
                        // COMMIT
                        dataContext.CommitIfRequired(isTransactionBeginHere);


                        //  INT_PIRService.PIR_Rendszer = PIR_Rendszer;
                        Contentum.eIntegrator.Service.ServiceFactory sf =
                            Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory;

                        Contentum.eIntegrator.Service.INT_PIRService service_piredok = sf.getINT_PIRService(PIR_Rendszer);

                        ExecParam execParam_piredok = ExecParam.Clone();
                        execParam_piredok.Record_Id = result_szamlaInsert.Uid;

                        result_piredok = service_piredok.PirSzamlaFejFelvitel(execParam_piredok, pirEdokSzamla);

                        //Teszt
                        //ExecParam execParam_piredokfile = ExecParam.Clone();
                        //execParam_piredokfile.Record_Id = result_szamlaInsert.Uid;
                        //PIRFileContent image = new PIRFileContent();
                        //image.Id = pirEdokSzamla.Szamlaszam;
                        //image.FileName = "Teszt.txt";
                        //byte[]  x = System.IO.File.ReadAllBytes("c:\\Contentum_files\\pdf-sample.pdf");
                        //image.Content = x;
                        //  Result result_piredokfile = service_piredok.PirSzamlaKepFeltoltes(execParam_piredokfile, image);
                        //  if (result_piredokfile.IsError)
                        //{
                        //    throw new ResultException(result_piredokfile);
                        //}


                        if (result_piredok.IsError)
                        {
                            // EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
                            throw new ResultException(result_piredok);
                        }

                    }
                    catch (Exception e)
                    {

                        dataContext.RollbackIfRequired(isTransactionBeginHere);
                        result = ResultException.GetResultFromException(e);
                        // Ha a PIR webservice hib�t dob, akkor a sz�mla st�tusz�t fogadottra �ll�tjuk (mivel m�r felvitt�k)
                        if (!String.IsNullOrEmpty(insertedSzamlaId))
                        {
                            ExecParam execParam_szamlaUpdate = ExecParam.Clone();
                            execParam_szamlaUpdate.Record_Id = insertedSzamlaId;
                            Result szamlaResult = this.Get(execParam_szamlaUpdate);
                            EREC_Szamlak szamla = (EREC_Szamlak)szamlaResult.Record;
                            if (szamla != null)
                            {
                                if (szamla.PIRAllapot != KodTarak.PIR_ALLAPOT.Fogadott)
                                {
                                    szamla.PIRAllapot = KodTarak.PIR_ALLAPOT.Fogadott;
                                    szamla.Updated.PIRAllapot = true;

                                    Result szamlaUpdateResult = this.Update(execParam_szamlaUpdate, szamla);
                                }

                                //result = ResultError.CreateNewResultWithErrorCode(1000132);
                                result.Record = szamla;
                                result.ErrorMessage = result.ErrorMessage + @"\n\n A sz�mla Fogadott st�tusszal r�gz�t�sre ker�lt!!!\n A sz�mla m�dos�t�s k�perny�n van lehet�s�g �jrak�ld�sre. ";
                            }
                        }
                    }
                }
                #endregion PirEdok

                // COMMIT
                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
            // CR kapcs�n hibakezel�s m�dos�t�sa
            //if (!String.IsNullOrEmpty(insertedSzamlaId))
            //{
            //    ExecParam execParam_szamlaUpdate = ExecParam.Clone();
            //    execParam_szamlaUpdate.Record_Id = insertedSzamlaId;
            //    Result szamlaResult = this.Get(execParam_szamlaUpdate);
            //    EREC_Szamlak szamla = (EREC_Szamlak)szamlaResult.Record;
            //    if (szamla != null)
            //    {
            //        if (szamla.PIRAllapot != KodTarak.PIR_ALLAPOT.Fogadott)
            //        {
            //            szamla.PIRAllapot = KodTarak.PIR_ALLAPOT.Fogadott;
            //            szamla.Updated.PIRAllapot = true;

            //            Result szamlaUpdateResult = this.Update(execParam_szamlaUpdate, szamla);
            //        }

            //        //result = ResultError.CreateNewResultWithErrorCode(1000132);
            //        result.Record = szamla;
            //        result.ErrorMessage = result.ErrorMessage + "<br /><br /> A sz�mla 'Fogadott' st�tusszal r�gz�t�sre ker�lt!!! <br /> A RENDBEN gomb lenyom�sa �j r�gz�t�st eredm�nyez! <br /> A sz�mla m�dos�t�s k�perny�n van lehet�s�g �jrak�ld�sre. ";
            //    }
            //}
            //return result;

        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// K�ldem�ny m�dos�t�sa + partner ad�sz�m �s c�m insert/update, ha kell
    /// Felel�s v�ltoz�s�nak figyel�se: ha v�ltozik a felel�s, k�zbes�t�si t�tel gener�l�s
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result SzamlaModositas(ExecParam ExecParam, EREC_Szamlak _EREC_Szamlak, SzamlaErkeztetesParameterek _SzamlaErkeztetesParameterek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            //isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            isTransactionBeginHere = true;

            bool isPIREnabled = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
            // CR3234 P�nz�gyi interface
            string PIR_Rendszer = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.PIR_RENDSZER);

            PirEdokInterfaceService service_piredok = null;
            ExecParam execParam_piredok = null;
            PirEdokSzamla pirEdokSzamla = new PirEdokSzamla();
            FillPartnerAdatokForPIR(ExecParam.Clone(), _EREC_Szamlak, pirEdokSzamla);
            FillSzamlaAdatokForPIR(_EREC_Szamlak, pirEdokSzamla);
            FillErkeztetesAdatokForPIR(_SzamlaErkeztetesParameterek, pirEdokSzamla);
            if (isPIREnabled)
            {

                #region PirEdok �llapot ellen�rz�s

                if (PIR_Rendszer == "PIR")
                {
                    service_piredok = new PirEdokInterfaceService();
                    execParam_piredok = ExecParam.Clone();

                    // PIR: Allapot ellen�rz�se
                    Result result_piredokCheckAllapot = service_piredok.CheckPirEdokSzamlaFejAllapot(execParam_piredok, _SzamlaErkeztetesParameterek.Vonalkod, PirEdokInterfaceService.MuveletType.Modositas);
                }
                else
                {

                    Contentum.eIntegrator.Service.ServiceFactory sf =
                        Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory;

                    Contentum.eIntegrator.Service.INT_PIRService service_piredokCheck = sf.getINT_PIRService(PIR_Rendszer);

                    ExecParam execParam_piredokCheck = ExecParam.Clone();
                    // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                    execParam_piredokCheck.Record_Id = _EREC_Szamlak.Id;

                    //execParam_piredok.Record_Id = result_szamlaInsert.Uid;

                    Result result_piredokCheckAllapot = service_piredokCheck.PirSzamlaFejAllapot(execParam_piredokCheck, pirEdokSzamla);
                    if (result_piredokCheckAllapot.IsError)
                    {
                        throw new ResultException(result_piredokCheckAllapot);
                    }

                    if (result_piredokCheckAllapot.Record != null)
                    {
                        string pirallapot = result_piredokCheckAllapot.Record.ToString().Trim();

                        // result.Record = pirallapot;

                        Contentum.eRecord.BaseUtility.PirEdokSzamlak.Statusz statusz = new Contentum.eRecord.BaseUtility.PirEdokSzamlak.Statusz(null, _SzamlaErkeztetesParameterek.Vonalkod, pirallapot);

                        ErrorDetails errorDetail;

                        if (!Contentum.eRecord.BaseUtility.PirEdokSzamlak.Modosithato(statusz, ExecParam, out errorDetail))
                        {
                            throw new ResultException(100000, errorDetail);
                        }
                    }

                }
                #endregion PirEdok �llapot ellen�rz�s
            }

            Result result_updatePartnerData = UpdatePartnerDataIfNecessary(ExecParam, _EREC_Szamlak, _SzamlaErkeztetesParameterek);

            if (result_updatePartnerData.IsError)
            {
                throw new ResultException(result_updatePartnerData);
            }

            Result result_szamlaUpdate = sp.Insert(Constants.Update, ExecParam, _EREC_Szamlak);

            if (result_szamlaUpdate.IsError)
            {
                throw new ResultException(result_szamlaUpdate);
            }

            //PirEdokSzamla pirEdokSzamlaUpdate = new PirEdokSzamla();
            //FillPartnerAdatokForPIR(ExecParam.Clone(), _EREC_Szamlak, pirEdokSzamla);
            //FillSzamlaAdatokForPIR(_EREC_Szamlak, pirEdokSzamla);
            //FillErkeztetesAdatokForPIR(_SzamlaErkeztetesParameterek, pirEdokSzamla);

            if (isPIREnabled)
            {

                #region PirEdok
                if (PIR_Rendszer == "PIR")
                {
                    Result result_piredok = service_piredok.PirEdokSzamlaFejModositas(execParam_piredok, pirEdokSzamla);

                    if (result_piredok.IsError)
                    {
                        throw new ResultException(result_piredok);
                    }
                }
                else
                {
                    // CR3234: PIR interface miatt (mivel az k�l�n webservicen fut, a tranzakci� nem vihet� �t
                    // COMMIT
                    dataContext.CommitIfRequired(isTransactionBeginHere);

                    Contentum.eIntegrator.Service.ServiceFactory sf =
                        Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory;

                    Contentum.eIntegrator.Service.INT_PIRService service_piredokUpdate = sf.getINT_PIRService(PIR_Rendszer);

                    ExecParam execParam_piredokUpdate = ExecParam.Clone();
                    // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                    execParam_piredokUpdate.Record_Id = _EREC_Szamlak.Id;
                    Result result_piredokUpdate = service_piredokUpdate.PirSzamlaFejModositas(execParam_piredokUpdate, pirEdokSzamla);

                    if (result_piredokUpdate.IsError)
                    {
                        throw new ResultException(result_piredokUpdate);
                    }
                    #endregion PirEdok
                }
            }
            result = result_szamlaUpdate;

            #region Esem�nynapl�z�s
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_Szamlak", "SzamlaModify").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)   
            // CR kapcs�n hibakezel�s m�dos�t�sa     
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.ErrorCode.Substring(0, 3) == "PIR")
                {
                    ExecParam execParam_szamlaUpdate = ExecParam.Clone();
                    execParam_szamlaUpdate.Record_Id = _EREC_Szamlak.Id;
                    Result szamlaResult = this.Get(execParam_szamlaUpdate);
                    EREC_Szamlak szamla = (EREC_Szamlak)szamlaResult.Record;
                    if (szamla != null)
                    {
                        if (szamla.PIRAllapot != KodTarak.PIR_ALLAPOT.Fogadott)
                        {
                            szamla.PIRAllapot = KodTarak.PIR_ALLAPOT.Fogadott;
                            szamla.Updated.PIRAllapot = true;

                            Result szamlaUpdateResult = this.Update(execParam_szamlaUpdate, szamla);
                        }


                        result.Record = szamla;
                        result.ErrorMessage = result.ErrorMessage + "<br/><br/>A sz�mlaadatok �tad�sa nem siker�lt!<br/>A m�dos�t�sok ment�sre ker�ltek";
                    }
                }
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Sz�mla ront�s/sztorn� (val�j�ban csak PIRAllapot be�ll�t�s, felt�ve, hogy van akt�v, enged�lyezett PIR interf�sz)
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza, Record_Id tartalmazza a sztorn�zand� k�ldem�ny azonos�t�j�t.</param>
    /// <returns>A m�velet sikeress�g�t jelz� Result objektum.</returns>
    [WebMethod(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_KuldKuldemenyek))]
    public Result SzamlaSztorno(ExecParam ExecParam, String BarCode, String SztornozasIndoka)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        bool isPIREnabled = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);

        if (isPIREnabled)
        {
            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

                #region PirEdok
                // CR3234 P�nz�gyi interface
                string PIR_Rendszer = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.PIR_RENDSZER);


                if (PIR_Rendszer == "PIR")
                {
                    PirEdokInterfaceService service_piredok = new PirEdokInterfaceService();
                    ExecParam execParam_piredok = ExecParam.Clone();

                    // PIR: Allapot ellen�rz�se
                    Result result_piredokCheckAllapot = service_piredok.CheckPirEdokSzamlaFejAllapot(execParam_piredok, BarCode, PirEdokInterfaceService.MuveletType.Sztorno);

                    if (result_piredokCheckAllapot.IsError)
                    {
                        throw new ResultException(result_piredokCheckAllapot);
                    }

                    // Rontas (PIR)
                    Result result_piredok = service_piredok.PirEdokSzamlaFejRontas(execParam_piredok, BarCode, SztornozasIndoka);

                    if (result_piredok.IsError)
                    {
                        throw new ResultException(result_piredok);
                    }
                }
                else
                {

                    //EREC_SzamlakService service_szamla = eRecordService.ServiceFactory.GetEREC_SzamlakService();
                    ExecParam execParam_szamlaUpdate = ExecParam.Clone();
                    Result szamlaResult = this.Get(execParam_szamlaUpdate);
                    EREC_Szamlak _EREC_Szamlak = (EREC_Szamlak)szamlaResult.Record;


                    PirEdokSzamla pirEdokSzamla = new PirEdokSzamla();
                    FillPartnerAdatokForPIR(ExecParam.Clone(), _EREC_Szamlak, pirEdokSzamla);
                    FillSzamlaAdatokForPIR(_EREC_Szamlak, pirEdokSzamla);
                    //FillErkeztetesAdatokForPIR(_SzamlaErkeztetesParameterek, pirEdokSzamla);

                    Contentum.eIntegrator.Service.ServiceFactory sf =
                        Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory;

                    Contentum.eIntegrator.Service.INT_PIRService service_piredok = sf.getINT_PIRService(PIR_Rendszer);

                    ExecParam execParam_piredok = ExecParam.Clone();
                    //execParam_piredok.Record_Id = result_szamlaInsert.Uid;

                    // CR3359 Sz�mla �tad�sn�l (PIR interface) �j csoportos�t� mez� (ett�l f�gg hogy BOPMH-ban melyik webservice-re adja �t a sz�ml�t)
                    // �llapotcheck-n�l a teljes pirEdokSzamla �tad�sa
                    //Result result_piredokCheckAllapot = service_piredok.PirSzamlaFejAllapot(execParam_piredok, _EREC_Szamlak.SzamlaSorszam);
                    Result result_piredokCheckAllapot = service_piredok.PirSzamlaFejAllapot(execParam_piredok, pirEdokSzamla);

                    if (result_piredokCheckAllapot.IsError)
                    {
                        throw new ResultException(result_piredokCheckAllapot);
                    }
                    string pirallapot = KodTarak.PIR_ALLAPOT.Fogadott;
                    if (result_piredokCheckAllapot.Record != null && string.IsNullOrEmpty(result_piredokCheckAllapot.Record.ToString()))
                        pirallapot = result_piredokCheckAllapot.Record.ToString().Trim();

                    // result.Record = pirallapot;

                    Contentum.eRecord.BaseUtility.PirEdokSzamlak.Statusz statusz = new Contentum.eRecord.BaseUtility.PirEdokSzamlak.Statusz(null, BarCode, pirallapot);

                    ErrorDetails errorDetail;
                    if (!Contentum.eRecord.BaseUtility.PirEdokSzamlak.Sztornozhato(statusz, ExecParam, out errorDetail))
                    {
                        throw new ResultException(100000, errorDetail);
                    }

                    Result result_piredokRontas = service_piredok.PirSzamlaFejSztorno(execParam_piredok, _EREC_Szamlak.SzamlaSorszam);
                    if (result_piredokRontas.IsError)
                    {
                        throw new ResultException(result_piredokRontas);
                    }
                }

                #endregion PirEdok

                #region Rontas �llapot visszavezet�se (EDOK)
                Result result_szamla = this.Get(ExecParam);

                if (result_szamla.IsError)
                {
                    throw new ResultException(result_szamla);
                }

                EREC_Szamlak erec_Szamlak = (EREC_Szamlak)result_szamla.Record;

                if (erec_Szamlak != null)
                {
                    ExecParam execParam_szamlaUpdate = ExecParam.Clone();
                    execParam_szamlaUpdate.Record_Id = erec_Szamlak.Id;

                    erec_Szamlak.Updated.SetValueAll(false);
                    erec_Szamlak.Base.Updated.SetValueAll(false);

                    erec_Szamlak.PIRAllapot = KodTarak.PIR_ALLAPOT.Sztornozott;
                    erec_Szamlak.Updated.PIRAllapot = true;

                    erec_Szamlak.Base.Updated.Ver = true;

                    Result result_szamlaUpdate = this.Update(execParam_szamlaUpdate, erec_Szamlak);

                    if (result_szamlaUpdate.IsError)
                    {
                        throw new ResultException(result_szamlaUpdate);
                    }

                    result = result_szamlaUpdate;
                }
                #endregion Rontas �llapot visszavezet�se (EDOK)

                #region Esem�nynapl�z�s
                if (isConnectionOpenHere)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                    KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, erec_Szamlak.Id, "EREC_Szamlak", "SzamlaSztorno").Record;

                    if (!String.IsNullOrEmpty(SztornozasIndoka))
                    {
                        eventLogRecord.Base.Note = SztornozasIndoka;
                    }

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
                #endregion

                // COMMIT
                dataContext.CommitIfRequired(isTransactionBeginHere);
            }
            catch (Exception e)
            {
                dataContext.RollbackIfRequired(isTransactionBeginHere);
                result = ResultException.GetResultFromException(e);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }

        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    private static DateTime? lastPIRAllapotUpdate = null;
    private readonly TimeSpan tsSchedulePIRUpdate = new TimeSpan(0, 10, 0);
    private static readonly object _sync = new object();

    /// <summary>
    /// Sz�ml�k PIR �llapot�nak aktualiz�l�sa(felt�ve, hogy van akt�v, enged�lyezett PIR interf�sz)
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza.</param>
    /// <returns>A m�velet sikeress�g�t jelz� Result objektum.</returns>
    [WebMethod(false)]
    public Result PIRAllapotUpdate_Tomeges(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        // egyidej� aszinkron h�v�sok kiz�r�sa
        if (System.Threading.Monitor.TryEnter(_sync))
        {
            bool isConnectionOpenHere = false;
            bool isTransactionBeginHere = false;
            bool isPIREnabled = Rendszerparameterek.GetBoolean(ExecParam, Rendszerparameterek.PIR_INTERFACE_ENABLED);
            // CR3234 P�nz�gyi interface
            // TODO: Jelenleg a t�meges v�ltoz�skezel�s nincs megval�s�tva
            string PIR_Rendszer = Rendszerparameterek.Get(ExecParam, Rendszerparameterek.PIR_RENDSZER);

            bool isNextSchedule = (lastPIRAllapotUpdate.HasValue ? DateTime.Now.Subtract(lastPIRAllapotUpdate.Value) > tsSchedulePIRUpdate : true);

            Logger.Info(String.Format("PIRAllapotUpdate_Tomeges: PIREnabled: {0}; Schedule start: {1};Last update: {2}", isPIREnabled, isNextSchedule, lastPIRAllapotUpdate));

            try
            {
                if (isPIREnabled && isNextSchedule)
                {
                    DateTime dtNow = DateTime.Now;

                    if (PIR_Rendszer == "PIR")
                    {
                        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


                        #region PirEdok
                        PirEdokInterfaceService service_piredok = new PirEdokInterfaceService();

                        // PIR: Allapotok lek�r�se
                        Result result_piredok = service_piredok.PirEdokSzamlaFejAllapotValtozas(ExecParam, lastPIRAllapotUpdate.HasValue ? lastPIRAllapotUpdate.Value : DateTime.MinValue);

                        if (result_piredok.IsError)
                        {
                            throw new ResultException(result_piredok);
                        }

                        System.Collections.Generic.Dictionary<string, string> dictPIRAllapot = new System.Collections.Generic.Dictionary<string, string>();
                        foreach (DataRow row in result_piredok.Ds.Tables[0].Rows)
                        {
                            string vonalkod = row[0].ToString();
                            string pirallapot = row[1].ToString();
                            if (!String.IsNullOrEmpty(vonalkod) && !dictPIRAllapot.ContainsKey(vonalkod))
                            {
                                dictPIRAllapot.Add(vonalkod, pirallapot);
                            }
                        }
                        #endregion PirEdok

                        #region PIR �llapot visszavezet�se (EDOK)

                        string szamlaIds = String.Empty;

                        if (dictPIRAllapot.Count > 0)
                        {
                            string[] vonalkodok = new string[dictPIRAllapot.Count];
                            string[] ids = new string[dictPIRAllapot.Count];
                            string[] vers = new string[dictPIRAllapot.Count];
                            string[] pirallapotok = new string[dictPIRAllapot.Count];
                            dictPIRAllapot.Keys.CopyTo(vonalkodok, 0);

                            ExecParam execParam_byVonalkod = ExecParam.Clone();

                            EREC_SzamlakSearch search_byVonalkod = new EREC_SzamlakSearch(true);
                            search_byVonalkod.Extended_EREC_KuldKuldemenyekSearch.BarCode.Value = Search.GetSqlInnerString(vonalkodok);
                            search_byVonalkod.Extended_EREC_KuldKuldemenyekSearch.BarCode.Operator = Query.Operators.inner;

                            Result result_byVonalkod = this.GetAllWithExtension(execParam_byVonalkod, search_byVonalkod);

                            if (result_byVonalkod.IsError)
                            {
                                throw new ResultException(result_byVonalkod);
                            }

                            int j = 0;
                            for (int i = 0; i < result_byVonalkod.Ds.Tables[0].Rows.Count; i++)
                            {
                                DataRow row = result_byVonalkod.Ds.Tables[0].Rows[i];
                                string id = row["Id"].ToString();
                                string vonalkod = row["BarCode"].ToString();
                                string ver = row["Ver"].ToString();
                                string curPIRAllapot = row["PIRAllapot"].ToString();

                                if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(ver)
                                    && !String.IsNullOrEmpty(vonalkod)
                                    && dictPIRAllapot.ContainsKey(vonalkod))
                                {
                                    if (dictPIRAllapot[vonalkod] != curPIRAllapot)
                                    {
                                        ids[j] = id;
                                        pirallapotok[j] = dictPIRAllapot[vonalkod];
                                        vers[j] = ver;
                                        j++;
                                    }
                                    else
                                    {
                                        Logger.Debug(String.Format("PIRAllapot UPDATE allapot azonos: id: {0};vonalkod: {1}; ver: {2}; pirallapot: {3}", id, vonalkod, ver, curPIRAllapot));
                                    }
                                }
                                else
                                {
                                    Logger.Warn(String.Format("PIRAllapot UPDATE hiba: id: {0};vonalkod: {1}; ver: {2}; pirallapot: {3}", id, vonalkod, ver, curPIRAllapot));
                                }
                            }

                            szamlaIds = Search.GetSqlInnerString(ids);

                            ExecParam execParam_szamlaUpdate = ExecParam.Clone();
                            Result result_szamlaUpdate = sp.UpdatePIRAllapot_Tomeges(ExecParam, szamlaIds
                                , String.Join(",", vers)
                                , Search.GetSqlInnerString(pirallapotok)
                                , DateTime.Now);

                            if (result_szamlaUpdate.IsError)
                            {
                                throw new ResultException(result_szamlaUpdate);
                            }


                        }
                        else
                        {
                            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
                            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

                            #region PirEdok
                            Contentum.eIntegrator.Service.ServiceFactory sf = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory;
                            Contentum.eIntegrator.Service.INT_PIRService service_pir = sf.getINT_PIRService(PIR_Rendszer);

                            // PIR: Allapotok lek�r�se
                            Result result_pir = service_pir.PirSzamlaFejAllapotValtozas(ExecParam, lastPIRAllapotUpdate.HasValue ? lastPIRAllapotUpdate.Value : DateTime.MinValue);

                            if (result_pir.IsError)
                            {
                                throw new ResultException(result_piredok);
                            }

                            #endregion PirEdok

                            string[][] allapotChanged = (string[][])result_pir.Record;
                            szamlaIds = Search.GetSqlInnerString(allapotChanged[0]);

                            ExecParam execParam_szamlaUpdate = ExecParam.Clone();
                            Result result_szamlaUpdate = sp.UpdatePIRAllapot_Tomeges(ExecParam, szamlaIds
                                , String.Join(",", allapotChanged[1])
                                , Search.GetSqlInnerString(allapotChanged[2])
                                , DateTime.Now);

                            if (result_szamlaUpdate.IsError)
                            {
                                throw new ResultException(result_szamlaUpdate);
                            }
                            result = result_szamlaUpdate;

                            lastPIRAllapotUpdate = dtNow;

                            #region Esem�nynapl�z�s
                            if (isTransactionBeginHere)
                            {
                                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(ExecParam, szamlaIds, "SzamlaModify");
                                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                                    Logger.Debug("[ERROR]EREC_Szamlak::SzamlaModify: ", ExecParam, eventLogResult);
                            }
                            #endregion
                        }
                        #endregion PIR �llapot visszavezet�se (EDOK)
                        // COMMIT
                        dataContext.CommitIfRequired(isTransactionBeginHere);
                    }
                }
            }
            catch (Exception e)
            {
                dataContext.RollbackIfRequired(isTransactionBeginHere);
                result = ResultException.GetResultFromException(e);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);

                System.Threading.Monitor.Exit(_sync);
            }
        }
        else
        {
            Logger.Info("PIRAllapotUpdate_Tomeges: konkurrens h�v�s.");
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}