using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eRecord.Service;
using System.Data;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_SzignalasiJegyzekekService : System.Web.Services.WebService
{


    #region Seg�delj�r�sok

    /// <summary>
    /// Szign�l�s t�pus�nak lek�r�se:
    /// Result.Record -ban k�ldj�k vissza az eredm�nyt
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetSzignalasTipusaByIratMetaDefinicio(ExecParam execParam, String erec_IratMetaDefinicio_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_SzignalasiJegyzekekSearch search = new EREC_SzignalasiJegyzekekSearch();

            search.UgykorTargykor_Id.Value = erec_IratMetaDefinicio_Id;
            search.UgykorTargykor_Id.Operator = Query.Operators.equals;

            Result result_GetAll = this.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
            {
                // hiba: 
                throw new ResultException(result_GetAll);
            }

            // nem lehet t�bb tal�lat:
            if (result_GetAll.Ds.Tables[0].Rows.Count > 1)
            { 
                // hiba:
                throw new ResultException(52591);
            }

            if (result_GetAll.Ds.Tables[0].Rows.Count == 1)
            {
                DataRow row = result_GetAll.Ds.Tables[0].Rows[0];

                string szignalasTipusa = row["SzignalasTipusa"].ToString();

                result.Record = szignalasTipusa;
            }
            else
            {
                result.Record = "";
            }

            

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }



    /// <summary>
    /// Szign�l�s t�pus�nak lek�r�se:
    /// Result.Record -ban k�ldj�k vissza a szign�l�s t�pus�t
    /// Result.Uid -ba ker�l a Javasolt felel�s, ha volt megadva
    ///  
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetSzignalasTipusaByUgykorUgytipus(ExecParam execParam, String Ugykor_Id, String Ugytipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        if (String.IsNullOrEmpty(Ugykor_Id))
        {
            //szign�l�s t�pusa
            result.Record = KodTarak.SZIGNALAS_TIPUSA._6_IktatoSzabadonValaszthat;
            return result;
        }

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // EREC_IratMetaDefiniciok t�bl�ban elvileg ahhoz a sorhoz van szign�l�si jegyz�k, ahol Ugytipus is null, 
            // de lek�rj�k az olyan sorokat is, ahol Ugytipus meg van adva VAGY Ugytipus is null

            EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);
            EREC_IratMetaDefinicioSearch search_iratMetaDef = new EREC_IratMetaDefinicioSearch();

            search_iratMetaDef.Ugykor_Id.Value = Ugykor_Id;
            search_iratMetaDef.Ugykor_Id.Operator = Query.Operators.equals;

            if (!String.IsNullOrEmpty(Ugytipus))
            {
                search_iratMetaDef.Ugytipus.Value = Ugytipus;
                search_iratMetaDef.Ugytipus.Operator = Query.Operators.equals;
                search_iratMetaDef.Ugytipus.Group = "789";
                search_iratMetaDef.Ugytipus.GroupOperator = Query.Operators.or;

                search_iratMetaDef.Manual_Ugytipus_isNull.Value = "";
                search_iratMetaDef.Manual_Ugytipus_isNull.Operator = Query.Operators.isnull;
                search_iratMetaDef.Manual_Ugytipus_isNull.Group = "789";
                search_iratMetaDef.Manual_Ugytipus_isNull.GroupOperator = Query.Operators.or;
            }
            else
            {
                search_iratMetaDef.Ugytipus.Value = "";
                search_iratMetaDef.Ugytipus.Operator = Query.Operators.isnull;
            }

            search_iratMetaDef.Manual_SzignalasTipusa.Value = "";
            search_iratMetaDef.Manual_SzignalasTipusa.Operator = Query.Operators.notnull;
            
            // als�bb hierarchiaszinteken nem lehet �rt�k
            search_iratMetaDef.EljarasiSzakasz.Operator = Query.Operators.isnull;
            search_iratMetaDef.Irattipus.Operator = Query.Operators.isnull;

            Result result_iratMetaDef_GetAll = service_iratMetaDef.GetAllWithExtension(execParam, search_iratMetaDef);
            if (!String.IsNullOrEmpty(result_iratMetaDef_GetAll.ErrorCode))
            {
                // hiba:
                if (!String.IsNullOrEmpty(result_iratMetaDef_GetAll.ErrorCode))
                {
                    throw new ResultException(result_iratMetaDef_GetAll);
                }
            }

            string szignalasTipusa = "";
            string javasoltFelelos_Id = "";

            // elvileg csak k�t sor j�hetett (ahol Ugytipus null, meg ahol meg van adva)

            int rowCount = result_iratMetaDef_GetAll.Ds.Tables[0].Rows.Count;

            foreach(DataRow row in result_iratMetaDef_GetAll.Ds.Tables[0].Rows)
            {
                string ugytipus_row = row["Ugytipus"].ToString();

                if (rowCount > 1)
                {
                    // ha t�bb sor j�tt, az kell, ahol az Ugytipus nem null
                    if (!String.IsNullOrEmpty(ugytipus_row))
                    {
                        szignalasTipusa = row["SzignalasTipusa_Kod"].ToString();
                        javasoltFelelos_Id = row["SzignalasiJegyzek_JavasoltFelelos"].ToString();
                    }
                }
                else
                {
                    szignalasTipusa = row["SzignalasTipusa_Kod"].ToString();
                    javasoltFelelos_Id = row["SzignalasiJegyzek_JavasoltFelelos"].ToString();
                }
            }


            result.Record = szignalasTipusa;
            // javasolt felel�s a result.Uid -ba:
            result.Uid = javasoltFelelos_Id;

            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }




    #endregion
}
