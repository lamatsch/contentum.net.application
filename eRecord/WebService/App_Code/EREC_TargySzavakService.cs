using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Web.Script.Services;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_TargySzavakService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_TargySzavakSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_TargySzavakSearch _EREC_TargySzavakSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_TargySzavakSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    
    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public string[] GetTargySzavakList(string prefixText, int count, string contextKey)
    //{
    //    string[] parameters = contextKey.Split(';');
    //    String userId = parameters[0];

    //    EREC_TargySzavakService service = new EREC_TargySzavakService();
    //    ExecParam execparam = new ExecParam();
    //    execparam.Felhasznalo_Id = userId;

    //    EREC_TargySzavakSearch search = new EREC_TargySzavakSearch();
    //    search.OrderBy = " EREC_TargySzavak.TargySzavak " + Utility.customCollation + " ASC, EREC_TargySzavak.Id";
        
    //    if (!String.IsNullOrEmpty(prefixText))
    //    {
    //        search.TargySzavak.Value = prefixText + '%';
    //        search.TargySzavak.Operator = Contentum.eQuery.Query.Operators.like;
    //        search.TopRow = count;
    //        Result res = service.GetAll(execparam, search);

    //        if (String.IsNullOrEmpty(res.ErrorCode))
    //        {
    //            return TargySzavakDataSourceToStringPairArray(res);
    //        }
    //    }

    //    return null;

    //}
    
    //private static string[] TargySzavakDataSourceToStringPairArray(Result res)
    //{
    //    if (res.Ds != null)
    //    {
    //        string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
    //        string value = "";
    //        string text = "";
    //        for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
    //        {
    //            value = res.Ds.Tables[0].Rows[i]["Id"].ToString();
    //    text = String.Empty;

    //            text += res.Ds.Tables[0].Rows[i]["TargySzavak"].ToString();

    //            string item =  Utility.CreateAutoCompleteItem(text, value);
    //            StringArray.SetValue(item, i);
    //        }
    //        return StringArray;
    //    }
    //    else
    //    {
    //        return null;
    //    }
    //}    

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTargyszavakList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("GetTargyszavakList webservice elindul");
        Logger.Debug(String.Format("Param�terek: prefixText {0}, count: {1}, contextKey {2}", prefixText, count, contextKey));

        String userId = contextKey.Split(';')[0];

        Logger.Debug("Felhaszn�l� id: " + userId);

      
        if (!String.IsNullOrEmpty(prefixText))
        {
            ExecParam execparam = new ExecParam();
            execparam.Felhasznalo_Id = userId;

            EREC_TargySzavakSearch search = new EREC_TargySzavakSearch();
            search.OrderBy = " TargySzavak " + Utility.customCollation + " ASC";
            search.TargySzavak.Value = prefixText + '%';
            search.TargySzavak.Operator = Query.Operators.like;
            search.TopRow = count;

            Result res = this.GetAllWithExtension(execparam, search);

            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Debug("GetTargyszavakList webservice v�ge");
                Logger.Debug("A visszat�rt elemek sz�ma: " + res.Ds.Tables[0].Rows.Count);
                Logger.DebugEnd();
                return TargySzavakDataSourceToStringPairArray(res);
            }
            else
            {
                Logger.Error(String.Format("GetAllWithExtension webservice hiba: Hibakod: {0}, Hibauzenet: {1} ",res.ErrorCode,res.ErrorMessage));
            }
        }

        Logger.Debug("GetTargyszavakList webservice v�ge null visszat�r�si �rt�kkel");
        Logger.DebugEnd();
        return null;

    }

    private static string[] TargySzavakDataSourceToStringPairArray(Result res)
    {
        const string delimiter = "��";
        if (res.Ds != null)
        {
            string[] StringArray = new string[res.Ds.Tables[0].Rows.Count];
            string text = "";
            for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
            {
                string[] values = new string[7];
                values[0] = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                values[1] = res.Ds.Tables[0].Rows[i]["Tipus"].ToString();
                values[2] = res.Ds.Tables[0].Rows[i]["RegExp"].ToString();
                values[3] = res.Ds.Tables[0].Rows[i]["AlapertelmezettErtek"].ToString();
                values[4] = res.Ds.Tables[0].Rows[i]["ToolTip"].ToString();
                values[5] = res.Ds.Tables[0].Rows[i]["ControlTypeSource"].ToString();
                values[6] = res.Ds.Tables[0].Rows[i]["ControlTypeDataSource"].ToString();

                text = res.Ds.Tables[0].Rows[i]["TargySzavak"].ToString();
                string item = Utility.CreateAutoCompleteItem(text, String.Join(delimiter, values));
                StringArray.SetValue(item, i);
            }
            return StringArray;
        }
        else
        {
            return null;
        }
    }

}