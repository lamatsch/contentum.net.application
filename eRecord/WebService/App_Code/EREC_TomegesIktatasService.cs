using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System.Xml;
using System.Text;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_TomegesIktatasService : System.Web.Services.WebService
{
    /// <summary>
    /// FILEPATH
    /// </summary>
    public static string CONST_DBF_ATCH_FILEPATH_COL_NAME = "FILEPATH";
    public static char[] CONST_DBF_ATCH_FILEPATH_SEPARATOR = new char[] { '|' };

    private ExecParam _execParam;
    #region CR3228 - ha egy f�jlban l�v� adatszerkezet nem megfelel�, akkor ne k�sz�ts�nk eredm�ny f�jlt
    private bool resultFileNeed = true;
    private int errCount = 0;
    #endregion
    private EREC_TomegesIktatas ServiceParams;

    #region GetAllWithFK
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_TomegesIktatasSearch))]
    public Result GetAllWithFK(ExecParam ExecParam, EREC_TomegesIktatasSearch _EREC_TomegesIktatasSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithFK(ExecParam, _EREC_TomegesIktatasSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    #region GetAlairasTipusok
    [WebMethod()]
    public Result GetAlairasTipusok(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAlairasTipusok(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    #region GetAlairasTipusokSzerepAlapjan
    [WebMethod()]
    public Result GetAlairasTipusokSzerepAlapjan(ExecParam ExecParam, string AlairoSzerep)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAlairasTipusokSzerepAlapjan(ExecParam, AlairoSzerep);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion GetAlairasTipusokSzerepAlapjan


    #region GetAlairoSzabylById
    [WebMethod()]
    public Result GetAlairoSzabylById(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAlairoSzabylById(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    #region InsertWithAlairasSzabaly
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_TomegesIktatas))]
    public Result Insert(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            if (Record.AlairasKell.Equals("1"))
            {
                if (string.IsNullOrEmpty(Record.AlairasMod)) throw new Exception("Nincs megadva az al��r�st�pus");
                if (string.IsNullOrEmpty(AlairoSzerep)) throw new Exception("Nincs megadva az al��r� szerep");
                using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
                {
                    sqlConnection.Open();
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandText = "select p.Id from KRT_AlairasSzabalyok p where p.AlairasTipus_Id=@alairasTipus and p.AlairoSzerep=@alairoSzerep and p.ErvKezd<=getdate() and (p.ErvVege>=getdate() or p.ErvVege is null)";
                        sqlCommand.Parameters.Add("@alairasTipus", SqlDbType.VarChar, 100, "alairasTipus").Value = Record.AlairasMod;
                        sqlCommand.Parameters.Add("@alairoSzerep", SqlDbType.VarChar, 100, "alairoSzerep").Value = AlairoSzerep;
                        var alairoSzabaly = sqlCommand.ExecuteScalar();
                        if (alairoSzabaly == null) throw new Exception("A megadott al��r�st�pushoz �s szerephez nem l�tezik al��r�sszab�ly.");
                        Record.AlairasSzabaly_Id = alairoSzabaly.ToString();
                        Record.Updated.AlairasSzabaly_Id = true;
                    }
                }
            }
            result = sp.Insert(Constants.Insert, ExecParam, Record);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    #region UpdateWithAlairasSzabaly
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_TomegesIktatas))]
    public Result Update(ExecParam ExecParam, EREC_TomegesIktatas Record, string AlairoSzerep)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            if (Record.AlairasKell.Equals("1"))
            {
                if (string.IsNullOrEmpty(Record.AlairasMod)) throw new Exception("Nincs megadva az al��r�st�pus");
                if (string.IsNullOrEmpty(AlairoSzerep)) throw new Exception("Nincs megadva az al��r� szerep");
                using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
                {
                    sqlConnection.Open();
                    using (var sqlCommand = new SqlCommand())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqlCommand.CommandText = "select p.Id from KRT_AlairasSzabalyok p where p.AlairasTipus_Id=@alairasTipus and p.AlairoSzerep=@alairoSzerep and p.ErvKezd<=getdate() and (p.ErvVege>=getdate() or p.ErvVege is null)";
                        sqlCommand.Parameters.Add("@alairasTipus", SqlDbType.VarChar, 100, "alairasTipus").Value = Record.AlairasMod;
                        sqlCommand.Parameters.Add("@alairoSzerep", SqlDbType.VarChar, 100, "alairoSzerep").Value = AlairoSzerep;
                        var alairoSzabaly = sqlCommand.ExecuteScalar();
                        if (alairoSzabaly == null) throw new Exception("A megadott al��r�st�pushoz �s szerephez nem l�tezik al��r�sszab�ly.");
                        Record.AlairasSzabaly_Id = alairoSzabaly.ToString();
                        Record.Updated.AlairasSzabaly_Id = true;
                    }
                }
            }
            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_TomegesIktatas", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    #region tomegesiktat�s service
    [WebMethod(Description = "T�meges iktat�s.")]
    public Result TomegesIktatas(ExecParam ExecParam, XmlDocument xmlDoc, string IktatoKonyv, string IratTipus/*forr�srendszer*/, string dbfFilePath, bool isElozmenyes)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(_execParam, Context, GetType());
        Logger.Info("T�meges iktat�s Start", _execParam);

        #region CR4141
        using (var serviceParams = eRecordService.ServiceFactory.GetEREC_TomegesIktatasService())
        {
            var _execParams = ExecParam.Clone();
            _execParams.Record_Id = IratTipus;
            var parameters = serviceParams.Get(_execParams);
            if (!parameters.IsError) ServiceParams = (EREC_TomegesIktatas)parameters.Record;
        }

        #endregion


        var outResult = new Result();
        _execParam = ExecParam;
        var ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        var ugyiratdarabService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
        var iratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        var iratHataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
        var iratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        string eMailMsg = "";

        if (xmlDoc != null && ServiceParams != null)
        {
            try
            {
                //f�sz�mos
                #region xml forr�s
                foreach (XmlNode root in xmlDoc.ChildNodes)
                {
                    //var res = new Result();
                    if (root.Name.Equals("iratok"))
                    {
                        #region Common
                        var resCsoport = getCsoport();
                        if (resCsoport == null || resCsoport.Record == null || resCsoport.IsError)
                        {
                            Logger.Error("A csoport beazonos�t�sa nem siker�lt.", _execParam, resCsoport);
                            eMailMsg += Environment.NewLine + "A csoport beazonos�t�sa nem siker�lt.";
                        }
                        var resAgazat = getAgazat();
                        if (resAgazat == null || resAgazat.Record == null || resAgazat.IsError)
                        {
                            Logger.Error("Az �gazat beazonos�t�sa nem siker�lt.", _execParam, resAgazat);
                            eMailMsg += Environment.NewLine + "Az �gazat beazonos�t�sa nem siker�lt.";
                        }
                        var resIrattar = getIrattar();
                        if (resIrattar == null || resIrattar.Record == null || resIrattar.IsError)
                        {
                            Logger.Error("Az iratt�r beazonos�t�sa nem siker�lt.", _execParam, resIrattar);
                            eMailMsg += Environment.NewLine + "Az iratt�r beazonos�t�sa nem siker�lt.";
                        }
                        var resUgytipus = getUgytipus();
                        if (resUgytipus == null || resUgytipus.Record == null || resUgytipus.IsError)
                        {
                            Logger.Error("Az �gyt�pus beazonos�t�sa nem siker�lt.", _execParam, resUgytipus);
                            eMailMsg += Environment.NewLine + "Az �gyt�pus beazonos�t�sa nem siker�lt.";
                        }
                        #endregion

                        int rowIndex = 0;
                        foreach (XmlNode dr in root.ChildNodes)
                        {
                            #region iratok iktat�sa
                            if (dr.Name.Equals("irat"))
                            {
                                string tempError = "";
                                try
                                {
                                    ++rowIndex;
                                    Logger.InfoStart(String.Format("A(z) {0}. sor iktat�sa: ", (rowIndex).ToString()));
                                    Logger.Info(String.Format("ADOZOAZON: {0}, NEV: {1}, IRSZ: {2}, UTCA: {3}, UGYTIPUS: {4}, ELOADO: {5}, UGYIRATSZ: {6}, ADOTARGY: {7}, SORSZ: {8}, NAPLO: {9}, HATSZAM: {10}",
                                         dr.ChildNodes[0].ToString().Trim(), dr.ChildNodes[1].ToString().Trim(), dr.ChildNodes[2].ToString().Trim(), dr.ChildNodes[3].ToString().Trim(),
                                         dr.ChildNodes[4].ToString().Trim(), dr.ChildNodes[5].ToString().Trim(), dr.ChildNodes[6].ToString().Trim(), dr.ChildNodes[7].ToString().Trim(),
                                         dr.ChildNodes[8].ToString().Trim(), dr.ChildNodes[9].ToString().Trim(), dr.ChildNodes[10].ToString().Trim()));

                                    if (dr.ChildNodes.Count == 12 && (dr.ChildNodes[11].ToString().Trim().Equals("*") || String.IsNullOrEmpty(dr.ChildNodes[6].ToString()))) continue;


                                    var resTelepules = new Result();
                                    var resPartnerek = new Result();
                                    var resKRT_PartnerCimek = new Result();
                                    #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                                    var resKRT_Cimek = new Result();
                                    #endregion
                                    var resUgyintezo = new Result();

                                    var execParamPartnerek = _execParam.Clone();
                                    var execParamUgyintezo = _execParam.Clone();
                                    var elozmeny = new Result();
                                    var ugyirat = new EREC_UgyUgyiratok();
                                    var irat = new EREC_IraIratok();
                                    var iratPeldanyok = new EREC_PldIratPeldanyok();
                                    string cimSTR = "";
                                    string nevSTR = "";
                                    foreach (XmlNode col in dr.ChildNodes)
                                    {

                                        #region telep�l�s/c�m
                                        if (col.Name.Equals("IRSZ"))
                                        {
                                            resTelepules = getTelepules(_execParam, col.InnerText.ToString().Trim());
                                            if (resTelepules.Ds.Tables[0].Rows.Count == 0)
                                            {
                                                Logger.Error("Az ir�ny�t�sz�m alapj�n a telep�l�s beazonost�sa nem siker�lt." + col.InnerText.ToString().Trim(), _execParam, resTelepules);
                                                tempError += "Az ir�ny�t�sz�m alapj�n a telep�l�s beazonost�sa nem siker�lt.";
                                            }
                                            else
                                            {
                                                cimSTR += col.InnerText.ToString().Trim() + " " + resTelepules.Ds.Tables[0].Rows[0]["Nev"].ToString();
                                            }
                                        }
                                        if (col.Name.Equals("UTCA"))
                                        {
                                            cimSTR += " " + col.InnerText.ToString().Trim();
                                        }
                                        #endregion telep�l�s/c�m
                                        #region N�v
                                        if (col.Name.Equals("NEV"))
                                        {
                                            nevSTR += col.InnerText.ToString().Trim();
                                        }
                                        #endregion
                                        #region getPartner/�gyf�l
                                        if (col.Name.Equals("UGYIRATSZ"))
                                        {
                                            #region getPartnerid/�gyf�l
                                            if (col.InnerText != null && !string.IsNullOrEmpty(col.InnerText.ToString()))
                                            {
                                                var partnerId = getPartnerId(col.InnerText.ToString().ToString().Replace("-", "").Trim());
                                                if (partnerId != null && !string.IsNullOrEmpty(partnerId.ToString()))
                                                {
                                                    Logger.Info(String.Format("Az al�bbi ad�azonos�t�/ad�sz�m-hoz: {0} tartoz� partnerid: {1}", col.InnerText.ToString().Replace("-", "").Trim(), partnerId.ToString()));
                                                    execParamPartnerek.Record_Id = partnerId.ToString();
                                                }
                                                else
                                                {
                                                    Logger.Error("A partner/�gyf�l meghat�roz�sa nem siker�lt! Az al�bbi ad�azonos�t�/ad�sz�m alapj�n: " + col.InnerText.ToString().Replace("-", "").Trim(), _execParam, resPartnerek);
                                                    tempError += "A partner/�gyf�l meghat�roz�sa nem siker�lt! Az al�bbi ad�azonos�t�/ad�sz�m alapj�n: " + col.InnerText.ToString().Replace("-", "").Trim();
                                                }
                                            }
                                            #endregion
                                            if (!string.IsNullOrEmpty(execParamPartnerek.Record_Id))
                                            {
                                                resPartnerek = getPartner(execParamPartnerek);
                                            }
                                            #region KRT_partnerCimRekord
                                            if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                            {
                                                resKRT_PartnerCimek = getKRT_PartnerCimek(((KRT_Partnerek)resPartnerek.Record).Id);
                                                if (resKRT_PartnerCimek.Ds.Tables[0].Rows.Count == 0)
                                                {

                                                    Logger.Error("A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id, _execParam, resKRT_PartnerCimek);
                                                    //tempError += "A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id;
                                                }
                                                else
                                                {
                                                    #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                                                    if (resKRT_PartnerCimek.Ds != null && resKRT_PartnerCimek.Ds.Tables != null && resKRT_PartnerCimek.Ds.Tables[0].Rows != null && !string.IsNullOrEmpty(resKRT_PartnerCimek.Ds.Tables[0].Rows[0]["Cim_Id"].ToString()))
                                                    {
                                                        resKRT_Cimek = getKRT_Cimek(resKRT_PartnerCimek.Ds.Tables[0].Rows[0]["Cim_Id"].ToString());
                                                        if (resKRT_Cimek.Ds.Tables[0].Rows.Count == 0)
                                                        {

                                                            Logger.Error("A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id, _execParam, resKRT_PartnerCimek);
                                                            //tempError += "A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Logger.Error("A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id, _execParam, resKRT_PartnerCimek);
                                                        //tempError += "A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id;
                                                    }
                                                    #endregion
                                                }
                                            }
                                            #endregion KRT_partnerCimRekord                                            
                                        }
                                        #endregion getPartner/�gyf�l
                                        #region �gyint�z�
                                        if (col.Name.Equals("ELOADO"))
                                        {
                                            #region getfelhasznaloid

                                            if (col.InnerText != null && !string.IsNullOrEmpty(col.InnerText.ToString()))
                                            {
                                                var felhasznaloId = getFelhasznaloId(col.InnerText.ToString().Trim());
                                                if (felhasznaloId != null && !string.IsNullOrEmpty(felhasznaloId.ToString()))
                                                {
                                                    Logger.Info(String.Format("Az al�bbi azonos�t�-hoz: {0} tartoz� felhaszn�l� Id: {1}", col.InnerText.ToString().Trim(), felhasznaloId.ToString()));
                                                    execParamUgyintezo.Record_Id = felhasznaloId.ToString();
                                                }
                                                else
                                                {
                                                    Logger.Error("Az �gyint�z�/El�ad� meghat�roz�sa nem siker�lt! Az al�bbi azonos�t� alapj�n: " + col.InnerText.ToString().Trim(), _execParam, resUgyintezo);
                                                    tempError += "Az �gyint�z�/El�ad� meghat�roz�sa nem siker�lt! Az al�bbi azonos�t� alapj�n: " + col.InnerText.ToString().Trim();
                                                }

                                            }
                                            #endregion getfelhasznaloid

                                            if (!string.IsNullOrEmpty(execParamUgyintezo.Record_Id))
                                            {
                                                resUgyintezo = getUgyintezo(execParamUgyintezo);
                                            }
                                        }
                                        #endregion �gyint�z�
                                        #region el�zm�ny keres�s, CR3172 param�ter, hogy kell-e el�zm�ny
                                        if (isElozmenyes)
                                        {
                                            if (col.Name.Equals("ADOTARGY"))
                                            {
                                                if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                                {
                                                    elozmeny = getElozmeny(IktatoKonyv, ((KRT_Partnerek)resPartnerek.Record).Id, ((EREC_IraIrattariTetelek)resIrattar.Record).Id, col.InnerText.ToString().Trim());
                                                    if (elozmeny.Ds.Tables[0].Rows.Count == 1)
                                                    {
                                                        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                                                            Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(
                                                                  new EREC_UgyUgyiratok()
                                                                  {
                                                                      Id = elozmeny.Ds.Tables[0].Rows[0]["Id"].ToString(),
                                                                      Allapot = elozmeny.Ds.Tables[0].Rows[0]["Allapot"].ToString(),
                                                                      FelhasznaloCsoport_Id_Orzo = elozmeny.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Orzo"].ToString(),
                                                                      TovabbitasAlattAllapot = elozmeny.Ds.Tables[0].Rows[0]["TovabbitasAlattAllapot"].ToString(),
                                                                      Csoport_Id_Felelos = elozmeny.Ds.Tables[0].Rows[0]["Csoport_Id_Felelos"].ToString(),
                                                                      Kovetkezo_Felelos_Id = elozmeny.Ds.Tables[0].Rows[0]["Kovetkezo_Felelos_Id"].ToString(),
                                                                      FelhasznaloCsoport_Id_Ugyintez = elozmeny.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Ugyintez"].ToString(),
                                                                      RegirendszerIktatoszam = elozmeny.Ds.Tables[0].Rows[0]["RegirendszerIktatoszam"].ToString(),
                                                                      LezarasDat = elozmeny.Ds.Tables[0].Rows[0]["LezarasDat"].ToString(),
                                                                      IraIktatokonyv_Id = elozmeny.Ds.Tables[0].Rows[0]["IraIktatokonyv_Id"].ToString(),
                                                                      Foszam = elozmeny.Ds.Tables[0].Rows[0]["Foszam"].ToString(),
                                                                      Jelleg = elozmeny.Ds.Tables[0].Rows[0]["Jelleg"].ToString()
                                                                  });
                                                        ErrorDetails errorDetail = new ErrorDetails();
                                                        if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(_execParam.Clone(), ugyiratStatusz, out errorDetail))
                                                        {
                                                            Logger.Info(String.Format("Az al�bbi param�terek alapj�n tal�lhat� el�zm�ny �s iktathat�: T�rgy: {0} tartoz� iktat�k�nyv id: {1}, partner id: {2}, iratt�ri t�tel id: {3}, el�zm�ny id: {4}", col.InnerText.ToString().Trim(), IktatoKonyv, ((KRT_Partnerek)resPartnerek.Record).Id, ((EREC_IraIrattariTetelek)resIrattar.Record).Id, elozmeny.Ds.Tables[0].Rows[0]["Id"].ToString()));
                                                            irat.Targy = ServiceParams.TargyPrefix + col.InnerText.ToString().Trim();
                                                        }
                                                        else
                                                        {
                                                            #region CR3290
                                                            tempError += String.Format("Az �gyirathoz kapcsol�d�an tal�lhat� el�zm�ny viszont nem iktathat� alsz�mra: T�rgy: {0} {1}\tHiba: {2}", dr["ADOTARGY"].ToString().Trim(), Environment.NewLine, errorDetail.Message);
                                                            #endregion
                                                            elozmeny.Ds = null;
                                                            ugyirat.Targy = ServiceParams.TargyPrefix + col.InnerText.ToString().Trim();
                                                            irat.Targy = ServiceParams.TargyPrefix + col.InnerText.ToString().Trim();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        elozmeny.Ds = null;
                                                        ugyirat.Targy = ServiceParams.TargyPrefix + col.InnerText.ToString().Trim();
                                                        irat.Targy = ServiceParams.TargyPrefix + col.InnerText.ToString().Trim();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion el�zm�ny keres�s
                                    }


                                    var res = new Result();
                                    var ugyiratDarabok = new EREC_UgyUgyiratdarabok();
                                    var iratHataridosFeladatok = new EREC_HataridosFeladatok();
                                    var iktatasiParameterek = new IktatasiParameterek();

                                    #region �gyirat
                                    if (elozmeny.Ds == null || elozmeny.Ds.Tables == null || elozmeny.Ds.Tables[0].Rows.Count != 1)
                                    {
                                        ugyirat.IraIktatokonyv_Id = IktatoKonyv;
                                        ugyirat.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                                        ugyirat.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;

                                        ugyirat.UgyintezesModja = "0";
                                        ugyirat.UgyTipus = ((EREC_IratMetaDefinicio)resUgytipus.Record).Ugytipus;
                                        ugyirat.Hatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();
                                        if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                            ugyirat.Partner_Id_Ugyindito = ((KRT_Partnerek)resPartnerek.Record).Id;
                                        //if (resKRT_PartnerCimek.Ds != null && resKRT_PartnerCimek.Ds.Tables != null && resKRT_PartnerCimek.Ds.Tables[0].Rows != null)
                                        //    ugyirat.Cim_Id_Ugyindito = resKRT_PartnerCimek.Ds.Tables[0].Rows[0]["Id"].ToString();
                                        if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Nev))
                                            ugyirat.NevSTR_Ugyindito = ((KRT_Partnerek)resPartnerek.Record).Nev;
                                        else
                                            ugyirat.NevSTR_Ugyindito = nevSTR;
                                        #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                                        if (resKRT_Cimek.Ds != null && resKRT_Cimek.Ds.Tables != null && resKRT_Cimek.Ds.Tables[0].Rows != null && !String.IsNullOrEmpty(resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString()))
                                        {
                                            ugyirat.Cim_Id_Ugyindito = resKRT_Cimek.Ds.Tables[0].Rows[0]["Id"].ToString();
                                            ugyirat.CimSTR_Ugyindito = resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString();
                                        }
                                        else
                                            ugyirat.CimSTR_Ugyindito = cimSTR.ToUpper();
                                        #endregion
                                        ugyirat.FelhasznaloCsoport_Id_Ugyintez = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                        ugyirat.IraIrattariTetel_Id = ((EREC_IraIrattariTetelek)resIrattar.Record).Id;
                                        ugyirat.Csoport_Id_Cimzett = ((KRT_Partnerek)resPartnerek.Record).Id;
                                        ugyirat.Csoport_Id_Ugyfelelos = ((KRT_Csoportok)resCsoport.Record).Id;
                                        ugyirat.Jelleg = KodTarak.UGYIRAT_JELLEG.Papir;
                                    }
                                    #endregion �gyirat
                                    #region �gyiratDarab
                                    ugyiratDarabok.IraIktatokonyv_Id = IktatoKonyv;
                                    ugyiratDarabok.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                                    ugyiratDarabok.FelhasznaloCsoport_Id_Ugyintez = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                    ugyiratDarabok.Hatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();

                                    #endregion

                                    #region irat
                                    irat.AdathordozoTipusa = "0";
                                    irat.KiadmanyozniKell = "1";


                                    irat.Irattipus = getIrattipus();
                                    irat.UgyintezesModja = KodTarak.IRAT_UGYINT_MODJA.Szemelyes;
                                    irat.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                                    irat.Csoport_Id_Ugyfelelos = ((KRT_Csoportok)resCsoport.Record).Id;
                                    irat.FelhasznaloCsoport_Id_Ugyintez = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                    irat.Munkaallomas = _execParam.UserHostAddress;
                                    irat.Hatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();
                                    irat.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                    irat.Csoport_Id_Ugyfelelos = ((KRT_Csoportok)resCsoport.Record).Id;
                                    #endregion

                                    #region Iratp�ld�nyok
                                    if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                        iratPeldanyok.Partner_Id_Cimzett = ((KRT_Partnerek)resPartnerek.Record).Id;
                                    #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                                    if (resKRT_Cimek.Ds != null && resKRT_Cimek.Ds.Tables != null && resKRT_Cimek.Ds.Tables[0].Rows != null && !String.IsNullOrEmpty(resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString()))
                                    {
                                        iratPeldanyok.Cim_id_Cimzett = resKRT_Cimek.Ds.Tables[0].Rows[0]["Id"].ToString();
                                        iratPeldanyok.CimSTR_Cimzett = resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString();
                                    }
                                    else
                                        iratPeldanyok.CimSTR_Cimzett = cimSTR.ToUpper();
                                    #endregion
                                    iratPeldanyok.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                                    iratPeldanyok.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                    if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Nev))
                                        iratPeldanyok.NevSTR_Cimzett = ((KRT_Partnerek)resPartnerek.Record).Nev;
                                    else
                                        iratPeldanyok.NevSTR_Cimzett = nevSTR;
                                    iratPeldanyok.VisszaerkezesiHatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();
                                    iratPeldanyok.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                    iratPeldanyok.KuldesMod = KodTarak.KULD_KEZB_MODJA.posta;
                                    #endregion

                                    #region iktat�si param�terek
                                    iktatasiParameterek.Atiktatas = false;
                                    iktatasiParameterek.IratpeldanyVonalkodGeneralasHaNincs = false;
                                    iktatasiParameterek.KeszitoPeldanyaSzukseges = false;
                                    iktatasiParameterek.MunkaPeldany = false;
                                    iktatasiParameterek.UgyiratPeldanySzukseges = false;
                                    iktatasiParameterek.UgyiratUjranyitasaHaLezart = true;

                                    #endregion
                                    if (elozmeny.Ds != null && elozmeny.Ds.Tables != null && elozmeny.Ds.Tables[0].Rows.Count == 1)
                                        res = iratService.BelsoIratIktatasa_Alszamra(_execParam, IktatoKonyv, elozmeny.Ds.Tables[0].Rows[0]["Id"].ToString(), ugyiratDarabok, irat, iratHataridosFeladatok, iratPeldanyok, iktatasiParameterek);
                                    else
                                        res = iratService.BelsoIratIktatasa(_execParam, IktatoKonyv, ugyirat, ugyiratDarabok, irat, iratHataridosFeladatok, iratPeldanyok, iktatasiParameterek);


                                    if (String.IsNullOrEmpty(res.ErrorCode))
                                    {
                                        var execparamIrat = _execParam.Clone();
                                        execparamIrat.Record_Id = res.Uid;
                                        var resultIrat = iratService.Get(execparamIrat);

                                        var execparamUgyirat = _execParam.Clone();
                                        execparamUgyirat.Record_Id = ((EREC_IraIratok)resultIrat.Record).Ugyirat_Id;
                                        var resultUgyirat = ugyiratService.Get(execparamUgyirat);

                                        #region CR3141 - CR3138 - T�meges iktat�s (�nkad�) bet�lt�tt t�telekhez kapcsol�d�an
                                        if (ServiceParams.AlairasKell.Equals("1"))
                                        {
                                            var resultAlairo = SignatoryCreate((EREC_IraIratok)resultIrat.Record);
                                            if (resultAlairo != null && !resultAlairo.IsError && !String.IsNullOrEmpty(resultAlairo.Uid))
                                            {
                                                Logger.Info(String.Format("Az irat-hoz: {0} l�trej�tt EREC_IratAlairok rekord id-ja: {1}", res.Uid, resultAlairo.Uid));
                                            }
                                            else
                                            {
                                                Logger.Error("Az irathoz az al��r� rekord l�trehoz�sa nem siker�lt!", _execParam, resultAlairo);
                                                tempError += Environment.NewLine + "\tAz irathoz az al��r� rekord l�trehoz�sa nem siker�lt!";
                                            }
                                        }
                                        if (ServiceParams.HatosagiAdatlapKell.Equals("1"))
                                        {
                                            var resultHatosag = CommuneCreate((EREC_IraIratok)resultIrat.Record);
                                            if (resultHatosag != null && !resultHatosag.IsError)
                                            {
                                                Logger.Info(String.Format("Az irat-hoz: {0} l�trej�tt EREC_IraOnkormAdatok rekord id-ja: {1}", res.Uid, String.IsNullOrEmpty(resultHatosag.Uid) ? res.Uid : resultHatosag.Uid));
                                            }
                                            else
                                            {
                                                Logger.Error("Az irathoz a hat�s�gi adatlap rekord l�trehoz�sa nem siker�lt!", _execParam, resultHatosag);
                                                tempError += Environment.NewLine + "\tAz irathoz a hat�s�gi adatlap rekord l�trehoz�sa nem siker�lt!";
                                            }
                                        }
                                        #endregion
                                        #region Expedi�l�s
                                        if (((EREC_IraIratok)resultIrat.Record).Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott || ServiceParams.AutoExpedialasKell.Equals("1"))
                                        {
                                            var resultExpedialas = Expedialas(((EREC_IraIratok)resultIrat.Record).Id);
                                            if (resultExpedialas != null && !resultExpedialas.IsError)
                                            {
                                                Logger.Info(String.Format("Az irat-hoz: {0} tartoz� iratp�ld�ny expedi�l�sa megt�rt�nt.", res.Uid));
                                            }
                                            else
                                            {
                                                Logger.Error("Az irathoz tartoz� iratp�ld�ny expedi�l�sa nem siker�lt!", _execParam, resultExpedialas);
                                                tempError += Environment.NewLine + "\tAz irathoz tartoz� iratp�ld�ny expedi�l�sa nem siker�lt!";
                                            }
                                        }
                                        #endregion

                                        //xml kieg�sz�t�se
                                        XmlNode resultNode = dr.OwnerDocument.DocumentElement;
                                        foreach (XmlNode col in dr.ChildNodes)
                                        {
                                            if (col.Name.Equals("UGYIRATSZ"))
                                                col.InnerText = ((EREC_UgyUgyiratok)resultUgyirat.Record).Azonosito.Replace(" ", "");
                                        }
                                        XmlElement elem = dr.OwnerDocument.CreateElement("IKTATVA");
                                        elem.InnerText = "*";
                                        dr.AppendChild(elem);
                                        Logger.Info("A " + rowIndex + ". sorban, l�trej�tt irat: " + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\tAzonos�t�: " + ((EREC_IraIratok)resultIrat.Record).Azonosito.Replace(" ", ""));
                                        eMailMsg += Environment.NewLine + "A " + rowIndex + ". sorban, l�trej�tt irat:" + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\tAzonos�t�: " + ((EREC_IraIratok)resultIrat.Record).Azonosito.Replace(" ", "") + Environment.NewLine;
                                    }
                                    else
                                    {
                                        //xml kieg�sz�t�se
                                        XmlNode resultNode = dr.OwnerDocument.DocumentElement;
                                        foreach (XmlNode col in dr.ChildNodes)
                                        {
                                            if (col.Name.Equals("UGYIRATSZ"))
                                                col.InnerText = "";
                                        }
                                        XmlElement elem = dr.OwnerDocument.CreateElement("IKTATVA");
                                        elem.InnerText = "";
                                        dr.AppendChild(elem);

                                        //CR3228 
                                        ++errCount;
                                        Logger.Info("A " + rowIndex + ". sorban. Nem j�tt l�tre irat: " + res.ErrorMessage);
                                        #region CR3290
                                        eMailMsg += Environment.NewLine + "A " + rowIndex + ". sorban, nem j�tt l�tre irat:" + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\t" + getErrorMessageByErrorCode(res.ErrorMessage) + Environment.NewLine;
                                        #endregion
                                    }
                                    outResult = res;
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(Environment.NewLine + "Hiba: A " + rowIndex + ". sorban." + tempError + ex.Message);
                                    //xml kieg�sz�t�se
                                    XmlNode resultNode = dr.OwnerDocument.DocumentElement;
                                    foreach (XmlNode col in dr.ChildNodes)
                                    {
                                        if (col.Name.Equals("UGYIRATSZ"))
                                            col.InnerText = "";
                                    }

                                    XmlElement elem = dr.OwnerDocument.CreateElement("IKTATVA");
                                    elem.InnerText = "";
                                    dr.AppendChild(elem);

                                    //CR3228 
                                    ++errCount;
                                    eMailMsg += Environment.NewLine + "Hiba: A " + rowIndex + ". sorban." + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\t" + ex.Message + Environment.NewLine;
                                }
                                Logger.InfoEnd(String.Format("A(z) {0}. sor iktat�sa:", (rowIndex).ToString()));

                            }
                            #endregion iratok iktat�sa
                        }
                        //CR3228 
                        if (errCount == root.ChildNodes.Count)
                        {
                            resultFileNeed = false;
                        }
                    }


                    #endregion
                }

            }
            catch (Exception ex)
            {
                #region CR3228 - ha egy f�jlban l�v� adatszerkezet nem megfelel�, akkor ne k�sz�ts�nk eredm�ny f�jlt
                resultFileNeed = false;
                #endregion
                Logger.Error("Ismeretlen hiba a folyamat sikertelen." + ex.Message);
                eMailMsg += "" + Environment.NewLine + " Ismeretlen hiba a folyamat sikertelen." + ex.Message + Environment.NewLine;
            }
        }
        else if (xmlDoc == null && ServiceParams != null)
        {
            #region DBF forr�s
            DataSet dataset = new DataSet();
            dbfHandler DBFclass = new dbfHandler();
            try
            {
                String[] splitString = dbfFilePath.Split('\\');
                DBFclass.fileName = splitString[splitString.Length - 1];
                splitString = splitString.Where(w => w != splitString[splitString.Length - 1]).ToArray();
                DBFclass.folderPath = String.Join("\\", splitString);

                DBFclass.connectionString =
                     @"Provider=VFPOLEDB.1;Data Source=" + DBFclass.folderPath + ";Collating Sequence=machine;";
                dataset = DBFclass.LoadDBF();

                #region Common
                var resCsoport = getCsoport();
                if (resCsoport == null || resCsoport.Record == null || resCsoport.IsError)
                {
                    Logger.Error("A csoport beazonos�t�sa nem siker�lt.", _execParam, resCsoport);
                    eMailMsg += Environment.NewLine + "A csoport beazonos�t�sa nem siker�lt.";
                }
                var resAgazat = getAgazat();
                if (resAgazat == null || resAgazat.Record == null || resAgazat.IsError)
                {
                    Logger.Error("Az �gazat beazonos�t�sa nem siker�lt.", _execParam, resAgazat);
                    eMailMsg += Environment.NewLine + "Az �gazat beazonos�t�sa nem siker�lt.";
                }
                var resIrattar = getIrattar();
                if (resIrattar == null || resIrattar.Record == null || resIrattar.IsError)
                {
                    Logger.Error("Az iratt�r beazonos�t�sa nem siker�lt.", _execParam, resIrattar);
                    eMailMsg += Environment.NewLine + "Az iratt�r beazonos�t�sa nem siker�lt.";
                }
                var resUgytipus = getUgytipus();
                if (resUgytipus == null || resUgytipus.Record == null || resUgytipus.IsError)
                {
                    Logger.Error("Az �gyt�pus beazonos�t�sa nem siker�lt.", _execParam, resUgytipus);
                    eMailMsg += Environment.NewLine + "Az �gyt�pus beazonos�t�sa nem siker�lt.";
                }
                #endregion

                int rowIndex = 0;

                #region iratok iktat�sa
                string iratId = null;
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    string tempError = "";
                    try
                    {
                        ++rowIndex;
                        Logger.InfoStart(String.Format("A(z) {0}. sor iktat�sa: ", (rowIndex).ToString()));
                        if (dataset.Tables[0].Columns.Contains(CONST_DBF_ATCH_FILEPATH_COL_NAME))
                            Logger.Info(String.Format("ADOZOAZON: {0}, NEV: {1}, IRSZ: {2}, UTCA: {3}, UGYTIPUS: {4}, ELOADO: {5}, UGYIRATSZ: {6}, IKTATVA: {7}, ADOTARGY: {8}, SORSZ: {9}, NAPLO: {10}, HATSZAM: {11}, FILEPATH: {12}",
                                 dr[0].ToString().Trim(), dr[1].ToString().Trim(), dr[2].ToString().Trim(), dr[3].ToString().Trim(),
                                 dr[4].ToString().Trim(), dr[5].ToString().Trim(), dr[6].ToString().Trim(), dr[7] == null ? "" : dr[7].ToString().Trim(), dr[8].ToString().Trim(),
                                 dr[9].ToString().Trim(), dr[10].ToString().Trim(), dr[11].ToString().Trim(),
                                 dr[12].ToString().Trim()));
                        else
                            Logger.Info(String.Format("ADOZOAZON: {0}, NEV: {1}, IRSZ: {2}, UTCA: {3}, UGYTIPUS: {4}, ELOADO: {5}, UGYIRATSZ: {6}, IKTATVA: {7}, ADOTARGY: {8}, SORSZ: {9}, NAPLO: {10}, HATSZAM: {11}",
                                  dr[0].ToString().Trim(), dr[1].ToString().Trim(), dr[2].ToString().Trim(), dr[3].ToString().Trim(),
                                  dr[4].ToString().Trim(), dr[5].ToString().Trim(), dr[6].ToString().Trim(), dr[7] == null ? "" : dr[7].ToString().Trim(), dr[8].ToString().Trim(),
                                  dr[9].ToString().Trim(), dr[10].ToString().Trim(), dr[11].ToString().Trim()));

                        //if (dr[7] != null && dr[7].ToString().Trim().Equals("*") || String.IsNullOrEmpty(dr[6].ToString())) continue;

                        if (!dr["IKTATVA"].Equals("*"))
                        {
                            #region IKTATAS
                            var resTelepules = new Result();
                            var resPartnerek = new Result();
                            var resKRT_PartnerCimek = new Result();
                            #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                            var resKRT_Cimek = new Result();
                            #endregion
                            var resUgyintezo = new Result();

                            var execParamPartnerek = _execParam.Clone();
                            var execParamUgyintezo = _execParam.Clone();

                            #region telep�l�s
                            //ha az IRSZ nincs kit�ltve, akkor nem keres�nk hozz� telep�l�st
                            if (!String.IsNullOrEmpty(dr["IRSZ"].ToString().Trim()))
                            {
                                resTelepules = getTelepules(_execParam, dr["IRSZ"].ToString().Trim());
                                if (resTelepules.Ds.Tables[0].Rows.Count == 0)
                                {
                                    Logger.Error("Az ir�ny�t�sz�m alapj�n a telep�l�s beazonost�sa nem siker�lt." + dr["IRSZ"].ToString().Trim(), _execParam, resTelepules);
                                    tempError += "" + Environment.NewLine + "Az ir�ny�t�sz�m alapj�n a telep�l�s beazonost�sa nem siker�lt.";
                                }
                            }
                            #endregion telep�l�s
                            #region getPartner / �gyf�l
                            if (dr["UGYIRATSZ"] != null && !string.IsNullOrEmpty(dr["UGYIRATSZ"].ToString()))
                            {
                                var partnerId = getPartnerId(dr["UGYIRATSZ"].ToString().ToString().Replace("-", "").Trim());
                                if (partnerId != null && !string.IsNullOrEmpty(partnerId.ToString()))
                                {
                                    Logger.Info(String.Format("Az al�bbi ad�azonos�t�/ad�sz�m-hoz: {0} tartoz� partnerid: {1}", dr["UGYIRATSZ"].ToString().Replace("-", "").Trim(), partnerId.ToString()));
                                    execParamPartnerek.Record_Id = partnerId.ToString();
                                }
                                else
                                {
                                    Logger.Error("A partner/�gyf�l meghat�roz�sa nem siker�lt! Az al�bbi ad�azonos�t�/ad�sz�m alapj�n: " + dr["UGYIRATSZ"].ToString().Replace("-", "").Trim(), _execParam, resPartnerek);
                                    tempError += Environment.NewLine + "\tA partner/�gyf�l meghat�roz�sa nem siker�lt! Az al�bbi ad�azonos�t�/ad�sz�m alapj�n: " + dr["UGYIRATSZ"].ToString().Replace("-", "").Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(execParamPartnerek.Record_Id))
                            {
                                resPartnerek = getPartner(execParamPartnerek);
                            }
                            #endregion getPartner / �gyf�l
                            #region KRT_partnerCimRekord
                            if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                            {
                                resKRT_PartnerCimek = getKRT_PartnerCimek(((KRT_Partnerek)resPartnerek.Record).Id);
                                if (resKRT_PartnerCimek.Ds.Tables[0].Rows.Count == 0)
                                {

                                    Logger.Error("A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id, _execParam, resKRT_PartnerCimek);
                                    //tempError += Environment.NewLine +"A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id;
                                }
                                else
                                {
                                    #region CR3202 - T�meges iktat�s, ha van a partnerhez KRT_cimek rekord, azt k�ne bet�lteni
                                    if (resKRT_PartnerCimek.Ds != null && resKRT_PartnerCimek.Ds.Tables != null && resKRT_PartnerCimek.Ds.Tables[0].Rows != null && !string.IsNullOrEmpty(resKRT_PartnerCimek.Ds.Tables[0].Rows[0]["Cim_Id"].ToString()))
                                    {
                                        resKRT_Cimek = getKRT_Cimek(resKRT_PartnerCimek.Ds.Tables[0].Rows[0]["Cim_Id"].ToString());
                                        if (resKRT_Cimek.Ds.Tables[0].Rows.Count == 0)
                                        {

                                            Logger.Error("A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id, _execParam, resKRT_PartnerCimek);
                                            //tempError += "A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id;
                                        }
                                    }
                                    else
                                    {
                                        Logger.Error("A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id, _execParam, resKRT_PartnerCimek);
                                        //tempError += "A partner alapj�n a c�m meghat�roz�sa nem siker�lt! Az al�bbi partnerId alapj�n: " + ((KRT_Partnerek)resPartnerek.Record).Id;
                                    }
                                    #endregion
                                }
                            }
                            #endregion KRT_partnerCimRekord
                            #region �gyint�z�
                            #region getfelhasznaloid
                            if (dr["ELOADO"] != null && !string.IsNullOrEmpty(dr["ELOADO"].ToString()))
                            {
                                var felhasznaloId = getFelhasznaloId(dr["ELOADO"].ToString().Trim());
                                if (felhasznaloId != null && !string.IsNullOrEmpty(felhasznaloId.ToString()))
                                {
                                    Logger.Info(String.Format("Az al�bbi azonos�t�-hoz: {0} tartoz� felhaszn�l� Id: {1}", dr["ELOADO"].ToString().Trim(), felhasznaloId.ToString()));
                                    execParamUgyintezo.Record_Id = felhasznaloId.ToString();
                                }
                                else
                                {
                                    Logger.Error("Az �gyint�z�/El�ad� meghat�roz�sa nem siker�lt! Az al�bbi azonos�t� alapj�n: " + dr["ELOADO"].ToString().Trim(), _execParam, resUgyintezo);
                                    tempError += Environment.NewLine + "\tAz �gyint�z�/El�ad� meghat�roz�sa nem siker�lt! Az al�bbi azonos�t� alapj�n: " + dr["ELOADO"].ToString().Trim();
                                }

                            }
                            #endregion getfelhasznaloid
                            if (!string.IsNullOrEmpty(execParamUgyintezo.Record_Id))
                            {
                                resUgyintezo = getUgyintezo(execParamUgyintezo);
                            }

                            #endregion �gyint�z�

                            var res = new Result();
                            var ugyirat = new EREC_UgyUgyiratok();
                            var ugyiratDarabok = new EREC_UgyUgyiratdarabok();
                            var irat = new EREC_IraIratok();
                            var iratHataridosFeladatok = new EREC_HataridosFeladatok();
                            var iratPeldanyok = new EREC_PldIratPeldanyok();
                            var iktatasiParameterek = new IktatasiParameterek();
                            var elozmeny = new Result();
                            #region �gyirat

                            #region el�zm�ny keres�s, CR3172 param�ter, hogy kell-e el�zm�ny
                            if (isElozmenyes)
                            {
                                if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                {
                                    elozmeny = getElozmeny(IktatoKonyv, ((KRT_Partnerek)resPartnerek.Record).Id, ((EREC_IraIrattariTetelek)resIrattar.Record).Id, dr["ADOTARGY"].ToString().Trim());
                                    if (elozmeny.Ds.Tables[0].Rows.Count == 1)
                                    {
                                        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                                             Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(
                                                    new EREC_UgyUgyiratok()
                                                    {
                                                        Id = elozmeny.Ds.Tables[0].Rows[0]["Id"].ToString(),
                                                        Allapot = elozmeny.Ds.Tables[0].Rows[0]["Allapot"].ToString(),
                                                        FelhasznaloCsoport_Id_Orzo = elozmeny.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Orzo"].ToString(),
                                                        TovabbitasAlattAllapot = elozmeny.Ds.Tables[0].Rows[0]["TovabbitasAlattAllapot"].ToString(),
                                                        Csoport_Id_Felelos = elozmeny.Ds.Tables[0].Rows[0]["Csoport_Id_Felelos"].ToString(),
                                                        Kovetkezo_Felelos_Id = elozmeny.Ds.Tables[0].Rows[0]["Kovetkezo_Felelos_Id"].ToString(),
                                                        FelhasznaloCsoport_Id_Ugyintez = elozmeny.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Ugyintez"].ToString(),
                                                        RegirendszerIktatoszam = elozmeny.Ds.Tables[0].Rows[0]["RegirendszerIktatoszam"].ToString(),
                                                        LezarasDat = elozmeny.Ds.Tables[0].Rows[0]["LezarasDat"].ToString(),
                                                        IraIktatokonyv_Id = elozmeny.Ds.Tables[0].Rows[0]["IraIktatokonyv_Id"].ToString(),
                                                        Foszam = elozmeny.Ds.Tables[0].Rows[0]["Foszam"].ToString(),
                                                        Jelleg = elozmeny.Ds.Tables[0].Rows[0]["Jelleg"].ToString()
                                                    });
                                        ErrorDetails errorDetail = new ErrorDetails();
                                        if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(_execParam.Clone(), ugyiratStatusz, out errorDetail))
                                        {
                                            Logger.Info(String.Format("Az al�bbi param�terek alapj�n tal�lhat� el�zm�ny �s iktathat� alsz�mra: T�rgy: {0} tartoz� iktat�k�nyv id: {1}, partner id: {2}, iratt�ri t�tel id: {3}, el�zm�ny id: {4}", dr["ADOTARGY"].ToString().Trim(), IktatoKonyv, ((KRT_Partnerek)resPartnerek.Record).Id, ((EREC_IraIrattariTetelek)resIrattar.Record).Id, elozmeny.Ds.Tables[0].Rows[0]["Id"].ToString()));
                                        }
                                        else
                                        {
                                            #region CR3290
                                            tempError += String.Format("Az �gyirathoz kapcsol�d�an tal�lhat� el�zm�ny viszont nem iktathat� alsz�mra: T�rgy: {0} {1}\tHiba: {2}", dr["ADOTARGY"].ToString().Trim(), Environment.NewLine, errorDetail.Message);
                                            #endregion
                                            elozmeny.Ds = null;
                                        }
                                    }
                                }
                            }
                            #endregion �gyirat
                            if (elozmeny.Ds == null || elozmeny.Ds.Tables == null || elozmeny.Ds.Tables[0].Rows.Count != 1)
                            {
                                ugyirat.IraIktatokonyv_Id = IktatoKonyv;
                                ugyirat.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                                ugyirat.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                ugyirat.Targy = ServiceParams.TargyPrefix + dr["ADOTARGY"].ToString().Trim();
                                ugyirat.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir;
                                ugyirat.UgyTipus = ((EREC_IratMetaDefinicio)resUgytipus.Record).Ugytipus;
                                ugyirat.Hatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();
                                if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                    ugyirat.Partner_Id_Ugyindito = ((KRT_Partnerek)resPartnerek.Record).Id;
                                //if (resKRT_PartnerCimek.Ds != null && resKRT_PartnerCimek.Ds.Tables != null && resKRT_PartnerCimek.Ds.Tables[0].Rows != null && resKRT_PartnerCimek.Ds.Tables[0].Rows.Count == 1)
                                //    ugyirat.Cim_Id_Ugyindito = resKRT_PartnerCimek.Ds.Tables[0].Rows[0]["Id"].ToString();
                                if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Nev))
                                    ugyirat.NevSTR_Ugyindito = ((KRT_Partnerek)resPartnerek.Record).Nev;
                                else
                                    ugyirat.NevSTR_Ugyindito = convertString852toUTF8(dr["NEV"].ToString().Trim());
                                #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                                if (resKRT_Cimek.Ds != null && resKRT_Cimek.Ds.Tables != null && resKRT_Cimek.Ds.Tables[0].Rows != null && !String.IsNullOrEmpty(resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString()))
                                {
                                    ugyirat.Cim_Id_Ugyindito = resKRT_Cimek.Ds.Tables[0].Rows[0]["Id"].ToString();
                                    ugyirat.CimSTR_Ugyindito = resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString();
                                }
                                //ha az IRSZ nincs kit�ltve, de az UTCA igen, akkor felt�telezz�k, hogy a teljes c�m (pl. Hivatali Kapu c�m) az UTCA mez?ben van
                                else if (String.IsNullOrEmpty(dr["IRSZ"].ToString().Trim()) && !String.IsNullOrEmpty(dr["UTCA"].ToString().Trim()))
                                {
                                    ugyirat.CimSTR_Ugyindito = dr["UTCA"].ToString().Trim();
                                }
                                else
                                    ugyirat.CimSTR_Ugyindito = dr["IRSZ"].ToString().Trim() + " " + resTelepules.Ds.Tables[0].Rows[0]["Nev"].ToString().ToUpper() + " " + convertString852toUTF8(dr["UTCA"].ToString().Trim());
                                #endregion


                                ugyirat.FelhasznaloCsoport_Id_Ugyintez = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                                ugyirat.IraIrattariTetel_Id = ((EREC_IraIrattariTetelek)resIrattar.Record).Id;
                                if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                    ugyirat.Csoport_Id_Cimzett = ((KRT_Partnerek)resPartnerek.Record).Id;
                                ugyirat.Csoport_Id_Ugyfelelos = ((KRT_Csoportok)resCsoport.Record).Id;
                                ugyirat.Jelleg = KodTarak.UGYIRAT_JELLEG.Papir;

                            }

                            #endregion �gyirat

                            #region �gyiratDarab
                            ugyiratDarabok.IraIktatokonyv_Id = IktatoKonyv;
                            ugyiratDarabok.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                            ugyiratDarabok.FelhasznaloCsoport_Id_Ugyintez = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                            ugyiratDarabok.Hatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();

                            #endregion

                            #region irat

                            #region LZS
                            //irat.AdathordozoTipusa = "0";
                            irat.AdathordozoTipusa = ServiceParams.AdathordozoTipusa;
                            irat.UgyintezesAlapja = ServiceParams.UgyintezesAlapja;
                            #endregion

                            irat.KiadmanyozniKell = "1";
                            irat.Targy = ServiceParams.TargyPrefix + dr["ADOTARGY"].ToString().Trim();
                            irat.Irattipus = getIrattipus();//KodTarak.IRATTIPUS.Hatarozat;
                            irat.UgyintezesModja = KodTarak.IRAT_UGYINT_MODJA.Szemelyes;
                            irat.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                            irat.Csoport_Id_Ugyfelelos = ((KRT_Csoportok)resCsoport.Record).Id;
                            irat.FelhasznaloCsoport_Id_Ugyintez = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                            irat.Munkaallomas = ExecParam.UserHostAddress;
                            irat.Hatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();
                            irat.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;

                            #endregion

                            #region Iratp�ld�nyok
                            if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Id))
                                iratPeldanyok.Partner_Id_Cimzett = ((KRT_Partnerek)resPartnerek.Record).Id;
                            #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
                            if (resKRT_Cimek.Ds != null && resKRT_Cimek.Ds.Tables != null && resKRT_Cimek.Ds.Tables[0].Rows != null && !String.IsNullOrEmpty(resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString()))
                            {
                                iratPeldanyok.Cim_id_Cimzett = resKRT_Cimek.Ds.Tables[0].Rows[0]["Id"].ToString();
                                iratPeldanyok.CimSTR_Cimzett = resKRT_Cimek.Ds.Tables[0].Rows[0]["Nev"].ToString();
                            }
                            else if (String.IsNullOrEmpty(dr["IRSZ"].ToString().Trim()) && !String.IsNullOrEmpty(dr["UTCA"].ToString().Trim()))
                            {
                                iratPeldanyok.CimSTR_Cimzett = dr["UTCA"].ToString().Trim();
                            }
                            else
                                iratPeldanyok.CimSTR_Cimzett = dr["IRSZ"].ToString().Trim() + " " + resTelepules.Ds.Tables[0].Rows[0]["Nev"].ToString().ToUpper() + " " + convertString852toUTF8(dr["UTCA"].ToString().Trim());
                            #endregion

                            iratPeldanyok.Csoport_Id_Felelos = _execParam.Felhasznalo_Id;
                            iratPeldanyok.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                            if (resPartnerek != null && resPartnerek.Record != null && !String.IsNullOrEmpty(((KRT_Partnerek)resPartnerek.Record).Nev))
                                iratPeldanyok.NevSTR_Cimzett = ((KRT_Partnerek)resPartnerek.Record).Nev;
                            else
                                iratPeldanyok.NevSTR_Cimzett = convertString852toUTF8(dr["NEV"].ToString().Trim());

                            iratPeldanyok.VisszaerkezesiHatarido = DateTime.Now.AddDays(Convert.ToDouble(((EREC_IratMetaDefinicio)resUgytipus.Record).UgyiratIntezesiIdo)).ToString();
                            iratPeldanyok.FelhasznaloCsoport_Id_Orzo = ((KRT_Felhasznalok)resUgyintezo.Record).Id;
                            //iratPeldanyok.KuldesMod = KodTarak.KULD_KEZB_MODJA.posta;
                            #region LZS
                            iratPeldanyok.KuldesMod = ServiceParams.ExpedialasModja;
                            #endregion
                            #endregion

                            #region iktat�si param�terek
                            iktatasiParameterek.Atiktatas = false;
                            iktatasiParameterek.IratpeldanyVonalkodGeneralasHaNincs = false;
                            iktatasiParameterek.KeszitoPeldanyaSzukseges = false;
                            iktatasiParameterek.MunkaPeldany = false;
                            iktatasiParameterek.UgyiratPeldanySzukseges = false;
                            iktatasiParameterek.UgyiratUjranyitasaHaLezart = true;
                            #endregion


                            if (elozmeny.Ds != null && elozmeny.Ds.Tables != null && elozmeny.Ds.Tables[0].Rows.Count == 1)
                                res = iratService.BelsoIratIktatasa_Alszamra(_execParam, IktatoKonyv, elozmeny.Ds.Tables[0].Rows[0]["Id"].ToString(), ugyiratDarabok, irat, iratHataridosFeladatok, iratPeldanyok, iktatasiParameterek);
                            else
                                res = iratService.BelsoIratIktatasa(_execParam, IktatoKonyv, ugyirat, ugyiratDarabok, irat, iratHataridosFeladatok, iratPeldanyok, iktatasiParameterek);
                            if (!res.IsError)
                            {


                                var execparamIrat = _execParam.Clone();
                                execparamIrat.Record_Id = res.Uid;
                                var resultIrat = iratService.Get(execparamIrat);

                                var execparamUgyirat = _execParam.Clone();
                                execparamUgyirat.Record_Id = ((EREC_IraIratok)resultIrat.Record).Ugyirat_Id;
                                iratId = ((EREC_IraIratok)resultIrat.Record).Id;

                                #region CR3141 - CR3138 - T�meges iktat�s (�nkad�) bet�lt�tt t�telekhez kapcsol�d�an
                                if (ServiceParams.AlairasKell.Equals("1"))
                                {
                                    var resultAlairo = SignatoryCreate((EREC_IraIratok)resultIrat.Record);
                                    if (resultAlairo != null && !resultAlairo.IsError && !String.IsNullOrEmpty(resultAlairo.Uid))
                                    {
                                        Logger.Info(String.Format("Az irat-hoz: {0} l�trej�tt EREC_IratAlairok rekord id-ja: {1}", res.Uid, resultAlairo.Uid));
                                    }
                                    else
                                    {
                                        Logger.Error("Az irathoz az al��r� rekord l�trehoz�sa nem siker�lt!", _execParam, resultAlairo);
                                        tempError += Environment.NewLine + "\tAz irathoz az al��r� rekord l�trehoz�sa nem siker�lt!";
                                    }
                                }
                                if (ServiceParams.HatosagiAdatlapKell.Equals("1"))
                                {
                                    var resultHatosag = CommuneCreate((EREC_IraIratok)resultIrat.Record);
                                    if (resultHatosag != null && !resultHatosag.IsError)
                                    {
                                        Logger.Info(String.Format("Az irat-hoz: {0} l�trej�tt EREC_IraOnkormAdatok rekord id-ja: {1}", res.Uid, String.IsNullOrEmpty(resultHatosag.Uid) ? res.Uid : resultHatosag.Uid));
                                    }
                                    else
                                    {
                                        Logger.Error("Az irathoz a hat�s�gi adatlap rekord l�trehoz�sa nem siker�lt!", _execParam, resultHatosag);
                                        tempError += Environment.NewLine + "\tAz irathoz a hat�s�gi adatlap rekord l�trehoz�sa nem siker�lt!";
                                    }
                                }

                                #endregion

                                #region Expedi�l�s
                                resultIrat = iratService.Get(execparamIrat);
                                if (((EREC_IraIratok)resultIrat.Record).Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott && ServiceParams.AutoExpedialasKell.Equals("1"))
                                {
                                    var resultExpedialas = Expedialas(((EREC_IraIratok)resultIrat.Record).Id);
                                    if (resultExpedialas != null && !resultExpedialas.IsError)
                                    {
                                        Logger.Info(String.Format("Az irat-hoz: {0} tartoz� iratp�ld�ny expedi�l�sa megt�rt�nt.", res.Uid));
                                    }
                                    else
                                    {
                                        Logger.Error("Az irathoz tartoz� iratp�ld�ny expedi�l�sa nem siker�lt!", _execParam, resultExpedialas);
                                        tempError += Environment.NewLine + String.Format("\tAz irathoz tartoz� iratp�ld�ny expedi�l�sa nem siker�lt! Hiba: {0}", resultExpedialas.ErrorCode);
                                    }
                                }
                                #endregion

                                dr["IKTATVA"] = "*";
                                dr["UGYIRATSZ"] = ((EREC_IraIratok)resultIrat.Record).Azonosito.Replace(" ", "");
                                Logger.Info("A " + rowIndex + ". sorban, l�trej�tt irat: " + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\tAzonos�t�: " + ((EREC_IraIratok)resultIrat.Record).Azonosito.Replace(" ", ""));
                                eMailMsg += Environment.NewLine + "\tA " + rowIndex + ". sorban, l�trej�tt irat:" + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\tAzonos�t�: " + ((EREC_IraIratok)resultIrat.Record).Azonosito.Replace(" ", "") + Environment.NewLine;
                            }
                            else
                            {
                                //CR3228 
                                ++errCount;
                                dr["UGYIRATSZ"] = "";
                                Logger.Info("A " + rowIndex + ". sorban. Nem j�tt l�tre irat: " + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\t" + res.ErrorMessage);
                                #region CR3290
                                eMailMsg += Environment.NewLine + "A " + rowIndex + ". sorban, nem j�tt l�tre irat:" + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\t" + getErrorMessageByErrorCode(res.ErrorMessage) + Environment.NewLine;
                                #endregion
                            }
                            outResult = res;
                            #endregion IKTATAS
                        }
                        else
                        {
                            Logger.InfoStart(String.Format("A(z) {0}. sor m�r iktatva: ", (rowIndex).ToString()));
                        }
                    }
                    catch (Exception ex)
                    {
                        //CR3228 
                        ++errCount;

                        Logger.Error(Environment.NewLine + "Hiba: A " + rowIndex + ". sorban." + tempError + ex.Message);
                        dr["UGYIRATSZ"] = "";
                        eMailMsg += Environment.NewLine + "\tHiba: A " + rowIndex + ". sorban." + Environment.NewLine + "\t" + tempError + Environment.NewLine + "\t" + ex.Message + Environment.NewLine;
                    }
                    Logger.InfoEnd(String.Format("A(z) {0}. sor iktat�sa:", (rowIndex).ToString()));

                    #region FILE CSATOLAS
                    if (
                           !string.IsNullOrEmpty(dr["UGYIRATSZ"].ToString())
                        && (dataset.Tables[0].Columns.Contains(CONST_DBF_ATCH_FILEPATH_COL_NAME))
                        && !string.IsNullOrEmpty(dr[CONST_DBF_ATCH_FILEPATH_COL_NAME].ToString()))
                    {
                        #region FIND IRAT
                        if (string.IsNullOrEmpty(iratId))
                        {
                            Result resultFindIrat = FindIratByAzonosito(ExecParam, iratService, dr["UGYIRATSZ"].ToString());
                            if (resultFindIrat.IsError)
                            {
                                Logger.Error("Irat nem tal�lhat� : " + dr["UGYIRATSZ"].ToString());
                                eMailMsg += Environment.NewLine + "Irat nem tal�lhat� : " + dr["UGYIRATSZ"].ToString();
                            }
                            else if (resultFindIrat.Ds.Tables[0].Rows.Count > 0)
                            {
                                iratId = resultFindIrat.Ds.Tables[0].Rows[0]["Id"].ToString();
                                eMailMsg += Environment.NewLine + dr["UGYIRATSZ"].ToString() + ": ";
                            }
                        }
                        #endregion

                        FileCsatolasIrathoz(ExecParam, ref iratService, ref eMailMsg, iratId, dr[CONST_DBF_ATCH_FILEPATH_COL_NAME].ToString());
                        dr[CONST_DBF_ATCH_FILEPATH_COL_NAME] = string.Empty;
                        iratId = string.Empty;
                        eMailMsg += Environment.NewLine;
                    }
                    #endregion FILE CSATOLAS
                }

                #endregion
                //CR3228 
                if (errCount == dataset.Tables[0].Rows.Count)
                {
                    resultFileNeed = false;
                }
                if (!DBFclass.CreateResultDBFFile(dataset))
                {
                    Logger.Error("Hiba:Az eredm�ny file l�trehoz�sa nem siker�lt!");
                    eMailMsg += Environment.NewLine + " Az eredm�ny file l�trehoz�sa nem siker�lt";
                }

            }
            catch (Exception ex)
            {
                #region CR3228 - ha egy f�jlban l�v� adatszerkezet nem megfelel�, akkor ne k�sz�ts�nk eredm�ny f�jlt
                resultFileNeed = false;
                #endregion
                Logger.Error("Ismeretlen hiba a folyamat sikertelen. " + ex.Message);
                eMailMsg += Environment.NewLine + " Ismeretlen hiba a folyamat sikertelen. " + ex.Message;
            }


            #endregion
        }
        else
        {
            Logger.Error("Valami nem j�, a param�terek alapj�n nem meghat�rozhat� a forr�s..");
            eMailMsg += "Valami nem j�, a param�terek alapj�n nem meghat�rozhat� a forr�s..";
            resultFileNeed = false;
        }
        byte[] resultFile = null;
        if (xmlDoc != null)
        {
            resultFile = Encoding.Default.GetBytes(xmlDoc.OuterXml);

        }
        else if (xmlDoc == null)
            resultFile = File.ReadAllBytes(dbfFilePath);

        string resultFilePath = "";
        //CR3228 - ha egy f�jlban l�v� adatszerkezet nem megfelel�, akkor ne k�sz�ts�nk eredm�ny f�jlt
        if (resultFile.Length > 0 && resultFileNeed)
        {
            try
            {
                if (xmlDoc != null)
                {
                    String[] splitString = dbfFilePath.Split('\\');
                    string fileName = splitString[splitString.Length - 1];
                    splitString = splitString.Where(w => w != splitString[splitString.Length - 1]).ToArray();
                    string folderPath = String.Join("\\", splitString);
                    if (System.IO.File.Exists(folderPath + "\\" + fileName.Replace(".xml", "_Original.xml")))
                    {
                        System.IO.File.Delete(folderPath + "\\" + fileName.Replace(".xml", "_Original.xml"));
                        System.IO.File.Copy(folderPath + "\\" + fileName, folderPath + "\\" + fileName.Replace(".xml", "_Original.xml"));
                        File.WriteAllBytes(folderPath + "\\" + fileName, resultFile);
                    }
                    else
                    {
                        System.IO.File.Copy(folderPath + "\\" + fileName, folderPath + "\\" + fileName.Replace(".xml", "_Original.xml"));
                        File.WriteAllBytes(folderPath + "\\" + fileName, resultFile);
                    }
                    resultFilePath = uploadFile("t_iktatas" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml", resultFile);
                }
                else if (xmlDoc == null)
                {
                    resultFilePath = uploadFile("t_iktatas" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".dbf", resultFile);
                }
                if (resultFilePath == null || string.IsNullOrEmpty(resultFilePath.Trim())) throw new Exception();
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("A file felt�lt�s nem siker�lt. {0} A file megtal�lhat� az al�bbi �tvonalon: {1}", ex.Message, dbfFilePath));
                eMailMsg += Environment.NewLine + "A file felt�lt�s nem siker�lt.";
            }
        }

        using (var felhasznaloService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService())
        {
            var exec = _execParam.Clone();
            exec.Record_Id = _execParam.Felhasznalo_Id;
            Result Felhasznalo = felhasznaloService.Get(exec);

            sendMail(resultFilePath, eMailMsg, new string[] { ((KRT_Felhasznalok)Felhasznalo.Record).EMail });
        }
        log.WsEnd(_execParam, outResult);
        return outResult;
    }

    #region CR3141 - CR3138 - T�meges iktat�s (�nkad�) bet�lt�tt t�telekhez kapcsol�d�an
    private Result SignatoryCreate(EREC_IraIratok irat)
    {
        Result resultAlairo = new Result();
        EREC_IratAlairok iratAlairo = new EREC_IratAlairok();

        using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
        {
            sqlConnection.Open();
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "select p.AlairasMod from KRT_AlairasTipusok p where p.Id = @azonosito";
                sqlCommand.Parameters.Add("@azonosito", SqlDbType.VarChar, 100, "azonosito").Value = ServiceParams.AlairasMod;
                iratAlairo.AlairasMod = sqlCommand.ExecuteScalar().ToString();
            }
        }
        using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
        {
            sqlConnection.Open();
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "select p.AlairoSzerep from KRT_AlairasSzabalyok p where p.Id = @azonosito";
                sqlCommand.Parameters.Add("@azonosito", SqlDbType.VarChar, 100, "azonosito").Value = ServiceParams.AlairasSzabaly_Id;
                var alairoSzerep = sqlCommand.ExecuteScalar();
                iratAlairo.AlairoSzerep = alairoSzerep == null ? KodTarak.ALAIRO_SZEREP.Kiadmanyozo : alairoSzerep.ToString();
            }
        }

        //BUG_2155 - csak ut�lagosan adminisztr�lt al��r�sn�l kell be�rni az al��r�s d�tum�t
        if (iratAlairo.AlairasMod == "M_UTO")
        {
            iratAlairo.AlairasDatuma = irat.IktatasDatuma;
            iratAlairo.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairt;
        }
        else
        {
            iratAlairo.Allapot = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
        }

        iratAlairo.AlairasSzabaly_Id = ServiceParams.AlairasSzabaly_Id;//"A1C17CD8-AC8E-4CB5-96C7-E27927A3BDA7";
        iratAlairo.Obj_Id = irat.Id;
        iratAlairo.FelhasznaloCsoport_Id_Alairo = ServiceParams.FelhasznaloCsoport_Id_Alairo;//"AF19749B-80B1-E611-92C1-0050569A6FBA";
        iratAlairo.FelhaszCsoport_Id_Helyettesito = ServiceParams.FelhaszCsoport_Id_Helyettesito;

        using (var alairoService = eRecordService.ServiceFactory.GetEREC_IratAlairokService())
        {
            resultAlairo = alairoService.Insert(_execParam.Clone(), iratAlairo);
        }
        return resultAlairo;
    }

    private Result CommuneCreate(EREC_IraIratok irat)
    {
        Result resultHatosag = new Result();
        EREC_IraOnkormAdatok iratHatosag = new EREC_IraOnkormAdatok();
        using (var hatosagService = eRecordService.ServiceFactory.GetEREC_IraOnkormAdatokService())
        {
            var execHatosag = _execParam.Clone();
            execHatosag.Record_Id = irat.Id;
            var resultGetHatosag = hatosagService.Get(execHatosag);
            if (!resultGetHatosag.IsError && resultGetHatosag.Record != null)
                iratHatosag = (EREC_IraOnkormAdatok)resultGetHatosag.Record;
            iratHatosag.DontesFormaja = ServiceParams.DontesFormaja;
            iratHatosag.DontestHozta = ServiceParams.DontestHozta;
            iratHatosag.EljarasiKoltseg = ServiceParams.EljarasiKoltseg;
            iratHatosag.FelfuggHatarozat = ServiceParams.FelfuggHatarozat;
            iratHatosag.FuggoHatalyuHatarozat = ServiceParams.FuggoHatalyuHatarozat;
            iratHatosag.FuggoHatalyuVegzes = ServiceParams.FuggoHatalyuVegzes;
            iratHatosag.HatAltalVisszafizOsszeg = ServiceParams.HatAltalVisszafizOsszeg;
            iratHatosag.HataridoTullepes = ServiceParams.HataridoTullepes;
            iratHatosag.HatosagiEllenorzes = ServiceParams.HatosagiEllenorzes;
            iratHatosag.HatTerheloEljKtsg = ServiceParams.HatTerheloEljKtsg;
            iratHatosag.IraIratok_Id = irat.Id;
            iratHatosag.KozigazgatasiBirsagMerteke = ServiceParams.KozigazgatasiBirsagMerteke;
            iratHatosag.MunkaorakSzama = ServiceParams.MunkaorakSzama;
            iratHatosag.NyolcNapBelulNemSommas = ServiceParams.NyolcNapBelulNemSommas;
            iratHatosag.SommasEljDontes = ServiceParams.SommasEljDontes;
            iratHatosag.UgyFajtaja = ServiceParams.UgyFajtaja;
            iratHatosag.UgyintezesHataridore = ServiceParams.UgyintezesHataridore;

            #region CR3290
            iratHatosag.Updated.SetValueAll(true);
            #endregion
            if (!resultGetHatosag.IsError && resultGetHatosag.Record != null)
                resultHatosag = hatosagService.Update(execHatosag, iratHatosag);
            else
                resultHatosag = hatosagService.Insert(_execParam.Clone(), iratHatosag);
        }
        return resultHatosag;
    }

    private Result Expedialas(string iratId)
    {
        Result resultExpedialas = new Result();
        EREC_PldIratPeldanyok iratPeldany = null;

        using (var iratPeldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
        {
            var res = iratPeldanyService.GetElsoIratPeldanyByIraIrat(_execParam.Clone(), iratId);
            if (!res.IsError)
            {
                iratPeldany = (EREC_PldIratPeldanyok)res.Record;
            }
            else
            {
                return res;
            }
            string[] iratPeldanyokIdArray = { iratPeldany.Id };
            if (iratPeldany.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott)
            {
                resultExpedialas = iratPeldanyService.Expedialas(_execParam.Clone(), iratPeldanyokIdArray, "", iratPeldany, null);
            }
        }
        return resultExpedialas;
    }
    #endregion

    private void sendMail(string filePath, string eMailMsg, string[] to)
    {
        using (Contentum.eAdmin.Service.EmailService mailService = eAdminService.ServiceFactory.GetEmailService())
        {
            var from = Rendszerparameterek.Get(_execParam, Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM);
            string msg = resultFileNeed == true ? "A folyamat sor�n l�trej�tt file az al�bbi �tvonalon megtal�lhat�:" + Environment.NewLine : "";
            msg += filePath + Environment.NewLine + "" + eMailMsg;
            var res = mailService.SendEmail(_execParam, from, to, "T�meges iktat�s", null, null, false, msg);
            if (!res) Logger.Error(String.Format("Az e-mail k�ld�s sikertelen. From: {0}, To: {1}, Message: {2}", from, to[0], eMailMsg));
        }
        return;
    }
    private string uploadFile(string FilePath, byte[] cont)
    {

        String[] splitString = FilePath.Split('\\');
        string fileName = splitString[splitString.Length - 1];
        string doktarFolderPath = "T�megesIktat�s";
        string documentStoreType = "SharePoint";
        string doktarSitePath = "OnkAdo";
        string doktarDocLibPath = "Files";
        string res = "";
        using (var service = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService())
        {
            var Result = service.DirectUploadWithCheckin(_execParam, documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, fileName, cont, "1");
            if (Result.IsError)
                Logger.Error("A file felt�lt�s sikertelen.", _execParam, Result);
            res = Result.Record.ToString();
        }

        return res;
    }

    public string convertString852toUTF8(string inputString)
    {
        //inputString = inputString.ToLower();
        inputString = inputString.Replace((char)0x99, '�');
        inputString = inputString.Replace((char)0x2122, '�');
        inputString = inputString.Replace((char)0x9a, '�');
        inputString = inputString.Replace((char)0x161, '�');
        inputString = inputString.Replace((char)0xe0, '�');
        inputString = inputString.Replace((char)0x155, '�');
        inputString = inputString.Replace((char)0x8a, '�');
        inputString = inputString.Replace((char)0x160, '�');
        inputString = inputString.Replace((char)0xe9, '�');
        inputString = inputString.Replace((char)0x90, '�');
        inputString = inputString.Replace((char)0xb5, '�');
        //inputString = inputString.Replace((char)0x, '�');161
        inputString = inputString.Replace((char)0xd6, '�');
        //var stringSplit = inputString.Split(new char[]{' '});
        //inputString = "";

        //foreach (string field in stringSplit)
        //{
        //    char first = field[0];
        //    inputString += " " +  first.ToString().ToUpper() + field.Substring(1, field.Length - 1).ToLower();
        //}
        return inputString.ToUpper().Trim();
    }
    #region CR3290
    private string getErrorMessageByErrorCode(string errorMsg)
    {
        string msg = "";
        Regex regex = new Regex(@"\[\d+\]");
        Match match = regex.Match(errorMsg);
        if (match.Success)
        {
            switch (errorMsg)
            {
                case "[52411]":
                    msg = "Nincs joga enged�lyezni a k�lcs�nz�st!";
                    break;
                case "[52403]":
                    msg = "Hiba a k�lcs�nz�s j�v�hagy�sa sor�n: K�lcs�nz�s lek�r�se az adatb�zisb�l sikertelen!";
                    break;
                case "[52404]":
                    msg = "A k�lcs�nz�s nem j�v�hagyhat�!";
                    break;
                case "[52407]":
                    msg = "Az �gyirat nem adhat� ki k�lcs�nz�sre!";
                    break;
                case "[52408]":
                    msg = "Hiba a k�lcs�nz�s kiad�sa sor�n: �gyirat lek�r�se az adatb�zisb�l sikertelen!";
                    break;
                case "[52108]":
                    msg = "Nincs megadva felel�s!";
                    break;
                case "[52106]":
                    msg = "Hiba az iktat�s sor�n: �gyirat lek�r�se az adatb�zisb�l sikertelen";
                    break;
                case "[52109]":
                    msg = "Az �gyirathoz nem lehet iktatni iratot!";
                    break;
                case "[52135]":
                    msg = "Az �gyiratban nem hozhat� l�tre �j munkap�ld�ny!";
                    break;
                default:
                    msg = "Hibak�d: " + errorMsg;
                    break;
            }
        }
        else msg = errorMsg;
        return msg;
    }
    #endregion CR3290
    #endregion

    #region parameters
    private Result getCsoport()
    {
        var resCsoport = new Result();
        #region csoport //felel�s
        using (var csoportokService = eAdminService.ServiceFactory.GetKRT_CsoportokService())
        {
            var execCsoport = _execParam.Clone();
            execCsoport.Record_Id = ServiceParams.FelelosCsoport_Id;
            resCsoport = csoportokService.Get(execCsoport);
        }
        #endregion
        return resCsoport;

    }
    private Result getAgazat()
    {
        var resAgazat = new Result();
        #region �gazat
        using (var agazatService = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService())
        {
            var execAgazat = _execParam.Clone();
            execAgazat.Record_Id = ServiceParams.AgazatiJelek_Id;
            resAgazat = agazatService.Get(execAgazat);
        }
        #endregion
        return resAgazat;
    }
    private Result getIrattar()
    {
        var resIrattar = new Result();
        #region iratt�ri t�tel
        using (var irattariTetelService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService())
        {
            var execirattariTetel = _execParam.Clone();
            execirattariTetel.Record_Id = ServiceParams.IraIrattariTetelek_Id;
            resIrattar = irattariTetelService.Get(execirattariTetel);
        }
        #endregion
        return resIrattar;
    }
    private Result getUgytipus()
    {
        var resUgytipus = new Result();
        #region �gyt�pus
        using (var iratMetadefinicioService = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService())
        {
            var execiratMetadefinicio = _execParam.Clone();
            execiratMetadefinicio.Record_Id = ServiceParams.Ugytipus_Id;
            resUgytipus = iratMetadefinicioService.Get(execiratMetadefinicio);
        }
        #endregion
        return resUgytipus;
    }
    private Result getTelepules(ExecParam _execParam, string dr)
    {
        var resTelepules = new Result();
        #region cim
        using (var telepulesekService = eAdminService.ServiceFactory.GetKRT_TelepulesekService())
        {
            resTelepules = telepulesekService.GetAll
                 (_execParam.Clone(),
                 new KRT_TelepulesekSearch()
                 {
                     IRSZ = new Field() { Name = "KRT_Telepulesek.IRSZ", Type = "String", Operator = Query.Operators.equals, Value = dr, GroupOperator = Query.Operators.and, Group = "100" },
                     ErvKezd = new Field() { Name = "KRT_Telepulesek.ErvKezd", Type = "DateTime", Operator = Query.Operators.lessorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss"), GroupOperator = Query.Operators.and, Group = "100" },
                     ErvVege = new Field() { Name = "KRT_Telepulesek.ErvVege", Type = "DateTime", Operator = Query.Operators.greaterorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss") }
                 }
                  );
            Logger.Info(dr + " ir�ny�t�sz�mhoz tartoz� telep�l�s.", _execParam, resTelepules);
        }

        #endregion
        return resTelepules;
    }
    private object getPartnerId(string dr)
    {
        object partnerId = null;
        #region getPartnerId // �gyf�l
        using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
        {
            sqlConnection.Open();
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "select p.id from KRT_Partnerek p inner join (select s.Partner_Id as pid, s.Adoazonosito as azonosito from KRT_Szemelyek s UNION select ss.Partner_Id as pid, ss.Adoszam as azonosito from KRT_Szemelyek ss UNION select v.partner_id, v.Adoszam from KRT_Vallalkozasok v) pk on p.id = pk.pid where REPLACE(pk.azonosito,'-','') = @azonosito";
                if (dr != null && !string.IsNullOrEmpty(dr))
                {
                    sqlCommand.Parameters.Add("@azonosito", SqlDbType.VarChar, 100, "azonosito").Value = dr.Replace("-", "").Trim();
                    partnerId = sqlCommand.ExecuteScalar();
                }
            }
        }

        #endregion
        return partnerId;
    }
    private Result getPartner(ExecParam execParamPartnerek)
    {
        var resPartnerek = new Result();
        #region getPartner // �gyf�l
        if (!string.IsNullOrEmpty(execParamPartnerek.Record_Id))
        {
            using (var partnerekService = eAdminService.ServiceFactory.GetKRT_PartnerekService())
            {
                resPartnerek = partnerekService.Get(execParamPartnerek);
            }
        }
        #endregion
        return resPartnerek;
    }
    //KRT_PartnerCimekRekord
    private Result getKRT_PartnerCimek(string Id)
    {
        var resKRT_PartnerCimek = new Result();
        using (var KRT_partnerCimekService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService())
        {
            resKRT_PartnerCimek = KRT_partnerCimekService.GetAll
                 (_execParam.Clone(),
                 new KRT_PartnerCimekSearch()
                 {
                     Partner_id = new Field() { Name = "KRT_PartnerCimek.Partner_id", Type = "Guid", Operator = Query.Operators.equals, Value = Id, GroupOperator = Query.Operators.and, Group = "100" },
                     ErvKezd = new Field() { Name = "KRT_PartnerCimek.ErvKezd", Type = "DateTime", Operator = Query.Operators.lessorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss"), GroupOperator = Query.Operators.and, Group = "100" },
                     ErvVege = new Field() { Name = "KRT_PartnerCimek.ErvVege", Type = "DateTime", Operator = Query.Operators.greaterorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss") }
                 }
                  );
        }
        return resKRT_PartnerCimek;
    }
    #region CR3202 - T�meges iktat�s, ha van a pertnerhez KRT_cimek rekord, azt k�ne bet�lteni
    //KRT_CimekRekord
    private Result getKRT_Cimek(string Id)
    {
        var resKRT_Cimek = new Result();
        using (var KRT_CimekService = eAdminService.ServiceFactory.GetKRT_CimekService())
        {
            resKRT_Cimek = KRT_CimekService.GetAll
                 (_execParam.Clone(),
                 new KRT_CimekSearch()
                 {
                     Id = new Field() { Name = "KRT_Cimek.Id", Type = "Guid", Operator = Query.Operators.equals, Value = Id, GroupOperator = Query.Operators.and, Group = "100" },
                     ErvKezd = new Field() { Name = "KRT_Cimek.ErvKezd", Type = "DateTime", Operator = Query.Operators.lessorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss"), GroupOperator = Query.Operators.and, Group = "100" },
                     ErvVege = new Field() { Name = "KRT_Cimek.ErvVege", Type = "DateTime", Operator = Query.Operators.greaterorequal, Value = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss") }
                 }
                  );
        }
        return resKRT_Cimek;
    }
    #endregion
    private object getFelhasznaloId(string dr)
    {
        object felhasznaloId = null;
        #region getfelhasznaloId // �gyint�z�
        using (var sqlConnection = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.NetConnectionString"].ToString()))
        {
            sqlConnection.Open();
            using (var sqlCommand = new SqlCommand())
            {
                sqlCommand.Connection = sqlConnection;
                sqlCommand.CommandText = "select p.id from KRT_Felhasznalok p where p.Note = @Note and getdate() between ErvKezd and ErvVege";
                if (dr != null && !string.IsNullOrEmpty(dr))
                {
                    sqlCommand.Parameters.Add("@Note", SqlDbType.VarChar, 100, "Note").Value = dr.ToString().Trim();
                    felhasznaloId = sqlCommand.ExecuteScalar();
                }
            }
        }

        #endregion
        return felhasznaloId;
    }
    private Result getUgyintezo(ExecParam execParamUgyintezo)
    {
        var resUgyintezo = new Result();
        if (!string.IsNullOrEmpty(execParamUgyintezo.Record_Id))
        {
            using (var felhasznaloService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService())
            {
                resUgyintezo = felhasznaloService.Get(execParamUgyintezo);
            }
        }
        return resUgyintezo;
    }
    private Result getElozmeny(string IktatoKonyv, string ugyfelId, string irattarId, string AdoTargy)
    {
        var elozmeny = new Result();
        var iktatokonyvObj = new Result();
        #region el�zm�ny keres�s
        using (var iktatokonyvService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService())
        {
            var iktatokonyvExecParam = _execParam.Clone();
            iktatokonyvExecParam.Record_Id = IktatoKonyv;
            iktatokonyvObj = iktatokonyvService.Get(iktatokonyvExecParam);
        }
        if (!iktatokonyvObj.IsError && ((EREC_IraIktatoKonyvek)iktatokonyvObj.Record).Ev == DateTime.Now.Year.ToString())
        {
            using (var ugyiratService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService())
            {
                elozmeny = ugyiratService.GetAll(
                  _execParam.Clone(),
                  new EREC_UgyUgyiratokSearch()
                  {
                      TopRow = 1,
                      Targy = new Field() { Name = "EREC_UgyUgyiratok.Targy", Type = "FTSString", Operator = Query.Operators.like, Value = "%" + AdoTargy.Trim() + "%", GroupOperator = Query.Operators.and, Group = "100" },
                      IraIktatokonyv_Id = new Field() { Name = "EREC_UgyUgyiratok.IraIktatokonyv_Id", Type = "String", Operator = Query.Operators.equals, Value = IktatoKonyv, GroupOperator = Query.Operators.and, Group = "100" },
                      Partner_Id_Ugyindito = new Field() { Name = "EREC_UgyUgyiratok.Partner_Id_Ugyindito", Type = "Guid", Operator = Query.Operators.equals, Value = ugyfelId, GroupOperator = Query.Operators.and, Group = "100" },
                      IraIrattariTetel_Id = new Field() { Name = "EREC_UgyUgyiratok.IraIrattariTetel_Id", Type = "Guid", Operator = Query.Operators.equals, Value = irattarId },
                      OrderBy = "EREC_UgyUgyiratok.Foszam desc"
                  }
              );
            }
        }
        #endregion el�zm�ny keres�s
        return elozmeny;
    }

    private string getIrattipus()
    {
        var resIrattipus = new Result();
        #region iratt�pus /k�dt�r
        using (var kodtarService = eAdminService.ServiceFactory.GetKRT_KodTarakService())
        {
            var execiratTipus = _execParam.Clone();
            execiratTipus.Record_Id = ServiceParams.Irattipus_Id;
            resIrattipus = kodtarService.Get(execiratTipus);
        }
        #endregion
        return ((KRT_KodTarak)resIrattipus.Record).Kod;
    }
    #endregion

    /// <summary>
    /// CheckUNCFile
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private bool CheckUNCFile(string path)
    {
        if (!File.Exists(path))
            return false;
        return true;
    }
    /// <summary>
    /// LoadFileFromPath
    /// </summary>
    /// <param name=CONST_DBF_FILEPATH_COLUMN_NAME></param>
    /// <returns></returns>
    private byte[] LoadFileFromPath(string filePath)
    {
        return File.ReadAllBytes(filePath);
    }

    /// <summary>
    /// GetFile
    /// </summary>
    /// <param name=CONST_DBF_FILEPATH_COLUMN_NAME></param>
    /// <returns></returns>
    public byte[] GetFile(String FilePath)
    {
        System.IO.FileStream fs = null;
        byte[] file_buffer = null;
        try
        {
            fs = new System.IO.FileStream(FilePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        }
        catch (System.IO.FileNotFoundException e)
        {
            throw e;
        }

        try
        {
            file_buffer = new byte[fs.Length];

            fs.Read(file_buffer, 0, (int)fs.Length);

            fs.Flush();
            fs.Close();
        }
        catch (System.IO.FileLoadException e)
        {
            throw e;
        }

        return file_buffer;
    }

    /// <summary>
    /// FindIratByAzonosito
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="iratService"></param>
    /// <param name="dr"></param>
    /// <returns></returns>
    private static Result FindIratByAzonosito(ExecParam ExecParam, Contentum.eRecord.Service.EREC_IraIratokService iratService, string azonosito)
    {
        EREC_IraIratokSearch src = new EREC_IraIratokSearch
        {
            TopRow = 1
        };
        //az iktat�sz�m vissza�r�sakor kivessz�k a space-eket, ez�rt a keres�skor is ki kell venni a DB oldalon
        src.WhereByManual = " AND REPLACE(EREC_IraIratok.Azonosito,' ','') = '" + azonosito + "'";
        //src.Azonosito.Value = azonosito;
        //src.Azonosito.Operator = Query.Operators.equals;

        Result resultFindIrat = iratService.GetAll(ExecParam.Clone(), src);
        return resultFindIrat;
    }

    /// <summary>
    /// Generate_CsatolmanyObj
    /// </summary>
    /// <param name=CONST_DBF_FILEPATH_COLUMN_NAME></param>
    /// <param name="fileInfo"></param>
    /// <returns></returns>
    private Csatolmany Generate_CsatolmanyObj(string filePath, System.IO.FileInfo fileInfo)
    {
        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Tartalom = GetFile(filePath);
        csatolmany.Nev = fileInfo.Name;
        csatolmany.TartalomHash = Contentum.eUtility.FileFunctions.CalculateSHA1(csatolmany.Tartalom);

        return csatolmany;
    }
    /// <summary>
    /// GenerateCsatolmanyObj
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="linkedKuldId"></param>
    /// <param name="linkedIratId"></param>
    /// <param name="dokumentId"></param>
    /// <returns></returns>
    private static EREC_Csatolmanyok Generate_EREC_CsatolmanyObj(ExecParam execParam, string linkedIratId)
    {
        EREC_Csatolmanyok csatolmany = new EREC_Csatolmanyok();

        if (!string.IsNullOrEmpty(linkedIratId))
        {
            csatolmany.IraIrat_Id = linkedIratId;
            csatolmany.Updated.IraIrat_Id = true;
        }
        return csatolmany;
    }
    /// <summary>
    /// FileCsatolasIrathoz UNC alapjan.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="iratService"></param>
    /// <param name="eMailMsg"></param>
    /// <param name="iratId"></param>
    /// <param name="dr"></param>
    private void FileCsatolasIrathoz(ExecParam ExecParam, ref Contentum.eRecord.Service.EREC_IraIratokService iratService, ref string eMailMsg, string iratId, string fileUNCPath)
    {
        if (string.IsNullOrEmpty(iratId))
            return;
        if (string.IsNullOrEmpty(fileUNCPath))
            return;

        string[] splittedFilePath = fileUNCPath.Split(CONST_DBF_ATCH_FILEPATH_SEPARATOR);
        foreach (string filePath in splittedFilePath)
        {
            if (CheckUNCFile(filePath))
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
                byte[] fileContent = LoadFileFromPath(filePath);
                if (fileContent != null)
                {
                    ExecParam execParamCsatolmany = ExecParam.Clone();
                    execParamCsatolmany.Record_Id = iratId;

                    Csatolmany csatolmany = Generate_CsatolmanyObj(filePath, fileInfo);
                    EREC_Csatolmanyok erec_Csatolmanyok = Generate_EREC_CsatolmanyObj(execParamCsatolmany.Clone(), iratId);
                    Result resultCsatolmany = iratService.CsatolmanyUpload(execParamCsatolmany, erec_Csatolmanyok, csatolmany);
                    if (resultCsatolmany.IsError)
                    {
                        Logger.Error("A file csatol�s sikertelen.", ExecParam, resultCsatolmany);
                    }
                    Logger.Info("A file csatol�s sikeres.", ExecParam, resultCsatolmany);
                    eMailMsg += Environment.NewLine + "Csatol�s sikeres: " + fileUNCPath;
                }
                else
                {
                    Logger.Error("Csatol�s nem t�rt�nt, file path-on l�v� f�jlb�l nem siker�lt az adatbeolvas�s: " + fileUNCPath);
                    eMailMsg += Environment.NewLine + "Csatol�s nem t�rt�nt, file path-on l�v� f�jlb�l nem siker�lt az adatbeolvas�s: " + fileUNCPath;
                }
            }
            else
            {
                Logger.Error("Csatol�s nem t�rt�nt, file path nem l�tezik: " + fileUNCPath);
                eMailMsg += Environment.NewLine + "Csatol�s nem t�rt�nt, file path nem l�tezik: " + fileUNCPath;
            }
        }
    }

}
public class dbfHandler
{
    public string errMSG { get; set; }
    public string fileName { get; set; }
    public string folderPath { get; set; }
    public string connectionString { get; set; }
    private DataTable datatable;
    private DataSet dsImport;
    private System.Data.OleDb.OleDbDataAdapter dataadapter;
    private System.Data.OleDb.OleDbCommand oleselectCommand;
    private OleDbConnection dBaseConnection { get; set; }
    public dbfHandler()
    {
        this.datatable = new DataTable();
        this.dsImport = new DataSet();
    }

    public DataSet LoadDBF()
    {
        // DataSet dsImport = new DataSet();
        using (dBaseConnection = new System.Data.OleDb.OleDbConnection(connectionString))
        {
            oleselectCommand = new System.Data.OleDb.OleDbCommand();
            dBaseConnection.Open();
            // Read the table
            oleselectCommand.Connection = dBaseConnection;
            oleselectCommand.CommandText = @"SELECT * FROM " + fileName;
            using (dataadapter = new System.Data.OleDb.OleDbDataAdapter())
            {

                dataadapter.SelectCommand = oleselectCommand;
                dataadapter.Fill(dsImport);
            }
            dBaseConnection.Close();
        }
        List<DataColumn> Pk = new List<DataColumn>();
        foreach (DataColumn dc in dsImport.Tables[0].Columns)
        {
            // BLG_1698
            //if (dc.ColumnName.ToUpper() == "HATSZAM" || dc.ColumnName.ToUpper() == "ADOTARGY" || dc.ColumnName.ToUpper() == "ADOZOAZON") 
            if (dc.ColumnName.ToUpper() == "HATSZAM" || dc.ColumnName.ToUpper() == "ADOTARGY" || dc.ColumnName.ToUpper() == "ADOZOAZON" || dc.ColumnName.ToUpper() == "SORSZ")
                Pk.Add(dc);
        }
        dsImport.Tables[0].PrimaryKey = Pk.ToArray();
        return dsImport;
    }

    public bool CreateResultDBFFile(DataSet dataset)//,�gyiratsz�m
    {
        string TableName = fileName;
        bool res = true;
        try
        {
            if (System.IO.File.Exists(folderPath + "\\" + TableName.Replace(".dbf", "_Original.dbf")))
            {
                System.IO.File.Delete(folderPath + "\\" + TableName.Replace(".dbf", "_Original.dbf"));
                System.IO.File.Copy(folderPath + "\\" + fileName, folderPath + "\\" + TableName.Replace(".dbf", "_Original.dbf"));
            }
            else
            {
                System.IO.File.Copy(folderPath + "\\" + fileName, folderPath + "\\" + TableName.Replace(".dbf", "_Original.dbf"));
            }
            //}
            using (dBaseConnection = new OleDbConnection(connectionString))
            {
                dBaseConnection.Open();
                using (dataadapter = new System.Data.OleDb.OleDbDataAdapter())
                {
                    OleDbCommand oleupdateCommand = dBaseConnection.CreateCommand();
                    List<DataColumn> Pk = new List<DataColumn>();
                    foreach (DataColumn dc in dataset.Tables[0].Columns)
                    {
                        //if (dc.ColumnName.ToUpper() == "ADOTARGY" || dc.ColumnName.ToUpper() == "HATSZAM" )//
                        // BLG_1698
                        //if (dc.ColumnName.ToUpper() == "HATSZAM" || dc.ColumnName.ToUpper() == "ADOTARGY" || dc.ColumnName.ToUpper() == "ADOZOAZON")  
                        if (dc.ColumnName.ToUpper() == "HATSZAM" || dc.ColumnName.ToUpper() == "ADOTARGY" || dc.ColumnName.ToUpper() == "ADOZOAZON" || dc.ColumnName.ToUpper() == "SORSZ")
                            Pk.Add(dc);
                    }

                    dataset.Tables[0].PrimaryKey = Pk.ToArray();
                    // BLG_1698
                    //string updateStatement = "UPDATE " + TableName + " SET UGYIRATSZ=?,IKTATVA=?  WHERE ADOTARGY=? AND HATSZAM=? AND ADOZOAZON=?"; 
                    string updateStatement = null;

                    if (dataset.Tables[0].Columns.Contains(EREC_TomegesIktatasService.CONST_DBF_ATCH_FILEPATH_COL_NAME))
                    {
                        updateStatement = "UPDATE " + TableName + " SET UGYIRATSZ=?,IKTATVA=?," + EREC_TomegesIktatasService.CONST_DBF_ATCH_FILEPATH_COL_NAME + "=?  WHERE ADOTARGY=? AND HATSZAM=? AND ADOZOAZON=? AND SORSZ=?";
                    }
                    else
                    {
                        updateStatement = "UPDATE " + TableName + " SET UGYIRATSZ=?,IKTATVA=?  WHERE ADOTARGY=? AND HATSZAM=? AND ADOZOAZON=? AND SORSZ=?";
                    }

                    //oleupdateCommand.Parameters.Add("@NEV", OleDbType.VarChar, 30, "NEV");
                    //oleupdateCommand.Parameters.Add("@IRSZ", OleDbType.VarChar, 4, "IRSZ");
                    //oleupdateCommand.Parameters.Add("@UTCA", OleDbType.VarChar, 30, "UTCA");
                    //oleupdateCommand.Parameters.Add("@UGYTIPUS", OleDbType.VarChar, 2, "UGYTIPUS");
                    //oleupdateCommand.Parameters.Add("@ELOADO", OleDbType.VarChar, 2, "ELOADO");
                    oleupdateCommand.Parameters.Add("@UGYIRATSZ", OleDbType.VarChar, 20, "UGYIRATSZ");
                    oleupdateCommand.Parameters.Add("@IKTATVA", OleDbType.VarChar, 1, "IKTATVA");
                    if (dataset.Tables[0].Columns.Contains(EREC_TomegesIktatasService.CONST_DBF_ATCH_FILEPATH_COL_NAME))
                    {
                        oleupdateCommand.Parameters.Add("@" + EREC_TomegesIktatasService.CONST_DBF_ATCH_FILEPATH_COL_NAME, OleDbType.VarChar, 254, EREC_TomegesIktatasService.CONST_DBF_ATCH_FILEPATH_COL_NAME);
                    }
                    oleupdateCommand.Parameters.Add("@ADOTARGY", OleDbType.VarChar, 15, "ADOTARGY");
                    //oleupdateCommand.Parameters.Add("@SORSZ", OleDbType.VarChar, 4, "SORSZ");
                    //oleupdateCommand.Parameters.Add("@NAPLO", OleDbType.VarChar, 3, "NAPLO");
                    oleupdateCommand.Parameters.Add("@HATSZAM", OleDbType.Numeric, 5, "HATSZAM");
                    oleupdateCommand.Parameters.Add("@ADOZOAZON", OleDbType.VarChar, 6, "ADOZOAZON");
                    // BLG_1698
                    oleupdateCommand.Parameters.Add("@SORSZ", OleDbType.VarChar, 4, "SORSZ");
                    oleupdateCommand.CommandText = updateStatement;
                    dataadapter.UpdateCommand = oleupdateCommand;
                    dataadapter.Update(dataset);
                }
            }
        }
        catch (Exception ex)
        {
            res = false;
            Logger.Error("hiba az eredm�ny file l�trehoz�sa k�zben: " + ex.Message);
            errMSG = ex.Message;
        }
        return res;
    }
}