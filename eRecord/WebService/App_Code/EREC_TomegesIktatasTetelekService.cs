using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

using log4net;
using log4net.Config;

using System.Collections.Generic;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_TomegesIktatasTetelekService : System.Web.Services.WebService
{
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "T0bb rekord felvétele az adott táblába. A rekordok adatait a Records parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_TomegesIktatasTetelek))]
    public Result InsertBulk(ExecParam ExecParam, EREC_TomegesIktatasTetelek[] Records)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.InsertBulk(Constants.Insert, ExecParam, Records);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                //KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_TomegesIktatasTetelek", "New").Record;

                //Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}
