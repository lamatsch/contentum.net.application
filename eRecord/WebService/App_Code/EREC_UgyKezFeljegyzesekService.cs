using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_UgyKezFeljegyzesekService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyKezFeljegyzesekSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyKezFeljegyzesekSearch _EREC_UgyKezFeljegyzesekSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyKezFeljegyzesekSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result GetLastIrattariKezelesiFeljegyzes(ExecParam ExecParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_UgyKezFeljegyzesekSearch srch = new EREC_UgyKezFeljegyzesekSearch();
            srch.UgyUgyirat_Id.Value = ugyiratId;
            srch.UgyUgyirat_Id.Operator = Query.Operators.equals;
            srch.KezelesTipus.Value = Contentum.eUtility.KodTarak.KezelesiFeljegyzes.Irattarozasra;
            srch.KezelesTipus.Operator = Query.Operators.equals;
            srch.OrderBy = " EREC_UgyKezFeljegyzesek.LetrehozasIdo DESC";

            result = sp.GetAllWithExtension(ExecParam, srch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count > 0)
            {
                EREC_UgyKezFeljegyzesek _EREC_UgyKezFeljegyzesek = new EREC_UgyKezFeljegyzesek();
                Utility.LoadBusinessDocumentFromDataRow(_EREC_UgyKezFeljegyzesek, result.Ds.Tables[0].Rows[0]);
                result.Record = _EREC_UgyKezFeljegyzesek;

            }
            else
            {
                result.Record = null;
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}