using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_UgyUgyiratdarabokService : System.Web.Services.WebService
{
    #region Gener�ltb�l �tvettek
    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_UgyUgyiratdarabok Record)
    /// Egy rekord felv�tele a EREC_UgyUgyiratdarabok t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratdarabok))]
    public Result Insert(ExecParam ExecParam, EREC_UgyUgyiratdarabok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region ACL kezel�s

            Result aclResult = result;
            RightsService rs = new RightsService(this.dataContext);
            aclResult = rs.AddScopeIdToJogtargy(ExecParam, result.Uid, "EREC_UgyUgyiratdarabok", Record.UgyUgyirat_Id, 'S', null);
            if (!string.IsNullOrEmpty(aclResult.ErrorCode))
            {
                throw new ResultException(aclResult);
            }

            #endregion

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_UgyUgyiratDarabok", "New").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }



    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_UgyUgyiratdarabok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratdarabok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ellen�rz�s, van-e alatta irat:
            EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

            EREC_IraIratokSearch search = new EREC_IraIratokSearch();

            search.UgyUgyIratDarab_Id.Value = ExecParam.Record_Id;
            search.UgyUgyIratDarab_Id.Operator = Query.Operators.equals;

            ExecParam execParam_iratokGetall = ExecParam.Clone();
            Result result_iratokGetAll = service_iratok.GetAll(execParam_iratokGetall, search);
            if (!String.IsNullOrEmpty(result_iratokGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratokGetAll);
            }

            int iratokSzama = result_iratokGetAll.Ds.Tables[0].Rows.Count;

            if (iratokSzama > 0)
            {
                // van m�g benne irat, nem t�r�lhet�:
                throw new ResultException(52551);
            }

            #endregion

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_UgyUgyiratdarabok", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    #endregion
    
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratdarabokSearch))]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra). 
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_EREC_UgyUgyiratdarabokSearch">A keres�si felt�teleket tartalmaz� objektum</param>
    /// <returns>Result.Ds DataSet objektum ker�l felt�lt�sre a lek�rdez�s eredm�ny�vel</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratdarabokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyUgyiratdarabokSearch _EREC_UgyUgyiratdarabokSearch)
    {
        return GetAllWithExtensionAndJogosultak(ExecParam, _EREC_UgyUgyiratdarabokSearch, false);
    }

    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra). 
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_EREC_UgyUgyiratdarabokSearch">A keres�si felt�teleket tartalmaz� objektum</param>
    /// <returns>Result.Ds DataSet objektum ker�l felt�lt�sre a lek�rdez�s eredm�ny�vel</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratdarabokSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_UgyUgyiratdarabokSearch _EREC_UgyUgyiratdarabokSearch, bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratdarabokSearch, Jogosultak);

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_UgyUgyiratdarabokSearch, new EREC_UgyUgyiratdarabokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_UgyUgyiratDarabok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_UgyUgyiratdarabokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_EREC_UgyUgyiratdarabokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_UgyUgyiratdarabokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }





    /// DEPRECATED (2007.11.26.)
    /// <summary>
    /// A megadott �gyirat nyitott �gyiratdarabj�t adja vissza, ha van.
    /// Ha hiba volt, hibak�ddal t�r vissza; 
    /// ha nem volt hiba, de nincs is tal�lat, akkor egy �res Result objektummal t�r vissza
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="UgyUgyirat_Id">Az �gyirat Id-ja, aminek a nyitott �gyiratdarabj�t le szeretn�nk k�rni.</param>
    /// <returns>Result.Record tartalmazza az �gyiratdarabot, ha van nyitott �gyiratdarab, egy�bk�nt Result.Record 'null' lesz.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratdarabok))]
    public Result GetNyitottUgyUgyiratDarabByUgyUgyirat(ExecParam ExecParam, String UgyUgyirat_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();

            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = UgyUgyirat_Id;
            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;

            erec_UgyUgyiratdarabokSearch.LezarasDat.Operator = Query.Operators.isnull;

            result = sp.GetAll(ExecParam, erec_UgyUgyiratdarabokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                //log.WsEnd(ExecParam, result);
                //return result;

                throw new ResultException(result);
            }
            else
            {

                System.Data.DataSet ds = result.Ds;
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        //Result result1 = ResultError.CreateNewResultWithErrorCode(51102);
                        //log.WsEnd(ExecParam, result1);
                        //return result1;

                        throw new ResultException(51102);
                    }
                    else
                    {
                        String Id = ds.Tables[0].Rows[0]["Id"].ToString();

                        // az objektumnak a lek�r�se az Id alapj�n:
                        EREC_UgyUgyiratdarabokService erec_UgyUgyiratdarabokService = new EREC_UgyUgyiratdarabokService(this.dataContext);

                        ExecParam execParam_Get = ExecParam.Clone();
                        execParam_Get.Record_Id = Id;

                        Result result_get = erec_UgyUgyiratdarabokService.Get(execParam_Get);

                        result = result_get;

                        if (!String.IsNullOrEmpty(result.ErrorCode))
                        {
                            throw new ResultException(result);
                        }

                    }
                }
                else
                {
                    // Nem volt hiba, viszont nincs tal�lat (Result.Record == null lesz)

                    result = new Result();
                }

            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// DEPRECATED (2007.11.26.)
    // Nem kell bel�le webmethod, az iktat�shoz kell (EREC_IraIratokService haszn�lja egy lez�rt �gyirat utols� darabj�nak lek�r�s�hez)
    /// <summary>
    /// Visszaadja egy �gyirat utols� �gyiratdarabj�t
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="UgyUgyirat_Id"></param>
    /// <returns></returns>
    public Result GetUtolsoLezartUgyUgyiratDarabByUgyUgyirat(ExecParam ExecParam, String UgyUgyirat_Id)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();

            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = UgyUgyirat_Id;
            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;

            erec_UgyUgyiratdarabokSearch.OrderBy = "LetrehozasIdo DESC";
            erec_UgyUgyiratdarabokSearch.TopRow = 1;

            erec_UgyUgyiratdarabokSearch.Allapot.Value = KodTarak.UGYIRATDARAB_ALLAPOT.Lezart;
            erec_UgyUgyiratdarabokSearch.Allapot.Operator = Query.Operators.equals;

            result = sp.GetAll(ExecParam, erec_UgyUgyiratdarabokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                //return result;

                throw new ResultException(result);
            }
            else
            {

                System.Data.DataSet ds = result.Ds;
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        //return ResultError.CreateNewResultWithErrorCode(51102);

                        throw new ResultException(51102);
                    }
                    else
                    {
                        String Id = ds.Tables[0].Rows[0]["Id"].ToString();

                        // az objektumnak a lek�r�se az Id alapj�n:
                        EREC_UgyUgyiratdarabokService erec_UgyUgyiratdarabokService = new EREC_UgyUgyiratdarabokService(this.dataContext);

                        ExecParam execParam_Get = ExecParam.Clone();
                        execParam_Get.Record_Id = Id;

                        result = erec_UgyUgyiratdarabokService.Get(execParam_Get);

                        if (!String.IsNullOrEmpty(result.ErrorCode))
                        {
                            throw new ResultException(result);
                        }
                    }
                }
                else
                {
                    // Nem volt hiba, viszont nincs tal�lat (Result.Record == null lesz)
                    result = new Result();
                }

            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    // DEPRECATED (2007.11.26.)
    /// <summary>
    /// �gyiratdarab elint�zett� nyilv�n�t�sa; Az �gyiratdarab elint�z�s�vel a felettes �gyirat is elint�zett� v�lik
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratdarabok_Id"></param>
    /// <param name="elintezesDatuma"></param>
    /// <param name="elintezesMod"></param>
    /// <param name="erec_UgyKezFeljegyzesek"></param>
    /// <returns></returns>
    //[WebMethod()]
    //[System.Xml.Serialization.XmlInclude(typeof(EREC_UgyKezFeljegyzesek))]
    //public Result ElintezetteNyilvanitas(ExecParam execParam, String erec_UgyUgyiratdarabok_Id, String elintezesDatuma
    //        , String elintezesMod, EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
    //    // Param�terek ellen�rz�se:
    //    if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
    //        || String.IsNullOrEmpty(erec_UgyUgyiratdarabok_Id))
    //    {
    //        // hiba
    //        Result result1 = ResultError.CreateNewResultWithErrorCode(52250);
    //        log.WsEnd(execParam, result1);
    //        return result1;
    //    }

    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


    //        // �gyiratdarab lek�r�se:
    //        EREC_UgyUgyiratdarabokService service_UgyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
    //        ExecParam execParam_ugyiratDarabGet = execParam.Clone();
    //        execParam_ugyiratDarabGet.Record_Id = erec_UgyUgyiratdarabok_Id;

    //        Result result_ugyiratDarabGet = service_UgyiratDarabok.Get(execParam_ugyiratDarabGet);
    //        if (!String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
    //        {
    //            // hiba:
    //            //log.WsEnd(execParam_ugyiratDarabGet, result_ugyiratDarabGet);
    //            //return result_ugyiratDarabGet;

    //            throw new ResultException(result_ugyiratDarabGet);
    //        }
    //        else
    //        {
    //            if (result_ugyiratDarabGet.Record == null)
    //            {
    //                // hiba:
    //                //Result result1 = ResultError.CreateNewResultWithErrorCode(52251);
    //                //log.WsEnd(execParam_ugyiratDarabGet, result1);
    //                //return result1;

    //                throw new ResultException(52251);
    //            }

    //            EREC_UgyUgyiratdarabok erec_UgyUgyiratdarab = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;

    //            // Felettes �gyirat lek�r�se:
    //            EREC_UgyUgyiratokService service_Ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);
    //            ExecParam execParam_ugyiratGet = execParam.Clone();
    //            execParam_ugyiratGet.Record_Id = erec_UgyUgyiratdarab.UgyUgyirat_Id;

    //            Result result_UgyiratGet = service_Ugyiratok.Get(execParam_ugyiratGet);
    //            if (!String.IsNullOrEmpty(result_UgyiratGet.ErrorCode))
    //            {
    //                // hiba:
    //                //log.WsEnd(execParam_ugyiratGet, result_UgyiratGet);
    //                //return result_UgyiratGet;

    //                throw new ResultException(result_UgyiratGet);
    //            }
    //            else
    //            {
    //                if (result_UgyiratGet.Record == null)
    //                {
    //                    // hiba:
    //                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52252);
    //                    //log.WsEnd(execParam_ugyiratGet, result1);
    //                    //return result1;

    //                    throw new ResultException(52252);
    //                }

    //                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_UgyiratGet.Record;

    //                // Elint�zhet�s�g ellen�rz�se:
    //                Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz ugyiratDarabStatusz =
    //                    Contentum.eRecord.BaseUtility.UgyiratDarabok.GetAllapotByBusinessDocument(erec_UgyUgyiratdarab);

    //                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
    //                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);

    //                ExecParam execParam_ellenorzeshez = execParam.Clone();

    //                if (Contentum.eRecord.BaseUtility.Ugyiratok.ElintezetteNyilvanithato(
    //                        ugyiratStatusz, execParam_ellenorzeshez) == false
    //                    || Contentum.eRecord.BaseUtility.UgyiratDarabok.ElintezetteNyilvanithato(
    //                        ugyiratDarabStatusz, ugyiratStatusz, execParam) == false)
    //                {
    //                    // Nem lehet elint�zett� nyilv�n�tani:
    //                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52253);
    //                    //log.WsEnd(execParam_ellenorzeshez, result1);
    //                    //return result1;

    //                    throw new ResultException(52253);
    //                }
    //                else
    //                {
    //                    // �gyiratDarab UPDATE:
    //                    erec_UgyUgyiratdarab.Updated.SetValueAll(false);
    //                    erec_UgyUgyiratdarab.Base.Updated.SetValueAll(false);
    //                    erec_UgyUgyiratdarab.Base.Updated.Ver = true;

    //                    /// Amit m�dos�tani kell:
    //                    /// ElintezesDat, ElintezesMod, Allapot
    //                    /// 

    //                    try
    //                    {
    //                        DateTime dateTime_elintezes = DateTime.Parse(elintezesDatuma);

    //                        erec_UgyUgyiratdarab.ElintezesDat = dateTime_elintezes.ToString();
    //                        erec_UgyUgyiratdarab.Updated.ElintezesDat = true;
    //                    }
    //                    catch
    //                    {
    //                        // ha nem j� form�tum� a d�tum, be�ll�tjuk a mostani id�re:
    //                        erec_UgyUgyiratdarab.ElintezesDat = DateTime.Now.ToString();
    //                        erec_UgyUgyiratdarab.Updated.ElintezesDat = true;
    //                    }

    //                    if (!String.IsNullOrEmpty(elintezesMod))
    //                    {
    //                        erec_UgyUgyiratdarab.ElintezesMod = elintezesMod;
    //                        erec_UgyUgyiratdarab.Updated.ElintezesMod = true;
    //                    }

    //                    // �llapot be�ll�t�sa elint�zettre:
    //                    erec_UgyUgyiratdarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Elintezette_nyilvanitott;
    //                    erec_UgyUgyiratdarab.Updated.Allapot = true;

    //                    ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
    //                    execParam_ugyiratDarabUpdate.Record_Id = erec_UgyUgyiratdarab.Id;

    //                    Result result_UgyiratDarabUpdate =
    //                        service_UgyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_UgyUgyiratdarab);

    //                    if (!String.IsNullOrEmpty(result_UgyiratDarabUpdate.ErrorCode))
    //                    {
    //                        // hiba:
    //                        throw new ResultException(result_UgyiratDarabUpdate);
    //                    }
    //                    else
    //                    {
    //                        // �gyirat update:

    //                        erec_UgyUgyirat.Updated.SetValueAll(false);
    //                        erec_UgyUgyirat.Base.Updated.SetValueAll(false);
    //                        erec_UgyUgyirat.Base.Updated.Ver = true;

    //                        // ElintezesDat:
    //                        erec_UgyUgyirat.ElintezesDat = erec_UgyUgyiratdarab.ElintezesDat;
    //                        erec_UgyUgyirat.Updated.ElintezesDat = true;

    //                        //�llapot:
    //                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Elintezett;
    //                        erec_UgyUgyirat.Updated.Allapot = true;

    //                        ExecParam execParam_UgyiratUpdate = execParam.Clone();
    //                        execParam_UgyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

    //                        Result result_ugyiratUpdate =
    //                            service_Ugyiratok.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);

    //                        if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
    //                        {
    //                            // hiba:
    //                            throw new ResultException(result_ugyiratUpdate);
    //                        }
    //                        else
    //                        {
    //                            // Kezel�si feljegyz�s INSERT, ha megadt�k
    //                            if (erec_UgyKezFeljegyzesek != null &&
    //                                (!String.IsNullOrEmpty(erec_UgyKezFeljegyzesek.KezelesTipus)
    //                                 || !String.IsNullOrEmpty(erec_UgyKezFeljegyzesek.Leiras))
    //                                )
    //                            {
    //                                EREC_UgyKezFeljegyzesekService erec_UgyKezFeljegyzesekService = new EREC_UgyKezFeljegyzesekService(this.dataContext);
    //                                ExecParam execParam_kezFeljInsert = execParam.Clone();

    //                                // �gyirathoz k�t�s
    //                                erec_UgyKezFeljegyzesek.UgyUgyirat_Id = erec_UgyUgyirat.Id;
    //                                erec_UgyKezFeljegyzesek.Updated.UgyUgyirat_Id = true;

    //                                Result result_kezFeljInsert = erec_UgyKezFeljegyzesekService.Insert(
    //                                    execParam_kezFeljInsert, erec_UgyKezFeljegyzesek);
    //                                if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
    //                                {
    //                                    // hiba:
    //                                    throw new ResultException(result_kezFeljInsert);
    //                                }
    //                            }

    //                            result = result_UgyiratDarabUpdate;

    //                        }
    //                    }
    //                }
    //            }
    //        }

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(execParam, result);
    //    return result;

    //}



    //// DEPRECATED (2007.11.26.)
    //[WebMethod()]
    //public Result Lezaras(ExecParam execParam, String erec_UgyUgyiratdarabok_Id, String lezarasDatuma, String lezarasOka)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
    //    // Param�terek ellen�rz�se:
    //    if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
    //        || String.IsNullOrEmpty(erec_UgyUgyiratdarabok_Id))
    //    {
    //        // hiba
    //        Result result1 = ResultError.CreateNewResultWithErrorCode(52370);
    //        log.WsEnd(execParam, result1);
    //        return result1;
    //    }

    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            

    //        // �gyiratdarab lek�r�se:
    //        EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
    //        ExecParam execParam_UgyiratDarabGet = execParam.Clone();
    //        execParam_UgyiratDarabGet.Record_Id = erec_UgyUgyiratdarabok_Id;

    //        Result result_ugyiratDarabGet = service_ugyiratDarabok.Get(execParam_UgyiratDarabGet);
    //        if (!String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
    //        {
    //            // hiba:
    //            //log.WsEnd(execParam_UgyiratDarabGet, result_ugyiratDarabGet);
    //            //return result_ugyiratDarabGet;

    //            throw new ResultException(result_ugyiratDarabGet);
    //        }
    //        else
    //        {
    //            if (result_ugyiratDarabGet.Record == null)
    //            {
    //                // hiba
    //                //Result result1 = ResultError.CreateNewResultWithErrorCode(52372);
    //                //log.WsEnd(execParam_UgyiratDarabGet, result1);
    //                //return result1;

    //                throw new ResultException(52372);
    //            }

    //            EREC_UgyUgyiratdarabok erec_UgyUgyiratDarab = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;

    //            ExecParam execParam_lezarhato = execParam.Clone();
    //            Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz ugyiratDarabStatusz =
    //                Contentum.eRecord.BaseUtility.UgyiratDarabok.GetAllapotByBusinessDocument(erec_UgyUgyiratDarab);
    //            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
    //                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotById(ugyiratDarabStatusz.Ugyirat_Id, execParam_lezarhato, null);

    //            // Ellen�rz�s, hogy lez�rhat�-e az �gyiratdarab:
    //            if (Contentum.eRecord.BaseUtility.UgyiratDarabok.Lezarhato(
    //                ugyiratDarabStatusz, ugyiratStatusz, execParam_lezarhato) == false)
    //            {
    //                // Nem z�rhat� le az �gyirat:
    //                //Result result1 = ResultError.CreateNewResultWithErrorCode(52371);
    //                //log.WsEnd(execParam_lezarhato, result1);
    //                //return result1;

    //                throw new ResultException(52371);
    //            }
    //            else
    //            {
    //                // �gyiratDarab lez�r�sa:
    //                // �gyiratDarab UPDATE:
    //                erec_UgyUgyiratDarab.Updated.SetValueAll(false);
    //                erec_UgyUgyiratDarab.Base.Updated.SetValueAll(false);
    //                erec_UgyUgyiratDarab.Base.Updated.Ver = true;

    //                /// Amit m�dos�tani kell:
    //                /// LezarasDat, LezarasOka, Allapot
    //                /// 

    //                try
    //                {
    //                    DateTime dateTime_lezaras = DateTime.Parse(lezarasDatuma);

    //                    erec_UgyUgyiratDarab.LezarasDat = dateTime_lezaras.ToString();
    //                    erec_UgyUgyiratDarab.Updated.LezarasDat = true;
    //                }
    //                catch
    //                {
    //                    // ha nem j� form�tum� a d�tum, be�ll�tjuk a mostani id�re:
    //                    erec_UgyUgyiratDarab.LezarasDat = DateTime.Now.ToString();
    //                    erec_UgyUgyiratDarab.Updated.LezarasDat = true;
    //                }

    //                erec_UgyUgyiratDarab.LezarasOka = lezarasOka;
    //                erec_UgyUgyiratDarab.Updated.LezarasOka = true;

    //                erec_UgyUgyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Lezart;
    //                erec_UgyUgyiratDarab.Updated.Allapot = true;

    //                ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
    //                execParam_ugyiratDarabUpdate.Record_Id = erec_UgyUgyiratDarab.Id;

    //                Result result_ugyiratDarabUpdate = service_ugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_UgyUgyiratDarab);
    //                if (!String.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
    //                {
    //                    // hiba:
    //                    //ContextUtil.SetAbort();
    //                    //log.WsEnd(execParam_ugyiratDarabUpdate, result_ugyiratDarabUpdate);
    //                    //return result_ugyiratDarabUpdate;

    //                    throw new ResultException(result_ugyiratDarabUpdate);
    //                }
    //                else
    //                {

    //                    // ha minden OK:
    //                    result = result_ugyiratDarabUpdate;

    //                }
    //            }
    //        }

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    log.WsEnd(execParam, result);
    //    return result;
    //}



    /// <summary>
    /// A megadott �gyiratban l�trehoz egy �j �gyiratdarabot a megadott n�vvel
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="EREC_UgyUgyiratok_Id"></param>
    /// <param name="UgyiratDarab_Azonosito"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result CreateNewUgyiratDarab(ExecParam execParam, string EREC_UgyUgyiratok_Id, string UgyiratDarab_Azonosito)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Info("�j �gyiratdarab l�trehoz�sa ide", execParam);

        // Param�ter ellen�rz�s:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(EREC_UgyUgyiratok_Id)
            )
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52540);
        }

        if (String.IsNullOrEmpty(UgyiratDarab_Azonosito))
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52541);
        }


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ellen�rz�s

            // �gyirat lek�r�se (m�dos�t�s jog ellen�rz�ssel:)
            EREC_UgyUgyiratokService service_ugyirat = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam execParam_ugyiratGet = execParam.Clone();
            execParam_ugyiratGet.Record_Id = EREC_UgyUgyiratok_Id;

            Result result_ugyiratGet = service_ugyirat.GetWithRightCheck(execParam_ugyiratGet, 'I');
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }

            EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok) result_ugyiratGet.Record;

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
            ErrorDetails errorDetail = null;

            if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetUjUgyiratDarabotLetrehozni(ugyiratStatusz, execParam, out errorDetail) == false)
            {
                // Nem lehet �j �gyiratdarabot l�trehozni
                throw new ResultException(52542, errorDetail);
            }
                
            #endregion

            #region �gyiratDarab INSERT:

            EREC_UgyUgyiratdarabok erec_UgyUgyiratDarabok = new EREC_UgyUgyiratdarabok();

            // �gyirathoz kapcsol�s:
            erec_UgyUgyiratDarabok.UgyUgyirat_Id = erec_UgyUgyiratok.Id;
            erec_UgyUgyiratDarabok.Updated.UgyUgyirat_Id = true;

            // Azonos�t�:
            erec_UgyUgyiratDarabok.Azonosito = UgyiratDarab_Azonosito;
            erec_UgyUgyiratDarabok.Updated.Azonosito = true;

            // �llapot:
            erec_UgyUgyiratDarabok.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
            erec_UgyUgyiratDarabok.Updated.Allapot = true;

            // Elj�r�si szakasz:
            // Mindenk�ppen be kell valamit �ll�tani, mert k�l�nben default �gyiratdarabnak fogjuk tekinteni
            erec_UgyUgyiratDarabok.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Altalanos;
            erec_UgyUgyiratDarabok.Updated.EljarasiSzakasz = true;


            // Iktat�k�nyv, f�sz�m be�ll�t�sa ugyanarra, mint az �gyiratn�l:
            erec_UgyUgyiratDarabok.IraIktatokonyv_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;
            erec_UgyUgyiratDarabok.Updated.IraIktatokonyv_Id = true;

            erec_UgyUgyiratDarabok.Foszam = erec_UgyUgyiratok.Foszam;
            erec_UgyUgyiratDarabok.Updated.Foszam = true;


            ExecParam execParam_UgyiratDarabInsert = execParam.Clone();
            Result result_ugyiratdarabInsert = this.Insert(execParam_UgyiratDarabInsert, erec_UgyUgyiratDarabok);

            if (!String.IsNullOrEmpty(result_ugyiratdarabInsert.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratdarabInsert);
            }
            
            #endregion

            result = result_ugyiratdarabInsert;


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Irat �thelyez�se m�sik �gyiratdarabba
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="EREC_IraIratok_Id"></param>
    /// <param name="EREC_UgyUgyiratDarabok_Id_Cel"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result IratAthelyezeseUgyiratDarabba(ExecParam execParam, string EREC_IraIratok_Id, string EREC_UgyUgyiratDarabok_Id_Cel)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Info("Irat �thelyez�se m�sik �gyiratdarabba", execParam);

        // Param�ter ellen�rz�s:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(EREC_IraIratok_Id)
            || String.IsNullOrEmpty(EREC_UgyUgyiratDarabok_Id_Cel)
            )
        {
            // hiba:
            return ResultError.CreateNewResultWithErrorCode(52560);
        }


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ellen�rz�s:
            // Irat �gyirat�nak �s a c�l �gyiratdarab �gyirat�nak MEG KELL EGYEZNIE:
                        

            // C�l �gyiratdarab Get:
            ExecParam execParam_celUgyiratDarabGet = execParam.Clone();
            execParam_celUgyiratDarabGet.Record_Id = EREC_UgyUgyiratDarabok_Id_Cel;

            Result result_celUGyiratDarabGet = this.Get(execParam_celUgyiratDarabGet);
            if (!String.IsNullOrEmpty(result_celUGyiratDarabGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_celUGyiratDarabGet);
            }

            EREC_UgyUgyiratdarabok erec_UgyUgyiratDarab_Cel = (EREC_UgyUgyiratdarabok)result_celUGyiratDarabGet.Record;


            // forr�s Irat GET:
            EREC_IraIratok src_Irat = null;
            EREC_UgyUgyiratdarabok src_UgyiratDarab = null;
            EREC_UgyUgyiratok src_Ugyirat = null;

            EREC_IraIratokService service_irat = new EREC_IraIratokService(this.dataContext);

            Result result_objHierGet = service_irat.GetObjectsHierarchyByIrat(execParam, EREC_IraIratok_Id,out src_Irat,out src_UgyiratDarab,out src_Ugyirat);
            if (!String.IsNullOrEmpty(result_objHierGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_objHierGet);
            }

            // forr�s �gyirat egyezik-e a c�llal?
            if (src_Ugyirat.Id != erec_UgyUgyiratDarab_Cel.UgyUgyirat_Id)
            {
                // hiba:
                throw new ResultException(52561);
            }

            // �gyirat nyitott �llapotban van-e?
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(src_Ugyirat);

            if (Contentum.eRecord.BaseUtility.Ugyiratok.Nyitott(ugyiratStatusz) == false)
            {
                // nem helyezhet� �t ide:
                Logger.Error("Az irat nem helyezhet� �t ebben az �gyiratba (�gyirat �llapota miatt):" + src_Ugyirat.Id,execParam);
                throw new ResultException(52562);
            }


            // Minden ok:
            #region Irat UPDATE:
            
            src_Irat.Updated.SetValueAll(false);
            src_Irat.Base.Updated.SetValueAll(false);
            src_Irat.Base.Updated.Ver = true;
            
            // c�l �gyiratdarabot kell m�dos�tani
            src_Irat.UgyUgyIratDarab_Id = erec_UgyUgyiratDarab_Cel.Id;
            src_Irat.Updated.UgyUgyIratDarab_Id = true;

            // ha v�ltozott az elj�r�si szakasz, az iratt�pusf�gg� metaadat hozz�rendel�seket is m�dos�tani kell majd
            if (src_UgyiratDarab.EljarasiSzakasz != erec_UgyUgyiratDarab_Cel.EljarasiSzakasz)
            {
                src_Irat.Updated.Irattipus = true;
            }

            ExecParam execParam_iratUpdate = execParam.Clone();
            execParam_iratUpdate.Record_Id = src_Irat.Id;

            Result result_iratUpdate = service_irat.Update(execParam_iratUpdate, src_Irat);
            if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_iratUpdate);
            }

            #endregion

            result = result_iratUpdate;

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    




    #region Seg�delj�r�sok

    /// <summary>
    /// A megadott �gyirat default, nem megjelen�tend� �gyirat�t adja meg
    /// NEM WEBMETHOD, seg�delj�r�s
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyirat_Id"></param>
    /// <returns></returns>
    public Result GetDefaultUgyiratDarab(ExecParam execParam, string ugyirat_Id)
    {
        Logger.Info("Default �gyiratdarab lek�rdez�se", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            EREC_UgyUgyiratdarabokSearch search = new EREC_UgyUgyiratdarabokSearch();

            // default �gyiratdarab: ahol nincs megadva elj�r�si szakasz
            // Elvileg ebb�l csak egy lehet. Ha t�bb van, visszaadjuk az els�t,
            // ha nincs, nem dobunk hib�t, majd fels�bb szinten lekezelik.

            // �gyirat hozz�k�t�se:
            search.UgyUgyirat_Id.Value = ugyirat_Id;
            search.UgyUgyirat_Id.Operator = Query.Operators.equals;

            //// elj�r�si szakasz IS NULL
            //search.EljarasiSzakasz.Value = "";
            //search.EljarasiSzakasz.Operator = Query.Operators.isnull;

            // CR#1080: Elj�r�si szakasz: Osztatlan
            search.EljarasiSzakasz.Value = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
            search.EljarasiSzakasz.Operator = Query.Operators.equals;

            search.OrderBy = "EREC_UgyUgyiratdarabok.LetrehozasIdo";

            Result resultGetAll = this.GetAll(execParam, search);
            if (!String.IsNullOrEmpty(resultGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(resultGetAll);
            }

            int talalat = resultGetAll.Ds.Tables[0].Rows.Count;

            if (talalat > 0)
            {
                Logger.Debug("�gyiratDarabok lek�rdez�se: Tal�lat: " + talalat, execParam);
 
                string ugyiratDarab_Id = resultGetAll.Ds.Tables[0].Rows[0]["Id"].ToString();

                // �gyiratDarab GET:
                ExecParam execParam_ugyiratDarabGet = execParam.Clone();
                execParam_ugyiratDarabGet.Record_Id = ugyiratDarab_Id;

                Result result_ugyiratDarabGet = this.Get(execParam_ugyiratDarabGet);

                result = result_ugyiratDarabGet;

            }
            else
            {
                // nincs tal�lat (de hiba sem volt)
                Logger.Warn("Nincs tal�lat", execParam);

                result.Record = null;
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    
    #endregion
}