﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;

/// <summary>
/// Summary description for EREC_UgyUgyiratokServiceVol1
/// </summary>
public partial class EREC_UgyUgyiratokService
{
    #region WEBMETHODS

    /// <summary>
    ///  A lezárásokat a webservice ellenőrzi és ennek az évnek az utolsó napjára állítja a lezárást, ha szükséges.
    /// </summary>
    /// <param name="FelhasznaloId"></param>
    /// <param name="FelhasznaloSzervezetId"></param>
    /// <param name="Ev"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public string UgyiratLezarasEllenorzesSimple(string FelhasznaloId, string FelhasznaloSzervezetId, int Ev)
    {
        ExecParam xpm = GetExecParam(FelhasznaloId, FelhasznaloSzervezetId);
        return UgyiratLezarasEllenorzes(xpm, Ev);
    }

    /// <summary>
    ///  A lezárásokat a webservice ellenőrzi és ennek az évnek az utolsó napjára állítja a lezárást, ha szükséges.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="Ev"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public string UgyiratLezarasEllenorzes(ExecParam execParam, int Ev)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        string result;
        if (execParam == null || string.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            throw new ArgumentException("Paraméter hiba: Execparam");
        }
        if (Ev < 1900 || Ev > 2100)
        {
            throw new ArgumentException("Paraméter hiba: Ev");
        }

        bool isConnectionOpenHere = false;

        DateTime evUtolsoNapja = new DateTime(Ev, 12, 31);

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region FIND ITEMS

            EREC_UgyUgyiratokSearch searchUgy = new EREC_UgyUgyiratokSearch();

            searchUgy.LezarasDat.Value = evUtolsoNapja.ToString();
            searchUgy.LezarasDat.Operator = Query.Operators.greater;

            if (searchUgy.Extended_EREC_IraIktatoKonyvekSearch == null)
            {
                searchUgy.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
            }
            searchUgy.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value = evUtolsoNapja.Year.ToString();
            searchUgy.Extended_EREC_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;

            Result resultUgy = sp.GetAllWithExtension(execParam, searchUgy);
            resultUgy.CheckError();

            if (resultUgy.GetCount == 0) { return "OK"; }

            #endregion FIND ITEMS

            EREC_UgyUgyiratok tmpUgy;
            string iktatasEv;
            for (int i = 0; i < resultUgy.GetCount; i++)
            {
                tmpUgy = GetUgyBo(resultUgy.GetRows()[i]);
                if (resultUgy.GetRows()[i].Table.Columns.Contains("Ev"))
                {
                    iktatasEv = resultUgy.GetRows()[i]["Ev"].ToString();
                }
                else
                {
                    continue;
                }

                if (tmpUgy.Allapot == KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                {
                    SetSzereltUgyLezarasEsMegorzesiIdo(execParam, ref tmpUgy);
                }
                else if (IsSzerelesiLancTagja(execParam.Clone(), tmpUgy.Id))
                {
                    SetSzereltUgyLezarasEsMegorzesiIdo(execParam, ref tmpUgy);
                }
                else
                {
                    SetNemSzereltUgyLezarasEsMegorzesiIdo(execParam, tmpUgy, iktatasEv);
                }
            }

            result = string.Format("OK ! COUNT:{0} ", resultUgy.GetCount);
        }
        catch (ResultException e)
        {
            result = string.Format("NOK: {1} {0}", e.ErrorMessage, e.ErrorCode);
        }
        catch (Exception e)
        {
            result = string.Format("NOK: {0}", e.Message);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam);
        return result;
    }

    #endregion WEBMETHODS

    #region LEZARAS+MEGORZESI IDO

    /// <summary>
    /// SetNemSzereltUgyLezarasEsMegorzesiIdo
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="tmpUgy"></param>
    /// <param name="iktatasEv"></param>
    private void SetNemSzereltUgyLezarasEsMegorzesiIdo(ExecParam execParam, EREC_UgyUgyiratok tmpUgy, string iktatasEv)
    {
        tmpUgy.LezarasDat = GetAdottEvUtolsoNapja(iktatasEv).ToString();
        tmpUgy.Updated.LezarasDat = true;

        if (!string.IsNullOrEmpty(tmpUgy.LezarasDat))
        {
            Result resSetMegorzesiIdo = SetMegorzesiIdoFromIrattariTetel(execParam, tmpUgy, tmpUgy.IraIrattariTetel_Id);
            if (!string.IsNullOrEmpty(resSetMegorzesiIdo.ErrorCode))
            {
                throw new ResultException(resSetMegorzesiIdo);
            }
            UpdateLezarasEsMegorzesiIdoFromBOUgyirat(execParam, tmpUgy);
        }
    }

    /// <summary>
    /// SetSzereltUgyLezarasEsMegorzesiIdo
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="evUtolsoNapja"></param>
    /// <param name="listUgy"></param>
    private void SetSzereltUgyLezarasEsMegorzesiIdo(ExecParam execParam, ref EREC_UgyUgyiratok baseUgy)
    {
        #region SZERELESI LANC LEKERESE

        Result resultUgySzerelesiLanc = GetSzerelesiLanc(execParam.Clone(), baseUgy.Id);

        #endregion SZERELESI LANC LEKERESE

        #region BO

        string tmpIktatasiEv = null;
        List<EREC_UgyUgyiratok> lanc = new List<EREC_UgyUgyiratok>();
        Dictionary<string, string> ugyiratIktatasiEv = new Dictionary<string, string>();

        EREC_UgyUgyiratok tmpSzereltUgy;
        for (int i = 0; i < resultUgySzerelesiLanc.GetCount; i++)
        {
            tmpSzereltUgy = GetUgyBo(execParam, resultUgySzerelesiLanc.GetRows()[i], out tmpIktatasiEv);
            lanc.Add(tmpSzereltUgy);
            ugyiratIktatasiEv.Add(tmpSzereltUgy.Id, tmpIktatasiEv);
        }

        #endregion BO

        #region UTOLSO SZERELT LEZARHATO ?

        EREC_UgyUgyiratok utolsoSzereltUgy = GetUtolsoSzerelt(lanc);
        if (!IsUgyLezarhato(execParam.Clone(), utolsoSzereltUgy))
        {
            return;
        }

        #endregion UTOLSO SZERELT LEZARHATO ?

        #region SET LEZARAS DATUMA

        for (int i = 0; i < lanc.Count; i++)
        {
            lanc[i].LezarasDat = GetAdottEvUtolsoNapja(ugyiratIktatasiEv.FirstOrDefault(a => a.Key == lanc[i].Id).Value).ToString();
            lanc[i].Updated.LezarasDat = true;
        }

        #endregion SET LEZARAS DATUMA

        #region MEGORZESI IDO

        string szamitasMod = Rendszerparameterek.Get(execParam, Rendszerparameterek.SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA);
        if (string.IsNullOrEmpty(szamitasMod)) { return; }

        switch (szamitasMod)
        {
            case "1":

                #region UTOLSO SZERELT MEGORZESI IDEJE

                utolsoSzereltUgy = SetMegorzesiIdo(execParam, utolsoSzereltUgy);

                #endregion UTOLSO SZERELT MEGORZESI IDEJE

                #region MINDEN SZERELT MEGORZESI IDO = UTOLSO SZERELT MEGORZESI IDO

                for (int i = 0; i < lanc.Count; i++)
                {
                    lanc[i].MegorzesiIdoVege = utolsoSzereltUgy.MegorzesiIdoVege;
                    lanc[i].Updated.MegorzesiIdoVege = true;
                }

                #endregion MINDEN SZERELT MEGORZESI IDO = UTOLSO SZERELT MEGORZESI IDO

                break;

            case "2":

                #region MAX MEGORZESI IDO KERESESE

                EREC_UgyUgyiratok tmp = null;
                DateTime? maxMegorzesiIdo = null;
                DateTime tmpMegorzesiIdo;
                foreach (EREC_UgyUgyiratok entry in lanc)
                {
                    tmp = SetMegorzesiIdo(execParam, entry);

                    if (tmp.MegorzesiIdoVege == null) { continue; }

                    DateTime.TryParse(tmp.MegorzesiIdoVege, out tmpMegorzesiIdo);
                    if (maxMegorzesiIdo == null || maxMegorzesiIdo < tmpMegorzesiIdo)
                    {
                        maxMegorzesiIdo = tmpMegorzesiIdo;
                    }
                }

                #endregion MAX MEGORZESI IDO KERESESE

                #region MEGORZESI IDO BEALLITASA

                if (maxMegorzesiIdo != null)
                {
                    for (int i = 0; i < lanc.Count; i++)
                    {
                        lanc[i].MegorzesiIdoVege = maxMegorzesiIdo.ToString();
                        lanc[i].Updated.MegorzesiIdoVege = true;
                    }
                }

                #endregion MEGORZESI IDO BEALLITASA

                break;
        }

        #endregion MEGORZESI IDO

        UpdateLezarasEsMegorzesiIdoFromBOUgyirat(execParam, lanc);
    }

    #endregion LEZARAS+MEGORZESI IDO

    #region GET

    /// <summary>
    /// GetSzerelesiLanc
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private Result GetSzerelesiLanc(ExecParam execParam, string ugyiratId)
    {
        execParam.Record_Id = ugyiratId;
        Result resultUgySzerelesiLanc = GetAllSzereltByNode(execParam);
        resultUgySzerelesiLanc.CheckError();

        return resultUgySzerelesiLanc;
    }

    private bool IsSzerelesiLancTagja(ExecParam execParam, string ugyiratId)
    {
        execParam.Record_Id = ugyiratId;
        Result resultUgySzerelesiLanc = GetAllSzereltByNode(execParam);
        resultUgySzerelesiLanc.CheckError();
        return resultUgySzerelesiLanc.GetCount > 1 ? true : false;
    }

    ///// <summary>
    ///// GetMegorzesiIdoFromSzerelesiLanc
    ///// </summary>
    ///// <param name="execParam"></param>
    ///// <param name="lanc"></param>
    //private void GetMegorzesiIdoFromSzerelesiLanc(ExecParam execParam, ref List<EREC_UgyUgyiratok> lanc)
    //{
    //    string szamitasMod = Rendszerparameterek.Get(execParam, Rendszerparameterek.SZERELT_UGYIRAT_MEGORZESI_IDO_SZAMITASA);
    //    if (string.IsNullOrEmpty(szamitasMod)) { return; }

    //    switch (szamitasMod)
    //    {
    //        case "1":
    //            List<EREC_UgyUgyiratok> listUgyObjLanc = GetUgyBo(resultUgySzerelesiLanc);
    //            string utolsoSzereltIktatasEveStr = null;
    //            EREC_UgyUgyiratok utolsoSzereltUgy = GetUtolsoSzerelt(execParam, resultUgySzerelesiLanc.GetRows, out utolsoSzereltIktatasEve);
    //            if (utolsoSzereltUgy != null && !string.IsNullOrEmpty(utolsoSzereltIktatasEveStr))
    //            {
    //                DateTime utolsoSzereltIktatasEve = GetAdottEvUtolsoNapja(utolsoSzereltIktatasEveStr);
    //            }
    //            else
    //            {
    //                Logger.Debug("Nem található utolsó szerelt ügyirat ! ");
    //            }
    //            break;

    //        case "2":
    //            break;

    //        default:
    //            return null;
    //    }
    //    return null;
    //}

    /// <summary>
    /// GetUtolsoSzerelt
    /// </summary>
    /// <param name="listLanc"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUtolsoSzerelt(List<EREC_UgyUgyiratok> listLanc)
    {
        if (listLanc == null || listLanc.Count < 1) { return null; }

        return listLanc.FirstOrDefault(s => string.IsNullOrEmpty(s.UgyUgyirat_Id_Szulo));
    }

    /// <summary>
    /// GetUtolsoSzerelt
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="listLanc"></param>
    /// <param name="iktatasEv"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUtolsoSzerelt(ExecParam execParam, DataRowCollection listLanc, out string iktatasEv)
    {
        iktatasEv = null;
        if (listLanc == null || listLanc.Count < 1) { return null; }

        EREC_UgyUgyiratok tmpSzereltUgy;
        for (int i = 0; i < listLanc.Count; i++)
        {
            if (listLanc[i]["UgyUgyirat_Id_Szulo"].ToString() == null)
            {
                tmpSzereltUgy = GetUgyBo(listLanc[i]);
                if (IsUgyLezarhato(execParam.Clone(), tmpSzereltUgy))
                {
                    iktatasEv = listLanc[i]["Ev"].ToString();
                    return tmpSzereltUgy;
                }
            }
        }

        return null;
    }

    #endregion GET

    #region SET

    /// <summary>
    /// SetMegorzesiIdo
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugy"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok SetMegorzesiIdo(ExecParam execParam, EREC_UgyUgyiratok ugy)
    {
        if (!string.IsNullOrEmpty(ugy.LezarasDat))
        {
            Result resSetMegorzesiIdo = SetMegorzesiIdoFromIrattariTetel(execParam, ugy, ugy.IraIrattariTetel_Id);
            if (!string.IsNullOrEmpty(resSetMegorzesiIdo.ErrorCode))
            {
                throw new ResultException(resSetMegorzesiIdo);
            }
        }
        return ugy;
    }

    #endregion SET

    #region HELPER

    /// <summary>
    /// IsUgyLezarhato
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="tmpUgy"></param>
    /// <returns></returns>
    private bool IsUgyLezarhato(ExecParam execParam, EREC_UgyUgyiratok tmpUgy)
    {
        ErrorDetails errorDetail;
        if (tmpUgy == null) { return false; }

        if (tmpUgy.Allapot == KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart)
        { return true; }

        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(tmpUgy);
        execParam.Record_Id = tmpUgy.Id;
        if (!Contentum.eRecord.BaseUtility.Ugyiratok.Lezarhato(ugyiratStatusz, execParam, out errorDetail))
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// GetUgyBo
    /// </summary>
    /// <param name="row"></param>
    /// <param name="iktatasEv"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUgyBo(ExecParam execParam, DataRow row, out string iktatasEv)
    {
        iktatasEv = null;
        if (row == null) { return null; }

        EREC_UgyUgyiratok tmpUgy = GetUgyBo(row);

        if (row.Table.Columns.Contains("Ev"))
        {
            iktatasEv = row["Ev"].ToString();
        }
        else
        {
            Result result = GetUgyWithExtension(execParam, tmpUgy.Id);
            if (result.GetCount > 0)
            {
                if (result.GetRows()[0].Table.Columns.Contains("Ev"))
                {
                    iktatasEv = result.GetRows()[0]["Ev"].ToString();
                }
            }
        }

        return tmpUgy;
    }

    private Result GetUgyWithExtension(ExecParam execParam, string id)
    {
        EREC_UgyUgyiratokSearch searchUgy = new EREC_UgyUgyiratokSearch();

        searchUgy.Id.Value = id;
        searchUgy.Id.Operator = Query.Operators.equals;

        Result resultUgy = sp.GetAllWithExtension(execParam, searchUgy);
        resultUgy.CheckError();

        return resultUgy;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    private List<EREC_UgyUgyiratok> GetUgyBo(Result result)
    {
        List<EREC_UgyUgyiratok> listUgy = new List<EREC_UgyUgyiratok>();
        if (!result.IsError)
        {
            EREC_UgyUgyiratok tmpUgy;
            for (int i = 0; i < result.GetCount; i++)
            {
                tmpUgy = GetUgyBo(result.GetRows()[i]);
                listUgy.Add(tmpUgy);
            }
        }
        return listUgy;
    }

    /// <summary>
    /// GetUgyBo
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUgyBo(DataRow row)
    {
        if (row != null)
        {
            EREC_UgyUgyiratok tmpUgy = new EREC_UgyUgyiratok();
            Utility.LoadBusinessDocumentFromDataRow(tmpUgy, row);
            tmpUgy.Updated.SetValueAll(false);
            tmpUgy.Base.Updated.SetValueAll(false);
            tmpUgy.Base.Updated.Ver = true;
            return tmpUgy;
        }
        return null;
    }

    /// <summary>
    /// GetAdottEvUtolsoNapja
    /// </summary>
    /// <param name="ev"></param>
    /// <returns></returns>
    private DateTime GetAdottEvUtolsoNapja(string ev)
    {
        if (string.IsNullOrEmpty(ev))
            return DateTime.Today.AddYears(1).AddDays(-1);
        return new DateTime(int.Parse(ev), 12, 31);
    }

    private EREC_UgyUgyiratok GetUgyActualVersion(ExecParam xpm, string id)
    {
        xpm.Record_Id = id;
        Result result = Get(xpm);
        result.CheckError();

        EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)result.Record;
        ugy.Updated.SetValueAll(false);
        ugy.Base.Updated.SetValueAll(false);
        ugy.Base.Updated.Ver = true;

        return ugy;
    }
    /// <summary>
    /// UpdateUgyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="list"></param>
    private void UpdateLezarasEsMegorzesiIdoFromBOUgyirat(ExecParam execParam, List<EREC_UgyUgyiratok> list)
    {
        foreach (EREC_UgyUgyiratok item in list)
        {
            UpdateLezarasEsMegorzesiIdoFromBOUgyirat(execParam, item);
        }
    }
    /// <summary>
    /// UpdateLezarasEsMegorzesiIdoFromBOUgyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="item"></param>
    private void UpdateLezarasEsMegorzesiIdoFromBOUgyirat(ExecParam execParam, EREC_UgyUgyiratok item)
    {
        EREC_UgyUgyiratok toSave = GetUgyActualVersion(execParam, item.Id);

        toSave.LezarasDat = item.LezarasDat;
        toSave.Updated.LezarasDat = true;

        toSave.MegorzesiIdoVege = item.MegorzesiIdoVege;
        toSave.Updated.MegorzesiIdoVege = true;

        UpdateBoUgyirat(execParam, toSave);
    }

    /// <summary>
    /// UpdateBoUgyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="entry"></param>
    private void UpdateBoUgyirat(ExecParam execParam, EREC_UgyUgyiratok entry)
    {
        execParam.Record_Id = entry.Id;
        Result resultUpdateUgy = Update(execParam, entry);
        resultUpdateUgy.CheckError();
    }
    /// <summary>
    /// GetExecParam
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="FelhasznaloSzervezetId"></param>
    /// <returns></returns>
    private ExecParam GetExecParam(string felhasznaloId, string FelhasznaloSzervezetId)
    {
        ExecParam ExecParamContentum = new ExecParam();
        ExecParamContentum.Felhasznalo_Id = felhasznaloId;
        ExecParamContentum.FelhasznaloSzervezet_Id = FelhasznaloSzervezetId;
        return ExecParamContentum;
    }

    #endregion HELPER
}