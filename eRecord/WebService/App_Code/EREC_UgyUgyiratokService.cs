﻿//#define IsUpdateAllIraIratIrattarbaAdasVetelDat

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using Contentum.eAdmin.Service;
using System.Collections.Generic;
using System.Reflection;
using Contentum.eMigration.BusinessDocuments;
using Contentum;
using Contentum.eMigration.Service;
using System.Linq;
using System.Data.SqlTypes;
using Newtonsoft.Json;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_UgyUgyiratokService : System.Web.Services.WebService
{
    // CR#2171 (EB): a szignált tétel mozgás közbeni állapotát szabályozó flag
    // - ha értéke false, a korábbi mûködésnek megfelelõen a tétel mozgása közben "Szignált"
    // állapotban marad, és ha az ügyintézõ átveszi (simán, nem ügyintézésre),
    // akkor is "Szignált" állapotban marad
    // - ha értéke true, a tétel Továbbítás alatt (Szignált) állapotba kerül, amíg át nem veszik,
    // így pl. a vezetõ láthatja, hogy a szignált tételt még nem vették át
    private const bool bSzignalasCausesTovabbitasAlattAllapot = false;

    //EREC_IraIratok.IrattarbaKuldDatuma, EREC_IraIratok.IrattarbaVetelDat mezõk töltése

    /// <summary>
    /// CERTOP: hagyományos elintézetté nyilvánítás, amúgy elektronikus irattározás
    /// </summary>
    /// <param name="xpm"></param>
    /// <returns></returns>
    public static bool IsElintezetteNyilvanitasEnabled(ExecParam xpm)
    {
        return Rendszerparameterek.GetBoolean(xpm, Rendszerparameterek.ELINTEZETTE_NYILVANITAS_ENABLED, false);
    }

    //bizonyos esetekben esmenynapló letiltása
    private bool AddEsemenyNaplo = true;

    #region Helper Functions

    public int IncrementUtolsoAlszam(ExecParam execParam, EREC_UgyUgyiratok erec_ugyugyiratok)
    {
        Logger.Debug("IncrementUtolsoAlszam start", execParam);

        int utolsoAlszam = this.GetUtolsoAlszam(execParam, erec_ugyugyiratok);

        Logger.Debug(String.Format("UtolsoAlszam:{0}", utolsoAlszam));

        utolsoAlszam++;
        erec_ugyugyiratok.UtolsoAlszam = utolsoAlszam.ToString();
        erec_ugyugyiratok.Updated.UtolsoAlszam = true;

        Logger.Debug(String.Format("UtolsoAlszam:{0}", utolsoAlszam));

        IncrementIratszam(execParam, erec_ugyugyiratok);

        Logger.Debug("IncrementUtolsoAlszam end", execParam);
        return utolsoAlszam;
    }

    public int GetUtolsoAlszam(ExecParam execParam, EREC_UgyUgyiratok erec_ugyugyiratok)
    {
        Logger.Debug("GetUtolsoAlszam start", execParam);

        int utolsoAlszam;
        if (!erec_ugyugyiratok.Typed.UtolsoAlszam.IsNull &&
        Int32.TryParse(erec_ugyugyiratok.UtolsoAlszam, out utolsoAlszam))
        {
            Logger.Debug("UtolsoAlszam helyesen toltve");
        }
        else
        {
            Logger.Warn("Ügyirat.utolsóalszám nincs megadva --> le kell kérni az ügyirathoz tartozó legnagyobb alszámot", execParam);

            ExecParam execParam_utolsoAlszam = execParam.Clone();
            Result result_utolsoAlszam = this.GetUtolsoAlszam(execParam_utolsoAlszam, erec_ugyugyiratok.Id);
            if (!String.IsNullOrEmpty(result_utolsoAlszam.ErrorCode))
            {
                Logger.Error("Hiba az utolsó alszám lekérdezésekor", execParam);
                throw new ResultException(result_utolsoAlszam);
            }

            string strUtolsoAlszam = result_utolsoAlszam.Record.ToString();

            utolsoAlszam = Int32.Parse(strUtolsoAlszam);

            Logger.Debug("Utolsó alszám lekérdezése end");

        }

        Logger.Debug("GetUtolsoAlszam end", execParam);
        return utolsoAlszam;
    }

    public int IncrementIratszam(ExecParam execParam, EREC_UgyUgyiratok erec_ugyugyiratok)
    {
        Logger.Debug("IncrementIratszam start", execParam);

        int iratszam = this.GetIratszam(execParam, erec_ugyugyiratok);

        Logger.Debug(String.Format("Iratszam:{0}", iratszam));

        iratszam++;
        erec_ugyugyiratok.IratSzam = iratszam.ToString();
        erec_ugyugyiratok.Updated.IratSzam = true;

        Logger.Debug(String.Format("Iratszam:{0}", iratszam));

        Logger.Debug("IncrementIratszam end", execParam);
        return iratszam;

    }

    public int DecrementIratszam(ExecParam execParam, string ugyiratId)
    {
        Logger.Debug("DecrementIratszam start", execParam);
        Logger.Debug(String.Format("UgyiratId: {0}", ugyiratId ?? "NULL"));

        Arguments args = new Arguments();
        args.Add(new Argument("Ügyirat Id", ugyiratId, ArgumentTypes.Guid));
        args.ValidateArguments();

        Logger.Debug("Ugyirat lekerese start");
        ExecParam xpmGet = execParam.Clone();
        xpmGet.Record_Id = ugyiratId;

        Result resGet = this.Get(xpmGet);
        if (resGet.IsError)
        {
            Logger.Error("Ugyirat lekerese hiba", xpmGet, resGet);
            throw new ResultException(resGet);
        }

        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)resGet.Record;

        Logger.Debug("Ugyirat lekerese end");

        ugyirat.Updated.SetValueAll(false);
        ugyirat.Base.Updated.SetValueAll(false);
        ugyirat.Base.Updated.Ver = true;

        int ujIratszam = this.DecrementIratszam(execParam, ugyirat);

        Logger.Debug(String.Format("Uj Iratszam: {0}", ujIratszam));

        Logger.Debug("UgyiratUpdate start");

        ExecParam xpmUpdate = xpmGet.Clone();
        Result resUpdate = this.Update(xpmUpdate, ugyirat);

        if (resUpdate.IsError)
        {
            Logger.Error("UgyiratUpdate hiba", xpmUpdate, resUpdate);
            throw new ResultException(resUpdate);
        }

        Logger.Debug("UgyiratUpdate end");

        Logger.Debug("DecrementIratszam end", execParam);
        return ujIratszam;
    }

    public int DecrementIratszam(ExecParam execParam, EREC_UgyUgyiratok erec_ugyugyiratok)
    {
        Logger.Debug("DecrementIratszam start", execParam);

        int iratszam = this.GetIratszam(execParam, erec_ugyugyiratok);

        Logger.Debug(String.Format("Iratszam:{0}", iratszam));

        iratszam--;
        if (iratszam < 0)
        {
            Logger.Warn("Iratszam kisebb mint nulla: " + iratszam);
            iratszam = 0;
        }
        erec_ugyugyiratok.IratSzam = iratszam.ToString();
        erec_ugyugyiratok.Updated.IratSzam = true;

        Logger.Debug(String.Format("Iratszam:{0}", iratszam));

        Logger.Debug("DecrementIratszam end", execParam);
        return iratszam;

    }

    public int GetIratszam(ExecParam execParam, EREC_UgyUgyiratok erec_ugyugyiratok)
    {
        Logger.Debug("GetIratszam start", execParam);

        int iratszam;
        if (!erec_ugyugyiratok.Typed.IratSzam.IsNull &&
            Int32.TryParse(erec_ugyugyiratok.IratSzam, out iratszam))
        {
            //Az ügyiratban helyesen töltve van az iratszám mezõ
            Logger.Debug("Iratszam helyesen toltve");
        }
        else
        {
            Logger.Warn("Ugyirat.Iratszam nincs megadva --> le kell kerni adatbazisbol", execParam);

            ExecParam execParam_iratszam = execParam.Clone();
            Result result_iratszam = this.GetIratszam(execParam_iratszam, erec_ugyugyiratok.Id);

            if (!String.IsNullOrEmpty(result_iratszam.ErrorCode))
            {
                Logger.Error("Hiba az iratszam lekérdezésekor", execParam);
                throw new ResultException(result_iratszam);
            }

            string strIratszam = result_iratszam.Record.ToString();

            iratszam = Int32.Parse(strIratszam);

            Logger.Debug("Iratszam lekérdezése end");
        }

        Logger.Debug("GetIratszam end", execParam);
        return iratszam;
    }

    // BLG_619
    [WebMethod]
    public Result GetUgyFelelosSzervezetKod(ExecParam execParam, string ugyiratId)
    {
        Logger.Debug("Erec_UgyUgyiratokService GetUgyFelelosSzervezetKod start");
        Logger.Debug(String.Format("UgyiratId: {0}", ugyiratId));
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetUgyFelelosSzervezetKod(execParam, ugyiratId);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error("Erec_UgyUgyiratokService GetUgyFelelosSzervezetKod hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("Erec_UgyUgyiratokService GetUgyFelelosSzervezetKod end");

        return result;
    }
    #endregion

    #region Generáltból átvettek (Insert, Update)

    /// <summary>
    /// Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza.
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result Insert(ExecParam ExecParam, EREC_UgyUgyiratok Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            /// Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
            /// Kézbesítési tételt kell létrehozni, ha be van állítva a felelõs, és az nem egyezik a felhasználó egyszemélyes csoportjával
            /// 

            bool kellKezbesitesiTetel = false;

            // ki van-e töltve a felelõs mezõ?
            if (Record.Updated.Csoport_Id_Felelos && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos))
            {
                // ha nem a felhasználó saját csoportja:
                if (Csoportok.GetFelhasznaloSajatCsoportId(ExecParam) != Record.Csoport_Id_Felelos)
                {
                    kellKezbesitesiTetel = true;
                }
            }

            // ha kézbesítési tételt is kell csinálni, akkor továbbítás alatti állapotot kell az ügyiratnak beállítani
            if (kellKezbesitesiTetel)
            {
                Record.TovabbitasAlattAllapot = Record.Allapot;
                Record.Updated.TovabbitasAlattAllapot = true;

                Record.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                Record.Updated.Allapot = true;

                //Record.Csoport_Id_Felelos_Elozo = Record.Csoport_Id_Felelos;
                Record.Csoport_Id_Felelos_Elozo = ExecParam.Felhasznalo_Id;
                Record.Updated.Csoport_Id_Felelos_Elozo = true;
            }

            if (Record.Updated.UtolsoAlszam)
            {
                Record.IratSzam = Record.UtolsoAlszam;
                Record.Updated.IratSzam = true;
            }

            //ügyfajta beállítása
            Record.Ugy_Fajtaja = GetUgyIratUgyFajta(ExecParam, Record);

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            // Kézbesítési tétel létrehozása, ha szükséges:
            if (kellKezbesitesiTetel && String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_KezbesitesiTetelInsert = ExecParam.Clone();

                EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();

                //String kuldemeny_Id = ExecParam.Record_Id;
                String ugyirat_Id = result.Uid;

                kezbesitesiTetel.Obj_Id = ugyirat_Id;
                kezbesitesiTetel.Updated.Obj_Id = true;

                // TODO: ObjTip_Id -t is kéne majd állítani valamire
                // helyette: Obj_type a tábla neve lesz (EREC_KuldKuldemenyek)

                kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                kezbesitesiTetel.Updated.Obj_type = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_USER = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                kezbesitesiTetel.Csoport_Id_Cel = Record.Csoport_Id_Felelos;
                kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                // Állapot állítása:
                KRT_CsoportokService csoportokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                KRT_CsoportokSearch csoportokSearch = new KRT_CsoportokSearch();
                csoportokSearch.Id.Filter(Record.Csoport_Id_Felelos);

                Result subCsoportResult = csoportokService.GetAllSubCsoport(ExecParam.Clone(), ExecParam.FelhasznaloSzervezet_Id, true, csoportokSearch);
                if (string.IsNullOrEmpty(subCsoportResult.ErrorCode) && (subCsoportResult.GetCount != 0 || Record.Csoport_Id_Felelos == ExecParam.FelhasznaloSzervezet_Id))
                {
                    kezbesitesiTetel.AtadasDat = DateTime.Now.ToString();
                    kezbesitesiTetel.Updated.AtadasDat = true;

                    kezbesitesiTetel.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott;
                    kezbesitesiTetel.Updated.Allapot = true;

                    EREC_IraKezbesitesiFejekService kezbesitesiFejekService = new EREC_IraKezbesitesiFejekService(this.dataContext);
                    EREC_IraKezbesitesiFejek kezbesitesiFej = new EREC_IraKezbesitesiFejek();

                    kezbesitesiFej.CsomagAzonosito = "TomegesAtadas";
                    kezbesitesiFej.Updated.CsomagAzonosito = true;

                    kezbesitesiFej.Tipus = "A";
                    kezbesitesiFej.Updated.Tipus = true;

                    Result kezbesitesiFejInsertResult = kezbesitesiFejekService.Insert(execParam_KezbesitesiTetelInsert.Clone(), kezbesitesiFej);
                    if (!string.IsNullOrEmpty(kezbesitesiFejInsertResult.ErrorCode))
                        throw new ResultException(kezbesitesiFejInsertResult);

                    if (string.IsNullOrEmpty(kezbesitesiFejInsertResult.Uid))
                        throw new ResultException(0);

                    kezbesitesiTetel.KezbesitesFej_Id = kezbesitesiFejInsertResult.Uid;
                    kezbesitesiTetel.Updated.KezbesitesFej_Id = true;
                }
                else
                {
                    kezbesitesiTetel.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
                    kezbesitesiTetel.Updated.Allapot = true;
                }

                Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);

                if (!String.IsNullOrEmpty(result_kezbesitesiTetel_Insert.ErrorCode))
                {
                    //// hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(execParam_KezbesitesiTetelInsert, result_kezbesitesiTetel_Insert);
                    //return result_kezbesitesiTetel_Insert;

                    throw new ResultException(result_kezbesitesiTetel_Insert);
                }
            }

            //if (!string.IsNullOrEmpty(Record.Csoport_Id_Felelos) && !string.IsNullOrEmpty(result.ErrorCode))
            //{
            //    Contentum.eAdmin.Service.RightsService rs = Contentum.eUtility.eAdminService.ServiceFactory.getRightsService();
            //    Result res = rs.AddCsoportToJogtargy(ExecParam, result.Uid, Record.Csoport_Id_Felelos, 'N');
            //    result.ErrorCode = res.ErrorCode;
            //    result.ErrorMessage = res.ErrorMessage;
            //}

            #region Partner cím
            Result result_bindCim = BindPartnerCim(ExecParam.Clone(), Record);

            if (result_bindCim.IsError)
            {
                throw new ResultException(result_bindCim);
            }
            #endregion Partner cím

            #region ACL kezelés

            if (Rendszerparameterek.GetBoolean(ExecParam, "TULAJDONOS_JOGOSULTSAG_ROGZITES", true))
            {
                Result aclResult = result;
                RightsService rs = new RightsService(this.dataContext);
                aclResult = rs.AddScopeIdToJogtargyWithJogszint(ExecParam, result.Uid, "EREC_UgyUgyiratok", Record.IraIktatokonyv_Id, 'S', ExecParam.Felhasznalo_Id, 'O');
                if (!string.IsNullOrEmpty(aclResult.ErrorCode))
                {
                    throw new ResultException(aclResult);
                }
            }

            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_UgyUgyiratok", "UgyiratNew").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    public static bool KellKezbesitesiTetel(EREC_UgyUgyiratok erec_UgyUgyiratok, EREC_UgyUgyiratok erec_UgyUgyiratokRegi)
    {
        //Felelõs vizsgálata
        if (erec_UgyUgyiratok.Updated.Csoport_Id_Felelos
            && !String.IsNullOrEmpty(erec_UgyUgyiratok.Csoport_Id_Felelos))
        {
            /// Irattárba küldésnél nem kell kézbesítési tétel
            /// (tehát ha az állapot is változik 'Irattárba küldött'-re )
            if (erec_UgyUgyiratok.Updated.Allapot &&
                erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
            {
                return false;
            }

            //Elintézetté nyilvánításnál nem kell
            if (erec_UgyUgyiratok.Updated.Allapot &&
                erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Elintezett)
            {
                return false;
            }

            if (erec_UgyUgyiratokRegi != null)
            {
                //Elintézetté nyilvánított ügyiratba iktatáskor nem kell
                if (erec_UgyUgyiratok.Updated.Allapot
                && erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt
                    && erec_UgyUgyiratokRegi.Allapot == KodTarak.UGYIRAT_ALLAPOT.Elintezett)
                {
                    return false;
                }

                if (erec_UgyUgyiratokRegi.Csoport_Id_Felelos != erec_UgyUgyiratok.Csoport_Id_Felelos)
                {
                    return true;
                }
            }
        }

        return false;
    }
    /// <summary>
    /// Ügyirat módosítás (Update)
    /// Felelõs változásának figyelése: ha változik a felelõs, kézbesítési tétel generálás
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    [WebMethod(Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának módosítása. A módosított adatokat a Record paraméter tartalmazza. Az ExecParam adatai a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result Update(ExecParam ExecParam, EREC_UgyUgyiratok Record)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();



            EREC_UgyUgyiratok ugyirat_regi = null;
            string ugyiratOrzoElozo = "";
            string ugyiratFelelosElozo = "";

            #region régi ügyirat lekérése DB-bõl
            /// Meg kell vizsgálni, hogy ténylegesen változott-e a felelõs
            /// (Ehhez le kell kérni a rekordot)
            /// 
            ExecParam execParam_ugyiratGet = ExecParam.Clone();
            execParam_ugyiratGet.Record_Id = ExecParam.Record_Id;

            Result result_UgyiratGet = this.Get(execParam_ugyiratGet);

            if (!String.IsNullOrEmpty(result_UgyiratGet.ErrorCode))
            {
                throw new ResultException(result_UgyiratGet);
            }

            ugyiratOrzoElozo = (result_UgyiratGet.Record as EREC_UgyUgyiratok).FelhasznaloCsoport_Id_Orzo;
            ugyiratFelelosElozo = (result_UgyiratGet.Record as EREC_UgyUgyiratok).Csoport_Id_Felelos;

            ugyirat_regi = (EREC_UgyUgyiratok)result_UgyiratGet.Record;  // megjegyezzük késõbbi felhasználásra, összehasonlításra
            #endregion

            bool ugykorUgytipusValtozott = false;
            bool ugyiratMozog = false;

            // BLG_2156
            if (ugyirat_regi != null &&
              (Record.Updated.IraIrattariTetel_Id && ugyirat_regi.IraIrattariTetel_Id != Record.IraIrattariTetel_Id))
            {
                // CR 3043 Megörzési idõ számítása a Lezárás dátuma alapján történik, így azt ellenõrizni kell hogy nem null-e 
                // nekrisz
                if (!String.IsNullOrEmpty(ugyirat_regi.LezarasDat))
                {
                    ExecParam execParam_MegorzIdo = ExecParam.Clone();
                    Record.LezarasDat = ugyirat_regi.LezarasDat;
                    Result resMegorzesiIdo = SetMegorzesiIdoFromIrattariTetel(execParam_MegorzIdo, Record, Record.IraIrattariTetel_Id);

                    if (!String.IsNullOrEmpty(resMegorzesiIdo.ErrorCode))
                    {
                        throw new ResultException(resMegorzesiIdo);
                    }
                }
            }


            // változott-e az ügykör és/vagy ügytípus?
            // BLG_2020
            //if (ugyirat_regi != null &&
            //    ((Record.Updated.IraIrattariTetel_Id && ugyirat_regi.IraIrattariTetel_Id != Record.IraIrattariTetel_Id)
            //      || (Record.Updated.UgyTipus && ugyirat_regi.UgyTipus != Record.UgyTipus))
            //    )
            if (ugyirat_regi != null &&
          ((Record.Updated.IraIrattariTetel_Id && ugyirat_regi.IraIrattariTetel_Id != Record.IraIrattariTetel_Id)
            || (Record.Updated.UgyTipus && ugyirat_regi.UgyTipus != Record.UgyTipus)
            || (ugyirat_regi.Ugy_Fajtaja != Record.Ugy_Fajtaja))
          )
            {
                ugykorUgytipusValtozott = true;
            }


            string[] IraIratok_Ids = null;

            // ha az ügykör és/vagy ügytípus változik, meg kell határozni az ügyirat iratait (hatósági adatok, metaadatok miatt)            
            if (ugykorUgytipusValtozott)
            {
                #region Ügyirat iratMetaDefId-jának módosítása

                EREC_IratMetaDefinicioService service_iratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);
                string ugykorId = (Record.Updated.IraIrattariTetel_Id) ? Record.IraIrattariTetel_Id : ugyirat_regi.IraIrattariTetel_Id;
                string ugytipus = (Record.Updated.UgyTipus) ? Record.UgyTipus : ugyirat_regi.UgyTipus;

                Result result_iratMetaDef = service_iratMetaDef.GetIratMetaDefinicioByUgykorUgytipus(ExecParam.Clone(), ugykorId, ugytipus);
                if (!String.IsNullOrEmpty(result_iratMetaDef.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratMetaDef);
                }

                EREC_IratMetaDefinicio erec_iratMetaDefinicio = (EREC_IratMetaDefinicio)result_iratMetaDef.Record;
                string iratMetaDefId = "";
                string generaltTargy = "";
                if (erec_iratMetaDefinicio != null)
                {
                    iratMetaDefId = erec_iratMetaDefinicio.Id;
                    generaltTargy = erec_iratMetaDefinicio.GeneraltTargy;
                }

                Record.IratMetadefinicio_Id = iratMetaDefId;
                Record.Updated.IratMetadefinicio_Id = true;

                Record.GeneraltTargy = generaltTargy;
                Record.Updated.GeneraltTargy = true;

                #endregion

                #region Ügyirat iratainak meghatározása
                // beolvassuk az összes iratot, mely az adott ügyirathoz tartozik
                // 1. hatósági adatok kezelése
                // 2. metaadatok kezelése

                Logger.Debug("Ügyirat iratainak meghatározása start");

                EREC_IraIratokService service_IraIratok = new EREC_IraIratokService(this.dataContext);
                ExecParam execParam_IraIratok = ExecParam.Clone();

                //Result result_IraIratok = service_IraIratok.GetAllWithExtension(execParam_IraIratok, search_IraIratok);
                Result result_IraIratok = service_IraIratok.GetAllIratIdsByUgyirat(execParam_IraIratok, ExecParam.Record_Id);

                if (!String.IsNullOrEmpty(result_IraIratok.ErrorCode))
                {
                    Logger.Debug("Az ügyirat iratainak meghatározása sikertelen: ", execParam_IraIratok, result_IraIratok);
                    throw new ResultException(result_IraIratok);
                }

                IraIratok_Ids = result_IraIratok.Record as string[];

                Logger.Debug("Az ügyirat iratainak meghatározása sikeres.");

                #endregion #endregion Ügyirat iratainak meghatározása
            }

            // Meg kell vizsgálni a Csoport_Id_Felelos mezõt:
            // Ha változtatjuk, Kézbesítési tételt kell létrehozni:
            bool kellKezbesitesiTetel = EREC_UgyUgyiratokService.KellKezbesitesiTetel(Record, ugyirat_regi);

            #region Ugyirat mozgásának vizsgálata

            if (Record.Updated.Csoport_Id_Felelos && !String.IsNullOrEmpty(Record.Csoport_Id_Felelos) && Record.Csoport_Id_Felelos != ugyirat_regi.Csoport_Id_Felelos
                && Record.Updated.FelhasznaloCsoport_Id_Orzo && !String.IsNullOrEmpty(Record.FelhasznaloCsoport_Id_Orzo) && Record.FelhasznaloCsoport_Id_Orzo != ugyirat_regi.FelhasznaloCsoport_Id_Orzo)
            {
                ugyiratMozog = true;
            }

            #endregion Ugyirat mozgásának vizsgálata

            // Benne van-e fizikai dossziéban?
            if (ugyiratMozog && this.IsUgyiratInFizikaiDosszie(ExecParam, ExecParam.Record_Id))
                throw new ResultException(64006);

            Result result_ugyiratUpdate = sp.Insert(Constants.Update, ExecParam, Record);


            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                throw new ResultException(result_ugyiratUpdate);
            }

            if (ugykorUgytipusValtozott)
            {
                #region Metaadatok érvénytelenítése ügyirat szinten

                EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
                ExecParam execParam_ObjektumTargyszavai = ExecParam.Clone();

                Logger.Debug("Metadatok automatikus érvénytelenítése ügyirat szinten start.", execParam_ObjektumTargyszavai);

                Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.InvalidateByDefinicioTipus(execParam_ObjektumTargyszavai
                    , ugyirat_regi.Id
                    , null
                    , "EREC_UgyUgyiratok"
                    , "C2"
                    , false
                    , true
                    );

                if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
                {
                    Logger.Debug("Metaadatok automatikus érvénytelenítése ügyirat szinten sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

                    // hiba 
                    throw new ResultException(result_ObjektumTargyszavai);
                }

                Logger.Debug("Metaadatok automatikus érvénytelenítése ügyirat szinten sikeres.");

                #endregion Metaadatok érvénytelenítése ügyirat szinten

                // kiszedve: (2008.07.04) (felesleges hozzárendelni)
                //#region Metaadatok hozzárendelése ügyirathoz
                //// csak az érték nélküli tárgyszavak hozzárendelése, ha van ilyen...

                ////EREC_ObjektumTargyszavaiService service_ObjektumTargyszavai = new EREC_ObjektumTargyszavaiService(this.dataContext);
                ////ExecParam execParam_ObjektumTargyszavai = ExecParam.Clone();

                //Logger.Debug("Metadatok automatikus hozzárendelése az ügyirathoz az objektum metadefiníciói alapján", execParam_ObjektumTargyszavai);

                ////Result result_ObjektumTargyszavai = service_ObjektumTargyszavai.AssignByDefinicioTipus(execParam_ObjektumTargyszavai
                //result_ObjektumTargyszavai = service_ObjektumTargyszavai.AssignByDefinicioTipus(execParam_ObjektumTargyszavai
                //    , ExecParam.Record_Id
                //    , null
                //    , "EREC_UgyUgyiratok"
                //    , ""
                //    , false);

                //if (!String.IsNullOrEmpty(result_ObjektumTargyszavai.ErrorCode))
                //{
                //    Logger.Debug("Metaadatok automatikus hozzárendelése ügyirathoz sikertelen: ", execParam_ObjektumTargyszavai, result_ObjektumTargyszavai);

                //    // hiba 
                //    throw new ResultException(result_ObjektumTargyszavai);
                //}

                //Logger.Debug("Metadatok automatikus hozzárendelése az ügyirathoz az objektum metadefiníciói alapján sikeres.");

                //#endregion Metaadatok hozzárendelése ügyirathoz
            }

            #region Kézbesítési tétel létrehozása, ha szükséges:
            if (kellKezbesitesiTetel)
            {
                EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_KezbesitesiTetelInsert = ExecParam.Clone();

                String ugyirat_Id = ExecParam.Record_Id;

                /// ha van valamiért már kézbesítési tétel a rekordra (Átadásra kijelölt, vagy Átadott állapotú),
                /// azt érvényteleníteni még az új létrehozása elõtt
                /// 
                Result result_inv =
                    service_KezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_KezbesitesiTetelInsert, ugyirat_Id);
                if (!String.IsNullOrEmpty(result_inv.ErrorCode))
                {
                    throw new ResultException(result_inv);
                }

                EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();


                kezbesitesiTetel.Obj_Id = ugyirat_Id;
                kezbesitesiTetel.Updated.Obj_Id = true;

                // TODO: ObjTip_Id -t is kéne majd állítani valamire
                // helyette: Obj_type a tábla neve lesz (EREC_UgyUgyiratok)

                kezbesitesiTetel.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                kezbesitesiTetel.Updated.Obj_type = true;

                // Átadóból két mezõ is van; nem tom melyik mire jó, úgyhogy töltöm mind a kettõt

                kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                kezbesitesiTetel.Felhasznalo_Id_Atado_USER = ExecParam.Felhasznalo_Id;
                kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                kezbesitesiTetel.Csoport_Id_Cel = Record.Csoport_Id_Felelos;
                kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                // Állapot állítása:
                kezbesitesiTetel.Allapot = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
                kezbesitesiTetel.Updated.Allapot = true;

                // TODO: Azonosító állítása (Iktatókönyv megküljel. / Fõszám (Év)) a listában való megjelenítéshez

                Result result_kezbesitesiTetel_Insert = service_KezbesitesiTetelek.Insert(execParam_KezbesitesiTetelInsert, kezbesitesiTetel);

                if (!String.IsNullOrEmpty(result_kezbesitesiTetel_Insert.ErrorCode))
                {
                    //// hiba:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(execParam_KezbesitesiTetelInsert, result_kezbesitesiTetel_Insert);
                    //return result_kezbesitesiTetel_Insert;

                    throw new ResultException(result_kezbesitesiTetel_Insert);
                }

                //a keledkezett kézbesítési tétel id-jának visszaadása az Uid változóban
                result_ugyiratUpdate.Uid = result_kezbesitesiTetel_Insert.Uid;

            }

            #endregion


            // ha az ügykör és/vagy ügytípus változik, meg kell vizsgálni minden iratot és frissíteni a hatósági jelleget és a metaadatokat
            // + Iratoknál IratMetaDefId módosítása
            if (ugykorUgytipusValtozott)
            {

                if (IraIratok_Ids != null && IraIratok_Ids.Length > 0)
                {
                    #region EREC_IraOnkormAdatok UPDATE

                    Logger.Debug("Önkormányzati adatok aktualizálása start");
                    // meghatározzuk az ügyfajtát az ügykör (irattári tétel) és ügytípus alapján
                    string ugyfajta = "";
                    string ugyfajta_regi = "";
                    // BLG_2020
                    ExecParam execparam_IraOnkormAdatok = ExecParam.Clone();
                    EREC_IratMetaDefinicioService service_IratMetaDefinicio = new EREC_IratMetaDefinicioService(this.dataContext);


                    bool IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE = (Rendszerparameterek.GetBoolean(execparam_IraOnkormAdatok, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true));
                    if (IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE)
                    {
                        //EREC_IratMetaDefinicioService service_IratMetaDefinicio = new EREC_IratMetaDefinicioService(this.dataContext);
                        ExecParam execparam_IratMetaDefinicio = ExecParam.Clone();
                        Result result_IraMetaDefinicio;

                        Logger.Debug("Régi irat metadefiníció ill. ügyfajta meghatározása start");
                        // BLG_2020
                        //string ugyfajta_regi = "";
                        // régi UgyFajta kiolvasasa az EREC_IratMetaDefinicio alapján
                        // (nem hatósági v. önkormányzati v. államigazgatási)
                        if (!String.IsNullOrEmpty(ugyirat_regi.IratMetadefinicio_Id))
                        {
                            execparam_IratMetaDefinicio.Record_Id = ugyirat_regi.IratMetadefinicio_Id;
                            Result result_IraMetaDefinicio_regi = service_IratMetaDefinicio.Get(execparam_IratMetaDefinicio);
                            if (!String.IsNullOrEmpty(result_IraMetaDefinicio_regi.ErrorCode))
                            {
                                Logger.Debug("Régi irat metadefiníció ill. ügyfajta meghatározása sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio_regi);
                                // itt nem állunk meg hiba miatt, hanem úgy vesszük, hogy nem volt megadva
                            }
                            else
                            {
                                EREC_IratMetaDefinicio erec_IratMetaDefinicio_regi = (EREC_IratMetaDefinicio)result_IraMetaDefinicio_regi.Record;

                                if (erec_IratMetaDefinicio_regi != null)
                                {
                                    ugyfajta_regi = erec_IratMetaDefinicio_regi.UgyFajta;
                                }
                            }
                        }

                        Logger.Debug("Aktuális irat metadefiníció ill. ügyfajta meghatározása start");
                        // UgyFajta kiolvasasa az EREC_IratMetaDefinicio alapján
                        // (nem hatósági v. önkormányzati v. államigazgatási)
                        if (!String.IsNullOrEmpty(Record.IratMetadefinicio_Id))
                        {
                            execparam_IratMetaDefinicio.Record_Id = Record.IratMetadefinicio_Id;
                            result_IraMetaDefinicio = service_IratMetaDefinicio.Get(execparam_IratMetaDefinicio);
                            if (!String.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                            {
                                Logger.Debug("Aktuális irat metadefiníció ill. ügyfajta meghatározása sikertelen: ", execparam_IratMetaDefinicio, result_IraMetaDefinicio);

                                // hiba 
                                throw new ResultException(result_IraMetaDefinicio);
                            }

                            EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_IraMetaDefinicio.Record;

                            if (erec_IratMetaDefinicio != null)
                            {
                                ugyfajta = erec_IratMetaDefinicio.UgyFajta;
                            }
                        }
                    }
                    else
                    {
                        ugyfajta = Record.Ugy_Fajtaja;
                        ugyfajta_regi = ugyirat_regi.Ugy_Fajtaja;
                    }

                    // csak akkor hajtjuk végre, ha változott az ügyfajta
                    Logger.Debug("ugyfajta_regi: " + ugyfajta_regi);
                    Logger.Debug("ugyfajta: " + ugyfajta);

                    #endregion EREC_IraOnkormAdatok UPDATE

                    #region Metaadatok hozzárendelése irathoz
                    // az Irat Update elvégzi, amikor az IratMetaDef_Id módosul

                    #endregion Metaadatok hozzárendelése irathoz

                    #region IratMetaDefId módosítása az iratoknál

                    // Iratok adatainak lekérése GetAllWithExtension-nel:
                    EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);

                    EREC_IraIratokSearch search_iratok = new EREC_IraIratokSearch(true);
                    string ugyiratId = ExecParam.Record_Id;
                    search_iratok.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Filter(ugyiratId);

                    Result result_iratokGetAll = service_iratok.GetAllWithExtension(ExecParam.Clone(), search_iratok);
                    if (!String.IsNullOrEmpty(result_iratokGetAll.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iratokGetAll);
                    }

                    if (result_iratokGetAll.Ds != null)
                    {
                        foreach (DataRow row in result_iratokGetAll.Ds.Tables[0].Rows)
                        {
                            // Ellenõrzés:
                            string row_UgyiratId = row["UgyiratId"].ToString();
                            if (row_UgyiratId != ugyiratId)
                            {
                                // hiba:
                                throw new ResultException("Hiba az iratok lekérése során!");
                            }

                            string row_Id = row["Id"].ToString();
                            string row_Ver = row["Ver"].ToString();
                            string row_EljarasiSzakasz = row["EljarasiSzakasz"].ToString();
                            string row_Irattipus = row["Irattipus"].ToString();
                            string row_IratMetaDef_Id = row["IratMetaDef_Id"].ToString();

                            string ugykorId = (Record.Updated.IraIrattariTetel_Id) ? Record.IraIrattariTetel_Id : ugyirat_regi.IraIrattariTetel_Id;
                            string ugytipus = (Record.Updated.UgyTipus) ? Record.UgyTipus : ugyirat_regi.UgyTipus;

                            // IratMetaDefId meghatározása:
                            Result result_IratMetaDefGet = service_IratMetaDefinicio.GetIratMetaDefinicioByIrattipus(ExecParam.Clone()
                                , ugykorId, ugytipus, row_EljarasiSzakasz, row_Irattipus);
                            if (!string.IsNullOrEmpty(result_IratMetaDefGet.ErrorCode))
                            {
                                // hiba:
                                throw new ResultException(result_IratMetaDefGet);
                            }

                            EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_IratMetaDefGet.Record;
                            string iratMetaDefId = "";
                            string generaltTargy = "";
                            if (erec_IratMetaDefinicio != null)
                            {
                                iratMetaDefId = erec_IratMetaDefinicio.Id;
                                generaltTargy = erec_IratMetaDefinicio.GeneraltTargy;
                            }

                            // ha változott, Irat Update:
                            if (iratMetaDefId != row_IratMetaDef_Id)
                            {
                                #region Irat Update

                                EREC_IraIratok iratObj = new EREC_IraIratok();
                                iratObj.Updated.SetValueAll(false);
                                iratObj.Base.Updated.SetValueAll(false);
                                iratObj.Base.Ver = row_Ver;
                                iratObj.Base.Updated.Ver = true;

                                iratObj.IratMetaDef_Id = iratMetaDefId;
                                iratObj.Updated.IratMetaDef_Id = true;

                                iratObj.GeneraltTargy = generaltTargy;
                                iratObj.Updated.GeneraltTargy = true;

                                iratObj.Ugy_Fajtaja = erec_IratMetaDefinicio.UgyFajta;
                                iratObj.Updated.Ugy_Fajtaja = true;

                                ExecParam execParam_iratUpdate = ExecParam.Clone();
                                execParam_iratUpdate.Record_Id = row_Id;

                                Result result_iratUpdate = service_iratok.Update(execParam_iratUpdate, iratObj);
                                if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
                                {
                                    // hiba:
                                    throw new ResultException(result_iratUpdate);
                                }

                                #endregion
                            }
                        }
                    }

                    #endregion
                }
            }

            result = result_ugyiratUpdate;

            #region UGY / IRAT UGY_FAJTA            
            string iratUgyFajta = GetUgyIratUgyFajta(ExecParam, Record);
            Ugyirat_Irat_UgyFajtaModositas(ExecParam, ExecParam.Record_Id, iratUgyFajta);
            #endregion

            #region Partner cím
            Result result_bindCim = BindPartnerCim(ExecParam.Clone(), Record);

            if (result_bindCim.IsError)
            {
                throw new ResultException(result_bindCim);
            }
            #endregion Partner cím

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_UgyUgyiratok", "Modify").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            #region Szerelt ügyiratok updat-elése

            Result resSzereltUpdate = UpdateSzereltUgyiratok(ExecParam, ugyirat_regi, Record);

            if (!String.IsNullOrEmpty(resSzereltUpdate.ErrorCode))
            {
                throw new ResultException(resSzereltUpdate);
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;

    }



    #endregion

    private bool _TomegesUpdateKellKezbesitesiTetel = true;

    public bool TomegesUpdateKellKezbesitesiTetel
    {
        get { return _TomegesUpdateKellKezbesitesiTetel; }
        set { _TomegesUpdateKellKezbesitesiTetel = value; }
    }

    public Result Update_Tomeges(ExecParam ExecParam, DataTable tableUgyiratokBeforeTable, string[] Ids, string[] Vers, EREC_UgyUgyiratok Record, DateTime ExecutionTime)
    {
        return Update_Tomeges(ExecParam, tableUgyiratokBeforeTable, Ids, Vers, null, Record, ExecutionTime);
    }

    public Result Update_Tomeges(ExecParam ExecParam, DataTable tableUgyiratokBeforeTable, string[] Ids, string[] Vers,
        string[] Csoport_Id_Felelos_Array, EREC_UgyUgyiratok Record, DateTime ExecutionTime)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        try
        {
            //LZS - BUG_13474
            //This list will contain all the splitted arrays.
            List<string[]> splittedIds = new List<string[]>();
            List<string[]> splittedVers = new List<string[]>();
            List<string[]> splittedCsoportIdFelelosArray = new List<string[]>();

            int lengthToSplit = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.UGYIRAT_TOMEGES_UPDATE_LIMIT, 5000);

            int IdsLength = Ids.Length;

            bool HasCsoport_Id_Felelos_Array = Csoport_Id_Felelos_Array != null && Csoport_Id_Felelos_Array.Length > 0;

            for (int i = 0; i < IdsLength; i = i + lengthToSplit)
            {
                string[] subIds = new string[lengthToSplit];
                string[] subVers = new string[lengthToSplit];
                string[] subCsoportIdFelelosArray = new string[lengthToSplit];

                if (IdsLength < i + lengthToSplit)
                {
                    lengthToSplit = IdsLength - i;
                }

                Array.Copy(Ids, i, subIds, 0, lengthToSplit);
                Array.Copy(Vers, i, subVers, 0, lengthToSplit);

                if (HasCsoport_Id_Felelos_Array)
                    Array.Copy(Csoport_Id_Felelos_Array, i, subCsoportIdFelelosArray, 0, lengthToSplit);

                splittedIds.Add(subIds);
                splittedVers.Add(subVers);

                if (HasCsoport_Id_Felelos_Array)
                    splittedCsoportIdFelelosArray.Add(subCsoportIdFelelosArray);

            }

            //Végrehajtás darabokban:
            for (int i = 0; i < splittedIds.Count(); i++)
            {
                if (HasCsoport_Id_Felelos_Array)
                    result = sp.Update_Tomeges(ExecParam, splittedIds[i], splittedVers[i], splittedCsoportIdFelelosArray[i], Record, ExecutionTime, this.TomegesUpdateKellKezbesitesiTetel);
                else
                    result = sp.Update_Tomeges(ExecParam, splittedIds[i], splittedVers[i], null, Record, ExecutionTime, this.TomegesUpdateKellKezbesitesiTetel);

                result.CheckError();
            }


            #region RegiSzereltek Update

            DataTable tableFilteredForIds = tableUgyiratokBeforeTable.Clone();
            foreach (DataRow row in tableUgyiratokBeforeTable.Select("Convert(Id,System.String) in (" + Contentum.eUtility.Search.GetSqlInnerString(Ids) + ")"))
            {
                tableFilteredForIds.ImportRow(row);
            }
            ExecParam xpm = ExecParam.Clone();
            TomegesUgyiratUpdateExecuted(xpm, tableFilteredForIds, Record);

            #endregion
        }
        catch (ResultException re)
        {
            result = re.GetResult();
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra). 
    /// Az ExecParam.Record_Id nem használt, a további adatok a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).
    /// </summary>
    /// <param name="ExecParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="_EREC_UgyUgyiratokSearch">A szûrési feltételeket tartalmazza</param>
    /// <returns>Result.Ds DataSet objektum kerül feltöltésre a lekérdezés eredményével</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Több egymáskoz kapcsolt táblára vonatkozó keresés eredményhalmazának elkérése. A szûrési feltételeket paraméter tartalmazza (*Search). 
    /// (A GetAll kiterjesztése join mûvelettel összekapcsolt táblákra). 
    /// Az ExecParam.Record_Id nem használt, a további adatok a mûvelet naplózásához szükségesek (felhasználó azonosító, hívó alkalmazás, hívó HTML oldal).
    /// Jogosultságra szûréssel
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_UgyUgyiratokSearch"></param>
    /// <param name="Jogosultak"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, bool Jogosultak)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // Snapshot Isolation Level
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch, Jogosultak, false);

            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_UgyUgyiratokSearch, new EREC_UgyUgyiratokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, null, "EREC_UgyUgyiratok", (String.IsNullOrEmpty(ExecParam.FunkcioKod) ? "List" : ExecParam.FunkcioKod)).Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

                    #region where feltétel összeállítása
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_UgyUgyiratokSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_UgyUgyiratokSearch.WhereByManual;
                    }

                    //#region FullTextSearch
                    //string strContainsTargyszo = String.Empty;
                    //string strContainsTargy = String.Empty;
                    //string strWhere = String.Empty;
                    ////if (!string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.Targy.Value)) {
                    ////    strContainsTargy = new FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.Targy.Value).Normalized;
                    ////    strWhere += "contains( EREC_UgyUgyiratok.Targy , '" + strContainsTargy + "' )";
                    ////}
                    //if (_EREC_UgyUgyiratokSearch.fts_targyszavak != null && !string.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.fts_targyszavak.Filter))
                    //{
                    //    strContainsTargyszo = new FullTextSearch.SQLContainsCondition(_EREC_UgyUgyiratokSearch.fts_targyszavak.Filter).Normalized;
                    //    if (!string.IsNullOrEmpty(strWhere)) {
                    //        strWhere += " and ";
                    //    }
                    //    strWhere += "contains( (EREC_ObjektumTargyszavai.Targyszo, EREC_ObjektumTargyszavai.Note, EREC_ObjektumTargyszavai.Ertek ), '" + strContainsTargyszo + "' )";
                    //}

                    //if (!string.IsNullOrEmpty(strWhere))
                    //{
                    //    if (!string.IsNullOrEmpty(query.Where))
                    //        query.Where += " and " + strWhere;
                    //    else
                    //        query.Where += strWhere;
                    //}
                    //#endregion

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="_EREC_UgyUgyiratokSearch"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionForStatistics(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionForStatistics(ExecParam, _EREC_UgyUgyiratokSearch);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionForEloadoiIvek(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionForEloadoiIvek(ExecParam, _EREC_UgyUgyiratokSearch);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionForUgyiratpotlo(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionForUgyiratpotlo(ExecParam, _EREC_UgyUgyiratokSearch);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat rekord lekérése jogosultság ellenõrzéssel
    /// </summary>
    /// <param name="execParam">RecordId mezõ tartalmazza az ügyirat Id-ját</param>
    /// <param name="Jogszint"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetWithRightCheck(ExecParam execParam, char Jogszint)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "View").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat rekord lekérése jogosultság ellenõrzéssel
    /// </summary>
    /// <param name="execParam">RecordId mezõ tartalmazza az ügyirat Id-ját</param>
    /// <param name="Jogszint"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetWithRightCheckWithoutEventLogging(ExecParam execParam, char Jogszint)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithRightCheck(execParam, Jogszint);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionForLeaders(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch
        , String KezdDat, String VegeDat)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionForLeaders(ExecParam, _EREC_UgyUgyiratokSearch, KezdDat, VegeDat);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result ForXml(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForXml(ExecParam, _EREC_UgyUgyiratokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    #region BLG 2969
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public String CreateUgyiratIratokDexXml(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        String result = String.Empty;
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.CreateUgyiratIratokDexXml(ExecParam, _EREC_UgyUgyiratokSearch);

        }
        catch (Exception e)
        {
            result = e.Message;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam);
        return result;
    }
    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetSzereltekFoszam(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetSzereltekFoszam(ExecParam, _EREC_UgyUgyiratokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result ForElszamoltatasiJk(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForElszamoltatasiJk(ExecParam, _EREC_UgyUgyiratokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result ForElszamoltatasiJkSSRS(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, bool Alszam)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForElszamoltatasiJkSSRS(ExecParam, _EREC_UgyUgyiratokSearch, Alszam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result ForXmlNincsUgyintezo(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.ForXmlNincsUgyintezo(ExecParam, _EREC_UgyUgyiratokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat lista lekérése SSRS riporthoz, a szerelési láncokkal együtt.
    /// </summary>
    /// <param name="ExecParam">Felhasználói információk naplózáshoz, jogellenõrzéshez, stb.</param>
    /// <param name="_EREC_UgyUgyiratokSearch">Keresési feltételek</param>
    /// <returns></returns>
    [WebMethod(Description = "Ügyirat lista lekérése SSRS riporthoz, a szerelési láncokkal együtt.")]
    public Result GetAllWithExtensionAndSzereltek(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtensionAndSzereltek(ExecParam, _EREC_UgyUgyiratokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllForHatosagiStatisztikaUgyiratforgalom(ExecParam ExecParam, String KezdDat, String VegeDat, String WorkSheetName)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllForHatosagiStatisztikaUgyiratforgalom(ExecParam, KezdDat, VegeDat, WorkSheetName);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllForHatosagiStatisztikaOnkormanyzat(ExecParam ExecParam, String KezdDat, String VegeDat, String WorkSheetName)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllForHatosagiStatisztikaOnkormanyzat(ExecParam, KezdDat, VegeDat, WorkSheetName);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllForHatosagiStatisztikaAllamigazgatas(ExecParam ExecParam, String KezdDat, String VegeDat, String WorkSheetName)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllForHatosagiStatisztikaAllamigazgatas(ExecParam, KezdDat, VegeDat, WorkSheetName);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(VezetoiPanelParameterek))]
    public Result GetSummaryForVezetoiPanel(ExecParam ExecParam, VezetoiPanelParameterek _VezetoiPanelParameterek)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetSummaryForVezetoiPanel(ExecParam, _VezetoiPanelParameterek);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Lekérdezi az execParamban szereplõ felhasználói adatok alapján, hogy a felhasználó (esetleg megbízott)
    /// rendelkezik-e a function paraméterben megadott kódú funkciójoggal. Ha a lekérés során hiba lép fel, false-t ad vissza!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="function">A funkció kódja, melynek meglétét vizsgáljuk</param>
    /// <returns></returns>
    private static bool HasFunctionRight(ExecParam execParam, string function)
    {
        Logger.DebugStart("HasFunctionRight - Start");
        Logger.Debug("Felhasznalo_Id: " + (execParam.Felhasznalo_Id ?? "NULL") + " LoginUser_Id: " + (execParam.LoginUser_Id ?? "NULL"));
        Logger.Debug("CsoportTag_Id: " + (execParam.CsoportTag_Id ?? "NULL") + " FelhasznaloSzervezet_Id: " + (execParam.FelhasznaloSzervezet_Id ?? "NULL"));

        bool HasFunctionRight = false;
        bool isMegbizas = false;
        // ha hiba volt a lekérés során, akkor a végén false-t adunk vissza
        if (!String.IsNullOrEmpty(execParam.Helyettesites_Id))
        {
            Logger.Debug("Helyettesites_Id: " + execParam.Helyettesites_Id);
            KRT_HelyettesitesekService service_helyettesitesek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();
            ExecParam execParam_helyettesitesek = execParam.Clone();
            execParam_helyettesitesek.Record_Id = execParam.Helyettesites_Id;
            Result result_helyettesitesGet = service_helyettesitesek.Get(execParam_helyettesitesek);

            if (String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode))
            {
                KRT_Helyettesitesek krt_Helyettesitesek = (KRT_Helyettesitesek)result_helyettesitesGet.Record;
                if (krt_Helyettesitesek != null)
                {
                    if (krt_Helyettesitesek.HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Megbizas)
                    {
                        isMegbizas = true;
                        Logger.Debug("isMegbizas = true");
                    }
                }
                else
                {
                    // hiba
                    Logger.Debug("Error: krt_Helyettesitesek == null");
                    Logger.DebugEnd("HasFunctionRight - End");
                    return false;
                }
            }
            else
            {
                // hiba
                Logger.Debug("Error: !String.IsNullOrEmpty(result_helyettesitesGet.ErrorCode)");
                Logger.DebugEnd("HasFunctionRight - End");
                return false;
            }
        }

        // Mindig a Felhasznalo -ra kérjük le a funkciójogosultságokat
        // (Helyettesítésnél is a Felhasználó lesz a helyettesített) - kivéve megbízás
        Result _ret;

        Contentum.eAdmin.Service.KRT_FunkciokService _KRT_FunkciokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FunkciokService();
        if (isMegbizas)
        {
            execParam.Record_Id = execParam.Helyettesites_Id;
            Logger.Debug("HasFunctionRight: GetAllByMegbizas", execParam);
            _ret = _KRT_FunkciokService.GetAllByMegbizas(execParam);
        }
        else
        {
            // szervezet és felhasználó adott, de szükség van a csoporttagságra
            execParam.Record_Id = execParam.CsoportTag_Id;
            Logger.Debug("HasFunctionRight: GetAllByCsoporttagSajatJogu", execParam);
            _ret = _KRT_FunkciokService.GetAllByCsoporttagSajatJogu(execParam);
        }

        if (_ret.Ds != null)
        {
            DataRow[] rows_funkcio = _ret.Ds.Tables[0].Select("Kod='" + function + "'");
            if (rows_funkcio.Length > 0)
            {
                HasFunctionRight = true;
            }
        }
        else
        {
            // hiba
            Logger.Debug("Error: _ret.Ds == null");
            Logger.DebugEnd("HasFunctionRight - End");
            return false;
        }

        Logger.Debug("HasFunctionRight = " + HasFunctionRight.ToString());
        Logger.DebugEnd("HasFunctionRight - End");
        return HasFunctionRight;
    }


    /// <summary>
    /// Ügyirat szignálása
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">A szignálandó ügyirat Id-ja</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A következõ felelõs csoport Id-ja, akire az ügyiratot szignálják.
    /// Ha a megadott csoport egy egyszemélyes csoport, beállítjuk rá az ügyintézõt is</param>
    /// <param name="erec_HataridosFeladatok">A szignáláshoz esetleg megadott kezelési feljegyzés, ami a megadott ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A szignálás sikerességérõl tájékoztatást adó Result objektum.</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Szignalas(ExecParam execParam, string erec_UgyUgyiratok_Id, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Szignálás - Start", execParam);

        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = SzignalasInternal(execParam, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok, log, isTransactionBeginHere);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        // Ha minden OK:
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyiratok tömeges szignálása
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Ids">A szignálandó ügyiratok Id-ja</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A következõ felelõs csoport Id-ja, akire az ügyiratokat szignálják.
    /// Ha a megadott csoport egy egyszemélyes csoport, beállítjuk rá az ügyintézõt is</param>
    /// <param name="erec_HataridosFeladatok">A szignáláshoz esetleg megadott kezelési feljegyzés, ami a megadott ügyiratok
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is</param>
    /// <returns>A szignálás sikerességérõl tájékoztatást adó Result objektum.</returns>
    [WebMethod]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result TomegesSzignalas(ExecParam execParam, string[] erec_UgyUgyiratok_Ids, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Tömeges szignálás - Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string erec_UgyUgyiratok_Id in erec_UgyUgyiratok_Ids)
            {
                result = SzignalasInternal(execParam, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok, log, isTransactionBeginHere);
                if (result.IsError)
                    throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        // Ha minden OK:
        log.WsEnd(execParam, result);
        return result;
    }

    private Result SzignalasInternal(ExecParam execParam, string erec_UgyUgyiratok_Id, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok, Log log, bool isTransactionBeginHere)
    {
        Result result = new Result() { Uid = erec_UgyUgyiratok_Id };

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(erec_UgyUgyiratok_Id) || String.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
        {
            // hiba
            result = ResultError.CreateNewResultWithErrorCode(52150);
            result.Uid = erec_UgyUgyiratok_Id;
            log.WsEnd(execParam, result);
            Logger.Error("Szignálás: Hibás paraméterlista", execParam);
            throw new ResultException(result);
        }

        // Ügyirat lekérése:
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execParam_UgyiratGet = execParam.Clone();
        execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

        Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
        if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_ugyiratGet);
        }
        else
        {
            if (result_ugyiratGet.Record == null)
            {
                // hiba
                throw new ResultException(52151);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam_szignalhato = execParam.Clone();
            ErrorDetails errorDetail = null;

            // Ellenõrzés, hogy Szignálható-e az ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.Szignalhato(ugyiratStatusz, execParam_szignalhato, out errorDetail) == false)
            {
                // Nem Szignálható az ügyirat:
                throw new ResultException(52152, errorDetail);
            }
            else
            {

                #region Ellenõrzés, hogy a címzettre szignálható-e az ügyirat; ha igen, megfelelõ felelõs beállítása

                Logger.Debug("Szignálás: Felelõs csoport lekérdezése - Start", execParam);

                Result result_csoportGet = sp.GetSzignaltCsoport(execParam_UgyiratGet, csoport_Id_Felelos_Kovetkezo);
                if (!string.IsNullOrEmpty(result_csoportGet.ErrorCode))
                {
                    Logger.Error("Hiba: Felelõs csoport lekérése", execParam_UgyiratGet);
                    throw new ResultException(result_csoportGet);
                }

                KRT_Csoportok krt_csoportok_felelos = (KRT_Csoportok)result_csoportGet.Record;
                csoport_Id_Felelos_Kovetkezo = krt_csoportok_felelos.Id;

                #endregion

                #region Felelõs csoport lekérése

                /***** elõzõ ellenõrzés során le kell kérni a csoportot, ezért van kommentezve*****/

                // Ha egyszemélyes csoport, ügyintézõ beállítása
                //Logger.Debug("Szignálás: Felelõs csoport lekérdezése - Start", execParam);

                // CSOPORT GET
                //KRT_CsoportokService service_csoportok =
                //    Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                //ExecParam execParam_csoportGet = execParam.Clone();
                //execParam_csoportGet.Record_Id = csoport_Id_Felelos_Kovetkezo;

                //Result result_csoportGet = service_csoportok.Get(execParam_csoportGet);
                //if (!String.IsNullOrEmpty(result_csoportGet.ErrorCode))
                //{
                //    // hiba:
                //    Logger.Error("Hiba: Felelõs csoport lekérése", execParam_csoportGet);
                //    throw new ResultException(result_csoportGet);
                //}
                //KRT_Csoportok krt_csoportok_felelos = (KRT_Csoportok)result_csoportGet.Record;

                #endregion

                // ÜGYIRAT UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // ha változott a felelõs (nem magára szignálta), át kell adni
                bool kellAtadas = false;
                // BUG_8765
                //   if (csoport_Id_Felelos_Kovetkezo.Equals(erec_UgyUgyirat.Csoport_Id_Felelos, StringComparison.InvariantCultureIgnoreCase) == false)
                if ((csoport_Id_Felelos_Kovetkezo.Equals(erec_UgyUgyirat.Csoport_Id_Felelos, StringComparison.InvariantCultureIgnoreCase) == false)
                    && (Rendszerparameterek.GetBoolean(execParam, "TUK_SZIGNALAS_ENABLED", false) == false))
                {
                    // Elõzõ felelõs csoport beállítása
                    erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                    // Felelõs beállítása a megadottra
                    erec_UgyUgyirat.Csoport_Id_Felelos = csoport_Id_Felelos_Kovetkezo;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                    kellAtadas = true;
                }

                // BUG_11085
                //// Elõzõ felelõs csoport beállítása
                //erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                //erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                //// Felelõs beállítása a megadottra
                //erec_UgyUgyirat.Csoport_Id_Felelos = csoport_Id_Felelos_Kovetkezo;
                //erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                // Ügyintézõ beállítása:
                if (krt_csoportok_felelos.Tipus == KodTarak.CSOPORTTIPUS.Dolgozo)
                {
                    erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez = krt_csoportok_felelos.Id;
                    erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                }
                else
                {
                    Logger.Debug("Szignálás: Felelõs csoport nem egyszemélyes, ügyintézõ mezõ nincs kitöltve", execParam);
                }

                // Állapot átállítása:
                if (bSzignalasCausesTovabbitasAlattAllapot && kellAtadas)
                {
                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Szignalt;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                }
                else
                {
                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Szignalt;
                    erec_UgyUgyirat.Updated.Allapot = true;
                }

                // BLG_1677
                // Szignálás rögzítése
                erec_UgyUgyirat.SzignaloId = execParam.Felhasznalo_Id;
                erec_UgyUgyirat.Updated.SzignaloId = true;

                erec_UgyUgyirat.SzignalasIdeje = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.SzignalasIdeje = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                {
                    // hiba volt:
                    throw new ResultException(result_ugyiratUpdate);
                }
                else
                {
                    if (Rendszerparameterek.GetBoolean(execParam, "IRAT_SZIGNALAS_ELERHETO", false))
                    {
                        EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
                        ExecParam execParam_Irat = execParam.Clone();
                        Result iratSzignalasResult = erec_IraIratokService.SzignalasByUgyirat(execParam_Irat, erec_UgyUgyiratok_Id, erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez);
                        iratSzignalasResult.CheckError();
                    }
                    else
                    {
                        // BUG_7574
                        #region IratUpdate
                        EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
                        ExecParam execParam_Irat = execParam.Clone();
                        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
                        iratSearch.Ugyirat_Id.Filter(erec_UgyUgyiratok_Id);

                        iratSearch.Alszam.Filter("1");

                        // Állapot még nem sztornózott:
                        iratSearch.Allapot.NotEquals(KodTarak.IRAT_ALLAPOT.Sztornozott);

                        iratSearch.TopRow = 1;

                        Result result_elsoAlszam = erec_IraIratokService.GetAll(execParam_Irat, iratSearch);
                        result_elsoAlszam.CheckError();
                        if (result_elsoAlszam.GetCount == 0)
                        {
                            // Hiba a szignálás során: 1-es alszám lekérése sikertelen!
                            throw new ResultException(52154, errorDetail);
                        }

                        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
                        Utility.LoadBusinessDocumentFromDataRow(erec_IraIratok, result_elsoAlszam.Ds.Tables[0].Rows[0]);

                        erec_IraIratok.Updated.SetValueAll(false);
                        erec_IraIratok.Base.Updated.SetValueAll(false);
                        erec_IraIratok.Base.Updated.Ver = true;

                        erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez;
                        erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                        execParam_Irat.Record_Id = erec_IraIratok.Id;

                        Result result_iratUpdate = erec_IraIratokService.Update(execParam_Irat, erec_IraIratok);


                        if (result_iratUpdate.IsError)
                        {
                            // hiba:
                            throw new ResultException(result_iratUpdate);
                        }

                        #endregion iratUpdate
                    }

                    #region kézbesítési tétel továbbítása
                    if (kellAtadas && !String.IsNullOrEmpty(result_ugyiratUpdate.Uid))
                    {
                        ExecParam execParam_atadas = execParam.Clone();
                        execParam_atadas.Record_Id = result_ugyiratUpdate.Uid;

                        EREC_IraKezbesitesiTetelekService service_atadas = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                        Result result_atadas = service_atadas.Atadas(execParam_atadas, result_ugyiratUpdate.Uid);

                        if (result_atadas.IsError)
                        {
                            throw new ResultException(result);
                        }
                    }
                    #endregion kézbesítési tétel továbbítása

                    // Kezelési feljegyzés INSERT, ha megadták
                    if (erec_HataridosFeladatok != null)
                    {
                        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                        ExecParam execParam_kezFeljInsert = execParam.Clone();

                        // Ügyirathoz kötés
                        erec_HataridosFeladatok.Obj_Id = erec_UgyUgyirat.Id;
                        erec_HataridosFeladatok.Updated.Obj_Id = true;

                        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                        erec_HataridosFeladatok.Updated.Obj_type = true;

                        if (!String.IsNullOrEmpty(erec_UgyUgyirat.Azonosito))
                        {
                            erec_HataridosFeladatok.Azonosito = erec_UgyUgyirat.Azonosito;
                            erec_HataridosFeladatok.Updated.Azonosito = true;
                        }

                        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                            execParam_kezFeljInsert, erec_HataridosFeladatok);
                        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_kezFeljInsert);
                        }
                    }


                    result = result_ugyiratUpdate;
                }
            }
        }

        #region Eseménynaplózás

        if (isTransactionBeginHere)
        {
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "Szignalas").Record;
            eventLogService.Insert(execParam, eventLogRecord);
        }

        #endregion

        return result;
    }

    /// <summary>
    /// Ügyirat szignálása szervezetre. Az ügyirat következõ kezelõje a szervezet vezetõje lesz
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id">A szignálandó ügyirat Id-ja</param>
    /// <param name="csoportId_Szervezet">A szervezet Id-ja</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result SzignalasSzervezetre(ExecParam execParam, string erec_UgyUgyiratok_Id, string CsoportId_Szervezet, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Szignálás szervezetre - Start", execParam);

        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = SzignalasSzervezetreInternal(execParam, erec_UgyUgyiratok_Id, CsoportId_Szervezet, erec_HataridosFeladatok, log, isTransactionBeginHere);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        // Ha minden OK:
        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyiratok tömeges szignálása szervezetre. Az ügyiratok következõ kezelõje a szervezet vezetõje lesz
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Ids">A szignálandó ügyiratok Id-ja</param>
    /// <param name="csoportId_Szervezet">A szervezet Id-ja</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result TomegesSzignalasSzervezetre(ExecParam execParam, string[] erec_UgyUgyiratok_Ids, string csoportId_Szervezet, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Tömeges szignálás szervezetre - Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            foreach (string erec_UgyUgyiratok_Id in erec_UgyUgyiratok_Ids)
            {
                result = SzignalasSzervezetreInternal(execParam, erec_UgyUgyiratok_Id, csoportId_Szervezet, erec_HataridosFeladatok, log, isTransactionBeginHere);
                if (result.IsError)
                    throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        // Ha minden OK:
        log.WsEnd(execParam, result);
        return result;
    }

    private Result SzignalasSzervezetreInternal(ExecParam execParam, String erec_UgyUgyiratok_Id, String CsoportId_Szervezet, EREC_HataridosFeladatok erec_HataridosFeladatok, Log log, bool isTransactionBeginHere)
    {
        Result result = new Result() { Uid = erec_UgyUgyiratok_Id };

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(erec_UgyUgyiratok_Id) || String.IsNullOrEmpty(CsoportId_Szervezet))
        {
            // hiba
            result = ResultError.CreateNewResultWithErrorCode(52610);
            result.Uid = erec_UgyUgyiratok_Id;
            log.WsEnd(execParam, result);
            Logger.Error("Szignálás: Hibás paraméterlista", execParam);
            throw new ResultException(result);
        }

        // Ügyirat lekérése:
        EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execParam_UgyiratGet = execParam.Clone();
        execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

        Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
        if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_ugyiratGet);
        }
        else
        {
            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam_szignalhato = execParam.Clone();
            ErrorDetails errorDetail = null;

            // Ellenõrzés, hogy Szignálható-e az ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.Szignalhato(ugyiratStatusz, execParam_szignalhato, out errorDetail) == false)
            {
                // Nem Szignálható az ügyirat:
                throw new ResultException(52611, errorDetail);
            }
            else
            {
                #region szignálás típusának lekérése

                string szignalasTipusa = null;

                // SzignalasTipus meghatározása az Ugykor_Id, Ugytipus alapján:
                EREC_SzignalasiJegyzekekService service_szignJegyzek = new EREC_SzignalasiJegyzekekService(this.dataContext);

                ExecParam execParam_szignTipus = execParam.Clone();

                Result result_szignalasTipus = service_szignJegyzek.GetSzignalasTipusaByUgykorUgytipus(execParam_szignTipus
                    , erec_UgyUgyirat.IraIrattariTetel_Id, erec_UgyUgyirat.UgyTipus);
                if (!String.IsNullOrEmpty(result_szignalasTipus.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_szignalasTipus);
                }

                if (result_szignalasTipus.Record != null && (!String.IsNullOrEmpty(result_szignalasTipus.Record.ToString())))
                {
                    szignalasTipusa = result_szignalasTipus.Record.ToString();
                    //javasoltFelelos = result_szignalasTipus.Uid;

                    Logger.Info("Szignálás típusa: " + szignalasTipusa, execParam);
                    //Logger.Info("Javasolt felelõs mezõ: " + javasoltFelelos, _ExecParam);
                }
                else
                {
                    //szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._1_Szervezetre_Ugyintezore_Iktatas;
                    // default a 6-os:
                    szignalasTipusa = KodTarak.SZIGNALAS_TIPUSA._6_IktatoSzabadonValaszthat;
                }

                #endregion szignálás típusának lekérése

                string Csoport_Id_Felelos_Uj = CsoportId_Szervezet;

                if (szignalasTipusa != KodTarak.SZIGNALAS_TIPUSA._6_IktatoSzabadonValaszthat)
                {
                    // 2009.01.12 (a szervezet lesz a kezelõ, nem a vezetõ)

                    // CR#2245 EB: 2009.12.16 - ha van megadva szignálás típus, akkor MÉGIS a vezetõ lesz a kezelõ
                    // másképp mindenki láthatja a tételt
                    #region Megadott szervezet vezetõjének lekérdezése:

                    KRT_CsoportokService service_csoportok =
                        Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

                    ExecParam execParam_getLeader = execParam.Clone();

                    Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, CsoportId_Szervezet);
                    if (!String.IsNullOrEmpty(result_GetLeader.ErrorCode)
                        || result_GetLeader.Record == null)
                    {

                        // hiba: Nincs meg a szervezet vezetõje

                        Logger.Error("Ügyirat szervezetre szignálása: Nem határozható meg a csoport vezetõje: " + CsoportId_Szervezet, execParam);
                        throw new ResultException(52612);
                    }

                    KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;

                    if (String.IsNullOrEmpty(csoportVezeto.Id))
                    {
                        // hiba: Nincs meg a szervezet vezetõje

                        Logger.Error("Ügyirat szervezetre szignálása: Nem határozható meg a csoport vezetõje: " + CsoportId_Szervezet, execParam);
                        throw new ResultException(52612);
                    }

                    Csoport_Id_Felelos_Uj = csoportVezeto.Id;

                    #endregion
                }

                // ha változott a felelõs (nem magára szignálta), át kell adni
                bool kellAtadas = false;
                // BUG_8765
                // BUG_11085
                if (CsoportId_Szervezet.Equals(erec_UgyUgyirat.Csoport_Id_Felelos, StringComparison.InvariantCultureIgnoreCase) == false)
                //if ((CsoportId_Szervezet.Equals(erec_UgyUgyirat.Csoport_Id_Felelos, StringComparison.InvariantCultureIgnoreCase) == false)
                //    && (Rendszerparameterek.GetBoolean(execParam, "TUK_SZIGNALAS_ENABLED", false) == false))
                {
                    kellAtadas = true;
                }

                // ÜGYIRAT UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // Felelõs (Kezelõ), Ügyfelelõs, Állapot beállítása:

                // BUG_11085
                if (Rendszerparameterek.GetBoolean(execParam, "TUK_SZIGNALAS_ENABLED", false) == false)
                {
                    // Elõzõ felelõs csoport beállítása
                    erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                    // Felelõs (kezelõ) beállítása  a szervezetre - vagy ha van szignálás típus, akkor a szervezet vezetõjére:
                    //erec_UgyUgyirat.Csoport_Id_Felelos = CsoportId_Szervezet;
                    erec_UgyUgyirat.Csoport_Id_Felelos = Csoport_Id_Felelos_Uj;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;
                }

                // Ügyfelelõs beállítása a megadott szervezetre:
                erec_UgyUgyirat.Csoport_Id_Ugyfelelos = CsoportId_Szervezet;
                erec_UgyUgyirat.Updated.Csoport_Id_Ugyfelelos = true;

                // Szervezetre szignáláskor töröljük az ügyintézõt:
                erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez = String.Empty;
                erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                //// Ügyintézõ beállítása:
                //if (krt_csoportok_felelos.Tipus == KodTarak.CSOPORTTIPUS.Dolgozo)
                //{
                //    erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez = krt_csoportok_felelos.Id;
                //    erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                //}
                //else
                //{
                //    Logger.Debug("Szignálás: Felelõs csoport nem egyszemélyes, ügyintézõ mezõ nincs kitöltve", execParam);
                //}

                // Állapot átállítása:
                if (bSzignalasCausesTovabbitasAlattAllapot && kellAtadas)
                {
                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.Szignalt;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                }
                else
                {
                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Szignalt;
                    erec_UgyUgyirat.Updated.Allapot = true;
                }

                // BLG_1677
                // Szignálás rögzítése
                erec_UgyUgyirat.SzignaloId = execParam.Felhasznalo_Id;
                erec_UgyUgyirat.Updated.SzignaloId = true;

                erec_UgyUgyirat.SzignalasIdeje = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.SzignalasIdeje = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                {
                    // hiba volt:
                    throw new ResultException(result_ugyiratUpdate);
                }
                else
                {
                    #region kézbesítési tétel továbbítása
                    if (kellAtadas && !String.IsNullOrEmpty(result_ugyiratUpdate.Uid))
                    {
                        ExecParam execParam_atadas = execParam.Clone();
                        execParam_atadas.Record_Id = result_ugyiratUpdate.Uid;

                        EREC_IraKezbesitesiTetelekService service_atadas = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                        Result result_atadas = service_atadas.Atadas(execParam_atadas, result_ugyiratUpdate.Uid);

                        if (result_atadas.IsError)
                        {
                            throw new ResultException(result);
                        }
                    }
                    #endregion kézbesítési tétel továbbítása

                    // Kezelési feljegyzés INSERT, ha megadták
                    if (erec_HataridosFeladatok != null)
                    {
                        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                        ExecParam execParam_kezFeljInsert = execParam.Clone();

                        // Ügyirathoz kötés
                        erec_HataridosFeladatok.Obj_Id = erec_UgyUgyirat.Id;
                        erec_HataridosFeladatok.Updated.Obj_Id = true;

                        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                        erec_HataridosFeladatok.Updated.Obj_type = true;

                        if (!String.IsNullOrEmpty(erec_UgyUgyirat.Azonosito))
                        {
                            erec_HataridosFeladatok.Azonosito = erec_UgyUgyirat.Azonosito;
                            erec_HataridosFeladatok.Updated.Azonosito = true;
                        }

                        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                            execParam_kezFeljInsert, erec_HataridosFeladatok);
                        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_kezFeljInsert);
                        }
                    }


                    result = result_ugyiratUpdate;
                }

                //if (Rendszerparameterek.GetBoolean(execParam, "IRAT_SZIGNALAS_ELERHETO", false))
                //{
                //    EREC_IraIratokService erec_IraIratokService = new EREC_IraIratokService(this.dataContext);
                //    ExecParam execParam_Irat = execParam.Clone();
                //    Result iratSzignalasResult = erec_IraIratokService.SzignalasByUgyirat(execParam_Irat, erec_UgyUgyiratok_Id, CsoportId_Szervezet);
                //    iratSzignalasResult.CheckError();
                //}
            }
        }

        #region Eseménynaplózás
        if (isTransactionBeginHere)
        {
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "Szignalas").Record;

            eventLogService.Insert(execParam, eventLogRecord);
        }
        #endregion

        return result;
    }

    /// <summary>
    /// Ügyiratok tömeges átvétele ügyintézésre
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="id_Array"></param>    
    /// <returns></returns>
    [WebMethod()]
    public Result AtvetelUgyintezesre_Tomeges(ExecParam execParam, string[] id_Array)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || id_Array == null || id_Array.Length == 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52162);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ügyiratok ellenõrzése

            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGetAll = execParam.Clone();

            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();

            ugyiratokSearch.Id.In(id_Array);

            Result result_ugyiratGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, ugyiratokSearch);
            if (!String.IsNullOrEmpty(result_ugyiratGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGetAll);
            }

            // Ellenõrzés, megvan-e minden rekord:
            if (result_ugyiratGetAll.Ds == null
                || result_ugyiratGetAll.GetCount != id_Array.Length)
            {
                // hiba:
                throw new ResultException(52161);
            }

            // Verziók kigyûjtéséhez:
            Dictionary<string, string> idVerDictionary = new Dictionary<string, string>();



            // Ellenõrzés egyesével:
            foreach (DataRow row in result_ugyiratGetAll.Ds.Tables[0].Rows)
            {
                idVerDictionary.Add(row["Id"].ToString(), row["Ver"].ToString());

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataRow(row);

                ExecParam execParam_AtvehetoUgyintezesre = execParam.Clone();
                execParam_AtvehetoUgyintezesre.Record_Id = ugyiratStatusz.Id;
                ErrorDetails errorDetail = new ErrorDetails();

                // Ellenõrzés, hogy átvehetõ-e ügyintézéaew
                if (Contentum.eRecord.BaseUtility.Ugyiratok.AtvehetoUgyintezesre(
                    ugyiratStatusz, execParam_AtvehetoUgyintezesre, out errorDetail) == false)
                {
                    // Nem vehetõ át ügyintéézésres    
                    throw new ResultException(52161, errorDetail);
                }

                ExecParam ep = execParam_AtvehetoUgyintezesre.Clone();
                AtvetelUgyintezesre(ep, row["Id"].ToString());


            }

            #endregion

            // Verziólista felépítése: 
            string vers = "";

            foreach (string ugyiratId in id_Array)
            {
                // a dictionary-ben benne kell lenni az ügyiratId-nak (ha nincs, hiba lesz)
                string ugyiratVer = idVerDictionary[ugyiratId];

                if (!String.IsNullOrEmpty(vers))
                {
                    vers += ",";
                }

                vers += ugyiratVer;
            }


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba volt:
                throw new ResultException(result);
            }
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat átvétele ügyintézésre
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza:
    /// execParam.Felhasznalo_Id lesz az, aki megpróbálja az ügyiratot ügyintézésre átvenni</param>
    /// <param name="erec_UgyUgyiratok_Id">Az ügyirat Id-ja</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod()]
    public Result AtvetelUgyintezesre(ExecParam execParam, String erec_UgyUgyiratok_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52160);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    throw new ResultException(52162);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                #region Ellenõrzés - Átvehetõ-e ügyintézésre

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atveheto = execParam.Clone();
                execParam_atveheto.Record_Id = erec_UgyUgyiratok_Id;
                ErrorDetails errorDetail = null;

                // Ellenõrzés, hogy Átvehetõ-e ügyintézésre az ügyirat:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam_atveheto, out errorDetail) == false)
                {
                    throw new ResultException(52161, errorDetail);
                }

                if (erec_UgyUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Elintezett
                    && !(ugyiratStatusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott && ugyiratStatusz.IsElektronikus)) // BUG_14775 DUP - Elektronikus irattárban lévő iratok további kezelése
                {
                    // !!!
                    // Ellenõrzés, tagja-e a felelõs csoportnak (Ugyiratok.AtvehetoUgyintezesre ezt nem kezeli)
                    RightsService service_rightsService = new RightsService(this.dataContext);

                    Result result_isMember = service_rightsService.IsUserMember(execParam_atveheto, erec_UgyUgyirat.Csoport_Id_Felelos);
                    if (!String.IsNullOrEmpty(result_isMember.ErrorCode)
                        || ((bool)result_isMember.Record) != true)
                    {
                        throw new ResultException(52235);
                    }

                    if (erec_UgyUgyirat.Csoport_Id_Felelos != execParam_atveheto.Felhasznalo_Id)
                    {
                        KRT_FunkciokService krt_funkciokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FunkciokService();

                        if (!krt_funkciokService.HasFunctionRight(execParam.Clone(), "AtvetelSzervezettol"))
                        {
                            throw new ResultException(52237);
                        }
                    }

                    // ha engedélyezett kikérõn lévõ állapotban van és betekintésre kérték el, nem vehetõ át
                    if (erec_UgyUgyirat.TovabbitasAlattAllapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
                    {
                        EREC_IrattariKikeroService kikeroService = new EREC_IrattariKikeroService(this.dataContext);
                        ExecParam kikeroExecParam = execParam.Clone();
                        kikeroExecParam.Record_Id = erec_UgyUgyiratok_Id;
                        Result kikeroResult = kikeroService.GetByUgyiratId(kikeroExecParam);

                        if (!string.IsNullOrEmpty(kikeroResult.ErrorCode) || (kikeroResult.Record as EREC_IrattariKikero).FelhasznalasiCel == "B")
                        {
                            throw new ResultException(52235);
                        }
                    }
                }
                #endregion

                // ÜGYIRAT UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // Módosul az õrzõ, (ügyintézõ,??) állapot

                String felhSajatCsopId = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);
                if (String.IsNullOrEmpty(felhSajatCsopId))
                {
                    // hiba:
                    throw new ResultException(52160);
                }

                //elintézett ügyiratnál, elintézés visszavonása
                this.ElintezetteNyilvanitasVisszavonasaInternal(execParam.Clone(), erec_UgyUgyirat);

                erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = felhSajatCsopId;
                erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                //// Ügyintézõ mezõt ügyintézõre szignáláskor nem állítjuk, az Szignáláskor lett állítva
                // Szervezetre átadott és szervezetre szignált ügyiratnál nincs elõre kitöltve az ügyintézõ
                if (String.IsNullOrEmpty(erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez))
                {
                    erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez = felhSajatCsopId;
                    erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                }

                // Állapot állítása:
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                erec_UgyUgyirat.Updated.Allapot = true;

                if (!String.IsNullOrEmpty(erec_UgyUgyirat.TovabbitasAlattAllapot))
                {
                    erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                }

                // Felelõs állítása, ha az nem az örzõ
                if (erec_UgyUgyirat.Csoport_Id_Felelos != felhSajatCsopId)
                {
                    erec_UgyUgyirat.Csoport_Id_Felelos = felhSajatCsopId;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;
                }

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                {
                    // hiba volt:
                    //ContextUtil.SetAbort();
                    //log.WsEnd(execParam_ugyiratUpdate, result_ugyiratUpdate);
                    //return result_ugyiratUpdate;

                    throw new ResultException(result_ugyiratUpdate);
                }
                else
                {

                    /// Kézbesítési tételek vizsgálata:
                    /// ha volt a rekordra kézbesítési tétel, átadottnak beállítani
                    /// 

                    EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                    ExecParam execParam_kezbTetel = execParam.Clone();

                    Result result_kezbTetelUpdate =
                        service_KezbesitesiTetelek.SetKezbesitesiTetelToAtvett(execParam_kezbTetel, erec_UgyUgyirat.Id);

                    if (!String.IsNullOrEmpty(result_kezbTetelUpdate.ErrorCode))
                    {
                        // hiba:
                        //ContextUtil.SetAbort();
                        //log.WsEnd(execParam_kezbTetel, result_kezbTetelUpdate);
                        //return result_kezbTetelUpdate;

                        throw new ResultException(result_kezbTetelUpdate);
                    }



                    // ha minden OK:

                    result = result_ugyiratUpdate;
                }

            }

            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratAtvetelUgyintezesre").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat átvétele ügyintézésre + sakkóra státusz állítás
    /// (NMHH-s webservice-hez)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="SakkoraStatus"></param>
    /// <param name="OkKod"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result AtvetelUgyintezesreSakkoraAllitassal(ExecParam execParam, String erec_UgyUgyiratok_Id, string sakkoraStatus, string okKod)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Átvétel ügyintézésre hívása:
            result = this.AtvetelUgyintezesre(execParam, erec_UgyUgyiratok_Id);
            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Ügyirat UPDATE

            // sakkoraStatus értéke alapján az okKod beírása vagy a FelfuggesztesOka, vagy a LezarasOka mezõbe

            if (!String.IsNullOrEmpty(sakkoraStatus)
                && !String.IsNullOrEmpty(okKod))
            {
                var sakkoraKodok = KodTar_Cache.GetKodtarakByKodCsoportList("IRAT_HATASA_UGYINTEZESRE", execParam, HttpContext.Current.Cache, null);
                var sakkoraKodtarElem = sakkoraKodok.FirstOrDefault(e => e.Kod == sakkoraStatus);

                if (sakkoraKodtarElem == null)
                {
                    throw new ResultException("Érvénytelen sakkórakód: " + sakkoraStatus);
                }

                ExecParam execParam_UgyiratGet = execParam.Clone();
                execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

                Result result_ugyiratGet = this.Get(execParam_UgyiratGet);
                if (result_ugyiratGet.IsError)
                {
                    throw new ResultException(result_ugyiratGet);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                #region UGYIRAT TULAJDONSAG BEALLITAS
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                switch (sakkoraKodtarElem.Egyeb)
                {
                    case "PAUSE":
                        erec_UgyUgyirat.FelfuggesztesOka = new Sakkora().TurnFelfuggesztesOka(execParam, okKod);
                        erec_UgyUgyirat.Updated.FelfuggesztesOka = true;
                        break;
                    case "STOP":
                        erec_UgyUgyirat.LezarasOka = okKod;
                        erec_UgyUgyirat.Updated.LezarasOka = true;
                        break;
                }
                #endregion UGYIRAT TULAJDONSAG BEALLITAS

                // Ügyirat UPDATE:
                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_ugyiratUpdate = this.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (result_ugyiratUpdate.IsError)
                {
                    throw new ResultException(result_ugyiratUpdate);
                }
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    private static bool IsSkontroJovahagyasEnabled(ExecParam xpm)
    {
        bool skontroJovahagyasEnabled = Rendszerparameterek.GetBoolean(xpm, Rendszerparameterek.SKONTRO_JOVAHAGYAS_ENABLED);

        return skontroJovahagyasEnabled;
    }

    private static bool NeedSkontroJovahagyas(ExecParam p_xpm, out string vezetoID)
    {
        vezetoID = String.Empty;

        if (!IsSkontroJovahagyasEnabled(p_xpm))
            return false;

        //vezetoID = Csoportok.GetFelhasznaloVezetoID(p_xpm);
        //if (vezetoID == p_xpm.Felhasznalo_Id)
        //    return false;


        //// ha van SkontrobaAdasJovahagyasNelkul joga, akkor sem kell jóváhagyni
        //bool bHasFunctionRight = HasFunctionRight(p_xpm, Contentum.eUtility.Constants.Funkcio.UgyiratSkontrobaHelyezesJovahagyasNelkul);
        //return !bHasFunctionRight;

        // ha van UgyiratAtadasIrattarbaJovahagyasNelkul joga, akkor sem kell jóváhagyni
        if (HasFunctionRight(p_xpm, Contentum.eUtility.Constants.Funkcio.UgyiratSkontrobaHelyezesJovahagyasNelkul))
        {
            return false;
        }

        vezetoID = Csoportok.GetFirstRightedLeader_ID(p_xpm, true, Contentum.eUtility.Constants.Funkcio.UgyiratSkontrobaHelyezesJovahagyas);
        if (vezetoID == p_xpm.Felhasznalo_Id)
            return false;
        else
            return true; // TODO: hibakezelés!!!
    }


    public void AddKezelesiFeljegyzes(ExecParam p_ExecParam, string p_UgyiratID, string p_Leiras, string p_Tipus)
    {
        Logger.Debug("HataridosFeladatokService Insert start", p_ExecParam);
        Logger.Debug("Leiras: " + p_Leiras ?? "NULL");
        Logger.Debug("KuldemenyID: " + p_UgyiratID ?? "NULL");
        Logger.Debug("Tipus: " + p_Tipus ?? "NULL");

        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
        ExecParam execParam_kezFeljInsert = p_ExecParam.Clone();

        EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
        erec_HataridosFeladatok.Updated.SetValueAll(false);
        erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

        // Ügyirathoz kötés
        erec_HataridosFeladatok.Obj_Id = p_UgyiratID;
        erec_HataridosFeladatok.Updated.Obj_Id = true;

        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
        erec_HataridosFeladatok.Updated.Obj_type = true;

        erec_HataridosFeladatok.Leiras = p_Leiras;
        erec_HataridosFeladatok.Updated.Leiras = true;

        erec_HataridosFeladatok.Altipus = p_Tipus;
        erec_HataridosFeladatok.Updated.Altipus = true;

        erec_HataridosFeladatok.Tipus = KodTarak.FELADAT_TIPUS.Megjegyzes;
        erec_HataridosFeladatok.Updated.Tipus = true;

        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(execParam_kezFeljInsert, erec_HataridosFeladatok);
        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
        {
            // hiba:
            Logger.Error("HataridosFeladatokService Insert hiba", execParam_kezFeljInsert, result_kezFeljInsert);
            throw new ResultException(result_kezFeljInsert);
        }
        Logger.Debug("HataridosFeladatokService Insert end", p_ExecParam);
    }

    /// <summary>
    /// Ügyirat skontróba helyezése
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">Az ügyirat Id-ja</param>
    /// <param name="skontroHatarido">A skontró határideje, megadása nem kötelezõ</param>
    /// <param name="skontroOka">A skontróba helyezés oka, megadása nem kötelezõ</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    [WebMethod()]
    public Result SkontroInditas(ExecParam execParam, String erec_UgyUgyiratok_Id, String skontroHatarido, String skontroOka)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52170);
            Logger.Error("(execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)|| String.IsNullOrEmpty(erec_UgyUgyiratok_Id)", execParam, result1);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            bool esemenyNaplozas_SkontroJovahagyas = false;

            // Ügyirat lekérése:
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                //// hiba:
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    Logger.Error("result_ugyiratGet.Record == null", execParam_UgyiratGet);
                    throw new ResultException(52172);
                }
                Logger.Debug("Ugyirat lekerese end");

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_skontrobahelyzheto = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Ellenõrzés, hogy skontróba helyezhetõ-e az ügyirat:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontrobaHelyezheto(
                    ugyiratStatusz, execParam_skontrobahelyzheto, out errorDetail) == false)
                {
                    // Nem helyezhetõ skontróba az ügyirat:
                    Logger.Error("Ugyiratok.SkontrobaHelyezheto(ugyiratStatusz, execParam_skontrobahelyzheto) = false", execParam_skontrobahelyzheto);
                    throw new ResultException(52171, errorDetail);
                }
                else
                {

                    // ÜGYIRAT UPDATE:
                    // mezõk módosíthatóságának letiltása:
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    // Amit módosítani kell: SkontrobaDat, SkontroVege(Hatarido),SkontroOka, Allapot

                    erec_UgyUgyirat.SkontrobaDat = DateTime.Now.ToString();
                    erec_UgyUgyirat.Updated.SkontrobaDat = true;

                    erec_UgyUgyirat.SkontroVege = skontroHatarido;
                    erec_UgyUgyirat.Updated.SkontroVege = true;

                    // CR3407
                    // Ha SKONTRO_OKA_VALASZTHATO Rendszerparameter = 1, akkor a skontroOka a skontroOka_Kod értéket, ha nem akkor a szöveges okot tartalmazza
                    if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.SKONTRO_OKA_VALASZTHATO, false))
                    {
                        erec_UgyUgyirat.SkontroOka_Kod = skontroOka;
                        erec_UgyUgyirat.Updated.SkontroOka_Kod = true;
                    }
                    else
                    {
                        erec_UgyUgyirat.SkontroOka = skontroOka;
                        erec_UgyUgyirat.Updated.SkontroOka = true;
                    }
                    // Állapot változtatás:
                    string vezetoID;
                    bool needJovahagyas = false;
                    if (NeedSkontroJovahagyas(execParam, out vezetoID))
                    {
                        Logger.Debug("NeedSkontroJovahagyas");
                        Logger.Debug("vezetoID: " + vezetoID);
                        needJovahagyas = true;

                        if (String.IsNullOrEmpty(vezetoID))
                        {
                            // Nem található az aktuális felhasználó fölé rendelt,
                            // adott jogosultsággal rendelkezõ vezetõ a szervezeti hierarchiában!
                            throw new ResultException(53202);
                        }
                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa;
                        erec_UgyUgyirat.Updated.Allapot = true;
                        //Vezetõ beállítása következõ felelõsnek!!!
                        erec_UgyUgyirat.Kovetkezo_Felelos_Id = vezetoID;
                        erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;
                    }
                    else
                    {
                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Skontroban;
                        erec_UgyUgyirat.Updated.Allapot = true;

                        esemenyNaplozas_SkontroJovahagyas = true;
                    }


                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                    {
                        // hiba volt:
                        Logger.Error("Ugyirat update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
                        throw new ResultException(result_ugyiratUpdate);
                    }
                    else
                    {
                        // Kezelési feljegyzés készítése, ha megadták a skontró okát:
                        if (!needJovahagyas && !String.IsNullOrEmpty(skontroOka))
                        {
                            // CR3407
                            String skontroOkaTxt = String.Empty;
                            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.SKONTRO_OKA_VALASZTHATO, false))
                            {
                                skontroOkaTxt = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("SKONTRO_OKA", skontroOka, execParam, HttpContext.Current.Cache) + " (" + skontroOka + ")";
                            }
                            else
                                skontroOkaTxt = skontroOka;

                            Logger.Debug("!needJovahagyas && !String.IsNullOrEmpty(skontroOka)");
                            Logger.Debug("skontoOka:" + skontroOkaTxt);
                            this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, skontroOkaTxt, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                        }


                        // ha minden OK:
                        result = result_ugyiratUpdate;
                    }
                }
            }


            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id
                    , "EREC_UgyUgyiratok", "UgyiratSkontrobaHelyezes").Record;

                eventLogService.Insert(execParam, eventLogRecord);

                // ha rögtön skontróba is lett téve, naplózunk egy jóváhagyást is
                if (esemenyNaplozas_SkontroJovahagyas)
                {
                    KRT_Esemenyek eventLogRecord_jovahagyas = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id
                    , "EREC_UgyUgyiratok", "UgyiratSkontrobaHelyezesJovahagyasa").Record;

                    eventLogService.Insert(execParam, eventLogRecord_jovahagyas);
                }
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Skontróba helyezési kérelem jóváhagyása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SkontroJovahagyas(ExecParam execParam, String erec_UgyUgyiratok_Id, String Megjegyzes)
    {
        Logger.Debug("UgyUgyiratokService SkontroJovahagyas start");
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null)
        {
            // hiba
            Logger.Error("SkontroJovahagyas hiba: execParam = null");
            Result result1 = ResultError.CreateNewResultWithErrorCode(52177);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Arguments args = new Arguments();
            args.Add(new Argument("Ügyirat azonosító", erec_UgyUgyiratok_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("erec_UgyUgyiratok_Id: " + erec_UgyUgyiratok_Id);


            // Ügyirat lekérése:
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                //// hiba:
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }

            if (result_ugyiratGet.Record == null)
            {
                // hiba
                Logger.Error("result_ugyiratGet.Record = null", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(52172);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
            Logger.Debug("Ugyirat lekerese end");

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam_skontroJovahagyhato = execParam.Clone();
            ErrorDetails errorDetail = null;

            // Ellenõrzés, hogy skontróba helyezhetõ-e az ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontroJovahagyhato(
                ugyiratStatusz, execParam_skontroJovahagyhato, out errorDetail) == false)
            {
                // Nem helyezhetõ skontróba az ügyirat:
                Logger.Error("Contentum.eRecord.BaseUtility.Ugyiratok.SkontroJovahagyhato = false", execParam_skontroJovahagyhato, result);
                throw new ResultException(52171, errorDetail);
            }

            // ÜGYIRAT UPDATE:
            // mezõk módosíthatóságának letiltása:
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            // Amit módosítani kell: Allapot
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Skontroban;
            erec_UgyUgyirat.Updated.Allapot = true;

            //Törölni a következõ felelõst!!!
            erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;


            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

            Logger.Debug("Ugyirat Update start");
            Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba volt:
                Logger.Error("Ugyirat Update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
                throw new ResultException(result_ugyiratUpdate);
            }
            Logger.Debug("Ugyirat Update end");
            // Kezelési feljegyzés készítése, ha megadták a skontró okát:
            // CR3407
            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.SKONTRO_OKA_VALASZTHATO, false))
            {
                if (!String.IsNullOrEmpty(erec_UgyUgyirat.SkontroOka_Kod))
                {
                    String skontroOkaTxt = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("SKONTRO_OKA", erec_UgyUgyirat.SkontroOka_Kod, execParam, HttpContext.Current.Cache) + " (" + erec_UgyUgyirat.SkontroOka_Kod + ")";

                    Logger.Debug("SkontroOka: " + skontroOkaTxt);
                    // this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, erec_UgyUgyirat.SkontroOka, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                    this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, skontroOkaTxt, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                }
            }
            else
            {

                if (!String.IsNullOrEmpty(erec_UgyUgyirat.SkontroOka))
                {
                    Logger.Debug("SkontroOka: " + erec_UgyUgyirat.SkontroOka);
                    this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, erec_UgyUgyirat.SkontroOka, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                }
            }
            if (!String.IsNullOrEmpty(Megjegyzes))
            {
                Logger.Debug("Megjegyzes: " + Megjegyzes);
                this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, Megjegyzes, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }

            // ha minden OK:
            result = result_ugyiratUpdate;


            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id
                    , "EREC_UgyUgyiratok", "UgyiratSkontrobaHelyezesJovahagyasa").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("UgyUgyiratokService SkontroJovahagyas hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("UgyUgyiratokService SkontroJovahagyas end");
        return result;

    }

    /// <summary>
    /// Skontróba helyezési kérelem elutasítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SkontroElutasitas(ExecParam execParam, String erec_UgyUgyiratok_Id, String Megjegyzes)
    {
        Logger.Debug("UgyUgyiratokService SkontroElutasitas start");
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null)
        {
            // hiba
            Logger.Error("SkontroElutasitas hiba: execParam = null");
            Result result1 = ResultError.CreateNewResultWithErrorCode(52178);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Arguments args = new Arguments();
            args.Add(new Argument("Ügyirat azonosító", erec_UgyUgyiratok_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("erec_UgyUgyiratok_Id: " + erec_UgyUgyiratok_Id);


            // Ügyirat lekérése:
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                //// hiba:
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }

            if (result_ugyiratGet.Record == null)
            {
                // hiba
                Logger.Error("result_ugyiratGet.Record = null", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(52172);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
            Logger.Debug("Ugyirat lekerese end");

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam_skontroElutasithato = execParam.Clone();
            ErrorDetails errorDetail = null;

            // Ellenõrzés, hogy skontróba helyezhetõ-e az ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontroElutasithato(
                ugyiratStatusz, execParam_skontroElutasithato, out errorDetail) == false)
            {
                // Nem helyezhetõ skontróba az ügyirat:
                Logger.Error("Contentum.eRecord.BaseUtility.Ugyiratok.SkontroElutasithato = false", execParam_skontroElutasithato, result);
                throw new ResultException(52176, errorDetail);
            }

            // ÜGYIRAT UPDATE:
            // mezõk módosíthatóságának letiltása:
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            // Amit módosítani kell: SkontrobaDat, SkontroVege, SkontroOka
            erec_UgyUgyirat.SkontrobaDat = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontrobaDat = true;

            erec_UgyUgyirat.SkontroVege = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontroVege = true;

            string skontroOka = erec_UgyUgyirat.SkontroOka;
            erec_UgyUgyirat.SkontroOka = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontroOka = true;

            // CR3407
            string skontroOka_Kod = erec_UgyUgyirat.SkontroOka_Kod;
            erec_UgyUgyirat.SkontroOka_Kod = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontroOka_Kod = true;

            // Állapot módosítása:
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            erec_UgyUgyirat.Updated.Allapot = true;

            //Következõ felelõs törlése
            erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

            Logger.Debug("Ugyirat Update start");
            Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba volt:
                Logger.Error("Ugyirat Update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
                throw new ResultException(result_ugyiratUpdate);
            }
            Logger.Debug("Ugyirat Update end");
            // Kezelési feljegyzés készítése, ha megadták a skontró okát:
            // CR3407
            if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.SKONTRO_OKA_VALASZTHATO, false))
            {
                if (!String.IsNullOrEmpty(skontroOka_Kod))
                {
                    String skontroOkaTxt = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("SKONTRO_OKA", skontroOka_Kod, execParam, HttpContext.Current.Cache) + " (" + skontroOka_Kod + ")";

                    Logger.Debug("SkontroOka: " + skontroOkaTxt);
                    // this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, erec_UgyUgyirat.SkontroOka, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                    this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, skontroOkaTxt, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                }
            }
            else
            {

                if (!String.IsNullOrEmpty(skontroOka))
                {
                    Logger.Debug("SkontroOka: " + skontroOka);
                    this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, skontroOka, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                }
            }

            //if (!String.IsNullOrEmpty(skontroOka))
            //{
            //    Logger.Debug("SkontroOka: " + skontroOka);
            //    this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, skontroOka, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            //}

            if (!String.IsNullOrEmpty(Megjegyzes))
            {
                Logger.Debug("Megjegyzes: " + Megjegyzes);
                this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, Megjegyzes, KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }

            // ha minden OK:
            result = result_ugyiratUpdate;


            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id
                    , "EREC_UgyUgyiratok", "UgyiratSkontrobaHelyezesElutasitasa").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("UgyUgyiratokService SkontroElutasitas hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("UgyUgyiratokService SkontroElutasitas end");
        return result;

    }

    /// <summary>
    /// Skontróba helyezési kérelem visszavonása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SkontroVisszavonas(ExecParam execParam, String erec_UgyUgyiratok_Id, String Megjegyzes)
    {
        Logger.Debug("UgyUgyiratokService SkontroVisszavonas start");
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null)
        {
            // hiba
            Logger.Error("SkontroVisszavonas hiba: execParam = null");
            Result result1 = ResultError.CreateNewResultWithErrorCode(52179);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Arguments args = new Arguments();
            args.Add(new Argument("Ügyirat azonosító", erec_UgyUgyiratok_Id, ArgumentTypes.Guid));
            args.ValidateArguments();

            Logger.Debug("erec_UgyUgyiratok_Id: " + erec_UgyUgyiratok_Id);


            // Ügyirat lekérése:
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                //// hiba:
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }

            if (result_ugyiratGet.Record == null)
            {
                // hiba
                Logger.Error("result_ugyiratGet.Record = null", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(52172);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
            Logger.Debug("Ugyirat lekerese end");

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam_skontroVisszavonhato = execParam.Clone();
            ErrorDetails errorDetail = null;

            //Ellenõrzés, hogy skontróba helyezhetõ-e az ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontroVisszavonhato(
                ugyiratStatusz, execParam_skontroVisszavonhato, out errorDetail) == false)
            {
                // Nem helyezhetõ skontróba az ügyirat:
                Logger.Error("Contentum.eRecord.BaseUtility.Ugyiratok.SkontroVisszavonhato = false", execParam_skontroVisszavonhato, result);
                throw new ResultException(52174, errorDetail);
            }

            // ÜGYIRAT UPDATE:
            // mezõk módosíthatóságának letiltása:
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            // Amit módosítani kell: SkontrobaDat, SkontroVege, SkontroOka
            erec_UgyUgyirat.SkontrobaDat = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontrobaDat = true;

            erec_UgyUgyirat.SkontroVege = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontroVege = true;

            string skontroOka = erec_UgyUgyirat.SkontroOka;
            erec_UgyUgyirat.SkontroOka = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontroOka = true;

            // CR3407
            string skontroOka_Kod = erec_UgyUgyirat.SkontroOka_Kod;
            erec_UgyUgyirat.SkontroOka_Kod = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.SkontroOka_Kod = true;


            // Állapot módosítása:
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            erec_UgyUgyirat.Updated.Allapot = true;

            //Következõ felelõs törlése
            erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

            Logger.Debug("Ugyirat Update start");
            Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba volt:
                Logger.Error("Ugyirat Update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
                throw new ResultException(result_ugyiratUpdate);
            }
            Logger.Debug("Ugyirat Update end");

            // ha minden OK:
            result = result_ugyiratUpdate;


            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id
                    , "EREC_UgyUgyiratok", "UgyiratSkontrobaHelyezesVisszavonasa").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error("UgyUgyiratokService SkontroVisszavonas hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("UgyUgyiratokService SkontroVisszavonas end");
        return result;
    }

    /// <summary>
    /// Ügyirat kivétele skontróból
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">Az ügyirat Id-ja, amit skontróból ki szándékozunk venni</param>
    /// <param name="skontroVege">A skontró vége (ha nincs megadva, a mai dátum)</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    [WebMethod()]
    public Result SkontrobolKivetel(ExecParam execParam, string erec_UgyUgyiratok_Id, string skontroVegeDat)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52180);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52183);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_skontrobolkiveheto = execParam.Clone();


                // Ellenõrzés, hogy skontróból kivehetõ-e az ügyirat:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontrobolKiveheto(
                    ugyiratStatusz, execParam_skontrobolkiveheto) == false)
                {
                    // Nem vehetõ ki skontróból az ügyirat:
                    throw new ResultException(52181);
                }
                else
                {

                    // ÜGYIRAT UPDATE:
                    // mezõk módosíthatóságának letiltása:
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    /// Amit módosítani kell: SkontrobaDat, SkontroVege(Hatarido),SkontroOka : értéküket törölni
                    /// + SkontrobanOsszesen-t növelni, Hatarido-t novelni a skontróban töltött idõvel,
                    /// + Allapot
                    /// 

                    try
                    {
                        DateTime skontroKezd = DateTime.Parse(erec_UgyUgyirat.SkontrobaDat);

                        DateTime skontroVege = DateTime.Now;
                        if (!String.IsNullOrEmpty(skontroVegeDat))
                        {
                            try
                            {
                                skontroVege = DateTime.Parse(skontroVegeDat);
                            }
                            catch
                            {
                                skontroVege = DateTime.Now;
                            }
                        }

                        //TimeSpan kulonbseg_TimeSpan = DateTime.Now.Subtract(skontroKezd);
                        TimeSpan kulonbseg_TimeSpan = skontroVege.Subtract(skontroKezd);

                        int skontrobanToltottNapok = (Int32)kulonbseg_TimeSpan.TotalDays;
                        int skontrobanOsszesen = 0;
                        try
                        {
                            skontrobanOsszesen = Int32.Parse(erec_UgyUgyirat.SkontrobanOsszesen);
                        }
                        catch
                        {
                            skontrobanOsszesen = 0;
                        }

                        if (skontrobanToltottNapok > 0)
                        {
                            skontrobanOsszesen += skontrobanToltottNapok;

                            // Skontróban eddig töltött napok számának növelése
                            erec_UgyUgyirat.SkontrobanOsszesen = skontrobanOsszesen.ToString();
                            erec_UgyUgyirat.Updated.SkontrobanOsszesen = true;

                            DateTime hatarIdo = DateTime.Parse(erec_UgyUgyirat.Hatarido);
                            hatarIdo.AddDays(skontrobanToltottNapok);

                            // Határidõ kitolása a legutóbbi skontróidõtartammal
                            erec_UgyUgyirat.Hatarido = hatarIdo.ToShortDateString();
                            erec_UgyUgyirat.Updated.Hatarido = true;
                        }
                    }
                    catch
                    {
                    }

                    erec_UgyUgyirat.SkontrobaDat = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.SkontrobaDat = true;

                    erec_UgyUgyirat.SkontroVege = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.SkontroVege = true;

                    erec_UgyUgyirat.SkontroOka = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.SkontroOka = true;
                    // CR3407
                    erec_UgyUgyirat.SkontroOka_Kod = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    erec_UgyUgyirat.Updated.SkontroOka_Kod = true;

                    // Állapot módosítása:
                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                    erec_UgyUgyirat.Updated.Allapot = true;


                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                    {
                        // hiba volt:
                        throw new ResultException(result_ugyiratUpdate);
                    }
                    else
                    {

                        // ha minden OK:
                        result = result_ugyiratUpdate;
                    }
                }
            }


            #region Eseménynaplózás
            if (isConnectionOpenHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id
                    , "EREC_UgyUgyiratok", "UgyiratSkontrobolKivetel").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyiratok tömeges kivétele skontróból
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="id_Array"></param>
    /// <param name="skontroVegeDat"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SkontrobolKivetel_Tomeges(ExecParam execParam, string[] id_Array, string skontroVegeDat)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || id_Array == null || id_Array.Length == 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52180);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ügyiratok ellenõrzése

            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGetAll = execParam.Clone();

            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();

            ugyiratokSearch.Id.In(id_Array);

            Result result_ugyiratGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, ugyiratokSearch);
            if (!String.IsNullOrEmpty(result_ugyiratGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGetAll);
            }

            // Ellenõrzés, megvan-e minden rekord:
            if (result_ugyiratGetAll.Ds == null
                || result_ugyiratGetAll.GetCount != id_Array.Length)
            {
                // hiba:
                throw new ResultException(52184);
            }

            // Verziók kigyûjtéséhez:
            Dictionary<string, string> idVerDictionary = new Dictionary<string, string>();

            DateTime dtSkontroVege = DateTime.Parse(skontroVegeDat);

            // Ellenõrzés egyesével:
            foreach (DataRow row in result_ugyiratGetAll.Ds.Tables[0].Rows)
            {
                idVerDictionary.Add(row["Id"].ToString(), row["Ver"].ToString());

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataRow(row);

                ExecParam execParam_skontrobolkiveheto = execParam.Clone();

                // Ellenõrzés, hogy skontróból kivehetõ-e az ügyirat:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.SkontrobolKiveheto(
                    ugyiratStatusz, execParam_skontrobolkiveheto) == false)
                {
                    // Nem vehetõ ki skontróból az ügyirat:
                    throw new ResultException(52181);
                }

                // A skontroVegeDat nem lehet kisebb, mint a skontro kezdete
                string row_SkontrobaDat = row["SkontrobaDat"].ToString();
                if (!String.IsNullOrEmpty(row_SkontrobaDat))
                {
                    DateTime dtSkontrobaDat = DateTime.Parse(row_SkontrobaDat);
                    if (dtSkontroVege < dtSkontrobaDat)
                    {
                        // hiba: A skontro vége dátuma nem lehet a skontróba adás dátuma elõtti!
                        throw new ResultException(52185);
                    }
                }
            }

            #endregion

            // Verziólista felépítése: 
            string vers = "";

            foreach (string ugyiratId in id_Array)
            {
                // a dictionary-ben benne kell lenni az ügyiratId-nak (ha nincs, hiba lesz)
                string ugyiratVer = idVerDictionary[ugyiratId];

                if (!String.IsNullOrEmpty(vers))
                {
                    vers += ",";
                }

                vers += ugyiratVer;
            }



            ExecParam execParam_skontrobolKi = execParam.Clone();


            DateTime skontroVege = DateTime.Today;
            try
            {
                skontroVege = DateTime.Parse(skontroVegeDat);
            }
            catch
            {
                skontroVege = DateTime.Today;
            }

            result = sp.SkontrobolKivetel_Tomeges(execParam_skontrobolKi, Search.GetSqlInnerString(id_Array), vers, skontroVege);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba volt:
                throw new ResultException(result);
            }
            #region Ugyirat Tomeges Update Tortent

            EREC_UgyUgyiratok UpdatedRecords = new EREC_UgyUgyiratok();
            UpdatedRecords.Updated.SetValueAll(false);
            UpdatedRecords.Base.Updated.SetValueAll(false);
            UpdatedRecords.SkontroVege = skontroVege.ToString();
            UpdatedRecords.Updated.SkontroVege = true;
            UpdatedRecords.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            UpdatedRecords.Updated.Allapot = true;

            TomegesUgyiratUpdateExecuted(execParam, result_ugyiratGetAll.Ds.Tables[0], UpdatedRecords);

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat sztornózása
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">A sztornózandó ügyirat Id-ja</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    [WebMethod()]
    public Result Sztornozas(ExecParam execParam, String erec_UgyUgyiratok_Id, String SztornozasIndoka)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52190);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52191);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_sztornozhato = execParam.Clone();

                #region Ellenõrzés, sztornózható-e

                /// Ellenõrzés, hogy sztornózható-e az ügyirat:
                /// 1. Állapota alapján sztornózható (Nem lezárt)
                /// 2. TODO: A felhasználó nem tagja egyetlen olyan jogalanyi csoportnak sem, amely törlési jogszinttel elérheti az objektumot
                /// 3. Már több alszámmal rendelkezik az ügyirat
                /// 4. TODO: Olyan alszámmal rendelkezik az ügyirat, amely már postázva lett vagy más módon ki lett küldve.
                /// 5. Az ügyiratra másik (beleszerelt, belecsatolt) ügyirat hivatkozik


                // Állapota alapján sztornózható?      
                ErrorDetails errorDetail;
                if (Contentum.eRecord.BaseUtility.Ugyiratok.Sztornozhato(
                    ugyiratStatusz, execParam_sztornozhato, out errorDetail) == false)
                {
                    // Nem sztornózható az ügyirat:
                    throw new ResultException(52192, errorDetail);
                }

                // Hány irat van benne? (ami nincs sztornózva/felszabadítva)
                int iratSzam = 0;
                try
                {
                    iratSzam = Int32.Parse(erec_UgyUgyirat.IratSzam);
                }
                catch
                {
                    Result result_iratSzam = erec_UgyUgyiratokService.GetIratszam(execParam, erec_UgyUgyirat.Id);
                    if (!String.IsNullOrEmpty(result_iratSzam.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_iratSzam);
                    }


                    iratSzam = Int32.Parse(result_iratSzam.Record.ToString());
                }

                if (iratSzam > 1)
                {
                    // több alszámmal is rendelkezik az ügyirat

                    throw new ResultException(52193);
                }


                /// TODO: A felhasználó nem tagja egyetlen olyan jogalanyi csoportnak sem, amely törlési jogszinttel elérheti az objektumot


                #region Iratok ellenõrzése (Nem lehet kiadmányozott)

                // Irat(ok) lekérése:
                EREC_IraIratokService service_iraIratok = new EREC_IraIratokService(this.dataContext);
                ExecParam execParam_iratokGetAll = execParam.Clone();

                EREC_IraIratokSearch search_iratok = new EREC_IraIratokSearch(true);

                search_iratok.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Filter(erec_UgyUgyirat.Id);

                // BUG_13800
                // Szűrésből kihagyandó
                List<string> AllapotList = new List<string>();
                AllapotList.Add(KodTarak.IRAT_ALLAPOT.Sztornozott);
                AllapotList.Add(KodTarak.IRAT_ALLAPOT.Atiktatott);
                AllapotList.Add(KodTarak.IRAT_ALLAPOT.Felszabaditva);

                if (AllapotList.Count > 0)
                {
                    search_iratok.Allapot.Value = Search.GetSqlInnerString(AllapotList.ToArray());
                    search_iratok.Allapot.Operator = Query.Operators.notinner;
                }

                Result result_iratokGetAll = service_iraIratok.GetAllWithExtension(execParam_iratokGetAll, search_iratok);
                if (!String.IsNullOrEmpty(result_iratokGetAll.ErrorCode))
                {
                    // hiba
                    throw new ResultException(result_iratokGetAll);
                }

                // BUG_13800
                if (result_iratokGetAll.HasData())
                {

                    DataRow row_irat = result_iratokGetAll.Ds.Tables[0].Rows[0];
                    // irat állapotának lekérése:
                    String iratAllapot = row_irat["Allapot"].ToString();

                    if (iratAllapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
                    {
                        // Nem sztornózható az ügyirat:
                        throw new ResultException(52194);
                    }
                }
                #endregion


                #region Az ügyiratra másik (beleszerelt, belecsatolt) ügyirat hivatkozik-e?

                Logger.Info("Ügyirat sztornózás - szerelt ügyiratok vizsgálata Start", execParam);

                #region Beleszerelt ügyirat van-e?

                EREC_UgyUgyiratokSearch search_szereltek = new EREC_UgyUgyiratokSearch();

                // hozni kell azokat, ahol õ a szülõügyirat (elõiratok) (ebbe az ügyiratba szerelt ügyiratok)

                search_szereltek.UgyUgyirat_Id_Szulo.Filter(erec_UgyUgyirat.Id);

                ExecParam execParam_getAllSzereltek = execParam.Clone();

                Result result_getAllSzereltek = this.GetAll(execParam_getAllSzereltek, search_szereltek);
                if (!String.IsNullOrEmpty(result_getAllSzereltek.ErrorCode))
                {
                    // hiba:
                    Logger.Error("Hiba a szerelt ügyiratok lekérése során", execParam, result_getAllSzereltek);
                    throw new ResultException(result_getAllSzereltek.ErrorCode);
                }

                if (result_getAllSzereltek.GetCount > 0)
                {
                    // van beleszerelt ügyirat, nem sztornózható:
                    Logger.Error("Az ügyirat nem sztornózható, már van beleszerelve másik ügyirat", execParam);

                    throw new ResultException(52196);
                }
                #endregion

                #region Csatolás és migrált szerelés ellenõrzése az EREC_UgyiratObjKapcsolatok táblából

                Logger.Debug("Ügyirat sztronózás - Csatolás és migrált szerelés ellenõrzése...", execParam);

                /// Nem lehet sztornózni, ha benne van egy csatolásban, illetve ha migrált ügyirat van beleszerelve

                EREC_UgyiratObjKapcsolatokService service_ugyiratObjKapcsolatok = new EREC_UgyiratObjKapcsolatokService(this.dataContext);

                EREC_UgyiratObjKapcsolatokSearch search_ugyiratObjKapcsolatok = new EREC_UgyiratObjKapcsolatokSearch();

                // vagy az Obj_Id_Elozmeny-ben, vagy az Obj_Id_Kapcsolt-ban benne van-e, ahol kapcsolattípus Csatolt, vagy MigraltSzereles
                search_ugyiratObjKapcsolatok.Obj_Id_Elozmeny.Filter(erec_UgyUgyirat.Id);
                search_ugyiratObjKapcsolatok.Obj_Id_Elozmeny.OrGroup("123");

                search_ugyiratObjKapcsolatok.Obj_Id_Kapcsolt.Filter(erec_UgyUgyirat.Id);
                search_ugyiratObjKapcsolatok.Obj_Id_Kapcsolt.OrGroup("123");

                // Csatolt, vagy MigraltSzereles:
                search_ugyiratObjKapcsolatok.KapcsolatTipus.In(new string[] { KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt, KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles });

                ExecParam execParam_ugyiratCsatolasok = execParam.Clone();

                Result result_ugyiratObjKapcs = service_ugyiratObjKapcsolatok.GetAll(execParam_ugyiratCsatolasok, search_ugyiratObjKapcsolatok);
                if (result_ugyiratObjKapcs.IsError)
                {
                    // hiba:
                    throw new ResultException(result_ugyiratObjKapcs);
                }

                if (result_ugyiratObjKapcs.GetCount > 0)
                {
                    // vagy Csatolás miatt, vagy migrált szerelés miatt nem sztornózható az ügyirat:
                    DataRow row = result_ugyiratObjKapcs.Ds.Tables[0].Rows[0];

                    string kapcsolatTipus = row["KapcsolatTipus"].ToString();

                    Logger.Debug("Ügyirat sztornó - Ellenõrzés: EREC_UgyiratObjKapcsolatok.KapcsolatTipus: " + kapcsolatTipus, execParam);

                    if (kapcsolatTipus == KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt)
                    {
                        // hiba, az ügyirat csatolva van:
                        throw new ResultException(52197);
                    }
                    else if (kapcsolatTipus == KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles)
                    {
                        // hiba, az ügyiratba szerelve van migrált ügyirat:
                        throw new ResultException(52199);
                    }
                }
                #endregion

                #endregion


                #endregion


                #region ÜGYIRAT UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // Módosítani kell: Állapot, Sztornózás dátuma

                erec_UgyUgyirat.SztornirozasDat = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.SztornirozasDat = true;

                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Sztornozott;
                erec_UgyUgyirat.Updated.Allapot = true;

                // Iratszám legyen 0
                erec_UgyUgyirat.IratSzam = "0";
                erec_UgyUgyirat.Updated.IratSzam = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_UgyiratUpdate);
                }
                #endregion

                #region Ügyirat sztornózása esetén az esetleges kézbesítési tételek törlése

                EREC_IraKezbesitesiTetelekService service_kezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_kezbTetelInvalidate = execParam.Clone();

                Result result_kezbTetelInvalidate =
                    service_kezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_kezbTetelInvalidate, erec_UgyUgyirat.Id);

                if (!String.IsNullOrEmpty(result_kezbTetelInvalidate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kezbTetelInvalidate);
                }
                #endregion

                #region Ügyirat alatti EGYETLEN irat sztornózott állapotba állítása:

                // elvileg csak egy irat lehet az ügyirat alatt (ha több volt, már hibát dobtunk)
                // (az irat az ellenõrzéskor már le lett kérve)

                string iratId = "";

                if (result_iratokGetAll.GetCount == 1)
                {
                    iratId = result_iratokGetAll.Ds.Tables[0].Rows[0]["Id"].ToString();
                    string iratVer = result_iratokGetAll.Ds.Tables[0].Rows[0]["Ver"].ToString();
                    string irat_Allapot = result_iratokGetAll.Ds.Tables[0].Rows[0]["Allapot"].ToString();

                    #region IRAT UPDATE

                    // Csak akkor kell sztornózni, ha sem átiktatott, sem felszabadított, se nem sztornózott az irat állapota
                    if (irat_Allapot != KodTarak.IRAT_ALLAPOT.Atiktatott
                        && irat_Allapot != KodTarak.IRAT_ALLAPOT.Sztornozott
                        && irat_Allapot != KodTarak.IRAT_ALLAPOT.Felszabaditva)
                    {
                        EREC_IraIratok iratObj_update = new EREC_IraIratok();

                        iratObj_update.Updated.SetValueAll(false);
                        iratObj_update.Base.Updated.SetValueAll(false);

                        iratObj_update.Base.Ver = iratVer;
                        iratObj_update.Base.Updated.Ver = true;

                        iratObj_update.Id = iratId;

                        // Állapot mezõ: Sztornózott
                        iratObj_update.Allapot = KodTarak.IRAT_ALLAPOT.Sztornozott;
                        iratObj_update.Updated.Allapot = true;

                        // Sztornó dátuma:
                        iratObj_update.SztornirozasDat = DateTime.Now.ToString();
                        iratObj_update.Updated.SztornirozasDat = true;

                        ExecParam execParam_IratUpdate = execParam.Clone();
                        execParam_IratUpdate.Record_Id = iratId;

                        Result result_iratUpdate = service_iraIratok.Update(execParam_IratUpdate, iratObj_update);
                        if (!String.IsNullOrEmpty(result_iratUpdate.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_iratUpdate);
                        }
                    }
                    #endregion
                }

                #endregion

                #region Iratpéldányok sztornózása:

                KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);
                Result barkodResult = null;

                if (!String.IsNullOrEmpty(iratId))
                {
                    Logger.Info("Ügyirat iratpéldányainak sztornózása", execParam);

                    EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);

                    EREC_PldIratPeldanyokSearch search_getAllPld = new EREC_PldIratPeldanyokSearch();

                    search_getAllPld.IraIrat_Id.Filter(iratId);

                    ExecParam execParam_getAllPld = execParam.Clone();

                    Result result_getAllPld = service_Pld.GetAll(execParam_getAllPld, search_getAllPld);
                    if (!String.IsNullOrEmpty(result_getAllPld.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_getAllPld);
                    }

                    foreach (DataRow row in result_getAllPld.Ds.Tables[0].Rows)
                    {
                        // Ellenõrzés, tényleg jót kértünk-e le:
                        string pld_iratId = row["IraIrat_Id"].ToString();
                        if (pld_iratId != iratId)
                        {
                            // hiba:
                            Logger.Error("Hiba: Ügyirat sztornózás - Iratpéldányok lekérése", execParam);
                            throw new ResultException(52198);
                        }

                        string pldId = row["Id"].ToString();
                        string pldVer = row["Ver"].ToString();

                        #region IRATPÉLDÁNY UPDATE

                        EREC_PldIratPeldanyok pldObj_Update = new EREC_PldIratPeldanyok();

                        pldObj_Update.Updated.SetValueAll(false);
                        pldObj_Update.Base.Updated.SetValueAll(false);

                        pldObj_Update.Base.Ver = pldVer;
                        pldObj_Update.Base.Updated.Ver = true;

                        // Állapot: Sztornózott:
                        pldObj_Update.Allapot = KodTarak.IRATPELDANY_ALLAPOT.Sztornozott;
                        pldObj_Update.Updated.Allapot = true;

                        // Sztornózás dátuma:
                        pldObj_Update.SztornirozasDat = DateTime.Now.ToString();
                        pldObj_Update.Updated.SztornirozasDat = true;

                        // Vonalkód
                        pldObj_Update.BarCode = row["BarCode"].ToString();

                        ExecParam execParam_pldUpdate = execParam.Clone();
                        execParam_pldUpdate.Record_Id = pldId;

                        Result result_pldUpdate = service_Pld.Update(execParam_pldUpdate, pldObj_Update);
                        if (!String.IsNullOrEmpty(result_pldUpdate.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_pldUpdate);
                        }

                        #region Vonalkód felszabadítása

                        if (!string.IsNullOrEmpty(pldObj_Update.BarCode))
                        {
                            barkodResult = barkodService.FreeBarcode(execParam.Clone(), pldObj_Update.BarCode);
                            if (!string.IsNullOrEmpty(barkodResult.ErrorCode))
                                throw new ResultException(barkodResult);
                        }

                        #endregion

                        #endregion

                        #region Iratpéldány kézbesítési tételeinek törlése:

                        ExecParam execParam_kezbTetelInv_Pld = execParam.Clone();

                        Result result_kezbTetelInv_Pld =
                            service_kezbesitesiTetelek.NemAtadottKezbesitesiTetelInvalidate(execParam_kezbTetelInv_Pld, pldId);

                        if (!String.IsNullOrEmpty(result_kezbTetelInv_Pld.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_kezbTetelInv_Pld);
                        }

                        #endregion
                    }

                }

                #endregion

                #region Vonalkód felszabadítása

                if (!string.IsNullOrEmpty(erec_UgyUgyirat.BARCODE))
                {
                    barkodResult = barkodService.FreeBarcode(execParam.Clone(), erec_UgyUgyirat.BARCODE);
                    if (!string.IsNullOrEmpty(barkodResult.ErrorCode))
                        throw new ResultException(barkodResult);
                }

                #endregion

                // ha minden OK:
                result = result_UgyiratUpdate;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratSztorno").Record;

                if (!String.IsNullOrEmpty(SztornozasIndoka))
                {
                    eventLogRecord.Base.Note = SztornozasIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            // CR3191 : EDOK-ban rollback-elt tranzakció visszagörgetése migrált állományban is
            // Átiktatás sztornózásnál tovább kell dobni a hibát 
            if (execParam.FunkcioKod == "AtIktatas")
            {
                throw new ResultException(result);
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyiratok tömeges átadásra kijelölése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id_Array"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result AtadasraKijeloles_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52200);
            }
            else
            {
                Result result_atadasraKijeloles = new Result();

                EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
                search.Id.In(erec_UgyUgyiratok_Id_Array);

                Result getAllResult = GetAll(execParam.Clone(), search);
                if (!string.IsNullOrEmpty(getAllResult.ErrorCode))
                    throw new ResultException(getAllResult);

                System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), getAllResult.Ds);
                if (statusz.Count == 0)
                    throw new ResultException(52380);
                ErrorDetails errorDetail = null;

                var ugyiratIds = string.Empty;
                string selectedVers = string.Empty;
                foreach (DataRow r in getAllResult.Ds.Tables[0].Rows)
                {
                    ugyiratIds += ",'" + r["Id"].ToString() + "'";
                    selectedVers += "," + r["Ver"].ToString();

                    if (!Contentum.eRecord.BaseUtility.Ugyiratok.AtadasraKijelolheto((statusz[r["Id"].ToString()] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)
                        , execParam.Clone(), out errorDetail))
                        throw new ResultException(52201, errorDetail);
                }

                ugyiratIds = ugyiratIds.TrimStart(',');
                selectedVers = selectedVers.TrimStart(',');

                //result = sp.AtadasraKijeloles_Tomeges(execParam.Clone(),
                //            ugyiratIds, selectedVers,
                //            csoport_Id_Felelos_Kovetkezo,
                //            erec_HataridosFeladatok != null ? erec_HataridosFeladatok.KezelesTipus : string.Empty,
                //            erec_HataridosFeladatok != null ? erec_HataridosFeladatok.Leiras : string.Empty
                //            );

                result = sp.AtadasraKijeloles_Tomeges(execParam.Clone(),
                            ugyiratIds, selectedVers,
                            csoport_Id_Felelos_Kovetkezo,
                            string.Empty,
                            string.Empty
                            );

                #region Határidõs Feladat

                if (erec_HataridosFeladatok != null)
                {
                    EREC_HataridosFeladatokService svcHataridosFeladat = new EREC_HataridosFeladatokService(dataContext);
                    svcHataridosFeladat.SimpleInsertTomeges(execParam.Clone(), erec_HataridosFeladatok, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, erec_UgyUgyiratok_Id_Array);
                }

                #endregion

                result.CheckError();

                #region Eseménynaplózás
                if (AddEsemenyNaplo)
                {
                    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                    Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ugyiratIds, "UgyiratAtadas");
                    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                        Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtadasTomeges: ", execParam, eventLogResult);
                }
                #endregion

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// Ügyirat átadásra kijelölése
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">Az átadásra kijelölendõ ügyirat Id-ja</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A felelõs csoport megadása, akinek az ügyiratot átadni szándékozzák</param>
    /// <param name="erec_HataridosFeladatok">A mûvelethez esetleg megadott kezelési feljegyzés, ami a megadott ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod()]
    public Result AtadasraKijeloles(ExecParam execParam, String erec_UgyUgyiratok_Id
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id)
            || String.IsNullOrEmpty(csoport_Id_Felelos_Kovetkezo))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52200);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52204);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atadasraKijelolheto = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Átadásra kijelölhetõ?
                if (csoport_Id_Felelos_Kovetkezo == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam_atadasraKijelolheto).Obj_Id)
                {
                }
                else if (Contentum.eRecord.BaseUtility.Ugyiratok.AtadasraKijelolheto(
                    ugyiratStatusz, execParam_atadasraKijelolheto, out errorDetail) == false)
                {
                    // Nem jelölhetõ ki átadásra az ügyirat:
                    throw new ResultException(52201, errorDetail);
                }


                // ÜGYIRAT UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // Módosítani kell: Felelõs, Elõzõ felelõs, Állapot, TovabbitasAlattAllapot

                // Elõzõ felelõs állítása:
                erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = erec_UgyUgyirat.Csoport_Id_Felelos;
                erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;

                // Új felelõs beállítása:
                erec_UgyUgyirat.Csoport_Id_Felelos = csoport_Id_Felelos_Kovetkezo;
                erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                // Továbbítás alatti állapot beállítása a mostanira (kivéve ha most továbbítás alattiban vagyunk):
                if (erec_UgyUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                {
                    erec_UgyUgyirat.TovabbitasAlattAllapot = erec_UgyUgyirat.Allapot;
                    erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                }

                // Új állapot: Továbbítás alatt
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                erec_UgyUgyirat.Updated.Allapot = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                Result result_UgyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (!String.IsNullOrEmpty(result_UgyiratUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_UgyiratUpdate);
                }
                else
                {
                    // Kezelési feljegyzés INSERT, ha megadták
                    if (erec_HataridosFeladatok != null)
                    {
                        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                        ExecParam execParam_kezFeljInsert = execParam.Clone();

                        // Ügyirathoz kötés
                        erec_HataridosFeladatok.Obj_Id = erec_UgyUgyirat.Id;
                        erec_HataridosFeladatok.Updated.Obj_Id = true;

                        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                        erec_HataridosFeladatok.Updated.Obj_type = true;

                        if (!String.IsNullOrEmpty(erec_UgyUgyirat.Azonosito))
                        {
                            erec_HataridosFeladatok.Azonosito = erec_UgyUgyirat.Azonosito;
                            erec_HataridosFeladatok.Updated.Azonosito = true;
                        }

                        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                            execParam_kezFeljInsert, erec_HataridosFeladatok);
                        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                        {
                            // hiba:
                            //ContextUtil.SetAbort();
                            //log.WsEnd(execParam_kezFeljInsert, result_kezFeljInsert);
                            //return result_kezFeljInsert;

                            throw new ResultException(result_kezFeljInsert);
                        }
                    }

                    // ha minden OK:
                    result = result_UgyiratUpdate;
                }
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "Atadas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat Irattarba kuldese
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">Az átadásra kijelölendõ ügyirat Id-ja</param>
    /// <param name="IrattarJellege">Irattar jellege: Atmeneti 0, Kozponti 1</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A felelõs csoport megadása, akinek az ügyiratot átadni szándékozzák</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasIrattarba(ExecParam execParam, String erec_UgyUgyiratok_Id
        , String IraIrattariTetel_Id, String Megjegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            Logger.Error("(execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)|| String.IsNullOrEmpty(IrattarJellege)", execParam, result1);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52204);
                    //log.WsEnd(execParam_UgyiratGet, result1);
                    //return result1;
                    Logger.Error("result_ugyiratGet.Record == null", execParam_UgyiratGet);
                    throw new ResultException(52204);
                }
                Logger.Debug("Ugyirat lekerese end");

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atadasraKijelolheto = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Irattárba küldhetõ?
                if (Contentum.eRecord.BaseUtility.Ugyiratok.IrattarbaKuldheto(
                    ugyiratStatusz, execParam_atadasraKijelolheto, out errorDetail) == false)
                {
                    // Nem küldhetõ irattárba:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52202);
                    //log.WsEnd(execParam_atadasraKijelolheto, result1);
                    //return result1;
                    Logger.Error("Ugyiratok.IrattarbaKuldheto(ugyiratStatusz, execParam_atadasraKijelolheto) = false", execParam_atadasraKijelolheto);
                    throw new ResultException(52202, errorDetail);
                }

                // Elõkészített szerelésre ellenõrzés
                #region Elõkészített szerelésre ellenõrzés
                ExecParam execParamGetAll = execParam.Clone();
                EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(false);
                search.UgyUgyirat_Id_Szulo.Filter(erec_UgyUgyiratok_Id);

                search.Allapot.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Szerelt);

                search.TovabbitasAlattAllapot.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
                // elég a létezést vizsgálni
                search.TopRow = 1;

                Result result_getAll = sp.GetAll(execParamGetAll, search);
                if (result_getAll.IsError)
                {
                    throw new ResultException(result_getAll);
                }

                // Ha volt elõkészített szerelés, akkor hiba
                if (result_getAll.GetCount > 0)
                {
                    //"Az ügyirat nem küldhetõ irattárba, mert szerelésre elõkészített ügyiratot tartalmaz!"
                    throw new ResultException(50207);
                }
                #endregion Elõkészített szerelésre ellenõrzés

                string vezetoID;
                //átadás vezetõnek vagy egybõl az irattárnak (vezetõ a Kovetkezo_Felelos_Id-be kerül, de nem történik tényleges átadás
                Result resAtadas = new Result();
                ExecParam xpmAtadas = execParam.Clone();
                if (NeedIrattarozasJovahagyas(execParam, out vezetoID))
                {
                    Logger.Debug("NeedIrattarozasJovahagyas");
                    Logger.Debug("vezetoID: " + vezetoID);

                    if (String.IsNullOrEmpty(vezetoID))
                    {
                        // Nem található az aktuális felhasználó fölé rendelt,
                        // adott jogosultsággal rendelkezõ vezetõ a szervezeti hierarchiában!
                        throw new ResultException(53202);
                    }

                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa;
                    erec_UgyUgyirat.Updated.Allapot = true;
                    //Vezetõ beállítása következõ felelõsnek!!!
                    erec_UgyUgyirat.Kovetkezo_Felelos_Id = vezetoID;
                    erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                    // címzett és továbbítás alatt állapot beállítása
                    erec_UgyUgyirat.Csoport_Id_Cimzett = csoport_Id_Felelos_Kovetkezo;
                    erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);
                    if (result_ugyiratUpdate.IsError)
                    {
                        // hiba volt:
                        Logger.Error("Ugyirat update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
                        throw new ResultException(result_ugyiratUpdate);
                    }
                    else
                    {
                        // Kezelési feljegyzés létrehozása, ha van megjegyzés:
                        if (!String.IsNullOrEmpty(Megjegyzes.Trim()))
                        {
                            Logger.Debug("!needJovahagyas && !String.IsNullOrEmpty(Megjegyzes.Trim())");
                            Logger.Debug("Megjegyzes.Trim():" + Megjegyzes.Trim());
                            this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, Megjegyzes.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                        }

                        result = result_ugyiratUpdate;
                    }
                }
                else
                {
                    resAtadas = this.AtadasIrattarbaJovahagyasa(xpmAtadas, erec_UgyUgyiratok_Id, IraIrattariTetel_Id, Megjegyzes, IrattarJellege, csoport_Id_Felelos_Kovetkezo);
                    if (resAtadas.IsError)
                    {
                        throw new ResultException(resAtadas);
                    }

                    result = resAtadas;
                }

                // Az esetleges munkairatok sztornózása:
                this.MunkairatokSztornozasa(erec_UgyUgyiratok_Id, execParam);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratAtadasIrattarba").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// A megadott ügyiratban lévõ munkairatok sztornózása
    /// </summary>
    /// <param name="ugyiratId"></param>
    private void MunkairatokSztornozasa(string ugyiratId, ExecParam execParam)
    {
        this.MunkairatokSztornozasa(new List<string>() { ugyiratId }, execParam);
    }

    /// <summary>
    /// A megadott ügyiratokban lévõ munkairatok sztornózása
    /// </summary>
    /// <param name="ugyiratIdList"></param>
    /// <param name="execParam"></param>
    private void MunkairatokSztornozasa(List<string> ugyiratIdList, ExecParam execParam)
    {
        Logger.Debug("MunkairatokSztornozasa Start...");

        if (ugyiratIdList == null || ugyiratIdList.Count == 0) { return; }

        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
        iratSearch.Ugyirat_Id.In(ugyiratIdList);

        // Munkairat: az alszám vagy null, vagy 0
        iratSearch.Alszam.Value = "0";
        iratSearch.Alszam.Operator = Query.Operators.isnullorequals;

        // Állapot még nem sztornózott:
        iratSearch.Allapot.NotEquals(KodTarak.IRAT_ALLAPOT.Sztornozott);

        var iratService = new EREC_IraIratokService(this.dataContext);

        // Ügyirat iratainak lekérése:
        var resultMunkairatok = iratService.GetAll(execParam, iratSearch);
        resultMunkairatok.CheckError();

        foreach (DataRow rowMunkairat in resultMunkairatok.Ds.Tables[0].Rows)
        {
            // Irat sztornózása:
            string munkaIratId = rowMunkairat["Id"].ToString();
            Logger.Info("Irat sztornó: " + munkaIratId);

            ExecParam execParamIratSztorno = execParam.Clone();
            execParamIratSztorno.Record_Id = munkaIratId;

            string ugyiratId = rowMunkairat["Ugyirat_Id"].ToString();

            var resultIratSztorno = iratService.Sztornozas(execParamIratSztorno, null, ugyiratId);
            resultIratSztorno.CheckError();
        }

        Logger.Debug("MunkairatokSztornozasa End...");
    }

    private void SetNotes(EREC_UgyUgyiratok erec_UgyUgyirat, string megjegyzes, string feljegyzes)
    {
        //LZS - BUG_7099
        //Hozzáfűzzük az eddigi szöveghez sortöréssel elválasztva a megjegyzés textboxba írt szöveget.
        if (megjegyzes != null && megjegyzes.Trim().Length > 0)
        {
            // TODO: handle max 4000 chars or change db field to nvarchar(max)
            // BUG_11081
            var note = String.IsNullOrEmpty(erec_UgyUgyirat.Base.Note) ? null
                : JsonConvert.DeserializeObject<Contentum.eRecord.BaseUtility.Note_JSON>(erec_UgyUgyirat.Base.Note);
            if (note == null) // új objektumot kell létrehozni
            {
                note = new Contentum.eRecord.BaseUtility.Note_JSON();
            }
            note.Megjegyzes = String.IsNullOrEmpty(note.Megjegyzes) ? megjegyzes : note.Megjegyzes + Environment.NewLine + megjegyzes;
            erec_UgyUgyirat.Base.Note = JsonConvert.SerializeObject(note);
            erec_UgyUgyirat.Base.Updated.Note = true;
        }

        //LZS - BUG_7099
        //Lementjük a Feljegyzés szöveget is az EREC_UgyUgyiratok.Megjegyzés mezőbe.
        if (feljegyzes != null && feljegyzes.Trim().Length > 0)
        {
            erec_UgyUgyirat.Megjegyzes = feljegyzes;
            erec_UgyUgyirat.Updated.Megjegyzes = true;
        }
    }

    #region Irattárba küldés tõmeges 
    /// <summary>
    /// Ügyirat Irattarba kuldese tömegesen
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="erec_UgyUgyiratok_Id">Az átadásra kijelölendõ ügyiratok Id-ja</param>
    /// <param name="IrattarJellege">Irattar jellege: Atmeneti 0, Kozponti 1</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A felelõs csoport megadása, akinek az ügyiratot átadni szándékozzák</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasIrattarba_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Ids
        , String IraIrattariTetel_Id, String Megjegyzes, String Feljegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            Logger.Error("(execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)|| String.IsNullOrEmpty(IrattarJellege)", execParam, result1);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ügyirat lekérése:
            //TODO: Tömegesítés
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.In(erec_UgyUgyiratok_Ids);

            Result result_ugyiratGet = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGet, search);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }
            if (result_ugyiratGet.GetCount == 0)
            {
                //hiba
                Logger.Error("result_ugyiratGet.Record == null", execParam_UgyiratGet);
                throw new ResultException(52204);
            }
            Logger.Debug("Ugyirat lekerese end");
            ErrorDetails errorDetail = null;

            System.Collections.Hashtable statuszok = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), result_ugyiratGet.Ds);

            ExecParam execParam_atadasraKijelolheto = execParam.Clone();
            ExecParam xpmAtadas = execParam.Clone();

            List<string> ugyiratIds = new List<string>();
            List<string> ugyiratVers = new List<string>();
            List<string> ugyiratAzon = new List<string>();

            Result resAtadas = new Result();

            string vezetoID;
            bool needJovahagyas = NeedIrattarozasJovahagyas(execParam, out vezetoID);
            if (needJovahagyas)
            {
                Logger.Debug("NeedIrattarozasJovahagyas");
                Logger.Debug("vezetoID: " + vezetoID);
            }

            var beforeTextFromNote = "";
            foreach (DataRow dr in result_ugyiratGet.Ds.Tables[0].Rows)
            {
                string id = dr["Id"].ToString();
                string ver = dr["Ver"].ToString();
                string azonosito = dr["Azonosito"].ToString();
                ugyiratIds.Add(id);
                ugyiratVers.Add(ver);
                ugyiratAzon.Add(azonosito);

                //BUG_11384, BUG_7099
                //Kiszedjük, és eltároljuk az EREC_UgyUgyiratok.Note mező értékét.
                if (String.IsNullOrEmpty(beforeTextFromNote))
                {
                    beforeTextFromNote = dr["Note"].ToString();
                }

                // Irattárba küldhetõ?
                if (!Contentum.eRecord.BaseUtility.Ugyiratok.IrattarbaKuldheto(
                    statuszok[id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz, execParam_atadasraKijelolheto, out errorDetail))
                {
                    //hiba
                    Logger.Error("Ugyiratok.IrattarbaKuldheto check", execParam_atadasraKijelolheto);
                    throw new ResultException(52202, errorDetail);
                }
            }
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();

            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);

            erec_UgyUgyirat.Base.Note = beforeTextFromNote; // NOTE: a "Base.Updated.Note = true"-t a SetNotes állítja be, ha szükséges

            #region Ügyiratok átadása Irattárba vagy ha jováhagyás szükséges a vezetõnek
            if (needJovahagyas)
            {
                if (String.IsNullOrEmpty(vezetoID))
                {
                    // Nem található az aktuális felhasználó fölé rendelt,
                    // adott jogosultsággal rendelkezõ vezetõ a szervezeti hierarchiában!
                    throw new ResultException(53202);
                }

                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa;
                erec_UgyUgyirat.Updated.Allapot = true;
                //Vezetõ beállítása következõ felelõsnek!!!
                erec_UgyUgyirat.Kovetkezo_Felelos_Id = vezetoID;
                erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                // címzett és továbbítás alatt állapot beállítása
                erec_UgyUgyirat.Csoport_Id_Cimzett = csoport_Id_Felelos_Kovetkezo;
                erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

                // BUG_11384: megjegyzés, feljegyzés mentése
                SetNotes(erec_UgyUgyirat, Megjegyzes, Feljegyzes);

                int a = 0;
                foreach (var ugyiratId in ugyiratIds)
                {
                    var erec_HataridosFeladatok = CreateTaskNote(Feljegyzes);
                    if (erec_HataridosFeladatok != null)
                    {
                        erec_UgyUgyirat.Id = ugyiratId;
                        erec_UgyUgyirat.Azonosito = ugyiratAzon[a];
                        this.AddEREC_HataridosFeladatok(execParam.Clone(), erec_UgyUgyirat, erec_HataridosFeladatok);
                    }
                    a++;
                }
                erec_UgyUgyirat.Id = "";
            }
            else // Átadás az irattárnak
            {
                //Feljegyzés
                var erec_HataridosFeladatok = CreateTaskNote(Feljegyzes);

                AddEsemenyNaplo = false;
                resAtadas = this.Atadas_Tomeges(xpmAtadas, ugyiratIds.ToArray(), csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
                AddEsemenyNaplo = true;

                resAtadas.CheckError();
                result = resAtadas;

                erec_UgyUgyirat.Allapot = KovetkezoAllapotElektronikusJovahagyaskor(IrattarJellege, execParam, erec_UgyUgyirat);
                erec_UgyUgyirat.Updated.Allapot = true;

                erec_UgyUgyirat.TovabbitasAlattAllapot = String.Empty;
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;

                SetNotes(erec_UgyUgyirat, Megjegyzes, Feljegyzes);

                //Verzió növelése
                for (int i = 0; i < ugyiratVers.Count; i++)
                {
                    int ver;
                    if (Int32.TryParse(ugyiratVers[i], out ver))
                    {
                        ugyiratVers[i] = (ver + 1).ToString();
                    }
                }
            }
            #endregion

            // ügyirat tömeges módosítás
            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            string ugyiratupdIds = Search.GetSqlInnerString(ugyiratIds.ToArray());

            string ugyiratupdVers = String.Join(",", ugyiratVers.ToArray());

            Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update_Tomeges(execParam_ugyiratUpdate, result_ugyiratGet.Ds.Tables[0], ugyiratIds.ToArray(), ugyiratVers.ToArray(), erec_UgyUgyirat, DateTime.Now);
            if (result_ugyiratUpdate.IsError)
            {
                // hiba volt:
                Logger.Error("Ugyirat update hiba", execParam_ugyiratUpdate, result_ugyiratUpdate);
                throw new ResultException(result_ugyiratUpdate);
            }
            result = result_ugyiratUpdate;

            // Esetleges munkairatok sztornózása:
            this.MunkairatokSztornozasa(ugyiratIds, execParam);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string ids = Search.GetSqlInnerString(ugyiratIds.ToArray());

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "UgyiratAtadasIrattarba");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtadasIrattarbaTomeges: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion


    private static bool IsIrattarozasJovahagyasEnabled(ExecParam xpm)
    {
        bool irattarozasEnabled = Rendszerparameterek.GetBoolean(xpm, Rendszerparameterek.IRATTAROZAS_JOVAHAGYAS_ENABLED);

        return irattarozasEnabled;
    }

    private static bool NeedIrattarozasJovahagyas(ExecParam p_xpm, out string vezetoID)
    {
        vezetoID = String.Empty;

        if (!IsIrattarozasJovahagyasEnabled(p_xpm))
            return false;

        //vezetoID = Csoportok.GetFelhasznaloVezetoID(p_xpm);
        //if (vezetoID == p_xpm.Felhasznalo_Id)
        //    return false;

        //// ha van UgyiratAtadasIrattarbaJovahagyasNelkul joga, akkor sem kell jóváhagyni
        //bool bHasFunctionRight = HasFunctionRight(p_xpm, Contentum.eUtility.Constants.Funkcio.UgyiratAtadasIrattarbaJovahagyasNelkul);
        //return !bHasFunctionRight;

        // ha van UgyiratAtadasIrattarbaJovahagyasNelkul joga, akkor sem kell jóváhagyni
        if (HasFunctionRight(p_xpm, Contentum.eUtility.Constants.Funkcio.UgyiratAtadasIrattarbaJovahagyasNelkul))
        {
            return false;
        }

        vezetoID = Csoportok.GetFirstRightedLeader_ID(p_xpm, true, Contentum.eUtility.Constants.Funkcio.UgyiratAtadasIrattarbaJovahagyas);
        if (vezetoID == p_xpm.Felhasznalo_Id)
            return false;
        else
            return true; // TODO: hibakezelés!!!

    }

    /// <summary>
    /// Ügyirat irattárba adási kérelmének jóváhagyása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="IraIrattariTetel_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <param name="IrattarJellege"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasIrattarbaJovahagyasa(ExecParam execParam, String erec_UgyUgyiratok_Id
        , String IraIrattariTetel_Id, String Megjegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            Logger.Debug("Ugyirat lekerese start");
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;
                Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52204);
                    //log.WsEnd(execParam_UgyiratGet, result1);
                    //return result1;
                    Logger.Error("result_ugyiratGet.Record == null", execParam_UgyiratGet);
                    throw new ResultException(52204);
                }
                Logger.Debug("Ugyirat lekerese end");

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atadasraKijelolheto = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Irattárba küldhetõ?
                if (Contentum.eRecord.BaseUtility.Ugyiratok.IrattarbaKuldheto(
                    ugyiratStatusz, execParam_atadasraKijelolheto, out errorDetail) == false)
                {
                    // Nem küldhetõ irattárba:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52202);
                    //log.WsEnd(execParam_atadasraKijelolheto, result1);
                    //return result1;
                    Logger.Error("Ugyiratok.IrattarbaKuldheto(ugyiratStatusz, execParam_atadasraKijelolheto) = false", execParam_atadasraKijelolheto);
                    throw new ResultException(52202, errorDetail);
                }

                //Átadás az irattárnak

                //kezelési feljegyzés létrehozása, ha van megjegyzés
                var erec_HataridosFeladatok = CreateTaskNote(Megjegyzes);

                //átadás
                ExecParam xpmAtadas = execParam.Clone();
                Result resAtadas = this.Atadas(xpmAtadas, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

                if (!String.IsNullOrEmpty(resAtadas.ErrorCode))
                {
                    throw new ResultException(resAtadas);
                }

                //állapot beállítása, irattári tétel, megõrzési idõ
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                /// BUG#7169
                /// Az előző műveletnél (Atadas) akár 1-gyel többel is nőhetett az ügyirat verziószáma, 
                /// így kénytelenek vagyunk a verzió miatt újra lekérni az ügyiratot
                /// 
                var resultUgyiratGet = this.Get(execParam_UgyiratGet);
                resultUgyiratGet.CheckError();

                erec_UgyUgyirat.Base.Ver = (resultUgyiratGet.Record as EREC_UgyUgyiratok).Base.Ver;

                erec_UgyUgyirat.Allapot = KovetkezoAllapotElektronikusJovahagyaskor(IrattarJellege, execParam, erec_UgyUgyirat);
                erec_UgyUgyirat.Updated.Allapot = true;

                // címzett és továbbítás alatt állapot beállítása
                erec_UgyUgyirat.Csoport_Id_Cimzett = csoport_Id_Felelos_Kovetkezo;
                erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

                //erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa;
                erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                //Törölni a következõ felelõst!!!
                erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
                erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                erec_UgyUgyirat.IraIrattariTetel_Id = IraIrattariTetel_Id;
                erec_UgyUgyirat.Updated.IraIrattariTetel_Id = true;

                //if (IrattarJellege == Contentum.eUtility.Constants.IrattarJellege.KozpontiIrattar)
                //{
                ////Irattárba helyezés ideje (csak központi irattárba küldés esetén töltendõ)
                // CR#2062: Átmeneti irattárnál is töltjük
                erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;
                //}

                // CR 3043 Megörzési idõ számítása a Lezárás dátuma alapján történik, így azt ellenõrizni kell hogy nem null-e 
                // nekrisz
                if (!String.IsNullOrEmpty(erec_UgyUgyirat.LezarasDat))
                {
                    Result resMegorzesiIdo = SetMegorzesiIdoFromIrattariTetel(execParam, erec_UgyUgyirat, IraIrattariTetel_Id);

                    if (!String.IsNullOrEmpty(resMegorzesiIdo.ErrorCode))
                    {
                        throw new ResultException(resMegorzesiIdo);
                    }
                }
                ExecParam xpmUpdate = execParam.Clone();
                xpmUpdate.Record_Id = erec_UgyUgyiratok_Id;
                Result resUpdate = this.Update(xpmUpdate, erec_UgyUgyirat);

                if (!String.IsNullOrEmpty(resUpdate.ErrorCode))
                {
                    throw new ResultException(resUpdate);
                }


                result = resUpdate;
            }

            #region Eseménynaplózás
            // Mindig naplózzunk, ha nem közvetlenül hívtuk, akkor is:
            //if (isTransactionBeginHere)
            //{
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                , erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "IrattarbaAdasJovahagyasa").Record;

            Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    // BUG_4765
    private string KovetkezoAllapotElektronikusJovahagyaskor(string irattarJellege, ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        var isElektronikusIrattar = erec_UgyUgyirat != null
           && !Contentum.eRecord.BaseUtility.Ugyiratok.IsIrattarozasElektronikusJovahagyasNelkul(execParam)
           && irattarJellege == "E";

        if (isElektronikusIrattar)
        {
            SetUgyIratElektronikusIrattarba(execParam, ref erec_UgyUgyirat);
        }

        return isElektronikusIrattar ? KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott : KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
    }

    // BUG_8005
    /// <summary>
    /// 
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Ids"></param>
    /// <param name="IraIrattariTetel_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <param name="IrattarJellege"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasIrattarbaJovahagyasa_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Ids,
        String IraIrattariTetel_Id, String Megjegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (erec_UgyUgyiratok_Ids == null || erec_UgyUgyiratok_Ids.Length == 0)
            {
                // hiba
                throw new ResultException(52203);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.In(erec_UgyUgyiratok_Ids);


            Result result_ugyiratGet = erec_UgyUgyiratokService.GetAll(execParam, search);

            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;

                throw new ResultException(result_ugyiratGet);
            }

            System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), result_ugyiratGet.Ds);
            if (statusz.Count == 0)
                throw new ResultException(52380);
            ErrorDetails errorDetail = null;

            List<string> ugyiratIdsList = new List<string>();
            List<string> ugyiratVersList = new List<string>();
            foreach (DataRow r in result_ugyiratGet.Ds.Tables[0].Rows)
            {
                string id = r["Id"].ToString();
                ugyiratIdsList.Add(id);
                string ver = r["Ver"].ToString();
                ugyiratVersList.Add(ver);

                if (!Contentum.eRecord.BaseUtility.Ugyiratok.IrattarbaKuldheto((statusz[id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)
                    , execParam.Clone(), out errorDetail))
                    throw new ResultException(52202, errorDetail);
            }


            //Átadás az irattárnak

            //kezelési feljegyzés létrehozása, ha van megjegyzés
            var erec_HataridosFeladatok = CreateTaskNote(Megjegyzes);

            //átadás
            ExecParam xpmAtadas = execParam.Clone();

            AddEsemenyNaplo = false;
            Result resAtadas = this.Atadas_Tomeges(xpmAtadas, ugyiratIdsList.ToArray(), csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
            AddEsemenyNaplo = true;

            if (!String.IsNullOrEmpty(resAtadas.ErrorCode))
            {
                throw new ResultException(resAtadas);
            }

            //állapot beállítása
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);

            erec_UgyUgyirat.Allapot = KovetkezoAllapotElektronikusJovahagyaskor(IrattarJellege, execParam, erec_UgyUgyirat);
            erec_UgyUgyirat.Updated.Allapot = true;

            // címzett és továbbítás alatt állapot beállítása
            erec_UgyUgyirat.Csoport_Id_Cimzett = csoport_Id_Felelos_Kovetkezo;
            erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

            erec_UgyUgyirat.TovabbitasAlattAllapot = String.Empty;
            erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

            erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
            erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;

            //Törölni a következõ felelõst!!!
            erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

            // BUG_9179
            if (!String.IsNullOrEmpty(IraIrattariTetel_Id))
            {
                erec_UgyUgyirat.IraIrattariTetel_Id = IraIrattariTetel_Id;
                erec_UgyUgyirat.Updated.IraIrattariTetel_Id = true;
            }

            //if (IrattarJellege == Contentum.eUtility.Constants.IrattarJellege.KozpontiIrattar)
            //{
            ////Irattárba helyezés ideje (csak központi irattárba küldés esetén töltendõ)
            // CR#2062: Átmeneti irattárnál is töltjük
            erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
            erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;
            //}

            // CR 3043 Megörzési idõ számítása a Lezárás dátuma alapján történik, így azt ellenõrizni kell hogy nem null-e 
            // nekrisz
            // TODO Ez a tömeges irattarbaadásnál sem szerepel, kell?
            //if (!String.IsNullOrEmpty(erec_UgyUgyirat.LezarasDat))
            //{
            //    Result resMegorzesiIdo = SetMegorzesiIdoFromIrattariTetel(execParam, erec_UgyUgyirat, IraIrattariTetel_Id);

            //    if (!String.IsNullOrEmpty(resMegorzesiIdo.ErrorCode))
            //    {
            //        throw new ResultException(resMegorzesiIdo);
            //    }
            //}

            ExecParam xpmUpdate = execParam.Clone();

            string ugyiratIds = Search.GetSqlInnerString(ugyiratIdsList.ToArray());

            //Verzió növelése
            for (int i = 0; i < ugyiratVersList.Count; i++)
            {
                int ver;
                if (Int32.TryParse(ugyiratVersList[i], out ver))
                {
                    ugyiratVersList[i] = (ver + 1).ToString();

                }
            }

            string ugyiratVers = String.Join(",", ugyiratVersList.ToArray());

            Result resUpdate = this.Update_Tomeges(xpmUpdate, result_ugyiratGet.Ds.Tables[0], ugyiratIdsList.ToArray(), ugyiratVersList.ToArray(), erec_UgyUgyirat, DateTime.Now);

            if (!String.IsNullOrEmpty(resUpdate.ErrorCode))
            {
                throw new ResultException(resUpdate);
            }

            result = resUpdate;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ugyiratIds, "UgyiratAtadasIrattarbaJovahagyas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtadasJovahagyasTomeges: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    // BUG_8005
    /// <summary>
    /// 
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Ids"></param>
    /// <param name="IraIrattariTetel_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <param name="IrattarJellege"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasIrattarbaVisszautasitasa_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Ids,
        String IraIrattariTetel_Id, String Megjegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (erec_UgyUgyiratok_Ids == null || erec_UgyUgyiratok_Ids.Length == 0)
            {
                // hiba
                throw new ResultException(52203);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.In(erec_UgyUgyiratok_Ids);


            Result result_ugyiratGet = erec_UgyUgyiratokService.GetAll(execParam, search);

            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;

                throw new ResultException(result_ugyiratGet);
            }

            System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), result_ugyiratGet.Ds);
            if (statusz.Count == 0)
                throw new ResultException(52380);
            ErrorDetails errorDetail = null;

            List<string> ugyiratIdsList = new List<string>();
            List<string> ugyiratVersList = new List<string>();
            foreach (DataRow r in result_ugyiratGet.Ds.Tables[0].Rows)
            {
                string id = r["Id"].ToString();
                ugyiratIdsList.Add(id);
                string ver = r["Ver"].ToString();
                ugyiratVersList.Add(ver);

                if (!Contentum.eRecord.BaseUtility.Ugyiratok.IrattarbaKuldesVisszautasithato((statusz[id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)
                    , execParam.Clone(), out errorDetail))
                    throw new ResultException(52207, errorDetail);
            }


            //Átadás az elõzõ felelõsnek
            ////kezelési feljegyzés létrehozása, ha van megjegyzés
            //EREC_HataridosFeladatok erec_HataridosFeladatok = null;
            //if (!String.IsNullOrEmpty(Megjegyzes.Trim()))
            //{
            //    erec_HataridosFeladatok = new EREC_HataridosFeladatok();
            //    erec_HataridosFeladatok.Updated.SetValueAll(false);
            //    erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

            //    erec_HataridosFeladatok.KezelesTipus = KodTarak.KezelesiFeljegyzes.Megjegyzes;
            //    erec_HataridosFeladatok.Updated.KezelesTipus = true;

            //    erec_HataridosFeladatok.Leiras = Megjegyzes;
            //    erec_HataridosFeladatok.Updated.Leiras = true;
            //}

            //kezelési feljegyzés létrehozása, ha van megjegyzés
            var erec_HataridosFeladatok = CreateTaskNote(Megjegyzes);
            if (erec_HataridosFeladatok != null)
            {
                EREC_HataridosFeladatokService svcHataridosFeladat = new EREC_HataridosFeladatokService(dataContext);
                svcHataridosFeladat.SimpleInsertTomeges(execParam.Clone(), erec_HataridosFeladatok, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, erec_UgyUgyiratok_Ids);
            }

            // már nem történik átadás a vezetõ felé, ezért nem is kell visszaadni
            ////átadás
            //ExecParam xpmAtadas = execParam.Clone();

            //AddEsemenyNaplo = false;
            //Result resAtadas = this.Atadas_Tomeges(xpmAtadas, ugyiratIdsList.ToArray(), csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
            //AddEsemenyNaplo = true;

            //if (!String.IsNullOrEmpty(resAtadas.ErrorCode))
            //{
            //    throw new ResultException(resAtadas);
            //}

            //állapot beállítása(lezárt), irattári tétel
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;
            //erec_UgyUgyirat.Base.Ver = (Int32.Parse(erec_UgyUgyirat.Base.Ver) + 1).ToString();

            //erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
            //erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
            // csak biztonság kedvéért töröljük
            erec_UgyUgyirat.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
            erec_UgyUgyirat.Updated.Allapot = true;

            //Következõ felelõs törlése
            erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

            //Címzett törlése
            erec_UgyUgyirat.Csoport_Id_Cimzett = Contentum.eUtility.Constants.BusinessDocument.nullString;
            erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

            erec_UgyUgyirat.IraIrattariTetel_Id = IraIrattariTetel_Id;
            erec_UgyUgyirat.Updated.IraIrattariTetel_Id = true;

            ExecParam xpmUpdate = execParam.Clone();

            string ugyiratIds = Search.GetSqlInnerString(ugyiratIdsList.ToArray());

            ////Verzió növelése
            //for (int i = 0; i < ugyiratVersList.Count; i++)
            //{
            //    int ver;
            //    if (Int32.TryParse(ugyiratVersList[i], out ver))
            //    {
            //        ugyiratVersList[i] = (ver + 1).ToString();

            //    }
            //}

            string ugyiratVers = String.Join(",", ugyiratVersList.ToArray());

            Result resUpdate = this.Update_Tomeges(xpmUpdate, result_ugyiratGet.Ds.Tables[0], ugyiratIdsList.ToArray(), ugyiratVersList.ToArray(), erec_UgyUgyirat, DateTime.Now);

            if (!String.IsNullOrEmpty(resUpdate.ErrorCode))
            {
                throw new ResultException(resUpdate);
            }

            result = resUpdate;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ugyiratIds, "UgyiratAtadasIrattarbaVisszautasitas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtadasVisszautasitasTomeges: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Maximális megõrzési idõ beállítása irattári tételbõl
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyirat"></param>
    /// <param name="IraIrattariTetel_Id"></param>
    /// <returns></returns>
    private Result SetMegorzesiIdoFromIrattariTetel(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat, string IraIrattariTetel_Id)
    {
        //Megõrzési idõ kiszámítása irattári tételbõl
        EREC_IraIrattariTetelekService svcIrattariTetelek = new EREC_IraIrattariTetelekService(this.dataContext);
        EREC_IraIrattariTetelekSearch schIrattariTetelek = new EREC_IraIrattariTetelekSearch();
        schIrattariTetelek.Id.Filter(IraIrattariTetel_Id);

        #region érvényesség ellenõrzés kikapcsolása
        schIrattariTetelek.ErvKezd.Clear();
        schIrattariTetelek.ErvVege.Clear();
        #endregion érvényesség ellenõrzés kikapcsolása

        ExecParam xpmIrattariTetelek = execParam.Clone();

        Result resIrattariTetelek = svcIrattariTetelek.GetAllWithExtension(xpmIrattariTetelek, schIrattariTetelek);

        if (!String.IsNullOrEmpty(resIrattariTetelek.ErrorCode))
        {
            throw new ResultException(resIrattariTetelek);
        }

        if (resIrattariTetelek.GetCount == 0)
        {
            //Irattári tétel nem található
            throw new ResultException(52600);
        }

        if (resIrattariTetelek.Ds.Tables[0].Rows[0]["IrattariJel"].ToString() == "S" || resIrattariTetelek.Ds.Tables[0].Rows[0]["IrattariJel"].ToString() == "L")
        {
            // BLG_2156
            //if (resIrattariTetelek.Ds.Tables[0].Rows[0]["MegorzesiIdo"].ToString() != "0"
            //    || resIrattariTetelek.Ds.Tables[0].Rows[0]["MegorzesiIdo"].ToString() != "HN")
            if (resIrattariTetelek.Ds.Tables[0].Rows[0]["MegorzesiIdo"].ToString() != "HN")
            {
                int megorzesiIdo;
                if (Int32.TryParse(resIrattariTetelek.Ds.Tables[0].Rows[0]["MegorzesiIdo_Nev"].ToString(), out megorzesiIdo))
                {
                    // CR 3043 Megörzési idõ számítása a Lezárás dátuma alapján történik 
                    // nekrisz 
                    //erec_UgyUgyirat.Typed.MegorzesiIdoVege = this.GetMegorzesiIdoVege(DateTime.Now, megorzesiIdo);
                    // CR3064 Lezárás dátuma alapján számol, ellenõrizni kell nem null-e
                    if (!String.IsNullOrEmpty(erec_UgyUgyirat.LezarasDat))
                    {
                        // BLG_2156
                        string idoegyseg = resIrattariTetelek.Ds.Tables[0].Rows[0]["Idoegyseg"].ToString();
                        //erec_UgyUgyirat.Typed.MegorzesiIdoVege = this.GetMegorzesiIdoVege(Convert.ToDateTime(erec_UgyUgyirat.LezarasDat), megorzesiIdo);
                        erec_UgyUgyirat.Typed.MegorzesiIdoVege = this.GetMegorzesiIdoVege(Convert.ToDateTime(erec_UgyUgyirat.LezarasDat), megorzesiIdo, idoegyseg);
                        erec_UgyUgyirat.Updated.MegorzesiIdoVege = true;
                    }
                }
            }
        }
        if (resIrattariTetelek.Ds.Tables[0].Rows[0]["MegorzesiIdo"].ToString() == "HN")
        {
            erec_UgyUgyirat.Typed.MegorzesiIdoVege = SqlDateTime.Null;
            erec_UgyUgyirat.Updated.MegorzesiIdoVege = true;
        }
        return resIrattariTetelek;
    }

    /// <summary>
    /// CERTOP
    /// ügyirat lezárást követõ év elsõ napjától kell számolni
    /// </summary>
    /// <param name="megorzesiIdoVege"></param>
    /// <returns></returns>
    // BLG_2156
    //private DateTime GetMegorzesiIdoVege(DateTime lezarasDatuma, int megorzesiIdoEv)
    private DateTime GetMegorzesiIdoVege(DateTime lezarasDatuma, int megorzesiIdo, string idoegyseg)
    {
        //CERTOP: dec 31
        DateTime megorzesiIdoVege = new DateTime(lezarasDatuma.Year, 12, 31);
        //FPH
        //DateTime megorzesiIdoVege = lezarasDatuma;
        // BLG_2156
        //megorzesiIdoVege = megorzesiIdoVege.AddYears(megorzesiIdoEv);
        megorzesiIdoVege = AddMegorzesiIdo(megorzesiIdo, idoegyseg, megorzesiIdoVege);

        return megorzesiIdoVege;
    }

    private static DateTime AddMegorzesiIdo(int megorzesiIdo, string idoegyseg, DateTime megorzesiIdoVege)
    {
        switch (idoegyseg)
        {
            case KodTarak.IDOEGYSEG.Nap:
                megorzesiIdoVege = megorzesiIdoVege.AddDays(Convert.ToInt32(megorzesiIdo));
                break;
            case KodTarak.IDOEGYSEG.Het:
                megorzesiIdoVege = megorzesiIdoVege.AddDays(Convert.ToInt32(megorzesiIdo) * 7);
                break;
            case KodTarak.IDOEGYSEG.Honap:
                megorzesiIdoVege = megorzesiIdoVege.AddMonths(Convert.ToInt32(megorzesiIdo));
                break;
            case KodTarak.IDOEGYSEG.Ev:
                megorzesiIdoVege = megorzesiIdoVege.AddYears(Convert.ToInt32(megorzesiIdo));
                break;
        }

        return megorzesiIdoVege;
    }


    /// <summary>
    /// Ügyirat központi irattárba adása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="IraIrattariTetel_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <param name="IrattarJellege"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasKozpontiIrattarba(ExecParam execParam, String erec_UgyUgyiratok_Id
        , String IraIrattariTetel_Id, String Megjegyzes, String Feljegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;

                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52204);
                    //log.WsEnd(execParam_UgyiratGet, result1);
                    //return result1;

                    throw new ResultException(52204);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atadasraKijelolheto = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Irattárba küldhetõ?
                if (Contentum.eRecord.BaseUtility.Ugyiratok.KozpontiIrattarbaKuldheto(
                    ugyiratStatusz, execParam_atadasraKijelolheto, out errorDetail) == false)
                {
                    // Nem küldhetõ irattárba:
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52202);
                    //log.WsEnd(execParam_atadasraKijelolheto, result1);
                    //return result1;

                    throw new ResultException(52202, errorDetail);
                }


                //Átadás az irattárnak

                //kezelési feljegyzés létrehozása, ha van megjegyzés
                var erec_HataridosFeladatok = CreateTaskNote(Feljegyzes);

                //átadás
                ExecParam xpmAtadas = execParam.Clone();
                Result resAtadas = this.Atadas(xpmAtadas, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
                resAtadas.CheckError();

                //állapot beállítása, irattári tétel
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;
                erec_UgyUgyirat.Base.Ver = (Int32.Parse(erec_UgyUgyirat.Base.Ver) + 1).ToString();

                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
                erec_UgyUgyirat.Updated.Allapot = true;

                erec_UgyUgyirat.TovabbitasAlattAllapot = String.Empty;
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;

                erec_UgyUgyirat.IraIrattariTetel_Id = IraIrattariTetel_Id;
                erec_UgyUgyirat.Updated.IraIrattariTetel_Id = true;

                SetNotes(erec_UgyUgyirat, Megjegyzes, Feljegyzes);

                ExecParam xpmUpdate = execParam.Clone();
                xpmUpdate.Record_Id = erec_UgyUgyiratok_Id;
                Result resUpdate = this.Update(xpmUpdate, erec_UgyUgyirat);
                resUpdate.CheckError();

                // ügyirat iratainak irattárba adás mezõjének töltése
#if IsUpdateAllIraIratIrattarbaAdasVetelDat
                Result iraIratUpdateResult = sp.UpdateAllIraIratIrattarbaAdasDat(execParam, new string[] { erec_UgyUgyirat.Id }, DateTime.Now);
                if (!string.IsNullOrEmpty(iraIratUpdateResult.ErrorCode))
                    throw new ResultException(iraIratUpdateResult);
#endif

                result = resUpdate;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratAtadasIrattarba").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private EREC_HataridosFeladatok CreateTaskNote(string megjegyzes)
    {
        if (megjegyzes != null && megjegyzes.Trim().Length > 0)
        {
            var erec_HataridosFeladatok = new EREC_HataridosFeladatok();
            erec_HataridosFeladatok.Updated.SetValueAll(false);
            erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

            erec_HataridosFeladatok.Altipus = KodTarak.FELADAT_ALTIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Altipus = true;

            erec_HataridosFeladatok.Leiras = megjegyzes;
            erec_HataridosFeladatok.Updated.Leiras = true;

            erec_HataridosFeladatok.Tipus = KodTarak.FELADAT_TIPUS.Megjegyzes;
            erec_HataridosFeladatok.Updated.Tipus = true;

            return erec_HataridosFeladatok;
        }

        return null;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasKozpontiIrattarba_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array,
        String Megjegyzes, String Feljegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52203);
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.In(erec_UgyUgyiratok_Id_Array);

            Result result_ugyiratGet = erec_UgyUgyiratokService.GetAll(execParam, search);
            result_ugyiratGet.CheckError();

            System.Collections.Hashtable statusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), result_ugyiratGet.Ds);
            if (statusz.Count == 0)
                throw new ResultException(52380);
            ErrorDetails errorDetail = null;

            List<string> ugyiratIdsList = new List<string>();
            List<string> ugyiratVersList = new List<string>();

            //LZS - BUG_7099
            //A sortörés előtti szöveg eltárolására szolgáló változó.
            string beforeTextFromNote = "";

            foreach (DataRow r in result_ugyiratGet.Ds.Tables[0].Rows)
            {
                string id = r["Id"].ToString();
                ugyiratIdsList.Add(id);
                string ver = r["Ver"].ToString();
                ugyiratVersList.Add(ver);

                //LZS - BUG_7099
                //Kiszedjük, és eltároljuk az EREC_UgyUgyiratok.Note mező értékét.
                if (!String.IsNullOrEmpty(beforeTextFromNote))
                {
                    beforeTextFromNote = r["Note"].ToString();
                }

                if (!Contentum.eRecord.BaseUtility.Ugyiratok.KozpontiIrattarbaKuldheto((statusz[id] as Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)
                    , execParam.Clone(), out errorDetail))
                    throw new ResultException(52202, errorDetail);
            }

            //Átadás az irattárnak

            //kezelési feljegyzés létrehozása, ha van megjegyzés
            var erec_HataridosFeladatok = CreateTaskNote(Feljegyzes);

            //átadás
            ExecParam xpmAtadas = execParam.Clone();

            AddEsemenyNaplo = false;
            Result resAtadas = this.Atadas_Tomeges(xpmAtadas, ugyiratIdsList.ToArray(), csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);
            AddEsemenyNaplo = true;

            resAtadas.CheckError();

            //állapot beállítása
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);

            erec_UgyUgyirat.Base.Note = beforeTextFromNote; // NOTE: a "Base.Updated.Note = true"-t a SetNotes állítja be, ha szükséges

            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
            erec_UgyUgyirat.Updated.Allapot = true;

            erec_UgyUgyirat.TovabbitasAlattAllapot = String.Empty;
            erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

            erec_UgyUgyirat.IrattarbaKuldDatuma = DateTime.Now.ToString();
            erec_UgyUgyirat.Updated.IrattarbaKuldDatuma = true;

            SetNotes(erec_UgyUgyirat, Megjegyzes, Feljegyzes);

            ExecParam xpmUpdate = execParam.Clone();

            string ugyiratIds = Search.GetSqlInnerString(ugyiratIdsList.ToArray());

            //Verzió növelése
            for (int i = 0; i < ugyiratVersList.Count; i++)
            {
                int ver;
                if (Int32.TryParse(ugyiratVersList[i], out ver))
                {
                    ugyiratVersList[i] = (ver + 1).ToString();
                }
            }

            string ugyiratVers = String.Join(",", ugyiratVersList.ToArray());

            Result resUpdate = this.Update_Tomeges(xpmUpdate, result_ugyiratGet.Ds.Tables[0], ugyiratIdsList.ToArray(), ugyiratVersList.ToArray(), erec_UgyUgyirat, DateTime.Now);
            resUpdate.CheckError();

            result = resUpdate;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ugyiratIds, "UgyiratAtadasIrattarba");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtadasTomeges: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat irattárba adási kérelmének visszautasítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="IraIrattariTetel_Id"></param>
    /// <param name="Megjegyzes"></param>
    /// <param name="IrattarJellege"></param>
    /// <param name="csoport_Id_Felelos_Kovetkezo"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtadasIrattarbaVisszautasitasa(ExecParam execParam, String erec_UgyUgyiratok_Id
        , String IraIrattariTetel_Id, String Megjegyzes, String IrattarJellege, String csoport_Id_Felelos_Kovetkezo)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(IrattarJellege)
            )
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52290);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;

                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52204);
                    //log.WsEnd(execParam_UgyiratGet, result1);
                    //return result1;

                    throw new ResultException(52204);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_atadasraKijelolheto = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Irattárba küldhetõ?
                if (Contentum.eRecord.BaseUtility.Ugyiratok.IrattarbaKuldesVisszautasithato(
                    ugyiratStatusz, execParam_atadasraKijelolheto, out errorDetail) == false)
                {
                    // Nem utasítható vissza a küldés

                    throw new ResultException(52207, errorDetail);
                }


                //Átadás az elõzõ felelõsnek


                ////kezelési feljegyzés létrehozása, ha van megjegyzés
                //EREC_HataridosFeladatok erec_HataridosFeladatok = null;
                //if (!String.IsNullOrEmpty(Megjegyzes.Trim()))
                //{
                //    erec_HataridosFeladatok = new EREC_HataridosFeladatok();
                //    erec_HataridosFeladatok.Updated.SetValueAll(false);
                //    erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

                //    erec_HataridosFeladatok.KezelesTipus = KodTarak.KezelesiFeljegyzes.Megjegyzes;
                //    erec_HataridosFeladatok.Updated.KezelesTipus = true;

                //    erec_HataridosFeladatok.Leiras = Megjegyzes;
                //    erec_HataridosFeladatok.Updated.Leiras = true;
                //}

                // Kezelési feljegyzés létrehozása, ha van megjegyzés:
                if (!String.IsNullOrEmpty(Megjegyzes.Trim()))
                {
                    Logger.Debug("!String.IsNullOrEmpty(Megjegyzes.Trim())");
                    Logger.Debug("Megjegyzes.Trim():" + Megjegyzes.Trim());
                    this.AddKezelesiFeljegyzes(execParam, erec_UgyUgyirat.Id, Megjegyzes.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
                }

                // már nem történik átadás a vezetõ felé, ezért nem is kell visszaadni
                ////átadás
                //ExecParam xpmAtadas = execParam.Clone();
                //Result resAtadas = this.Atadas(xpmAtadas, erec_UgyUgyiratok_Id, erec_UgyUgyirat.Csoport_Id_Felelos_Elozo, erec_HataridosFeladatok);

                //if (!String.IsNullOrEmpty(resAtadas.ErrorCode))
                //{
                //    throw new ResultException(resAtadas);
                //}
                //állapot beállítása(lezárt), irattári tétel
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;
                //erec_UgyUgyirat.Base.Ver = (Int32.Parse(erec_UgyUgyirat.Base.Ver) + 1).ToString();

                //erec_UgyUgyirat.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                //erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                // csak biztonság kedvéért töröljük
                erec_UgyUgyirat.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;

                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                erec_UgyUgyirat.Updated.Allapot = true;

                //Következõ felelõs törlése
                erec_UgyUgyirat.Kovetkezo_Felelos_Id = Contentum.eUtility.Constants.BusinessDocument.nullString;
                erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                //Címzett törlése
                erec_UgyUgyirat.Csoport_Id_Cimzett = Contentum.eUtility.Constants.BusinessDocument.nullString;
                erec_UgyUgyirat.Updated.Csoport_Id_Cimzett = true;

                erec_UgyUgyirat.IraIrattariTetel_Id = IraIrattariTetel_Id;
                erec_UgyUgyirat.Updated.IraIrattariTetel_Id = true;

                ExecParam xpmUpdate = execParam.Clone();
                xpmUpdate.Record_Id = erec_UgyUgyiratok_Id;
                Result resUpdate = this.Update(xpmUpdate, erec_UgyUgyirat);

                if (!String.IsNullOrEmpty(resUpdate.ErrorCode))
                {
                    throw new ResultException(resUpdate);
                }


                result = resUpdate;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id
                    , erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "IrattarbaAdasVisszautasitasa").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat Irattarba vetele
    /// NEM HASZNÁLT, HELYETTE AZ AtvetelIrattarba_Tomeges HASZNÁLANDÓ
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza, Record_Id a amit atvesz</param>
    /// <returns></returns>
    [WebMethod()]
    public Result AtvetelIrattarba(ExecParam execParam)
    {
        return AtvetelIrattarba_Tomeges(execParam, execParam.Record_Id);
    }

    /// <summary>
    /// Ügyiratok tömeges irattárba vétele
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza, Record_Id a amit atvesz</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result AtvetelIrattarba_Tomeges(ExecParam execParam, string ugyiratIds)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ugyiratIds))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        result.Ds = new DataSet();
        result.Ds.Tables.Add();
        result.Ds.Tables[0].Columns.Add("Id");
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            DateTime executionTime = DateTime.Now;

            // Ügyiratok lekérése:
            List<string> selectedIds = new List<string>(ugyiratIds.Split(','));
            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();

            search.Id.In(selectedIds);

            Result getAllResult = this.GetAll(execParam.Clone(), search);

            if (!String.IsNullOrEmpty(getAllResult.ErrorCode))
            {
                throw new ResultException(getAllResult);
            }

            if (getAllResult.GetCount == 0)
            {
                throw new ResultException(52231);
            }

            #region irattári kikérõ vizsgálása
            // irattári kikérõ lekérése
            // (ha egyéb irattári kikérõ van, akkor irattárba visszaküldés van => kikérõt módosítani kell)
            List<string> kikeroIdsUpdatelniKell = new List<string>();
            List<string> kikeroVersUpdatelniKell = new List<string>();

            #region Irattári kikérõ(k) lekérdezése

            ExecParam kikeroExecParam = execParam.Clone();

            EREC_IrattariKikeroService kikeroService = new EREC_IrattariKikeroService(this.dataContext);
            Result kikeroResult = kikeroService.GetAllErvenyesKikeroByUgyiratIds(kikeroExecParam, ugyiratIds.Split(','));

            if (kikeroResult.IsError)
            {
                throw new ResultException(kikeroResult);
            }

            #endregion

            foreach (DataRow row_kikero in kikeroResult.Ds.Tables[0].Rows)
            {
                string kikeroAllapot = row_kikero["Allapot"].ToString();
                string kikeroId = row_kikero["Id"].ToString();
                string kikeroVer = row_kikero["Ver"].ToString();

                if (kikeroAllapot != KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto)
                {
                    kikeroIdsUpdatelniKell.Add(kikeroId);
                    kikeroVersUpdatelniKell.Add(kikeroVer);
                }

            }


            if (kikeroIdsUpdatelniKell.Count > 0)
            {
                EREC_IrattariKikero kikeroUpdaterRecord = new EREC_IrattariKikero();
                kikeroUpdaterRecord.Base.Updated.SetValueAll(false);
                kikeroUpdaterRecord.Updated.SetValueAll(false);

                // jovahagyott allapot beallitasa:
                kikeroUpdaterRecord.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott;
                kikeroUpdaterRecord.Updated.Allapot = true;

                kikeroUpdaterRecord.FelhasznaloCsoport_Id_Visszave = execParam.Felhasznalo_Id;
                kikeroUpdaterRecord.Updated.FelhasznaloCsoport_Id_Visszave = true;

                Result kikeroUpdateResult = kikeroService.Update_Tomeges(execParam, kikeroIdsUpdatelniKell.ToArray(), kikeroVersUpdatelniKell.ToArray(), kikeroUpdaterRecord);
                if (!String.IsNullOrEmpty(kikeroUpdateResult.ErrorCode))
                {
                    throw new ResultException(kikeroUpdateResult);
                }

            }

            #endregion

            System.Collections.Hashtable statuszok = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataSet(execParam.Clone(), getAllResult.Ds);

            List<string> selectedVers = new List<string>();
            string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam.Clone()).Obj_Id.ToUpper();

            selectedIds.Clear();
            selectedVers.Clear();

            ErrorDetails errorDetail = null;

            List<string> kozpontiIrattarbaVeendoUgyiratok = new List<string>();

            foreach (DataRow r in getAllResult.Ds.Tables[0].Rows)
            {
                string ugyiratId = r["Id"].ToString();
                string ugyiratVer = r["Ver"].ToString();
                if (String.Compare(r["Csoport_Id_Felelos"].ToString(), kozpontiIrattarId, true) == 0)
                {

                    if (!Contentum.eRecord.BaseUtility.Ugyiratok.KozpontiIrattarbaVeheto(
                        (Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)statuszok[ugyiratId], execParam.Clone(), out errorDetail)
                       )
                    {
                        result.Ds.Tables[0].Rows.Add(ugyiratId);
                        throw new ResultException(52205, errorDetail);
                    }

                    kozpontiIrattarbaVeendoUgyiratok.Add(ugyiratId);
                }
                else
                {
                    if (!Contentum.eRecord.BaseUtility.Ugyiratok.AtmenetiIrattarbaVeheto(
                        (Contentum.eRecord.BaseUtility.Ugyiratok.Statusz)statuszok[ugyiratId], execParam.Clone(), out errorDetail))
                    {
                        result.Ds.Tables[0].Rows.Add(ugyiratId);
                        throw new ResultException(52206, errorDetail);
                    }

                }


                /// Állapot beállítása:
                /// Ha Továbbítás alatt állapotban van, akkor visszaállítjuk a továbbítás elõtti állapotára,
                /// egyébként marad ugyanott
                if (r["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                {
                    if (!String.IsNullOrEmpty(r["TovabbitasAlattAllapot"].ToString()) && (r["TovabbitasAlattAllapot"].ToString() != KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa && r["TovabbitasAlattAllapot"].ToString() != KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart) && r["Csoport_Id_Felelos"].ToString().ToUpper() != kozpontiIrattarId)
                    {
                        result.Ds.Tables[0].Rows.Add(ugyiratId);
                        throw new ResultException(52206);
                    }
                }
                else
                {
                    result.Ds.Tables[0].Rows.Add(ugyiratId);
                    throw new ResultException(52233);
                }

                selectedIds.Add(ugyiratId);
                selectedVers.Add(ugyiratVer);

            }

            //ügyirat iratainak irattárba vétel mezõjének töltése (csak központi irattár esetén kell tölteni)
#if IsUpdateAllIraIratIrattarbaAdasVetelDat
            if (kozpontiIrattarbaVeendoUgyiratok.Count > 0)
            {
                Result iraIratUpdateResult = sp.UpdateAllIraIratIrattarbaVetelDat(execParam.Clone(), kozpontiIrattarbaVeendoUgyiratok.ToArray(), executionTime);
                if (!string.IsNullOrEmpty(iraIratUpdateResult.ErrorCode))
                {
                    throw new ResultException(iraIratUpdateResult);
                }
            }
#endif

            string FelhCsoport_Id_IrattariAtvevo = Csoportok.GetFelhasznaloSajatCsoportId(execParam);
            Result resultIrratarbaVetel = AtvetelIrattarbaTomegesInternal(execParam.Clone(), selectedIds.ToArray(), selectedVers.ToArray(), FelhCsoport_Id_IrattariAtvevo, executionTime);
            if (resultIrratarbaVetel.IsError)
            {
                throw new ResultException(resultIrratarbaVetel);
            }

            // Set selejtezesi ido
            foreach (string id in selectedIds)
            {
                ExecParam execParam_UgyiratGet = execParam.Clone();
                execParam_UgyiratGet.Record_Id = id;

                Result result_ugyiratGet = Get(execParam_UgyiratGet);
                if (result_ugyiratGet.IsError)
                {
                    // hiba:
                    Logger.Error("Ugyirat lekerese hiba", execParam_UgyiratGet, result_ugyiratGet);
                    throw new ResultException(result_ugyiratGet);
                }

                EREC_UgyUgyiratok ugyirat = result_ugyiratGet.Record as EREC_UgyUgyiratok;
                ugyirat.Updated.SetValueAll(false);
                ugyirat.Base.Updated.SetValueAll(false);
                ugyirat.Base.Updated.Ver = true;

                SetMegorzesiIdoFromIrattariTetel(execParam_UgyiratGet, ugyirat, ugyirat.IraIrattariTetel_Id);

                Result result_ugyiratUpdate = this.Update(execParam_UgyiratGet, ugyirat);

                if (result_ugyiratUpdate.IsError)
                {
                    // hiba:
                    Logger.Error("Ugyirat update hiba", execParam_UgyiratGet, result_ugyiratUpdate);
                    throw new ResultException(result_ugyiratUpdate);
                }
            }

            result = resultIrratarbaVetel;

            //Kézbesítési tétel állapotának állítása
            EREC_IraKezbesitesiTetelekService svcKezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);

            string szumIds = Search.GetSqlInnerString(selectedIds.ToArray());

            Result resIraKezbesitesiTetelek = svcKezbesitesiTetelek.SetKezbesitesiTetelekToAtvettTomeges(execParam.Clone(), szumIds);

            if (!String.IsNullOrEmpty(resIraKezbesitesiTetelek.ErrorCode))
            {
                throw new ResultException(resIraKezbesitesiTetelek);
            }

            #region migrált szerelt fõszámok
            EREC_UgyUgyiratok erec_UgyUgyirat = new EREC_UgyUgyiratok();
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott;
            erec_UgyUgyirat.Updated.Allapot = true;

            List<string> irattarbolElkertUgyiratok = new List<string>();
            foreach (DataRow row_kikero in kikeroResult.Ds.Tables[0].Rows)
            {
                string kikeroAllapot = row_kikero["Allapot"].ToString();
                string ugyiratId = row_kikero["UgyUgyirat_Id"].ToString();

                if (!irattarbolElkertUgyiratok.Contains(ugyiratId) && kikeroAllapot == KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto)
                {
                    irattarbolElkertUgyiratok.Add(ugyiratId);
                }
            }

            DataTable ugyiratTable = getAllResult.Ds.Tables[0];
            DataTable irattarbolElkertUgyiratTable = ugyiratTable.Clone();
            for (int i = ugyiratTable.Rows.Count - 1; i >= 0; i--)
            {
                DataRow rowUgyirat = ugyiratTable.Rows[i];
                string ugyiratId = rowUgyirat["Id"].ToString();
                if (irattarbolElkertUgyiratok.Contains(ugyiratId))
                {
                    irattarbolElkertUgyiratTable.ImportRow(rowUgyirat);
                    ugyiratTable.Rows.Remove(rowUgyirat);
                }
            }

            TomegesUgyiratUpdateExecuted(execParam.Clone(), ugyiratTable, erec_UgyUgyirat);
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert;
            TomegesUgyiratUpdateExecuted(execParam.Clone(), irattarbolElkertUgyiratTable, erec_UgyUgyirat);
            #endregion

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                string ids = Search.GetSqlInnerString(selectedIds.ToArray());

                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "IrattarbaUgyiratAtvetel");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]EREC_UgyUgyiratok::IrattarbaUgyiratAtvetel: ", execParam, eventLogResult);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);

        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat átvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atvetel(ExecParam execParam, String erec_UgyUgyiratok_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // A tömeges átvétel fv. meghívva:
            string[] ugyiratIds = new string[1];
            ugyiratIds[0] = erec_UgyUgyiratok_Id;

            result = this.Atvetel_Tomeges(execParam, ugyiratIds, execParam.Felhasznalo_Id);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                throw new ResultException(result);
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyiratok tömeges átvétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratok_Id_Array"></param>
    /// <param name="kovetkezoOrzoId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atvetel_Tomeges(ExecParam execParam, string[] ugyiratok_Id_Array, string kovetkezoOrzoId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_IraKezbesitesiTetelekService kezbTetelekService = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            EREC_IraKezbesitesiTetelekSearch kezbTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

            kezbTetelekSearch.Obj_Id.In(ugyiratok_Id_Array);

            kezbTetelekSearch.Allapot.In(new string[] { KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott });

            kezbTetelekSearch.Csoport_Id_Cel.In(new string[] { execParam.Felhasznalo_Id, execParam.FelhasznaloSzervezet_Id });

            Result kezbTetelekGetAllResult = kezbTetelekService.GetAll(execParam.Clone(), kezbTetelekSearch);
            if (!string.IsNullOrEmpty(kezbTetelekGetAllResult.ErrorCode))
                throw new ResultException(kezbTetelekGetAllResult);

            string[] kezbTetelIds = new string[kezbTetelekGetAllResult.GetCount];
            // ha a kézbesítési tételek száma nem egyezik az átveendõ küldemények számával -> hiba (kézbesítési tétel lekérése sikertelen)
            if (kezbTetelIds.Length != ugyiratok_Id_Array.Length)
                throw new ResultException(52224);

            for (int i = 0; i < kezbTetelIds.Length; i++)
            {
                kezbTetelIds[i] = kezbTetelekGetAllResult.Ds.Tables[0].Rows[i]["Id"].ToString();
            }


            result = kezbTetelekService.Atvetel_Tomeges(execParam.Clone(), kezbTetelIds);

            #region Eseménynaplózás
            //naplózás a kezbTetelekService.Atvetel_Tomeges által visszahívott másik, nem WebMethod AtvetelTomeges metódusban
            //if (isTransactionBeginHere)
            //{
            //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, ids, "UgyiratAtvetel");
            //if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //    Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtvetelTomeges: ", execParam, eventLogResult);
            //}
            #endregion

            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public Result AtvetelTomegesInternal(ExecParam execParam, string[] Ids, string[] Vers, string KovetkezoOrzoId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Atvetel_Tomeges(execParam, Ids, Vers, KovetkezoOrzoId, null, null);

            #region Eseménynaplózás
            //if (isTransactionBeginHere)
            //{
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            string objIds = Search.GetSqlInnerString(Ids);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, objIds, "UgyiratAtvetel");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]EREC_UgyUgyiratok::AtvetelTomeges: ", execParam, eventLogResult);
            //}
            #endregion

            #region ucm
            SetUCMJogosultsag(execParam, Ids);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    #region Visszaküldés
    /// <summary>
    /// Ügyirat átvételének visszautasítása, ügyirat visszaküldése
    /// A kézbesítési tételt a hívó szervíznek kell módosítania a sikeres futás után!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraKezbesitesiTetelek"></param>
    /// <returns></returns>
    public Result Visszakuldes(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_Id) || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_type))
        {
            // hiba
            // Hiba a visszaküldés során: hiányzó paraméter(ek)!
            Result result1 = ResultError.CreateNewResultWithErrorCode(53700);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Kézbesítési tétel ellenõrzés
            Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldheto(execParam, erec_IraKezbesitesiTetelek);
            if (result_kezbesitesiTetelCheck.IsError)
            {
                throw new ResultException(result_kezbesitesiTetelCheck);
            }

            if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok)
            {
                Logger.Error(String.Format("Kézbesítési tételbõl kapott objektum típus: {0}; Várt objektum típus: {1}"
                    , erec_IraKezbesitesiTetelek.Obj_type
                    , Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok));

                // Hiba a visszaküldés során: a kézbesítési tétel objektum típusa nem megfelelõ!
                throw new ResultException(53707);
            }
            #endregion Kézbesítési tétel ellenõrzés

            string ugyirat_Id = erec_IraKezbesitesiTetelek.Obj_Id;
            string visszakuldesIndoka = erec_IraKezbesitesiTetelek.Base.Note;

            #region Ügyirat objektum lekérése
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = ugyirat_Id;

            Result result_UgyiratGet = this.Get(execParam_UgyiratGet);
            if (result_UgyiratGet.IsError)
            {
                // hiba
                throw new ResultException(result_UgyiratGet);
            }

            EREC_UgyUgyiratok erec_UgyUgyiratok_regi = (EREC_UgyUgyiratok)result_UgyiratGet.Record;

            #endregion Ügyirat objektum lekérése

            #region Ügyirat UPDATE
            EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();

            erec_UgyUgyiratok.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Ver = erec_UgyUgyiratok_regi.Base.Ver;
            erec_UgyUgyiratok.Base.Updated.Ver = true;

            // Visszaküldés: a következõ felelõs az elõzõ felelõs (és még jelenlegi õrzõ) lesz
            string felelos_Id = erec_UgyUgyiratok_regi.Csoport_Id_Felelos;

            if (!String.IsNullOrEmpty(erec_UgyUgyiratok_regi.Csoport_Id_Felelos_Elozo))
            {
                // Felelõs visszaállítása az elõzõ felelõsre:
                erec_UgyUgyiratok.Csoport_Id_Felelos = erec_UgyUgyiratok_regi.Csoport_Id_Felelos_Elozo;
                erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_UgyUgyiratok.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_UgyUgyiratok.Updated.Csoport_Id_Felelos_Elozo = true;
            }
            else
            {
                erec_UgyUgyiratok.Csoport_Id_Felelos = erec_UgyUgyiratok_regi.FelhasznaloCsoport_Id_Orzo;
                erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

                // Elõzõ felelõs állítása a mostanira:
                erec_UgyUgyiratok.Csoport_Id_Felelos_Elozo = felelos_Id;
                erec_UgyUgyiratok.Updated.Csoport_Id_Felelos_Elozo = true;
            }

            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = ugyirat_Id;
            // a StoredProcedure szintet hívjuk, hogy ne keletkezzen új kézbesítési tétel
            result = sp.Insert(Constants.Update, execParam_ugyiratUpdate, erec_UgyUgyiratok);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #endregion Ügyirat UPDATE

            #region Kezelési feljegyzés
            // Kezelési feljegyzés létrehozása, ha van megjegyzés:
            if (!String.IsNullOrEmpty(visszakuldesIndoka.Trim()))
            {
                Logger.Debug("Visszakuldes indoka:" + visszakuldesIndoka.Trim());
                this.AddKezelesiFeljegyzes(execParam, ugyirat_Id, visszakuldesIndoka.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }
            #endregion Kezelési feljegyzés

            #region Eseménynaplózás
            // naplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyirat_Id, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, "UgyiratVisszakuldes").Record;
                if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
                {
                    eventLogRecord.Base.Note = visszakuldesIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            #region Szerelt ügyiratok updat-elése
            ExecParam execParam_szereltek = execParam.Clone();
            execParam_szereltek.Record_Id = ugyirat_Id;
            Result resSzereltUpdate = UpdateSzereltUgyiratok(execParam_szereltek, erec_UgyUgyiratok_regi, erec_UgyUgyiratok);

            if (!String.IsNullOrEmpty(resSzereltUpdate.ErrorCode))
            {
                throw new ResultException(resSzereltUpdate);
            }

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }
    #endregion Visszaküldés

    #region Visszaküldés irattárból
    /// <summary>
    /// Ügyirat irattárba átvételének visszautasítása, ügyirat visszaküldése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyirat_Id"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result VisszakuldesIrattarbol(ExecParam execParam, string ugyirat_Id, string visszakuldesIndoka)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(ugyirat_Id))
        {
            // hiba
            // Hiba az irattárból visszaküldés során: hiányzó paraméter(ek)!
            Result result1 = ResultError.CreateNewResultWithErrorCode(53750);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Kézbesítési tétel lekérése
            EREC_IraKezbesitesiTetelekService service_kezbTetel = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbTetelGet = execParam.Clone();

            Result result_kezbTetelGet = service_kezbTetel.GetKezbesitesitetelByObject(execParam_kezbTetelGet
                , ugyirat_Id, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok);

            if (result_kezbTetelGet.IsError)
            {
                throw new ResultException(result_kezbTetelGet);
            }

            EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek = (EREC_IraKezbesitesiTetelek)result_kezbTetelGet.Record;

            #endregion Kézbesítési tétel lekérése

            #region Kézbesítési tétel ellenõrzés
            Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldhetoIrattarbol(execParam, erec_IraKezbesitesiTetelek);
            if (result_kezbesitesiTetelCheck.IsError)
            {
                throw new ResultException(result_kezbesitesiTetelCheck);
            }

            if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok)
            {
                Logger.Error(String.Format("Kézbesítési tételbõl kapott objektum típus: {0}; Várt objektum típus: {1}"
                    , erec_IraKezbesitesiTetelek.Obj_type
                    , Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok));

                // Hiba az irattárból visszaküldés során: nem megfelelõ típusú iratkezelési objektum (nem ügyirat)!
                throw new ResultException(53757);
            }
            #endregion Kézbesítési tétel ellenõrzés

            #region Ügyirat objektum lekérése
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = ugyirat_Id;

            Result result_UgyiratGet = this.Get(execParam_UgyiratGet);
            if (result_UgyiratGet.IsError)
            {
                // hiba
                throw new ResultException(result_UgyiratGet);
            }

            EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result_UgyiratGet.Record;

            #endregion Ügyirat objektum lekérése

            #region Ügyirat ellenõrzése
            bool sendBackFromKozpontiIrattarToAtmenetiIrattar = false;
            string atmenetiIrattar_Id = null;

            // Cél irattár:
            // Központi irattár
            if (erec_UgyUgyiratok.Csoport_Id_Felelos == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id)
            {
                // Küldõ: átmeneti irattár?
                KRT_PartnerKapcsolatokService service_partnerkapcsolatok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                KRT_PartnerKapcsolatokSearch search_partnerkapcsolatok = new KRT_PartnerKapcsolatokSearch();
                search_partnerkapcsolatok.Tipus.Filter(KodTarak.PartnerKapcsolatTipus.irattara);
                search_partnerkapcsolatok.Partner_id_kapcsolt.Filter(erec_UgyUgyiratok.Csoport_Id_Felelos_Elozo);

                // elég, ha 1 van
                search_partnerkapcsolatok.TopRow = 1;

                Result result_allirattar = service_partnerkapcsolatok.GetAll(execParam.Clone(), search_partnerkapcsolatok);

                if (result_allirattar.IsError)
                {
                    throw new ResultException(result_allirattar);
                }

                Logger.Debug("Eredmény: " + result_allirattar.GetCount);
                if (result_allirattar.GetCount > 0)
                {
                    sendBackFromKozpontiIrattarToAtmenetiIrattar = true;
                    atmenetiIrattar_Id = erec_UgyUgyiratok.Csoport_Id_Felelos_Elozo;
                }

            }
            else
            {
                // Átmeneti irattár
                KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                Result result_irattarak = service_csoportok.GetAllKezelhetoIrattar(execParam);

                if (result_irattarak.IsError)
                {
                    throw new ResultException(result_irattarak);
                }

                DataRow[] rows = result_irattarak.Ds.Tables[0].Select(String.Format("Id='{0}'", erec_UgyUgyiratok.Csoport_Id_Felelos));

                if (rows.Length == 0)
                {
                    // Az ügyiratot nem küldheti vissza az irattárból, mert Ön nem jogosult az irattár kezelésére!
                    throw new ResultException(53756);
                }
            }

            #endregion Ügyirat ellenõrzése

            #region Ügyirat UPDATE
            erec_UgyUgyiratok.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Updated.Ver = true;

            // Visszaküldés:
            // - ha nem volt jóváhagyás, a következõ felelõs az elõzõ felelõs (és még jelenlegi õrzõ) lesz
            // - jóváhagyás esetén pedig a jóváhagyó
            // ellenkezõ esetben az ügyirat és a kézbesítés célja eltérõ lenne
            // (jóváhagyáskor a jóváhagyó lesz az átadó, de az ügyirat õrzõje nem változik)
            string felelos_Id = erec_UgyUgyiratok.Csoport_Id_Felelos;
            string visszakuldes_Felelos_Id = null;
            if (sendBackFromKozpontiIrattarToAtmenetiIrattar)
            {
                visszakuldes_Felelos_Id = erec_UgyUgyiratok.Csoport_Id_Felelos_Elozo;
            }
            else
            {
                visszakuldes_Felelos_Id = erec_IraKezbesitesiTetelek.Felhasznalo_Id_Atado_USER;
            }


            // Felelõs visszaállítása az elõzõ felelõsre:
            erec_UgyUgyiratok.Csoport_Id_Felelos = visszakuldes_Felelos_Id;
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;

            // Elõzõ felelõs állítása a mostanira:
            erec_UgyUgyiratok.Csoport_Id_Felelos_Elozo = felelos_Id;
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos_Elozo = true;

            // Az állapotot is módosítani kell
            if (sendBackFromKozpontiIrattarToAtmenetiIrattar)
            {
                // Átmeneti irattárból központiba
                // Irattárba küldött
                erec_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
                erec_UgyUgyiratok.Updated.Allapot = true;

                erec_UgyUgyiratok.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;
                erec_UgyUgyiratok.Updated.TovabbitasAlattAllapot = true;
            }
            else
            {
                // Továbbítás alatti, a továbbítás alatti állapotot pedig Lezárt
                erec_UgyUgyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt;
                erec_UgyUgyiratok.Updated.Allapot = true;

                erec_UgyUgyiratok.TovabbitasAlattAllapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                erec_UgyUgyiratok.Updated.TovabbitasAlattAllapot = true;
            }

            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = ugyirat_Id;
            // a StoredProcedure szintet hívjuk, hogy ne keletkezzen új kézbesítési tétel
            result = sp.Insert(Constants.Update, execParam_ugyiratUpdate, erec_UgyUgyiratok);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #endregion Ügyirat UPDATE

            #region Kézbesítési tétel UPDATE
            ExecParam execParam_kezbTetelUpdate = execParam.Clone();
            execParam_kezbTetelUpdate.Record_Id = erec_IraKezbesitesiTetelek.Id;

            Result result_kezbTetelUpdate = service_kezbTetel.VisszakuldesIrattarbolInternal(execParam_kezbTetelUpdate, erec_IraKezbesitesiTetelek, visszakuldesIndoka, atmenetiIrattar_Id);

            if (result_kezbTetelUpdate.IsError)
            {
                throw new ResultException(result_kezbTetelUpdate);
            }
            #endregion Kézbesítési tétel UPDATE

            #region Kezelési feljegyzés
            // Kezelési feljegyzés létrehozása, ha van megjegyzés:
            if (!String.IsNullOrEmpty(visszakuldesIndoka.Trim()))
            {
                Logger.Debug("Visszakuldes indoka:" + visszakuldesIndoka.Trim());
                this.AddKezelesiFeljegyzes(execParam, ugyirat_Id, visszakuldesIndoka.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }
            #endregion Kezelési feljegyzés

            #region Eseménynaplózás
            // naplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyirat_Id, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, "UgyiratVisszakuldes").Record;
                if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
                {
                    eventLogRecord.Base.Note = visszakuldesIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }
    #endregion Visszaküldés irattárból

    public Result AtvetelIrattarbaTomegesInternal(ExecParam execParam, string[] Ids, string[] Vers, string FelhCsoport_Id_IrattariAtvevo, DateTime IrattarbaVetelDat)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Atvetel_Tomeges(execParam, Ids, Vers, null, FelhCsoport_Id_IrattariAtvevo, IrattarbaVetelDat);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Átadásra kijelölés visszavonása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result AtadasraKijelolesSztorno(ExecParam execParam, String erec_UgyUgyiratok_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52350);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;
            string elozoFelelosId = "";

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                //log.WsEnd(execParam_UgyiratGet, result_ugyiratGet);
                //return result_ugyiratGet;

                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52351);
                    //log.WsEnd(execParam_UgyiratGet, result1);
                    //return result1;

                    throw new ResultException(52351);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                elozoFelelosId = erec_UgyUgyirat.Csoport_Id_Felelos;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_visszavonhato = execParam.Clone();

                if (Contentum.eRecord.BaseUtility.Ugyiratok.AtadasraKijelolesVisszavonhato(ugyiratStatusz, execParam_visszavonhato) == false)
                {
                    // Nem vonható vissza
                    //Result result1 = ResultError.CreateNewResultWithErrorCode(52352);
                    //log.WsEnd(execParam_visszavonhato, result1);
                    //return result1;

                    throw new ResultException(52352);
                }


                #region Ha Irattárból kiadás lett sztornózva, akkor az irattári kikérõt is vissza kell állítani

                if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                    && (erec_UgyUgyirat.TovabbitasAlattAllapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
                        || erec_UgyUgyirat.TovabbitasAlattAllapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                        ))
                {
                    Logger.Debug("Irattári kikérõ lekérdezése", execParam);

                    EREC_IrattariKikeroService service_irattariKikero = new EREC_IrattariKikeroService(this.dataContext);

                    ExecParam execParam_irattariKikero = execParam.Clone();
                    execParam_irattariKikero.Record_Id = erec_UgyUgyirat.Id;

                    Result result_irattariKikeroGet = service_irattariKikero.GetByUgyiratId(execParam_irattariKikero);

                    if (result_irattariKikeroGet.IsError)
                    {
                        // hiba
                        throw new ResultException(result_irattariKikeroGet);
                    }

                    EREC_IrattariKikero irattariKikero = (EREC_IrattariKikero)result_irattariKikeroGet.Record;
                    if (irattariKikero == null)
                    {
                        // hiba:
                        // Hiba az átadásra kijelölés visszavonása során: Irattári kikérõ lekérése az adatbázisból sikertelen!
                        throw new ResultException(52353);
                    }

                    // Ellenõrzés: csak az irattárból kiadó felhasználó sztornózhatja:
                    if (irattariKikero.FelhasznaloCsoport_Id_Kiado != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                    {
                        // hiba:
                        // Az irattárból kiadás mûvelet nem vonható vissza, mert azt nem Ön hajtotta végre!
                        throw new ResultException(52354);
                    }

                    #region Irattári kikérõ update
                    irattariKikero.Updated.SetValueAll(false);
                    irattariKikero.Base.Updated.SetValueAll(false);
                    irattariKikero.Base.Updated.Ver = true;

                    // Ha kölcsönzés kiadást kell visszavonni, akkor az irattári kikérõt engedélyezettbe tesszük, amúgy engedélyezhetõbe
                    if (erec_UgyUgyirat.TovabbitasAlattAllapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
                    {
                        irattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett;
                    }
                    else
                    {
                        irattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto;
                    }
                    irattariKikero.Updated.Allapot = true;

                    irattariKikero.KiadasDatuma = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    irattariKikero.Updated.KiadasDatuma = true;

                    irattariKikero.FelhasznaloCsoport_Id_Kiado = Contentum.eUtility.Constants.BusinessDocument.nullString;
                    irattariKikero.Updated.FelhasznaloCsoport_Id_Kiado = true;

                    ExecParam execParam_irattariKikeroUpdate = execParam.Clone();
                    execParam_irattariKikeroUpdate.Record_Id = irattariKikero.Id;

                    Result result_irattariKikeroUpdate = service_irattariKikero.Update(execParam_irattariKikeroUpdate, irattariKikero);
                    if (result_irattariKikeroUpdate.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_irattariKikeroUpdate);
                    }

                    #endregion

                }

                #endregion


                // ÜGYIRAT UPDATE:
                // mezõk módosíthatóságának letiltása:
                erec_UgyUgyirat.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat.Base.Updated.Ver = true;

                // Módosítani kell: Felelõs, Állapot (Továbbításalattiállapot)

                string felelos_Id = erec_UgyUgyirat.Csoport_Id_Felelos;

                if (!String.IsNullOrEmpty(erec_UgyUgyirat.Csoport_Id_Felelos_Elozo))
                {
                    // Felelõs visszaállítása az elõzõ felelõsre:
                    erec_UgyUgyirat.Csoport_Id_Felelos = erec_UgyUgyirat.Csoport_Id_Felelos_Elozo;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                    // Elõzõ felelõs állítása a mostanira:
                    erec_UgyUgyirat.Csoport_Id_Felelos_Elozo = felelos_Id;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos_Elozo = true;
                }

                if (bSzignalasCausesTovabbitasAlattAllapot == false)
                {
                    // Állapot állítása: (ha továbbítás alattiban volt, akkor vissza az azelõttire,
                    // ha szignáltban volt, akkor vissza Iktatott-ra)
                    if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                        && !String.IsNullOrEmpty(erec_UgyUgyirat.TovabbitasAlattAllapot))
                    {
                        erec_UgyUgyirat.Allapot = erec_UgyUgyirat.TovabbitasAlattAllapot;
                        erec_UgyUgyirat.Updated.Allapot = true;

                        erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                    }
                    else if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Szignalt)
                    {
                        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Iktatott;
                        erec_UgyUgyirat.Updated.Allapot = true;
                    }
                }
                else
                {
                    // CR#2171 szerint módosítva: (a szignálás után automatikus átadás, nem csak a felelõs kitöltése,
                    // és nem szignált, hanem Továbbítás alatt (Szignált) állapotba kerül)
                    // Állapot állítása: (ha továbbítás alattiban volt, akkor vissza az azelõttire,
                    // kivéve, ha szignáltban volt, akkor vissza Iktatott-ra)
                    if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                        && !String.IsNullOrEmpty(erec_UgyUgyirat.TovabbitasAlattAllapot))
                    {
                        if (erec_UgyUgyirat.TovabbitasAlattAllapot == KodTarak.UGYIRAT_ALLAPOT.Szignalt)
                        {
                            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Iktatott;
                            erec_UgyUgyirat.Updated.Allapot = true;
                        }
                        else
                        {
                            erec_UgyUgyirat.Allapot = erec_UgyUgyirat.TovabbitasAlattAllapot;
                            erec_UgyUgyirat.Updated.Allapot = true;
                        }
                        erec_UgyUgyirat.TovabbitasAlattAllapot = "";
                        erec_UgyUgyirat.Updated.TovabbitasAlattAllapot = true;
                    }
                }

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                // Közvetlenül sp objektumnak hívjuk az update-jét, hogy ne csináljon megint kézbesítési tételt
                // a felelõs megváltozása miatt:
                Result result_UgyiratUpdate = sp.Insert(Constants.Update, execParam_ugyiratUpdate, erec_UgyUgyirat);
                if (result_UgyiratUpdate.IsError)
                {
                    // hiba:                    

                    throw new ResultException(result_UgyiratUpdate);
                }

                /// Kézbesítési tételek vizsgálata:
                /// ha volt a rekordra kézbesítési tétel (átadásra kijelölt), kézb. tétel érvénytelenítése:
                /// 

                EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                ExecParam execParam_kezbTetel = execParam.Clone();

                Result result_kezbTetelUpdate =
                    service_KezbesitesiTetelek.AtadasraKijeloltekInvalidate(execParam_kezbTetel, erec_UgyUgyirat.Id);

                if (result_kezbTetelUpdate.IsError)
                {
                    // hiba:
                    throw new ResultException(result_kezbTetelUpdate);
                }


                #region Elsõdleges iratpéldányok visszaállítása

                Result result_iratPeldanyokVissza = sp.UgyiratAtadasSztorno_ElsodlegesIratpeldanyokVissza(execParam, erec_UgyUgyiratok_Id);
                if (result_iratPeldanyokVissza.IsError)
                {
                    // hiba:
                    throw new ResultException(result_iratPeldanyokVissza);
                }

                #endregion

                // ha minden OK:

                result = result_UgyiratUpdate;

            }

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratAtadasSztorno").Record;

            eventLogService.Insert(execParam, eventLogRecord);
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;

    }

    #region Elintézetté nyilvánítás
    /// <summary>
    /// Ügyirat elintézetté nyilvánítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <param name="elintezesDatuma"></param>
    /// <param name="elintezesMod"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result ElintezetteNyilvanitas(ExecParam execParam, String elintezesDatuma, String elintezesMod, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52240);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ügyirat lekérése:
            ExecParam execParam_UgyiratGet = execParam.Clone();

            Result result_ugyiratGet = this.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }

            if (result_ugyiratGet.Record == null)
            {
                // hiba
                throw new ResultException(52241);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
            ExecParam execParam_elintezetteNyilvanithato = execParam.Clone();
            ErrorDetails errorDetail = null;

            // Ellenõrzés, hogy elintézetté nyilvánítható-e az ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.ElintezetteNyilvanithato(
                ugyiratStatusz, execParam_elintezetteNyilvanithato, out errorDetail) == false)
            {
                // Nem nyilvánítható elintézetté az ügyirat:
                throw new ResultException(52242, errorDetail);
            }

            if (Rendszerparameterek.GetBoolean(execParam, "UGYIRAT_ELINTEZES_FELTETEL", false))
            {
                int errorCode;
                if (!ElintezetteNyilvanithatoExtended(execParam, erec_UgyUgyirat, out errorCode))
                {
                    // Nem nyilvánítható elintézetté az ügyirat:
                    throw new ResultException(errorCode);
                }
            }

            // Ügyirat UPDATE:
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            // ElintezesDat:
            try
            {
                DateTime dateTime_elintezes = DateTime.Parse(elintezesDatuma);

                erec_UgyUgyirat.ElintezesDat = dateTime_elintezes.ToString();
                erec_UgyUgyirat.Updated.ElintezesDat = true;
            }
            catch
            {
                // ha nem jó formátumú a dátum, beállítjuk a mostani idõre:
                erec_UgyUgyirat.ElintezesDat = DateTime.Now.ToString();
                erec_UgyUgyirat.Updated.ElintezesDat = true;
            }

            // ElintezesMod:
            erec_UgyUgyirat.ElintezesMod = elintezesMod;
            erec_UgyUgyirat.Updated.ElintezesMod = true;

            bool jovahagyasSzukseges = false;

            if (IsElintezetteNyilvanitasEnabled(execParam))
            {
                if (erec_UgyUgyirat.Jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus)
                {
                    // elektronikus ügyiratnál elintézetté nyilvánításkor automatikusan elektronikus irattárba kerül az ügyirat, ha IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL=1:
                    if (Contentum.eRecord.BaseUtility.Ugyiratok.IsIrattarozasElektronikusJovahagyasNelkul(execParam))
                    {
                        this.ElintezetteNyilvanitasEsElektronikusIrattarbaHelyezesMentessel(execParam, erec_UgyUgyirat, erec_HataridosFeladatok);
                    }
                    // elintézett lesz, de nem kerül az elektronikus irattárba:
                    else
                    {
                        //if () { jovahagyasSzukseges = true; } else { // ha az elektronikus ügyirat elintézéséhez is jóváhagyás szükséges
                        this.ElintezetteNyilvanitasMentessel(execParam, erec_UgyUgyirat, erec_HataridosFeladatok);
                        // }
                    }
                }
                else
                {
                    this.ElintezetteNyilvanitasMentessel(execParam, erec_UgyUgyirat, erec_HataridosFeladatok);
                }
            }
            else
            {
                jovahagyasSzukseges = true;
            }

            // jóváhagyásra küldés
            if (jovahagyasSzukseges)
            {
                erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa;
                erec_UgyUgyirat.Updated.Allapot = true;

                string jovahagyoId;
                if (NeedElintezesJovahagyas(execParam.Clone(), out jovahagyoId))
                {
                    //Jóváhagyó beállítása következõ felelõsnek
                    erec_UgyUgyirat.Kovetkezo_Felelos_Id = jovahagyoId;
                    erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

                    ExecParam execParam_UgyiratUpdate = execParam.Clone();
                    result = this.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);
                    if (result.IsError)
                        throw new ResultException(result);

                }
                else
                {
                    //Jóváhagyó beállítása következõ felelõsnek
                    erec_UgyUgyirat.Kovetkezo_Felelos_Id = execParam.Felhasznalo_Id;
                    erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;
                    this.ElintezetteNyilvanitasJovahagyasaInternal(execParam.Clone(), erec_UgyUgyirat, erec_HataridosFeladatok);
                }
            }

            this.AddEREC_HataridosFeladatok(execParam.Clone(), erec_UgyUgyirat, erec_HataridosFeladatok);


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "UgyiratElintezes").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public void AddEREC_HataridosFeladatok(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat, EREC_HataridosFeladatok erec_HataridosFeladat)
    {
        if (erec_HataridosFeladat != null)
        {
            EREC_HataridosFeladatokService svc = new EREC_HataridosFeladatokService(this.dataContext);

            // Ügyirathoz kötés
            erec_HataridosFeladat.Obj_Id = erec_UgyUgyirat.Id;
            erec_HataridosFeladat.Updated.Obj_Id = true;

            erec_HataridosFeladat.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
            erec_HataridosFeladat.Updated.Obj_type = true;

            erec_HataridosFeladat.ObjTip_Id = "A04417A6-54AC-4AF1-9B63-5BABF0203D42";
            erec_HataridosFeladat.Updated.ObjTip_Id = true;

            if (!String.IsNullOrEmpty(erec_UgyUgyirat.Azonosito))
            {
                erec_HataridosFeladat.Azonosito = erec_UgyUgyirat.Azonosito;
                erec_HataridosFeladat.Updated.Azonosito = true;
            }

            Result res = svc.Insert(execParam, erec_HataridosFeladat);

            if (res.IsError)
            {
                Logger.Error("AddEREC_HataridosFeladatok hiba", execParam, res);
                throw new ResultException(res);
            }
        }
    }

    private static bool NeedElintezesJovahagyas(ExecParam p_xpm, out string vezetoID)
    {
        if (HasFunctionRight(p_xpm, Contentum.eUtility.Constants.Funkcio.UgyiratElintezesJovahagyas))
        {
            vezetoID = p_xpm.Felhasznalo_Id;
            return false;
        }

        vezetoID = Csoportok.GetFirstRightedLeader_ID(p_xpm, true, Contentum.eUtility.Constants.Funkcio.UgyiratElintezesJovahagyas);

        if (String.IsNullOrEmpty(vezetoID))
        {
            // Nem található az aktuális felhasználó fölé rendelt,
            // adott jogosultsággal rendelkezõ vezetõ a szervezeti hierarchiában!
            throw new ResultException(53202);
        }

        if (vezetoID == p_xpm.Felhasznalo_Id)
            return false;
        else
            return true;

    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result ElintezetteNyilvanitasJovahagyasa(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52240);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            ExecParam execParam_UgyiratGet = execParam.Clone();

            Result result_ugyiratGet = this.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }

            if (result_ugyiratGet.Record == null)
            {
                // hiba
                throw new ResultException(52241);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            // Ügyirat UPDATE:
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;

            ElintezetteNyilvanitasJovahagyasaInternal(execParam.Clone(), erec_UgyUgyirat, erec_HataridosFeladatok);

            this.AddEREC_HataridosFeladatok(execParam.Clone(), erec_UgyUgyirat, erec_HataridosFeladatok);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "UgyiratElintezesJovahagyas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    internal void ElintezetteNyilvanitasJovahagyasaInternal(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
               Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);

        ErrorDetails errorDetail = null;

        if (!Contentum.eRecord.BaseUtility.Ugyiratok.ElintezetteNyilvanitasJovahagyhato(
            ugyiratStatusz, execParam.Clone(), out errorDetail))
        {
            //Az ügyirat elintézetté nyilvánítása nem hagyható jóvá!
            throw new ResultException(52245, errorDetail);
        }

        //Jóváhagyó törlése
        erec_UgyUgyirat.Typed.Kovetkezo_Felelos_Id = System.Data.SqlTypes.SqlGuid.Null;
        erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

        //Állapot:
        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Elintezett;
        erec_UgyUgyirat.Updated.Allapot = true;

        // Küldés elektronikus irattárba, ha IRATTAROZAS_ELEKTRONIKUS_JOVAHAGYAS_NELKUL=1
        if (Contentum.eRecord.BaseUtility.Ugyiratok.IsIrattarozasElektronikusJovahagyasNelkul(execParam))
        {
            SetUgyIratElektronikusIrattarba(execParam, ref erec_UgyUgyirat);
        }

        // update
        ExecParam execParam_UgyiratUpdate = execParam.Clone();

        Result result = this.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);

        if (result.IsError)
            throw new ResultException(result);
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result ElintezetteNyilvanitasVisszautasitasa(ExecParam execParam, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(execParam.Record_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52240);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ügyirat lekérése:
            ExecParam execParam_UgyiratGet = execParam.Clone();

            Result result_ugyiratGet = this.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }

            if (result_ugyiratGet.Record == null)
            {
                // hiba
                throw new ResultException(52241);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;


            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);

            ErrorDetails errorDetail = null;

            if (!Contentum.eRecord.BaseUtility.Ugyiratok.ElintezetteNyilvanitasVisszautasithato(
                ugyiratStatusz, execParam.Clone(), out errorDetail))
            {
                // Az ügyirat elintézetté nyilvánítása nem utasítható vissza!
                throw new ResultException(52246, errorDetail);
            }

            // Ügyirat UPDATE:
            erec_UgyUgyirat.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat.Base.Updated.Ver = true;


            // ElintezesDat törlése
            erec_UgyUgyirat.Typed.ElintezesDat = System.Data.SqlTypes.SqlDateTime.Null;
            erec_UgyUgyirat.Updated.ElintezesDat = true;

            // ElintezesMod törlése
            erec_UgyUgyirat.Typed.ElintezesMod = null;
            erec_UgyUgyirat.Updated.ElintezesMod = true;

            //Állapot:
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            erec_UgyUgyirat.Updated.Allapot = true;

            //Jóváhagyó törlése
            erec_UgyUgyirat.Typed.Kovetkezo_Felelos_Id = System.Data.SqlTypes.SqlGuid.Null;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

            ExecParam execParam_UgyiratUpdate = execParam.Clone();

            result = this.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);

            this.AddEREC_HataridosFeladatok(execParam.Clone(), erec_UgyUgyirat, erec_HataridosFeladatok);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "UgyiratElintezesVisszautasitas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    internal void ElintezetteNyilvanitasVisszavonasaInternal(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Elintezett)
        {
            // BUG_8122
            if (erec_UgyUgyirat.Jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus)
            {
                if (String.IsNullOrEmpty(erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez))
                {
                    erec_UgyUgyirat.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                    erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = execParam.Felhasznalo_Id;
                    erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;
                }
                else
                {
                    erec_UgyUgyirat.Csoport_Id_Felelos = erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                    erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez;
                    erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;
                }
            }
        }

        if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa)
        {
            //Jóváhagyó törlése
            erec_UgyUgyirat.Typed.Kovetkezo_Felelos_Id = System.Data.SqlTypes.SqlGuid.Null;
            erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;
        }

        if (erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Elintezett
            || erec_UgyUgyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa)
        {
            // ElintezesDat törlése
            erec_UgyUgyirat.Typed.ElintezesDat = System.Data.SqlTypes.SqlDateTime.Null;
            erec_UgyUgyirat.Updated.ElintezesDat = true;

            // ElintezesMod törlése
            erec_UgyUgyirat.Typed.ElintezesMod = null;
            erec_UgyUgyirat.Updated.ElintezesMod = true;

            //Állapot:
            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
            erec_UgyUgyirat.Updated.Allapot = true;
        }
    }

    #endregion
    /// <summary>
    /// Ügyirat lezárása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id">A lezárandó ügyirat Id-ja</param>
    /// <param name="lezarasDatuma">A lezárás dátuma</param>
    /// <returns></returns>
    [WebMethod()]
    public Result Lezaras(ExecParam execParam, String erec_UgyUgyiratok_Id, String lezarasDatuma, string lezarasOka)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52260);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52262);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;


                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_lezarhato = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Ellenõrzés, hogy lezárható-e az ügyirat:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.Lezarhato(
                    ugyiratStatusz, execParam_lezarhato, out errorDetail) == false)
                {
                    // Nem zárható le az ügyirat:
                    throw new ResultException(52261, errorDetail);
                }
                else
                {

                    // Ügyirat UPDATE:
                    /// Amit módosítani kell:
                    /// LezarasDat, Allapot
                    /// 
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    try
                    {
                        // dátumformátum-ellenõrzés:
                        DateTime datetime_lezaras = DateTime.Parse(lezarasDatuma);

                        erec_UgyUgyirat.LezarasDat = datetime_lezaras.ToString();
                        erec_UgyUgyirat.Updated.LezarasDat = true;
                    }
                    catch
                    {
                        erec_UgyUgyirat.LezarasDat = DateTime.Now.ToString();
                        erec_UgyUgyirat.Updated.LezarasDat = true;
                    }

                    // Állapot:

                    erec_UgyUgyirat.ElozoAllapot = erec_UgyUgyirat.Allapot;
                    erec_UgyUgyirat.Updated.ElozoAllapot = true;

                    erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                    erec_UgyUgyirat.Updated.Allapot = true;

                    //Lezárás oka
                    erec_UgyUgyirat.LezarasOka = lezarasOka;
                    erec_UgyUgyirat.Updated.LezarasOka = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_ugyiratUpdate =
                        erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);

                    if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_ugyiratUpdate);
                    }
                    else
                    {
                        // ÜgyiratDarabokat lezárt állapotúvá tesszük:
                        // (Elvileg nem kell, de korábban sok helyen az ügyiratdarab állapotot is figyeltük, ez még bent van néhány helyen)

                        EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
                        ExecParam execParam_ugyiratDarabGetAll = execParam.Clone();

                        EREC_UgyUgyiratdarabokSearch search = new EREC_UgyUgyiratdarabokSearch();

                        search.UgyUgyirat_Id.Filter(erec_UgyUgyirat.Id);

                        Result result_ugyiratDarabGetAll = service_ugyiratDarabok.GetAll(execParam_ugyiratDarabGetAll, search);
                        if (!String.IsNullOrEmpty(result_ugyiratDarabGetAll.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_ugyiratDarabGetAll);
                        }

                        foreach (DataRow row in result_ugyiratDarabGetAll.Ds.Tables[0].Rows)
                        {
                            string UgyUgyirat_Id = row["UgyUgyirat_Id"].ToString();
                            // ellenõrzés:
                            if (UgyUgyirat_Id != erec_UgyUgyirat.Id)
                            {
                                // hiba:
                                Logger.Error("Hiba az ügyiratdarab lekérdezésekor", execParam);
                                throw new ResultException(52260);
                            }

                            string Id = row["Id"].ToString();
                            string Ver = row["Ver"].ToString();

                            // Ügyiratdarab UPDATE:
                            EREC_UgyUgyiratdarabok erec_ugyugyiratDarab = new EREC_UgyUgyiratdarabok();
                            erec_ugyugyiratDarab.Updated.SetValueAll(false);
                            erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);

                            erec_ugyugyiratDarab.Base.Ver = Ver;
                            erec_ugyugyiratDarab.Base.Updated.Ver = true;

                            erec_ugyugyiratDarab.LezarasDat = erec_UgyUgyirat.LezarasDat;
                            erec_ugyugyiratDarab.Updated.LezarasDat = true;

                            erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Lezart;
                            erec_ugyugyiratDarab.Updated.Allapot = true;

                            ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
                            execParam_ugyiratDarabUpdate.Record_Id = Id;

                            Result result_ugyiratDarabUpdate =
                                service_ugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

                            if (!String.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
                            {
                                // hiba:
                                throw new ResultException(result_ugyiratDarabUpdate);
                            }

                        }

                        //// Utolsó nyitott ügyiratdarab lezárása (ha még nincs):
                        //if (erec_ugyugyiratDarab != null
                        //    && erec_ugyugyiratDarab.Allapot != KodTarak.UGYIRATDARAB_ALLAPOT.Lezart
                        //    && erec_ugyugyiratDarab.Allapot != KodTarak.UGYIRATDARAB_ALLAPOT.Szerelt)
                        //{
                        //    // ÜgyiratDarab UPDATE

                        //    // Módosítani kell: LezarasDat, Allapot

                        //    erec_ugyugyiratDarab.Updated.SetValueAll(false);
                        //    erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);
                        //    erec_ugyugyiratDarab.Base.Updated.Ver = true;

                        //    erec_ugyugyiratDarab.LezarasDat = erec_UgyUgyirat.LezarasDat;
                        //    erec_ugyugyiratDarab.Updated.LezarasDat = true;

                        //    erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Lezart;
                        //    erec_ugyugyiratDarab.Updated.Allapot = true;

                        //    ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
                        //    execParam_ugyiratDarabUpdate.Record_Id = erec_ugyugyiratDarab.Id;

                        //    Result result_ugyiratDarabUpdate =
                        //        service_UgyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

                        //    if (!String.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
                        //    {
                        //        // hiba:
                        //        throw new ResultException(result_ugyiratDarabUpdate);
                        //    }

                        //}

                        // ha minden ok:
                        result = result_ugyiratUpdate;
                    }
                }
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratLezaras").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result Lezaras_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array, String lezarasDatuma, string lezarasOka)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        foreach (string erec_UgyUgyiratok_Id in erec_UgyUgyiratok_Id_Array)
        {
            result = Lezaras(execParam, erec_UgyUgyiratok_Id, lezarasDatuma, lezarasOka);

            if (result.IsError)
                break;
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat lezárásának visszavonása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result LezarasVisszavonas(ExecParam execParam, string erec_UgyUgyiratok_Id)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_UgyUgyiratok_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52260);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGet = execParam.Clone();
            execParam_UgyiratGet.Record_Id = erec_UgyUgyiratok_Id;

            Result result_ugyiratGet = erec_UgyUgyiratokService.Get(execParam_UgyiratGet);
            if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGet);
            }
            else
            {
                if (result_ugyiratGet.Record == null)
                {
                    // hiba
                    throw new ResultException(52262);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;


                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);
                ExecParam execParam_lezarhato = execParam.Clone();
                ErrorDetails errorDetail = null;

                // Ellenõrzés, hogy visszavonható-e a lezárás
                if (Contentum.eRecord.BaseUtility.Ugyiratok.LezarasVisszavonhato(
                    ugyiratStatusz, execParam_lezarhato, out errorDetail) == false)
                {
                    // Nem vonható vissza a lezárás
                    throw new ResultException(52261, errorDetail);
                }
                else
                {

                    // Ügyirat UPDATE:
                    /// Amit módosítani kell:
                    /// Allapot
                    /// 
                    erec_UgyUgyirat.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.SetValueAll(false);
                    erec_UgyUgyirat.Base.Updated.Ver = true;

                    if (String.IsNullOrEmpty(erec_UgyUgyirat.ElozoAllapot))
                    {
                        if (!string.IsNullOrEmpty(erec_UgyUgyirat.ElintezesDat))
                            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Elintezett;
                        else
                            erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                    }
                    else
                    {
                        erec_UgyUgyirat.Allapot = erec_UgyUgyirat.ElozoAllapot;
                        erec_UgyUgyirat.ElozoAllapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
                    }

                    erec_UgyUgyirat.Updated.Allapot = true;
                    erec_UgyUgyirat.Updated.ElozoAllapot = true;

                    erec_UgyUgyirat.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
                    erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

                    ExecParam execParam_ugyiratUpdate = execParam.Clone();
                    execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat.Id;

                    Result result_ugyiratUpdate =
                        erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_UgyUgyirat);

                    if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_ugyiratUpdate);
                    }
                    else
                    {
                        // ÜgyiratDarabokat újra meg kell nyitni:
                        // (Elvileg nem kell, de korábban sok helyen az ügyiratdarab állapotot is figyeltük, ez még bent van néhány helyen)

                        EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
                        ExecParam execParam_ugyiratDarabGetAll = execParam.Clone();

                        EREC_UgyUgyiratdarabokSearch search = new EREC_UgyUgyiratdarabokSearch();

                        search.UgyUgyirat_Id.Filter(erec_UgyUgyirat.Id);

                        //search.EljarasiSzakasz.Value = "";
                        //search.EljarasiSzakasz.Operator = Query.Operators.isnull;

                        Result result_ugyiratDarabGetAll = service_ugyiratDarabok.GetAll(execParam_ugyiratDarabGetAll, search);
                        if (!String.IsNullOrEmpty(result_ugyiratDarabGetAll.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_ugyiratDarabGetAll);
                        }

                        foreach (DataRow row in result_ugyiratDarabGetAll.Ds.Tables[0].Rows)
                        {
                            string UgyUgyirat_Id = row["UgyUgyirat_Id"].ToString();
                            // ellenõrzés:
                            if (UgyUgyirat_Id != erec_UgyUgyirat.Id)
                            {
                                // hiba:
                                Logger.Error("Hiba az ügyiratdarab lekérdezésekor", execParam);
                                throw new ResultException(52260);
                            }

                            string Id = row["Id"].ToString();
                            string Ver = row["Ver"].ToString();

                            // Ügyiratdarab UPDATE:
                            EREC_UgyUgyiratdarabok erec_ugyugyiratDarab = new EREC_UgyUgyiratdarabok();
                            erec_ugyugyiratDarab.Updated.SetValueAll(false);
                            erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);

                            erec_ugyugyiratDarab.Base.Ver = Ver;
                            erec_ugyugyiratDarab.Base.Updated.Ver = true;

                            erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
                            erec_ugyugyiratDarab.Updated.Allapot = true;

                            ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
                            execParam_ugyiratDarabUpdate.Record_Id = Id;

                            Result result_ugyiratDarabUpdate =
                                service_ugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

                            if (!String.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
                            {
                                // hiba:
                                throw new ResultException(result_ugyiratDarabUpdate);
                            }

                        }

                        // ha minden ok:
                        result = result_ugyiratUpdate;
                    }
                }
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, erec_UgyUgyiratok_Id, "EREC_UgyUgyiratok", "UgyiratLezarasVisszavonas").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod]
    public Result LezarasVisszavonas_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        foreach (string erec_UgyUgyiratok_Id in erec_UgyUgyiratok_Id_Array)
        {
            result = LezarasVisszavonas(execParam, erec_UgyUgyiratok_Id);

            if (result.IsError)
                break;
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public Result Kikeres(String Ugyirat_Id, Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus kikeresIrattarTipus, ExecParam execParam_kikeres, string Note)
    {
        #region Kikérés indítása:

        EREC_IrattariKikeroService service_irattariKikero = new EREC_IrattariKikeroService(this.dataContext);
        execParam_kikeres.Record_Id = Ugyirat_Id;

        EREC_IrattariKikero irattariKikero = new EREC_IrattariKikero();
        irattariKikero.Updated.SetValueAll(false);
        irattariKikero.Base.Updated.SetValueAll(false);

        irattariKikero.DokumentumTipus = "U";
        irattariKikero.Updated.DokumentumTipus = true;

        irattariKikero.FelhasznalasiCel = "U";
        irattariKikero.Updated.FelhasznalasiCel = true;

        irattariKikero.KeresDatuma = DateTime.Now.ToString();
        irattariKikero.Updated.KeresDatuma = true;

        irattariKikero.FelhasznaloCsoport_Id_Kikero = execParam_kikeres.Felhasznalo_Id;
        irattariKikero.Updated.FelhasznaloCsoport_Id_Kikero = true;

        irattariKikero.Keres_note = Note;
        irattariKikero.Updated.Keres_note = true;

        Result result_kikeres = new Result();

        switch (kikeresIrattarTipus)
        {
            case Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.AtmenetiIrattar:
                result_kikeres = service_irattariKikero.KikeresAtmenetiIrattarbol(execParam_kikeres, Ugyirat_Id, irattariKikero, true);
                break;
            case Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.SkontroIrattar:
                // csak tömeges lehet
                string[] Ugyirat_Id_Array = new string[1];
                Ugyirat_Id_Array[0] = Ugyirat_Id;
                result_kikeres = service_irattariKikero.KikeresSkontroIrattarbol_Tomeges(execParam_kikeres, Ugyirat_Id_Array, irattariKikero);
                break;
            default:
                throw new ResultException("Ismeretlen irattár típus, a kikérést nem sikerült végrehajtani.");
        }

        if (!String.IsNullOrEmpty(result_kikeres.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_kikeres);
        }

        #endregion

        return result_kikeres;
    }

    public Result Kolcsonzes(String Ugyirat_Id, ExecParam execParam_kolcsonzes, String Note)
    {
        #region Kölcsönzés indítása, automatikus jóváhagyással:
        EREC_IrattariKikeroService service_irattariKikero = new EREC_IrattariKikeroService(this.dataContext);
        execParam_kolcsonzes.Record_Id = Ugyirat_Id;

        EREC_IrattariKikero irattariKikero = new EREC_IrattariKikero();
        irattariKikero.Updated.SetValueAll(false);
        irattariKikero.Base.Updated.SetValueAll(false);

        irattariKikero.DokumentumTipus = "U";
        irattariKikero.Updated.DokumentumTipus = true;

        irattariKikero.FelhasznalasiCel = "U";
        irattariKikero.Updated.FelhasznalasiCel = true;

        irattariKikero.KeresDatuma = DateTime.Now.ToString();
        irattariKikero.Updated.KeresDatuma = true;

        // 30 napos határidõt kell beállítani:
        irattariKikero.KikerKezd = DateTime.Today.ToString();
        irattariKikero.Updated.KikerKezd = true;

        irattariKikero.KikerVege = DateTime.Today.AddDays(30).ToString();
        irattariKikero.Updated.KikerVege = true;

        irattariKikero.Allapot = KodTarak.IRATTARIKIKEROALLAPOT.Nyitott;
        irattariKikero.Updated.Allapot = true;

        irattariKikero.FelhasznaloCsoport_Id_Kikero = execParam_kolcsonzes.Felhasznalo_Id;
        irattariKikero.Updated.FelhasznaloCsoport_Id_Kikero = true;

        irattariKikero.UgyUgyirat_Id = Ugyirat_Id;
        irattariKikero.Updated.UgyUgyirat_Id = true;

        irattariKikero.Keres_note = Note;
        irattariKikero.Updated.Keres_note = true;

        Result result_kolcsonzes = service_irattariKikero.Kolcsonzes(execParam_kolcsonzes, irattariKikero, true, false);
        if (!String.IsNullOrEmpty(result_kolcsonzes.ErrorCode))
        {
            // hiba:
            throw new ResultException(result_kolcsonzes);
        }
        #endregion

        return result_kolcsonzes;
    }


    public bool ExistsIrattariKikero(String Ugyirat_Id, ExecParam _ExecParam)
    {
        Logger.Debug("Kikérõk vizsgálata, van-e érvényes kikérõ az ügyiratra...", _ExecParam);

        /// Kikérõk vizsgálata, van-e érvényes kikérõ az ügyiratra:
        /// 
        EREC_IrattariKikeroService service_irattariKikero = new EREC_IrattariKikeroService(this.dataContext);

        EREC_IrattariKikeroSearch search_kikerok = new EREC_IrattariKikeroSearch();

        search_kikerok.UgyUgyirat_Id.Filter(Ugyirat_Id);
        search_kikerok.Allapot.In(new string[] {
            KodTarak.IRATTARIKIKEROALLAPOT.Engedelyezheto,
            KodTarak.IRATTARIKIKEROALLAPOT.Engedelyzett,
            KodTarak.IRATTARIKIKEROALLAPOT.Nyitott
        });

        Result result_irattariKikerokGetAll = service_irattariKikero.GetAll(_ExecParam.Clone(), search_kikerok);
        if (result_irattariKikerokGetAll.IsError)
        {
            // hiba:
            throw new ResultException(result_irattariKikerokGetAll);
        }

        // ha van találat --> már kikérték, nem kell újra kikérni:
        if (result_irattariKikerokGetAll.GetCount > 0)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Ügyirat szerelése vagy szerelésre elõkészítése egy másik ügyiratba, szükség esetén kikérés indításával
    /// Munkaügyirat élesítésénél használatos
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="szerelendo_Ugyirat_Id">Az ügyirat Id-ja, amit szerelni akarunk</param>
    /// <param name="cel_Ugyirat_Id">Az ügyirat Id-ja, ahova szerelni akarjuk a szerelendõ ügyiratot</param>
    /// <param name="erec_HataridosFeladatok">A mûvelethez esetleg megadott kezelési feljegyzés, ami a 'cél' ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum. A Record értéke true, ha sikeres szerelés, false, ha csak sikeres elõkészítés történt</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result SzerelesVagyElokeszitesKikeressel(ExecParam execParam, String szerelendo_Ugyirat_Id, String cel_Ugyirat_Id
           , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(szerelendo_Ugyirat_Id)
            || String.IsNullOrEmpty(cel_Ugyirat_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52270);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Cél ügyirat lekérése
            // (Cél) Ügyirat lekérése:
            ExecParam execParam_Ugyirat_Cel_Get = execParam.Clone();
            execParam_Ugyirat_Cel_Get.Record_Id = cel_Ugyirat_Id;

            Result result_ugyirat_Cel_Get = sp.Get(execParam_Ugyirat_Cel_Get);
            if (result_ugyirat_Cel_Get.IsError)
            {
                // hiba:
                throw new ResultException(result_ugyirat_Cel_Get);
            }

            if (result_ugyirat_Cel_Get.Record == null)
            {
                // hiba
                throw new ResultException(52272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_Cel = (EREC_UgyUgyiratok)result_ugyirat_Cel_Get.Record;


            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz celUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Cel);
            ExecParam execParam_szerelhetoBeleUgyirat = execParam.Clone();
            ErrorDetails errorDetail = null;

            // ha nem KÜK, ellenõrizzük az õrzõt
            if (execParam.FelhasznaloSzervezet_Id != KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id)
            {
                // Ellenõrzés, hogy szerelhetõ-e bele ügyirat:
                if (Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyirat(
                    celUgyiratStatusz, execParam_szerelhetoBeleUgyirat, out errorDetail) == false)
                {
                    // Nem lehet az ügyiratba szerelni:
                    throw new ResultException(52271, errorDetail);
                }
            }
            // ha KÜK, nem ellenõrizzük az õrzõt, csak az állapototkat
            else if (Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyiratAllapotu(celUgyiratStatusz, out errorDetail) == false)
            {
                // Nem lehet az ügyiratba szerelni:
                throw new ResultException(52271, errorDetail);
            }

            #endregion Cél ügyirat lekérése

            #region Szerelendõ ügyirat lekérése
            // Szerelendõ ügyirat lekérése és ellenõrzése

            ExecParam execParam_szerelendo_Get = execParam.Clone();
            execParam_szerelendo_Get.Record_Id = szerelendo_Ugyirat_Id;

            Result result_szerelendo_Get = sp.Get(execParam_szerelendo_Get);
            if (result_szerelendo_Get.IsError)
            {
                // hiba:
                throw new ResultException(result_szerelendo_Get);
            }

            if (result_szerelendo_Get.Record == null)
            {
                // hiba:
                throw new ResultException(52272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_Szerelendo =
                (EREC_UgyUgyiratok)result_szerelendo_Get.Record;

            #endregion Szerelendõ ügyirat lekérése

            #region Elõzmény kikérés/kölcsönzés, ha szükséges, hogy késõbb szerelni lehessen

            #region Irattárban van-e/Kikölcsönzött?
            bool kolcsonzesInditasKell = false;
            bool atmenetiIrattarbolKikeresKell = false;// lehet átmeneti vagy skontró irattár is
            bool kikolcsonzott = false;

            Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.None;

            if (erec_UgyUgyirat_Szerelendo.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                || erec_UgyUgyirat_Szerelendo.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
            {
                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam.Clone()).Obj_Id;

                // Ha központi irattárban van:
                if (!String.IsNullOrEmpty(erec_UgyUgyirat_Szerelendo.Csoport_Id_Felelos) && !String.IsNullOrEmpty(kozpontiIrattarId)
                    && erec_UgyUgyirat_Szerelendo.Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                {
                    kolcsonzesInditasKell = true;
                }
                else
                {
                    // Átmeneti irattárban van:
                    atmenetiIrattarbolKikeresKell = true;
                    kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.AtmenetiIrattar;
                }


                /// Irattárba küldött állapotnál elõfordulhat, hogy valaki már beleiktatott, és keletkezett kikérõ
                /// Ilyenkor már nem indítunk új kikérést
                if (erec_UgyUgyirat_Szerelendo.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott)
                {
                    // ha van találat --> már kikérték, nem kell újra kikérni:
                    if (ExistsIrattariKikero(erec_UgyUgyirat_Szerelendo.Id, execParam.Clone()))
                    {
                        kolcsonzesInditasKell = false;
                        atmenetiIrattarbolKikeresKell = false;
                    }
                }

            }
            else if (erec_UgyUgyirat_Szerelendo.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
            {
                // Skontró irattárban van-e:
                string skontroIrattarId = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam.Clone()).Obj_Id;
                if (!String.IsNullOrEmpty(erec_UgyUgyirat_Szerelendo.Csoport_Id_Felelos) && !String.IsNullOrEmpty(skontroIrattarId)
                && erec_UgyUgyirat_Szerelendo.Csoport_Id_Felelos.ToLower() == skontroIrattarId.ToLower())
                {
                    atmenetiIrattarbolKikeresKell = true;
                    kikeresIrattarTipus = Contentum.eRecord.BaseUtility.IrattariKikero.KikeresIrattarTipus.SkontroIrattar;
                }
            }
            else if (erec_UgyUgyirat_Szerelendo.Allapot == KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott)
            {
                kikolcsonzott = true;
            }

            #endregion

            #region Irattárban lévõ ügyiratnál további mûveletek

            if (atmenetiIrattarbolKikeresKell)
            {
                #region Kikérés indítása:
                Result result_kikeres = Kikeres(erec_UgyUgyirat_Szerelendo.Id, kikeresIrattarTipus, execParam.Clone(), "Szerelés");
                #endregion
            }
            else if (kolcsonzesInditasKell)
            {
                #region Kölcsönzés indítása, automatikus jóváhagyással:
                Result result_kikeres = Kolcsonzes(erec_UgyUgyirat_Szerelendo.Id, execParam.Clone(), "Szerelés");
                #endregion
            }
            else if (kikolcsonzott)
            {
                // csak szereljük, itt nem kell ügyintézésre kikérni
            }

            if (atmenetiIrattarbolKikeresKell || kolcsonzesInditasKell)
            {
                #region Szerelendõ ügyirat ismételt lekérése
                // a kikérés során módosult az ügyirat, a verzió miatt újra le kell kérni
                Logger.Debug("SzerelesVagyElokeszitesKikeressel: Ügyirat ismételt lekérés - Start", execParam);

                ExecParam execParam_ugyiratGet = execParam.Clone();
                execParam_ugyiratGet.Record_Id = szerelendo_Ugyirat_Id;

                Result result_ugyiratGet = sp.Get(execParam_ugyiratGet);
                if (result_ugyiratGet.IsError)
                {
                    // hiba
                    throw new ResultException(result_ugyiratGet);
                }
                else
                {
                    erec_UgyUgyirat_Szerelendo = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                }

                Logger.Debug("SzerelesVagyElokeszitesKikeressel: Ügyirat ismételt lekérés - End", execParam);
                #endregion Szerelendõ ügyirat ismételt lekérése
            }

            #endregion Irattárban lévõ ügyiratnál további mûveletek

            #endregion Elõzmény kikérés/kölcsönzés, ha szükséges

            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz szerelendoUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Szerelendo);

            if (Contentum.eRecord.BaseUtility.Ugyiratok.Szerelheto(szerelendoUgyiratStatusz, execParam.Clone(), out errorDetail))
            {
                #region Szerelés

                Logger.Info(String.Format("SzerelesVagyElokeszitesKikeressel: Szerelés - Elõzmény ügyirat: {0}; Cél ügyirat: {1}", szerelendo_Ugyirat_Id, cel_Ugyirat_Id), execParam);

                ExecParam execParam_szereles = execParam.Clone();
                Result result_szereles = this.Szereles(execParam_szereles, erec_UgyUgyirat_Szerelendo, erec_UgyUgyirat_Cel, erec_HataridosFeladatok);

                //Result result_szereles = this.Szereles(execParam_szereles, szerelendo_Ugyirat_Id, cel_Ugyirat_Id, null);
                if (!String.IsNullOrEmpty(result_szereles.ErrorCode))
                {
                    // hiba:
                    Logger.Error("SzerelesVagyElokeszitesKikeressel: Hiba a szerelés során", execParam, result_szereles);

                    throw new ResultException(result_szereles);
                }

                result = result_szereles;
                result.Record = true;

                #endregion
            }
            else
            {
                #region Szerelés elõkészítés

                Logger.Info(String.Format("SzerelesVagyElokeszitesKikeressel: Szerelés elõkészítése (szülõ bejegyzése, ügyirat UPDATE) - Elõzmény ügyirat: {0}; Cél ügyirat: {1}", szerelendo_Ugyirat_Id, cel_Ugyirat_Id), execParam);

                erec_UgyUgyirat_Szerelendo.Updated.SetValueAll(false);
                erec_UgyUgyirat_Szerelendo.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat_Szerelendo.Base.Updated.Ver = true;

                erec_UgyUgyirat_Szerelendo.UgyUgyirat_Id_Szulo = cel_Ugyirat_Id;
                erec_UgyUgyirat_Szerelendo.Updated.UgyUgyirat_Id_Szulo = true;

                ExecParam execParam_elokeszites = execParam.Clone();
                execParam_elokeszites.Record_Id = szerelendo_Ugyirat_Id;
                Result result_elokeszites = this.Update(execParam_elokeszites, erec_UgyUgyirat_Szerelendo);
                if (result_elokeszites.IsError)
                {
                    // hiba:
                    Logger.Error("SzerelesVagyElokeszitesKikeressel: Hiba a szerelés elõkészítés során", execParam, result_elokeszites);

                    throw new ResultException(result_elokeszites);
                }

                //// Elõkészített szerelés automatikus végrehajtása: itt már nem kell Lezárt állapotúnak lennie, elég, ha a felhasználónál van
                //Result result_szerelesvegrehajtas = this.ElokeszitettSzerelesVegrehajtasa(execParam.Clone(), szerelendo_Ugyirat_Id, cel_Ugyirat_Id, erec_HataridosFeladatok);
                //if (result_szerelesvegrehajtas.IsError)
                //{
                //    // hiba, marad az elõkészítés:
                //    Logger.Warn("SzerelesVagyElokeszitesKikeressel: Hiba az elõkészített szerelés véglegesítés során", execParam, result_szerelesvegrehajtas);
                result = result_elokeszites;
                result.Record = false;
                //}
                //else
                //{
                //    result = result_szerelesvegrehajtas;
                //    result.Record = true;
                //}
                #endregion
            }


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, szerelendo_Ugyirat_Id, "EREC_UgyUgyiratok", "UgyiratSzereles").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// Ügyirat szerelése egy másik ügyiratba
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="szerelendo_Ugyirat_Id">Az ügyirat Id-ja, amit szerelni akarunk</param>
    /// <param name="cel_Ugyirat_Id">Az ügyirat Id-ja, ahova szerelni akarjuk a szerelendõ ügyiratot</param>
    /// <param name="erec_HataridosFeladatok">A mûvelethez esetleg megadott kezelési feljegyzés, ami a 'cél' ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result Szereles(ExecParam execParam, String szerelendo_Ugyirat_Id, String cel_Ugyirat_Id
           , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(szerelendo_Ugyirat_Id)
            || String.IsNullOrEmpty(cel_Ugyirat_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52270);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();



            // (Cél) Ügyirat lekérése:
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_Ugyirat_Cel_Get = execParam.Clone();
            execParam_Ugyirat_Cel_Get.Record_Id = cel_Ugyirat_Id;

            Result result_ugyirat_Cel_Get = erec_UgyUgyiratokService.Get(execParam_Ugyirat_Cel_Get);
            if (result_ugyirat_Cel_Get.IsError)
            {
                // hiba:
                throw new ResultException(result_ugyirat_Cel_Get);
            }

            if (result_ugyirat_Cel_Get.Record == null)
            {
                // hiba
                throw new ResultException(52272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_Cel = (EREC_UgyUgyiratok)result_ugyirat_Cel_Get.Record;


            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz celUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Cel);
            ExecParam execParam_szerelhetoBeleUgyirat = execParam.Clone();
            ErrorDetails errorDetail = null;

            // Ellenõrzés, hogy szerelhetõ-e bele ügyirat:
            if (Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyirat(
                celUgyiratStatusz, execParam_szerelhetoBeleUgyirat, out errorDetail) == false)
            {
                // Nem lehet az ügyiratba szerelni:
                throw new ResultException(52271, errorDetail);
            }


            // Szerelendõ ügyirat lekérése és ellenõrzése:

            ExecParam execParam_szerelendo_Get = execParam.Clone();
            execParam_szerelendo_Get.Record_Id = szerelendo_Ugyirat_Id;

            Result result_szerelendo_Get = erec_UgyUgyiratokService.Get(execParam_szerelendo_Get);
            if (result_szerelendo_Get.IsError)
            {
                // hiba:
                throw new ResultException(result_szerelendo_Get);
            }

            if (result_szerelendo_Get.Record == null)
            {
                // hiba:
                throw new ResultException(52272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_Szerelendo =
                (EREC_UgyUgyiratok)result_szerelendo_Get.Record;

            // Ellenõrzés, szerelhetõ-e:
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz szerelendoUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Szerelendo);
            ExecParam execParam_szerelheto = execParam.Clone();

            if (Contentum.eRecord.BaseUtility.Ugyiratok.Szerelheto(
                szerelendoUgyiratStatusz, execParam_szerelheto, out errorDetail) == false)
            {
                // hiba:
                throw new ResultException(52273, errorDetail);
            }

            result = Szereles(execParam, erec_UgyUgyirat_Szerelendo, erec_UgyUgyirat_Cel, erec_HataridosFeladatok);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, szerelendo_Ugyirat_Id, "EREC_UgyUgyiratok", "UgyiratSzereles").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Ügyirat szerelése egy másik ügyiratba - segédfv. - már szerelhetõségre ellenõrzött ügyiratobjektumokra használandó!
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="szerelendo_Ugyirat_Id">Az ügyirat Id-ja, amit szerelni akarunk</param>
    /// <param name="cel_Ugyirat_Id">Az ügyirat Id-ja, ahova szerelni akarjuk a szerelendõ ügyiratot</param>
    /// <param name="erec_HataridosFeladatok">A mûvelethez esetleg megadott kezelési feljegyzés, ami a 'cél' ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    private Result Szereles(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat_Szerelendo, EREC_UgyUgyiratok erec_UgyUgyirat_Cel
           , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {

        // Iktatókönyvek alapján ellenõrzés
        // 1. Évek összehasonlítása, ha az iktatókönyvek eltérõek
        //// 2. Egy iktatókönyvön belül fõszámok összehasonlítása
        bool bSameBook = (String.Compare(erec_UgyUgyirat_Szerelendo.IraIktatokonyv_Id, erec_UgyUgyirat_Cel.IraIktatokonyv_Id, true) == 0);
        if (!bSameBook)
        {
            EREC_IraIktatoKonyvekService service_IktatoKonyvek = new EREC_IraIktatoKonyvekService(this.dataContext);

            #region szerelendõ
            ExecParam execParam_IktatoKonyv_Szerelendo_Get = execParam.Clone();
            execParam_IktatoKonyv_Szerelendo_Get.Record_Id = erec_UgyUgyirat_Szerelendo.IraIktatokonyv_Id;

            Result result_IktatoKonyv_Szerelendo_Get = service_IktatoKonyvek.Get(execParam_IktatoKonyv_Szerelendo_Get);
            if (result_IktatoKonyv_Szerelendo_Get.IsError)
            {
                throw new ResultException(result_IktatoKonyv_Szerelendo_Get);
            }

            EREC_IraIktatoKonyvek erec_IraIktatoKonyvek_Szerelendo = (EREC_IraIktatoKonyvek)result_IktatoKonyv_Szerelendo_Get.Record;
            #endregion szerelendõ

            #region cél
            ExecParam execParam_IktatoKonyv_Cel_Get = execParam.Clone();
            execParam_IktatoKonyv_Cel_Get.Record_Id = erec_UgyUgyirat_Cel.IraIktatokonyv_Id;

            Result result_IktatoKonyv_Cel_Get = service_IktatoKonyvek.Get(execParam_IktatoKonyv_Cel_Get);
            if (result_IktatoKonyv_Cel_Get.IsError)
            {
                throw new ResultException(result_IktatoKonyv_Cel_Get);
            }

            EREC_IraIktatoKonyvek erec_IraIktatoKonyvek_Cel = (EREC_IraIktatoKonyvek)result_IktatoKonyv_Cel_Get.Record;
            #endregion cél

            int Ev_Szerelendo;
            int Ev_Cel;

            if (Int32.TryParse(erec_IraIktatoKonyvek_Szerelendo.Ev, out Ev_Szerelendo)
                && Int32.TryParse(erec_IraIktatoKonyvek_Cel.Ev, out Ev_Cel))
            {
                if (Ev_Szerelendo > Ev_Cel)
                {
                    throw new ResultException(52277); //Késõbbi évben iktatott ügyirat nem szerelhetõ egy korábbi évbõl származó ügyiratba!
                }
            }
            else
            {
                throw new ResultException(52278); //Hibás formátum: iktatókönyv év!
            }
        }

        #region Ügyirat jelleg (elektronikus, vegyes, papír) módosulás meghatározása
        bool isUgyiratJellegChanged = false;
        string UgyiratJelleg = null;
        //if (erec_UgyUgyirat_Cel.Jelleg != erec_UgyUgyirat_Szerelendo.Jelleg)
        //{
        //    // elvileg minden inhomogén esetben változik, csak biztonság kedvéért ellenõrizzük fajtánként
        //    // továbbá ha a szerelendõ ügyiratnál hiányzott a jelleg, papír alapúnak tekintjük
        //    if (erec_UgyUgyirat_Cel.Jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus)
        //    {
        //        if (String.IsNullOrEmpty(erec_UgyUgyirat_Szerelendo.Jelleg) // alapértelmezés: papír
        //            || erec_UgyUgyirat_Szerelendo.Jelleg == KodTarak.UGYIRAT_JELLEG.Papir
        //            || erec_UgyUgyirat_Szerelendo.Jelleg == KodTarak.UGYIRAT_JELLEG.Vegyes)
        //        {
        //            UgyiratJelleg = KodTarak.UGYIRAT_JELLEG.Vegyes;
        //            isUgyiratJellegChanged = true;
        //        }
        //    }
        //    else if (erec_UgyUgyirat_Cel.Jelleg == KodTarak.UGYIRAT_JELLEG.Papir)
        //    {
        //        if (erec_UgyUgyirat_Szerelendo.Jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus
        //            || erec_UgyUgyirat_Szerelendo.Jelleg == KodTarak.UGYIRAT_JELLEG.Vegyes)
        //        {
        //            UgyiratJelleg = KodTarak.UGYIRAT_JELLEG.Vegyes;
        //            isUgyiratJellegChanged = true;
        //        }
        //    }
        //}

        UgyiratJelleg = this.CombineUgyiratJelleg(erec_UgyUgyirat_Cel.Jelleg, erec_UgyUgyirat_Szerelendo.Jelleg);
        isUgyiratJellegChanged = (UgyiratJelleg != erec_UgyUgyirat_Cel.Jelleg);
        #endregion Ügyirat jelleg (elektronikus, vegyes, papír) módosulás meghatározása

        // Ügyirat UPDATE:
        erec_UgyUgyirat_Szerelendo.Updated.SetValueAll(false);
        erec_UgyUgyirat_Szerelendo.Base.Updated.SetValueAll(false);
        erec_UgyUgyirat_Szerelendo.Base.Updated.Ver = true;

        // UgyUgyirat_Id_Szulo beállítása a cél ügyiratra:
        erec_UgyUgyirat_Szerelendo.UgyUgyirat_Id_Szulo = erec_UgyUgyirat_Cel.Id;
        erec_UgyUgyirat_Szerelendo.Updated.UgyUgyirat_Id_Szulo = true;


        // Állapot átállítása:
        erec_UgyUgyirat_Szerelendo.Allapot = KodTarak.UGYIRAT_ALLAPOT.Szerelt;
        erec_UgyUgyirat_Szerelendo.Updated.Allapot = true;

        //Szülõ állapotának beállítása
        erec_UgyUgyirat_Szerelendo.TovabbitasAlattAllapot = erec_UgyUgyirat_Cel.Allapot;
        erec_UgyUgyirat_Szerelendo.Updated.TovabbitasAlattAllapot = true;

        ExecParam execParam_ugyiratUpdate = execParam.Clone();
        execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat_Szerelendo.Id;

        Result result_ugyiratUpdate = this.Update(
                    execParam_ugyiratUpdate, erec_UgyUgyirat_Szerelendo);

        if (result_ugyiratUpdate.IsError)
        {
            // hiba:
            throw new ResultException(result_ugyiratUpdate);
        }

        // Kezelési feljegyzés INSERT, ha megadták
        if (erec_HataridosFeladatok != null)
        {
            EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
            ExecParam execParam_kezFeljInsert = execParam.Clone();

            // Ügyirathoz kötés
            erec_HataridosFeladatok.Obj_Id = erec_UgyUgyirat_Cel.Id;
            erec_HataridosFeladatok.Updated.Obj_Id = true;

            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
            erec_HataridosFeladatok.Updated.Obj_type = true;

            if (!String.IsNullOrEmpty(erec_UgyUgyirat_Cel.Azonosito))
            {
                erec_HataridosFeladatok.Azonosito = erec_UgyUgyirat_Cel.Azonosito;
                erec_HataridosFeladatok.Updated.Azonosito = true;
            }

            Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                execParam_kezFeljInsert, erec_HataridosFeladatok);
            if (result_kezFeljInsert.IsError)
            {
                // hiba:
                throw new ResultException(result_kezFeljInsert);
            }
        }

        // Szerelendõ ügyirat összes ügyiratdarabjának lekérése,
        // és állapotuk beállítása szereltre

        EREC_UgyUgyiratdarabokService service_ugyugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
        EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();

        erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Filter(erec_UgyUgyirat_Szerelendo.Id);

        ExecParam execParam_ugyiratDarabokGetAll = execParam.Clone();

        Result result_ugyiratDarabokGetAll = service_ugyugyiratDarabok.GetAll(execParam_ugyiratDarabokGetAll, erec_UgyUgyiratdarabokSearch);

        if (result_ugyiratDarabokGetAll.IsError)
        {
            // hiba:
            throw new ResultException(result_ugyiratDarabokGetAll);
        }


        DataSet ds_ugyiratDarabok = result_ugyiratDarabokGetAll.Ds;
        if (ds_ugyiratDarabok == null
            || ds_ugyiratDarabok.Tables[0] == null
            || ds_ugyiratDarabok.Tables[0].Rows.Count == 0)
        {
            // hiba: ügyiratdarabok lekérése sikertelen
            throw new ResultException(52274);
        }


        // Összes ügyiratDarabon végigmegyünk, állapot update
        foreach (DataRow row in ds_ugyiratDarabok.Tables[0].Rows)
        {
            String ugyiratDarab_Id = row["Id"].ToString();
            String Ver = row["Ver"].ToString();


            EREC_UgyUgyiratdarabok erec_ugyugyiratDarab = new EREC_UgyUgyiratdarabok();

            // UPDATE:
            erec_ugyugyiratDarab.Updated.SetValueAll(false);
            erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);

            erec_ugyugyiratDarab.Base.Updated.Ver = true;
            erec_ugyugyiratDarab.Base.Ver = Ver;

            // Állapot állítása:
            erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Szerelt;
            erec_ugyugyiratDarab.Updated.Allapot = true;

            ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
            execParam_ugyiratDarabUpdate.Record_Id = ugyiratDarab_Id;

            Result result_ugyiratDarabUpdate =
                service_ugyugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

            if (result_ugyiratDarabUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_ugyiratDarabUpdate);
            }
        }

        #region Cél ügyirat jelleg módosítás, ha szükséges
        if (isUgyiratJellegChanged)
        {
            Logger.Debug(String.Format("Ügyirat jelleg módosulás: cél eredeti: {0}; szerelendõ: {1}; cél új: {2}"
                , KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("UGYIRAT_JELLEG", erec_UgyUgyirat_Cel.Jelleg, execParam, Context.Cache)
                , KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("UGYIRAT_JELLEG", erec_UgyUgyirat_Szerelendo.Jelleg, execParam, Context.Cache)
                , KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("UGYIRAT_JELLEG", UgyiratJelleg, execParam, Context.Cache)));

            erec_UgyUgyirat_Cel.Updated.SetValueAll(false);
            erec_UgyUgyirat_Cel.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat_Cel.Base.Updated.Ver = true;

            //Jelleg beállítása
            erec_UgyUgyirat_Cel.Jelleg = UgyiratJelleg;
            erec_UgyUgyirat_Cel.Updated.Jelleg = true;

            ExecParam execParam_ugyiratCelUpdate = execParam.Clone();
            execParam_ugyiratCelUpdate.Record_Id = erec_UgyUgyirat_Cel.Id;

            Result result_ugyiratCelUpdate = this.Update(
                        execParam_ugyiratCelUpdate, erec_UgyUgyirat_Cel);

            if (result_ugyiratCelUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_ugyiratCelUpdate);
            }
        }
        #endregion Cél ügyirat jelleg módosítás, ha szükséges

        #region SAKKORA OROKLES

        if (Rendszerparameterek.UseSakkora(execParam))
        {
            Result result_ugyiratCelOroklesUpdate = null;

            if (string.IsNullOrEmpty(erec_UgyUgyirat_Cel.SakkoraAllapot) && !string.IsNullOrEmpty(erec_UgyUgyirat_Szerelendo.SakkoraAllapot))
            {
                EREC_UgyUgyiratok ugyCelSakk = GetUgyirat(execParam.Clone(), erec_UgyUgyirat_Cel.Id);
                ugyCelSakk.Hatarido = erec_UgyUgyirat_Szerelendo.Hatarido;
                ugyCelSakk.Updated.Hatarido = true;
                ugyCelSakk.ElteltIdo = erec_UgyUgyirat_Szerelendo.ElteltIdo;
                ugyCelSakk.Updated.ElteltIdo = true;
                ugyCelSakk.ElteltIdoIdoEgyseg = erec_UgyUgyirat_Szerelendo.ElteltIdoIdoEgyseg;
                ugyCelSakk.Updated.ElteltIdoIdoEgyseg = true;
                ugyCelSakk.ElteltidoAllapot = erec_UgyUgyirat_Szerelendo.ElteltidoAllapot;
                ugyCelSakk.Updated.ElteltidoAllapot = true;
                ugyCelSakk.SakkoraAllapot = erec_UgyUgyirat_Szerelendo.SakkoraAllapot;
                ugyCelSakk.Updated.SakkoraAllapot = true;

                ExecParam execParam_ugyiratoroklesCelUpdate = execParam.Clone();
                execParam_ugyiratoroklesCelUpdate.Record_Id = ugyCelSakk.Id;

                result_ugyiratCelOroklesUpdate = this.Update(execParam_ugyiratoroklesCelUpdate, ugyCelSakk);
                Logger.Debug(string.Format("Szereles sakkora orokles: cel:{0} szerelendo:{1}", erec_UgyUgyirat_Cel.Id, erec_UgyUgyirat_Szerelendo));
                if (result_ugyiratCelOroklesUpdate.IsError)
                {
                    throw new ResultException(result_ugyiratCelOroklesUpdate);
                }
            }
            if (result_ugyiratCelOroklesUpdate != null)
            {
                return result_ugyiratCelOroklesUpdate;
            }
        }
        #endregion SAKKORA OROKLES
        // ha minden OK:        

        return result_ugyiratUpdate;
    }

    /// <summary>
    /// Ügyirat(ok) szerelése az utóügyiratba (egy tranzakcióban)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="szerelendoUgyiratIdList"></param>
    /// <param name="celUgyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SzerelesTomeges(ExecParam execParam, List<Guid> szerelendoUgyiratIdList, Guid celUgyiratId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || szerelendoUgyiratIdList == null
            || szerelendoUgyiratIdList.Count == 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52270);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Egymás után meghívjuk a szereléseket:
            foreach (Guid szerelendoUgyiratId in szerelendoUgyiratIdList)
            {
                var resultSzereles = this.Szereles(execParam, szerelendoUgyiratId.ToString(), celUgyiratId.ToString(), null);
                if (resultSzereles.IsError)
                {
                    EREC_UgyUgyiratok eloIrat = GetUgyiratById(execParam, szerelendoUgyiratId.ToString());
                    EREC_UgyUgyiratok utoIrat = GetUgyiratById(execParam, celUgyiratId.ToString());
                    if (eloIrat == null || utoIrat == null)
                    {
                        throw new ResultException(resultSzereles);
                    }
                    else
                    {
                        ErrorDetails errorDetail = new ErrorDetails()
                        {
                            Code = 1,
                            Message = string.Format("Utóirat: {0}, Előirat: {1}", utoIrat.Azonosito, eloIrat.Azonosito)
                        };
                        throw new ResultException(resultSzereles, errorDetail);
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat szerelése egy másik ügyiratba visszavonás
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="szerelendo_Ugyirat_Id">Az ügyirat Id-ja, amit szerelni akarunk visszavonni</param>
    /// <param name="cel_Ugyirat_Id">Az ügyirat Id-ja, ahonnan szerelni visszavonni akarjuk a szerelendõ ügyiratot</param>
    /// <param name="erec_HataridosFeladatok">A mûvelethez esetleg megadott kezelési feljegyzés, ami a 'cél' ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    [WebMethod()]
    public Result SzerelesVisszavonasa(ExecParam execParam, String visszavonando_Ugyirat_Id)
    {
        Logger.DebugStart();
        Logger.Debug("EREC_UgyUgyiratokService SzerelesVisszavonasa start");
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(62270);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Arguments args = new Arguments();
        args.Add(new Argument("FelhasznaloId", execParam.Felhasznalo_Id, ArgumentTypes.Guid));
        args.Add(new Argument("Visszavonando_Ugyirat_Id", visszavonando_Ugyirat_Id, ArgumentTypes.Guid));
        args.ValidateArguments();

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Visszavonandó ügyirat lekérése és ellenõrzése

            Logger.Debug("Visszavonando ugyirat lekerese start");

            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);

            ExecParam execParam_visszavonando_Get = execParam.Clone();
            execParam_visszavonando_Get.Record_Id = visszavonando_Ugyirat_Id;

            Result result_visszavonando_Get = erec_UgyUgyiratokService.Get(execParam_visszavonando_Get);
            if (!String.IsNullOrEmpty(result_visszavonando_Get.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("Visszavonando ugyirat lekerese hiba: {0},{1}", result_visszavonando_Get.ErrorCode, result_visszavonando_Get.ErrorMessage));
                throw new ResultException(result_visszavonando_Get);
            }
            if (result_visszavonando_Get.Record == null)
            {
                // hiba:
                Logger.Error("result_visszavonando_Get.Record = null");
                throw new ResultException(62272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_Visszavonando =
                (EREC_UgyUgyiratok)result_visszavonando_Get.Record;

            Logger.Debug("Visszavonando ugyirat lekerese end");

            //Ellenõrzés, szerelhetõ-e:
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz visszavonandoUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Visszavonando);
            ExecParam execParam_szerelheto = execParam.Clone();
            ErrorDetails errorDetail = null;

            if (Contentum.eRecord.BaseUtility.Ugyiratok.SzereltVisszavonhato(
                visszavonandoUgyiratStatusz, execParam_szerelheto, out errorDetail) == false)
            {
                // hiba:
                Logger.Warn("Szerelt ugyirat nem deszerelheto");
                Logger.Debug("Ugyirat allapota: " + visszavonandoUgyiratStatusz.Allapot);
                Logger.Debug("Ugyirat orzoje: " + visszavonandoUgyiratStatusz.FelhCsopId_Orzo);
                throw new ResultException(62273, errorDetail);
            }

            #endregion

            #region szulougyirat lekerese
            if (String.IsNullOrEmpty(erec_UgyUgyirat_Visszavonando.UgyUgyirat_Id_Szulo))
            {
                Logger.Error("A szerelt ugyirat szulo ugyirat id-ja nincs kitoltve");
                throw new ResultException(62277);
            }
            string szulo_Ugyirat_Id = erec_UgyUgyirat_Visszavonando.UgyUgyirat_Id_Szulo;
            //Logger.Debug("szulo ugyirat lekerese start");
            //Logger.Debug("szulo ugyirat Id: " + szulo_Ugyirat_Id);

            //ExecParam execParam_Ugyirat_szulo_Get = execParam.Clone();
            //execParam_Ugyirat_szulo_Get.Record_Id = szulo_Ugyirat_Id;

            //Result result_ugyirat_szulo_Get = erec_UgyUgyiratokService.Get(execParam_Ugyirat_szulo_Get);
            //if (!String.IsNullOrEmpty(result_ugyirat_szulo_Get.ErrorCode))
            //{
            //    // hiba:
            //    Logger.Error(String.Format("szulo ugyirat lekerese hiba: {0},{1}", result_ugyirat_szulo_Get.ErrorCode, result_ugyirat_szulo_Get.ErrorMessage));
            //    throw new ResultException(result_ugyirat_szulo_Get);
            //}
            //if (result_ugyirat_szulo_Get.Record == null)
            //{
            //    // hiba
            //    Logger.Error("result_ugyirat_szulo_Get.Record = null");
            //    throw new ResultException(62272);
            //}

            //EREC_UgyUgyiratok erec_UgyUgyirat_szulo = (EREC_UgyUgyiratok)result_ugyirat_szulo_Get.Record;

            //Logger.Debug("szulo ugyirat lekerese end");

            //Contentum.eRecord.BaseUtility.Ugyiratok.Statusz szuloUgyiratStatusz =
            //    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_szulo);
            //ExecParam execParam_visszavonhatBeloleUgyirat = execParam.Clone();

            //if (Contentum.eRecord.BaseUtility.Ugyiratok.VisszavonhatoBeloleSzereltUgyirat(
            //    szuloUgyiratStatusz, execParam_visszavonhatBeloleUgyirat) == false)
            //{
            //    Logger.Warn("Ugyiratbol nem vonhato vissza szereles");
            //    Logger.Debug("Ugyirat allapota: " + szuloUgyiratStatusz.Allapot);
            //    Logger.Debug("Ugyirat orzoje: " + szuloUgyiratStatusz.FelhCsopId_Orzo);
            //    throw new ResultException(62271);
            //}
            #endregion

            // Ügyirat UPDATE:
            erec_UgyUgyirat_Visszavonando.Updated.SetValueAll(false);
            erec_UgyUgyirat_Visszavonando.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat_Visszavonando.Base.Updated.Ver = true;

            // UgyUgyirat_Id_Szulo beállítása:
            erec_UgyUgyirat_Visszavonando.Typed.UgyUgyirat_Id_Szulo = System.Data.SqlTypes.SqlGuid.Null;
            erec_UgyUgyirat_Visszavonando.Updated.UgyUgyirat_Id_Szulo = true;


            // Állapot átállítása:
            Logger.Debug("Visszavonando ugyirat tovabbitas alatt allapota: " + erec_UgyUgyirat_Visszavonando.TovabbitasAlattAllapot);
            erec_UgyUgyirat_Visszavonando.Allapot = KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart;
            erec_UgyUgyirat_Visszavonando.Updated.Allapot = true;

            //Szülõ állapotának beállítása
            erec_UgyUgyirat_Visszavonando.TovabbitasAlattAllapot = Contentum.eUtility.Constants.BusinessDocument.nullString;//String.Empty;
            erec_UgyUgyirat_Visszavonando.Updated.TovabbitasAlattAllapot = true;

            ExecParam execParam_ugyiratUpdate = execParam.Clone();
            execParam_ugyiratUpdate.Record_Id = erec_UgyUgyirat_Visszavonando.Id;

            Logger.Debug("UgyiratUpdate start");

            Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(
                        execParam_ugyiratUpdate, erec_UgyUgyirat_Visszavonando);

            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("UgyiratUpdate hiba: {0},{1}", result_ugyiratUpdate.ErrorCode, result_ugyiratUpdate.ErrorMessage));
                throw new ResultException(result_ugyiratUpdate);
            }

            Logger.Debug("UgyiratUpdate end");

            // Szerelendõ ügyirat összes ügyiratdarabjának lekérése,
            // és állapotuk beállítása

            EREC_UgyUgyiratdarabokService service_ugyugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
            EREC_UgyUgyiratdarabokSearch erec_UgyUgyiratdarabokSearch = new EREC_UgyUgyiratdarabokSearch();

            erec_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Filter(erec_UgyUgyirat_Visszavonando.Id);

            ExecParam execParam_ugyiratDarabokGetAll = execParam.Clone();

            Logger.Debug("UgyiratDarabok GetAll start");

            Result result_ugyiratDarabokGetAll = service_ugyugyiratDarabok.GetAll(execParam_ugyiratDarabokGetAll, erec_UgyUgyiratdarabokSearch);

            if (!String.IsNullOrEmpty(result_ugyiratDarabokGetAll.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("UgyiratDarabok GetAll hiba: {0},{1}", result_ugyiratDarabokGetAll.ErrorCode, result_ugyiratDarabokGetAll.ErrorMessage));
                throw new ResultException(result_ugyiratDarabokGetAll);
            }

            DataSet ds_ugyiratDarabok = result_ugyiratDarabokGetAll.Ds;
            if (ds_ugyiratDarabok == null
                || ds_ugyiratDarabok.Tables[0] == null
                || ds_ugyiratDarabok.Tables[0].Rows.Count == 0)
            {
                // hiba: ügyiratdarabok lekérése sikertelen
                Logger.Error("UgyiratDarabok lekerese sikertelen");
                throw new ResultException(62274);
            }

            Logger.Debug("UgyiratDarabok GetAll end");


            // Összes ügyiratDarabon végigmegyünk, állapot update
            foreach (DataRow row in ds_ugyiratDarabok.Tables[0].Rows)
            {
                String ugyiratDarab_Id = row["Id"].ToString();
                String Ver = row["Ver"].ToString();


                EREC_UgyUgyiratdarabok erec_ugyugyiratDarab = new EREC_UgyUgyiratdarabok();

                // UPDATE:
                erec_ugyugyiratDarab.Updated.SetValueAll(false);
                erec_ugyugyiratDarab.Base.Updated.SetValueAll(false);

                erec_ugyugyiratDarab.Base.Updated.Ver = true;
                erec_ugyugyiratDarab.Base.Ver = Ver;

                // Állapot állítása:
                erec_ugyugyiratDarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Iktatott;
                erec_ugyugyiratDarab.Updated.Allapot = true;

                ExecParam execParam_ugyiratDarabUpdate = execParam.Clone();
                execParam_ugyiratDarabUpdate.Record_Id = ugyiratDarab_Id;

                Logger.Debug("UgyiratDarabUpdate start");
                Logger.Debug("UgyiratDarab Id: " + ugyiratDarab_Id);
                Logger.Debug("UgyiratDarab Verzio: " + Ver);

                Result result_ugyiratDarabUpdate =
                    service_ugyugyiratDarabok.Update(execParam_ugyiratDarabUpdate, erec_ugyugyiratDarab);

                if (!String.IsNullOrEmpty(result_ugyiratDarabUpdate.ErrorCode))
                {
                    // hiba:
                    Logger.Error(String.Format("UgyiratDarabUpdate hiba: {0},{1}", result_ugyiratDarabUpdate.ErrorCode, result_ugyiratDarabUpdate.ErrorMessage));
                    throw new ResultException(result_ugyiratDarabUpdate);
                }

                Logger.Debug("UgyiratDarabUpdate end");
            }

            #region Szülõ ügyirat lekérése és Jelleg, UgyintezesModja beállítása, ha szükséges

            Logger.Debug("szulo ugyirat lekerese start");
            Logger.Debug("szulo ugyirat Id: " + szulo_Ugyirat_Id);

            ExecParam execParam_Ugyirat_szulo_Get = execParam.Clone();
            execParam_Ugyirat_szulo_Get.Record_Id = szulo_Ugyirat_Id;

            Result result_ugyirat_szulo_Get = erec_UgyUgyiratokService.Get(execParam_Ugyirat_szulo_Get);
            if (result_ugyirat_szulo_Get.IsError)
            {
                // hiba:
                Logger.Error(String.Format("szulo ugyirat lekerese hiba: {0},{1}", result_ugyirat_szulo_Get.ErrorCode, result_ugyirat_szulo_Get.ErrorMessage));
                throw new ResultException(result_ugyirat_szulo_Get);
            }
            if (result_ugyirat_szulo_Get.Record == null)
            {
                // hiba
                Logger.Error("result_ugyirat_szulo_Get.Record = null");
                throw new ResultException(62272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_szulo = (EREC_UgyUgyiratok)result_ugyirat_szulo_Get.Record;

            Logger.Debug("szulo ugyirat lekerese end");

            ExecParam execParam_szuloJelleg = execParam.Clone();
            // minden mezõ módosítás flag kikapcsolása a Jelleg, UgyintezesModja beállítás elõtt
            erec_UgyUgyirat_szulo.Updated.SetValueAll(false);
            erec_UgyUgyirat_szulo.Base.Updated.SetValueAll(false);
            erec_UgyUgyirat_szulo = SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(execParam_szuloJelleg, erec_UgyUgyirat_szulo, null);

            // ha volt változás a mezõkben, UPDATE
            if (erec_UgyUgyirat_szulo.Updated.Jelleg || erec_UgyUgyirat_szulo.Updated.UgyintezesModja)
            {
                execParam_szuloJelleg.Record_Id = szulo_Ugyirat_Id;

                erec_UgyUgyirat_szulo.Base.Updated.Ver = true;

                Result result_szuloUgyiratUpdate = this.Update(execParam_szuloJelleg, erec_UgyUgyirat_szulo);

                if (result_szuloUgyiratUpdate.IsError)
                {
                    throw new ResultException(result_szuloUgyiratUpdate);
                }
            }

            #endregion Szülõ ügyirat lekérése és Jelleg, UgyintezesModja beállítása, ha szükséges

            // ha minden OK:
            result = result_ugyiratUpdate;

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, visszavonando_Ugyirat_Id, "EREC_UgyUgyiratok", "UgyiratSzerelesVisszavonasa").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("EREC_UgyUgyiratokService SzerelesVisszavonasa hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        Logger.Debug("EREC_UgyUgyiratokService SzerelesVisszavonasa end");
        Logger.DebugEnd();
        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Elõkészített szerelés végrehajtása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="szerelendo_Ugyirat_Id"></param>
    /// <param name="cel_Ugyirat_Id"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result ElokeszitettSzerelesVegrehajtasa(ExecParam execParam, string szerelendo_Ugyirat_Id, string cel_Ugyirat_Id
           , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(szerelendo_Ugyirat_Id)
            || String.IsNullOrEmpty(cel_Ugyirat_Id))
        {
            /// hiba
            /// Az elõkészített szerelés végrehajtása sikertelen
            Result result1 = ResultError.CreateNewResultWithErrorCode(52770);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Szerelendo Ügyirat GET

            ExecParam execParam_szerelendoGet = execParam.Clone();
            execParam_szerelendoGet.Record_Id = szerelendo_Ugyirat_Id;

            Result result_szerelendoGet = this.Get(execParam_szerelendoGet);
            if (result_szerelendoGet.IsError)
            {
                // hiba:
                throw new ResultException(result_szerelendoGet);
            }

            EREC_UgyUgyiratok szerelendoUgyiratObj = (EREC_UgyUgyiratok)result_szerelendoGet.Record;

            #endregion

            #region Cél ügyirat (utóirat) GET

            ExecParam execParam_celUgyiratGet = execParam.Clone();
            execParam_celUgyiratGet.Record_Id = cel_Ugyirat_Id;

            Result result_celUgyiratGet = this.Get(execParam_celUgyiratGet);
            if (result_celUgyiratGet.IsError)
            {
                // hiba
                throw new ResultException(result_celUgyiratGet);
            }

            EREC_UgyUgyiratok celUgyiratObj = (EREC_UgyUgyiratok)result_celUgyiratGet.Record;

            #endregion

            #region Ellenõrzés

            // Id-k egyeznek-e?
            if (szerelendoUgyiratObj.UgyUgyirat_Id_Szulo != celUgyiratObj.Id)
            {
                // hiba:
                Logger.Error("Hiba az elõkészített szerelés végrehajtása során! "
                    + " szerelendoUgyiratObj.UgyUgyirat_Id_Szulo == '" + szerelendoUgyiratObj.UgyUgyirat_Id_Szulo + "'"
                    + " celUgyiratObj.Id == '" + celUgyiratObj.Id + "'", execParam);
                throw new ResultException(52770);
            }

            // Elõkészített szerelés végrehajtható-e?
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz szerelendoUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(szerelendoUgyiratObj);
            ErrorDetails errorDetail = null;

            if (Contentum.eRecord.BaseUtility.Ugyiratok.ElokeszitettSzerelesVegrehajthato(szerelendoUgyiratStatusz
                , szerelendoUgyiratObj.UgyUgyirat_Id_Szulo, execParam, out errorDetail) == false)
            {
                // hiba:
                Logger.Error("Az elõkészített szerelés nem hajtható végre!");
                throw new ResultException(52771, errorDetail);
            }

            // Lehet-e szerelni az új ügyiratba?
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz celUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(celUgyiratObj);
            if (Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyirat(celUgyiratStatusz, execParam, out errorDetail) == false)
            {
                // hiba:
                Logger.Error("Az ügyiratba nem szerelhetõ bele másik ügyirat!", execParam);
                throw new ResultException(52772, errorDetail);
            }

            #endregion
            // -------------------------------------------------------------------------
            // EB 2011.01.13.: CR#2593: Szerelés egységesen a privát metódus hívásával
            // -------------------------------------------------------------------------
            #region Szerelendõ Ügyirat UPDATE

            //// Állítani kell: Állapotot szereltre, Lezárás dátumát

            //szerelendoUgyiratObj.Updated.SetValueAll(false);
            //szerelendoUgyiratObj.Base.Updated.SetValueAll(false);
            //szerelendoUgyiratObj.Base.Updated.Ver = true;

            //// Állapotot szereltre:
            //szerelendoUgyiratObj.Allapot = KodTarak.UGYIRAT_ALLAPOT.Szerelt;
            //szerelendoUgyiratObj.Updated.Allapot = true;

            //// Továbbítás alatt állapotba letesszük a cél ügyirat állapotát:
            //szerelendoUgyiratObj.TovabbitasAlattAllapot = celUgyiratObj.Allapot;
            //szerelendoUgyiratObj.Updated.TovabbitasAlattAllapot = true;

            // EB 2011.01.13.: ezt beállítjuk, mert bár most már egységesen a Szereles metódust hívjuk,
            // de a normál szereléstõl eltérõen itt nem biztosított elõzõleg a lezárt állapot (A/a lezárt)
            // Lezárás dátumát beállítjuk, ha nem volt beállítva:
            if (String.IsNullOrEmpty(szerelendoUgyiratObj.LezarasDat))
            {
                szerelendoUgyiratObj.LezarasDat = DateTime.Now.ToString();
                szerelendoUgyiratObj.Updated.LezarasDat = true;
            }

            //ExecParam execParam_szerelendoUgyiratUpdate = execParam.Clone();
            //execParam_szerelendoUgyiratUpdate.Record_Id = szerelendoUgyiratObj.Id;

            //Result result_szerelendoUpdate = this.Update(execParam_szerelendoUgyiratUpdate, szerelendoUgyiratObj);
            //if (result_szerelendoUpdate.IsError)
            //{
            //    // hiba:
            //    throw new ResultException(result_szerelendoUpdate);
            //}

            #endregion

            #region Kezelési feljegyzés Insert, ha volt

            //// Kezelési feljegyzés INSERT, ha megadták
            //if (erec_HataridosFeladatok != null)
            //{
            //    EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
            //    ExecParam execParam_kezFeljInsert = execParam.Clone();

            //    // Ügyirathoz kötés
            //    erec_HataridosFeladatok.Obj_Id = celUgyiratObj.Id;
            //    erec_HataridosFeladatok.Updated.Obj_Id = true;

            //    erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
            //    erec_HataridosFeladatok.Updated.Obj_type = true;

            //    if (!String.IsNullOrEmpty(celUgyiratObj.Azonosito))
            //    {
            //        erec_HataridosFeladatok.Azonosito = celUgyiratObj.Azonosito;
            //        erec_HataridosFeladatok.Updated.Azonosito = true;
            //    }

            //    Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
            //        execParam_kezFeljInsert, erec_HataridosFeladatok);
            //    if (result_kezFeljInsert.IsError)
            //    {
            //        // hiba:
            //        throw new ResultException(result_kezFeljInsert);
            //    }
            //}

            #endregion

            result = this.Szereles(execParam.Clone(), szerelendoUgyiratObj, celUgyiratObj, erec_HataridosFeladatok);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, szerelendo_Ugyirat_Id, "EREC_UgyUgyiratok", "UgyiratSzereles").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    /// <summary>
    /// Elõkészített szerelés felbontása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="szerelendo_Ugyirat_Id"></param>
    /// <param name="cel_Ugyirat_Id"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result ElokeszitettSzerelesFelbontasa(ExecParam execParam, string szerelendo_Ugyirat_Id, string cel_Ugyirat_Id
           , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(szerelendo_Ugyirat_Id)
            || String.IsNullOrEmpty(cel_Ugyirat_Id))
        {
            /// hiba
            /// Az elõkészített szerelés felbontása sikertelen
            Result result1 = ResultError.CreateNewResultWithErrorCode(52775);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Szerelendo Ügyirat GET

            ExecParam execParam_szerelendoGet = execParam.Clone();
            execParam_szerelendoGet.Record_Id = szerelendo_Ugyirat_Id;

            Result result_szerelendoGet = this.Get(execParam_szerelendoGet);
            if (result_szerelendoGet.IsError)
            {
                // hiba:
                throw new ResultException(result_szerelendoGet);
            }

            EREC_UgyUgyiratok szerelendoUgyiratObj = (EREC_UgyUgyiratok)result_szerelendoGet.Record;

            #endregion

            #region Cél ügyirat (utóirat) GET

            ExecParam execParam_celUgyiratGet = execParam.Clone();
            execParam_celUgyiratGet.Record_Id = cel_Ugyirat_Id;

            Result result_celUgyiratGet = this.Get(execParam_celUgyiratGet);
            if (result_celUgyiratGet.IsError)
            {
                // hiba
                throw new ResultException(result_celUgyiratGet);
            }

            EREC_UgyUgyiratok celUgyiratObj = (EREC_UgyUgyiratok)result_celUgyiratGet.Record;

            #endregion

            #region Ellenõrzés

            // Id-k egyeznek-e?
            if (szerelendoUgyiratObj.UgyUgyirat_Id_Szulo != celUgyiratObj.Id)
            {
                // hiba:
                Logger.Error("Hiba az elõkészített szerelés felbontása során! "
                    + " szerelendoUgyiratObj.UgyUgyirat_Id_Szulo == '" + szerelendoUgyiratObj.UgyUgyirat_Id_Szulo + "'"
                    + " celUgyiratObj.Id == '" + celUgyiratObj.Id + "'", execParam);
                throw new ResultException(52775);
            }

            // Elõkészített szerelés felbontható-e?
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz szerelendoUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(szerelendoUgyiratObj);
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz celUgyiratStatusz =
                Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(celUgyiratObj);
            ErrorDetails errorDetail = null;

            if (Contentum.eRecord.BaseUtility.Ugyiratok.ElokeszitettSzerelesFelbonthato(celUgyiratStatusz, szerelendoUgyiratStatusz
                , szerelendoUgyiratObj.UgyUgyirat_Id_Szulo, execParam, out errorDetail) == false)
            {
                // hiba:
                Logger.Error("Az elõkészített szerelés nem bontható fel!");
                throw new ResultException(52773, errorDetail);
            }

            #endregion

            #region Szerelendõ Ügyirat UPDATE

            // Állítani kell: UgyUgyirat_Id_Szulo -t ki kell törölni, állapotot nem kell bántani

            szerelendoUgyiratObj.Updated.SetValueAll(false);
            szerelendoUgyiratObj.Base.Updated.SetValueAll(false);
            szerelendoUgyiratObj.Base.Updated.Ver = true;

            szerelendoUgyiratObj.UgyUgyirat_Id_Szulo = String.Empty;
            szerelendoUgyiratObj.Updated.UgyUgyirat_Id_Szulo = true;

            ExecParam execParam_szerelendoUgyiratUpdate = execParam.Clone();
            execParam_szerelendoUgyiratUpdate.Record_Id = szerelendoUgyiratObj.Id;

            Result result_szerelendoUpdate = this.Update(execParam_szerelendoUgyiratUpdate, szerelendoUgyiratObj);
            if (result_szerelendoUpdate.IsError)
            {
                // hiba:
                throw new ResultException(result_szerelendoUpdate);
            }

            #endregion

            #region Kezelési feljegyzés Insert, ha volt

            // Kezelési feljegyzés INSERT, ha megadták
            if (erec_HataridosFeladatok != null)
            {
                EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                ExecParam execParam_kezFeljInsert = execParam.Clone();

                // Ügyirathoz kötés
                erec_HataridosFeladatok.Obj_Id = celUgyiratObj.Id;
                erec_HataridosFeladatok.Updated.Obj_Id = true;

                erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                erec_HataridosFeladatok.Updated.Obj_type = true;

                if (!String.IsNullOrEmpty(celUgyiratObj.Azonosito))
                {
                    erec_HataridosFeladatok.Azonosito = celUgyiratObj.Azonosito;
                    erec_HataridosFeladatok.Updated.Azonosito = true;
                }

                Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                    execParam_kezFeljInsert, erec_HataridosFeladatok);
                if (result_kezFeljInsert.IsError)
                {
                    // hiba:
                    throw new ResultException(result_kezFeljInsert);
                }
            }

            #endregion

            #region EseményNaplózás

            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, szerelendoUgyiratObj.Id, "EREC_UgyUgyiratok", "UgyiratSzerelesVisszavonasa").Record;

            eventLogService.Insert(execParam, eventLogRecord);

            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    #region Segédmetódusok: Ügyirat Jelleg meghatározása (pl. szerelés vagy szerelés visszavonása során)
    /// <summary>
    /// Megállapítja és kitölti egy ügyirat Jelleg értékét (elektronikus, papír, vegyes) a benne lévõ iratokból,
    /// valamint a közvetlenül bele szerelt ügyiratokból (beleértve a régi, migrált adatokat is)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="_EREC_UgyUgyiratok">A kérdéses ügyirat (szükség van az Id, Jelleg mezõire)</param>
    /// <param name="except_IraIrat_Id">Ezt az iratot figyelmen kívül hagyja (pl. irat sztornó, átiktatás miatt enélkül az irat nélkül vizsgáljuk)</param>
    /// <returns>A paraméterben megadott _EREC_UgyUgyiratok módosított Jelleg és UgyintezesModja mezõkkel</returns>
    public EREC_UgyUgyiratok SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(ExecParam execParam, EREC_UgyUgyiratok _EREC_UgyUgyiratok, string except_IraIrat_Id)
    {
        EREC_UgyUgyiratok erec_UgyUgyiratok = _EREC_UgyUgyiratok;

        string UgyiratJellegOriginal = erec_UgyUgyiratok.Jelleg;
        string UgyiratJellegNew = null;
        bool isUgyiratJellegChanged = false;

        EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();

        iratokSearch.Allapot.NotIn(new string[] { KodTarak.IRAT_ALLAPOT.Atiktatott, KodTarak.IRAT_ALLAPOT.Sztornozott });

        // pl. irat sztornó, átiktatás miatt enélkül az irat nélkül vizsgáljuk
        if (!string.IsNullOrEmpty(except_IraIrat_Id))
        {
            iratokSearch.Id.NotEquals(except_IraIrat_Id);
        }

        iratokSearch.Ugyirat_Id.Filter(erec_UgyUgyiratok.Id);

        Result jellegGetAllResult = service_iratok.GetAll(execParam.Clone(), iratokSearch);

        if (jellegGetAllResult.IsError)
        {
            throw new ResultException(jellegGetAllResult);
        }

        if (jellegGetAllResult.GetCount > 0)
        {
            string jelleg = jellegGetAllResult.Ds.Tables[0].Rows[0]["AdathordozoTipusa"].ToString();

            int i = 1;
            bool isHomogeneous = true;
            while (i < jellegGetAllResult.GetCount && isHomogeneous)
            {
                if (jellegGetAllResult.Ds.Tables[0].Rows[i]["AdathordozoTipusa"].ToString() != jelleg
                    // Ha van 'Vegyes' az iratok között, akkor az ügyirat mindenképpen 'Vegyes' lesz
                    || jelleg == KodTarak.UGYINTEZES_ALAPJA.Vegyes)
                {
                    isHomogeneous = false;
                }

                i++;
            }

            if (isHomogeneous)
            {
                switch (jelleg)
                {
                    case KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir:
                        UgyiratJellegNew = KodTarak.UGYIRAT_JELLEG.Elektronikus;
                        break;
                    case KodTarak.UGYINTEZES_ALAPJA.Vegyes:
                        UgyiratJellegNew = KodTarak.UGYIRAT_JELLEG.Vegyes;
                        break;
                    default:
                        UgyiratJellegNew = KodTarak.UGYIRAT_JELLEG.Papir;
                        break;
                }

                // meg kell vizsgálni a szerelteket:
                string UgyiratJellegSzereltek = this.GetUgyiratJellegBySzereltek(execParam, erec_UgyUgyiratok);

                // ha üres, akkor nem volt benne szerelt! vizsgálni kell!
                if (!String.IsNullOrEmpty(UgyiratJellegSzereltek))
                {
                    UgyiratJellegNew = this.CombineUgyiratJelleg(UgyiratJellegNew, UgyiratJellegSzereltek);
                }

                if (UgyiratJellegNew != UgyiratJellegOriginal)
                {
                    isUgyiratJellegChanged = true;
                }
            }
            else
            {
                // ha vegyes, nem kell vizsgálni a szerelteket
                UgyiratJellegNew = KodTarak.UGYIRAT_JELLEG.Vegyes;
                if (UgyiratJellegNew != UgyiratJellegOriginal)
                {
                    isUgyiratJellegChanged = true;
                }
            }

            string ugyintezesModja = null;
            if (isUgyiratJellegChanged)
            {
                erec_UgyUgyiratok.Jelleg = UgyiratJellegNew;
                erec_UgyUgyiratok.Updated.Jelleg = true;

                // új alapján
                // csak akkor elektronikus, ha az ügyirat is az, egyébként mindig hagyományos (papír és vegyes ügyiratjellegnél is)
                ugyintezesModja = (UgyiratJellegNew == KodTarak.UGYIRAT_JELLEG.Elektronikus ? KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir : KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir);
            }
            else
            {
                erec_UgyUgyiratok.Updated.Jelleg = false;

                // eredeti alapján: elvileg jnak kell lennie, biztonsági ellenõrzés
                // csak akkor elektronikus, ha az ügyirat is az, egyébként mindig hagyományos (papír és vegyes ügyiratjellegnél is)
                ugyintezesModja = (UgyiratJellegOriginal == KodTarak.UGYIRAT_JELLEG.Elektronikus ? KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir : KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir);

            }

            // Ha az ügyintézés módja != az új jelleggel, UPDATE
            //if (erec_UgyUgyiratok.UgyintezesModja != ugyintezesModja)
            //{
            //    erec_UgyUgyiratok.UgyintezesModja = ugyintezesModja;
            //    erec_UgyUgyiratok.Updated.UgyintezesModja = true;
            //}
            //else
            //{
            //    erec_UgyUgyiratok.Updated.UgyintezesModja = false;
            //}

        }

        return erec_UgyUgyiratok;
    }

    /// <summary>
    /// SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek hívása, és ha volt módosulás, akkor mentés az adatbázisba.
    /// </summary>
    public void UpdateUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(ExecParam execParam, string ugyiratId)
    {
        // Ügyirat lekérése:
        ExecParam execParamUgyiratGet = execParam.Clone();
        execParamUgyiratGet.Record_Id = ugyiratId;

        var resultUgyiratGet = this.Get(execParamUgyiratGet);
        if (resultUgyiratGet.IsError)
        {
            throw new ResultException(resultUgyiratGet);
        }

        EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)resultUgyiratGet.Record;

        ugyiratObj.Updated.SetValueAll(false);
        ugyiratObj.Base.Updated.SetValueAll(false);
        ugyiratObj.Base.Updated.Ver = true;

        this.SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(execParam, ugyiratObj, null);

        // Ha a Jelleg, vagy az UgyintezesModja mezõ módosult, akkor Update:
        if (ugyiratObj.Updated.Jelleg
            || ugyiratObj.Updated.UgyintezesModja)
        {
            // UPDATE:
            var resultUpdate = this.Update(execParamUgyiratGet, ugyiratObj);
            if (resultUpdate.IsError)
            {
                throw new ResultException(resultUpdate);
            }
        }
    }

    /// Megállapítja egy ügyirat Jelleg értékét (elektronikus, papír, vegyes) a közvetlenül
    /// bele szerelt ügyiratokból (beleértve a régi, migrált adatokat is)
    private string GetUgyiratJellegBySzereltek(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat_szulo)
    {
        string UgyiratJelleg = null;

        // top level szereltek
        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
        search.UgyUgyirat_Id_Szulo.Filter(erec_UgyUgyirat_szulo.Id); // szülõ ügyirat        
        search.Allapot.Filter(KodTarak.UGYIRAT_ALLAPOT.Szerelt);

        Result result_GetAllSzerelt = this.GetAll(execParam, search);

        if (result_GetAllSzerelt.IsError)
        {
            throw new ResultException(result_GetAllSzerelt);
        }

        if (result_GetAllSzerelt.GetCount > 0)
        {
            string jelleg = result_GetAllSzerelt.Ds.Tables[0].Rows[0]["Jelleg"].ToString();

            // ha nincs kitöltve, papírnak vesszük
            if (String.IsNullOrEmpty(jelleg))
            {
                jelleg = KodTarak.UGYIRAT_JELLEG.Papir;
            }

            int i = 1;
            bool isHomogeneous = true;
            while (i < result_GetAllSzerelt.GetCount && isHomogeneous)
            {
                string jelleg_row = result_GetAllSzerelt.Ds.Tables[0].Rows[i]["Jelleg"].ToString();

                // ha nincs kitöltve, papírnak vesszük
                if (String.IsNullOrEmpty(jelleg_row))
                {
                    jelleg_row = KodTarak.UGYIRAT_JELLEG.Papir;
                }

                if (jelleg_row != jelleg)
                {
                    isHomogeneous = false;
                }
                i++;
            }

            if (isHomogeneous)
            {
                UgyiratJelleg = jelleg;
            }
            else
            {
                UgyiratJelleg = KodTarak.UGYIRAT_JELLEG.Vegyes;
            }
        }

        if (String.IsNullOrEmpty(UgyiratJelleg) || UgyiratJelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus)
        {
            #region Régi adat ellenõrzés
            // van-e benne régi szerelt - feltételezés: régi adat csak papír alapú lehet
            if (!String.IsNullOrEmpty(erec_UgyUgyirat_szulo.RegirendszerIktatoszam))
            {
                UgyiratJelleg = this.CombineUgyiratJelleg(UgyiratJelleg, KodTarak.UGYIRAT_JELLEG.Papir);
            }
            #endregion Régi adat ellenõrzés
        }
        return UgyiratJelleg;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="UgyiratJellegOriginal">Azon ügyirat jellege, amit a másik jelleg befolyásolhat. Ha üres, papír alapúnak vesszük!</param>
    /// <param name="UgyiratJellegModifier">Az eredeti ügyirat jellegét befolyásoló (pl. bele szerelt) ügyirat jellege. Ha üres, papír alapúnak vesszük!</param>
    /// <returns>A két jelleg alapján meghatározott új ügyirat jelleg</returns>
    private string CombineUgyiratJelleg(string UgyiratJellegOriginal, string UgyiratJellegModifier)
    {
        if (String.IsNullOrEmpty(UgyiratJellegOriginal))
        {
            // alapértelmezés: papír
            UgyiratJellegOriginal = KodTarak.UGYIRAT_JELLEG.Papir;
        }

        if (String.IsNullOrEmpty(UgyiratJellegModifier))
        {
            // alapértelmezés: papír
            UgyiratJellegModifier = KodTarak.UGYIRAT_JELLEG.Papir;
        }

        string UgyiratJelleg = UgyiratJellegOriginal;
        if (UgyiratJellegOriginal != UgyiratJellegModifier && UgyiratJellegOriginal != KodTarak.UGYIRAT_JELLEG.Vegyes)
        {
            // elvileg minden inhomogén esetben változik
            UgyiratJelleg = KodTarak.UGYIRAT_JELLEG.Vegyes;
        }
        return UgyiratJelleg;
    }
    #endregion Segédmetódusok: Ügyirat Jelleg meghatározása (pl. szerelés vagy szerelés visszavonása során)

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllExcelDetails(ExecParam ExecParam, string KezdDat, string VegeDat)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllExcelDetails(ExecParam, KezdDat, VegeDat);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllExcelLogDetails(ExecParam ExecParam, string KezdDat, string VegeDat)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllExcelLogDetails(ExecParam, KezdDat, VegeDat);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    /// <summary>
    /// Ügyirat átadása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id">Az átadandó ügyirat</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A következõ kezelõ Id-ja</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Atadas(ExecParam execParam, String erec_UgyUgyiratok_Id
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.AtadasraKijeloles(execParam, erec_UgyUgyiratok_Id, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //átadott állapotba kerülés
            if (String.IsNullOrEmpty(result.Uid))
            {
                Logger.Debug("Nincs kézbesítési tétel");
            }
            else
            {
                //Kézbesítési tétel átadása
                Logger.Debug("Kézbesítési tétel átadása");
                string kezbesitesiTetelId = result.Uid;
                ExecParam xpmAtadas = execParam.Clone();
                EREC_IraKezbesitesiTetelekService svcKezbesitesiTeletelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                Result resAtadas = svcKezbesitesiTeletelek.Atadas(xpmAtadas, kezbesitesiTetelId);

                if (!String.IsNullOrEmpty(resAtadas.ErrorCode))
                {
                    throw new ResultException(resAtadas);
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirat átadása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyiratok_Id">Az átadandó ügyirat</param>
    /// <param name="csoport_Id_Felelos_Kovetkezo">A következõ kezelõ Id-ja</param>
    /// <param name="erec_HataridosFeladatok"></param>
    /// <returns></returns>
    public Result Atadas_Tomeges(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array
        , String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.AtadasraKijeloles_Tomeges(execParam, erec_UgyUgyiratok_Id_Array,
                csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //átadott állapotba kerülés

            EREC_IraKezbesitesiTetelekService svcKezbesitesiTeletelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            EREC_IraKezbesitesiTetelekSearch schKezbesitesiTeletelek = new EREC_IraKezbesitesiTetelekSearch();
            schKezbesitesiTeletelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt);
            schKezbesitesiTeletelek.Obj_Id.In(erec_UgyUgyiratok_Id_Array);

            Result resKezbesitesiTetelek = svcKezbesitesiTeletelek.GetAll(execParam.Clone(), schKezbesitesiTeletelek);

            if (resKezbesitesiTetelek.IsError)
            {
                throw new ResultException(resKezbesitesiTetelek);
            }

            List<string> erec_IraKezbesitesiTetelek_Id_List = new List<string>();

            foreach (DataRow row in resKezbesitesiTetelek.Ds.Tables[0].Rows)
            {
                erec_IraKezbesitesiTetelek_Id_List.Add(row["Id"].ToString());
            }

            if (erec_IraKezbesitesiTetelek_Id_List.Count == 0)
            {
                Logger.Debug("Nincs kézbesítési tétel");
            }
            else
            {
                //Kézbesítési tétel átadása
                Logger.Debug("Kézbesítési tétel átadása");
                ExecParam xpmAtadas = execParam.Clone();
                Result resAtadas = svcKezbesitesiTeletelek.Atadas_Tomeges(xpmAtadas, erec_IraKezbesitesiTetelek_Id_List.ToArray());

                if (!String.IsNullOrEmpty(resAtadas.ErrorCode))
                {
                    throw new ResultException(resAtadas);
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    public void TomegesUgyiratUpdateExecuted(ExecParam execParam, DataTable UpdatedRowsTable, EREC_UgyUgyiratok UpdatedRecords)
    {
        Logger.Debug("TomegesUgyiratUpdateExecuted start", execParam);
        Logger.Debug(String.Format("UpdatedRowsTable.Count: {0}", UpdatedRowsTable.Rows.Count));

        Result result = new Result();

        result = UpdateSzereltUgyiratok(execParam, UpdatedRowsTable, UpdatedRecords, SzereltUgyiratUpdateMode.JustRegi);

        if (result.IsError)
        {
            Logger.Error("TomegesUgyiratUpdateExecuted hiba", execParam, result);
            throw new ResultException(result);
        }

        Logger.Debug("TomegesUgyiratUpdateExecuted end", execParam);
    }

    #region felülvizsgálat
    [WebMethod()]
    public Result Meghosszabitas(ExecParam execParam, String[] erec_UgyUgyiratok_Id_Array
    , String megorzesiIdo, String idoegyseg, DateTime felulvizsgDat, string megjegyzes)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (erec_UgyUgyiratok_Id_Array == null || erec_UgyUgyiratok_Id_Array.Length == 0)
            {
                // hiba
                throw new ResultException(52700);
            }
            else
            {
                Result result_update = new Result();
                EREC_UgyUgyiratok erecUgyirat = new EREC_UgyUgyiratok();

                for (int i = 0; i < erec_UgyUgyiratok_Id_Array.Length; i++)
                {
                    String ugyirat_Id = erec_UgyUgyiratok_Id_Array[i];

                    // Ügyirat lekérése:
                    ExecParam execParam_UgyiratGet = execParam.Clone();
                    execParam_UgyiratGet.Record_Id = ugyirat_Id;

                    Result result_ugyiratGet = this.Get(execParam_UgyiratGet);
                    if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_ugyiratGet);
                    }
                    else
                    {
                        if (result_ugyiratGet.Record == null)
                        {
                            // hiba
                            throw new ResultException(52701);
                        }

                        erecUgyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                        erecUgyirat.Updated.SetValueAll(false);
                        erecUgyirat.Base.Updated.SetValueAll(false);
                        erecUgyirat.Base.Updated.Ver = true;

                        //bernat.laszlo modified : új Megõrzési idõ számítás + UjOrzesiIdo értékállítás
                        // BLG_2156
                        //erecUgyirat.Typed.MegorzesiIdoVege = erecUgyirat.Typed.MegorzesiIdoVege.Value.AddYears(Convert.ToInt32(megorzesiIdo));
                        erecUgyirat.Typed.MegorzesiIdoVege = AddMegorzesiIdo(Convert.ToInt32(megorzesiIdo), idoegyseg, erecUgyirat.Typed.MegorzesiIdoVege.Value);
                        erecUgyirat.Updated.MegorzesiIdoVege = true;
                        // BLG_2156
                        erecUgyirat.UjOrzesiIdo = megorzesiIdo;
                        erecUgyirat.Updated.UjOrzesiIdo = true;
                        erecUgyirat.UjOrzesiIdoIdoegyseg = idoegyseg;
                        erecUgyirat.Updated.UjOrzesiIdoIdoegyseg = true;
                        erecUgyirat.FelulvizsgalatDat = felulvizsgDat.ToString();
                        erecUgyirat.Updated.FelulvizsgalatDat = true;
                        erecUgyirat.FelhCsoport_Id_Felulvizsgalo = execParam.Felhasznalo_Id;
                        erecUgyirat.Updated.FelhCsoport_Id_Felulvizsgalo = true;

                        //bernat.laszlo eddig

                        result_update = this.Update(execParam_UgyiratGet, erecUgyirat);

                        // ha hiba volt az egyiknél, nem megyünk tovább:
                        if (!String.IsNullOrEmpty(result_update.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_update);
                        }

                        //kezelési feljegyzés létrehozása, ha van megjegyzés
                        var erec_HataridosFeladatok = CreateTaskNote(megjegyzes);
                        if (erec_HataridosFeladatok != null)
                        {
                            EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                            ExecParam execParam_kezFeljInsert = execParam.Clone();

                            // Ügyirathoz kötés
                            erec_HataridosFeladatok.Obj_Id = erecUgyirat.Id;
                            erec_HataridosFeladatok.Updated.Obj_Id = true;

                            erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                            erec_HataridosFeladatok.Updated.Obj_type = true;

                            if (!String.IsNullOrEmpty(erecUgyirat.Azonosito))
                            {
                                erec_HataridosFeladatok.Azonosito = erecUgyirat.Azonosito;
                                erec_HataridosFeladatok.Updated.Azonosito = true;
                            }

                            Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                                execParam_kezFeljInsert, erec_HataridosFeladatok);

                            if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                            {
                                throw new ResultException(result_kezFeljInsert);
                            }
                        }
                    }
                }

                result = result_update;
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllSelejtezheto(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            _EREC_UgyUgyiratokSearch.Manual_IrattariJel.Filter(UI.JegyzekTipus.SelejtezesiJegyzek.Value);

            _EREC_UgyUgyiratokSearch.Allapot.In(new string[] {
                KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
                KodTarak.UGYIRAT_ALLAPOT.Elintezett,
                KodTarak.UGYIRAT_ALLAPOT.Szerelt
            });
            _EREC_UgyUgyiratokSearch.Allapot.OrGroup("69");

            _EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.Filter(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
            _EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.OrGroup("69");

            // BUG 6520 - szereltek kiszűrése
            bool excludeSzereltek = String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.Manual_Szereltek.Value);
            if (!excludeSzereltek)
            {
                // csak a flag kell, a Manual_Allapot_Filter fogja majd szűrni
                _EREC_UgyUgyiratokSearch.Manual_Szereltek.Clear();
            }

            List<String> notInAllapotok = new List<string>
            {
                KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett,
                KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo,
                KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott
            };
            if (excludeSzereltek)
            {
                notInAllapotok.Add(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
            }

            // egyéb szervezetnek átadási jegyzéknél a továbbítás alatt állapotban be van jegyezve
            // a korábbi állapot, ezért ezeket ki kell szûrni a többi jegyzék listájáról
            _EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.NotIn(notInAllapotok);

            bool felulvizsgalatMindenIrattarra = Rendszerparameterek.GetBoolean(ExecParam.Clone(), "FELULVIZSGALAT_MINDENIRATTARRA", false);

            string elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(ExecParam);
            string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id;

            if (String.IsNullOrEmpty(elektronikusIrattarId))
            {
                Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                throw new ResultException(errorResult);
            }

            //LZS - BUG_13898
            if (felulvizsgalatMindenIrattarra)
            {
                if (String.IsNullOrEmpty(kozpontiIrattarId))
                {
                    Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                    throw new ResultException(errorResult);
                }

                _EREC_UgyUgyiratokSearch.Csoport_Id_Felelos.In(new string[] {
                kozpontiIrattarId,
                elektronikusIrattarId
                 }
                );
            }
            else
            {
                _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.In(new string[] {
                String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value) ? ExecParam.FelhasznaloSzervezet_Id : _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value, // BLG_8686
                kozpontiIrattarId
                 }
                );
            }

            if (String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.MegorzesiIdoVege.Value))
            {
                //LZS - BUG_10771
                //Korábban - LZS - BUG_9398
                //Mivel az történik, hogy a tárolt eljárás lekéri a rekordokat, majd ebből a CheckMegorzésiIdo függvényen keresztül
                //töröljük a megőrizendőket, előfordulhat, hogy a lekért rekordszámból nagy mennyiségű kerül törlésre. Pl: lekérünk egy 
                //10-es listát, és abból kitöröl 8-at, akkor 2 marad és ez zavaró. 
                //TODO: A korrekt megoldás az lenne, hogy le kellene vinni a szűrés ezen részét is a tárolt eljárásba. 
                //Ez kerül most bele a BUG_10771-el.

                //_EREC_UgyUgyiratokSearch.MegorzesiIdoVege.LessOrEqual(DateTime.Now.ToString());
                _EREC_UgyUgyiratokSearch.WhereByManual = "dbo.fn_EREC_UgyUgyiratok_SelejtezhetosegDatum(erec_ugyugyiratok.Id) <= getdate()";
            }


            result = this.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private Result CheckMegorzesiIdo(ExecParam ExecParam, DataTable dataTable, int? OriginalPageSize)
    {
        //LZS - BUG_9398
        int counter = 0;

        Result result = new Result();
        try
        {
            List<DataRow> deletableRows = new List<DataRow>();

            foreach (DataRow rowUgyirat in dataTable.Rows)
            {
                //LZS - BUG_9398
                //Ha eléri a felületről lekért rekordszámot, akkor kilép a függvényből.
                if (counter >= OriginalPageSize)
                {
                    foreach (DataRow row in deletableRows)
                    {
                        dataTable.Rows.Remove(row);
                    }

                    return new Result();
                }

                if (IsMegorzendo(ExecParam, rowUgyirat))
                {
                    deletableRows.Add(rowUgyirat);
                }
                else //LZS - BUG_9398
                {
                    counter++;
                }
            }

            foreach (DataRow row in deletableRows)
            {
                dataTable.Rows.Remove(row);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        return new Result();
    }

    private bool IsMegorzendo(ExecParam ExecParam, DataRow rowUgyirat)
    {
        ExecParam xpm = ExecParam.Clone();
        if (rowUgyirat["Allapot"].ToString() == KodTarak.UGYIRAT_ALLAPOT.Szerelt || Convert.ToInt32(rowUgyirat["SzereltCount"]) > 0)
        {
            xpm.Record_Id = rowUgyirat["Id"].ToString();
            Result res = this.GetAllSzereltByNode(xpm);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new ResultException(res);
            }

            foreach (DataRow rowSzerelt in res.Ds.Tables[0].Rows)
            {
                DateTime megorzesiIdo = DateTime.Parse(rowSzerelt["MegorzesiIdoVege"].ToString());
                if (megorzesiIdo > DateTime.Now)
                {
                    return true;
                }
            }
        }
        else
        {
            return false;
        }

        return false;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllLeveltarbaAdhato(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            _EREC_UgyUgyiratokSearch.Manual_IrattariJel.Filter(UI.JegyzekTipus.LeveltariAtadasJegyzek.Value);

            _EREC_UgyUgyiratokSearch.Allapot.In(new string[] {
                KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
                KodTarak.UGYIRAT_ALLAPOT.Elintezett
                }
               );
            _EREC_UgyUgyiratokSearch.Allapot.OrGroup("69");

            _EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.Filter(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
            _EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.OrGroup("69");

            // egyéb szervezetnek átadási jegyzéknél a továbbítás alatt állapotban be van jegyezve
            // a korábbi állapot, ezért ezeket ki kell szûrni a többi jegyzék listájáról
            _EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.NotIn(new string[] {
                KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett,
                KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo,
                KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott }
                );

            bool felulvizsgalatMindenIrattarra = Rendszerparameterek.GetBoolean(ExecParam.Clone(), "FELULVIZSGALAT_MINDENIRATTARRA", false);

            string elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(ExecParam);
            string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id;

            if (String.IsNullOrEmpty(elektronikusIrattarId))
            {
                Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                throw new ResultException(errorResult);
            }

            //LZS - BUG_13898
            if (felulvizsgalatMindenIrattarra)
            {
                if (String.IsNullOrEmpty(kozpontiIrattarId))
                {
                    Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                    throw new ResultException(errorResult);
                }

                _EREC_UgyUgyiratokSearch.Csoport_Id_Felelos.In(new string[] {
                kozpontiIrattarId,
                elektronikusIrattarId
                 }
                );
            }
            else
            {
                _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.In(new string[] {
                String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value) ? ExecParam.FelhasznaloSzervezet_Id : _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value, // BLG_8686
                kozpontiIrattarId
                 }
                );
            }

            _EREC_UgyUgyiratokSearch.MegorzesiIdoVege.LessOrEqual(DateTime.Now.ToString());

            result = this.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            Result resCheck = CheckMegorzesiIdo(ExecParam, result.Ds.Tables[0], null);

            if (!String.IsNullOrEmpty(resCheck.ErrorCode))
            {
                throw new ResultException(resCheck);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllEgyebSzervezetnekAtadhato(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // egyelõre nincs feltétel az egyéb szervezetnek átadhatóságra,
            // kivéve, hogy ne legyen mozgásban, de ezeken kívül minden ügyiratot hozunk,
            // ami megfelel a kapott keresési objektumnak

            List<string> notinnerAllapotok = new List<string>(new string[] {KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt
                , KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                , KodTarak.UGYIRAT_ALLAPOT.Sztornozott
                , KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett
                , KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo
                , KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott
                , KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott
                , KodTarak.UGYIRAT_ALLAPOT.Selejtezett});

            _EREC_UgyUgyiratokSearch.Allapot.NotIn(notinnerAllapotok);


            List<string> notinnerTovabbitasAlattAllapotok = new List<string>(new string[] {KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett
                , KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo
                , KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott
                , KodTarak.UGYIRAT_ALLAPOT.LeveltarbaAdott
                , KodTarak.UGYIRAT_ALLAPOT.Selejtezett});

            _EREC_UgyUgyiratokSearch.WhereByManual = String.Format("not({0}='{1}' and {2} in ({3}))"
                , _EREC_UgyUgyiratokSearch.Allapot.Name
                , KodTarak.UGYIRAT_ALLAPOT.Szerelt
                , _EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.Name
                , Search.GetSqlInnerString(notinnerTovabbitasAlattAllapotok.ToArray()));

            result = this.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            //Result resCheck = CheckMegorzesiIdo(ExecParam, result.Ds.Tables[0]);

            //if (!String.IsNullOrEmpty(resCheck.ErrorCode))
            //{
            //    throw new ResultException(resCheck);
            //}

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllFelulvizsgalando(ExecParam execParam, EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            erec_UgyUgyiratokSearch.Manual_IrattariJel.Filter("F");

            erec_UgyUgyiratokSearch.Allapot.In(new string[] {
                KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
                KodTarak.UGYIRAT_ALLAPOT.Elintezett
                }
                          );
            erec_UgyUgyiratokSearch.Allapot.OrGroup("69");

            erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Filter(KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott);
            erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.OrGroup("69");

            // egyéb szervezetnek átadási jegyzéknél a továbbítás alatt állapotban be van jegyezve
            // a korábbi állapot, ezért ezeket ki kell szûrni a többi jegyzék listájáról
            erec_UgyUgyiratokSearch.Manual_Allapot_Filter.NotIn(new string[] {
                KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett,
                KodTarak.UGYIRAT_ALLAPOT.Lezart_jegyzekben_levo,
                KodTarak.UGYIRAT_ALLAPOT.EgyebSzervezetnekAtadott }
                );

            bool felulvizsgalatMindenIrattarra = Rendszerparameterek.GetBoolean(execParam.Clone(), "FELULVIZSGALAT_MINDENIRATTARRA", false);

            string elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(execParam);

            if (String.IsNullOrEmpty(elektronikusIrattarId))
            {
                Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                throw new ResultException(errorResult);
            }

            if (!felulvizsgalatMindenIrattarra)
            {
                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id;

                if (String.IsNullOrEmpty(kozpontiIrattarId))
                {
                    Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                    throw new ResultException(errorResult);
                }

                erec_UgyUgyiratokSearch.Csoport_Id_Felelos.In(new string[] {
                kozpontiIrattarId,
                elektronikusIrattarId
                 }
                );
            }
            else
            {
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.In(new string[] {
                String.IsNullOrEmpty(erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value) ? execParam.FelhasznaloSzervezet_Id : erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value, // BLG_8686
                elektronikusIrattarId
                 }
                );
            }

            erec_UgyUgyiratokSearch.MegorzesiIdoVege.LessOrEqual(DateTime.Now.ToString());

            result = this.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            Result resCheck = CheckMegorzesiIdo(execParam, result.Ds.Tables[0], null);

            if (!String.IsNullOrEmpty(resCheck.ErrorCode))
            {
                throw new ResultException(resCheck);
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion

    #region szerelt ügyiratok kezelése

    private enum SzereltUgyiratUpdateMode
    {
        All = 1,
        JustEdok = 2,
        JustRegi = 3
    }

    private Result UpdateSzereltUgyiratok(ExecParam execParam, DataTable ParentUgyiratokTable, EREC_UgyUgyiratok parentUgyiratRegi, EREC_UgyUgyiratok parentUgyirat, SzereltUgyiratUpdateMode Mode)
    {
        Logger.DebugStart();
        Logger.Debug("UgyiratokService UpdateSzereltUgyiratok start");

        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        if (Mode == SzereltUgyiratUpdateMode.JustRegi)
        {
            if (!MapEdokUgyiratToMigUgyirat(parentUgyiratRegi, parentUgyirat, new MIG_Foszam()))
                return result;
        }

        List<string> parentUgyiratIdList = new List<string>();
        List<string> ugyiratIdRegiSzerelveList = new List<string>();

        if (ParentUgyiratokTable == null)
        {
            if (!String.IsNullOrEmpty(parentUgyiratRegi.RegirendszerIktatoszam))
            {
                Logger.Debug(String.Format("ParentUgyiratRegi RegiRendszerIktatoszam: {0}", parentUgyiratRegi.RegirendszerIktatoszam));
                ugyiratIdRegiSzerelveList.Add(execParam.Record_Id);
            }
            result = this.GetAllSzereltByParent(execParam);
        }
        else
        {
            foreach (DataRow row in ParentUgyiratokTable.Rows)
            {
                string id = row["Id"].ToString();
                parentUgyiratIdList.Add(id);
                string regiRendszerIktatoszam = row["RegiRendszerIktatoszam"].ToString();
                if (!String.IsNullOrEmpty(regiRendszerIktatoszam))
                    ugyiratIdRegiSzerelveList.Add(id);
            }

            Logger.Debug(String.Format("ParentUgyiratIdList.Count: {0}", parentUgyiratIdList.Count));
            result = this.GetAllSzereltByParent(execParam, parentUgyiratIdList.ToArray());
        }


        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error(String.Format("GetAllSzereltByParent hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
            throw new ResultException(result);
        }

        //nincs kapcsolódó szerelt ügyirat
        if (result.GetCount == 0)
        {
            Logger.Debug("Nincs szerelt ugyirat");
        }
        else
        {
            ExecParam xpm = execParam.Clone();
            foreach (DataRow ugyiratRow in result.Ds.Tables[0].Rows)
            {
                string RegirendszerIktatoszam = ugyiratRow["RegirendszerIktatoszam"].ToString();
                if (!String.IsNullOrEmpty(RegirendszerIktatoszam))
                    ugyiratIdRegiSzerelveList.Add(ugyiratRow["Id"].ToString());

                if (Mode == SzereltUgyiratUpdateMode.All || Mode == SzereltUgyiratUpdateMode.JustEdok)
                {
                    EREC_UgyUgyiratok szereltUgyirat = new EREC_UgyUgyiratok();
                    Utility.LoadBusinessDocumentFromDataRow(szereltUgyirat, ugyiratRow);
                    result = UpdateSzereltUgyirat(xpm, parentUgyirat, szereltUgyirat);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        Logger.Error("UpdateSzereltUgyirat hiba", xpm, result);
                        throw new ResultException(result);
                    }
                }
            }
        }

        if (Mode == SzereltUgyiratUpdateMode.All || Mode == SzereltUgyiratUpdateMode.JustRegi)
        {
            if (ugyiratIdRegiSzerelveList.Count > 0)
            {
                Logger.Debug(String.Format("UgyiratIdList.Count: {0}", ugyiratIdRegiSzerelveList.Count));
                ExecParam xpmUpdateRegiSzereltUgyiratok = execParam.Clone();
                result = this.UpdateRegiSzereltUgyiratok(xpmUpdateRegiSzereltUgyiratok, ugyiratIdRegiSzerelveList.ToArray(), parentUgyiratRegi, parentUgyirat);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    Logger.Error("UpdateRegiSzereltUgyiratok hiba", xpmUpdateRegiSzereltUgyiratok, result);
                    throw new ResultException(result);
                }
            }
        }

        log.WsEnd(execParam, result);
        Logger.Debug("UgyiratokService UpdateSzereltUgyiratok end");
        Logger.DebugEnd();
        return result;
    }

    private Result UpdateSzereltUgyiratok(ExecParam execParam, DataTable ParentUgyiratokTable, EREC_UgyUgyiratok parentUgyirat, SzereltUgyiratUpdateMode Mode)
    {
        if (ParentUgyiratokTable.Rows.Count == 0)
        {
            Logger.Error("ParentUgyiratokTable.Rows.Count == 0");
            return new Result();
        }

        EREC_UgyUgyiratok parentUgyiratElotte = new EREC_UgyUgyiratok();
        Utility.LoadBusinessDocumentFromDataRow(parentUgyiratElotte, ParentUgyiratokTable.Rows[0]);

        return UpdateSzereltUgyiratok(execParam, ParentUgyiratokTable, parentUgyiratElotte, parentUgyirat, Mode);
    }

    private Result UpdateSzereltUgyiratok(ExecParam execParam, EREC_UgyUgyiratok parentUgyiratRegi, EREC_UgyUgyiratok parentUgyirat)
    {
        return UpdateSzereltUgyiratok(execParam, null, parentUgyiratRegi, parentUgyirat, SzereltUgyiratUpdateMode.All);
    }

    private Result UpdateSzereltUgyirat(ExecParam execParam, EREC_UgyUgyiratok parentUgyirat, EREC_UgyUgyiratok szereltUgyirat)
    {
        Result result = new Result();

        string ugyiratOrzoElozo = szereltUgyirat.FelhasznaloCsoport_Id_Orzo;
        string ugyiratFelelosElozo = szereltUgyirat.Csoport_Id_Felelos;

        if (MapParentUgyiratToSzerelt(execParam, parentUgyirat, szereltUgyirat))
        {
            ExecParam xpm = execParam.Clone();
            xpm.Record_Id = szereltUgyirat.Id;

            Result result_ugyiratUpdate = sp.Insert(Constants.Update, xpm, szereltUgyirat);


            if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
            {
                throw new ResultException(result_ugyiratUpdate);
            }

            result = result_ugyiratUpdate;

        }

        return result;
    }

    private bool MapParentUgyiratToSzerelt(ExecParam execParam, EREC_UgyUgyiratok parentUgyirat, EREC_UgyUgyiratok szereltUgyirat)
    {
        bool needUpdate = false;
        szereltUgyirat.Updated.SetValueAll(false);
        szereltUgyirat.Base.Updated.SetValueAll(false);
        szereltUgyirat.Base.Updated.Ver = true;

        //19 oszlop figyelve
        List<string> MappedColumns = new List<string>();
        MappedColumns.Add("IrattarbaKuldDatuma");
        MappedColumns.Add("IrattarbaVetelDat");
        MappedColumns.Add("FelhCsoport_Id_IrattariAtvevo");
        MappedColumns.Add("SelejtezesDat");
        MappedColumns.Add("FelhCsoport_Id_Selejtezo");
        MappedColumns.Add("LeveltariAtvevoNeve");
        MappedColumns.Add("FelhCsoport_Id_Felulvizsgalo");
        MappedColumns.Add("FelulvizsgalatDat");
        MappedColumns.Add("IrattariHely");
        MappedColumns.Add("Csoport_Id_Felelos");
        MappedColumns.Add("FelhasznaloCsoport_Id_Orzo");
        MappedColumns.Add("Csoport_Id_Cimzett");
        //MappedColumns.Add("MegorzesiIdoVege");
        MappedColumns.Add("Csoport_Id_Felelos_Elozo");
        MappedColumns.Add("KolcsonKikerDat");
        MappedColumns.Add("KolcsonKiadDat");
        MappedColumns.Add("Kolcsonhatarido");
        MappedColumns.Add("Kovetkezo_Orzo_Id");
        MappedColumns.Add("Kovetkezo_Felelos_Id");

        Type ugyiratType = parentUgyirat.GetType();
        Type updateType = parentUgyirat.Updated.GetType();
        Type ugyiratTyped = parentUgyirat.Typed.GetType();

        foreach (string propertyName in MappedColumns)
        {
            PropertyInfo pi = ugyiratType.GetProperty(propertyName);
            PropertyInfo piUpdated = updateType.GetProperty(propertyName);
            PropertyInfo piTyped = ugyiratTyped.GetProperty(propertyName);
            if (pi != null && piUpdated != null)
            {
                object parentValue = piTyped.GetValue(parentUgyirat.Typed, null);
                object szereltValue = piTyped.GetValue(szereltUgyirat.Typed, null);
                bool parentUpdated = Convert.ToBoolean(piUpdated.GetValue(parentUgyirat.Updated, null));
                bool szereltUpdated = Convert.ToBoolean(piUpdated.GetValue(szereltUgyirat.Updated, null));


                if (parentUpdated && parentValue != szereltValue)
                {
                    needUpdate = true;
                    piTyped.SetValue(szereltUgyirat.Typed, parentValue, null);
                    piUpdated.SetValue(szereltUgyirat.Updated, true, null);
                }
            }
        }

        //Állapot kezelése
        if (parentUgyirat.Updated.Allapot && parentUgyirat.Allapot != szereltUgyirat.TovabbitasAlattAllapot)
        {
            szereltUgyirat.TovabbitasAlattAllapot = parentUgyirat.Allapot;
            szereltUgyirat.Updated.TovabbitasAlattAllapot = true;
            needUpdate = true;
        }

        //Megõrzési idõ kezelése
        if (parentUgyirat.Updated.MegorzesiIdoVege && parentUgyirat.MegorzesiIdoVege != szereltUgyirat.MegorzesiIdoVege)
        {
            Result resSetMegorzesiIdo = this.SetMegorzesiIdoFromIrattariTetel(execParam, szereltUgyirat, szereltUgyirat.IraIrattariTetel_Id);

            if (!String.IsNullOrEmpty(resSetMegorzesiIdo.ErrorCode))
            {
                throw new ResultException(resSetMegorzesiIdo);
            }

            if (szereltUgyirat.Updated.MegorzesiIdoVege)
            {
                needUpdate = true;
            }
        }

        return needUpdate;
    }

    ///<summary>
    ///ExecParam.RecordId-ban megadott ügyirathoz lekéri a szerelt ügyiratokat
    ///</summary>
    ///<param name="execParam"></param>
    ///<returns></returns>
    public Result GetAllSzereltByParent(ExecParam execParam)
    {
        if (String.IsNullOrEmpty(execParam.Record_Id))
            return new Result();

        return GetAllSzereltByParent(execParam, new string[1] { execParam.Record_Id });
    }

    /// <summary>
    /// A ParentIdArray-ban megadott ügyiratokhoz szerelt ügyiratok lekérése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ParentIdArray"></param>
    /// <returns></returns>
    public Result GetAllSzereltByParent(ExecParam execParam, string[] ParentIdList)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        result = sp.GetAllSzereltByParent(execParam, ParentIdList);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        log.WsEnd(execParam, result);

        return result;
    }

    ///<summary>
    ///ExecParam.RecordId-ban megadott ügyirathoz lekéri az összes szerelt ügyiratokat, ami hozzá vagy bármely szülõjéhez van szerelve
    ///</summary>
    ///<param name="execParam"></param>
    ///<returns></returns>
    [WebMethod()]
    public Result GetAllSzereltByNode(ExecParam execParam)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllSzereltByNode(execParam);

            //if (!String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    throw new ResultException(result);
            //}

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);

        return result;
    }


    ///<summary>
    ///ExecParam.RecordId-ban megadott ügyirathoz lekéri az elõkészített szerelt ügyiratokat (mind, vagy csak a hierarchiában elsõket)
    ///</summary>
    ///<param name="execParam"></param>
    ///<param name="bOnlyFirstInHierarchy">Ha értéke true, megállunk a hierarchiában elsõ elõkészített szerelésnél, nem megyünk tovább a láncban</param>
    ///<returns></returns>
    public Result GetAllElokeszitettSzereltByNode(ExecParam execParam, bool bOnlyFirstInHierarchy)
    {
        if (String.IsNullOrEmpty(execParam.Record_Id))
            return new Result();

        return GetAllElokeszitettSzereltByNode(execParam, new string[1] { execParam.Record_Id }, bOnlyFirstInHierarchy);
    }

    /// <summary>
    /// A NodeIdArray-ban megadott ügyiratokhoz elõkészített szerelt ügyiratok lekérése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="NodeIdArray"></param>
    /// <returns></returns>
    public Result GetAllElokeszitettSzereltByNode(ExecParam execParam, string[] NodeIdArray)
    {
        return GetAllElokeszitettSzereltByNode(execParam, NodeIdArray, false);
    }

    /// <summary>
    /// A NodeIdArray-ban megadott ügyiratokhoz elõkészített szerelt ügyiratok lekérése (mind, vagy csak a hierarchiában elsõket)
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="NodeIdArray"></param>
    ///<param name="bOnlyFirstInHierarchy">Ha értéke true, megállunk a hierarchiában elsõ elõkészített szerelésnél, nem megyünk tovább a láncban</param>
    /// <returns></returns>
    public Result GetAllElokeszitettSzereltByNode(ExecParam execParam, string[] NodeIdArray, bool bOnlyFirstInHierarchy)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        result = sp.GetAllElokeszitettSzereltByNode(execParam, NodeIdArray, bOnlyFirstInHierarchy);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        log.WsEnd(execParam, result);

        return result;
    }
    #endregion

    /// <summary>
    /// Ügyirattérképhez szükséges adatok lekérése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="nodeType"></param>
    /// <param name="nodeId"></param>
    /// <param name="utoIratHierarchia"></param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(IratHierarchia))]
    [System.Xml.Serialization.XmlInclude(typeof(IratPeldanyHierarchia))]
    public Result GetAllUgyiratTerkepDataSets(ExecParam execParam, int nodeType, string nodeId, bool utoIratHierarchia)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Info("Ügyirattérkép adatainak lekérése", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(nodeId)
            || (nodeType != Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.Ugyirat
                && nodeType != Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.UgyiratDarab
                && nodeType != Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.Irat
                && nodeType != Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.IratPeldany
                && nodeType != Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.Kuldemeny))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52630);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            string ugyiratId = "";
            string ugyiratDarabId = "";
            string iratId = "";
            string iratPeldanyId = "";

            EREC_UgyUgyiratok ugyiratObj = null;

            #region OrderBy
            const string iratOrderBy = "EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC";

            #endregion

            #region StopWatch
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            #endregion StopWatch

            #region Hierarchia lekérése

            switch (nodeType)
            {
                case Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.IratPeldany:
                    {
                        EREC_PldIratPeldanyokService service_Pld = new EREC_PldIratPeldanyokService(this.dataContext);
                        ExecParam execParam_pldGet = execParam.Clone();
                        execParam_pldGet.Record_Id = nodeId;

                        Result result_pldHier = service_Pld.GetIratPeldanyHierarchia(execParam_pldGet);
                        if (!String.IsNullOrEmpty(result_pldHier.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_pldHier);
                        }

                        IratPeldanyHierarchia iratPeldanyHier = (IratPeldanyHierarchia)result_pldHier.Record;
                        iratPeldanyId = iratPeldanyHier.IratPeldanyObj.Id;
                        iratId = iratPeldanyHier.IratObj.Id;
                        ugyiratDarabId = iratPeldanyHier.UgyiratDarabObj.Id;
                        ugyiratId = iratPeldanyHier.UgyiratObj.Id;

                        ugyiratObj = iratPeldanyHier.UgyiratObj;

                        result.Record = iratPeldanyHier;

                        break;
                    }
                case Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.Irat:
                    {
                        EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
                        ExecParam execParam_iratGet = execParam.Clone();
                        execParam_iratGet.Record_Id = nodeId;

                        Result result_iratHier = service_iratok.GetIratHierarchia(execParam_iratGet);
                        if (!String.IsNullOrEmpty(result_iratHier.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_iratHier);
                        }

                        IratHierarchia iratHierarchia = (IratHierarchia)result_iratHier.Record;
                        iratId = iratHierarchia.IratObj.Id;
                        ugyiratDarabId = iratHierarchia.UgyiratDarabObj.Id;
                        ugyiratId = iratHierarchia.UgyiratObj.Id;

                        ugyiratObj = iratHierarchia.UgyiratObj;

                        result.Record = iratHierarchia;

                        break;
                    }
                case Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.UgyiratDarab:
                    {
                        EREC_UgyUgyiratdarabokService service_uiDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);
                        ExecParam execParam_uiDarab = execParam.Clone();
                        execParam_uiDarab.Record_Id = nodeId;

                        Result result_uiDarabGet = service_uiDarabok.Get(execParam_uiDarab);
                        if (!String.IsNullOrEmpty(result_uiDarabGet.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_uiDarabGet);
                        }
                        EREC_UgyUgyiratdarabok ugyiratDarab = (EREC_UgyUgyiratdarabok)result_uiDarabGet.Record;

                        ugyiratDarabId = ugyiratDarab.Id;
                        ugyiratId = ugyiratDarab.UgyUgyirat_Id;

                        result.Record = ugyiratDarab;

                        break;
                    }
                case Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.Ugyirat:
                    {
                        ugyiratId = nodeId;

                        break;
                    }
            }

            #endregion

            result.Ds = new DataSet();

            #region Ügyirat és szerelt ügyiratok lekérése

            // Szülõ id lista a kereséshez:
            var szuloIdLista = new List<string> { ugyiratId };

            if (utoIratHierarchia)
            {
                // Elõirat hierarchia lekérése:
                if (ugyiratObj == null
                    || (ugyiratObj != null && !String.IsNullOrEmpty(ugyiratObj.UgyUgyirat_Id_Szulo)))
                {
                    ExecParam execParam_szuloHier = execParam.Clone();
                    execParam_szuloHier.Record_Id = ugyiratId;
                    Result result_szuloHier = this.GetSzuloHiearchy(execParam_szuloHier);
                    if (!String.IsNullOrEmpty(result_szuloHier.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_szuloHier);
                    }

                    // Id-k kigyûjtése:
                    foreach (DataRow row in result_szuloHier.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        szuloIdLista.Add(id);
                    }
                }
            }

            EREC_UgyUgyiratokSearch search_ugyiratok = new EREC_UgyUgyiratokSearch();

            // hozni kell azokat is, ahol õ a szülõügyirat (elõiratok) (ebbe az ügyiratba szerelt ügyiratok)
            // illetve azt, amibe õt beleszerelték

            search_ugyiratok.Id.In(szuloIdLista);
            search_ugyiratok.Id.OrGroup("468");

            search_ugyiratok.UgyUgyirat_Id_Szulo.In(szuloIdLista);
            search_ugyiratok.UgyUgyirat_Id_Szulo.OrGroup("468");

            ExecParam execParam_ugyiratokGetAll = execParam.Clone();

            Result result_ugyiratGetAll = this.GetAllWithExtension(execParam_ugyiratokGetAll, search_ugyiratok);
            if (!String.IsNullOrEmpty(result_ugyiratGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGetAll);
            }

            //result.Ds.Tables.Add(result_ugyiratGetAll.Ds.Tables[0]);
            DataTable table_ugyirat = result_ugyiratGetAll.Ds.Tables[0].Copy();
            table_ugyirat.TableName = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;

            result.Ds.Tables.Add(table_ugyirat);

            #region Régi adatok lekérése
            // az összes utóirathoz tartozó régi adatok le kell kérni

            Logger.Debug("Regi szerelt ugyiratok lekerese start");

            ExecParam xpmGetAllRegiSzerelt = execParam.Clone();
            //xpmGetAllRegiSzerelt.Record_Id = ugyiratId;

            //string regiRendszerIktatoszam = row["RegirendszerIktatoszamEredeti"].ToString();

            Result resGetAllRegiszerelet = GetAllRegiSzereltAdat(xpmGetAllRegiSzerelt, szuloIdLista);

            if (!String.IsNullOrEmpty(resGetAllRegiszerelet.ErrorCode))
            {
                Logger.Error(String.Format("Regi szerelt ugyiratok lekerese hiba: {0},{1}", resGetAllRegiszerelet.ErrorCode, resGetAllRegiszerelet.ErrorMessage));
                throw new ResultException(resGetAllRegiszerelet);
            }

            DataTable table_regi = resGetAllRegiszerelet.Ds.Tables[0].Copy();
            table_regi.TableName = Contentum.eUtility.Constants.TableNames.MIG_Foszam;

            result.Ds.Tables.Add(table_regi);

            Logger.Debug("Regi szerelt ugyiratok lekerese end");

            #endregion

            #endregion

            #region ÜgyiratDarabok lekérése

            EREC_UgyUgyiratdarabokService service_ugyiratDarabok = new EREC_UgyUgyiratdarabokService(this.dataContext);

            EREC_UgyUgyiratdarabokSearch search_ugyiratDarabGetAll = new EREC_UgyUgyiratdarabokSearch();

            // Az összes utóirathoz tartozó ügyiratdarabot is le kell kérni:
            search_ugyiratDarabGetAll.UgyUgyirat_Id.In(szuloIdLista);

            ExecParam execParam_ugyiratDarabGetAll = execParam.Clone();

            Result result_ugyiratDarabGetAll = service_ugyiratDarabok.GetAllWithExtension(execParam_ugyiratDarabGetAll, search_ugyiratDarabGetAll);
            if (!String.IsNullOrEmpty(result_ugyiratDarabGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratDarabGetAll);
            }

            //result.Ds.Tables.Add(result_ugyiratDarabGetAll.Ds.Tables[0]);
            DataTable table_ugyiratDarab = result_ugyiratDarabGetAll.Ds.Tables[0].Copy();
            table_ugyiratDarab.TableName = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratdarabok;
            result.Ds.Tables.Add(table_ugyiratDarab);

            #endregion

            #region UgyiratKuldemeny kapcsolaton keresztüli küldemények lekérése

            if (szuloIdLista.Count > 0)
            {
                Result result_kuld_ugyiratkapcsolt = GetUgyiratKapcsoltKuldemenyei(execParam.Clone(), szuloIdLista);

                if (result_kuld_ugyiratkapcsolt.IsError)
                {
                    throw new ResultException(result_kuld_ugyiratkapcsolt);
                }

                result.Ds.Tables.Add(result_kuld_ugyiratkapcsolt.Ds.Tables[0].Copy());
            }
            #endregion UgyiratKuldemeny kapcsolaton keresztüli küldemények lekérése

            #region Iratok lekérése

            // ha nodeType ügyirat (vagy ügyiratdarab) volt, akkor a default ügyiratdarabo(ka)t kell kinyitni,
            // egyébként pedig a hierarchiában szereplõt:

            #region Default ügyiratdarabok lekérése

            List<string> ugyiratDarabList = new List<string>();

            //if (String.IsNullOrEmpty(ugyiratDarabId))
            //{
            // megkeressük a default ügyiratdarabot

            foreach (DataRow row in result_ugyiratDarabGetAll.Ds.Tables[0].Rows)
            {
                string row_id = row["Id"].ToString();
                string row_UgyUgyiratokId = row["UgyUgyirat_Id"].ToString();
                string row_eljarasiszakasz = row["EljarasiSzakasz"].ToString();

                // CR#1080: Default ügyiratdarab: ahol eljárási szakasz Osztatlan
                if (row_UgyUgyiratokId == ugyiratId
                    && (row_eljarasiszakasz == KodTarak.ELJARASI_SZAKASZ.Osztatlan || String.IsNullOrEmpty(row_eljarasiszakasz)))
                {
                    // default ügyiratdarab:
                    if (ugyiratDarabList.Contains(row_id) == false)
                    {
                        ugyiratDarabList.Add(row_id);
                    }

                    //ugyiratDarabId = id;
                    //break;
                }
            }
            //}
            #endregion

            DataTable table_irat_jogosult = null;
            if (!String.IsNullOrEmpty(ugyiratDarabId) || ugyiratDarabList.Count > 0)
            {
                watch.Reset();
                watch.Start();
                EREC_IraIratokService service_iratok = new EREC_IraIratokService(this.dataContext);
                EREC_IraIratokSearch search_iratokGetAll = new EREC_IraIratokSearch();

                // default ügyiratdarabokhoz a hierarchiában szereplõ ügyiratdarab hozzáadása:
                if (!String.IsNullOrEmpty(ugyiratDarabId) && ugyiratDarabList.Contains(ugyiratDarabId) == false)
                {
                    ugyiratDarabList.Add(ugyiratDarabId);
                }

                #region Keresési feltétel összeállítása a listából

                //System.Text.StringBuilder sb_uiDarabList = new System.Text.StringBuilder();
                //foreach (string uiDarabId in ugyiratDarabList)
                //{
                //    if (sb_uiDarabList.Length == 0)
                //    {
                //        sb_uiDarabList.Append("'" + uiDarabId + "'");
                //    }
                //    else
                //    {
                //        sb_uiDarabList.Append(",'" + uiDarabId + "'");
                //    }
                //}
                #endregion



                //search_iratokGetAll.UgyUgyIratDarab_Id.Value = ugyiratDarabId;
                //search_iratokGetAll.UgyUgyIratDarab_Id.Operator = Query.Operators.equals;
                search_iratokGetAll.UgyUgyIratDarab_Id.In(ugyiratDarabList.ToArray());//sb_uiDarabList.ToString();
                search_iratokGetAll.OrderBy = iratOrderBy;

                ExecParam execParam_iratGetAll = execParam.Clone();
                search_iratokGetAll.IsUgyiratTerkepQuery = true;
                Result result_iratGetAll = service_iratok.GetAllWithExtension(execParam_iratGetAll, search_iratokGetAll);
                if (!String.IsNullOrEmpty(result_iratGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_iratGetAll);
                }

                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Iratok lekérése: {0}", watch.Elapsed));


                //result.Ds.Tables.Add(result_iratGetAll.Ds.Tables[0]);
                DataTable table_iratok = result_iratGetAll.Ds.Tables[0].Copy();
                table_iratok.TableName = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;

                #region Iratok lekérése jogosultságvizsgálathoz
                watch.Reset();
                watch.Start();
                Result result_irat_jogosult = service_iratok.GetAllWithExtensionAndJogosultak(execParam.Clone(), search_iratokGetAll, true);
                if (result_irat_jogosult.IsError)
                {
                    // hiba:
                    throw new ResultException(result_irat_jogosult);
                }
                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Iratok lekérése jogosultságszûréshez: {0}", watch.Elapsed));

                watch.Reset();
                watch.Start();
                table_iratok.Columns.Add("CsatolmanyJogosult", typeof(bool));

                table_irat_jogosult = result_irat_jogosult.Ds.Tables[0];
                table_irat_jogosult.PrimaryKey = new DataColumn[] { table_irat_jogosult.Columns["Id"] };

                foreach (DataRow row in table_iratok.Rows)
                {
                    string id = row["Id"].ToString();
                    DataRow row_jogosult = result_irat_jogosult.Ds.Tables[0].Rows.Find(id);
                    row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);
                }

                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Iratok jogosultság beállítása: {0}", watch.Elapsed));
                #endregion Iratok lekérése jogosultságvizsgálathoz

                result.Ds.Tables.Add(table_iratok);
            }
            else
            {
                DataTable table_iratok = new DataTable();
                table_iratok.TableName = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;
                // üres tábla hozzáadása az irat szintre:
                result.Ds.Tables.Add(table_iratok);
            }

            #endregion

            #region IratPéldányok lekérése

            if (nodeType == Contentum.eUtility.Constants.UgyiratTerkepNodeTypes.IratPeldany
                && !String.IsNullOrEmpty(iratId))
            {
                EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);

                EREC_PldIratPeldanyokSearch search_pld = new EREC_PldIratPeldanyokSearch();
                search_pld.IraIrat_Id.Filter(iratId);

                ExecParam execParam_pldGetAll = execParam.Clone();

                Result result_pldGetAll = service_pld.GetAllWithExtension(execParam_pldGetAll, search_pld);
                if (!String.IsNullOrEmpty(result_pldGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_pldGetAll);
                }

                //result.Ds.Tables.Add(result_pldGetAll.Ds.Tables[0]);
                DataTable table_pld = result_pldGetAll.Ds.Tables[0].Copy();
                table_pld.TableName = Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok;

                result.Ds.Tables.Add(table_pld);


                #region Iratpéldányhoz tartozó küldemények lekérése
                // nekrisz CR 2961

                EREC_Kuldemeny_IratPeldanyaiService service_kuldemenyIratPld = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);

                EREC_Kuldemeny_IratPeldanyaiSearch search_kuldemenyIratPld = new EREC_Kuldemeny_IratPeldanyaiSearch();

                search_kuldemenyIratPld.Peldany_Id.Filter(table_pld.Rows[0]["Id"].ToString());

                ExecParam execParam_kuldGetAll = execParam.Clone();

                Result result_kuldemenyekByIratPld = service_kuldemenyIratPld.GetAllWithExtension(execParam_kuldGetAll, search_kuldemenyIratPld);

                if (!String.IsNullOrEmpty(result_kuldemenyekByIratPld.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kuldemenyekByIratPld);
                }


                DataTable table_kuldIratPld = result_kuldemenyekByIratPld.Ds.Tables[0].Copy();
                table_kuldIratPld.TableName = Contentum.eUtility.Constants.TableNames.EREC_Kuldemeny_IratPeldanyai;
                result.Ds.Tables.Add(table_kuldIratPld);
                //#region Küldemények jogosultság beállítása iratból
                //// küldemény beállítás iratból
                //watch.Reset();
                //watch.Start();
                //table_kuld.Columns.Add("CsatolmanyJogosult", typeof(bool));
                //DataRow row_jogosult = table_irat_jogosult.Rows.Find(iratId);
                //foreach (DataRow row in table_kuld.Rows)
                //{
                //    row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);
                //}

                //watch.Stop();
                //System.Diagnostics.Debug.WriteLine(String.Format("Küldemények jogosultság beállítása: {0}", watch.Elapsed));
                //#endregion Küldemények jogosultság beállítása iratból

                //result.Ds.Tables.Add(table_kuld);

                #endregion

            }
            #endregion

            #region Küldemény lekérése (ha szükséges)

            // ha a hierarchiában szereplõ irat bejövõ keletkezésû, tartozik hozzá küldemény:

            bool bejovoIrat = false;
            string kuldemenyId = "";

            // irat megkeresése a lekért datasetben:
            //foreach (DataRow row in result.Ds.Tables[Contentum.eUtility.Constants.TableNames.EREC_IraIratok].Rows)
            //{
            //    string id = row["Id"].ToString();
            //    string postazasIranya = row["PostazasIranya"].ToString();
            //    kuldemenyId = row["KuldKuldemenyek_Id"].ToString();

            //    if (id == iratId)
            //    {
            //        if (postazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            //        {
            //            bejovoIrat = true;
            //        }
            //    }
            //}

            DataRow[] rows_irat_kuld = result.Ds.Tables[Contentum.eUtility.Constants.TableNames.EREC_IraIratok].Select(String.Format("Id='{0}'", iratId));
            if (rows_irat_kuld.Length == 1)
            {
                string postazasIranya = rows_irat_kuld[0]["PostazasIranya"].ToString();
                kuldemenyId = rows_irat_kuld[0]["KuldKuldemenyek_Id"].ToString();

                if (postazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                {
                    bejovoIrat = true;
                }
            }


            if (bejovoIrat && !String.IsNullOrEmpty(kuldemenyId))
            {
                watch.Reset();
                watch.Start();
                EREC_KuldKuldemenyekService service_kuldemeny = new EREC_KuldKuldemenyekService(this.dataContext);

                EREC_KuldKuldemenyekSearch search_kuldemeny = new EREC_KuldKuldemenyekSearch();
                search_kuldemeny.Id.Filter(kuldemenyId);

                ExecParam execParam_kuldGetAll = execParam.Clone();

                Result result_kuldGetAll = service_kuldemeny.GetAllWithExtension(execParam_kuldGetAll, search_kuldemeny);
                if (!String.IsNullOrEmpty(result_kuldGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kuldGetAll);
                }
                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Küldemények lekérése: {0}", watch.Elapsed));

                //result.Ds.Tables.Add(result_kuldGetAll.Ds.Tables[0]);
                DataTable table_kuld = result_kuldGetAll.Ds.Tables[0].Copy();
                table_kuld.TableName = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;

                #region Küldemények jogosultság beállítása iratból
                // küldemény beállítás iratból
                watch.Reset();
                watch.Start();
                table_kuld.Columns.Add("CsatolmanyJogosult", typeof(bool));
                DataRow row_jogosult = table_irat_jogosult.Rows.Find(iratId);
                foreach (DataRow row in table_kuld.Rows)
                {
                    row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);
                }

                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Küldemények jogosultság beállítása: {0}", watch.Elapsed));
                #endregion Küldemények jogosultság beállítása iratból

                result.Ds.Tables.Add(table_kuld);
            }

            #region IratKuldemeny kapcsolaton keresztüli küldemények lekérése

            if (!String.IsNullOrEmpty(iratId))
            {
                Result result_kuld_kapcsolt = GetIratKapcsoltKuldemenyei(execParam.Clone(), iratId, table_irat_jogosult);

                if (result_kuld_kapcsolt.IsError)
                {
                    throw new ResultException(result_kuld_kapcsolt);
                }

                result.Ds.Tables.Add(result_kuld_kapcsolt.Ds.Tables[0].Copy());
            }
            #endregion IratKuldemeny kapcsolaton keresztüli küldemények lekérése

            #endregion

            // Ügyirat csatolmány megtekintési jogosultság vizsgálat, csak ha pontosan 1 csatolmány van 
            #region Ügyiratok csatolmány jogosultság beállítása
            watch.Reset();
            watch.Start();

            if (table_ugyirat != null)
            {
                table_ugyirat.Columns.Add("CsatolmanyJogosult", typeof(bool));

                if (result.Ds.Tables[Contentum.eUtility.Constants.TableNames.EREC_IraIratok] != null)
                {
                    foreach (DataRow row in table_ugyirat.Rows)
                    {
                        row["CsatolmanyJogosult"] = true;
                        string id = row["Id"].ToString();
                        string csatolmanyCount = row["CsatolmanyCount"].ToString();
                        if (csatolmanyCount == "1")
                        {
                            DataRow[] rows_irat = result.Ds.Tables[Contentum.eUtility.Constants.TableNames.EREC_IraIratok].Select(String.Format("Ugyirat_Id='{0}' and CsatolmanyCount>0", id));

                            if (rows_irat.Length == 1)
                            {
                                row["CsatolmanyJogosult"] = rows_irat[0]["CsatolmanyJogosult"];
                            }
                        }
                    }
                }
            }
            watch.Stop();
            System.Diagnostics.Debug.WriteLine(String.Format("Ügyiratok jogosultság beállítása: {0}", watch.Elapsed));
            #endregion Ügyiratok csatolmány jogosultság beállítása


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyirattérképhez szükséges adatok lekérése
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetAllUgyiratTerkepDataSetsForExpandAll(ExecParam execParam, string[] arrayUgyiratIds, string[] arrayUgyiratDarabIds, string[] arrayIratIds)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Info("Ügyirattérkép adatainak lekérése teljes mélységben", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52630);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            DataTable table_ugyirat = null;
            DataTable table_regi = null;
            DataTable table_ugyiratdarabok = null;
            DataTable table_iratok = null;
            DataTable table_pld = null;
            DataTable table_kuld = null;
            DataTable table_kuld_kapcsolt = null;
            DataTable table_kuld_ugyiratkapcsolt = null;
            DataTable table_kuldIratPld = null;


            #region OrderBy
            const string iratOrderBy = "EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC";

            #endregion

            // az Id-k listája a lekérdezések folyamán bõvülhet
            var UgyiratIds = new List<string>(arrayUgyiratIds ?? new string[0]);
            string UgyiratDarabIds = String.Empty;
            string IratIds = String.Empty;

            if (arrayUgyiratDarabIds != null && arrayUgyiratDarabIds.Length > 0)
            {
                UgyiratDarabIds = Search.GetSqlInnerString(arrayUgyiratDarabIds);// "'" + String.Join("','", arrayUgyiratDarabIds) + "'";
            }

            if (arrayIratIds != null && arrayIratIds.Length > 0)
            {
                IratIds = Search.GetSqlInnerString(arrayIratIds);//"'" + String.Join("','", arrayIratIds) + "'";
            }

            #region StopWatch
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            #endregion StopWatch

            #region Ügyirat és elõiratok lekérése
            if (UgyiratIds != null && UgyiratIds.Count > 0)
            {
                ExecParam execParam_ugyirat_szerelt = execParam.Clone();

                Result result_ugyirat_szerelt = this.GetAllSzereltByParent(execParam_ugyirat_szerelt, arrayUgyiratIds);

                if (result_ugyirat_szerelt.IsError)
                {
                    throw new ResultException(result_ugyirat_szerelt);
                }
                List<string> CurrentSzereltIds = new List<string>();

                while (result_ugyirat_szerelt.GetCount > 0)
                {
                    foreach (DataRow row in result_ugyirat_szerelt.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        CurrentSzereltIds.Add(id);
                    }

                    result_ugyirat_szerelt = GetAllSzereltByParent(execParam_ugyirat_szerelt, CurrentSzereltIds.ToArray());
                    if (result_ugyirat_szerelt.IsError)
                    {
                        throw new ResultException(result_ugyirat_szerelt);
                    }

                    UgyiratIds.AddRange(CurrentSzereltIds);
                    CurrentSzereltIds.Clear();
                }

                EREC_UgyUgyiratokSearch search_ugyirat = new EREC_UgyUgyiratokSearch();

                // az eredeti ügyiratok és a beléjük szerelt láncok
                search_ugyirat.Id.In(UgyiratIds);

                Result result_ugyirat = GetAllWithExtension(execParam, search_ugyirat);

                if (result_ugyirat.IsError)
                {
                    // hiba:
                    throw new ResultException(result_ugyirat);
                }

                table_ugyirat = result_ugyirat.Ds.Tables[0].Copy();
                table_ugyirat.TableName = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;

                #region Régi adatok lekérése
                // az összes utóirathoz tartozó régi adatok le kell kérni

                ExecParam execParam_regi = execParam.Clone();

                Result result_regi = GetAllRegiSzereltAdat(execParam_regi, UgyiratIds);

                if (result_regi.IsError)
                {
                    // hiba:
                    throw new ResultException(result_regi);
                }

                table_regi = result_regi.Ds.Tables[0].Copy();
                table_regi.TableName = Contentum.eUtility.Constants.TableNames.MIG_Foszam;
                #endregion

            }
            #endregion Ügyirat és elõiratok lekérése

            #region UgyiratKuldemeny kapcsolaton keresztüli küldemények lekérése

            if (UgyiratIds.Count > 0)
            {
                Result result_kuld_ugyiratkapcsolt = GetUgyiratKapcsoltKuldemenyei(execParam, UgyiratIds);

                if (result_kuld_ugyiratkapcsolt.IsError)
                {
                    throw new ResultException(result_kuld_ugyiratkapcsolt);
                }

                table_kuld_ugyiratkapcsolt = result_kuld_ugyiratkapcsolt.Ds.Tables[0].Copy();
            }
            #endregion UgyiratKuldemeny kapcsolaton keresztüli küldemények lekérése


            #region Ügyiratdarabok lekérése

            if (UgyiratIds.Count > 0 || !String.IsNullOrEmpty(UgyiratDarabIds))
            {
                ExecParam execParam_ugyiratdarab = execParam.Clone();

                EREC_UgyUgyiratdarabokService service_ugyiratdarab = new EREC_UgyUgyiratdarabokService(this.dataContext);
                EREC_UgyUgyiratdarabokSearch search_ugyiratdarab = new EREC_UgyUgyiratdarabokSearch();

                if (UgyiratIds.Count > 0)
                {
                    search_ugyiratdarab.UgyUgyirat_Id.In(UgyiratIds);
                }

                if (!String.IsNullOrEmpty(UgyiratDarabIds))
                {
                    search_ugyiratdarab.Id.Value = UgyiratDarabIds;
                    search_ugyiratdarab.Id.Operator = Query.Operators.inner;
                }

                if (UgyiratIds.Count > 0 && !String.IsNullOrEmpty(UgyiratDarabIds))
                {
                    search_ugyiratdarab.UgyUgyirat_Id.OrGroup("444");
                    search_ugyiratdarab.Id.OrGroup("444");
                }

                Result result_ugyiratdarab = service_ugyiratdarab.GetAllWithExtension(execParam_ugyiratdarab, search_ugyiratdarab);

                if (result_ugyiratdarab.IsError)
                {
                    // hiba:
                    throw new ResultException(result_ugyiratdarab);
                }

                table_ugyiratdarabok = result_ugyiratdarab.Ds.Tables[0].Copy();
                table_ugyiratdarabok.TableName = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratdarabok;

                List<string> lstUgyiratDarabIds = new List<string>();
                foreach (DataRow row in result_ugyiratdarab.Ds.Tables[0].Rows)
                {
                    string id = row["Id"].ToString();

                    lstUgyiratDarabIds.Add(id);

                }
                if (lstUgyiratDarabIds.Count > 0)
                {
                    UgyiratDarabIds = Search.GetSqlInnerString(lstUgyiratDarabIds.ToArray());//"'" + String.Join("','", lstUgyiratDarabIds.ToArray()) + "'";
                }
            }
            #endregion Ügyiratdarabok lekérése


            if (!String.IsNullOrEmpty(IratIds) || !String.IsNullOrEmpty(UgyiratDarabIds))
            {
                watch.Reset();
                watch.Start();
                #region Iratok lekérése
                Dictionary<string, EREC_IraIratok> EREC_IraIratokDictionary = new Dictionary<string, EREC_IraIratok>();

                ExecParam execParam_irat = execParam.Clone();

                EREC_IraIratokService service_irat = new EREC_IraIratokService(this.dataContext);
                EREC_IraIratokSearch search_irat = new EREC_IraIratokSearch();

                if (!String.IsNullOrEmpty(IratIds))
                {
                    search_irat.Id.Value = IratIds;
                    search_irat.Id.Operator = Query.Operators.inner;
                }

                if (!String.IsNullOrEmpty(UgyiratDarabIds))
                {
                    search_irat.UgyUgyIratDarab_Id.Value = UgyiratDarabIds;
                    search_irat.UgyUgyIratDarab_Id.Operator = Query.Operators.inner;
                }

                if (!String.IsNullOrEmpty(IratIds) && !String.IsNullOrEmpty(UgyiratDarabIds))
                {
                    search_irat.Id.OrGroup("333");
                    search_irat.UgyUgyIratDarab_Id.OrGroup("333");
                }

                search_irat.OrderBy = iratOrderBy;
                search_irat.IsUgyiratTerkepQuery = true;

                Result result_irat = service_irat.GetAllWithExtension(execParam_irat, search_irat);

                if (result_irat.IsError)
                {
                    // hiba:
                    throw new ResultException(result_irat);
                }
                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Iratok lekérése: {0}", watch.Elapsed));

                table_iratok = result_irat.Ds.Tables[0].Copy();
                table_iratok.TableName = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;

                DataTable table_irat_jogosult = null;

                #region Iratok lekérése jogosultságvizsgálathoz
                watch.Reset();
                watch.Start();
                Result result_irat_jogosult = service_irat.GetAllWithExtensionAndJogosultak(execParam.Clone(), search_irat, true);
                if (result_irat_jogosult.IsError)
                {
                    // hiba:
                    throw new ResultException(result_irat_jogosult);
                }
                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Iratok lekérése jogosultságszûréshez: {0}", watch.Elapsed));

                watch.Reset();
                watch.Start();
                table_iratok.Columns.Add("CsatolmanyJogosult", typeof(bool));
                table_irat_jogosult = result_irat_jogosult.Ds.Tables[0];
                table_irat_jogosult.PrimaryKey = new DataColumn[] { table_irat_jogosult.Columns["Id"] };

                foreach (DataRow row in table_iratok.Rows)
                {
                    string id = row["Id"].ToString();
                    DataRow row_jogosult = result_irat_jogosult.Ds.Tables[0].Rows.Find(id);
                    row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);
                }

                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Iratok jogosultság beállítása: {0}", watch.Elapsed));
                #endregion Iratok lekérése jogosultságvizsgálathoz

                #endregion Iratok lekérése

                List<string> lstIratIds = new List<string>();

                List<string> lstKuldemenyIds = new List<string>();
                string KuldemenyIds = String.Empty;

                foreach (DataRow row in result_irat.Ds.Tables[0].Rows)
                {
                    string id = row["Id"].ToString();
                    string postazasIranya = row["PostazasIranya"].ToString();

                    if (postazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                    {
                        string kuldemenyId = row["KuldKuldemenyek_Id"].ToString();
                        if (!String.IsNullOrEmpty(kuldemenyId))
                        {
                            lstKuldemenyIds.Add(kuldemenyId);
                        }
                    }

                    lstIratIds.Add(id);
                }

                if (lstIratIds.Count > 0)
                {
                    IratIds = Search.GetSqlInnerString(lstIratIds.ToArray());//"'" + String.Join("','", lstIratIds.ToArray()) + "'";
                }

                #region Iratpéldányok lekérése

                if (!String.IsNullOrEmpty(IratIds))
                {
                    ExecParam execParam_pld = execParam.Clone();

                    EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);
                    EREC_PldIratPeldanyokSearch search_pld = new EREC_PldIratPeldanyokSearch();
                    search_pld.IraIrat_Id.Value = IratIds;
                    search_pld.IraIrat_Id.Operator = Query.Operators.inner;

                    Result result_pld = service_pld.GetAllWithExtension(execParam_pld, search_pld);

                    if (result_pld.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_pld);
                    }

                    table_pld = result_pld.Ds.Tables[0].Copy();
                    table_pld.TableName = Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok;

                    #region Iratpéldányhoz tartozó küldemények lekérése
                    // nekrisz CR 2961

                    EREC_Kuldemeny_IratPeldanyaiService service_kuldemenyIratPld = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);

                    EREC_Kuldemeny_IratPeldanyaiSearch search_kuldemenyIratPld = new EREC_Kuldemeny_IratPeldanyaiSearch();

                    search_kuldemenyIratPld.Peldany_Id.Filter(table_pld.Rows[0]["Id"].ToString());

                    ExecParam execParam_kuldGetAll = execParam.Clone();

                    Result result_kuldemenyekByIratPld = service_kuldemenyIratPld.GetAllWithExtension(execParam_kuldGetAll, search_kuldemenyIratPld);

                    if (!String.IsNullOrEmpty(result_kuldemenyekByIratPld.ErrorCode))
                    {
                        // hiba:
                        throw new ResultException(result_kuldemenyekByIratPld);
                    }


                    table_kuldIratPld = result_kuldemenyekByIratPld.Ds.Tables[0].Copy();
                    table_kuldIratPld.TableName = Contentum.eUtility.Constants.TableNames.EREC_Kuldemeny_IratPeldanyai;

                    #endregion
                }

                #endregion Iratpéldányok lekérése

                #region Küldemények lekérése (ha szükséges)
                if (lstKuldemenyIds.Count > 0)
                {
                    watch.Reset();
                    watch.Start();

                    ExecParam execParam_kuld = execParam.Clone();

                    EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);
                    EREC_KuldKuldemenyekSearch search_kuld = new EREC_KuldKuldemenyekSearch();
                    search_kuld.Id.In(lstKuldemenyIds);

                    Result result_kuld = service_kuld.GetAllWithExtension(execParam_kuld, search_kuld);

                    if (result_kuld.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_kuld);
                    }
                    watch.Stop();
                    System.Diagnostics.Debug.WriteLine(String.Format("Küldemények lekérése: {0}", watch.Elapsed));

                    table_kuld = result_kuld.Ds.Tables[0].Copy();
                    table_kuld.TableName = Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek;

                    #region Küldemények lekérése jogosultságvizsgálathoz
                    // küldemény beállítás iratból
                    watch.Reset();
                    watch.Start();
                    table_kuld.Columns.Add("CsatolmanyJogosult", typeof(bool));

                    foreach (DataRow row in table_kuld.Rows)
                    {

                        string id = row["Id"].ToString();
                        DataRow[] rows_irat_jogosult = table_irat_jogosult.Select(String.Format("KuldKuldemenyek_id='{0}'", id));
                        if (rows_irat_jogosult.Length == 1)
                        {
                            row["CsatolmanyJogosult"] = true;
                        }
                        else
                        {
                            row["CsatolmanyJogosult"] = false;
                        }
                    }


                    watch.Stop();
                    System.Diagnostics.Debug.WriteLine(String.Format("Küldemények jogosultság beállítása: {0}", watch.Elapsed));
                    #endregion Küldemények lekérése jogosultságvizsgálathoz

                }

                #region IratKuldemeny kapcsolaton keresztüli küldemények lekérése

                if (!String.IsNullOrEmpty(IratIds))
                {
                    Result result_kuld_kapcsolt = GetIratKapcsoltKuldemenyei(execParam.Clone(), IratIds, result_irat_jogosult.Ds.Tables[0]);

                    if (result_kuld_kapcsolt.IsError)
                    {
                        throw new ResultException(result_kuld_kapcsolt);
                    }

                    table_kuld_kapcsolt = result_kuld_kapcsolt.Ds.Tables[0].Copy();
                }
                #endregion IratKuldemeny kapcsolaton keresztüli küldemények lekérése

                #endregion Küldemények lekérése (ha szükséges)

            }

            // Ügyirat csatolmány megtekintési jogosultság vizsgálat, csak ha pontosan 1 csatolmány van 
            #region Ügyiratok csatolmány jogosultság beállítása
            watch.Reset();
            watch.Start();

            if (table_ugyirat != null)
            {
                table_ugyirat.Columns.Add("CsatolmanyJogosult", typeof(bool));

                if (table_iratok != null)
                {
                    foreach (DataRow row in table_ugyirat.Rows)
                    {
                        row["CsatolmanyJogosult"] = true;
                        string id = row["Id"].ToString();
                        string csatolmanyCount = row["CsatolmanyCount"].ToString();
                        if (csatolmanyCount == "1")
                        {
                            DataRow[] rows_irat = table_iratok.Select(String.Format("Ugyirat_Id='{0}' and CsatolmanyCount>0", id));

                            if (rows_irat.Length == 1)
                            {
                                row["CsatolmanyJogosult"] = rows_irat[0]["CsatolmanyJogosult"];
                            }
                        }
                    }
                }
            }
            watch.Stop();
            System.Diagnostics.Debug.WriteLine(String.Format("Ügyiratok jogosultság beállítása: {0}", watch.Elapsed));
            #endregion Ügyiratok csatolmány jogosultság beállítása

            result.Ds = new DataSet();

            if (table_ugyirat != null)
            {
                result.Ds.Tables.Add(table_ugyirat);
            }
            if (table_regi != null)
            {
                result.Ds.Tables.Add(table_regi);
            }
            if (table_ugyiratdarabok != null)
            {
                result.Ds.Tables.Add(table_ugyiratdarabok);
            }
            if (table_iratok != null)
            {
                result.Ds.Tables.Add(table_iratok);
            }
            if (table_pld != null)
            {
                result.Ds.Tables.Add(table_pld);
            }
            if (table_kuld != null)
            {
                result.Ds.Tables.Add(table_kuld);
            }
            if (table_kuld_kapcsolt != null)
            {
                result.Ds.Tables.Add(table_kuld_kapcsolt);
            }
            if (table_kuld_ugyiratkapcsolt != null)
            {
                result.Ds.Tables.Add(table_kuld_ugyiratkapcsolt);
            }
            if (table_kuldIratPld != null)
            {
                result.Ds.Tables.Add(table_kuldIratPld);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Irat kapcsolt küldeményeinek lekérése (ügyirattérképhez)
    /// </summary>
    /// <param name="execParam">Naplózáshoz szükséges információk (felhasználó stb.)</param>
    /// <param name="iratIds">A vizsgált iratok azonosítói, aposztrófok között vagy anélkül, vesszõvel elválasztva</param>
    /// <param name="table_irat_jogosult">Azon iratokra vonatkozó adatsorok, melyekhez a felhasználónak joga van. Ha null, a küldeményekre készül jogosultságvizsgálat</param>
    /// <returns>Hiba vagy a kapcsolt küldeményekre vonatkozó adatsorokból álló tábla a Result.Ds-ben</returns>
    [WebMethod()]
    public Result GetIratKapcsoltKuldemenyei(ExecParam execParam, string iratIds, DataTable table_irat_jogosult)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Info("Iratok kapcsolt küldeményeinek lekérése", execParam);

        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52630);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            if (String.IsNullOrEmpty(iratIds))
            {
                throw new ResultException("Nincs megadva irat azonosító! Az irathoz kapcsolt küldemények nem kérdezhetõk le.");
            }
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

            watch.Reset();
            watch.Start();

            // egy vagy több iratra is lehet szûrni
            // ha nem szerepel benne aposztróf (Guid ill. Guid lista), átalakítjuk
            if (!iratIds.Contains("'"))
            {
                iratIds = String.Format("'{0}'", iratIds.Replace(",", "','"));
            }

            EREC_UgyiratObjKapcsolatokService service_kapcsolat = new EREC_UgyiratObjKapcsolatokService(this.dataContext);
            EREC_UgyiratObjKapcsolatokSearch search_kapcsolat = new EREC_UgyiratObjKapcsolatokSearch();
            search_kapcsolat.Obj_Id_Elozmeny.Value = iratIds;
            search_kapcsolat.Obj_Id_Elozmeny.Operator = Query.Operators.inner;

            search_kapcsolat.Obj_Type_Elozmeny.Filter(Contentum.eUtility.Constants.TableNames.EREC_IraIratok);
            search_kapcsolat.Obj_Type_Kapcsolt.Filter(Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek);

            //// ne tegyük fel kétszer...
            //if (!String.IsNullOrEmpty(kuldemenyId))
            //{
            //    search_kapcsolat.Obj_Id_Kapcsolt.Value = kuldemenyId;
            //    search_kapcsolat.Obj_Id_Kapcsolt.Operator = Query.Operators.notequals;
            //}

            search_kapcsolat.KapcsolatTipus.Filter(KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.IratKuldemeny);

            Result result_kapcsolat = service_kapcsolat.GetAll(execParam.Clone(), search_kapcsolat);

            if (result_kapcsolat.IsError)
            {
                throw new ResultException(result_kapcsolat);
            }

            watch.Stop();
            System.Diagnostics.Debug.WriteLine(String.Format("Kapcsolatok lekérése (Irat-Küldemény): {0}", watch.Elapsed));


            if (result_kapcsolat.GetCount > 0)
            {
                Dictionary<string, string> kapcsoltKuldemenyek = new Dictionary<string, string>(result_kapcsolat.GetCount);

                foreach (DataRow row in result_kapcsolat.Ds.Tables[0].Rows)
                {
                    string kapcsoltKuldId = row["Obj_Id_Kapcsolt"].ToString();
                    string kapcsoltKuldIratId = row["Obj_Id_Elozmeny"].ToString();
                    if (!String.IsNullOrEmpty(kapcsoltKuldId) && !kapcsoltKuldemenyek.ContainsKey(kapcsoltKuldId))
                    {
                        kapcsoltKuldemenyek.Add(kapcsoltKuldId, kapcsoltKuldIratId);
                    }
                }

                string[] keysKapcsoltKuldemenyek = new string[kapcsoltKuldemenyek.Count];
                kapcsoltKuldemenyek.Keys.CopyTo(keysKapcsoltKuldemenyek, 0);

                watch.Reset();
                watch.Start();
                EREC_KuldKuldemenyekService service_kuldemeny = new EREC_KuldKuldemenyekService(this.dataContext);

                EREC_KuldKuldemenyekSearch search_kuldemeny = new EREC_KuldKuldemenyekSearch();
                search_kuldemeny.Id.In(keysKapcsoltKuldemenyek);

                Result result_kapcsoltKuldGetAll = service_kuldemeny.GetAllWithExtension(execParam, search_kuldemeny);
                if (!String.IsNullOrEmpty(result_kapcsoltKuldGetAll.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_kapcsoltKuldGetAll);
                }
                watch.Stop();
                System.Diagnostics.Debug.WriteLine(String.Format("Kapcsolt küldemények lekérése: {0}", watch.Elapsed));

                //result.Ds.Tables.Add(result_kuldGetAll.Ds.Tables[0]);
                DataTable table_kuld_kapcsolt = result_kapcsoltKuldGetAll.Ds.Tables[0]; //.Copy();

                table_kuld_kapcsolt.Columns.Add("CsatolmanyJogosult", typeof(bool));
                if (!table_kuld_kapcsolt.Columns.Contains("IraIratok_Id"))
                {
                    table_kuld_kapcsolt.Columns.Add("IraIratok_Id", typeof(string));
                }

                if (table_irat_jogosult != null)
                {
                    #region Kapcsolt küldemények jogosultság beállítása iratból
                    // küldemény beállítás iratból
                    watch.Reset();
                    watch.Start();

                    foreach (DataRow row in table_kuld_kapcsolt.Rows)
                    {
                        string iratId = kapcsoltKuldemenyek[row["Id"].ToString()];
                        DataRow row_jogosult = table_irat_jogosult.Rows.Find(iratId);
                        row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);

                        // irat id beállítása (azonosítás céljából az ügyirattérképen)
                        row["IraIratok_Id"] = iratId; // TODO: elvileg akár több irathoz is lehetne csatolni, itt most csak egyet nézünk...
                    }

                    watch.Stop();
                    System.Diagnostics.Debug.WriteLine(String.Format("Kapcsolt küldemények jogosultság beállítása: {0}", watch.Elapsed));
                    #endregion Kapcsolt küldemények jogosultság beállítása iratból
                }
                else
                {
                    #region Küldemények lekérése jogosultságvizsgálathoz
                    watch.Reset();
                    watch.Start();
                    Result result_kuldemeny_jogosult = service_kuldemeny.GetAllWithExtensionAndJogosultak(execParam, search_kuldemeny, true);
                    if (result_kuldemeny_jogosult.IsError)
                    {
                        // hiba:
                        throw new ResultException(result_kuldemeny_jogosult);
                    }
                    watch.Stop();
                    System.Diagnostics.Debug.WriteLine(String.Format("Küldemények (irathoz kapcsolt) lekérése jogosultságszûréshez: {0}", watch.Elapsed));

                    watch.Reset();
                    watch.Start();

                    DataTable table_kuldemeny_jogosult = result_kuldemeny_jogosult.Ds.Tables[0];
                    table_kuldemeny_jogosult.PrimaryKey = new DataColumn[] { table_kuldemeny_jogosult.Columns["Id"] };

                    foreach (DataRow row in table_kuld_kapcsolt.Rows)
                    {
                        string id = row["Id"].ToString();
                        DataRow row_jogosult = result_kuldemeny_jogosult.Ds.Tables[0].Rows.Find(id);
                        row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);

                        if (kapcsoltKuldemenyek.ContainsKey(id))
                        {
                            row["IraIratok_Id"] = kapcsoltKuldemenyek[id];
                        }
                    }

                    watch.Stop();
                    System.Diagnostics.Debug.WriteLine(String.Format("Küldemények (irathoz kapcsolt) jogosultság beállítása: {0}", watch.Elapsed));
                    #endregion Küldemények lekérése jogosultságvizsgálathoz
                }
                result = result_kapcsoltKuldGetAll;
            }
            else
            {
                // ne legyen üres a DataSet
                result.Ds = new DataSet();
                result.Ds.Tables.Add(new DataTable());
            }

            if (result.Ds.Tables.Count > 0)
            {
                result.Ds.Tables[0].TableName = String.Concat(Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek, "Kapcsolt");
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    private Result GetUgyiratKapcsoltKuldemenyei(ExecParam execParam, List<string> ugyiratIds)
    {
        Result result = new Result();

        //try
        //{
        if (ugyiratIds == null || ugyiratIds.Count == 0)
        {
            throw new ResultException("Nincs megadva ügyirat azonosító! Az ügyirathoz kapcsolt küldemények nem kérdezhetõk le.");
        }
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

        watch.Reset();
        watch.Start();

        // egy vagy több ügyiratra is lehet szûrni

        EREC_UgyiratObjKapcsolatokService service_kapcsolat = new EREC_UgyiratObjKapcsolatokService(this.dataContext);
        EREC_UgyiratObjKapcsolatokSearch search_kapcsolat = new EREC_UgyiratObjKapcsolatokSearch();
        search_kapcsolat.Obj_Id_Elozmeny.In(ugyiratIds);

        search_kapcsolat.Obj_Type_Elozmeny.Filter(Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok);
        search_kapcsolat.Obj_Type_Kapcsolt.Filter(Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek);

        search_kapcsolat.KapcsolatTipus.Filter(KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyiratKuldemeny);

        Result result_kapcsolat = service_kapcsolat.GetAll(execParam, search_kapcsolat);

        if (result_kapcsolat.IsError)
        {
            throw new ResultException(result_kapcsolat);
        }

        watch.Stop();
        System.Diagnostics.Debug.WriteLine(String.Format("Kapcsolatok lekérése (Ügyirat-Küldemény): {0}", watch.Elapsed));

        if (result_kapcsolat.GetCount > 0)
        {
            // az ügyiratok visszakereséséhez
            Dictionary<string, string> kapcsoltKuldemenyIds = new Dictionary<string, string>(result_kapcsolat.GetCount);

            foreach (DataRow row in result_kapcsolat.Ds.Tables[0].Rows)
            {
                string kapcsoltKuldId = row["Obj_Id_Kapcsolt"].ToString();
                string ugyiratIdElozmeny = row["Obj_Id_Elozmeny"].ToString();
                if (!String.IsNullOrEmpty(kapcsoltKuldId) && !kapcsoltKuldemenyIds.ContainsKey(kapcsoltKuldId))
                {
                    kapcsoltKuldemenyIds.Add(kapcsoltKuldId, ugyiratIdElozmeny);
                }
            }

            watch.Reset();
            watch.Start();
            EREC_KuldKuldemenyekService service_kuldemeny = new EREC_KuldKuldemenyekService(this.dataContext);

            string[] keysKapcsoltKuldemenyIds = new string[kapcsoltKuldemenyIds.Count];
            kapcsoltKuldemenyIds.Keys.CopyTo(keysKapcsoltKuldemenyIds, 0);

            EREC_KuldKuldemenyekSearch search_kuldemeny = new EREC_KuldKuldemenyekSearch();
            search_kuldemeny.Id.In(keysKapcsoltKuldemenyIds);

            Result result_kapcsoltKuldGetAll = service_kuldemeny.GetAllWithExtension(execParam, search_kuldemeny);
            if (!String.IsNullOrEmpty(result_kapcsoltKuldGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_kapcsoltKuldGetAll);
            }
            watch.Stop();
            System.Diagnostics.Debug.WriteLine(String.Format("Kapcsolt küldemények lekérése: {0}", watch.Elapsed));

            DataTable table_kuld_ugyiratkapcsolt = result_kapcsoltKuldGetAll.Ds.Tables[0]; //.Copy();

            #region Küldemények lekérése jogosultságvizsgálathoz
            watch.Reset();
            watch.Start();
            Result result_kuldemeny_jogosult = service_kuldemeny.GetAllWithExtensionAndJogosultak(execParam, search_kuldemeny, true);
            if (result_kuldemeny_jogosult.IsError)
            {
                // hiba:
                throw new ResultException(result_kuldemeny_jogosult);
            }
            watch.Stop();
            System.Diagnostics.Debug.WriteLine(String.Format("Küldemények (ügyirathoz kapcsolt) lekérése jogosultságszûréshez: {0}", watch.Elapsed));

            watch.Reset();
            watch.Start();
            table_kuld_ugyiratkapcsolt.Columns.Add("CsatolmanyJogosult", typeof(bool));
            if (!table_kuld_ugyiratkapcsolt.Columns.Contains("UgyUgyiratok_Id"))
            {
                table_kuld_ugyiratkapcsolt.Columns.Add("UgyUgyiratok_Id", typeof(string));
            }

            DataTable table_kuldemeny_jogosult = result_kuldemeny_jogosult.Ds.Tables[0];
            table_kuldemeny_jogosult.PrimaryKey = new DataColumn[] { table_kuldemeny_jogosult.Columns["Id"] };

            foreach (DataRow row in table_kuld_ugyiratkapcsolt.Rows)
            {
                string id = row["Id"].ToString();
                DataRow row_jogosult = result_kuldemeny_jogosult.Ds.Tables[0].Rows.Find(id);
                row["CsatolmanyJogosult"] = (row_jogosult != null ? true : false);

                if (kapcsoltKuldemenyIds.ContainsKey(id))
                {
                    row["UgyUgyiratok_Id"] = kapcsoltKuldemenyIds[id];
                }
            }

            watch.Stop();
            System.Diagnostics.Debug.WriteLine(String.Format("Küldemények (ügyirathoz kapcsolt) jogosultság beállítása: {0}", watch.Elapsed));
            #endregion Küldemények lekérése jogosultságvizsgálathoz

            result = result_kapcsoltKuldGetAll;
        }
        else
        {
            // ne legyen üres a DataSet
            result.Ds = new DataSet();
            result.Ds.Tables.Add(new DataTable());
        }
        //}
        //catch (Exception ex)
        //{
        //    ResultException.GetResultFromException(ex);
        //}

        if (result.Ds.Tables.Count > 0)
        {
            result.Ds.Tables[0].TableName = String.Concat(Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek, "UgyiratKapcsolt");
        }

        return result;
    }


    #region Regi Adat

    /// <summary>
    /// Regi adat szerelése egy másik ügyiratba
    /// </summary>
    /// <param name="execParam">A végrehajtó felhasználót és egyéb általános paramétereket tartalmazza</param>
    /// <param name="cel_Ugyirat_Id">Az ügyirat Id-ja, ahova szerelni akarjuk a szerelendõ ügyiratot</param>
    /// <param name="cel_Ugyirat_Azon">Az ügyirat azonosítója(iktatószám), ahova szerelni akarjuk a szerelendõ ügyiratot</param>
    /// <param name="szerelendo_Ugyirat_Azonosito">A régi(migrált) ügyirat azonosítója, amit szerelni akarunk</param>
    /// <param name="erec_HataridosFeladatok">A mûvelethez esetleg megadott kezelési feljegyzés, ami a 'cél' ügyirat
    /// kezelési feljegyzései közé fog bekerülni. Nem kötelezõ a megadása, lehet 'null' is.</param>
    /// <returns>A mûvelet sikerességét jelzõ Result objektum</returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_HataridosFeladatok))]
    public Result RegiAdatSzereles(ExecParam execParam, string cel_Ugyirat_Id, string cel_Ugyirat_Azon, string szerelendo_Ugyirat_Id, string szerelendo_Ugyirat_Azonosito
           , EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Logger.DebugStart();
        Logger.Debug("EREC_UgyUgyiratokService RegiAdatSzereles method start");
        Logger.Debug("Felhasznalo id: " + execParam.Felhasznalo_Id);
        Logger.Debug("Cel_Ugyirat_Id: " + cel_Ugyirat_Id);
        Logger.Debug("Cel_Ugyirat_Azon: " + cel_Ugyirat_Azon);
        Logger.Debug("Szerelendo_Ugyirat_Id: " + szerelendo_Ugyirat_Id);
        Logger.Debug("Szerelendo_Ugyirat_Azonosito: " + szerelendo_Ugyirat_Azonosito);
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Parameterek ellenorzese start");
        Arguments args = new Arguments();
        args.Add(new Argument("ExecParam.Felhasznalo_Id", execParam.Felhasznalo_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Cel_Ugyirat_Id", cel_Ugyirat_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Cel_Ugyirat_Azon", cel_Ugyirat_Azon, ArgumentTypes.String, false, null));
        args.Add(new Argument("Szerelendo_Ugyirat_Id", szerelendo_Ugyirat_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Szerelendo_Ugyirat_Azonosito", szerelendo_Ugyirat_Azonosito, ArgumentTypes.String, false, null));
        args.ValidateArguments();
        Logger.Debug("Parameterek ellenorzese end");

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();



            // (Cél) Ügyirat lekérése:
            Logger.Debug("Cel ugyirat lekerese start");
            ExecParam execParam_Ugyirat_Cel_Get = execParam.Clone();
            execParam_Ugyirat_Cel_Get.Record_Id = cel_Ugyirat_Id;

            Result result_ugyirat_Cel_Get = this.Get(execParam_Ugyirat_Cel_Get);
            if (!String.IsNullOrEmpty(result_ugyirat_Cel_Get.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("Cel ugyirat lekerese sikertelen: {0},{1}", result_ugyirat_Cel_Get.ErrorCode, result_ugyirat_Cel_Get.ErrorMessage));
                throw new ResultException(result_ugyirat_Cel_Get);
            }
            else
            {
                if (result_ugyirat_Cel_Get.Record == null)
                {
                    // hiba
                    Logger.Error("Cel ugyirat record null");
                    throw new ResultException(52272);
                }

                EREC_UgyUgyiratok erec_UgyUgyirat_Cel = (EREC_UgyUgyiratok)result_ugyirat_Cel_Get.Record;
                Logger.Debug("Cel ugyirat lekerese end");

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz celUgyiratStatusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Cel);
                ExecParam execParam_szerelhetoBeleUgyirat = execParam.Clone();
                ErrorDetails errorDetail = null;

                Logger.Debug("Cel ugyirat ellenorzese start");
                // ha nem KÜK, ellenõrizzük az õrzõt
                bool isSzerelheto = false;
                if (execParam.FelhasznaloSzervezet_Id != KodTarak.SPEC_SZERVEK.GetKozpontiIktato(execParam).Obj_Id)
                {
                    Logger.Debug("RegiAdatSzereles: nem KÜK");
                    // Ellenõrzés, hogy szerelhetõ-e bele ügyirat:
                    isSzerelheto = Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyirat(
                        celUgyiratStatusz, execParam_szerelhetoBeleUgyirat, out errorDetail);
                }
                // ha KÜK, nem ellenõrizzük az õrzõt, csak az állapototkat
                else
                {
                    Logger.Debug("RegiAdatSzereles: KÜK");
                    isSzerelheto = Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyiratAllapotu(celUgyiratStatusz, out errorDetail);
                }

                if (isSzerelheto == false)
                {
                    // Nem lehet az ügyiratba szerelni:
                    Logger.Debug("Cel ugyirat allapota:" + celUgyiratStatusz.Allapot);
                    Logger.Debug("Cel ugyirat tovabbitas alatti allapota:" + celUgyiratStatusz.TovabbitasAlattiAllapot);
                    Logger.Debug("Cel ugyirat felelose:" + celUgyiratStatusz.CsopId_Felelos);
                    Logger.Debug("Cel ugyirat orzoje:" + celUgyiratStatusz.FelhCsopId_Orzo);
                    Logger.Debug("Cel ugyirat ugyintezo:" + celUgyiratStatusz.CsopId_Ugyintezo);
                    Logger.Warn("Az ugyiratba nem lehet szerelni");
                    throw new ResultException(52271, errorDetail);
                }

                //if (Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleUgyirat(
                //    celUgyiratStatusz, execParam_szerelhetoBeleUgyirat, out errorDetail) == false)
                //{
                //    // Nem lehet az ügyiratba szerelni:
                //    Logger.Debug("Cel ugyirat allapota:" + celUgyiratStatusz.Allapot);
                //    Logger.Debug("Cel ugyirat tovabbitas alatti allapota:" + celUgyiratStatusz.TovabbitasAlattiAllapot);
                //    Logger.Debug("Cel ugyirat felelose:" + celUgyiratStatusz.CsopId_Felelos);
                //    Logger.Debug("Cel ugyirat orzoje:" + celUgyiratStatusz.FelhCsopId_Orzo);
                //    Logger.Debug("Cel ugyirat ugyintezo:" + celUgyiratStatusz.CsopId_Ugyintezo);
                //    Logger.Warn("Az ugyiratba nem lehet szerelni");
                //    throw new ResultException(52271, errorDetail);
                //}

                if (!Contentum.eRecord.BaseUtility.Ugyiratok.SzerelhetoBeleRegiUgyirat(celUgyiratStatusz, execParam_szerelhetoBeleUgyirat))
                {
                    Logger.Warn("A ugyiratba nem szerelheto regi ugyirat");
                    throw new ResultException(ErrorCodes.NemSzerelhetoRegiUgyirat);

                }

                Logger.Debug("Cel ugyirat ellenorzese end");

                Logger.Debug("Cel ugyirat update start");
                ExecParam xpmUgyiratUpdate = execParam.Clone();
                xpmUgyiratUpdate.Record_Id = cel_Ugyirat_Id;

                erec_UgyUgyirat_Cel.Updated.SetValueAll(false);
                erec_UgyUgyirat_Cel.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat_Cel.Base.Updated.Ver = true;

                if (!String.IsNullOrEmpty(erec_UgyUgyirat_Cel.RegirendszerIktatoszam))
                {
                    //Ha már van szerelve ügyirat, akkor azt a MIG_Foszam string-el jelezzük a RegirendszerIktatoszam mezõben, ekkor a EREC_UgyiratObjKapcsolatok táblából kell lekérni a szerelt ügyiratok adatait
                    Logger.Debug("A ugyiratba mar szerelve van regi ugyirat");
                    erec_UgyUgyirat_Cel.RegirendszerIktatoszam = Contentum.eUtility.Constants.TableNames.EREC_UgyiratObjKapcsolatok;
                    erec_UgyUgyirat_Cel.Updated.RegirendszerIktatoszam = true;
                }
                else
                {
                    Logger.Debug("A ugyiratba meg nics szerelve van regi ugyirat");
                    erec_UgyUgyirat_Cel.RegirendszerIktatoszam = szerelendo_Ugyirat_Azonosito;
                    erec_UgyUgyirat_Cel.Updated.RegirendszerIktatoszam = true;
                }
                #region Migrált ügyirat get 
                MIG_FoszamService service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
                if (string.IsNullOrEmpty(szerelendo_Ugyirat_Azonosito))
                {
                    Logger.Error("Üres paraméter : szerelendo_Ugyirat_Azonosito");
                    throw new ResultException(ErrorCodes.UgyiratNemTalalhato);
                }
                Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch search = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();
                RegiAdatAzonosito.SetSerchObject(szerelendo_Ugyirat_Azonosito, search);
                ExecParam execParam_foszam = execParam.Clone();
                Result result_foszam = service.GetAll(execParam_foszam, search);
                if (result_foszam.IsError)
                {
                    throw new ResultException(result_foszam);
                }
                if (result_foszam.GetCount == 0)
                {
                    Logger.Error("Szerelendo Foszam Ds üres");
                    throw new ResultException(ErrorCodes.UgyiratNemTalalhato);
                }
                DataRow dr = result_foszam.Ds.Tables[0].Rows[0];
                string regi_tipus = dr["Ugyirat_tipus"].ToString();
                string regi_ugyirat_jelleg = Contentum.eRecord.BaseUtility.KodTarak.GetUjKodFromRegiAdatTipus(regi_tipus);
                #endregion



                #region Ügyirat jelleg (elektronikus, vegyes, papír) módosulás meghatározása
                Logger.Debug(string.Format("[regi ugyirat_jelleg] : {0}" + Environment.NewLine +
                                           "[Cel ugyirat_jelleg] : {1}" + Environment.NewLine +
                                           "[Kombinalt ugyirat jelleg] : {2}", regi_ugyirat_jelleg, erec_UgyUgyirat_Cel.Jelleg, CombineUgyiratJelleg(erec_UgyUgyirat_Cel.Jelleg, regi_ugyirat_jelleg)));
                erec_UgyUgyirat_Cel.Jelleg = CombineUgyiratJelleg(erec_UgyUgyirat_Cel.Jelleg, regi_ugyirat_jelleg);
                erec_UgyUgyirat_Cel.Updated.Jelleg = true;
                #endregion Ügyirat jelleg (elektronikus, vegyes, papír) módosulás meghatározása

                Result result_ugyiratUpdate = this.Update(
                            xpmUgyiratUpdate, erec_UgyUgyirat_Cel);

                if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                {
                    // hiba:
                    Logger.Debug(String.Format("Cel ugyirat update hiba: {0},{1}", result_ugyiratUpdate.ErrorCode, result_ugyiratUpdate.ErrorMessage));
                    throw new ResultException(result_ugyiratUpdate);
                }

                Logger.Debug("Cel ugyirat update end");

                #region EREC_UgyiratObjKapcsolatok insert
                Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt start");

                ExecParam xmpObjKapcsInsert = execParam.Clone();
                EREC_UgyiratObjKapcsolatokService svcObjKapcs = new EREC_UgyiratObjKapcsolatokService(this.dataContext);

                svcObjKapcs.InsertMigraltSzerelt(xmpObjKapcsInsert, cel_Ugyirat_Id, szerelendo_Ugyirat_Id, szerelendo_Ugyirat_Azonosito);

                Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt end");
                #endregion

                #region kezelesi feljegyzes
                if (erec_HataridosFeladatok != null)
                {
                    Logger.Debug("Kezelesi feljegyzes insert start");
                    Logger.Debug("Kezeles tipusa: " + erec_HataridosFeladatok.Altipus);
                    Logger.Debug("Kezeles leiras: " + erec_HataridosFeladatok.Leiras);

                    EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
                    ExecParam execParam_kezFeljInsert = execParam.Clone();

                    // Ügyirathoz kötés
                    erec_HataridosFeladatok.Obj_Id = erec_UgyUgyirat_Cel.Id;
                    erec_HataridosFeladatok.Updated.Obj_Id = true;

                    erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
                    erec_HataridosFeladatok.Updated.Obj_type = true;

                    if (!String.IsNullOrEmpty(erec_UgyUgyirat_Cel.Azonosito))
                    {
                        erec_HataridosFeladatok.Azonosito = erec_UgyUgyirat_Cel.Azonosito;
                        erec_HataridosFeladatok.Updated.Azonosito = true;
                    }

                    Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(
                        execParam_kezFeljInsert, erec_HataridosFeladatok);

                    if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
                    {
                        // hiba:
                        Logger.Debug(String.Format("Kezelesi feljegyzes insert hiba: {0},{1}", result_kezFeljInsert.ErrorCode, result_kezFeljInsert.ErrorMessage));
                        throw new ResultException(result_kezFeljInsert);
                    }

                    Logger.Debug("Kezelesi feljegyzes insert end");
                }
                #endregion

                Logger.Debug("FoszamSzereles start");

                ExecParam xpmFoszamSzereles = execParam.Clone();
                Result resFoszamSzereles = FoszamSzereles(xpmFoszamSzereles, szerelendo_Ugyirat_Azonosito, cel_Ugyirat_Id, cel_Ugyirat_Azon);

                Logger.Debug("FoszamSzereles end");
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, cel_Ugyirat_Id, "EREC_UgyUgyiratok", "UgyiratSzereles").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("EREC_UgyUgyiratokService RegiAdatSzereles method hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("EREC_UgyUgyiratokService RegiAdatSzereles method end");
        Logger.DebugEnd();
        return result;

    }


    /// <summary>
    /// Régi adat szerelésének visszavonása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="szulo_Ugyirat_Id"></param>
    /// <param name="visszavonando_Ugyirat_Azonosito"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result RegiAdatSzerelesVisszavonasa(ExecParam execParam, string szulo_Ugyirat_Id, string visszavonando_Ugyirat_Azonosito)
    {
        Logger.DebugStart();
        Logger.Debug("EREC_UgyUgyiratokService RegiAdatSzerelesVisszavonasa method start");
        Logger.Debug("Felhasznalo id: " + execParam.Felhasznalo_Id);
        Logger.Debug("Szulo_Ugyirat_Id: " + szulo_Ugyirat_Id);
        Logger.Debug("Visszavonando_Ugyirat_Azonosito: " + visszavonando_Ugyirat_Azonosito);
        Log log = Log.WsStart(execParam, Context, GetType());

        Logger.Debug("Parameterek ellenorzese start");
        Arguments args = new Arguments();
        args.Add(new Argument("ExecParam.Felhasznalo_Id", execParam.Felhasznalo_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Szulo_Ugyirat_Id", szulo_Ugyirat_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Visszavonando_Ugyirat_Azonosito", visszavonando_Ugyirat_Azonosito, ArgumentTypes.String));
        args.ValidateArguments();
        Logger.Debug("Parameterek ellenorzese end");

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();



            // (Cél) Ügyirat lekérése:
            Logger.Debug("Szulo ugyirat lekerese start");
            ExecParam execParam_Ugyirat_Szulo_Get = execParam.Clone();
            execParam_Ugyirat_Szulo_Get.Record_Id = szulo_Ugyirat_Id;

            Result result_ugyirat_Szulo_Get = this.Get(execParam_Ugyirat_Szulo_Get);
            if (!String.IsNullOrEmpty(result_ugyirat_Szulo_Get.ErrorCode))
            {
                // hiba:
                Logger.Error(String.Format("Szulo ugyirat lekerese sikertelen: {0},{1}", result_ugyirat_Szulo_Get.ErrorCode, result_ugyirat_Szulo_Get.ErrorMessage));
                throw new ResultException(result_ugyirat_Szulo_Get);
            }
            if (result_ugyirat_Szulo_Get.Record == null)
            {
                // hiba
                Logger.Error("result_ugyirat_Szulo_Get.Record == null");
                throw new ResultException(62272);
            }

            EREC_UgyUgyiratok erec_UgyUgyirat_Szulo = (EREC_UgyUgyiratok)result_ugyirat_Szulo_Get.Record;
            Logger.Debug("Szulo ugyirat lekerese end");

            //Contentum.eRecord.BaseUtility.Ugyiratok.Statusz szuloUgyiratStatusz =
            //    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat_Szulo);
            //ExecParam execParam_szerelhetoBeleUgyirat = execParam.Clone();

            //Logger.Debug("Szulo ugyirat ellenorzese start");
            //if (Contentum.eRecord.BaseUtility.Ugyiratok.VisszavonhatoBeloleSzereltUgyirat(
            //    szuloUgyiratStatusz, execParam_szerelhetoBeleUgyirat) == false)
            //{
            //    // Nem lehet az ügyiratba szerelni:
            //    Logger.Debug("Szulo ugyirat allapota:" + szuloUgyiratStatusz.Allapot);
            //    Logger.Debug("Szulo ugyirat tovabbitas alatti allapota:" + szuloUgyiratStatusz.TovabbitasAlattiAllapot);
            //    Logger.Debug("Szulo ugyirat felelose:" + szuloUgyiratStatusz.CsopId_Felelos);
            //    Logger.Debug("Szulo ugyirat orzoje:" + szuloUgyiratStatusz.FelhCsopId_Orzo);
            //    Logger.Debug("Szulo ugyirat ugyintezo:" + szuloUgyiratStatusz.CsopId_Ugyintezo);
            //    Logger.Warn("Az ugyiratbol nem lehert szerelest visszavonni");
            //    throw new ResultException(62271);
            //}

            Logger.Debug("Szulo ugyirat ellenorzese end");

            int regiSzereltCount = 0;
            string lastRegiAdatAzonosito = String.Empty;

            if (!String.IsNullOrEmpty(erec_UgyUgyirat_Szulo.RegirendszerIktatoszam))
            {
                //Ha már van szerelve ügyirat, akkor azt a MIG_Foszam string-el jelezzük a RegirendszerIktatoszam mezõben, ekkor a EREC_UgyiratObjKapcsolatok táblából kell lekérni a szerelt ügyiratok adatait
                Logger.Debug("A ugyiratba szerelve van regi ugyirat");
                Logger.Debug("Az ugyirat RegirendszerIktatoszam mezoje: " + erec_UgyUgyirat_Szulo.RegirendszerIktatoszam);

                #region EREC_UgyiratObjKapcsolatok GetAll
                Logger.Debug("EREC_UgyiratObjKapcsolatokService Get All start");

                ExecParam xmpObjKapcsGetAll = execParam.Clone();
                EREC_UgyiratObjKapcsolatokService svcObjKapcs = new EREC_UgyiratObjKapcsolatokService(this.dataContext);

                EREC_UgyiratObjKapcsolatokSearch schObjKapcs = new EREC_UgyiratObjKapcsolatokSearch();

                schObjKapcs.KapcsolatTipus.Filter(KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles);

                schObjKapcs.Obj_Id_Kapcsolt.Filter(szulo_Ugyirat_Id);

                Result resObjKapcsGetAll = svcObjKapcs.GetAll(xmpObjKapcsGetAll, schObjKapcs);

                if (!String.IsNullOrEmpty(resObjKapcsGetAll.ErrorCode))
                {
                    Logger.Error(String.Format("EREC_UgyiratObjKapcsolatokService Get All hiba: {0},{1}", resObjKapcsGetAll.ErrorCode, resObjKapcsGetAll.ErrorMessage));
                    throw new ResultException(resObjKapcsGetAll);
                }

                Logger.Debug("EREC_UgyiratObjKapcsolatokService Get All end");
                Logger.Debug("Eredmeny szamossaga: " + resObjKapcsGetAll.GetCount);

                foreach (DataRow row in resObjKapcsGetAll.Ds.Tables[0].Rows)
                {
                    string regiAdatAzonosito = row["Leiras"].ToString();
                    if (regiAdatAzonosito == visszavonando_Ugyirat_Azonosito)
                    {
                        string id = row["Id"].ToString();
                        Logger.Debug("EREC_UgyiratObjKapcsolatokService Invalidate start");
                        Logger.Debug("RecordId: " + id);
                        ExecParam xpmObjKapcsInvalidate = execParam.Clone();
                        xpmObjKapcsInvalidate.Record_Id = id;

                        Result resObjKapcsInvalidate = svcObjKapcs.Invalidate(xpmObjKapcsInvalidate);

                        if (!String.IsNullOrEmpty(resObjKapcsInvalidate.ErrorCode))
                        {
                            throw new ResultException(resObjKapcsInvalidate);
                        }

                        Logger.Debug("EREC_UgyiratObjKapcsolatokService Invalidate end");
                    }
                    else
                    {
                        lastRegiAdatAzonosito = regiAdatAzonosito;
                        regiSzereltCount++;
                    }
                }

                #endregion
            }
            else
            {
                Logger.Error("A ugyiratba nics szerelve regi ugyirat");
                throw new ResultException(62276);
            }

            if (erec_UgyUgyirat_Szulo.RegirendszerIktatoszam == Contentum.eUtility.Constants.TableNames.EREC_UgyiratObjKapcsolatok && regiSzereltCount > 1)
            {
                Logger.Debug("Nem kell ugyirat update");
            }
            else
            {
                Logger.Debug("Szulo ugyirat update start");
                ExecParam xpmUgyiratUpdate = execParam.Clone();
                xpmUgyiratUpdate.Record_Id = szulo_Ugyirat_Id;

                erec_UgyUgyirat_Szulo.Updated.SetValueAll(false);
                erec_UgyUgyirat_Szulo.Base.Updated.SetValueAll(false);
                erec_UgyUgyirat_Szulo.Base.Updated.Ver = true;

                if (regiSzereltCount == 0)
                {
                    Logger.Debug("regiSzereltCount == 0");
                    erec_UgyUgyirat_Szulo.Typed.RegirendszerIktatoszam = System.Data.SqlTypes.SqlString.Null;
                    erec_UgyUgyirat_Szulo.Updated.RegirendszerIktatoszam = true;
                }
                else
                {
                    if (regiSzereltCount == 1 && !String.IsNullOrEmpty(lastRegiAdatAzonosito))
                    {
                        Logger.Debug("regiSzereltCount == 1");
                        Logger.Debug("LastRegiAdatAzonosito: " + lastRegiAdatAzonosito);
                        erec_UgyUgyirat_Szulo.Typed.RegirendszerIktatoszam = lastRegiAdatAzonosito;
                        erec_UgyUgyirat_Szulo.Updated.RegirendszerIktatoszam = true;
                    }
                    else
                    {
                        Logger.Debug("regiSzereltCount: " + regiSzereltCount);
                        erec_UgyUgyirat_Szulo.Typed.RegirendszerIktatoszam = Contentum.eUtility.Constants.TableNames.EREC_UgyiratObjKapcsolatok;
                        erec_UgyUgyirat_Szulo.Updated.RegirendszerIktatoszam = true;
                    }
                }

                #region Ügyirat Jelleg és UgyintezesModja meghatározása
                // feltételezés: régi adat csak papír alapú lehet, ezért csak Vegyes típusra változhatott szereléssel
                if (erec_UgyUgyirat_Szulo.Jelleg == KodTarak.UGYIRAT_JELLEG.Vegyes)
                {
                    erec_UgyUgyirat_Szulo = SetUgyiratJellegEsUgyintezesModjaByIratokAndSzereltek(execParam.Clone(), erec_UgyUgyirat_Szulo, null);
                }

                #endregion Ügyirat Jelleg és UgyintezesModja meghatározása

                Result result_ugyiratUpdate = this.Update(
                            xpmUgyiratUpdate, erec_UgyUgyirat_Szulo);

                if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                {
                    // hiba:
                    Logger.Debug(String.Format("Szulo ugyirat update hiba: {0},{1}", result_ugyiratUpdate.ErrorCode, result_ugyiratUpdate.ErrorMessage));
                    throw new ResultException(result_ugyiratUpdate);
                }

                Logger.Debug("Szulo ugyirat update end");
            }

            Logger.Debug("FoszamSzerelesVisszavonasa start");

            ExecParam xpmFoszamSzerelesVissza = execParam.Clone();
            Result resFoszamSzerelesVissza = FoszamSzerelesVisszavonasa(xpmFoszamSzerelesVissza, visszavonando_Ugyirat_Azonosito);

            Logger.Debug("FoszamSzerelesVisszavonasa end");

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, szulo_Ugyirat_Id, "EREC_UgyUgyiratok", "UgyiratSzerelesVisszavonasa").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("EREC_UgyUgyiratokService RegiAdatSzerelesVisszavonasa method hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("EREC_UgyUgyiratokService RegiAdatSzerelesVisszavonasa method end");
        Logger.DebugEnd();
        return result;

    }

    public Result GetAllRegiSzereltAdat(ExecParam execParam, IEnumerable<string> szuloUgyiratLista)
    {
        Logger.DebugStart();
        Logger.Debug("Erec_UgyUgyiratokService GetAllRegiSzereltAdat start");
        Logger.Debug("szuloUgyiratLista: " + szuloUgyiratLista == null ? "NULL" : String.Join(",", szuloUgyiratLista.ToArray()));
        Result res = new Result();
        res.Ds = new DataSet();
        DataTable dt = new DataTable();
        dt.Columns.Add("RegiRendszerIktatoszamId");
        dt.Columns.Add("RegiRendszerIktatoszam");
        dt.Columns.Add("SzuloUgyiratId");

        EREC_UgyiratObjKapcsolatokService svcObjKapcs = new EREC_UgyiratObjKapcsolatokService(this.dataContext);
        ExecParam xpmObkKapcs = execParam.Clone();

        EREC_UgyiratObjKapcsolatokSearch schObjKapcs = new EREC_UgyiratObjKapcsolatokSearch();
        schObjKapcs.Obj_Id_Kapcsolt.In(szuloUgyiratLista);
        schObjKapcs.KapcsolatTipus.Filter(KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles);

        Logger.Debug("EREC_UgyiratObjKapcsolatokService GetAll start");
        Result resObjKapcs = svcObjKapcs.GetAll(xpmObkKapcs, schObjKapcs);

        if (!String.IsNullOrEmpty(resObjKapcs.ErrorCode))
        {
            Logger.Debug(String.Format("EREC_UgyiratObjKapcsolatokService GetAll hiba:{0},{1}", resObjKapcs.ErrorCode, resObjKapcs.ErrorMessage));
            return resObjKapcs;
        }

        Logger.Debug("EREC_UgyiratObjKapcsolatokService GetAll end");
        Logger.Debug("Szerelt regi adatok szama: " + resObjKapcs.GetCount);
        Logger.Debug("Szerelt regi adatok lista start");

        foreach (DataRow row in resObjKapcs.Ds.Tables[0].Rows)
        {
            DataRow newRow = dt.NewRow();
            newRow["RegiRendszerIktatoszamId"] = row["Obj_Id_Elozmeny"].ToString();
            newRow["RegiRendszerIktatoszam"] = row["Leiras"].ToString();
            newRow["SzuloUgyiratId"] = row["Obj_Id_Kapcsolt"].ToString();
            Logger.Debug("RegiRendszerIktatoszamId: " + row["Obj_Id_Elozmeny"].ToString());
            Logger.Debug("RegiRendszerIktatoszam: " + row["Leiras"].ToString());
            Logger.Debug("SzuloUgyiratId: " + row["Obj_Id_Kapcsolt"].ToString());
            dt.Rows.Add(newRow);
        }

        Logger.Debug("Szerelt regi adatok lista end");

        res.Ds.Tables.Add(dt);
        Logger.Debug("Erec_UgyUgyiratokService GetAllRegiSzereltAdat end");
        Logger.DebugEnd();
        return res;
    }

    private Result UpdateRegiSzereltUgyiratok(ExecParam execParam, string[] EdokUgyiratIdArray, EREC_UgyUgyiratok parentUgyiratRegi, EREC_UgyUgyiratok parentUgyirat)
    {
        Logger.DebugStart();
        Logger.Debug("UpdateRegiSzereltUgyiratok start", execParam);
        Result result = new Result();

        if (EdokUgyiratIdArray.Length == 0)
        {
            Logger.Debug("EdokUgyiratIdArray.Length == 0");
            return result;
        }

        MIG_Foszam FoszamUpdateRecord = new MIG_Foszam();

        if (!MapEdokUgyiratToMigUgyirat(parentUgyiratRegi, parentUgyirat, FoszamUpdateRecord))
        {
            Logger.Debug("Nem kell update");
            return result;
        }

        ExecParam xpmGetAllRegiSzerelt = execParam.Clone();
        string ugyiratIdList = Search.GetSqlInnerString(EdokUgyiratIdArray);
        Logger.Debug(String.Format("UgyiratIdList: {0}", ugyiratIdList));
        Result resGetAllRegiSzerelt = this.GetAllRegiSzereltAdat(xpmGetAllRegiSzerelt, EdokUgyiratIdArray);

        if (resGetAllRegiSzerelt.IsError)
        {
            Logger.Error("GetAllRegiSzerelt hiba", xpmGetAllRegiSzerelt, resGetAllRegiSzerelt);
            throw new ResultException(resGetAllRegiSzerelt);
        }

        DataTable table = resGetAllRegiSzerelt.Ds.Tables[0];
        Logger.Debug(String.Format("GetAllRegiSzerelt eredmenyszam: {0}", table.Rows.Count));
        string[] regiRendszerIktatoszamIdArray = new String[table.Rows.Count];

        for (int i = 0; i < table.Rows.Count; i++)
        {
            regiRendszerIktatoszamIdArray[i] = table.Rows[i]["RegiRendszerIktatoszamId"].ToString();
        }

        Logger.Debug("FoszamAthelyzes start");
        ExecParam xpm = execParam.Clone();
        result = FoszamAthelyezes(xpm, regiRendszerIktatoszamIdArray, FoszamUpdateRecord, true);
        Logger.Debug("FoszamAthelyzes end");

        Logger.Debug("UpdateRegiSzereltUgyiratok end", execParam);
        Logger.DebugEnd();

        return result;
    }

    private bool MapEdokUgyiratToMigUgyirat(EREC_UgyUgyiratok parentUgyiratRegi, EREC_UgyUgyiratok parentUgyirat, MIG_Foszam FoszamUpdateRecord)
    {
        Logger.Debug("MapEdokUgyiratToMigUgyirat start");
        Logger.Debug("Ugyirat Allapot: " + parentUgyirat.Allapot);
        Logger.Debug(String.Format("Regi Ugyirat Allapot: {0}", parentUgyiratRegi.Allapot));

        FoszamUpdateRecord.Updated.SetValueAll(false);
        FoszamUpdateRecord.Base.Updated.SetValueAll(false);

        if (parentUgyirat.Updated.Allapot)
        {
            //Skontroba,Irattarba
            switch (parentUgyirat.Allapot)
            {
                case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                    Logger.Debug("Skontroba kuldes");
                    FoszamUpdateRecord.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Skontro;
                    FoszamUpdateRecord.Updated.UGYHOL = true;
                    FoszamUpdateRecord.SCONTRO = parentUgyirat.SkontroVege;
                    FoszamUpdateRecord.Updated.SCONTRO = true;
                    return true;
                case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                    Logger.Debug("Irattarba helyezes");
                    FoszamUpdateRecord.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Irattarban;
                    FoszamUpdateRecord.Updated.UGYHOL = true;
                    return true;
                default:
                    //Skonrobol ki, Irattarbol ki
                    switch (parentUgyiratRegi.Allapot)
                    {
                        case KodTarak.UGYIRAT_ALLAPOT.Skontroban:
                            Logger.Debug("Skontrobol ki");
                            FoszamUpdateRecord.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Osztalyon;
                            FoszamUpdateRecord.Updated.UGYHOL = true;
                            return true;
                        case KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott:
                        case KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert:
                            if (parentUgyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Jegyzekre_helyezett && parentUgyirat.TovabbitasAlattAllapot != KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
                            {
                                Logger.Debug("Irattarbol ki");
                                FoszamUpdateRecord.UGYHOL = Contentum.eUtility.Constants.MIG_IratHelye.Osztalyon;
                                FoszamUpdateRecord.Updated.UGYHOL = true;
                                return true;
                            }
                            break;
                    }
                    Logger.Debug("MapEdokUgyiratToMigUgyirat end: return false");
                    return false;
            }
        }
        else
        {
            Logger.Debug("MapEdokUgyiratToMigUgyirat end: ParentUgyiratAllapot nem updated");
            return false;
        }
    }

    #region eMigration Wrapper

    public static Result FoszamSzereles(ExecParam _ExecParam, string RegiAdatAzonosito, string Edok_Ugyirat_Id, string Edok_Ugyirat_Azonosito, bool isVisszavonas)
    {
        Logger.Debug("Elozmenyezes regi adatba start");
        Logger.Debug(String.Format("Felhasznalo Id: {0}", _ExecParam.Felhasznalo_Id));
        string _azonosito = RegiAdatAzonosito;
        Logger.Debug(String.Format("RegiRendszerIktatoszam: {0}", _azonosito));
        string _edok_ugyirat_id = Edok_Ugyirat_Id;
        Logger.Debug(String.Format("Edok_ugyirat_id: {0}", _edok_ugyirat_id));
        string _edok_ugyirat_azon = Edok_Ugyirat_Azonosito;
        Logger.Debug(String.Format("Edok_ugyirat_azon: {0}", _edok_ugyirat_azon));
        Logger.Debug("IsVisszavonas: " + isVisszavonas.ToString());

        Logger.Debug("Bemeneti parameterek ellenorzese start");
        Arguments args = new Arguments();
        args.Add(new Argument("RegiRendszerIktatoszam", _azonosito, ArgumentTypes.String, false, null));
        if (!isVisszavonas)
        {
            args.Add(new Argument("Edok_ugyirat_id", _edok_ugyirat_id, ArgumentTypes.Guid, false, null));
            args.Add(new Argument("Edok_ugyirat_azon", _edok_ugyirat_azon, ArgumentTypes.String, false, null));
        }
        args.ValidateArguments();
        Logger.DebugStart("Bemeneti parameterek ellenorzese end");

        Contentum.eMigration.Service.MIG_FoszamService foszamService = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        ExecParam xpmFoszamSzereles = _ExecParam.Clone();
        Result resFoszamSzereles = foszamService.FoszamSzereles(xpmFoszamSzereles, _azonosito, _edok_ugyirat_id, _edok_ugyirat_azon, isVisszavonas);

        if (!String.IsNullOrEmpty(resFoszamSzereles.ErrorCode))
        {
            Logger.Error(String.Format("Elozmenyezes regi adatba hiba: {0},{1}", resFoszamSzereles.ErrorCode, resFoszamSzereles.ErrorMessage));
            throw new ResultException(resFoszamSzereles);
        }

        Logger.Debug("Elozmenyezes regi adatba end");
        return resFoszamSzereles;
    }

    public static Result FoszamSzereles(ExecParam _ExecParam, string RegiAdatAzonosito, string Edok_Ugyirat_Id, string Edok_Ugyirat_Azonosito)
    {
        return FoszamSzereles(_ExecParam, RegiAdatAzonosito, Edok_Ugyirat_Id, Edok_Ugyirat_Azonosito, false);
    }

    public static Result FoszamSzerelesVisszavonasa(ExecParam _ExecParam, string RegiAdatAzonosito)
    {
        return FoszamSzereles(_ExecParam, RegiAdatAzonosito, null, null, true);
    }

    public static Result FoszamAthelyezes(ExecParam execParam, string[] FoszamIdArray, MIG_Foszam Record, bool moveHierarchy)
    {
        Logger.DebugStart();
        Logger.Debug("UgyiratokService FoszamAthelyezes start");
        Logger.Debug(String.Format("Felhasznalo Id: {0}", execParam.Felhasznalo_Id));
        Logger.Debug(String.Format("FoszamIdArray Length: {0}", FoszamIdArray.Length));
        Logger.Debug("Ugyhol: " + Record.UGYHOL);
        Logger.Debug(String.Format("Skontro: {0}", Record.SCONTRO ?? "NULL"));

        if (FoszamIdArray.Length == 0)
        {
            Logger.Debug("UgyiratokService FoszamAthelyezes end: FoszamIdArray hossza 0");
            Logger.DebugEnd();
            return new Result();
        }

        Logger.Debug("Bemeneti parameterek ellenorzese start");
        Arguments args = new Arguments();
        args.Add(new Argument("Ugyhol", Record.UGYHOL, ArgumentTypes.String, false, null));
        args.ValidateArguments();
        Logger.DebugStart("Bemeneti parameterek ellenorzese end");

        Contentum.eMigration.Service.MIG_FoszamService service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        ExecParam xpm = execParam.Clone();
        Result res = service.FoszamAthelyzesTomeges(xpm, FoszamIdArray, Record, moveHierarchy);
        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            Logger.Error(String.Format("UgyiratokService FoszamAthelyezes hiba: {0},{1}", res.ErrorCode, res.ErrorMessage));
            throw new ResultException(res);
        }

        Logger.Debug("UgyiratokService FoszamAthelyezes end");
        Logger.DebugEnd();
        return res;
    }

    #endregion

    #endregion

    /// <summary>
    /// Ügyiratok lekérése a szülõ ügyirat hierarchiával együtt (minden ügyiratra jön a szerelési lánc)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="search"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord elkérése az adott táblából a természetes azonositók alapján (Ev, Iktatokonyv, Foszam). A szerelt ügyirat szülõ hiearchiáját is visszaadja.")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetUgyiratWithSzulokByAzonosito(ExecParam execParam, EREC_UgyUgyiratokSearch search)
    {
        Logger.DebugStart();
        Logger.Debug("UgyUgyiratokService GetUgyiratWithSzulokByAzonosito start");
        Logger.Debug("FelhasznaloId: " + execParam.Record_Id);
        Logger.Debug("Ev: " + search.Extended_EREC_IraIktatoKonyvekSearch.Ev.Value);
        Logger.Debug("Iktatohely: " + search.Extended_EREC_IraIktatoKonyvekSearch.Iktatohely.Value);
        Logger.Debug("MegkulonboztetoJelzes: " + search.Extended_EREC_IraIktatoKonyvekSearch.MegkulJelzes.Value);
        Logger.Debug("Foszam: " + search.Foszam.Value);
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Logger.Debug("Ugyirat kereses start");
            Result res = this.GetAllWithExtensionAndJogosultak(execParam, search, true);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                Logger.Error(String.Format("UgyiratokService GetAllWithExtension hiba: {0},{1}", res.ErrorCode, res.ErrorMessage));
                throw new ResultException(res);
            }

            Logger.Debug("Ugyirat kereses end");

            if (res.GetCount == 0)
            {
                Logger.Debug("A kereses eredmenytelen");
                throw new ResultException(ErrorCodes.UgyiratNemTalalhato);
            }

            if (res.GetCount > 1)
            {
                Logger.Error("A kerese eredmenye tobb record: " + res.GetCount);
                throw new ResultException(ErrorCodes.TalalatNemEgyertelmu);
            }

            DataRow ugyiratRow = res.Ds.Tables[0].Rows[0];
            string ugyiratId = ugyiratRow["Id"].ToString();
            Logger.Debug("Megtalalt ugyirat id-ja: " + ugyiratId);
            Logger.Debug("Megtalalt ugyirat iktatószáma: " + ugyiratRow["Foszam_Merge"]);
            string Allapot = ugyiratRow["Allapot"].ToString();
            Logger.Debug("Megtalalt ugyirat allapota: " + ugyiratRow["Allapot"]);

            // elõkészített szerelés is lehet - az állapot még nem szerelt
            string ugyiratSzuloId = ugyiratRow["UgyUgyirat_Id_Szulo"].ToString();
            Logger.Debug("UgyUgyirat_Id_Szulo: " + ugyiratSzuloId);

            // szerelt állapotnál kell lennie utóiratnak
            if (Allapot == KodTarak.UGYIRAT_ALLAPOT.Szerelt && String.IsNullOrEmpty(ugyiratSzuloId))
            {
                Logger.Error("Az UgyUgyirat_Id_Szulo ures");
                throw new ResultException(ErrorCodes.NincsSzuloSzereltUgyiratnak);
            }

            if (!String.IsNullOrEmpty(ugyiratSzuloId))
            {
                Logger.Debug("Ugyirat szerelt");
                Logger.Debug("SzuloHiearchia lekerdezese start");
                //string ugyiratSzuloId = ugyiratRow["UgyUgyirat_Id_Szulo"].ToString();
                //Logger.Debug("UgyUgyirat_Id_Szulo: " + ugyiratSzuloId);
                //if (String.IsNullOrEmpty(ugyiratSzuloId))
                //{
                //    Logger.Error("Az UgyUgyirat_Id_Szulo ures");
                //    throw new ResultException(ErrorCodes.NincsSzuloSzereltUgyiratnak);
                //}

                ExecParam xpmSzulo = execParam.Clone();
                xpmSzulo.Record_Id = ugyiratRow["Id"].ToString();
                Result resSzulo = this.GetSzuloHiearchy(xpmSzulo);
                if (!String.IsNullOrEmpty(resSzulo.ErrorCode))
                {
                    Logger.Error(String.Format("UgyUgyiratokService GetSzuloHiearchy hiba: {0},{1}", resSzulo.ErrorCode, resSzulo.ErrorMessage));
                    throw new ResultException(resSzulo);
                }

                Logger.Debug("Eredmeny szama: " + resSzulo.GetCount);

                if (resSzulo.GetCount < 2)
                {
                    Logger.Error("A szulo hiearchia lekerdezese sikertelen");
                    throw new ResultException(ErrorCodes.SzuloHiearchiaLekerdezeseSikertelen);
                }

                Logger.Debug("SzuloHiearchia lekerdezese end");
                result = resSzulo;
            }
            else
            {
                result = res;
            }

            Logger.Debug("Eredmeny utolso soranak betoltese a Result.Record-ba start");
            DataRow nemSzereltUgyiratRow = result.Ds.Tables[0].Rows[result.Ds.Tables[0].Rows.Count - 1];
            EREC_UgyUgyiratok nemSzereltUgyirat = new EREC_UgyUgyiratok();
            Utility.LoadBusinessDocumentFromDataRow(nemSzereltUgyirat, nemSzereltUgyiratRow);

            if (String.IsNullOrEmpty(nemSzereltUgyirat.Id))
            {
                Logger.Error("Nem szerelt ugyirat Id ures");
                throw new ResultException(ErrorCodes.UzletiObjekumFeltolteseSikertelen);
            }

            Logger.Debug("Nem szerelt ugyirat Id: " + nemSzereltUgyirat.Id);

            if (String.IsNullOrEmpty(nemSzereltUgyirat.Azonosito))
            {
                Logger.Debug("Azonosito kitoltese");
                nemSzereltUgyirat.Azonosito = nemSzereltUgyiratRow["Foszam_Merge"].ToString();
                Logger.Debug("Azonosito: " + nemSzereltUgyirat.Azonosito);
            }

            result.Record = nemSzereltUgyirat;

            Logger.Debug("Eredmeny utolso soranak betoltese a Result.Record-ba end");

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("UgyUgyiratokService GetUgyiratWithSzulokByAzonosito hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("UgyUgyiratokService GetUgyiratWithSzulokByAzonosito end");
        return result;
    }

    public static class ErrorCodes
    {
        public static int NemSzerelhetoRegiUgyirat = 52276;
        public static int UgyiratNemTalalhato = 57000;
        public static int TalalatNemEgyertelmu = 57001;
        public static int NincsSzuloSzereltUgyiratnak = 57002;
        public static int SzuloHiearchiaLekerdezeseSikertelen = 57003;
        public static int UzletiObjekumFeltolteseSikertelen = 50700;
        public static int TeljessegEllenorzesSikertelen = 58000;
        public static int UgyiratNemTeljessegEllenorizheto = 58001;
        public static int IrathozNemTartozikElsodlegesPeldany = 58002;
        public static int ElsodlegesPeldanySztornozva = 58003;
        public static int PeldanyTovabbitasAlatt = 58004;
        public static int UgyiratEsPeldanyHelyeNemAzonos = 58005;
        public static int UgyiratElokeszitettSzerelesAlatt = 58006;
    }

    public Result GetSzuloHiearchy(ExecParam execParam)
    {
        Logger.Debug("UgyiratokService GetSzuloHiearchy start");
        Logger.Debug("RecordId: " + execParam.Record_Id);
        Logger.Debug("FelhasznaloId " + execParam.Felhasznalo_Id);
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        Arguments args = new Arguments();
        args.Add(new Argument("Record Id", execParam.Record_Id, ArgumentTypes.Guid));
        args.ValidateArguments();

        result = sp.GetSzuloHiearchy(execParam);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug(String.Format("UgyiratokService GetSzuloHiearchy hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
            throw new ResultException(result);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("UgyiratokService GetSzuloHiearchy eredmeny szamossaga: " + result.GetCount);
        Logger.Debug("UgyiratokService GetSzuloHiearchy end");
        return result;
    }

    /// <summary>
    /// Ügyirat teljesség-ellenõrzése: annak vizsgálata, hogy az elsõdleges iratpéldányok az ügyiratban vannak-e,
    /// illetve az ügyirat nincs elõkészített szerelésben
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="erec_ugyugyirat"></param>
    /// <param name="ReturnBadItems"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Teljesség ellenõrzés")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result TeljessegEllenorzes(ExecParam execParam, string ugyiratId, EREC_UgyUgyiratok erec_ugyugyirat, bool ReturnBadItems)
    {
        Logger.DebugStart();
        Logger.Debug("Erec_UgyUgyiratokService TeljessegEllenorzes start");
        Logger.Debug(String.Format("UgyiratId: {0}", ugyiratId ?? "NULL"));
        Logger.Debug(String.Format("ReturnBadItems: {0}", ReturnBadItems));
        Log log = Log.WsStart(execParam, Context, GetType());


        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        // BLG#2366: Esetleges figyelmeztetõ üzenet a felhasználónak (sikeresség ellenére is)
        string warningMsg = String.Empty;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            if (erec_ugyugyirat == null)
            {
                Logger.Debug("erec_ugyugyirat == null");
                Arguments args = new Arguments();
                args.Add(new Argument("Ügyirat Id", ugyiratId, ArgumentTypes.Guid));
                args.ValidateArguments();

                Logger.Debug("Ugyirat lekerese start");
                ExecParam xpmUgyiratGet = execParam.Clone();
                xpmUgyiratGet.Record_Id = ugyiratId;
                Result resUgyiratGet = this.Get(xpmUgyiratGet);
                if (resUgyiratGet.IsError)
                {
                    Logger.Error(String.Format("Ugyirat lekerese hiba {0},{1}", resUgyiratGet.ErrorCode, resUgyiratGet.ErrorMessage));
                    throw new ResultException(resUgyiratGet);
                }
                erec_ugyugyirat = (EREC_UgyUgyiratok)resUgyiratGet.Record;

                if (erec_ugyugyirat == null)
                {
                    Logger.Error("erec_ugyugyirat == null");
                    throw new ResultException("Ügyirat üzleti objektum null");
                }

                Logger.Debug("Ugyirat lekerese end");
            }
            else
            {
                ugyiratId = erec_ugyugyirat.Id;
            }

            Logger.Debug("Ugyirat teljessegellenorizheto-e start");
            ExecParam xpm = execParam.Clone();
            Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_ugyugyirat);
            ErrorDetails errorDetail;
            if (!Contentum.eRecord.BaseUtility.Ugyiratok.TeljessegEllenorizheto(ugyiratStatusz, xpm, out errorDetail))
            {
                Logger.Error("Ugyirat nem teljesseg ellenorizheto");
                throw new ResultException(ErrorCodes.UgyiratNemTeljessegEllenorizheto, errorDetail);
            }
            Logger.Debug("Ugyirat teljessegellenorizheto-e end");

            string ugyiratOrzoje = erec_ugyugyirat.FelhasznaloCsoport_Id_Orzo;
            Logger.Debug("Ugyirat orzoje: " + ugyiratOrzoje);

            EREC_PldIratPeldanyokService svcPeldanyok = new EREC_PldIratPeldanyokService(this.dataContext);
            ExecParam xpmPeldanyok = execParam.Clone();

            EREC_PldIratPeldanyokSearch PeldanyokJoinFilter = new EREC_PldIratPeldanyokSearch();
            //PeldanyokJoinFilter.Sorszam.Value = "1";
            //PeldanyokJoinFilter.Sorszam.Operator = Query.Operators.equals;

            EREC_UgyUgyiratokSearch UgyiratSearch = new EREC_UgyUgyiratokSearch();
            UgyiratSearch.Id.Filter(ugyiratId);

            Logger.Debug("PeldanyokService GetAllByUgyirat start");

            Result resPeldanyok = svcPeldanyok.GetAllByUgyirat(xpmPeldanyok, UgyiratSearch, null, PeldanyokJoinFilter);

            if (resPeldanyok.IsError)
            {
                Logger.Error(String.Format("PeldanyokService GetAllByUgyirat hiba {0},{1}", resPeldanyok.ErrorCode, resPeldanyok.ErrorMessage));
                throw new ResultException(resPeldanyok);
            }

            Logger.Debug("PeldanyokService GetAllByUgyirat end");

            DataTable table = resPeldanyok.Ds.Tables[0];

            Dictionary<string, int> errorDictionary = new Dictionary<string, int>();

            List<string> listPeldanyIds = new List<string>();
            List<string> listIratIds = new List<string>();
            List<string> listUgyiratIds = new List<string>(); // elõkészített szerelésben részt vevõ ügyirat kerülhet ide

            Logger.Debug(String.Format("Visszakapott sorok száma: {0}", table.Rows.Count));

            Logger.Debug("Peldanyok ellenorzese start");

            foreach (DataRow row in table.Rows)
            {
                string iratAllapot = row["Irat_Allapot"].ToString();
                if (iratAllapot == KodTarak.IRAT_ALLAPOT.Sztornozott || iratAllapot == KodTarak.IRAT_ALLAPOT.Atiktatott
                    || iratAllapot == KodTarak.IRAT_ALLAPOT.Felszabaditva)
                {
                    //Irat sztornózott, átiktatott vagy felszabadított: nem kell foglalkozni vele
                    continue;
                }

                // BLG#2366: Ha munkapéldány volt, akkor majd figyelmeztetõ üzenet kell a felhasználónak:
                string iratAlszam = row["Irat_Alszam"].ToString();
                if (String.IsNullOrEmpty(iratAlszam) || iratAlszam == "0")
                {
                    warningMsg = "Az ügyiratban van munkairat. A munkairat az irattárba küldéskor törlésre kerül!";
                }

                string id = row["Id"].ToString();
                string iratId = row["IraIrat_Id"].ToString();
                string sorszam = row["Sorszam"].ToString();
                bool elsodlegesPeldany = (sorszam == "1");

                if (String.IsNullOrEmpty(id))
                {
                    //Irathoz nem tarozik ellsõdleges példány
                    errorDictionary.Add(iratId, ErrorCodes.IrathozNemTartozikElsodlegesPeldany);
                    listIratIds.Add(iratId);
                    continue;
                }

                string allapot = row["Allapot"].ToString();

                if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Sztornozott)
                {
                    if (elsodlegesPeldany)
                    {
                        //Elsõdleges példány sztornózva van
                        errorDictionary.Add(id, ErrorCodes.ElsodlegesPeldanySztornozva);
                        listPeldanyIds.Add(id);
                        continue;
                    }
                    else
                    {
                        continue;
                    }
                }

                if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                {
                    //példány továbbítás alatt
                    errorDictionary.Add(id, ErrorCodes.PeldanyTovabbitasAlatt);
                    listPeldanyIds.Add(id);
                    continue;
                }

                //Bug 6600: postázott, címzett átvette, címzett nem vette át példányokat ne vizsgáljuk
                if (!Rendszerparameterek.GetBoolean(execParam, "POSTAZOTT_TELJESSEGELLENORZES_KELL", true))
                {
                    if (allapot == KodTarak.IRATPELDANY_ALLAPOT.Postazott
                        || allapot == KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette
                        || allapot == KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at)
                    {
                        continue;
                    }
                }

                string peldanyOrzo = row["FelhasznaloCsoport_Id_Orzo"].ToString();

                if (ugyiratOrzoje != peldanyOrzo)
                {
                    //példány és az ügyirat helye nem azonos
                    errorDictionary.Add(id, ErrorCodes.UgyiratEsPeldanyHelyeNemAzonos);
                    listPeldanyIds.Add(id);
                }
            }

            Logger.Debug("Peldanyok ellenorzese end");

            #region Elõkészített szerelés ellenõrzése

            // Elõkészített szerelés elõirata?
            // (Ha ki van töltve az UgyiratId_Szulo mezõje, de nem szerelt)
            if (!String.IsNullOrEmpty(erec_ugyugyirat.UgyUgyirat_Id_Szulo)
                && erec_ugyugyirat.Allapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt
                && erec_ugyugyirat.TovabbitasAlattAllapot != KodTarak.UGYIRAT_ALLAPOT.Szerelt)
            {
                // õ az elõirat egy elõkészített szerelésben:
                errorDictionary.Add(erec_ugyugyirat.Id, ErrorCodes.UgyiratElokeszitettSzerelesAlatt);
                listUgyiratIds.Add(erec_ugyugyirat.Id);
            }

            // Benne van-e egy elõkészített szerelésben, ahol ez az utóirat?
            EREC_UgyUgyiratokSearch szerelesreElokeszitettUgyiratSearch = new EREC_UgyUgyiratokSearch();

            szerelesreElokeszitettUgyiratSearch.UgyUgyirat_Id_Szulo.Filter(erec_ugyugyirat.Id);

            // Az állapot nem szerelt:
            szerelesreElokeszitettUgyiratSearch.Allapot.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Szerelt);

            //szerelesreElokeszitettUgyiratSearch.TovabbitasAlattAllapot.Value = KodTarak.UGYIRAT_ALLAPOT.Szerelt;
            //szerelesreElokeszitettUgyiratSearch.TovabbitasAlattAllapot.Operator = Query.Operators.notequals;

            Result result_elokeszitettUgyiratSearch = this.GetAll(execParam.Clone(), szerelesreElokeszitettUgyiratSearch);
            if (result_elokeszitettUgyiratSearch.IsError)
            {
                // hiba:
                throw new ResultException(result_elokeszitettUgyiratSearch);
            }

            if (result_elokeszitettUgyiratSearch.GetCount > 0)
            {
                // Van találat:
                string eloiratId = result_elokeszitettUgyiratSearch.Ds.Tables[0].Rows[0]["Id"].ToString();
                errorDictionary.Add(eloiratId, ErrorCodes.UgyiratElokeszitettSzerelesAlatt);
                listUgyiratIds.Add(eloiratId);
            }

            #endregion

            if (errorDictionary.Count > 0)
            {
                Logger.Debug(String.Format("Hibas peldanyok talalva: {0}", errorDictionary.Count));
                ResultError.SetResultByErrorCode(result, ErrorCodes.TeljessegEllenorzesSikertelen);

                if (ReturnBadItems)
                {
                    result.Ds = new DataSet();
                    result.Record = erec_ugyugyirat;


                    if (listPeldanyIds.Count > 0)
                    {
                        Logger.Debug("Peldanyok GetAllWithExtension start");
                        ExecParam xpmPeldanyokGetAllWithExtension = execParam.Clone();
                        EREC_PldIratPeldanyokSearch schPeldanyok = new EREC_PldIratPeldanyokSearch();
                        schPeldanyok.Id.In(listPeldanyIds);
                        Result resPeldanyokGetAllWithExtension = svcPeldanyok.GetAllWithExtension(xpmPeldanyokGetAllWithExtension, schPeldanyok);
                        if (resPeldanyokGetAllWithExtension.IsError)
                        {
                            Logger.Error(String.Format("Peldanyok GetAllWithExtension hiba {0},{1}", resPeldanyokGetAllWithExtension.ErrorCode, resPeldanyokGetAllWithExtension.ErrorMessage));
                            throw new ResultException(resPeldanyokGetAllWithExtension);
                        }

                        Logger.Debug("Peldanyok GetAllWithExtension end");

                        DataTable tablePeldanyok = resPeldanyokGetAllWithExtension.Ds.Tables[0];
                        resPeldanyokGetAllWithExtension.Ds.Tables.RemoveAt(0);
                        tablePeldanyok.Columns.Add("ErrorCode", typeof(Int32));
                        tablePeldanyok.TableName = Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok;

                        foreach (DataRow row in tablePeldanyok.Rows)
                        {
                            row["ErrorCode"] = errorDictionary[row["Id"].ToString()];
                        }

                        result.Ds.Tables.Add(tablePeldanyok);
                    }

                    if (listIratIds.Count > 0)
                    {
                        Logger.Debug("Iratok GetAllWithExtension start");
                        EREC_IraIratokService svcIratok = new EREC_IraIratokService(this.dataContext);
                        ExecParam xpmIratokGetAllWithExtension = execParam.Clone();
                        EREC_IraIratokSearch schIratok = new EREC_IraIratokSearch();
                        schIratok.Id.In(listIratIds);
                        Result resIratokGetAllWithExtension = svcIratok.GetAllWithExtension(xpmIratokGetAllWithExtension, schIratok);
                        if (resIratokGetAllWithExtension.IsError)
                        {
                            Logger.Error(String.Format("Iratok GetAllWithExtension hiba {0},{1}", resIratokGetAllWithExtension.ErrorCode, resIratokGetAllWithExtension.ErrorMessage));
                            throw new ResultException(resIratokGetAllWithExtension);
                        }

                        Logger.Debug("Iratok GetAllWithExtension end");

                        DataTable tableIratok = resIratokGetAllWithExtension.Ds.Tables[0];
                        resIratokGetAllWithExtension.Ds.Tables.RemoveAt(0);
                        tableIratok.Columns.Add("ErrorCode", typeof(Int32));
                        tableIratok.TableName = Contentum.eUtility.Constants.TableNames.EREC_IraIratok;

                        foreach (DataRow row in tableIratok.Rows)
                        {
                            row["ErrorCode"] = errorDictionary[row["Id"].ToString()];
                        }

                        result.Ds.Tables.Add(tableIratok);
                    }

                    if (listUgyiratIds.Count > 0)
                    {
                        Logger.Debug("Ugyiratok GetAllWithExtension start");
                        EREC_UgyUgyiratokSearch schUgyiratok = new EREC_UgyUgyiratokSearch();
                        schUgyiratok.Id.In(listUgyiratIds);

                        Result resUgyiratokGetAllWithExtension = this.GetAllWithExtension(execParam.Clone(), schUgyiratok);
                        if (resUgyiratokGetAllWithExtension.IsError)
                        {
                            // hiba:
                            throw new ResultException(resUgyiratokGetAllWithExtension);
                        }
                        Logger.Debug("Ugyiratok GetAllWithExtension End");

                        DataTable tableUgyiratok = resUgyiratokGetAllWithExtension.Ds.Tables[0];
                        resUgyiratokGetAllWithExtension.Ds.Tables.RemoveAt(0);
                        tableUgyiratok.Columns.Add("ErrorCode", typeof(Int32));
                        tableUgyiratok.TableName = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;

                        foreach (DataRow row in tableUgyiratok.Rows)
                        {
                            row["ErrorCode"] = errorDictionary[row["Id"].ToString()];
                        }

                        result.Ds.Tables.Add(tableUgyiratok);
                    }
                }
            }

            if (!String.IsNullOrEmpty(warningMsg))
            {
                result.ErrorDetail = new ErrorDetails(warningMsg, null, null, null, null);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere && ReturnBadItems)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, ugyiratId, "EREC_UgyUgyiratok", "TeljessegEllenorzes").Record;

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        Logger.Debug("EREC_UgyUgyiratokService TeljessegEllenorzes end");
        Logger.DebugEnd();
        return result;
    }

    /// <summary>
    /// Ügyirathoz tartozó iratok közül a legnagyobb alszám lekérése
    /// </summary>
    /// <param name="execParam_utolsoAlszam"></param>
    /// <param name="ugyirat_id"></param>
    /// <returns></returns>
    public Result GetUtolsoAlszam(ExecParam execParam, string ugyirat_id)
    {
        Logger.Debug("Erec_UgyUgyiratokService GetUtolsoAlszam start");
        Logger.Debug(String.Format("UgyiratId: {0}", ugyirat_id));
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetUtolsoAlszam(execParam, ugyirat_id);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error("Erec_UgyUgyiratokService GetUtolsoAlszam hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("Erec_UgyUgyiratokService GetUtolsoAlszam end");
        return result;
    }

    /// <summary>
    /// Ügyiratban lévõ élõ (iktatott, kiadmányozott) iratok számának lekérése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    public Result GetIratszam(ExecParam execParam, string ugyiratId)
    {
        Logger.Debug("Erec_UgyUgyiratokService GetIratszam start");
        Logger.Debug(String.Format("UgyiratId: {0}", ugyiratId));
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetIratszam(execParam, ugyiratId);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error("Erec_UgyUgyiratokService GetIratszam hiba", execParam, result);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("Erec_UgyUgyiratokService GetIratszam end");
        return result;
    }

    /// <summary>
    /// A megadott ügyirathoz tartozó események lekérdezése
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="ugyiratId">Az ügyirat Id-ja</param>
    /// <param name="orderBy"></param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Történet tablap lekérdezés")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result GetAllEsemenyWithExtension(ExecParam ExecParam, KRT_EsemenyekSearch _KRT_EsemenyekSearch, string ugyiratId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllEsemenyWithExtension(ExecParam, _KRT_EsemenyekSearch, ugyiratId, null, false);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Munkanaplón intézkedések lekérdezése")]
    public Result GetAllEsemenyWithExtensionForMunkanaplo(ExecParam ExecParam, KRT_EsemenyekSearch _KRT_EsemenyekSearch, string ugyiratId, string ugyintezoId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllEsemenyWithExtension(ExecParam, _KRT_EsemenyekSearch, ugyiratId, ugyintezoId, true);

            Logger.Debug("Elozmenyek lekerese start");
            this.GetElozmenyek(ExecParam, result);
            Logger.Debug("Elozmenyek lekerese end");

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionAndJogosultakAndElozmenyek(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, bool Jogosultak)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // Snapshot Isolation Level
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch, Jogosultak, false);

            if (result.IsError)
            {
                throw new ResultException(result);
            }



            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_UgyUgyiratokSearch, new EREC_UgyUgyiratokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_UgyUgyiratok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

                    #region where feltétel összeállítása
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_UgyUgyiratokSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_UgyUgyiratokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokSearch))]
    public Result GetAllWithExtensionAndJogosultakForMunkanaplo(ExecParam ExecParam, EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch, bool Jogosultak)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            // Snapshot Isolation Level
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            if (String.IsNullOrEmpty(_EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value)
                || _EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator != Query.Operators.equals)
            {
                throw new ResultException("Hiba a munkanapló lekérésekor: Munkanapló esetén kötelezõ megadni az ügyintézõt!");
            }

            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyUgyiratokSearch, Jogosultak, true);

            Logger.Debug("Elozmenyek lekerese start");
            this.GetElozmenyek(ExecParam, result);
            Logger.Debug("Elozmenyek lekerese end");

            #region Eseménynaplózás
            if (isTransactionBeginHere && !Search.IsIdentical(_EREC_UgyUgyiratokSearch, new EREC_UgyUgyiratokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_UgyUgyiratok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_UgyUgyiratokSearch);

                    #region where feltétel összeállítása
                    if (string.IsNullOrEmpty(query.Where))
                    {
                        query.Where = _EREC_UgyUgyiratokSearch.WhereByManual;
                    }
                    else
                    {
                        query.Where += " and " + _EREC_UgyUgyiratokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    public void GetElozmenyek(ExecParam execParam, Result result)
    {
        if (result.Ds == null || result.Ds.Tables.Count == 0)
        {
            Logger.Debug("Result ures");
            return;
        }

        Logger.Debug("GetElozmenyek start");
        bool isConnectionOpenHere = false;
        try
        {
            Logger.Debug("Elozmenyek lekerese start");

            DataTable table = result.Ds.Tables[0];
            table.Columns.Add("ElozmenyAzonositok");
            table.Columns.Add("ElozmenyIds");
            table.Columns.Add("ElozmenyTypes");

            if (table.Rows.Count == 0)
            {
                Logger.Debug("table.Rows.Count == 0");
                return;
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            string[] ugyiratIds = new string[table.Rows.Count];
            for (int i = 0; i < table.Rows.Count; i++)
            {
                ugyiratIds[i] = table.Rows[i]["Id"].ToString();
            }

            Result resElozmenyek = sp.GetAllElozmeny(execParam, ugyiratIds);

            if (resElozmenyek.IsError)
            {
                Logger.Error("Elozmenyek lekerese hiba", execParam, resElozmenyek);
                throw new ResultException(resElozmenyek);
            }

            Logger.Debug("Elozmenyek lekerese end");

            Logger.Debug("Elozmenyek feldolgozasa start");

            if (resElozmenyek.GetCount > 0)
            {
                Logger.Debug("Elozmenyek száma: " + resElozmenyek.GetCount);
                DataTable tableElozmenyek = resElozmenyek.Ds.Tables[0];
                Dictionary<string, string[]> dictElozmenyek = new Dictionary<string, string[]>();

                foreach (DataRow row in tableElozmenyek.Rows)
                {
                    string id = row["Id"].ToString();
                    string elozmenyId = row["ElozmenyId"].ToString();
                    string elozmenyAzonosito = row["ElozmenyAzonosito"].ToString();
                    string elozmenyType = row["ElozmenyType"].ToString();


                    if (dictElozmenyek.ContainsKey(id))
                    {
                        string[] value = dictElozmenyek[id];
                        value[0] = value[0] + ";" + elozmenyAzonosito;
                        value[1] = value[1] + ";" + elozmenyId;
                        value[2] = value[2] + ";" + elozmenyType;
                        dictElozmenyek[id] = value;
                    }
                    else
                    {
                        string[] value = new string[3];
                        value[0] = elozmenyAzonosito;
                        value[1] = elozmenyId;
                        value[2] = elozmenyType;
                        dictElozmenyek.Add(id, value);
                    }
                }

                foreach (DataRow row in table.Rows)
                {
                    string id = row["Id"].ToString();
                    if (dictElozmenyek.ContainsKey(id))
                    {
                        row["ElozmenyAzonositok"] = dictElozmenyek[id][0];
                        row["ElozmenyIds"] = dictElozmenyek[id][1];
                        row["ElozmenyTypes"] = dictElozmenyek[id][2];
                    }
                }
            }

            Logger.Debug("Elozmenyek feldolgozasa end");
        }
        catch (Exception e)
        {
            Result res = ResultException.GetResultFromException(e);
            Logger.Error("GetElozmenyek hiba", execParam, res);
            throw new ResultException(res);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("GetElozmenyek end");
    }

    public Result GetUgyiratObjektumai(ExecParam ExecParam, string Obj_Id, string Obj_type, bool WithParent)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);

            result = sp.GetUgyiratObjektumai(ExecParam, Obj_Id, Obj_type, WithParent);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private bool IsUgyiratInFizikaiDosszie(ExecParam execParam, string Ugyirat_Id)
    {
        KRT_MappaTartalmakService service = new KRT_MappaTartalmakService(this.dataContext);
        KRT_MappaTartalmakSearch search = new KRT_MappaTartalmakSearch();

        search.Obj_Id.Filter(Ugyirat_Id);

        Result result = service.GetAllWithExtension(execParam, search);

        if (result.IsError)
            return true;

        foreach (DataRow r in result.Ds.Tables[0].Rows)
        {
            if (r["Mappa_Tipus"].ToString() == KodTarak.MAPPA_TIPUS.Fizikai)
                return true;
        }

        return false;

    }

    #region CR3206 - Irattári struktúra
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result GetAllByIrattariHelyErtek(ExecParam ExecParam, string IrattarId)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByIrattariHelyErtek(ExecParam, IrattarId);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
    #endregion

    /// <summary>
    /// Ügyirat tömeges felfüggesztés
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="id_Array"></param>
    /// <param name="felfuggesztesOka"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Felfuggesztes_Tomeges(ExecParam execParam, string[] id_Array, string felfuggesztesOka)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenõrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || id_Array == null || id_Array.Length == 0)
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52180);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // Tömbbõl összefûzött string összeállítása:

            string ids = "";
            foreach (string ugyiratId in id_Array)
            {
                if (!string.IsNullOrEmpty(ids))
                {
                    ids += ",";
                }

                ids += "'" + ugyiratId + "'";
            }


            #region Ügyiratok ellenõrzése

            EREC_UgyUgyiratokService erec_UgyUgyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
            ExecParam execParam_UgyiratGetAll = execParam.Clone();

            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();

            ugyiratokSearch.Id.Value = ids;
            ugyiratokSearch.Id.Operator = Query.Operators.inner;

            Result result_ugyiratGetAll = erec_UgyUgyiratokService.GetAll(execParam_UgyiratGetAll, ugyiratokSearch);
            if (!String.IsNullOrEmpty(result_ugyiratGetAll.ErrorCode))
            {
                // hiba:
                throw new ResultException(result_ugyiratGetAll);
            }

            // Ellenõrzés, megvan-e minden rekord:
            if (result_ugyiratGetAll.Ds == null
                || result_ugyiratGetAll.GetCount != id_Array.Length)
            {
                // hiba:
                throw new ResultException(52184);
            }

            #region STATUS CHECK
            // Ellenõrzés egyesével:
            foreach (DataRow row in result_ugyiratGetAll.Ds.Tables[0].Rows)
            {
                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByDataRow(row);
                if (ugyiratStatusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt)
                    throw new ResultException("Nem megfelelõ ügyirat állapot !");
            }
            #endregion

            #endregion

            #region UGYIRAT TOMEGES UPDATE TORTENT

            foreach (DataRow row in result_ugyiratGetAll.Ds.Tables[0].Rows)
            {
                string Id = row["Id"].ToString();
                string Ver = row["Ver"].ToString();

                EREC_UgyUgyiratok erec_ugyugyiratok = new EREC_UgyUgyiratok();
                erec_ugyugyiratok.Updated.SetValueAll(false);
                erec_ugyugyiratok.Base.Updated.SetValueAll(false);

                erec_ugyugyiratok.Base.Ver = Ver;
                erec_ugyugyiratok.Base.Updated.Ver = true;

                erec_ugyugyiratok.Allapot = KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett;
                erec_ugyugyiratok.Updated.Allapot = true;

                erec_ugyugyiratok.FelfuggesztesOka = new Sakkora().TurnFelfuggesztesOka(execParam, felfuggesztesOka);
                erec_ugyugyiratok.Updated.FelfuggesztesOka = true;

                ExecParam execParam_ugyiratUpdate = execParam.Clone();
                execParam_ugyiratUpdate.Record_Id = Id;

                Result result_ugyiratUpdate = erec_UgyUgyiratokService.Update(execParam_ugyiratUpdate, erec_ugyugyiratok);

                if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyiratUpdate);
                }
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.Required*/)]
    public Result UpdateIrattariTetel(ExecParam execParam, string irattariTetelId)
    {
        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)Get(execParam).Record;
        ugyirat.IraIrattariTetel_Id = irattariTetelId;
        return Update(execParam, ugyirat);
    }

    #region partnercim

    private Result BindPartnerCim(ExecParam execParam, EREC_UgyUgyiratok _EREC_UgyUgyiratok)
    {
        Result result = new Result();

        if (_EREC_UgyUgyiratok.Updated.Cim_Id_Ugyindito && _EREC_UgyUgyiratok.Updated.Partner_Id_Ugyindito
            && !String.IsNullOrEmpty(_EREC_UgyUgyiratok.Cim_Id_Ugyindito) && !String.IsNullOrEmpty(_EREC_UgyUgyiratok.Partner_Id_Ugyindito))
        {
            Contentum.eAdmin.Service.KRT_PartnerekService service_partner = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            execParam.Record_Id = _EREC_UgyUgyiratok.Partner_Id_Ugyindito;

            result = service_partner.BindCim(execParam, _EREC_UgyUgyiratok.Cim_Id_Ugyindito);
        }

        return result;
    }

    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratokCalendarViewSearch))]
    public Result CalendarView(ExecParam ExecParam, EREC_UgyUgyiratokCalendarViewSearch _EREC_UgyUgyiratokCalendarViewSearch)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.CalendarView(ExecParam, _EREC_UgyUgyiratokCalendarViewSearch);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string id)
    {
        EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam ugyiratokExecParam = execParam.Clone();
        ugyiratokExecParam.Record_Id = id;
        Result ugyiratokResult = ugyiratokService.Get(ugyiratokExecParam);

        if (ugyiratokResult.IsError)
            throw new ResultException(ugyiratokResult);

        EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)ugyiratokResult.Record;
        return ugy;
    }

    public void SetUCMJogosultsag(ExecParam execParam, string[] ids)
    {
        Logger.Info(String.Format("EREC_UgyUgyiratokkService.SetUCMJogosultsag: {0}", ids));
        try
        {
            string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
            Logger.Debug(String.Format("documentStoreType={0}", documentStoreType));
            if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            {
                EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);

                EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
                iratokSearch.Ugyirat_Id.In(ids);

                Result iratokResult = iratokService.GetAll(execParam, iratokSearch);

                if (iratokResult.IsError)
                    throw new ResultException(iratokResult);

                List<string> iratIds = new List<string>();

                foreach (DataRow row in iratokResult.Ds.Tables[0].Rows)
                {
                    string iratId = row["Id"].ToString();

                    if (!iratIds.Contains(iratId))
                        iratIds.Add(iratId);
                }

                foreach (string iratId in iratIds)
                {
                    iratokService.SetUCMJogosultsag(execParam, iratId);
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("EREC_UgyUgyiratokService.SetUCMJogosultsag hiba", ex);
        }
    }

    [WebMethod()]
    public Result GetTomegesOlvasasiJog(ExecParam execParam, string[] ugyiratIds)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetTomegesOlvasasiJog(execParam, ugyiratIds);

            if (result.IsError)
            {
                throw new ResultException(result);
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}