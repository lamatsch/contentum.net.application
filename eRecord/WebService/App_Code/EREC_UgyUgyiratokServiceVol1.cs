﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Data;
using System;
using System.Web.Services;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for EREC_UgyUgyiratokServiceVol1
/// </summary>
public partial class EREC_UgyUgyiratokService
{
    /// <summary>
    /// Elintezetté Nyilvanitas Es Elektronikus Irattarba Helyezes
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyirat"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    internal void ElintezetteNyilvanitasEsElektronikusIrattarbaHelyezesMentessel(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        SetUgyIratElintezett(execParam, ref erec_UgyUgyirat);
        SetUgyIratElektronikusIrattarba(execParam, ref erec_UgyUgyirat);

        ExecParam execParam_UgyiratUpdate = execParam.Clone();

        Result result = this.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);

        if (result.IsError)
            throw new ResultException(result);
    }
    /// <summary>
    /// ElintezetteNyilvanitas
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyirat"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    internal void ElintezetteNyilvanitasMentessel(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        SetUgyIratElintezett(execParam, ref erec_UgyUgyirat);

        ExecParam execParam_UgyiratUpdate = execParam.Clone();

        Result result = this.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);

        if (result.IsError)
            throw new ResultException(result);
    }
    /// <summary>
    /// ElektronikusIrattarbaHelyezes
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyirat"></param>
    /// <param name="erec_HataridosFeladatok"></param>
    internal void ElektronikusIrattarbaHelyezesMentessel(ExecParam execParam, EREC_UgyUgyiratok erec_UgyUgyirat, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        SetUgyIratElektronikusIrattarba(execParam, ref erec_UgyUgyirat);

        ExecParam execParam_UgyiratUpdate = execParam.Clone();

        Result result = this.Update(execParam_UgyiratUpdate, erec_UgyUgyirat);

        if (result.IsError)
            throw new ResultException(result);
    }

    #region HELPER
    /// <summary>
    /// SetUgyIratElintezett
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyirat"></param>
    internal void SetUgyIratElintezett(ExecParam execParam, ref EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        //Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
        //    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyirat);

        //ErrorDetails errorDetail = null;
        //if (!Contentum.eRecord.BaseUtility.Ugyiratok.ElintezetteNyilvanitasJovahagyhato(
        //ugyiratStatusz, execParam.Clone(), out errorDetail))
        //{
        //    //Az ügyirat elintézetté nyilvánítása nem hagyható jóvá!
        //    throw new ResultException(52245, errorDetail);
        //}

        //Jóváhagyó törlése
        erec_UgyUgyirat.Typed.Kovetkezo_Felelos_Id = System.Data.SqlTypes.SqlGuid.Null;
        erec_UgyUgyirat.Updated.Kovetkezo_Felelos_Id = true;

        //Állapot:
        erec_UgyUgyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Elintezett;
        erec_UgyUgyirat.Updated.Allapot = true;
    }
    /// <summary>
    /// SetUgyIratElektronikusIrattarba
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_UgyUgyirat"></param>
    internal void SetUgyIratElektronikusIrattarba(ExecParam execParam, ref EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        string elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(execParam);

        erec_UgyUgyirat.Csoport_Id_Felelos = elektronikusIrattarId;
        erec_UgyUgyirat.Updated.Csoport_Id_Felelos = true;

        erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo = elektronikusIrattarId;
        erec_UgyUgyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;
    }
    #endregion

    internal bool ElintezetteNyilvanithatoExtended(ExecParam execParam, EREC_UgyUgyiratok ugyirat, out int errorCode)
    {
        string ugyiratId = ugyirat.Id;

        //iratok elintézettek
        EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
        iratokSearch.Ugyirat_Id.Filter(ugyiratId);

        iratokSearch.PostazasIranya.In(new string[] { KodTarak.POSTAZAS_IRANYA.Belso, KodTarak.POSTAZAS_IRANYA.Kimeno });
        iratokSearch.Allapot.NotIn(new string[] { KodTarak.IRAT_ALLAPOT.Atiktatott, KodTarak.IRAT_ALLAPOT.Sztornozott, KodTarak.IRAT_ALLAPOT.Felszabaditva });

        Result iratokResult = iratokService.GetAll(execParam, iratokSearch);
        iratokResult.CheckError();

        List<Guid> iratIdList = new List<Guid>();

        foreach (DataRow row in iratokResult.Ds.Tables[0].Rows)
        {
            iratIdList.Add((Guid)row["Id"]);

            string elintezett = row["Elintezett"].ToString();

            if (elintezett != "1")
            {
                //Az ügyiratnak van olyan irata, ami nem elintézett!
                errorCode = 52248;
                return false;
            }

            // BLG#7145: „helyben maradó” belső iratnak kiadmányozottnak kell lennie:
            string postazasIranya = row["PostazasIranya"].ToString();
            string kiadmanyozniKell = row["KiadmanyozniKell"] == DBNull.Value ? null : row["KiadmanyozniKell"].ToString();
            if (postazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                string iratAllapot = row["Allapot"].ToString();
                Guid? felhasznaloIdKiadmanyozo = row["FelhasznaloCsoport_Id_Kiadmany"] as Guid?;
                if (kiadmanyozniKell != "1")
                {
                    if (felhasznaloIdKiadmanyozo != null)
                    {
                        // Az ügyirat nem nyilvánítható elintézetté, mert van még nem kiadmányozott belső irata amihez kiadványozó van rendelve
                        errorCode = 52265;
                        return false;
                    }
                }
                else if (iratAllapot != KodTarak.IRAT_ALLAPOT.Kiadmanyozott
                    && felhasznaloIdKiadmanyozo == null)
                {
                    // Az ügyirat nem nyilvánítható elintézetté, mert van még nem kiadmányozott belső irata.
                    errorCode = 52257;
                    return false;
                }
            }
        }

        foreach (DataRow row in iratokResult.Ds.Tables[0].Rows)
        {
            string iratId = row["Id"].ToString();
            string postazasIranya = row["PostazasIranya"].ToString();

            // Irat statisztika kitöltés: csak a kimenőre és belsőre ellenőrizzük:
            if (postazasIranya == KodTarak.POSTAZAS_IRANYA.Belso
                || postazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
            {
                if (!IratStatisztikaKitoltve(execParam, iratId))
                {
                    //Az ügyiratnak van olyan irata, melyben nincs kitöltve a statisztika!
                    errorCode = 52249;
                    return false;
                }
            }
        }

        if (!UgyiratStatisztikaKitoltve(execParam, ugyiratId))
        {
            //Az ügyirat statisztika nincs kitöltve!
            errorCode = 52254;
            return false;
        }

        #region Kimenő (belső keletkezésű) iratok iratpéldányai postázottak-e

        EREC_PldIratPeldanyokService pldService = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
        ugyiratokSearch.Id.Value = ugyiratId;
        ugyiratokSearch.Id.Operator = Query.Operators.equals;
        EREC_PldIratPeldanyokSearch pldSearch = new EREC_PldIratPeldanyokSearch(true);
        pldSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Belso;
        pldSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Operator = Query.Operators.equals;
        // Iratpéldányok lekérése:
        Result result = pldService.GetAllByUgyirat(execParam, ugyiratokSearch, pldSearch, null);
        result.CheckError();

        if (result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
        {
            var rows = result.Ds.Tables[0].Rows;

            for (int i = 0; i < rows.Count; i++)
            {
                string allapot = rows[i]["Allapot"].ToString();
                string expMode = rows[i]["KuldesMod"].ToString();

                // Ha még nem került postázásra (állapota szerint), és az expediálás módja ki van töltve, és nem helyben marad, akkor nem lehet elintézni az ügyiratot:
                if ((allapot == KodTarak.IRATPELDANY_ALLAPOT.Iktatott || allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kiadmanyozott
                    || allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt || allapot == KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo
                    || allapot == KodTarak.IRATPELDANY_ALLAPOT.Kimeno_kuldemenyben || allapot == KodTarak.IRATPELDANY_ALLAPOT.Munkaallapotban)
                    && (!String.IsNullOrEmpty(expMode) && expMode != KodTarak.KULDEMENY_KULDES_MODJA.Helyben && expMode != KodTarak.KULDEMENY_KULDES_MODJA.HelybenMarad_NMHH))
                {
                    errorCode = 52258;
                    return false;
                }
            }
        }

        #endregion

        #region Ellenőrzés, van-e csatolmány minden irathoz 

        // TÜK-ben nem kell
        if (!Rendszerparameterek.IsTUK(execParam)
            && iratIdList.Count > 0)
        {
            // BLG#7145: Ha valamelyik irathoz nincs csatolmány, akkor nem lehet elintézetté nyilvánítani az ügyiratot:
            #region Ügyirat iratainak a csatolmányainak lekérése

            EREC_CsatolmanyokSearch searchCsatolmanyok = new EREC_CsatolmanyokSearch();
            searchCsatolmanyok.IraIrat_Id.Value = "'" + String.Join("','", iratIdList.Select(e => e.ToString()).ToArray()) + "'";
            searchCsatolmanyok.IraIrat_Id.Operator = Query.Operators.inner;

            EREC_CsatolmanyokService serviceCsat = new EREC_CsatolmanyokService(this.dataContext);
            var resultCsatolmanyok = serviceCsat.GetAll(execParam, searchCsatolmanyok);
            resultCsatolmanyok.CheckError();

            #endregion

            /// Azok az irat Id-k, amelyekhez nem tartoznak csatolmányok.
            /// Végigmegyünk a lekért csatolmányokon, és amelyik irathoz van csatolmány, azt kivesszük ebből a listából:
            List<Guid> iratIdListNincsCsatolmany = new List<Guid>(iratIdList);
            foreach (DataRow rowCsat in resultCsatolmanyok.Ds.Tables[0].Rows)
            {
                Guid iratId = (Guid)rowCsat["IraIrat_Id"];
                if (iratIdListNincsCsatolmany.Contains(iratId))
                {
                    iratIdListNincsCsatolmany.Remove(iratId);
                }
            }

            // Ha van olyan irat, amelyhez nincs csatolmány, akkor nem lehet elintézni:
            if (iratIdListNincsCsatolmany.Count > 0)
            {
                // Az ügyirat nem nyilvánítható elintézetté, mert van olyan irata, amelyhez nincs feltöltve csatolmány.
                errorCode = 52259;
                return false;
            }
        }

        #endregion


        errorCode = 0;
        return true;
    }

    internal bool IratStatisztikaKitoltve(ExecParam execParam, string iratId)
    {
        bool ret = false;

        EREC_ObjektumTargyszavaiService otService = new EREC_ObjektumTargyszavaiService(this.dataContext);
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        Result otResult = otService.GetAllMetaByObjMetaDefinicio(execParam, search, iratId, null
                , Contentum.eUtility.Constants.TableNames.EREC_IraIratok, null, null, false
                , KodTarak.OBJMETADEFINICIO_TIPUS.B3, false, true);

        if (otResult.IsError)
            throw new ResultException(otResult);

        foreach (DataRow row in otResult.Ds.Tables[0].Rows)
        {
            string belsoAzonosito = row["BelsoAzonosito"].ToString();
            string ertek = row["Ertek"].ToString();


            if (belsoAzonosito == "IT1")
            {
                ret = !String.IsNullOrEmpty(ertek);
                break;
            }
        }

        return ret;
    }

    internal bool UgyiratStatisztikaKitoltve(ExecParam execParam, string ugyiratId)
    {
        bool ret = false;

        EREC_ObjektumTargyszavaiService otService = new EREC_ObjektumTargyszavaiService(this.dataContext);
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        Result otResult = otService.GetAllMetaByObjMetaDefinicio(execParam, search, ugyiratId, null
                , Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, null, null, false
                , KodTarak.OBJMETADEFINICIO_TIPUS.B3, false, true);

        if (otResult.IsError)
            throw new ResultException(otResult);

        foreach (DataRow row in otResult.Ds.Tables[0].Rows)
        {
            string belsoAzonosito = row["BelsoAzonosito"].ToString();
            string ertek = row["Ertek"].ToString();


            if (belsoAzonosito == "FT1")
            {
                ret = !String.IsNullOrEmpty(ertek);
                break;
            }
        }

        return ret;
    }

    #region HATOSAGI ADATOK
    /// <summary>
    /// Ugy Ugy fajta beallitas
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="id"></param>
    /// <param name="ugyFajta"></param>
    /// <returns></returns>
    [WebMethod(Description = "Ugy Ugy_fajta beallitas")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyUgyiratok))]
    public Result UpdateUgyFajta(ExecParam ExecParam, string ugyFajta)
    {
        Log log = Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Régi irat lekérése DB-bõl
            Result result_iratGet = this.Get(ExecParam);
            EREC_UgyUgyiratok Record = result_iratGet.Record as EREC_UgyUgyiratok;
            #endregion

            if (Record.Ugy_Fajtaja != ugyFajta)
            {
                Record.Ugy_Fajtaja = ugyFajta;
                Record.Updated.Ugy_Fajtaja = true;

                EREC_UgyUgyiratokStoredProcedure sp = new EREC_UgyUgyiratokStoredProcedure(dataContext);
                result = sp.Insert(Constants.Update, ExecParam, Record);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// UgyiratIratUgyFajtaModositas
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <param name="IraIratok_Ids"></param>
    /// <param name="ugyfajta"></param>
    /// <param name="IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE"></param>
    private void Ugyirat_Irat_UgyFajtaModositas(ExecParam ExecParam, string ugyiratId, string ugyfajta)
    {
        if (String.IsNullOrEmpty(ugyiratId))
            return;

        Logger.Debug("Irat.Ugy_Fajtaja = Ugy_Fajtaja");
        EREC_IraIratokService serviceIraIratok = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch searchIraIratok = new EREC_IraIratokSearch();
        searchIraIratok.Ugyirat_Id.Value = ugyiratId;
        searchIraIratok.Ugyirat_Id.Operator = Query.Operators.equals;
        searchIraIratok.Ugy_Fajtaja.Name = String.Format("IsNull({0},'<null>')", "EREC_IraIratok.Ugy_Fajtaja");
        searchIraIratok.Ugy_Fajtaja.Value = ugyfajta == null ? "<null>" : ugyfajta;
        searchIraIratok.Ugy_Fajtaja.Operator = Query.Operators.notequals;

        ExecParam execParam = ExecParam.Clone();
        Result resultIratok = serviceIraIratok.GetAll(execParam, searchIraIratok);
        resultIratok.CheckError();

        foreach (DataRow row in resultIratok.Ds.Tables[0].Rows)
        {
            ExecParam execParam_IraIratokUFGet = ExecParam.Clone();
            execParam_IraIratokUFGet.Record_Id = row["Id"].ToString();
            Result resultIratUFUpdate = serviceIraIratok.UpdateUgyFajta(execParam_IraIratokUFGet, ugyfajta);
            resultIratUFUpdate.CheckError();
        }

        Result resultUgyUFUpdate = UpdateUgyFajta(ExecParam, ugyfajta);
        resultUgyUFUpdate.CheckError();
    }

    /// <summary>
    /// SetIratUgyFajta
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_IraIratok"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    private string GetUgyIratUgyFajta(ExecParam _ExecParam, EREC_UgyUgyiratok _EREC_UgyUgyiratok)
    {
        string ugyFajta = string.Empty;

        bool IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE = (Rendszerparameterek.GetBoolean(_ExecParam, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true));
        if (IKTATAS_UGYTIPUS_IRATMETADEFBOL_VALUE)
        {
            EREC_IratMetaDefinicioService service_IratMetaDefinicio = new EREC_IratMetaDefinicioService(this.dataContext);
            ExecParam execparam_IratMetaDefinicio = _ExecParam.Clone();
            Result result_IraMetaDefinicio;

            // UgyFajta kiolvasasa az EREC_IratMetaDefinicio alapján
            if (!string.IsNullOrEmpty(_EREC_UgyUgyiratok.IratMetadefinicio_Id))
            {
                execparam_IratMetaDefinicio.Record_Id = _EREC_UgyUgyiratok.IratMetadefinicio_Id;
                result_IraMetaDefinicio = service_IratMetaDefinicio.Get(execparam_IratMetaDefinicio);
                if (!string.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                {
                    throw new ResultException(result_IraMetaDefinicio);
                }
            }
            if (!string.IsNullOrEmpty(_EREC_UgyUgyiratok.UgyTipus) && !string.IsNullOrEmpty(_EREC_UgyUgyiratok.IraIrattariTetel_Id))
            {
                EREC_IratMetaDefinicioSearch search_IratMetaDefinicio = new EREC_IratMetaDefinicioSearch();
                search_IratMetaDefinicio.Ugytipus.Filter(_EREC_UgyUgyiratok.UgyTipus);

                search_IratMetaDefinicio.Ugykor_Id.Filter(_EREC_UgyUgyiratok.IraIrattariTetel_Id);

                search_IratMetaDefinicio.EljarasiSzakasz.IsNull();
                search_IratMetaDefinicio.Irattipus.IsNull();

                search_IratMetaDefinicio.TopRow = 1;

                search_IratMetaDefinicio.ErvKezd.Clear();
                search_IratMetaDefinicio.ErvVege.Clear();

                search_IratMetaDefinicio.OrderBy = "EREC_IratMetaDefinicio.LetrehozasIdo desc";

                result_IraMetaDefinicio = service_IratMetaDefinicio.GetAll(execparam_IratMetaDefinicio, search_IratMetaDefinicio);

                if (!string.IsNullOrEmpty(result_IraMetaDefinicio.ErrorCode))
                {
                    throw new ResultException(result_IraMetaDefinicio);
                }
                else
                {
                    if (result_IraMetaDefinicio.GetCount > 0)
                    {
                        ugyFajta = result_IraMetaDefinicio.Ds.Tables[0].Rows[0]["UgyFajta"].ToString();
                    }
                    else
                    {
                        // nincs definíció az UgyFajtaja-ra
                        ugyFajta = "<null>";
                    }
                }
            }
            else
            {
                // UgyTipus üres
                ugyFajta = "<null>";
            }
        }
        else
        {
            ugyFajta = _EREC_UgyUgyiratok.Ugy_Fajtaja;
        }

        return ugyFajta;
    }

    public string GetUgyIratUgyFajta(ExecParam _ExecParam, string ugyiratId)
    {
        if (!String.IsNullOrEmpty(ugyiratId))
        {
            EREC_UgyUgyiratok ugyirat = this.GetUgyirat(_ExecParam, ugyiratId);
            return GetUgyIratUgyFajta(_ExecParam, ugyirat);
        }

        return String.Empty;
    }

    /// <summary>
    /// GetIratIdByUgyirat
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private string[] GetIratIdByUgyirat(ExecParam _ExecParam, string ugyiratId)
    {
        EREC_IraIratokService service_IraIratok = new EREC_IraIratokService(this.dataContext);
        ExecParam execParam_IraIratok = _ExecParam.Clone();

        Result result_IraIratok = service_IraIratok.GetAllIratIdsByUgyirat(execParam_IraIratok, _ExecParam.Record_Id);

        if (!string.IsNullOrEmpty(result_IraIratok.ErrorCode))
        {
            Logger.Debug("Az ügyirat iratainak meghatározása sikertelen: ", execParam_IraIratok, result_IraIratok);
            throw new ResultException(result_IraIratok);
        }

        return result_IraIratok.Record as string[];
    }

    /// <summary>
    /// GetUgyiratById
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUgyiratById(ExecParam _ExecParam, string ugyiratId)
    {
        EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
        _ExecParam.Record_Id = ugyiratId;
        Result result = service.Get(_ExecParam);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug("Az ügyirat meghatározása sikertelen: "+ugyiratId);
            throw new ResultException(result);
        }

        return result.Record as EREC_UgyUgyiratok;
    }
    #endregion

    /// <summary>
    /// UpdateUgyiratDirectWithoutFlow
    /// </summary>
    /// <param name="ExecParam"></param>
    /// <param name="Record"></param>
    /// <returns></returns>
    public Result UpdateUgyiratDirectWithoutFlow(ExecParam ExecParam, EREC_UgyUgyiratok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();

        try
        {
            result = sp.Insert(Constants.Update, ExecParam, Record);
            if (result.IsError)
            {
                throw new ResultException(result);
            }

        }
        catch (ResultException re)
        {
            result = re.GetResult();
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}