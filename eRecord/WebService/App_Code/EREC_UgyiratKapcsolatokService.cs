using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_UgyiratKapcsolatokService : System.Web.Services.WebService
{

    #region Gener�ltb�l �tvettek


    [WebMethod(Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratKapcsolatok))]
    public Result Insert(ExecParam ExecParam, EREC_UgyiratKapcsolatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            // Besz�r�skor l�tre kell hozni m�g egy kapcsolatot (felcser�lt �gyiratmez�kkel), 
            // hogy a m�sik �gyiratn�l is megjelen�thess�k egyszer�en a kapcsolatot

            // Plusz vizsg�lat: a kapcsolatn�l nem egyezhet meg a k�t �gyirat mez�
            if (Record.KapcsolatTipus == KodTarak.UGYIRAT_KAPCSOLATTIPUS.Csatolt)
            {
                Logger.Info("�gyiratok csatol�sa", ExecParam);

                // Ellen�rz�s, meg van-e adva mindk�t �gyirat id?
                if (String.IsNullOrEmpty(Record.Ugyirat_Ugyirat_Beepul) || String.IsNullOrEmpty(Record.Ugyirat_Ugyirat_Felepul))
                {
                    // hiba:
                    throw new ResultException(52330);
                }                

                if (Record.Ugyirat_Ugyirat_Beepul == Record.Ugyirat_Ugyirat_Felepul)
                {
                    // hiba:
                    throw new ResultException(52331);
                }

                #region Csatolhat�s�g ellen�rz�se a k�t �gyiratra

                // �gyirat1 GET (beepul)
                EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

                ExecParam execParam_Ugyirat1Get = ExecParam.Clone();
                execParam_Ugyirat1Get.Record_Id = Record.Ugyirat_Ugyirat_Beepul;

                Result result_ugyirat1Get = service_ugyiratok.Get(execParam_Ugyirat1Get);
                if (!String.IsNullOrEmpty(result_ugyirat1Get.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyirat1Get);
                }

                EREC_UgyUgyiratok ugyirat1Beepulo = (EREC_UgyUgyiratok) result_ugyirat1Get.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyirat1Statusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat1Beepulo);
                ErrorDetails errorDetail = null;

                if (Contentum.eRecord.BaseUtility.Ugyiratok.Csatolhato(ugyirat1Statusz, out errorDetail) == false)
                {
                    // nem csatolhat�:
                    Logger.Warn("Az �gyirat nem csatolhat�", ExecParam);
                    throw new ResultException(52331, errorDetail);
                }

                // �gyirat2 GET (felepul)
                ExecParam execParam_Ugyirat2Get = ExecParam.Clone();
                execParam_Ugyirat2Get.Record_Id = Record.Ugyirat_Ugyirat_Felepul;

                Result result_ugyirat2Get = service_ugyiratok.Get(execParam_Ugyirat2Get);
                if (!String.IsNullOrEmpty(result_ugyirat2Get.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_ugyirat2Get);
                }

                EREC_UgyUgyiratok ugyirat2Felepul = (EREC_UgyUgyiratok)result_ugyirat2Get.Record;

                Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyirat2Statusz =
                    Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat2Felepul);

                if (Contentum.eRecord.BaseUtility.Ugyiratok.Csatolhato(ugyirat2Statusz, out errorDetail) == false)
                {
                    // nem csatolhat�:
                    Logger.Warn("Az �gyirat nem csatolhat�", ExecParam);
                    throw new ResultException(52331, errorDetail);
                }

                #endregion
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }


            if (String.IsNullOrEmpty(result.ErrorCode) && Record.KapcsolatTipus == KodTarak.UGYIRAT_KAPCSOLATTIPUS.Csatolt)
            {
                // �j rekord besz�r�sa:
                String ugyiratId_beepul = Record.Ugyirat_Ugyirat_Beepul;

                Record.Ugyirat_Ugyirat_Beepul = Record.Ugyirat_Ugyirat_Felepul;
                Record.Ugyirat_Ugyirat_Felepul = ugyiratId_beepul;

                Result result2 = sp.Insert(Constants.Insert, ExecParam, Record);
                if (!String.IsNullOrEmpty(result2.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result2);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratKapcsolatok))]
    public Result Update(ExecParam ExecParam, EREC_UgyiratKapcsolatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratKapcsolatok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        // Csatol�sn�l duplik�lt bejegyz�sek vannak a kapcsolatokra:
        // �gy rekord t�rl�sn�l t�r�lni kell a m�sik bejegyz�st is
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            else
            {
                // Rekord lek�r�se:
                Result result_Get = sp.Get(ExecParam);
                if (!String.IsNullOrEmpty(result_Get.ErrorCode))
                {
                    // hiba:
                    throw new ResultException(result_Get);
                }
                else
                {
                    EREC_UgyiratKapcsolatok erec_UgyiratKapcsolatok = (EREC_UgyiratKapcsolatok)result_Get.Record;
                    if (erec_UgyiratKapcsolatok.KapcsolatTipus == KodTarak.UGYIRAT_KAPCSOLATTIPUS.Csatolt)
                    {
                        // a m�sik rekord megkeres�se:

                        EREC_UgyiratKapcsolatokSearch search = new EREC_UgyiratKapcsolatokSearch();

                        search.Ugyirat_Ugyirat_Beepul.Value = erec_UgyiratKapcsolatok.Ugyirat_Ugyirat_Felepul;
                        search.Ugyirat_Ugyirat_Beepul.Operator = Query.Operators.equals;

                        search.Ugyirat_Ugyirat_Felepul.Value = erec_UgyiratKapcsolatok.Ugyirat_Ugyirat_Beepul;
                        search.Ugyirat_Ugyirat_Felepul.Operator = Query.Operators.equals;

                        search.KapcsolatTipus.Value = KodTarak.UGYIRAT_KAPCSOLATTIPUS.Csatolt;
                        search.KapcsolatTipus.Operator = Query.Operators.equals;

                        ExecParam execParam_GetAll = ExecParam.Clone();
                        Result result_GetAll = sp.GetAll(execParam_GetAll, search);
                        if (!String.IsNullOrEmpty(result_GetAll.ErrorCode))
                        {
                            // hiba:
                            throw new ResultException(result_GetAll);
                        }
                        else
                        {
                            if (result_GetAll.Ds.Tables[0].Rows.Count == 1)
                            {
                                DataRow row = result_GetAll.Ds.Tables[0].Rows[0];

                                String id = row["Id"].ToString();

                                // T�rl�s:
                                ExecParam execParam_invalidate2 = ExecParam.Clone();
                                execParam_invalidate2.Record_Id = id;

                                Result result_invalidate2 = sp.Invalidate(execParam_invalidate2);
                                if (!String.IsNullOrEmpty(result_invalidate2.ErrorCode))
                                {
                                    // hiba:
                                    throw new ResultException(result_invalidate2);
                                }
                            }
                            else
                            {
                                // TODO: hiba jelz�se (kell?)
                            }
                        }
                    }
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


    #endregion

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratKapcsolatokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyiratKapcsolatokSearch _EREC_UgyiratKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyiratKapcsolatokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}