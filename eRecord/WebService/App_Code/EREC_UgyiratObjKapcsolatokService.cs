﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Collections.Generic;
using Contentum.eQuery;
using System.Collections;

/// <summary>
/// Summary description for EREC_UgyiratObjKapcsolatokService
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_UgyiratObjKapcsolatokService : System.Web.Services.WebService
{
    #region Utility
    private enum KapcsolatTipus
    {
        UgyiratokCsatolasa = 1,
        IratFordulat = 2,
        MegkereseaValasz = 3,
        UgyinditoPeldany = 4,
        Egyeb = 5
    }

    /// <summary>
    /// A webservice-ben felhasznált hibakódok
    /// 55000-tól
    /// </summary>
    private static class ErrorCodes
    {
        public static int AzonosObjektumok = 55000;
        //55010- Ügyiratok
        public static int UgyiratNemCsatolhato = 55010;
        //55020- Irat
        public static int IratNemCsatolhato = 55020;
        public static int IratLekereseSikertelen = 55021;
        public static int IratokNemEgyUgyiratban = 55022;
        //55030- IratPeldany
        public static int IratPeldanyNemCsatolhato = 55030;
        public static int IratPeldanyLekereseSikertelen = 55031;
        public static int IratPeldanyIrathozTartozik = 55032;
        public static int IratPeldanyIratNemEgyUgyiratban = 55033;
        public static int IratPeldanyUgyirathozTartozik = 55034;
    }

    #endregion

    #region generáltból

    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok Record)
    /// Egy rekord felvétele a EREC_UgyiratObjKapcsolatok táblába. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felvétele az adott táblába. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratObjKapcsolatok))]
    public Result Insert(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok Record)
    {
        Logger.Debug("Ugyirat objektum kapcsolatok service insert method start");
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("A bemeneti paraméterek ellenőrzése start");
            this.ValidateInputParams(Record);
            Logger.Debug("A bemeneti paraméterek ellenőrzése end");
            Logger.Debug("A kapcsolat típusának meghatározása");
            KapcsolatTipus tipus = this.GetKapcsolatTipus(Record);
            Logger.Debug("A kapcsolat típusa: " + tipus.ToString());

            switch (tipus)
            {
                case KapcsolatTipus.UgyiratokCsatolasa:
                    {
                        Logger.Debug("UgyiratokCsatolasa");
                        UgyiratokCsatolasa(ExecParam, Record);
                        break;
                    }
                case KapcsolatTipus.IratFordulat:
                    {
                        Logger.Debug("IratFordulat");
                        IratokCsatolasa(ExecParam, Record);
                        break;
                    }
                case KapcsolatTipus.MegkereseaValasz:
                    {
                        Logger.Debug("MegkeresesValasz");
                        MegkeresesValasz(ExecParam, Record);
                        break;
                    }
                case KapcsolatTipus.UgyinditoPeldany:
                    {
                        Logger.Debug("UgyinditoPeldany");
                        UgyinditoPeldany(ExecParam, Record);
                        break;
                    }
            }

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, Record.Obj_Id_Kapcsolt, Record.Obj_Type_Kapcsolt, "UgyiratCsatolasNew").Record;
                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);

                eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, Record.Obj_Id_Elozmeny, Record.Obj_Type_Elozmeny, "UgyiratCsatolasNew").Record;
                eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("Ugyirat objektum kapcsolatok service insert method hiba: {0},{1}",result.ErrorCode,result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("Ugyirat objektum kapcsolatok service insert method end");
        return result;
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az EREC_UgyiratObjKapcsolatok tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának logikai törlése (érvényesség vege beállítás). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott tábla tételazonosítóval (ExecParam.Record_Id paraméter) kijelölt rekordjának logikai törlése (érvényesség vege beállítás). Az ExecParam. adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratObjKapcsolatok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Logger.Debug("ÜgyiratoObjKapcsolatok Invalidate start");
        Logger.Debug("A record id-ja: " + ExecParam.Record_Id);
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Debug("Record lekérése start");
            Result resGet = this.Get(ExecParam);
            if (!String.IsNullOrEmpty(resGet.ErrorCode))
            {
                Logger.Error(String.Format("Record lekérése hiba: {0},{1}", resGet.ErrorCode, resGet.ErrorMessage));
                throw new ResultException(resGet);
            }
            Logger.Debug("Record lekérése end");

            EREC_UgyiratObjKapcsolatok objKapcsolat = (EREC_UgyiratObjKapcsolatok)resGet.Record;
            Logger.Debug("A kapcsolat típusának meghatározása");
            KapcsolatTipus tipus = this.GetKapcsolatTipus(objKapcsolat);
            Logger.Debug("A kapcsolat típusa: " + tipus.ToString());

            switch (tipus)
            {
                case KapcsolatTipus.UgyiratokCsatolasa:
                    {
                        Logger.Debug("UgyiratokCsatolasa");
                        UgyiratokCsatolasInvalidate(ExecParam, objKapcsolat);
                        break;
                    }
                case KapcsolatTipus.IratFordulat:
                    {
                        Logger.Debug("IratFordulat");
                        break;
                    }
                case KapcsolatTipus.MegkereseaValasz:
                    {
                        Logger.Debug("MegkeresesValasz");
                        break;
                    }
                case KapcsolatTipus.UgyinditoPeldany:
                    {
                        Logger.Debug("UgyinditoPeldany");
                        break;
                    }
            }

            result = sp.Invalidate(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, objKapcsolat.Obj_Id_Kapcsolt, objKapcsolat.Obj_Type_Kapcsolt, "UgyiratCsatolasInvalidate").Record;
            Result eventLogResult = eventLogService.Insert(ExecParam,eventLogRecord);

            eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam, dataContext.Tranz_Id, objKapcsolat.Obj_Id_Elozmeny, objKapcsolat.Obj_Type_Elozmeny, "UgyiratCsatolasInvalidate").Record;
            eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            #endregion
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        Logger.Debug("ÜgyiratoObjKapcsolatok Invalidate end");
        return result;
    }

    /// <summary>
    /// MultiInvalidate(ExecParam ExecParams)
    /// A EREC_UgyiratObjKapcsolatok táblában több rekord logikai törlése (érvényesség vege beállítás). A törlendő tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemenő paraméter. Az ExecParam adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal, Record_Id).</param> 
    /// <returns>Hibaüzenet, eredményhalmaz objektum.</returns>    
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott táblában több rekord logikai törlése (érvényesség vege beállítás). A törlendő tételek azonosítóit (Record_Id) a ExecParams tömb tartalmazza. Az ExecParam további adatai a művelet naplózásához szükségesek (felhasználó azonosító, hívó alkalamazás, hívó HTML oldal).")]
    //[AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratObjKapcsolatok))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            for (int i = 0; i < ExecParams.Length; i++)
            {
                Result result_invalidate = Invalidate(ExecParams[i]);
                if (!String.IsNullOrEmpty(result_invalidate.ErrorCode))
                {
                    throw new ResultException(result_invalidate);
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParams, result);
        return result;
    }

    #endregion

    #region Csatolási forgatókönyvek

    private void UgyiratokCsatolasa(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok Record)
    {
        #region Csatolhatóság ellenőrzése a két ügyiratra
        Logger.Debug("Csatolhatóság ellenőrzése a két ügyiratra start");

        //előzmény ügyirat ellenőrzése
        Logger.Debug("Csatolhatóság ellenőrzése az előzmény ügyiratra");
        ExecParam xpmElozmeny = ExecParam.Clone();
        xpmElozmeny.Record_Id = Record.Obj_Id_Elozmeny;
        this.CheckUgyiratCsatolhatosag(xpmElozmeny);

        //előzmény ügyirat ellenőrzése
        Logger.Debug("Csatolhatóság ellenőrzése a kapcsolt ügyiratra");
        ExecParam xpmKapcsolt = ExecParam.Clone();
        xpmKapcsolt.Record_Id = Record.Obj_Id_Kapcsolt;
        this.CheckUgyiratCsatolhatosag(xpmKapcsolt);

        Logger.Debug("Csatolhatóság ellenőrzése a két ügyiratra end");
        #endregion

        // Új rekord beszúrása:
        Logger.Debug("Az ügyirat csatolás beszúrása start");
        Result result = sp.Insert(Constants.Insert, ExecParam, Record);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            Logger.Error(String.Format("Az ügyirat csatolás beszúrása hiba: {0},{1}",result.ErrorCode,result.ErrorMessage));
            throw new ResultException(result);
        }
        Logger.Debug("Az ügyirat csatolás beszúrása end");
        //Csatolás megfordítása
        String ugyiratIdElozmeny = Record.Obj_Id_Elozmeny;

        Record.Obj_Id_Elozmeny = Record.Obj_Id_Kapcsolt;
        Record.Obj_Id_Kapcsolt = ugyiratIdElozmeny;
    }

    private void IratokCsatolasa(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok Record)
    {
        #region Csatolhatóság ellenőrzése a két iratra
        Logger.Debug("Csatolhatóság ellenőrzése a két iratra start");
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        ExecParam xpm = ExecParam.Clone();
        EREC_IraIratokSearch search = new EREC_IraIratokSearch();
        search.Id.Value = "'" + Record.Obj_Id_Elozmeny + "','" + Record.Obj_Id_Kapcsolt +"'";
        search.Id.Operator = Query.Operators.inner;

        Logger.Debug("IratokGetAllWithExtension start");

        Result res = service.GetAllWithExtension(xpm, search);
        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            Logger.Error(String.Format("IratokGetAllWithExtension hiba: {0},{1}",res.ErrorCode,res.ErrorMessage));
            throw new ResultException(res);
        }
        Logger.Debug("IratokGetAllWithExtension end");

        Hashtable htStatusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByDataSet(res.Ds);

        //A tárolt eljárás nem adta vissza az előzmény a record-ot
        if (!htStatusz.ContainsKey(Record.Obj_Id_Elozmeny))
        {
            Logger.Error("Az előzmény irat lekérése sikertelen");
            throw new ResultException(ErrorCodes.IratLekereseSikertelen);
        }

        if (!htStatusz.ContainsKey(Record.Obj_Id_Kapcsolt))
        {
            Logger.Error("A kapcsolt irat lekérése sikertelen");
            throw new ResultException(ErrorCodes.IratLekereseSikertelen);
        }

        if (!Contentum.eRecord.BaseUtility.Iratok.IsIartokInSameUgyirat(htStatusz))
        {
            Logger.Warn(String.Format("Az iratok nem egy ügyiratban vannak: {0},{1}",Record.Obj_Id_Elozmeny,Record.Obj_Id_Kapcsolt));
            throw new ResultException(ErrorCodes.IratokNemEgyUgyiratban);
        }

        Contentum.eRecord.BaseUtility.Iratok.Statusz statuszElozmeny = htStatusz[Record.Obj_Id_Elozmeny] as Contentum.eRecord.BaseUtility.Iratok.Statusz;
        Contentum.eRecord.BaseUtility.Iratok.Statusz statuszKapcsolt = htStatusz[Record.Obj_Id_Kapcsolt] as Contentum.eRecord.BaseUtility.Iratok.Statusz;

        ErrorDetails errorDetail = null;
        if(!Contentum.eRecord.BaseUtility.Iratok.Csatolhato(statuszElozmeny, out errorDetail))
        {
            Logger.Warn("Az előzmény irat nem csatolható");
            throw new ResultException(ErrorCodes.IratNemCsatolhato, errorDetail);
        }

        if (!Contentum.eRecord.BaseUtility.Iratok.Csatolhato(statuszKapcsolt, out errorDetail))
        {
            Logger.Warn("A kapcsolt irat nem csatolható");
            throw new ResultException(ErrorCodes.IratNemCsatolhato, errorDetail);
        }

        Logger.Debug("Csatolhatóság ellenőrzése a két iratra end");
        #endregion
    }

    private void MegkeresesValasz(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok Record)
    {
        #region Csatolhatóság ellenőrzése iratra(kapcsolt) és iratpeldanyra(előzmény)
        Logger.Debug("Csatolhatóság ellenőrzése az iratra és iratpéldányra start");

        Logger.Debug("Előzmény iartpéldány csatolhatóságának ellenőrzése");
        ExecParam xpmIratPeldany = ExecParam.Clone();
        xpmIratPeldany.Record_Id = Record.Obj_Id_Elozmeny;
        Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz statuszIratPeldany = this.CheckIratPeldanyCsatolhatosag(xpmIratPeldany);

        Logger.Debug("Iratpéldány az irathoz tartozáságának vizsgálata");
        if (Record.Obj_Id_Kapcsolt == statuszIratPeldany.Irat_Id)
        {
            Logger.Warn("Az iratpéldány a kapcsolandó irathoz tartozik");
            throw new ResultException(ErrorCodes.IratPeldanyIrathozTartozik);
        }

        Logger.Debug("Kapcsolt irat csatolhatóságának ellenőrzése");
        ExecParam xpmIrat = ExecParam.Clone();
        xpmIrat.Record_Id = Record.Obj_Id_Kapcsolt;
        Contentum.eRecord.BaseUtility.Iratok.Statusz statuszIrat = this.CheckIratCsatolhatosag(xpmIrat);

        Logger.Debug("Iratpéldány és az irat egy ügyiratban lévőségének vizsgálata");
        if (!Contentum.eRecord.BaseUtility.IratPeldanyok.IsIratAndIratPeldanyInSameUgyirat(statuszIrat, statuszIratPeldany))
        {
            Logger.Warn("Az iratpéldány és az irat nem azonos ügyirathoz tartozik");
            throw new ResultException(ErrorCodes.IratPeldanyIratNemEgyUgyiratban);
        }

        Logger.Debug("Csatolhatóság ellenőrzése az iratra és iratpéldányra end");
        #endregion
    }

    private void UgyinditoPeldany(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok Record)
    {
        #region Csatolhatóság ellenőrzése ügyiratra(kapcsolt) és iratpeldanyra(előzmény)
        Logger.Debug("Csatolhatóság ellenőrzése az ügyiratra és iratpéldányra start");

        Logger.Debug("Előzmény iartpéldány csatolhatóságának ellenőrzése");
        ExecParam xpmIratPeldany = ExecParam.Clone();
        xpmIratPeldany.Record_Id = Record.Obj_Id_Elozmeny;
        Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz statuszIratPeldany = this.CheckIratPeldanyCsatolhatosag(xpmIratPeldany);

        Logger.Debug("Iratpéldány az ügyirathoz tartozáságának vizsgálata");
        if (Record.Obj_Id_Kapcsolt == statuszIratPeldany.UgyiratId)
        {
            Logger.Warn("Az iratpéldány a kapcsolandó ügyirathoz tartozik");
            throw new ResultException(ErrorCodes.IratPeldanyUgyirathozTartozik);
        }

        Logger.Debug("Kapcsolt ügyirat csatolhatóságának ellenőrzése");
        ExecParam xpmUgyirat = ExecParam.Clone();
        xpmUgyirat.Record_Id = Record.Obj_Id_Kapcsolt;
        this.CheckUgyiratCsatolhatosag(xpmUgyirat);

        Logger.Debug("Csatolhatóság ellenőrzése az ügyiratra és iratpéldányra end");
        #endregion
    }

    #endregion

    #region Csatolás invalidálása

    private void UgyiratokCsatolasInvalidate(ExecParam ExecParam, EREC_UgyiratObjKapcsolatok objKapcsolat)
    {
        Logger.Debug("Másik csatolt ügyirat invalidálása start");
        ExecParam xpm = ExecParam.Clone();
        EREC_UgyiratObjKapcsolatokSearch search = new EREC_UgyiratObjKapcsolatokSearch();
        search.Obj_Id_Elozmeny.Value = objKapcsolat.Obj_Id_Kapcsolt;
        search.Obj_Id_Elozmeny.Operator = Query.Operators.equals;
        search.Obj_Id_Kapcsolt.Value = objKapcsolat.Obj_Id_Elozmeny;
        search.Obj_Id_Kapcsolt.Operator = Query.Operators.equals;
        search.KapcsolatTipus.Value = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt;
        search.KapcsolatTipus.Operator = Query.Operators.equals;

        Logger.Debug("Másik csatolt ügyirat lekérése start");
        Result resGet = this.GetAll(xpm, search);

        if (!String.IsNullOrEmpty(resGet.ErrorCode))
        {
            Logger.Error(String.Format("Másik csatolt ügyirat lekérése hiba: {0},{1}",resGet.ErrorCode,resGet.ErrorMessage));
            throw new ResultException(resGet);
        }
        Logger.Debug("Másik csatolt ügyirat lekérése end");
        Logger.Debug("Eredmény sorainak száma: " + resGet.Ds.Tables[0].Rows.Count.ToString());

        if (resGet.Ds.Tables[0].Rows.Count > 0)
        {
            string id = resGet.Ds.Tables[0].Rows[0]["Id"].ToString();
            Logger.Debug("Másik kapcsolat id-ja: " + id);
            xpm.Record_Id = id;
            Result resInvalidate = sp.Invalidate(xpm);

            if (!String.IsNullOrEmpty(resInvalidate.ErrorCode))
            {
                Logger.Error(String.Format("Másik csatolt ügyirat invalidálása hiba: {0},{1}",resInvalidate.ErrorCode,resInvalidate.ErrorMessage));
                throw new ResultException(resInvalidate);
            }
        }

        Logger.Debug("Másik csatolt ügyirat invalidálása end");
    }

    #endregion

    /// <summary>
    /// Ellenőrzi a bemenetként kapott üzleti objektumot
    /// ResultException-t dobhat
    /// </summary>
    /// <param name="objKapcsolat"></param>
    private void ValidateInputParams(EREC_UgyiratObjKapcsolatok objKapcsolat)
    {
        Logger.Debug("Az előzmény objektum id-ja: " + objKapcsolat.Obj_Id_Elozmeny);
        Logger.Debug("Az előzmény objektum típusa: " + objKapcsolat.Obj_Type_Elozmeny);
        Logger.Debug("A kapcsolt objektum id-ja: " + objKapcsolat.Obj_Id_Kapcsolt);
        Logger.Debug("A kapcsolt objektum típusa: " + objKapcsolat.Obj_Type_Kapcsolt);
        Logger.Debug("A kapcsolat típusa: " + objKapcsolat.KapcsolatTipus);

        if (objKapcsolat.Obj_Id_Elozmeny == objKapcsolat.Obj_Id_Kapcsolt)
        {
            Logger.Error("Azonos objektumok(ügyiratkezelési entitások) nem kapcsolhatóak");
            throw new ResultException(ErrorCodes.AzonosObjektumok);
        }

        Arguments args = new Arguments();
        args.Add(new Argument("Obj_Id_Elozmeny", objKapcsolat.Obj_Id_Elozmeny, ArgumentTypes.Guid));
        args.Add(new Argument("Obj_Id_Kapcsolt", objKapcsolat.Obj_Id_Kapcsolt, ArgumentTypes.Guid));
        args.Add(new Argument("Obj_Type_Elozmeny", objKapcsolat.Obj_Type_Elozmeny, ArgumentTypes.String));
        args.Add(new Argument("Obj_Type_Kapcsolt", objKapcsolat.Obj_Type_Kapcsolt, ArgumentTypes.String));
        args.Add(new Argument("KapcsolatTipus", objKapcsolat.KapcsolatTipus, ArgumentTypes.String, false, null));
        args.ValidateArguments();
    }

    /// <summary>
    /// Kapcsolat típusának meghatározása
    /// </summary>
    /// <param name="objKapcsolat"></param>
    /// <returns></returns>
    private KapcsolatTipus GetKapcsolatTipus(EREC_UgyiratObjKapcsolatok objKapcsolat)
    {
        switch (objKapcsolat.KapcsolatTipus)
        {
            case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt:
                {
                    if (objKapcsolat.Obj_Type_Elozmeny == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok &&
                        objKapcsolat.Obj_Type_Kapcsolt == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok)
                    {
                        return KapcsolatTipus.UgyiratokCsatolasa;
                    }
                    break;
                }
            case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Iratfordulat:
                {
                    if (objKapcsolat.Obj_Type_Elozmeny == Contentum.eUtility.Constants.TableNames.EREC_IraIratok &&
                        objKapcsolat.Obj_Type_Kapcsolt == Contentum.eUtility.Constants.TableNames.EREC_IraIratok)
                    {
                        return KapcsolatTipus.IratFordulat;
                    }
                    break;
                }
            case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz:
                {
                    if (objKapcsolat.Obj_Type_Elozmeny == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok &&
                        objKapcsolat.Obj_Type_Kapcsolt == Contentum.eUtility.Constants.TableNames.EREC_IraIratok)
                    {
                        return KapcsolatTipus.MegkereseaValasz;
                    }
                    break;
                }
            case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany:
                {
                    if (objKapcsolat.Obj_Type_Elozmeny == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok &&
                        objKapcsolat.Obj_Type_Kapcsolt == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok)
                    {
                        return KapcsolatTipus.UgyinditoPeldany;
                    }
                    break;
                }
        }

        return KapcsolatTipus.Egyeb;

    }

    #region Csatolhatosag Vizsgalata Objektumokra

    private void CheckUgyiratCsatolhatosag(ExecParam xpm)
    {
        EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
        Result res = service.Get(xpm);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            // hiba:
            Logger.Error(String.Format("Hiba az ügyirat lekérése közben: {0},{1}", res.ErrorCode, res.ErrorMessage));
            throw new ResultException(res);
        }

        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;

        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz statusz =
            Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);
        ErrorDetails errorDetail = null;

        if (Contentum.eRecord.BaseUtility.Ugyiratok.Csatolhato(statusz, out errorDetail) == false)
        {
            // nem csatolható:
            Logger.Warn("Az ügyirat nem csatolható", xpm);
            throw new ResultException(ErrorCodes.UgyiratNemCsatolhato, errorDetail);
        }
    }

    private Contentum.eRecord.BaseUtility.Iratok.Statusz CheckIratCsatolhatosag(ExecParam xpm)
    {
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch search = new EREC_IraIratokSearch();
        search.Id.Value = xpm.Record_Id;
        search.Id.Operator = Query.Operators.equals;

        Result res = service.GetAllWithExtension(xpm,search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            // hiba:
            Logger.Error(String.Format("Hiba az irat lekérése közben: {0},{1}", res.ErrorCode, res.ErrorMessage));
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error("Iratok GetAllWithExtension nulla rekorddal tért vissza");
            throw new ResultException(ErrorCodes.IratLekereseSikertelen);
        }

        Hashtable htStatusz = Contentum.eRecord.BaseUtility.Iratok.GetAllapotByDataSet(res.Ds);


        Contentum.eRecord.BaseUtility.Iratok.Statusz statusz = htStatusz[xpm.Record_Id] as Contentum.eRecord.BaseUtility.Iratok.Statusz;
        ErrorDetails errorDetail = null;

        if (Contentum.eRecord.BaseUtility.Iratok.Csatolhato(statusz, out errorDetail) == false)
        {
            // nem csatolható:
            Logger.Warn("Az irat nem csatolható", xpm);
            throw new ResultException(ErrorCodes.IratNemCsatolhato, errorDetail);
        }

        return statusz;
    }

    private Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz CheckIratPeldanyCsatolhatosag(ExecParam xpm)
    {
        EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
        search.Id.Value = xpm.Record_Id;
        search.Id.Operator = Query.Operators.equals;

        Result res = service.GetAllWithExtension(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            // hiba:
            Logger.Error(String.Format("Hiba az iratpéldány lekérése közben: {0},{1}", res.ErrorCode, res.ErrorMessage));
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Error("Iratpéldánya GetAllWithExtension nulla rekorddal tért vissza");
            throw new ResultException(ErrorCodes.IratPeldanyLekereseSikertelen);
        }

        Hashtable htStatusz = Contentum.eRecord.BaseUtility.IratPeldanyok.GetAllapotByDataSet(xpm, res.Ds);


        Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz statusz = htStatusz[xpm.Record_Id] as Contentum.eRecord.BaseUtility.IratPeldanyok.Statusz;

        ErrorDetails errorDetail = null;
        if (Contentum.eRecord.BaseUtility.IratPeldanyok.Csatolhato(statusz, out errorDetail) == false)
        {
            // nem csatolható:
            Logger.Warn("Az iratpéldány nem csatolható", xpm);
            throw new ResultException(ErrorCodes.IratPeldanyNemCsatolhato, errorDetail);
        }

        return statusz;
    }

    #endregion

    public Result InsertMigraltSzerelt(ExecParam execParam, string cel_Ugyirat_Id, string szerelendo_Ugyirat_Id, string szerelendo_Ugyirat_Azonosito)
    {
        Logger.DebugStart();
        Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt start");
        Logger.Debug("Felhasznalo id: " + execParam.Felhasznalo_Id);
        Logger.Debug("Cel_Ugyirat_Id: " + cel_Ugyirat_Id);
        Logger.Debug("Szerelendo_Ugyirat_Id: " + szerelendo_Ugyirat_Id);
        Logger.Debug("Szerelendo_Ugyirat_Azonosito: " + szerelendo_Ugyirat_Azonosito);

        Logger.Debug("Parameterek ellenorzese start");
        Arguments args = new Arguments();
        args.Add(new Argument("ExecParam.Felhasznalo_Id", execParam.Felhasznalo_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Cel_Ugyirat_Id", cel_Ugyirat_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Szerelendo_Ugyirat_Id", cel_Ugyirat_Id, ArgumentTypes.Guid, false, null));
        args.Add(new Argument("Szerelendo_Ugyirat_Azonosito", szerelendo_Ugyirat_Azonosito, ArgumentTypes.String, false, null));
        args.ValidateArguments();
        Logger.Debug("Parameterek ellenorzese end");

        ExecParam xmpObjKapcsInsert = execParam.Clone();
        EREC_UgyiratObjKapcsolatok objKapcsolat = new EREC_UgyiratObjKapcsolatok();
        objKapcsolat.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MigraltSzereles;
        objKapcsolat.Obj_Id_Kapcsolt = cel_Ugyirat_Id;
        objKapcsolat.Obj_Type_Kapcsolt = Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok;
        objKapcsolat.Obj_Id_Elozmeny = szerelendo_Ugyirat_Id;
        objKapcsolat.Obj_Type_Elozmeny = Contentum.eUtility.Constants.TableNames.MIG_Foszam;
        //A regi adat azonosítójának tárolása a leírás mezőben
        objKapcsolat.Leiras = szerelendo_Ugyirat_Azonosito;

        Logger.Debug("EREC_UgyiratObjKapcsolatokService insert start");

        Result resObjKapcsInsert = this.Insert(xmpObjKapcsInsert, objKapcsolat);

        if (!String.IsNullOrEmpty(resObjKapcsInsert.ErrorCode))
        {
            // hiba:
            Logger.Debug(String.Format("EREC_UgyiratObjKapcsolatokService insert hiba: {0},{1}", resObjKapcsInsert.ErrorCode, resObjKapcsInsert.ErrorMessage));
            throw new ResultException(resObjKapcsInsert);
        }

        Logger.Debug("EREC_UgyiratObjKapcsolatokService insert end");

        Logger.Debug("EREC_UgyiratObjKapcsolatokService InsertMigraltSzerelt end");
        Logger.DebugEnd();

        return resObjKapcsInsert;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_UgyiratObjKapcsolatokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_UgyiratObjKapcsolatokSearch _EREC_UgyiratObjKapcsolatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_UgyiratObjKapcsolatokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }


}
