﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for WorkFlowService
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class EREC_WorkFlowService : System.Web.Services.WebService 
{
    private EREC_WorkFlowStoredProcedure sp = null;

    private DataContext dataContext;

    public EREC_WorkFlowService () 
    {
        dataContext = new DataContext(this.Application);

        sp = new EREC_WorkFlowStoredProcedure(dataContext);
    }

    public EREC_WorkFlowService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;

        sp = new EREC_WorkFlowStoredProcedure(dataContext);
    }

    [WebMethod()]
    public Result GetWorkFlowAdat(ExecParam ExecParam, String ContentType, String FazisErtek, String AllapotErtek,
        String Kimenet, String BazisObj_Id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            #region GetTypedParameters

            SqlString typedContentType =Contentum.eBusinessDocuments.Utility.SetSqlStringFromString(ContentType, SqlString.Null);
            SqlString typedFazisErtek = Contentum.eBusinessDocuments.Utility.SetSqlStringFromString(FazisErtek, SqlString.Null);
            SqlString typedAllapotErtek = Contentum.eBusinessDocuments.Utility.SetSqlStringFromString(AllapotErtek, SqlString.Null);
            SqlString typedKimenet = Contentum.eBusinessDocuments.Utility.SetSqlStringFromString(Kimenet, SqlString.Null);
            SqlGuid typedBazisObj_Id = Contentum.eBusinessDocuments.Utility.SetSqlGuidFromString(BazisObj_Id, SqlGuid.Null);

            #endregion

            result = sp.GetWorkFlowAdat(ExecParam,typedContentType,typedFazisErtek,typedAllapotErtek,
                typedKimenet,typedBazisObj_Id);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }
}

