using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eDocument.Service;
using System.Collections.Generic;

/// <summary>
///    A(z) EREC_eBeadvanyCsatolmanyok t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_eBeadvanyCsatolmanyokService : System.Web.Services.WebService
{
    [WebMethod]
    public Result DocumentumCsatolas(ExecParam execParam, string eBeadvany_Id, Csatolmany[] csatolmanyok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        List<string> dokumantumIds = new List<string>();
        List<string> dokumentumNevek = new List<string>();

        #region tranzakci�n k�v�l

        try
        {
            foreach (Csatolmany csatolmany in csatolmanyok)
            {
                if (csatolmany == null)
                {
                    continue;
                }

                Result result_upload = DocumentumFeltoltes(execParam, csatolmany, false);
                result_upload.CheckError();

                dokumantumIds.Add(result_upload.Uid);
                dokumentumNevek.Add(csatolmany.Nev);
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        if (result.IsError)
        {
            log.WsEnd(execParam, result);
            return result;
        }

        #endregion

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;


        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.DocumentumCsatolasFeltoltesNelkul(execParam, eBeadvany_Id, dokumantumIds, dokumentumNevek);
            result.CheckError();


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam);
        return result;
    }

    public Result DocumentumFeltoltes(ExecParam execParam, Csatolmany csatolmany, bool isKapuVeveny)
    {
        // KRT_Dokumentumok objektum l�trehoz�sa
        KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
        if (csatolmany.Megnyithato != null)
        {
            krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
        }
        if (csatolmany.Olvashato != null)
        {
            krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
        }
        if (csatolmany.ElektronikusAlairas != null)
        {
            krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
        }
        if (csatolmany.SablonAzonosito != null)
        {
            krt_Dokumentumok.SablonAzonosito = csatolmany.SablonAzonosito;
        }

        DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
        documentService.Timeout = (int)TimeSpan.FromMinutes(20).TotalMilliseconds;


        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                "<iktatokonyv></iktatokonyv>" +
                                                "<sourceSharePath></sourceSharePath>" +
                                                "<foszam>{0}</foszam>" +
                                                "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                "<megjegyzes></megjegyzes>" +
                                                "<munkaanyag>{2}</munkaanyag>" +
                                                "<ujirat>{3}</ujirat>" +
                                                "<vonalkod></vonalkod>" +
                                                "<docmetaIratId></docmetaIratId>" +
                                                "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                "<ucmiktatokonyv>{5}</ucmiktatokonyv>" +
                                                "<customDoktarDocLibPath>{6}</customDoktarDocLibPath>" +
                                            "</uploadparameterek>"
                                            , String.Empty
                                            , String.Empty
                                            , "NEM"
                                            , "IGEN"
                                            , "IGEN"
                                            , "HK"
                                            , isKapuVeveny ? "KapuRendszerVeveny" : String.Empty
                                            );

        Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
            (
               execParam
               , Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
               , csatolmany.Nev
               , csatolmany.Tartalom
               , krt_Dokumentumok
               , uploadXmlStrParams
            );

        return result_upload;
    }

    public Result DocumentumCsatolasFeltoltesNelkul(ExecParam execParam, string eBeadvany_Id, List<string> dokumentumIds, List<string> dokumentumNevek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            for (int i = 0; i< dokumentumIds.Count; i++)
            {
                string dokumentumId = dokumentumIds[i];
                string dokumentumNev = dokumentumNevek[i];

                EREC_eBeadvanyCsatolmanyok erec_eBeadvanyCsatolmanyok = new EREC_eBeadvanyCsatolmanyok();
                erec_eBeadvanyCsatolmanyok.eBeadvany_Id = eBeadvany_Id;
                erec_eBeadvanyCsatolmanyok.Nev = dokumentumNev;
                erec_eBeadvanyCsatolmanyok.Dokumentum_Id = dokumentumId;

                Result _ret = Insert(execParam, erec_eBeadvanyCsatolmanyok);
                if (_ret.IsError)
                {
                    throw new ResultException(_ret);
                }

                result = _ret;

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam);
        return result;
    }
}