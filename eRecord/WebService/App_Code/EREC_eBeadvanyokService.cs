using Contentum.eBusinessDocuments;
using Contentum.eQuery;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.EKF;
using System;
using System.Data;
using System.Net;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.Web.Services;
using System.IO;
using System.Xml;
using Contentum.eIntegrator.Service;
using Contentum.eRecordWebService.eBeadvanyok;

/// <summary>
///    A(z) EREC_eBeadvanyok t�bl�hoz tartoz� Web szolg�ltat�sok.
/// </summary>

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_eBeadvanyokService : System.Web.Services.WebService
{
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eBeadvanyokSearch _EREC_eBeadvanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (!FunctionRights.HasFunctionRight(ExecParam, "eBeadvanyokOsszesCelRendszer"))
            {
                _EREC_eBeadvanyokSearch.Cel.Value = KodTarak.eBeadvany_CelRendszer.WEB;
                _EREC_eBeadvanyokSearch.Cel.Operator = Query.Operators.equals;
            }

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_eBeadvanyokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    // CR3114 : HAIR adatok let�lt�se
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyokSearch))]
    public Result GetAllWithExtension_HAIR(ExecParam ExecParam, EREC_eBeadvanyokSearch _EREC_eBeadvanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            _EREC_eBeadvanyokSearch.Cel.Value = "HAIR";
            _EREC_eBeadvanyokSearch.Cel.Operator = "=";

            result = sp.GetAllWithExtension(ExecParam, _EREC_eBeadvanyokSearch);

            if (!result.IsError)
            {
                if (result.Ds.Tables[0] != null)
                {

                    result.Ds.Tables[0].Columns.Add(new DataColumn("FileContent", typeof(string)));

                    string url;
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        url = row["External_Link"].ToString();
                        byte[] cont;
                        try
                        {
                            WebClient wc = new System.Net.WebClient();
                            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserName");
                            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServicePassword");
                            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserDomain");
                            wc.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                            cont = wc.DownloadData(url);

                        }
                        catch (Exception exc)
                        {
                            result = ResultException.GetResultFromException(exc);
                            return result;
                        }

                        row["FileContent"] = System.Convert.ToBase64String(cont);
                    }
                    result.Ds.AcceptChanges();
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Insert(ExecParam ExecParam, EREC_eBeadvanyok Record)
    /// Egy rekord felv�tele a EREC_eBeadvanyok t�bl�ba. 
    /// A rekord adatait a Record parameter tartalmazza. 
    /// </summary>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Egy rekord felv�tele az adott t�bl�ba. A rekord adatait a Record parameter tartalmazza. ")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result Insert(ExecParam ExecParam, EREC_eBeadvanyok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Insert, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "EREC_eBeadvanyok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    // <summary>
    /// Hivatali kapu modulb�l hivhat�
    /// Az �zenetet beregisztr�lja az adatb�zisba, felt�lti a tartalmat a SP szerverre, �s l�trehozza
    /// a kapcsolatot a SPS �s az EDOK k�z�tt
    /// </summary>
    /// <param name="execParam">�ltal�nos inform�ci�k a futtat� felhaszn�l�r�l</param>
    /// <param name="erec_eBeadvanyok">EREC_eBeadvanyok t�pus� objektum</param>
    /// <param name="csatolmanyok">Csatolm�nyok t�pus� objektum/param>
    /// <returns>A m�velet eredm�ny�t jelz� Result objektum; siker eset�n az UID mez�ben az �jonnan
    /// l�trej�tt elektronikus beadv�ny Id-ja, egy�bk�nt a hiba k�dja az ErrorCode mez�ben</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result InsertAndAttachDocument(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, Csatolmany[] csatolmanyok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        bool isKapuVeveny = false;
        List<string> dokumantumIds = new List<string>();
        List<string> dokumentumNevek = new List<string>();

        #region tranzakcion k�v�l

        try
        {
            #region PARTNER_ID ES CIM_ID MEGHATAROZAS
            if (string.IsNullOrEmpty(erec_eBeadvanyok.Partner_Id) && !string.IsNullOrEmpty(erec_eBeadvanyok.PartnerKapcsolatiKod))
            {
                KRT_Partnerek foundPartnerByKapcsKod = GetPartnerFromKapcsolatiKod(execParam, erec_eBeadvanyok.PartnerKapcsolatiKod);
                if (foundPartnerByKapcsKod != null)
                {
                    erec_eBeadvanyok.Partner_Id = foundPartnerByKapcsKod.Id;
                   
                    if (string.IsNullOrEmpty(erec_eBeadvanyok.Cim_Id))
                    {
                        KRT_Cimek foundCim = GetPartnerCimFromPartnerIdAndMakKod(execParam, foundPartnerByKapcsKod.Id, erec_eBeadvanyok.PartnerMAKKod);
                        if (foundCim != null)
                            erec_eBeadvanyok.Cim_Id = foundCim.Id;
                    }
                }
            }
            #endregion

            isKapuVeveny = IsKapuVeveny(erec_eBeadvanyok);

            if (csatolmanyok != null)
            {

                EREC_eBeadvanyCsatolmanyokService erec_eBeadvanyCsatolmanyokService = new EREC_eBeadvanyCsatolmanyokService();

                foreach (Csatolmany csatolmany in csatolmanyok)
                {
                    Result resUpload = erec_eBeadvanyCsatolmanyokService.DocumentumFeltoltes(execParam, csatolmany, isKapuVeveny);
                    resUpload.CheckError();

                    dokumantumIds.Add(resUpload.Uid);
                    dokumentumNevek.Add(csatolmany.Nev);
                }
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        if (result.IsError)
        {
            log.WsEnd(execParam, result);
            return result;
        }

        #endregion

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();



            //Az �zenet r�gz�t�se
            Result _ret = Insert(execParam, erec_eBeadvanyok);
            if (_ret.IsError)
            {
                throw new ResultException(_ret);
            }

            erec_eBeadvanyok.Id = _ret.Uid;

            if (csatolmanyok != null)
            {
                string eBeadvany_Id = _ret.Uid;
                EREC_eBeadvanyCsatolmanyokService erec_eBeadvanyCsatolmanyokService = new EREC_eBeadvanyCsatolmanyokService(this.dataContext);
                Result _ret_csatolmanyok = erec_eBeadvanyCsatolmanyokService.DocumentumCsatolasFeltoltesNelkul(execParam, eBeadvany_Id, dokumantumIds, dokumentumNevek);
                if (_ret_csatolmanyok.IsError)
                {
                    throw new ResultException(_ret_csatolmanyok);
                }
            }


            result = _ret;

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, result.Uid, "EREC_eBeadvanyok", "eBeadvanyokLetoltes").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    /// <summary>
    /// Az elektronikus �zenet ut�feldogloz�sa
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="eBeadvany_Id"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result PostProcess(ExecParam execParam, string eBeadvanyId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            ExecParam execParamGet = execParam.Clone();
            execParamGet.Record_Id = eBeadvanyId;

            Result resultGet = this.Get(execParamGet);
            resultGet.CheckError();

            EREC_eBeadvanyok erec_eBeadvanyok = resultGet.Record as EREC_eBeadvanyok;

            PostProcessInternal(execParam, erec_eBeadvanyok, true); ;
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        if (result.IsError)
        {
            string hiba = ResultError.GetErrorMessageFromResultObject(result);
            SetProcessError(execParam, eBeadvanyId, hiba);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, EREC_eBeadvanyok Record)
    /// Az EREC_eBeadvanyok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result Update(ExecParam ExecParam, EREC_eBeadvanyok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_eBeadvanyok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(Description = "Is EUzenetForras Elhisz ?")]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result IsEUzenetForras_Elhisz(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result.Record = IsEUzenetForrasElhisz(execParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod(Description ="A hib�ra futott vev�nyeket megpr�b�lja �jra feldolgozni.")]
    public Result PostProcessFailedVevenyek_Job()
    {
        PostProcessManager procManager = new PostProcessManager();
        ExecParam execParam = procManager.GetAdminExecParam();

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            procManager.ProcessFailedVevenyek_Job();
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #region vev�ny kezel�s

    private bool IsKapuVeveny(EREC_eBeadvanyok erec_eBeadvanyok)
    {
        if (String.IsNullOrEmpty(erec_eBeadvanyok.KR_DokTipusAzonosito))
            return false;

        string dokTipuAzonosito = erec_eBeadvanyok.KR_DokTipusAzonosito.Trim().ToLower();

        Logger.Debug(String.Format("DokTipusAzonosito: {0}", dokTipuAzonosito));

        switch (dokTipuAzonosito)
        {
            case "feladasiigazolas":
            case "atveteliertesito":
            case "letoltesiigazolas":
            case "meghiusulasiigazolas":
                return true;
            default:
                return false;
        }
    }

    private void FeladasiIgazolas(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, bool postaHibrid)
    {
        Logger.Info(String.Format("FeladasiIgazolas kezdete: {0}", erec_eBeadvanyok.Id));

        string hivatkozasiSzam = erec_eBeadvanyok.KR_HivatkozasiSzam;

        Logger.Debug(String.Format("KR_HivatkozasiSzam: {0}", hivatkozasiSzam));

        EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, hivatkozasiSzam);

        if (hivatkozotteBeadvany != null)
        {
            Logger.Debug(String.Format("hivatkozotteBeadvany: {0}", hivatkozotteBeadvany.Id));

            if (!postaHibrid)
            {
                string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);
                string postazasDatuma = erec_eBeadvanyok.KR_ErkeztetesiDatum;
                string ragszam = hivatkozasiSzam;

                //post�z�s
                Logger.Debug(String.Format("kimenoKuldemenyId: {0}", kimenoKuldemenyId));

                Postazas(execParam, kimenoKuldemenyId, postazasDatuma, ragszam);
            }

            //csatolm�ny kapcsol�sa vev�nyhez
            AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, erec_eBeadvanyok, hivatkozotteBeadvany);

            Invalidate(execParam, erec_eBeadvanyok);
        }

        Logger.Info(String.Format("FeladasiIgazolas vege: {0}", erec_eBeadvanyok.Id));

    }

    private void AtveteliErtesito(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, bool postaHibrid)
    {
        Logger.Info(String.Format("AtveteliErtesito kezdete: {0}", erec_eBeadvanyok.Id));

        string hivatkozasiSzam = erec_eBeadvanyok.KR_HivatkozasiSzam;

        Logger.Debug(String.Format("KR_HivatkozasiSzam: {0}", hivatkozasiSzam));

        EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, hivatkozasiSzam);

        if (hivatkozotteBeadvany != null)
        {
            //csatolm�ny kapcsol�sa vev�nyhez
            AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, erec_eBeadvanyok, hivatkozotteBeadvany);

            Invalidate(execParam, erec_eBeadvanyok);
        }

        Logger.Info(String.Format("AtveteliErtesito vege: {0}", erec_eBeadvanyok.Id));
    }

    private void LetoltesiIgazolas(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, bool postaHibrid)
    {
        Logger.Info(String.Format("LetoltesiIgazolas kezdete: {0}", erec_eBeadvanyok.Id));



        bool attached = false;

        if (!postaHibrid)
        {

            EREC_KuldTertivevenyek tertiveveny = GetTertiveveny(execParam, erec_eBeadvanyok.KR_HivatkozasiSzam);

            if (tertiveveny == null)
            {
                throw new Exception("A let�lt�si igazol�s feldogoz�sa sikertelen. A k�ldem�ny m�g nem lett post�zva!");
            }

            if (tertiveveny != null)
            {
                Logger.Debug(String.Format("tertiveveny: {0}", tertiveveny.Id));

                if (tertiveveny.Allapot == KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett)
                {
                    tertiveveny.Updated.SetValueAll(false);
                    tertiveveny.Base.Updated.SetValueAll(false);
                    tertiveveny.Base.Updated.Ver = true;

                    tertiveveny.TertivisszaKod = KodTarak.TERTIVEVENY_VISSZA_KOD.Kezbesitve_Cimzettnek;
                    tertiveveny.Updated.TertivisszaKod = true;

                    tertiveveny.TertivisszaDat = DateTime.Now.ToString();
                    tertiveveny.Updated.TertivisszaDat = true;

                    tertiveveny.AtvetelDat = erec_eBeadvanyok.KR_ErkeztetesiDatum;
                    tertiveveny.Updated.AtvetelDat = true;


                    EREC_KuldTertivevenyekService service = new EREC_KuldTertivevenyekService(this.dataContext);
                    Result res = service.TertivevenyErkeztetes(execParam, tertiveveny.Id, tertiveveny);

                    if (res.IsError)
                    {
                        throw new ResultException(res);
                    }

                    AttacheBeadVanyCsatolmanyToTertiveveny(execParam, erec_eBeadvanyok, tertiveveny);

                    attached = true;
                }
                else
                {
                    Logger.Debug("A t�rtivev�ny m�r �rkeztetve van!");
                }
            }
        }

        EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, erec_eBeadvanyok.KR_HivatkozasiSzam);

        if (hivatkozotteBeadvany != null)
        {
            Logger.Debug(String.Format("hivatkozotteBeadvany: {0}", hivatkozotteBeadvany.Id));

            if (hivatkozotteBeadvany.Allapot == "11")
            {
                hivatkozotteBeadvany.Updated.SetValueAll(false);
                hivatkozotteBeadvany.Base.Updated.SetValueAll(false);
                hivatkozotteBeadvany.Base.Updated.Ver = true;

                hivatkozotteBeadvany.Updated.Allapot = true;
                hivatkozotteBeadvany.Allapot = "12"; //C�mzett �tvette

                ExecParam execParamUpdate = execParam.Clone();
                execParamUpdate.Record_Id = hivatkozotteBeadvany.Id;

                Result res = this.Update(execParamUpdate, hivatkozotteBeadvany);

                if (res.IsError)
                {
                    throw new ResultException(res);
                }

            }
            else
            {
                Logger.Debug("Az eBeadvany m�r �rkeztetve van!");
            }

            if (!attached)
            {
                //csatolm�ny kapcsol�sa vev�nyhez
                AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, erec_eBeadvanyok, hivatkozotteBeadvany);
            }

            Invalidate(execParam, erec_eBeadvanyok);
        }

        Logger.Info(String.Format("LetoltesiIgazolas vege: {0}", erec_eBeadvanyok.Id));
    }

    private EREC_eBeadvanyok MeghiusulasiIgazolas(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, bool postaHibrid)
    {
        Logger.Info(String.Format("MeghiusulasiIgazolas kezdete: {0}", erec_eBeadvanyok.Id));

        bool attached = false;

        if (!postaHibrid)
        {

            EREC_KuldTertivevenyek tertiveveny = GetTertiveveny(execParam, erec_eBeadvanyok.KR_HivatkozasiSzam);

            if (tertiveveny == null)
            {
                throw new Exception("A meghi�sul�si igazol�s feldogoz�sa sikertelen. A k�ldem�ny m�g nem lett post�zva!");
            }

            if (tertiveveny != null)
            {
                Logger.Debug(String.Format("tertiveveny: {0}", tertiveveny.Id));

                if (tertiveveny.Allapot == KodTarak.TERTIVEVENY_ALLAPOT.Elokeszitett)
                {
                    tertiveveny.Updated.SetValueAll(false);
                    tertiveveny.Base.Updated.SetValueAll(false);
                    tertiveveny.Base.Updated.Ver = true;

                    string tertiVisszaKod = Rendszerparameterek.Get(execParam, "MEGHIUSULASI_IGAZOLAS_TERTI_VISSZA_KOD");
                    tertiveveny.TertivisszaKod = !String.IsNullOrEmpty(tertiVisszaKod) ? tertiVisszaKod : KodTarak.TERTIVEVENY_VISSZA_KOD.Visszakuldes_oka_nem_kereste;
                    tertiveveny.Updated.TertivisszaKod = true;

                    tertiveveny.TertivisszaDat = DateTime.Now.ToString();
                    tertiveveny.Updated.TertivisszaDat = true;

                    tertiveveny.KezbVelelemDatuma = erec_eBeadvanyok.KR_ErkeztetesiDatum;
                    tertiveveny.Updated.KezbVelelemDatuma = true;

                    tertiveveny.KezbVelelemBeallta = "1";
                    tertiveveny.Updated.KezbVelelemBeallta = true;

                    EREC_KuldTertivevenyekService service = new EREC_KuldTertivevenyekService(this.dataContext);
                    Result res = service.TertivevenyErkeztetes(execParam, tertiveveny.Id, tertiveveny);

                    if (res.IsError)
                    {
                        throw new ResultException(res);
                    }

                    AttacheBeadVanyCsatolmanyToTertiveveny(execParam, erec_eBeadvanyok, tertiveveny);
                    attached = true;
                }
                else
                {
                    Logger.Debug("A t�rtivev�ny m�r �rkeztetve van!");
                }
            }
        }

        EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, erec_eBeadvanyok.KR_HivatkozasiSzam);

        if (hivatkozotteBeadvany != null)
        {
            Logger.Debug(String.Format("hivatkozotteBeadvany: {0}", hivatkozotteBeadvany.Id));

            if (hivatkozotteBeadvany.Allapot == "11")
            {
                hivatkozotteBeadvany.Updated.SetValueAll(false);
                hivatkozotteBeadvany.Base.Updated.SetValueAll(false);
                hivatkozotteBeadvany.Base.Updated.Ver = true;

                hivatkozotteBeadvany.Updated.Allapot = true;
                hivatkozotteBeadvany.Allapot = "13"; //C�mzett nem vette �t

                ExecParam execParamUpdate = execParam.Clone();
                execParamUpdate.Record_Id = hivatkozotteBeadvany.Id;

                Result res = this.Update(execParamUpdate, hivatkozotteBeadvany);

                if (res.IsError)
                {
                    throw new ResultException(res);
                }
            }
            else
            {
                Logger.Debug("Az eBeadvany m�r �rkeztetve van!");
            }

            if (!attached)
            {
                //csatolm�ny kapcsol�sa vev�nyhez
                AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, erec_eBeadvanyok, hivatkozotteBeadvany);
            }

            Invalidate(execParam, erec_eBeadvanyok);

            return hivatkozotteBeadvany;
        }


        Logger.Info(String.Format("MeghiusulasiIgazolas vege: {0}", erec_eBeadvanyok.Id));

        return null;
    }

    private void AutomatikusVisszaigazolas(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok)
    {
        Logger.Info(String.Format("AutomatikusVisszaigazolas kezdete: {0}", erec_eBeadvanyok.Id));

        string hivatkozasiSzam = erec_eBeadvanyok.KR_HivatkozasiSzam;

        Logger.Debug(String.Format("KR_HivatkozasiSzam: {0}", hivatkozasiSzam));

        if (!String.IsNullOrEmpty(hivatkozasiSzam))
        {
            EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, hivatkozasiSzam);

            if (hivatkozotteBeadvany != null)
            {
                //csatolmany kapcsolasa melleklethez
                AttacheBeadVanyCsatolmanyToAutomatikusVisszaigazolas(execParam, erec_eBeadvanyok, hivatkozotteBeadvany);

                Invalidate(execParam, erec_eBeadvanyok);
            }
        }

        Logger.Info(String.Format("AutomatikusVisszaigazolas vege: {0}", erec_eBeadvanyok.Id));


    }

    #endregion

    #region seg�d f�ggv�nyek

    private EREC_eBeadvanyok GeteBeadvanyByErkeztetesiSzam(ExecParam execParam, string erkeztetesiSzam)
    {
        Logger.Info(String.Format("GeteBeadvanyByHivatkozasiSzam kezdete: {0}", erkeztetesiSzam));

        if (String.IsNullOrEmpty(erkeztetesiSzam))
            return null;

        EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
        search.KR_ErkeztetesiSzam.Value = erkeztetesiSzam;
        search.KR_ErkeztetesiSzam.Operator = Query.Operators.equals;
        //Kimen�
        search.Irany.Value = "1";
        search.Irany.Operator = Query.Operators.equals;

        Result res = GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Warn(String.Format("Nem tal�lhat� a hivatkoz�si sz�m: {0}", erkeztetesiSzam));
            return null;
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Warn(String.Format("A hivatkoz�si sz�m t�bbsz�r is szerepel: {0}", erkeztetesiSzam));
            return null;
        }

        EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
        Utility.LoadBusinessDocumentFromDataRow(eBeadvany, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GeteBeadvanyByHivatkozasiSzam vege: {0}", erkeztetesiSzam));

        return eBeadvany;
    }

    private string Postazas(ExecParam execParam, string kimenoKuldemenyId, string postazasDatuma, string ragszam)
    {
        Logger.Info(String.Format("Postazas kezdete: {0}, {1}, {2}", kimenoKuldemenyId, postazasDatuma, ragszam));

        EREC_KuldKuldemenyek kuldemeny = GetKimenoKuldemeny(execParam, kimenoKuldemenyId);

        if (kuldemeny.Allapot != KodTarak.KULDEMENY_ALLAPOT.Postazott)
        {

            kuldemeny.Updated.SetValueAll(false);
            kuldemeny.Base.Updated.SetValueAll(false);
            kuldemeny.Base.Updated.Ver = true;

            kuldemeny.BelyegzoDatuma = postazasDatuma;
            kuldemeny.Updated.BelyegzoDatuma = true;

            kuldemeny.Tertiveveny = "1";
            kuldemeny.Updated.Tertiveveny = true;

            kuldemeny.RagSzam = ragszam;
            kuldemeny.Updated.RagSzam = true;

            EREC_KuldKuldemenyekService service = new EREC_KuldKuldemenyekService(this.dataContext);

            ExecParam execParamPosta = execParam.Clone();
            execParamPosta.Felhasznalo_Id = kuldemeny.FelhasznaloCsoport_Id_Orzo;
            Result res = service.Postazas(execParamPosta, kuldemeny, "");

            if (res.IsError)
            {
                throw new ResultException(res);
            }
        }
        else
        {
            Logger.Warn("A k�ldem�ny m�r post�zott!");
        }

        Logger.Info(String.Format("Postazas vege: {0}, {1}, {2}", kimenoKuldemenyId, postazasDatuma, ragszam));

        return kuldemeny.Id;
    }

    private EREC_KuldKuldemenyek GetKimenoKuldemeny(ExecParam execParam, string kuldemenyId)
    {
        Logger.Info(String.Format("GetKimenoKuldemeny kezdete: {0}", kuldemenyId));

        EREC_KuldKuldemenyekService kuldService = new EREC_KuldKuldemenyekService(this.dataContext);
        ExecParam kuldExecParam = execParam.Clone();
        kuldExecParam.Record_Id = kuldemenyId;
        Result kuldRes = kuldService.Get(kuldExecParam);

        if (kuldRes.IsError)
        {
            throw new ResultException(kuldRes);
        }

        EREC_KuldKuldemenyek kuldemeny = kuldRes.Record as EREC_KuldKuldemenyek;

        Logger.Info(String.Format("GetKimenoKuldemeny vege: {0}", kuldemenyId));

        return kuldemeny;
    }

    private string GetKimenoKuldemenyByIratPeldany(ExecParam execParam, string peldanyId)
    {
        Logger.Info(String.Format("GetKimenoKuldemenyByIratPeldany kezdete: {0}", peldanyId));

        EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();
        search.Peldany_Id.Value = peldanyId;
        search.Peldany_Id.Operator = Query.Operators.equals;

        EREC_Kuldemeny_IratPeldanyaiService service = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            throw new ResultException(String.Format("Nem tal�lhat� kimen� k�ldem�ny: {0}", peldanyId));
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            throw new ResultException(String.Format("T�bb kimen� k�ldem�ny tal�lhat�: {0}", peldanyId));
        }

        string kuldemenyId = res.Ds.Tables[0].Rows[0]["KuldKuldemeny_Id"].ToString();

        Logger.Info(String.Format("GetKimenoKuldemenyByIratPeldany vege: {0}", peldanyId));

        return kuldemenyId;
    }

    private string GetKimenoKuldemenyId(ExecParam execParam, EREC_eBeadvanyok eBeadvany)
    {
        Logger.Info(String.Format("GetKimenoKuldemenyId kezdete"));
        string kuldemenyId = String.Empty;

        if (!String.IsNullOrEmpty(eBeadvany.KuldKuldemeny_Id))
        {
            kuldemenyId = eBeadvany.KuldKuldemeny_Id;
        }
        else
        {
            kuldemenyId = GetKimenoKuldemenyByIratPeldany(execParam, eBeadvany.IratPeldany_Id);
        }

        Logger.Info(String.Format("GetKimenoKuldemenyId vege"));
        return kuldemenyId;
    }

    private EREC_eBeadvanyCsatolmanyok GeteBeadvanyCsatolmany(ExecParam execParam, string eBeadvanyId)
    {
        Logger.Info(String.Format("GeteBeadvanyCsatolmany kezdete: {0}", eBeadvanyId));

        EREC_eBeadvanyCsatolmanyokService service = new EREC_eBeadvanyCsatolmanyokService(this.dataContext);
        EREC_eBeadvanyCsatolmanyokSearch search = new EREC_eBeadvanyCsatolmanyokSearch();
        search.eBeadvany_Id.Value = eBeadvanyId;
        search.eBeadvany_Id.Operator = Query.Operators.equals;

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Warn(String.Format("Nem tal�lhat� eBeadvanyCsatolmany: {0}", eBeadvanyId));
            return null;
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Warn(String.Format("T�bb eBeadvanyCsatolmany tal�lhat�: {0}", eBeadvanyId));
            return null;
        }

        EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = new EREC_eBeadvanyCsatolmanyok();
        Utility.LoadBusinessDocumentFromDataRow(eBeadvanyCsatolmany, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GeteBeadvanyCsatolmany vege: {0}", eBeadvanyId));

        return eBeadvanyCsatolmany;
    }

    private string AddCsatolmanyToKuldemeny(ExecParam execParam, string kuldemenyId, string dokumentumId)
    {
        Logger.Info(String.Format("AddCsatolmanyToKuldemeny kezdete: {0}, {1}", kuldemenyId, dokumentumId));

        EREC_Csatolmanyok csatolmany = new EREC_Csatolmanyok();
        csatolmany.KuldKuldemeny_Id = kuldemenyId;
        csatolmany.Dokumentum_Id = dokumentumId;
        //csatolmany.DokumentumSzerep = Contentum.eUtility.KodTarak.DOKUMENTUM_SZEREP.DokumentumMelleklet;
        EREC_CsatolmanyokService service = new EREC_CsatolmanyokService(this.dataContext);

        Result res = service.Insert(execParam, csatolmany);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        Logger.Info(String.Format("AddCsatolmanyToKuldemeny vege: {0}, {1}", kuldemenyId, dokumentumId));

        return res.Uid;
    }

    private EREC_Mellekletek GetMelleklet(ExecParam execParam, string kuldemenyId)
    {
        Logger.Info(String.Format("GetMelleklet kezdete: {0}", kuldemenyId));

        EREC_MellekletekSearch search = new EREC_MellekletekSearch();
        search.KuldKuldemeny_Id.Value = kuldemenyId;
        search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.Tertiveveny;
        search.AdathordozoTipus.Operator = Query.Operators.equals;

        EREC_MellekletekService service = new EREC_MellekletekService(this.dataContext);

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Warn(String.Format("Nem tal�lhat� a mell�klet: {0}", kuldemenyId));
            return null;
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Warn(String.Format("T�bb mell�klet tal�lhat�: {0}", kuldemenyId));
            return null;
        }

        EREC_Mellekletek melleklet = new EREC_Mellekletek();
        Utility.LoadBusinessDocumentFromDataRow(melleklet, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GetMelleklet vege: {0}", kuldemenyId));

        return melleklet;
    }

    private void AddIratelemKapcsolat(ExecParam execParam, string mellekletId, string csatolmanyId)
    {
        Logger.Info(String.Format("AddIratelemKapcsolat kezdete: {0}, {1}", mellekletId, csatolmanyId));

        EREC_IratelemKapcsolatokService service = new EREC_IratelemKapcsolatokService(this.dataContext);
        EREC_IratelemKapcsolatok kapcs = new EREC_IratelemKapcsolatok();
        kapcs.Csatolmany_Id = csatolmanyId;
        kapcs.Melleklet_Id = mellekletId;
        Result res = service.Insert(execParam, kapcs);


        if (res.IsError)
        {
            throw new ResultException(res);
        }

        Logger.Info(String.Format("AddIratelemKapcsolat vege: {0}, {1}", mellekletId, csatolmanyId));
    }

    private EREC_KuldTertivevenyek GetTertiveveny(ExecParam execParam, string hivatkozasiSzam)
    {
        Logger.Info(String.Format("GetTertiveveny kezdete: {0}", hivatkozasiSzam));

        EREC_KuldTertivevenyekSearch search = new EREC_KuldTertivevenyekSearch();
        search.Ragszam.Value = hivatkozasiSzam;
        search.Ragszam.Operator = Query.Operators.equals;

        EREC_KuldTertivevenyekService service = new EREC_KuldTertivevenyekService(this.dataContext);

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Warn(String.Format("Nem tal�lhat� a t�rtivev�ny: {0}", hivatkozasiSzam));
            return null;
        }

        if (res.Ds.Tables[0].Rows.Count > 1)
        {
            Logger.Warn(String.Format("T�bb t�rtivev�ny tal�lhat�: {0}", hivatkozasiSzam));
            return null;
        }

        EREC_KuldTertivevenyek tertiveveny = new EREC_KuldTertivevenyek();
        Utility.LoadBusinessDocumentFromDataRow(tertiveveny, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GetTertiveveny vege: {0}", hivatkozasiSzam));

        return tertiveveny;
    }

    private void CimzettNemVetteAt(ExecParam ExecParam)
    {
        bool isConnectionOpenHere = false;
        EREC_eBeadvanyok EREC_eBeadvanyok = null;
        string azonosito = String.Empty;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Result resGet = this.Get(ExecParam);

            if (resGet.IsError)
            {
                throw new ResultException(resGet);
            }

            EREC_eBeadvanyok = resGet.Record as EREC_eBeadvanyok;

            string peldanyId = GetIratPeldanyId(ExecParam, EREC_eBeadvanyok);
            EREC_eBeadvanyok.IratPeldany_Id = peldanyId;

            if (!String.IsNullOrEmpty(EREC_eBeadvanyok.IratPeldany_Id))
            {
                EREC_PldIratPeldanyokService pldService = new EREC_PldIratPeldanyokService(this.dataContext);
                ExecParam xpmPld = ExecParam.Clone();
                xpmPld.Record_Id = EREC_eBeadvanyok.IratPeldany_Id;

                Result resPld = pldService.Get(xpmPld);

                if (resPld.IsError)
                {
                    throw new ResultException(resPld);
                }

                EREC_PldIratPeldanyok peldany = resPld.Record as EREC_PldIratPeldanyok;
                azonosito = peldany.Azonosito;
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Contentum.eAdmin.Service.EmailService emailService = Contentum.eUtility.eAdminService.ServiceFactory.GetEmailService();
        ExecParam xpmEmail = ExecParam.Clone();

        bool ret = emailService.SendEmaileBeadvanyCimzettNemVetteAt(xpmEmail, EREC_eBeadvanyok, azonosito);

        if (!ret)
        {
            throw new Exception("E-mail k�ld�s hiba!");
        }
    }

    private void AttacheBeadVanyCsatolmanyToTertiveveny(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, EREC_KuldTertivevenyek tertiveveny)
    {
        Logger.Info("AttacheBeadVanyCsatolmanyToTertiveveny kezdete");
        Logger.Debug(String.Format("erec_eBeadvanyok: {0}", erec_eBeadvanyok.KR_ErkeztetesiSzam));
        Logger.Debug(String.Format("tertiveveny: {0}", tertiveveny.Ragszam));

        string kimenoKuldemenyId = tertiveveny.Kuldemeny_Id;

        EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = GeteBeadvanyCsatolmany(execParam, erec_eBeadvanyok.Id);
        if (eBeadvanyCsatolmany != null)
        {
            Logger.Debug(String.Format("eBeadvanyCsatolmany: {0}", eBeadvanyCsatolmany.Id));

            //dokumentum hozz�kapcsol�sa k�ldem�nyhez
            string csatolmanyId = AddCsatolmanyToKuldemeny(execParam, kimenoKuldemenyId, eBeadvanyCsatolmany.Dokumentum_Id);
            Logger.Debug(String.Format("csatolmanyId: {0}", csatolmanyId));

            //csatolm�ny kapcsol�sa t�rtivev�nyhez
            EREC_Mellekletek melleklet = GetMelleklet(execParam, kimenoKuldemenyId);
            if (melleklet != null)
            {
                Logger.Debug(String.Format("melleklet: {0}", melleklet.Id));
                AddIratelemKapcsolat(execParam, melleklet.Id, csatolmanyId);
            }

            //csatolm�ny kapcsol�sa vev�nyhez
            AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, kimenoKuldemenyId, csatolmanyId, KodTarak.AdathordozoTipus.KapuRendszerVeveny);
        }

        Logger.Info("AttacheBeadVanyCsatolmanyToTertiveveny vege");
    }



    private void AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, EREC_eBeadvanyok hivatkozotteBeadvany)
    {
        Logger.Info("AttacheBeadVanyCsatolmanyToKapuRendszerVeveny kezdete");
        Logger.Debug(String.Format("erec_eBeadvanyok: {0}", erec_eBeadvanyok.KR_ErkeztetesiSzam));
        Logger.Debug(String.Format("hivatkozotteBeadvany: {0}", hivatkozotteBeadvany.KR_ErkeztetesiSzam));

        string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);

        EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = GeteBeadvanyCsatolmany(execParam, erec_eBeadvanyok.Id);
        if (eBeadvanyCsatolmany != null)
        {
            Logger.Debug(String.Format("eBeadvanyCsatolmany: {0}", eBeadvanyCsatolmany.Id));

            //dokumentum hozz�kapcsol�sa k�ldem�nyhez
            string csatolmanyId = AddCsatolmanyToKuldemeny(execParam, kimenoKuldemenyId, eBeadvanyCsatolmany.Dokumentum_Id);
            Logger.Debug(String.Format("csatolmanyId: {0}", csatolmanyId));

            //csatolm�ny kapcsol�sa vev�nyhez
            AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, kimenoKuldemenyId, csatolmanyId, KodTarak.AdathordozoTipus.KapuRendszerVeveny);
        }

        Logger.Info("AttacheBeadVanyCsatolmanyToKapuRendszerVeveny vege");
    }

    private void AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(ExecParam execParam, string kimenoKuldemenyId, string csatolmanyId, string adathordozoTipusa)
    {
        bool ujMellektlet = false;
        //csatolm�ny kapcsol�sa t�rtivev�nyhez
        string mellekletId = GetKapuRendszerVeveny(execParam, kimenoKuldemenyId, adathordozoTipusa);

        if (String.IsNullOrEmpty(mellekletId))
        {
            mellekletId = AddKapuRendszerVeveny(execParam, kimenoKuldemenyId, adathordozoTipusa);
            ujMellektlet = true;
        }

        if (!String.IsNullOrEmpty(mellekletId))
        {
            Logger.Debug(String.Format("melleklet: {0}", mellekletId));
            AddIratelemKapcsolat(execParam, mellekletId, csatolmanyId);
            if (!ujMellektlet)
            {
                RefreshMellekletMennyiseg(execParam, mellekletId);
            }
        }
    }

    private string GetKapuRendszerVeveny(ExecParam execParam, string kuldemenyId, string adathordozoTipusa)
    {
        Logger.Info(String.Format("GetKapuRendszerVeveny kezdete: {0}", kuldemenyId));

        EREC_MellekletekSearch search = new EREC_MellekletekSearch();
        search.KuldKuldemeny_Id.Value = kuldemenyId;
        search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        search.AdathordozoTipus.Value = adathordozoTipusa;
        search.AdathordozoTipus.Operator = Query.Operators.equals;

        EREC_MellekletekService service = new EREC_MellekletekService(this.dataContext);

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        if (res.Ds.Tables[0].Rows.Count == 0)
        {
            Logger.Debug(String.Format("Nem tal�lhat� a mell�klet: {0}", kuldemenyId));
            return null;
        }

        EREC_Mellekletek melleklet = new EREC_Mellekletek();
        Utility.LoadBusinessDocumentFromDataRow(melleklet, res.Ds.Tables[0].Rows[0]);

        Logger.Info(String.Format("GetKapuRendszerVeveny vege: {0}", kuldemenyId));

        return melleklet.Id;
    }

    private string AddKapuRendszerVeveny(ExecParam execParam, string kuldemenyId, string adathordozoTipusa)
    {
        Logger.Info(String.Format("AddKapuRendszerVeveny kezdete: {0}", kuldemenyId));

        EREC_MellekletekService service_mellekletek = new EREC_MellekletekService(this.dataContext);
        EREC_Mellekletek erec_Mellekletek = new EREC_Mellekletek();
        erec_Mellekletek.Updated.SetValueAll(false);
        erec_Mellekletek.Base.Updated.SetValueAll(false);

        erec_Mellekletek.KuldKuldemeny_Id = kuldemenyId;
        erec_Mellekletek.Updated.KuldKuldemeny_Id = true;

        erec_Mellekletek.AdathordozoTipus = adathordozoTipusa;
        erec_Mellekletek.Updated.AdathordozoTipus = true;

        erec_Mellekletek.MennyisegiEgyseg = KodTarak.MennyisegiEgyseg.Darab;
        erec_Mellekletek.Updated.MennyisegiEgyseg = true;

        erec_Mellekletek.Mennyiseg = "1";
        erec_Mellekletek.Updated.Mennyiseg = true;

        Result result = service_mellekletek.Insert(execParam.Clone(), erec_Mellekletek, false);

        if (result.IsError)
        {
            throw new ResultException(result);
        }

        Logger.Info(String.Format("AddKapuRendszerVeveny vege: {0}", kuldemenyId));

        return result.Uid;
    }

    private void Invalidate(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok)
    {
        Logger.Info(String.Format("Invalidate kezdete: {0}", erec_eBeadvanyok.Id));

        ExecParam execParamInvalidate = execParam.Clone();
        execParamInvalidate.Record_Id = erec_eBeadvanyok.Id;

        Result res = this.Invalidate(execParamInvalidate);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        Logger.Info(String.Format("Invalidate vege: {0}", erec_eBeadvanyok.Id));
    }

    void RefreshMellekletMennyiseg(ExecParam execParam, string mellekletId)
    {
        Logger.Info(String.Format("RefreshMellekletMennyiseg kezdete: {0}", mellekletId));

        try
        {
            EREC_IratelemKapcsolatokService service = new EREC_IratelemKapcsolatokService(this.dataContext);
            EREC_IratelemKapcsolatokSearch search = new EREC_IratelemKapcsolatokSearch();
            search.Melleklet_Id.Value = mellekletId;
            search.Melleklet_Id.Operator = Query.Operators.equals;

            Result resGetAll = service.GetAll(execParam, search);


            if (resGetAll.IsError)
            {
                throw new ResultException(resGetAll);
            }

            int mennyiseg = resGetAll.Ds.Tables[0].Rows.Count;

            EREC_MellekletekService mellekletekService = new EREC_MellekletekService(this.dataContext);
            ExecParam getExecParam = execParam.Clone();
            getExecParam.Record_Id = mellekletId;

            Result getResult = mellekletekService.Get(getExecParam);

            if (getResult.IsError)
            {
                throw new ResultException(getResult);
            }

            EREC_Mellekletek melleklet = getResult.Record as EREC_Mellekletek;

            melleklet.Updated.SetValueAll(false);
            melleklet.Base.Updated.SetValueAll(false);
            melleklet.Base.Updated.Ver = true;

            melleklet.Mennyiseg = mennyiseg.ToString();
            melleklet.Updated.Mennyiseg = true;

            Result updateResult = mellekletekService.Update(getExecParam, melleklet);

            if (updateResult.IsError)
            {
                throw new ResultException(updateResult);
            }
        }
        catch (Exception ex)
        {
            Result error = ResultException.GetResultFromException(ex);
            Logger.Error("RefreshMellekletMennyiseg hiba", execParam, error);
            throw;
        }

        Logger.Info(String.Format("RefreshMellekletMennyiseg vege: {0}", mellekletId));
    }

    private string GetIratPeldanyId(ExecParam execParam, EREC_eBeadvanyok eBeadvany)
    {
        Logger.Info(String.Format("GetIratPeldanyId kezdete"));
        string peldanyId = String.Empty;

        if (!String.IsNullOrEmpty(eBeadvany.IratPeldany_Id))
        {
            peldanyId = eBeadvany.IratPeldany_Id;
        }
        else
        {
            if (!String.IsNullOrEmpty(eBeadvany.KuldKuldemeny_Id))
            {
                peldanyId = GetIratPeldanyIdByKimenoKuldemeny(execParam, eBeadvany.KuldKuldemeny_Id);
            }
        }

        Logger.Info(String.Format("GetIratPeldanyId vege"));
        return peldanyId;
    }

    private string GetIratPeldanyIdByKimenoKuldemeny(ExecParam execParam, string kuldemenyId)
    {
        Logger.Info(String.Format("GetKimenoKuldemenyIratPeldanyId kezdete: {0}", kuldemenyId));

        EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();
        search.KuldKuldemeny_Id.Value = kuldemenyId;
        search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

        EREC_Kuldemeny_IratPeldanyaiService service = new EREC_Kuldemeny_IratPeldanyaiService(this.dataContext);
        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        string peldanyId = String.Empty;

        if (res.Ds.Tables[0].Rows.Count > 0)
        {
            peldanyId = res.Ds.Tables[0].Rows[0]["Peldany_Id"].ToString();
        }

        Logger.Info(String.Format("GetKimenoKuldemenyIratPeldanyId vege"));

        return peldanyId;
    }

    private void AttacheBeadVanyCsatolmanyToAutomatikusVisszaigazolas(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, EREC_eBeadvanyok hivatkozotteBeadvany)
    {
        Logger.Info("AttacheBeadVanyCsatolmanyToAutomatikusVisszaigazolas kezdete");
        Logger.Debug(String.Format("erec_eBeadvanyok: {0}", erec_eBeadvanyok.KR_ErkeztetesiSzam));
        Logger.Debug(String.Format("hivatkozotteBeadvany: {0}", hivatkozotteBeadvany.KR_ErkeztetesiSzam));

        string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);

        EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = GeteBeadvanyCsatolmany(execParam, erec_eBeadvanyok.Id);
        if (eBeadvanyCsatolmany != null)
        {
            Logger.Debug(String.Format("eBeadvanyCsatolmany: {0}", eBeadvanyCsatolmany.Id));

            //dokumentum hozzakapcsolasa a kuldemenyhez
            string csatolmanyId = AddCsatolmanyToKuldemeny(execParam, kimenoKuldemenyId, eBeadvanyCsatolmany.Dokumentum_Id);
            Logger.Debug(String.Format("csatolmanyId: {0}", csatolmanyId));

            //csatolmany hozzakapcsolasa automatikus visszaigazolas melleklethez
            AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, kimenoKuldemenyId, csatolmanyId, KodTarak.AdathordozoTipus.AutomatikusVisszaigazolas);
        }

        Logger.Info("AttacheBeadVanyCsatolmanyToAutomatikusVisszaigazolas vege");
    }

    #endregion

    #region k�ld�si adatok

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result GetKuldesiAdatok(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();
            search.Irany.Value = KodTarak.eBeadvany_Irany.KuldesiAdatok;
            search.Irany.Operator = Query.Operators.equals;
            search.IraIrat_Id.Value = iratId;
            search.IraIrat_Id.Operator = Query.Operators.equals;

            Result resGetAll = this.GetAll(execParam, search);

            if (resGetAll.IsError)
                throw new ResultException(resGetAll);

            DataTable table = resGetAll.Ds.Tables[0];

            if (table.Rows.Count > 1)
                throw new Exception("A k�ld�si adatok lek�r�se t�bb mint egy sort adott vissza!");

            if (table.Rows.Count > 0)
            {
                DataRow row = table.Rows[0];
                EREC_eBeadvanyok _EREC_eBeadvanyok = new EREC_eBeadvanyok();
                Utility.LoadBusinessDocumentFromDataRow(_EREC_eBeadvanyok, row);
                result.Record = _EREC_eBeadvanyok;
            }
            else
            {
                //nincs k�ld�si adat megadva az irathoz
            }


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result SetKuldesiAdatok(ExecParam execParam, string iratId, EREC_eBeadvanyok eBeadvany)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            Result resGet = this.GetKuldesiAdatok(execParam, iratId);

            if (resGet.IsError)
                throw new ResultException(resGet);

            EREC_eBeadvanyok kuldesiAdatok = resGet.Record as EREC_eBeadvanyok;

            if (kuldesiAdatok != null)
            {
                Logger.Debug("SetKuldesiAdatok - Update");

                ExecParam updateExecParam = execParam.Clone();
                updateExecParam.Record_Id = kuldesiAdatok.Id;

                result = this.Update(updateExecParam, eBeadvany);

            }
            else
            {
                Logger.Debug("SetKuldesiAdatok - Insert");

                eBeadvany.Irany = KodTarak.eBeadvany_Irany.KuldesiAdatok;
                eBeadvany.Updated.Irany = true;

                eBeadvany.IraIrat_Id = iratId;
                eBeadvany.Updated.IraIrat_Id = true;

                result = this.Insert(execParam, eBeadvany);
            }


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eBeadvanyok))]
    public Result SetKuldesiAdatokFromXML(ExecParam execParam, string iratId, string elhiszParametersXML)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            //xml feldolgoz�sa
            XDocument doc = XDocument.Parse(elhiszParametersXML);

            var parameters = (from element in doc.Root.Elements()
                              where element.Name.LocalName == "ELHISZParameters"
                              select element).Single();

            var metaAdatok = from element in parameters.Elements()
                             where element.Name.LocalName == "HKPMeta"
                             select element;

            Dictionary<string, string> metaAdatokDict = new Dictionary<string, string>();

            foreach (XElement meta in metaAdatok)
            {
                string name = meta.Attribute("name").Value;
                string value = meta.Value;
                metaAdatokDict.Add(name, value);
            }

            var inputFormatNode = (from element in parameters.Elements()
                                   where element.Name.LocalName == "InputFormat"
                                   select element).FirstOrDefault();

            var outputFormatNode = (from element in parameters.Elements()
                                    where element.Name.LocalName == "OutputFormat"
                                    select element).FirstOrDefault();

            string inputFormat = String.Empty;

            if (inputFormatNode != null)
            {
                if (inputFormatNode.Attribute("format") != null)
                    inputFormat = inputFormatNode.Attribute("format").Value;
            }

            string outputFormat = String.Empty;
            string outputFormatDefinition = String.Empty;

            if (outputFormatNode != null)
            {
                if (outputFormatNode.Attribute("format") != null)
                    outputFormat = outputFormatNode.Attribute("format").Value;

                outputFormatDefinition = outputFormatNode.Value;
            }

            Result resGet = this.GetKuldesiAdatok(execParam, iratId);

            if (resGet.IsError)
                throw new ResultException(resGet);

            EREC_eBeadvanyok eBeadvany = resGet.Record as EREC_eBeadvanyok;

            if (eBeadvany == null)
            {
                eBeadvany = new EREC_eBeadvanyok();
            }

            eBeadvany.Updated.SetValueAll(false);
            eBeadvany.Base.Updated.SetValueAll(false);

            if (metaAdatokDict.ContainsKey("HivatkozasiSzam"))
            {
                eBeadvany.KR_HivatkozasiSzam = metaAdatokDict["HivatkozasiSzam"];
                eBeadvany.Updated.KR_HivatkozasiSzam = true;
            }

            if (metaAdatokDict.ContainsKey("DokTipusHivatal"))
            {
                eBeadvany.KR_DokTipusHivatal = metaAdatokDict["DokTipusHivatal"];
                eBeadvany.Updated.KR_DokTipusHivatal = true;
            }
            if (metaAdatokDict.ContainsKey("DokTipusAzonosito"))
            {
                eBeadvany.KR_DokTipusAzonosito = metaAdatokDict["DokTipusAzonosito"];
                eBeadvany.Updated.KR_DokTipusAzonosito = true;
            }
            if (metaAdatokDict.ContainsKey("DokTipusLeiras"))
            {
                eBeadvany.KR_DokTipusLeiras = metaAdatokDict["DokTipusLeiras"];
                eBeadvany.Updated.KR_DokTipusLeiras = true;
            }
            if (metaAdatokDict.ContainsKey("Megjegyzes"))
            {
                eBeadvany.KR_Megjegyzes = metaAdatokDict["Megjegyzes"];
                eBeadvany.Updated.KR_Megjegyzes = true;
            }
            if (metaAdatokDict.ContainsKey("ValaszUtvonal"))
            {
                eBeadvany.KR_Valaszutvonal = metaAdatokDict["ValaszUtvonal"];
                eBeadvany.Updated.KR_Valaszutvonal = true;
            }
            if (metaAdatokDict.ContainsKey("FileNev"))
            {
                eBeadvany.KR_FileNev = metaAdatokDict["FileNev"];
                eBeadvany.Updated.KR_FileNev = true;
            }
            if (metaAdatokDict.ContainsKey("ValaszTitkositas"))
            {
                eBeadvany.KR_Valasztitkositas = metaAdatokDict["ValaszTitkositas"];
                eBeadvany.Updated.KR_Valasztitkositas = true;
            }

            PR_Parameterek parameterek = new PR_Parameterek();
            parameterek.InputFormat = inputFormat;
            parameterek.OutputFormat = outputFormat;
            parameterek.OutputFormatDefinition = outputFormatDefinition;

            eBeadvany.Set_PR_Parameterek(parameterek);
            eBeadvany.Updated.PR_Parameterek = true;

            eBeadvany.Base.Updated.Ver = true;

            result = this.SetKuldesiAdatok(execParam, iratId, eBeadvany);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion

    #region AUTO EUZENET SZABALY
    /// <summary>
    /// Execute eUzenet szabaly
    /// </summary>
    /// <param name="eBeadvanyId"></param>
    /// <param name="execParam"></param>
    [WebMethod]
    public void ExecuteEUzenetSzabaly(string eBeadvanyId, ExecParam execParam)
    {
        Logger.Info("ExecuteEUzenetSzabaly kezdete. " + eBeadvanyId);

        #region SYNC
        ExecuteEUzenetSzabalyDoWork(eBeadvanyId, execParam);
        #endregion

        //#region ASYNC
        //object[] parameters = new object[] { eBeadvanyId, execParam };
        //System.ComponentModel.BackgroundWorker worker = new System.ComponentModel.BackgroundWorker();
        //worker.DoWork += ExecuteEUzenetSzabaly_DoWork;
        //worker.RunWorkerAsync(parameters);
        //#endregion

        Logger.Info("ExecuteEUzenetSzabaly vege");
    }
    /// <summary>
    /// background procedure : Run eUzenet szabaly
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ExecuteEUzenetSzabaly_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
    {
        try
        {
            object[] parameters = (object[])e.Argument;
            if (parameters == null || parameters.Length != 2)
                return;

            string id = parameters[0].ToString();
            ExecParam execPaam = (ExecParam)parameters[1];
            EKFManager man = new EKFManager(new Guid(id), execPaam);
            man.Start();
            if (man.OccuredMessages.Count > 0)
            {
                foreach (string item in man.OccuredMessages)
                {
                    Logger.Error("EREC_eBeadvanyokService.ExecuteEUzenetSzabaly_DoWork.EKFManager " + item);
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("EREC_eBeadvanyokService.ExecuteEUzenetSzabaly_DoWork", ex);
        }
    }
    /// <summary>
    /// ExecuteEUzenetSzabalyDoWork
    /// </summary>
    /// <param name="eBeadvanyId"></param>
    /// <param name="execParam"></param>
    /// <returns></returns>
    private Result ExecuteEUzenetSzabalyDoWork(string eBeadvanyId, ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            if (string.IsNullOrEmpty(eBeadvanyId))
                throw new Exception("ExecuteEUzenetSzabalyDoWork Hibas parameter !");

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Logger.Error("EREC_eBeadvanyokService.ExecuteEUzenetSzabalyDoWork.EKFManager.Start " + eBeadvanyId);

            EKFManager man = new EKFManager(new Guid(eBeadvanyId), execParam);
            man.Start();
            if (man.OccuredMessages.Count > 0)
            {
                foreach (string item in man.OccuredMessages)
                {
                    Logger.Error("EREC_eBeadvanyokService.ExecuteEUzenetSzabalyDoWork.EKFManager " + item);
                }
            }

            Logger.Error("EREC_eBeadvanyokService.ExecuteEUzenetSzabalyDoWork.EKFManager.Stop " + eBeadvanyId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
    #endregion AUTO EUZENET SZABALY

    #region Adatkapu

    public Result SetRegistrationIdentifier(ExecParam execParam, string iratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();

        try
        {
            if (IsEUzenetForrasElhisz(execParam))
            {
                Logger.Debug("SetRegistrationIdentifier start");
                EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);
                ExecParam iratokExecParam = execParam.Clone();
                iratokExecParam.Record_Id = iratId;

                Result iratokResult = iratokService.Get(iratokExecParam);

                if (iratokResult.IsError)
                    throw new ResultException(iratokResult);

                EREC_IraIratok iratok = iratokResult.Record as EREC_IraIratok;

                string kuldemenyId = iratok.KuldKuldemenyek_Id;

                if (!String.IsNullOrEmpty(kuldemenyId))
                {
                    EREC_eBeadvanyokSearch eBeadvanyokSearch = new EREC_eBeadvanyokSearch();
                    eBeadvanyokSearch.KuldKuldemeny_Id.Value = kuldemenyId;
                    eBeadvanyokSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;
                    eBeadvanyokSearch.KuldoRendszer.Value = "'" + KodTarak.KULD_KEZB_MODJA.elektronikus_uton_Adatkapu + "','Adatkapu'";
                    eBeadvanyokSearch.KuldoRendszer.Operator = Query.Operators.inner;

                    Result eBeadvanyokGetAllResult = this.GetAll(execParam, eBeadvanyokSearch);

                    if (eBeadvanyokGetAllResult.IsError)
                        throw new ResultException(eBeadvanyokGetAllResult);

                    //adatkapus elektronikus �zenet lett iktatva
                    if (eBeadvanyokGetAllResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        string eBeadvanyId = eBeadvanyokGetAllResult.Ds.Tables[0].Rows[0]["Id"].ToString();

                        string formName = String.Format("{0}.xml", eBeadvanyokGetAllResult.Ds.Tables[0].Rows[0]["UzenetTipusa"]);
                        string formId = GetFormId(execParam, eBeadvanyId, formName);
                        string iktatoszam = iratok.Azonosito;
                        string iktato = GetFelhasznaloUserNeve(execParam, iratok.FelhasznaloCsoport_Id_Iktato);

                        Contentum.eIntegrator.Service.AdatkapuService adatkapuService = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetAdatkapuService();
                        result = adatkapuService.SetRegistrationIdentifier(execParam, formId, iktatoszam, iktato);
                    }
                }
            }
            else
            {
                Logger.Debug("SetRegistrationIdentifier skip. Source is not ELHISZ.");
            }
        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error(ex.Message);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    string GetFormId(ExecParam execParam, string eBeadvanyId, string formName)
    {
        Contentum.eDocument.Service.KRT_DokumentumokService dokumentumokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        KRT_DokumentumokSearch dokumentumokSearch = new KRT_DokumentumokSearch();
        dokumentumokSearch.Id.Value = String.Format("select Dokumentum_Id from EREC_eBeadvanyCsatolmanyok where eBeadvany_Id = '{0}' and GETDATE() between EREC_eBeadvanyCsatolmanyok.ErvKezd and EREC_eBeadvanyCsatolmanyok.ErvVege", eBeadvanyId);
        dokumentumokSearch.Id.Operator = Query.Operators.inner;
        dokumentumokSearch.FajlNev.Value = formName;
        dokumentumokSearch.FajlNev.Operator = Query.Operators.equals;

        Result dokumentumokResult = dokumentumokService.GetAll(execParam, dokumentumokSearch);

        if (dokumentumokResult.IsError)
            throw new ResultException(dokumentumokResult);

        if (dokumentumokResult.Ds.Tables[0].Rows.Count == 0)
            throw new Exception(String.Format("A {0} csatolm�ny nem tal�lhat�!", formName));

        string externalSource = dokumentumokResult.Ds.Tables[0].Rows[0]["External_Source"].ToString();
        string externalLink = dokumentumokResult.Ds.Tables[0].Rows[0]["External_Link"].ToString();

        byte[] docContent = GetDocContent(externalSource, externalLink);

        MemoryStream ms = new MemoryStream(docContent);
        XmlReader reader = XmlReader.Create(ms);
        XDocument doc = XDocument.Load(reader);

        string formId = (from element in doc.Root.Descendants()
                         where element.Name.LocalName == "form.id"
                         select element.Value).FirstOrDefault();

        if (String.IsNullOrEmpty(formId))
            throw new Exception("A form.id tag nem tal�lhat�!");

        return formId;


    }

    byte[] GetDocContent(string externalSource, string externalLink)
    {
        Logger.Debug(String.Format("GetDocContent: {0}", externalLink));

        using (WebClient wc = new WebClient())
        {
            if (externalSource == "SharePoint")
            {
                wc.Credentials = new NetworkCredential(UI.GetAppSetting("eSharePointBusinessServiceUserName"), UI.GetAppSetting("eSharePointBusinessServicePassword"), UI.GetAppSetting("eSharePointBusinessServiceUserDomain"));
            }
            else
            {
                wc.UseDefaultCredentials = true;
            }
            return wc.DownloadData(externalLink);
        }
    }

    string GetFelhasznaloUserNeve(ExecParam execParam, string felhasznaloId)
    {
        Contentum.eAdmin.Service.KRT_FelhasznalokService felhasznalokService = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        ExecParam felhasznalokExecParam = execParam.Clone();
        felhasznalokExecParam.Record_Id = felhasznaloId;

        Result res = felhasznalokService.Get(felhasznalokExecParam);

        if (res.IsError)
            throw new ResultException(res);

        KRT_Felhasznalok felhasznalo = res.Record as KRT_Felhasznalok;

        return felhasznalo.UserNev;
    }

    #endregion

    #region PostaHibrid

    void Process_PostaHibridCertificate(ExecParam execParam, EREC_eBeadvanyok eBeadvany, Csatolmany[] csatolmanyok)
    {
        //vev�ny
        if (!String.IsNullOrEmpty(eBeadvany.KR_HivatkozasiSzam))
        {
            EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, eBeadvany.KR_HivatkozasiSzam);

            if (hivatkozotteBeadvany != null)
            {
                string DispatchNonDispatchCertificatePDFName = "DispatchNonDispatchCertificate.pdf";
                string HibridReceiptNonReceiptCertificatePDFName = "HibridReceiptNonReceiptCertificate.pdf";

                Csatolmany dispatchNonDispatchCertificatePDF = csatolmanyok.Where(cs => cs.Nev == DispatchNonDispatchCertificatePDFName).FirstOrDefault();

                if (dispatchNonDispatchCertificatePDF != null)
                {
                    Process_DispatchNonDispatchCertificate(execParam, eBeadvany, dispatchNonDispatchCertificatePDF, hivatkozotteBeadvany);
                }
                else
                {
                    Csatolmany hibridReceiptNonReceiptCertificatePDF = csatolmanyok.Where(cs => cs.Nev == HibridReceiptNonReceiptCertificatePDFName).FirstOrDefault();
                    if (hibridReceiptNonReceiptCertificatePDF != null)
                    {
                        Process_HibridReceiptNonReceiptCertificate(execParam, eBeadvany, hibridReceiptNonReceiptCertificatePDF, hivatkozotteBeadvany);
                    }
                }
            }
        }
    }
    void Process_DispatchNonDispatchCertificate(ExecParam execParam, EREC_eBeadvanyok eBeadvany, Csatolmany dispatchNonDispatchCertificatePDF, EREC_eBeadvanyok hivatkozotteBeadvany)
    {
        AttachPostaHibridVisszaigazolasok(execParam, eBeadvany);

        PostaHibrid.DispatchNonDispatchCertificate.evidence evidence = PostaHibridHelper.GetDispatchNonDispatchCertificateFromPDF(dispatchNonDispatchCertificatePDF);

        if (evidence != null)
        {
            if (evidence.evidenceType == "DispatchCertificate")
            {
            } //sztorn�z�s
            else if (evidence.evidenceType == "NonDispatchCertificate")
            {
                string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);
                EREC_KuldKuldemenyekService kuldService = new EREC_KuldKuldemenyekService(this.dataContext);
                ExecParam kuldExecParam = execParam.Clone();
                kuldExecParam.Record_Id = kimenoKuldemenyId;
                string sztornozasIndoka = evidence.notifications == null ? String.Empty : String.Join(",", evidence.notifications);
                Result kuldRes = kuldService.Sztorno(kuldExecParam, sztornozasIndoka);

                if (kuldRes.IsError)
                {
                    throw new ResultException(kuldRes);
                }
            }
        }

        Invalidate(execParam, eBeadvany);
    }
    void Process_HibridReceiptNonReceiptCertificate(ExecParam execParam, EREC_eBeadvanyok eBeadvany, Csatolmany hibridReceiptNonReceiptCertificatePDF, EREC_eBeadvanyok hivatkozotteBeadvany)
    {
        AttachPostaHibridVisszaigazolasok(execParam, eBeadvany);

        PostaHibrid.HibridReceiptNonReceiptCertificate.evidence evidence = PostaHibridHelper.GetHibridReceiptNonReceiptCertificateFromPDF(hibridReceiptNonReceiptCertificatePDF);

        if (evidence != null)
        {
            //post�z�s
            if (evidence.evidenceType == "HybridReceipt")
            {
                string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);
                string postazasDatuma = evidence.eventTime.ToString();
                string ragszam = evidence.recipients.recipient.postalAddress.registeredCode;

                Postazas(execParam, kimenoKuldemenyId, postazasDatuma, ragszam);
            }
            //sztorn�z�s
            else if (evidence.evidenceType == "HybridNonReceipt")
            {
                string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);
                EREC_KuldKuldemenyekService kuldService = new EREC_KuldKuldemenyekService(this.dataContext);
                ExecParam kuldExecParam = execParam.Clone();
                kuldExecParam.Record_Id = kimenoKuldemenyId;
                string sztornozasIndoka = evidence.notifications == null ? String.Empty : String.Join(",", evidence.notifications);
                Result kuldRes = kuldService.Sztorno(kuldExecParam, sztornozasIndoka);

                if (kuldRes.IsError)
                {
                    throw new ResultException(kuldRes);
                }
            }
        }

        Invalidate(execParam, eBeadvany);
    }
    void AttachPostaHibridVisszaigazolasok(ExecParam execParam, EREC_eBeadvanyok eBeadvany)
    {
        EREC_eBeadvanyok hivatkozotteBeadvany = GeteBeadvanyByErkeztetesiSzam(execParam, eBeadvany.KR_HivatkozasiSzam);

        if (hivatkozotteBeadvany != null)
        {
            string kimenoKuldemenyId = GetKimenoKuldemenyId(execParam, hivatkozotteBeadvany);

            List<EREC_eBeadvanyCsatolmanyok> igazolasok = GetPostaHibridVisszaigazolasok(execParam, eBeadvany.Id);

            foreach (EREC_eBeadvanyCsatolmanyok igazolas in igazolasok)
            {
                Logger.Debug(String.Format("eBeadvanyCsatolmany: {0}", igazolas.Id));

                //dokumentum hozz�kapcsol�sa a k�ldem�nyhez
                string csatolmanyId = AddCsatolmanyToKuldemeny(execParam, kimenoKuldemenyId, igazolas.Dokumentum_Id);
                Logger.Debug(String.Format("csatolmanyId: {0}", csatolmanyId));

                //csatolm��ny kapcsol�sa vev�nyhez
                AttacheBeadVanyCsatolmanyToKapuRendszerVeveny(execParam, kimenoKuldemenyId, csatolmanyId, KodTarak.AdathordozoTipus.PostaHibridVisszaigazolas);
            }
        }
    }
    private List<EREC_eBeadvanyCsatolmanyok> GetPostaHibridVisszaigazolasok(ExecParam execParam, string eBeadvanyId)
    {
        List<EREC_eBeadvanyCsatolmanyok> igazolasok = new List<EREC_eBeadvanyCsatolmanyok>();

        EREC_eBeadvanyCsatolmanyokService service = new EREC_eBeadvanyCsatolmanyokService(this.dataContext);
        EREC_eBeadvanyCsatolmanyokSearch search = new EREC_eBeadvanyCsatolmanyokSearch();
        search.eBeadvany_Id.Value = eBeadvanyId;
        search.eBeadvany_Id.Operator = Query.Operators.equals;

        Result res = service.GetAll(execParam, search);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        foreach (DataRow row in res.Ds.Tables[0].Rows)
        {
            EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = new EREC_eBeadvanyCsatolmanyok();
            Utility.LoadBusinessDocumentFromDataRow(eBeadvanyCsatolmany, row);

            string ext = Path.GetExtension(eBeadvanyCsatolmany.Nev);

            if (".krx".Equals(ext, StringComparison.InvariantCultureIgnoreCase) || ".pdf".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
            {
                igazolasok.Add(eBeadvanyCsatolmany);
            }
        }

        return igazolasok;
    }

    #endregion

    // BLG_1643
    #region HivataliKapu
    /// <summary>
    /// Automata v�lasz k�ld�se Hivatali kapu fel�
    /// </summary>
    [WebMethod]
    public void SendHKPAutomataValasz(String KR_Fiok, List<EREC_eBeadvanyok> eBeadvanyok, List<Csatolmany> csatolmanyok, ExecParam execParam)
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        svc.Timeout = (int)TimeSpan.FromMinutes(30).TotalMilliseconds;
        Result resultFeltoltes = svc.CsoportosDokumentumFeltoltes(KR_Fiok, csatolmanyok.ToArray(), eBeadvanyok.ToArray());

        if (resultFeltoltes.IsError)
        {
            throw new ResultException(resultFeltoltes);
        }

        CsoportosDokumentumFeltoltesValasz valasz = resultFeltoltes.Record as CsoportosDokumentumFeltoltesValasz;

        for (int i = 0; i < valasz.UjDokumentumAdatokList.Length; i++)
        {
            UjDokumentumAdatokValasz ujDok = valasz.UjDokumentumAdatokList[i];
            eBeadvanyok[i].KR_ErkeztetesiSzam = ujDok.ErkeztetesiSzam;
            eBeadvanyok[i].Updated.KR_ErkeztetesiSzam = true;
            eBeadvanyok[i].KR_ErkeztetesiDatum = ujDok.ErkeztetesiDatum;
            eBeadvanyok[i].Updated.KR_ErkeztetesiDatum = true;
            eBeadvanyok[i].KR_Idopecset = ujDok.Idopecset;
            eBeadvanyok[i].Updated.KR_Idopecset = true;
        }

        #region �zenet elt�rol�sa

        EREC_eBeadvanyokService eBeadvanyokServcie = new EREC_eBeadvanyokService(this.dataContext);
        EREC_eBeadvanyCsatolmanyokService eBeadvanyCsatolmanyokService = new EREC_eBeadvanyCsatolmanyokService(this.dataContext);

        foreach (EREC_eBeadvanyok eBeadvany in eBeadvanyok)
        {
            string dokumentumId = eBeadvany.Base.Note;
            eBeadvany.Base.Note = String.Empty;
            eBeadvany.Base.Updated.Note = true;

            Result reseBeadvanyok = eBeadvanyokServcie.Insert(execParam, eBeadvany);

            if (reseBeadvanyok.IsError)
            {
                throw new Contentum.eUtility.ResultException(reseBeadvanyok);
            }

            string eBeadvanyIdnew = reseBeadvanyok.Uid;

            if (String.IsNullOrEmpty(dokumentumId))
            {

                if (csatolmanyok != null)
                {
                    string eBeadvany_Id = eBeadvanyIdnew;
                    EREC_eBeadvanyCsatolmanyokService erec_eBeadvanyCsatolmanyokService = new EREC_eBeadvanyCsatolmanyokService(this.dataContext);
                    Result _ret_csatolmanyok = erec_eBeadvanyCsatolmanyokService.DocumentumCsatolas(execParam, eBeadvany_Id, csatolmanyok.ToArray());
                    if (_ret_csatolmanyok.IsError)
                    {
                        throw new ResultException(_ret_csatolmanyok);
                    }
                }
            }
            else
            {
                EREC_eBeadvanyCsatolmanyok eBeadvanyCsatolmany = new EREC_eBeadvanyCsatolmanyok();
                eBeadvanyCsatolmany.eBeadvany_Id = eBeadvanyIdnew;
                eBeadvanyCsatolmany.Dokumentum_Id = dokumentumId;
                eBeadvanyCsatolmany.Nev = eBeadvany.KR_FileNev;

                Result reseBeadvanyokCsatolmany = eBeadvanyCsatolmanyokService.Insert(execParam, eBeadvanyCsatolmany);

                if (reseBeadvanyokCsatolmany.IsError)
                {
                    throw new Contentum.eUtility.ResultException(reseBeadvanyokCsatolmany);
                }
            }
        }
        #endregion �zenet elt�rol�sa
    }
    #endregion HivataliKapu

    /// <summary>
    /// IsEUzenetForrasElhisz
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    private bool IsEUzenetForrasElhisz(ExecParam execParam)
    {
        try
        {
            Dictionary<string, string> items = GetIntParametersForEUzenet(execParam);
            if (items == null || items.Count < 1 || !items.ContainsKey(KodTarak.INT_PARAMETEREK.eUzenetForras))
                return false;

            string value = items[KodTarak.INT_PARAMETEREK.eUzenetForras].ToString();
            if ("ELHISZ".Equals(value, StringComparison.CurrentCultureIgnoreCase))
                return true;
        }
        catch (Exception ex)
        {
            Logger.Error("IsEUzenetForrasElhisz", ex);
            return false;
        }
        return false;
    }
    /// <summary>
    /// GetIntParametersForEUzenet
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns></returns>
    public static Dictionary<string, string> GetIntParametersForEUzenet(ExecParam execParam)
    {
        Contentum.eIntegrator.Service.INT_ParameterekService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
        Result resultGet = svc.GetByModulNev(execParam, KodTarak.INT_MODULOK.eUzenet);

        if (resultGet.IsError)
        {
            throw new ResultException(resultGet);
        }
        Dictionary<string, string> result = new Dictionary<string, string>();
        foreach (DataRow row in resultGet.Ds.Tables[0].Rows)
        {
            string nev = row["Nev"].ToString();
            string ertek = row["Ertek"].ToString();

            if (string.IsNullOrEmpty(nev) || string.IsNullOrEmpty(ertek))
                continue;

            if (!result.ContainsKey(nev))
                result.Add(nev, ertek);
        }

        return result;
    }

    /// <summary>
    /// GetPartnerFromKapcsolatiKod
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kapcsolatiKod"></param>
    /// <returns></returns>
    private KRT_Partnerek GetPartnerFromKapcsolatiKod(ExecParam execParam, string kapcsolatiKod)
    {
        if (string.IsNullOrEmpty(kapcsolatiKod))
            return null;

        KRT_PartnerekSearch searchObj = new KRT_PartnerekSearch();
        searchObj.KulsoAzonositok.Value = "%"+ kapcsolatiKod + "%";
        searchObj.KulsoAzonositok.Operator = Query.Operators.like;
        searchObj.TopRow = 1;

        var svc = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        var resultPartnerek = svc.GetAll(execParam, searchObj);
        resultPartnerek.CheckError();

        if (resultPartnerek.Ds.Tables[0].Rows.Count < 1)
            return null;

        ExecParam execParamFind = execParam.Clone();
        execParamFind.Record_Id = resultPartnerek.Ds.Tables[0].Rows[0]["Id"].ToString();
        Result resultFind = svc.Get(execParamFind);
        resultFind.CheckError();

        return (KRT_Partnerek)resultFind.Record;
    }
    /// <summary>
    /// GetPartnerCimFromPartnerIdAndMakKod
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="partnerId"></param>
    /// <param name="makkod"></param>
    /// <returns></returns>
    private KRT_Cimek GetPartnerCimFromPartnerIdAndMakKod(ExecParam execParam, string partnerId, string makkod)
    {
        if (string.IsNullOrEmpty(partnerId))
            return null;

        KRT_PartnerekSearch searchObjPartner = new KRT_PartnerekSearch();
        searchObjPartner.Id.Value = partnerId;
        searchObjPartner.Id.Operator = Query.Operators.equals;

        KRT_CimekSearch searchObjCim = new KRT_CimekSearch();
        if (!string.IsNullOrEmpty(makkod))
        {
            searchObjCim.CimTobbi.Value = makkod;
            searchObjCim.CimTobbi.Operator = Query.Operators.equals;
        }
        searchObjCim.Tipus.Value = KodTarak.Cim_Tipus.MAK_KOD;
        searchObjCim.Tipus.Operator = Query.Operators.equals;

        searchObjCim.Kategoria.Value = KodTarak.Cim_Kategoria.K;
        searchObjCim.Kategoria.Operator = Query.Operators.equals;
        searchObjCim.TopRow = 1;

        var svc = eAdminService.ServiceFactory.GetKRT_PartnerekService();
        var resultPartnerek = svc.FindByPartnerAndCim(execParam, searchObjPartner, searchObjCim);
        resultPartnerek.CheckError();

        if (resultPartnerek.Ds.Tables[0].Rows.Count < 1)
            return null;

        //string Partner_Id = resultPartnerek.Ds.Tables[0].Rows[0]["Partner_Id"].ToString();
        string Cim_Id = resultPartnerek.Ds.Tables[0].Rows[0]["Cim_Id"].ToString();
        //string PartnerCim_Id = resultPartnerek.Ds.Tables[0].Rows[0]["Partner_Id"].ToString();

        ExecParam execParamFindCim = execParam.Clone();
        execParamFindCim.Record_Id = Cim_Id;
        var svcCim = eAdminService.ServiceFactory.GetKRT_CimekService();
        Result resultFind = svcCim.Get(execParamFindCim);
        resultFind.CheckError();

        return (KRT_Cimek)resultFind.Record;
    }

    #region PostProcess

    internal void SetProcessError(ExecParam execParam, string eBeadvanyId, string hiba)
    {
        SetProcessStatusz(execParam, eBeadvanyId, eBeadvanyokConstants.FeldolgozasStatusz.Hibas, hiba);
    }

    void SetProcessOk(ExecParam execParam, string eBeadvanyId)
    {
        SetProcessStatusz(execParam, eBeadvanyId, "0", null);
    }

    void SetProcessStatusz(ExecParam execParam, string eBeadvanyId, string statusz, string hiba)
    {
        try
        {
            ExecParam execGet = execParam.Clone();
            execGet.Record_Id = eBeadvanyId;

            Result resGet = this.Get(execGet);
            resGet.CheckError();

            EREC_eBeadvanyok eBeadvany = resGet.Record as EREC_eBeadvanyok;
            eBeadvany.Updated.SetValueAll(false);
            eBeadvany.Base.Updated.SetValueAll(false);
            eBeadvany.Base.Updated.Ver = true;


            eBeadvany.FeldolgozasStatusz = statusz;
            eBeadvany.Updated.FeldolgozasStatusz = true;

            eBeadvany.FeldolgozasiHiba = String.IsNullOrEmpty(hiba) ? "<null>" : hiba;
            eBeadvany.Updated.FeldolgozasiHiba = true;

            Result resUpdate = this.Update(execGet, eBeadvany);
        }
        catch (Exception ex)
        {
            Logger.Error("SetProcessStatusz hiba", ex);
        }
    }


    internal void PostProcessInternal(ExecParam execParam, EREC_eBeadvanyok erec_eBeadvanyok, bool processFailedVevenyek)
    {
        Result result = new Result();

        #region adatok �sszegy�jt�se

        string eBeadvanyId = erec_eBeadvanyok.Id;
        bool isPostaHibridSzolgaltatas = false;
        bool isWeb = false;
        bool isKapuVeveny = false;
        List<Csatolmany> csatolmanyok = new List<Csatolmany>();
        bool getCsatolmanyok = false;


        isKapuVeveny = this.IsKapuVeveny(erec_eBeadvanyok);
        string postaKRID = PostaHibridHelper.GetPostaKRID(execParam);
        isPostaHibridSzolgaltatas = (erec_eBeadvanyok.PartnerKRID == postaKRID);
        isWeb = (erec_eBeadvanyok.Cel == "WEB");

        getCsatolmanyok = isPostaHibridSzolgaltatas;

        if (getCsatolmanyok)
        {
            Contentum.eDocument.Service.KRT_DokumentumokService dokService = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch dokSearch = new KRT_DokumentumokSearch();
            dokSearch.Id.Value = String.Format("select Dokumentum_Id from EREC_eBeadvanyCsatolmanyok where eBeadvany_Id = '{0}' and getdate() between ErvKezd and ErvVege", eBeadvanyId);
            dokSearch.Id.Operator = Query.Operators.inner;

            Result dokResult = dokService.GetAll(execParam, dokSearch);

            dokResult.CheckError();

            foreach (DataRow row in dokResult.Ds.Tables[0].Rows)
            {
                string externalSource = row["External_Source"].ToString();
                string externalLink = row["External_Link"].ToString();
                string fajlNev = row["FajlNev"].ToString();
                Csatolmany csatolmany = new Csatolmany();
                csatolmany.Nev = fajlNev;
                csatolmany.Tartalom = GetDocContent(externalSource, externalLink);
                csatolmanyok.Add(csatolmany);
            }
        }

        #endregion

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        EREC_eBeadvanyok hivatkozotteBeadvany = null;
        bool sendEmail = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if ((!String.IsNullOrEmpty(erec_eBeadvanyok.KR_DokTipusAzonosito)) && isWeb)
            {
                string dokTipus = erec_eBeadvanyok.KR_DokTipusAzonosito.Trim().ToLower();

                Logger.Debug(String.Format("DokTipusAzonosito: {0}", dokTipus));

                switch (dokTipus)
                {
                    case "feladasiigazolas":
                        Logger.Debug("feladasiigazolas");
                        FeladasiIgazolas(execParam, erec_eBeadvanyok, isPostaHibridSzolgaltatas);
                        break;
                    case "atveteliertesito":
                        Logger.Debug("atveteliertesito");
                        AtveteliErtesito(execParam, erec_eBeadvanyok, isPostaHibridSzolgaltatas);
                        break;
                    case "letoltesiigazolas":
                        Logger.Debug("letoltesiigazolas");
                        LetoltesiIgazolas(execParam, erec_eBeadvanyok, isPostaHibridSzolgaltatas);
                        break;
                    case "meghiusulasiigazolas":
                        Logger.Debug("meghiusulasiigazolas");
                        hivatkozotteBeadvany = MeghiusulasiIgazolas(execParam, erec_eBeadvanyok, isPostaHibridSzolgaltatas);
                        sendEmail = true;
                        break;
                }
            }

            //PostaHibrid
            if (isPostaHibridSzolgaltatas)
            {
                Process_PostaHibridCertificate(execParam, erec_eBeadvanyok, csatolmanyok.ToArray());
            }
            else
            {
                if (isWeb && !isKapuVeveny &&
                    erec_eBeadvanyok.KR_Rendszeruzenet == "1" &&
                    !String.IsNullOrEmpty(erec_eBeadvanyok.KR_HivatkozasiSzam))
                {
                    AutomatikusVisszaigazolas(execParam, erec_eBeadvanyok);
                }
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (result.IsError)
        {
            throw new ResultException(result);
        }
        else
        {
            //C�mzett nem vette �t
            if (sendEmail)
            {
                if (hivatkozotteBeadvany != null)
                {
                    Logger.Debug("CimzettNemVetteAt email k�ld�se");
                    try
                    {
                        ExecParam execParamEmail = execParam.Clone();
                        execParam.Record_Id = hivatkozotteBeadvany.Id;
                        CimzettNemVetteAt(execParam);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("CimzettNemVetteAt hiba", execParam, ResultException.GetResultFromException(ex));
                    }
                }
            }

            if (processFailedVevenyek && isKapuVeveny)
            {
                TryProcessFailedVevenyek(execParam, erec_eBeadvanyok.KR_HivatkozasiSzam);
            }
        }
    }

    void TryProcessFailedVevenyek(ExecParam execParam, string hivatkozasiSzam)
    {
        Result result = new Result();
        try
        {
            if (!String.IsNullOrEmpty(hivatkozasiSzam))
            {
                EREC_eBeadvanyokSearch search = new EREC_eBeadvanyokSearch();

                search.KR_HivatkozasiSzam.Filter(hivatkozasiSzam);
                search.FeldolgozasStatusz.Filter(eBeadvanyokConstants.FeldolgozasStatusz.Hibas);

                Result resultGetAll = this.GetAll(execParam, search);
                resultGetAll.CheckError();

                foreach (DataRow row in resultGetAll.Ds.Tables[0].Rows)
                {
                    EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
                    Utility.LoadBusinessDocumentFromDataRow(eBeadvany, row);

                    bool isKapuVeveny = this.IsKapuVeveny(eBeadvany);

                    if (isKapuVeveny)
                    {
                        try
                        {
                            PostProcessInternal(execParam, eBeadvany, false);
                            //SetProcessOk(execParam, eBeadvany.Id);
                        }
                        catch (Exception ex)
                        {
                            result = ResultException.GetResultFromException(ex);
                            Logger.Error(String.Format("PostProcessInternal({0})", eBeadvany.KR_ErkeztetesiSzam), execParam, result);
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            result = ResultException.GetResultFromException(ex);
            Logger.Error(String.Format("TryProcessFailedVevenyek({0})", hivatkozasiSzam), execParam, result);
        }
    }


    #endregion
}