using System;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using Contentum.eBusinessDocuments;
using Contentum.eUtility;

/// <summary>
/// Summary description for EREC_eGovPortalKuldemenyek
/// </summary>
[WebService(Namespace = "http://Contentum.eGovPortalKuldemenyek.WebService/", Description = "Az EGovPortal �s az eDok rendszerek k�z�tti kapcsolat.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class EREC_eGovPortalKuldemenyek : System.Web.Services.WebService
{

    public EREC_eGovPortalKuldemenyek()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(false, TransactionOption = TransactionOption.RequiresNew)]
    //, Description = "<br><p>- bemeneti param�ter t�pusa: <b>InputDTO</b><ol><li>string <b>A</b>lairtFileNev <i>(Az EGovPortal �ltal al��rt �llom�ny neve)</i></li><li>byte[] <b>A</b>lairtTartalom <i>(Az EGovPortal �ltal al��rt �llom�ny tartalma)</i></li><li>DateTime <b>E</b>govElsoatvetIdeje</li><li>string <b>E</b>govErkeztetoSzam</li><li>string <b>E</b>govTargy</li><li>string <b>F</b>ileNev</li><li>string <b>K</b>uldemenyErkeztetoKonyv</li><li>string <b>P</b>rtCime</li><li>string <b>P</b>rtId</li><li>string <b>P</b>rtMegnevezes</li><li>byte[] <b>T</b>artalom</li></ol>")]
    [System.Xml.Serialization.XmlInclude(typeof(Contentum.eBusinessDocuments.InputDTO))]
    public Contentum.eBusinessDocuments.KuldemenyDTO KET_Erkeztetes(Contentum.eBusinessDocuments.InputDTO p_input)
    {
        Contentum.eBusinessDocuments.KuldemenyDTO rv = new Contentum.eBusinessDocuments.KuldemenyDTO();
        return rv;
    }

    [WebMethod(false, TransactionOption = TransactionOption.RequiresNew)]
    //, Description = "<br><p><u>KET Iktat�sz�mok lek�rdez�se</u>.</p> <ol><li>string <b>p_erkeztetoszam</b></li></ol>")]
    public Contentum.eBusinessDocuments.IktatoszamokDTO[] KET_Iktatoszamok_Lekerdezese(string p_erkeztetoszam)
    {
        Contentum.eBusinessDocuments.IktatoszamokDTO[] rv = new Contentum.eBusinessDocuments.IktatoszamokDTO[0];
        return rv;
    }

    [WebMethod(false, TransactionOption = TransactionOption.RequiresNew)]
    //, Description = "<br><p><u>KET Kiadm�nyoz�s</u>.</p>")]
    [System.Xml.Serialization.XmlInclude(typeof(Contentum.eBusinessDocuments.InputDTO))]
    public Contentum.eBusinessDocuments.KiadmanyokDTO KET_Kiadmanyozas(Contentum.eBusinessDocuments.InputDTO p_input)
    {
        Contentum.eBusinessDocuments.KiadmanyokDTO rv = new Contentum.eBusinessDocuments.KiadmanyokDTO();
        return rv;
    }

}

