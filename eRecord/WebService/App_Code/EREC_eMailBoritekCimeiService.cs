using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_eMailBoritekCimeiService : System.Web.Services.WebService
{
    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra).
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_EREC_eMailBoritekCimeiSearch">A sz�r�si felt�teleket tartalmazza</param>
    /// <returns>Result.Ds DataSet objektum ker�l felt�lt�sre a lek�rdez�s eredm�ny�vel</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekCimeiSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekCimeiSearch _EREC_eMailBoritekCimeiSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_eMailBoritekCimeiSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_EREC_eMailBoritekCimeiSearch, new EREC_eMailBoritekCimeiSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "EREC_eMailBoritekCimei", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_EREC_eMailBoritekCimeiSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_EREC_eMailBoritekCimeiSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _EREC_eMailBoritekCimeiSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}