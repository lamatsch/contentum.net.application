using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eUtility;
using System.Xml;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_eMailBoritekCsatolmanyokService
{
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekCsatolmanyokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekCsatolmanyokSearch _EREC_eMailBoritekCsatolmanyokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_eMailBoritekCsatolmanyokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekCsatolmanyokSearch))]
    public Result DocumentumCsatolas(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritek, Csatolmany[] csatolmanyok, String egyebParamsXml)
    {
        
        #region xml param string feldolgozasa

        Logger.Error(String.Format("EREC_eMailBoritekCsatolmanyokService.DocumentumCsatolas indul."));

        XmlDocument xmlDoc = new XmlDocument();
        String tartalomSHA1 = String.Empty;

        try
        {
            //  kapott XML konvertalas
            //
            xmlDoc.LoadXml(egyebParamsXml);

            if (xmlDoc.SelectNodes("//params/tartalomSHA1").Count == 1)
                tartalomSHA1 = xmlDoc.SelectSingleNode("//params/tartalomSHA1").InnerText;
        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("Hiba a kapott xml parameterek feldogozasa soran!\negyebParamsXml: {0}\nMessage: {1}\nStackTrace: {2}", egyebParamsXml, ex.Message, ex.StackTrace));
            Result ret = new Result();
            ret.ErrorCode = "XMLPARERR001";
            ret.ErrorMessage = String.Format("Hiba a kapott xml parameterek feldogozasa soran!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            throw new ResultException(ret);
        }

        #endregion

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            foreach (Csatolmany csatolmany in csatolmanyok)
            {
                if (csatolmany == null)
                {
                    continue;
                }

                // KRT_Dokumentumok objektum l�trehoz�sa
                KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
                if (csatolmany.Megnyithato != null)
                {
                    krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
                }
                if (csatolmany.Olvashato != null)
                {
                    krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
                }
                if (csatolmany.ElektronikusAlairas != null)
                {
                    krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
                }

                DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                
                /*
                Result result_upload = documentService.UploadFromeRecord(ExecParam
                    , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.EMAIL_CSATOLMANY_DOKUMENTUMTARTIPUS)
                    , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.EMAIL_CSATOLMANY_DOKUMENTUMTARTERULET)
                    , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.EMAIL_CSATOLMANY_DOKUMENTUMKONYVTAR)
                    , erec_eMailBoritek.Id
                    , csatolmany.Nev
                    , csatolmany.Tartalom
                    , krt_Dokumentumok
                    , false);
                */


                // Upload webservicenek �tadand� adatok al��r�s eset�n xml-ben:
                string uploadXmlStrParams_alairasResz = "";
                if (csatolmany.AlairasAdatok.CsatolmanyAlairando)
                {
                    uploadXmlStrParams_alairasResz = csatolmany.AlairasAdatok.GetInnerXml();
                }

                String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                        "<iktatokonyv></iktatokonyv>" +
                                                        "<sourceSharePath></sourceSharePath>" +
                                                        "<foszam>{0}</foszam>" +
                                                        "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                        "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                        "<megjegyzes></megjegyzes>" +
                                                        "<munkaanyag>{2}</munkaanyag>" +
                                                        "<ujirat>{3}</ujirat>" +
                                                        "<vonalkod></vonalkod>" +
                                                        "<docmetaIratId></docmetaIratId>" +
                                                        "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                        "<tartalomSHA1>{5}</tartalomSHA1>" +
                                                        "<ucmiktatokonyv>{6}</ucmiktatokonyv>" +
                                                        uploadXmlStrParams_alairasResz +
                                                    "</uploadparameterek>"
                                                    , erec_eMailBoritek.Id
                                                    , String.Empty
                                                    , "NEM"
                                                    , "IGEN"
                                                    , "IGEN"
                                                    , tartalomSHA1
                                                    , "EB"
                                                    );

                Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
                    (
                       ExecParam
                       , Contentum.eUtility.Rendszerparameterek.Get(ExecParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
                       , csatolmany.Nev
                       , csatolmany.Tartalom
                       , krt_Dokumentumok
                       , uploadXmlStrParams
                    );

                if (result_upload.IsError)
                {
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, result_upload);
                    //return result_upload;

                    throw new ResultException(result_upload);
                }

                #region esemenykezeles

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExecParam.Clone()
                    , dataContext.Tranz_Id
                    , result_upload.Uid
                    , "KRT_Dokumentumok"
                    , "DokumentumokNewEmail").Record;

                //
                //  nem egeszen vilagos ezt miert is nem adja at nehany helyen....

                if (String.IsNullOrEmpty(ExecParam.Felhasznalo_Id))
                {
                    Logger.Error("Ures a ExecParam.Felhasznalo_Id valtozo, ezert nem lesz bejegyezve a DocumentumokNewEmail esemeny!");
                }
                else
                {
                    eventLogRecord.Felhasznalo_Id_Login = (String.IsNullOrEmpty(ExecParam.Helyettesites_Id) == false) ? ExecParam.LoginUser_Id : ExecParam.Felhasznalo_Id;
                }

                Result eventLogResult = eventLogService.Insert(ExecParam.Clone(), eventLogRecord);

                #endregion


                EREC_eMailBoritekCsatolmanyok erec_eMailBoritekCsatolmanyok = new EREC_eMailBoritekCsatolmanyok();
                erec_eMailBoritekCsatolmanyok.eMailBoritek_Id = erec_eMailBoritek.Id;
                erec_eMailBoritekCsatolmanyok.Nev = csatolmany.Nev;
                erec_eMailBoritekCsatolmanyok.Dokumentum_Id = result_upload.Uid;

                Result _ret = sp.Insert(Constants.Insert, ExecParam, erec_eMailBoritekCsatolmanyok);
                if (_ret.IsError)
                {
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, _ret);
                    //return _ret;

                    throw new ResultException(_ret);
                }

                result = _ret;
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam);
        return result;
    }

}
