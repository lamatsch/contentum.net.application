using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using Contentum.eUtility;
using System.Xml;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class EREC_eMailBoritekokService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_eMailBoritekokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekokSearch))]
    public Result GetAllWithExtensionAndJogosultak(ExecParam ExecParam, EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch,bool Jogosultak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _EREC_eMailBoritekokSearch,Jogosultak);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekok))]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekCsatolmanyok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            Result res;
            EREC_eMailBoritekCsatolmanyokService service = new EREC_eMailBoritekCsatolmanyokService(this.dataContext);
            EREC_eMailBoritekCsatolmanyokSearch search = new EREC_eMailBoritekCsatolmanyokSearch();
            search.eMailBoritek_Id.Value = ExecParam.Record_Id;
            search.eMailBoritek_Id.Operator = Query.Operators.equals;
            res = service.GetAll(ExecParam, search);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }
            if (res.Ds != null && res.Ds.Tables[0].Rows.Count > 0)
            {
                ExecParam[] execParams = new ExecParam[res.Ds.Tables[0].Rows.Count];
                for (int i = 0; i < res.Ds.Tables[0].Rows.Count; i++)
                {
                    execParams[i] = ExecParam.Clone();
                    execParams[i].Alkalmazas_Id = ExecParam.Alkalmazas_Id;
                    execParams[i].Felhasznalo_Id = ExecParam.Felhasznalo_Id;
                    execParams[i].Record_Id = res.Ds.Tables[0].Rows[i]["Id"].ToString();
                }
                res = service.MultiInvalidate(execParams);

                if (!String.IsNullOrEmpty(res.ErrorCode))
                {
                    //ContextUtil.SetAbort();
                    //log.WsEnd(ExecParam, res);
                    //return res;

                    throw new ResultException(res);
                }
            }
            res = sp.Invalidate(ExecParam);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                //log.WsEnd(ExecParam, res);
                //return res;

                throw new ResultException(res);
            }

            result = res;

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {

                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "EREC_eMailBoritekok", "Invalidate").Record;

                eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// E-Mail kliensb�l h�vhat� method, feladata az erec_eMailBoritekok param�terben megadott
    /// E-Mail-t beregisztr�lja az adatb�zisba, felt�lti a tartalmat a SP szerverre, �s l�trehozza
    /// a kapcsolatot a SPS �s az EDOK k�z�tt
    /// </summary>
    /// <param name="ExecParam">�ltal�nos inform�ci�k a futtat� felhaszn�l�r�l</param>
    /// <param name="erec_eMailBoritekok">EREC_eMailBoritekok t�pus� objektum</param>
    /// <param name="csatolmanyok">Csatolm�nyok t�pus� t�mb, az E-Mail csatolm�nyai</param>
    /// <param name="kuldesMod">"KUK" �rt�k eset�n csak a felhaszn�l� kap jogot az E-Mailhez, b�rmely
    /// m�s �rt�k eset�n k�zponti �rkeztet�s hajt�dik v�gre</param>
    /// <param name="egyebParamsXml">egy�b param�terek XML form�tumban</param>
    /// <returns>A m�velet eredm�ny�t jelz� Result objektum; siker eset�n az UID mez�ben az �jonnan
    /// l�trej�nn E-Mail bor�t�k Id-ja, egy�bk�nt a hiba k�dja az ErrorCode mez�ben</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekCsatolmanyokSearch))]
    public Result InsertAndAttachDocument(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritekok, Csatolmany[] csatolmanyok, String kuldesMod, String egyebParamsXml)
    {
        #region ellenorzes SHA1 tartalomra

        Logger.Error(String.Format("EREC_eMailBoritekokService.InsertAndAttachDocument indul."));

        try 
        {
            //  kapott XML konvertalas
            //
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(egyebParamsXml);

        } 
        catch (Exception ex) 
        {
            Logger.Error(String.Format("Hiba a kapott xml parameterek feldogozasa soran!\negyebParamsXml: {0}\nMessage: {1}\nStackTrace: {2}", egyebParamsXml, ex.Message, ex.StackTrace));
            Result ret = new Result();
            ret.ErrorCode = "XMLPARERR001";
            ret.ErrorMessage = String.Format("Hiba a kapott xml parameterek feldogozasa soran!\nMessage: {0}\nStackTrace: {1}", ex.Message, ex.StackTrace);
            throw new ResultException(ret);
        }


        #endregion

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // TODO: ide meg kell a funkciojog ellenorzes
            
            //Bek�ld� ellen�rz�se
            Result result_forrastipus = sp.GetEMailForrasTipusByFelado(ExecParam, erec_eMailBoritekok.Felado);

            if (result_forrastipus.IsError)
            {
                throw new ResultException(result_forrastipus);
            }
			erec_eMailBoritekok.ForrasTipus = (string)result_forrastipus.Record;
           
            //Az email bor�t�k r�gz�t�se
            Result _ret = Insert(ExecParam, erec_eMailBoritekok);
            if (_ret.IsError)
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, _ret);
                //return _ret;

                throw new ResultException(_ret);
            }

            //A bor�t�khoz tartoz� �sszes email c�m r�gz�t�s
            string emailcimek = String.Concat(erec_eMailBoritekok.Cimzett, ",", erec_eMailBoritekok.CC);
            string[] cimzettek = emailcimek.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
			EREC_eMailBoritekCimei erec_eMailBoritekCimei;
			int sorszam = 0;

			EREC_eMailBoritekCimeiService bcservice = new EREC_eMailBoritekCimeiService(this.dataContext);
            foreach (string cimzett in cimzettek)
            {
                //if(cimzett.Length > 0)
                //{
                erec_eMailBoritekCimei = new EREC_eMailBoritekCimei();
                //erec_eMailBoritekCimei.Id = new 
                erec_eMailBoritekCimei.eMailBoritek_Id = _ret.Uid;
                erec_eMailBoritekCimei.MailCim = cimzett;
                erec_eMailBoritekCimei.Sorszam = sorszam.ToString();

                Result result_cimtipus = sp.GetEMailBoritekCimTipusByFelado(ExecParam, cimzett);
                if (result_cimtipus.IsError)
                {
                    throw new ResultException(result_cimtipus);
                }


                System.Data.DataRow dr = (DataRow)result_cimtipus.Record;

                erec_eMailBoritekCimei.Tipus = dr["BoritekCimTipus"].ToString();
                erec_eMailBoritekCimei.Partner_Id = dr["PartnerId"].ToString();

                Result result_boritekcim = bcservice.Insert(ExecParam, erec_eMailBoritekCimei);

                if (result_boritekcim.IsError)
                {
                    throw new ResultException(result_boritekcim);
                }
                sorszam++;
                //}
            }
            
	        Contentum.eBusinessDocuments.EREC_eMailBoritekok _erec_eMailBoritekok_Csatolashoz = new Contentum.eBusinessDocuments.EREC_eMailBoritekok();
            _erec_eMailBoritekok_Csatolashoz.Id = _ret.Uid;

            EREC_eMailBoritekCsatolmanyokService erec_eMailBoritekCsatolmanyokService = new EREC_eMailBoritekCsatolmanyokService(this.dataContext);
            Result _ret_csatolmanyok = erec_eMailBoritekCsatolmanyokService.DocumentumCsatolas(ExecParam, _erec_eMailBoritekok_Csatolashoz, csatolmanyok, egyebParamsXml);
            if (_ret_csatolmanyok.IsError)
            {
                //ContextUtil.SetAbort();
                //log.WsEnd(ExecParam, _ret_csatolmanyok);
                //return _ret_csatolmanyok;

                throw new ResultException(_ret_csatolmanyok);
            }

            #region ACL kezel�s

            RightsService rs = new RightsService(this.dataContext);
            Result aclResult = rs.AddScopeIdToJogtargyWithJogszint(ExecParam, _ret.Uid, "EREC_eMailBoritekok", null, 'X', ExecParam.Felhasznalo_Id, 'I');
            
            if (aclResult.IsError)
            {
                throw new ResultException(aclResult);
            }

            if (kuldesMod != "KUK")
            {
                Contentum.eAdmin.Service.KRT_ObjTipusokService ots = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ObjTipusokService();
                KRT_ObjTipusokSearch otsearch = new KRT_ObjTipusokSearch();
                otsearch.Kod.Value = "EREC_eMailBoritekok";
                otsearch.Kod.Operator = Query.Operators.equals;
                string mailObjTip = ots.GetAll(ExecParam,otsearch).Ds.Tables[0].Rows[0]["Id"].ToString();

                Contentum.eAdmin.Service.KRT_KodTarakService kts = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();

                Result result_acl = kts.GetAllByKodcsoportKod(ExecParam, "ACL_DEFAULT_BINDINGS");

                if (result_acl.IsError)
                {
                    throw new ResultException(result_acl);
                }

                foreach (DataRow dr in result_acl.Ds.Tables[0].Rows)
                {
                    if (dr["ObjTip_Id_AdatElem"].ToString() == mailObjTip)
                        rs.RemoveCsoportFromJogtargyByTipus(ExecParam,_ret.Uid,dr["Obj_Id"].ToString(),'N','D');
                }
            }

            #endregion

            result = _ret;

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;

    }
}