﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eUtility.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class ErtesitesService : System.Web.Services.WebService
{
    private ErtesitesStoredProcedure sp = null;
    private DataContext dataContext;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="_dataContext"></param>
    public ErtesitesService(DataContext _dataContext)
    {
        dataContext = _dataContext;
        sp = new ErtesitesStoredProcedure(dataContext);
    }
    /// <summary>
    /// Constructor
    /// </summary>
    public ErtesitesService()
    {
        dataContext = new DataContext(this.Application);
        sp = new ErtesitesStoredProcedure(dataContext);
    }

    /// <summary>
    /// Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="felhasznaloSzervezetId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas(string felhasznaloId, string felhasznaloSzervezetId)
    {
        if (string.IsNullOrEmpty(felhasznaloId))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "";
            errorResult.ErrorMessage = "Nincs megadva a Felhasznalo_Id!";
            Logger.Error("Nincs megadva a Felhasznalo_Id!");
            return errorResult;
        }

        //if (string.IsNullOrEmpty(felhasznaloSzervezetId))
        //{
        //    Result errorResult = new Result();
        //    errorResult.ErrorCode = "";
        //    errorResult.ErrorMessage = "Nincs megadva a FelhasznaloSzervezet_Id!";
        //    Logger.Error("Nincs megadva a FelhasznaloSzervezet_Id!");
        //    return errorResult;
        //}
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;
        if (!string.IsNullOrEmpty(felhasznaloSzervezetId))
            execParam.FelhasznaloSzervezet_Id = felhasznaloSzervezetId;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas Start", execParam);
        Result result = new Result();

        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas(execParam);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas webservice hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (result != null && result.Ds.Tables[0].Rows.Count > 0)
        {
            try
            {
                Result resultSablon = GetKrtParameterItem(execParam.Clone(), Rendszerparameterek.ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_SABLON);
                string sablon = GetKrtParameterNoteValue(resultSablon);
                string targy = Rendszerparameterek.Get(execParam, Rendszerparameterek.ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY);
                string messageFrom = Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);

                if (string.IsNullOrEmpty(sablon))
                {
                    throw new Exception("Email sablon nem definiált !");
                }

                List<LejartMegorzesiIdoModel> models = new List<LejartMegorzesiIdoModel>();
                LejartMegorzesiIdoModel tmpModel;
                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    tmpModel = new LejartMegorzesiIdoModel();
                    tmpModel.Id = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    tmpModel.MegorzesiIdoVege = result.Ds.Tables[0].Rows[i]["MegorzesiIdoVege"].ToString();
                    tmpModel.Csoport_Id_Felelos_Nev = result.Ds.Tables[0].Rows[i]["Csoport_Id_Felelos_Nev"].ToString();
                    tmpModel.Csoport_Id_Felelos_Email = result.Ds.Tables[0].Rows[i]["Csoport_Id_Felelos_Email"].ToString();
                    tmpModel.Csoport_Id_Felelos = result.Ds.Tables[0].Rows[i]["Csoport_Id_Felelos"].ToString();
                    tmpModel.Azonosito = result.Ds.Tables[0].Rows[i]["Azonosito"].ToString();
                    models.Add(tmpModel);
                }

                List<string> cimzettek = new List<string>();
                string sablonTartalom = GetSablonTartalom(models, out cimzettek);
                string linkUrl = GetFelulvizsgalatPageUrl();
                string levelTorzs;
                if (!string.IsNullOrEmpty(linkUrl))
                    levelTorzs = string.Format(sablon, sablonTartalom, string.Format("<a href=\"{0}\">Link</a>", linkUrl));
                else
                    levelTorzs = string.Format(sablon, sablonTartalom, string.Empty);
                if (cimzettek != null && cimzettek.Count > 0)
                {
                    #region MAIL
                    Contentum.eAdmin.Service.EmailService emailService = Contentum.eUtility.eAdminService.ServiceFactory.GetEmailService();
                    bool ret = emailService.SendEmail(execParam, messageFrom, cimzettek.ToArray(), targy, null, null, true, levelTorzs);

                    if (!ret)
                        throw new Exception("Sikertelen email küldés !");
                    #endregion
                }
            }
            catch (Exception e)
            {
                result = ResultException.GetResultFromException(e);
                Logger.Error(string.Format("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas webservice hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }
        }

        Logger.Debug("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas webservice vége");
        Logger.DebugEnd();
        return result;
    }
    /// <summary>
    /// Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes
    /// </summary>
    /// <param name="felhasznaloId"></param>
    /// <param name="felhasznaloSzervezetId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes(string felhasznaloId, string felhasznaloSzervezetId)
    {
        if (string.IsNullOrEmpty(felhasznaloId))
        {
            Result errorResult = new Result();
            errorResult.ErrorCode = "";
            errorResult.ErrorMessage = "Nincs megadva a Felhasznalo_Id!";
            Logger.Error("Nincs megadva a Felhasznalo_Id!");
            return errorResult;
        }

        //if (string.IsNullOrEmpty(felhasznaloSzervezetId))
        //{
        //    Result errorResult = new Result();
        //    errorResult.ErrorCode = "";
        //    errorResult.ErrorMessage = "Nincs megadva a FelhasznaloSzervezet_Id!";
        //    Logger.Error("Nincs megadva a FelhasznaloSzervezet_Id!");
        //    return errorResult;
        //}
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;
        if (!string.IsNullOrEmpty(felhasznaloSzervezetId))
            execParam.FelhasznaloSzervezet_Id = felhasznaloSzervezetId;

        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Logger.Debug("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes Start", execParam);
        Result result = new Result();

        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes(execParam);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(string.Format("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes webservice hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        if (result != null && result.Ds.Tables[0].Rows.Count > 0)
        {
            try
            {
                Result resultSablon = GetKrtParameterItem(execParam.Clone(), Rendszerparameterek.ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_SABLON);
                string sablon = GetKrtParameterNoteValue(resultSablon);
                string targy = Rendszerparameterek.Get(execParam, Rendszerparameterek.ERTESITES_UGYUGYIRATOK_LEJARTMEGORZESIIDO_LEVELTARIATADAS_EMAIL_TARGY);
                string messageFrom = Rendszerparameterek.Get(execParam, Rendszerparameterek.RENDSZERFELUGYELET_EMAIL_CIM);

                if (string.IsNullOrEmpty(sablon))
                {
                    throw new Exception("Email sablon nem definiált !");
                }

                List<LejartMegorzesiIdoModel> models = new List<LejartMegorzesiIdoModel>();
                LejartMegorzesiIdoModel tmpModel;
                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    tmpModel = new LejartMegorzesiIdoModel();
                    tmpModel.Id = result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    tmpModel.MegorzesiIdoVege = result.Ds.Tables[0].Rows[i]["MegorzesiIdoVege"].ToString();
                    tmpModel.Csoport_Id_Felelos_Nev = result.Ds.Tables[0].Rows[i]["Csoport_Id_Felelos_Nev"].ToString();
                    tmpModel.Csoport_Id_Felelos_Email = result.Ds.Tables[0].Rows[i]["Csoport_Id_Felelos_Email"].ToString();
                    tmpModel.Csoport_Id_Felelos = result.Ds.Tables[0].Rows[i]["Csoport_Id_Felelos"].ToString();
                    tmpModel.Azonosito = result.Ds.Tables[0].Rows[i]["Azonosito"].ToString();
                    models.Add(tmpModel);
                }

                List<string> cimzettek = new List<string>();
                string sablonTartalom = GetSablonTartalom(models, out cimzettek);
                string linkUrl = GetFelulvizsgalatPageUrl();
                string levelTorzs;
                if (!string.IsNullOrEmpty(linkUrl))
                    levelTorzs = string.Format(sablon, sablonTartalom, string.Format("<a href=\"{0}\">Link</a>", linkUrl));
                else
                    levelTorzs = string.Format(sablon, sablonTartalom, string.Empty);
                if (cimzettek != null && cimzettek.Count > 0)
                {
                    #region MAIL
                    Contentum.eAdmin.Service.EmailService emailService = Contentum.eUtility.eAdminService.ServiceFactory.GetEmailService();
                    bool ret = emailService.SendEmail(execParam, messageFrom, cimzettek.ToArray(), targy, null, null, true, levelTorzs);

                    if (!ret)
                        throw new Exception("Sikertelen email küldés !");
                    #endregion
                }
            }
            catch (Exception e)
            {
                result = ResultException.GetResultFromException(e);
                Logger.Error(string.Format("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes webservice hiba: {0},{1}", result.ErrorCode, result.ErrorMessage));
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }
        }

        Logger.Debug("Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes webservice vége");
        Logger.DebugEnd();
        return result;
    }
    #region HELPER
    /// <summary>
    /// GetSablonTartalom
    /// </summary>
    /// <param name="models"></param>
    /// <param name="outEmailAddresses"></param>
    /// <returns></returns>
    private string GetSablonTartalom(List<LejartMegorzesiIdoModel> models, out List<string> outEmailAddresses)
    {
        outEmailAddresses = new List<string>();
        if (models == null || models.Count < 1)
            return string.Empty;

        List<string> uniqueIds = models.Select(s => s.Id).Distinct().ToList();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < uniqueIds.Count; i++)
        {
            LejartMegorzesiIdoModel tmpModel = models.FirstOrDefault(f => f.Id == uniqueIds[i]);
            sb.Append("<tr style='border - bottom:1pt solid black;'>");
            sb.Append("<td style=\"border: 1px solid black; padding-left: 5px; padding-right: 5px;\">");
            sb.Append(tmpModel.Azonosito);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append(Environment.NewLine);
        }

        outEmailAddresses =
            models
            .Where(w => (!string.IsNullOrEmpty(w.Csoport_Id_Felelos_Email)) && w.Csoport_Id_Felelos_Email.Contains("@"))
            .Select(s => s.Csoport_Id_Felelos_Email)
            .Distinct()
            .ToList();

        return sb.ToString();
    }
    /// <summary>
    /// GetFelulvizsgalatPageUrl
    /// </summary>
    /// <returns></returns>
    private string GetFelulvizsgalatPageUrl()
    {
        string find = "eAdminBusinessServiceUrl"; /*"eRecordBusinessServiceUrl"*/
        string fullUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(find);
        if (!string.IsNullOrEmpty(fullUrl) && fullUrl.Contains("eAdminWebService"))
        {
            return fullUrl.Replace("eAdminWebService/", "eRecord/FelulvizsgalatList.aspx");
        }
        return string.Empty;
    }

    private Result GetKrtParameterItem(ExecParam execParam, string kodNev)
    {
        Contentum.eAdmin.Service.KRT_ParameterekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_ParameterekService();
        KRT_ParameterekSearch src = new KRT_ParameterekSearch();
        src.Nev.Value = kodNev;
        src.Nev.Operator = Query.Operators.equals;
        src.TopRow = 1;

        Result result = service.GetAll(execParam, src);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("GetKrtParameterItem", execParam, result);
            return null;
        }
        return result;
    }
    /// <summary>
    /// GetKrtParameterNoteValue
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    private string GetKrtParameterNoteValue(Result result)
    {
        if (result == null || !string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
            throw new ResultException(result);

        return result.Ds.Tables[0].Rows[0]["Note"].ToString();
    }
    #endregion
}