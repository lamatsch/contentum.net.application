using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

public partial class EREC_Ira_KuldemenyIratokService : System.Web.Services.WebService
{
    private EREC_Ira_KuldemenyIratokStoredProcedure sp = null;
    public EREC_Ira_KuldemenyIratokService()
    {
        sp = new EREC_Ira_KuldemenyIratokStoredProcedure(this.Application);
    }

    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratok))]
    public Result Get(ExecParam ExecParam)
    {
        return sp.Get(ExecParam);
    }
    
    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratokSearch))]
    public Result GetAll(ExecParam ExecParam, EREC_Ira_KuldemenyIratokSearch _EREC_Ira_KuldemenyIratokSearch)
    {
        return sp.GetAll(ExecParam, _EREC_Ira_KuldemenyIratokSearch);
    }
    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratok))]
    public Result Insert(ExecParam ExecParam, EREC_Ira_KuldemenyIratok Record)
    {
        return sp.Insert(Constants.Insert, ExecParam, Record);
    }
    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratok))]
    public Result Update(ExecParam ExecParam, EREC_Ira_KuldemenyIratok Record)
    {
        return sp.Insert(Constants.Update, ExecParam, Record);
    }
    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        return sp.Invalidate(ExecParam);
    }    
    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [AutoComplete(false)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratok))]
    public Result MultiInvalidate(ExecParam[] ExecParams)
    {        
        for (int i = 0; i < ExecParams.Length; i++)
        {
            Result result = Invalidate(ExecParams[i]);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                //RollBack:
                ContextUtil.SetAbort();
                return result;
            }
        }
        //Commit:
        ContextUtil.SetComplete();
        return new Result();
    }
    [WebMethod(TransactionOption = TransactionOption.RequiresNew)]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_Ira_KuldemenyIratok))]
    public Result Delete(ExecParam ExecParam)
    {
        return sp.Delete(ExecParam);
    }       
}