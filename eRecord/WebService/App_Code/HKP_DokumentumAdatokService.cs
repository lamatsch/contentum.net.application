using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Data;
using Contentum.eUtility;
using System.Xml;
using Contentum.eDocument.Service;
using System.Net;
using System.IO;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class HKP_DokumentumAdatokService : System.Web.Services.WebService
{

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(HKP_DokumentumAdatokSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, HKP_DokumentumAdatokSearch _HKP_DokumentumAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            // CR3114 : HAIR adatok let�lt�se
            _HKP_DokumentumAdatokSearch.Rendszer.Value = "WEB";
            _HKP_DokumentumAdatokSearch.Rendszer.Operator = "=";

            result = sp.GetAllWithExtension(ExecParam, _HKP_DokumentumAdatokSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    // CR3114 : HAIR adatok let�lt�se
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(HKP_DokumentumAdatokSearch))]
    public Result GetAllWithExtension_HAIR(ExecParam ExecParam, HKP_DokumentumAdatokSearch _HKP_DokumentumAdatokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            _HKP_DokumentumAdatokSearch.Rendszer.Value = "HAIR";
            _HKP_DokumentumAdatokSearch.Rendszer.Operator = "=";

            result = sp.GetAllFromeBeadvanyok(ExecParam, _HKP_DokumentumAdatokSearch);

            if (!result.IsError)
            {
                if (result.Ds.Tables[0] != null)
                {

                    result.Ds.Tables[0].Columns.Add(new DataColumn("FileContent", typeof(string)));

                    string url;
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        url = row["External_Link"].ToString();

                        if (!String.IsNullOrEmpty(url))
                        {
                            byte[] cont;
                            try
                            {
                                WebClient wc = new System.Net.WebClient();
                                string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserName");
                                string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServicePassword");
                                string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("eSharePointBusinessServiceUserDomain");
                                wc.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                                cont = wc.DownloadData(url);

                            }
                            catch (Exception exc)
                            {
                                result = ResultException.GetResultFromException(exc);
                                return result;
                            }

                            row["FileContent"] = System.Convert.ToBase64String(cont);
                        }
                    }
                    result.Ds.AcceptChanges();
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// Hivatali kapu modulb�l hivhat�
    /// Az �zenetet beregisztr�lja az adatb�zisba, felt�lti a tartalmat a SP szerverre, �s l�trehozza
    /// a kapcsolatot a SPS �s az EDOK k�z�tt
    /// </summary>
    /// <param name="execParam">�ltal�nos inform�ci�k a futtat� felhaszn�l�r�l</param>
    /// <param name="erec_eMailBoritekok">HKP_DokumentumAdatok t�pus� objektum</param>
    /// <param name="csatolmanyok">Csatolm�nyok t�pus� objektum/param>
    /// <returns>A m�velet eredm�ny�t jelz� Result objektum; siker eset�n az UID mez�ben az �jonnan
    /// l�trej�nn hivatali kapu �zenet Id-ja, egy�bk�nt a hiba k�dja az ErrorCode mez�ben</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(HKP_DokumentumAdatok))]
    public Result InsertAndAttachDocument(ExecParam execParam, HKP_DokumentumAdatok hpk_DokumentumAdatok, Csatolmany csatolmany)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            //Az �zenet r�gz�t�se
            Result _ret = Insert(execParam, hpk_DokumentumAdatok);
            if (_ret.IsError)
            {
                throw new ResultException(_ret);
            }

            if (csatolmany != null)
            {
                hpk_DokumentumAdatok.Id = _ret.Uid;

                Result resUpload = UploadDocument(execParam, hpk_DokumentumAdatok, csatolmany);

                hpk_DokumentumAdatok.Dokumentum_Id = resUpload.Uid;
                hpk_DokumentumAdatok.Updated.Dokumentum_Id = true;

                ExecParam execParamUpdate = execParam.Clone();
                execParamUpdate.Record_Id = hpk_DokumentumAdatok.Id;
                hpk_DokumentumAdatok.Base.Ver = "1";
                hpk_DokumentumAdatok.Base.Updated.Ver = true;

                Result resultUpdate = Update(execParamUpdate, hpk_DokumentumAdatok);

                if (resultUpdate.IsError)
                {
                    throw new ResultException(resultUpdate);
                }
            }


            result = _ret;

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, result.Uid, "HKP_DokumentumAdatok", "DokumentumAdatokLetoltes").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    private Result UploadDocument(ExecParam execParam, HKP_DokumentumAdatok hpk_DokumentumAdatok, Csatolmany csatolmany)
    {
        if (String.IsNullOrEmpty(csatolmany.Nev))
        {
            csatolmany.Nev = hpk_DokumentumAdatok.FileNev;
        }
        // KRT_Dokumentumok objektum l�trehoz�sa
        KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
        if (csatolmany.Megnyithato != null)
        {
            krt_Dokumentumok.Megnyithato = csatolmany.Megnyithato;
        }
        if (csatolmany.Olvashato != null)
        {
            krt_Dokumentumok.Olvashato = csatolmany.Olvashato;
        }
        if (csatolmany.ElektronikusAlairas != null)
        {
            krt_Dokumentumok.ElektronikusAlairas = csatolmany.ElektronikusAlairas;
        }

        DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();


        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                "<iktatokonyv></iktatokonyv>" +
                                                "<sourceSharePath></sourceSharePath>" +
                                                "<foszam>{0}</foszam>" +
                                                "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                "<megjegyzes></megjegyzes>" +
                                                "<munkaanyag>{2}</munkaanyag>" +
                                                "<ujirat>{3}</ujirat>" +
                                                "<vonalkod></vonalkod>" +
                                                "<docmetaIratId></docmetaIratId>" +
                                                "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                "<ucmiktatokonyv>{6}</ucmiktatokonyv>" +
                                            "</uploadparameterek>"
                                            , hpk_DokumentumAdatok.Id
                                            , String.Empty
                                            , "NEM"
                                            , "IGEN"
                                            , "IGEN"
                                            , "HKP"
                                            );

        Result result_upload = documentService.UploadFromeRecordWithCTTCheckin
            (
               execParam
               , Contentum.eUtility.Rendszerparameterek.Get(execParam, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS)
               , csatolmany.Nev
               , csatolmany.Tartalom
               , krt_Dokumentumok
               , uploadXmlStrParams
            );

        if (result_upload.IsError)
        {
            throw new ResultException(result_upload);
        }

        return result_upload;
    }

    /// <summary>
    /// Update(ExecParam ExecParam, HKP_DokumentumAdatok Record)
    /// Az HKP_DokumentumAdatok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param>
    /// <param name="Record">A m�dos�tott adatokat a Record param�ter tartalmazza. </param>
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum.</returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak m�dos�t�sa. A m�dos�tott adatokat a Record param�ter tartalmazza. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(HKP_DokumentumAdatok))]
    public Result Update(ExecParam ExecParam, HKP_DokumentumAdatok Record)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;
        bool sendEmail = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Insert(Constants.Update, ExecParam, Record);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (Record.Updated.Allapot)
            {
                sendEmail = SetIratpeldanyAllapot(ExecParam, Record.Allapot, Record.ErkeztetesiDatum);
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "HKP_DokumentumAdatok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        //C�mzett nem vette �t
        if (sendEmail)
        {
            try
            {
                CimzettNemVetteAt(ExecParam);
            }
            catch (Exception ex)
            {
                log.WsError(ExecParam, ResultException.GetResultFromException(ex));
            }
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private bool SetIratpeldanyAllapot(ExecParam ExecParam, string uzenetAllapot, string erkeztetesiDatum)
    {
        bool ujraKuldendo = false;

        Result resGet = this.Get(ExecParam);

        if (resGet.IsError)
        {
            throw new ResultException(resGet);
        }

        HKP_DokumentumAdatok hkp_dokumentumAdatok = resGet.Record as HKP_DokumentumAdatok;

        string iratPeldany_Id = hkp_dokumentumAdatok.IratPeldany_Id;

        if (!String.IsNullOrEmpty(iratPeldany_Id))
        {
            //C�mzett nem vette �t
            if ("13".Equals(uzenetAllapot))
            {
                bool iratPeldanyUpdated = UpdateIratPelany(ExecParam, iratPeldany_Id, Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Ujrakuldendo, null);
                ujraKuldendo = iratPeldanyUpdated;
            }

            //C�mzett �tvette
            if ("12".Equals(uzenetAllapot))
            {
                bool mindenUzenetAtvett = this.MindenUzenetAtvett(ExecParam, iratPeldany_Id);

                if (mindenUzenetAtvett)
                {
                    UpdateIratPelany(ExecParam, iratPeldany_Id, Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette, erkeztetesiDatum);
                }
            }
        }

        return ujraKuldendo;
    }

    private bool UpdateIratPelany(ExecParam ExecParam, string iratPeldany_Id, string allapot, string atvetelDatuma)
    {
        bool iratPeldanyUpdated = false;

        EREC_PldIratPeldanyokService pldService = new EREC_PldIratPeldanyokService(this.dataContext);
        ExecParam pldXpm = ExecParam.Clone();
        pldXpm.Record_Id = iratPeldany_Id;

        Result res = pldService.Get(pldXpm);

        if (res.IsError)
        {
            throw new ResultException(res);
        }

        EREC_PldIratPeldanyok peldany = res.Record as EREC_PldIratPeldanyok;

        if (!allapot.Equals(peldany.Allapot))
        {

            peldany.Updated.SetValueAll(false);
            peldany.Base.Updated.SetValueAll(true);
            peldany.Base.Updated.Ver = true;

            peldany.Allapot = allapot;
            peldany.Updated.Allapot = true;

            if (!String.IsNullOrEmpty(atvetelDatuma))
            {
                peldany.AtvetelDatuma = atvetelDatuma;
                peldany.Updated.AtvetelDatuma = true;
            }

            Result resUpdate = pldService.Update(pldXpm, peldany);

            if (resUpdate.IsError)
            {
                throw new ResultException(resUpdate);
            }

            iratPeldanyUpdated = true;
        }
        else
        {
            iratPeldanyUpdated = false;
        }

        return iratPeldanyUpdated;

    }

    private bool MindenUzenetAtvett(ExecParam ExecParam, string iratPeldany_Id)
    {
        HKP_DokumentumAdatokSearch search = new HKP_DokumentumAdatokSearch();
        search.IratPeldany_Id.Value = iratPeldany_Id;
        search.IratPeldany_Id.Operator = Query.Operators.equals;

        ExecParam xpmGetAll = ExecParam.Clone();
        Result resGetAll = this.GetAll(xpmGetAll, search);

        if (resGetAll.IsError)
        {
            throw new ResultException(resGetAll);
        }

        bool allIsCimzettAtvett = true;
        foreach (DataRow row in resGetAll.Ds.Tables[0].Rows)
        {
            string allapot = row["Allapot"].ToString();

            if (!"12".Equals(allapot))
            {
                allIsCimzettAtvett = false;
                break;
            }
        }

        return allIsCimzettAtvett;
    }

    private void CimzettNemVetteAt(ExecParam ExecParam)
    {
        bool isConnectionOpenHere = false;
        HKP_DokumentumAdatok hkp_dokumentumAdatok = null;
        string azonosito = String.Empty;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Result resGet = this.Get(ExecParam);

            if (resGet.IsError)
            {
                throw new ResultException(resGet);
            }

            hkp_dokumentumAdatok = resGet.Record as HKP_DokumentumAdatok;

            if (!String.IsNullOrEmpty(hkp_dokumentumAdatok.IratPeldany_Id))
            {
                EREC_PldIratPeldanyokService pldService = new EREC_PldIratPeldanyokService(this.dataContext);
                ExecParam xpmPld = ExecParam.Clone();
                xpmPld.Record_Id = hkp_dokumentumAdatok.IratPeldany_Id;

                Result resPld = pldService.Get(xpmPld);

                if (resPld.IsError)
                {
                    throw new ResultException(resPld);
                }

                EREC_PldIratPeldanyok peldany = resPld.Record as EREC_PldIratPeldanyok;
                azonosito = peldany.Azonosito;
            }
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Contentum.eAdmin.Service.EmailService emailService = Contentum.eUtility.eAdminService.ServiceFactory.GetEmailService();
        ExecParam xpmEmail = ExecParam.Clone();

        bool ret = emailService.SendEmailCimzettNemVetteAt(xpmEmail, hkp_dokumentumAdatok, azonosito);

        if (!ret)
        {
            throw new Exception("E-mail k�ld�s hiba!");
        }
    }

    /// <summary>
    /// Invalidate(ExecParam ExecParam)
    /// Az HKP_DokumentumAdatok t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). 
    /// </summary>
    /// <param name="ExecParam">Az ExecParam bemen� param�ter. Az ExecParam adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal, Record_Id).</param> 
    /// <returns>Hiba�zenet, eredm�nyhalmaz objektum./returns>
    [WebMethod(/*TransactionOption = TransactionOption.Required,*/ Description = "Az adott t�bla t�telazonos�t�val (ExecParam.Record_Id param�ter) kijel�lt rekordj�nak logikai t�rl�se (�rv�nyess�g vege be�ll�t�s). Az ExecParam. adatai a m�velet napl�z�s�hoz sz�ks�gesek (felhaszn�l� azonos�t�, h�v� alkalamaz�s, h�v� HTML oldal).")]
    [System.Xml.Serialization.XmlInclude(typeof(HKP_DokumentumAdatok))]
    public Result Invalidate(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        EREC_eBeadvanyokService eBeadvanyokService = new EREC_eBeadvanyokService(this.dataContext);
        Result result = eBeadvanyokService.Invalidate(ExecParam);

        log.WsEnd(ExecParam, result);
        return result;
    }
}