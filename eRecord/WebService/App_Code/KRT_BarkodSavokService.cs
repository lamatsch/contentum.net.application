using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;

using log4net;
using log4net.Config;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_BarkodSavokService : System.Web.Services.WebService
{
	private static readonly ILog log = LogManager.GetLogger(typeof(Logger));

    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_BarkodSavokSearch))]
    public Result GetAllWithExtensions(ExecParam ExecParam, KRT_BarkodSavokSearch _KRT_BarkodSavokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetAllWithExtension(ExecParam, _KRT_BarkodSavokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(_KRT_BarkodSavokSearch, new KRT_BarkodSavokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_BarkodSavok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_BarkodSavokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(_KRT_BarkodSavokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_BarkodSavokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result BarkodSav_Igenyles(ExecParam ExecParam, String SavType, int FoglalandoDarab)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            result = sp.BarkodSav_Igenyles(ExecParam, SavType, FoglalandoDarab);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // TODO: (k�s�bb majd kivenni, ha a t�rolt elj�r�s m�r vissza tudja adni a gener�lt vonalk�dot is, nem csak az Id-t)
            // Addig is: vonalk�d k�l�n lek�r�se:
            ExecParam execParam_barcodeGet = ExecParam.Clone();
            execParam_barcodeGet.Record_Id = result.Uid;

            Result result_barcodeGet = sp.Get(execParam_barcodeGet);
            if (!String.IsNullOrEmpty(result_barcodeGet.ErrorCode))
            {
                throw new ResultException(result_barcodeGet);
            }
            else
            {
                KRT_BarkodSavok krt_barkodSavok = (KRT_BarkodSavok)result_barcodeGet.Record;
                //result.Record = krt_barkodSavok.Kod;
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_BarkodSavok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result BarkodSav_Feltoltes(ExecParam ExecParam, String SavId, String SavKezdete, String SavVege, String ReszSavFeltoltes)
    {
        log4net.Config.XmlConfigurator.Configure();
        log.Debug("SavId: "+SavId);
        log.Debug("SavKezdete: "+SavKezdete);
        log.Debug("SavVege: "+SavVege);
        log.Debug("ReszSavFeltoltes: "+ReszSavFeltoltes);
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            result = sp.BarkodSav_Feltoltes(ExecParam, SavId, SavKezdete, SavVege, ReszSavFeltoltes);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // TODO: (k�s�bb majd kivenni, ha a t�rolt elj�r�s m�r vissza tudja adni a gener�lt vonalk�dot is, nem csak az Id-t)
            // Addig is: vonalk�d k�l�n lek�r�se:
            ExecParam execParam_barcodeGet = ExecParam.Clone();
            execParam_barcodeGet.Record_Id = result.Uid;

            Result result_barcodeGet = sp.Get(execParam_barcodeGet);
            if (!String.IsNullOrEmpty(result_barcodeGet.ErrorCode))
            {
                throw new ResultException(result_barcodeGet);
            }
            else
            {
                KRT_BarkodSavok krt_barkodSavok = (KRT_BarkodSavok)result_barcodeGet.Record;
                //result.Record = krt_barkodSavok.Kod;
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, SavId, "KRT_BarkodSavok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        log.Debug("Result: "+result);
        return result;
    }

    [WebMethod()]
    public Result BarkodSav_Szetosztas(ExecParam ExecParam, String SavId, String SavKezdete, String SavVege, String Szervezet1Id, String Szervezet2Id)
    {
        log4net.Config.XmlConfigurator.Configure();
        log.Debug("SavId: "+SavId);
        log.Debug("SavKezdete: "+SavKezdete);
        log.Debug("SavVege: "+SavVege);
        log.Debug("Szervezet1Id: "+Szervezet1Id);
        log.Debug("Szervezet2Id: "+Szervezet2Id);
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            result = sp.BarkodSav_Szetosztas(ExecParam, SavId, SavKezdete, SavVege, Szervezet1Id, Szervezet2Id);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // TODO: (k�s�bb majd kivenni, ha a t�rolt elj�r�s m�r vissza tudja adni a gener�lt vonalk�dot is, nem csak az Id-t)
            // Addig is: vonalk�d k�l�n lek�r�se:
            ExecParam execParam_barcodeGet = ExecParam.Clone();
            execParam_barcodeGet.Record_Id = result.Uid;

            Result result_barcodeGet = sp.Get(execParam_barcodeGet);
            if (!String.IsNullOrEmpty(result_barcodeGet.ErrorCode))
            {
                throw new ResultException(result_barcodeGet);
            }
            else
            {
                KRT_BarkodSavok krt_barkodSavok = (KRT_BarkodSavok)result_barcodeGet.Record;
                //result.Record = krt_barkodSavok.Kod;
            }

            #region Esem�nynapl�z�s
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, SavId, "KRT_BarkodSavok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        //log.Debug("Result: "+result);
        return result;
    }

    [WebMethod()]
    public Result BarkodSav_Listazas(ExecParam ExecParam, String SavId, String SzervezetId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Debug("SavId: "+SavId);
        Logger.Debug("SzervezetId: " + SzervezetId);
        
        //string rv = null;
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //rv = sp.BarkodSav_Listazas(ExecParam, SavId, SzervezetId);
            result = sp.BarkodSav_Listazas(ExecParam, SavId, SzervezetId);
            //log.Debug((rv == null ? "rv NULL" : "result OK, "+rv));
            
            ////if (!String.IsNullOrEmpty(result.ErrorCode))
            ////{
            //    //throw new ResultException(result);
            ////}
            //// TODO: (k�s�bb majd kivenni, ha a t�rolt elj�r�s m�r vissza tudja adni a gener�lt vonalk�dot is, nem csak az Id-t)
            //// Addig is: vonalk�d k�l�n lek�r�se:
            //ExecParam execParam_barcodeGet = ExecParam.Clone();
            ////execParam_barcodeGet.Record_Id = result.Uid;

            //Result result_barcodeGet = sp.Get(execParam_barcodeGet);
            //if (!String.IsNullOrEmpty(result_barcodeGet.ErrorCode))
            //{
            //    throw new ResultException(result_barcodeGet);
            //}
            //else
            //{
            //    KRT_BarkodSavok krt_barkodSavok = (KRT_BarkodSavok)result_barcodeGet.Record;
            //    //result.Record = krt_barkodSavok.Kod;
            //}

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            //rv = result.ErrorMessage;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        //log.Debug("Result: "+result);
        //return rv;
        return result;
    }
}
