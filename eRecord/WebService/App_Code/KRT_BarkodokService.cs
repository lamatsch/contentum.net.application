using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_BarkodokService : System.Web.Services.WebService
{

    public Result BarkodGeneralas(ExecParam execParam)
    {
        return BarkodSav_Igenyles(execParam, "G", 1);
    }

    [WebMethod()]
    public Result BarkodSav_Igenyles(ExecParam ExecParam, String SavType, int FoglalandoDarab)
    {
         Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            
            result = sp.BarkodSav_Igenyles(ExecParam, SavType, FoglalandoDarab);
            
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // TODO: (k�s�bb majd kivenni, ha a t�rolt elj�r�s m�r vissza tudja adni a gener�lt vonalk�dot is, nem csak az Id-t)
            // Addig is: vonalk�d k�l�n lek�r�se:
            ExecParam execParam_barcodeGet = ExecParam.Clone();
            execParam_barcodeGet.Record_Id = result.Uid;

            Result result_barcodeGet = sp.Get(execParam_barcodeGet);
            if (!String.IsNullOrEmpty(result_barcodeGet.ErrorCode))
            {
                throw new ResultException(result_barcodeGet);
            }
            else
            {
                KRT_Barkodok krt_barkod = (KRT_Barkodok)result_barcodeGet.Record;
                result.Record = krt_barkod.Kod;
            }
            

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }



    public Result BarkodBindToUgyirat(ExecParam execParam, string barkod_Id, string ugyirat_Id)
    {
        return BarkodBindToObject(execParam, barkod_Id, ugyirat_Id, Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok);
    }

    public Result BarkodBindToIratPeldany(ExecParam execParam, string barkod_Id, string iratPeldany_Id)
    {
        return BarkodBindToObject(execParam, barkod_Id, iratPeldany_Id, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok);
    }

    public Result BarkodBindToIratPeldany(ExecParam execParam, string barkod_Id, string iratPeldany_Id, string barkod_Ver)
    {
        return BarkodBindToObject(execParam, barkod_Id, iratPeldany_Id, Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok,barkod_Ver);
    }

    public Result BarkodBindToKuldemeny(ExecParam execParam, string barkod_Id, string kuldemeny_Id)
    {
        return BarkodBindToObject(execParam, barkod_Id, kuldemeny_Id, Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek);
    }

    public Result BarkodBindToKuldemeny(ExecParam execParam, string barkod_Id, string kuldemeny_Id, string barkodVer)
    {
        return BarkodBindToObject(execParam, barkod_Id, kuldemeny_Id, Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek, barkodVer);
    }

    public Result BarkodBindToKuldMelleklet(ExecParam execParam, string barkod_Id, string melleklet_Id, string barkodVer)
    {
        return BarkodBindToObject(execParam, barkod_Id, melleklet_Id, Contentum.eUtility.Constants.TableNames.EREC_KuldMellekletek, barkodVer);
    }

    public Result BarkodBindToIratMelleklet(ExecParam execParam, string barkod_Id, string iratMelleklet_Id, string barkodVer)
    {
        return BarkodBindToObject(execParam, barkod_Id, iratMelleklet_Id, Contentum.eUtility.Constants.TableNames.EREC_IratMellekletek, barkodVer);
    }

    public Result BarkodBindToKuldTertiveveny(ExecParam execParam, string barkod_Id, string tertiveveny_Id, string barkodVer)
    {
        return BarkodBindToObject(execParam, barkod_Id, tertiveveny_Id, Contentum.eUtility.Constants.TableNames.EREC_KuldTertivevenyek, barkodVer);
    }

    /// <summary>
    /// Barkod rekord m�dos�t�sa: hozz�k�t�se a megadott objektumhoz (�gyirathoz, iratp�ld�nyhoz, k�ldem�nyhez,...)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="barkod_Id"></param>
    /// <param name="Obj_Id"></param>
    /// <param name="Obj_type"></param>
    /// <param name="barkodVer">barkode record verzi�ja</param>
    /// <returns></returns>
    public Result BarkodBindToObject(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type, string barkodVer)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        if (execParam == null || String.IsNullOrEmpty(barkod_Id) || String.IsNullOrEmpty(Obj_Id)
            || String.IsNullOrEmpty(Obj_type))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52480);
            log.WsEnd(execParam, result1);
            return result1;
        }
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            KRT_Barkodok krt_Barkod = new KRT_Barkodok();
            krt_Barkod.Updated.SetValueAll(false);
            krt_Barkod.Base.Updated.SetValueAll(false);

            // Verzi�
            if (!String.IsNullOrEmpty(barkodVer))
            {
                krt_Barkod.Base.Ver = barkodVer;
            }
            else
            {
                krt_Barkod.Base.Ver = "1";
            }
            krt_Barkod.Base.Updated.Ver = true;

            krt_Barkod.Obj_Id = Obj_Id;
            krt_Barkod.Updated.Obj_Id = true;

            krt_Barkod.Obj_type = Obj_type;
            krt_Barkod.Updated.Obj_type = true;

            krt_Barkod.Allapot = "F";
            krt_Barkod.Updated.Allapot = true;

            execParam.Record_Id = barkod_Id;


            result = this.Update(execParam, krt_Barkod);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(execParam, result);
        return result;
    }


    /// <summary>
    /// Barkod rekord m�dos�t�sa: hozz�k�t�se a megadott objektumhoz (�gyirathoz, iratp�ld�nyhoz, k�ldem�nyhez,...)
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="barkod_Id"></param>
    /// <param name="Obj_Id"></param>
    /// <param name="Obj_type"></param>
    /// <returns></returns>
    private Result BarkodBindToObject(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type)
    {
        return this.BarkodBindToObject(execParam, barkod_Id, Obj_Id,Obj_type, String.Empty);
    }

    [WebMethod()]
    public Result Atadas_Tomeges(ExecParam execParam, 
        String[] erec_UgyUgyiratok_Id_Array, String[] erec_KuldKuldemenyek_Id_Array, String[] erec_PldIratPeldanyok_Id_Array, String[] krt_mappak_Id_Array,
        String csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            // iratp�ld�ny:
            if (erec_PldIratPeldanyok_Id_Array != null && erec_PldIratPeldanyok_Id_Array.Length > 0)
            {
                EREC_PldIratPeldanyokService service_pld = new EREC_PldIratPeldanyokService(this.dataContext);

                result = service_pld.AtadasraKijeloles_Tomeges(execParam, erec_PldIratPeldanyok_Id_Array
                    , csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }
            }

            // �gyirat:
            if (erec_UgyUgyiratok_Id_Array != null && erec_UgyUgyiratok_Id_Array.Length > 0)
            {
                EREC_UgyUgyiratokService service_ugyiratok = new EREC_UgyUgyiratokService(this.dataContext);

                result = service_ugyiratok.AtadasraKijeloles_Tomeges(execParam
                    , erec_UgyUgyiratok_Id_Array, csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }
            }

            // k�ldem�ny:
            if (erec_KuldKuldemenyek_Id_Array != null && erec_KuldKuldemenyek_Id_Array.Length > 0)
            {
                EREC_KuldKuldemenyekService service_kuld = new EREC_KuldKuldemenyekService(this.dataContext);

                result = service_kuld.AtadasraKijeloles_Tomeges(execParam, erec_KuldKuldemenyek_Id_Array
                    , csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }
            }

            // dosszi�k:
            if (krt_mappak_Id_Array != null && krt_mappak_Id_Array.Length > 0)
            {
                KRT_MappakService service_mappak = new KRT_MappakService(this.dataContext);

                result = service_mappak.AtadasraKijeloles_Tomeges(execParam, krt_mappak_Id_Array
                    , csoport_Id_Felelos_Kovetkezo, erec_HataridosFeladatok);

                if (result.IsError)
                {
                    // hiba:
                    throw new ResultException(result);
                }
            }


            #region k�zbes�t�si t�telek �tadottra �ll�t�sa:
            // a 4 t�mbb�l egy legy�rt�sa:
            int obj_count = 0;
            int obj_count_ugyirat = 0;
            int obj_count_pld = 0;
            int obj_count_kuld = 0;
            int obj_count_dosszie = 0;
            if (erec_UgyUgyiratok_Id_Array != null)
            {
                obj_count_ugyirat = erec_UgyUgyiratok_Id_Array.Length;
                obj_count += obj_count_ugyirat;
            }
            if (erec_KuldKuldemenyek_Id_Array != null)
            {
                obj_count_kuld = erec_KuldKuldemenyek_Id_Array.Length;
                obj_count += obj_count_kuld;
            }
            if (erec_PldIratPeldanyok_Id_Array != null)
            {
                obj_count_pld = erec_PldIratPeldanyok_Id_Array.Length;
                obj_count += obj_count_pld;
            }
            if (krt_mappak_Id_Array != null)
            {
                obj_count_dosszie = krt_mappak_Id_Array.Length;
                obj_count += obj_count_dosszie;
            }

            String[] obj_Ids_Array = new String[obj_count];
            //for (int i = 0; i < obj_count_ugyirat; i++)
            //{
            //    obj_Ids_Array[i] = erec_UgyUgyiratok_Id_Array[i];
            //}
            //for (int i = 0; i < obj_count_kuld; i++)
            //{
            //    obj_Ids_Array[obj_count_ugyirat + i] = erec_KuldKuldemenyek_Id_Array[i];
            //}
            //for (int i = 0; i < obj_count_pld; i++)
            //{
            //    obj_Ids_Array[obj_count_ugyirat + obj_count_kuld + i] = erec_PldIratPeldanyok_Id_Array[i];
            //}
            //for (int i = 0; i < obj_count_dosszie; i++)
            //{
            //    obj_Ids_Array[obj_count_ugyirat + obj_count_kuld + obj_count_pld + i] = krt_mappak_Id_Array[i];
            //}

            int currentStartIndex = 0;
            if (obj_count_ugyirat > 0)
            {
                erec_UgyUgyiratok_Id_Array.CopyTo(obj_Ids_Array, currentStartIndex);
                currentStartIndex += obj_count_ugyirat;
            }
            if (obj_count_kuld > 0)
            {
                erec_KuldKuldemenyek_Id_Array.CopyTo(obj_Ids_Array, currentStartIndex);
                currentStartIndex += obj_count_kuld;
            }
            if (obj_count_pld > 0)
            {
                erec_PldIratPeldanyok_Id_Array.CopyTo(obj_Ids_Array, currentStartIndex);
                currentStartIndex += obj_count_pld;
            }
            if (obj_count_dosszie > 0)
            {
                krt_mappak_Id_Array.CopyTo(obj_Ids_Array, currentStartIndex);
            }
            
            EREC_IraKezbesitesiTetelekService service = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbTetelAtadas = execParam.Clone();

            Result result_kezbTetelAtadas = service.SetKezbesitesiTetelToAtadott_Tomeges(execParam_kezbTetelAtadas, obj_Ids_Array);
            if (result_kezbTetelAtadas.IsError)
            {
                throw new ResultException(result_kezbTetelAtadas);
            }

            #endregion

            result = result_kezbTetelAtadas;

            //string objIds = String.Format("'{0}'", String.Join("','", obj_Ids_Array));
            #region Esem�nynapl�z�s
            //if (isTransactionBeginHere)
            //{
            //    KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            //    Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, objIds, "VonalkodosAtadas");
            //    if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
            //        Logger.Debug("[ERROR]KRT_Barkodok::AtadasTomeges: ", execParam, eventLogResult);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }






    [WebMethod()]
    public Result Atvetel_Tomeges(ExecParam execParam,
        String[] erec_UgyUgyiratok_Id_Array, String[] erec_KuldKuldemenyek_Id_Array, String[] erec_PldIratPeldanyok_Id_Array, String[] krt_mappak_Id_Array)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region az objektumokhoz tartoz� k�zbes�t�si t�telek lek�r�se
            // a 4 t�mbb�l egy lista �s egy dictionary legy�rt�sa:
            System.Collections.Generic.List<string> obj_Ids_Array = new System.Collections.Generic.List<string>();
            System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<string>> dictObjTypes = new System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<string>>();

            if (erec_UgyUgyiratok_Id_Array != null && erec_UgyUgyiratok_Id_Array.Length > 0)
            {
                obj_Ids_Array.AddRange(erec_UgyUgyiratok_Id_Array);
                dictObjTypes.Add(Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok, new System.Collections.Generic.List<string>(erec_UgyUgyiratok_Id_Array));
            }
            if (erec_KuldKuldemenyek_Id_Array != null && erec_KuldKuldemenyek_Id_Array.Length > 0)
            {
                obj_Ids_Array.AddRange(erec_KuldKuldemenyek_Id_Array);
                dictObjTypes.Add(Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek, new System.Collections.Generic.List<string>(erec_KuldKuldemenyek_Id_Array));
            }
            if (erec_PldIratPeldanyok_Id_Array != null && erec_PldIratPeldanyok_Id_Array.Length > 0)
            {
                obj_Ids_Array.AddRange(erec_PldIratPeldanyok_Id_Array);
                dictObjTypes.Add(Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok, new System.Collections.Generic.List<string>(erec_PldIratPeldanyok_Id_Array));
            }
            if (krt_mappak_Id_Array != null && krt_mappak_Id_Array.Length > 0)
            {
                obj_Ids_Array.AddRange(krt_mappak_Id_Array);
                dictObjTypes.Add(Contentum.eUtility.Constants.TableNames.KRT_Mappak, new System.Collections.Generic.List<string>(krt_mappak_Id_Array));
            }

            if (obj_Ids_Array.Count > 0)
            {
                EREC_IraKezbesitesiTetelekService kezbesitesiTetelekService = new EREC_IraKezbesitesiTetelekService(this.dataContext);
                EREC_IraKezbesitesiTetelekSearch kezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

                kezbesitesiTetelekSearch.Obj_Id.Value = Search.GetSqlInnerString(obj_Ids_Array.ToArray()); //objIds;
                kezbesitesiTetelekSearch.Obj_Id.Operator = Query.Operators.inner;

                kezbesitesiTetelekSearch.Allapot.Value = Search.GetSqlInnerString(new string[] { Contentum.eUtility.Constants.KezbesitesiTetel_Allapot.Atadasra_kijelolt
                , Contentum.eUtility.Constants.KezbesitesiTetel_Allapot.Atadott });
                kezbesitesiTetelekSearch.Allapot.Operator = Query.Operators.inner;

                Result kezbesitesiTetelekGetAllResult = kezbesitesiTetelekService.GetAll(execParam.Clone(), kezbesitesiTetelekSearch);
                if (kezbesitesiTetelekGetAllResult.IsError)
                    throw new ResultException(kezbesitesiTetelekGetAllResult);

                // k�zbes�t�si t�telek id-jainak �sszegy�jt�se + objektumok id-jainak �sszegy�jt�se, amikhez nem tartozik k�zb.t�tel
                //string kezbesitesiTetelIds = string.Empty;
                System.Collections.Generic.List<string> kezbesitesiTetelIds = new System.Collections.Generic.List<string>(kezbesitesiTetelekGetAllResult.Ds.Tables[0].Rows.Count);
                foreach (DataRow r in kezbesitesiTetelekGetAllResult.Ds.Tables[0].Rows)
                {
                    kezbesitesiTetelIds.Add(r["id"].ToString());

                    string obj_id = r["Obj_Id"].ToString();
                    string obj_type = r["Obj_type"].ToString();

                    // a megtal�ltakat elt�vol�tjuk a list�b�l
                    if (dictObjTypes.ContainsKey(obj_type))
                    {
                        if (dictObjTypes[obj_type].Contains(obj_id))
                        {
                            dictObjTypes[obj_type].Remove(obj_id);
                        }
                    }
                }

                #endregion

                #region k�zbes�t�si t�tel l�trehoz�sa azokhoz a t�telekhez, amikhez nem volt
                EREC_IraKezbesitesiTetelek kezbesitesiTetel = new EREC_IraKezbesitesiTetelek();
                Result kezbesitesiTetelInsertResult = new Result();

                string strDate = DateTime.Now.ToString();

                // mind (�gyiratok, k�ldem�nyek, iratp�ld�nyok, dosszi�k)
                foreach (string key in dictObjTypes.Keys)
                {
                    foreach (string id in dictObjTypes[key])
                    {
                        kezbesitesiTetel.Obj_Id = id;
                        kezbesitesiTetel.Updated.Obj_Id = true;

                        kezbesitesiTetel.Obj_type = key;
                        kezbesitesiTetel.Updated.Obj_type = true;

                        kezbesitesiTetel.Csoport_Id_Cel = execParam.Felhasznalo_Id;
                        kezbesitesiTetel.Updated.Csoport_Id_Cel = true;

                        kezbesitesiTetel.Felhasznalo_Id_Atado_USER = execParam.Felhasznalo_Id;
                        kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_USER = true;

                        kezbesitesiTetel.Felhasznalo_Id_Atado_LOGIN = execParam.LoginUser_Id;
                        kezbesitesiTetel.Updated.Felhasznalo_Id_Atado_LOGIN = true;

                        kezbesitesiTetel.AtadasDat = strDate;
                        kezbesitesiTetel.Updated.AtadasDat = true;

                        kezbesitesiTetel.Allapot = Contentum.eUtility.Constants.KezbesitesiTetel_Allapot.Atadott;
                        kezbesitesiTetel.Updated.Allapot = true;

                        kezbesitesiTetel.Base.Note = "VonalkodosAtvetelTomeges";
                        kezbesitesiTetel.Base.Updated.Note = true;

                        kezbesitesiTetelInsertResult = kezbesitesiTetelekService.Insert(execParam, kezbesitesiTetel);
                        if (kezbesitesiTetelInsertResult.IsError)
                        {
                            throw new ResultException(kezbesitesiTetelInsertResult);
                        }
                        kezbesitesiTetelIds.Add(kezbesitesiTetelInsertResult.Uid);
                    }
                }
                #endregion


                #region K�zbes�t�si t�tel szerinti Tomeges_Atvetel

                result = kezbesitesiTetelekService.Atvetel_Tomeges(execParam.Clone(), kezbesitesiTetelIds.ToArray());
                if (result.IsError)
                    throw new ResultException(result);

                #endregion
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }


    [WebMethod(Description = "Vonalk�d regisztr�lts�g�nak �s foglalts�g�nak ellen�rz�se. Ha a vonalk�d nem szerepel a rendszerben, vagy foglalt, a Result objektumban a megfelel� hib�t adja vissza, egy�bk�nt a result.Uid tartalmazza a megtal�lt szabad vonalk�d azonos�t�j�t (Id).")]
    public Result CheckBarcode(ExecParam execParam, string barkodValue)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        //nincs megadva barcode
        if (String.IsNullOrEmpty(barkodValue.Trim()))
        {
            return result;
        }

        try
        {
            KRT_BarkodokSearch srchBarkod = new KRT_BarkodokSearch();
            srchBarkod.Kod.Value = barkodValue;
            srchBarkod.Kod.Operator = Query.Operators.equals;
            //srchBarkod.Allapot.Value = "S";
            //srchBarkod.Allapot.Operator = Query.Operators.equals;
            srchBarkod.KodType.Value = "'N','P','G'";
            srchBarkod.KodType.Operator = Query.Operators.inner;

            result = this.GetAll(execParam, srchBarkod);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count == 0)
            {
                //A vonalk�d nincs regisztr�lva
                throw new ResultException(52481);
            }

            if (result.Ds.Tables[0].Rows[0]["Allapot"].ToString() != "S")
            {
                //A vonalk�d nem szabad
                throw new ResultException(52482);
            }
            //Id visszaad�sa
            result.Uid = result.Ds.Tables[0].Rows[0]["Id"].ToString();
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    public Result FreeBarcode(ExecParam execParam, string barkodValue)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        //nincs megadva barcode
        if (String.IsNullOrEmpty(barkodValue.Trim()))
        {
            return result;
        }

        try
        {
            KRT_BarkodokSearch srchBarkod = new KRT_BarkodokSearch();
            srchBarkod.Kod.Value = barkodValue;
            srchBarkod.Kod.Operator = Query.Operators.equals;

            result = this.GetAll(execParam, srchBarkod);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count == 0)
            {
                //A vonalk�d nincs regisztr�lva, itt nem hiba
                throw new ResultException(result);
            }

            //Felszabad�t�s elv�gz�se
            KRT_Barkodok barcode = new KRT_Barkodok();
            Utility.LoadBusinessDocumentFromDataRow(barcode,result.Ds.Tables[0].Rows[0]);
            barcode.Updated.SetValueAll(false);
            barcode.Base.Updated.SetValueAll(false);
            barcode.Base.Updated.Ver = true;

            barcode.Obj_Id = String.Empty;
            barcode.Updated.Obj_Id = true;
            barcode.Typed.Obj_type = System.Data.SqlTypes.SqlString.Null;
            barcode.Updated.Obj_type = true;
            barcode.Allapot = "S";
            barcode.Updated.Allapot = true;

            //Id visszaad�sa
            ExecParam xpm = execParam.Clone();
            xpm.Record_Id = barcode.Id;
            result = this.Update(xpm, barcode);
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    /// <summary>
    /// Result.Record -ban visszaadja a k�dhoz tartoz� vonalk�d objektum-ot
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="?"></param>
    /// <returns></returns>
    public Result GetBarcodeByValue(ExecParam execParam, string barkodValue)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        //nincs megadva barcode
        if (String.IsNullOrEmpty(barkodValue.Trim()))
        {
            return result;
        }

        try
        {
            KRT_BarkodokSearch srchBarkod = new KRT_BarkodokSearch();
            srchBarkod.Kod.Value = barkodValue;
            srchBarkod.Kod.Operator = Query.Operators.equals;

            result = this.GetAll(execParam, srchBarkod);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result.Ds.Tables[0].Rows.Count == 0)
            {
                //A vonalk�d nincs regisztr�lva
                throw new ResultException(52481);
            }

            //Barkod objektum felt�lt�se
            KRT_Barkodok barcode = new KRT_Barkodok();
            Utility.LoadBusinessDocumentFromDataRow(barcode,result.Ds.Tables[0].Rows[0]);
            result.Ds = null;
            result.Record = barcode;
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    /// <summary>
    /// Keres�s vonalk�dok alapj�n
    /// A tal�lati list�ban benne lesznek a vonalk�dokhoz k�t�tt objektumok azonos�t�i is
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="krt_barkodokSearch">Keres�si objektum, ebben adhat�k meg a sz�r�si felt�telek (vonalk�dra sz�r�s)</param>
    /// <returns></returns>
    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_BarkodokSearch))]
    public Result GetAllWithExtension(ExecParam execParam, KRT_BarkodokSearch krt_barkodokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(execParam, krt_barkodokSearch);

            #region Esem�nynapl�z�s
            if (!Search.IsIdentical(krt_barkodokSearch, new KRT_BarkodokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, null, "KRT_Barkodok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(krt_barkodokSearch);

                    #region where felt�tel �ssze�ll�t�sa
                    if (!string.IsNullOrEmpty(krt_barkodokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = krt_barkodokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(execParam, eventLogRecord);
                }
            }
            #endregion
            
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        
        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod()]
    public Result BarCodeBindToObject(ExecParam execParam, string barkod_Id, string Obj_Id, string Obj_type)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();
            return this.BarkodBindToObject(execParam, barkod_Id, Obj_Id, Obj_type, String.Empty);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }
}
