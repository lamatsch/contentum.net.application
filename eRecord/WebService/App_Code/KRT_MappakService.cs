﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Contentum.eQuery;
using System.Data;

public partial class KRT_MappakService : System.Web.Services.WebService
{
    
    [WebMethod()]
    public Result GetWithTreeAndRightCheck(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetWithTreeAndRightCheck(execParam);

            #region //Eseménynaplózás
            //KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

            //KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "EREC_UgyUgyiratok", "View").Record;

            //eventLogService.Insert(execParam, eventLogRecord);
            #endregion


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result InvalidateWithChildrenCheck(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.InvalidateWithChildrenCheck(ExecParam);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, ExecParam.Record_Id, "KRT_Mappak", "Invalidate").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

    private Result GetAllParents(ExecParam execParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllParents(execParam);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result InsertAndAttachBarcode(ExecParam execParam, KRT_Mappak krt_mappak, bool isPublic)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            if (!String.IsNullOrEmpty(krt_mappak.SzuloMappa_Id))
            {
                ExecParam execParam_szulok = execParam.Clone();
                execParam_szulok.Record_Id = krt_mappak.SzuloMappa_Id;
                Result result_szulok = this.GetAllParents(execParam_szulok);
                if (result_szulok.IsError)
                {
                    throw new ResultException(result_szulok);
                }
                
                if (krt_mappak.Tipus == KodTarak.MAPPA_TIPUS.Virtualis
                    && result_szulok.Ds.Tables[0].Select(String.Format("Tipus='{0}'", KodTarak.MAPPA_TIPUS.Fizikai)).Length > 0)
                { // Fizikai dossziéban nem hozható létre virtuális mappa!
                    throw new ResultException(64040);
                }

                if (result_szulok.Ds.Tables[0].Select(String.Format("Tipus='{0}' and Csoport_Id_Tulaj <> '{1}'", KodTarak.MAPPA_TIPUS.Fizikai, execParam.Felhasznalo_Id)).Length > 0)
                {
                    // A dosszié nem helyezhető a szülőnek kiválaszott fizikai dossziéba, mert a dossziék tulajdonosai különböznek!
                    throw new ResultException(64038); 
                }
            }

            result = this.Insert(execParam.Clone(), krt_mappak);

            if (result.IsError)
                throw new ResultException(result);


            #region Vonalkód regisztrálása

            if (!string.IsNullOrEmpty(krt_mappak.BarCode))
            {
                KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);

                Result barkodResult = barkodService.GetBarcodeByValue(execParam, krt_mappak.BarCode);

                if (barkodResult.IsError)
                    throw new ResultException(barkodResult);

                KRT_Barkodok barkod = (KRT_Barkodok)barkodResult.Record;

                barkodResult = barkodService.BarkodBindToObject(execParam.Clone(), barkod.Id, result.Uid, "KRT_Mappak", "1");


                if (barkodResult.IsError)
                    throw new ResultException(barkodResult);
            }

            #endregion Vonalkód regisztrálása


            #region Publikus mappához jog a szervezetnek

            if (isPublic)
            {
                RightsService aclService = new RightsService(this.dataContext);
                
                Result aclResult = aclService.AddCsoportToJogtargy(execParam.Clone(), result.Uid, execParam.FelhasznaloSzervezet_Id, 'I');

                if (aclResult.IsError)
                    throw new ResultException(aclResult);
            }

            #endregion Publikus mappához jog a szervezetnek


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id,result.Uid, "KRT_Mappak", "Insert").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod()]
    public Result UpdateWithCheck(ExecParam execParam, KRT_Mappak krt_mappak)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = this.Get(execParam.Clone());

            if (result.IsError)
                throw new ResultException(result);

            KRT_Mappak oldMappa = (KRT_Mappak)result.Record;

            
            // Típus változott-e
            if (oldMappa.Tipus != krt_mappak.Tipus && krt_mappak.Tipus == KodTarak.MAPPA_TIPUS.Fizikai)
            {
                //TODO: a mappa tartalmát ellenőrizni kell, hogy minden a felhasználónál van-e
            }

            // vonalkód változott-e
            if (oldMappa.BarCode != krt_mappak.BarCode)
            {
                KRT_BarkodokService barkodService = new KRT_BarkodokService(this.dataContext);

                Result barkodResult = null;

                // régi vonalkódot felszabadítjuk, ha meg volt adva
                if (!string.IsNullOrEmpty(oldMappa.BarCode))
                {
                    barkodResult = barkodService.FreeBarcode(execParam.Clone(), oldMappa.BarCode);

                    if (barkodResult.IsError)
                        throw new ResultException(barkodResult);
                }

                // új vonalkódot lecsekkoljuk és hozzákötjük a mappához
                if (!string.IsNullOrEmpty(krt_mappak.BarCode))
                {
                    barkodResult = barkodService.CheckBarcode(execParam.Clone(), oldMappa.BarCode);

                    if (barkodResult.IsError)
                        throw new ResultException(barkodResult);


                    barkodResult = barkodService.GetBarcodeByValue(execParam.Clone(), krt_mappak.BarCode);

                    if (barkodResult.IsError)
                        throw new ResultException(barkodResult);

                    KRT_Barkodok barkod = (KRT_Barkodok)barkodResult.Record;

                    barkodResult = barkodService.BarkodBindToObject(execParam.Clone(), barkod.Id, execParam.Record_Id, "KRT_Mappak", barkod.Base.Ver);

                    if (barkodResult.IsError)
                        throw new ResultException(barkodResult);
                }
            }

            result = sp.UpdateWithCheck(execParam, krt_mappak);

            if (result.IsError)
                throw new ResultException(result);


            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(execParam, dataContext.Tranz_Id, execParam.Record_Id, "KRT_Mappak", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;

    }

    [WebMethod()]
    public Result GetAllWithExtension(ExecParam execParam, KRT_MappakSearch search)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllWithExtension(execParam, search,false);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result GetAllWithExtensionAndJogosultak(ExecParam execParam, KRT_MappakSearch search)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetAllWithExtension(execParam, search, true);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result AtadasraKijeloles_Tomeges(ExecParam execParam, string[] dosszieIds, string csoport_Id_Felelos_Kovetkezo, EREC_HataridosFeladatok erec_HataridosFeladatok)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            if (dosszieIds == null || dosszieIds.Length == 0)
            {
                // hiba
                throw new ResultException(52200);
            }
            else
            {
                Result result_atadasraKijeloles = new Result();

                // az összes ügyirat lekérése
                //string dosszieIdsConc = "'" + dosszieIds[0] + "'";

                //for (int i = 1; i < dosszieIds.Length; i++)
                //{
                //    dosszieIdsConc += ",'" + dosszieIds[i] + "'";
                //}
                string dosszieIdsConc = Search.GetSqlInnerString(dosszieIds);

                result = sp.AtadasraKijeloles_Tomeges(execParam.Clone(), dosszieIdsConc, csoport_Id_Felelos_Kovetkezo);

                #region Határidős Feladat

                if (erec_HataridosFeladatok != null)
                {
                    EREC_HataridosFeladatokService svcHataridosFeladat = new EREC_HataridosFeladatokService(dataContext);
                    svcHataridosFeladat.SimpleInsertTomeges(execParam.Clone(), erec_HataridosFeladatok, Contentum.eUtility.Constants.TableNames.KRT_Mappak, dosszieIds);
                }

                #endregion

                if (!string.IsNullOrEmpty(result.ErrorCode))
                    throw new ResultException(result);

                #region Eseménynaplózás
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
                Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, dosszieIdsConc, "MappakAtadas");
                if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                    Logger.Debug("[ERROR]KRT_Mappak::AtadasTomeges: ", execParam, eventLogResult);
                #endregion

            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    [WebMethod()]
    public Result AtadasraKijelolesSztorno(ExecParam execParam, string dosszieId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Ellenőrzések

            result = this.GetStatus(execParam, dosszieId);

            DosszieStatus status = new DosszieStatus(result);

            // Nem is adták át a dossziét
            if (status.Allapot != DosszieStatus.AllapotTipus.TovabbitasAlatt)
                throw new ResultException(64031);

            // Nem a user indította az átadást
            if (status.AtadoUser != execParam.Felhasznalo_Id)
                throw new ResultException(64032);

            #endregion Ellenőrzések

            // dossziéban lévő objektumok átadásának sztornózása
            result = sp.MappaTartalmak_AtadasraKijelolesSztorno(execParam.Clone(), dosszieId);

            if (result.IsError)
                throw new ResultException(result);


            #region Kézbesítési tétel sztornózása

            /// Kézbesítési tételek vizsgálata:
            /// ha volt a rekordra kézbesítési tétel (átadásra kijelölt), kézb. tétel érvénytelenítése:
            /// 
            EREC_IraKezbesitesiTetelekService service_KezbesitesiTetelek = new EREC_IraKezbesitesiTetelekService(this.dataContext);
            ExecParam execParam_kezbTetel = execParam.Clone();

            Result result_kezbTetelUpdate =
                service_KezbesitesiTetelek.AtadasraKijeloltekInvalidate(execParam_kezbTetel, dosszieId);

            if (result_kezbTetelUpdate.IsError)
                throw new ResultException(result_kezbTetelUpdate);

            #endregion Kézbesítési tétel sztornózása

            #region Eseménynaplózás
            //if (isTransactionBeginHere)
            //{
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, dosszieId, "KRT_Mappak", "MappakAtadasSztorno").Record;

                Result eventLogResult = eventLogService.Insert(execParam, eventLogRecord);
            //}
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public Result Atvetel_Tomeges(ExecParam execParam, string dosszieIds, string KovetkezoOrzoId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.Atvetel_Tomeges(execParam, dosszieIds, KovetkezoOrzoId);

            #region Eseménynaplózás
            KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);
            Result eventLogResult = eventLogService.InsertTomegesByFunkcioKod(execParam, dosszieIds, "MappakAtvetel");
            if (!string.IsNullOrEmpty(eventLogResult.ErrorCode))
                Logger.Debug("[ERROR]KRT_Mappak::AtvetelTomeges: ", execParam, eventLogResult);
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    public void AddKezelesiFeljegyzes(ExecParam p_ExecParam, string p_MappaID, string p_Leiras, string p_Tipus)
    {
        Logger.Debug("HataridosFeladatokService Insert start", p_ExecParam);
        Logger.Debug("Leiras: " + p_Leiras ?? "NULL");
        Logger.Debug("MappaID: " + p_MappaID ?? "NULL");
        Logger.Debug("Tipus: " + p_Tipus ?? "NULL");

        EREC_HataridosFeladatokService erec_HataridosFeladatokService = new EREC_HataridosFeladatokService(this.dataContext);
        ExecParam execParam_kezFeljInsert = p_ExecParam.Clone();

        EREC_HataridosFeladatok erec_HataridosFeladatok = new EREC_HataridosFeladatok();
        erec_HataridosFeladatok.Updated.SetValueAll(false);
        erec_HataridosFeladatok.Base.Updated.SetValueAll(false);

        // Dossziéhoz kötés
        erec_HataridosFeladatok.Obj_Id = p_MappaID;
        erec_HataridosFeladatok.Updated.Obj_Id = true;

        erec_HataridosFeladatok.Obj_type = Contentum.eUtility.Constants.TableNames.KRT_Mappak;
        erec_HataridosFeladatok.Updated.Obj_type = true;

        erec_HataridosFeladatok.Leiras = p_Leiras;
        erec_HataridosFeladatok.Updated.Leiras = true;

        erec_HataridosFeladatok.Altipus = p_Tipus;
        erec_HataridosFeladatok.Updated.Altipus = true;

        erec_HataridosFeladatok.Tipus = KodTarak.FELADAT_TIPUS.Megjegyzes;
        erec_HataridosFeladatok.Updated.Tipus = true;

        Result result_kezFeljInsert = erec_HataridosFeladatokService.Insert(execParam_kezFeljInsert, erec_HataridosFeladatok);
        if (!String.IsNullOrEmpty(result_kezFeljInsert.ErrorCode))
        {
            // hiba:
            Logger.Error("HataridosFeladatokService Insert hiba", execParam_kezFeljInsert, result_kezFeljInsert);
            throw new ResultException(result_kezFeljInsert);
        }
        Logger.Debug("HataridosFeladatokService Insert end", p_ExecParam);
    }
    #region Visszaküldés
    /// <summary>
    /// Dosszié átvételének visszautasítása, dosszié visszaküldése
    /// A kézbesítési tételt a hívó szervíznek kell módosítania a sikeres futás után!
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="erec_IraKezbesitesiTetelek"></param>
    /// <returns></returns>
    public Result Visszakuldes(ExecParam execParam, EREC_IraKezbesitesiTetelek erec_IraKezbesitesiTetelek)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());
        // Paraméterek ellenőrzése:
        if (execParam == null || String.IsNullOrEmpty(execParam.Felhasznalo_Id)
            || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_Id) || String.IsNullOrEmpty(erec_IraKezbesitesiTetelek.Obj_type))
        {
            // hiba
            Result result1 = ResultError.CreateNewResultWithErrorCode(52230);
            log.WsEnd(execParam, result1);
            return result1;
        }

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            #region Kézbesítési tétel ellenőrzés
            Result result_kezbesitesiTetelCheck = EREC_IraKezbesitesiTetelekService.CheckKezbesitesiTetelVisszakuldheto(execParam, erec_IraKezbesitesiTetelek);
            if (result_kezbesitesiTetelCheck.IsError)
            {
                throw new ResultException(result_kezbesitesiTetelCheck);
            }

            if (erec_IraKezbesitesiTetelek.Obj_type != Contentum.eUtility.Constants.TableNames.KRT_Mappak)
            {
                Logger.Error(String.Format("Kézbesítési tételből kapott objektum típus: {0}; Várt objektum típus: {1}"
                    , erec_IraKezbesitesiTetelek.Obj_type
                    , Contentum.eUtility.Constants.TableNames.KRT_Mappak));

                // Hiba a visszaküldés során: a kézbesítési tétel objektum típusa nem megfelelő!
                throw new ResultException(53707);
            }
            #endregion Kézbesítési tétel ellenőrzés

            string mappa_Id = erec_IraKezbesitesiTetelek.Obj_Id;
            string visszakuldesIndoka = erec_IraKezbesitesiTetelek.Base.Note;

            #region Ellenőrzések

            result = this.GetStatus(execParam, mappa_Id);

            DosszieStatus status = new DosszieStatus(result);

            // Nem adták át a dossziét
            if (status.Allapot != DosszieStatus.AllapotTipus.TovabbitasAlatt)
            {
                // A dosszié nem küldhető vissza az átadónak, mert nem továbbították!
                ErrorDetails errorDetail = new ErrorDetails(53711
                    , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.KRT_Mappak
                    , mappa_Id
                    , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Felhasznalo_Id_Atado_USER
                    , status.AtadoUser);
                throw new ResultException(53710, errorDetail);
            }

            if (status.AtadoUser == execParam.Felhasznalo_Id)
            {
                // A tétel nem küldhető vissza, mert azt Ön adta át és csak a címzett küldheti vissza Önnek!
                ErrorDetails errorDetail = new ErrorDetails(53702
                    , Contentum.eUtility.Constants.ErrorDetails.ObjectTypes.KRT_Mappak
                    , mappa_Id
                    , Contentum.eUtility.Constants.ErrorDetails.ColumnTypes.Csoport_Id_Cel
                    , status.CimzettUser);
                // A dosszié nem küldhető vissza!
                throw new ResultException(53710, errorDetail);
            }

            #endregion Ellenőrzések

            #region Dosszié objektum lekérése
            ExecParam execParam_MappaGet = execParam.Clone();
            execParam_MappaGet.Record_Id = mappa_Id;

            Result result_MappaGet = this.Get(execParam_MappaGet);
            if (result_MappaGet.IsError)
            {
                // hiba
                throw new ResultException(result_MappaGet);
            }

            KRT_Mappak krt_Mappak = (KRT_Mappak)result_MappaGet.Record;

            #endregion Dosszié objektum lekérése

            #region Dosszié UPDATE
            krt_Mappak.Updated.SetValueAll(false);
            krt_Mappak.Base.Updated.SetValueAll(false);
            krt_Mappak.Base.Updated.Ver = true;

            // Visszaküldés: átadó és címzett csere - mappa tulaját az eredeti küldőre állítani
            string felelos_Id = status.AtadoUser;

            krt_Mappak.Csoport_Id_Tulaj = felelos_Id;
            krt_Mappak.Updated.Csoport_Id_Tulaj = true;

            ExecParam execParam_mappaUpdate = execParam.Clone();
            execParam_mappaUpdate.Record_Id = mappa_Id;
            // a StoredProcedure szintet hívjuk, hogy ne keletkezzen új kézbesítési tétel
            result = sp.Insert(Constants.Update, execParam_mappaUpdate, krt_Mappak);

            if (result.IsError)
            {
                throw new ResultException(result);
            }
            #endregion Dosszié UPDATE

            // dossziéban lévő objektumok átadásának sztornózása
            result = sp.MappaTartalmak_Visszakuldes(execParam.Clone(), mappa_Id);

            if (result.IsError)
            {
                throw new ResultException(result);
            }

            #region Kezelési feljegyzés
            // Kezelési feljegyzés létrehozása, ha van megjegyzés:
            if (!String.IsNullOrEmpty(visszakuldesIndoka.Trim()))
            {
                Logger.Debug("Visszakuldes indoka:" + visszakuldesIndoka.Trim());
                this.AddKezelesiFeljegyzes(execParam, mappa_Id, visszakuldesIndoka.Trim(), KodTarak.FELADAT_ALTIPUS.Megjegyzes);
            }
            #endregion Kezelési feljegyzés

            #region Eseménynaplózás
            // naplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(execParam, dataContext.Tranz_Id, mappa_Id, Contentum.eUtility.Constants.TableNames.KRT_Mappak, "MappaVisszakuldes").Record;
                if (eventLogRecord != null && !String.IsNullOrEmpty(visszakuldesIndoka))
                {
                    eventLogRecord.Base.Note = visszakuldesIndoka;
                }

                eventLogService.Insert(execParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }


        log.WsEnd(execParam, result);
        return result;
    }
    #endregion Visszaküldés

    /// <summary>
    /// Dosszié státuszát adja vissza
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="id"></param>
    /// <returns>Egy soros tábla.
    /// Allapot (int): 0-Normál; 1-Továbbítás alatt
    /// AtadoUser (uniqueidentifier): Felelős
    /// CimzettUser (uniqueidentifier): Őrző
    /// Típus nvarchar(64): Mappa.Tipus; 01:Fizikai; 02:Virutalis</returns>
    [WebMethod()]
    public Result GetStatus(ExecParam execParam, string id)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired(DataContext.DefaultGetAllWithExtensionIsolationLevel);


            result = sp.GetStatus(execParam, id);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }


    public class DosszieStatus
    {
        public enum AllapotTipus
        {
            Normal, TovabbitasAlatt
        }

        private string _allapot;
        public AllapotTipus Allapot
        {
            get { return _allapot == "0" ? AllapotTipus.Normal : AllapotTipus.TovabbitasAlatt; }
            set { this._allapot = value == AllapotTipus.Normal ? "0" : "1"; }
        }

        public string AtadoUser;

        public string CimzettUser;

        public string Tipus;

        public DosszieStatus(Result result)
        {
            this.GetFromResult(result);
        }

        public void GetFromResult(Result result)
        {
            if (result.IsError || result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count < 1)
            {
                return;
            }

            this._allapot = result.Ds.Tables[0].Rows[0]["Allapot"].ToString();
            this.AtadoUser = result.Ds.Tables[0].Rows[0]["AtadoUser"].ToString();
            this.CimzettUser = result.Ds.Tables[0].Rows[0]["CimzettUser"].ToString();
            this.Tipus = result.Ds.Tables[0].Rows[0]["Tipus"].ToString();
        }
    }

}
