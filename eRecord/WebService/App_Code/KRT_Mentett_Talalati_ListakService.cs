using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_Mentett_Talalati_ListakService : System.Web.Services.WebService
{
    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se. A sz�r�si felt�teleket param�ter tartalmazza (*Search). 
    /// (A GetAll kiterjeszt�se join m�velettel �sszekapcsolt t�bl�kra). 
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_KRT_Mentett_Talalati_ListakSearch">A keres�si felt�teleket tartalmaz� objektum</param>
    /// <returns>Result.Ds DataSet objektum ker�l felt�lt�sre a lek�rdez�s eredm�ny�vel</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Mentett_Talalati_ListakSearch))]
    public Result GetAllWithExtension(ExecParam ExecParam, KRT_Mentett_Talalati_ListakSearch _KRT_Mentett_Talalati_ListakSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllWithExtension(ExecParam, _KRT_Mentett_Talalati_ListakSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// T�bb egym�skoz kapcsolt t�bl�ra vonatkoz� keres�s eredm�nyhalmaz�nak elk�r�se, sz�k�tve a megadott iktat�k�nyvre.
    /// </summary>
    /// <param name="ExecParam">A v�grehajt� felhaszn�l�t �s egy�b �ltal�nos param�tereket tartalmazza</param>
    /// <param name="_KRT_Mentett_Talalati_Listak">Az iktat�k�nyvet tartalmaz� objektum, amire a tal�lati lista sz�k�tve lesz.</param>
    /// <param name="_KRT_Mentett_Talalati_ListakSearch">A t�bl�ra vonatkoz� plusz keres�si felt�teleket tartalmaz� objektum.</param>
    /// <returns></returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_Mentett_Talalati_ListakSearch))]
    public Result GetAllByMentettTalalatiListak(ExecParam ExecParam, KRT_Mentett_Talalati_Listak _KRT_Mentett_Talalati_Listak
        , KRT_Mentett_Talalati_ListakSearch _KRT_Mentett_Talalati_ListakSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAllByMentettTalalatiListak(ExecParam, _KRT_Mentett_Talalati_Listak, _KRT_Mentett_Talalati_ListakSearch);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(ExecParam, result);
        return result;
    }

}