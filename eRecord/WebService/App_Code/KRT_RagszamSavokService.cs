using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using log4net;
using System;
using System.Web.Services;

//[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class KRT_RagszamSavokService : System.Web.Services.WebService
{
    private static readonly ILog log = LogManager.GetLogger(typeof(Logger));

    [WebMethod(false/*, TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_RagszamSavokSearch))]
    public Result GetAllWithExtensions(ExecParam ExecParam, KRT_RagszamSavokSearch _KRT_RagszamSavokSearch)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetAllWithExtension(ExecParam, _KRT_RagszamSavokSearch);

            #region Eseménynaplózás
            if (!Search.IsIdentical(_KRT_RagszamSavokSearch, new KRT_RagszamSavokSearch()))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, null, "KRT_RagszamSavok", "List").Record;

                if (eventLogRecord != null)
                {
                    Query query = new Query();
                    query.BuildFromBusinessDocument(_KRT_RagszamSavokSearch);

                    #region where feltétel összeállítása
                    if (!string.IsNullOrEmpty(_KRT_RagszamSavokSearch.WhereByManual))
                    {
                        if (!string.IsNullOrEmpty(query.Where))
                            query.Where += " and ";
                        query.Where = _KRT_RagszamSavokSearch.WhereByManual;
                    }

                    eventLogRecord.KeresesiFeltetel = query.Where;
                    if (String.IsNullOrEmpty(result.ErrorCode) && result.Ds.Tables.Count > 1)
                    {
                        eventLogRecord.TalalatokSzama = result.Ds.Tables[1].Rows[0]["RecordNumber"].ToString();
                    }


                    #endregion

                    eventLogService.Insert(ExecParam, eventLogRecord);
                }
            }
            #endregion

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_Igenyles(ExecParam ExecParam, Guid RagszamSav_Id, Guid? Postakonyv_Id, int FoglalandoDarab, string @FoglalandoAllapota)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_Igenyles(ExecParam, RagszamSav_Id, Postakonyv_Id, FoglalandoDarab, @FoglalandoAllapota);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result == null || result.Uid == null)
                throw new ResultException(result);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_RagszamSavok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_Igenyles_WithoutInsert(ExecParam ExecParam, Guid RagszamSav_Id, string Kod, float KodNum, Guid? Postakonyv_Id, int FoglalandoDarab, string @FoglalandoAllapota)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_Igenyles_WithoutInsert(ExecParam, RagszamSav_Id, Kod, KodNum, Postakonyv_Id, FoglalandoDarab, @FoglalandoAllapota);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result == null || result.Uid == null)
                throw new ResultException(result);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_RagszamSavok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_IgenylesManual(ExecParam ExecParam, string Ragszam, float RagszamNum, Guid? Postakonyv_Id, string SavTipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_IgenylesManual(ExecParam, Ragszam, RagszamNum, Postakonyv_Id, SavTipus);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result == null || result.Uid == null)
                throw new ResultException(result);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_RagszamSavok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_Listazas(ExecParam ExecParam, String SavId, String SzervezetId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());
        Logger.Debug("SavId: " + SavId);
        Logger.Debug("SzervezetId: " + SzervezetId);

        //string rv = null;
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            //rv = sp.RagszamSav_Listazas(ExecParam, SavId, SzervezetId);
            result = sp.RagszamSav_Listazas(ExecParam, SavId, SzervezetId);
            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
            //rv = result.ErrorMessage;
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_Letrehozas(ExecParam ExecParam, Guid SavId, Guid PostakonyvId, string SavKezdete, string SavVege, float SavKezdeteNum, float SavVegeNum, float IgenyeltDarabszam, string SzervezetId, string SavTipus)
    {
        log4net.Config.XmlConfigurator.Configure();
        log.Debug("SavId: " + SavId);
        log.Debug("SavKezdete: " + SavKezdete);
        log.Debug("SavVege: " + SavVege);
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_Letrehozas(ExecParam, SavId, PostakonyvId, SavKezdete, SavVege, SavKezdeteNum, SavVegeNum, IgenyeltDarabszam, SavTipus);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            ExecParam execParam_barcodeGet = ExecParam.Clone();
            execParam_barcodeGet.Record_Id = result.Uid;

            Result result_barcodeGet = sp.Get(execParam_barcodeGet);
            if (!String.IsNullOrEmpty(result_barcodeGet.ErrorCode))
            {
                throw new ResultException(result_barcodeGet);
            }
            else
            {
                KRT_RagszamSavok krt_RagszamSavok = (KRT_RagszamSavok)result_barcodeGet.Record;
                //result.Record = krt_RagszamSavok.Kod;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, SavId.ToString(), "KRT_RagszamSavok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        log.Debug("Result: " + result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_Modositas(ExecParam ExecParam, Guid SavId, Guid PostakonyvId, string SavKezdete, string SavVege, float SavKezdeteNum, float SavVegeNum, float IgenyeltDarabszam, string SzervezetId, string SavAllapot, string SavTipus)
    {
        log4net.Config.XmlConfigurator.Configure();
        log.Debug("SavId: " + SavId);
        log.Debug("SavKezdete: " + SavKezdete);
        log.Debug("SavVege: " + SavVege);
        log.Debug("SavAllapot: " + SavAllapot);
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_Modositas(ExecParam, SavId, PostakonyvId, SavKezdete, SavVege, SavKezdeteNum, SavVegeNum, IgenyeltDarabszam, SzervezetId, SavAllapot, SavTipus);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }
            ExecParam execParam_barcodeGet = ExecParam.Clone();
            execParam_barcodeGet.Record_Id = result.Uid;

            Result result_rszsGet = sp.Get(execParam_barcodeGet);
            if (!String.IsNullOrEmpty(result_rszsGet.ErrorCode))
            {
                throw new ResultException(result_rszsGet);
            }
            else
            {
                KRT_RagszamSavok krt_RagszamSavok = (KRT_RagszamSavok)result_rszsGet.Record;
                //result.Record = krt_RagszamSavok.Kod;
            }

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, SavId.ToString(), "KRT_RagszamSavok", "Modify").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        log.Debug("Result: " + result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_Foglalas(ExecParam ExecParam, Guid RagszamSav_Id, Guid? Postakonyv_Id, int FoglalandoDarab, string @FoglalandoAllapota)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_Foglalas(ExecParam, RagszamSav_Id, Postakonyv_Id, FoglalandoDarab, @FoglalandoAllapota);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result == null || result.Uid == null)
                throw new ResultException(result);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_RagszamSavok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod()]
    public Result RagszamSav_FoglalasManual(ExecParam ExecParam, string Ragszam, float RagszamNum, Guid? Postakonyv_Id, string SavTipus)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.RagszamSav_FoglalasManual(ExecParam, Ragszam, RagszamNum, Postakonyv_Id, SavTipus);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            if (result == null || result.Uid == null)
                throw new ResultException(result);

            #region Eseménynaplózás
            if (isTransactionBeginHere)
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByMuveletKod(ExecParam, dataContext.Tranz_Id, result.Uid, "KRT_RagszamSavok", "New").Record;

                Result eventLogResult = eventLogService.Insert(ExecParam, eventLogRecord);
            }
            #endregion


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        //log.WsEnd(ExecParam, result);
        return result;
    }

}
