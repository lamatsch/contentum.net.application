using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System.Configuration;
using System.Data;
using System.Collections.Generic;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class PirEdokInterfaceService : System.Web.Services.WebService
{

    // Org k�dok eredm�ny�nek cache-el�s�hez
    private static String cache_key_OrgKodDictionary = "OrgKodDictionary";

    private const string successCode = "1";
    private const string errorCode_MissingBusinessObject = "[100021]";
    private const string errorCode_MissingParam = "[100022]";
    private const string errorCode_MissingResultTable = "[100023]";
    private const string errorCode_IllegalResultTableFormat = "[100024]";
    private const string errorCode_ResultTableHasNoRow = "[100025]";
    private const string errorCode_PirEdokInterfaceError = "[100000]";
    private const string errorMessage_MissingBusinessObject = "Nincs megadva az �zleti objektum!";
    private const string errorMessage_MissingParam = "Hi�nyz� param�ter '{0}'!";
    private const string errorMessage_MissingResultTable = "Hi�nyz� eredm�nyt�bla!";
    private const string errorMessage_IllegalResultTableFormat = "Az eredm�nyt�bla form�tuma nem megfelel�!";
    private const string errorMessage_ResultTableHasNoRow = "Az eredm�nyt�bla nem tartalmaz sorokat!";

    private PirEdokInterfaceStoredProcedure sp = null;
    
    private DataContext dataContext;

    public PirEdokInterfaceService()
    {
        dataContext = new DataContext(this.PirEdokConnectionString); //new DataContext(this.Application);
        
        sp = new PirEdokInterfaceStoredProcedure(dataContext);
    }

    public PirEdokInterfaceService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new PirEdokInterfaceStoredProcedure(dataContext);
    }

    private string PirEdokConnectionString
    {
        get
        {
            ConnectionStringSettings connStringSettings = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Contentum.PirEdokConnectionString"];

            if (connStringSettings != null)
            {
                string connectionString = connStringSettings.ConnectionString; //"Data Source=srvvirtual;Initial Catalog=msdb;Integrated Security=SSPI;";

                return connectionString;
            }

            return String.Empty;
        }
    }

    private static string[] ObligateParameters = new string[] {
        "Vonalkod",
        "Szamlaszam",
        "ErkeztetesDatuma",
        "SzervezetiEgysegKod",
        "SzervezetiEgysegNev",
        "FizetesiMod",
        //"TeljesitesDatuma",
        //"BizonylatDatuma",
        //"FizetesiHatarido",
        "EllenorzoOsszeg",
        "EDOKPartnerKod",
        "EDOKPartnerNev",
        "EDOKPartnerIranyitoszam",
        "EDOKPartnerHelyseg",
        "EDOKPartnerCim"
   };

    public static bool IsObligateParameter(string name)
    {
        if (Array.Exists(ObligateParameters, delegate(string item) { return item == name; }))
        {
            return true;
        }

        return false;
    }

    /*
    f_vonalkod	char(13) 	Vonalk�d			Kit�lt�se k�telez�
    f_kbsz		char(16) 	Sz�mlasz�m			Kit�lt�se k�telez�
    f_erkdat		smalldatetime �rkeztet�s d�tuma		Kit�lt�se k�telez�
    f_fldhely		char(36)	Szerv. egys�g EDOK k�d	Kit�lt�se k�telez�
    f_fldhelynev	char(100)	Szerv. egys�g EDOK neve	Kit�lt�se k�telez�
    f_fizmod		char(4)		Fizet�si m�d			Kit�lt�se k�telez� (�K)
    f_tdat		smalldatetime Teljes�t�s d�tuma		Kit�lt�se opcion�lis
    f_bizdat		smalldatetime	Bizonylat d�tuma		Kit�lt�se opcion�lis
    f_fhi		smalldatetime	Fizet�si hat�rid�		Kit�lt�se opcion�lis
    f_dkod		char(3)		Deviza k�d			Kit. opc., alap�rt.: �HUF�
    f_vidat		smalldatetime	Visszak�ld�s d�tuma		Kit�lt�se opcion�lis
    f_csumma		float		Ellen�rz� �sszeg		Kit�lt�se k�telez�
    f_szrpartn		char(36)	EDOK partnerk�d		Kit�lt�se k�telez�
    f_nev1		varchar(400)	EDOK partner neve		Kit�lt�se k�telez�
    f_irsz		char(10)	EDOK part.ir�ny�t�sz�ma	Kit�lt�se k�telez�
    f_helyseg		char(20)	EDOK part. helys�ge		Kit�lt�se k�telez�
    f_hcim		char(100)	EDOK partner c�me		Kit�lt�se opcion�lis
    f_aiasz		char(15)	Belf�ldi ad�sz�m		Kit�lt�se opcion�lis
    f_kaiasz		char(15)	K�z�ss�gi (EU) ad�sz�m	Kit�lt�se opcion�lis
    f_adoazon		char(10)	Mag�nszem�ly ad�azon. jele	Kit�lt�se opcion�lis
    f_telex		char(15)	Helyrajzi sz�m		Kit�lt�se opcion�lis
    f_pbsz1		char(8)		Banksz�mlasz�m1		Kit�lt�se opcion�lis
    f_pbsz2		char(8)		Banksz�mlasz�m2		Kit�lt�se opcion�lis
    f_pbsz3		char(8)		Banksz�mlasz�m3		Kit�lt�se opcion�lis
    f_tel1		char(15)	Telefonsz�m1			Kit�lt�se opcion�lis
    f_fax		char(15)	Fax				Kit�lt�se opcion�lis
    f_email		varchar(60)	E-mail c�m			Kit�lt�se opcion�lis
    f_fstatus		char(1)	PIR-beli �llapot			Kit�lt�se opcion�lis
    f_mgj		varchar(40)	Megjegyz�s			Kit�lt�se opcion�lis
    */
    private string CheckObligateParametersFromBusinessObject(PirEdokSzamla pirEdokSzamla)
    {
        DataTable table = new DataTable();
        table.Columns.Add().DefaultValue = errorCode_MissingParam;
        table.Columns.Add();

        if (pirEdokSzamla != null)
        {
            foreach (System.Reflection.PropertyInfo pi in pirEdokSzamla.GetType().GetProperties())
            {
                if (IsObligateParameter(pi.Name) && String.IsNullOrEmpty((string)pi.GetValue(pirEdokSzamla, null)))
                {
                        DataRow row = table.NewRow();
                        row[1] = String.Format(errorMessage_MissingParam, pi.Name);
                        table.Rows.Add(row);
                }
            }
        }
        else
        {
            DataRow row = table.NewRow();
            row[0] = errorCode_MissingBusinessObject;
            row[1] = errorMessage_MissingBusinessObject;
            table.Rows.Add(row);
        }

        if (table.Rows.Count > 0)
        {
            return GetErrorTextFromResultTable(table);
        }
        return String.Empty;
    }

    private bool SetResultRecord(Result result)
    {
        if (result.IsError)
        {
            return false; 
        }

        if (result.Ds.Tables.Count == 0)
        {
            result.ErrorCode = errorCode_MissingResultTable;
            result.ErrorMessage = errorCode_MissingResultTable;//errorMessage_MissingResultTable;
            return false;
        }

        if (result.Ds.Tables[0].Columns.Count != 2)
        {
            result.ErrorCode = errorCode_IllegalResultTableFormat;
            result.ErrorMessage = errorCode_IllegalResultTableFormat;//errorMessage_IllegalResultTableFormat;
            return false;
        }

        if (result.Ds.Tables[0].Rows.Count == 0)
        {
            result.ErrorCode = errorCode_ResultTableHasNoRow;
            result.ErrorMessage = errorCode_ResultTableHasNoRow; //errorMessage_ResultTableHasNoRow;
            return false;
        }

        if (result.Ds.Tables[0].Rows.Count == 1
            && result.Ds.Tables[0].Rows[0][0].ToString().Trim().Equals(successCode))
        {
            result.Record = result.Ds.Tables[0].Rows[0][1];
            return true;
        }

        result.ErrorCode = errorCode_PirEdokInterfaceError;
        result.ErrorMessage = GetErrorTextFromResultTable(result.Ds.Tables[0]);
        return false;
        
    }

    private string GetErrorTextFromResultTable(DataTable resultTable)
    {
        System.Collections.Generic.List<string> lstResultErrors = new System.Collections.Generic.List<string>();

        foreach (DataRow row in resultTable.Rows)
        {
            if (!row[0].ToString().Trim().Equals(successCode))
            {
                lstResultErrors.Add(row[1].ToString().Trim());
            }
        }

        return GetErrorTextFromList(lstResultErrors);
    }

    private string GetErrorTextFromList(System.Collections.Generic.List<string> errorList)
    {
        if (errorList != null && errorList.Count > 0)
        {
            return String.Join(";" + System.Environment.NewLine, errorList.ToArray());
        }

        return String.Empty;
    }

    private Result GetOrgKod(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            if (Context.Cache[cache_key_OrgKodDictionary] == null)
            {
                Dictionary<string, string> OrgKodDictionary = new Dictionary<string, string>();

                #region OrgGet

                Contentum.eAdmin.Service.KRT_OrgokService service_org = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_OrgokService();
                ExecParam execParam_orgGet = ExecParam.Clone();
                execParam_orgGet.Record_Id = ExecParam.Org_Id;

                Result result_orgGet = service_org.Get(execParam_orgGet);

                if (result_orgGet.IsError)
                {
                    throw new ResultException(result_orgGet);
                }

                KRT_Orgok krt_Orgok = (KRT_Orgok)result_orgGet.Record;

                #endregion OrgGet

                OrgKodDictionary.Add(ExecParam.Org_Id, krt_Orgok.Kod);

                // Dictionary felv�tele a Cache-be:
                Context.Cache.Insert(cache_key_OrgKodDictionary, OrgKodDictionary);
            }            
            
            if (Context.Cache[cache_key_OrgKodDictionary] != null)
            {
                Dictionary<string, string> OrgKodDictionary = Context.Cache[cache_key_OrgKodDictionary] as Dictionary<string, string>;
                if (OrgKodDictionary != null && OrgKodDictionary.ContainsKey(ExecParam.Org_Id))
                {
                    result.Record = OrgKodDictionary[ExecParam.Org_Id];
                }
            }

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result PirEdokSzamlaFejFelvitel(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            #region Check Obligate Parameters
            string strCheckObligateParameters = CheckObligateParametersFromBusinessObject(pirEdokSzamla);
            if (!String.IsNullOrEmpty(strCheckObligateParameters))
            {
                throw new ResultException(errorCode_PirEdokInterfaceError, strCheckObligateParameters);
            }
            #endregion Check Obligate Parameters

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.PirEdokSzamlaFejFelvitel(ExecParam, pirEdokSzamla);

            #region Check Result for Errors
            //if (!result.IsError)
            //{
            //    string strResult = GetErrorTextFromResultTable(result.Ds.Tables[0]);
            //    if (!String.IsNullOrEmpty(strResult))
            //    {
            //        throw new ResultException(strResult);
            //    }
            //}
            if (!SetResultRecord(result))
            {
                throw new ResultException(result);
            }
            #endregion Check Result for Errors
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result PirEdokSzamlaFejModositas(ExecParam ExecParam, PirEdokSzamla pirEdokSzamla)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            #region Check Obligate Parameters
            string strCheckObligateParameters = CheckObligateParametersFromBusinessObject(pirEdokSzamla);
            if (!String.IsNullOrEmpty(strCheckObligateParameters))
            {
                throw new ResultException(errorCode_PirEdokInterfaceError, strCheckObligateParameters);
            }
            #endregion Check Obligate Parameters

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.PirEdokSzamlaFejModositas(ExecParam, pirEdokSzamla);

            #region Check Result for Errors
            //if (!result.IsError)
            //{
            //    string strResult = GetErrorTextFromResultTable(result.Ds.Tables[0]);
            //    if (!String.IsNullOrEmpty(strResult))
            //    {
            //        throw new ResultException(strResult);
            //    }
            //}
            if (!SetResultRecord(result))
            {
                throw new ResultException(result);
            }
            #endregion Check Result for Errors
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result PirEdokSzamlaFejRontas(ExecParam ExecParam, string vonalkod, string rontasOka)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            #region Check Obligate Parameters
            System.Collections.Generic.List<string> errorList = new System.Collections.Generic.List<string>();
            if (String.IsNullOrEmpty(vonalkod))
            {
                errorList.Add(String.Format(errorMessage_MissingParam, "Vonalkod"));
            }

            if (String.IsNullOrEmpty(rontasOka))
            {
                errorList.Add(String.Format(errorMessage_MissingParam, "RontasOka"));
            }

            if (errorList.Count > 0)
            {
               throw new ResultException(errorCode_PirEdokInterfaceError, GetErrorTextFromList(errorList));
            }
            #endregion Check Obligate Parameters

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.PirEdokSzamlaFejRontas(ExecParam, vonalkod, rontasOka);

            #region Check Result for Errors
            //if (!result.IsError)
            //{
            //    string strResult = GetErrorTextFromResultTable(result.Ds.Tables[0]);
            //    if (!String.IsNullOrEmpty(strResult))
            //    {
            //        throw new ResultException(strResult);
            //    }
            //}
            if (!SetResultRecord(result))
            {
                throw new ResultException(result);
            }
            #endregion Check Result for Errors
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result PirEdokSzamlaFejAllapotLekerdezes(ExecParam ExecParam, string vonalkod)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            #region Check Obligate Parameters
            System.Collections.Generic.List<string> errorList = new System.Collections.Generic.List<string>();
            if (String.IsNullOrEmpty(vonalkod))
            {
                errorList.Add(String.Format(errorMessage_MissingParam, "Vonalkod"));
            }

            if (errorList.Count > 0)
            {
                throw new ResultException(errorCode_PirEdokInterfaceError, GetErrorTextFromList(errorList));
            }
            #endregion Check Obligate Parameters

            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.PirEdokSzamlaFejAllapotLekerdez(ExecParam, vonalkod);

            #region Check Result for Errors
            //if (!result.IsError)
            //{
            //    string strResult = GetErrorTextFromResultTable(result.Ds.Tables[0]);
            //    if (!String.IsNullOrEmpty(strResult))
            //    {
            //        throw new ResultException(strResult);
            //    }
            //}
            if (!SetResultRecord(result))
            {
                throw new ResultException(result);
            }
            #endregion Check Result for Errors
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    public enum MuveletType { Modositas, Sztorno }

    // M�dosithat�s�g ellen�rz�se:
    // Sz�ll�t�sz�mla EDOK fogad�si �llapotai:
    // F - Fogadott: EDOK-b�l �temel�sre ker�lt, de r�szletes ellen�rz�sre, kieg�sz�t�sre m�g nem ker�lt a PIR �ltal (term�szetesen a minim�lis ellen�rz�sen t�l van, ami a fogad�s felt�tele). M�g m�dos�that� EDOK-b�l.
    // E - Ellen�rz�tt: A PIR �ltal ellen�rz�tt, illetve PIR felhaszn�l� �ltal a hi�nyz� adatokkal kieg�sz�t�sre ker�lt a sz�mla fej adata. M�r nem m�dos�that� EDOK-b�l.
    // S - Sztorn�zott: EDOK fel�leten sztorn�zott. Nem m�dos�that�.
    // R - Rontott: PIR k�ztes fel�leten rontott� nyilv�n�tott. Nem m�dos�that� EDOK-b�l sem.
    // V - V�gleges�tett: Tov�bb�tva lett a PIR sz�ll�t�sz�mla nyilv�ntart�s�ba, nem m�dos�that� EDOK-b�l, csak PIR fel�letr�l.
    // U - Elutas�tott: Hi�nyos adatok (pl. bels� c�mzett) miatt visszak�ldve a sz�ll�t�nak, nem m�dos�that� sem EDOK-b�l, sem PIR fel�letr�l.
    // A Record tartalmazza a visszakapott �llapotot (nem m�dos�that� �llapot eset�n is), ha az �llapotlek�r�s maga sikeres volt.
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result CheckPirEdokSzamlaFejAllapot(ExecParam ExecParam, string vonalkod, MuveletType Muvelet)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            Result result_pirallapot = this.PirEdokSzamlaFejAllapotLekerdezes(ExecParam, vonalkod);

            if (result_pirallapot.IsError)
            {
                throw new ResultException(result_pirallapot);
            }

            string pirallapot = result_pirallapot.Record.ToString().Trim();

            result.Record = pirallapot;

            Contentum.eRecord.BaseUtility.PirEdokSzamlak.Statusz statusz = new Contentum.eRecord.BaseUtility.PirEdokSzamlak.Statusz(null, vonalkod, pirallapot);

            ErrorDetails errorDetail;
            switch (Muvelet)
            {
                case MuveletType.Modositas:
                    if (!Contentum.eRecord.BaseUtility.PirEdokSzamlak.Modosithato(statusz, ExecParam, out errorDetail))
                    {
                        throw new ResultException(100000, errorDetail);
                    }
                    break;
                case MuveletType.Sztorno:
                    if (!Contentum.eRecord.BaseUtility.PirEdokSzamlak.Sztornozhato(statusz, ExecParam, out errorDetail))
                    {
                        throw new ResultException(100000, errorDetail);
                    }
                    break;
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    /// <summary>
    /// a sz�ll�t�i sz�ml�k �rkeztet� t�bl�j�ba felvitt t�telek �llapot�nak lek�rdez�se.
    /// - Ha ki van t�ltve a d�tum, akkor az adott d�tum �ta t�rt�nt �llapotv�ltoz�sokat adja vissza, f�ggetlen�l att�l, hogy azok az �llapotok le lettek-e m�r k�rdezve.
    /// - Ha nincs kit�ltve a d�tum, akkor az eddig m�g nem lek�rdezett �llapot v�ltoz�sokat adja vissza a program
    /// </summary>
    /// <param name="execParam"></param>
    /// <returns>nx2-es t�bl�zat: Vonalk�d	| �llapot</returns>
    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    public Result PirEdokSzamlaFejAllapotValtozas(ExecParam ExecParam, DateTime datum)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.PirEdokSzamlaFejAllapotValtozas(ExecParam, datum);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }
}
