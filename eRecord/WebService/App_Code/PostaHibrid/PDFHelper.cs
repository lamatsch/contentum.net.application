﻿using Contentum.eBusinessDocuments;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PDFHelper
/// </summary>
public class PDFHelper
{
    public static List<Csatolmany> GetAttachments(byte[] fileContent)
    {
        List<Csatolmany> attachments = new List<Csatolmany>();

        PdfDictionary documentNames = null;
        PdfDictionary embeddedFiles = null;
        PdfDictionary fileArray = null;
        PdfDictionary file = null;
        PRStream stream = null;

        PdfReader reader = new PdfReader(fileContent);
        PdfDictionary catalog = reader.Catalog;

        documentNames = (PdfDictionary)PdfReader.GetPdfObject(catalog.Get(PdfName.NAMES));

        if (documentNames != null)
        {
            embeddedFiles = (PdfDictionary)PdfReader.GetPdfObject(documentNames.Get(PdfName.EMBEDDEDFILES));
            if (embeddedFiles != null)
            {
                PdfArray filespecs = embeddedFiles.GetAsArray(PdfName.NAMES);

                for (int i = 0; i < filespecs.Size; i++)
                {
                    i++;
                    fileArray = filespecs.GetAsDict(i);
                    file = fileArray.GetAsDict(PdfName.EF);

                    foreach (PdfName key in file.Keys)
                    {
                        stream = (PRStream)PdfReader.GetPdfObject(file.GetAsIndirectObject(key));
                        string attachedFileName = fileArray.GetAsString(key).ToString();
                        byte[] attachedFileBytes = PdfReader.GetStreamBytes(stream);

                        attachments.Add(
                            new Csatolmany
                            {
                                Nev = attachedFileName,
                                Tartalom = attachedFileBytes
                            }
                       );
                    }

                }
            }
        }

        return attachments;
    }

    public static Csatolmany GetAttachment(byte[] fileContent, string attachmentName)
    {
        List<Csatolmany> attachments = GetAttachments(fileContent);
        return attachments.Where(cs => cs.Nev == attachmentName).FirstOrDefault();
    }
}