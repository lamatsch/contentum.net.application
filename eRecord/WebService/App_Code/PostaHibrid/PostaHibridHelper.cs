﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eBusinessDocuments.PH;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PostaHibridHelper
/// </summary>
public class PostaHibridHelper
{
    public static string GetPostaKRID(ExecParam execParam)
    {
        Contentum.eIntegrator.Service.INT_ParameterekService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
        Result result = svc.GetByModulNev(execParam, "PostaHibrid");

        if (result.IsError)
        {
            throw new ResultException(result);
        }

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string nev = row["Nev"].ToString();
            string ertek = row["Ertek"].ToString();

            if ("Posta_KRID".Equals(nev))
            {
                return ertek;

            }
        }

        return String.Empty;
    }

    public static void SetPostaCimzett(ExecParam execParam, EREC_eBeadvanyok _EREC_eBeadvanyok)
    {
        Contentum.eIntegrator.Service.INT_ParameterekService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.GetINT_ParameterekService();
        Result result = svc.GetByModulNev(execParam, "PostaHibrid");

        if (result.IsError)
        {
            throw new ResultException(result);
        }

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string nev = row["Nev"].ToString();
            string ertek = row["Ertek"].ToString();

            if ("Posta_KRID".Equals(nev))
            {
                _EREC_eBeadvanyok.PartnerKRID = ertek;
                _EREC_eBeadvanyok.Updated.PartnerKRID = true;
            }
            else if ("Posta_PartnerNev".Equals(nev))
            {
                _EREC_eBeadvanyok.PartnerNev = ertek;
                _EREC_eBeadvanyok.Updated.PartnerNev = true;
            }
            else if ("Posta_RovidNev".Equals(nev))
            {
                _EREC_eBeadvanyok.PartnerRovidNev = ertek;
                _EREC_eBeadvanyok.Updated.PartnerRovidNev = true;
            }
        }
    }

	public static Csatolmany GetDeliveryInstructionsFile(deliveryInstructions deliveryInstructions)
    {
        Csatolmany metaFile = new Csatolmany();
        metaFile.Nev = "DeliveryInstruction.xml";
        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(deliveryInstructions));
        using (MemoryStream memoryStream = new MemoryStream())
        {

            using (StreamWriter writer = new StreamWriter(memoryStream))
            {
                serializer.Serialize(writer, deliveryInstructions);
                metaFile.Tartalom = memoryStream.ToArray();
            }
        }
        return metaFile;
    }

    public static void ComputeCsatolmanyokHash(deliveryInstructions deliveryInstructions, List<Csatolmany> csatolmanyok)
    {
        List<byte[]> recordHashList = new List<byte[]>();

        foreach (deliveryInstructionsConsignmentAttachment attachment in deliveryInstructions.consignment.attachmentList)
        {
            Csatolmany csatolmany = csatolmanyok.Where(cs => cs.Nev == attachment.FileName).FirstOrDefault();

            if (csatolmany != null)
            {
                byte[] recordHash = ComputeSHA256(csatolmany.Tartalom);
                attachment.RecordHash = Convert.ToBase64String(recordHash);
                attachment.RecordAlgorithm = "SHA256";
                recordHashList.Add(recordHash);
            }
        }

        if (recordHashList.Count > 0)
        {
            byte[] consignmentHash = ConcatArrays(recordHashList);
            consignmentHash = ComputeSHA256(consignmentHash);
            deliveryInstructions.consignment.ConsignmentHash = Convert.ToBase64String(consignmentHash);
        }

    }

    static byte[] ComputeSHA256(byte[] content)
    {
        SHA256 hash = new SHA256CryptoServiceProvider();
        return hash.ComputeHash(content);
    }

    static byte[] ConcatArrays(List<byte[]> arrays)
    {
        return arrays.SelectMany(x => x).ToArray();
    }

    public static void SetKiadmanyozo(ExecParam execParam, DataContext dataContext, string kuldemenyId, deliveryInstructions deliveryInstructions)
    {
        EREC_IraIratokService iratokService = new EREC_IraIratokService(dataContext);

        EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
        iratSearch.Id.Value = String.Format("select p.IraIrat_Id from EREC_Kuldemeny_IratPeldanyai kp join EREC_PldIratPeldanyok p on kp.Peldany_Id = p.Id where kp.KuldKuldemeny_Id='{0}' and getdate() between kp.ErvKezd and kp.ErvVege", kuldemenyId);
        iratSearch.Id.Operator = Query.Operators.inner;

        Result iratResult = iratokService.GetAll(execParam, iratSearch);

        if (iratResult.IsError)
            throw new ResultException(iratResult);

        DataTable table = iratResult.Ds.Tables[0];

        bool isIssued = false;
        string sigRentPersName = "";

        if (table.Rows.Count > 0)
        {
            DataRow row = table.Rows[0];
            string allapot = row["Allapot"].ToString();
            string felhasznaloCsoport_Id_Kiadmany = row["FelhasznaloCsoport_Id_Kiadmany"].ToString();

            if (allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            {
                isIssued = true;

                KRT_FelhasznalokService felhasznalokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                ExecParam felhasznalokExecParam = execParam.Clone();
                felhasznalokExecParam.Record_Id = felhasznaloCsoport_Id_Kiadmany;

                Result felhasznaloResult = felhasznalokService.Get(felhasznalokExecParam);

                if (!felhasznaloResult.IsError && felhasznaloResult.Record != null)
                {
                    sigRentPersName = (felhasznaloResult.Record as KRT_Felhasznalok).Nev;
                }
            }
        }

        foreach (deliveryInstructionsConsignmentAttachment attachment in deliveryInstructions.consignment.attachmentList)
        {
            attachment.IsIssued = GetPHBoolean(isIssued);
            attachment.SigRentPersName = sigRentPersName;
        }
    }

    static string GetPHBoolean(bool value)
    {
        return value ? "Y" : "N";
    }

    public static PostaHibrid.DispatchNonDispatchCertificate.evidence GetDispatchNonDispatchCertificateFromPDF(Csatolmany dispatchNonDispatchCertificatePDF)
    {
        try
        {
            Csatolmany dispatchNonDispatchCertificateXML = PDFHelper.GetAttachment(dispatchNonDispatchCertificatePDF.Tartalom, "DispatchNonDispatchCertificate.xml");

            if (dispatchNonDispatchCertificateXML != null)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(PostaHibrid.DispatchNonDispatchCertificate.evidence));
                using (MemoryStream ms = new MemoryStream(dispatchNonDispatchCertificateXML.Tartalom))
                {
                    return serializer.Deserialize(ms) as PostaHibrid.DispatchNonDispatchCertificate.evidence;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("GetDispatchNonDispatchCertificateFromPDF", ex);
        }

        return null;
    }

    public static PostaHibrid.HibridReceiptNonReceiptCertificate.evidence GetHibridReceiptNonReceiptCertificateFromPDF(Csatolmany hibridReceiptNonReceiptCertificatePDF)
    {
        try
        {
            Csatolmany hibridReceiptNonReceiptCertificateXML = PDFHelper.GetAttachment(hibridReceiptNonReceiptCertificatePDF.Tartalom, "HibridReceiptNonReceiptCertificate.xml");

            if (hibridReceiptNonReceiptCertificateXML != null)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(PostaHibrid.HibridReceiptNonReceiptCertificate.evidence));
                using (MemoryStream ms = new MemoryStream(hibridReceiptNonReceiptCertificateXML.Tartalom))
                {
                    return serializer.Deserialize(ms) as PostaHibrid.HibridReceiptNonReceiptCertificate.evidence;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("GetHibridReceiptNonReceiptCertificateFromPDF", ex);
        }

        return null;
    }

}