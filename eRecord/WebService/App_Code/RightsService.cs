using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
//using System.EnterpriseServices;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;


/// <summary>
/// Summary description for JogosultsagService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class RightsService : System.Web.Services.WebService
{
    private RightsServiceStoredProcedure sp = null;

    private DataContext dataContext;

    public RightsService()
    {
        dataContext = new DataContext(this.Application);

        //sp = new RightsServiceStoredProcedure(this.Application);
        sp = new RightsServiceStoredProcedure(dataContext);

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    public RightsService(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
        sp = new RightsServiceStoredProcedure(dataContext);
    }

    [WebMethod]
    public Result AddCsoportToJogtargy(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi)
    {

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargy(execParam, JogtargyId, CsoportId, Kezi, 'I', '0');

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod]
    public Result AddCsoportToJogtargyWithJogszint(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi,
        char Jogszint,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddCsoportToJogtargy(execParam, JogtargyId, CsoportId, Kezi, Jogszint, Tipus);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod]
    public Result AddScopeIdToJogtargy(ExecParam execParam,
        string JogtargyId,
        string JogtargyTablaNev,
        string JogtargySzuloId,
        char CsoportOroklesMod,
        string CsoportId)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddScopeIdToJogtargy(execParam, JogtargyId, JogtargyTablaNev, JogtargySzuloId, CsoportOroklesMod, CsoportId, 'I');


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result AddScopeIdToJogtargyWithJogszint(ExecParam execParam,
        string JogtargyId,
        string JogtargyTablaNev,
        string JogtargySzuloId,
        char CsoportOroklesMod,
        string CsoportId, char OroklottJogszint)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.AddScopeIdToJogtargy(execParam, JogtargyId, JogtargyTablaNev, JogtargySzuloId, CsoportOroklesMod, CsoportId, OroklottJogszint);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    //[WebMethod]
    //public Result ChangeJogtargyRightInherit(ExecParam execParam,
    //    string JogtargyId,
    //    string SzuloJogtargyId,
    //    char UjOroklesTipus,
    //    char OroklesMasolas)
    //{
    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;
    //    bool isTransactionBeginHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
    //        isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


    //        result = sp.ChangeJogtargyRightInherit(execParam, JogtargyId, SzuloJogtargyId, UjOroklesTipus, OroklesMasolas);


    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            throw new ResultException(result);
    //        }

    //        // COMMIT
    //        dataContext.CommitIfRequired(isTransactionBeginHere);
    //    }
    //    catch (Exception e)
    //    {
    //        dataContext.RollbackIfRequired(isTransactionBeginHere);
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }

    //    return result;
    //}

    [WebMethod]
    public Result GetRightsByJogtargyIdAndCsoportId(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Jogszint)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetRightsByJogtargyIdAndCsoportId(execParam, JogtargyId, CsoportId, Jogszint);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod]
    public Result GetRightsByJogtargyIdAndCsoportIdAndTipus(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Jogszint,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetRightsByJogtargyIdAndCsoportId(execParam, JogtargyId, CsoportId, Jogszint,Tipus);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result RemoveCsoportFromJogtargy(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.RemoveCsoportFromJogtargy(execParam, JogtargyId, CsoportId, Kezi, '0');


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result RemoveCsoportFromJogtargyByTipus(ExecParam execParam,
        string JogtargyId,
        string CsoportId,
        char Kezi,
        char Tipus)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.RemoveCsoportFromJogtargy(execParam, JogtargyId, CsoportId, Kezi, Tipus);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result GetAllRightedCsoportByJogtargy(ExecParam exeParam, string JogtargyId, RowRightsCsoportSearch where)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllRightedCsoportByJogtargy(exeParam, JogtargyId, where);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod]
    public Result RemoveCsoportFromJogtargyById(ExecParam execParam)
    {
        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            result = sp.RemoveCsoportFromJogtargyById(execParam);


            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                throw new ResultException(result);
            }

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }


    [WebMethod()]
    public Result MultiRemoveCsoportFromJogtargyById(ExecParam[] execParams)
    {
        //Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParams, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();


            for (int i = 0; i < execParams.Length; i++)
            {
                result = RemoveCsoportFromJogtargyById(execParams[i]);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    //RollBack:
                    //ContextUtil.SetAbort();
                    ////log.WsEnd(ExecParams, result);
                    //return result;

                    throw new ResultException(result);
                }
            }


            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;

    }

    public Result IsUserMember(ExecParam execParam, string csoportId)
    {
        Result result;
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            result = sp.IsUserMember(execParam,csoportId);

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(KRT_CsoportokSearch))]
    public Result GetAllRightedSubCsoportByJogtargy(ExecParam exeParam, string JogtargyId, KRT_CsoportokSearch search)
    {
        Logger.DebugStart();
        Logger.Debug("GetAllRightedSubCsoportByJogtargy webservice elindul");
        Logger.Debug(String.Format("Paraméterek: felhasználó id: {0}, jogtárgy id: {1}",exeParam.Felhasznalo_Id,JogtargyId));
        
        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();


            result = sp.GetAllRightedSubCsoportByJogtargy(exeParam, JogtargyId, search);


        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
            Logger.Error(String.Format("GetAllRightedSubCsoportByJogtargy webservice hiba: {0},{1}",result.ErrorCode,result.ErrorMessage));
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        Logger.Debug("GetAllRightedSubCsoportByJogtargy webservice vége");
        Logger.DebugEnd();
        return result;
    }
}

