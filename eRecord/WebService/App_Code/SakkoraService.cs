﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Xml.Serialization.XmlInclude(typeof(ErkeztetesIktatasResult))]
public partial class SakkoraService : System.Web.Services.WebService
{
    #region VARIABLES

    private SakkoraStoredProcedure sp = null;
    private DataContext dataContext;

    #endregion VARIABLES

    #region CONSTRUCTORS

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="_dataContext"></param>
    public SakkoraService(DataContext _dataContext)
    {
        dataContext = _dataContext;
        sp = new SakkoraStoredProcedure(dataContext);
    }

    /// <summary>
    /// Constructor
    /// </summary>
    public SakkoraService()
    {
        dataContext = new DataContext(this.Application);
        sp = new SakkoraStoredProcedure(dataContext);
    }

    #endregion CONSTRUCTORS

    #region INTERFACES

    /// <summary>
    /// Ellenõrzi, hogy a sakkóra státusz váltás kivitelezhetõ-e?
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="sakkoraKod"></param>
    /// <returns></returns>
    [WebMethod()]
    [Obsolete]
    public Result CanSetSakkoraStatus(ExecParam execParam, string ugyiratId, string sakkoraKod)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Debug("CanSetSakkoraStatus Start", execParam);
        Result result = new Result();
        result.Record = false;

        if (string.IsNullOrEmpty(ugyiratId))
        {
            throw new Exception("Hibás paraméter: ugyiratId");
        }

        if (string.IsNullOrEmpty(sakkoraKod))
        {
            throw new Exception("Hibás paraméter: sakkoraKod");
        }

        #region SAKKORA KOD ATFORGAT

        if (sakkoraKod.Equals("0"))
        {
            sakkoraKod = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
        }

        #endregion SAKKORA KOD ATFORGAT

        KodTar_Cache.KodTarElem kodtar = GetKodtarakByKodCsoportList(execParam, sakkoraKod);

        List<Sakkora.EnumIratHatasaUgyintezesre> fromStates = null;
        Sakkora.EnumIratHatasaUgyintezesre toStates = new Sakkora.EnumIratHatasaUgyintezesre();
        Sakkora.EnumIratHatasaUgyintezesre toState = Sakkora.ExtractIratHatasStates(kodtar.Egyeb, ref fromStates, ref toStates);

        EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, ugyiratId);

        //Ugyirat  llapot ügyintézés alatt és felfüggeszt típusú - > felfüggeszt
        if (toState == Sakkora.EnumIratHatasaUgyintezesre.PAUSE && Sakkora.IsUgyiratUgyintezesAlatt(ugyirat))
        {
            result.Record = true;
        }
        //Ügyirat felfüggesztett vagy lezárt, típus indító -> indít
        else if (toState == Sakkora.EnumIratHatasaUgyintezesre.START && (Sakkora.IsUgyiratFelfuggesztett(ugyirat) || Sakkora.IsUgyiratLezart(ugyirat)))
        {
            result.Record = true;
        }
        //Ügyirat felfüggesztett vagy lezárt, típus indító -> indít
        else if (toState == Sakkora.EnumIratHatasaUgyintezesre.STOP && (Sakkora.IsUgyiratFelfuggesztett(ugyirat) || Sakkora.IsUgyiratUgyintezesAlatt(ugyirat)))
        {
            result.Record = true;
        }
        else
        {
            result.Record = false;
            string msg = string.Format("Sakkora státusz atmenet nem lehetseges Sakkora: {0} -> {1}", ugyirat.SakkoraAllapot, sakkoraKod);
            Logger.Error(msg);
            result.ErrorCode = "-1";
            result.ErrorMessage = msg;
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ellenõrzi, hogy a sakkóra státusz váltás kivitelezhetõ-e?
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="sakkoraKod"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result CanSetSakkora(ExecParam execParam, string iratId, string ugyiratId, string sakkoraKod)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Debug("CanSetSakkoraStatus Start", execParam);
        Result result = new Result();
        result.Record = false;

        if (string.IsNullOrEmpty(ugyiratId))
        {
            throw new Exception("Hibás paraméter: ugyiratId");
        }

        if (string.IsNullOrEmpty(iratId))
        {
            throw new Exception("Hibás paraméter: iratId");
        }

        if (string.IsNullOrEmpty(sakkoraKod))
        {
            throw new Exception("Hibás paraméter: sakkoraKod");
        }

        #region SAKKORA KOD ATFORGAT

        if (sakkoraKod.Equals("0"))
        {
            sakkoraKod = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
        }

        #endregion SAKKORA KOD ATFORGAT

        EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, ugyiratId);
        EREC_IraIratok irat = GetIrat(execParam, iratId);

        List<string> toStates;
        bool canSetSakkora = Sakkora.CheckSakkoraAtmenet(execParam, ugyirat.SakkoraAllapot, ugyirat.Ugy_Fajtaja, irat.EljarasiSzakasz, sakkoraKod, out toStates);
        if (canSetSakkora)
        {
            result.Record = true;
        }
        else
        {
            result.Record = false;
            // Az elérhető státuszok összefűzve a hibaüzenethez:
            //string toStatesStr = toStates != null ? string.Join(", ", toStates.ToArray()) : string.Empty;
            // Ügyirat státusz a hibaüzenethez:
            //var ugyiratStatuszok = KodTar_Cache.GetKodtarakByKodCsoport("UGYIRAT_ALLAPOT", execParam, HttpContext.Current.Cache);
            //string ugyiratStatuszStr = (ugyiratStatuszok != null && ugyiratStatuszok.ContainsKey(ugyirat.Allapot)) ? ugyiratStatuszok[ugyirat.Allapot] : string.Empty;

            //string msg = string.Format("Sakkóra státusz átmenet nem lehetséges: {0} -> {1} (Elérhető sakkóra státuszok: {2}; Ügyirat állapot: {3})"
            //        , ugyirat.SakkoraAllapot, sakkoraKod, toStatesStr, ugyiratStatuszStr);

            // BUG#6878:
            string msg = String.Format("A Sakkóra beállítás nem lehetséges, mert az ügy sakkóra állapota akadályozza: \"{0}\"", ugyirat.SakkoraAllapot);

            Logger.Error(msg);
            result.ErrorCode = "-1";
            result.ErrorMessage = msg;
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// SetSakkoraStatus
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="sakkoraStatusz"></param>
    /// <param name="okKod"></param>
    /// <param name="userID"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SetSakkoraStatus(ExecParam execParam, string iratId, string ugyiratId, string sakkoraStatusz, string okKod, string userID)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Debug("SetSakkoraStatus Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            KodTar_Cache.KodTarElem kodtar = GetKodtarakByKodCsoportList(execParam, sakkoraStatusz);

            List<Sakkora.EnumIratHatasaUgyintezesre> fromStates = null;
            Sakkora.EnumIratHatasaUgyintezesre toStates = new Sakkora.EnumIratHatasaUgyintezesre();
            Sakkora.EnumIratHatasaUgyintezesre toState = Sakkora.ExtractIratHatasStates(kodtar.Egyeb, ref fromStates, ref toStates);

            #region IRAT

            IratHatasMentesProcedure(execParam.Clone(), iratId, sakkoraStatusz, toState, okKod);

            #endregion IRAT

            #region ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL

            SetUgyHataridoSakkoraAlapjan(execParam.Clone(), iratId);

            #endregion ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL

            #region UGY SAKKORA ALAP ALLAPOTBA TEHETO

            SetUgySakkora(execParam.Clone(), iratId);

            #endregion UGY SAKKORA ALAP ALLAPOTBA TEHETO

            #region OBSOLETE

            //#region UGYIRAT
            //EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, ugyiratId);
            ////Ugyirat  llapot ügyintézés alatt és felfüggeszt típusú - > felfüggeszt
            //if (toState == Sakkora.EnumIratHatasaUgyintezesre.PAUSE && Sakkora.IsUgyiratUgyintezesAlatt(ugyirat))
            //{
            //    UgyiratFelfuggesztesProcedure(execParam, new string[] { ugyirat.Id }, okKod);
            //    result.Record = true;
            //}
            ////Ügyirat felfüggesztett vagy lezárt, típus indító -> indít
            //else if (toState == Sakkora.EnumIratHatasaUgyintezesre.START && (Sakkora.IsUgyiratFelfuggesztett(ugyirat) || Sakkora.IsUgyiratLezart(ugyirat)))
            //{
            //    UgyiratFolytatassProcedure(execParam, new string[] { ugyirat.Id });
            //    result.Record = true;
            //}
            ////Ügyirat felfüggesztett vagy lezárt, típus indító -> indít
            //else if (toState == Sakkora.EnumIratHatasaUgyintezesre.STOP && (Sakkora.IsUgyiratFelfuggesztett(ugyirat) || Sakkora.IsUgyiratUgyintezesAlatt(ugyirat)))
            //{
            //    UgyiratLezarasProcedure(execParam, new string[] { ugyirat.Id }, okKod);
            //    result.Record = true;
            //}
            //#endregion

            #endregion OBSOLETE

            // COMMIT
            dataContext.CommitIfRequired(isTransactionBeginHere);
        }
        catch (Exception e)
        {
            dataContext.RollbackIfRequired(isTransactionBeginHere);
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Sakkóra határidõk
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <param name="hataridoMarNovelve"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetSakkoraHataridok(ExecParam execParam, string ugyiratId, bool hataridoMarNovelve)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Debug("GetSakkoraHataridok Start", execParam);

        Result result = new Result();
        bool isConnectionOpenHere = false;
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.GetSakkoraHataridok(execParam, ugyiratId, hataridoMarNovelve);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Ügyintézési idő újraszámítása
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SakkoraUgyintezesiIdoUjraSzamolas(ExecParam execParam, string ugyiratId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        if (Rendszerparameterek.UseSakkora(execParam))
        {
            Logger.Debug("SakkoraUgyintezesiIdoUjraSzamolas Start", execParam);
            bool isConnectionOpenHere = false;
            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

                EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, ugyiratId);
                if (ugyirat == null)
                {
                    Logger.Debug("Ügyirat nem található !");
                    throw new ResultException("Ügyirat nem található !");
                }

                if (!string.IsNullOrEmpty(ugyirat.SakkoraAllapot))
                {
                    if (string.IsNullOrEmpty(ugyirat.Hatarido))
                    {
                        if (Sakkora.IsUgyFajtaHatosagi(execParam, ugyirat.Ugy_Fajtaja) && ugyirat.ElteltIdo != Sakkora.CONST_ELTELT_IDO_DEFAULT)
                        {
                            ugyirat.ElteltIdo = Sakkora.CONST_ELTELT_IDO_DEFAULT;
                            ugyirat.Updated.ElteltIdo = true;

                            ugyirat.ElteltIdoUtolsoModositas = Sakkora.GetTodayLastMinuteAsString();
                            ugyirat.Updated.ElteltIdoUtolsoModositas = true;

                            #region UPDATE UGY

                            UpdateUgyirat(execParam, ugyirat);

                            #endregion UPDATE UGY
                        }
                    }
                    else
                    {
                        Result resultAlszamok = GetUgyiratAlszamai(execParam, ugyiratId, true);
                        if (!string.IsNullOrEmpty(resultAlszamok.ErrorCode))
                        {
                            Logger.Debug(string.Format("Sakkóra: GetUgyiratAlszamai hiba ECODE={0} EMSG={1} IDS={2}", resultAlszamok.ErrorCode, resultAlszamok.ErrorMessage, ugyiratId));
                            throw new ResultException(resultAlszamok);
                        }

                        DateTime? ujraSzamoltUgyHatarido = null;
                        DateTime? ujraSzamoltUgyintezesKezdete = null;
                        double ujraSzamoltElteltUgyintezesiIdo = Sakkora.CONST_ELTELT_IDO_DEFAULT_INT;

                        #region UJRASZAMOL - ELTELT IDO

                        ujraSzamoltElteltUgyintezesiIdo = UjraSzamolUgyintezesiIdoProcedure(execParam, resultAlszamok, ugyirat);

                        #endregion UJRASZAMOL - ELTELT IDO

                        #region UJRASZAMOL - FELFUGGESZTETT NAPOK / HATARIDO KITOLAS

                        double felfuggesztettNapokSzamaUjraSzamolva = GetFelfuggesztettNapokSzamaForUjraszamolas(execParam, resultAlszamok, ujraSzamoltUgyHatarido);

                        #endregion UJRASZAMOL - FELFUGGESZTETT NAPOK / HATARIDO KITOLAS

                        #region UJRASZAMOL - HATARIDO / UGYINTEZES KEZDETE

                        UjraSzamolUgyintezesKezdeteEsHataridoProcedure(execParam, resultAlszamok, ugyirat, felfuggesztettNapokSzamaUjraSzamolva, out ujraSzamoltUgyintezesKezdete, out ujraSzamoltUgyHatarido);

                        #endregion UJRASZAMOL - HATARIDO / UGYINTEZES KEZDETE

                        #region UPDATE UGY

                        bool kellupdate = false;
                        if (ugyirat.ElteltIdo != ujraSzamoltElteltUgyintezesiIdo.ToString())
                        {
                            ugyirat.ElteltIdo = ujraSzamoltElteltUgyintezesiIdo.ToString();
                            ugyirat.Updated.ElteltIdo = true;
                            kellupdate = true;
                        }
                        if (ujraSzamoltUgyHatarido.HasValue && ugyirat.Hatarido != ujraSzamoltUgyHatarido.ToString())
                        {
                            ugyirat.Hatarido = ujraSzamoltUgyHatarido.ToString();
                            ugyirat.Updated.Hatarido = true;
                            kellupdate = true;
                        }
                        if (ujraSzamoltUgyintezesKezdete.HasValue && ugyirat.UgyintezesKezdete != ujraSzamoltUgyintezesKezdete.ToString())
                        {
                            ugyirat.UgyintezesKezdete = ujraSzamoltUgyintezesKezdete.ToString();
                            ugyirat.Updated.UgyintezesKezdete = true;
                            kellupdate = true;
                        }
                        if (ugyirat.FelfuggesztettNapokSzama != felfuggesztettNapokSzamaUjraSzamolva.ToString())
                        {
                            ugyirat.FelfuggesztettNapokSzama = felfuggesztettNapokSzamaUjraSzamolva.ToString();
                            ugyirat.Updated.FelfuggesztettNapokSzama = true;
                            kellupdate = true;
                        }

                        if (Sakkora.IsIratHatasaStartStrict(execParam, ugyirat.SakkoraAllapot) && SetUgyElteltIdoAllapotToStartProcedure(execParam, ref ugyirat))
                        {
                            kellupdate = true;
                        }

                        if (kellupdate)
                        {
                            UpdateUgyirat(execParam, ugyirat);
                        }

                        #endregion UPDATE UGY
                    }
                }
            }
            catch (Exception e)
            {
                result = ResultException.GetResultFromException(e);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// Sakkora lathato kovetkezo allapotok.
    /// Vesszovel elvalasztott lista a Record mezoben.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result GetSakkoraLathatoAllapotok(ExecParam execParam, string iratId, string ugyiratId)
    {
        Log log = Log.WsStart(execParam, Context, GetType());
        Logger.Debug("CanSetSakkoraStatus Start", execParam);
        Result result = new Result();

        if (string.IsNullOrEmpty(ugyiratId))
        {
            throw new Exception("Hibás paraméter: ugyiratId");
        }

        if (string.IsNullOrEmpty(iratId))
        {
            throw new Exception("Hibás paraméter: iratId");
        }

        EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, ugyiratId);
        EREC_IraIratok irat = GetIrat(execParam, iratId);

        Sakkora.EnumIratHatasaUgyintezesre actualType;
        List<string> lathatoAllapotok = Sakkora.SakkoraLathatoAllapotok(execParam, ugyirat.SakkoraAllapot, ugyirat.Ugy_Fajtaja, irat.EljarasiSzakasz, out actualType, false);

        result.Record = string.Join(",", lathatoAllapotok.ToArray());

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// SakkoraFelfuggesztettAllapotEsetenHataridoNoveles
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SakkoraFelfuggesztettAllapotEsetenHataridoNoveles(ExecParam execParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.SakkoraFelfuggesztettAllapotEsetenHataridoNoveles(execParam, ugyiratId);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    /// <summary>
    /// SakkoraElteltIdoNoveles
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    [WebMethod()]
    public Result SakkoraElteltIdoNoveles(ExecParam execParam, string ugyiratId)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(execParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            result = sp.SakkoraElteltIdoNoveles(execParam, ugyiratId);
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        log.WsEnd(execParam, result);
        return result;
    }

    #endregion INTERFACES

    #region PUBLIC

    /// <summary>
    /// SetUgyiratElteltIdoDefault
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="_EREC_UgyUgyiratok"></param>
    public void SetUgyiratElteltIdoDefault(ExecParam _ExecParam, ref EREC_UgyUgyiratok _EREC_UgyUgyiratok)
    {
        #region SAKKORA ELTELT IDO DEFAULT

        if (_EREC_UgyUgyiratok.ElteltIdo != Sakkora.CONST_ELTELT_IDO_DEFAULT)
        {
            _EREC_UgyUgyiratok.ElteltIdo = Sakkora.CONST_ELTELT_IDO_DEFAULT;
            _EREC_UgyUgyiratok.Updated.ElteltIdo = true;
        }
        string idoEgyseg = KodTarak.IDOEGYSEG.Nap;
        try
        {
            idoEgyseg = GetUgyiratIdoEgyseg(_ExecParam, _EREC_UgyUgyiratok.Id);
        }
        catch (Exception exc) { Logger.Error("SetUgyiratElteltIdoDefault", exc); }
        if (_EREC_UgyUgyiratok.ElteltIdoIdoEgyseg != idoEgyseg)
        {
            _EREC_UgyUgyiratok.ElteltIdoIdoEgyseg = idoEgyseg;
            _EREC_UgyUgyiratok.Updated.ElteltIdoIdoEgyseg = true;
        }
        SetUgyElteltIdoAllapotToStart(ref _EREC_UgyUgyiratok);

        #endregion SAKKORA ELTELT IDO DEFAULT
    }

    /// <summary>
    /// SetUgyUgyintezesKezdoDatumaKuldemenyBeerkezesere
    /// </summary>
    /// <param name="irat"></param>
    /// <param name="kuldemeny"></param>
    public void SetUgyUgyintezesKezdoDatumaKuldemenyBeerkezesere(ref EREC_IraIratok irat, EREC_KuldKuldemenyek kuldemeny)
    {
        if (irat == null)
        {
            return;
        }

        if (!string.IsNullOrEmpty(irat.UgyintezesKezdoDatuma))
        {
            return;
        }

        if (kuldemeny != null && !string.IsNullOrEmpty(kuldemeny.BeerkezesIdeje))
        {
            irat.UgyintezesKezdoDatuma = GetDateTimeNextHourWithZeroMinutesAsString(kuldemeny.BeerkezesIdeje);
        }
        else
        {
            irat.UgyintezesKezdoDatuma = GetDateTimeNextHourWithZeroMinutesAsString(DateTime.Now.ToString());
        }

        irat.Updated.UgyintezesKezdoDatuma = true;
    }

    /// <summary>
    /// Ugy sakkora allapota es eltelt ido bealliasa, sakkóra hatására
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="iratId"></param>
    public void SetUgySakkora(ExecParam _ExecParam, string iratId)
    {
        SetSakkoraV2(_ExecParam, iratId, Sakkora.NormalVagyAlszamraIktatas.NULL);
    }

    /// <summary>
    /// Set sakkora
    /// </summary>
    /// <param name="_ExecParam"></param>
    /// <param name="iktatasResult"></param>
    /// <param name="iktatasTipus"></param>
    public void SetSakkora(ExecParam _ExecParam, ErkeztetesIktatasResult iktatasResult, Sakkora.NormalVagyAlszamraIktatas iktatasTipus)
    {
        SetSakkoraV2(_ExecParam, iktatasResult.IratId, iktatasTipus);
    }

    /// <summary>
    /// SetSakkoraV2
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="iktatasTipus"></param>
    public void SetSakkoraV2(ExecParam execParam, string iratId, Sakkora.NormalVagyAlszamraIktatas iktatasTipus)
    {
        if (!Rendszerparameterek.UseSakkora(execParam))
        {
            return;
        }

        if (string.IsNullOrEmpty(iratId))
        {
            return;
        }

        try
        {
            EREC_IraIratok irat = GetIrat(execParam, iratId);
            if (irat == null)
            {
                return;
            }

            EREC_UgyUgyiratok ugy = null;
            ugy = GetUgyirat(execParam, irat.Ugyirat_Id);
            if (ugy == null)
            {
                return;
            }

            #region SET UGY ALLAPOTA IKTATAS SORAN UGYINTEZES ALATTIRA

            if (SetUgyAllapotaUgyintezesAlattira(execParam, iktatasTipus, irat, ugy))
            {
                ugy = GetUgyirat(execParam, irat.Ugyirat_Id);
            }

            #endregion SET UGY ALLAPOTA IKTATAS SORAN UGYINTEZES ALATTIRA

            #region HATOSAGI UGY & ELJARASI SZAKASZ URES -> DEFAULT 1FOK

            SetEljarasiSzakaszDefault(execParam, ref irat, ugy);

            #endregion HATOSAGI UGY & ELJARASI SZAKASZ URES -> DEFAULT 1FOK

            SetUgySakkoraAllapotaV2(execParam, ref irat, ref ugy);

            SetUgyElteltIdoV2(execParam, ref irat, ref ugy);

            UpdateUgyirat(execParam, ugy);

            if (ugy != null && ugy.Allapot == KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt &&
                (
              ugy.SakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy
              ||
              ugy.SakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy
              ||
              ugy.SakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot))
            {
                SakkoraUgyintezesiIdoUjraSzamolas(execParam, ugy.Id);
                Logger.Debug("SakkoraUgyintezesiIdoUjraSzamolas");
            }
        }
        catch (Exception exc)
        {
            Logger.Error("sakkora.SetSakkoraV2 ", exc);
        }
    }
    /// <summary>
    /// SetUgyAllapotaUgyintezesAlattira
    /// </summary>
    /// <param name="iktatasTipus"></param>
    /// <param name="irat"></param>
    /// <param name="ugy"></param>
    private bool SetUgyAllapotaUgyintezesAlattira(ExecParam execParam, Sakkora.NormalVagyAlszamraIktatas iktatasTipus, EREC_IraIratok irat, EREC_UgyUgyiratok ugy)
    {
        switch (iktatasTipus)
        {
            case Sakkora.NormalVagyAlszamraIktatas.NormalIktatas:
                if (!string.IsNullOrEmpty(irat.IratHatasaUgyintezesre) && !Sakkora.SakkoraNullas(irat.IratHatasaUgyintezesre))
                {
                    ugy.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                    ugy.Updated.Allapot = true;
                    UpdateUgyirat(execParam, ugy);
                    return true;
                }
                break;

            case Sakkora.NormalVagyAlszamraIktatas.AlszamraIktatas:
                if (IsUgyiratIktatott(ugy))
                {
                    ugy.Allapot = KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt;
                    ugy.Updated.Allapot = true;
                    UpdateUgyirat(execParam, ugy);
                    return true;
                }
                break;

            default:
                break;
        }
        return false;
    }

    /// <summary>
    /// SetUgyHataridoSakkoraAlapjan
    /// Sakkora
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    public void SetUgyHataridoSakkoraAlapjan(ExecParam execParam, string iratId)
    {
        Logger.Debug("SetUgyHataridoSakkoraAlapjan.Start  " + iratId);
        EREC_IraIratok irat = GetIrat(execParam, iratId);
        if (irat == null && irat.Hatarido == null)
        {
            return;
        }

        SetUgyHataridoSakkoraAlapjan(execParam, irat);

        Logger.Debug("SetUgyHataridoSakkoraAlapjan.Stop  " + iratId);
    }

    /// <summary>
    /// SetUgyHataridoSakkoraAlapjan
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    public void SetUgyHataridoSakkoraAlapjan(ExecParam execParam, EREC_IraIratok irat)
    {
        if (irat == null && irat.Hatarido == null)
        {
            return;
        }

        Logger.Debug("SetUgyHataridoSakkoraAlapjan.Start  " + irat.Azonosito);

        if (string.IsNullOrEmpty(irat.UgyintezesKezdoDatuma) && string.IsNullOrEmpty(irat.Hatarido))
        {
            return;
        }

        if (!Sakkora.IsIratHatasaStart(execParam, irat.IratHatasaUgyintezesre))
        {
            Logger.Debug(string.Format("SetUgyHataridoSakkoraAlapjan ! IsIratHatasaStart: AZ={0}", irat.Azonosito));
            return;
        }

        EREC_UgyUgyiratok ugy = GetUgyiratByIrat(execParam.Clone(), irat);

        if (irat.IratHatasaUgyintezesre == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy)
        {
            if (GetIratVoltElsoInduloSakkora(execParam.Clone(), irat))
            {
                #region VOLT POSTAZAS, INDULOS SAKKORA, ELTELT IDO SZAMOLAS INDITASA

                if (SetUgyElteltIdoAllapotToStartProcedure(execParam, ref ugy))
                {
                    execParam.Record_Id = ugy.Id;
                    UpdateUgyirat(execParam.Clone(), ugy);
                }

                #endregion VOLT POSTAZAS, INDULOS SAKKORA, ELTELT IDO SZAMOLAS INDITASA

                Logger.Debug(string.Format("SetUgyHataridoSakkoraAlapjan.IratVoltElsoInduloSakkora: AZ={0}", irat.Azonosito));
                return;
            }
        }
        else if (irat.IratHatasaUgyintezesre != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy
                &&
                 irat.IratHatasaUgyintezesre != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
        {
            Logger.Debug(string.Format("SetUgyHataridoSakkoraAlapjan. Nem 4 vagy 5-os a sakkora: AZ={0}", irat.Azonosito));
            return;
        }

        if (ugy == null)
        {
            return;
        }

        bool kellUpdate = false;

        if (ugy.SakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy || ugy.SakkoraAllapot == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
        {
            if (!string.IsNullOrEmpty(irat.UgyintezesKezdoDatuma) && ugy.UgyintezesKezdete != irat.UgyintezesKezdoDatuma)
            {
                ugy.UgyintezesKezdete = irat.UgyintezesKezdoDatuma;
                ugy.Updated.UgyintezesKezdete = true;
                kellUpdate = true;
                Logger.Debug(string.Format("SetUgyHataridoSakkoraAlapjan.Ugy.Set.UgyintezesKezdoDatuma: AZ={0} DT={1}", ugy.Azonosito, ugy.UgyintezesKezdete));
            }

            if (!string.IsNullOrEmpty(irat.Hatarido) && ugy.Hatarido != irat.Hatarido)
            {
                ugy.Hatarido = irat.Hatarido;
                ugy.Updated.Hatarido = true;
                kellUpdate = true;
                Logger.Debug(string.Format("SetUgyHataridoSakkoraAlapjan.Ugy.Set.Hatarido: AZ={0} DT={1}", ugy.Azonosito, ugy.Hatarido));
            }
        }

        #region NULLAZAS UTAN, ELSO INDULOS ESETEN, UJRA AKTIVALJA AZ ELTELT IDO SZAMOLAST

        if (SetUgyElteltIdoAllapotToStartProcedure(execParam, ref ugy))
        {
            kellUpdate = true;
        }

        #endregion NULLAZAS UTAN, ELSO INDULOS ESETEN, UJRA AKTIVALJA AZ ELTELT IDO SZAMOLAST

        execParam.Record_Id = ugy.Id;
        if (kellUpdate)
        {
            UpdateUgyirat(execParam.Clone(), ugy);
        }

        Logger.Debug("SetUgyHataridoSakkoraAlapjan.Stop  " + irat.Azonosito);
    }

    #endregion PUBLIC

    #region SERVICES

    /// <summary>
    /// Ugyirat keresése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string ugyiratId)
    {
        EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
        execParam.Record_Id = ugyiratId;

        Result result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)result.Record;
        ugy.Updated.SetValueAll(false);
        ugy.Base.Updated.SetValueAll(false);
        ugy.Base.Updated.Ver = true;

        return ugy;
    }

    /// <summary>
    /// Irat keresése
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private EREC_IraIratok GetIrat(ExecParam execParam, string iratId)
    {
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        execParam.Record_Id = iratId;

        Result result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            throw new ResultException(result);
        }

        EREC_IraIratok irat = (EREC_IraIratok)result.Record;
        irat.Updated.SetValueAll(false);
        irat.Base.Updated.SetValueAll(false);
        irat.Base.Updated.Ver = true;

        return irat;
    }

    /// <summary>
    /// GetUgyiratAlszamai
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private Result GetUgyiratAlszamai(ExecParam execParam, string ugyiratId, bool novekvo)
    {
        Result result = null;

        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch src = new EREC_IraIratokSearch();

        src.Ugyirat_Id.Value = ugyiratId;
        src.Ugyirat_Id.Operator = Query.Operators.equals;

        src.OrderBy = novekvo ? " Alszam ASC" : " Alszam DESC";

        result = service.GetAll(execParam.Clone(), src);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        return result.Ds.Tables[0].Rows.Count < 1 ? null : result;
    }

    /// <summary>
    /// UpdateUgyirat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugy"></param>
    private void UpdateUgyirat(ExecParam execParam, EREC_UgyUgyiratok ugy)
    {
        EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam ugyiratokExecParam = execParam.Clone();
        ugyiratokExecParam.Record_Id = ugy.Id;
        //V2
        Result result = ugyiratokService.UpdateUgyiratDirectWithoutFlow(execParam, ugy);

        //V1
        //Result result = ugyiratokService.Update(ugyiratokExecParam, ugy);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug(string.Format("Sakkóra: Ügy mentés hiba ECODE={0} EMSG={1} IDS={2}", result.ErrorCode, result.ErrorMessage, ugy.Id));
            throw new ResultException(result);
        }
    }

    /// <summary>
    /// GetUgyiratByIrat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    /// <returns></returns>
    private EREC_UgyUgyiratok GetUgyiratByIrat(ExecParam execParam, EREC_IraIratok irat)
    {
        EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam ugyiratokExecParam = execParam.Clone();
        ugyiratokExecParam.Record_Id = irat.Ugyirat_Id;
        Result ugyiratokResult = ugyiratokService.Get(ugyiratokExecParam);

        if (ugyiratokResult.IsError)
        {
            throw new ResultException(ugyiratokResult);
        }

        EREC_UgyUgyiratok ugy = (EREC_UgyUgyiratok)ugyiratokResult.Record;
        ugy.Updated.SetValueAll(false);
        ugy.Base.Updated.SetValueAll(false);
        ugy.Base.Updated.Ver = true;
        return ugy;
    }

    /// <summary>
    /// UpdateIrat
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    private Result UpdateIrat(ExecParam execParam, EREC_IraIratok irat)
    {
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        execParam.Record_Id = irat.Id;

        Result result = service.UpdateIratDirectWithoutFlow(execParam, irat);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug(string.Format("Sakkóra: Irat mentés hiba ECODE={0} EMSG={1} IDS={2}", result.ErrorCode, result.ErrorMessage, irat.Id));
        }
        return result;
    }

    /// <summary>
    /// GetIratVoltElsoInduloSakkora
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    /// <returns></returns>
    private bool GetIratVoltElsoInduloSakkora(ExecParam execParam, EREC_IraIratok irat)
    {
        EREC_IraIratokService serviceIratok = new EREC_IraIratokService(this.dataContext);
        return serviceIratok.GetIratVoltElsoInduloSakkora(execParam.Clone(), irat);
    }

    #endregion SERVICES

    #region HELPER

    #region SAKKORA PROCEDURES

    /// <summary>
    /// IratHatasMentesProcedure
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <param name="toState"></param>
    /// <param name="sakkoraStatusz"></param>
    /// <param name="ok"></param>
    private void IratHatasMentesProcedure(ExecParam execParam, string iratId, string sakkoraStatusz, Sakkora.EnumIratHatasaUgyintezesre toState, string ok)
    {
        EREC_IraIratok irat = GetIrat(execParam, iratId);
        irat.IratHatasaUgyintezesre = sakkoraStatusz;
        irat.Updated.IratHatasaUgyintezesre = true;

        if (!string.IsNullOrEmpty(ok))
        {
            if (toState == Sakkora.EnumIratHatasaUgyintezesre.PAUSE)
            {
                irat.FelfuggesztesOka = new Sakkora().TurnFelfuggesztesOka(execParam, ok);
                irat.Updated.FelfuggesztesOka = true;
            }
            else if (toState == Sakkora.EnumIratHatasaUgyintezesre.STOP)
            {
                irat.LezarasOka = ok;
                irat.Updated.LezarasOka = true;
            }
        }
        UpdateIrat(execParam, irat);
    }

    #region UJRASZAMOLAS

    [Obsolete]
    /// <summary>
    /// GetUgyintezesiIdoUjraSzamolas
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratok"></param>
    /// <returns></returns>
    private double GetUgyintezesiIdoHataridoUjraSzamolasProcedure(ExecParam execParam, Result iratok, EREC_UgyUgyiratok ugyirat, out DateTime? hataridoUj)
    {
        #region OUT

        hataridoUj = null;

        #endregion OUT

        #region VARIABLES

        double resultUgyintezesiIdo = 0;
        double tempUgyIdo = 0;

        DateTime maDatum = Sakkora.GetTodayLastMinute();
        DateTime? iktatasDatum = null;

        string tempStringAktualisIratKezdoDatuma = null;
        DateTime tempKezdoDatum;
        DateTime? tempAktualisIratHatarideje = null;

        DateTime? tempUtolsoIndulasosIratHatarideje = null;
        DateTime? tempPostazasDatuma = null;
        DateTime? utolsoPostazasDatuma = null;
        bool utolsoIndulasosVagyNullasVolt = false;

        string tempSakkora;
        bool tempSakkoraIsStart;
        bool tempSakkoraIsStop;
        bool tempSakkoraIsPause;
        bool tempSakkoraIsZero;
        bool? utolsoIndulasosVolt = null;
        bool utolsoNullasVolt = false;

        int indulasosIratCount = 0;
        int nullasIratCount = 0;
        int szunetIratCount = 0;
        int megallitasosIratCount = 0;

        int tmpPostazasokSzama = 0;

        #endregion VARIABLES

        foreach (System.Data.DataRow row in iratok.Ds.Tables[0].Rows)
        {
            tempUgyIdo = 0;
            tempSakkoraIsStart = false;
            tempSakkoraIsStop = false;
            tempSakkoraIsPause = false;
            tempSakkoraIsZero = false;

            #region SET KEZOO DATUM

            tempKezdoDatum = DateTime.MinValue;

            tempStringAktualisIratKezdoDatuma = GetAktualisIratKezdoDatuma(row);

            #endregion SET KEZOO DATUM

            tempSakkora = row["IratHatasaUgyintezesre"].ToString();
            if (!string.IsNullOrEmpty(tempSakkora))
            {
                if (Sakkora.SakkoraNullas(tempSakkora))
                {
                    tempSakkoraIsZero = true;
                    ++nullasIratCount;
                }
                else if (Sakkora.IsIratHatasaStartStrict(execParam, tempSakkora))
                {
                    tempSakkoraIsStart = true;
                    ++indulasosIratCount;
                }
                else if (Sakkora.IsIratHatasaPauseStrict(execParam, tempSakkora))
                {
                    tempSakkoraIsPause = true;
                    ++szunetIratCount;
                }
                else if (Sakkora.IsIratHatasaStopStrict(execParam, tempSakkora))
                {
                    tempSakkoraIsStop = true;
                    ++megallitasosIratCount;
                }

                GetAktualisIratHatarideje(ref tempAktualisIratHatarideje, row);
                if (tempSakkoraIsStart)
                {
                    utolsoNullasVolt = false;
                    utolsoIndulasosVagyNullasVolt = true;

                    if (DateTime.TryParse(tempStringAktualisIratKezdoDatuma, out tempKezdoDatum))
                    {
                        #region NEM VOLT MEG INDULASOS, VAGY VOLT MAR POSTAZOTT VAGY 4-ES/5-OS SAKKORA

                        if (!iktatasDatum.HasValue || tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy || tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
                        {
                            iktatasDatum = tempKezdoDatum;
                        }

                        #endregion NEM VOLT MEG INDULASOS, VAGY VOLT MAR POSTAZOTT VAGY 4-ES/5-OS SAKKORA
                    }

                    #region HATARIDO MENTESE A NULLAZASHOZ

                    if (!string.IsNullOrEmpty(ugyirat.IntezesiIdo))
                    {
                        int intezesIdo;
                        if (int.TryParse(ugyirat.IntezesiIdo, out intezesIdo))
                        {
                            DateTime kezdet = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, tempKezdoDatum, ugyirat.IntezesiIdo);
                            string errorMessage;
                            tempUtolsoIndulasosIratHatarideje = Sakkora.GetIratintezesHataridoGlobal(execParam, kezdet.ToString(), ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
                        }
                    }
                    else if (tempAktualisIratHatarideje.HasValue)
                    {
                        DateTime kezdet = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, DateTime.Parse(tempStringAktualisIratKezdoDatuma), ugyirat.IntezesiIdo);
                        string errorMessage;
                        tempUtolsoIndulasosIratHatarideje = Sakkora.GetIratintezesHataridoGlobal(execParam, kezdet.ToString(), ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
                    }

                    #endregion HATARIDO MENTESE A NULLAZASHOZ

                    #region 4-ES/5-OS SAKKORA & VOLT POSTAZAS KORABBAN --> UGYIRATNAK UJ HATARIDO ERTEKE LESZ

                    if ((tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy || tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
                            && tempAktualisIratHatarideje.HasValue
                            //&& tempPostazasDatuma.HasValue
                            )
                    {
                        #region UJ HATARIDO

                        string tempString4EsSakkoraKezdoDatum = tempStringAktualisIratKezdoDatuma;

                        if (!string.IsNullOrEmpty(tempString4EsSakkoraKezdoDatum) && ugyirat != null && !string.IsNullOrEmpty(ugyirat.IntezesiIdo))
                        {
                            DateTime temp4EsSakkoraDateStart;
                            int temp4EsSakkoraIntezesiIdo;
                            if (DateTime.TryParse(tempString4EsSakkoraKezdoDatum, out temp4EsSakkoraDateStart))
                            {
                                if (int.TryParse(ugyirat.IntezesiIdo, out temp4EsSakkoraIntezesiIdo))
                                {
                                    if (ugyirat.IntezesiIdoegyseg == KodTarak.IDOEGYSEG.Munkanap)
                                    {
                                        hataridoUj = GetMunkanapHataridoFromExtraNapok(execParam, temp4EsSakkoraDateStart, temp4EsSakkoraIntezesiIdo);
                                    }
                                    else if (ugyirat.IntezesiIdoegyseg == KodTarak.IDOEGYSEG.Nap)
                                    {
                                        hataridoUj = temp4EsSakkoraDateStart.AddDays(temp4EsSakkoraIntezesiIdo);
                                    }
                                    else
                                    {
                                        hataridoUj = temp4EsSakkoraDateStart.AddDays(temp4EsSakkoraIntezesiIdo);
                                    }
                                }
                            }
                        }
                        else
                        {
                            hataridoUj = tempAktualisIratHatarideje;
                        }

                        #endregion UJ HATARIDO

                        resultUgyintezesiIdo = 0;
                    }

                    #endregion 4-ES/5-OS SAKKORA & VOLT POSTAZAS KORABBAN --> UGYIRATNAK UJ HATARIDO ERTEKE LESZ

                    utolsoIndulasosVolt = true;
                }
                else if (tempSakkoraIsStop || tempSakkoraIsPause)
                {
                    utolsoNullasVolt = false;
                    utolsoIndulasosVagyNullasVolt = false;
                    tempPostazasDatuma = GetIratPostazasDatuma(execParam, row["Id"].ToString());
                    if (tempPostazasDatuma.HasValue)
                    {
                        utolsoPostazasDatuma = tempPostazasDatuma.Value.AddDays(1);
                    }

                    #region VOLT POSTAZAS

                    if (tempPostazasDatuma.HasValue)
                    {
                        ++tmpPostazasokSzama;
                        if (utolsoIndulasosVolt.HasValue && utolsoIndulasosVolt.Value && iktatasDatum.HasValue)
                        {
                            #region VOLT POSTAZAS ES ELOZO INDULASOS -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - ELOZO.ALSZAM.IKT.DATUM)

                            tempUgyIdo = GetNaptariNapokSzama(execParam, iktatasDatum.Value, tempPostazasDatuma.Value);

                            resultUgyintezesiIdo += tempUgyIdo;
                            tempPostazasDatuma = null;
                            iktatasDatum = null;

                            #endregion VOLT POSTAZAS ES ELOZO INDULASOS -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - ELOZO.ALSZAM.IKT.DATUM)
                        }
                        else if (iktatasDatum.HasValue)
                        {
                            #region VOLT POSTAZAS - DE ELOZO SEMMI/STOP/PAUSE -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - AKTUALIS.ALSZAM IKT.DATUM)

                            if (DateTime.TryParse(tempStringAktualisIratKezdoDatuma, out tempKezdoDatum))
                            {
                                tempUgyIdo = GetNaptariNapokSzama(execParam, tempKezdoDatum, tempPostazasDatuma.Value);

                                resultUgyintezesiIdo += tempUgyIdo;
                                tempPostazasDatuma = null;
                                iktatasDatum = null;
                            }

                            #endregion VOLT POSTAZAS - DE ELOZO SEMMI/STOP/PAUSE -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - AKTUALIS.ALSZAM IKT.DATUM)
                        }
                    }
                    else
                    {
                        #region STOP/PAUSE ESETEN NINCS MEG POSTAZAS - DE TELIK AZ IDO A MAI NAPIG

                        if (DateTime.TryParse(tempStringAktualisIratKezdoDatuma, out tempKezdoDatum))
                        {
                            if (!iktatasDatum.HasValue)
                            {
                                iktatasDatum = tempKezdoDatum;
                            }
                        }

                        #endregion STOP/PAUSE ESETEN NINCS MEG POSTAZAS - DE TELIK AZ IDO A MAI NAPIG
                    }

                    #endregion VOLT POSTAZAS

                    utolsoIndulasosVolt = false;
                }
                else if (tempSakkoraIsZero)
                {
                    utolsoNullasVolt = true;
                    utolsoIndulasosVolt = false;
                    utolsoIndulasosVagyNullasVolt = true;
                }
            }
        }

        #region CASE 6 - VEGEROL VISSZANULLAZAS ESETEN - A HATARIDO VISSZAIRASA AZ UTOLSO INDULASOSRA

        if (utolsoNullasVolt && tempUtolsoIndulasosIratHatarideje.HasValue)
        {
            hataridoUj = tempUtolsoIndulasosIratHatarideje;
        }

        #endregion CASE 6 - VEGEROL VISSZANULLAZAS ESETEN - A HATARIDO VISSZAIRASA AZ UTOLSO INDULASOSRA

        #region CASE 7 - 1 INDULASOS, A TOBBI NULLAS - A HATARIDO VISSZAIRASA AZ INDULASOSRA

        if (utolsoIndulasosVolt.HasValue && utolsoIndulasosVolt.Value && indulasosIratCount == 1 && megallitasosIratCount == 0 && szunetIratCount == 0)
        {
            hataridoUj = tempUtolsoIndulasosIratHatarideje;
        }

        #endregion CASE 7 - 1 INDULASOS, A TOBBI NULLAS - A HATARIDO VISSZAIRASA AZ INDULASOSRA

        #region RESULT

        if (iktatasDatum.HasValue)
        {
            if (!utolsoPostazasDatuma.HasValue)
            {
                tempUgyIdo = GetNaptariNapokSzama(execParam, iktatasDatum.Value, maDatum);
            }
            else if (iktatasDatum.HasValue && utolsoIndulasosVagyNullasVolt)
            {
                tempUgyIdo = GetNaptariNapokSzama(execParam, iktatasDatum.Value.AddDays(1), maDatum); ///VOLT POSTAZAS, INDULOS SAKKORA ES NULLAZAS VISSZAFELE
            }
            else if (iktatasDatum.Value > utolsoPostazasDatuma.Value && utolsoIndulasosVolt.HasValue && utolsoIndulasosVolt.Value)
            {
                tempUgyIdo = GetNaptariNapokSzama(execParam, iktatasDatum.Value.AddDays(1), maDatum); ///POSTAZAS UTANI NAPON INDUL EL AZ ELTELT IDO SZAMOLAS
            }
            else
            {
                tempUgyIdo = GetNaptariNapokSzama(execParam, utolsoPostazasDatuma.Value, maDatum);
            }

            if (tempUgyIdo <= 0)
            {
                tempUgyIdo = 1;
            }
            resultUgyintezesiIdo += tempUgyIdo;
        }

        #endregion RESULT

        #region POSTAZASKOR MAR NE SZAMOLJUNK TELJES NAPOT - TEMP SLN

        if (tmpPostazasokSzama > 0)
        {
            --tmpPostazasokSzama;
            resultUgyintezesiIdo -= tmpPostazasokSzama;
        }

        #endregion POSTAZASKOR MAR NE SZAMOLJUNK TELJES NAPOT - TEMP SLN

        return resultUgyintezesiIdo;
    }

    /// <summary>
    /// UjraSzamolUgyintezesiIdoProcedure
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratok"></param>
    /// <param name="ugyirat"></param>
    /// <returns></returns>
    private double UjraSzamolUgyintezesiIdoProcedure(ExecParam execParam, Result iratok, EREC_UgyUgyiratok ugyirat)
    {
        #region VARIABLES

        double resultUgyintezesiIdo = 0;
        double tempUgyIdo = 0;

        DateTime maDatum = Sakkora.GetTodayLastMinute();
        DateTime? iktatasDatum = null;

        string tempStringAktualisIratKezdoDatuma = null;
        DateTime tempKezdoDatum;
        DateTime? tempAktualisIratHatarideje = null;

        DateTime? tempUtolsoIndulasosIratHatarideje = null;
        DateTime? tempPostazasDatuma = null;
        DateTime? utolsoPostazasDatuma = null;
        bool utolsoIndulasosVagyNullasVolt = false;

        string tempSakkora;
        bool tempSakkoraIsStart;
        bool tempSakkoraIsStop;
        bool tempSakkoraIsPause;
        bool tempSakkoraIsZero;
        bool? utolsoIndulasosVolt = null;
        //bool utolsoNullasVolt = false;

        int indulasosIratCount = 0;
        int nullasIratCount = 0;
        int szunetIratCount = 0;
        int megallitasosIratCount = 0;

        int tmpPostazasokSzama = 0;

        #endregion VARIABLES

        foreach (System.Data.DataRow row in iratok.Ds.Tables[0].Rows)
        {
            tempUgyIdo = 0;
            tempSakkoraIsStart = false;
            tempSakkoraIsStop = false;
            tempSakkoraIsPause = false;
            tempSakkoraIsZero = false;

            #region SET KEZOO DATUM

            tempKezdoDatum = DateTime.MinValue;

            tempStringAktualisIratKezdoDatuma = GetAktualisIratKezdoDatuma(row);

            #endregion SET KEZOO DATUM

            tempSakkora = row["IratHatasaUgyintezesre"].ToString();
            if (!string.IsNullOrEmpty(tempSakkora))
            {
                if (Sakkora.SakkoraNullas(tempSakkora))
                {
                    tempSakkoraIsZero = true;
                    ++nullasIratCount;
                }
                else if (Sakkora.IsIratHatasaStartStrict(execParam, tempSakkora))
                {
                    tempSakkoraIsStart = true;
                    ++indulasosIratCount;
                }
                else if (Sakkora.IsIratHatasaPauseStrict(execParam, tempSakkora))
                {
                    tempSakkoraIsPause = true;
                    ++szunetIratCount;
                }
                else if (Sakkora.IsIratHatasaStopStrict(execParam, tempSakkora))
                {
                    tempSakkoraIsStop = true;
                    ++megallitasosIratCount;
                }

                GetAktualisIratHatarideje(ref tempAktualisIratHatarideje, row);
                if (tempSakkoraIsStart)
                {
                    //utolsoNullasVolt = false;
                    utolsoIndulasosVagyNullasVolt = true;

                    if (DateTime.TryParse(tempStringAktualisIratKezdoDatuma, out tempKezdoDatum))
                    {
                        #region NEM VOLT MEG INDULASOS, VAGY VOLT MAR POSTAZOTT VAGY 4-ES/5-OS SAKKORA

                        if (!iktatasDatum.HasValue || tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy || tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
                        {
                            iktatasDatum = tempKezdoDatum;
                        }

                        #endregion NEM VOLT MEG INDULASOS, VAGY VOLT MAR POSTAZOTT VAGY 4-ES/5-OS SAKKORA
                    }

                    #region HATARIDO MENTESE A NULLAZASHOZ

                    if (!string.IsNullOrEmpty(ugyirat.IntezesiIdo))
                    {
                        int intezesIdo;
                        if (int.TryParse(ugyirat.IntezesiIdo, out intezesIdo))
                        {
                            DateTime kezdet = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, tempKezdoDatum, ugyirat.IntezesiIdo);
                            string errorMessage;
                            tempUtolsoIndulasosIratHatarideje = Sakkora.GetIratintezesHataridoGlobal(execParam, kezdet.ToString(), ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
                        }
                    }
                    else if (tempAktualisIratHatarideje.HasValue)
                    {
                        DateTime kezdet = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, DateTime.Parse(tempStringAktualisIratKezdoDatuma), ugyirat.IntezesiIdo);
                        string errorMessage;
                        tempUtolsoIndulasosIratHatarideje = Sakkora.GetIratintezesHataridoGlobal(execParam, kezdet.ToString(), ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
                    }

                    #endregion HATARIDO MENTESE A NULLAZASHOZ

                    #region 4-ES/5-OS SAKKORA & VOLT POSTAZAS KORABBAN --> UGYIRATNAK UJ HATARIDO ERTEKE LESZ

                    if ((tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy || tempSakkora == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
                            && tempAktualisIratHatarideje.HasValue
                            //&& tempPostazasDatuma.HasValue
                            )
                    {
                        resultUgyintezesiIdo = 0;
                    }

                    #endregion 4-ES/5-OS SAKKORA & VOLT POSTAZAS KORABBAN --> UGYIRATNAK UJ HATARIDO ERTEKE LESZ

                    utolsoIndulasosVolt = true;
                }
                else if (tempSakkoraIsStop || tempSakkoraIsPause)
                {
                    //utolsoNullasVolt = false;
                    utolsoIndulasosVagyNullasVolt = false;
                    tempPostazasDatuma = GetIratPostazasDatuma(execParam, row["Id"].ToString());
                    if (tempPostazasDatuma.HasValue)
                    {
                        utolsoPostazasDatuma = tempPostazasDatuma.Value.AddDays(1);
                    }

                    #region VOLT POSTAZAS

                    if (tempPostazasDatuma.HasValue)
                    {
                        ++tmpPostazasokSzama;
                        if (utolsoIndulasosVolt.HasValue && utolsoIndulasosVolt.Value && iktatasDatum.HasValue)
                        {
                            #region VOLT POSTAZAS ES ELOZO INDULASOS -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - ELOZO.ALSZAM.IKT.DATUM)

                            tempUgyIdo = GetNaptariNapokSzama4ElteltIdo(execParam, iktatasDatum.Value, tempPostazasDatuma.Value);

                            resultUgyintezesiIdo += tempUgyIdo;
                            tempPostazasDatuma = null;
                            iktatasDatum = null;

                            #endregion VOLT POSTAZAS ES ELOZO INDULASOS -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - ELOZO.ALSZAM.IKT.DATUM)
                        }
                        else if (iktatasDatum.HasValue)
                        {
                            #region VOLT POSTAZAS - DE ELOZO SEMMI/STOP/PAUSE -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - AKTUALIS.ALSZAM IKT.DATUM)

                            if (DateTime.TryParse(tempStringAktualisIratKezdoDatuma, out tempKezdoDatum))
                            {
                                tempUgyIdo = GetNaptariNapokSzama4ElteltIdo(execParam, tempKezdoDatum, tempPostazasDatuma.Value);

                                resultUgyintezesiIdo += tempUgyIdo;
                                tempPostazasDatuma = null;
                                iktatasDatum = null;
                            }

                            #endregion VOLT POSTAZAS - DE ELOZO SEMMI/STOP/PAUSE -  (AKTUALIS.ALSZAM.POSTAZAS.DATUM - AKTUALIS.ALSZAM IKT.DATUM)
                        }
                    }
                    else
                    {
                        #region STOP/PAUSE ESETEN NINCS MEG POSTAZAS - DE TELIK AZ IDO A MAI NAPIG

                        if (DateTime.TryParse(tempStringAktualisIratKezdoDatuma, out tempKezdoDatum) && !iktatasDatum.HasValue)
                        {
                            iktatasDatum = tempKezdoDatum;
                        }

                        #endregion STOP/PAUSE ESETEN NINCS MEG POSTAZAS - DE TELIK AZ IDO A MAI NAPIG
                    }

                    #endregion VOLT POSTAZAS

                    utolsoIndulasosVolt = false;
                }
                else if (tempSakkoraIsZero)
                {
                    //utolsoNullasVolt = true;
                    utolsoIndulasosVolt = false;
                    utolsoIndulasosVagyNullasVolt = true;
                }
            }
        }

        #region RESULT

        if (iktatasDatum.HasValue)
        {
            if (!utolsoPostazasDatuma.HasValue)
            {
                tempUgyIdo = GetNaptariNapokSzama4ElteltIdo(execParam, iktatasDatum.Value, maDatum);
            }
            else if (iktatasDatum.HasValue && utolsoIndulasosVagyNullasVolt)
            {
                tempUgyIdo = GetNaptariNapokSzama4ElteltIdo(execParam, iktatasDatum.Value, maDatum); ///VOLT POSTAZAS, INDULOS SAKKORA ES NULLAZAS VISSZAFELE
            }
            else if (iktatasDatum.Value > utolsoPostazasDatuma.Value && utolsoIndulasosVolt.HasValue && utolsoIndulasosVolt.Value)
            {
                tempUgyIdo = GetNaptariNapokSzama4ElteltIdo(execParam, iktatasDatum.Value.AddDays(1), maDatum); ///POSTAZAS UTANI NAPON INDUL EL AZ ELTELT IDO SZAMOLAS
            }
            else
            {
                tempUgyIdo = GetNaptariNapokSzama4ElteltIdo(execParam, utolsoPostazasDatuma.Value, maDatum);
            }

            resultUgyintezesiIdo += tempUgyIdo;
        }

        #endregion RESULT

        #region POSTAZASKOR MAR NE SZAMOLJUNK TELJES NAPOT - TEMP SLN

        //if (tmpPostazasokSzama > 0)
        //{
        //    --tmpPostazasokSzama;
        //    if (resultUgyintezesiIdo != 0)
        //    {
        //        resultUgyintezesiIdo -= tmpPostazasokSzama;
        //    }
        //}

        #endregion POSTAZASKOR MAR NE SZAMOLJUNK TELJES NAPOT - TEMP SLN

        return resultUgyintezesiIdo < 0 ? 0 : resultUgyintezesiIdo;
    }

    /// <summary>
    /// UjraSzamolUgyintezesKezdeteEsHataridoProcedure
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratok"></param>
    /// <param name="ugyirat"></param>
    /// <param name="ujUgyintezesKezdete"></param>
    /// <param name="ujHatarido"></param>
    /// <returns></returns>
    private bool UjraSzamolUgyintezesKezdeteEsHataridoProcedure(ExecParam execParam, Result iratok, EREC_UgyUgyiratok ugyirat, double felfuggesztettNapok, out DateTime? ujUgyintezesKezdete, out DateTime? ujHatarido)
    {
        ujUgyintezesKezdete = null;
        ujHatarido = null;
        EREC_IraIratok iratForHatarido = GetUtolsoIndulasosIratForUjraszamolas(iratok);
        EREC_IraIratok iratForUgyKezdete = GetIndulasosIratForUgyKezdeteUjraszamolas(iratok);
        if (iratForHatarido == null || string.IsNullOrEmpty(iratForHatarido.Id))
        {
            return false;
        }

        DateTime? tmpUjUgyintezesKezdete;
        if (GetUgyKezdeteUtolsoIndulasosIratbol4UjraSzamolas(execParam, iratForUgyKezdete, ugyirat, out tmpUjUgyintezesKezdete))
        {
            ujUgyintezesKezdete = tmpUjUgyintezesKezdete;
        }

        if (!ujUgyintezesKezdete.HasValue)
        {
            return false;
        }

        DateTime? tmpUjHatarido;
        if (GetUgyHataridejeUtolsoIndulasosIratbol4UjraSzamolas(execParam, iratForHatarido, ugyirat, ujUgyintezesKezdete.Value, felfuggesztettNapok, out tmpUjHatarido))
        {
            ujHatarido = tmpUjHatarido;
        }

        return false;
    }

    /// <summary>
    /// GetUtolsoIndulasosIratForUjraszamolas
    /// </summary>
    /// <param name="iratok"></param>
    /// <returns></returns>
    private EREC_IraIratok GetUtolsoIndulasosIratForUjraszamolas(Result iratok)
    {
        EREC_IraIratok resultIrat = new EREC_IraIratok();
        if (iratok == null || iratok.Ds.Tables[0].Rows.Count < 1)
        { return null; }

        foreach (System.Data.DataRow row in iratok.Ds.Tables[0].Rows)
        {
            if (string.IsNullOrEmpty(row["IratHatasaUgyintezesre"].ToString()))
            {
                continue;
            }

            if (row["IratHatasaUgyintezesre"].ToString().Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy)
                 ||
                 row["IratHatasaUgyintezesre"].ToString().Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy)
                 ||
                 row["IratHatasaUgyintezesre"].ToString().Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot))
            {
                Utils.LoadBusinessDocumentFromDataRow(resultIrat, row);
            }
        }
        return resultIrat;
    }

    /// <summary>
    /// GetIndulasosIratForUgyKezdeteUjraszamolas
    /// </summary>
    /// <param name="iratok"></param>
    /// <returns></returns>
    private EREC_IraIratok GetIndulasosIratForUgyKezdeteUjraszamolas(Result iratok)
    {
        EREC_IraIratok resultIrat = new EREC_IraIratok();
        if (iratok == null || iratok.Ds.Tables[0].Rows.Count < 1)
        { return null; }

        System.Data.DataRowCollection iratokRows = iratok.Ds.Tables[0].Rows;
        bool voltMasodfok = false;
        string sakkoraErtek;

        for (int i = 0; i < iratokRows.Count; i++)
        {
            sakkoraErtek = iratokRows[i]["IratHatasaUgyintezesre"].ToString();
            if (string.IsNullOrEmpty(sakkoraErtek)
                ||
                (!sakkoraErtek.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy)
                 &&
                 !sakkoraErtek.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy)
                 &&
                 !sakkoraErtek.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot))
                )
            {
                continue;
            }

            if (resultIrat == null || string.IsNullOrEmpty(resultIrat.Id))
            {
                Utils.LoadBusinessDocumentFromDataRow(resultIrat, iratokRows[i]);
                continue;
            }
            else if (
                sakkoraErtek.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy)
                ||
                sakkoraErtek.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot))
            {
                Utils.LoadBusinessDocumentFromDataRow(resultIrat, iratokRows[i]);
                voltMasodfok = true;
                continue;
            }
            else if (voltMasodfok && sakkoraErtek.Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy))
            {
                Utils.LoadBusinessDocumentFromDataRow(resultIrat, iratokRows[i]);
                continue;
            }
        }
        return resultIrat;
    }

    /// <summary>
    /// GetFelfuggesztettNapokSzamaForUjraszamolas
    /// </summary>
    /// <param name="iratok"></param>
    /// <returns></returns>
    private double GetFelfuggesztettNapokSzamaForUjraszamolas(ExecParam execParam, Result iratok, DateTime? ugyiratHatarido)
    {
        double resultFelfuggesztettNapokSzama = 0;
        if (iratok == null || iratok.Ds.Tables[0].Rows.Count < 1)
        { return 0; }

        bool voltPostazas = false;

        DateTime? tempPostazasDatuma = null;
        double tempFelfugesztettNapok = 0;
        DateTime? tempDateAktualisIratKezdoDatuma;
        bool isPause = false;
        bool isStop = false;
        foreach (System.Data.DataRow row in iratok.Ds.Tables[0].Rows)
        {
            if (string.IsNullOrEmpty(row["IratHatasaUgyintezesre"].ToString()))
            {
                continue;
            }
            isPause = Sakkora.IsIratHatasaPauseStrict(execParam, row["IratHatasaUgyintezesre"].ToString());
            isStop = Sakkora.IsIratHatasaStopStrict(execParam, row["IratHatasaUgyintezesre"].ToString());
            if (isPause || isStop)
            {
                tempPostazasDatuma = GetIratPostazasDatuma(execParam, row["Id"].ToString());
                if (tempPostazasDatuma.HasValue)
                {
                    voltPostazas = true;
                }
                else
                {
                    voltPostazas = false;
                    tempPostazasDatuma = null;
                }
            }
            else if (
                 voltPostazas
                 &&
                 (
                     row["IratHatasaUgyintezesre"].ToString().Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy)
                     ||
                     row["IratHatasaUgyintezesre"].ToString().Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy)
                     ||
                     row["IratHatasaUgyintezesre"].ToString().Equals(KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot))
                )
            {
                tempDateAktualisIratKezdoDatuma = GetAktualisIratKezdoDatumaAsDate(row);
                if (tempDateAktualisIratKezdoDatuma.HasValue)
                {
                    tempFelfugesztettNapok = GetNaptariNapokSzamaForFelfuggesztes(execParam, tempPostazasDatuma.Value, tempDateAktualisIratKezdoDatuma.Value);
                    resultFelfuggesztettNapokSzama += tempFelfugesztettNapok;

                    #region RESET - CALCULATED ALREADY

                    voltPostazas = false;
                    tempPostazasDatuma = null;

                    #endregion RESET - CALCULATED ALREADY
                }
            }
        }
        if (voltPostazas && tempPostazasDatuma.HasValue && isPause)
        {
            tempFelfugesztettNapok = GetNaptariNapokSzama(execParam, tempPostazasDatuma.Value, DateTime.Today);
            resultFelfuggesztettNapokSzama += tempFelfugesztettNapok;
        }
        return resultFelfuggesztettNapokSzama;
    }

    /// <summary>
    /// GetUgyKezdeteUtolsoIndulasosIratbol4UjraSzamolas
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratForUgyKezdete"></param>
    /// <param name="ugyirat"></param>
    /// <param name="ujUgyintezesKezdete"></param>
    /// <returns></returns>
    private bool GetUgyKezdeteUtolsoIndulasosIratbol4UjraSzamolas(ExecParam execParam, EREC_IraIratok iratForUgyKezdete, EREC_UgyUgyiratok ugyirat, out DateTime? ujUgyintezesKezdete)
    {
        ujUgyintezesKezdete = null;
        if (iratForUgyKezdete == null)
        {
            return false;
        }

        #region UGY KEZDETE

        string tmpStringUgyintezesKezdete4UgyKezdete;
        if (!string.IsNullOrEmpty(iratForUgyKezdete.UgyintezesKezdoDatuma))
        {
            tmpStringUgyintezesKezdete4UgyKezdete = iratForUgyKezdete.UgyintezesKezdoDatuma;
        }
        else if (!string.IsNullOrEmpty(iratForUgyKezdete.IktatasDatuma))
        {
            tmpStringUgyintezesKezdete4UgyKezdete = iratForUgyKezdete.IktatasDatuma;
        }
        else
        {
            return false;
        }
        DateTime tmpDateTimeUgyintezesKezdete4UgyKezdete;
        if (DateTime.TryParse(tmpStringUgyintezesKezdete4UgyKezdete, out tmpDateTimeUgyintezesKezdete4UgyKezdete))
        {
            ujUgyintezesKezdete = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, tmpDateTimeUgyintezesKezdete4UgyKezdete, ugyirat.IntezesiIdo);
        }

        #endregion UGY KEZDETE

        if (!ujUgyintezesKezdete.HasValue)
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// GetUgyHataridejeUtolsoIndulasosIratbol4UjraSzamolas
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratForHatarido"></param>
    /// <param name="ugyirat"></param>
    /// <param name="ujUgyintezesKezdete"></param>
    /// <param name="felfuggesztetNapokSzamaUjraSzamolva"></param>
    /// <param name="ujHatarido"></param>
    /// <returns></returns>
    private bool GetUgyHataridejeUtolsoIndulasosIratbol4UjraSzamolas(ExecParam execParam, EREC_IraIratok iratForHatarido, EREC_UgyUgyiratok ugyirat, DateTime ujUgyintezesKezdete, double felfuggesztetNapokSzamaUjraSzamolva, out DateTime? ujHatarido)
    {
        ujHatarido = null;
        if (iratForHatarido == null)
        {
            return false;
        }

        #region VAGY UTOLSO INDULASOS IRAT HATARIDEJE, VAGY UGYIRAT ALAP HATARIDEJE LESZ AZ UGYIRAT UJ HATARIDEJE

        string errorMessage;
        DateTime ugyiratBaseHatarido = Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, ujUgyintezesKezdete, ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
        if (felfuggesztetNapokSzamaUjraSzamolva > 0)
        {
            ugyiratBaseHatarido = ugyiratBaseHatarido.AddDays(felfuggesztetNapokSzamaUjraSzamolva);
        }

        DateTime utolsoIratHatarido;
        DateTime.TryParse(iratForHatarido.Hatarido, out utolsoIratHatarido);

        ujHatarido = (utolsoIratHatarido != null && utolsoIratHatarido > ugyiratBaseHatarido) ? utolsoIratHatarido : ugyiratBaseHatarido;

        #endregion VAGY UTOLSO INDULASOS IRAT HATARIDEJE, VAGY UGYIRAT ALAP HATARIDEJE LESZ AZ UGYIRAT UJ HATARIDEJE

        return true;
    }

    [Obsolete]
    /// <summary>
    /// Get UgyKezdete Hatarideje
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    /// <param name="ugyirat"></param>
    /// <param name="ujUgyintezesKezdete"></param>
    /// <param name="ujHatarido"></param>
    private bool GetUgyKezdeteHataridejeUtolsoIndulasosIratbol4Ujraszmaolas(ExecParam execParam, EREC_IraIratok iratForHatarido, EREC_IraIratok iratForUgyKezdete, EREC_UgyUgyiratok ugyirat, out DateTime? ujUgyintezesKezdete, out DateTime? ujHatarido)
    {
        ujUgyintezesKezdete = null;
        ujHatarido = null;
        if (iratForHatarido == null || iratForUgyKezdete == null)
        {
            return false;
        }

        #region UGY KEZDETE

        string tmpStringUgyintezesKezdete4UgyKezdete;
        if (!string.IsNullOrEmpty(iratForUgyKezdete.UgyintezesKezdoDatuma))
        {
            tmpStringUgyintezesKezdete4UgyKezdete = iratForUgyKezdete.UgyintezesKezdoDatuma;
        }
        else if (!string.IsNullOrEmpty(iratForUgyKezdete.IktatasDatuma))
        {
            tmpStringUgyintezesKezdete4UgyKezdete = iratForUgyKezdete.IktatasDatuma;
        }
        else
        {
            return false;
        }
        DateTime tmpDateTimeUgyintezesKezdete4UgyKezdete;
        if (DateTime.TryParse(tmpStringUgyintezesKezdete4UgyKezdete, out tmpDateTimeUgyintezesKezdete4UgyKezdete))
        {
            ujUgyintezesKezdete = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, tmpDateTimeUgyintezesKezdete4UgyKezdete, ugyirat.IntezesiIdo);
        }

        #endregion UGY KEZDETE

        if (!ujUgyintezesKezdete.HasValue)
        {
            return false;
        }

        #region UGY HATARIDO

        #region VAGY UTOLSO INDULASOS IRAT HATARIDEJE, VAGY UGYIRAT ALAP HATARIDEJE LESZ AZ UGYIRAT UJ HATARIDEJE

        string errorMessage;
        DateTime ugyiratBaseHatarido = Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, ujUgyintezesKezdete.Value, ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);

        DateTime utolsoIratHatarido;
        DateTime.TryParse(iratForHatarido.Hatarido, out utolsoIratHatarido);

        ujHatarido = (utolsoIratHatarido != null && utolsoIratHatarido > ugyiratBaseHatarido) ? utolsoIratHatarido : ugyiratBaseHatarido;

        #endregion VAGY UTOLSO INDULASOS IRAT HATARIDEJE, VAGY UGYIRAT ALAP HATARIDEJE LESZ AZ UGYIRAT UJ HATARIDEJE

        #region V1

        //string tmpStringUgyintezesKezdete4Hatarido;
        //if (!string.IsNullOrEmpty(iratForHatarido.UgyintezesKezdoDatuma))
        //{
        //    tmpStringUgyintezesKezdete4Hatarido = iratForHatarido.UgyintezesKezdoDatuma;
        //}
        //else if (!string.IsNullOrEmpty(iratForHatarido.IktatasDatuma))
        //{
        //    tmpStringUgyintezesKezdete4Hatarido = iratForHatarido.IktatasDatuma;
        //}
        //else
        //{
        //    return false;
        //}

        //DateTime tmpDateTimeUgyintezesKezdete4Hatarido;
        //if (DateTime.TryParse(tmpStringUgyintezesKezdete4Hatarido, out tmpDateTimeUgyintezesKezdete4Hatarido))
        //{
        //    DateTime ugyintezesKezdete4Hatarido = Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, tmpDateTimeUgyintezesKezdete4Hatarido, ugyirat.IntezesiIdo);
        //    string errorMessage;
        //    ujHatarido = Sakkora.GetIratintezesHataridoGlobal(execParam, ugyintezesKezdete4Hatarido.ToString(), ugyirat.IntezesiIdo, ugyirat.IntezesiIdoegyseg, out errorMessage);
        //    if (!string.IsNullOrEmpty(errorMessage))
        //    {
        //        Logger.Error("Sakkora.GetIratintezesHataridoGlobal: " + errorMessage);
        //    }
        //    return true;
        //}

        #endregion V1

        #endregion UGY HATARIDO

        return true;
    }

    #endregion UJRASZAMOLAS

    /// <summary>
    /// GetAktualisIratHatarideje
    /// </summary>
    /// <param name="tempAktualisIratHatarideje"></param>
    /// <param name="row"></param>
    private static void GetAktualisIratHatarideje(ref DateTime? tempAktualisIratHatarideje, System.Data.DataRow row)
    {
        #region AKTUALIS IRAT HATARIDO

        if (!string.IsNullOrEmpty(row["Hatarido"].ToString()))
        {
            DateTime date;
            if (DateTime.TryParse(row["Hatarido"].ToString(), out date))
            {
                tempAktualisIratHatarideje = date;
            }
        }

        #endregion AKTUALIS IRAT HATARIDO
    }

    /// <summary>
    /// GetAktualisIratIktatasDatuma
    /// </summary>
    /// <param name="tempAktualisIratIktatasDatum"></param>
    /// <param name="row"></param>
    private static void GetAktualisIratIktatasDatuma(ref DateTime? tempAktualisIratIktatasDatum, System.Data.DataRow row)
    {
        #region AKTUALIS IRAT HATARIDO

        if (!string.IsNullOrEmpty(row["IktatasDatuma"].ToString()))
        {
            DateTime date;
            if (DateTime.TryParse(row["IktatasDatuma"].ToString(), out date))
            {
                tempAktualisIratIktatasDatum = date;
            }
        }

        #endregion AKTUALIS IRAT HATARIDO
    }

    /// <summary>
    /// GetAktualisIratKezdoDatuma
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private static string GetAktualisIratKezdoDatuma(System.Data.DataRow row)
    {
        string tempStringAktualisIratKezdoDatuma;
        if (!string.IsNullOrEmpty(row["UgyintezesKezdoDatuma"].ToString()))
        {
            tempStringAktualisIratKezdoDatuma = row["UgyintezesKezdoDatuma"].ToString();
        }
        else if (!string.IsNullOrEmpty(row["IktatasDatuma"].ToString()))
        {
            tempStringAktualisIratKezdoDatuma = row["IktatasDatuma"].ToString();
        }
        else
        {
            tempStringAktualisIratKezdoDatuma = null;
        }
        return tempStringAktualisIratKezdoDatuma;
    }

    /// <summary>
    /// GetAktualisIratKezdoDatumaAsDate
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private static DateTime? GetAktualisIratKezdoDatumaAsDate(System.Data.DataRow row)
    {
        string tempStringIratKezdoDatuma = GetAktualisIratKezdoDatuma(row);
        if (string.IsNullOrEmpty(tempStringIratKezdoDatuma))
        {
            return null;
        }
        else
        {
            DateTime tempDateIratKezdoDatuma;
            DateTime.TryParse(tempStringIratKezdoDatuma, out tempDateIratKezdoDatuma);
            return tempDateIratKezdoDatuma;
        }
    }

    /// <summary>
    /// GetIratPostazasDatuma
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    /// <returns></returns>
    private DateTime? GetIratPostazasDatuma(ExecParam execParam, string iratId)
    {
        EREC_PldIratPeldanyokService service = new EREC_PldIratPeldanyokService(this.dataContext);
        EREC_PldIratPeldanyokSearch src = new EREC_PldIratPeldanyokSearch();
        src.IraIrat_Id.Value = iratId;
        src.IraIrat_Id.Operator = Query.Operators.equals;

        src.PostazasDatuma.Value = "";
        src.PostazasDatuma.Operator = Query.Operators.notnull;

        src.TopRow = 1;
        src.OrderBy = " PostazasDatuma DESC";
        Result result = service.GetAll(execParam, src);

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug(string.Format("Sakkóra: GetIratPostazasDatuma lekérdezés hiba ECODE={0} EMSG={1} IDS={2}", result.ErrorCode, result.ErrorMessage, iratId));
        }

        if (result.Ds.Tables[0].Rows.Count < 1)
        {
            return null;
        }

        string tempPostazasDatuma = result.Ds.Tables[0].Rows[0]["PostazasDatuma"].ToString();
        DateTime resultDate;
        if (DateTime.TryParse(tempPostazasDatuma, out resultDate))
        {
            return resultDate;
        }
        return null;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="from"></param>
    /// <param name="intezesiIdo"></param>
    /// <returns></returns>
    private DateTime GetMunkanapHataridoFromExtraNapok(ExecParam execParam, DateTime from, int sorszam)
    {
        Contentum.eAdmin.Service.KRT_Extra_NapokService extraNapokService = eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
        Result result = extraNapokService.KovetkezoMunkanap(execParam, from, sorszam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Debug(string.Format("Sakkóra: GetMunkanapHataridoFromExtraNapok hiba ECODE={0} EMSG={1} ", result.ErrorCode, result.ErrorMessage));
            return DateTime.Today;
        }
        else
        {
            try
            {
                return (DateTime)result.Record;
            }
            catch
            {
                return DateTime.Today;
            }
        }
    }

    /// <summary>
    /// GetNapokSzama
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    private double GetNaptariNapokSzama(ExecParam execParam, DateTime from, DateTime to)
    {
        if (to == DateTime.MinValue)
        {
            to = DateTime.Today;
        }

        if (from > to)
        {
            Logger.Debug("Sakkora.GetNapokSzama: Datumok hibasak !");
            return 0;
        }
        DateTime dtTo;
        DateTime dtFrom;
        try
        {
            dtTo = new DateTime(to.AddDays(1).Year, to.AddDays(1).Month, to.AddDays(1).Day, 0, 0, 1);
            dtFrom = new DateTime(from.Year, from.Month, from.Day, 23, 59, 59);
            return (dtTo - dtFrom).Days;
        }
        catch (Exception exc)
        {
            Logger.Debug("Sakkora.GetNaptariNapokSzama: " + exc.Message + exc.StackTrace);
            return 1;
        }
    }

    /// <summary>
    /// GetNaptariNapokSzamaForFelfuggesztes
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    private double GetNaptariNapokSzamaForFelfuggesztes(ExecParam execParam, DateTime from, DateTime to)
    {
        if (to == DateTime.MinValue)
        {
            to = DateTime.Today;
        }

        if (from > to)
        {
            Logger.Debug("Sakkora.GetNaptariNapokSzamaForFelfuggesztes: Datumok hibasak !");
            return 0;
        }
        DateTime dtTo;
        DateTime dtFrom;
        try
        {
            dtFrom = new DateTime(from.Year, from.Month, from.Day, 23, 59, 59);
            dtTo = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);
            return (dtTo - dtFrom).Days;
        }
        catch (Exception exc)
        {
            Logger.Debug("Sakkora.GetNaptariNapokSzamaForFelfuggesztes: " + exc.Message + exc.StackTrace);
            return 1;
        }
    }

    /// <summary>
    /// GetNaptariNapokSzama4ElteltIdo
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <returns></returns>
    private double GetNaptariNapokSzama4ElteltIdo(ExecParam execParam, DateTime from, DateTime to)
    {
        if (to == DateTime.MinValue)
        {
            to = DateTime.Today;
        }

        if (from > to)
        {
            Logger.Debug("Sakkora.GetNapokSzama: Datumok hibasak !");
            return 0;
        }

        if (from.Year == to.Year && from.Month == to.Month && from.Day == to.Day)
        {
            return 0;
        }

        DateTime dtTo;
        DateTime dtFrom;
        try
        {
            dtFrom = new DateTime(from.Year, from.Month, from.Day, 23, 59, 59);
            dtTo = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);
            return (dtTo - dtFrom).Days;
        }
        catch (Exception exc)
        {
            Logger.Debug("Sakkora.GetNaptariNapokSzama4ElteltIdo: " + exc.Message + exc.StackTrace);
            return 1;
        }
    }

    /// <summary>
    /// GetUgyAlszamokUtolsoNemNullasSakkoraErteke
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private string GetUgyAlszamokUtolsoNemNullasSakkoraErteke(ExecParam execParam, string ugyiratId)
    {
        string utolsoSakkoraAllapota = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
        Result resultAlszamok = GetUgyiratAlszamai(execParam, ugyiratId, false);
        if (!string.IsNullOrEmpty(resultAlszamok.ErrorCode))
        {
            Logger.Debug(string.Format("Sakkóra: GetUgyiratAlszamai hiba ECODE={0} EMSG={1} IDS={2}", resultAlszamok.ErrorCode, resultAlszamok.ErrorMessage, ugyiratId));
            throw new ResultException(resultAlszamok);
        }

        if (resultAlszamok == null || resultAlszamok.Ds.Tables[0].Rows.Count < 1)
        {
            return KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString();
        }

        string tmpSakkoraErtek = null;
        for (int i = 0; i < resultAlszamok.Ds.Tables[0].Rows.Count; i++)
        {
            tmpSakkoraErtek = resultAlszamok.Ds.Tables[0].Rows[i]["IratHatasaUgyintezesre"].ToString();
            if (IsSakkoraUresVagyNullas(tmpSakkoraErtek))
            {
                continue;
            }
            else
            {
                return tmpSakkoraErtek;
            }
        }
        return utolsoSakkoraAllapota;
    }

    #endregion SAKKORA PROCEDURES

    #region HELP

    /// <summary>
    /// IsUgyiratIktatott
    /// </summary>
    /// <param name="ugyirat"></param>
    /// <returns></returns>
    private bool IsUgyiratIktatott(EREC_UgyUgyiratok ugyirat)
    {
        return (ugyirat != null) && (ugyirat.Allapot == KodTarak.UGYIRAT_ALLAPOT.Iktatott);
    }

    /// <summary>
    /// Visszaadja a kódcsoporthoz tartozó kódtári elemet.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kodcsoportKod"></param>
    /// <param name="kodtarKod"></param>
    /// <returns></returns>
    private KodTar_Cache.KodTarElem GetKodtarakByKodCsoportList(ExecParam execParam, string kodtarKod)
    {
        List<KodTar_Cache.KodTarElem> sakkoraIratHatasai = KodTar_Cache.GetKodtarakByKodCsoportList(Sakkora.IRAT_HATASA_UGYINTEZESRE, execParam, HttpContext.Current.Cache, null);
        KodTar_Cache.KodTarElem result = null;
        foreach (KodTar_Cache.KodTarElem item in sakkoraIratHatasai)
        {
            if (item.Kod.Equals(kodtarKod))
            {
                result = item;
                break;
            }
        }

        if (result == null)
        {
            throw new Exception(string.Format("Nem található ilyen kódú kódtárelem: '{0}' (Kódcsoport: '{1}')!", kodtarKod, Sakkora.IRAT_HATASA_UGYINTEZESRE));
        }

        return result;
    }

    /// <summary>
    /// IsSakkoraUresVagyNullas
    /// </summary>
    /// <param name="sakkoraStatusz"></param>
    /// <returns></returns>
    private bool IsSakkoraUresVagyNullas(string sakkoraStatusz)
    {
        if (string.IsNullOrEmpty(sakkoraStatusz)
             || string.Equals(sakkoraStatusz, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE.ToString(), StringComparison.CurrentCultureIgnoreCase)
             || string.Equals(sakkoraStatusz, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS.ToString(), StringComparison.CurrentCultureIgnoreCase)
             || string.Equals(sakkoraStatusz, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE.ToString(), StringComparison.CurrentCultureIgnoreCase)
             || string.Equals(sakkoraStatusz, KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP.ToString(), StringComparison.CurrentCultureIgnoreCase)
             )
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// OsszesAlszamNullasSakkoras
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugy"></param>
    /// <param name="irat"></param>
    /// <returns></returns>
    private bool OsszesAlszamNullasSakkoras(ExecParam execParam, EREC_UgyUgyiratok ugy, EREC_IraIratok irat)
    {
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch src = new EREC_IraIratokSearch();

        src.Ugyirat_Id.Value = ugy.Id;
        src.Ugyirat_Id.Operator = Query.Operators.equals;

        Result result = service.GetAll(execParam.Clone(), src);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            return false;
        }

        if (result.Ds.Tables[0].Rows.Count < 1)
        {
            return true;
        }

        string sakkoraValue = null;
        foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
        {
            sakkoraValue = row["IratHatasaUgyintezesre"].ToString();

            if (sakkoraValue != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE
                   &&
                sakkoraValue != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS
                   &&
                sakkoraValue != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE
                   &&
                sakkoraValue != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP)
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Ugyirat idoegysege
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    private string GetUgyiratIdoEgyseg(ExecParam execParam, string ugyiratId)
    {
        EREC_UgyUgyiratok ugyiratObj = GetUgyirat(execParam.Clone(), ugyiratId);

        string idoEgyseg = null;

        if (!string.IsNullOrEmpty(ugyiratObj.IratMetadefinicio_Id))
        {
            EREC_IratMetaDefinicioService serviceIratMetaDef = new EREC_IratMetaDefinicioService(this.dataContext);

            var execParamGetIratMetaDef = execParam.Clone();
            execParamGetIratMetaDef.Record_Id = ugyiratObj.IratMetadefinicio_Id;

            var resultIratMetaDef = serviceIratMetaDef.Get(execParamGetIratMetaDef);
            if (resultIratMetaDef.IsError)
            {
                throw new ResultException(resultIratMetaDef);
            }

            EREC_IratMetaDefinicio iratMetaDef = (EREC_IratMetaDefinicio)resultIratMetaDef.Record;
            idoEgyseg = iratMetaDef.Idoegyseg;
        }
        if (string.IsNullOrEmpty(idoEgyseg))
        {
            idoEgyseg = Rendszerparameterek.Get(execParam.Clone(), Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG) ?? KodTarak.IDOEGYSEG.Nap;
        }
        return idoEgyseg;
    }

    /// <summary>
    /// GetDateTimeNextHourWithZeroMinutesAsString
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    private string GetDateTimeNextHourWithZeroMinutesAsString(string date)
    {
        return GetDateTimeNextHourWithZeroMinutes(date).ToString();
    }

    /// <summary>
    /// GetDateTimeNextHourWithZeroMinutes
    /// </summary>
    /// <param name="dateString"></param>
    /// <returns></returns>
    private DateTime GetDateTimeNextHourWithZeroMinutes(string dateString)
    {
        DateTime date;
        if (string.IsNullOrEmpty(dateString))
        {
            date = DateTime.Now.AddHours(1);
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, 0, 0);
        }

        if (DateTime.TryParse(dateString, out date))
        {
            date = date.AddHours(1);
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, 0, 0);
        }

        date = DateTime.Now.AddHours(1);
        return new DateTime(date.Year, date.Month, date.Day, date.Hour, 0, 0);
    }

    /// <summary>
    /// SetUgySakkoraAllapotaV2
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    /// <param name="ugy"></param>
    private void SetUgySakkoraAllapotaV2(ExecParam execParam, ref EREC_IraIratok irat, ref EREC_UgyUgyiratok ugy)
    {
        if (irat == null || ugy == null)
        {
            return;
        }

        try
        {
            Sakkora.SetUgySakkoraAllapotaFromIrat(irat.IratHatasaUgyintezesre, ref ugy);
            if (IsSakkoraUresVagyNullas(irat.IratHatasaUgyintezesre))
            {
                if (OsszesAlszamNullasSakkoras(execParam, ugy, irat))
                {
                    UpdateUgyirat(execParam, ugy);

                    SakkoraReset(execParam.Clone(), ugy);
                    Logger.Debug("Sakkora reset: " + ugy.Azonosito);
                    return;
                }
                else
                {
                    string tmpSakkora = GetUgyAlszamokUtolsoNemNullasSakkoraErteke(execParam, ugy.Id);

                    if (ugy.SakkoraAllapot != tmpSakkora)
                    {
                        Sakkora.SetUgySakkoraAllapotaFromIrat(tmpSakkora, ref ugy);
                        Logger.Debug(string.Format("Ugy {0}, SakkoraAllapot:{1}", ugy.Azonosito, ugy.SakkoraAllapot));
                    }
                }
            }
            else
            {
                if (ugy.SakkoraAllapot != irat.IratHatasaUgyintezesre)
                {
                    Sakkora.SetUgySakkoraAllapotaFromIrat(irat.IratHatasaUgyintezesre, ref ugy);
                }

                #region 4-ES VAGY 5-OS SAKKORA ESETEN

                if (
                    irat.IratHatasaUgyintezesre == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Egy
                    ||
                    irat.IratHatasaUgyintezesre == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Negy
                    ||
                    irat.IratHatasaUgyintezesre == KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Ot)
                {
                    SetUgyElteltIdoAllapotToStartProcedure(execParam, ref ugy);
                }

                #endregion 4-ES VAGY 5-OS SAKKORA ESETEN
            }

            #region ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL

            SetUgyHataridoSakkoraAlapjan(execParam, irat);

            #endregion ELSO INDULO ESETEN - UGYIRAT ADATOK FRISSITESE IRATEVAL
        }
        catch (Exception exc)
        {
            Logger.Error("SetUgyiratSakkoraAllapot", exc);
        }
    }

    /// <summary>
    /// Ügyirat és irat sakkóra állapotának alapállapotab tétele
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyiratId"></param>
    /// <returns></returns>
    private bool SakkoraReset(ExecParam execParam, EREC_UgyUgyiratok ugy)
    {
        if (ugy == null)
        {
            return false;
        }

        #region UGY UPDATE

        if (ugy.SakkoraAllapot != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE)
        {
            ugy.SakkoraAllapot = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE;
            ugy.Updated.SakkoraAllapot = true;
            execParam.Record_Id = ugy.Id;

            UpdateUgyirat(execParam.Clone(), ugy);
        }

        #endregion UGY UPDATE

        #region IRAT UPDATE

        EREC_IraIratokService serviceIrat = new EREC_IraIratokService(this.dataContext);
        EREC_IraIratokSearch src = new EREC_IraIratokSearch();

        src.Ugyirat_Id.Value = ugy.Id;
        src.Ugyirat_Id.Operator = Query.Operators.equals;

        src.IratHatasaUgyintezesre.Value = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE;
        src.IratHatasaUgyintezesre.Operator = Query.Operators.notequals;

        Result resultIratAll = serviceIrat.GetAll(execParam.Clone(), src);
        if (!string.IsNullOrEmpty(resultIratAll.ErrorCode))
        {
            return false;
        }

        if (resultIratAll.Ds.Tables[0].Rows.Count < 1)
        {
            return true;
        }

        EREC_IraIratok iratTemp;
        ExecParam execParamIratGet = execParam.Clone();

        foreach (System.Data.DataRow row in resultIratAll.Ds.Tables[0].Rows)
        {
            execParamIratGet.Record_Id = row["Id"].ToString();
            iratTemp = new EREC_IraIratok();
            Utility.LoadBusinessDocumentFromDataRow(iratTemp, row);
            iratTemp.Updated.SetValueAll(false);
            iratTemp.Base.Updated.SetValueAll(false);
            iratTemp.Base.Updated.Ver = true;

            if (iratTemp.IratHatasaUgyintezesre != KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE)
            {
                iratTemp.IratHatasaUgyintezesre = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE;
                iratTemp.Updated.IratHatasaUgyintezesre = true;

                UpdateIrat(execParam, iratTemp);
            }
        }

        #endregion IRAT UPDATE

        return true;
    }

    #endregion HELP

    #region ELTELT IDO

    /// <summary>
    /// Eltelt idő állapot beállítása
    /// Sakkora
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="iratId"></param>
    private void SetUgyElteltIdoV2(ExecParam execParam, ref EREC_IraIratok irat, ref EREC_UgyUgyiratok ugy)
    {
        if (irat == null)
        {
            return;
        }

        if (string.IsNullOrEmpty(ugy.ElteltidoAllapot))
        {
            SetUgyiratElteltIdoDefault(execParam, ref ugy);
        }
        else if (ugy.ElteltidoAllapot == KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.NEW || Sakkora.IsIratHatasaStart(execParam, ugy.SakkoraAllapot))
        {
            SetUgyElteltIdoAllapotToStartProcedure(execParam, ref ugy);
        }
    }

    /// <summary>
    /// SetUgyElteltIdoAllapotToStart
    /// </summary>
    /// <param name="ugyirat"></param>
    private void SetUgyElteltIdoAllapotToStart(ref EREC_UgyUgyiratok ugyirat)
    {
        ugyirat.ElteltidoAllapot = KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START;
        ugyirat.Updated.ElteltidoAllapot = true;

        ugyirat.ElteltIdoUtolsoModositas = Sakkora.GetTodayLastMinuteAsString(); Sakkora.GetTodayLastMinuteAsString();
        ugyirat.Updated.ElteltIdoUtolsoModositas = true;
    }

    /// <summary>
    /// SetUgyElteltIdoAllapotToStartProcedure
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugy"></param>
    /// <returns></returns>
    private bool SetUgyElteltIdoAllapotToStartProcedure(ExecParam execParam, ref EREC_UgyUgyiratok ugy)
    {
        if (ugy == null || ugy.ElteltidoAllapot == KodTarak.UGYIRAT_ELTELT_IDO_ALLAPOT.START)
            return false;

        SetUgyElteltIdoAllapotToStart(ref ugy);
        Logger.Debug(string.Format("ElteltIdoAllapotUjraAktivalasPostazasUtan.Induk: AZ={0}", ugy.Azonosito));
        return true;
    }

    #endregion ELTELT IDO

    #endregion HELPER

    /// <summary>
    /// Set irat eljarasi szakasz to 1, if ugyirat hatosagi.
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="irat"></param>
    /// <param name="ugy"></param>
    /// <returns></returns>
    private void SetEljarasiSzakaszDefault(ExecParam execParam, ref EREC_IraIratok irat, EREC_UgyUgyiratok ugy)
    {
        if (irat != null && ugy != null && string.IsNullOrEmpty(irat.EljarasiSzakasz) && Sakkora.IsUgyFajtaHatosagi(execParam.Clone(), ugy.Ugy_Fajtaja))
        {
            irat.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ_FOK.I;
            irat.Updated.EljarasiSzakasz = true;
            Result result = UpdateIrat(execParam.Clone(), irat);
            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                if (result.Record != null && result.Record is EREC_IraIratok)
                {
                    irat = (EREC_IraIratok)result.Record;
                }
                else
                {
                    irat = GetIrat(execParam, irat.Id);
                }
            }
        }
    }
}