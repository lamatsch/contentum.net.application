﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;
using System.Data;
using Contentum.eDocument.Service;
using Contentum.eAdmin.Service;
using eSign;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NPOI.XWPF.UserModel;

/// <summary>
/// The entry point for the application.
/// </summary>
/// <param name="args">A list of command line arguments</param> 
[WebService(Namespace = "Contentum.eScanWebService", Description = "TODO: WebService-nek a leirasa!!")]
//[WebService(Namespace = "http://tempuri.org/", Description="TODO: WebService-nek a leirasa!!")]
//[WebServiceBinding(ConformsTo = WsiProfiles.None)] 
[SoapRpcService(Use = System.Web.Services.Description.SoapBindingUse.Literal, RoutingStyle = SoapServiceRoutingStyle.SoapAction)]
public class ScanService : System.Web.Services.WebService
{
    //  starteamhoz:
    public const string Header = "";
    public const string Version = "";
    private DataContext dataContext;

    public ScanService()
    {
        this.dataContext = new DataContext(this.Application);

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        Logger.Debug(String.Format("Version: {0}", Version));
    }

    private string ReadFile(string p_fn)
    {
        string rv = "";
        FileStream sdxmfile = new FileStream(p_fn, FileMode.Open, FileAccess.Read);
        StreamReader sr = new StreamReader(sdxmfile, System.Text.Encoding.UTF8, false, 1024);
        rv = sr.ReadToEnd();
        sr.Close();
        sdxmfile.Close();
        return rv;
    }

    private void SaveFile(string p_fn, string p_content)
    {
        FileStream file = new FileStream(p_fn, FileMode.Create, FileAccess.Write);
        StreamWriter sw = new StreamWriter(file);
        sw.Write(p_content);
        sw.Close();
        file.Close();
    }

    /// <summary>
    /// File alairasa.
    /// </summary>
    /// <param name="FileName">File neve. Uttal is lehet.</param>
    /// <param name="ConfigId">Config ide.</param>
    /// <param name="Contents">Tartalom string.</param>
    /// <returns>Az sdxm neve.</returns>
    private String Sign(string FileName, int ConfigId, String Contents)
    {
        String result = "";
        int idx = FileName.LastIndexOf("\\");
        string bfn = (idx == -1) ? FileName : FileName.Substring(idx + 1, FileName.Length - idx - 1);
        string contentToSign = "";
        contentToSign += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        contentToSign += "<xml>";
        contentToSign += "<config_item>" + ConfigId + "</config_item>";
        contentToSign += "<filename>" + bfn + "</filename>";
        contentToSign += "<signdata>" + Contents + "</signdata>";
        contentToSign += "</xml>";
        eSign.SDXM.Url = UI.GetAppSetting("eASZBusinessServiceUrl");
        eSign.common.SignResult sr = eSign.SDXM.Sign(contentToSign);
        this.SaveFile(FileName + ".sdxm", eSign.Utils.Strings.DecodeUtf8From64(sr.Result));
        result = FileName + ".sdxm";
        Logger.Debug("Az aláírt tartalom kimentése " + FileName + ".sdxm" + " névvel");
        return result;
    }

    private String Sign(string FileName, int ConfigId, byte[] Contents)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        String strContents = encoder.GetString(Contents);
        return this.Sign(FileName, ConfigId, strContents);
    }

    private String Sign(string FileName, int ConfigId)
    {
        string signdata = eSign.Utils.Strings.EncodeUtf8To64(this.ReadFile(FileName));
        return this.Sign(FileName, ConfigId, signdata);
    }

    [WebMethod]
    [SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Wrapped)]
    public Result Teszt()
    {
        Result res = new Result();
        res.Record = Convert.ToString(this.IsUserValid("axis\\varsanyi.peter"));

        //  2009.06.19
        //  szkennelés teszt
        //UserName: varsanyi.peter;
        //Domain: AXIS;
        //Feladás típusa: B;
        //Azonosító: 1000005129642;     /* FPH027 /14 - 4 /2009 */
        //Oldalszám: 2;
        //Megjegyzés: ;
        //Aláírás: False;
        //Ocr: 1;
        //Fájlnév: 01-00002513-0001-1000006538625.tif

        //byte[] contents = { (byte)'a', (byte)'b' };

        //res.Uid = this.LoadDocument("varsanyi.peter", "AXIS", "B", "1000005129217", "2", "", "False", "1", "01-00002513-0001-1000006538324.tif", contents);

        //if (String.IsNullOrEmpty(res.Uid))
        //    res.Uid = "OKKER";
        //else
        //    Logger.Debug(String.Format("BAMBULA: {0}", res.Record));

        return res;
    }

    /// <summary>
    /// Scannelés ezzel állapítja meg a felhasználó szerepkörét es a valódiságát.
    /// Ha nincs felhasználó vagy nincs szerepköre: 0.
    /// Ha mindekét szerepköre megvan, a nagyobbat, az admint adja visssza.
    /// </summary>
    /// <param name="UserName">username domainnnel.</param>
    /// <returns>0: nem scan user vagy nem valid vagy hiba volt; 1: scan-admin; 2: scan-oparator</returns>
    /// 
    [WebMethod]
    [SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Wrapped)]
    public int IsUserValid(string UserName)
    {
        Logger.Debug(String.Format("IsUserValid indul. Param: {0}", UserName));

        #region kapott juzername valos-e

        String userId = String.Empty;

        try
        {
            //Contentum.eAdmin.Service.AuthenticationService asd = Contentum.eUtility.eAdminService.ServiceFactory.GetAuthenticationService(bs);

            Logger.Debug("Service Factory meghataraozasa.");
            AuthenticationService asd = eAdminService.ServiceFactory.GetAuthenticationService();

            ExecParam ex = new ExecParam();

            Logger.Debug(String.Format("Login obj letrehozasa. Usernev: {0} ", UserName));

            Login loggin = new Login();
            loggin.FelhasznaloNev = UserName;
            loggin.Tipus = "Windows";

            Logger.Debug("Login method futtatasa.");
            Result ret = asd.Login(ex, loggin);

            if (!String.IsNullOrEmpty(ret.ErrorCode))
            {
                Logger.Error(String.Format("Hiba a felhasznalo azonositaskor!\nErrorCode: {0}\nErrorMessage: {1}", ret.ErrorCode, ret.ErrorMessage));
                return 0;
            }

            if (String.IsNullOrEmpty(ret.Uid))
            {
                Logger.Error(String.Format("Hiba a felhasznalo azonositaskor! Ures (result.Uid) user id!"));
                return 0;
            }

            Logger.Debug(String.Format("Kapott id: {0}", ret.Uid));
            userId = ret.Uid;

        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("EXCEPTION a juzer id meghatarozasakor!!!"));
            Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            return 0;
        }


        #endregion

        #region valos juzernek milyen scannelesi szerepkore van

        int retVal = 0;

        try
        {
            Logger.Debug(String.Format("Szerepkorok meghataraozasa. User id: {0}", userId));
            KRT_SzerepkorokService szkserv = eAdminService.ServiceFactory.GetKRT_SzerepkorokService();

            ExecParam ex = new ExecParam();
            ex.Felhasznalo_Id = userId;

            KRT_Felhasznalok felh = new KRT_Felhasznalok();
            felh.Id = userId;

            KRT_Felhasznalo_SzerepkorSearch felhSzerepkorSearch = new KRT_Felhasznalo_SzerepkorSearch();

            felhSzerepkorSearch.ErvVege.Value = Query.SQLFunction.getdate;
            felhSzerepkorSearch.ErvVege.Operator = Query.Operators.greaterorequal;
            felhSzerepkorSearch.ErvVege.Group = "100";
            felhSzerepkorSearch.ErvVege.GroupOperator = Query.Operators.and;

            Logger.Debug(String.Format("szkserv.GetAllByFelhasznalo meghivasa elott."));
            Result resSzk = szkserv.GetAllByFelhasznalo(ex, felh, felhSzerepkorSearch);

            Logger.Debug(String.Format("szkserv.GetAllByFelhasznalo lefutott."));

            if (!String.IsNullOrEmpty(resSzk.ErrorCode))
            {
                Logger.Error(String.Format("Hiba a szerepkorok lekredezesenel!\nErrorCode: {0}\nErrorMessage: {1}", resSzk.ErrorCode, resSzk.ErrorMessage));
                return 0;
            }

            Logger.Debug(String.Format("while ciklus elott."));

            //
            //  szerepkorok feldolgozasa
            int i = 0;
            while (i < resSzk.Ds.Tables[0].Rows.Count)
            {
                if ("FPH_SZKENNELORENDSZERGAZDA".Equals(Convert.ToString(resSzk.Ds.Tables[0].Rows[i]["Szerepkor_Nev"]).ToUpper()))
                    retVal = 1;

                if ("FPH_SZKENNELOOPERATOR".Equals(Convert.ToString(resSzk.Ds.Tables[0].Rows[i]["Szerepkor_Nev"]).ToUpper()) && retVal == 0)
                    retVal = 2;

                i++;
            } //while i

        }
        catch (Exception exc)
        {
            Logger.Error(String.Format("EXCEPTION a juzer szerepkorok meghatarozasakor!!!"));
            Logger.Error(String.Format("Message: {0}\nStackTrace: {1}", exc.Message, exc.StackTrace));
            return 0;
        }

        #endregion

        return retVal;
    }

    /// <summary>
    /// Scannelésből kapott file feltöltése.
    /// 
    /// A fileParams text 'file' valtozoi:
    /// 
    ///    $FS_DOC=01-00000166-0007-1000005120326
    ///    $FS_PAGE=01-00000166-0007-1000005120326.tif
    ///    $FS_BARKOD=1231231230123
    ///    $FS_ERKNUM=1111111111111111111111111
    ///    $FS_IRATNUM=1111111111111111111111111
    ///    $FS_DOKMEGJ=hdaslkhdaslkdasjlkdasjdasd
    ///    $FS_UZENET=ashdaséda
    ///    $FS_PAGECOUNT=12
    ///    $FS_OCR=9
    ///    $FS_ALAIRAS=0
    ///    $FS_USERNAME=axis\varsanyi.peter
    ///    
    /// Soronként egy-egy; a sort enter zárja le; a stringből a GetFSErtek függvénnyel lehet kivenni értekeket.
    /// 
    /// A vonalkódos ág van használatban, annak kell kitöltve lenni!
    /// 
    /// </summary>
    /// <param name="fileContent">byte tömb - a file tartalma</param>
    /// <param name="fileParams">string file - egyeb parameterek; $FS_VALTOZONEVE=ertek soronkent.</param>
    /// <returns>"OK" string, ha rendben; egyébként hibaüzenet.</returns>
    [WebMethod]
    [SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Wrapped)]
    public String LoadDocument(byte[] fileContent, String fileParams)
    {
        #region valtozok es ellenorzesek

        Logger.Debug(String.Format("ScanService.LoadDocument indul. Kapott fileParams parameter: {0}", fileParams));

        //
        //  felhasznalo nevenek ellenorzese

        String userName = this.GetFSErtek(fileParams, "$FS_USERNAME");

        if (String.IsNullOrEmpty(userName))
        {
            return "ERROR: Üres vagy nem létezk a fileParams paraméterben kapott $FS_USERNAME változó!";
        }

        //
        //  a domain meghatarozasa

        String userDomain = (userName.IndexOf("//") != -1) ? String.Empty : userName.Substring(0, userName.IndexOf("\\")).Trim();

        if (String.IsNullOrEmpty(userDomain))
        {
            //return "ERROR: Nem állapítható meg a kapott felhasználó (domain) tartománya!";

            // Ez nem hiba, mert lehet helyi (domanintelen) juzer is. csak megjegyezzuk.
            Logger.Debug(String.Format("A kapott domain ures! (ebbol az ertekbol probaltuk kicsiholni: {0})", userName));
        }
        else
        {
            //
            // az usernamerol leszedjuk a domaint

            userName = userName.Substring(userName.IndexOf("\\") + 1).Trim();
        }

        ///..... TODO: feladattipust konstans B = vonalkod alapu azonositas

        String feladatTipusa = "B";
        String azonosito = String.Empty;

        String oldalszam = this.GetFSErtek(fileParams, "$FS_PAGECOUNT");
        String megjegyzes = this.GetFSErtek(fileParams, "$FS_DOKMEGJ");
        String alairas = this.GetFSErtek(fileParams, "$FS_ALAIRAS");
        String fileName = this.GetFSErtek(fileParams, "$FS_PAGE");
        String ocr = this.GetFSErtek(fileParams, "$FS_OCR");

        azonosito = this.GetFSErtek(fileParams, "$FS_BARKOD");
        String tmp_erkszam = this.GetFSErtek(fileParams, "$FS_ERKNUM");
        String tmp_iratszam = this.GetFSErtek(fileParams, "$FS_IRATNUM");

        #endregion

        //
        //  az eredeti LoadDocument meghivasa

        // CR#2343 (EB 2011.02.07): nyitunk egy Connection-t, másképp az eseményservice hibára fut
        // és nem ad vissza eseményrekordot
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            return this.LoadDocument(userName, userDomain, feladatTipusa, azonosito, oldalszam, megjegyzes, alairas, ocr, fileName, fileContent);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
    }

    // CR 3051 DocSoft Scannelés integráció
    // nekrisz
    [WebMethod]
    [SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Wrapped)]
    //[SoapRpcMethod(Use = System.Web.Services.Description.SoapBindingUse.Default)]
    public string LoadDocumentDS(String UserName
        , String Vonalkod
        , String Oldalszam
        , String Megjegyzes
        , String FileName
        , byte[] Content
        , String AlairtFileName
        , byte[] AlairtContent)
    {
        #region valtozok es ellenorzesek

        Logger.Debug(String.Format("ScanService.LoadDocumentDS indul. Kapott parameterek: Felh.név: {0}, Vonalkód: {1}, Oldalszám: {2}, Megjegyzés: {3}, FileName:{4}", UserName, Vonalkod, Oldalszam, Megjegyzes, FileName));

        #region DOMAIN ES USERNEV

        if (String.IsNullOrEmpty(UserName))
            return "ERROR: Felhasználónév üres!";

        string userName = null;
        string userDomain = string.Empty;
        string[] resArray = null;
        if (UserName.Contains("/") || UserName.Contains(@"\"))
        {
            if (UserName.Contains("/"))
                resArray = UserName.Split(new string[] { @"/" }, StringSplitOptions.RemoveEmptyEntries);
            else if (UserName.Contains(@"\"))
                resArray = UserName.Split(new string[] { @"\" }, StringSplitOptions.RemoveEmptyEntries);

            if (resArray.Length == 1)
                userName = resArray[0];
            else if (resArray.Length == 2)
            {
                userDomain = resArray[0];
                userName = resArray[1];
            }
        }
        else
        {
            userName = UserName.Trim();
        }

        Logger.Debug(String.Format("A kapott domain {0}, kapott felhasználónév: {1})", userDomain, userName));
        #endregion

        //String userDomain = (userName.IndexOf("//") != -1) ? String.Empty : userName.Substring(0, userName.IndexOf("\\")).Trim();

        //if (String.IsNullOrEmpty(userDomain))
        //{
        //    //return "ERROR: Nem állapítható meg a kapott felhasználó (domain) tartománya!";

        //    // Ez nem hiba, mert lehet helyi (domanintelen) user is. csak megjegyezzuk.
        //    Logger.Debug(String.Format("A kapott domain ures! (kapott felhasználónév: {0})", UserName));
        //}
        //else
        //{

        //    // az usernamerol leszedjuk a domaint
        //    userName = userName.Substring(userName.IndexOf("\\") + 1).Trim();
        //}

        ///..... TODO: feladattipust konstans B = vonalkod alapu azonositas
        String feladatTipusa = "B";
        String alairas = String.Empty;   //Nincs aláírás
        String ocr = "0";                //Nincs OCR

        #endregion

        //  az eredeti LoadDocument meghivasa

        // CR#2343 (EB 2011.02.07): nyitunk egy Connection-t, másképp az eseményservice hibára fut
        // és nem ad vissza eseményrekordot
        bool isConnectionOpenHere = false;

        string result = "";
        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = this.LoadDocument(userName, userDomain, feladatTipusa, Vonalkod, Oldalszam, Megjegyzes, alairas, ocr, FileName, Content);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        string alairtFileName = AlairtFileName;
        string alairtMegj = Megjegyzes + " aláírt fájl";

        if ((result == "OK") && (!String.IsNullOrEmpty(alairtFileName)))
        {
            try
            {
                isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

                result = this.LoadDocument(userName, userDomain, feladatTipusa, Vonalkod, Oldalszam, alairtMegj, alairas, ocr, alairtFileName, AlairtContent);
            }
            finally
            {
                dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
            }
        }
        return result;
    }

    /// <summary>
    /// Szkennelt dokumentumok automatizált feltöltése vonalkód alapján
    /// BLG 2445
    /// </summary>
    /// <param name="userName">Felhasználónév</param>
    /// <param name="domain">Domain</param>
    /// <returns></returns>
    [WebMethod]
    [SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Wrapped)]
    public string LoadDocumentFromFileShare(string userName, string domain)
    {
        Logger.Debug("ScanService.LoadDocumentFromFileShare paraméterek: userName=" + userName + ";domain=" + domain);

        bool result = true;

        ExecParam exParam = new ExecParam();
        if (string.IsNullOrEmpty(userName))
        {
            string scanUserName = Rendszerparameterek.Get(exParam, Rendszerparameterek.SCAN_USER_NAME);

            Logger.Debug("ScanService.LoadDocumentFromFileShare userName paraméter üres, " + Rendszerparameterek.SCAN_USER_NAME + " rendszerparaméterből feltöltve: " + scanUserName);

            if (string.IsNullOrEmpty(scanUserName))
            {
                Logger.Error("ScanService.LoadDocumentFromFileShare hiányzó Rendszerparaméter:" + Rendszerparameterek.SCAN_USER_NAME);
                return "Hiba";
            }

            string[] scanUserNameParts = scanUserName.Split("\\".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (scanUserName.Length > 1)
            {
                domain = scanUserNameParts[0];
                userName = scanUserNameParts[1];
            }
            else if (scanUserNameParts.Length > 0)
            {
                userName = scanUserNameParts[0];
            }

            if (!Login(exParam, scanUserName))
            {
                Logger.Error("ScanService.LoadDocumentFromFileShare nem megfelelő felhasználó név, username=" + userName + ";domain=" + domain);
                return "Hiba";
            }
        }

        string archivePath = Rendszerparameterek.Get(exParam, Rendszerparameterek.SCAN_ARCHIVE_PATH);
        bool deleteFiles = string.IsNullOrEmpty(archivePath);
        bool archiveInPlace = !Directory.Exists(archivePath);
        if (!archiveInPlace)
        {
            string dummyFile = "dummy.txt";
            string dummyPath = Path.Combine(archivePath, dummyFile);
            try
            {
                File.WriteAllText(dummyPath, "");
                File.Delete(dummyPath);
            }
            catch
            {
                Logger.Warn("ScanService.LoadDocumentFromFileShare nem lehet írni a megadott archive útvonalra: " + archiveInPlace);
                Logger.Warn("ScanService.LoadDocumentFromFileShare az archiválás helyi könyvtárban történik!");
                archiveInPlace = true;
            }
        }

        string inputDelayString = Rendszerparameterek.Get(exParam, Rendszerparameterek.SCAN_INPUT_DELAY);
        int inputDelay = 0;
        int.TryParse(inputDelayString, out inputDelay);

        StringBuilder sbErrors = new StringBuilder();
        StringBuilder ldErrors = new StringBuilder();

        string inputPaths = Rendszerparameterek.Get(exParam, Rendszerparameterek.SCAN_INPUT_PATH);
        IEnumerable<string> inputPathList = inputPaths.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        foreach (string inputPath in inputPathList)
            if (!LoadDocumentFromDirectory(userName, domain, inputPath, archivePath, archiveInPlace, inputDelay, deleteFiles, sbErrors, ldErrors))
                result = false;

        if (sbErrors.Length > 0 || ldErrors.Length > 0)
        {
            result = false;
            using (EmailService mailService = eAdminService.ServiceFactory.GetEmailService())
            {
                string message = string.Empty;
                if (sbErrors.Length > 0)
                    message += sbErrors.ToString();
                if (ldErrors.Length > 0)
                    message += ldErrors.ToString().Trim('\n');
                string from_address = Rendszerparameterek.Get(exParam, Rendszerparameterek.HIBABEJELENTES_EMAIL_CIM);
                string to_address = Rendszerparameterek.Get(exParam, "SCAN_ERROR_EMAIL");
                bool sent = mailService.SendEmail(exParam, from_address, new string[] { to_address }, string.Format("Hiba a dokumentum szkennelési folyamatban! {0}", DateTime.Now), null, null, false, message);
                if (!sent)
                    Logger.Error(string.Format("Az e-mail küldés sikertelen. From: {0}, To: {1}, Message: {2}", from_address, to_address, message));
            }
        }

        Logger.Debug("ScanService.LoadDocumentFromFileShare visszatérési érték: " + (result ? "OK" : ldErrors.ToString()));
        return result ? "OK" : ldErrors.ToString();
    }

    private bool TryCreateDirectory(string path)
    {
        try
        {
            Directory.CreateDirectory(path);
            return true;
        }
        catch
        {
            Logger.Error("ScanService.TryCreateDirectory Nem sikerült létrehozni a könyvtárat: " + path);
            return false;
        }
    }

    private void TryDeleteScannedFile(string filePath, StringBuilder sbErrors)
    {
        try
        {
            File.Delete(filePath);
        }
        catch (Exception ex)
        {
            string message = string.Format("A fájl törlése sikertelen [Fájl: {0}]:", filePath, ex.Message);
            sbErrors.AppendLine(message);
            Logger.Error("ScanService.TryDeleteScannedFile " + message);
        }
    }

    private void TryMoveScannedFile(string filePath, string destinationDir, StringBuilder sbErrors)
    {
        //BUG 2608 - adott idöpont alapján mappába helyezés, ha nincs ilyen mappa létre kell hozni
        string subdir = DateTime.Now.ToString("yyyyMMddHHmm");
        string destinationDir_new = Path.Combine(destinationDir, subdir);
        if (!Directory.Exists(destinationDir_new))
        {           
            try
            {
                Directory.CreateDirectory(destinationDir_new);
            }
            catch (Exception ex)
            {
                string message = string.Format("Alkönyvtár létrehozása sikertelen [mappa: {0}]: {1}", destinationDir_new, ex.Message);
                sbErrors.AppendLine(message);
                Logger.Error("ScanService.TryMoveScannedFile " + message);
                destinationDir_new = destinationDir;
            }
        }

        try
        {
               File.Move(filePath, Path.Combine(destinationDir_new, Path.GetFileName(filePath)));
        }
        catch (Exception ex)
        {
            string message = string.Format("A fájl áthelyezése sikertelen [Fájl: {0} Cél mappa: {1}]: {2}", filePath, destinationDir_new, ex.Message);
            sbErrors.AppendLine(message);
            Logger.Error("ScanService.TryMoveScannedFile " + message);
        }
    }

    private bool LoadDocumentFromDirectory(string userName, string domain, string inputPath, string archivePath, bool archiveInPlace, int inputDelay, bool deleteFiles, StringBuilder sbErrors, StringBuilder ldErrors)
    {
        Logger.Debug("ScanService.LoadDocumentFromFileShare paraméterek: userName=" + userName + ";domain=" + domain + ";inputPath=" + inputPath + ";archivePath=" + archivePath + ";archiveInPlace=" + archiveInPlace + ";deleteFiles=" + deleteFiles);

        string inPlaceArchiveDirectory = "archive";
        string errorDirectory = "ERROR";
        StringBuilder sbErrorsInternal = new StringBuilder();

        if (!Directory.Exists(inputPath))
        {
            Logger.Error("ScanService.LoadDocumentFromDirectory a szkennelt fájlokat tartalmazó könyvtár nem létezik: " + inputPath);
            return false;
        }

        string inPlaceArchivePath = Path.Combine(inputPath, inPlaceArchiveDirectory);
        if (!Directory.Exists(inPlaceArchivePath) && !TryCreateDirectory(inPlaceArchivePath))
        {
            Logger.Error("ScanService.LoadDocumentFromDirectory az archivált fájlokat tartalmazó könyvtár nem létezik: " + inPlaceArchivePath);
            return false;
        }

        string errorPath = Path.Combine(inputPath, errorDirectory);
        if (!Directory.Exists(errorPath) && !TryCreateDirectory(errorPath))
        {
            Logger.Error("ScanService.LoadDocumentFromDirectory a hibás fájlokat tartalmazó könyvtár nem létezik: " + errorPath);
            return false;
        }

        string errorLogPath = Path.Combine(errorPath, "error.log");

        bool result = true;
        IEnumerable<string> files = Directory.GetFiles(inputPath);

        if (files.Count() > 0)
        {
            Logger.Info("ScanService.LoadDocumentFromDirectory rendelkezésre álló fájlok száma: " + files.Count().ToString());
        }

        int processed_file_count = 0;
        int error_file_count = 0;

        foreach (string file in files)
        {
            TimeSpan writeTimeSpan = DateTime.Now - File.GetLastWriteTime(file);
            if (writeTimeSpan.TotalSeconds < inputDelay)
                continue;

            string fileName = Path.GetFileName(file);
            byte[] content = File.ReadAllBytes(file);
            string loadResult = string.Empty;
            try
            {
                loadResult = LoadDocument(userName, domain, "B", Path.GetFileNameWithoutExtension(file), "1", "", "", "", fileName, content);
            }
            catch (Exception e)
            {

            }
            
            if (loadResult.Equals("ok", StringComparison.OrdinalIgnoreCase))
            {
                processed_file_count++;

                Logger.Info("ScanService.LoadDocumentFromDirectory sikeres fájl betöltés: " + fileName);

                if (deleteFiles)
                    TryDeleteScannedFile(file, sbErrorsInternal);
                else if (archiveInPlace)
                    TryMoveScannedFile(file, inPlaceArchivePath, sbErrorsInternal);
                else
                    TryMoveScannedFile(file, archivePath, sbErrorsInternal);
            }
            else
            {
                error_file_count++;

                Logger.Error("ScanService.LoadDocumentFromDirectory sikertelen fájl betöltés: " + fileName);

                result = false;
                TryMoveScannedFile(file, errorPath, sbErrorsInternal);
                string errorLog = Path.Combine(errorPath, "error.log");
                bool errorLogExists = File.Exists(errorLog);
                string message = string.Format("A fájl betöltése sikertelen [Fájl: {0}, Hiba: {1}]", fileName, loadResult);
                File.AppendAllText(errorLog, errorLogExists ? string.Format("{0}{1} {2}", Environment.NewLine, DateTime.Now, message) : string.Format("{0} {1}", DateTime.Now, message), Encoding.UTF8);
                ldErrors.AppendFormat("Az alábbi fájlokat nem sikerült betölteni (a lista elérhetö az alábbi URL-en is: {0} ):{1}", errorLogPath, Environment.NewLine);
                ldErrors.AppendLine(message);
                Logger.Error("ScanService.LoadDocumentFromDirectory " + message);
            }
        }

        if (sbErrorsInternal.Length > 0)
        {
            sbErrors.AppendFormat("A SCAN_INPUT_PATH rendszerparaméterben (értéke: {0}) meghatározott mappában lévö alábbi fájlokat nem sikerült betölteni a rendszerbe, vagy nem sikerült törölni/áthelyezni őket!{1}", inputPath, Environment.NewLine);
            sbErrors.AppendLine("Az alábbi fájlokat nem sikerült áthelyezni/törölni:");
            sbErrors.Append(sbErrorsInternal.ToString());
            Logger.Error("ScanService.LoadDocumentFromDirectory az alábbi fájlokat nem sikerült áthelyezni/törölni: " + sbErrorsInternal.ToString());
        }

        if (error_file_count > 0)
        {
            Logger.Warn("ScanService.LoadDocumentFromDirectory sikertelen file betöltések száma: " + error_file_count.ToString());
        }

        if (files.Count() > 0)
        { 
            Logger.Info("ScanService.LoadDocumentFromDirectory sikeres file betöltések száma: " + processed_file_count.ToString());
        }

        Logger.Debug("ScanService.LoadDocumentFromDirectory visszatérési érték: " + result.ToString());

        return result;
    }

    //[WebMethod]
    //[SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Wrapped)]
    //[SoapRpcMethod(Use = System.Web.Services.Description.SoapBindingUse.Default)]
    private string LoadDocument(String UserName
        , String Domain
        , String Feladastipusa
        , String Azonosito
        , String Oldalszam
        , String Megjegyzes
        , String Alairas
        , String Ocr
        , String FileName
        , byte[] Content)
    {
        Logger.Debug("ScanService.LoadDocument paraméterek: " + "UserName: " + UserName + ";Domain: " + Domain + ";Feladás típusa: " + Feladastipusa + ";Azonosító: " + Azonosito + ";Oldalszám: " + Oldalszam + ";Megjegyzés: " + Megjegyzes + ";Aláírás: " + Alairas + ";Ocr: " + Ocr + ";Fájlnév: " + FileName);
        String Erkeztetoszam = "", Iktatoszam = "", Vonalkod = "";
        String KuldemenyId = "", MellekletId = string.Empty;
        String IratId = "";
        //kommentbe: Peter 2008.08.21
        //String FileFullName = FileSharedDirectoryName + "\\" + FileName;
        String CsatolmanyMegjegyzes = ""; ;
        //Aláírás
        String SignedFileName = "";

        switch (Feladastipusa)
        {
            case "E": Erkeztetoszam = Azonosito; break;
            case "I": Iktatoszam = Azonosito; break;
            case "B": Vonalkod = Azonosito; break;
            default:
                Logger.Error("ScanService.LoadDocument Nem sikerült sem Érkeztetőszámot sem Iktatószámot sem Vonalkódot azonosítani! Feladastipusa=" + Feladastipusa);
                return "Vonalkód azonosítás szükséges!";//Nem sikerült Erkeztetoszamot/Iktatoszamot/Vonalkodot azonosítani(Feladastipusa!=E|I|B)

        }
        //kommentbe: Peter 2008.08.21 - innen
        //if (!System.IO.Directory.Exists(FileSharedDirectoryName))
        //{
        //    Logger.Error("FileSharedDirectory nem található: " + FileSharedDirectoryName + "!"); 
        //    return "FileSharedDirectory nem található: " + FileSharedDirectoryName + "!";

        //}
        //if (!System.IO.File.Exists(FileFullName))
        //{
        //    Logger.Error("File nem található: " + FileFullName + "!"); 
        //    return "File nem található: " + FileFullName + "!";

        //}
        //kommentbe: Peter 2008.08.21 - idaig

        String krtDokId_esemenyKezeleshez = String.Empty;

        ExecParam ExParam = new ExecParam();

        //LZS - BUG_5607
        //Az volt a hiba, hogyha nem Windows-al vagyunk autentikálva a contentumba, akkor a scanservice-be nem autentikál és 
        //a térti képek nem kerülnek feltöltésre. 
        //Megoldás: ki kell kapcsolni ennél a funkciónál az autentikálást (a felhasználó már úgy is be van jelentkezve a contentumba).
        //Pontosabban a beépített SCAN_USER-el authentikálunk, akkor ha nem Windows-os userrel léptünk be.
        String userFullName = (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Domain) ) ? Rendszerparameterek.Get(ExParam, Rendszerparameterek.SCAN_USER_NAME) : string.Format("{0}\\{1}", Domain, UserName);
        Logger.Debug("ScanService.LoadDocument User=" + userFullName);

        if (!Login(ExParam, userFullName))
        {
            Logger.Error("ScanService.LoadDocument Hiba a felhasználó azonosítása során: " + userFullName + "!");
            return "LoadDocument Hiba a felhasználó azonosítása során: " + userFullName + "!";
        }
    

        // Audit Log:
        //// Csak ideiglenesen:
        //ExParam.Page_Id = "9C14E2BE-AB75-DD11-9FBF-0019DB4CF22D";
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExParam, Context, GetType());

        Logger.Debug("ScanService.LoadDocument Autentikáció OK!");

        Contentum.eAdmin.Service.KRT_AlkalmazasokService AlkalmazasokService = eAdminService.ServiceFactory.GetKRT_AlkalmazasokService();

        // TODO: alkalmazást beállítani a Scannelő alkalmazásra 
        #region AlkalmazasId beállítása
        KRT_AlkalmazasokSearch AlkalmazasokSearch = new KRT_AlkalmazasokSearch();
        AlkalmazasokSearch.Kod.Value = "eRecord";
        AlkalmazasokSearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;
        Result resAlk = AlkalmazasokService.GetAll(ExParam, AlkalmazasokSearch);
        if (!String.IsNullOrEmpty(resAlk.ErrorCode))
        {
            Logger.Error("ScanService.LoadDocument Hiba az alkalmazás azonosítása során!");
            return "Hiba az alkalmazás azonosítása során!";
        }
        System.Data.DataSet dsAlk = resAlk.Ds;
        ExParam.Alkalmazas_Id = dsAlk.Tables[0].Rows[0]["Id"].ToString();
        #endregion

        EREC_KuldKuldemenyekService KuldKuldemenyekService = new EREC_KuldKuldemenyekService();
        EREC_IraIratokService IraIratokService = new EREC_IraIratokService();

        if (!string.IsNullOrEmpty(Vonalkod))
        {
            Logger.Info("ScanService.LoadDocument Vonalkód használata, Azonosito=" + Azonosito);
            string BarkodType = String.Empty;
            string ObjId = String.Empty;    // KRT_Barkodok.Obj_Id

            #region FIND RAGSZAM
            string error = null;
            string errorMelleklet = "A megadott vonalkód alapján nem található olyan küldemény, ahol EREC_KuldKuldemenyek.Ragszam = " + Azonosito;
            List<EREC_KuldKuldemenyek> kuldList = FindKuldKuldemenyByRagszam(ExParam, Azonosito, out error);
            if (kuldList != null && kuldList.Count > 0)
            {
                if (kuldList.Count > 1)
                {
                    Logger.Error("ScanService.LoadDocument Küldemény elemet nem sikerült egyértelműen azonosítani a ragszám alapján! Találatok száma: " + kuldList.Count + ";Azonosito=" + Azonosito);
                    return "Küldemény elemet nem sikerült egyértelműen azonosítani a ragszám alapján!";
                }
                else
                {
                    BarkodType = "EREC_KuldKuldemenyek";
                    ObjId = kuldList[0].Id;
                    Logger.Debug("ScanService.LoadDocument EREC_KuldKuldemenyek.Obj_type = " + BarkodType + " EREC_KuldKuldemenyek.Obj_Id = " + ObjId);

                    #region FIND MELLEKLET
                    EREC_Mellekletek foundMelleklet = FindMelleklet(ExParam, kuldList[0].Id, "14", out errorMelleklet);
                    #endregion

                    #region SET FIELDS
                    if (foundMelleklet != null && !string.IsNullOrEmpty(foundMelleklet.Id))
                    {
                        MellekletId = foundMelleklet.Id;
                        IratId = foundMelleklet.IraIrat_Id;
                        CsatolmanyMegjegyzes = foundMelleklet.Megjegyzes;
                        Logger.Debug("ScanService.LoadDocument " + string.Format("MellekletId = {0}  Iratid ={1} ", MellekletId, IratId));
                    }
                    else
                    {
                        Logger.Warn("ScanService.LoadDocument A megadott vonalkód alapján létezik a küldemény, de nem található hozzá melléklet. Azonosító: " + Azonosito);
                    }
                    #endregion
                }
            }
            else
            {
                Logger.Warn("ScanService.LoadDocument A megadott vonalkód alapján nem található olyan küldemény, ahol EREC_KuldKuldemenyek.Ragszam=" + Azonosito);
            }
            #endregion

            #region FIND BARCODE
            if (!String.IsNullOrEmpty(errorMelleklet))
            {
                //String ObjTipusId = "";
                KRT_BarkodokService BarkodokService = new KRT_BarkodokService();
                KRT_BarkodokSearch BarkodokSearch = new KRT_BarkodokSearch();

                BarkodokSearch.Kod.Value = Azonosito;
                BarkodokSearch.Kod.Operator = Contentum.eQuery.Query.Operators.equals;
                if (Rendszerparameterek.Get(ExParam, Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                    BarkodokSearch.WhereByManual = "or replace(kod, '-','') = '" + Azonosito + "'";

                //// Nem lehet a vonalkód generált (biztos??)
                //BarkodokSearch.KodType.Value = "G";
                //BarkodokSearch.KodType.Operator = Contentum.eQuery.Query.Operators.notequals;

                Result res_BarKod = BarkodokService.GetAll(ExParam, BarkodokSearch);
                if (!String.IsNullOrEmpty(res_BarKod.ErrorCode) || res_BarKod.Ds.Tables[0].Rows.Count == 0)
                {
                    Logger.Error("ScanService.LoadDocument Vonalkód nem található! Azonosito=" + Azonosito);
                    if (!String.IsNullOrEmpty(res_BarKod.ErrorCode))
                    {
                        Logger.Error(res_BarKod.ErrorMessage);
                    }
                    return "Vonalkód nem található! Azonosito=" + Azonosito;
                }
                else
                {
                    System.Data.DataSet ds = res_BarKod.Ds;

                    //ObjTipusId = ds.Tables[0].Rows[0]["ObjTip_Id"].ToString();
                    String Allapot = Allapot = ds.Tables[0].Rows[0]["Allapot"].ToString();

                    switch (Allapot)
                    {
                        case "F":
                            Logger.Warn("ScanService.LoadDocument Felhasznált Vonalkód állapot! Azonosito=" + Azonosito);
                            break;
                        case "T":
                            Logger.Error("ScanService.LoadDocument Érvénytelenített Vonalkód(Törölt)! Azonosito=" + Azonosito);
                            return "Érvénytelenített Vonalkód(Törölt)! Azonosito=" + Azonosito;
                        case "S":
                            Logger.Error("ScanService.LoadDocument Vonalkód nincs adminisztrálva a rendszerben! Azonosito=" + Azonosito);
                            return "Vonalkód nincs adminisztrálva a rendszerben! Azonosito=" + Azonosito;
                        default:
                            Logger.Error("ScanService.LoadDocument Érvénytelen vonalkód(Hibás állapot)! Azonosito=" + Azonosito);
                            return "Érvénytelen vonalkód(Hibás állapot)! Azonosito=" + Azonosito;
                    }

                    BarkodType = ds.Tables[0].Rows[0]["Obj_type"].ToString();
                    ObjId = ds.Tables[0].Rows[0]["Obj_Id"].ToString();

                    Logger.Debug("ScanService.LoadDocument KRT_Barkodok.Obj_type=" + BarkodType + ";KRT_Barkodok.Obj_Id=" + ObjId);
                }
            }
            #endregion

            if (BarkodType == "EREC_KuldKuldemenyek")
            {
                #region BarkodType == "EREC_KuldKuldemenyek"

                // TODO: ellenőrzés, van-e már ilyen csatolmány

                KuldemenyId = ObjId;

                #endregion
            }
            else if (BarkodType == "EREC_KuldMellekletek" || BarkodType == "EREC_KuldTertivevenyek")
            {
                #region BarkodType == "EREC_KuldMellekletek"

                // TODO: ez jó így??
                // Ellenőrzés, van-e már feltöltött dokumentum a vonalkódhoz                
                EREC_CsatolmanyokService csatService = new EREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();
                csatSearch.Manual_Dokumentum_BarCode.Value = Azonosito;
                csatSearch.Manual_Dokumentum_BarCode.Operator = Contentum.eQuery.Query.Operators.equals;
                Result resCsat = csatService.GetAllWithExtension(ExParam, csatSearch);
                if (resCsat.Ds.Tables[0].Rows.Count > 0)
                {
                    Logger.Error("ScanService.LoadDocument Ehhez a vonalkódhoz már tartozik feltöltött dokumentum! Azonosito=" + Azonosito);
                    return "Ehhez a vonalkódhoz már tartozik feltöltött dokumentum! Azonosito=" + Azonosito;
                }

                EREC_MellekletekService MellekletekService = new EREC_MellekletekService();
                EREC_MellekletekSearch MellekletekSearch = new EREC_MellekletekSearch();
                if (BarkodType == "EREC_KuldKuldemenyek")
                {
                    //FELADAT: Ebben az esetben megnézni, hogy létezik -e a mellékletek között az objectum id

                }
                MellekletekSearch.BarCode.Value = Azonosito;
                MellekletekSearch.BarCode.Operator = Contentum.eQuery.Query.Operators.equals;

                Result result = MellekletekService.GetAll(ExParam, MellekletekSearch);
                if (result.IsError)
                {
                    Logger.Error("ScanService.LoadDocument Küldemény elemek között nem találta a vonalkódot! Azonosito=" + Azonosito + " hibaüzenet=" + result.ErrorMessage);
                    return "Küldemény elemek között nem találta a vonalkódot!";
                }

                if (result.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Error("ScanService.LoadDocument Küldemény elemet nem sikerült egyértelműen azonosítani a vonalkód alapján! Találatok száma: " + result.Ds.Tables[0].Rows.Count + ";Azonosito=" + Azonosito);
                    return "Küldemény elemet nem sikerült egyértelműen azonosítani a vonalkód alapján! Azonosito=" + Azonosito;
                }
                else if (result.Ds.Tables[0].Rows.Count == 1)
                {
                    System.Data.DataSet ds = result.Ds;
                    MellekletId = ds.Tables[0].Rows[0]["Id"].ToString();
                    KuldemenyId = ds.Tables[0].Rows[0]["KuldKuldemeny_Id"].ToString();
                    CsatolmanyMegjegyzes = ds.Tables[0].Rows[0]["Megjegyzes"].ToString();
                    Logger.Debug("ScanService.LoadDocument Küldemény Id =" + KuldemenyId);
                }
                #endregion
            }
            else if (BarkodType == "EREC_IratMellekletek")
            {
                #region BarkodType == "EREC_IratMellekletek"

                // TODO: ellenőrzés, van-e már ilyen
                EREC_CsatolmanyokService csatService = new EREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();
                csatSearch.Manual_Dokumentum_BarCode.Value = Azonosito;
                csatSearch.Manual_Dokumentum_BarCode.Operator = Contentum.eQuery.Query.Operators.equals;
                Result resCsat = csatService.GetAllWithExtension(ExParam, csatSearch);
                if (resCsat.Ds.Tables[0].Rows.Count > 0)
                {
                    Logger.Error("ScanService.LoadDocument Ehhez a vonalkódhoz már tartozik feltöltött dokumentum! Azonosito=" + Azonosito);
                    return "Ehhez a vonalkódhoz már tartozik feltöltött dokumentum! Azonosito=" + Azonosito;
                }

                EREC_MellekletekService IratMellekletekService = new EREC_MellekletekService();
                EREC_MellekletekSearch IratMellekletekSearch = new EREC_MellekletekSearch();

                IratMellekletekSearch.BarCode.Value = Azonosito;
                IratMellekletekSearch.BarCode.Operator = Contentum.eQuery.Query.Operators.equals;

                Result resiratGet = IratMellekletekService.GetAll(ExParam, IratMellekletekSearch);
                if (!String.IsNullOrEmpty(resiratGet.ErrorCode) || resiratGet.Ds.Tables[0].Rows.Count == 0)
                {
                    Logger.Error("ScanService.LoadDocument Iratok között nem találta a vonalkódot! Azonosito=" + Azonosito +" hibaüzenet=" + resiratGet.ErrorMessage);
                    return "Iratok között nem találta a vonalkódot! Azonosito=" + Azonosito;
                }
                if (resiratGet.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Error("ScanService.LoadDocument Iratok nem sikerült egyértelműen azonosítani a vonalkód alapján! Találatok száma: " + resiratGet.Ds.Tables[0].Rows.Count + ";Azonosito=" + Azonosito);
                    return "Iratok nem sikerült egyértelműen azonosítani a vonalkód alapján! Azonosito=" + Azonosito;
                }
                else if (resiratGet.Ds.Tables[0].Rows.Count == 1)
                {
                    System.Data.DataSet ds = resiratGet.Ds;
                    MellekletId = ds.Tables[0].Rows[0]["Id"].ToString();
                    IratId = ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();
                    CsatolmanyMegjegyzes = ds.Tables[0].Rows[0]["Megjegyzes"].ToString();
                    Logger.Debug("ScanService.LoadDocument Iratid=" + IratId);
                }
                #endregion
            }
            else if (BarkodType == "EREC_PldIratPeldanyok")
            {
                #region BarkodType == "EREC_PldIratPeldanyok"

                // TODO: ellenőrzés, van-e már ilyen

                // Vonalkód keresése az iratpéldányok között
                EREC_PldIratPeldanyokService iratPeldanyokService = new EREC_PldIratPeldanyokService();

                EREC_PldIratPeldanyokSearch iratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

                iratPeldanyokSearch.BarCode.Value = Azonosito;
                iratPeldanyokSearch.BarCode.Operator = Query.Operators.equals;

                Result result_PldSearch = iratPeldanyokService.GetAll(ExParam, iratPeldanyokSearch);
                if (result_PldSearch.IsError)
                {
                    // hiba:
                    Logger.Error("ScanService.LoadDocument Hiba az iratpéldány keresésekor! Azonosito=" + Azonosito, ExParam, result_PldSearch);
                    return "Hiba az iratpéldány keresésekor! Azonosito=" + Azonosito;
                }

                if (result_PldSearch.Ds.Tables[0].Rows.Count == 0)
                {
                    Logger.Error("Nem található ilyen vonalkódú iratpéldány! Azonosito=" + Azonosito, ExParam);
                    return "Nem található ilyen vonalkódú iratpéldány! Azonosito=" + Azonosito;
                }
                else if (result_PldSearch.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Error("Nem sikerült egyértelműen azonosítani az iratot! Találatok száma: " + result_PldSearch.Ds.Tables[0].Rows.Count + ";Azonosito=" + Azonosito, ExParam);
                    return "Nem sikerült egyértelműen azonosítani az iratot! Azonosito=" + Azonosito;
                }
                else if (result_PldSearch.Ds.Tables[0].Rows.Count == 1)
                {
                    IratId = result_PldSearch.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();
                    Logger.Debug("ScanService.LoadDocument Iratid=" + IratId);
                }
                #endregion
            }
            else
            {
                Logger.Error("ScanService.LoadDocument Objectum típus hiba BarkodType='" + BarkodType + "'! Ellenőrzni a 'Barkodok' táblában az obj_tipus értékét. Lehetséges értékek: EREC_KuldKuldemenyek,EREC_KuldMellekletek,EREC_IratMellekletek.");
                return "Objectum típus hiba BarkodType='" + BarkodType + "'! Ellenőrzni a 'Barkodok' táblában az obj_tipus értékét. Lehetséges értékek: EREC_KuldKuldemenyek,EREC_KuldMellekletek,EREC_IratMellekletek.";

            }


        }
        else if (!string.IsNullOrEmpty(Iktatoszam))
        {
            #region TODO: Azonosítás iktatószám alapján

            //Egyenlőre ez az ág nem kell! N.GY.D. 2007.12.13
            Logger.Error("ScanService.LoadDocument Vonalkód azonosítás szükséges! Azonosito=" + Azonosito);
            return "Vonalkód azonosítás szükséges! Azonosito=" + Azonosito;

            #endregion
        }
        else if (!string.IsNullOrEmpty(Erkeztetoszam))
        {
            #region TODO: Azonosítás érkeztetőszám alapján

            //Egyenlőre ez az ág nem kell! N.GY.D. 2007.12.13
            Logger.Error("ScanService.LoadDocument Vonalkód azonosítás szükséges! Azonosito=" + Azonosito);
            return "Vonalkód azonosítás szükséges! Azonosito=" + Azonosito;


            //Logger.Info("Érkeztetőszám használata");
            //EREC_KuldKuldemenyekSearch KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
            //KuldKuldemenyekSearch.Erkezteto_Szam.Value = Azonosito;
            //KuldKuldemenyekSearch.Erkezteto_Szam.Operator = Contentum.eQuery.Query.Operators.equals;
            //Result result = KuldKuldemenyekService.GetAll(ExParam, KuldKuldemenyekSearch);
            //if (!String.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count == 0)
            //{
            //    Logger.Error("Küldemények között nem találta az érkeztetőszámot!");
            //    return "Küldemények között nem találta az érkeztetőszámot!";
            //}
            //if (result.Ds.Tables[0].Rows.Count > 1)
            //{
            //    Logger.Error("Küldemény elemet nem sikerült egyértelműen azonosítani az érkeztető szám alapján!");
            //    return "Küldemény elemet nem sikerült egyértelműen azonosítani az érkeztető szám alapján!";
            //}
            //System.Data.DataSet ds = result.Ds;
            //KuldemenyId = ds.Tables[0].Rows[0]["Id"].ToString();
            //CsatolmanyMegjegyzes = ds.Tables[0].Rows[0]["Targy"].ToString();

            //ExParam.Record_Id = KuldemenyId;
            //Logger.Info("Küldemény id = " + KuldemenyId);

            ////SharedFileId = resUL.Uid;
            #endregion
        }



        if (String.IsNullOrEmpty(KuldemenyId) && String.IsNullOrEmpty(IratId))
        {
            Logger.Error("ScanService.LoadDocument Vonalkód nincs meg! Azonosito=" + Azonosito + ";KuldemenyId=" + KuldemenyId + ";IratId=" + IratId);
            return "Vonalkód nincs meg! Azonosito=" + Azonosito;
        }
        else if (!String.IsNullOrEmpty(KuldemenyId))
        {
            ExParam.Record_Id = KuldemenyId;
            Result resKK = KuldKuldemenyekService.Get(ExParam);
            if (!String.IsNullOrEmpty(resKK.ErrorCode))
            {
                Logger.Error("ScanService.LoadDocument Küldemény azonosítása nem sikerült!" + "! Azonosito=" + Azonosito + ";KuldemenyId=" + KuldemenyId + " hibaüzenet=" + resKK.ErrorMessage);
                return "Küldemény elem azonosítása nem sikerült! Azonosito=" + Azonosito;
            }

            EREC_KuldKuldemenyek KuldKuldemenyek = (EREC_KuldKuldemenyek)resKK.Record;

            if (KuldKuldemenyek.Allapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva
                || (KuldKuldemenyek.Allapot == KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt && KuldKuldemenyek.TovabbitasAlattAllapot == KodTarak.KULDEMENY_ALLAPOT.Iktatva)
               )
            {
                EREC_IraIratokService _EREC_IraIratokService = new EREC_IraIratokService();
                EREC_IraIratokSearch _EREC_IraIratokSearch = new EREC_IraIratokSearch();

                _EREC_IraIratokSearch.KuldKuldemenyek_Id.Value = KuldemenyId;
                _EREC_IraIratokSearch.KuldKuldemenyek_Id.Operator = Query.Operators.equals;

                // BUG_1811
                _EREC_IraIratokSearch.Allapot.Value = Search.GetSqlInnerString(new string[] { KodTarak.IRAT_ALLAPOT.Atiktatott, KodTarak.IRAT_ALLAPOT.Sztornozott });
                _EREC_IraIratokSearch.Allapot.Operator = Query.Operators.notinner;

                Result _res = _EREC_IraIratokService.GetAll(ExParam, _EREC_IraIratokSearch);
                if (!String.IsNullOrEmpty(_res.ErrorCode))
                {
                    Logger.Error("ScanService.LoadDocument Irat azonosítása nem sikerült! Azonosito=" + Azonosito + ";KuldemenyId=" + KuldemenyId + " hibaüzenet=" + resKK.ErrorMessage);
                    return "Irat azonosítása nem sikerült! Azonosito=" + Azonosito;
                }
                else
                {
                    if (_res.Ds.Tables[0].Rows.Count == 1)
                    {
                        System.Data.DataSet ds = _res.Ds;
                        IratId = ds.Tables[0].Rows[0]["Id"].ToString();
                        //FELADAT: korrektül összehozni (pl.: függvénnyel)
                        ExParam.Record_Id = IratId;
                        Result resII = IraIratokService.Get(ExParam);
                        if (!String.IsNullOrEmpty(resII.ErrorCode))
                        {
                            Logger.Error("ScanService.LoadDocument Irat azonosítása nem sikerült! Azonosito=" + Azonosito + ";KuldemenyId=" + KuldemenyId + " hibaüzenet=" + resKK.ErrorMessage);
                            return "Irat azonosítása nem sikerült! Azonosito=" + Azonosito;
                        }
                        EREC_IraIratok IraIratok = (EREC_IraIratok)resII.Record;
                        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

                        //erec_Csatolmanyok.BarCode = Azonosito;
                        erec_Csatolmanyok.IraIrat_Id = IratId;

                        Csatolmany csatolmanyIR = new Csatolmany();
                        csatolmanyIR.BarCode = Azonosito;

                        //TODO: Aláírás
                        if ("I".CompareTo(Alairas) == 0)
                        {
                            SignedFileName = Sign(FileName, 0, Content);
                            erec_Csatolmanyok.Leiras = "\"" + CsatolmanyMegjegyzes + "\" aláírt fájl";
                            csatolmanyIR.Nev = FileName + ".sdxm";
                            //csatolmanyIR.SourceSharePath = FileSharedDirectoryName;
                            csatolmanyIR.Tartalom = Content;
                            Logger.Info("ScanService.LoadDocument SharePoint feltöltés kezdete - aláírt file FileName=" + FileName);

                            Result _resULSigned = IraIratokService.CsatolmanyUpload(ExParam, erec_Csatolmanyok, csatolmanyIR);
                            Logger.Info("ScanService.LoadDocument SharePoint feltöltés vége - aláírt file FileName=" + FileName);
                            if (!String.IsNullOrEmpty(_resULSigned.ErrorCode))
                            {
                                Logger.Error("ScanService.LoadDocument Hiba az aláírt file SharePoint feltöltése során (5): FileName=" + FileName + " hibaüzenet=" + _resULSigned.ErrorMessage + "!");
                                return String.Format("Hiba az aláírt file SharePoint feltöltése során! (5) FileName=" + FileName + " \nKapott üzenet: {0}", _resULSigned.ErrorMessage); //hiba a SharePoint feltöltésben _resUL.ErrorMessage;
                            }

                            krtDokId_esemenyKezeleshez = _resULSigned.Uid;

                        }
                        erec_Csatolmanyok.Leiras = CsatolmanyMegjegyzes;
                        csatolmanyIR.Nev = FileName;
                        //csatolmanyIR.SourceSharePath = FileSharedDirectoryName;
                        csatolmanyIR.Tartalom = Content;
                        Logger.Info("ScanService.LoadDocument SharePoint feltöltés kezdete - eredeti file FileName=" + FileName);

                        Result _resUL = IraIratokService.CsatolmanyUpload(ExParam, erec_Csatolmanyok, csatolmanyIR);
                        Logger.Info("ScanService.LoadDocument SharePoint feltöltés vége - eredeti file FileName=" + FileName);
                        if (!String.IsNullOrEmpty(_resUL.ErrorCode))
                        {
                            Logger.Error("ScanService.LoadDocument Hiba a SharePoint feltöltés során! (6): FileName=" + FileName + " hibaüzenet=" + _resUL.ErrorMessage + "!");
                            return String.Format("Hiba a SharePoint feltöltés során! (6) FileName=" + FileName + " \nKapott üzenet: {0}", _resUL.ErrorMessage); //hiba a SharePoint feltöltésben _resUL.
                        }

                        krtDokId_esemenyKezeleshez = _resUL.Uid;

                    }
                }
            }
            EREC_Csatolmanyok erec_Csatolmanyok_kuld = new EREC_Csatolmanyok();
            //erec_Csatolmanyok.BarCode = Azonosito;
            erec_Csatolmanyok_kuld.KuldKuldemeny_Id = KuldemenyId;

            Csatolmany csatolmany = new Csatolmany();
            csatolmany.BarCode = Azonosito;
            //TODO: Aláírás
            if ("I".CompareTo(Alairas) == 0)
            {
                //return "" + Alairas + "";
                SignedFileName = Sign(FileName, 0, Content);
                erec_Csatolmanyok_kuld.Leiras = "\"" + CsatolmanyMegjegyzes + "\" aláírt fájl";
                csatolmany.Nev = FileName + ".sdxm";
                //csatolmany.SourceSharePath = FileSharedDirectoryName;
                csatolmany.Tartalom = Content;
                Logger.Info("ScanService.LoadDocument SharePoint feltöltés kezdete - aláírt file FileName=" + FileName);
                Result resULSigned = KuldKuldemenyekService.CsatolmanyUpload(ExParam, erec_Csatolmanyok_kuld, csatolmany);
                Logger.Info("ScanService.LoadDocument SharePoint feltöltés vége - aláírt file FileName=" + FileName);
                if (!String.IsNullOrEmpty(resULSigned.ErrorCode))
                {
                    Logger.Error("ScanService.LoadDocument Hiba az aláírt file SharePoint feltöltése során (1): FileName=" + FileName + " hibaüzenet=" + resULSigned.ErrorMessage + "!");
                    return String.Format("Hiba az aláírt file SharePoint feltöltése során (1) FileName=" + FileName + " \nKapott üzenet: {0}", resULSigned.ErrorMessage); //hiba a SharePoint feltöltésben resUL.ErrorMessage;
                }

                krtDokId_esemenyKezeleshez = resULSigned.Uid;

            }
            erec_Csatolmanyok_kuld.Leiras = CsatolmanyMegjegyzes;
            csatolmany.Nev = FileName;
            //csatolmany.SourceSharePath = FileSharedDirectoryName;
            csatolmany.Tartalom = Content;

            // OCR beállítása a csatolmányban
            int ocr_prior = 0;

            if (int.TryParse(Ocr, out ocr_prior) && ocr_prior > 0)
            {
                csatolmany.OCRAdatok.OCRAllapot = "1";
                csatolmany.OCRAdatok.OCRPrioritas = Ocr;
            }

            Logger.Debug("ScanService.LoadDocument OCRPrioritas(s): " + Ocr + "; OCRPrioritas(p): " + ocr_prior.ToString() + "; csatolmanyokOCRAllapot: " + csatolmany.OCRAdatok.OCRAllapot);

            Logger.Info("ScanService.LoadDocument SharePoint feltöltés kezdete - eredeti file FileName=" + FileName);

            Result resUL = KuldKuldemenyekService.CsatolmanyUpload(ExParam, erec_Csatolmanyok_kuld, csatolmany);

            Logger.Info("ScanService.LoadDocument SharePoint feltöltés vége - eredeti file FileName=" + FileName);
            if (!String.IsNullOrEmpty(resUL.ErrorCode))
            {
                Logger.Error("ScanService.LoadDocument Hiba a SharePoint feltöltés során! (2) FileName=" + FileName + " hibaüzenet=" + resUL.ErrorMessage);
                return String.Format("Hiba a SharePoint feltöltés során! (2) FileName=" + FileName + " \nKapott üzenet: {0}", resUL.ErrorMessage); //hiba a SharePoint feltöltésben resUL.
            }

            krtDokId_esemenyKezeleshez = resUL.Uid;

            #region IRATELEM KAPCSOLATOK INSERT
            string kapcsInsertError = null;
            if (!AddIratelemKapcsolat(MellekletId, resUL.Uid, out kapcsInsertError))
            {
                Logger.Error("ScanService.LoadDocument Hiba az iratelemek feltöltés során!");
                return kapcsInsertError;
            }
            #endregion

            Logger.Info("ScanService.LoadDocument Sikeresen lefutott!");
        }
        else if (!String.IsNullOrEmpty(IratId))
        {
            ExParam.Record_Id = IratId;
            Result resII = IraIratokService.Get(ExParam);
            if (!String.IsNullOrEmpty(resII.ErrorCode))
            {
                Logger.Error("ScanService.LoadDocument Irat azonosítása nem sikerült! IratId=IratId hibaüzenet=" + resII.ErrorMessage);
                return "Irat azonosítása nem sikerült!";
            }
            EREC_IraIratok IraIratok = (EREC_IraIratok)resII.Record;
            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

            //erec_Csatolmanyok.BarCode = Azonosito;
            erec_Csatolmanyok.IraIrat_Id = IratId;

            Csatolmany csatolmany = new Csatolmany();
            csatolmany.BarCode = Azonosito;

            //TODO: Aláírás
            if ("I".CompareTo(Alairas) == 0)
            {
                SignedFileName = Sign(FileName, 0, Content);
                erec_Csatolmanyok.Leiras = "\"" + CsatolmanyMegjegyzes + "\" aláírt fájl";
                csatolmany.Nev = FileName + ".sdxm";
                //csatolmany.SourceSharePath = FileSharedDirectoryName;
                csatolmany.Tartalom = Content;
                Logger.Info("SharePoint feltöltés kezdete - aláírt file FileName=" + FileName);

                Result resULSigned = IraIratokService.CsatolmanyUpload(ExParam, erec_Csatolmanyok, csatolmany);
                Logger.Info("SharePoint feltöltés vége - aláírt file FileName=" + FileName);
                if (!String.IsNullOrEmpty(resULSigned.ErrorCode))
                {
                    Logger.Error("Hiba az aláírt file SharePoint feltöltése során! (3) FileName=" + FileName + " hibaüzenet=" + resULSigned.ErrorMessage);
                    return String.Format("Hiba az aláírt file SharePoint feltöltése során! (3) FileName=" + FileName + " \nKapott üzenet: {0}", resULSigned.ErrorMessage); //hiba a SharePoint feltöltésben resUL.ErrorMessage;
                }

                krtDokId_esemenyKezeleshez = resULSigned.Uid;

            }
            erec_Csatolmanyok.Leiras = CsatolmanyMegjegyzes;
            csatolmany.Nev = FileName;
            //csatolmany.SourceSharePath = FileSharedDirectoryName;
            csatolmany.Tartalom = Content;

            // OCR beállítása a csatolmányban
            int ocr_prior = 0;

            if (int.TryParse(Ocr, out ocr_prior) && ocr_prior > 0)
            {
                csatolmany.OCRAdatok.OCRAllapot = "1";
                csatolmany.OCRAdatok.OCRPrioritas = Ocr;
            }

            Logger.Info("SharePoint feltöltés kezdete - eredeti file FileName=" + FileName);

            Result resUL = IraIratokService.CsatolmanyUpload(ExParam, erec_Csatolmanyok, csatolmany);
            Logger.Info("SharePoint feltöltés vége - eredeti file FileName=" + FileName);
            if (!String.IsNullOrEmpty(resUL.ErrorCode))
            {
                Logger.Error("Hiba a SharePoint feltöltés során! (4) FileName=" + FileName + " hibaüzenet=" + resUL.ErrorMessage);
                return String.Format("Hiba a SharePoint feltöltés során! (4)  FileName=" + FileName + " \nKapott üzenet: {0}", resUL.ErrorMessage); //hiba a SharePoint feltöltésben resUL.
            }

            krtDokId_esemenyKezeleshez = resUL.Uid;

            #region IRATELEM KAPCSOLATOK INSERT
            string kapcsInsertError = null;
            if (!AddIratelemKapcsolat(MellekletId, resUL.Uid, out kapcsInsertError))
            {
                Logger.Error("ScanService.LoadDocument Hiba az iratelemek feltöltés során!");
                return kapcsInsertError;
            }

            #endregion
            Logger.Info("ScanService.LoadDocument Sikeresen lefutott!");

        }

        #region esemenykezeles

        try
        {
            Logger.Debug("ScanService.LoadDocument DokumentumokNewScan esemenybejegyzes.");

            if (!String.IsNullOrEmpty(krtDokId_esemenyKezeleshez))
            {
                KRT_EsemenyekService eventLogService = new KRT_EsemenyekService(this.dataContext);

                KRT_Esemenyek eventLogRecord = (KRT_Esemenyek)eventLogService.GetParamsByFunkcioKod(ExParam.Clone()
                    , dataContext.Tranz_Id
                    , krtDokId_esemenyKezeleshez
                    , "KRT_Dokumentumok"
                    , "DokumentumokNewScan").Record;

                //
                //  nem egeszen vilagos ezt miert is nem adja at nehany helyen....
                //  az emailnal pl ha ezt hozzaadtuk, akkor mukodott rendesen
                //  itt is hozzaadjuk a biztosag kedveert

                if (String.IsNullOrEmpty(ExParam.Felhasznalo_Id))
                {
                    Logger.Error("ScanService.LoadDocument Üres a ExParam.Felhasznalo_Id valtozó, ezért nem lesz bejegyezve a DocumentumokNewScan esemény!");
                }
                else
                {
                    eventLogRecord.Felhasznalo_Id_Login = ExParam.Felhasznalo_Id;
                    //ExParam.LoginUser_Id = ExParam.Felhasznalo_Id;
                }

                Logger.Debug("ScanService.LoadDocument scan event log előtt");

                Result eventLogResult = eventLogService.Insert(ExParam.Clone(), eventLogRecord);

                Logger.Debug("ScanService.LoadDocument scan event log után");

                if (!String.IsNullOrEmpty(eventLogResult.ErrorCode))
                {
                    Logger.Error(String.Format("ScanService.LoadDocument Hiba az esemény bejegyzésekor: {0} - {1}", eventLogResult.ErrorCode, eventLogResult.ErrorMessage));
                }

            }
            else
            {
                Logger.Error("ScanService.LoadDocument Üres a KRT_Dok_id valtozo, ezért nem lesz bejegyezve a DocumentumokNewScan esemeny!");
            }

        }
        catch (Exception ex)
        {
            Logger.Error(String.Format("ScanService.LoadDocument Hiba az esemenykezeles bejegyzéskor: {0}\nStackTrace: {1}\nSource: {2}", ex.Message, ex.StackTrace, ex.Source));
        }

        #endregion

        log.WsEnd(ExParam);

        Logger.Debug("ScanService.LoadDocument visszatérési érték: OK");

        return "OK";
    }

    private bool Login(ExecParam exParam, string userFullName)
    {
        Login login = new Login();
        login.FelhasznaloNev = userFullName;
        login.Tipus = "Windows";
        login.Jelszo = "";
        Logger.Debug("Autentikáció kezdete!");
        AuthenticationService AuthService = eAdminService.ServiceFactory.GetAuthenticationService();
        Result ret = AuthService.Login(exParam, login);

        if (ret.Uid != null && !String.IsNullOrEmpty(ret.Uid) && String.IsNullOrEmpty(ret.ErrorCode))
        {
            exParam.Felhasznalo_Id = ret.Uid;
            exParam.LoginUser_Id = ret.Uid;
            return true;
        }
        else
        {
            Logger.Error("Hiba a felhasználó azonosítása során: " + userFullName + "!");
            return false;
        }
    }

    private List<EREC_KuldKuldemenyek> FindKuldKuldemenyByRagszam(ExecParam execParam, string ragszam, out string error)
    {
        List<EREC_KuldKuldemenyek> resultList = new List<EREC_KuldKuldemenyek>();
        EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
        search.RagSzam = new Field();
        search.RagSzam.Name = "EREC_KuldKuldemenyek.RagSzam";
        search.RagSzam.Operator = "=";
        search.RagSzam.Value = ragszam;
        search.RagSzam.Group = "0";
        search.RagSzam.GroupOperator = "and";
        search.RagSzam.Type = "String";

        Result svcResult;
        using (EREC_KuldKuldemenyekService client = new EREC_KuldKuldemenyekService())
        {
            svcResult = client.GetAll(execParam, search);
        }

        error = null;
        Logger.Info("FindKuldKuldemenyByRagszam EREC_KuldKuldemenyekService keresés vége");
        if (!String.IsNullOrEmpty(svcResult.ErrorCode))
        {
            Logger.Error("FindKuldKuldemenyByRagszam Hiba a EREC_KuldKuldemenyekService keresés során! (6) ragszam=" + ragszam +" hibaünezet=" + svcResult.ErrorMessage);
            error = String.Format("Hiba a EREC_KuldKuldemenyekService keresés során! (6) ragszam=" + ragszam + " \nKapott üzenet: {0}", svcResult.ErrorMessage);
            return null;
        }
        if (svcResult.Ds != null && svcResult.Ds.Tables.Count > 0 && svcResult.Ds.Tables[0].Rows.Count > 0)
        {
            EREC_KuldKuldemenyek res = null;
            foreach (DataRow item in svcResult.Ds.Tables[0].Rows)
            {
                res = new EREC_KuldKuldemenyek();

                #region FILL ENTITY
                res.AdathordozoTipusa = item["AdathordozoTipusa"].ToString();
                res.Ajanlott = item["Ajanlott"].ToString();
                res.Allapot = item["Allapot"].ToString();
                res.Ar = item["Ar"].ToString();
                res.Azonosito = item["Azonosito"].ToString();
                res.BarCode = item["BarCode"].ToString();
                res.BeerkezesIdeje = item["BeerkezesIdeje"].ToString();
                res.BelyegzoDatuma = item["BelyegzoDatuma"].ToString();
                res.BontasiMegjegyzes = item["BontasiMegjegyzes"].ToString();
                res.BoritoTipus = item["BoritoTipus"].ToString();
                res.CimSTR_Bekuldo = item["CimSTR_Bekuldo"].ToString();
                res.CimzesTipusa = item["CimzesTipusa"].ToString();
                res.Cim_Id = item["Cim_Id"].ToString();
                res.CsoportFelelosEloszto_Id = item["CsoportFelelosEloszto_Id"].ToString();
                res.Csoport_Id_Cimzett = item["Csoport_Id_Cimzett"].ToString();
                res.Csoport_Id_Felelos = item["Csoport_Id_Felelos"].ToString();
                res.Csoport_Id_Felelos_Elozo = item["Csoport_Id_Felelos_Elozo"].ToString();
                res.Elektronikus_Kezbesitesi_Allap = item["Elektronikus_Kezbesitesi_Allap"].ToString();
                res.Elsobbsegi = item["Elsobbsegi"].ToString();
                res.ElsodlegesAdathordozoTipusa = item["ElsodlegesAdathordozoTipusa"].ToString();
                res.Erkeztetes_Ev = item["Erkeztetes_Ev"].ToString();
                res.Erkezteto_Szam = item["Erkezteto_Szam"].ToString();
                res.ErvKezd = item["ErvKezd"].ToString();
                res.ErvVege = item["ErvVege"].ToString();
                res.ExpedialasIdeje = item["ExpedialasIdeje"].ToString();
                res.E_elorejelzes = item["E_elorejelzes"].ToString();
                res.E_ertesites = item["E_ertesites"].ToString();
                res.FelbontasDatuma = item["FelbontasDatuma"].ToString();
                res.FelhasznaloCsoport_Id_Alairo = item["FelhasznaloCsoport_Id_Alairo"].ToString();
                res.FelhasznaloCsoport_Id_Atvevo = item["FelhasznaloCsoport_Id_Atvevo"].ToString();
                res.FelhasznaloCsoport_Id_Bonto = item["FelhasznaloCsoport_Id_Bonto"].ToString();
                res.FelhasznaloCsoport_Id_Expedial = item["FelhasznaloCsoport_Id_Expedial"].ToString();
                res.FelhasznaloCsoport_Id_Orzo = item["FelhasznaloCsoport_Id_Orzo"].ToString();
                res.Fizikai_Kezbesitesi_Allapot = item["Fizikai_Kezbesitesi_Allapot"].ToString();
                // jav. nekrisz 17.10.11 (BLG_361)
                //res.HivatkozasiSzam = item["AdathordozoTipusa"].ToString();
                res.HivatkozasiSzam = item["HivatkozasiSzam"].ToString();
                res.Id = item["Id"].ToString();
                res.IktatastNemIgenyel = item["IktatastNemIgenyel"].ToString();
                res.Iktathato = item["Iktathato"].ToString();
                res.IktatniKell = item["IktatniKell"].ToString();
                res.IraIktatokonyv_Id = item["IraIktatokonyv_Id"].ToString();
                res.IraIratok_Id = item["IraIratok_Id"].ToString();
                res.KezbesitesModja = item["KezbesitesModja"].ToString();
                res.KimenoKuldemenyFajta = item["KimenoKuldemenyFajta"].ToString();
                res.KimenoKuld_Sorszam = item["KimenoKuld_Sorszam"].ToString();
                res.Kovetkezo_Felelos_Id = item["Kovetkezo_Felelos_Id"].ToString();
                res.Kovetkezo_Orzo_Id = item["Kovetkezo_Orzo_Id"].ToString();
                res.KuldesMod = item["KuldesMod"].ToString();
                res.KuldKuldemeny_Id_Szulo = item["KuldKuldemeny_Id_Szulo"].ToString();
                res.MegorzesJelzo = item["MegorzesJelzo"].ToString();
                res.MegtagadasDat = item["MegtagadasDat"].ToString();
                res.MegtagadasIndoka = item["MegtagadasIndoka"].ToString();
                res.Megtagado_Id = item["Megtagado_Id"].ToString();
                res.Minosites = item["Minosites"].ToString();
                res.Munkaallomas = item["Munkaallomas"].ToString();
                res.NevSTR_Bekuldo = item["NevSTR_Bekuldo"].ToString();
                res.Partner_Id_Bekuldo = item["Partner_Id_Bekuldo"].ToString();
                res.PeldanySzam = item["PeldanySzam"].ToString();
                res.PostaiLezaroSzolgalat = item["PostaiLezaroSzolgalat"].ToString();
                res.PostazasIranya = item["PostazasIranya"].ToString();
                res.RagSzam = item["RagSzam"].ToString();
                res.SajatKezbe = item["SajatKezbe"].ToString();
                res.SerultKuldemeny = item["SerultKuldemeny"].ToString();
                res.Surgosseg = item["Surgosseg"].ToString();
                res.SztornirozasDat = item["SztornirozasDat"].ToString();
                res.Targy = item["Targy"].ToString();
                res.Tartalom = item["Tartalom"].ToString();
                res.Tertiveveny = item["Tertiveveny"].ToString();
                res.TevesCimzes = item["TevesCimzes"].ToString();
                res.TevesErkeztetes = item["TevesErkeztetes"].ToString();
                res.Tipus = item["Tipus"].ToString();
                res.TovabbitasAlattAllapot = item["TovabbitasAlattAllapot"].ToString();
                res.Tovabbito = item["Tovabbito"].ToString();
                res.UgyintezesModja = item["UgyintezesModja"].ToString();
                res.Base.Ver = item["Ver"].ToString();
                res.Base.LetrehozasIdo = item["LetrehozasIdo"].ToString();
                #endregion

                resultList.Add(res);
            }
        }
        return resultList;
    }
    private bool AddIratelemKapcsolat(string mellekletId, string csatolmanyId, out string error)
    {
        error = null;

        #region IRATELEM KAPCSOLATOK INSERT
        if (string.IsNullOrEmpty(mellekletId) || string.IsNullOrEmpty(csatolmanyId))
            return true;

        EREC_IratelemKapcsolatokService service_iratelemKapcs = new EREC_IratelemKapcsolatokService();
        EREC_IratelemKapcsolatok kapcs = new EREC_IratelemKapcsolatok();
        kapcs.Csatolmany_Id = csatolmanyId;
        kapcs.Melleklet_Id = mellekletId;
        kapcs.ErvKezd = DateTime.Now.ToString();
        kapcs.Id = Guid.NewGuid().ToString();

        ExecParam ExParam = new ExecParam();
        ExParam.Record_Id = kapcs.Id;
        Result result = service_iratelemKapcs.Insert(ExParam, kapcs);
        Logger.Info("AddIratelemKapcsolat EREC_IratelemKapcsolatok feltöltés vége");
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("AddIratelemKapcsolat Hiba a EREC_IratelemKapcsolatok feltöltés során! (6) mellekletId=" + mellekletId + ";csatolmanyId=" + csatolmanyId + " hibaüzenet=" + result.ErrorMessage);
            error = String.Format("Hiba a EREC_IratelemKapcsolatok feltöltés során! (6) mellekletId=" + mellekletId + ";csatolmanyId=" + csatolmanyId + " \nKapott üzenet: {0}", result.ErrorMessage);
            return false;
        }
        return true;
        #endregion
    }
    private EREC_Mellekletek FindMelleklet(ExecParam execParam, string kuldKuldemenyId, string adathordozoTipus, out string error)
    {
        EREC_MellekletekSearch search = new EREC_MellekletekSearch();
        search.AdathordozoTipus.Value = KodTarak.AdathordozoTipus.Tertiveveny;
        search.AdathordozoTipus.Operator = Contentum.eQuery.Query.Operators.equals;
        search.KuldKuldemeny_Id.Value = kuldKuldemenyId;
        search.KuldKuldemeny_Id.Operator = Contentum.eQuery.Query.Operators.equals;

        Result svcResult;
        using (EREC_MellekletekService client = new EREC_MellekletekService())
        {
            svcResult = client.GetAll(execParam, search);
        }
        error = null;
        Logger.Info("FindMelleklet EREC_MellekletekSearch keresés vége");
        if (!String.IsNullOrEmpty(svcResult.ErrorCode))
        {
            Logger.Error("FindMelleklet Hiba a EREC_MellekletekSearch keresés során! (6) kuldKuldemenyId=" + kuldKuldemenyId + ";adathordozoTipus=" + adathordozoTipus + " hibaüzenet=" + svcResult.ErrorMessage);
            error = String.Format("Hiba a EREC_MellekletekSearch keresés során! (6) kuldKuldemenyId=" + kuldKuldemenyId + ";adathordozoTipus=" + adathordozoTipus + " \nKapott üzenet: {0}", svcResult.ErrorMessage);
            return null;
        }
        if (svcResult.Ds == null || svcResult.Ds.Tables.Count == 0 || svcResult.Ds.Tables[0].Rows.Count == 0)
        {
            string errorMessage = "Hiba a EREC_MellekletekFindMelleklet client.GetAll() során! Nincs találat!";
            Logger.Error(errorMessage);
            error = String.Format(errorMessage);
            return null;
        }
        return new EREC_Mellekletek()
        {
            Id = svcResult.Ds.Tables[0].Rows[0]["Id"].ToString(),
            IraIrat_Id = svcResult.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString(),
            Megjegyzes = svcResult.Ds.Tables[0].Rows[0]["Megjegyzes"].ToString(),
            KuldKuldemeny_Id = svcResult.Ds.Tables[0].Rows[0]["KuldKuldemeny_Id"].ToString(),
            AdathordozoTipus = svcResult.Ds.Tables[0].Rows[0]["AdathordozoTipus"].ToString(),
            BarCode = svcResult.Ds.Tables[0].Rows[0]["BarCode"].ToString(),
            ErvKezd = svcResult.Ds.Tables[0].Rows[0]["ErvKezd"].ToString(),
        };
    }

    //[WebMethod()]
    //public string ScanWS_Teszt(string vonalkod, string fajlNev)
    //{
    //    //ExecParam execParam = new ExecParam();
    //    //execParam.Felhasznalo_Id = "f088e89e-7917-4d0b-b94f-f4dac84f0f95";
    //    //execParam.LoginUser_Id = execParam.Felhasznalo_Id;


    //    System.Text.UTF8Encoding utf8encoding = new System.Text.UTF8Encoding();
    //    byte[] tartalom = utf8encoding.GetBytes("Egy meggymag nem meggymag");


    //    //ScanService scanService = new ScanService();

    //    return this.LoadDocument("kovacs.andras", "AXIS", "B", vonalkod, "1", "megjegyzés", "N", "OCR", fajlNev, tartalom);
    //}



    private String UpLoadUgyirat(String FileSharedDirectoryName, String FileName, String UgyiratId, String Azonosito, String Alairas, String CsatolmanyMegjegyzes)
    {
        //
        return "Ok";
    }

    private String UpLoadIrat(String FileSharedDirectoryName, String FileName, String IratId, String Azonosito, String Alairas, String CsatolmanyMegjegyzes)
    {
        ExecParam ExParam = new ExecParam();
        EREC_IraIratokService IraIratokService = new EREC_IraIratokService();
        String FileFullName = FileSharedDirectoryName + "\\" + FileName;
        ExParam.Record_Id = IratId;
        Result resII = IraIratokService.Get(ExParam);
        if (!String.IsNullOrEmpty(resII.ErrorCode))
        {
            Logger.Error("Irat azonosítása nem sikerült:" + resII.ErrorMessage + "!");
            return "{return=Irat azonosítása nem sikerült!}";
        }
        EREC_IraIratok IraIratok = (EREC_IraIratok)resII.Record;
        EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

        //erec_Csatolmanyok.BarCode = Azonosito;
        erec_Csatolmanyok.IraIrat_Id = IratId;

        Csatolmany csatolmanyIR = new Csatolmany();
        csatolmanyIR.BarCode = Azonosito;

        //Aláírás
        if ("I".CompareTo(Alairas) == 0)
        {
            String SignedFileName = Sign(FileFullName, 0);
            erec_Csatolmanyok.Leiras = "\"" + CsatolmanyMegjegyzes + "\" aláírt fájl";
            csatolmanyIR.Nev = FileName + ".sdxm";
            csatolmanyIR.SourceSharePath = FileSharedDirectoryName;
            Logger.Info("SharePoint feltöltés kezdete - aláírt file");
            Result _resULSigned = IraIratokService.CsatolmanyUpload(ExParam, erec_Csatolmanyok, csatolmanyIR);
            Logger.Info("SharePoint feltöltés vége - aláírt file");
            if (!String.IsNullOrEmpty(_resULSigned.ErrorCode))
            {
                Logger.Error("Hiba az aláírt file SharePoint feltöltése során:" + _resULSigned.ErrorMessage + "!");
                return "{return=Hiba az aláírt file SharePoint feltöltése során!}"; //hiba a SharePoint feltöltésben _resUL.ErrorMessage;
            }
        }
        erec_Csatolmanyok.Leiras = CsatolmanyMegjegyzes;
        csatolmanyIR.Nev = FileName;
        csatolmanyIR.SourceSharePath = FileSharedDirectoryName;
        Logger.Info("SharePoint feltöltés kezdete - eredeti file");

        Result _resUL = IraIratokService.CsatolmanyUpload(ExParam, erec_Csatolmanyok, csatolmanyIR);
        Logger.Info("SharePoint feltöltés vége - eredeti file");
        if (!String.IsNullOrEmpty(_resUL.ErrorCode))
        {
            Logger.Error("Hiba a SharePoint feltöltés során!" + _resUL.ErrorMessage);
            return "{return=Hiba a SharePoint feltöltés során!}"; //hiba a SharePoint feltöltésben _resUL.
        }

        return "Ok";

    }

    /// <summary>
    /// A scennelo altal adott meta info param fileban (a LoadDocument 2-dik paramja)
    /// adott ertekek valamelyiket adja vissza.
    /// </summary>
    /// <param name="valuesStr">String - a paramban kapott teljes file</param>
    /// <param name="key">String - a keresett kulcs neve: $FS_...   ; pl: $FS_USERNAME</param>
    /// <returns>String ertek vagy ha nincs, akkor String.Empty</returns>
    /// 
    private String GetFSErtek(String valuesStr, String key)
    {
        //System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
        //String valuesStr = enc.GetString(byteArr);

        if (valuesStr.IndexOf(key) != -1)
        {
            int start = valuesStr.IndexOf(key);
            int end = valuesStr.IndexOf("\n", start + 1);
            if (end == -1) end = valuesStr.Length;
            int egyenlo = valuesStr.IndexOf("=", start + 1);
            if (egyenlo > start && egyenlo < end)
            {
                return valuesStr.Substring(egyenlo + 1, end - egyenlo - 1);
            }
        }

        return String.Empty;
    }

} //class
