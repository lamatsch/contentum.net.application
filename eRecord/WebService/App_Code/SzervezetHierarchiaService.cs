using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;
//using System.EnterpriseServices;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public partial class SzervezetHierarchiaService : System.Web.Services.WebService
{
    private SzervezetHierarchiaStoredProcedure sp = null;
    
    private DataContext dataContext;

    public SzervezetHierarchiaService()
    {
        dataContext = new DataContext(this.Application);
        
        sp = new SzervezetHierarchiaStoredProcedure(dataContext);
    }

    public SzervezetHierarchiaService(DataContext _dataContext)
    {
         this.dataContext = _dataContext;
         sp = new SzervezetHierarchiaStoredProcedure(dataContext);
    }  

    [WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    [System.Xml.Serialization.XmlInclude(typeof(SzervezetHierarchiaSearch))]
    public Result GetAll(ExecParam ExecParam)
    {
        Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

        Result result = new Result();
        bool isConnectionOpenHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

            result = sp.GetAll(ExecParam);

        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }
        log.WsEnd(ExecParam, result);
        return result;
    }

    //[WebMethod(/*TransactionOption = TransactionOption.RequiresNew*/)]
    //[System.Xml.Serialization.XmlInclude(typeof(SzervezetHierarchiaSearch))]
    //public Result GetAll(ExecParam ExecParam, SzervezetHierarchiaSearch _SzervezetHierarchiaSearch)
    //{
    //    Contentum.eUtility.Log log = Contentum.eUtility.Log.WsStart(ExecParam, Context, GetType());

    //    Result result = new Result();
    //    bool isConnectionOpenHere = false;

    //    try
    //    {
    //        isConnectionOpenHere = dataContext.OpenConnectionIfRequired();

    //        result = sp.GetAll(ExecParam, _SzervezetHierarchiaSearch);

    //    }
    //    catch (Exception e)
    //    {
    //        result = ResultException.GetResultFromException(e);
    //    }
    //    finally
    //    {
    //        dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
    //    }
    //    log.WsEnd(ExecParam, result);
    //    return result;
    //}

}
