﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;

/// <summary>
/// Summary description for TomegesIktatasManager
/// </summary>
public class TomegesIktatasManager
{
    class IratWithUgyirat
    {
        public EREC_IraIratok Irat { get; set; }
        public EREC_UgyUgyiratok Ugyirat { get; set; }
    }

    private DataContext dataContext;
    private string _priority;

    public TomegesIktatasManager(DataContext _dataContext)
    {
        this.dataContext = _dataContext;
    }

    public Result TomegesIktatas_INIT(ExecParam execParam, Guid iktatokonyv_Id, Guid? erkeztetokonyv_Id, int? foszam, int iratDarabszam, int iratIrany, bool kuldemenyAzonositott)
    {
        Result result = new Result();
        List<IratWithUgyirat> iratok = new List<IratWithUgyirat>();
        List<string> kuldemenyIdList;

        //bejövő
        if (iratIrany == 1 && !kuldemenyAzonositott)
        {
            kuldemenyIdList = Kuldemenyek_INIT(execParam, erkeztetokonyv_Id, iratDarabszam);
        }
        else
        {
            kuldemenyIdList = Enumerable.Repeat("", iratDarabszam).ToList();
        }

        //főszámok létrehozása
        if (foszam == null)
        {
            EREC_IraIktatoKonyvek iktatokonyv;
            List<EREC_UgyUgyiratok> ugyiratok = Ugyiratok_INIT(execParam, iktatokonyv_Id, iratDarabszam, out iktatokonyv);
            Ugyiratdarabok_INIT(execParam, ugyiratok);
            iratok = Iratok_INIT(execParam, iktatokonyv, ugyiratok, kuldemenyIdList, iratIrany);
        }
        else
        {
            iratok = Iratok_INIT(execParam, iktatokonyv_Id, foszam.Value, kuldemenyIdList, iratIrany, iratDarabszam);
        }

        DataSet ds = new DataSet();
        DataTable table = new DataTable();
        table.Columns.Add("Id", typeof(Guid));
        table.Columns.Add("Azonosito", typeof(String));
        table.Columns.Add("Foszam", typeof(int));
        table.Columns.Add("Alszam", typeof(int));
        table.Columns.Add("Ugyirat_Id", typeof(Guid));
        table.Columns.Add("Kuldemenyek_Id", typeof(Guid));
        foreach (IratWithUgyirat irat in iratok)
        {
            DataRow row = table.NewRow();
            row["Id"] = irat.Irat.Id;
            row["Azonosito"] = irat.Irat.Azonosito;
            row["Foszam"] = irat.Ugyirat.Foszam;
            row["Alszam"] = irat.Irat.Alszam;
            row["Ugyirat_Id"] = string.IsNullOrEmpty(irat.Irat.Ugyirat_Id) ? Guid.Empty.ToString() : irat.Irat.Ugyirat_Id;
            row["Kuldemenyek_Id"] = string.IsNullOrEmpty(irat.Irat.KuldKuldemenyek_Id) ? Guid.Empty.ToString() : irat.Irat.KuldKuldemenyek_Id; ;
            table.Rows.Add(row);
        }
        ds.Tables.Add(table);

        result.Ds = ds;

        return result;
    }

    List<string> Kuldemenyek_INIT(ExecParam execParam, Guid? erkeztetokonyv_Id, int darabszam)
    {
        List<string> kuldemenyIdList = new List<string>();

        if (erkeztetokonyv_Id == null)
        {
            throw new ResultException("Nincs megadva érkeztetőkönyv!");
        }

        #region Érkeztetőkönyv GET

        EREC_IraIktatoKonyvekService service = new EREC_IraIktatoKonyvekService(this.dataContext);
        ExecParam IraIktatoKonyvekExecParam = execParam.Clone();

        IraIktatoKonyvekExecParam.Record_Id = erkeztetokonyv_Id.ToString();

        Result IraIktatoKonyvekResult = service.Get(IraIktatoKonyvekExecParam);
        if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
        {
            throw new ResultException(IraIktatoKonyvekResult);
        }

        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (IraIktatoKonyvekResult.Record as EREC_IraIktatoKonyvek);

        // lezárt érkeztetőkönyvbe nem lehet érkeztetni
        if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek))
        {
            throw new ResultException(52449);
        }

        #endregion

        int utolsoFoszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoFoszam);
        int erkeztetoszam = utolsoFoszam + 1;
        utolsoFoszam = utolsoFoszam + darabszam;

        #region Érkeztetőkönyv UPDATE

        erec_IraIktatoKonyvek.Updated.SetValueAll(false);
        erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);

        erec_IraIktatoKonyvek.UtolsoFoszam = utolsoFoszam.ToString();
        erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;

        erec_IraIktatoKonyvek.Base.Updated.Ver = true;

        IraIktatoKonyvekResult = service.Update(IraIktatoKonyvekExecParam, erec_IraIktatoKonyvek);
        if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
        {
            throw new ResultException(IraIktatoKonyvekResult);
        }
        #endregion

        EREC_KuldKuldemenyek kuldemeny = new EREC_KuldKuldemenyek();
        kuldemeny.Typed.IraIktatokonyv_Id = erkeztetokonyv_Id.Value;
        kuldemeny.Allapot = KodTarak.KULDEMENY_ALLAPOT.Foglalt;
        kuldemeny.Targy = "Tömeges iktatás folyamatban";
        //NOT NULL mezők
        kuldemeny.BeerkezesIdeje = DateTime.Now.ToString();
        kuldemeny.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        kuldemeny.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.Postai_sima;
        kuldemeny.Surgosseg = KodTarak.SURGOSSEG.Normal;
        kuldemeny.PeldanySzam = "1";

        List<string> erkezteto_SzamList = new List<string>();
        List<string> azonositoList = new List<string>();

        for (int i = erkeztetoszam; i < erkeztetoszam + darabszam; i++)
        {
            kuldemeny.Erkezteto_Szam = i.ToString();
            kuldemeny.Azonosito = Contentum.eRecord.BaseUtility.Kuldemenyek.GetFullErkeztetoSzam(execParam, erec_IraIktatoKonyvek, kuldemeny);

            erkezteto_SzamList.Add(kuldemeny.Erkezteto_Szam);
            azonositoList.Add(kuldemeny.Azonosito);
        }

        TomegesIktatasDataBaseManager sp = new TomegesIktatasDataBaseManager(this.dataContext);
        Result kuldemenyResult = sp.KuldKuldemenyekTomegesInit(execParam, kuldemeny, erkezteto_SzamList, azonositoList);
        kuldemenyResult.CheckError();

        foreach (DataRow row in kuldemenyResult.Ds.Tables[0].Rows)
        {
            kuldemenyIdList.Add(row[0].ToString());
        }

        return kuldemenyIdList;
    }

    List<EREC_UgyUgyiratok> Ugyiratok_INIT(ExecParam execParam, Guid iktatokonyv_Id, int darabszam, out EREC_IraIktatoKonyvek iktatokonyv)
    {
        List<EREC_UgyUgyiratok> ugyiratok = new List<EREC_UgyUgyiratok>();
        List<string> foszamList = new List<string>();
        List<string> azonositoList = new List<string>();

        #region Iktatókönyv GET

        EREC_IraIktatoKonyvekService service = new EREC_IraIktatoKonyvekService(this.dataContext);
        ExecParam IraIktatoKonyvekExecParam = execParam.Clone();

        IraIktatoKonyvekExecParam.Record_Id = iktatokonyv_Id.ToString();

        Result IraIktatoKonyvekResult = service.Get(IraIktatoKonyvekExecParam);
        if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
        {
            throw new ResultException(IraIktatoKonyvekResult);
        }

        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (IraIktatoKonyvekResult.Record as EREC_IraIktatoKonyvek);

        // lezárt iktatókönvybe nem lehet iktatni
        if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek))
        {
            throw new ResultException(52119);
        }

        iktatokonyv = erec_IraIktatoKonyvek;

        #endregion

        int utolsoFoszam = Int32.Parse(erec_IraIktatoKonyvek.UtolsoFoszam);
        int erkeztetoszam = utolsoFoszam + 1;
        utolsoFoszam = utolsoFoszam + darabszam;

        #region Érkeztetőkönyv UPDATE

        erec_IraIktatoKonyvek.Updated.SetValueAll(false);
        erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);

        erec_IraIktatoKonyvek.UtolsoFoszam = utolsoFoszam.ToString();
        erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;

        erec_IraIktatoKonyvek.Base.Updated.Ver = true;

        IraIktatoKonyvekResult = service.Update(IraIktatoKonyvekExecParam, erec_IraIktatoKonyvek);
        if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
        {
            throw new ResultException(IraIktatoKonyvekResult);
        }
        #endregion
        for (int i = erkeztetoszam; i < erkeztetoszam + darabszam; i++)
        {

            EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
            ugyirat.Typed.IraIktatokonyv_Id = iktatokonyv_Id;
            ugyirat.Allapot = KodTarak.UGYIRAT_ALLAPOT.Foglalt;
            ugyirat.Targy = "Tömeges iktatás folyamatban";
            ugyirat.UtolsoAlszam = "1";
            ugyirat.IratSzam = "1";
            ugyirat.Foszam = i.ToString();
            ugyirat.Azonosito = Contentum.eRecord.BaseUtility.Ugyiratok.GetFullFoszam(execParam, ugyirat, erec_IraIktatoKonyvek);

            foszamList.Add(ugyirat.Foszam);
            azonositoList.Add(ugyirat.Azonosito);
            ugyiratok.Add(ugyirat);
        }

        TomegesIktatasDataBaseManager sp = new TomegesIktatasDataBaseManager(this.dataContext);
        Result ugyiratResult = sp.UgyUgyiratokTomegesInit(execParam, ugyiratok[0], foszamList, azonositoList);
        ugyiratResult.CheckError();

        int j = 0;
        foreach (DataRow row in ugyiratResult.Ds.Tables[0].Rows)
        {
            ugyiratok[j].Id = row[0].ToString();
            j++;
        }

        return ugyiratok;
    }

    List<EREC_UgyUgyiratdarabok> Ugyiratdarabok_INIT(ExecParam execParam, List<EREC_UgyUgyiratok> ugyiratok)
    {
        List<EREC_UgyUgyiratdarabok> ugyiratdarabok = new List<EREC_UgyUgyiratdarabok>();
        List<string> ugyiratIdList = new List<string>();

        foreach (EREC_UgyUgyiratok ugyirat in ugyiratok)
        {
            EREC_UgyUgyiratdarabok ugyiratdarab = new EREC_UgyUgyiratdarabok();
            ugyiratdarab.UgyUgyirat_Id = ugyirat.Id;
            ugyiratdarab.Allapot = KodTarak.UGYIRATDARAB_ALLAPOT.Foglalt;
            ugyiratdarab.EljarasiSzakasz = KodTarak.ELJARASI_SZAKASZ.Osztatlan;
            ugyiratIdList.Add(ugyirat.Id);

            ugyiratdarabok.Add(ugyiratdarab);
        }

        TomegesIktatasDataBaseManager sp = new TomegesIktatasDataBaseManager(this.dataContext);
        Result iratResult = sp.UgyUgyiratdarabokTomegesInit(execParam, ugyiratdarabok[0], ugyiratIdList);
        iratResult.CheckError();

        int i = 0;
        foreach (DataRow row in iratResult.Ds.Tables[0].Rows)
        {
            ugyiratdarabok[i].Id = row[0].ToString();
            i++;
        }

        return ugyiratdarabok;
    }

    List<IratWithUgyirat> Iratok_INIT(ExecParam execParam, EREC_IraIktatoKonyvek iktatokonyv, List<EREC_UgyUgyiratok> ugyiratok, List<string> kuldemenyIdList, int iratIrany)
    {
        List<IratWithUgyirat> iratok = new List<IratWithUgyirat>();

        List<string> alszamList = new List<string>();
        List<string> azonositoList = new List<string>();
        List<string> ugyiratIdList = new List<string>();

        foreach (EREC_UgyUgyiratok ugyirat in ugyiratok)
        {
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.PostazasIranya = iratIrany.ToString();
            irat.UtolsoSorszam = "0";
            irat.IktatasDatuma = DateTime.Now.ToString();
            irat.Targy = "Tömeges iktatás folyamatban";
            irat.FelhasznaloCsoport_Id_Iktato = execParam.Felhasznalo_Id;
            irat.Allapot = KodTarak.IRAT_ALLAPOT.Foglalt;
            irat.Alszam = "1";
            irat.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(execParam, iktatokonyv, ugyirat, irat);
            irat.Ugyirat_Id = ugyirat.Id;

            alszamList.Add(irat.Alszam);
            azonositoList.Add(irat.Azonosito);
            ugyiratIdList.Add(ugyirat.Id);

            iratok.Add(new IratWithUgyirat { Irat = irat, Ugyirat = ugyirat });
        }

        TomegesIktatasDataBaseManager sp = new TomegesIktatasDataBaseManager(this.dataContext);
        Result iratResult = sp.IraIratokTomegesInit(execParam, iratok[0].Irat, alszamList, azonositoList, kuldemenyIdList, ugyiratIdList);
        iratResult.CheckError();

        int i = 0;
        foreach (DataRow row in iratResult.Ds.Tables[0].Rows)
        {
            iratok[i].Irat.Id = row[0].ToString();
            i++;
        }

        return iratok;
    }

    List<IratWithUgyirat> Iratok_INIT(ExecParam execParam, Guid iktatokonyv_Id, int foszam, List<string> kuldemenyIdList, int iratIrany, int darabszam)
    {
        List<IratWithUgyirat> iratok = new List<IratWithUgyirat>();

        List<string> alszamList = new List<string>();
        List<string> azonositoList = new List<string>();
        List<string> ugyiratIdList = new List<string>();

        EREC_IraIktatoKonyvekService service = new EREC_IraIktatoKonyvekService(this.dataContext);
        ExecParam IraIktatoKonyvekExecParam = execParam.Clone();

        IraIktatoKonyvekExecParam.Record_Id = iktatokonyv_Id.ToString();

        Result IraIktatoKonyvekResult = service.Get(IraIktatoKonyvekExecParam);
        if (!String.IsNullOrEmpty(IraIktatoKonyvekResult.ErrorCode))
        {
            throw new ResultException(IraIktatoKonyvekResult);
        }

        EREC_IraIktatoKonyvek iktatokonyv = (IraIktatoKonyvekResult.Record as EREC_IraIktatoKonyvek);


        EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, iktatokonyv_Id, foszam);
        ugyirat.Updated.SetValueAll(false);
        ugyirat.Base.Updated.SetValueAll(false);
        ugyirat.Base.Updated.Ver = true;

        Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz = Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(ugyirat);
        ErrorDetails errorDetail = null;
        ExecParam xpm = execParam.Clone();

        if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(xpm, ugyiratStatusz, out errorDetail) == false)
        {
            Logger.Warn("Állapotellenőrzés: alszámos iktatás nem lehetséges az ügyiratra", xpm);

            throw new ResultException(52109, errorDetail);
        }

        int utolsoAlszam = Int32.Parse(ugyirat.UtolsoAlszam);
        int alszam = utolsoAlszam + 1;
        utolsoAlszam = utolsoAlszam + darabszam;

        ugyirat.UtolsoAlszam = utolsoAlszam.ToString();
        ugyirat.Updated.UtolsoAlszam = true;

        int iratSzam = Int32.Parse(ugyirat.IratSzam);
        iratSzam = iratSzam + darabszam;

        ugyirat.IratSzam = iratSzam.ToString();
        ugyirat.Updated.IratSzam = true;

        EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam execParam_ugyiratUpdate = execParam.Clone();
        execParam_ugyiratUpdate.Record_Id = ugyirat.Id;
        Result result_ugyiratUpdate = ugyiratokService.Update(execParam_ugyiratUpdate, ugyirat);
        if (!String.IsNullOrEmpty(result_ugyiratUpdate.ErrorCode))
        {
            // hiba
            throw new ResultException(result_ugyiratUpdate);
        }

        for (int i = alszam; i < alszam + darabszam; i++)
        {
            EREC_IraIratok irat = new EREC_IraIratok();
            irat.PostazasIranya = iratIrany.ToString();
            irat.UtolsoSorszam = "0";
            irat.IktatasDatuma = DateTime.Now.ToString();
            irat.Targy = "Tömeges iktatás folyamatban";
            irat.FelhasznaloCsoport_Id_Iktato = execParam.Felhasznalo_Id;
            irat.Allapot = KodTarak.IRAT_ALLAPOT.Foglalt;
            irat.Alszam = i.ToString();
            irat.Azonosito = Contentum.eRecord.BaseUtility.Iratok.GetFullIktatoszam(execParam, iktatokonyv, ugyirat, irat);
            irat.Ugyirat_Id = ugyirat.Id;

            alszamList.Add(irat.Alszam);
            azonositoList.Add(irat.Azonosito);
            ugyiratIdList.Add(ugyirat.Id);

            iratok.Add(new IratWithUgyirat { Irat = irat, Ugyirat = ugyirat });
        }

        TomegesIktatasDataBaseManager sp = new TomegesIktatasDataBaseManager(this.dataContext);
        Result iratResult = sp.IraIratokTomegesInit(execParam, iratok[0].Irat, alszamList, azonositoList, kuldemenyIdList, ugyiratIdList);
        iratResult.CheckError();

        int j = 0;
        foreach (DataRow row in iratResult.Ds.Tables[0].Rows)
        {
            iratok[j].Irat.Id = row[0].ToString();
            j++;
        }

        return iratok;
    }

    EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, Guid iktatokonyv_Id, int foszam)
    {
        EREC_UgyUgyiratokService ugyiratokService = new EREC_UgyUgyiratokService(this.dataContext);
        EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
        ugyiratokSearch.IraIktatokonyv_Id.Filter(iktatokonyv_Id.ToString());
        ugyiratokSearch.Foszam.Filter(foszam.ToString());

        Result ugyiratResult = ugyiratokService.GetAll(execParam, ugyiratokSearch);
        ugyiratResult.CheckError();

        if (ugyiratResult.GetCount == 0)
        {
            throw new Exception("Az ügyirat nem található!");
        }

        if (ugyiratResult.GetCount > 1)
        {
            throw new Exception("Az adott főszámra több ügyirat található!");
        }

        DataRow row = ugyiratResult.Ds.Tables[0].Rows[0];
        EREC_UgyUgyiratok ugyirat = new EREC_UgyUgyiratok();
        Utility.LoadBusinessDocumentFromDataRow(ugyirat, row);

        return ugyirat;
    }

    public Result TomegesIktatas_EXEC(ExecParam execParam, Guid folyamatId)
    {
        Result hibasResult = null;

        var folyamat = new TomegesIktatasFolyamat(execParam, folyamatId);
        var resultFolyamatAllapot = folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.VegrehajtasFolyamatban);
        if (!resultFolyamatAllapot.IsError)
        {
            // prioritás lekérése
            var folyamatRecord = resultFolyamatAllapot.Record as EREC_TomegesIktatasFolyamat;
            if (folyamatRecord != null && _priority == null)
            {
                _priority = folyamatRecord.Typed.Prioritas.Value;
            }

            // tételek lekérése
            var tetelek = folyamat.GetTetelek(execParam, null);

            // tételek iktatása
            foreach (EREC_TomegesIktatasTetelek tetel in tetelek)
            {
                var result = TomegesIktatasTetel_EXEC(execParam, tetel.Typed.Id.Value);

                if (result.IsError)
                {
                    if (hibasResult == null) { hibasResult = result; }
                    Logger.Error(String.Format("TomegesIktatasTetel_EXEC hiba: {0}", tetel.Id), execParam, result);
                }

                // várakozás a tételek között, ha szükséges
                if (_priority == TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS.HatterszalonVarakozassal.ToString() || _priority == TOMEGES_IKTATAS_FOLYAMAT_PRIORITAS.JobVarakozassal.ToString())
                {
                    System.Threading.Thread.Sleep(3000);
                }
            }

            // Folyamat Vegrehajtott állapot beállítása
            folyamat.SetAllapot(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.Vegrehajtott);
        }
        return hibasResult ?? new Result();
    }

    public Result TomegesIktatasTetel_EXEC(ExecParam execParam, Guid tetelId)
    {
        Result result = new Result();

        bool isConnectionOpenHere = false;
        bool isTransactionBeginHere = false;

        try
        {
            isConnectionOpenHere = dataContext.OpenConnectionIfRequired();
            isTransactionBeginHere = dataContext.BeginTransactionIfRequired();

            EREC_TomegesIktatasTetelek tetel = GetTetel(execParam, tetelId);

            if (tetel.Allapot == KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Inicializalt)
            {
                try
                {
                    result = Iktatas(tetel);
                    SetTetelAllapot(execParam, tetel, KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Iktatott, null);
                    // COMMIT
                    dataContext.CommitIfRequired(isTransactionBeginHere);
                }
                catch (Exception ex)
                {
                    dataContext.RollbackIfRequired(isTransactionBeginHere);
                    result = ResultException.GetResultFromException(ex);
                    SetTetelAllapot(execParam, tetel, KodTarak.TOMEGES_IKTATAS_TETEL_ALLAPOT.Hibas, result.ErrorMessage);
                }
            }
        }
        catch (Exception e)
        {
            result = ResultException.GetResultFromException(e);
        }
        finally
        {
            dataContext.CloseConnectionIfRequired(isConnectionOpenHere);
        }

        return result;
    }

    EREC_TomegesIktatasTetelek GetTetel(ExecParam execParam, Guid tetelId)
    {
        EREC_TomegesIktatasTetelekService tetelekService = new EREC_TomegesIktatasTetelekService(this.dataContext);
        ExecParam getExecParam = execParam.Clone();
        getExecParam.Typed.Record_Id = tetelId;

        Result tetelekResult = tetelekService.Get(getExecParam);
        tetelekResult.CheckError();

        EREC_TomegesIktatasTetelek tetel = tetelekResult.Record as EREC_TomegesIktatasTetelek;

        return tetel;
    }

    void SetTetelAllapot(ExecParam execParam, EREC_TomegesIktatasTetelek tetel, string allapot, string hiba)
    {
        EREC_TomegesIktatasTetelekService service = new EREC_TomegesIktatasTetelekService(this.dataContext);

        tetel.Updated.SetValueAll(false);
        tetel.Base.Updated.SetValueAll(false);
        tetel.Base.Updated.Ver = true;

        tetel.Allapot = allapot;
        tetel.Updated.Allapot = true;
        if (!String.IsNullOrEmpty(hiba))
        {
            tetel.Hiba = hiba;
            tetel.Updated.Hiba = true;
        }

        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = tetel.Id;
        Result res = service.Update(xpm, tetel);

        if (res.IsError)
        {
            Logger.Error("EREC_TomegesIktatasTetelek.Update hiba", xpm, res);
        }
    }

    Result Iktatas(EREC_TomegesIktatasTetelek tetel)
    {
        Result result = new Result();

        TomegesIktatasTetelekBusinessObjects tetelWithBo = new TomegesIktatasTetelekBusinessObjects(tetel);
        tetelWithBo.DeserializeBusinessObects();

        ExecParam execParam = tetelWithBo.ExecParam;

        if (tetelWithBo.ErkeztetesParameterek == null)
        {
            tetelWithBo.ErkeztetesParameterek = new ErkeztetesParameterek();
        }
        tetelWithBo.ErkeztetesParameterek.TomegesErkeztetesVegrehajtasa = true;
        tetelWithBo.ErkeztetesParameterek.TomegesErkeztetesDatuma = tetel.Base.Typed.LetrehozasIdo.Value;

        if (tetelWithBo.IktatasiParameterek == null)
        {
            tetelWithBo.IktatasiParameterek = new IktatasiParameterek();
        }
        tetelWithBo.IktatasiParameterek.TomegesIktatasVegrehajtasa = true;
        tetelWithBo.IktatasiParameterek.TomegesIktatasDatuma = tetel.Base.Typed.LetrehozasIdo.Value;

        if (String.IsNullOrEmpty(tetel.Iktatokonyv_Id))
        {
            throw new Exception("Iktatokonyv_Id paraméter üres!");
        }

        if (String.IsNullOrEmpty(tetel.Irat_Id))
        {
            throw new Exception("Irat_Id paraméter üres!");
        }

        if (tetelWithBo.EREC_IraIratok == null)
        {
            throw new Exception("EREC_IraIratok paraméter üres!");
        }

        if (tetelWithBo.EREC_UgyUgyiratdarabok == null)
        {
            throw new Exception("EREC_UgyUgyiratdarabok paraméter üres!");
        }

        if (tetel.Alszamra == "1")
        {
            if (String.IsNullOrEmpty(tetel.Ugyirat_Id))
            {
                throw new Exception("Ugyirat_Id paraméter üres!");
            }
        }
        else
        {
            if (tetelWithBo.EREC_UgyUgyiratok == null)
            {
                throw new Exception("EREC_UgyUgyiratok paraméter üres!");
            }
        }

        if (tetel.IktatasTipus == "0")
        {
            if (String.IsNullOrEmpty(tetel.Kuldemeny_Id) && tetelWithBo.EREC_KuldKuldemenyek == null)
            {
                throw new Exception("Kuldemeny_Id vagy EREC_KuldKuldemenyek paraméter üres!");
            }
        }
        else
        {
            if (tetelWithBo.EREC_PldIratPeldanyok == null)
            {
                throw new Exception("EREC_PldIratPeldanyok paraméter üres!");
            }
        }

        EREC_IraIratok irat = GetIrat(execParam, tetel.Irat_Id);
        irat.Updated.SetValueAll(false);
        irat.Base.Updated.SetValueAll(false);
        irat.Base.Updated.Ver = true;
        CopyProperties(tetelWithBo.EREC_IraIratok, irat, new EREC_IraIratok());

        EREC_UgyUgyiratdarabok ugyiratDarab = GetUgyiratdarab(execParam, irat.UgyUgyIratDarab_Id);
        ugyiratDarab.Updated.SetValueAll(false);
        ugyiratDarab.Base.Updated.SetValueAll(false);
        ugyiratDarab.Base.Updated.Ver = true;
        CopyProperties(tetelWithBo.EREC_UgyUgyiratdarabok, ugyiratDarab, new EREC_UgyUgyiratdarabok());

        EREC_UgyUgyiratok ugyirat = GetUgyirat(execParam, irat.Ugyirat_Id);
        ugyirat.Updated.SetValueAll(false);
        ugyirat.Base.Updated.SetValueAll(false);
        ugyirat.Base.Updated.Ver = true;

        if (tetel.Alszamra == "0")
        {
            CopyProperties(tetelWithBo.EREC_UgyUgyiratok, ugyirat, new EREC_UgyUgyiratok());
            ugyirat.Updated.IratSzam = false;
        }

        EREC_IraIratokService iratokService = new EREC_IraIratokService(this.dataContext);
        EREC_KuldKuldemenyekService kuldemenyekService = new EREC_KuldKuldemenyekService(this.dataContext);

        //bejövő irat iktatás
        if (tetel.IktatasTipus == "0")
        {
            string kuldemenyId = String.Empty;
            if (!String.IsNullOrEmpty(tetel.Kuldemeny_Id))
            {
                kuldemenyId = tetel.Kuldemeny_Id;
            }
            else
            {
                kuldemenyId = irat.KuldKuldemenyek_Id;
                //érkeztetés
                EREC_KuldKuldemenyek kuldemeny = GetKuldemeny(execParam, irat.KuldKuldemenyek_Id);
                CopyProperties(tetelWithBo.EREC_KuldKuldemenyek, kuldemeny, new EREC_KuldKuldemenyek());
                result = kuldemenyekService.Erkeztetes(execParam, kuldemeny, null, tetelWithBo.ErkeztetesParameterek);
                result.CheckError();
            }

            tetelWithBo.IktatasiParameterek.KuldemenyId = kuldemenyId;

            //alszámos iktatás
            if (tetel.Alszamra == "1")
            {
                result = iratokService.BejovoIratIktatasa_Alszamra(execParam, tetel.Iktatokonyv_Id, tetel.Ugyirat_Id,
                    ugyiratDarab, irat, tetelWithBo.EREC_HataridosFeladatok, tetelWithBo.IktatasiParameterek);
            }
            else
            {
                result = iratokService.BejovoIratIktatasa(execParam, tetel.Iktatokonyv_Id, ugyirat,
                    ugyiratDarab, irat, tetelWithBo.EREC_HataridosFeladatok, tetelWithBo.IktatasiParameterek);
            }
        }
        //belső irat iktatás
        else
        {
            //alszámos iktatás
            if (tetel.Alszamra == "1")
            {
                result = iratokService.BelsoIratIktatasa_Alszamra(execParam, tetel.Iktatokonyv_Id, tetel.Ugyirat_Id,
                    ugyiratDarab, irat, tetelWithBo.EREC_HataridosFeladatok, tetelWithBo.EREC_PldIratPeldanyok
                    , tetelWithBo.IktatasiParameterek);
            }
            else
            {
                result = iratokService.BelsoIratIktatasa(execParam, tetel.Iktatokonyv_Id, ugyirat,
                    ugyiratDarab, irat, tetelWithBo.EREC_HataridosFeladatok, tetelWithBo.EREC_PldIratPeldanyok
                    , tetelWithBo.IktatasiParameterek);
            }
        }

        result.CheckError();

        return result;
    }

    void CopyProperties(object source, object dest, object def)
    {
        System.Reflection.PropertyInfo[] properties = dest.GetType().GetProperties();

        System.Reflection.FieldInfo updatedField = dest.GetType().GetField("Updated");
        object destUpdateObject = updatedField.GetValue(dest);

        System.Reflection.PropertyInfo[] updatedProperties = destUpdateObject.GetType().GetProperties();


        foreach (PropertyInfo p in properties)
        {
            object sourceValue = p.GetValue(source, null);
            object destValue = p.GetValue(dest, null);
            object defValue = p.GetValue(def, null);

            if (!Object.Equals(sourceValue, defValue))
            {
                p.SetValue(dest, sourceValue, null);
                PropertyInfo up = updatedProperties.Where(pp => pp.Name == p.Name).FirstOrDefault();
                if (up != null) up.SetValue(destUpdateObject, true, null);
            }
        }
    }

    EREC_IraIratok GetIrat(ExecParam execParam, string id)
    {
        EREC_IraIratokService service = new EREC_IraIratokService(this.dataContext);
        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = id;

        Result res = service.Get(xpm);
        res.CheckError();

        return res.Record as EREC_IraIratok;
    }

    EREC_UgyUgyiratok GetUgyirat(ExecParam execParam, string id)
    {
        EREC_UgyUgyiratokService service = new EREC_UgyUgyiratokService(this.dataContext);
        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = id;

        Result res = service.Get(xpm);
        res.CheckError();

        return res.Record as EREC_UgyUgyiratok;
    }

    EREC_UgyUgyiratdarabok GetUgyiratdarab(ExecParam execParam, string id)
    {
        EREC_UgyUgyiratdarabokService service = new EREC_UgyUgyiratdarabokService(this.dataContext);
        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = id;

        Result res = service.Get(xpm);
        res.CheckError();

        return res.Record as EREC_UgyUgyiratdarabok;
    }

    EREC_KuldKuldemenyek GetKuldemeny(ExecParam execParam, string id)
    {
        EREC_KuldKuldemenyekService service = new EREC_KuldKuldemenyekService(this.dataContext);
        ExecParam xpm = execParam.Clone();
        xpm.Record_Id = id;

        Result res = service.Get(xpm);
        res.CheckError();

        return res.Record as EREC_KuldKuldemenyek;
    }

    const int initIntervalMin = 10;
    public Result TomegesIktatas_EXEC_Job(ExecParam execParam)
    {
        Logger.Debug("TomegesIktatas_EXEC_Job:");
        Result result = new Result();

        EREC_TomegesIktatasFolyamatService folyamatokService = new EREC_TomegesIktatasFolyamatService();
        EREC_TomegesIktatasFolyamatSearch folyamatokSearch = new EREC_TomegesIktatasFolyamatSearch();
        folyamatokSearch.Allapot.In(new string[] {
            KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.InicializalasFolyamatban,
            KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.VegrehajtasFolyamatban
        });
        Logger.Debug("TomegesIktatas_EXEC_Job: Folyamatban lévő FolyamatokSearch Prepare");

        Result folyamatokResult = folyamatokService.GetAll(execParam, folyamatokSearch);
        folyamatokResult.CheckError();
        Logger.Debug("TomegesIktatas_EXEC_Job: Folyamatban lévő FolyamatokSearch OK");

        if (folyamatokResult.GetCount > 0)
        {
            Logger.Debug("TomegesIktatas_EXEC_Job:Fut inicializálás vagy végrehajtás!");
            return result;
        }

        folyamatokSearch = new EREC_TomegesIktatasFolyamatSearch();
        folyamatokSearch.Allapot.Filter(KodTarak.TOMEGES_IKTATAS_FOLYAMAT_ALLAPOT.Inicializalt);
        folyamatokSearch.OrderBy = "LetrehozasIdo ASC";

        folyamatokResult = folyamatokService.GetAll(execParam, folyamatokSearch);

        folyamatokResult.CheckError();
        Logger.Debug("TomegesIktatas_EXEC_Job: FolyamatokSearch OK");
        if (folyamatokResult.GetCount == 0)
        {
            Logger.Debug("TomegesIktatas_EXEC_Job: Nincs inicializált folyamat!");
            return result;
        }

        EREC_TomegesIktatasFolyamat folyamat = new EREC_TomegesIktatasFolyamat();
        Utility.LoadBusinessDocumentFromDataRow(folyamat, folyamatokResult.Ds.Tables[0].Rows[0]);

        DateTime date = DateTime.Now.AddMinutes(-1.0 * initIntervalMin);
        DateTime modositasiIdo = folyamat.Base.Typed.ModositasIdo.IsNull ? folyamat.Base.Typed.LetrehozasIdo.Value : folyamat.Base.Typed.ModositasIdo.Value;

        if (modositasiIdo > date)
        {
            Logger.Debug("TomegesIktatas_EXEC_Job: A folyamat kevesebb mint 10 perce lett inicializálva!");
            return result;
        }

        _priority = folyamat.Typed.Prioritas.Value;
        TomegesIktatas_EXEC(execParam, folyamat.Typed.Id.Value);

        //Újra meghívódik a metódus, van-e még végrehajtandó folyamat 
        bool isSuccess = ThreadPool.QueueUserWorkItem(ExecuteTomegesIktatasExecJobFromThread, new ThreadParams
        {
            HttpContextReference = HttpContext.Current,
            ExecParam = execParam
        });

        Logger.Debug("TomegesIktatas_EXEC_Job: Új végrehajtási szál ütemezése succes: " + isSuccess);
        return result;
    }
    class ThreadParams
    {
        public HttpContext HttpContextReference { get; set; }
        public ExecParam ExecParam { get; set; }
    }

    void ExecuteTomegesIktatasExecJobFromThread(object data)
    {
        Logger.Debug("ExecuteTomegesIktatasExecJobFromThread. Thread: " + Thread.CurrentThread.ManagedThreadId);
        try
        {
            HttpContext.Current = (data as ThreadParams).HttpContextReference;
            var execParam = (data as ThreadParams).ExecParam;
            TomegesIktatas_EXEC_Job(execParam);
        }
        catch (Exception e)
        {
            Logger.Error("ExecuteTomegesIktatasExecJobFromThread hiba: " + e.Message,e);
        }
    }
}