﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Contentum.eRecordWebService.eBeadvanyok
{
    /// <summary>
    /// Summary description for PostProcessManager
    /// </summary>
    public class PostProcessManager
    {
        private class Configuration
        {
            public int ErkeztetveFromDays = 30; 
        }

        Configuration config = new Configuration();

        public PostProcessManager()
        {
        }

        public ExecParam GetAdminExecParam()
        {
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
            return execParam;
        }

        public void ProcessFailedVevenyek_Job()
        {
            Logger.Info("PostProcessManager - ProcessFailedVevenyek_Job kezdete");

            ExecParam execParam = GetAdminExecParam();

            DataTable vevenyekTable = GetFailedVevenyek(execParam);

            int count = vevenyekTable.Rows.Count;

            Logger.Debug(String.Format("Vevenyek szama: {0}", count));

            foreach (DataRow row in vevenyekTable.Rows)
            {
                EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
                Utility.LoadBusinessDocumentFromDataRow(eBeadvany, row);

                ProcessFaliledVeveny(execParam, eBeadvany);
            }

            Logger.Info("PostProcessManager - ProcessFailedVevenyek_Job vege");
        }

        DataTable GetFailedVevenyek(ExecParam execParam)
        {
            EREC_eBeadvanyokService eBeadvanyokService = new EREC_eBeadvanyokService();

            DateTime erkeztetesiDatumTol = DateTime.Today.AddDays(-1 * config.ErkeztetveFromDays);

            EREC_eBeadvanyokSearch eBeadvanyokSearch = new EREC_eBeadvanyokSearch();
            eBeadvanyokSearch.FeldolgozasStatusz.Filter(eBeadvanyokConstants.FeldolgozasStatusz.Hibas);
            eBeadvanyokSearch.KR_ErkeztetesiDatum.GreaterOrEqual(erkeztetesiDatumTol.ToShortDateString());
            eBeadvanyokSearch.OrderBy = "KR_ErkeztetesiDatum asc";

            Result eBeadvanyokResult = eBeadvanyokService.GetAll(execParam, eBeadvanyokSearch);
            eBeadvanyokResult.CheckError();

            return eBeadvanyokResult.Ds.Tables[0];
        }

        void ProcessFaliledVeveny(ExecParam execParam, EREC_eBeadvanyok eBeadvany)
        {
            EREC_eBeadvanyokService eBeadvanyokService = new EREC_eBeadvanyokService();
            try
            {
                eBeadvanyokService.PostProcessInternal(execParam, eBeadvany, false);
            }
            catch (Exception ex)
            {
                Result result = ResultException.GetResultFromException(ex);
                Logger.Warn(String.Format("ProcessFaliledVeveny({0}) hiba", eBeadvany.Id), execParam, result);
                string hiba = ResultError.GetErrorMessageFromResultObject(result);
                eBeadvanyokService.SetProcessError(execParam, eBeadvany.Id, hiba);
            }
        }
    }
}