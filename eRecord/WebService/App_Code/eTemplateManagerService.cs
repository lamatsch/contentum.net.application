﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.UI;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

/// <summary>
/// Summary description for eTemplateManagerService
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebService")]
//[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[SoapRpcService(Use = System.Web.Services.Description.SoapBindingUse.Literal, RoutingStyle = SoapServiceRoutingStyle.SoapAction)]
public class eTemplateManagerService : System.Web.Services.WebService {

    /*public eTemplateManagerService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }*/

    /// <summary>
    /// A háttérben lefutott TemplateManager eredményének a kezelése
    /// </summary>
    /// <param name="res" >A TemplateManagertől visszérkezik egy result (res), amely tartalmazza a kész dokumentumot (res.Record) és generálást indító felhasználó id-ját (res.Uid), </param>
    /// <param name="name">ezen kívül pedig az elkészült dokumentum nevét (ennek formátuma:'dokumentum tartalmára utaló név'_'készítés kezdete(dátum + idő)'.kiterjesztés)</param>
    /// <returns></returns>
    [WebMethod(Description = "A TemplateManagertől visszérkezik egy result (res), amely tartalmazza a kész dokumentumot (res.Record) és generálást indító felhasználó id-ját (res.Uid), ezen kívül pedig az elkészült dokumentum nevét (ennek formátuma:'dokumentum tartalmára utaló név'_'készítés kezdete(dátum + idő)'.kiterjesztés).")]
    public int CallBack(Contentum.eBusinessDocuments.Result res, string name)
    {
        Result result = new Result();
        //ExecParam execParam = UI.SetExecParamDefault(new Page(), new ExecParam());
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = res.Uid;

        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
        
        string doktarSitePath = ds.GetTemplateManagerSitePath();
        string doktarDocLibPath = ds.GetTemplateManagerDocLib();
        string doktarFolderPath = "eTemplateManager";
        string documentStoreType = "SharePoint";
        
        result = ds.DirectUploadWithCheckin(execParam, documentStoreType, doktarSitePath, doktarDocLibPath, doktarFolderPath, name, (byte[])res.Record, "1");

        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            KRT_FelhasznalokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
            execParam.Record_Id = res.Uid;

            res = service.Get(execParam);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                return -1;
            }

            KRT_Felhasznalok felhasznalo = (res.Record as KRT_Felhasznalok);

            string NotifiedAddress = felhasznalo.EMail;

            string url = result.Record.ToString();
            
            Contentum.eAdmin.Service.EmailService es = Contentum.eUtility.eAdminService.ServiceFactory.GetEmailService();
            es.SendDocReady(execParam, NotifiedAddress, url);

            return 0;
        }
        else
        {
            return -1;
        }
    }
    
}

