﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;

public partial class LejartMegorzesiIdo : System.Web.UI.Page
{
    protected void Page_PreRender(object sender, EventArgs e)
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Test_Click(object sender, EventArgs e)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        execParam.FelhasznaloSzervezet_Id = "BD00C8D0-CF99-4DFC-8792-33220B7BFCC6";
        ErtesitesService svc = new ErtesitesService();
        try
        {
            //Result res1 = svc.Ertesites_UgyUgyiratok_LejartMegorzesiIdo_LeveltariAtadas(execParam.Felhasznalo_Id, null);
            //res1.CheckError();

            Result res2 = svc.Ertesites_UgyUgyiratok_LejartMegorzesiIdo_Selejtezes(execParam.Felhasznalo_Id, null);
            res2.CheckError();
        }
        catch (Exception ex)
        {
            Result error = ResultException.GetResultFromException(ex);
            Response.Write(String.Format("ErrorCode: {0}, ErrorMessage {1}", error.ErrorCode, error.ErrorMessage));
        }
    }
}