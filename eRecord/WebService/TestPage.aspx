﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestPage.aspx.cs" Inherits="TestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button ID="btnTest" runat="server" Text="Teszt" OnClick="btnTest_Click" />
        </div>
        <div>
            <asp:Label ID="output" runat="server" />
        </div>
        <hr />
        <div>
            <div>
                KRID
            </div>
            <div>
                <asp:TextBox ID="krid" runat="server" />
            </div>
            <div>
                <asp:FileUpload ID="file" runat="server" />
            </div>
            <div>
                <asp:Button runat="server" ID="btnEncrypt" OnClick="btnEncrypt_Click" Text="Titkosít" />
            </div>
            <div>
                <asp:Label ID="outputEncrypt" runat="server" />
            </div>
        </div>



        <br />
        <br />
        <br />
        <br />
        Év
        <asp:TextBox ID="TextBoxUgyiratLezarasEllenorzes" runat="server" />
        <br />
        <asp:Button runat="server" ID="ButtonUgyiratLezarasEllenorzes" Text="Ügyirat lezárás ellenőrzés" OnClick="ButtonUgyiratLezarasEllenorzes_Click" />
        <br />
    </form>
</body>
</html>
