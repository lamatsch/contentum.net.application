﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.IO;

public partial class TestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        EREC_IraJegyzekekService service = new EREC_IraJegyzekekService();
        ExecParam xpm = new ExecParam();
        xpm.Felhasznalo_Id = "827F7449-4E45-E711-80C2-00155D020D7E";

        try
        {
            string[] files;
            Result result = service.CreateLeveltariAdatCsomag(xpm, "24e236c3-2e00-e811-80c9-00155d020dd3", null, out files);

            if (result.IsError)
            {
                output.Text = result.ErrorMessage;
            }
            else
            {
                foreach (string file in files)
                {
                    output.Text += file + "<br/>";
                }
            }
        }
        catch (Exception ex)
        {
            output.Text = ex.Message;
        }



    }

    protected void btnEncrypt_Click(object sender, EventArgs e)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";

        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        Result kulcsResult = svc.PublikusKulcsLekerdezese("BPFPH", null, krid.Text);

        if (kulcsResult.IsError)
        {
            outputEncrypt.Text = kulcsResult.ErrorMessage;
            return;
        }

        string publicKey = kulcsResult.Record as string;

        GPGEncryptFileManager efm = new GPGEncryptFileManager(execParam);
        string error;
        byte[] result = efm.EncryptFile(file.FileName, file.FileBytes, publicKey, out error);
        if (error != null)
        {
            outputEncrypt.Text = error;
        }
        else
        {
            outputEncrypt.Text = Path.Combine(@"C:\Temp", file.FileName + ".enc");
            File.WriteAllBytes(outputEncrypt.Text, result);
        }
    }

    protected void ButtonUgyiratLezarasEllenorzes_Click(object sender, EventArgs e)
    {
        throw new Exception("TEST ONLY");

        if (string.IsNullOrEmpty(TextBoxUgyiratLezarasEllenorzes.Text))
        {
            throw new ArgumentNullException();
        }
        EREC_UgyUgyiratokService svc = new EREC_UgyUgyiratokService();
        svc.UgyiratLezarasEllenorzesSimple("54E861A5-36ED-44CA-BAA7-C287D125B309", "BD00C8D0-CF99-4DFC-8792-33220B7BFCC6", int.Parse(TextBoxUgyiratLezarasEllenorzes.Text));
    }
}