﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="eBeadvanyTestPage.aspx.cs" Inherits="eBeadvanyTestPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            Érkeztetési szám:
        </div>
        <div>
            <asp:TextBox ID="ErkeztetesiSzam" runat="server" Text="" Width="500px"/>
        </div>
        <div>
            DokTipus:
        </div>
        <div>
            <asp:DropDownList ID="DokTipus" runat="server" Width="500px">
                <asp:ListItem Text="FeladasiIgazolas" />
                <asp:ListItem Text="AtveteliErtesito" />
                <asp:ListItem Text="LetoltesiIgazolas" />
                <asp:ListItem Text="MeghiusulasiIgazolas" />
            </asp:DropDownList>
        </div>
        <div>
            Hivatkozási szám:
        </div>
        <div>
            <asp:TextBox ID="hivatkozasiSzam" runat="server" Text="101098399201711141617949404" Width="500px"/>
        </div>
        <div>
            Fájl:
        </div>
        <div>
            <asp:FileUpload ID="KR_File" runat="server" Width="500px"/>
        </div>
        <div>
            <asp:Button ID="Test" runat="server" OnClick="Test_Click" Text="Teszt"/>
        </div>
        <div>
            Eredmény:
        </div>
        <div>
            <asp:Label ID="output" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>
