﻿using Contentum.eBusinessDocuments;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;

public partial class eBeadvanyTestPage : System.Web.UI.Page
{
    protected void Page_PreRender(object sender, EventArgs e)
    {
        ErkeztetesiSzam.Text = DateTime.Now.Ticks.ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Test_Click(object sender, EventArgs e)
    {
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = "827F7449-4E45-E711-80C2-00155D020D7E";
        //execParam.Felhasznalo_Id = "54E861A5-36ED-44CA-BAA7-C287D125B309";
        EREC_eBeadvanyokService service = new EREC_eBeadvanyokService();

        EREC_eBeadvanyok eBeadvany = new EREC_eBeadvanyok();
        eBeadvany.KR_HivatkozasiSzam = hivatkozasiSzam.Text;
        eBeadvany.KR_ErkeztetesiDatum = DateTime.Now.ToString();
        eBeadvany.KR_ErkeztetesiSzam = ErkeztetesiSzam.Text;
        eBeadvany.KR_DokTipusAzonosito = DokTipus.Text;
        eBeadvany.KR_FileNev = KR_File.FileName;
        eBeadvany.KR_Rendszeruzenet = "1";
        eBeadvany.Cel = "WEB";
        eBeadvany.Irany = "0";

        Csatolmany csatolmany = new Csatolmany();
        csatolmany.Nev = KR_File.FileName;
        csatolmany.Tartalom = KR_File.FileBytes;


        try
        {
            Result res = service.InsertAndAttachDocument(execParam, eBeadvany, new Csatolmany[] { csatolmany });

            if (res.IsError)
            {
                throw new ResultException(res);
            }
        }
        catch (Exception ex)
        {
            Result error = ResultException.GetResultFromException(ex);
            Write(String.Format("ErrorCode: {0}, ErrorMessage {1}", error.ErrorCode, error.ErrorMessage));
        }
    }

    void Write(string msg)
    {
        output.Text = msg;
    }

    public static string Serialize(object value)
    {
        if (value == null)
        {
            return string.Empty;
        }
        try
        {
            var xmlserializer = new XmlSerializer(value.GetType());
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter))
            {
                xmlserializer.Serialize(writer, value);
                return stringWriter.ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("An error occurred", ex);
        }
    }
}