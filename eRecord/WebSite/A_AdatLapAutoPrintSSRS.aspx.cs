﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Text;

public partial class A_AdatLapAutoPrintSSRS : Contentum.eUtility.UI.PageBase
{
    //Irat id tárolására
    private string ids = String.Empty;
    private string ugy_fajtaja = String.Empty;

    #region not used
    private string[] uid = { };
    private string riport_path = String.Empty;
    private string docId = String.Empty;
    private string iratIdsForServices = String.Empty;
    private string executor = String.Empty;
    private string szervezet = String.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //Irat id kiszedése a query stringből
        if (Request.QueryString[QueryStringVars.IratId] != null)
        {
            iratIdsForServices = ConvertToServiceIdList(Request.QueryString[QueryStringVars.IratId]);
            ids = Request.QueryString[QueryStringVars.IratId].ToString();
            uid = ids.Split(',');

            ugy_fajtaja = Request.QueryString[QueryStringVars.Ugy_fajtaja].ToString();

        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Report server paraméterek
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);

            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;

            //Riport elérési út
            riport_path = ReportViewer1.ServerReport.ReportPath;
            riport_path += "/A_AdatLap";




            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + riport_path;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            //Riport paraméterek beállítása
            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }

    private string ConvertToServiceIdList(string ids)
    {
        StringBuilder result = new StringBuilder();
        string[] splitted = ids.Split(',');
        bool first = true;
        foreach (var item in splitted)
        {
            if (first)
                first = false;
            else
                result.Append(",");

            result.Append("'");
            result.Append(item);
            result.Append("'");
        }
        return result.ToString();
    }


    //Riport paraméterek beállítása
    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                ReportParameters[0] = new ReportParameter(rpis[0].Name);
                ReportParameters[0].Values.AddRange(uid);

                ReportParameters[1] = new ReportParameter(rpis[1].Name);
                ReportParameters[1].Values.Add(ugy_fajtaja);

            }

        }

        return ReportParameters;
    }
}

