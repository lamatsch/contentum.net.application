<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="AgazatiJelekForm.aspx.cs" Inherits="AgazatiJelekForm" Title="Ágazati jelek karbantartása" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 868px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKodStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="labelKod" runat="server" Text="Kód:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="RequiredTextBox_Kod" runat="server" Validate="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="labelNevStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="labelNev" runat="server" Text="Megnevezés:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="RequiredTextBox_Nev" runat="server" CssClass="mrUrlapInput" TextBoxMode="MultiLine"
                                        Rows="3"  Validate="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelErvenyessegStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                    </uc6:ErvenyessegCalendarControl>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                &nbsp; &nbsp;&nbsp; &nbsp;
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
