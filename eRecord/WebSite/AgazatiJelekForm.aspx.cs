using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class AgazatiJelekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    #region utility
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }
    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        RequiredTextBox_Nev.TextBox.Enabled = false;
        RequiredTextBox_Kod.TextBox.Enabled = false;
        //ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        //ErvenyessegCalendarControl1.Enable_ErvVege = false;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelKod.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        //RequiredTextBox_Kod.TextBox.Enabled = false;
        //labelKod.CssClass = "mrUrlapInputWaterMarked";

        RequiredTextBox_Kod.Validate = true;
        RequiredTextBox_Nev.Validate = true;
    }

    /// <summary>
    /// New m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetNewControls()
    {
        RequiredTextBox_Kod.Validate = true;
        RequiredTextBox_Nev.Validate = true;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
       
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AgazatiJel" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_AgazatiJelek EREC_AgazatiJelek = (EREC_AgazatiJelek)result.Record;
                    LoadComponentsFromBusinessObject(EREC_AgazatiJelek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        
        if (Command == CommandName.New)
        {
            SetNewControls();
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.AgazatiJelekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_AgazatiJelek erec_AgazatiJelek)
    {
        RequiredTextBox_Kod.Text = erec_AgazatiJelek.Kod;
        RequiredTextBox_Nev.Text = erec_AgazatiJelek.Nev;      
        
        ErvenyessegCalendarControl1.ErvKezd = erec_AgazatiJelek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_AgazatiJelek.ErvVege;

        FormHeader1.Record_Ver = erec_AgazatiJelek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_AgazatiJelek.Base);
    }

    // form --> business object
    private EREC_AgazatiJelek GetBusinessObjectFromComponents()
    {
        EREC_AgazatiJelek erec_AgazatiJelek = new EREC_AgazatiJelek();
        // �sszes mez� update-elhet�s�g�t kezdetben letiltani:
        erec_AgazatiJelek.Updated.SetValueAll(false);
        erec_AgazatiJelek.Base.Updated.SetValueAll(false);

        erec_AgazatiJelek.Kod = RequiredTextBox_Kod.Text;
        erec_AgazatiJelek.Updated.Kod = pageView.GetUpdatedByView(RequiredTextBox_Kod);
        erec_AgazatiJelek.Nev = RequiredTextBox_Nev.Text;
        erec_AgazatiJelek.Updated.Nev = pageView.GetUpdatedByView(RequiredTextBox_Nev);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_AgazatiJelek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        erec_AgazatiJelek.Base.Ver = FormHeader1.Record_Ver;
        erec_AgazatiJelek.Base.Updated.Ver = true;

        return erec_AgazatiJelek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(RequiredTextBox_Kod);
            compSelector.Add_ComponentOnClick(RequiredTextBox_Nev);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            
            FormFooter1.SaveEnabled = false;            
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                            EREC_AgazatiJelek erec_AgazatiJelek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_AgazatiJelek);
                           
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, RequiredTextBox_Nev.Text))
                                {
                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                    if (!String.IsNullOrEmpty(refreshCallingWindow)
                                        && refreshCallingWindow == "1"
                                        )
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
                                EREC_AgazatiJelek erec_AgazatiJelek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_AgazatiJelek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
