<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AgazatiJelekList.aspx.cs" Inherits="AgazatiJelekList" Title="Iktat�k�nyvek lek�rdez�se" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="AgazatiJelekCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="AgazatiJelekUpdatePanel" runat="server" OnLoad="AgazatiJelekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="AgazatiJelekCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="AgazatiJelekCPEButton"
                            CollapseControlID="AgazatiJelekCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="AgazatiJelekCPEButton" ExpandedSize="0" ExpandedText="�gazati jelek list�ja"
                            CollapsedText="�gazati jelek list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="AgazatiJelekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="AgazatiJelekGridView_RowCommand" OnPreRender="AgazatiJelekGridView_PreRender"
                                            OnSorting="AgazatiJelekGridView_Sorting" OnRowDataBound="AgazatiJelekGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Kod" HeaderText="K�d" SortExpression="Kod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">
                                    <ajaxToolkit:TabPanel ID="IrattariTetelekTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelIrattariTetelek" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label ID="headerIrattariTetelek" runat="server" Text="Iratt�ri t�telek"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="IrattariTetelekUpdatePanel" runat="server" OnLoad="IrattariTetelekUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="IrattariTetelekPanel" runat="server" Visible="true" Width="100%">
                                                        <uc1:SubListHeader ID="IrattariTetelekSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="IrattariTetelekCPE" runat="server" TargetControlID="panelIrattariTetelekList"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="panelIrattariTetelekList" runat="server">
                                                                        <asp:GridView ID="IrattariTetelekGridView" runat="server" AutoGenerateColumns="False"
                                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                            OnPreRender="IrattariTetelekGridView_PreRender" OnRowCommand="IrattariTetelekGridView_RowCommand"
                                                                            DataKeyNames="Id" OnSorting="IrattariTetelekGridView_Sorting" OnRowDataBound="IrattariTetelekGridView_RowDataBound">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Nev") %>'
                                                                                            CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="IrattariTetelszam" HeaderText="T�telsz�m" SortExpression="IrattariTetelszam">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="IRATTARI_JEL" HeaderText="Iratt�ri&nbsp;jel" SortExpression="IRATTARI_JEL">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MegorzesiIdo_Nev" HeaderText="�rz�si&nbsp;id�" SortExpression="MegorzesiIdo_Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
