using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class AgazatiJelekList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AgazatiJelekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        IrattariTetelekSubListHeader.RowCount_Changed += new EventHandler(IrattariTetelekSubListHeader_RowCount_Changed);

        IrattariTetelekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IrattariTetelekSubListHeader_ErvenyessegFilter_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.AgazatiJelekListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_AgazatiJelekSearch);
        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = false;// TODO: printform
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, AgazatiJelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, AgazatiJelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(AgazatiJelekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AgazatiJelekListajaPrintForm.aspx");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(AgazatiJelekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(AgazatiJelekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(AgazatiJelekGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(AgazatiJelekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, AgazatiJelekUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = AgazatiJelekGridView;

        IrattariTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IrattariTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IrattariTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IrattariTetelekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IrattariTetelekGridView.ClientID);
        IrattariTetelekSubListHeader.ButtonsClick += new CommandEventHandler(IrattariTetelekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        IrattariTetelekSubListHeader.AttachedGridView = IrattariTetelekGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(AgazatiJelekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariTetelekGridView);


        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_AgazatiJelekSearch());

        if (!IsPostBack) AgazatiJelekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.ViewHistory);

        ListHeader1.IktatokonyvLezarasEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.Lezaras);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "AgazatiJel" + CommandName.Lock);

        IrattariTetelekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "IrattariTetelekList");

        IrattariTetelekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.New);
        IrattariTetelekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.View);
        IrattariTetelekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.Modify);
        IrattariTetelekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.Invalidate);


        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(AgazatiJelekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }


    #endregion

    #region Master List

    protected void AgazatiJelekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("AgazatiJelekGridView", ViewState, "Kod");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("AgazatiJelekGridView", ViewState);

        AgazatiJelekGridViewBind(sortExpression, sortDirection);
    }

    protected void AgazatiJelekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(AgazatiJelekGridView);

        EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_AgazatiJelekSearch search = null;
        search = (EREC_AgazatiJelekSearch)Search.GetSearchObject(Page, new EREC_AgazatiJelekSearch());

        search.OrderBy = Search.GetOrderBy("AgazatiJelekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAll(ExecParam, search);

        UI.GridViewFill(AgazatiJelekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void AgazatiJelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void AgazatiJelekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = AgazatiJelekGridView.PageIndex;

        AgazatiJelekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = AgazatiJelekGridView.PageCount;

        if (prev_PageIndex != AgazatiJelekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, AgazatiJelekCPE);
            AgazatiJelekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, AgazatiJelekCPE);
        }

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(AgazatiJelekGridView);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        AgazatiJelekGridViewBind();
        ActiveTabClear();
    }

    protected void AgazatiJelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(AgazatiJelekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //if (Header.Text.IndexOf(" (") > 0)
            //    Header.Text = Header.Text.Remove(Header.Text.IndexOf(" ("));
            //if (headerIrattariTetelek.Text.IndexOf(" (") > 0)
            //    headerIrattariTetelek.Text = headerIrattariTetelek.Text.Remove(headerIrattariTetelek.Text.IndexOf(" ("));
            //if (Label1.Text.IndexOf(" (") > 0)
            //    Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));

            ActiveTabRefresh(TabContainer1, id);
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, AgazatiJelekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("AgazatiJelekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, AgazatiJelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_AgazatiJelek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, AgazatiJelekUpdatePanel.ClientID);

            IrattariTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.AgazatiJelId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IrattariTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            ListHeader1.PrintOnClientClick = "javascript:window.open('AgazatiJelekPrintForm.aspx?" + QueryStringVars.AgazatiJelId + "=" + id + "')";

        }
    }

    protected void AgazatiJelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    AgazatiJelekGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedAgazatiJelek();
            AgazatiJelekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedAgazatiJelRecords();
                AgazatiJelekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedAgazatiJelRecords();
                AgazatiJelekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedAgazatiJelek();
                break;
        }
    }

    private void LockSelectedAgazatiJelRecords()
    {
        LockManager.LockSelectedGridViewRecords(AgazatiJelekGridView, "EREC_AgazatiJelek"
            , "AgazatiJelLock", "AgazatiJelForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedAgazatiJelRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(AgazatiJelekGridView, "EREC_AgazatiJelek"
            , "AgazatiJelLock", "AgazatiJelForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a AgazatiJelekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedAgazatiJelek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "AgazatiJelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(AgazatiJelekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a AgazatiJelekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedAgazatiJelek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(AgazatiJelekGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_AgazatiJelek");
        }
    }

    protected void AgazatiJelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        AgazatiJelekGridViewBind(e.SortExpression, UI.GetSortToGridView("AgazatiJelekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (AgazatiJelekGridView.SelectedIndex == -1)
        {
            return;
        }
        string AgazatiJel_Id = UI.GetGridViewSelectedRecordId(AgazatiJelekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, AgazatiJel_Id);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string AgazatiJel_Id)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                IrattariTetelekGridViewBind(AgazatiJel_Id);
                IrattariTetelekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(IrattariTetelekGridView);
                break;
        }
    }

    protected void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        IrattariTetelekSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(IrattariTetelekTabPanel))
        {
            IrattariTetelekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(IrattariTetelekGridView));
        }
    }


    #endregion


    #region IrattariTetelek Detail

    protected void IrattariTetelekGridViewBind(String AgazatiJel_Id)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IrattariTetelekGridView", ViewState, "IrattariTetelszam");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IrattariTetelekGridView", ViewState);

        IrattariTetelekGridViewBind(AgazatiJel_Id, sortExpression, sortDirection);

    }

    protected void IrattariTetelekGridViewBind(String AgazatiJel_Id, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(AgazatiJel_Id))
        {
            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();
            search.AgazatiJel_Id.Value = AgazatiJel_Id;
            search.AgazatiJel_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("IrattariTetelekGridView", ViewState, SortExpression, SortDirection);

            IrattariTetelekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);

            UI.GridViewFill(IrattariTetelekGridView, res, IrattariTetelekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, headerIrattariTetelek);

            ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariTetelekGridView);
        }
        else
        {
            ui.GridViewClear(IrattariTetelekGridView);
            UI.ClearTabHeaderRowCountText(headerIrattariTetelek);
        }

    }

    //SubListHeader f�ggv�nyei(4)
    private void IrattariTetelekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIrattariTetelek();
            IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
        }
    }

    private void deleteSelectedIrattariTetelek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IrattariTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    private void IrattariTetelekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
    }

    private void IrattariTetelekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(IrattariTetelekSubListHeader.RowCount);
        IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
    }

    //GridView esem�nykezel�i(3-4)
    ////specialis, nem mindig kell kell
    protected void IrattariTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page
            , "cb_System", "System");
    }

    protected void IrattariTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IrattariTetelekGridView, selectedRowNumber, "check");
        }
    }

    protected void IrattariTetelekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IrattariTetelekGridView.PageIndex;

        IrattariTetelekGridView.PageIndex = IrattariTetelekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != IrattariTetelekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IrattariTetelekCPE);
            IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(IrattariTetelekSubListHeader.Scrollable, IrattariTetelekCPE);
        }
        IrattariTetelekSubListHeader.PageCount = IrattariTetelekGridView.PageCount;
        IrattariTetelekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(IrattariTetelekGridView);


        ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariTetelekGridView);

    }

    protected void IrattariTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(AgazatiJelekGridView)
            , e.SortExpression, UI.GetSortToGridView("IrattariTetelekGridView", ViewState, e.SortExpression));
    }

    //UpdatePanel esem�nykezel�i(1)
    protected void IrattariTetelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(IrattariTetelekTabPanel))
                    //{
                    //    IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(AgazatiJelekGridView));
                    //}
                    ActiveTabRefreshDetailList(IrattariTetelekTabPanel);
                    break;
            }
        }
    }

    //Gombok klien oldali szkripjeinek friss�t�se(1)
    private void IrattariTetelekGridView_RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            IrattariTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IrattariTetelekUpdatePanel.ClientID);

            IrattariTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IrattariTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    #endregion

}

