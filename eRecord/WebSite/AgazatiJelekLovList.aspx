<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" 
AutoEventWireup="true" CodeFile="AgazatiJelekLovList.aspx.cs" Inherits="AgazatiJelekLovList" %>



<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style>
   .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas{
    text-align: left;
    width: 50px;
    min-width: 50px;
}
</style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,AgazatiJelekLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr>
                                  <td>                                    
                                    <table>
                                        <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                            <td>
                                                <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                            </td>
                                        </tr>  
                                        <tr class="urlapSor">                                            
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label3" runat="server" Text="K�d:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Kod_TextBox" runat="server" CssClass="mrUrlapInput">*</asp:TextBox></td>
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label1" runat="server" Text="N�v:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput">*</asp:TextBox></td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td colspan="4">
                                                <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                                                <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s" /></td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                        <div class="listaFulFelsoCsikKicsi">
                                            <img src="images/hu/design/spacertrans.gif" alt="" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                    
                                                    <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                    <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                    <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                    
                                                        <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                            DataKeyNames="Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" SortExpression="Id">
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <HeaderStyle CssClass = "GridViewLovListInvisibleCoulumnStyle" />
                                                                </asp:BoundField>  
                                                                <asp:BoundField DataField="Kod" HeaderText="K�d">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Nev" HeaderText="N�v">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="500px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>                                                                
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <%--
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        &nbsp;
                                        <br />
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" 
                                        ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                        onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"
                                        AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
