using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class AgazatiJelekLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;


    protected void Page_PreInit(object sender, EventArgs e)
    {        
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AgazatiJelekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.Enabled = false;
        UI.SetImageButtonStyleToDisabled(ImageButtonReszletesAdatok);
        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("UgyUgyiratokForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.Enabled = false;
        UI.SetImageButtonStyleToDisabled(ButtonAdvancedSearch);
        //ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx", ""
        //    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);


        if (!IsPostBack)
        {           
            FillGridViewSearchResult(false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        //if (IsPostBack)
        //{
        //    String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
        //    RefreshOnClientClicks(selectedRowId);
        //}
    }

    //private void RefreshOnClientClicks(string id)
    //{
    //    if (!String.IsNullOrEmpty(id))
    //    {

    //        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick(
    //            "UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View
    //            + "&" + QueryStringVars.Id + "=" + id
    //        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, null);
    //    }
    //}

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
        disable_refreshLovList = true;
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_AgazatiJelekService service = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();
        EREC_AgazatiJelekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_AgazatiJelekSearch)Search.GetSearchObject(Page, new EREC_AgazatiJelekSearch());
        }
        else
        {
            search = new EREC_AgazatiJelekSearch();
            
            // K�d:
            if (!String.IsNullOrEmpty(Kod_TextBox.Text))
            {
                search.Kod.Value = Kod_TextBox.Text;
                search.Kod.Operator = Search.GetOperatorByLikeCharater(Kod_TextBox.Text);
            }

            // N�v:
            if (!String.IsNullOrEmpty(Nev_TextBox.Text))
            {
                search.Nev.Value = Nev_TextBox.Text;
                search.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
            }

        }

        // Rendez�s:
        search.OrderBy = "Kod";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(execParam, search);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1)
                        + " (" + UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2) + ")";

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }



    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}
