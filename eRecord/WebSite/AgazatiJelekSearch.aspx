<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="AgazatiJelekSearch.aspx.cs" Inherits="AgazatiJelekSearch" Title="<%$Resources:Search,AgazatiJelekSearchHeaderTitle%>" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,AgazatiJelekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapSpacer">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelKod" runat="server" Text="K�d:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="Megnevez�s:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                     <uc5:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                    </uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                               </tr>
                               <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                               </tr>
                               <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                               </tr>
                                <tr class="urlapSor">
                                     <td colspan="5">
                                     <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                                        runat="server">
                                      </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

