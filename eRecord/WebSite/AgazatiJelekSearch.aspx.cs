using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class AgazatiJelekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_AgazatiJelekSearch);

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            EREC_AgazatiJelekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_AgazatiJelekSearch)Search.GetSearchObject(Page, new EREC_AgazatiJelekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_AgazatiJelekSearch _EREC_AgazatiJelekSearch = (EREC_AgazatiJelekSearch)searchObject;

        if (_EREC_AgazatiJelekSearch != null)
        {
            textNev.Text = _EREC_AgazatiJelekSearch.Nev.Value;
            textKod.Text = _EREC_AgazatiJelekSearch.Kod.Value;

            Ervenyesseg_SearchFormComponent1.SetDefault(
                _EREC_AgazatiJelekSearch.ErvKezd, _EREC_AgazatiJelekSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private EREC_AgazatiJelekSearch SetSearchObjectFromComponents()
    {
        EREC_AgazatiJelekSearch _EREC_AgazatiJelekSearch = (EREC_AgazatiJelekSearch)SearchHeader1.TemplateObject;

        if (_EREC_AgazatiJelekSearch == null)
        {
            _EREC_AgazatiJelekSearch = new EREC_AgazatiJelekSearch();
        }

        _EREC_AgazatiJelekSearch.Nev.Value = textNev.Text;
        _EREC_AgazatiJelekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(textNev.Text);
        _EREC_AgazatiJelekSearch.Kod.Value = textKod.Text;
        _EREC_AgazatiJelekSearch.Kod.Operator = Search.GetOperatorByLikeCharater(textKod.Text);
        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _EREC_AgazatiJelekSearch.ErvKezd, _EREC_AgazatiJelekSearch.ErvVege);

        return _EREC_AgazatiJelekSearch;
    }
    
    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_AgazatiJelekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }        
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private EREC_AgazatiJelekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_AgazatiJelekSearch();
    }
}
