﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="AlairasAdatok.aspx.cs" Inherits="AlairasAdatok" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
        
        <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
              
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    
    <asp:Panel ID="MainPanel" runat="server">
      
                 <table style="width: 95%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="AlairasAdatokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" PagerSettings-Visible="false" AutoGenerateColumns="False"
                                            >
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:BoundField DataField="AlairtFajlnev" HeaderText="Aláírt&nbsp;fájl&nbsp;neve">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ElektronikusAlairas" HeaderText="Elektronikus&nbsp;aláírás">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="140px" />
                                                </asp:BoundField>                                                
                                                <asp:BoundField DataField="BelsoTipus" HeaderText="Aláírás&nbsp;típus">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="140px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Idopecset" HeaderText="Időpecsét">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>                                                
                                                <asp:BoundField DataField="AlairasTulajdonos" HeaderText="Aláírás&nbsp;tulajdonos">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>                                            
                                                <asp:BoundField DataField="AlairasIdopont" HeaderText="Aláírás&nbsp;időpontja">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>       
                                                <asp:TemplateField HeaderText="Aláírás rendben">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemTemplate>
                                                        <%# ((Eval("AlairasRendben") as string) == "1") ? "Igen" : ((Eval("AlairasRendben") as string) == "0" ? "Nem" : "")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                    </tr>
                    </table>
                    <br />
                    <uc2:FormFooter ID="FormFooter1" runat="server" /> 
    
    </asp:Panel>

</asp:Content>

