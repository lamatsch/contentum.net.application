﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eDocument.Service;

public partial class AlairasAdatok : Contentum.eUtility.UI.PageBase
{
    private string DokumentumId;
    private UI ui = new UI();
    private const string FunkcioKod_DokumentumAlairasAdatok = "DokumentumAlairasAdatok";

    protected void Page_Init(object sender, EventArgs e)
    {
        DokumentumId = Request.QueryString.Get(QueryStringVars.DokumentumId);

        // Funkciójog ellenőrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_DokumentumAlairasAdatok);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //JavaScripts.RegisterPopupWindowClientScript(Page);

        if (!IsPostBack && !string.IsNullOrEmpty(DokumentumId))
        {
            // Jogosultságellenőrzés az objektumra:
            CheckRights(DokumentumId);

            LoadIktatasiAdatok();
        }

        // HeaderText
        FormHeader1.FullManualHeaderTitle = Resources.Form.DokumentumAlairasAdatokFormHeaderTitle;

    }



    private void LoadIktatasiAdatok()
    {
        if (!string.IsNullOrEmpty(DokumentumId))
        {
            AlairasokService service = eRecordService.ServiceFactory.GetAlairasokService();

            ExecParam execParam = UI.SetExecParamDefault(Page);
            execParam.Record_Id = DokumentumId;

            Result result = service.GetAll_PKI_AlairasAdatok(execParam);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                MainPanel.Visible = false;
                return;
            }
            else
            {                
                ui.GridViewFill(AlairasAdatokGridView, result, FormHeader1.ErrorPanel, null);
            }
        }
    }


    private void CheckRights(string dokumentumId)
    {
        char jogszint = 'O';

        KRT_DokumentumokService service = Contentum.eRecord.Utility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = dokumentumId;

        Result result = service.GetWithRightCheck(execParam, jogszint);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            MainPanel.Visible = false;
            return;
        }
    }
}
