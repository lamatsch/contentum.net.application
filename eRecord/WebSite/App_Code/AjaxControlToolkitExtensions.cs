﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for AjaxControlToolkitExtender
/// </summary>
public static class AjaxControlToolkitExtender
{
    /// <summary>
    /// AjaxControlToolkit.TabContainer használt verziójában problémát jelentenek az elrejtett tabok, 
    /// az activetab -ba az a tab kerül, ami az összes tab közül azon a pozíción van, ahová a látható tabok közül kattintottunk.
    /// A függvény megnézi, hogy a látható tabok közül melyik van az adott pozíción, és azt adja vissza.
    /// </summary>
    /// <param name="tabContainer">TabContainer, amin a kiválasztott TabPanel van</param>
    /// <returns>Kiválasztott TabPanel</returns>
    public static AjaxControlToolkit.TabPanel GetRealActiveTab(this AjaxControlToolkit.TabContainer tabContainer) {
        if (tabContainer!=null && tabContainer.Tabs.Count > 0)
        {
            if (tabContainer.ActiveTab == null)
                tabContainer.ActiveTab = tabContainer.Tabs[0];

            //TODO ha hiba javításra kerül, akkor az adott erziótól felesleges a variálás:
            //string url = HttpContext.Current.Request.Url.ToString() + "/Bin/AjaxControlToolkit.dll";
            //Assembly assembly = Assembly.LoadFrom(url);
            //Version currentToolkitVersion = assembly.GetName().Version;
            //Version stableToolkitVersion = new Version();
            //if (currentToolkitVersion.CompareTo(stableToolkitVersion) >= 0)
            //{
            //    return tabContainer.ActiveTab;
            //}


            List<AjaxControlToolkit.TabPanel> VisiblePanels = new List<AjaxControlToolkit.TabPanel>();

            int selectedtabNumber = 0;
            int panelInd = 0;
            foreach (AjaxControlToolkit.TabPanel panel in tabContainer.Tabs)
            {
                if (panel.Visible != false)
                {
                    VisiblePanels.Add(panel);
                }
                
                if (panel == tabContainer.ActiveTab)
                {
                    selectedtabNumber = panelInd;
                }
                panelInd++;
            }
            if (selectedtabNumber < VisiblePanels.Count)
            {
                return VisiblePanels[selectedtabNumber];
            }
        }

        return new AjaxControlToolkit.TabPanel();
    }

}