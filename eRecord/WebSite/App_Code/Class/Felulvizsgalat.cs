﻿using System;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;

/// <summary>
/// Felülvizsgálat segédosztály
/// </summary>
//FelulvizsgalatForm -ról kivéve, mivel az iratform minősítésfelülvizsgálat tablapján is szükség van rá
public static class Felulvizsgalat
{
    public static readonly ListItem Selejtezes = new ListItem("Selejtezés", "S");
    public static readonly ListItem LeveltarbaAdas = new ListItem("Levéltárba adás", "L");
    public static readonly ListItem EgyebSzervezetnekAtadas = new ListItem("Egyéb szervezetnek átadás", "E");
    //BLG2156
    //public static readonly ListItem Meghosszabitas = new ListItem("Jegyzékre helyezés", "M");
    public static readonly ListItem Meghosszabitas = new ListItem("Őrzési idő meghosszabítása", "M");

    #region BLG1722 Listitems
    public static readonly ListItem MinositesFelulvizsgalata = new ListItem("Minősítés felülvizsgálata", "F");

    public static readonly ListItem MinositesMegszuntetese = new ListItem("Minősítés megszüntetése", "D");
    public static readonly ListItem ErvenyessegiIdoModositas = new ListItem("Érvényességi idő módosítás", "T");
    public static readonly ListItem MinositesiSzintCsokkentes = new ListItem("Minősítési szint csökkentés", "C");
    public static readonly ListItem MinositesFenntartasa = new ListItem("Minősítés fenntartása", "I");
    #endregion

    public static readonly ListItem Felulvizsgalando = new ListItem("Átsorolás más megőrzési mód alá", "FV");

    public static void FillDropDownList(DropDownList list, string startup, bool isTuk, bool isMinositesFelulvizsgalat)
    {
        FillDropDownList(list, startup, isTuk, isMinositesFelulvizsgalat, false);
    }

    public static void FillDropDownList(DropDownList list, string startup, bool isTuk, bool isMinositesFelulvizsgalat, bool needEmptyRow)
    {
        FillAndSetSelectedValue(list, startup, isTuk, isMinositesFelulvizsgalat, needEmptyRow, null);
    }

    public static void FillAndSetSelectedValue(DropDownList list, string startup, bool isTuk, bool isMinositesFelulvizsgalat, bool needEmptyRow, string selectedValue)
    {
        list.Items.Clear();
        if (needEmptyRow) list.Items.Add(new ListItem(string.Empty, string.Empty));
        if (isMinositesFelulvizsgalat)
        {
            if (!isTuk) return;
            list.Items.Add(MinositesMegszuntetese);
            list.Items.Add(ErvenyessegiIdoModositas);
            list.Items.Add(MinositesiSzintCsokkentes);
            list.Items.Add(MinositesFenntartasa);
        }
        else
        {
            if (!String.IsNullOrEmpty(startup))
            {
                if (startup == Constants.Startup.FromSelejtezes || startup == Constants.Startup.FromIratPeldany)
                {
                    list.Items.Add(Selejtezes);
                }

                if (startup == Constants.Startup.FromLeveltarbaAdas)
                {
                    list.Items.Add(LeveltarbaAdas);
                }

                if (startup == Constants.Startup.FromEgyebSzervezetnekAtadas)
                {
                    list.Items.Add(EgyebSzervezetnekAtadas);
                }

                if (startup == Constants.Startup.FromFelulvizsgalando)
                {
                    list.Items.Add(Felulvizsgalando);
                }
            }
            if (startup != Constants.Startup.FromIratPeldany)
            {
                list.Items.Add(Meghosszabitas);
            }

            if (isTuk)
                list.Items.Add(MinositesFelulvizsgalata);
        }

        if (!string.IsNullOrEmpty(selectedValue))
        {
            list.SelectedValue = selectedValue;
        }
        else
        {
            if (needEmptyRow) list.SelectedValue = string.Empty;
        }
    }
}