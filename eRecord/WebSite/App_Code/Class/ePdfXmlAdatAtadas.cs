﻿using System.Collections.Generic;
using System.Xml.Serialization;

[XmlRoot(ElementName = "Jegyzek")]
public class ePdfXmlAdatAtadasJegyzek
{
    [XmlElement(ElementName = "Ragszam")]
    public List<string> Ragszam { get; set; }
    [XmlElement(ElementName = "KezbesitesDatum")]
    public List<string> KezbesitesDatum { get; set; }
    [XmlElement(ElementName = "Kezbesitve")]
    public List<string> Kezbesitve { get; set; }
    [XmlElement(ElementName = "Kepfile")]
    public List<string> Kepfile { get; set; }
    [XmlElement(ElementName = "VisszakezbesitesDatum")]
    public List<string> VisszakezbesitesDatum { get; set; }
    [XmlElement(ElementName = "VisszakezbesitesOka")]
    public List<string> VisszakezbesitesOka { get; set; }
    [XmlElement(ElementName = "AtadasDatum")]
    public List<string> AtadasDatum { get; set; }
    [XmlElement(ElementName = "SajatAzonosito")]
    public List<string> SajatAzonosito { get; set; }
    [XmlElement(ElementName = "SzkennelesDatum")]
    public List<string> SzkennelesDatum { get; set; }
    [XmlElement(ElementName = "KotegAzonosito")]
    public List<string> KotegAzonosito { get; set; }
    [XmlAttribute(AttributeName = "id")]
    public string Id { get; set; }
    [XmlAttribute(AttributeName = "Datum")]
    public string Datum { get; set; }
}

[XmlRoot(ElementName = "AdatAtadas")]
public class ePdfXmlAdatAtadas
{
    [XmlElement(ElementName = "Jegyzek")]
    public ePdfXmlAdatAtadasJegyzek Jegyzek { get; set; }
}