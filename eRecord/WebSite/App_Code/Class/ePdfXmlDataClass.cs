﻿/// <summary>
/// Summary description for ePdfXmlDataClass
/// </summary>
public class ePdfXmlDataClass
{
    public string JegyzekAzonosito { get; set; }
    public string FelvetelDatuma { get; set; }
    public string Ragszam { get; set; }
    public string KezbDatuma { get; set; }
    public string Kezbesitve { get; set; }
    public string KepFile { get; set; }
    public string VisszakezbDatuma { get; set; }
    public string VisszakezbOka { get; set; }
    public string AdatatadasNapja { get; set; }
}
