﻿/// <summary>
/// Summary description for LotusNotesIratObj
/// </summary>
public class LotusNotesIratObj
{
    public string IratID { get; set; }
    public string UgyiratID { get; set; }
    public string Iktatoszam { get; set; }
    public string IratTargy { get; set; }
    public string IratVonalkod { get; set; }
    public string UgyiratTargy { get; set; }

}