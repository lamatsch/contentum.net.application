﻿/// <summary>
/// Summary description for LotusNotesUgyiratObj
/// </summary>
public class LotusNotesUgyiratObj
{
    public string UgyiratID { get; set; }
    public string Iktatoszam { get; set; }
    public string UgyiratTargy { get; set; }
    public string UgyiratVonalkod { get; set; }
}