﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eRecord.Service;

/// <summary>
/// Model a Targy mezo meghatarozasara
/// </summary>
public class UIBeadvanyokModel
{
    public EREC_eBeadvanyok EBeadvany { get; set; }

    public EREC_eBeadvanyok Get(ExecParam execParam, string id)
    {
        EREC_eBeadvanyokService service = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        execParam.Record_Id = id;
        Result res = service.Get(execParam);

        if (!string.IsNullOrEmpty(res.ErrorMessage))
            return new EREC_eBeadvanyok();

        return (EREC_eBeadvanyok)res.Record;
    }
}


