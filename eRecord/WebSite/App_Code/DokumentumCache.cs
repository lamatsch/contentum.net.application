﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Caching;
using System.Net;
using Contentum.eUtility;

/// <summary>
/// Summary description for DokumentumCache
/// </summary>
public static class DokumentumCache
{
    private static readonly TimeSpan ExpirationTime = TimeSpan.FromMinutes(10);

    public static Stream GetDokumentum(string externalLink, string version)
    {
        string key = externalLink + ";" + version;
        Cache cache = HttpContext.Current.Cache;
        byte[] fileData = (byte[])cache[key];
        
        if (fileData == null)
        {
            using (WebClient wc = new WebClient())
            {
                wc.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
                wc.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.Revalidate);
                fileData = wc.DownloadData(externalLink);
                cache.Insert(key, fileData, null, 
                    System.Web.Caching.Cache.NoAbsoluteExpiration, DokumentumCache.ExpirationTime, 
                    System.Web.Caching.CacheItemPriority.NotRemovable, null);
            }
        }

        return new MemoryStream(fileData);
    }

    //private const string tempDirectory = "C:\\Temp\\ContentumNet\\Documents";

    //private DirectoryInfo GetDocumentTempDirectory(string dokumentumId, string version)
    //{
    //    string path = Path.Combine(Path.Combine(tempDirectory, dokumentumId), version);
    //    DirectoryInfo di = new DirectoryInfo(path);
    //    if (!di.Exists)
    //    {
    //        di.Create();
    //    }

    //    return di;

    //}

    //private string GetDocumentTempPath(string dokumentumId, string version, string fileName)
    //{
    //    DirectoryInfo di = GetDocumentTempDirectory(dokumentumId, version);
    //    return Path.Combine(di.FullName, fileName);
    //}

    //private string SaveFileFromSharePoint(string dokumentumId, string version, string fileName, string externalLink)
    //{
    //    string filePath = GetDocumentTempPath(dokumentumId, version, fileName);

    //    if (!File.Exists(filePath))
    //    {
    //        using (WebClient wc = new WebClient())
    //        {
    //            wc.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
    //            wc.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.Revalidate);
    //            wc.DownloadFile(externalLink, filePath);
    //        }
    //    }

    //    return filePath;
    //}
}
