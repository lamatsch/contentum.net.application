﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Contentum.eUIControls;
using System.Web.UI;

/// <summary>
/// Summary description for DynamicValueControlGridViewTemplate
/// </summary>
public class DynamicValueControlGridViewTemplate : ITemplate
{
    string _ColumnName;
    DataControlRowType _rowType;
    string _ControlTypeSource;
    bool _Enabled = false;

    public string ControlTypeSource
    {
        get { return _ControlTypeSource; }
        set { _ControlTypeSource = value; }
    }

    public string ColumnName
    {
        get { return _ColumnName; }
        set { _ColumnName = value; }
    }

    public DataControlRowType RowType
    {
        get { return _rowType; }
        set { _rowType = value; }
    }

    public bool Enabled
    {
        get { return _Enabled; }
        set { _Enabled = value; }
    }

	public DynamicValueControlGridViewTemplate()
	{
		//
		// TODO: Add constructor logic here
		//

        // default
        _rowType = DataControlRowType.DataRow;
	}

    public DynamicValueControlGridViewTemplate(string ColumnName, DataControlRowType RowType, String ControlTypeSource) : this(ColumnName, RowType)
    {
        _ControlTypeSource = ControlTypeSource;
    }

    public DynamicValueControlGridViewTemplate(string ColumnName, DataControlRowType RowType) : this(ColumnName)
    {
        _rowType = RowType;
    }

    public DynamicValueControlGridViewTemplate(DataControlRowType RowType)
    {
        _rowType = RowType;
    }

    public DynamicValueControlGridViewTemplate(string ColumnName) : this()
    {
        _ColumnName = ColumnName;
    }

    public void InstantiateIn(System.Web.UI.Control container)
    {

        switch (_rowType)
        {
            case DataControlRowType.Header:
                Literal lc = new Literal();
                lc.Text = _ColumnName;
                container.Controls.Add(lc);
                break;
            case DataControlRowType.DataRow:
                DynamicValueControl dvc = new DynamicValueControl();
                dvc.DataBinding += new EventHandler(this.dvc_DataBind);
                container.Controls.Add(dvc);
                break;
            case DataControlRowType.Footer:
                // ...
                break;
            default:
                break;

        }
    }

    private void dvc_DataBind(Object sender, EventArgs e)
    {
        DynamicValueControl dvc  = (DynamicValueControl)sender;
        GridViewRow row = (GridViewRow)dvc.NamingContainer;
        dvc.ControlTypeSource = _ControlTypeSource; //DataBinder.Eval(row.DataItem, _ControlTypeSource).ToString();
        dvc.Value = DataBinder.Eval(row.DataItem, String.Format("[{0}]", _ColumnName)).ToString();
        dvc.Enabled = _Enabled;
    }

}
