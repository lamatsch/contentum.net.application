﻿using Contentum.eBusinessDocuments;

/// <summary>
/// Summary description for ElosztoIvHelper
/// </summary>
public static class ElosztoIvHelper
{
    public static bool IsCimzettElosztoIvCimLista(ExecParam execParam, string id)
    {
        Contentum.eAdmin.Service.EREC_IraElosztoivekService elosztoivekService = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        execParam.Record_Id = id;
        Result result = elosztoivekService.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
            return false;
        if (result.Record == null)
            return false;

        return true;
    }
}