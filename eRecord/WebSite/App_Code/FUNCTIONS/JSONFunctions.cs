﻿using System;
using Newtonsoft.Json;
/// <summary>
/// Serialize and DeSerialize OBJECT <-> JSON
/// </summary>
public static class JSONFunctions
{
    public static DBO_ManagedEmailParameters DeSerializeManagedEmailParameters(string json)
    {
        DBO_ManagedEmailParameters result = null;
        JsonSerializerSettings settings = new JsonSerializerSettings();
        settings.MissingMemberHandling = MissingMemberHandling.Error;

        try
        {
            result = JsonConvert.DeserializeObject<DBO_ManagedEmailParameters>(json, settings);
        }
        catch (Exception)
        {

        }
        return result;
    }

    public static string SerializeManagedEmailParameters(DBO_ManagedEmailParameters item)
    {
        string result = null;
        JsonSerializerSettings settings = new JsonSerializerSettings();
        settings.MissingMemberHandling = MissingMemberHandling.Error;

        try
        {
            result = JsonConvert.SerializeObject(item, settings);
        }
        catch (Exception)
        {

        }
        return result;
    }



    public static string SerializeLotusNotesUgyiratObj(LotusNotesUgyiratObj item)
    {
        string result = null;
        JsonSerializerSettings settings = new JsonSerializerSettings();
        settings.MissingMemberHandling = MissingMemberHandling.Error;

        try
        {
            result = JsonConvert.SerializeObject(item, settings);
        }
        catch (Exception)
        {

        }
        return result;
    }
    public static string SerializeLotusNotesIratObj(LotusNotesIratObj item)
    {
        string result = null;
        JsonSerializerSettings settings = new JsonSerializerSettings();
        settings.MissingMemberHandling = MissingMemberHandling.Error;

        try
        {
            result = JsonConvert.SerializeObject(item, settings);
        }
        catch (Exception)
        {

        }
        return result;
    }
}