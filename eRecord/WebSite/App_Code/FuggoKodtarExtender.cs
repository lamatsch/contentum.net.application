﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eUtility;

namespace Contentum.eUIControls
{
    /// <summary>
    /// Summary description for FuggoKodtarExtender
    /// </summary>
    [TargetControlType(typeof(DropDownList))]
    public class FuggoKodtarExtender : ExtenderControl
    {
        private bool _enabled = true;

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        private string _ServicePath = "~/WrappedWebService/Ajax.asmx";

        public string ServicePath
        {
            get { return _ServicePath; }
            set { _ServicePath = value; }
        }

        private string _ServiceMethod = "GetFuggoKodtarakList";

        public string ServiceMethod
        {
            get { return _ServiceMethod; }
            set { _ServiceMethod = value; }
        }

        private string _VezerloKodcsoportKod;

        public string VezerloKodcsoportKod
        {
            get { return _VezerloKodcsoportKod; }
            set { _VezerloKodcsoportKod = value; }
        }

        private string _FuggoKodcsoportKod;

        public string FuggoKodcsoportKod
        {
            get { return _FuggoKodcsoportKod; }
            set { _FuggoKodcsoportKod = value; }
        }

        private string _ParentControlClientID;

        public string ParentControlClientID
        {
            get { return _ParentControlClientID; }
            set { _ParentControlClientID = value; }
        }

        Page Page;

        public FuggoKodtarExtender()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public FuggoKodtarExtender(Page page)
        {
            this.Page = page;
            page.Init += Page_Init;
        }

        private void Page_Init(object sender, EventArgs e)
        {
            Control targetControl = Page.FindControl(this.TargetControlID);
            if (targetControl != null)
            {
                this.TargetControlID = targetControl.ID;
                targetControl.Parent.Controls.Add(this);
            }
        }

        protected override System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            if (!Enabled)
                return null;

            ScriptBehaviorDescriptor descriptor = new ScriptBehaviorDescriptor("Utility.FuggoKodtarBehavior", targetControl.ClientID);
            descriptor.AddProperty("FelhasznaloId", FelhasznaloProfil.FelhasznaloId(Page));
            descriptor.AddProperty("ServicePath", ResolveClientUrl(this.ServicePath));
            descriptor.AddProperty("ServiceMethod", this.ServiceMethod);
            descriptor.AddProperty("VezerloKodcsoportKod", this.VezerloKodcsoportKod);
            descriptor.AddProperty("FuggoKodcsoportKod", this.FuggoKodcsoportKod);
            descriptor.AddProperty("ParentControlClientID", this.ParentControlClientID);

            return new ScriptDescriptor[] { descriptor };
        }

        protected override System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
        {
            if (!Enabled)
                return null;

            ScriptReference reference = new ScriptReference();
            reference.Path = "~/JavaScripts/FuggoKodtarBehavior.js?t=20190808";

            return new ScriptReference[] { reference };
        }

        public static void Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page page, string targetControlID, string parentControlClientID)
        {
            if (Rendszerparameterek.GetBoolean(page, "UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA_FILTER_ENABLED", false))
            {
                FuggoKodtarExtender control = new FuggoKodtarExtender(page);
                control.TargetControlID = targetControlID;
                control.ParentControlClientID = parentControlClientID;
                control.VezerloKodcsoportKod = "UGYINTEZES_ALAPJA";
                control.FuggoKodcsoportKod = "KULDEMENY_KULDES_MODJA";
            }
        }
    }
}