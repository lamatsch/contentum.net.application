﻿using Contentum.eUIControls;
using System;

#region Control interfaces

public interface IVisible
{
    bool Visible { get; set; }
}

public interface IReadOnly
{
    bool ReadOnly { get; set; }
}

public interface IEnabled
{
    bool Enabled { get; set; }
}

public interface IValidate
{
    bool Validate { get; set; }
}

public interface ICommonControl : IEnabled, IReadOnly, IVisible, IValidate
{
}

public interface ICalendarControl : ICommonControl
{
    string Text { get; set; }
}

public interface IDropDownList : ICommonControl
{
    eDropDownList DropDownList { get; }

    String SelectedValue { get; set; }
}

#endregion