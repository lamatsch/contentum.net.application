﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUtility;

namespace Microsec
{
    /// <summary>
    /// Summary description for MicrosignerManager
    /// </summary>
    public class MicroSignerManager
    {
        private const string certfilename = "_signer_certificate.cer";
        private const string downloadedfilename = "_downloaded.pdf";
        private const string halfsignedfilename = "_halfsigned.pdf";
        private const string verifiedlistxmlfilename = "_list.xml";
        private const string halfsignedbase64filename = "_halfsigned_base64_encoded.txt";
        private const string signedfilename = "_signed.pdf";
        private const string logpdfsignfilename = "_log_pdf_sign.txt";
        private const string logpdfverifyfilename = "_log_pdf_verify.txt";
        private const string logpdfgetunsignedhashfilename = "_log_pdf_get_unssigned_hash.txt";
        private const string logpdfsetsignedhashfilename = "_log_pdf_set_signed_hash.txt";
        private const string logServersideSignName = "_log_server_sode_sign.txt";

        public static readonly Dictionary<int, string> ResultErrors = new Dictionary<int, string>()
    {
        {1, "A hívási paraméterek hibásak"},
        {2, "Nincs több memória"},
        {3, "A fájl nem olvasható"},
        {4, "A fájl nem írható"},
        {5, "Microsec belső programhiba"},
        {6, "Hiba a dokumentum formátumában"},
        {7, "Hiba az XML dokumentum adataiban"},
        {10, "Aláírás ellenőrzése, az irat sérült"},
        {11, "Aláírás ellenőrzése, az aláírás sérült"},
        {12, "Hiba az átvételi elismervény által tartalmazott lenyomat ellenőrzésénél"},
        {14, "Hiányzó aláírás"},
        {15, "Az e-akta még nincs megnyitva"},
        {16, "Hiányzik egy kötelező csomópont az XML aláírásból"},
        {17, "Hiányzó leíró adat"},
        {18, "Hiányzik az Object elem az XML dokumentumból"},
        {19, "Hiányzik a dokumentum forrása"},
        {20, "Hiányzik a XAdES szerint kötelező adat"},
        {21, "Hiányzó névtér"},
        {22, "Hiányzik az e-Szignó névtér"},
        {23, "Hiba az időbélyeg hozzáadásánál/ellenőrzésénél"},
        {24, "Érvénytelen kulcshasználat"},
        {25, "A tanúsítványlánc nem építhető fel egy megbízható tanúsítványig"},
        {26, "A tanúsítvány lejárt vagy még nem érvényes"},
        {27, "A tanúsítvány vissza van vonva"},
        {28, "Nem befejezett ellenőrzés"},
        {29, "A tanúsítvány hiányzik"},
        {30, "A tanúsítványt nem lehet dekódolni"},
        {31, "A tanúsítványtároló nem nyitható"},
        {32, "Hiba az aláírási szabályzat ellenőrzésénél"},
        {33, "Hiba az aláírás létrehozásánál"},
        {34, "Hiba az aláírás feldolgozásánál"},
        {35, "Tömörítési hiba"},
        {36, "A feldolgozás megszakítva"},
        {40, "Hiba a regisztráció során"},
        {41, "Hiányzik az e-akta szerint kötelező adat"},
        {42, "Az attribútum nem érvényes"},
        {43, "A funkció nem használható ezzel a regisztrációval"},
        {44, "Hiba az aláírás előtti elemek (pl. attribútum tanúsítványok) létrehozásánál"},
        {45, "Hiba az időbélyeg ellenőrzésekor"},
        {46, "Nem megengedett művelet"},
        {47, "Hibás PIN-kód"},
        {48, "Biztonsági incidens: jelenleg az export_recipient_list ezzel jelzi, ha a PKCS7 címzettlista bővebb, mint a XADES címzettlista"},
        {49, "Hiba a protokoll futása során"},
        {50, "A Cert nincs regisztrálva"},
        {51, "Hibás felhasználónév vagy jelszó"}
    };

        public static string GetErrorText(int errorCode)
        {
            //Figyelmeztetés
            if (errorCode > 64)
                errorCode = errorCode - 64;

            if (ResultErrors.ContainsKey(errorCode))
                return String.Format("{0} - {1}", errorCode, ResultErrors[errorCode]);

            return errorCode.ToString();
        }

        private ExecParam execParam = null;

        private List<string> RetryCodes = new List<string>();
        private int RetryNum = 0;

        bool isTestMode = false;
        string eSzignoFilePath = String.Empty;
        string pdf_set_signed_hash_params = String.Empty;
        string pdf_sign_params = String.Empty;

        public MicroSignerManager(ExecParam execParam)
        {
            this.execParam = execParam;
            InitParameters();
        }

        #region private

        private void InitParameters()
        {
            string paramRetryCodes = Rendszerparameterek.Get(execParam, Rendszerparameterek.PKI_RETRY_CODES);
            ParseParamRetryCodes(paramRetryCodes);
            RetryNum = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.PKI_RETRY_NUM, 0);
            isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsSignTestMode") == "true";
            eSzignoFilePath = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_eSzigno_FilePath");
            pdf_set_signed_hash_params = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_pdf_set_signed_hash_params");
            pdf_sign_params = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_serverside_pdf_sign_params");
        }

        private void ParseParamRetryCodes(string paramRetryCodes)
        {
            if (!String.IsNullOrEmpty(paramRetryCodes))
            {
                RetryCodes = new List<string>(paramRetryCodes.Split('|'));
            }
        }

        private Process StartProcess(string fileName, string arguments)
        {
            ProcessStartInfo info = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                CreateNoWindow = false,
                UseShellExecute = false
            };

            return Process.Start(info);
        }

        class DokumentumItem
        {
            public string SignedHash { get; set; }
            public string IratId { get; set; }
            public string DokumentumId { get; set; }
            public string CsatolmanyId { get; set; }
            public string FolyamatTetelId { get; set; }
            public string ExternalLink { get; set; }
            public bool IsSigned { get; set; }
            public bool IsUploaded { get; set; }
            public bool IsSuccessful
            {
                get
                {
                    return IsSigned && IsUploaded;
                }

            }
            public string ErrorMessage { get; set; }
        }

        class IratItem
        {
            public string IratId { get; set; }

            public List<DokumentumItem> Dokumentumok { get; set; }

            public bool IsError { get; set; }
            public string ErrorMessage { get; set; }

            public List<string> GetCsatolmanyIds()
            {
                return Dokumentumok.Select(x => x.CsatolmanyId).ToList();
            }

            public bool IsAllDokumentumSigned()
            {
                return Dokumentumok.All(x => x.IsSigned);
            }

            public bool IsAllDokumentumSuccesful()
            {
                return Dokumentumok.All(x => x.IsSuccessful);
            }

            public IratItem()
            {
                Dokumentumok = new List<DokumentumItem>();
            }
        }

        List<IratItem> GetIratItems(string[] signedHashes, string iratIds, string dokumentumIds, string csatolmanyokIds, string folyamatTetelekIds)
        {
            List<IratItem> iratItemList = new List<IratItem>();

            string[] iratIdsArray = iratIds.Split(';');
            string[] dokumentumIdsArray = dokumentumIds.Split(';');
            string[] csatolmanyokIdsArray = csatolmanyokIds.Split(';');
            string[] folyamatTetelekIdsArray = folyamatTetelekIds.Split(';');

            IratItem iratItem = null;

            for (int i = 0; i < iratIdsArray.Length; i++)
            {
                string signedHash = signedHashes[i];
                string iratId = iratIdsArray[i];
                string dokumentumId = dokumentumIdsArray[i];
                string csatolmanyId = csatolmanyokIdsArray[i];
                string folyamatTetelId = folyamatTetelekIdsArray[i];

                DokumentumItem dokumentumItem = new DokumentumItem
                {
                    SignedHash = signedHash,
                    IratId = iratId,
                    DokumentumId = dokumentumId,
                    CsatolmanyId = csatolmanyId,
                    FolyamatTetelId = folyamatTetelId
                };

                if (iratItem == null || iratItem.IratId != iratId)
                {
                    iratItem = new IratItem { IratId = iratId };
                    iratItemList.Add(iratItem);
                }

                iratItem.Dokumentumok.Add(dokumentumItem);
            }

            return iratItemList;
        }

        List<IratItem> GetIratItems(string iratIds, string dokumentumIds, string externalLinks, string folyamatTetelekIds, string csatolmanyokIds)
        {
            List<IratItem> iratItemList = new List<IratItem>();

            string[] iratIdsArray = iratIds.Split(';');
            string[] dokumentumIdsArray = dokumentumIds.Split(';');
            string[] externalLinksArray = externalLinks.Split('|');
            string[] folyamatTetelekIdsArray = folyamatTetelekIds.Split(';');
            string[] csatolmanyokIdsArray = csatolmanyokIds.Split(';');

            IratItem iratItem = null;

            for (int i = 0; i < iratIdsArray.Length; i++)
            {
                string iratId = iratIdsArray[i];
                string dokumentumId = dokumentumIdsArray[i];
                string externalLink = externalLinksArray[i];
                string folyamatTetelId = folyamatTetelekIdsArray[i];
                string csatolmanyId = csatolmanyokIdsArray[i];

                DokumentumItem dokumentumItem = new DokumentumItem
                {
                    IratId = iratId,
                    DokumentumId = dokumentumId,
                    ExternalLink = externalLink,
                    FolyamatTetelId = folyamatTetelId,
                    CsatolmanyId = csatolmanyId
                };

                if (iratItem == null || iratItem.IratId != iratId)
                {
                    iratItem = new IratItem { IratId = iratId };
                    iratItemList.Add(iratItem);
                }

                iratItem.Dokumentumok.Add(dokumentumItem);
            }

            return iratItemList;
        }

        void SetIratSignedHash(IratItem iratItem)
        {
            try
            {
                foreach (DokumentumItem dokumentumItem in iratItem.Dokumentumok)
                {
                    try
                    {
                        SetSignedHash(dokumentumItem);
                        dokumentumItem.IsSigned = true;
                    }
                    catch (Exception ex)
                    {
                        dokumentumItem.IsSigned = false;
                        dokumentumItem.ErrorMessage = String.Format("Csatolmány aláírása közben hiba lépett fel: {0}", ex.Message);
                    }
                }

                //Minden csatolmány aláírása sikeres
                if (iratItem.IsAllDokumentumSigned())
                {
                    foreach (DokumentumItem dokumentumItem in iratItem.Dokumentumok)
                    {
                        try
                        {
                            UploadSignedFile(dokumentumItem);
                            dokumentumItem.IsUploaded = true;
                        }
                        catch (Exception ex)
                        {
                            dokumentumItem.IsUploaded = false;
                            dokumentumItem.ErrorMessage = String.Format("Csatolmány feltöltése közben hiba lépett fel: {0}", ex.Message);
                            break;
                        }
                    }

                    foreach (DokumentumItem dokumentumItem in iratItem.Dokumentumok)
                    {
                        DeleteFiles(dokumentumItem.DokumentumId);
                    }


                    if (iratItem.IsAllDokumentumSuccesful())
                    {
                        IratCsatolmanyAlairas(iratItem, String.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("MicroSignerManager.SetIratSignedHas hiba: IratId={0}", iratItem.IratId), ex);
                throw;
            }
        }

        void SetSignedHash(DokumentumItem dokumentumItem)
        {
            SetSignedHash(dokumentumItem, 1);
        }

        void SetSignedHash(DokumentumItem dokumentumItem, int numberOfAttempts)
        {
            Result resSignedHashes = new Result();

            try
            {
                string testParams = @"-test yes -debug yes -debug_file_path " + eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + logpdfsetsignedhashfilename + " ";
                if (!isTestMode) { testParams = ""; }
                Process processSigned = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", testParams + @"pdf_set_signed_hash -in " + eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + halfsignedfilename + @" -out " + eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + signedfilename + @" -hash " + dokumentumItem.SignedHash + " " + pdf_set_signed_hash_params);
                processSigned.WaitForExit();
                int exitCode = processSigned.ExitCode;
                Logger.Debug("MicroSignerManager.SetSignedHash -  processSigned.ExitCode=" + exitCode);

                //sikeres aláírás: ExitCode = 0, vagy 65 és 125 közötti
                if (exitCode == 0 || (exitCode >= 65 && exitCode <= 125))
                {
                    // csak figyelmeztetés, az aláírás megtörtént
                    if (exitCode != 0)
                    {
                        Logger.Warn(String.Format("MicroSignerManager.SetSignedHash ({0}) figyelmeztetes, processSigned.ExitCode: {1}", dokumentumItem.DokumentumId, processSigned.ExitCode));
                    }

                    //aláírás megtörtént
                    SetFolyamatTetelStatus(dokumentumItem.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas, processSigned.ExitCode.ToString());
                }
                else
                {
                    //amennyiben a hibakód szerepel a PKI_RETRY_CODES között, akkor újra meg kell próbálni az aláírást, amíg a próbálkozázok száma el nem éri a PKI_RETRY_NUM paraméter értékét
                    bool retry = RetryCodes.Contains(exitCode.ToString()) && numberOfAttempts <= RetryNum;

                    if (retry)
                    {
                        Logger.Warn("MicroSignerManager.SetSignedHash retry, processSigned.ExitCode: " + exitCode);
                        //várni kell az újrapróbálkozás előtt: próbálkozások száma*2 mp -et,
                        System.Threading.Thread.Sleep(numberOfAttempts * 2000);
                        SetSignedHash(dokumentumItem, numberOfAttempts + 1);
                    }
                    else
                    {
                        Logger.Error(String.Format("MicroSignerManager.SetSignedHash ({0}) hiba, processSigned.ExitCode: {1}", dokumentumItem.DokumentumId, exitCode));
                        //SetFolyamatTetelStatus(hashItem.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, processSigned.ExitCode.ToString());
                        throw new Exception(String.Format("eSzigno hiba: {0}", GetErrorText(exitCode)));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.Error(String.Format("MicroSignerManager.SetSignedHash ({0}) hiba", dokumentumItem.DokumentumId), exp);
                SetFolyamatTetelStatus(dokumentumItem.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, exp.Message);
                throw;
            }
        }

        void UploadSignedFile(DokumentumItem dokumentumItem)
        {
            Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            string filename = String.Empty;

            ExecParam dokGetExecParam = execParam.Clone();
            dokGetExecParam.Record_Id = dokumentumItem.DokumentumId;
            Result dokGetResult = dokService.Get(dokGetExecParam);
            if (dokGetResult.IsError)
            {
                Logger.Error(String.Format("MicroSignerManager.UploadSignedFile ({0}) hiba - dokService.Get", dokumentumItem.DokumentumId), dokGetExecParam, dokGetResult);
                throw new ResultException(dokGetResult);
            }

            filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;

            byte[] documentData = File.ReadAllBytes(eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + signedfilename);
            Result spUploadResult = eDocumentService.UploadSignedFile(execParam.Clone(), dokumentumItem.IratId, dokumentumItem.DokumentumId, String.Empty, String.Empty, String.Empty, filename, documentData);

            if (spUploadResult.IsError)
            {
                Logger.Error(String.Format("MicroSignerManager.UploadSignedFile ({0}) hiba - eDocumentService.UploadSignedFile", dokumentumItem.DokumentumId), execParam, spUploadResult);
                throw new ResultException(spUploadResult);
            }
        }

        void SetFolyamatTetelStatus(string folyamatTetelId, string status, string hiba)
        {
            ExecParam execParamTomeges = execParam.Clone();
            Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelId, status, hiba);

            if (resTetel.IsError)
            {
                Logger.Error("MicroSignerManager.SetFolyamatTetelStatus hiba -  eDocumentService.SetFolyamatTetelStatus", execParamTomeges, resTetel);
                throw new ResultException(resTetel);
            }
        }

        void DeleteFiles(string dokumentumId)
        {
            if (!isTestMode)
            {
                try
                {
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + downloadedfilename);
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + halfsignedfilename);
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + halfsignedbase64filename);
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + signedfilename);
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + logpdfsignfilename);
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + logpdfgetunsignedhashfilename);
                    File.Delete(eSzignoFilePath + @"out\" + dokumentumId + logpdfsetsignedhashfilename);
                }
                catch (Exception ex)
                {
                    Logger.Error(String.Format("MicroSignerManager.DeleteFiles ({0}) hiba", dokumentumId), ex);
                }
            }
        }

        void IratCsatolmanyAlairas(IratItem iratItem, string megjegyzes)
        {
            //IratCsatolmanyokAlairas
            //eDocumentService.SetStatusAfterServerSideSign(execParam, iratIdsArray[i], recordIdsArray[i], megjegyzes);

            Contentum.eDocument.Service.DokumentumAlairasService service = eDocumentService.ServiceFactory.GetDokumentumAlairasService();
            ExecParam execParamAlairas = execParam.Clone();

            Result resultCsatolmanyokAlairas = service.IratCsatolmanyokAlairas(execParamAlairas, iratItem.GetCsatolmanyIds().ToArray(), megjegyzes);

            if (resultCsatolmanyokAlairas.IsError)
            {
                Logger.Error(String.Format("MicroSignerManager.IratCsatolmanyAlairas ({0}) -  DokumentumAlairasService.IratCsatolmanyokAlairas hiba", iratItem.IratId), execParamAlairas, resultCsatolmanyokAlairas);
                throw new ResultException(resultCsatolmanyokAlairas);
            }
        }

        string GetErrorMessage(List<IratItem> iratItemsList)
        {
            string errorMessage = String.Empty;

            var hibasIratItems = iratItemsList.Where(x => x.IsError || !x.IsAllDokumentumSuccesful());

            if (hibasIratItems.Count() > 0)
            {
                List<string> hibasIratIds = new List<string>();
                List<string> hibasDokumentumIds = new List<string>();

                foreach (IratItem iratItem in hibasIratItems)
                {
                    hibasIratIds.Add(iratItem.IratId);
                    hibasDokumentumIds.AddRange(iratItem.Dokumentumok.Where(h => !h.IsSuccessful).Select(h => h.DokumentumId).ToList());
                }

                Dictionary<string, string> iratAzonositok = GetIratAzonositok(hibasIratIds);
                Dictionary<string, string> dokumentumAzonositok = GetDokumentumAzonositok(hibasDokumentumIds);

                StringBuilder sb = new StringBuilder();
                sb.Append("Az aláírási folyamat során hiba történt az alábbi eset(ek)ben:");

                int errorNum = 0;
                foreach (IratItem iratItem in hibasIratItems)
                {
                    errorNum++;

                    if (errorNum > 1)
                        sb.Append(", ");

                    string iratAzonosito = iratItem.IratId;
                    if (iratAzonositok.ContainsKey(iratItem.IratId))
                        iratAzonosito = iratAzonositok[iratItem.IratId];

                    string error = String.Format("\nIrat iktatószáma: {0}", iratAzonosito);

                    if (iratItem.IsError)
                        error += String.Format(" ({0})", iratItem.ErrorMessage);

                    foreach (DokumentumItem hashItem in iratItem.Dokumentumok.Where(h => !h.IsSuccessful))
                    {
                        string dokAzonosito = hashItem.DokumentumId;
                        if (dokumentumAzonositok.ContainsKey(hashItem.DokumentumId))
                            dokAzonosito = dokumentumAzonositok[hashItem.DokumentumId];

                        error += String.Format(" Csatolmány fájlneve: {0} ({1})", dokAzonosito, hashItem.ErrorMessage);
                    }

                    sb.Append(error);
                }

                sb.Append("\nAzok az iratok, amelyek nem szerepelnek a listában, sikeresen aláírásra kerültek.");

                errorMessage = sb.ToString();

            }

            return errorMessage;
        }

        Dictionary<string, string> GetIratAzonositok(List<string> iratIds)
        {
            Dictionary<string, string> azonositok = new Dictionary<string, string>();

            if (iratIds.Count > 0)
            {
                Contentum.eRecord.Service.EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                EREC_IraIratokSearch search = new EREC_IraIratokSearch();
                search.Id.Value = Search.GetSqlInnerString(iratIds.ToArray());
                search.Id.Operator = Query.Operators.inner;
                Result result = service.GetAll(execParam, search);

                if (result.IsError)
                {
                    Logger.Error("MicroSignerManager.GetIratAzonositok hiba", execParam, result);
                }
                else
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        string azonosito = row["Azonosito"].ToString();

                        if (!azonositok.ContainsKey(id))
                            azonositok.Add(id, azonosito);
                    }
                }
            }

            return azonositok;
        }

        Dictionary<string, string> GetDokumentumAzonositok(List<string> dokumentumIds)
        {
            Dictionary<string, string> azonositok = new Dictionary<string, string>();

            if (dokumentumIds.Count > 0)
            {
                Contentum.eDocument.Service.KRT_DokumentumokService service = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                KRT_DokumentumokSearch search = new KRT_DokumentumokSearch();
                search.Id.Value = Search.GetSqlInnerString(dokumentumIds.ToArray());
                search.Id.Operator = Query.Operators.inner;
                Result result = service.GetAll(execParam, search);

                if (result.IsError)
                {
                    Logger.Error("MicroSignerManager.GetDokumentumAzonositok hiba", execParam, result);
                }
                else
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string id = row["Id"].ToString();
                        string azonosito = row["FajlNev"].ToString();

                        if (!azonositok.ContainsKey(id))
                            azonositok.Add(id, azonosito);
                    }
                }
            }

            return azonositok;
        }

        void IratServersidePDFSign(IratItem iratItem, string megjegyzes)
        {
            try
            {
                foreach (DokumentumItem dokumentumItem in iratItem.Dokumentumok)
                {
                    try
                    {
                        ServersidePDFSign(dokumentumItem);
                        dokumentumItem.IsSigned = true;
                    }
                    catch (Exception ex)
                    {
                        dokumentumItem.IsSigned = false;
                        dokumentumItem.ErrorMessage = String.Format("Csatolmány aláírása közben hiba lépett fel: {0}", ex.Message);
                    }
                }

                //Minden csatolmány aláírása sikeres
                if (iratItem.IsAllDokumentumSigned())
                {
                    foreach (DokumentumItem dokumentumItem in iratItem.Dokumentumok)
                    {
                        try
                        {
                            UploadSignedFile(dokumentumItem);
                            dokumentumItem.IsUploaded = true;
                        }
                        catch (Exception ex)
                        {
                            dokumentumItem.IsUploaded = false;
                            dokumentumItem.ErrorMessage = String.Format("Csatolmány feltöltése közben hiba lépett fel: {0}", ex.Message);
                            break;
                        }
                    }

                    foreach (DokumentumItem dokumentumItem in iratItem.Dokumentumok)
                    {
                        DeleteFiles(dokumentumItem.DokumentumId);
                    }


                    if (iratItem.IsAllDokumentumSuccesful())
                    {
                        IratCsatolmanyAlairas(iratItem, megjegyzes);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(String.Format("MicroSignerManager.SetIratSignedHas hiba: IratId={0}", iratItem.IratId), ex);
                throw;
            }
        }

        void ServersidePDFSign(DokumentumItem dokumentumItem)
        {
            ServersidePDFSign(dokumentumItem, 1);
        }

        void ServersidePDFSign(DokumentumItem dokumentumItem, int numberOfAttempts)
        {
            try
            {
                string spUser = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                string spPW = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                string spDomain = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

                using (var client = new WebClient())
                {
                    client.Credentials = new System.Net.NetworkCredential(spUser, spPW, spDomain);
                    client.DownloadFile(dokumentumItem.ExternalLink, eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + downloadedfilename);
                }

                string testParams = @"-test yes -debug yes -debug_file_path " + eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + logServersideSignName + " ";
                if (!isTestMode) { testParams = ""; }

                string serversideParams = testParams + @"pdf_sign -in " + eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + downloadedfilename + @" -out " + eSzignoFilePath + @"out\" + dokumentumItem.DokumentumId + signedfilename + pdf_sign_params;
                Process processServerSideSign = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", serversideParams);
                processServerSideSign.WaitForExit();
                int exitCode = processServerSideSign.ExitCode;

                Logger.Debug("MicroSignerManager.ServersidePDFSign -  processServerSideSign.ExitCode=" + exitCode);

                //sikeres aláírás: ExitCode = 0, vagy 65 és 125 közötti
                if (exitCode == 0 || (exitCode >= 65 && exitCode <= 125))
                {
                    // csak figyelmeztetés, az aláírás megtörtént
                    if (exitCode != 0)
                    {
                        Logger.Warn(String.Format("MicroSignerManager.ServersidePDFSign ({0}) figyelmeztetes, processSigned.ExitCode: {1}", dokumentumItem.DokumentumId, exitCode));
                    }

                    //aláírás megtörtént
                    SetFolyamatTetelStatus(dokumentumItem.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas, exitCode.ToString());
                }
                else
                {
                    //amennyiben a hibakód szerepel a PKI_RETRY_CODES között, akkor újra meg kell próbálni az aláírást, amíg a próbálkozázok száma el nem éri a PKI_RETRY_NUM paraméter értékét
                    bool retry = RetryCodes.Contains(exitCode.ToString()) && numberOfAttempts <= RetryNum;

                    if (retry)
                    {
                        Logger.Warn("MicroSignerManager.ServersidePDFSign retry, processSigned.ExitCode: " + exitCode);
                        //várni kell az újrapróbálkozás előtt: próbálkozások száma*2 mp -et,
                        System.Threading.Thread.Sleep(numberOfAttempts * 2000);
                        ServersidePDFSign(dokumentumItem, numberOfAttempts + 1);
                    }
                    else
                    {
                        Logger.Error(String.Format("MicroSignerManager.ServersidePDFSign ({0}) hiba, processSigned.ExitCode: {1}", dokumentumItem.DokumentumId, exitCode));
                        //SetFolyamatTetelStatus(hashItem.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, exitCode.ToString());
                        throw new Exception(String.Format("eSzigno hiba: {0}", GetErrorText(exitCode)));
                    }
                }
            }
            catch (Exception exp)
            {
                Logger.Error(String.Format("MicroSignerManager.ServersidePDFSign ({0}) hiba", dokumentumItem.DokumentumId), exp);
                SetFolyamatTetelStatus(dokumentumItem.FolyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, exp.Message);
                throw;
            }
        }

        #endregion

        #region public

        public Result SetSignedHashes(string[] signedHashes, string iratIds, string dokumentumIds, string csatolmanyokIds, string folyamatTetelekIds)
        {
            Result resSignedHashes = new Result();

            try
            {

                List<IratItem> iratItemsList = GetIratItems(signedHashes, iratIds, dokumentumIds, csatolmanyokIds, folyamatTetelekIds);

                foreach (IratItem iratItem in iratItemsList)
                {
                    try
                    {
                        SetIratSignedHash(iratItem);
                    }
                    catch (Exception ex)
                    {
                        iratItem.IsError = true;
                        iratItem.ErrorMessage = String.Format("Irat aláírása közben hiba lépett fel: {0}", ex.Message);
                    }
                }

                resSignedHashes.ErrorCode = resSignedHashes.ErrorMessage = GetErrorMessage(iratItemsList);
            }
            catch (Exception ex)
            {
                Logger.Error("MicroSignerManager.SetSignedHashes hiba", ex);
                string errorMessage = String.Format("Az aláírási folyamat során hiba történt: {0}", ex.Message);
                resSignedHashes.ErrorCode = resSignedHashes.ErrorMessage = errorMessage;
            }

            return resSignedHashes;
        }

        public Result ServersidePDFSign(string sessionId, string iratIds, string[] documentIds, string dokumentumIds, string externalLinks, string folyamatTetelekIds, string proc_Id, string megjegyzes, string csatolmanyokIds)
        {
            Result resServerSide = new Result();

            try
            {

                List<IratItem> iratItemsList = GetIratItems(iratIds, dokumentumIds, externalLinks, folyamatTetelekIds, csatolmanyokIds);

                foreach (IratItem iratItem in iratItemsList)
                {
                    try
                    {
                        IratServersidePDFSign(iratItem, megjegyzes);
                    }
                    catch (Exception ex)
                    {
                        iratItem.IsError = true;
                        iratItem.ErrorMessage = String.Format("Irat aláírása közben hiba lépett fel: {0}", ex.Message);
                    }
                }

                if (!iratItemsList.Any(x => x.IsError || !x.IsAllDokumentumSuccesful()))
                {
                    try
                    {
                        Result res = eDocumentService.SetFolyamatStatus(execParam, proc_Id, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas);
                        res.CheckError();
                    }
                    catch (Exception ex)
                    {
                        Result errorResult = ResultException.GetResultFromException(ex);
                        Logger.Error(String.Format("MicroSignerManager.ServersidePDFSign - eDocumentService.SetFolyamatStatus ({0}) hiba", proc_Id), execParam, errorResult);
                    }
                }

                resServerSide.ErrorCode = resServerSide.ErrorMessage = GetErrorMessage(iratItemsList);
            }
            catch (Exception ex)
            {
                Logger.Error("MicroSignerManager.ServersidePDFSign hiba", ex);
                string errorMessage = String.Format("Az aláírási folyamat során hiba történt: {0}", ex.Message);
                resServerSide.ErrorCode = resServerSide.ErrorMessage = errorMessage;
            }

            return resServerSide;
        }
        
        #endregion
    }
}