﻿ using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.ComponentModel;
using System.Drawing.Design;
using System.Collections;
using System.Collections.Specialized;

/// <summary>
/// Summary description for TreeGridView
/// </summary>
namespace Contentum.eUIControls
{
    // http://www.webswapp.com/categories/ViewSourceCode.aspx?id=ASPNET20GVMultiSelectC-GridView
    public class MultiRowSelectGridView : GridView//, IPostBackEventHandler
    {
         private const string HiddenFieldSelectedListID = "hfSelectedList";

         private bool _ShowSelectorLabelText = false;
         public bool ShowSelectorLabelText
         {
             get { return this._ShowSelectorLabelText; }
             set { this._ShowSelectorLabelText = value; }
         }

        private string _SelectorHeaderStyleCssClass = "GridViewCheckBoxTemplateHeaderStyle";
        public string SelectorHeaderStyleCssClass {
            get { return this._SelectorHeaderStyleCssClass; }
            set { this._SelectorHeaderStyleCssClass = value; }
        }

        private string _SelectorItemStyleCssClass = "GridViewCheckBoxTemplateItemStyle";
        public string SelectorItemStyleCssClass
        {
            get { return this._SelectorItemStyleCssClass; }
            set { this._SelectorItemStyleCssClass = value; }
        }

        private HiddenField HiddenFieldSelectedList = new HiddenField();

        public string HiddenFieldSelectedListClientID
        {
            get
            {
                return HiddenFieldSelectedList.ClientID;
            }
        }

        public MultiRowSelectGridView()
            : base()
        {
            TemplateField templateField = new TemplateField();
            templateField.SortExpression = DataKeyNames.Length > 0 ? DataKeyNames[0] : "";
            templateField.HeaderStyle.CssClass = SelectorHeaderStyleCssClass;
            templateField.HeaderTemplate = new MultiRowSelectGridViewSelectorTemplate(DataControlRowType.Header, ShowSelectorLabelText);
            templateField.ItemStyle.CssClass = SelectorItemStyleCssClass;
            templateField.ItemTemplate = new MultiRowSelectGridViewSelectorTemplate(DataControlRowType.DataRow, ShowSelectorLabelText);

            this.Columns.Add(templateField);
        }

        protected override void OnPreRender(EventArgs e)
        {
            #region Apply Properties
            if (this.Columns.Count > 0)
            {
                this.Columns[0].HeaderStyle.CssClass = SelectorHeaderStyleCssClass;
                this.Columns[0].ItemStyle.CssClass = SelectorItemStyleCssClass;
            }

            if (this.HeaderRow != null && this.HeaderRow.Cells.Count > 0)
            {
                CheckBox cbSelectAll = this.HeaderRow.Cells[0].FindControl(MultiRowSelectGridViewSelectorTemplate.CheckBoxSelectAllID) as CheckBox;
                if (cbSelectAll != null)
                {
                    cbSelectAll.LabelAttributes.CssStyle.Add("display", ShowSelectorLabelText ? "" : "none");
                }
            }

            if (this.Rows != null && this.Rows.Count > 0 && this.Rows[0].Cells.Count > 0)
            {
                CheckBox cbSelect = this.Rows[0].Cells[0].FindControl(MultiRowSelectGridViewSelectorTemplate.CheckBoxSelectID) as CheckBox;
                if (cbSelect != null)
                {
                    cbSelect.LabelAttributes.CssStyle.Add("display", ShowSelectorLabelText ? "" : "none");
                }
            }
            #endregion Apply Properties

            base.OnPreRender(e);
            RegisterSelectorJavaScripts();
        }

        protected override void OnRowCreated(GridViewRowEventArgs e)
        {
            base.OnRowCreated(e);
            this.Columns[1].SortExpression = DataKeyNames.Length > 0 ? DataKeyNames[0] : "";
            //if (e.Row.RowType == DataControlRowType.Pager)
            //{
            //    e.Row.Cells[0].ColumnSpan -= 2;
            //    TableCell td = new TableCell();
            //    td.CssClass = "Label1";
            //    td.Text = "Viewing page " + this.PageIndex + " of " + this.PageCount;
            //    e.Row.Cells.Add(td);
            //    td = new TableCell();
            //    HyperLink hl = new HyperLink();
            //    hl.NavigateUrl = "default.aspx";
            //    hl.Text = "Reset";
            //    td.Controls.Add(hl);
            //    e.Row.Cells.Add(td);


            //}
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox chkAll = (CheckBox)e.Row.FindControl(MultiRowSelectGridViewSelectorTemplate.CheckBoxSelectAllID);
                if (chkAll != null)
                {
                    chkAll.Attributes.Add("onclick", "SelectAllRows()");
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox cb = (CheckBox)e.Row.FindControl(MultiRowSelectGridViewSelectorTemplate.CheckBoxSelectID);
                if (cb != null)
                {
                    cb.Attributes.Add("onclick", "SelectRow()");
                }
                e.Row.Attributes.Add("onclick", "SelectRow()");
            }
        }

        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {
            int ret = base.CreateChildControls(dataSource, dataBinding);
            HiddenFieldSelectedList.ID = HiddenFieldSelectedListID;
            this.Controls.Add(HiddenFieldSelectedList);
            return ret;
        }

        void RegisterSelectorJavaScripts()
        {
            string hfClientID = this.HiddenFieldSelectedListClientID;

            string js = @"
function SelectRow()
{
    var obj = window.event.srcElement;
    if (obj && obj.tagName)
    {
        if(obj.tagName.toLowerCase()=='input')    //this is a checkbox
        {
            var row = obj.parentNode.parentNode;
            setRow(row, obj);
        }
        else if (obj.tagName.toLowerCase()=='td') //this a table cell
        {
            //get a pointer to the tablerow
            var row = obj.parentNode;
            var cb = row.cells[0].firstChild;
            cb.checked = !cb.checked;
            setRow(row, cb);
        }
    }
}
function setHiddenField(cb)
{
    var hf = $get('" + hfClientID + @"');
    if (hf && cb && cb.value)
    {
        if (cb.checked)
        {
            if (hf.value.indexOf(cb.value + ';') < 0)
            {
                hf.value += cb.value + ';';
            }
        }
        else
        {
            if (hf.value.indexOf(cb.value + ';') >= 0)
            {
                hf.value.replace(cb.value + ';', '')
            }
        }
    }
}
function setRow(row, cb)
{
    if (row && cb)
    {
        if (!row.style || row.style.display != 'none')
        {
            row.className= (cb.checked ? 'SelectedRow' : '" + this.RowStyle.CssClass + @"');
            setHiddenField(cb);
        }
    }
}
function SelectAllRows()
{
   var chkAll = window.event.srcElement; 
   var tbl = chkAll.parentNode.parentNode.parentNode.parentNode;
   
   if (chkAll)
   {
        for(var i=1;i<tbl.rows.length;i++)
        {
            var cb = tbl.rows[i].cells[0].firstChild;
            cb.checked=chkAll.checked;
            setRow(tbl.rows[i], cb);
        }
   }
}";

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "selectorScripts", js, true);
        
        }
    }
}

/// <summary>
/// Summary description for MultiRowSelectGridViewSelectorTemplate
/// </summary>
public class MultiRowSelectGridViewSelectorTemplate : ITemplate
{
    public static string CheckBoxSelectID = "cbSelect";
    public static string CheckBoxSelectAllID = "cbSelectAll";

    private bool _ShowSelectorLabelText = false;
    public bool ShowSelectorLabelText
    {
        get { return _ShowSelectorLabelText; }
        set { _ShowSelectorLabelText = value; }
    }

    private DataControlRowType _rowType;
    public DataControlRowType RowType
    {
        get { return _rowType; }
        set { _rowType = value; }
    }

    private bool _Enabled = true;
    public bool Enabled
    {
        get { return _Enabled; }
        set { _Enabled = value; }
    }

    public MultiRowSelectGridViewSelectorTemplate()
    {
        //
        // TODO: Add constructor logic here
        //

        // default
        _rowType = DataControlRowType.DataRow;
    }

    public MultiRowSelectGridViewSelectorTemplate(DataControlRowType rowType)
    {
        _rowType = rowType;
    }

    public MultiRowSelectGridViewSelectorTemplate(DataControlRowType rowType, bool showSelectorLabelText)
        : this(rowType)
    {
        _ShowSelectorLabelText = showSelectorLabelText;
    }

    public void InstantiateIn(System.Web.UI.Control container)
    {

        switch (_rowType)
        {
            case DataControlRowType.Header:
                {
                    CheckBox cb = new CheckBox();
                    cb.ID = CheckBoxSelectAllID;
                    if (ShowSelectorLabelText == false)
                    {
                        cb.LabelAttributes.CssStyle.Add("display", "none");
                    }
                    cb.Enabled = this.Enabled;
                    container.Controls.Add(cb);
                }
                break;
            case DataControlRowType.DataRow:
                {
                    CheckBox cb = new CheckBox();
                    cb.ID = CheckBoxSelectID;
                    if (ShowSelectorLabelText == false)
                    {
                        cb.LabelAttributes.CssStyle.Add("display", "none");
                    }
                    cb.Enabled = this.Enabled;
                    cb.DataBinding += new EventHandler(this.cb_DataBind);
                    container.Controls.Add(cb);
                }
                break;
            case DataControlRowType.Footer:
                // ...
                break;
            default:
                break;

        }
    }
    private void cb_DataBind(Object sender, EventArgs e)
    {
        CheckBox cb = (CheckBox)sender;
        GridViewRow row = (GridViewRow)cb.NamingContainer;
        GridView gv = (GridView)row.NamingContainer;
        if (gv.DataKeyNames != null && gv.DataKeyNames.Length > 0)
        {
            cb.Text = DataBinder.Eval(row.DataItem, gv.DataKeyNames[0]).ToString();
        }
    }

}




   