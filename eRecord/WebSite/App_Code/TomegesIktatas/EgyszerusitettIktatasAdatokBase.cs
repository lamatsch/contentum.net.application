﻿namespace Contentum.eRecord.TomegesIktatas
{
    public abstract class EgyszerusitettIktatasAdatokBase
    {
        public string ResultIktatoszam { get; set; }

        public string ResultHibaUzenet { get; set; }

        // Előzmény ügyirat előre hozva:
        public string ElozmenyUgyirat { get; set; }

        #region Ügyirat adatok

        //public string ElozmenyUgyiratEv { get; set; }
        //public string ElozmenyUgyiratIktatokonyv { get; set; }
        //public string ElozmenyUgyiratFoszam { get; set; }
        public string UgyFajtaja { get; set; }
        public string IrattariTetelszam { get; set; }
        public string Iktatokonyv { get; set; }
        public string UgyTipusa { get; set; }
        public string UgyiratTargya { get; set; }
        public string UgyiratUgyintezesiIdeje { get; set; }
        public string UgyiratUgyintezesiHatarideje { get; set; }
        public string SzervezetiEgyseg { get; set; }
        public string UgyiratUgyintezo { get; set; }
        public string Kezelo { get; set; }
        public string UgyfelUgyindito { get; set; }
        public string UgyinditoCime { get; set; }
        public string Adoszam { get; set; }

        #endregion

        #region Irat adatok

        public string IratTargya { get; set; }
        public string IratTipus { get; set; }
        public string IratMinositese { get; set; }
        public string IratUgyintezo { get; set; }
        public string AdathordozoTipusa { get; set; }
        public string ElsodlegesAdathordozoTipusa { get; set; }
        public string IratElintezesiIdopontja { get; set; }
        public string IratUgyintezesiIdeje { get; set; }
        public string IratUgyintezesiHatarideje { get; set; }
        public string FelelosSzervezet { get; set; }
        public string UgyintezesModja { get; set; }
        public string IratIranya { get; set; }
        public string IratHatasaAzUgyintezesre { get; set; }

        #endregion

        /// <summary>
        /// Csatolmányok ('|' karakterrel elválaszott fájlútvonalak)
        /// </summary>
        public string CsatolmanyUrls { get; set; }

        /// <summary>
        /// Csatolmányok feltöltési státuszai ("OK"/"Hiba")
        /// </summary>
        public string CsatolmanyUploadStatus { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(IratIranya) && string.IsNullOrEmpty(IratTargya) && string.IsNullOrEmpty(ResultIktatoszam) 
                && string.IsNullOrEmpty(Iktatokonyv) && string.IsNullOrEmpty(ElozmenyUgyirat) && string.IsNullOrEmpty(CsatolmanyUrls);
        }
    }
}
