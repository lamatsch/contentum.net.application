﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.TomegesIktatas
{
    public abstract class IktatasImportCsvItemBase
    {
        /// <summary>
        /// Több csatolmány megadásakor az elválasztó karakter
        /// </summary>
        public const char CsatolmanyUrlDelimiter = '|';
        public const string CsatolmanyUploadStatus_OK = "OK";
        public const string CsatolmanyUploadStatus_Hiba = "Hiba";

        public const string ResultIktatoszam_Hiba = "??";

        public EgyszerusitettIktatasAdatokBase ImportRowObj { get; protected set; }

        public int RowNumber { get; protected set; }
        public bool MarIktatvaVolt { get; set; }

        public EREC_UgyUgyiratok UgyiratObj { get; set; }

        public EREC_IraIratok IratObj { get; set; }

        public ExecParam ExecParam { get; set; }

        public IktatasiParameterek IktatasiParameterek { get; set; }

        public bool IsAlszamraIktatas { get; set; }

        public Guid? IktatokonyvId { get; set; }

        public Guid? ElozmenyUgyiratId { get; set; }

        public bool IktatasCompleted { get; set; }

        public string IktatasHibaUzenet { get; set; }

        //public Result IktatasResult { get; set; }

        public bool CsatolmanyUploadInProgress { get; set; }

        public bool CsatolmanyUploadCompleted { get; set; }

        public string CsatolmanyUploadHibaUzenet { get; set; }

        public EREC_TomegesIktatasTetelek TomegesIktatasTetel;

        protected TomegesIktatasHelper _helper;

        private EREC_Csatolmanyok CreateEREC_Csatolmanyok(string iratId)
        {
            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();
            erec_Csatolmanyok.Updated.SetValueAll(false);
            erec_Csatolmanyok.Base.Updated.SetValueAll(false);

            erec_Csatolmanyok.IraIrat_Id = iratId;
            erec_Csatolmanyok.Updated.IraIrat_Id = true;

            erec_Csatolmanyok.DokumentumSzerep = KodTarak.DOKUMENTUM_SZEREP.Fodokumentum;
            erec_Csatolmanyok.Updated.DokumentumSzerep = true;

            //erec_Csatolmanyok.Leiras = CsatolmanyMegjegyzes.Text;
            //erec_Csatolmanyok.Updated.Leiras = true;

            return erec_Csatolmanyok;
        }

        private Csatolmany CreateCsatolmanyObj(string filePath)
        {
            Csatolmany csatolmany = new Csatolmany();

            csatolmany.Tartalom = File.ReadAllBytes(filePath);
            csatolmany.Nev = Path.GetFileName(filePath);

            csatolmany.Megnyithato = "1";
            csatolmany.Olvashato = "1";

            return csatolmany;
        }

        /// <summary>
        /// A megadott, még feltöltendő csatolmányok létezésének ellenőrzése.
        /// Ha nem létezik, Exception-t dobunk.
        /// </summary>
        public void CheckCsatolmanyok()
        {
            if (String.IsNullOrEmpty(ImportRowObj.CsatolmanyUrls) || ImportRowObj.CsatolmanyUrls.Trim() == "-")
            {
                return;
            }

            string[] csatolmanyUrlParts = ImportRowObj.CsatolmanyUrls.Split(CsatolmanyUrlDelimiter);

            string[] csatolmanyUploadStatusParts;
            if (!String.IsNullOrEmpty(ImportRowObj.CsatolmanyUploadStatus))
            {
                csatolmanyUploadStatusParts = ImportRowObj.CsatolmanyUploadStatus.Split(CsatolmanyUrlDelimiter);
            }
            else
            {
                csatolmanyUploadStatusParts = new string[] { };
            }

            for (int i = 0; i < csatolmanyUrlParts.Length; i++)
            {
                string csatolmanyUrl = csatolmanyUrlParts[i];

                // Fel van-e már töltve:
                if (csatolmanyUploadStatusParts.Length > i
                    && csatolmanyUploadStatusParts[i] == CsatolmanyUploadStatus_OK)
                {
                    // OK, nem kell ellenőrizni:
                    continue;
                }

                // Ellenőrzés:
                if (!File.Exists(csatolmanyUrl))
                {
                    throw new FileNotFoundException(String.Format("A megadott csatolmány nem található! ({0})", csatolmanyUrl));
                }
            }
        }

        /// <summary>
        /// Csatolmányok feltöltése, ha vannak
        /// </summary>
        public bool CallCsatolmanyFeltoltes()
        {
            if (String.IsNullOrEmpty(ImportRowObj.CsatolmanyUrls) || ImportRowObj.CsatolmanyUrls.Trim() == "-")
            {
                return false;
            }

            try
            {
                CsatolmanyUploadInProgress = true;

                // Több csatolmányt is megadhatnak, felbontjuk részekre?
                string[] csatolmanyUrlParts = ImportRowObj.CsatolmanyUrls.Split(CsatolmanyUrlDelimiter);

                string[] csatolmanyUploadStatusParts;
                if (!String.IsNullOrEmpty(ImportRowObj.CsatolmanyUploadStatus))
                {
                    csatolmanyUploadStatusParts = ImportRowObj.CsatolmanyUploadStatus.Split(CsatolmanyUrlDelimiter);
                }
                else
                {
                    csatolmanyUploadStatusParts = new string[] { };
                }

                // Az eredmény státuszok, ezt mentjük majd vissza a csv-be:
                string[] resultCsatolmanyUploadStatusParts = new string[csatolmanyUrlParts.Length];
                for (int i = 0; i < resultCsatolmanyUploadStatusParts.Length; i++)
                {
                    resultCsatolmanyUploadStatusParts[i] = String.Empty;
                }

                for (int i = 0; i < csatolmanyUrlParts.Length; i++)
                {
                    string csatolmanyUrl = csatolmanyUrlParts[i];

                    // Fel van-e már töltve:
                    if (csatolmanyUploadStatusParts.Length > i
                        && csatolmanyUploadStatusParts[i] == CsatolmanyUploadStatus_OK)
                    {
                        // OK, már fel lett töltve:
                        resultCsatolmanyUploadStatusParts[i] = CsatolmanyUploadStatus_OK;
                        continue;
                    }

                    try
                    {
                        // Ha nem volt iktatás, csak csatolmány-kiegészítés, akkor az iktatószám alapján le kell kérni az iratId-t
                        string iratId;
                        //if (this.IktatasResult == null || this.IktatasResult.IsError)
                        {
                            // Ha nincs iktatószám sem, akkor sikertelen volt az iktatás is, ilyenkor nincs csatolmány-feltöltés sem
                            if (String.IsNullOrEmpty(ImportRowObj.ResultIktatoszam) || ImportRowObj.ResultIktatoszam == ResultIktatoszam_Hiba)
                            {
                                continue;
                            }
                            else
                            {
                                iratId = GetIratIdByIktatoSzam(ImportRowObj.ResultIktatoszam).ToString();
                            }
                        }
                        //else
                        //{
                        //    iratId = (this.IktatasResult.Record as ErkeztetesIktatasResult).IratId;
                        //}

                        if (!String.IsNullOrEmpty(iratId))
                        {
                            EREC_Csatolmanyok erec_Csatolmanyok = CreateEREC_Csatolmanyok(iratId);
                            Csatolmany csatolmany = CreateCsatolmanyObj(csatolmanyUrl);

                            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            ExecParam execParam = ExecParam.Clone();
                            execParam.Record_Id = iratId;
                            var resultUpload = erec_IraIratokService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);
                            resultUpload.CheckError();

                            // Sikeres feltöltés bejegyzése:
                            resultCsatolmanyUploadStatusParts[i] = CsatolmanyUploadStatus_OK;
                        }
                    }
                    catch (Exception exc)
                    {
                        resultCsatolmanyUploadStatusParts[i] = CsatolmanyUploadStatus_Hiba;
                        CsatolmanyUploadHibaUzenet = exc.Message;
                    }
                }

                // Státuszok bejegyzése a CSV mezőbe:
                if (resultCsatolmanyUploadStatusParts.Length > 0)
                {
                    ImportRowObj.CsatolmanyUploadStatus = String.Join(CsatolmanyUrlDelimiter.ToString(), resultCsatolmanyUploadStatusParts);
                }

                CsatolmanyUploadCompleted = true;
            }
            catch (Exception exc)
            {
                CsatolmanyUploadCompleted = true;

                CsatolmanyUploadHibaUzenet = exc.Message;
            }

            // Hibaüzenet hozzáfűzése a CSV objektum hibaüzenet mezőjébe:
            if (!String.IsNullOrEmpty(CsatolmanyUploadHibaUzenet))
            {
                if (String.IsNullOrEmpty(ImportRowObj.ResultHibaUzenet))
                {
                    ImportRowObj.ResultHibaUzenet = CsatolmanyUploadHibaUzenet;
                }
                else
                {
                    ImportRowObj.ResultHibaUzenet += " " + CsatolmanyUploadHibaUzenet;
                }
            }

            return true;
        }

        /// <summary>
        /// Irat Id lekérése iktatószám alapján
        /// </summary>
        /// <param name="iktatoSzam"></param>
        /// <returns></returns>
        public Guid GetIratIdByIktatoSzam(string iktatoSzam)
        {
            var iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
            iratokSearch.Azonosito.Filter(iktatoSzam);

            var result = iratokService.GetAll(ExecParam, iratokSearch);
            result.CheckError();

            if (result.GetCount == 0)
            {
                throw new ResultException(String.Format("Nem található ez az irat: '{0}'", iktatoSzam));
            }

            return (Guid)result.Ds.Tables[0].Rows[0]["Id"];
        }

        /// <summary>
        /// Iktatás eredményének log bejegyzése
        /// </summary>
        /// <returns></returns>
        public string GetIktatasLogMsg()
        {
            string logMsg = RowNumber + ". sor: ";
            if (!String.IsNullOrEmpty(IktatasHibaUzenet))
            {
                logMsg += "Hiba: " + IktatasHibaUzenet;
            }
            else if (MarIktatvaVolt)
            {
                logMsg += String.Format("Nem kellett iktatni ({0})", ImportRowObj.ResultIktatoszam);
            }
            else if (IktatasCompleted)
            {
                logMsg += String.Format("OK, irat iktatószáma: {0}", ImportRowObj.ResultIktatoszam);
            }

            return logMsg;
        }

        /// <summary>
        /// Csatolmány feltöltésének log bejegyzése
        /// </summary>
        /// <returns></returns>
        public string GetCsatolmanyFeltoltesLogMsg()
        {
            string logMsg = RowNumber + ". sor: ";

            if (String.IsNullOrEmpty(ImportRowObj.CsatolmanyUrls) || ImportRowObj.CsatolmanyUrls.Trim() == "-")
            {
                logMsg += "Nincs csatolmány";
            }

            // Csatolmány feltöltés hiba:
            if (!String.IsNullOrEmpty(CsatolmanyUploadHibaUzenet))
            {
                logMsg += " Csatolmány feltöltés hiba: " + CsatolmanyUploadHibaUzenet;
            }
            // Csatolmány feltöltés státuszok:
            else if (!String.IsNullOrEmpty(ImportRowObj.CsatolmanyUploadStatus))
            {
                logMsg += "Csatolmány feltöltés státusz(ok): " + ImportRowObj.CsatolmanyUploadStatus;
            }

            return logMsg;
        }
    }
}
