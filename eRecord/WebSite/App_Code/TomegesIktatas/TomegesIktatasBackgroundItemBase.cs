﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Contentum.eRecord.TomegesIktatas
{
    public abstract class TomegesIktatasBackgroundItemBase
    {
        protected TomegesIktatasHelper _helper;
        protected ExecParam _execParam;
        public int Prio;

        public class LogItem
        {
            public DateTime LogTime { get; private set; }

            public string LogMsg { get; private set; }

            public LogItem(string logMsg)
            {
                this.LogTime = DateTime.Now;
                this.LogMsg = logMsg;
            }
        }

        #region Constructor

        protected TomegesIktatasBackgroundItemBase(List<IktatasImportCsvItemBase> iktatasAdatokList, TomegesIktatasHelper helper, ExecParam execParam, int prio)
        {
            Prio = prio;
            _execParam = execParam;
            _helper = helper;
            IktatasAdatokList = iktatasAdatokList;
            BackgroundProcessId = Guid.NewGuid();
            Logs = new List<LogItem>();
        }

        #endregion

        #region Properties

        public List<IktatasImportCsvItemBase> IktatasAdatokList { get; private set; }

        public List<LogItem> Logs { get; private set; }

        /// <summary>
        /// A háttérfolyamat azonosítója
        /// </summary>
        public Guid BackgroundProcessId { get; private set; }

        /// <summary>
        /// A háttérfolyamat befejeződött
        /// </summary>
        public bool BackgroundProcessEnded { get; set; }

        public string BackgroundProcessError { get; set; }

        /// <summary>
        /// Az eredmény CSV fájl neve
        /// </summary>
        public string ResultTempFileName { get; set; }
        public string ResultTempDirName { get; set; }

        #endregion

        public abstract void StartIktatas(byte[] csvContent);

        public abstract byte[] GetResultCsvContent();


        private const string cacheKey_TomegesIktatasProcesses = "TomegesIktatasProcesses";

        private static Dictionary<Guid, TomegesIktatasBackgroundItemBase> GetTomegesIktatasProcesses()
        {
            return HttpContext.Current.Cache[cacheKey_TomegesIktatasProcesses] as Dictionary<Guid, TomegesIktatasBackgroundItemBase>;
        }

        /// <summary>
        /// Tömeges iktatás háttérfolyamat lekérése azonosító alapján
        /// </summary>
        /// <param name="processId"></param>
        /// <returns></returns>
        public static TomegesIktatasBackgroundItemBase GetTomegesIktatasBgItemByProcessId(Guid processId)
        {
            var tomegesIktatasProcesses = GetTomegesIktatasProcesses();
            if (tomegesIktatasProcesses == null
                 || !tomegesIktatasProcesses.ContainsKey(processId))
            {
                return null;
            }
            else
            {
                return tomegesIktatasProcesses[processId];
            }
        }

        /// <summary>
        /// Tömeges iktatás háttérfolyamat beregisztrálása, eltárolása a Cache-ben
        /// </summary>
        /// <param name="bgItem"></param>
        public void RegisterBackgroundProcess()
        {
            var tomegesIktatasProcesses = GetTomegesIktatasProcesses();
            if (tomegesIktatasProcesses == null)
            {
                tomegesIktatasProcesses = new Dictionary<Guid, TomegesIktatasBackgroundItemBase>();
                // 8 óra tétlenség után dobja ki a cache-ből:
                HttpContext.Current.Cache.Insert(cacheKey_TomegesIktatasProcesses, tomegesIktatasProcesses, null
                    , System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(8));
            }

            tomegesIktatasProcesses.Add(BackgroundProcessId, this);
        }

        public void Log(string message)
        {
            if (!String.IsNullOrEmpty(message))
            {
                Logs.Add(new LogItem(message));
            }
        }

        public void Finish(string fileNamePrefix)
        {
            Log(Prio < 3 ? "Tömeges iktatás befejeződött." : "Tömeges iktatás előkészítése befejeződött.");

            // Eredmény CSV fájl előállítása:
            byte[] resultCsvContent = GetResultCsvContent();
            // Temp fájl létrehozása:
            string tempDirectory;
            string tempFileName = fileNamePrefix + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";
            Contentum.eRecord.Utility.FileFunctions.Upload.SaveFileToTempDirectory(resultCsvContent, tempFileName, out tempDirectory);

            ResultTempDirName = tempDirectory;
            ResultTempFileName = tempFileName;
        }

    }
}
