﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using System;

namespace Contentum.eRecord.TomegesIktatas
{
    public class TomegesIktatasCim
    {
        public string OrszagNev { get; set; }
        public string IRSZ { get; set; }
        public string TelepulesNev { get; set; }
        public string KozteruletNev { get; set; }
        public string KozteruletTipusNev { get; set; }
        public string Hazszam { get; set; }
        public string Hazszamig { get; set; }
        public string HazszamBetujel { get; set; }
        public string Lepcsohaz { get; set; }
        public string Emelet { get; set; }
        public string Ajto { get; set; }
        public string AjtoBetujel { get; set; }
        public string HRSZ { get; set; }
        public string Postafiok { get; set; }
        public string ElektronikusCim { get; set; }

        public KRT_Cimek GetKRT_Cimek(string kuldesMod, bool isElektronikus)
        {
            var krt_Cimek = new KRT_Cimek
            {
                OrszagNev = OrszagNev,
                IRSZ = IRSZ,
                TelepulesNev = TelepulesNev,
                KozteruletNev = KozteruletNev,
                KozteruletTipusNev = KozteruletTipusNev,
                Hazszam = Hazszam,
                Hazszamig = Hazszamig,
                HazszamBetujel = HazszamBetujel,
                Lepcsohaz = Lepcsohaz,
                Szint = Emelet,
                Ajto = Ajto,
                AjtoBetujel = AjtoBetujel,
                HRSZ = HRSZ
            };

            krt_Cimek.Updated.SetValueAll(true);
            krt_Cimek.Base.Updated.SetValueAll(true);

            if (kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.Elhisz || kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu) //(!String.IsNullOrEmpty(ElektronikusCim))
            {
                krt_Cimek.Tipus = KodTarak.Cim_Tipus.Hivatali_Kapu;
                krt_Cimek.CimTobbi = ElektronikusCim;
            }
            else if (kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.E_mail)
            {
                krt_Cimek.Tipus = KodTarak.Cim_Tipus.Email;
                krt_Cimek.CimTobbi = ElektronikusCim;
            }
            else if (!String.IsNullOrEmpty(Postafiok))
            {
                krt_Cimek.Tipus = KodTarak.Cim_Tipus.Postafiok;
                krt_Cimek.CimTobbi = Postafiok;
            }
            else
            {
                krt_Cimek.Tipus = KodTarak.Cim_Tipus.Postai;
            }

            krt_Cimek.Kategoria = "K";

            return krt_Cimek;
        }

        public KRT_CimekSearch GetKRT_Cimek_Search()
        {
            var cim = new KRT_CimekSearch();

            cim.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
            cim.ErvKezd.AndGroup("100");
            cim.ErvVege.GreaterOrEqual(Query.SQLFunction.getdate);
            cim.ErvVege.AndGroup("100");

            cim.OrszagNev.FilterIfNotEmpty(OrszagNev);
            cim.IRSZ.FilterIfNotEmpty(IRSZ);
            cim.TelepulesNev.FilterIfNotEmpty(TelepulesNev);
            cim.KozteruletNev.FilterIfNotEmpty(KozteruletNev);
            cim.KozteruletTipusNev.FilterIfNotEmpty(KozteruletTipusNev);
            cim.Hazszam.FilterIfNotEmpty(Hazszam);
            cim.Hazszamig.FilterIfNotEmpty(Hazszamig);
            cim.HazszamBetujel.FilterIfNotEmpty(HazszamBetujel);
            cim.Lepcsohaz.FilterIfNotEmpty(Lepcsohaz);
            cim.Szint.FilterIfNotEmpty(Emelet);
            cim.Ajto.FilterIfNotEmpty(Ajto);
            cim.AjtoBetujel.FilterIfNotEmpty(AjtoBetujel);
            cim.HRSZ.FilterIfNotEmpty(HRSZ);

            if (!String.IsNullOrEmpty(Postafiok))
            {
                cim.Tipus.Filter(KodTarak.Cim_Tipus.Postafiok);
                cim.CimTobbi.Filter(Postafiok);
            }
            else if (!String.IsNullOrEmpty(ElektronikusCim))
            {
                cim.Tipus.Filter(KodTarak.Cim_Tipus.Email);
                cim.CimTobbi.Filter(ElektronikusCim);
            }
            else
            {
                cim.Tipus.Filter(KodTarak.Cim_Tipus.Postai);
            }

            return cim;
        }

        private bool IsOk(string s)
        {
            return !String.IsNullOrEmpty(s) && s.Trim().Length > 0;
        }

        public bool IsValid(bool isElektronikus)
        {
            if (isElektronikus)
            {
                return IsOk(ElektronikusCim);
            }
            else
            {
                var ok = IsOk(OrszagNev) && IsOk(TelepulesNev) && IsOk(IRSZ) && (IsOk(Postafiok) || IsOk(HRSZ) || (IsOk(KozteruletNev) && IsOk(KozteruletTipusNev) && IsOk(Hazszam)));
                return ok;
            }
        }
    }
}
