﻿using Contentum.eBusinessDocuments;
using Contentum.eIntegrator.Service.Helper;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.TomegesIktatas
{
    public abstract class TomegesIktatasCsvBase
    {
        protected UtilityTomegesIktatas _util;

        protected byte[] _csvFileContent;
        protected ExecParam _execParam;
        protected int _priority;

        //protected TomegesIktatasBackgroundItem _tomegesIktatasBackgroundItem;
        protected TomegesIktatasHelper _helper;

        #region Cache adatok

        /// <summary>
        /// Érkeztetőkönyv - Id párok
        /// </summary>
        protected Dictionary<string, Guid> _erkeztetokonyvNevekDict = new Dictionary<string, Guid>();

        /// <summary>
        /// Iktatókonyv név - EREC_IraIktatoKonyvek
        /// </summary>
        protected Dictionary<string, Utility.IktatoKonyvek.IktatokonyvItem> _iktatokonyvNevekDict = new Dictionary<string, Utility.IktatoKonyvek.IktatokonyvItem>();

        /// <summary>
        /// Ügyirat azonosító - Id párok
        /// </summary>
        protected Dictionary<string, Guid> _elozmenyUgyiratAzonDict = new Dictionary<string, Guid>();
        protected Dictionary<string, string> _elozmenyUgyiratFoszamDict = new Dictionary<string, string>();
        protected Dictionary<Guid, Guid> _elozmenyUgyiratIktatokonyvek = new Dictionary<Guid, Guid>();

        /// <summary>
        /// Csoport név - Id párok
        /// </summary>
        protected Dictionary<string, Guid> _csoportNevekDict = new Dictionary<string, Guid>();

        /// <summary>
        /// Partnernév - Id párok
        /// </summary>
        protected Dictionary<string, Guid> _partnerNevekDict = new Dictionary<string, Guid>();

        /// <summary>
        /// Partner adószám - Id párok
        /// </summary>
        protected Dictionary<string, Guid> _partnerAdoszamokDict = new Dictionary<string, Guid>();

        /// <summary>
        /// Irattári tételszám - Id párok
        /// </summary>
        protected Dictionary<string, Guid> _irattariTetelekDict = new Dictionary<string, Guid>();

        /// <summary>
        /// Ügytípus név - ügytípus kód párok
        /// </summary>
        protected Dictionary<string, string> _ugytipusNevekDict = new Dictionary<string, string>();

        #endregion

        #region Kódcsoport konstansok

        protected const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
        protected const string kcs_IRATTIPUS = "IRATTIPUS";
        protected const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";
        protected const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
        protected const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
        protected const string kcs_IRATMINOSITES = "IRAT_MINOSITES";
        protected const string kcs_IDOEGYSEG = "IDOEGYSEG";
        protected const string kcs_KULD_KEZB_MODJA = "KULD_KEZB_MODJA";
        protected const string kcs_KULD_CIMZES_TIPUS = "KULD_CIMZES_TIPUS";
        protected const string kcs_ErkeztetoKonyv = "ERKEZTETOKONYV";
        protected const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
        protected const string kcs_SURGOSSEG = "SURGOSSEG";
        protected const string kcs_BoritoTipus = "KULDEMENY_BORITO_TIPUS";
        protected const string kcs_IKTATASI_KOTELEZETTSEG = "IKTATASI_KOTELEZETTSEG";
        protected const string kcs_IRAT_HATASA_UGYINTEZESRE = "IRAT_HATASA_UGYINTEZESRE";
        protected const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";
        protected const string kcs_ELSODLEGES_ADATHORDOZO = "ELSODLEGES_ADATHORDOZO";
        protected const string kcs_UGYIRAT_INTEZESI_IDO = "UGYIRAT_INTEZESI_IDO";
        protected const string kcs_IRAT_UGYINT_MODJA = "IRAT_UGYINT_MODJA";
        protected const string kcs_POSTAZAS_IRANYA = "POSTAZAS_IRANYA";
        protected const string kcs_ALAIRO_SZEREP = "ALAIRO_SZEREP";

        #endregion


        #region Properties

        public System.Web.SessionState.HttpSessionState Session { get; protected set; }
        public System.Web.Caching.Cache Cache { get; protected set; }

        public List<IktatasImportCsvItemBase> IktatasAdatokList { get; set; }

        /// <summary>
        /// Az eredmény CSV fájl neve
        /// </summary>
        public string ResultTempFileName { get; set; }
        public string ResultTempDirName { get; set; }

        #endregion

        #region Methods

        private void AddResultToCache(Result result, Dictionary<string, Guid> dict, string dataField)
        {
            AddResultToCache(result, dict, dataField, "Id");
        }

        private void AddResultToCache(Result result, Dictionary<string, Guid> dict, string dataField, string idField)
        {
            result.CheckError();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                if (row[dataField] != DBNull.Value)
                {
                    var data = row[dataField].ToString();
                    if (!String.IsNullOrEmpty(data))
                    {
                        dict[data] = row[idField] == DBNull.Value ? Guid.Empty : (Guid)row[idField];
                    }
                }
            }
        }

        protected EREC_IraIktatoKonyvek GetIktatoKonyvByName(string iktatoKonyvNev)
        {
            Contentum.eRecord.Utility.IktatoKonyvek.IktatokonyvItem iktatoKonyv = null;
            if (!String.IsNullOrEmpty(iktatoKonyvNev))
            {
                if (_iktatokonyvNevekDict.ContainsKey(iktatoKonyvNev))
                {
                    iktatoKonyv = _iktatokonyvNevekDict[iktatoKonyvNev];
                }
                else
                {
                    throw new ResultException(String.Format("Nincs ilyen iktatókönyv: '{0}'", iktatoKonyvNev));
                }
            }

            return iktatoKonyv == null ? null : new EREC_IraIktatoKonyvek()
            {
                Id = iktatoKonyv.Id,
                Nev = iktatoKonyv.Text,
                Ev = iktatoKonyv.Ev,
                LezarasDatuma = iktatoKonyv.LezarasDatuma,
                IktSzamOsztas = iktatoKonyv.IktSzamOsztas,
                MegkulJelzes = iktatoKonyv.MegkulJelzes,
            };
        }

        /// <summary>
        /// Partner_Id felvétele az adószám szótárba
        /// </summary>
        /// <param name="adoszamField"></param>
        /// <param name="result"></param>
        protected void AddPartnerIdToAdoszamDict(string adoszamField, Result result)
        {
            AddResultToCache(result, _partnerAdoszamokDict, adoszamField, "Partner_Id");
        }

        /// <summary>
        /// Partner adószámok - Id szótár feltöltése
        /// </summary>
        /// <param name="partnerAdoszamok"></param>
        protected void FillPartnerAdoszamokCache(List<string> partnerAdoszamok)
        {
            _partnerAdoszamokDict.Clear();

            if (partnerAdoszamok.Count == 0) { return; }

            // személyek/adószám
            var searchSzemelyek = new KRT_SzemelyekSearch();
            searchSzemelyek.Adoszam.In(partnerAdoszamok.Distinct());

            var result = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_SzemelyekService().GetAll(
                UI.SetExecParamDefault(Session), searchSzemelyek);
            AddPartnerIdToAdoszamDict("Adoszam", result);

            // személyek/adóazonosító
            searchSzemelyek = new KRT_SzemelyekSearch();
            searchSzemelyek.Adoazonosito.In(partnerAdoszamok.Distinct());

            result = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_SzemelyekService().GetAll(
                UI.SetExecParamDefault(Session), searchSzemelyek);
            AddPartnerIdToAdoszamDict("AdoAzonosito", result);

            // vállalkozások
            var searchVallalkozasok = new KRT_VallalkozasokSearch();
            searchVallalkozasok.Adoszam.In(partnerAdoszamok.Distinct());

            result = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_VallalkozasokService().GetAll(
                UI.SetExecParamDefault(Session), searchVallalkozasok);
            AddPartnerIdToAdoszamDict("Adoszam", result);
        }

        /// <summary>
        /// Érkeztetőkönyvnevek szótár feltöltése
        /// </summary>
        protected void FillErkeztetokonyvNevek()
        {
            _erkeztetokonyvNevekDict.Clear();

            List<Contentum.eRecord.Utility.IktatoKonyvek.IktatokonyvItem> iktatokonyvekList;

            var result = Contentum.eRecord.Utility.IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(
                    Session, Contentum.eUtility.Constants.IktatoErkezteto.Erkezteto, true, false, out iktatokonyvekList);
            result.CheckError();

            foreach (var iktatokonyv in iktatokonyvekList)
            {
                _erkeztetokonyvNevekDict[iktatokonyv.Text] = new Guid(iktatokonyv.Id);
            }
        }

        /// <summary>
        /// Iktatókönyvnevek szótár feltöltése
        /// </summary>
        protected void FillIktatokonyvNevek()
        {
            _iktatokonyvNevekDict.Clear();

            List<Contentum.eRecord.Utility.IktatoKonyvek.IktatokonyvItem> iktatokonyvekList;

            var result = Contentum.eRecord.Utility.IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(
                    Session, Contentum.eUtility.Constants.IktatoErkezteto.Iktato, true, false, out iktatokonyvekList);

            result.CheckError();

            foreach (var iktatokonyv in iktatokonyvekList)
            {
                _iktatokonyvNevekDict[iktatokonyv.Text] = iktatokonyv;
            }
        }

        /// <summary>
        /// Előzmény ügyiratok szótár feltöltése
        /// </summary>
        /// <param name="ugyiratAzonList"></param>
        protected void FillElozmenyUgyiratok(List<string> ugyiratAzonList)
        {
            _elozmenyUgyiratAzonDict.Clear();
            _elozmenyUgyiratFoszamDict.Clear();
            // ügyiratok iktatókönyvei cache:
            _elozmenyUgyiratIktatokonyvek.Clear();

            if (ugyiratAzonList.Count == 0) { return; }

            var searchUgyirat = new EREC_UgyUgyiratokSearch();
            searchUgyirat.Azonosito.In(ugyiratAzonList.Distinct());

            var result = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService().GetAll(
                UI.SetExecParamDefault(Session), searchUgyirat);
            result.CheckError();

            // Eredmény feldolgozása:
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string azonosito = row["Azonosito"].ToString();
                Guid ugyiratId = (Guid)row["Id"];

                _elozmenyUgyiratAzonDict[azonosito] = ugyiratId;
                _elozmenyUgyiratFoszamDict[azonosito] = row["Foszam"].ToString();
                _elozmenyUgyiratIktatokonyvek[ugyiratId] = (Guid)row["IraIktatokonyv_Id"];
            }
        }

        /// <summary>
        /// Csoportnév - csoportId párok szótár feltöltése
        /// </summary>
        /// <param name="csoportNevek"></param>
        protected void FillCsoportNevekAll()
        {
            _csoportNevekDict.Clear();

            //if (csoportNevek == null
            //    || csoportNevek.Count == 0) { return; }

            KRT_CsoportokSearch searchCsop = new KRT_CsoportokSearch();
            //searchCsop.Nev.Value = "'" + String.Join("','", csoportNevek.ToArray()) + "'";
            //searchCsop.Nev.Operator = Query.Operators.inner;

            var result = eAdminService.ServiceFactory.GetKRT_CsoportokService().GetAll(
                UI.SetExecParamDefault(Session), searchCsop);
            AddResultToCache(result, _csoportNevekDict, "Nev");
        }

        /// <summary>
        /// Partnernév - Id szótár feltöltése
        /// </summary>
        /// <param name="partnerNevek"></param>
        protected void FillPartnerNevekCache(List<string> partnerNevek)
        {
            _partnerNevekDict.Clear();

            if (partnerNevek.Count == 0) { return; }

            KRT_PartnerekSearch searchPartner = new KRT_PartnerekSearch();
            searchPartner.Nev.In(partnerNevek.Distinct());

            var result = eAdminService.ServiceFactory.GetKRT_PartnerekService().GetAll(
                UI.SetExecParamDefault(Session), searchPartner);

            AddResultToCache(result, _partnerNevekDict, "Nev");
        }

        /// <summary>
        /// Irattári tételszámok - Id szótár feltöltése
        /// </summary>
        /// <param name="irattariTetelszamok"></param>
        protected void FillIrattariTetelszamokCache()
        {
            _irattariTetelekDict.Clear();

            //if (irattariTetelszamok == null
            //    || irattariTetelszamok.Count == 0)
            //{
            EREC_IraIrattariTetelekSearch searchIrattariTetelek = new EREC_IraIrattariTetelekSearch();
            //searchIrattariTetelek.IrattariTetelszam.Value = "'" + String.Join("','", irattariTetelszamok.ToArray()) + "'";
            //searchIrattariTetelek.IrattariTetelszam.Operator = Query.Operators.inner;

            var result = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService().GetAll(
                UI.SetExecParamDefault(Session), searchIrattariTetelek);
            AddResultToCache(result, _irattariTetelekDict, "IrattariTetelszam");
            //}
        }

        /// <summary>
        /// Ügytípus név - ügytípus kód párok feltöltése
        /// </summary>
        /// <param name="ugytipusok"></param>
        protected void FillUgyTipusNevekAll()
        {
            _ugytipusNevekDict.Clear();

            EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            ExecParam execParam = _execParam.Clone();

            EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

            //search.Ugykor_Id.Value = ugykor_Id;
            //search.Ugykor_Id.Operator = Query.Operators.equals;

            search.Ugytipus.NotNull();

            search.EljarasiSzakasz.IsNull();
            search.Irattipus.IsNull();

            search.OrderBy = "UgytipusNev";

            Result result = service.GetAll(execParam, search);
            result.CheckError();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                //string iratMetaDef_Id = row["Id"].ToString();
                string ugytipus = row["Ugytipus"].ToString();
                string ugytipusNev = row["UgytipusNev"].ToString();

                if (!String.IsNullOrEmpty(ugytipusNev))
                {
                    _ugytipusNevekDict[ugytipusNev] = ugytipus;
                }
            }
        }

        protected IktatasiParameterek CreateIktatasiParameterek(EgyszerusitettIktatasAdatokBase importRowObj, Guid? elozmenyUgyiratId, string ugytipus, string irattariTetelszamId, out bool isAlszamraIktatas, out Guid? iktatoKonyvId)
        {
            var iktatasiParameterek = new IktatasiParameterek();

            var erec_IraIktatoKonyv = GetIktatoKonyvByName(importRowObj.Iktatokonyv);
            iktatoKonyvId = erec_IraIktatoKonyv == null ? null : (Guid?)new Guid(erec_IraIktatoKonyv.Id);

            bool isIktatokonyvLezart = Contentum.eRecord.Utility.IktatoKonyvek.Lezart(erec_IraIktatoKonyv);
            bool isIktatokonyvLezartFolyosorszamos = erec_IraIktatoKonyv.IktSzamOsztas == KodTarak.IKT_SZAM_OSZTAS.Folyosorszamos;

            isAlszamraIktatas = elozmenyUgyiratId != null && (!isIktatokonyvLezart || isIktatokonyvLezartFolyosorszamos);

            if (isAlszamraIktatas)
            {
                if (!isIktatokonyvLezart || isIktatokonyvLezartFolyosorszamos)
                {
                    // Az előzmény ügyirat iktatókönyve kell:
                    iktatoKonyvId = _elozmenyUgyiratIktatokonyvek[elozmenyUgyiratId.Value];
                }
                else
                {
                    // Ha lezárt iktatókönyvben lévő ügyiratba iktatunk, akkor a Cascadingdrop downból vesszük az adatokat
                    //Erec_IraIktatokonyvek_Id = GetSelectedIktatokonyvId();
                    iktatasiParameterek.UgykorId = irattariTetelszamId;
                    iktatasiParameterek.Ugytipus = ugytipus;
                }
            }
            else
            {
                // ha előzményt választott, de az előzmény lezárt iktatókönyvben volt,
                // és van joga főszámra iktatni, akkor itt adjuk át a szerelendő eredeti ügyirat azonosítóját
                if (isIktatokonyvLezart && !isIktatokonyvLezartFolyosorszamos)
                {
                    iktatasiParameterek.SzerelendoUgyiratId = (elozmenyUgyiratId != null) ? elozmenyUgyiratId.ToString() : String.Empty;
                }

                iktatasiParameterek.UgykorId = irattariTetelszamId;
                iktatasiParameterek.Ugytipus = ugytipus;
            }
            return iktatasiParameterek;
        }

        private string GetCimWhere(string cimSTR)
        {
            return String.Format(@"dbo.fn_MergeCim
			  (KRT_Cimek.Tipus,
			   KRT_Cimek.OrszagNev,
			   KRT_Cimek.IRSZ,
			   KRT_Cimek.TelepulesNev,
			   KRT_Cimek.KozteruletNev,
			   KRT_Cimek.KozteruletTipusNev,
			   KRT_Cimek.Hazszam,
			   KRT_Cimek.Hazszamig,
			   KRT_Cimek.HazszamBetujel,
			   KRT_Cimek.Lepcsohaz,
			   KRT_Cimek.Szint,
			   KRT_Cimek.Ajto,
			   KRT_Cimek.AjtoBetujel,
			   -- BLG_1347
			   KRT_Cimek.HRSZ,
			   KRT_Cimek.CimTobbi) = '{0}'", cimSTR);
        }

        protected string GetPartnerCimId(string partnerId, string cimSTR)
        {
            var cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

            var cimSearch = new KRT_PartnerCimekSearch
            {
                WhereByManual = " and " + GetCimWhere(cimSTR)
            };

            cimSearch.ErvKezd.LessOrEqual(Query.SQLFunction.getdate);
            cimSearch.ErvKezd.AndGroup("100");
            cimSearch.ErvVege.GreaterOrEqual(Query.SQLFunction.getdate);
            cimSearch.ErvVege.AndGroup("100");

            Result res = cimekService.GetAllByPartner(_execParam, new KRT_Partnerek { Id = partnerId }, KodTarak.Cim_Tipus.Postai, cimSearch);

            if (!res.IsError && res.GetCount > 0)
            {
                return res.Ds.Tables[0].Rows[0]["Id"].ToString();
            }
            return String.Empty;
        }

        public EREC_UgyUgyiratok GetBusinessObjectFromComponents_EREC_UgyUgyiratok(EgyszerusitettIktatasAdatokBase row)
        {
            var erec_UgyUgyiratok = new EREC_UgyUgyiratok();
            // összes mező update-elhetőségét kezdetben letiltani:
            erec_UgyUgyiratok.Updated.SetValueAll(false);
            erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

            // ha van előzmény ügyirat, annak az iktatókönyve lezárt-e
            var erec_IraIktatoKonyv = GetIktatoKonyvByName(row.Iktatokonyv);
            bool elozmenyUgyiratIktatokonyvLezart = erec_IraIktatoKonyv != null && Contentum.eRecord.Utility.IktatoKonyvek.Lezart(erec_IraIktatoKonyv);

            // Ha nincs előzmény (nincs betöltve ügyirat) vagy az előzmény lezárt iktatókönyvben van
            if (String.IsNullOrEmpty(row.ElozmenyUgyirat) || elozmenyUgyiratIktatokonyvLezart)
            {
                #region Kötelező mezők ellenőrzése

                // Iktatókönyv kötelező:
                if (String.IsNullOrEmpty(row.Iktatokonyv))
                {
                    _util.ThrowRequiredFieldException("Iktatókönyv");
                }
                // Kezelő:
                if (String.IsNullOrEmpty(row.Kezelo))
                {
                    _util.ThrowRequiredFieldException("Kezelő");
                }
                // Tárgy:
                if (String.IsNullOrEmpty(row.UgyiratTargya))
                {
                    _util.ThrowRequiredFieldException("Ügyirat tárgya");
                }

                #endregion

                if (!String.IsNullOrEmpty(row.IrattariTetelszam))
                {
                    if (_irattariTetelekDict.ContainsKey(row.IrattariTetelszam))
                    {
                        erec_UgyUgyiratok.IraIrattariTetel_Id = _irattariTetelekDict[row.IrattariTetelszam].ToString();
                        erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = true;
                    }
                    else
                    {
                        throw new ResultException(String.Format("Nincs ilyen irattári tételszám: '{0}'", row.IrattariTetelszam));
                    }
                }

                if (!String.IsNullOrEmpty(row.UgyTipusa))
                {
                    if (_ugytipusNevekDict.ContainsKey(row.UgyTipusa))
                    {
                        erec_UgyUgyiratok.UgyTipus = _ugytipusNevekDict[row.UgyTipusa];
                        erec_UgyUgyiratok.Updated.UgyTipus = true;
                    }
                    else
                    {
                        throw new ResultException(String.Format("Nincs ilyen ügytípus: '{0}'", row.UgyTipusa));
                    }
                }

                /// Ügyirat intézési ideje csv-ben így kell megadni pl.: "22 nap"
                /// --> szétbontjuk két részre: IntezesiIdo + IntezesiIdoegyseg
                /// 

                if (!String.IsNullOrEmpty(row.UgyiratUgyintezesiIdeje))
                {
                    string[] intezesiIdoParts = row.UgyiratUgyintezesiIdeje.Split(' ');
                    string intezesiIdoStr = intezesiIdoParts[0];
                    string intezesiIdoEgysegStr = (intezesiIdoParts.Length > 1) ? intezesiIdoParts[1] : String.Empty;

                    erec_UgyUgyiratok.IntezesiIdo = _util.GetKodtarKodByKodtarNev(kcs_UGYIRAT_INTEZESI_IDO, intezesiIdoStr);
                    erec_UgyUgyiratok.Updated.IntezesiIdo = true;

                    if (!String.IsNullOrEmpty(intezesiIdoEgysegStr))
                    {
                        erec_UgyUgyiratok.IntezesiIdoegyseg = _util.GetKodtarKodByKodtarNev(kcs_IDOEGYSEG, intezesiIdoEgysegStr);
                        erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = true;
                    }
                }

                if (!String.IsNullOrEmpty(row.UgyiratUgyintezesiHatarideje))
                {
                    DateTime dat;
                    if (DateTime.TryParse(row.UgyiratUgyintezesiHatarideje, out dat))
                    {
                        erec_UgyUgyiratok.Hatarido = dat.ToString();
                        erec_UgyUgyiratok.Updated.Hatarido = true;
                    }
                    else
                    {
                        throw new ResultException(String.Format("A megadott 'Ügyirat ügyintézési határideje' nem megfelelő formátumú: '{0}'", row.UgyiratUgyintezesiHatarideje));
                    }
                }

                erec_UgyUgyiratok.Targy = row.UgyiratTargya;
                erec_UgyUgyiratok.Updated.Targy = true;

                // kezelő:
                if (!String.IsNullOrEmpty(row.Kezelo))
                {
                    if (_csoportNevekDict.ContainsKey(row.Kezelo))
                    {
                        erec_UgyUgyiratok.Csoport_Id_Felelos = _csoportNevekDict[row.Kezelo].ToString();
                        erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = true;
                    }
                    else
                    {
                        // Nincs ilyen nevű fehasználó/csoport:
                        throw new ResultException(String.Format("Nincs ilyen nevű felhasználó/csoport: '{0}'", row.Kezelo));
                    }
                }

                //ügyintéző
                if (!String.IsNullOrEmpty(row.UgyiratUgyintezo))
                {
                    if (_csoportNevekDict.ContainsKey(row.UgyiratUgyintezo))
                    {
                        erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = _csoportNevekDict[row.UgyiratUgyintezo].ToString();
                        erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;
                    }
                    else
                    {
                        // Nincs ilyen nevű fehasználó/csoport:
                        throw new ResultException(String.Format("Nincs ilyen nevű felhasználó/csoport: '{0}'", row.UgyiratUgyintezo));
                    }
                }

                // ügyfelelős (Szervezeti egység):
                if (!String.IsNullOrEmpty(row.SzervezetiEgyseg))
                {
                    if (_csoportNevekDict.ContainsKey(row.SzervezetiEgyseg))
                    {
                        erec_UgyUgyiratok.Csoport_Id_Ugyfelelos = _csoportNevekDict[row.SzervezetiEgyseg].ToString();
                        erec_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = true;
                    }
                    else
                    {
                        // Nincs ilyen nevű fehasználó/csoport:
                        throw new ResultException(String.Format("Nincs ilyen nevű szervezet: '{0}'", row.SzervezetiEgyseg));
                    }
                }

                // ügyindító/ügyfél adószám alapján
                if (!String.IsNullOrEmpty(row.Adoszam))
                {
                    if (_partnerAdoszamokDict.ContainsKey(row.Adoszam))
                    {
                        erec_UgyUgyiratok.Partner_Id_Ugyindito = _partnerAdoszamokDict[row.Adoszam].ToString();
                        erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito = true;
                    }
                }

                // ügyindító/ügyfél név alapján
                if (!erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito && !String.IsNullOrEmpty(row.UgyfelUgyindito))
                {
                    if (_partnerNevekDict.ContainsKey(row.UgyfelUgyindito))
                    {
                        erec_UgyUgyiratok.Partner_Id_Ugyindito = _partnerNevekDict[row.UgyfelUgyindito].ToString();
                        erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito = true;
                    }
                    else
                    {
                        // Hibát nem kell dobni, szövegesen úgyis bejegyezzük a partner nevét...
                    }
                }

                erec_UgyUgyiratok.NevSTR_Ugyindito = row.UgyfelUgyindito;
                erec_UgyUgyiratok.Updated.NevSTR_Ugyindito = true;

                // cím szövegesen
                erec_UgyUgyiratok.CimSTR_Ugyindito = row.UgyinditoCime;
                erec_UgyUgyiratok.Updated.CimSTR_Ugyindito = true;

                // cím keresése
                if (erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito)
                {
                    erec_UgyUgyiratok.Cim_Id_Ugyindito = GetPartnerCimId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito);
                    erec_UgyUgyiratok.Updated.Cim_Id_Ugyindito = true;
                }

                // TODO: ez kell
                // régi rendszer iktatószáma:
                //if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value) && !string.IsNullOrEmpty(regiAzonositoTextBox.Text))
                //{
                //    erec_UgyUgyiratok.RegirendszerIktatoszam = regiAzonositoTextBox.Text;
                //    erec_UgyUgyiratok.Updated.RegirendszerIktatoszam = true;
                //    IktatasiParameterek.RegiAdatAzonosito = regiAzonositoTextBox.Text;
                //    IktatasiParameterek.RegiAdatId = MigraltUgyiratID_HiddenField.Value;
                //}

                // BLG_44
                erec_UgyUgyiratok.Ugy_Fajtaja = _util.GetKodtarKodByKodtarNev(kcs_UGY_FAJTAJA, row.UgyFajtaja);
                erec_UgyUgyiratok.Updated.Ugy_Fajtaja = true;

                SetUgyintezesKezdete(row, erec_UgyUgyiratok);
            }

            return erec_UgyUgyiratok;
        }

        protected virtual void SetUgyintezesKezdete(EgyszerusitettIktatasAdatokBase imp, EREC_UgyUgyiratok erec_UgyUgyiratok)
        {
            erec_UgyUgyiratok.UgyintezesKezdete = DateTime.Now.ToString();
            erec_UgyUgyiratok.Updated.UgyintezesKezdete = true;
        }
        #endregion
    }
}
