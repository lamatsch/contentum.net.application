﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Contentum.eRecord.TomegesIktatas
{
    public class TomegesIktatasStatusz
    {
        /// <summary>
        /// A státusz lekérdezésének ideje
        /// </summary>
        public string StatusGetTime { get; set; }

        public List<string> NewLogItems { get; set; }

        /// <summary>
        /// A háttérfolyamat befejeződött
        /// </summary>
        public bool BackgroundProcessEnded { get; set; }

        public string BackgroundProcessError { get; set; }

        /// <summary>
        /// Az eredmény CSV fájl neve
        /// </summary>
        public string ResultTempFileName { get; set; }
        public string ResultTempDirName { get; set; }
    }
}
