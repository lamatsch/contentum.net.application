﻿using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for UtilityTomegesIktatas
/// </summary>
namespace Contentum.eRecord.TomegesIktatas
{
    public class UtilityTomegesIktatas
    {
        private readonly Page _page;
        private readonly eErrorPanel _errorPanel;

        public UtilityTomegesIktatas(Page page, eErrorPanel errorPanel)
        {
            _page = page;
            _errorPanel = errorPanel;
        }

        public void DownloadExcel(string fileName, byte[] content)
        {
            _page.Response.Clear();
            _page.Response.ClearContent();
            _page.Response.ClearHeaders();

            // Content-Type és fájl nevének beállítása
            _page.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            _page.Response.CacheControl = "public";
            _page.Response.HeaderEncoding = System.Text.Encoding.UTF8;
            _page.Response.Charset = "utf-8";
            _page.Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

            _page.Response.BinaryWrite(content);
            _page.Response.End();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Az eredmény CSV fájl letöltése a megadott temp helyről
        /// </summary>
        /// <param name="tempDirName"></param>
        /// <param name="tempFileName"></param>
        public void DownloadResultFile(string tempDirName, string tempFileName)
        {
            try
            {
                var content = Contentum.eRecord.Utility.FileFunctions.Upload.GetFileContentFromTempDirectory(tempDirName, tempFileName);
                DownloadExcel(tempFileName, content);
            }
            catch (Exception exc)
            {
                DisplayError(exc.Message);
            }
        }

        public void DisplayError(string message)
        {
            ResultError.DisplayErrorOnErrorPanel(_errorPanel, Resources.Error.DefaultErrorHeader, message);
        }

        /// <summary>
        /// Exception dobása: mező kitöltése kötelező
        /// </summary>
        /// <param name="fieldName"></param>
        public void ThrowRequiredFieldException(string fieldName)
        {
            throw new ResultException(String.Format("Az alábbi mező kitöltése kötelező: {0}", fieldName));
        }

        /// <summary>
        /// Megadott kódcsoporton belüli kódtár elem kódjának lekérése a kódtár neve alapján.
        /// </summary>
        /// <param name="kodcsoportKod"></param>
        /// <param name="kodtarNev"></param>
        /// <returns></returns>
        public string GetKodtarKodByKodtarNev(string kodcsoportKod, string kodtarNev)
        {
            if (String.IsNullOrEmpty(kodtarNev)) { return String.Empty; }

            var kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoportKod, UI.SetExecParamDefault(_page.Session), _page.Cache, null);

            if (kodtarList != null)
            {
                var kt = kodtarList.FirstOrDefault(e => e.Nev == kodtarNev);
                if (kt != null)
                {
                    return kt.Kod;
                }
            }

            // Hiba: Nincs ilyen nevű kódtárelem
            throw new ResultException(String.Format("Nincs ilyen nevű kódtárelem: '{0}' (Kódcsoport: '{1}')", kodtarNev, kodcsoportKod));
        }

        // BLG_2958
        // LZS - A tömeges iktatás folyamatának naplózási adatait menti ki egy text fájlba.
        public void DownloadLog(string text)
        {
            try
            {
                _page.Response.Clear();
                _page.Response.ClearContent();
                _page.Response.ClearHeaders();

                // Content-Type és fájl nevének beállítása
                _page.Response.AppendHeader("Content-Length", text.Length.ToString());
                _page.Response.AppendHeader("Content-Disposition", "attachment;filename=\"log.txt\"");
                _page.Response.ContentType = "text/plain";
                _page.Response.CacheControl = "public";
                _page.Response.HeaderEncoding = System.Text.Encoding.UTF8;
                _page.Response.Charset = "utf-8";
                _page.Response.Write(text);
                _page.Response.End();
            }
            catch (Exception exc)
            {
                DisplayError(exc.Message);
            }
        }

        public static TomegesIktatasStatusz GetTomegesIktatasStatusz(string processId, string lastStatusGetTime)
        {
            try
            {
                var tomegesIktatasBgItem = TomegesIktatasBackgroundItemBase.GetTomegesIktatasBgItemByProcessId(new Guid(processId));

                if (tomegesIktatasBgItem == null)
                {
                    throw new ApplicationException("Tömeges iktatás státuszának lekérése sikertelen!");
                }

                const string dtFormat = "yyyy.MM.dd HH.mm.ss.fff";
                DateTime currentStatusGetTime = DateTime.Now;
                DateTime? lastStatusGetTimeDt = null;
                DateTime dtSeged;
                if (DateTime.TryParseExact(lastStatusGetTime, dtFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AllowWhiteSpaces
                        , out dtSeged))
                {
                    lastStatusGetTimeDt = dtSeged;
                }

                var statusz = new TomegesIktatasStatusz()
                {
                    BackgroundProcessEnded = tomegesIktatasBgItem.BackgroundProcessEnded,
                    BackgroundProcessError = tomegesIktatasBgItem.BackgroundProcessError,
                    ResultTempDirName = tomegesIktatasBgItem.ResultTempDirName,
                    ResultTempFileName = tomegesIktatasBgItem.ResultTempFileName,
                    StatusGetTime = currentStatusGetTime.ToString(dtFormat),
                    NewLogItems = tomegesIktatasBgItem.Logs.Where(
                                        l => (lastStatusGetTimeDt == null || l.LogTime > lastStatusGetTimeDt.Value) && l.LogTime <= currentStatusGetTime)
                                        .Select(l => l.LogMsg)
                                        .ToList()
                };

                return statusz;
            }
            catch (Exception exc)
            {
                Logger.Error(exc.ToString());

                return new TomegesIktatasStatusz()
                {
                    BackgroundProcessError = exc.Message,
                    BackgroundProcessEnded = true
                };
            }
        }
    }
}