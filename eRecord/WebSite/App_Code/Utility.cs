using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.BaseUtility;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using DocumentFormat.OpenXml.Packaging;
using NPOI.SS.Formula.Functions;
using OfficeOpenXml.FormulaParsing.Excel.Operators;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Contentum.eRecord.Utility
{
    public class FileFunctions
    {
        public static byte[] GetFile(String FilePath)
        {
            FileStream fs = null;
            byte[] file_buffer = null;
            try
            {
                fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
            }
            catch (FileNotFoundException e)
            {
                throw e;
            }

            try
            {
                file_buffer = new byte[fs.Length];

                fs.Read(file_buffer, 0, (int)fs.Length);

                fs.Flush();
                fs.Close();
            }
            catch (FileLoadException e)
            {
                throw e;
            }

            return file_buffer;
        }

        public static Boolean DeleteFile(String FileName)
        {
            if (!File.Exists(FileName))
                return false;
            try
            {
                File.Delete(FileName);
                return true;
            }
            catch (IOException e)
            {
                throw e;
            }
        }

        #region Tartalom alap� hash k�pz�shez fv.-ek

        enum MsFileType
        {
            docx
            , xlsx
            , pptx
            , unknown
        }

        /// <summary>
        /// Kell-e ellen�rz�s a file tartalom hash-re (kiterjeszt�s alapj�n, .docx,.xlxs,.pptx f�jlokra)
        /// </summary>        
        public static bool NeedTartalomHashCheck(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return false;
            }

            if (fileName.ToLower().EndsWith(".docx")
                || fileName.ToLower().EndsWith(".xlsx")
                || fileName.ToLower().EndsWith(".pptx"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetMSFileContentSHA1(byte[] fileContent, string fileName)
        {
            string tartalomSha1 = String.Empty;

            // file t�pus eld�nt�se:
            MsFileType fileType = MsFileType.unknown;
            if (fileName.ToLower().EndsWith(".docx"))
            {
                fileType = MsFileType.docx;
            }
            else if (fileName.ToLower().EndsWith(".xlsx"))
            {
                fileType = MsFileType.xlsx;
            }
            else if (fileName.ToLower().EndsWith(".pptx"))
            {
                fileType = MsFileType.pptx;
            }
            else
            {
                return String.Empty;
            }

            try
            {
                MemoryStream stream_fileContent = new MemoryStream(fileContent);

                switch (fileType)
                {
                    case MsFileType.docx:
                        {
                            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(stream_fileContent, true))
                            {
                                MainDocumentPart mainPart = wordDoc.MainDocumentPart;

                                using (StreamReader streamReader = new StreamReader(mainPart.GetStream()))
                                {
                                    tartalomSha1 = Contentum.eUtility.FileFunctions.CalculateSHA1WithUTF8(streamReader.ReadToEnd());
                                }
                            }
                            break;
                        }
                    case MsFileType.xlsx:
                        {
                            using (SpreadsheetDocument excelDoc = SpreadsheetDocument.Open(stream_fileContent, true))
                            {
                                WorkbookPart workbookPart = excelDoc.WorkbookPart;

                                using (StreamReader streamReader = new StreamReader(workbookPart.GetStream()))
                                {
                                    StringBuilder sb = new StringBuilder();

                                    IEnumerator<IdPartPair> enumerator_workSheetPart = workbookPart.Parts.GetEnumerator();
                                    while (enumerator_workSheetPart.MoveNext())
                                    {
                                        // A CustomXmlPart-ba tesz a Sharepoint mindenf�l�t, ez�rt azt a r�szt kihagyjuk a hashk�pz�sb�l:
                                        if (enumerator_workSheetPart.Current.OpenXmlPart is CustomXmlPart)
                                        {
                                            continue;
                                        }

                                        string tartalom = new StreamReader(enumerator_workSheetPart.Current.OpenXmlPart.GetStream()).ReadToEnd();
                                        sb.Append(tartalom);
                                    }

                                    tartalomSha1 = Contentum.eUtility.FileFunctions.CalculateSHA1WithUTF8(sb.ToString());
                                }
                            }
                            break;
                        }
                    case MsFileType.pptx:
                        {
                            using (PresentationDocument pptDoc = PresentationDocument.Open(stream_fileContent, true))
                            {
                                PresentationPart presentationPart = pptDoc.PresentationPart;

                                StringBuilder sb = new StringBuilder();

                                IEnumerator<IdPartPair> enumerator_parts = pptDoc.PresentationPart.Parts.GetEnumerator();
                                while (enumerator_parts.MoveNext())
                                {
                                    // A CustomXmlPart-ba tesz a Sharepoint mindenf�l�t, ez�rt azt a r�szt kihagyjuk a hashk�pz�sb�l:
                                    if (enumerator_parts.Current.OpenXmlPart is CustomXmlPart)
                                    {
                                        continue;
                                    }

                                    string tartalom = new StreamReader(enumerator_parts.Current.OpenXmlPart.GetStream()).ReadToEnd();
                                    sb.Append(tartalom);
                                }

                                tartalomSha1 = Contentum.eUtility.FileFunctions.CalculateSHA1WithUTF8(sb.ToString());
                            }
                            break;
                        }
                }
            }
            catch (Exception)
            {
                Logger.Error("Hiba a f�jl feldolgoz�sakor");
                return String.Empty;
            }

            return tartalomSha1;
        }

        #endregion

        public static class Upload
        {
            public const string tempDirectory = "c:\\temp\\ContentumNet";

            /// <summary>
            /// K�nyvt�r gener�l�sa a Temp k�nyvt�ron bel�l
            /// </summary>
            /// <returns>A gener�lt k�nyvt�r neve</returns>            
            private static string CreateRandomDirectoryInTempDir()
            {
                DirectoryInfo diRandomDirectory;
                string randomDirectoryName;
                do
                {
                    randomDirectoryName = Path.GetFileNameWithoutExtension(Path.GetRandomFileName());
                    string randomDirectoryPath = Path.Combine(tempDirectory, randomDirectoryName);
                    diRandomDirectory = new DirectoryInfo(randomDirectoryPath);
                }
                while (diRandomDirectory.Exists);
                diRandomDirectory.Create();

                return randomDirectoryName;
            }

            public static void SaveFile(string filePath, ref byte[] buffer)
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Create))
                {
                    fs.Write(buffer, 0, buffer.Length);
                }
            }

            /// <summary>
            /// A fix Temp k�nyvt�ron bel�l egy k�nyvt�r gener�l�sa, �s a megadott f�jl elment�se ide
            /// </summary>
            /// <param name="buffer">F�jltartalom</param>
            /// <param name="fileName">F�jl neve</param>
            /// <param name="randomDirectoryName">out param�ter: a gener�lt k�nyvt�r neve</param>
            /// <returns></returns>
            public static bool SaveFileToTempDirectory(byte[] buffer, string fileName, out string randomDirectoryName)
            {
                // param�terek ellen�rz�se:
                if (buffer == null || String.IsNullOrEmpty(fileName))
                {
                    // hib�s param�terek
                    randomDirectoryName = String.Empty;
                    return false;
                }

                // K�nyvt�r gener�l�sa:
                randomDirectoryName = CreateRandomDirectoryInTempDir();
                if (String.IsNullOrEmpty(randomDirectoryName))
                {
                    // hiba, sikertelen k�nyvt�r gener�l�s
                    return false;
                }

                // File elment�se a gener�lt k�nyvt�rba:

                string randomDirPath = Path.Combine(tempDirectory, randomDirectoryName);
                string filePath = Path.Combine(randomDirPath, fileName);

                SaveFile(filePath, ref buffer);
                return true;
            }

            /// <summary>
            /// A Temp k�nyvt�ron bel�l megadott k�nyvt�rb�l kiolvassa a megadott f�jlt
            /// </summary>
            /// <param name="randomDirectoryName"></param>
            /// <param name="fileName"></param>
            /// <returns></returns>
            public static byte[] GetFileContentFromTempDirectory(string randomDirectoryName, string fileName)
            {
                string dirPath = Path.Combine(FileFunctions.Upload.tempDirectory, randomDirectoryName);
                string filePath = Path.Combine(dirPath, fileName);

                return FileFunctions.GetFile(filePath);
            }

            public static bool TryToDeleteFromTempTable(string randomDirectoryName, string fileName)
            {
                if (string.IsNullOrEmpty(randomDirectoryName) || string.IsNullOrEmpty(fileName))
                {
                    Logger.Error("Temp file t�rl�s�hez nincsenek megadva a param�terek!");
                    return false;
                }

                Logger.Info(String.Format("Felt�lt�tt ideiglenes file �s k�nyvt�r t�rl�se... TempDirName: {0} FileName: {1} "
                    , randomDirectoryName, fileName));

                string randomDirPath = Path.Combine(FileFunctions.Upload.tempDirectory, randomDirectoryName);
                string filePath = Path.Combine(randomDirPath, fileName);

                try
                {
                    // ellen�rz�s, megvan-e a f�jl:
                    if (!File.Exists(filePath))
                    {
                        Logger.Error("Nem l�tezik a file a megadott �tvonalon: " + filePath);
                        return false;
                    }

                    // f�jl t�rl�se:
                    File.Delete(filePath);

                    Logger.Info(String.Format("File t�r�lve. ({0})", filePath));

                    // k�nyvt�r t�rl�se:
                    Directory.Delete(randomDirPath, false);

                    Logger.Info(String.Format("K�nyvt�r t�r�lve. ({0})", randomDirPath));
                    return true;
                }
                catch (Exception e)
                {
                    Logger.Error("Hiba az ideiglenes k�nyvt�r/file t�rl�se sor�n", e);
                    return false;
                }
            }
        }


        public static string GetFileIconNameByFormatum(string formatum)
        {
            if (string.IsNullOrEmpty(formatum))
            {
                return "default.gif";
            }

            switch (formatum.ToLower())
            {
                case "xls":
                case "xlsx":
                    return "xls.gif";
                case "gif":
                case "bmp":
                    return "gif.gif";
                case "tiff":
                case "png":
                case "jpg":
                    return "jpg.gif";
                case "pdf":
                    return "pdf.gif";
                case "txt":
                    return "txt.gif";
                case "doc":
                case "docx":
                case "docm":
                case "dot":
                case "dotx":
                case "dotm":
                    return "doc.gif";
                case "ppt":
                case "pptx":
                    return "ppt.gif";
                case "zip":
                    return "zip.gif";
                default:
                    return "default.gif";
            }
        }

    }

    public class IrattariKikero : Contentum.eRecord.BaseUtility.IrattariKikero
    {
        public static void SetKolcsonzesJovahagyas(String IrattariKikeroId, Statusz statusz, ExecParam execparam, ImageButton image)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Felhasznalo" + CommandName.New)) // CommandName.Lezaras
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(IrattariKikeroId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (Jovahagyhato(statusz, execparam))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickConfirm("UIConfirmHeader_KolcsonzesJovahagyas");
            }
            else
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNemJovahagyhatoKolcsonzesAlert();
            }
        }

        public static void SetKolcsonzesVisszautasitas(String IrattariKikeroId, Statusz statusz, ExecParam execparam, ImageButton image)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Felhasznalo" + CommandName.New)) // CommandName.Lezaras
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(IrattariKikeroId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (Visszautasithato(statusz, execparam))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickConfirm("UIConfirmHeader_KolcsonzesVisszautasitas");
            }
            else
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNemVisszautasithatoKolcsonzesAlert();
            }

        }

        public static void SetKolcsonzesSztorno(String IrattariKikeroId, Statusz statusz, ExecParam execparam, ImageButton image)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Felhasznalo" + CommandName.New)) // CommandName.Lezaras
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(IrattariKikeroId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            ErrorDetails errorDetail = null;

            if (Sztornozhato(statusz, execparam, out errorDetail))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickConfirm("UIConfirmHeader_KolcsonzesSztorno");
            }
            else
            {
                image.OnClientClick = "alert('" + Resources.Error.UINemSztornozhatoKolcsonzes + "'); return false";
            }
        }

        public static void SetKolcsonzesKiadas(String IrattariKikeroId, Statusz statusz, ExecParam execparam, ImageButton image)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Felhasznalo" + CommandName.New)) // CommandName.Lezaras
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(IrattariKikeroId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (Kiadhato(statusz, execparam))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickConfirm("UIConfirmHeader_KolcsonzesKiadas");
            }
            else
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNemKiadhatoKolcsonzesAlert();
            }

        }

        public static void SetKolcsonzesVissza(String IrattariKikeroId, Statusz statusz, ExecParam execparam, ImageButton image)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Felhasznalo" + CommandName.New)) // CommandName.Lezaras
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(IrattariKikeroId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (Visszaadhato(statusz, execparam))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickConfirm("UIConfirmHeader_KolcsonzesVissza");
            }
            else
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNemVisszavehetoKolcsonzesAlert();
            }

        }

        public static Statusz GetAllapotFromDataRowView(DataRowView drv)
        {
            Statusz statusz = new Statusz(
                        drv["Allapot"].ToString(),
                        drv["FelhasznalasiCel"].ToString(),
                        drv["FelhasznaloCsoport_Id_Kikero"].ToString(),
                        drv["UgyUgyirat_Id"].ToString(),
                        drv["Tertiveveny_Id"].ToString(),
                        drv["FelhasznaloCsoport_Id_Jovahagy"].ToString(),
                        drv["KikerVege"].ToString());

            return statusz;
        }

        public static void GridView_RowDataBound_CheckKiadasAtmenetiIrattarbol(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                IrattariKikero.Statusz statusz = IrattariKikero.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool kiadhato = IrattariKikero.KiadhatoAtmenetiIrattarbol(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(kiadhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.UgyUgyirat_Id, page);
            }
        }

        public static void GridView_RowDataBound_CheckKikeresAtmenetiIrattarbolJovahagyas(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                IrattariKikero.Statusz statusz = IrattariKikero.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool jovahagyhato = IrattariKikero.JovahagyhatoWithJovahagyoCheck(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(jovahagyhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.UgyUgyirat_Id, page);
            }
        }

        public static void GridView_RowDataBound_CheckKiadasSkontroIrattarbol(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                IrattariKikero.Statusz statusz = IrattariKikero.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool kiadhato = IrattariKikero.KiadhatoSkontroIrattarbol(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(kiadhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.UgyUgyirat_Id, page);
            }
        }

    }

    public class eDocumentService : Contentum.eRecord.BaseUtility.eDocumentService
    {
    }
    public class eTemplateManagerService : Contentum.eRecord.BaseUtility.eTemplateManagerService
    {
    }
    public class Rendszerparameterek : Contentum.eRecord.BaseUtility.Rendszerparameterek
    {
        /// <summary>
        /// Bizonyos lovlistekn�l egyedi (pl. �gyiratokn�l 100), egy�bk�nt LOVLIST_MAX_ROW rendszerparam�terb�l
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static int Get_LOVLIST_MAX_ROW(Page page)
        {
            switch (page.GetType().Name)
            {
                case "ugyugyiratoklovlist_aspx":
                    return 100;
                case "dokumentumoklovlist_aspx":
                    return 100;
                default:
                    return Rendszerparameterek.GetInt(page, Rendszerparameterek.LOVLIST_MAX_ROW);
            }
        }

        // BUG_6459
        public const string LISTA_RENDEZETTSEG_IKTATOSZAM = "LISTA_RENDEZETTSEG_IKTATOSZAM";

        // BUG_8001
        public const string IKTATAS_BELSO_UGYINTEZO_IKTATO = "IKTATAS_BELSO_UGYINTEZO_IKTATO";

        // BUG_8826
        public const string UGYINT_IDO_MOD = "UGYINT_IDO_MOD";
    }

    public class XmlFunction : Contentum.eRecord.BaseUtility.XmlFunction
    {
    }

    public class Search : Contentum.eRecord.BaseUtility.Search
    {
        #region Keres�si objektumok l�trehoz�sa default sz�r�si felt�tellel

        /// <summary>
        /// A megadott t�pus� keres�si objektumot adja vissza a default keres�si felt�tellel
        /// </summary>
        /// <param name="searchObjectName"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static object CreateSearchObjectWithDefaultFilter(Type searchObjectType, Page page)
        {
            if (searchObjectType == null) { return null; }

            switch (searchObjectType.Name)
            {
                case (Constants.SearchObjectTypes.EREC_KuldKuldemenyekSearch):
                    {
                        return CreateSearchObjWithDefFilter_Kuldemeny(page);
                    }
                case (Constants.SearchObjectTypes.EREC_UgyUgyiratokSearch):
                    {
                        return CreateSearchObjWithDefFilter_Ugyirat(page);
                    }
                case (Constants.SearchObjectTypes.EREC_IraIratokSearch):
                    {
                        return CreateSearchObjWithDefFilter_Irat(page);
                    }
                case (Constants.SearchObjectTypes.EREC_PldIratPeldanyokSearch):
                    {
                        return CreateSearchObjWithDefFilter_IratPeldany(page);
                    }
                case (Constants.SearchObjectTypes.EREC_IraKezbesitesiFejekSearch):
                    {
                        return CreateSearchObjWithDefFilter_KezbesitesiFejek(page);
                    }
                case (Constants.SearchObjectTypes.EREC_IraIktatoKonyvekSearch):
                    {
                        return CreateSearchObjWithDefFilter_IktatoKonyvek(page);
                    }
                default:
                    // Type t�pus alapj�n �j objektum legy�rt�sa:
                    object newSearchObj = Activator.CreateInstance(searchObjectType);

                    return newSearchObj;
            }
        }


        private static EREC_IraKezbesitesiFejekSearch CreateSearchObjWithDefFilter_KezbesitesiFejek(Page page)
        {
            EREC_IraKezbesitesiFejekSearch searchObject = new EREC_IraKezbesitesiFejekSearch();

            // default �rt�kek be�ll�t�sa:

            // Alapb�l a felhaszn�l� k�zbes�t�si csomagjait hozzuk

            string felhasznaloId = FelhasznaloProfil.FelhasznaloId(page);
            string felhasznaloNev = CsoportNevek_Cache.GetCsoportNevFromCache(felhasznaloId, page);

            searchObject.Manual_Letrehozo_id.Filter(felhasznaloId);

            searchObject.ReadableWhere = "Felhaszn�l�: " + felhasznaloNev;

            return searchObject;
        }

        private static EREC_KuldKuldemenyekSearch CreateSearchObjWithDefFilter_Kuldemeny(Page page)
        {
            EREC_KuldKuldemenyekSearch searchObject = new EREC_KuldKuldemenyekSearch(true);

            bool isDefaultSearchEnabled = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED);
            if (isDefaultSearchEnabled)
            {
                // sz�r�s a legut�bb kezeltekre (utols� bel�p�s ideje - 3 nap)
                SetSearchObjectField_LegutobbKezeltek(searchObject.Manual_ModositasIdo, page);

                searchObject.ReadableWhere = "Legut�bb kezelt k�ldem�nyek ";
            }
            return searchObject;
        }

        private static EREC_UgyUgyiratokSearch CreateSearchObjWithDefFilter_Ugyirat(Page page)
        {
            EREC_UgyUgyiratokSearch searchObject = GetDefaultSearchObject_EREC_UgyUgyiratokSearch(); //new EREC_UgyUgyiratokSearch(true);

            bool isDefaultSearchEnabled = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED);
            if (isDefaultSearchEnabled)
            {
                // sz�r�s a legut�bb kezeltekre (utols� bel�p�s ideje - 3 nap)
                SetSearchObjectField_LegutobbKezeltek(searchObject.Manual_ModositasIdo, page);

                searchObject.ReadableWhere = "Legut�bb kezelt �gyiratok ";
            }
            return searchObject;
        }

        private static EREC_IraIratokSearch CreateSearchObjWithDefFilter_Irat(Page page)
        {
            EREC_IraIratokSearch searchObject = new EREC_IraIratokSearch(true);

            bool isDefaultSearchEnabled = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED);
            if (isDefaultSearchEnabled)
            {
                // sz�r�s a legut�bb kezeltekre (utols� bel�p�s ideje - 3 nap)
                SetSearchObjectField_LegutobbKezeltek(searchObject.Manual_ModositasIdo, page);

                searchObject.ReadableWhere = "Legut�bb kezelt iratok ";
            }
            // BUG_6459
            Search.SetActiveFilter(searchObject);
            SetFilter_NemLezartUgyirat(searchObject);

            return searchObject;
        }

        // BUG_8510
        public static void SetFilter_NemLezartUgyirat(BaseSearchObject searchObject)
        {
            if (searchObject != null)
            {
                var sql = " (EREC_UgyUgyiratok.Allapot  <>  '" + KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart + "') ";
                var iratokSearch = searchObject as EREC_IraIratokSearch;
                var pldSearch = searchObject as EREC_PldIratPeldanyokSearch;
                if (iratokSearch != null)
                {
                    WhereByManualIf(iratokSearch.CsakAktivIrat, ref iratokSearch.WhereByManual, sql);
                }
                else if (pldSearch != null)
                {
                    WhereByManualIf(pldSearch.CsakAktivIrat, ref pldSearch.WhereByManual, sql);
                }
            }
        }
        public static void WhereByManualIf(bool condition, ref string whereByManual, string sql)
        {
            if (!String.IsNullOrEmpty(whereByManual) && !sql.Trim().StartsWith("and ", StringComparison.InvariantCultureIgnoreCase)) { sql = " AND " + sql; }
            if (condition)
            {
                if (!whereByManual.Contains(sql))
                {
                    whereByManual += sql;
                }
            }
            else if (whereByManual.Contains(sql))
            {
                whereByManual = whereByManual.Replace(sql, "");
            }
        }

        private static EREC_PldIratPeldanyokSearch CreateSearchObjWithDefFilter_IratPeldany(Page page)
        {
            EREC_PldIratPeldanyokSearch searchObject = new EREC_PldIratPeldanyokSearch(true);

            bool isDefaultSearchEnabled = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.SEARCH_SETUP_LEGUTOBB_KEZELT_ENABLED);
            if (isDefaultSearchEnabled)
            {
                // sz�r�s a legut�bb kezeltekre (utols� bel�p�s ideje - 3 nap)
                SetSearchObjectField_LegutobbKezeltek(searchObject.Manual_ModositasIdo, page);

                searchObject.ReadableWhere = "Legut�bb kezelt iratp�ld�nyok ";
            }
            // BUG_6459
            Search.SetActiveFilter(searchObject);
            SetFilter_NemLezartUgyirat(searchObject);

            return searchObject;
        }

        private static EREC_IraIktatoKonyvekSearch CreateSearchObjWithDefFilter_IktatoKonyvek(Page page)
        {
            EREC_IraIktatoKonyvekSearch searchObject = new EREC_IraIktatoKonyvekSearch();

            // sz�r�s az aktu�lis �vre
            var filterYear = DateTime.Now.Year.ToString();

            searchObject.Ev.Between(filterYear, filterYear);
            searchObject.ReadableWhere = "Aktu�lis �v (" + searchObject.Ev.Value + ") ";

            return searchObject;
        }

        private static void SetSearchObjectField_LegutobbKezeltek(Field searchField_modositasIdo, Page page)
        {
            if (searchField_modositasIdo != null)
            {
                string filterTime = "";
                try
                {
                    DateTime lastLoginTime = DateTime.Parse(FelhasznaloProfil.GetLastLoginTime(page));
                    filterTime = lastLoginTime.AddDays(-3).Date.ToShortDateString();
                }
                catch
                {
                    filterTime = DateTime.Today.AddDays(-3).ToShortDateString();
                }

                searchField_modositasIdo.Greater(filterTime);

            }
        }

        public static EREC_IraIratokSearch CreateSearchObjWithSpecFilter_Irat_KozgyulesiEloterjesztes(Page page)
        {
            EREC_IraIratokSearch searchObject = CreateSearchObjWithDefFilter_Irat(page);

            searchObject = SetSearchObjectSpecFilter_Irat_KozgyulesiEloterjesztes(searchObject);

            return searchObject;
        }

        public static EREC_IraIratokSearch SetSearchObjectSpecFilter_Irat_KozgyulesiEloterjesztes(EREC_IraIratokSearch searchObject)
        {
            if (searchObject != null)
            {
                searchObject.Irattipus.Filter(KodTarak.IRATTIPUS.KozgyulesiEloterjesztes);
            }
            return searchObject;
        }

        public static EREC_IraIratokSearch CreateSearchObjWithSpecFilter_Irat_BizottsagiEloterjesztes(Page page)
        {
            EREC_IraIratokSearch searchObject = CreateSearchObjWithDefFilter_Irat(page);

            searchObject = SetSearchObjectSpecFilter_Irat_BizottsagiEloterjesztes(searchObject);

            return searchObject;
        }

        public static EREC_IraIratokSearch SetSearchObjectSpecFilter_Irat_BizottsagiEloterjesztes(EREC_IraIratokSearch searchObject)
        {
            if (searchObject != null)
            {
                searchObject.Irattipus.Filter(KodTarak.IRATTIPUS.BizottsagiEloterjesztes);
            }
            return searchObject;
        }

        #endregion

        #region alap�rtelmezett ("�res") keres�si objektumok l�trehoz�sa

        public static object GetDefaultSearchObject(Type searchObjectType)
        {
            if (searchObjectType == null) { return null; }

            switch (searchObjectType.Name)
            {
                case Constants.SearchObjectTypes.EREC_KuldKuldemenyekSearch:
                    return GetDefaultSearchObject_EREC_KuldKuldemenyekSearch();
                case Constants.SearchObjectTypes.EREC_UgyUgyiratokSearch:
                    return GetDefaultSearchObject_EREC_UgyUgyiratokSearch();
                case Constants.SearchObjectTypes.EREC_IraIratokSearch:
                    return GetDefaultSearchObject_EREC_IraIratokSearch();
                case Constants.SearchObjectTypes.EREC_PldIratPeldanyokSearch:
                    return GetDefaultSearchObject_EREC_PldIratPeldanyokSearch();
                default:
                    // Type t�pus alapj�n �j objektum legy�rt�sa:
                    object newSearchObj = Activator.CreateInstance(searchObjectType);
                    return newSearchObj;
            }

        }

        private static EREC_KuldKuldemenyekSearch GetDefaultSearchObject_EREC_KuldKuldemenyekSearch()
        {
            return new EREC_KuldKuldemenyekSearch(true);
        }

        private static EREC_UgyUgyiratokSearch GetDefaultSearchObject_EREC_UgyUgyiratokSearch()
        {
            EREC_UgyUgyiratokSearch searchObject = new EREC_UgyUgyiratokSearch(true);
            // sztorn�zottak alapb�l ne j�jjenek
            searchObject.Manual_Sztornozottak.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);
            Search.SetActiveFilter(searchObject);
            return searchObject;
        }

        private static EREC_IraIratokSearch GetDefaultSearchObject_EREC_IraIratokSearch()
        {
            EREC_IraIratokSearch searchObject = new EREC_IraIratokSearch(true);
            Search.SetActiveFilter(searchObject);
            return searchObject;
        }

        private static EREC_PldIratPeldanyokSearch GetDefaultSearchObject_EREC_PldIratPeldanyokSearch()
        {
            EREC_PldIratPeldanyokSearch searchObject = new EREC_PldIratPeldanyokSearch(true);
            Search.SetActiveFilter(searchObject);
            return searchObject;
        }
        #endregion alap�rtelmezett ("�res") keres�si objektumok l�trehoz�sa

        #region keres�si objektumok �sszehasonl�t�sa az alap�rtelmezett ("�res") keres�si objektumokkal
        public static bool IsDefaultSearchObject(Type searchObjectType, object searchObject)
        {
            return IsDefaultSearchObject(searchObjectType, searchObject, null);
        }
        public static bool IsDefaultSearchObject(Type searchObjectType, object searchObject, object defaultSearchObject)
        {
            if (searchObjectType == null) { return false; }
            if (searchObject.GetType() != searchObjectType) { return false; }
            switch (searchObjectType.Name)
            {
                case Constants.SearchObjectTypes.EREC_KuldKuldemenyekSearch:
                    return IsDefaultSearchObject_EREC_KuldKuldemenyekSearch(searchObject as EREC_KuldKuldemenyekSearch);
                case Constants.SearchObjectTypes.EREC_UgyUgyiratokSearch:
                    return IsDefaultSearchObject_EREC_UgyUgyiratokSearch(searchObject as EREC_UgyUgyiratokSearch, defaultSearchObject as EREC_UgyUgyiratokSearch);
                case Constants.SearchObjectTypes.EREC_IraIratokSearch:
                    return IsDefaultSearchObject_EREC_IraIratokSearch(searchObject as EREC_IraIratokSearch, defaultSearchObject as EREC_IraIratokSearch);
                case Constants.SearchObjectTypes.EREC_PldIratPeldanyokSearch:
                    return IsDefaultSearchObject_EREC_PldIratPeldanyokSearch(searchObject as EREC_PldIratPeldanyokSearch, defaultSearchObject as EREC_PldIratPeldanyokSearch);
                default:
                    // Type t�pus alapj�n �j objektum legy�rt�sa:
                    object newSearchObj = Activator.CreateInstance(searchObjectType);
                    return IsIdentical(searchObject, newSearchObj);
            }

        }

        private static bool IsDefaultSearchObject_EREC_KuldKuldemenyekSearch(EREC_KuldKuldemenyekSearch searchObject)
        {
            EREC_KuldKuldemenyekSearch defaultSearchObject = GetDefaultSearchObject_EREC_KuldKuldemenyekSearch();
            if (Search.IsIdentical(defaultSearchObject, searchObject, defaultSearchObject.WhereByManual, searchObject.WhereByManual)
                && Search.IsIdentical(searchObject.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch, defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_SzamlakSearch, defaultSearchObject.Extended_EREC_SzamlakSearch)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool IsDefaultSearchObject_EREC_UgyUgyiratokSearch(EREC_UgyUgyiratokSearch searchObject, EREC_UgyUgyiratokSearch defaultSearchObject)
        {
            if (defaultSearchObject == null) defaultSearchObject = GetDefaultSearchObject_EREC_UgyUgyiratokSearch();
            if (Search.IsIdentical(searchObject, defaultSearchObject, searchObject.WhereByManual, defaultSearchObject.WhereByManual)
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                && ((searchObject.Extended_EREC_KuldKuldemenyekSearch == null && defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch == null) || Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch))
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_IraIratokSearch, defaultSearchObject.Extended_EREC_IraIratokSearch, searchObject.Extended_EREC_IraIratokSearch.WhereByManual, defaultSearchObject.Extended_EREC_IraIratokSearch.WhereByManual)
                && Search.IsIdentical(searchObject.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch, searchObject.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch.WhereByManual)
                 && Search.IsIdentical(searchObject.Extended_EREC_IratMetaDefinicioSearch, defaultSearchObject.Extended_EREC_IratMetaDefinicioSearch)
                //&& ((searchObject.fts_targy == null)
                //|| String.IsNullOrEmpty(searchObject.fts_targy.Filter))
                && (searchObject.fts_tree_ugyirat_auto == null) // || searchObject.fts_tree_ugyirat_auto.LeafCount == 0)
                && String.IsNullOrEmpty(searchObject.ObjektumTargyszavai_ObjIdFilter)
                && ((searchObject.fts_targyszavak == null)
                || String.IsNullOrEmpty(searchObject.fts_targyszavak.Filter)) // id�ig nem szabad eljutnia
                && (searchObject.fts_altalanos == null)
                && searchObject.Csoporttagokkal == defaultSearchObject.Csoporttagokkal
                && searchObject.CsakAktivIrat == defaultSearchObject.CsakAktivIrat
                && searchObject.NaponBelulNincsUjAlszam == defaultSearchObject.NaponBelulNincsUjAlszam
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsDefaultSearchObject_EREC_IraIratokSearch(EREC_IraIratokSearch searchObject, EREC_IraIratokSearch defaultSearchObject)
        {
            if (defaultSearchObject == null)
            {
                defaultSearchObject = GetDefaultSearchObject_EREC_IraIratokSearch();
            }

            if (Search.IsIdentical(searchObject, defaultSearchObject, searchObject.WhereByManual, defaultSearchObject.WhereByManual)
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch, defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_IraOnkormAdatokSearch, defaultSearchObject.Extended_EREC_IraOnkormAdatokSearch)
                && (searchObject.fts_tree_ugyirat_auto == null)
                && (searchObject.fts_tree_irat_auto == null)
                //&& (searchObject.fts_targy == null
                //|| String.IsNullOrEmpty(searchObject.fts_targy.Filter))
                && String.IsNullOrEmpty(searchObject.ObjektumTargyszavai_ObjIdFilter)
                && ((searchObject.fts_targyszavak == null)
                || String.IsNullOrEmpty(searchObject.fts_targyszavak.Filter)) // id�ig nem szabad eljutnia
                && searchObject.Csoporttagokkal == defaultSearchObject.Csoporttagokkal
                && searchObject.CsakAktivIrat == defaultSearchObject.CsakAktivIrat
                && searchObject.CsatolmanySzures.Value == defaultSearchObject.CsatolmanySzures.Value
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool IsDefaultSearchObject_EREC_PldIratPeldanyokSearch(EREC_PldIratPeldanyokSearch searchObject, EREC_PldIratPeldanyokSearch defaultSearchObject)
        {
            if (defaultSearchObject == null) defaultSearchObject = GetDefaultSearchObject_EREC_PldIratPeldanyokSearch();
            if (Search.IsIdentical(searchObject, defaultSearchObject, defaultSearchObject.WhereByManual, searchObject.WhereByManual)
                && Search.IsIdentical(searchObject.Extended_EREC_IraIratokSearch, defaultSearchObject.Extended_EREC_IraIratokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_HataridosFeladatokSearch, defaultSearchObject.Extended_EREC_HataridosFeladatokSearch)
                && searchObject.Csoporttagokkal == defaultSearchObject.Csoporttagokkal
                && searchObject.CsakAktivIrat == defaultSearchObject.CsakAktivIrat)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion keres�si objektumok �sszehasonl�t�sa az alap�rtelmezett ("�res") keres�si objektumokkal

        public static HKP_DokumentumAdatokSearch GetDefaultSearchObject_HKP_DokumentumAdatokSearch()
        {
            HKP_DokumentumAdatokSearch search = new HKP_DokumentumAdatokSearch();
            //nem �rtkeztetett, iktatott
            search.KuldKuldemeny_Id.IsNull();
            return search;
        }


        //setReadableWhere: ha nem searchform bol h�vj�k akkor nek�nk kell a readable wheret be�ll�tani
        public static EREC_eBeadvanyokSearch GetDefaultSearchObject_EREC_eBeadvanyokSearch(Page page, bool setReadableWhere)
        {
            var search = new EREC_eBeadvanyokSearch();
            //EUZENET_OSSZES
            //1- nincs sz�r�s
            //0- csak az �rkeztetett ebeadv�nyok l�tsz�djanak
            bool onlyErkeztetesNelkuli = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.EUZENET_OSSZES, false) == false;
            //EUZENET_RENDSZERUZENETEK_SZURES
            //0 - Nem(Ne ker�ljenek be a list�ba a rendszer�zenetek)
            //1 - Igen(Ker�ljenek be a list�ba a rendszer�zenetek)
            //2 - �sszes(Ne vegye figyelembe a lek�rdez�s)
            var filterRendszerUzenetekValue = eUtility.Rendszerparameterek.GetInt(page, Rendszerparameterek.EUZENET_RENDSZERUZENETEK_SZURES).ToString();

            if (onlyErkeztetesNelkuli)
            {
                search.KuldKuldemeny_Id.IsNull();
            }

            switch (filterRendszerUzenetekValue)
            {
                case "0":
                    search.KR_Rendszeruzenet.IsNullOrEquals("0");
                    break;
                case "1":
                    search.KR_Rendszeruzenet.Operator = Contentum.eQuery.Query.Operators.equals;
                    search.KR_Rendszeruzenet.Value = filterRendszerUzenetekValue;
                    break;
                case "2":
                    search.KR_Rendszeruzenet.Clear();
                    break;
            }

            if (setReadableWhere)
            {
                search.ReadableWhere =
                    "St�tusz: "
                    + (onlyErkeztetesNelkuli ? " �rk. n�lk�li" : " �sszes")
                    + " |"
                    + " Rendszer�zenetek: "
                    + (filterRendszerUzenetekValue == "0" ? " Nem" : "")
                    + (filterRendszerUzenetekValue == "1" ? " Igen" : "")
                    + (filterRendszerUzenetekValue == "2" ? " �sszes" : "");
            }
            return search;
        }
        //setReadableWhere: ha nem searchform bol h�vj�k akkor nek�nk kell a readable wheret be�ll�tani
        public static EREC_IraIratokSearch GetDefaultSearchObject_EREC_IraIratokSearchForMunkaUgyiratok(Page page, bool setReadableWhere)
        {
            var search = new EREC_IraIratokSearch(true);
            string munkaUgyiratokAllapotFilter = Rendszerparameterek.Get(page, Rendszerparameterek.MUNKAUGYIRATOK_ALLAPOT_SZURES);
            if (string.IsNullOrEmpty(munkaUgyiratokAllapotFilter)) { return search; }
            search.Allapot.Filter(munkaUgyiratokAllapotFilter);
            if (!setReadableWhere)
            {
                return search;
            }
            string allapotNev = munkaUgyiratokAllapotFilter;
            switch (munkaUgyiratokAllapotFilter)
            {
                case "06":
                    allapotNev = "�tiktatott";
                    break;
                case "2":
                    allapotNev = "Befagyasztott(Munkairat)";
                    break;
                case "09":
                    allapotNev = "Elint�zett";
                    break;
                case "08":
                    allapotNev = "Felszabad�tott";
                    break;
                case "20":
                    allapotNev = "Foglalt";
                    break;
                case "1":
                    allapotNev = "Iktatand�(Munkairat)";
                    break;
                case "04":
                    allapotNev = "Iktatott";
                    break;
                case "07":
                    allapotNev = "Int�zked�s alatt";
                    break;
                case "30":
                    allapotNev = "Kiadm�nyozott";
                    break;
                case "0":
                    allapotNev = "Megnyitott(Munkairat)";
                    break;
                case "05":
                    allapotNev = "Szign�lt";
                    break;
                case "90":
                    allapotNev = "Sztorn�zott";
                    break;
            }
            search.ReadableWhere = "�llapot: " + allapotNev;
            return search;
        }

        public static EREC_IraJegyzekekSearch GetDefaultSearchObject_EREC_IraJegyzekekSearch(Page page)
        {
            EREC_IraJegyzekekSearch search = new EREC_IraJegyzekekSearch();

            bool csakSajatSzervezet = Rendszerparameterek.GetBoolean(page, Rendszerparameterek.JEGYZEK_CSAK_SAJATSZERVEZET_LATHATO, false);
            if (csakSajatSzervezet)
            {
                search.Csoport_Id_FelelosSzervezet.Filter(FelhasznaloProfil.FelhasznaloSzerverzetId(page));
            }

            return search;
        }

        public static EREC_PldIratPeldanyokSearch CreateSearchObjWithDefFilter_KimenoIratPeldany(Page page)
        {
            EREC_PldIratPeldanyokSearch search = (EREC_PldIratPeldanyokSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_PldIratPeldanyokSearch), page);
            /*POSTAZAS_IRANYA a BOPMH-ban a bels� = kimen�.*/
            search.Extended_EREC_IraIratokSearch.PostazasIranya.Filter(KodTarak.POSTAZAS_IRANYA.Belso);
            // BUG_6459
            Search.SetActiveFilter(search);
            SetFilter_NemLezartUgyirat(search);
            return search;
        }

        public static KRT_HelyettesitesekSearch CreateSearchObjWithDefFilter_Helyettesiteek()
        {

            KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();
            krt_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Helyettesites;
            krt_HelyettesitesekSearch.HelyettesitesMod.Operator = Contentum.eQuery.Query.Operators.equals;
            krt_HelyettesitesekSearch.HelyettesitesVege.Value = DateTime.Now.ToString();
            krt_HelyettesitesekSearch.HelyettesitesVege.Operator = Contentum.eQuery.Query.Operators.greater;

            return krt_HelyettesitesekSearch;
        }

        public static string whereNotEquals(string field, string value)
        {
            return String.Format(" AND {0}<>'{1}'", field, value.Replace("'", "''"));
        }

        /// <summary>
        /// A megadott Search objektum �res-e: 
        /// a be�ll�tott mez�k alapj�n gener�land� Where felt�tel �res-e
        /// </summary>
        /// <param name="searchObject"></param>
        /// <returns></returns>
        public static bool IsSearchObjectEmpty(object searchObject)
        {
            Query query = new Query();
            query.BuildFromBusinessDocument(searchObject);

            return String.IsNullOrEmpty(query.Where);
        }

        public static void SetActiveFilter(BaseSearchObject search)
        {
            if (search != null)
            {
                bool set = false;
                if (search is EREC_UgyUgyiratokSearch)
                {
                    (search as EREC_UgyUgyiratokSearch).CsakAktivIrat = true;
                    set = true;
                }
                else if (search is EREC_IraIratokSearch)
                {
                    (search as EREC_IraIratokSearch).CsakAktivIrat = true;
                    set = true;
                }
                else if (search is EREC_PldIratPeldanyokSearch)
                {
                    (search as EREC_PldIratPeldanyokSearch).CsakAktivIrat = true;
                    set = true;
                }

                if (set)
                {
                    search.ReadableWhere = "Akt�v �llapotban l�v� t�telek";
                }
            }
        }
    }

    public class eRecordService : Contentum.eRecord.BaseUtility.eRecordService
    {
    }

    public class UI : Contentum.eRecord.BaseUtility.UI
    {
        public static string DoPostBack(Control control, string eventArg)
        {
            return "__doPostBack('" + control.ClientID + "','" + eventArg + "');";
        }

        public static void SetRowCheckboxAndInfo(bool condition, GridViewRowEventArgs e, ErrorDetails errorDetail, string objectType, string objectId, Page page)
        {
            // CheckBox lek�r�se:
            CheckBox checkBox = e == null ? null : (CheckBox)e.Row.FindControl("check");

            if (!condition)
            {
                e.Row.BackColor = System.Drawing.Color.Coral;
                if (checkBox != null)
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = false;
                }

                // hiba�zenet be�ll�t�sa
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null && errorDetail != null)
                {
                    infoImageButton.Visible = true;
                    string infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail, page);
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                        + objectType + "','" + objectId + "','"
                        + errorDetail.ObjectType + "','" + errorDetail.ObjectId + "'); return false;";
                }
            }
            else if (checkBox != null)
            {
                checkBox.Checked = true;
                checkBox.Enabled = true;
            }
        }

        public static void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drw = (DataRowView)e.Row.DataItem;
                string csatolmanyCount = String.Empty;

                if (drw["CsatolmanyCount"] != null)
                {
                    csatolmanyCount = drw["CsatolmanyCount"].ToString();
                }

                if (!String.IsNullOrEmpty(csatolmanyCount))
                {
                    Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                    if (CsatolmanyImage != null)
                    {
                        int count = 0;
                        Int32.TryParse(csatolmanyCount, out count);
                        if (count > 0)
                        {
                            CsatolmanyImage.Visible = true;
                        }
                        else
                        {
                            CsatolmanyImage.Visible = false;
                        }
                    }
                }
                else
                {
                    Image CsatolmanyImage = (Image)e.Row.FindControl("CsatolmanyImage");
                    if (CsatolmanyImage != null)
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }
            }
        }

        public static void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drw = (DataRowView)e.Row.DataItem;
                string csatolsaCount = String.Empty;

                if (drw["CsatolasCount"] != null)
                {
                    csatolsaCount = drw["CsatolasCount"].ToString();
                }

                if (!String.IsNullOrEmpty(csatolsaCount))
                {
                    Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

                    if (CsatoltImage != null)
                    {
                        int count = 0;
                        Int32.TryParse(csatolsaCount, out count);
                        if (count > 0)
                        {
                            CsatoltImage.Visible = true;
                        }
                        else
                        {
                            CsatoltImage.Visible = false;
                        }
                    }

                }
                else
                {
                    Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");
                    if (CsatoltImage != null)
                    {
                        CsatoltImage.Visible = false;
                    }

                }
            }
        }

        public static void GridView_RowDataBound_SetSzerelesInfo(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drw = (DataRowView)e.Row.DataItem;
                string szereltCount = String.Empty;

                if (drw["SzereltCount"] != null)
                {
                    szereltCount = drw["SzereltCount"].ToString();
                }

                string elokeszitettSzerelesCount_str = String.Empty;
                if (drw["ElokeszitettSzerelesCount"] != null)
                {
                    elokeszitettSzerelesCount_str = drw["ElokeszitettSzerelesCount"].ToString();
                }
                /// ha van szerel�sre el�k�sz�tett �gyirat (UgyUgyiratId_Szulo ki van t�ltve erre, de nem szerelt m�g az �llapota),
                /// akkor a piros szerelt ikont rakjuk ki, am�gy a feket�t
                /// 
                int elokeszitettSzerelesCount = 0;
                if (!String.IsNullOrEmpty(elokeszitettSzerelesCount_str))
                {
                    Int32.TryParse(elokeszitettSzerelesCount_str, out elokeszitettSzerelesCount);
                }

                if (elokeszitettSzerelesCount > 0)
                {
                    Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                    if (SzereltImage != null)
                    {
                        SzereltImage.Visible = true;
                        SzereltImage.ImageUrl = "~/images/hu/ikon/szerelt_piros.gif";
                        SzereltImage.ToolTip = "El�k�sz�tett szerel�s";
                        SzereltImage.AlternateText = SzereltImage.ToolTip;
                    }
                }
                else if (!String.IsNullOrEmpty(szereltCount))
                {
                    Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                    if (SzereltImage != null)
                    {
                        int count = 0;
                        Int32.TryParse(szereltCount, out count);
                        if (count > 0)
                        {
                            SzereltImage.Visible = true;
                        }
                        else
                        {
                            SzereltImage.Visible = false;
                        }
                    }

                }
                else
                {
                    Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");
                    if (SzereltImage != null)
                    {
                        SzereltImage.Visible = false;
                    }

                }
            }
        }

        public static void GridView_RowDataBound_DisableSzereltCheckbox(GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drw = (DataRowView)e.Row.DataItem;
                string allapot = String.Empty;

                if (drw["Allapot"] != null)
                {
                    allapot = drw["Allapot"].ToString();
                }

                if (!String.IsNullOrEmpty(allapot))
                {
                    CheckBox check = (CheckBox)e.Row.FindControl("check");

                    if (check != null)
                    {
                        if (allapot == KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                        {
                            check.Enabled = false;
                        }
                    }

                }
            }
        }

        public static string GetObjectNameByType(string objectType)
        {
            string objectName = String.Empty;

            switch (objectType)
            {
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    objectName = Constants.ObjectNames.kuldemeny;
                    break;
                case Constants.TableNames.EREC_UgyUgyiratok:
                    objectName = Constants.ObjectNames.ugyirat;
                    break;
                case Constants.TableNames.EREC_IraIratok:
                    objectName = Constants.ObjectNames.irat;
                    break;
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    objectName = Constants.ObjectNames.iratpeldany;
                    break;
                case Constants.TableNames.EREC_KuldMellekletek:
                    objectName = Constants.ObjectNames.kuldmelleklet;
                    break;
                case Constants.TableNames.EREC_IratMellekletek:
                    objectName = Constants.ObjectNames.iratmelleklet;
                    break;
            }

            return objectName;
        }

        private static string GetMegorzesSessionName(string SessionName)
        {
            return SessionName + "Checked";
        }

        public static string GetSessionStoredValue(Page page, string SessionName, CheckBox cbMegorzesJelzo)
        {
            string StoredValue = String.Empty;
            string MegorzesSessioName = GetMegorzesSessionName(SessionName);
            if (UI.GetSession(page, MegorzesSessioName) == "true")
            {
                StoredValue = UI.GetSession(page, SessionName);
                cbMegorzesJelzo.Checked = true;
            }
            else
            {
                //session takar�t�sa
                page.Session.Remove(MegorzesSessioName);
                page.Session.Remove(SessionName);
            }

            return StoredValue;
        }

        public static bool IsStoredValueInSession(Page page, string SessionName)
        {
            string MegorzesSessioName = GetMegorzesSessionName(SessionName);
            return UI.GetSession(page, MegorzesSessioName) == "true";
        }

        public static void StoreValueInSession(Page page, string SessionName, string Value, CheckBox cbMegorzesJelzo)
        {
            string MegorzesSessionName = GetMegorzesSessionName(SessionName);
            if (cbMegorzesJelzo.Checked)
            {
                page.Session[SessionName] = Value;
                page.Session[MegorzesSessionName] = "true";
            }
            else
            {
                //session takar�t�sa
                page.Session.Remove(SessionName);
                page.Session.Remove(MegorzesSessionName);
            }
        }

        public static void GetUgyFajtaja(Page page, string ugyFajtaja, out string ugyFajtajaLabel, out string ugyFajtajaTooltip)
        {
            // BLG_44
            if (Rendszerparameterek.GetBoolean(page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                switch (ugyFajtaja)
                {
                    case KodTarak.UGY_FAJTAJA.NemHatosagiUgy:
                        ugyFajtajaLabel = "";
                        ugyFajtajaTooltip = "";
                        break;
                    case KodTarak.UGY_FAJTAJA.AllamigazgatasiUgy:
                        ugyFajtajaLabel = "�";
                        ugyFajtajaTooltip = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGY_FAJTAJA.KodcsoportKod, ugyFajtaja, page);
                        break;
                    case KodTarak.UGY_FAJTAJA.OnkormanyzatiUgy:
                        ugyFajtajaLabel = "�";
                        ugyFajtajaTooltip = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGY_FAJTAJA.KodcsoportKod, ugyFajtaja, page);
                        break;
                    default:
                        goto case KodTarak.UGY_FAJTAJA.NemHatosagiUgy;
                }
            }
            else
            {
                ugyFajtajaLabel = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGY_FAJTAJA.KodcsoportKod, ugyFajtaja, page);
                ugyFajtajaTooltip = ugyFajtajaLabel;
            }
        }

        public static string GetUgyFajtajaLabel(Page page, string ugyFajtaja)
        {
            string label;
            string tooltip;
            GetUgyFajtaja(page, ugyFajtaja, out label, out tooltip);
            return label;
        }

        public static string GetUgyFajtajaToolTip(Page page, string ugyFajtaja)
        {
            string label;
            string tooltip;
            GetUgyFajtaja(page, ugyFajtaja, out label, out tooltip);
            return tooltip;
        }

        public static string GetHatosagiAdatokUrl(string iratId)
        {
            string url = ObjektumTipusok.GetViewUrl(iratId, Constants.TableNames.EREC_IraIratok);
            url += "&" + QueryStringVars.SelectedTab + "=TabPanel_HatosagiStatisztikaPanel";
            return url;
        }

        public static string GetHatosagiAdatokUrl(string iratId, bool modify)
        {
            return OpenObjectUrl(iratId, Constants.TableNames.EREC_IraIratok, modify, "TabPanel_HatosagiStatisztikaPanel");
        }

        public static string OpenObjectViewUrl(string objId, string objType)
        {
            return OpenObjectUrl(objId, objType, false, "");
        }

        public static string OpenObjectModifyUrl(string objId, string objType)
        {
            return OpenObjectUrl(objId, objType, true, "");
        }

        public static string OpenObjectUrl(string objId, string objType, bool modify, string tab)
        {
            string url = modify ? ObjektumTipusok.GetModifyUrl(objId, objType) : ObjektumTipusok.GetViewUrl(objId, objType);
            if (!String.IsNullOrEmpty(tab))
            {
                url += "&" + QueryStringVars.SelectedTab + "=" + tab;
            }
            return url;
        }

        public static string OpenHatosagiAdatok(string iratId, string postBackContolClientName, string postBackArgument)
        {
            string url = GetHatosagiAdatokUrl(iratId, false);
            return JavaScripts.SetOnClientClick(url, "", Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, postBackContolClientName, postBackArgument);
        }

        public static string OpenHatosagiAdatokWithNoPostback(string iratId, bool modify)
        {
            string url = GetHatosagiAdatokUrl(iratId, true);
            return JavaScripts.SetOnCLientClick_NoPostBack(url, "", Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
        }

        public static void SetGridViewColumnVisiblity(GridView gridView, string dataField, bool visible)
        {
            BoundField column = GetGridViewColumn(gridView, dataField);

            if (column != null)
            {
                column.Visible = visible;
            }
        }

        public static BoundField GetGridViewColumn(GridView gridView, string dataFiled)
        {
            for (int i = 0; i < gridView.Columns.Count; i++)
            {
                DataControlField field = gridView.Columns[i];

                BoundField boundfield = field as BoundField;

                if (boundfield != null && boundfield.DataField == dataFiled)
                {
                    return boundfield;
                }
            }

            return null;
        }

        public static TemplateField GetGridViewTemplateColumn(GridView gridView, string headerText)
        {
            for (int i = 0; i < gridView.Columns.Count; i++)
            {
                DataControlField field = gridView.Columns[i];

                TemplateField templateField = field as TemplateField;

                if (templateField != null && templateField.HeaderText == headerText)
                {
                    return templateField;
                }
            }

            return null;
        }

        public static void SetGridViewTemplateColumnVisiblity(GridView gridView, string templateField, bool visible)
        {
            TemplateField column = GetGridViewTemplateColumn(gridView, templateField);

            if (column != null)
            {
                column.Visible = visible;
            }
        }

        /// <summary>
        /// A kiv�lasztott sorokban tal�lhat� megadott oszlop �rt�keit adja vissza
        /// </summary>
        /// <param name="gridView"></param>
        /// <param name="columnIndex">Oszlop sorsz�ma</param>
        /// <returns></returns>
        public static List<string> GetGridViewColumnValuesOfSelectedRowsByIndex(GridView gridView, int columnIndex)
        {
            if (gridView == null || columnIndex < 0 || columnIndex >= gridView.Columns.Count)
            {
                return null;
            }

            List<string> itemsList = new List<string>();

            for (int i = 0; i < gridView.Rows.Count; i++)
            {
                CheckBox checkB = (CheckBox)gridView.Rows[i].FindControl("check");
                if (checkB.Checked)
                {
                    // cella megkeres�se:
                    var cell = gridView.Rows[i].Cells[columnIndex];

                    if (cell != null)
                    {
                        // felvessz�k a list�ba a cella sz�veg�t:
                        itemsList.Add(cell.Text);
                    }
                }
            }

            return itemsList;
        }

        public static int MarkFailedRecordsInGridViewAndSetInfo(GridView gridView, Result result, string objectType)
        {
            if (result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
                return 0;

            int recordNumber = 0;

            foreach (GridViewRow r in gridView.Rows)
            {
                CheckBox checkBox = (CheckBox)r.FindControl("check");
                if (!checkBox.Enabled)
                    continue;

                for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
                {
                    if (gridView.DataKeys[r.RowIndex].Value.ToString() == result.Ds.Tables[0].Rows[i]["Id"].ToString())
                    {
                        r.BackColor = System.Drawing.Color.Red;
                        recordNumber++;

                        // hiba�zenet be�ll�t�sa
                        ImageButton infoImageButton = (ImageButton)r.FindControl("InfoImage");
                        if (infoImageButton != null)
                        {
                            infoImageButton.Visible = true;
                            string errorCode = result.Ds.Tables[0].Rows[i]["ErrorCode"].ToString();
                            string errorMessage = result.Ds.Tables[0].Rows[i]["ErrorMessage"].ToString();
                            string infoMessage = ResultError.GetErrorMessageFromResultObject(new Result { ErrorCode = errorCode, ErrorMessage = errorMessage });
                            infoImageButton.ToolTip = infoMessage;
                            infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                                + objectType + "','" + result.Ds.Tables[0].Rows[i]["Id"].ToString() + "'); return false;";
                        }

                        break;
                    }
                    else
                        r.BackColor = System.Drawing.Color.White;
                }
            }

            return recordNumber;
        }

        public static int MarkSuccededRecordsInGridView(GridView gridView, Result result, List<string> allRecords)
        {
            if (result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
                return 0;

            int recordNumber = 0;

            List<string> failedRecords = new List<string>();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                failedRecords.Add(row["Id"].ToString());
            }

            List<string> succededRecords = allRecords.Where(s => !failedRecords.Contains(s)).ToList();

            foreach (GridViewRow r in gridView.Rows)
            {
                CheckBox checkBox = (CheckBox)r.FindControl("check");
                if (!checkBox.Enabled)
                    continue;

                string id = gridView.DataKeys[r.RowIndex].Value.ToString();

                if (succededRecords.Contains(id))
                {
                    r.BackColor = System.Drawing.Color.LightGreen;
                    checkBox.Checked = false;
                    checkBox.Enabled = false;
                    recordNumber++;
                }
            }

            return recordNumber;
        }
    }

    public static class eMigration
    {
        private static string eMigrationWebsiteUrl;

        public static string EMigrationWebsiteUrl
        {
            get
            {
                if (String.IsNullOrEmpty(eMigrationWebsiteUrl))
                {
                    string url = UI.GetAppSetting("eMigrationWebSiteUrl");
                    try
                    {
                        Uri uri = new Uri(url);
                        eMigrationWebsiteUrl = uri.AbsolutePath;
                    }
                    catch (UriFormatException)
                    {
                        eMigrationWebsiteUrl = url;
                    }
                }
                return eMigrationWebsiteUrl;
            }
        }

        public static string migralasLovListUrl = EMigrationWebsiteUrl + "FoszamLOVList.aspx";

        public static string migralasFormUrl = EMigrationWebsiteUrl + "FoszamForm.aspx";
    }

    public class UIMezoObjektumErtekek : Contentum.eRecord.BaseUtility.UIMezoObjektumErtekek
    {
    }

    public class Log : Contentum.eRecord.BaseUtility.Log
    {
        public Log(Page page)
            : base(page)
        {
        }
    }

    public class Authentication : Contentum.eRecord.BaseUtility.Authentication
    {
    }

    public class FelhasznaloProfil : Contentum.eRecord.BaseUtility.FelhasznaloProfil
    {
        public static bool OrgIsBOPMH(Page page)
        {
            return OrgIs(page, Constants.OrgKod.BOPMH);
        }

        public static bool OrgIsNMHH(Page page)
        {
            return OrgIs(page, Constants.OrgKod.NMHH);
        }

        public static bool OrgIs(Page page, string org)
        {
            return org.Equals(FelhasznaloProfil.OrgKod(page), StringComparison.InvariantCultureIgnoreCase);
        }
    }

    public class FunctionRights : Contentum.eRecord.BaseUtility.FunctionRights
    {
    }

    public class LockManager : Contentum.eRecord.BaseUtility.LockManager
    {
    }

    public class JavaScripts : Contentum.eRecord.BaseUtility.JavaScripts
    {
        private static string AddQueryString(string url, string qs)
        {
            if (!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(qs))
            {
                if (url.Contains("?"))
                {
                    url += "&";
                }
                else
                {
                    url += "?";
                }

                url += qs;
            }

            return url;
        }
        public static string SetOnClientClickIFramePrintForm(string url)
        {
            Page page = HttpContext.Current.Handler as Page;
            return SetOnClientClickIFramePrintForm(url, page);
        }
        public static string SetOnClientClickIFramePrintForm(string url, Page page)
        {
            string javascript = "";

            if (page != null)
            {
                //bejelentkez�si adatok hozz�ad�sa az url-hez
                string loginQs = UI.GetLoginDataQueryString(page);
                url = AddQueryString(url, loginQs);

            }

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }

            if (pdf)
            {
                javascript = "window.open('" + url + "')";
            }
            else
            {
                javascript =
                   "if(document.getElementById(\"downloadDiv\") == null)" +
                   "{  var downloadDiv = document.createElement(\"div\");" +
                   "   document.getElementsByTagName(\"body\")[0].appendChild(downloadDiv);" +
                   "   downloadDiv.style.width = \"0px\";" +
                   "   downloadDiv.style.height = \"0px\";" +
                   "   downloadDiv.id = \"downloadDiv\"; }" +
                   "else var downloadDiv = document.getElementById(\"downloadDiv\");" +
                   "if(document.getElementById(\"downloadFrame\") == null)" +
                   "{  var downloadFrame = document.createElement(\"iframe\");" +
                   "   downloadFrame.id = \"downloadFrame\";" +
                   "   downloadFrame.src = '" + url + "';" +
                   "   downloadFrame.scrolling = \"no\";" +
                   "   downloadFrame.style.width = \"0px\";" +
                   "   downloadFrame.style.height = \"0px\";" +
                   "   downloadDiv.innerHTML = downloadFrame.outerHTML; }" +
                   "else { var downloadFrame = document.getElementById(\"downloadFrame\");" +
                   "       downloadFrame.src = '" + url + "'; }";
            }
            return javascript;
        }

        public static string SetOnClientClickSharePointOpenDocument()
        {
            string javascript = "";
            //javascript = "DispDocItem(this,'SharePoint.OpenDocuments'); return false;";
            javascript = "DispEx(this,event,'FALSE','FALSE','FALSE','SharePoint.OpenDocuments','0','SharePoint.OpenDocuments','','','14','14','0','0','0x7fffffffffffffff'); return false;";
            return javascript;
        }
        public static string SetOnClientClickConfirm(string questionId)
        {
            return
                "var count = 1;" +
                " if (count==0) {alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoSelectedItem")
                + "'); return false;} "
                + "if (count==1) { "
                + "if (confirm('" + Contentum.eUtility.Resources.Question.GetString(questionId) + " ')) { } else { return false; }"
                + " } else { alert('" + Contentum.eUtility.Resources.List.GetString("UI_NoMultiSelectedItem") + "'); return false; } ";
        }

        public static string SetOnClientClickSztornoConfirm()
        {
            return SetOnClientClickConfirm("UIConfirmHeader_Sztorno");
        }

        public static string SetOnClientClickLezarasConfirm()
        {
            return SetOnClientClickConfirm("UIConfirmHeader_Lezaras");
        }
        public static string SetOnClientClickElintezesConfirm()
        {
            return SetOnClientClickConfirm("UIConfirmHeader_Elintezes");
        }
        public static string SetOnClientClickElintezesVisszavonasConfirm()
        {
            return SetOnClientClickConfirm("UIConfirmHeader_ElintezesVisszavonas");
        }

        public static string SetOnClientClickNemKolcsonozhetoUgyiratAlert()
        {
            return "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemKolcsonozhetoUgyirat") + "'); return false;";
        }

        public static string VisibleIf(string condition, Control e)
        {
            return e == null || String.IsNullOrEmpty(condition) ? "" :
                @"if ($get('" + e.ClientID + @"')) { $get('" + e.ClientID + @"').style.display = (" + condition + ") ? 'inline' : 'none'; } ";
        }

        // ---------------------- /Dosszie -----------------------
        public static void RegisterSetDossziePositionScript(Page page, bool registerWithScriptManager)
        {
            string script = @"var PanelDossziekDivStartPosition = 0;
                            $addHandler(window,'load',function (){
                                var PanelDossziekDiv = $get('PanelDossziekDiv');
                                if (PanelDossziekDiv) { PanelDossziekDivStartPosition = PanelDossziekDiv.offsetTop;}
                            });
                            $addHandler(window, 'scroll', function(){
                                var PanelDossziekDiv = $get('PanelDossziekDiv');
                                if (PanelDossziekDiv) {
                                    if (PanelDossziekDiv.clientHeight > document.documentElement.clientHeight)
                                    {
                                        return true;
                                    }

                                    if (PanelDossziekDivStartPosition > document.documentElement.scrollTop)
                                    {
                                        PanelDossziekDiv.style.top = '0px';
                                    }
                                    else
                                    {
                                        PanelDossziekDiv.style.top = (document.documentElement.scrollTop - PanelDossziekDivStartPosition) + 'px';
                                    }
                                }
                            });";

            if (registerWithScriptManager)
            {
                ScriptManager.RegisterStartupScript(page, page.GetType(), script, script, true);
            }
            else
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), script
                    , script, true);
            }
        }

        // ---------------------- /Dosszie -----------------------
    }

    public class DefaultDates : Contentum.eRecord.BaseUtility.DefaultDates
    {
    }

    public class CommandName : Contentum.eRecord.BaseUtility.CommandName
    {
        public const string Cancel = "Cancel";
        public const string Version = "Version";
        public const string ElosztoLista = "ElosztoLista";
        // kiemelve az eUtilitybe, eMigration miatt
        //public const string SaveAndClose = "SaveAndClose";
        //public const string SaveAndNew = "SaveAndNew";
        public const string SaveAndAlszam = "SaveAndAlszam";
        public const string SaveAndAlszamBejovo = "SaveAndAlszamBejovo";
        public const string SaveAndAlszamBelso = "SaveAndAlszamBelso";

        public const string IktatasMegtagadas = "IktatasMegtagadas";

        public const string Bontas = "Bontas";
        public const string EloadoiIv = "EloadoiIv";
        public const string PeresEloadoiIv = "PeresEloadoiIv";
        //public const string Sztorno = "Sztorno"; // -> EB 2011.05.17: Kiemelve eUtilitybe, hogy eMigration is l�ssa
        public const string BejovoIratIktatas = "BejovoIratIktatas";
        public const string BelsoIratIktatas = "BelsoIratIktatas";
        public const string ElokeszitettIratIktatas = "ElokeszitettIratIktatas";
        public const string AtIktatas = "AtIktatas";
        public const string KimenoEmailIktatas = "KimenoEmailIktatas";
        public const string Lezaras = "Lezaras";
        public const string LezarasVisszavonas = "LezarasVisszavonas";
        public const string Boritonyomtatas = "Boritonyomtatas";
        public const string Felszabaditas = "Felszabaditas";
        // CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
        public const string MunkapeldanyBeiktatas_Bejovo = "MunkapeldanyBeiktatas_Bejovo";

        public const string Szignalas = "Szignalas";
        public const string AtadasraKijeloles = "AtadasraKijeloles";
        public const string Atadas = "Atadas";
        public const string AtadasListaval = "AtadasListaval";
        public const string Atvetel = "Atvetel";
        public const string AtvetelUgyintezesre = "AtvetelUgyintezesre";
        public const string Visszakuldes = "Visszakuldes";
        public const string AtvetelListaval = "AtvetelListaval";
        //public const string UgyiratpotloNyomtatasa = "UgyiratpotloNyomtatasa"; �tt�ve az eutility-be CR3207
        public const string ElintezetteNyilvanitas = "ElintezetteNyilvanitas";
        public const string ElintezetteNyilvanitasVisszavon = "ElintezetteNyilvanitasVisszavon";
        //BLG 1131 Ut�lagosan adminisztr�lt bels� adatmozg�sok
        public const string IratmozgasAdminisztralasa = "IratmozgasAdminisztralasa";
        //BLG 1130 �tvev� jegyz�s nyomtat�sa
        public const string AtvevoJegyzekNyomtatasa = "AtvevoJegyzekNyomtatasa";
        public const string AtvevoJegyzekNyomtatasaTomeges = "AtvevoJegyzekNyomtatasaTomeges";
        public const string Visszavetel = "Visszavetel";

        #region CR3152 - Expedi�l�s folyamat t�meges�t�se
        public const string Expedialas = "Expedialas";
        #endregion  CR3152 - Expedi�l�s folyamat t�meges�t�se
        public const string Postazas = "Postazas";
        public const string Athelyezes = "Athelyezes";

        public const string Atadandok = "Atadandok";
        public const string Atveendok = "Atveendok";
        public const string AltalanosKereses = "AltalanosKereses";

        public const string List = "List";

        public const string Kolcsonzes = "Kolcsonzes";
        //public const string KolcsonzesJovahagyas = "KolcsonzesJovahagyas";
        public const string KolcsonzesVisszautasitas = "KolcsonzesVisszautasitas";
        public const string KolcsonzesSztorno = "KolcsonzesSztorno";
        public const string KolcsonzesKiadas = "KolcsonzesKiadas";
        public const string KolcsonzesVissza = "KolcsonzesVissza";
        public const string Jegyzekrehelyezes = "Jegyzekrehelyezes";
        public const string KikeresAtmenetiIrattarbol = "KikeresAtmenetiIrattarbol";
        public const string KiadasAtmenetiIrattarbol = "KiadasAtmenetiIrattarbol";
        public const string KikeresSkontroIrattarbol = "KikeresSkontroIrattarbol";
        public const string KiadasSkontroIrattarbol = "KiadasSkontroIrattarbol";

        public const string VonalkodSavokIgenyles = "VonalkodSavokIgenyles";
        public const string VonalkodSavokNyomtatas = "VonalkodSavokNyomtatas";
        public const string VonalkodSavokFeltoltes = "VonalkodSavokFeltoltes";
        public const string VonalkodSavokSzetosztas = "VonalkodSavokSzetosztas";


        public const string RagszamIgenyles = "RagszamIgenyles";

        public const string SzlaExport = "SzlaExport";

        // -> EB 2011.05.17: Kiemelve eUtilitybe, hogy eMigration is l�ssa
        //public const string Felulvizsgalat = "Felulvizsgalat";
        //public const string JegyzekZarolas = "JegyzekZarolas";
        //public const string JegyzekVegrehajtas = "JegyzekVegrehajtas";

        public const string CsoportIdTulaj = "CsoportIdTulaj";

        public const string DossziebaHelyez = "DossziebaHelyez";
        public const string DossziebolKivesz = "DossziebolKivesz";

        public const string ElektronikusAlairas = "ElektronikusAlairas";
        public const string AlairasEllenorzes = "AlairasEllenorzes";
        public const string VisszaUtasitas = "VisszaUtasitas";
        public const string Alairas = "Alairas";
        public const string TomegesAlairas = "TomegesAlairas";
        //BLG 1880
        public const string TomegesRagszamGeneralas = "TomegesRagszamGeneralas";
        public const string Elutasitas = "Elutasitas";

        public const string KimenoKuldemenyFelvitel = "KimenoKuldemenyFelvitel";

        public const string NodeRights = "NodeRights";
        public const string ExpandNode = "ExpandNode";
        public const string CollapseNode = "CollapseNode";
        #region CR3206 - Iratt�ri strukt�ra
        public const string UgyiratokList = "UgyiratokList";
        public const string Irattarbarendezes = "Irattarbarendezes";
        #endregion

        public const string TeljessegEllenorzes = "TeljessegEllenorzes";
        public const string UgyiratSkontrobaHelyezesJovahagyasa = "UgyiratSkontrobaHelyezesJovahagyasa";
        public const string UgyiratSkontrobaHelyezesElutasitasa = "UgyiratSkontrobaHelyezesElutasitasa";
        public const string UgyiratSkontrobaHelyezesVisszavonasa = "UgyiratSkontrobaHelyezesVisszavonasa";

        public const string UgyiratIrattarozasJovahagyasa = "UgyiratIrattarozasJovahagyasa";
        public const string InfoszabIktatas = "InfoszabIktatas";

        public const string ElintezetteNyilvanitasJovahagyasa = "ElintezetteNyilvanitasJovahagyasa";
        public const string ElintezetteNyilvanitasVisszautasitasa = "ElintezetteNyilvanitasVisszautasitasa";

        public const string Filter = "Filter";
        public const string Felfuggesztes = "Felfuggesztes";

        public const string TomegesSzignalas = "TomegesSzignalas";

        public const string KikeresAtmenetiIrattarbolJovahagyas = "KikeresAtmenetiIrattarbolJovahagyas";

        public const string IratAtvetelIntezkedesre = "IratAtvetelIntezkedesre";
    }

    public class EventArgumentConst : Contentum.eRecord.BaseUtility.EventArgumentConst
    {
        public const string refreshIrattariTetelPanel = "refreshIrattariTetelPanel";
        public const string refreshCsatolasokList = "refreshCsatolasokList";
        public const string refreshIratPeldanyPanel = "refreshIratPeldanyPanel";
        public const string refreshFeladatok = "refreshFeladatok";
        public const string refreshUgyiratok = "refreshUgyiratok";
        public const string refreshUgyiratTerkep = "refreshUgyiratTerkep";
        public const string refreshAlairoSzabalyok = "refreshAlairoSzabalyok";
        public const string refreshCsatolmanyokList = "refreshCsatolmanyokList";
        public const string refreshDossziekList = "refreshDossziekList";
    }

    public class Constants : Contentum.eRecord.BaseUtility.Constants
    {

        public static class CustomSearchObjectSessionNames
        {
            public const string ErkeztetoKonyvekSearch = "EREC_IraIktatoKonyvekSearch_ErkeztetoKonyv";
            public const string XMLExportSearch = "KRT_XMLExportSearch";
            public const string IktatoKonyvekSearch = "EREC_IraIktatoKonyvekSearch_IktatoKonyv";
            public const string PostaKonyvekSearch = "EREC_IraIktatoKonyvekSearch_PostaKonyv";

            public const string AtadandokSearch = "EREC_IraKezbesitesiTetelekSearch_Atadandok";
            public const string AtveendokSearch = "EREC_IraKezbesitesiTetelekSearch_Atveendok";
            public const string AltalanosKeresesSearch = "EREC_IraKezbesitesiTetelekSearch_AltalanosKereses";

            public const string KimenoKuldemenyekSearch = "EREC_KuldKuldemenyekSearch_Kimeno";

            public const string UgyiratokLovList = "EREC_UgyUgyiratokLovListSearch";

            public const string AtmenetiIrattarSearch = "EREC_UgyUgyiratokSearch_AtmenetiIrattar";
            public const string KozpontiIrattarSearch = "EREC_UgyUgyiratokSearch_KozpontiIrattar";
            public const string SkontroIrattarSearch = "EREC_UgyUgyiratokSearch_SkontroIrattar";

            public const string AtmenetiIrattarbaVetelSearch = "EREC_UgyUgyiratokSearch_AtmenetiIrattarbaVetel";
            public const string KozpontiIrattarbaVetelSearch = "EREC_UgyUgyiratokSearch_KozpontiIrattarbaVetel";

            public const string SkontroSearch = "EREC_UgyUgyiratokSearch_Skontro";

            public const string MunaknaploSearch = "EREC_UgyUgyiratokSearch_Munkanaplo";
            public const string FeladataimSearch = "EREC_HataridosFeladatokSearch_Feladataim";
            public const string KiadottFeladatokSearch = "EREC_HataridosFeladatokSearch_Kiadott";
            public const string DolgozokFeladataiSearch = "EREC_HataridosFeladatokSearch_DolgozokFeladatai";

            public const string IratAlairandokSearch = "EREC_IraIratokSearch_Alairandok";

            public const string MunkaUgyiratokIrataiSearch = "MunkaUgyiratokIratai";
            public const string KozgyulesiEloterjesztesekSearch = "KozgyulesiEloterjesztesIratai";
            public const string BizottsagiEloterjesztesekSearch = "BizottsagiEloterjesztesekSearch";
            public const string StandaloneDokumentumokSearch = "StandaloneDokumentumok";
            public const string SzakagiDokumentumokSearch = "SzakagiDokumentumok";

            public const string SzamlaSearch = "EREC_KuldKuldemenyek_Szamla";

            public const string HivataliKapusMunkaUgyiratokIrataiSearch = "HivataliKapusMunkaUgyiratokIratai";

            // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
            public const string MunkairatSzignalasSearch = "MunkairatSzignalas";

            // CR3289 Vezet�i panel kezel�s
            public const string VezetoiPanelSearch = "VezetoiPanelSearch";

            // CR3355
            public const string IratkezelesiSegedletekSearch = "EREC_IraIktatoKonyvekSearch_IratkezelesiSegedlet";
            public const string FonyilvantartokonyvSearch = "EREC_IraIktatoKonyvekSearch_Fonyilvantartokonyv";
            public const string KimenoPeldanyokSearch = "EREC_PldIratPeldanyokSearch_Kimeno";
        }

        public enum FileUploadMode
        {
            single = 0,
            multiple = 1
        }

        public static class ParentForms
        {
            public const string IraIrat = "IraIrat";
            public const string Kuldemeny = "Kuldemeny";
            public const string Ugyirat = "Ugyirat";
            public const string UgyiratDarab = "UgyiratDarab";
            public const string IratPeldany = "IratPeldany";
            public const string Feladataim = "Feladataim";
            public const string KiadottFeladataim = "KiadottFeladataim";
            public const string DolgozokFeladatai = "DolgozokFeladatai";
            public const string Dokumentum = "Dokumentum";
        }

        public static class Startup
        {
            public const string StartupName = "Startup";
            public const string FromEmail = "FromEmail";
            public const string FromAtmenetiIrattar = "FromAtmenetiIrattar";
            public const string FromKozpontiIrattar = "FromKozpontiIrattar";
            public const string FromSkontroIrattar = "FromSkontroIrattar";
            public const string FromUgyirat = "FromUgyirat";
            public const string FromAtmenetiIrattarbaVetel = "FromAtmenetiIrattarbaVetel";
            public const string FromKozpontiIrattarbaVetel = "FromKozpontiIrattarbaVetel";
            public const string FromTertiveveny = "FromTertiveveny";
            public const string FromSelejtezes = "FromSelejtezes";
            public const string FromLeveltarbaAdas = "FromLeveltarbaAdas";
            public const string FromEgyebSzervezetnekAtadas = "FromEgyebSzervezetnekAtadas";
            public const string FromFelulvizsgalando = "FromFelulvizsgalando";
            public const string FromFelulvizsgalat = "FromFelulvizsgalat";
            public const string FromJegyzekTetel = "FromJegyzekTetel";
            public const string FromEgyszerusitettIktatas = "FromEgyszerusitettIktatas";
            public const string FromIrat = "FromIrat";
            public const string FromIratPeldany = "FromIratPeldany";
            public const string Skontro = "Skontro";
            public const string SearchForm = "SearchForm";
            public const string FastSearchForm = "FastSearchForm";
            public const string FromMunkanaplo = "FromMunkanaplo";
            public const string FromSzignalasiJegyzek = "FromSzignalasiJegyzek";
            public const string FromKuldemeny = "FromKuldemeny";
            public const string FromFeladatok = "FromFeladatok";
            public const string FromObjektumok = "FromObjektumok";
            public const string FromFeladataim = "FromFeladataim";
            public const string FromKiadottFeladatok = "FromKiadottFeladatok";
            public const string FromDolgozokFeladatai = "FromDolgozokFeladatai";
            public const string FromVezetoiPanel = "FromVezetoiPanel";
            // CR3289 Vezet�i panel kezel�s
            public const string FromVezetoiPanel_Ugyirat = "FromVezetoiPanel_Ugyirat";
            public const string FromVezetoiPanel_Irat = "FromVezetoiPanel_Irat";
            public const string FromVezetoiPanel_Kuldemeny = "FromVezetoiPanel_Kuldemeny";

            public const string FromIktatoKonyvek = "FromIktatoKonyvek";
            public const string FromErkeztetoKonyvek = "FromErkeztetoKonyvek";
            public const string FromAlairandok = "FromAlairandok";
            public const string FromMunkaUgyiratokIratai = "FromMunkaUgyiratokIratai";
            public const string FromKozgyulesiEloterjesztesek = "FromKozgyulesiEloterjesztesek";
            public const string FromBizottsagiEloterjesztesek = "FromBizottsagiEloterjesztesek";
            public const string Standalone = "Standalone";
            public const string Szakagi = "Szakagi";
            public const string Szamla = "Szamla";
            public const string SzamlaSearchForm = "SzamlaSearchForm";
            public const string SzamlaFelvitel = "SzamlaFelvitel";
            public const string AtmenetiIrattarSearchForm = "AtmenetiIrattarSearchForm";
            public const string AtmenetiIrattarFastSearchForm = "AtmenetiIrattarFastSearchForm";
            public const string KozpontiIrattarSearchForm = "KozpontiIrattarSearchForm";
            public const string KozpontiIrattarFastSearchForm = "KozpontiIrattarFastSearchForm";
            public const string SkontroIrattarSearchForm = "SkontroIrattarSearchForm";
            public const string SkontroIrattarFastSearchForm = "SkontroIrattarFastSearchForm";
            public const string KimenoKuldemenyFelvitel = "KimenoKuldemenyFelvitel";
            public const string FromHivataliKapusMunkaUgyiratokIratai = "FromHivataliKapusMunkaUgyiratokIratai";
            // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
            public const string FromMunkairatSzignalas = "FromMunkairatSzignalas";
            public const string FromPartnerekList = "FromPartnerekList";
            public const string FromKimenoIratPeldany = "FromKimenoIratPeldany";

            //LZS - BUG_10927
            public const string OktatasiAnyagok = "OktatasiAnyagok";


            //LZS - BLG_51
            public const string Sablonok = "Sablonok";
        }

        public const string LockedRecord = "LockedRecord";
        public const string IktathatoKuldemenyek = "IktathatoKuldemenyek";
        public const string AlszamraIktathatoak = "AlszamraIktathatoak";
        public const string Sajat = "Sajat";
        public const string SajatMappa = "SajatMappa"; // speci�lis kezel�s, m�sk�pp a kapcsol�d� list�kon is sz�rn�nk

        public static class JovahagyandokFilter
        {
            public const string Jovahagyandok = "Jovahagyandok";
            public const string Irattarozasra = "JovahagyandokIrattarozasra";
            public const string Kolcsonzesre = "JovahagyandokKolcsonzesre";
            public const string SkontrobaHelyezes = "JovahagyandokSkontrobaHelyezes";
            public const string Elintezesre = "JovahagyandokElintezesre";
            public const string Kikeresre = "JovahagyandokKikeresre";
        }

        public const string Expedialas = "Expedialas";
        public const string KimenoKuldemeny = "KimenoKuldemeny";
        public const string KuldTertivevenyek = "KuldTertivevenyek";
        public const string SelectedUgyiratIds = "SelectedUgyiratIds";
        public const string SelectedIratIds = "SelectedIratIds";

        public const string SelectedBarcodeIds = "SelectedBarcodeIds";
        public const string SelectedKuldemenyIds = "SelectedKuldemenyIds";
        public const string SelectedIratPeldanyIds = "SelectedIratPeldanyIds";
        public const string SelectedDosszieIds = "SelectedDosszieIds";

        public const string SelectedJegyzekTetelIds = "SelectedJegyzekTetelIds";

        public const string Postazas = "Postazas";  //nekrisz CR 2961

        public enum IktatoKonyvekDropDownListMode
        {
            ErkeztetoKonyvek = 0,
            Iktatokonyvek = 1,
            IktatokonyvekWithRegiAdatok = 2,
        }

        public class ObjectNames
        {
            public const string kuldemeny = "K�ldem�ny";
            public const string iratpeldany = "Iratp�ld�ny";
            public const string ugyirat = "�gyirat";
            public const string irat = "Irat";
            public const string kuldmelleklet = "K�ldem�ny mell�klet";
            public const string iratmelleklet = "Irat mell�klet";
        }

        public class SessionKeys
        {
            public const string SelectedIrattarNodeId = "SelectedIrattarNodeId";
            public const string SelectedIrattarNodePath = "SelectedIrattarNodePath";
        }

        public const string EngedelyezettKikeronLevo = "EngedelyezettKikeronLevo";
    }

    /// <summary>
    /// URL-ekben el�fordul� param�ternevek
    /// </summary>
    public class QueryStringVars : Contentum.eRecord.BaseUtility.QueryStringVars
    {
        public const string XMLExportId = "XMLExportId";
        public const string ErkeztetokonyvId = "ErkeztetokonyvId";
        public const string IktatokonyvId = "IktatokonyvId";
        public const string PostakonyvId = "PostakonyvId";
        public const string IrattariTetelId = "IrattariTetelId";
        public const string KuldemenyId = "KuldemenyId";
        public const string fileUploadMode = "FileuploadMode";
        public const string IratId = "IratId";
        public const string UgyiratId = "UgyiratId";
        public const string UgyiratDarabId = "UgyiratDarabId";
        public const string IratPeldanyId = "IratPeldanyId";
        public const string Vonalkodok = "Vonalkodok";
        public const string MaxSorszam = "MaxSorszam";
        public const string Tipus = "Tipus";
        public const string DefinicioTipusHiddenFieldId = "DefinicioTipusHiddenFieldId";
        public const string SPSSzinkronizaltHiddenFieldId = "SPSSzinkronizaltHiddenFieldId";
        public const string EmailBoritekokId = "EmailBoritekokId";
        public const string ObjektumKapcsolatTipus = "ObjektumKapcsolatTipus";
        public const string ObjMetaDefinicioId = "ObjMetaDefinicioId";
        public const string MellekletId = "MellekletId";
        public const string CsatolmanyId = "CsatolmanyId";
        public const string NoAncestorLoopWithId = "NoAncestorLoopWithId";
        public const string NoSuccessorLoopWithId = "NoSuccessorLoopWithId";
        public const string DefinicioTipus = "DefinicioTipus";
        public const string DokumentumId = "DokumentumId";
        public const string NevTextBoxId = "NevTextBoxId";
        public const string IrattariJelTextBoxId = "IrattariJelTextBoxId";
        public const string Azonosito = "Azonosito";
        public const string PostazasIranya = "PostazasIranya";
        public const string FeladatTipus = "FeladatTipus";
        public const string MunkaUgyiratokIratai = "MunkaUgyiratokIratai";
        public const string ItemUrl = "itemurl";
        public const string Hash = "hash";
        public const string OrderBy = "OrderBy";
        public const string SelectedUgyiratokSessionKey = "SelecteUgyiratokSessionKey";
        public const string Ev = "Ev";
        public const string HivataliKapusMunkaUgyiratokIratai = "HivataliKapusMunkaUgyiratokIratai";
        public const string PostBackClientID = "PostBackClientID";

        // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
        public const string MunkairatSzignalas = "MunkairatSzignalas";
        #region CR3206 - Iratt�ri strukt�ra
        public const string SzuloId = "SzuloId";
        #endregion
        // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
        public const string ModifyIrattar = "ModifyIrattar";
        // HALK al��r�s
        public const string ProcId = "Proc_Id";
        public const string DatumKezd = "DatumKezd";
        public const string DatumVege = "DatumVege";

        //LZS - BUG_8882
        public const string Ugy_fajtaja = "Ugy_fajtaja";

        #region BLG_2951
        //LZS
        public const string Jegyzek_Id = "Jegyzek_Id";
        #endregion

        public static string Get<T>(Page page, string paramName)
        {
            string ret = String.Empty;

            if (!String.IsNullOrEmpty(paramName))
            {
                string paramValue = page.Request.QueryString.Get(paramName);
                if (!String.IsNullOrEmpty(paramValue))
                {
                    Type paramType = typeof(T);
                    TypeCode paramTypeCode = Type.GetTypeCode(paramType);

                    if (paramType == typeof(Guid))
                    {
                        try
                        {
                            Guid guidParam = new Guid(paramValue);
                            ret = paramValue;
                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        switch (paramTypeCode)
                        {
                            case TypeCode.Int32:
                                int intParamValue;
                                if (Int32.TryParse(paramValue, out intParamValue))
                                {
                                    ret = paramValue;
                                }
                                break;
                            case TypeCode.Boolean:
                                bool boolParamValue;
                                if (Boolean.TryParse(paramValue, out boolParamValue))
                                {
                                    ret = paramValue;
                                }
                                break;
                            default:
                                ret = paramValue;
                                break;
                        }
                    }
                }
            }


            return ret;
        }

    }

    public class Defaults : Contentum.eRecord.BaseUtility.Defaults
    {
    }


    public class ResultError : Contentum.eRecord.BaseUtility.ResultError
    {
    }

    public class Notify : Contentum.eRecord.BaseUtility.Notify
    {
    }

    //LZS - BUG_11081
    public class Note_JSON : Contentum.eRecord.BaseUtility.Note_JSON
    {
    }

    public class KodTarak : Contentum.eRecord.BaseUtility.KodTarak
    {
    }

    public class PageView : BaseUtility.PageView
    {
        public PageView(Page ParentPage, StateBag ViewState)
            : base(ParentPage, ViewState)
        {
        }
    }

    public class StringFormatter
    {
        public static String GetUgyiratDarabMegnevezes(String UgyiratMegnevezes, String Hatarido, String LezarasDat)
        {
            String ugyiratDarabMegnevezes = UgyiratMegnevezes + " / " + Hatarido + " / " + LezarasDat;
            return ugyiratDarabMegnevezes;
        }
    }

    public class PirEdokSzamlak : Contentum.eRecord.BaseUtility.PirEdokSzamlak
    {

    }

    public class Ugyiratok : Contentum.eRecord.BaseUtility.Ugyiratok
    {
        public static string GetFullFoszam(EREC_UgyUgyiratok erec_UgyUgyiratok, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            if (erec_UgyUgyiratok != null)
            {
                EREC_IraIktatoKonyvekService IktatoKonyvekservice = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam IktatoKonyvekexecParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                IktatoKonyvekexecParam.Record_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;

                Result IktatoKonyvekresult = IktatoKonyvekservice.Get(IktatoKonyvekexecParam);
                if (String.IsNullOrEmpty(IktatoKonyvekresult.ErrorCode))
                {
                    EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)IktatoKonyvekresult.Record;
                    if (erec_IraIktatoKonyvek != null)
                    {
                        // BLG_292
                        //return GetFullFoszam(erec_UgyUgyiratok, erec_IraIktatoKonyvek);
                        ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                        //return GetFullFoszam(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek);
                        return Iktatoszam.GetFullAzonosito(execParam, erec_UgyUgyiratok, erec_IraIktatoKonyvek);

                    }
                    else
                    {
                        return "hiba!";
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, IktatoKonyvekresult);
                    if (ErrorUpdatePanel != null)
                    {
                        ErrorUpdatePanel.Update();
                    }
                    return "hiba!";
                }
            }
            else
            {
                return "hiba!";
            }
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra, amelyekbe lehet alsz�mra iktatni
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_AlszamraIktathatok(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            var allapotok = new List<string>() {
                KodTarak.UGYIRAT_ALLAPOT.Iktatott,
                KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt,
                KodTarak.UGYIRAT_ALLAPOT.Elintezett,
                KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart,
                KodTarak.UGYIRAT_ALLAPOT.Skontroban,
                KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott, // CR#2580 (2010.12.27): Iratt�rba k�ld�ttbe nem szabad iktatni: ezt csak megjelen�tj�k, kiv�lasztani majd nem lehet
                KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa,
                KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott,
                KodTarak.UGYIRAT_ALLAPOT.Szignalt,
                KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt,
                KodTarak.UGYIRAT_ALLAPOT.Szerelt, // ezt csak megjelen�tj�k, kiv�lasztani majd nem lehet
                KodTarak.UGYIRAT_ALLAPOT.Felfuggesztett
            };

            /// BUG#4758: Iratt�rban l�v�be iktathat-e (rendszerparam�ter vez�rli)
            /// (Ha nem iktathat, akkor n�h�ny st�tusszal kevesebb ker�l a sz�r�be.)
            /// 
            bool irattarbaIktathat = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(page), Rendszerparameterek.IRATTARBA_IKTATAS_ENABLED, true);
            if (irattarbaIktathat)
            {
                allapotok.AddRange(new string[] {
                    KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
                    KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott,
                    KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
                });
            }

            searchObject.Manual_AlszamraIktathatoak.In(allapotok);
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra, amelyek j�v�hagy�sra v�rnak
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_Jovahagyandok(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            var allapotok = new string[]
            {
                KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa,
                //KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
                KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
            };
            searchObject.Manual_Jovahagyandok.In(allapotok);

            searchObject.Csoport_Id_Felelos.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_Jovahagyandok;
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra, amelyek iratt�roz�shoz j�v�hagy�sra v�rnak
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_JovahagyandokIrattarozasra(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            searchObject.Manual_Jovahagyandok.Filter(KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa);

            //searchObject.Csoport_Id_Felelos.Value =
            //    Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page));
            //searchObject.Csoport_Id_Felelos.Operator = Contentum.eQuery.Query.Operators.equals;

            searchObject.Manual_Jovahagyo.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_JovahagyandokIrattarozasra;
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra, amelyek k�lcs�nz�shez j�v�hagy�sra v�rnak
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_JovahagyandokKolcsonzesre(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            //searchObject.Manual_Jovahagyandok.Filter(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert); // m�r van �llapotra sz�r�s

            //searchObject.Manual_Jovahagyo.IsNullOrEquals(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));
            searchObject.Manual_Jovahagyo.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_JovahagyandokKolcsonzesre;
        }

        public static void SetSearchObjectTo_JovahagyandokKikeresre(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            //searchObject.Manual_Jovahagyandok.Filter(KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert); // m�r van �llapotra sz�r�s

            searchObject.Manual_Jovahagyo.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_JovahagyandokKikeresre;
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra, amelyek skontr�ba helyez�sre j�v�hagy�sra v�rnak
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_JovahagyandokSkontrobaHelyezes(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            searchObject.Manual_Jovahagyandok.Filter(KodTarak.UGYIRAT_ALLAPOT.SkontrobaHelyezesJovahagyasa);

            searchObject.Manual_Jovahagyo.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_JovahagyandokSkontrobaHelyezes;
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra, amelyek elint�zett� nyilv�n�t�s jov�hagy�s�ra v�rnak
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_JovahagyandokElintezesre(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            searchObject.Manual_Jovahagyandok.Filter(KodTarak.UGYIRAT_ALLAPOT.ElintezesJovahagyasa);

            searchObject.Manual_Jovahagyo.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_JovahagyandokElintezesre;
        }

        /// <summary>
        /// Sz�r�si felt�telt �ll�t be: azokra az �gyiratokra melyen k�zponti iratt�rb�l kiad�sra v�rnak
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_EngedelyezettKikeronLevo(EREC_UgyUgyiratokSearch searchObject, Page page)
        {
            searchObject.Manual_Jovahagyandok.Filter(KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo);

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Ugyirat_EngedelyezettKikeronLevo;
        }

        /// <summary>
        /// (�gyiratok list�j�hoz) alap sz�r�si felt�tel be�ll�t�sa
        /// </summary>
        /// <param name="erec_UgyUgyiratokSearch"></param>
        public static void SetDefaultFilter(EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch)
        {
            // Allapot is not null
            erec_UgyUgyiratokSearch.Manual_Allapot_DefaultFilter.NotNull();
        }

        //CR 2121
        public static void Sztornozas(String ugyiratId, String SztornoIndoka, Page page
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(ugyiratId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratSztorno"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Sztornozas(execParam, ugyiratId, SztornoIndoka);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        /// <summary>
        /// �gyirat sztorn�z�sa
        /// </summary>
        public static void Sztornozas(String ugyiratId, Page page
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            Sztornozas(ugyiratId, String.Empty, page, errorPanel, errorUpdatePanel);
        }


        /// <summary>
        /// K�lcs�nz�s kiad�s
        /// </summary>        
        public static void KolcsonzesKiadasKozpontiIrattarbol(string ugyiratId, Page page
             , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(ugyiratId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "IrattarKolcsonzesKiadasa"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {
                EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                ExecParam execParam = UI.SetExecParamDefault(page);
                execParam.Record_Id = ugyiratId;

                Result result = service.KolcsonzesKiadas(execParam);
                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }


        /// <summary>
        /// �gyirat �tv�tele
        /// </summary>
        public static void Atvetel(String ugyiratId, Page page
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(ugyiratId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Atvetel(execParam, ugyiratId);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        /// <summary>
        /// �gyirat �tv�tele �gyint�z�sre
        /// </summary>
        public static void AtvetelUgyintezesre(String ugyiratId, Page page
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(ugyiratId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratAtvetelUgyintezesre"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.AtvetelUgyintezesre(execParam, ugyiratId);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        public static void Visszakuldes(String ugyiratId, String visszakuldesIndoka, Page page
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(ugyiratId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {
                if (!String.IsNullOrEmpty(ugyiratId))
                {
                    EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                    ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                    Result result = service.VisszakuldesByObject(execParam, ugyiratId, Constants.TableNames.EREC_UgyUgyiratok, visszakuldesIndoka);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                        if (errorUpdatePanel != null)
                        {
                            errorUpdatePanel.Update();
                        }
                    }
                }
            }
        }

        public static void VisszakuldesIrattarbol(String ugyiratId, String visszakuldesIndoka, Page page
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(ugyiratId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "AtmenetiIrattarAtvetel")
                && !FunctionRights.GetFunkcioJog(page, "KozpontiIrattarAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {
                if (!String.IsNullOrEmpty(ugyiratId))
                {
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                    Result result = service.VisszakuldesIrattarbol(execParam, ugyiratId, visszakuldesIndoka);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                        if (errorUpdatePanel != null)
                        {
                            errorUpdatePanel.Update();
                        }
                    }
                }
            }
        }

        #region Funkci�gombok �ll�t�sa

        public static void SetSzignalasFunkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
            , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratSzignalas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }


            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.Szignalhato(ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("SzignalasForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }

        public static void SetAtvetelUgyintezesreFunkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
           , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratAtvetelUgyintezesre"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("AtvetelUgyintezesreForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }


        public static void SetSkontrobaHelyezesFunkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
          , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratSkontroKezeles")
                && !FunctionRights.GetFunkcioJog(page, "UgyiratSkontrobaHelyezes"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.SkontrobaHelyezheto(ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("SkontroinditasForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }

        public static void SetSzerelesFunkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
          , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratSzereles"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.SzerelhetoBeleUgyirat(ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("SzerelesForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }



        public static void SetCsatolasFunkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
          , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratCsatolasNew"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.Csatolhato(ugyiratStatusz, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("CsatolasForm.aspx"
                    , QueryStringVars.Id + "=" + ugyiratId + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromUgyirat
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }



        public static void SetLezarasFunkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
         , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratLezaras"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.Lezarhato(ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("LezarasForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }

        public static void SetAlszamraIktatas_Elokeszit_Funkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
         , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "IktatasElokeszites"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            //if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            //{
            //    imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //}

            imageButton.OnClientClick = JavaScripts.SetOnClientClick("IktatasElokeszitesForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New
                //+ "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);



        }

        public static void SetAlszamraIktatas_Belso_Funkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
         , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "BelsoIratIktatas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            //ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
            ErrorDetails errorDetail = null;
            ExecParam xpm = UI.SetExecParamDefault(page);

            if (Ugyiratok.LehetAlszamraIktatni(xpm, ugyiratStatusz, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                    + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }

        public static void SetAlszamraIktatas_Bejovo_Funkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
         , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "BejovoIratIktatas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            //ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
            ErrorDetails errorDetail = null;
            ExecParam xpm = UI.SetExecParamDefault(page);

            if (Ugyiratok.LehetAlszamraIktatni(xpm, ugyiratStatusz, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    + "&" + QueryStringVars.Mode + "=" + CommandName.BejovoIratIktatas
                    + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }
        #region CR3153 - BOPMH-ban az iktat�s visszajelz� k�perny�re kellene irat �tadas gomb is.
        public static void SetAtadasraKijelolesMulti_Funkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
         , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratAtadas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.AtadasraKijelolheto(ugyiratStatusz, execParam, out errorDetail))
            {
                page.Session[Constants.SelectedUgyiratIds] = ugyiratId;

                page.Session[Constants.SelectedBarcodeIds] = null;
                page.Session[Constants.SelectedKuldemenyIds] = null;
                //page.Session[Constants.SelectedIratPeldanyIds] = null;
                page.Session[Constants.SelectedDosszieIds] = null;

                imageButton.OnClientClick = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx?isUgyirat=true", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);

                //imageButton.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                //    , QueryStringVars.UgyiratId + "=" + ugyiratId
                //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }

        #endregion

        public static void SetAtadasraKijeloles_Funkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
         , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratAtadas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.AtadasraKijelolheto(ugyiratStatusz, execParam, out errorDetail))
            {
                page.Session[Constants.SelectedUgyiratIds] = ugyiratId;

                page.Session[Constants.SelectedBarcodeIds] = null;
                page.Session[Constants.SelectedKuldemenyIds] = null;
                page.Session[Constants.SelectedIratPeldanyIds] = null;
                page.Session[Constants.SelectedDosszieIds] = null;

                imageButton.OnClientClick = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);

                //imageButton.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                //    , QueryStringVars.UgyiratId + "=" + ugyiratId
                //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }



        public static void SetElintezetteNyilvanitas_Funkciogomb(String ugyiratId, Ugyiratok.Statusz ugyiratStatusz
        , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "UgyiratElintezes"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(ugyiratId) || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Ugyiratok.ElintezetteNyilvanithato(ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("UgyiratElintezettForm.aspx"
                    , QueryStringVars.UgyiratId + "=" + ugyiratId
                    , Defaults.PopupWidth, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }


        public static void SetKozpontiIrattarKolcsonzesFunkciogomb(String UgyiratId, Statusz statusz, ImageButton image, UpdatePanel UgyUgyiratokUpdatePanel)
        {

            if (!FunctionRights.GetFunkcioJog(image.Page, "KozpontiIrattarKolcsonzes"))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            image.ToolTip = Resources.Buttons.Kolcsonzes;

            if (String.IsNullOrEmpty(UgyiratId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                if (FunctionRights.GetFunkcioJog(image.Page, "KolcsonzesJovahagyas"))
                {
                    // "K�lcs�nz�s / K�lcs�nz�s j�v�hagy�sa" felirat:
                    image.ToolTip = Resources.Buttons.Kolcsonzes + " / " + Resources.Buttons.KolcsonzesJovahagyas;
                }
                return;
            }

            ErrorDetails errorDetail = null;

            if (Kolcsonozheto(statusz, out errorDetail))
            {
                image.OnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Id + "=" + UgyiratId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                if (FunctionRights.GetFunkcioJog(image.Page, "KolcsonzesJovahagyas"))
                {
                    image.OnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.KolcsonzesJovahagyas + "&" + QueryStringVars.Id + "=" + UgyiratId
                       , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                    // BUG#7024: K�lcs�nz�s j�v�hagy�sa felirat
                    image.ToolTip = Resources.Buttons.KolcsonzesJovahagyas;
                }
                else
                {
                    image.OnClientClick = "alert('" + Contentum.eUtility.Resources.Error.GetString("UINemKolcsonozhetoUgyirat")
                        + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page)
                        + "'); return false;";
                }
            }

        }

        public static void SetKozpontiIrattarKolcsonzesVisszaFunkciogomb(String UgyiratId, Statusz statusz, ImageButton image, UpdatePanel UgyUgyiratokUpdatePanel)
        {

            if (!FunctionRights.GetFunkcioJog(image.Page, "KozpontiIrattarKolcsonzes")
                && !FunctionRights.GetFunkcioJog(image.Page, "IrattarKolcsonzesKiadasa")) // CR#2036: IrattarKolcsonzesKiadasa joggal vissza lehet vonni m�s n�vre t�rt�nt kik�r�st is!
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(UgyiratId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
                || statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
            {
                image.OnClientClick = "if (confirm('Biztosan visszavonja a k�lcs�nz�si k�relmet?')) {} else {return false;}";
            }
            else
            {
                image.OnClientClick = "alert('Az �gyirat k�lcs�nz�se nem vonhat� vissza!'); return false;";
            }
        }

        public static void SetAtmenetiIrattarbolKikeresFunkciogomb(String UgyiratId, Statusz statusz, ImageButton image, UpdatePanel UgyUgyiratokUpdatePanel, ExecParam execParam)
        {

            if (!FunctionRights.GetFunkcioJog(image.Page, "AtmenetiIrattarKolcsonzes")
                && !FunctionRights.GetFunkcioJog(image.Page, "AtmenetiIrattarbolKiadas"))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");
                return;
            }

            if (String.IsNullOrEmpty(UgyiratId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (AtmenetiIrattarbanOrzott(statusz, execParam))
            {
                image.OnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.KikeresAtmenetiIrattarbol + "&" + QueryStringVars.Id + "=" + UgyiratId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            }
            else if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert)
            {
                image.ToolTip = Resources.Buttons.Kiadas;
                image.AlternateText = image.ToolTip;

                image.OnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.KiadasAtmenetiIrattarbol + "&" + QueryStringVars.Id + "=" + UgyiratId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            }
            else
            {
                image.OnClientClick = "alert('Az �gyirat nem k�rhet� ki!'); return false;";
            }
        }


        public static void SetAtmenetiIrattarKikeresVisszaFunkciogomb(String UgyiratId, Statusz statusz, ImageButton image, UpdatePanel UgyUgyiratokUpdatePanel)
        {

            if (!FunctionRights.GetFunkcioJog(image.Page, "AtmenetiIrattarKolcsonzes")
                && !FunctionRights.GetFunkcioJog(image.Page, "IrattarKolcsonzesKiadasa")) // CR#2036: IrattarKolcsonzesKiadasa joggal vissza lehet vonni m�s n�vre t�rt�nt kik�r�st is!
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(UgyiratId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert
                || statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo)
            {
                image.OnClientClick = "if (confirm('Biztosan visszavonja a kik�r�si k�relmet?')) {} else {return false;}";
            }
            else
            {
                image.OnClientClick = "alert('A kik�r�s nem vonhat� vissza!'); return false;";
            }
        }

        public static void SetSkontroIrattarKikeresVisszaFunkciogomb(String UgyiratId, Statusz statusz, ImageButton image, UpdatePanel UgyUgyiratokUpdatePanel)
        {

            if (!FunctionRights.GetFunkcioJog(image.Page, "KikeresSkontroIrattarbol"))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(UgyiratId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert)
            {
                image.OnClientClick = "if (confirm('Biztosan visszavonja a kik�r�si k�relmet?')) {} else {return false;}";
            }
            else
            {
                image.OnClientClick = "alert('A kik�r�s nem vonhat� vissza!'); return false;";
            }
        }

        #endregion

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_UgyUgyiratok, Id, null, null);
                return false;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

            //Statusz statusz = new Statusz(
            //    erec_UgyUgyirat.Id, erec_UgyUgyirat.Allapot, erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo,
            //    erec_UgyUgyirat.TovabbitasAlattAllapot, erec_UgyUgyirat.Csoport_Id_Felelos, erec_UgyUgyirat.Kovetkezo_Felelos_Id,
            //    erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez, erec_UgyUgyirat.RegirendszerIktatoszam,
            //    erec_UgyUgyirat.LezarasDat, erec_UgyUgyirat.IraIktatokonyv_Id, erec_UgyUgyirat.Foszam);
            Ugyiratok.Statusz statusz = GetAllapotByBusinessDocument(erec_UgyUgyirat);
            return CheckAtadasraKijelolhetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tadhat�?
            bool atadasraKijelolheto = false;

            if (Ugyiratok.AtadasraKijelolheto(statusz, UI.SetExecParamDefault(page), out errorDetail))
            {
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "UgyiratAtadas"))
                {
                    //"�nnek nincs joga �gyiratot �tadni."
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52866, Statusz.ColumnTypes.CsopId_Felelos);
                }
                else
                {
                    atadasraKijelolheto = true;
                }
            }

            return atadasraKijelolheto;
        }

        /// <summary>
        /// Ha nem jel�lhet� ki �tad�sra, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void UgyiratokGridView_RowDataBound_CheckAtadasraKijeloles(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);
                ErrorDetails errorDetail;
                bool atadasraKijelolheto = CheckAtadasraKijelolhetoWithFunctionRight(page, statusz, out errorDetail);

                UI.SetRowCheckboxAndInfo(atadasraKijelolheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        #region CR3206 - Iratt�ri strukt�ra
        /// <summary>
        /// Ha nincs iratt�rban, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void UgyiratokGridView_RowDataBound_CheckIrattarbanKijeloles(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // Iratt�rban van ellen�rz�s:
                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);
                ErrorDetails errorDetail;
                bool irattarban = CheckIrattarban(page, statusz, out errorDetail);

                UI.SetRowCheckboxAndInfo(irattarban, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }
        public static bool CheckIrattarban(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tadhat�?
            bool atadasraKijelolheto = false;

            List<string> irattarAllapotok = new List<string>()
            {
                KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
                KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert,
                KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo,
                KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott,
                KodTarak.UGYIRAT_ALLAPOT.Skontroban,
                KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert
            };

            if (irattarAllapotok.Contains(statusz.Allapot))
            {
                atadasraKijelolheto = true;
            }
            else
            {
                //Nem megfelel� �llapot
                errorDetail = statusz.CreateErrorDetail(Resources.Error.UIUgyiratNincsIrattarban, Statusz.ColumnTypes.Allapot);
            }

            return atadasraKijelolheto;
        }
        #endregion
        public static bool CheckIrattarbaVehetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_UgyUgyiratok, Id, null, null);
                return false;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

            Ugyiratok.Statusz statusz = GetAllapotByBusinessDocument(erec_UgyUgyirat);
            return CheckAtadasraKijelolhetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckIrattarbaVehetoWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tadhat�?
            bool atadasraKijelolheto = false;

            if (Ugyiratok.IrattarbaKuldheto(statusz, UI.SetExecParamDefault(page), out errorDetail))
            {
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "UgyiratAtadasIrattarba"))
                {
                    //"�nnek nincs joga �gyiratot �tadni."
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52866, Statusz.ColumnTypes.CsopId_Felelos);
                }
                else
                {
                    atadasraKijelolheto = true;
                }
            }

            return atadasraKijelolheto;
        }
        public static bool CheckAtvehetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_UgyUgyiratok, Id, null, null);
                return false;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

            //Statusz statusz = new Statusz(
            //    erec_UgyUgyirat.Id, erec_UgyUgyirat.Allapot, erec_UgyUgyirat.FelhasznaloCsoport_Id_Orzo,
            //    erec_UgyUgyirat.TovabbitasAlattAllapot, erec_UgyUgyirat.Csoport_Id_Felelos, erec_UgyUgyirat.Kovetkezo_Felelos_Id,
            //    erec_UgyUgyirat.FelhasznaloCsoport_Id_Ugyintez, erec_UgyUgyirat.RegirendszerIktatoszam,
            //    erec_UgyUgyirat.LezarasDat, erec_UgyUgyirat.IraIktatokonyv_Id, erec_UgyUgyirat.Foszam);
            Ugyiratok.Statusz statusz = GetAllapotByBusinessDocument(erec_UgyUgyirat);
            return CheckAtvehetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckAtvehetoUgyintezesreWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_UgyUgyiratok, Id, null, null);
                return false;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

            Ugyiratok.Statusz statusz = GetAllapotByBusinessDocument(erec_UgyUgyirat);
            return CheckAtvehetoUgyintezesreWithFunctionRight(page, statusz, out errorDetail);
        }

        /// <summary>
        /// T�K visszav�teln�l ellen�riz
        /// </summary>
        /// <returns></returns>
        public static bool CheckVisszavehetoTUKWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tvehet�?
            bool atveheto = true;

            if (Ugyiratok.Atveheto(statusz, UI.SetExecParamDefault(page), out errorDetail))
            {
                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                    {
                        atveheto = false;
                        // Ez az �gyirat m�r �nn�l van.
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52861, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszak�ld�s
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);

                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount == 0)
                        {
                            atveheto = false;
                            // Ez az �gyirat m�r �nn�l van.
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52861, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        }
                    }
                }
            }

            return atveheto;
        }

        public static bool CheckAtvehetoWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tvehet�?
            bool atveheto = false;

            if (Ugyiratok.Atveheto(statusz, UI.SetExecParamDefault(page), out errorDetail))
            {
                //Felel�s ellen�rz�shez kell
                List<string> FelettesSzervezetList = Csoportok.GetFelhasznaloFelettesCsoportjai(page, FelhasznaloProfil.FelhasznaloId(page));

                if (Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(page), FelettesSzervezetList, statusz.CsopId_Felelos))
                {
                    if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "UgyiratAtvetel"))
                    {
                        //"�nnek nincs joga �gyiratot �tvenni."
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52851, Statusz.ColumnTypes.CsopId_Felelos);
                    }
                    else
                    {
                        atveheto = true;
                    }
                }
                else
                {
                    //"�n nem tagja az �gyirat kezel� csoportj�nak."
                    // ErrorCode_52234	Az �gyirat nem vehet� �t, mert �n nem tagja az �gyirat felel�s csoportj�nak!
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52234, Statusz.ColumnTypes.CsopId_Felelos);
                }

                if (FelettesSzervezetList.Contains(statusz.CsopId_Felelos) && !Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "AtvetelSzervezettol"))
                {
                    atveheto = false;
                    //"�nnek nincs joga szervezetnek k�ld�tt �gyiratot �tvenni."
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52236, Statusz.ColumnTypes.FelhCsopId_Orzo);
                }

                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                    {
                        atveheto = false;
                        // Ez az �gyirat m�r �nn�l van.
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52861, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszak�ld�s
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);

                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount == 0)
                        {
                            atveheto = false;
                            // Ez az �gyirat m�r �nn�l van.
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52861, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        }
                    }
                }
            }

            return atveheto;
        }

        public static bool CheckAtvehetoUgyintezesreWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tvehet�?
            bool atveheto = false;

            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = statusz.Id;

            if (Ugyiratok.AtvehetoUgyintezesre(statusz, execParam, out errorDetail))
            {
                //Felel�s ellen�rz�shez kell
                List<string> FelettesSzervezetList = Csoportok.GetFelhasznaloFelettesCsoportjai(page, FelhasznaloProfil.FelhasznaloId(page));

                if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Elintezett || Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(page), FelettesSzervezetList, statusz.CsopId_Felelos)
                    || (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott && statusz.IsElektronikus)
                    )
                {
                    if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "UgyiratAtvetelUgyintezesre"))
                    {
                        //"�nnek nincs joga �gyiratot �tvenni."
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52851, Statusz.ColumnTypes.CsopId_Felelos);
                    }
                    else
                    {
                        atveheto = true;
                    }

                }
                else
                {
                    //"�nnek nincs joga �gyiratot �tvenni."
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52851, Statusz.ColumnTypes.CsopId_Felelos);
                }

            }

            return atveheto;
        }

        /// <summary>
        /// T�K visszav�teln�l ellen�rizz�k a grid elemeket
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        public static void UgyiratokGridView_RowDataBound_CheckVisszavehetoTUK(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tvehet�?

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);
                ErrorDetails errorDetail;
                bool visszaveheto = CheckVisszavehetoTUKWithFunctionRight(page, statusz, out errorDetail);

                UI.SetRowCheckboxAndInfo(visszaveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }


        public static void UgyiratokGridView_RowDataBound_CheckAtvetel(GridViewRowEventArgs e, Page page, List<string> FelettesSzervezetList)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tvehet�?

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                //if (Ugyiratok.Atveheto(statusz, UI.SetExecParamDefault(page, new ExecParam()), out errorDetail))
                //{
                //    if (Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(page), FelettesSzervezetList, statusz.CsopId_Felelos))
                //    {
                //        atveheto = true;
                //    }
                //    else
                //    {
                //        errorDetail = statusz.CreateErrorDetail("�n nem tagja az �gyirat kezel� csoportj�nak.", Statusz.ColumnTypes.CsopId_Felelos);
                //    }

                //    if (FelettesSzervezetList.Contains(statusz.CsopId_Felelos) && !Contentum.eRecord.Utility.FunctionRights.GetFunkcioJog(page, "AtvetelSzervezettol"))
                //    {
                //        atveheto = false;
                //        errorDetail = statusz.CreateErrorDetail("�nnek nincs joga szervezetnek k�ld�tt �gyiratot �tvenni.", Statusz.ColumnTypes.FelhCsopId_Orzo);
                //    }

                //    if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                //    {
                //        atveheto = false;
                //        errorDetail = statusz.CreateErrorDetail("Ez az �gyirat m�r �nn�l van.", Statusz.ColumnTypes.FelhCsopId_Orzo);
                //    }
                //}

                bool atveheto = CheckAtvehetoWithFunctionRight(page, statusz, out errorDetail);
                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }


        public static void UgyiratokGridView_RowDataBound_CheckAtvetelUgyintezesre(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tvehet�?

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                //bool atveheto = false;
                ErrorDetails errorDetail;

                bool atveheto = CheckAtvehetoUgyintezesreWithFunctionRight(page, statusz, out errorDetail);
                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        #region CheckVisszakuldheto
        public static bool CheckVisszakuldhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_UgyUgyiratok, Id, null, null);
                return false;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

            Ugyiratok.Statusz statusz = GetAllapotByBusinessDocument(erec_UgyUgyirat);

            return CheckVisszakuldhetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckVisszakuldhetoWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Visszak�ldhet�?
            bool visszakuldheto = Ugyiratok.Visszakuldheto(UI.SetExecParamDefault(page), statusz, out errorDetail);

            if (visszakuldheto)
            {
                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                    {
                        visszakuldheto = false;
                        // Ez az �gyirat m�r �nn�l van!
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52861, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszak�ld�s
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);

                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount > 0)
                        {
                            visszakuldheto = false;
                            // A t�tel nem k�ldhet� vissza, mert azt visszak�ldt�k �nnek, mint eredeti felad�nak!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53705, Statusz.ColumnTypes.CsopId_Felelos);

                        }
                    }

                }
                else
                {
                    if (statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                    {
                        if (statusz.Allapot == KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt)
                        {
                            visszakuldheto = false;
                            // A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53702, Statusz.ColumnTypes.CsopId_Felelos);
                        }
                    }
                }

                if (visszakuldheto)
                {
                    visszakuldheto = CheckAtvehetoWithFunctionRight(page, statusz, out errorDetail);
                    if (errorDetail != null)
                    {
                        // A t�tel nem k�ldhet� vissza, mert csak az �tvehet� t�telek visszak�ld�se megengedett!
                        errorDetail.Message = String.Format("{0}\\n{1}", Resources.Error.ErrorCode_53708, errorDetail.Message ?? "");
                    }
                }
            }

            return visszakuldheto;
        }
        #endregion CheckVisszakuldheto

        #region CheckVisszakuldhetoIrattarbol
        public static bool CheckVisszakuldhetoIrattarbolWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_UgyUgyiratok, Id, null, null);
                return false;
            }

            EREC_UgyUgyiratok erec_UgyUgyirat = (EREC_UgyUgyiratok)result.Record;

            Ugyiratok.Statusz statusz = GetAllapotByBusinessDocument(erec_UgyUgyirat);

            return CheckVisszakuldhetoIrattarbolWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckVisszakuldhetoIrattarbolWithFunctionRight(Page page, Ugyiratok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Visszak�ldhet�?
            ExecParam execParam = UI.SetExecParamDefault(page);
            bool visszakuldheto = Ugyiratok.AtmenetiIrattarbolVisszakuldheto(execParam, statusz, out errorDetail)
                || Ugyiratok.KozpontiIrattarbolVisszakuldheto(execParam, statusz, out errorDetail);

            if (visszakuldheto)
            {
                bool hasFunctionRightKozpontiIrattar = Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "KozpontiIrattarAtvetel");
                bool hasFunctionRightAtmenetiIrattar = Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "AtmenetiIrattarAtvetel");
                if (!hasFunctionRightKozpontiIrattar
                        && !hasFunctionRightAtmenetiIrattar)
                {
                    // �nnek nincs joga �gyiratot iratt�rba venni, vagy onnan visszak�ldeni!
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53751);
                    visszakuldheto = false;
                }
                else
                {
                    // K�zponti iratt�r
                    if (statusz.CsopId_Felelos == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id)
                    {
                        if (!hasFunctionRightKozpontiIrattar)
                        {
                            // �nnek nincs joga �gyiratot k�zponti iratt�rba venni, vagy onnan visszak�ldeni!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53752, Statusz.ColumnTypes.CsopId_Felelos);
                            visszakuldheto = false;
                        }
                    }
                    else
                    {
                        // �tmeneti iratt�r
                        if (!hasFunctionRightAtmenetiIrattar)
                        {
                            // �nnek nincs joga �gyiratot �tmeneti iratt�rba venni, vagy onnan visszak�ldeni!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53753, Statusz.ColumnTypes.CsopId_Felelos);
                            visszakuldheto = false;
                        }
                        else
                        {
                            Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                            Result result_irattarak = service_csoportok.GetAllKezelhetoIrattar(execParam);

                            if (result_irattarak.IsError)
                            {
                                // hiba:
                                errorDetail = statusz.CreateErrorDetail(ResultError.GetErrorMessageFromResultObject(result_irattarak));
                                visszakuldheto = false;
                            }
                            else
                            {
                                DataRow[] rows = result_irattarak.Ds.Tables[0].Select(String.Format("Id='{0}'", statusz.CsopId_Felelos));

                                if (rows.Length == 0)
                                {
                                    // Az �gyiratot nem k�ldheti vissza az iratt�rb�l, mert �n nem jogosult az iratt�r kezel�s�re!
                                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53756, Statusz.ColumnTypes.CsopId_Felelos);
                                    visszakuldheto = false;
                                }
                            }
                        }
                    }
                }
            }

            return visszakuldheto;
        }
        #endregion CheckVisszakuldhetoIrattarbol

        /// <summary>
        /// Ha nem vehet� iratt�rba az �gyirat, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void UgyiratokGridView_RowDataBound_CheckIrattarbaVetel(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tvehet� iratt�rba?

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;
                ExecParam execParam = UI.SetExecParamDefault(page);

                // VAGY �tmeneti iratt�rba vehet�, VAGY K�zponti iratt�rba vehet�
                bool atvehetoIrattarba = Ugyiratok.AtmenetiIrattarbaVeheto(statusz, execParam, out errorDetail)
                    || Ugyiratok.KozpontiIrattarbaVeheto(statusz, execParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(atvehetoIrattarba, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }


        /// <summary>
        /// Ha nem vehet� ki skontr�b�l, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void UgyiratokGridView_RowDataBound_CheckSkontrobolKiveheto(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // Skontr�b�l kivehet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool skontrobolKiveheto = Ugyiratok.SkontrobolKiveheto(statusz, ugyiratExecParam);

                // CheckBox lek�r�se:
                CheckBox checkBox = (CheckBox)e.Row.FindControl("check");

                if (!skontrobolKiveheto)
                {
                    e.Row.BackColor = System.Drawing.Color.Coral;
                    if (checkBox != null)
                    {
                        checkBox.Checked = false;
                        checkBox.Enabled = false;
                    }
                }
                else
                {
                    checkBox.Checked = true;
                    checkBox.Enabled = true;
                }
            }
        }

        public static Statusz GetAllapotFromDataRowView(DataRowView drv)
        {
            Ugyiratok.Statusz statusz = null;

            if (drv != null && drv.Row != null)
            {
                statusz = Ugyiratok.GetAllapotByDataRow(drv.Row);
            }

            return statusz;
        }


        public static void FillUgytipusDropDownByIrattariTetel(Page page, DropDownList dropDownList
            , string irattariTetel_Id, string selectedUgytipus, Contentum.eUIControls.eErrorPanel errorPanel)
        {
            dropDownList.Items.Clear();
            if (String.IsNullOrEmpty(irattariTetel_Id))
            {
                return;
            }

            // IratMetaDef t�bl�b�l az iratt�ri t�telhez tartoz� �gyt�pusok lek�r�se:

            EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

            ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

            EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

            search.Ugykor_Id.Filter(irattariTetel_Id);

            search.Ugytipus.NotNull();

            search.EljarasiSzakasz.IsNull();
            search.Irattipus.IsNull();

            #region �rv�nyess�g ellen�rz�s kikapcsol�sa
            search.ErvKezd.Clear();
            search.ErvVege.Clear();
            #endregion �rv�nyess�g ellen�rz�s kikapcsol�sa

            search.OrderBy = "UgytipusNev";

            Result result = service.GetAll(execParam, search);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                return;
            }

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                try
                {
                    string row_Ugytipus = row["Ugytipus"].ToString();
                    string row_UgytipusNev = row["UgytipusNev"].ToString();

                    #region �rv�nyess�g ellen�rz�s
                    DateTime ErvKezd;
                    DateTime ErvVege;
                    bool ervenyes = true;

                    if (DateTime.TryParse(row["ErvVege"].ToString(), out ErvVege) && ErvVege < DateTime.Now)
                    {
                        ervenyes = false;
                    }
                    else if (DateTime.TryParse(row["ErvKezd"].ToString(), out ErvKezd) && ErvKezd > DateTime.Now)
                    {
                        ervenyes = false;
                    }
                    #endregion �rv�nyess�g ellen�rz�s

                    if (ervenyes)
                    {
                        ListItem newListItem = new ListItem(row_UgytipusNev, row_Ugytipus);
                        newListItem.Attributes.Add("title", row_UgytipusNev);
                        dropDownList.Items.Add(newListItem);
                    }
                }
                catch
                {
                    // hiba, pl. nem stimmel az oszlopn�v
                    dropDownList.Items.Clear();
                    dropDownList.Items.Add(new ListItem(Resources.Error.UIDropDownListFillingError, ""));
                    break;
                }
            }

            ListItem selectedItem = dropDownList.Items.FindByValue(selectedUgytipus);
            if (selectedItem != null)
            {
                dropDownList.SelectedValue = selectedUgytipus;
                dropDownList.ToolTip = selectedItem.Text;
            }
            else
            {
                DataRow[] rows = result.Ds.Tables[0].Select("Ugytipus = '" + selectedUgytipus + "'");
                string selectedUgytipusNev = "";
                if (rows != null && rows.Length > 0)
                {
                    selectedUgytipusNev = rows[0]["UgytipusNev"].ToString();
                }
                else
                {
                    selectedUgytipusNev = selectedUgytipus;
                }
                string deletedValue = "[" + Resources.Form.UI_ErvenytelenitettTetel + "]";
                dropDownList.Items.Insert(0, new ListItem(selectedUgytipusNev + " " + deletedValue, selectedUgytipus));
                dropDownList.ToolTip = selectedUgytipusNev + " " + deletedValue;
            }

        }


        /// <summary>
        /// CR#1305: (FPH_TITKARSAGNAL) Ellen�rz�s: Ha BejovoIratIktatasCsakAlszamra vagy BelsoIratIktatasCsakAlszamra joga van, akkor 
        /// nem iktathat bizonyos �llapot� �gyiratokba ()
        /// </summary>        
        public static bool CheckAlszamraIktatasSkontroIrattar_Funkciojog(Page page, EREC_UgyUgyiratok erec_ugyugyiratok, string Mode)
        {
            if (erec_ugyugyiratok == null) { return false; }

            if ((FunctionRights.GetFunkcioJog(page, "BejovoIratIktatasCsakAlszamra") && Mode == CommandName.BejovoIratIktatas
                 || FunctionRights.GetFunkcioJog(page, "BelsoIratIktatasCsakAlszamra") && Mode == CommandName.BelsoIratIktatas)
                    && (erec_ugyugyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban
                        || erec_ugyugyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott
                        || erec_ugyugyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott
                        || erec_ugyugyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa
                        || erec_ugyugyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckKikeresAtmenetiIrattarbol(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();
                bool kikerheto = Ugyiratok.AtmenetiIrattarbolKikerheto(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(kikerheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckKikeresAtmenetiIrattarbolJovahagyas(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);
                ErrorDetails errorDetail = null;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool jovahagyhato = Ugyiratok.AtmenetiIrattarbolKikeresJovahagyhato(statusz, ugyiratExecParam, out errorDetail);
                UI.SetRowCheckboxAndInfo(jovahagyhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckKiadasAtmenetiIrattarbol(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail = null;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                // CR#2220: Az �llapotellen�rz�s a felhaszn�l� jogosults�g vizsg�lat�val b�v�lt:
                // csak a saj�t szervezet�hez rendelt �tmeneti iratt�rakb�l adhat ki,
                // a t�bbiben l�v� �gyiratokat csak l�thatja, ha egy�bk�nt jogosult
                //if (Ugyiratok.AtmenetiIrattarbolElkert(statusz, ugyiratExecParam, out errorDetail))
                bool kiadhato = Ugyiratok.AtmenetiIrattarbolKiadhato(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(kiadhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckKikeresSkontroIrattarbol(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool kikerheto = Ugyiratok.SkontroIrattarbolKikerheto(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(kikerheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckKiadasSkontroIrattarbol(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                bool kiadhato = Ugyiratok.SkontroIrattarbolElkert(statusz, ugyiratExecParam, out errorDetail);

                UI.SetRowCheckboxAndInfo(kiadhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckIrattarbaKuldheto(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);
                string teljellenor = null;
                bool isIrattarbaKuldheto = false;
                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();
                // BUG_8005
                Result res = new Result();
                if (statusz.Allapot != KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
                {
                    EREC_UgyUgyiratokService srv = eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    res = srv.Get(ugyiratExecParam);
                    res = srv.TeljessegEllenorzes(ugyiratExecParam.Clone(), drv["Id"].ToString(), res.Record as EREC_UgyUgyiratok, true);
                    if (res.IsError)
                    {
                        teljellenor = ResultError.GetErrorMessageFromResultObject(res);
                        errorDetail = new ErrorDetails();
                        errorDetail.ObjectId = drv["Id"].ToString();
                        errorDetail.ObjectType = eUtility.Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok;
                    }
                }
                if (Ugyiratok.AtadasraKijelolheto(statusz, ugyiratExecParam, out errorDetail) && Ugyiratok.IrattarbaKuldheto(statusz, ugyiratExecParam, out errorDetail))
                {
                    isIrattarbaKuldheto = true;
                }
                // CheckBox lek�r�se:
                CheckBox checkBox = (CheckBox)e.Row.FindControl("check");
                if (!isIrattarbaKuldheto || res.IsError)
                {
                    e.Row.BackColor = System.Drawing.Color.Coral;
                    if (checkBox != null)
                    {
                        checkBox.Checked = false;
                        checkBox.Enabled = false;
                    }

                    // Info Image �ll�t�sa, hogy mi�rt nem lehet �tadni
                    ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                    if (infoImageButton != null && (errorDetail != null || teljellenor != null))
                    {
                        infoImageButton.Visible = true;
                        string infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail, page);
                        infoImageButton.ToolTip = string.IsNullOrEmpty(infoMessage) ? teljellenor : infoMessage;
                        if (errorDetail != null)
                            infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                                + Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok + "','" + statusz.Id + "','"
                                + errorDetail.ObjectType + "','" + errorDetail.ObjectId + "'); return false;";
                    }

                }
                else
                {
                    // BLG#2366: A sikeress�g ellen�re lehet hogy van egy�b megjegyz�s is, amit meg kell jelen�teni, ezeket a webservice az ErrorDetails-be teszi:
                    if (res.ErrorDetail != null && !String.IsNullOrEmpty(res.ErrorDetail.Message))
                    {
                        // Info Image be�ll�t�sa a megjegyz�ssel:
                        ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                        if (infoImageButton != null)
                        {
                            infoImageButton.Visible = true;
                            infoImageButton.ToolTip = res.ErrorDetail.Message;
                            infoImageButton.OnClientClick = "alert('" + res.ErrorDetail.Message + "'); return false;";
                        }

                        if (checkBox != null)
                        {
                            checkBox.CssClass += " hasWarningMsg";
                        }
                    }

                    checkBox.Checked = true;
                    checkBox.Enabled = true;
                }
            }
        }


        public static void UgyiratokGridView_RowDataBound_CheckKozpontiIrattarbaKuldheto(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(drv);

                bool isAtadasraKijelolheto = false;
                bool isKozpontiIrattarbaKuldheto = false;
                ErrorDetails errorDetail;

                ExecParam ugyiratExecParam = UI.SetExecParamDefault(page, new ExecParam());
                ugyiratExecParam.Record_Id = drv["Id"].ToString();

                if (Ugyiratok.AtadasraKijelolheto(statusz, ugyiratExecParam, out errorDetail))
                {
                    isAtadasraKijelolheto = true;
                }

                if (isAtadasraKijelolheto && Ugyiratok.KozpontiIrattarbaKuldheto(statusz, ugyiratExecParam, out errorDetail))
                {
                    isKozpontiIrattarbaKuldheto = true;
                }

                UI.SetRowCheckboxAndInfo(isKozpontiIrattarbaKuldheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void UgyiratokGridView_RowDataBound_CheckTomegesOlvasasiJog(GridViewRowEventArgs e, Page page, List<string> jogosultUgyiratok)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);
                var errorDetails = new ErrorDetails { Message = "Az �gyirathoz nincs joga jogosultat felvenni!" };
                bool vegrehajthato = jogosultUgyiratok.Contains(statusz.Id);
                UI.SetRowCheckboxAndInfo(vegrehajthato, e, errorDetails, Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok, statusz.Id, page);
            }
        }

        public static void SetSearchObjectTo_Skontroban(EREC_UgyUgyiratokSearch search)
        {
            string defaultAllapotok = "'" + KodTarak.UGYIRAT_ALLAPOT.Skontroban + "','"
                                          + KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert + "'";

            search.Manual_Allapot_DefaultFilter.Value = defaultAllapotok;
            search.Manual_Allapot_DefaultFilter.Operator = Contentum.eQuery.Query.Operators.inner;
            search.Manual_Allapot_DefaultFilter.Group = "99";
            search.Manual_Allapot_DefaultFilter.GroupOperator = Query.Operators.or;

            // BUG_3842: skontr�ban ne jelenjenek meg a szereltek
            search.IsSzereltekExcluded = true;
            // BUG_11604: tov�bb�t�s alattiak kezel�se
            search.Manual_Sajat_TovabbitasAlattAllapot.Name = "EREC_UgyUgyiratok.Allapot = '50' and EREC_UgyUgyiratok.TovabbitasAlattAllapot";
            search.Manual_Sajat_TovabbitasAlattAllapot.Value = defaultAllapotok;
            search.Manual_Sajat_TovabbitasAlattAllapot.Operator = Contentum.eQuery.Query.Operators.inner;
            search.Manual_Sajat_TovabbitasAlattAllapot.Group = "99";
            search.Manual_Sajat_TovabbitasAlattAllapot.GroupOperator = Query.Operators.or;
        }
    }

    public class IratPeldanyok : Contentum.eRecord.BaseUtility.IratPeldanyok
    {
        public static bool CheckRights_Csatolmanyok(Page ParentPage, string id)
        {
            return CheckRights_Csatolmanyok(ParentPage, id, true);
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, string id, bool checkRightForObj)
        {
            EREC_PldIratPeldanyok erec_PldIratPeldanyok = null;

            if (!String.IsNullOrEmpty(id))
            {
                ExecParam execParam = UI.SetExecParamDefault(ParentPage);
                execParam.Record_Id = id;
                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                Result result = null;
                if (checkRightForObj)
                {
                    result = service.GetWithRightCheck(execParam, 'O');
                }
                else
                {
                    result = service.Get(execParam);
                }
                if (!result.IsError && result.Record != null)
                {
                    erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result.Record;

                    if (erec_PldIratPeldanyok != null)
                    {
                        return IratPeldanyok.CheckRights_Csatolmanyok(ParentPage, erec_PldIratPeldanyok);
                    }
                }
            }
            return false;
        }


        public static bool CheckRights_Csatolmanyok(Page ParentPage, EREC_PldIratPeldanyok erec_PldIratPeldanyok, bool checkRightForObj)
        {
            if (erec_PldIratPeldanyok != null)
            {
                if (checkRightForObj)
                {
                    return CheckRights_Csatolmanyok(ParentPage, erec_PldIratPeldanyok.Id);
                }
                else
                {
                    return CheckRights_Csatolmanyok(ParentPage, erec_PldIratPeldanyok);
                }
            }
            return false;
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, EREC_PldIratPeldanyok erec_PldIratPeldanyok)
        {
            EREC_IraIratok erec_IraIratok = null;
            if (erec_PldIratPeldanyok != null)
            {
                if (FelhasznaloProfil.IsAdminInSzervezet(ParentPage)) // BUG_8360
                {
                    return true;
                }

                // az irat �rz�j�t itt a p�ld�nyainak az �rz�i hat�rozz�k meg, hiszen nyilv�n l�thatja, akin�l van p�ld�nya
                // ha az aktu�lis felhaszn�l� az �rz�, akkor biztosan van joga a csatolm�nyokhoz, ak�r bizalmas irat, ak�r nem
                if (erec_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo == FelhasznaloProfil.FelhasznaloId(ParentPage))
                {
                    return true;
                }
                else
                {
                    string irat_id = erec_PldIratPeldanyok.IraIrat_Id;

                    ExecParam execParam_irat = UI.SetExecParamDefault(ParentPage);
                    execParam_irat.Record_Id = irat_id;
                    EREC_IraIratokService service_irat = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    Result result_irat = service_irat.GetWithRightCheck(execParam_irat, 'O');
                    if (!result_irat.IsError && result_irat.Record != null)
                    {
                        erec_IraIratok = (EREC_IraIratok)result_irat.Record;

                        if (erec_IraIratok != null)
                        {
                            return Iratok.CheckRights_Csatolmanyok(ParentPage, erec_IraIratok);
                        }
                    }
                }
            }

            return false;
        }

        public static bool Set4ObjectsByIratPeldanyId(String iratPeldany_Id
            , out EREC_PldIratPeldanyok erec_PldIratPeldanyok, out EREC_IraIratok erec_IraIratok
            , out EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, out EREC_UgyUgyiratok erec_UgyUgyiratok
            , Page page, Contentum.eUIControls.eErrorPanel errorPanel)
        {
            erec_PldIratPeldanyok = null;
            erec_IraIratok = null;
            erec_UgyUgyiratdarabok = null;
            erec_UgyUgyiratok = null;
            try
            {
                EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam_pld = UI.SetExecParamDefault(page, new ExecParam());
                execParam_pld.Record_Id = iratPeldany_Id;

                Result result_pldHier = service_pld.GetIratPeldanyHierarchia(execParam_pld);

                if (!String.IsNullOrEmpty(result_pldHier.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_pldHier);
                    return false;
                }

                IratPeldanyHierarchia iratPeldanyHier = (IratPeldanyHierarchia)result_pldHier.Record;

                erec_PldIratPeldanyok = iratPeldanyHier.IratPeldanyObj;
                erec_IraIratok = iratPeldanyHier.IratObj;
                erec_UgyUgyiratdarabok = iratPeldanyHier.UgyiratDarabObj;
                erec_UgyUgyiratok = iratPeldanyHier.UgyiratObj;

                return true;


                //EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                //ExecParam execParam_pld = UI.SetExecParamDefault(page, new ExecParam());
                //execParam_pld.Record_Id = iratPeldany_Id;

                //Result result_pld = service_pld.Get(execParam_pld);
                //if (!String.IsNullOrEmpty(result_pld.ErrorCode))
                //{
                //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_pld);
                //    return false;
                //}
                //else
                //{
                //    erec_PldIratPeldanyok = (EREC_PldIratPeldanyok)result_pld.Record;

                //    EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                //    ExecParam execParam_irat = UI.SetExecParamDefault(page, new ExecParam());
                //    execParam_irat.Record_Id = erec_PldIratPeldanyok.IraIrat_Id;

                //    Result result_irat = service_iratok.Get(execParam_irat);
                //    if (!String.IsNullOrEmpty(result_irat.ErrorCode))
                //    {
                //        ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_irat);
                //        return false;
                //    }
                //    else
                //    {
                //        erec_IraIratok = (EREC_IraIratok)result_irat.Record;

                //        EREC_UgyUgyiratdarabokService service_ugyiratdarab = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                //        ExecParam execParam_ugyiratDarab = UI.SetExecParamDefault(page, new ExecParam());
                //        execParam_ugyiratDarab.Record_Id = erec_IraIratok.UgyUgyIratDarab_Id;

                //        Result result_ugyiratDarab = service_ugyiratdarab.Get(execParam_ugyiratDarab);
                //        if (!String.IsNullOrEmpty(result_ugyiratDarab.ErrorCode))
                //        {
                //            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_ugyiratDarab);
                //            return false;
                //        }
                //        else
                //        {
                //            erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result_ugyiratDarab.Record;

                //            EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                //            ExecParam execParam_ugyirat = UI.SetExecParamDefault(page, new ExecParam());
                //            execParam_ugyirat.Record_Id = erec_UgyUgyiratdarabok.UgyUgyirat_Id;

                //            Result result_ugyirat = service_ugyiratok.Get(execParam_ugyirat);
                //            if (!String.IsNullOrEmpty(result_ugyirat.ErrorCode))
                //            {
                //                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_ugyirat);
                //                return false;
                //            }
                //            else
                //            {
                //                erec_UgyUgyiratok = (EREC_UgyUgyiratok) result_ugyirat.Record;

                //                return true;
                //            }
                //        }
                //    }
                //}                
            }
            catch
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel,
                    ResultError.CreateNewResultWithErrorCode(50101));
                return false;
            }

        }


        /// <summary>
        /// A keres�si objektum be�ll�t�sa a kezelhet� iratp�ld�nyok keres�s�hez
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_Kezelhetok(EREC_PldIratPeldanyokSearch searchObject)
        {
            // Kezelhet�: NEM Sztorn�zott �S NEM C�mzett_�tvette
            searchObject.Manual_Kezelhetok.Value =
                "'" + KodTarak.IRATPELDANY_ALLAPOT.Sztornozott
                + "','" + KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette
                + "','" + KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at
                + "'";
            searchObject.Manual_Kezelhetok.Operator = Query.Operators.notinner;

        }

        /// <summary>
        /// A keres�si objektum be�ll�t�sa a j�v�hagyand� iratp�ld�nyok keres�s�hez
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_Jovahagyandok(EREC_PldIratPeldanyokSearch searchObject, Page page)
        {
            searchObject.Manual_Jovahagyandok.Value =
                "'" + KodTarak.IRATPELDANY_ALLAPOT.Jovahagyas_alatt
                + "'";
            searchObject.Manual_Jovahagyandok.Operator = Query.Operators.inner;

            searchObject.Csoport_Id_Felelos.Filter(Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(page)));

            searchObject.ReadableWhere = Resources.Search.ReadableWhere_Iratpeldany_Jovahagyandok;
        }


        //CR 2121
        public static void Sztornozas(String iratPeldanyId, String SztornoIndoka, Page page
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(iratPeldanyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "IratPeldanySztorno"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Sztornozas(execParam, iratPeldanyId, SztornoIndoka, false, String.Empty);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        /// BLG_2948 - LZS - Sztornozas kieg�sz�t�se a 'SztornoMegsemmisitve' �s a 'SztornoMegsemmisitesDatuma' param�terrel.
        public static void Sztornozas(String iratPeldanyId, String SztornoIndoka, bool? SztornoMegsemmisitve, string SztornoMegsemmisitesDatuma, Page page
         , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(iratPeldanyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "IratPeldanySztorno"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Sztornozas(execParam, iratPeldanyId, SztornoIndoka, SztornoMegsemmisitve, SztornoMegsemmisitesDatuma);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        public static void Sztornozas(String iratPeldanyId, Page page
           , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            Sztornozas(iratPeldanyId, String.Empty, page, errorPanel, errorUpdatePanel);
        }
        #region CR3153 - BOPMH-ban az iktat�s visszajelz� k�perny�re kellene irat �tadas gomb is.
        public static void SetAtadasraKijelolesMulti_Funkciogomb(String iratPeldanyId, IratPeldanyok.Statusz iratPldStatusz
          , Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "IratPeldanyAtadas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(iratPeldanyId) || iratPldStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (IratPeldanyok.AtadasraKijelolheto(iratPldStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                page.Session[Constants.SelectedIratPeldanyIds] = iratPeldanyId;

                page.Session[Constants.SelectedBarcodeIds] = null;
                page.Session[Constants.SelectedKuldemenyIds] = null;
                // page.Session[Constants.SelectedUgyiratIds] = null;
                page.Session[Constants.SelectedDosszieIds] = null;

                imageButton.OnClientClick = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx?isUgyirat=false", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshIratPeldanyPanel).TrimEnd("return false;".ToCharArray());

                //imageButton.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                //    , QueryStringVars.IratPeldanyId + "=" + iratPeldanyId
                //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshIratPeldanyPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }


        #endregion

        public static void SetAtadasraKijeloles_Funkciogomb(String iratPeldanyId, IratPeldanyok.Statusz iratPldStatusz
          , Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "IratPeldanyAtadas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(iratPeldanyId) || iratPldStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (IratPeldanyok.AtadasraKijelolheto(iratPldStatusz, iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                page.Session[Constants.SelectedIratPeldanyIds] = iratPeldanyId;

                page.Session[Constants.SelectedBarcodeIds] = null;
                page.Session[Constants.SelectedKuldemenyIds] = null;
                page.Session[Constants.SelectedUgyiratIds] = null;
                page.Session[Constants.SelectedDosszieIds] = null;

                imageButton.OnClientClick = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshIratPeldanyPanel).TrimEnd("return false;".ToCharArray());

                //imageButton.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                //    , QueryStringVars.IratPeldanyId + "=" + iratPeldanyId
                //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refreshIratPeldanyPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }


        /// <summary>
        /// Expedi�l�s funkci�gomb be�ll�t�sa
        /// </summary>        
        public static void SetExpedialas_Funkciogomb(string iratPeldanyId, IratPeldanyok.Statusz iratPldStatusz, bool iratKiadmanyozando
            , Iratok.Statusz iratStatusz, UgyiratDarabok.Statusz ugyiratDarabStatusz, Ugyiratok.Statusz ugyiratStatusz
            , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "Expedialas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(iratPeldanyId) || iratPldStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (IratPeldanyok.Expedialhato(iratPldStatusz, iratKiadmanyozando, iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("ExpedialasForm.aspx"
                   , QueryStringVars.IratPeldanyId + "=" + iratPeldanyId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refreshIratPeldanyPanel);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }

        }



        public static void SetIratPeldanyLetrehozasa_ImageButton(string iratId, string iratPeldanyId, ImageButton imageButton)
        {
            /// Funkci�jog ellen�rz�s:
            /// 
            if (!FunctionRights.GetFunkcioJog(imageButton.Page, "IratPeldanyNew"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
            // BUG_8852
            if (UtilityCsatolmanyok.HasKrCsatolmany(imageButton.Page, iratId))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
            else imageButton.Enabled = true;
            imageButton.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IratPeldanyId + "=" + iratPeldanyId
            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch(false);

            search.Id.Filter(Id);

            Result result = service.GetAllWithExtension(execParam, search);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_PldIratPeldanyok, Id, null, null);
                return false;
            }
            else if (result.GetCount == 0)
            {
                // iratp�ld�ny lek�rdez�se sikertelen
                errorDetail = new ErrorDetails(Resources.Error.ErrorCode_51104, Constants.TableNames.EREC_PldIratPeldanyok, Id, null, null);
                return false;
            }

            DataRow row = result.Ds.Tables[0].Rows[0];

            IratPeldanyok.Statusz statusz_pld = IratPeldanyok.GetAllapotByDataRow(row);

            Ugyiratok.Statusz statusz_ugyirat = IratPeldanyok.GetUgyiratAllapotByDataRow(row);

            return CheckAtadasraKijelolhetoWithFunctionRight(page, statusz_pld, statusz_ugyirat, out errorDetail);
        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, IratPeldanyok.Statusz statusz_pld, Ugyiratok.Statusz statusz_ugyirat, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tadhat�?
            bool atadasraKijelolheto = false;

            //Iratok.Statusz statusz_irat = Iratok.GetAllapotById(statusz_pld.Irat_Id, UI.SetExecParamDefault(page), null);
            // mivel az AtadasraKijelolheto vizsg�lat�ban val�j�ban nem haszn�ljuk fel, nem is k�rj�k le
            Iratok.Statusz statusz_irat = null;

            if (IratPeldanyok.AtadasraKijelolheto(statusz_pld, statusz_irat, statusz_ugyirat, UI.SetExecParamDefault(page), out errorDetail))
            {
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "IratPeldanyAtadas"))
                {
                    //"�nnek nincs joga iratp�ld�nyt �tadni."
                    errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52868, Statusz.ColumnTypes.CsopId_Felelos);
                }
                else
                {
                    atadasraKijelolheto = true;
                }
            }

            return atadasraKijelolheto;
        }



        /// <summary>
        /// Ha nem jel�lhet� ki �tad�sra, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void IratPeldanyokGridView_RowDataBound_CheckAtadasraKijeloles(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
                IratPeldanyok.Statusz pld_statusz;
                Iratok.Statusz irat_statusz;
                UgyiratDarabok.Statusz ugyiratDarab_statusz;
                Ugyiratok.Statusz ugyirat_statusz;

                IratPeldanyok.SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(drv
                    , out pld_statusz, out irat_statusz, out ugyiratDarab_statusz, out ugyirat_statusz);

                string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

                // �tad�sra kijel�lhet�s�g ellen�rz�se:     

                ErrorDetails errorDetail;

                bool atadasraKijelolheto = IratPeldanyok.AtadasraKijelolheto(pld_statusz, irat_statusz, ugyirat_statusz,
                    UI.SetExecParamDefault(page, new ExecParam()), out errorDetail);

                UI.SetRowCheckboxAndInfo(atadasraKijelolheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
            }
        }

        #region BLG1880 - expedi�lt �llapot� t�teleknek lehet csak t�meges ragsz�mot osztani
        /// <summary>
        /// Ha nem jel�lhet� ki �tad�sra, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void IratPeldanyokGridView_RowDataBound_CheckExpedialt(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
                IratPeldanyok.Statusz pld_statusz;
                Iratok.Statusz irat_statusz;
                UgyiratDarabok.Statusz ugyiratDarab_statusz;
                Ugyiratok.Statusz ugyirat_statusz;

                IratPeldanyok.SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(drv
                    , out pld_statusz, out irat_statusz, out ugyiratDarab_statusz, out ugyirat_statusz);

                string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

                // Expedi�lt �llapot ellen�rz�se:     

                bool Expedialt = false;
                ErrorDetails errorDetail = null;

                if (pld_statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Expedialt)
                {
                    Expedialt = true;
                    // ha nem a felhaszn�l� az �rz�:
                    ExecParam execParam = UI.SetExecParamDefault(page);
                    if (pld_statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                    {
                        errorDetail = pld_statusz.CreateErrorDetail("Az iratp�ld�nynak nem �n az �rz�je.", Statusz.ColumnTypes.FelhCsopId_Orzo);
                        Expedialt = false;
                    }
                }
                else
                {
                    errorDetail = pld_statusz.CreateErrorDetail("Az iratp�ld�ny �llapota nem megfelel�.", Statusz.ColumnTypes.Allapot);
                }

                UI.SetRowCheckboxAndInfo(Expedialt, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
            }
        }
        #endregion

        #region CR3152 - Expedi�l�s folyamat t�meges�t�se
        /// <summary>
        /// Ha nem jel�lhet� ki �tad�sra, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <param name="kiadmanyozando"></param>
        /// <returns></returns>
        public static void IratPeldanyokGridView_RowDataBound_CheckExpedialasKijeloles(GridViewRowEventArgs e, Page page, bool kiadmanyozando, EREC_PldIratPeldanyok peldany, EREC_IraIratok irat, string iratId, bool isOneKikuldhetoCsatolmany)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;


                // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
                IratPeldanyok.Statusz pld_statusz;
                Iratok.Statusz irat_statusz;
                UgyiratDarabok.Statusz ugyiratDarab_statusz;
                Ugyiratok.Statusz ugyirat_statusz;

                IratPeldanyok.SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(drv
                    , out pld_statusz, out irat_statusz, out ugyiratDarab_statusz, out ugyirat_statusz);


                string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

                bool expedialhato = false;
                ErrorDetails errorDetail;

                if (IratPeldanyok.TomegesenExpedialhato(peldany, kiadmanyozando, irat_statusz, ugyirat_statusz,
                    UI.SetExecParamDefault(page, new ExecParam()), out errorDetail))
                {
                    expedialhato = true;
                }

                //if (peldany.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu && String.IsNullOrEmpty(iratId))
                //{
                //    errorDetail = new ErrorDetails();
                //    errorDetail.Message = "Hivatali kapun kereszt�l egyszerre csak egy irat p�ld�nyai expedi�lhat�ak!";
                //    expedialhato = false;
                //}

                if (peldany.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu && !isOneKikuldhetoCsatolmany)
                {
                    errorDetail = new ErrorDetails();
                    errorDetail.Message = "A kik�ldend� csatolm�ny nem hat�rozhat� meg!";
                    expedialhato = false;
                }

                UI.SetRowCheckboxAndInfo(expedialhato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
            }
        }
        #endregion

        public static bool CheckAtvehetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch(false);

            search.Id.Filter(Id);

            Result result = service.GetAllWithExtension(execParam, search);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_PldIratPeldanyok, Id, null, null);
                return false;
            }
            else if (result.GetCount == 0)
            {
                // iratp�ld�ny lek�rdez�se sikertelen
                errorDetail = new ErrorDetails(Resources.Error.ErrorCode_51104, Constants.TableNames.EREC_PldIratPeldanyok, Id, null, null);
                return false;
            }

            DataRow row = result.Ds.Tables[0].Rows[0];

            IratPeldanyok.Statusz statusz_pld = IratPeldanyok.GetAllapotByDataRow(row);

            Ugyiratok.Statusz statusz_ugyirat = IratPeldanyok.GetUgyiratAllapotByDataRow(row);

            return CheckAtvehetoWithFunctionRight(page, statusz_pld, statusz_ugyirat, out errorDetail);
        }

        /// <summary>
        /// T�K visszav�teln�l csak azt ellen�rizz�k, hogy a T�K kezel�n�l van -e m�r az iratp�ld�ny
        /// </summary>
        /// <param name="page"></param>
        /// <param name="statusz_pld"></param>
        /// <param name="statusz_ugyirat"></param>
        /// <param name="errorDetail"></param>
        /// <returns></returns>
        public static bool CheckVisszavehetoTUKWithFunctionRight(Page page, IratPeldanyok.Statusz statusz_pld, Ugyiratok.Statusz statusz_ugyirat, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tvehet�?
            bool atveheto = true;

            if (Atveheto(statusz_pld, statusz_ugyirat, UI.SetExecParamDefault(page), out errorDetail))
            {
                if (statusz_pld.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz_pld.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {

                    if (statusz_pld.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                    {
                        atveheto = false;
                        // Ez az iratp�ld�ny m�r �nn�l van.
                        errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52863, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszak�ld�s
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);

                        search_kezbtetelek.Obj_Id.Filter(statusz_pld.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount == 0)
                        {
                            atveheto = false;
                            // Ez az iratp�ld�ny m�r �nn�l van.
                            errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52863, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        }
                    }
                }
            }

            return atveheto;
        }

        public static bool CheckAtvehetoWithFunctionRight(Page page, IratPeldanyok.Statusz statusz_pld, Ugyiratok.Statusz statusz_ugyirat, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tvehet�?
            bool atveheto = false;

            if (Atveheto(statusz_pld, statusz_ugyirat, UI.SetExecParamDefault(page), out errorDetail))
            {
                //Felel�s ellen�rz�shez kell
                List<string> FelettesSzervezetList = Csoportok.GetFelhasznaloFelettesCsoportjai(page, FelhasznaloProfil.FelhasznaloId(page));

                if (Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(page), FelettesSzervezetList, statusz_pld.CsopId_Felelos))
                {
                    if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "IratPeldanyAtvetel"))
                    {
                        //"�nnek nincs joga iratp�ld�nyt �tvenni."
                        errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52853, Statusz.ColumnTypes.CsopId_Felelos);
                    }
                    else
                    {
                        atveheto = true;
                    }
                }
                else
                {
                    // "�n nem tagja az iratp�ld�ny kezel� csoportj�nak."
                    // ErrorCode_52294	Az iratp�ld�ny nem vehet� �t, mert �n nem tagja az iratp�ld�ny felel�s csoportj�nak!
                    errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52294, Statusz.ColumnTypes.CsopId_Felelos);
                }

                if (FelettesSzervezetList.Contains(statusz_pld.CsopId_Felelos) && !Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "AtvetelSzervezettol"))
                {
                    atveheto = false;
                    //"�nnek nincs joga szervezetnek k�ld�tt iratp�ld�nyt �tvenni.",
                    errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52296, Statusz.ColumnTypes.FelhCsopId_Orzo);
                }

                if (statusz_pld.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz_pld.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {

                    if (statusz_pld.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                    {
                        atveheto = false;
                        // Ez az iratp�ld�ny m�r �nn�l van.
                        errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52863, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszak�ld�s
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);

                        search_kezbtetelek.Obj_Id.Filter(statusz_pld.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount == 0)
                        {
                            atveheto = false;
                            // Ez az iratp�ld�ny m�r �nn�l van.
                            errorDetail = statusz_pld.CreateErrorDetail(Resources.Error.ErrorCode_52863, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        }
                    }
                }
            }

            return atveheto;
        }

        /// <summary>
        /// T�K vsszav�tel eset�n egy sz�kebb ellen�rz�s
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        public static void IratPeldanyokGridView_RowDataBound_CheckTUKVisszavetel(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
                IratPeldanyok.Statusz pld_statusz;
                Iratok.Statusz irat_statusz;
                UgyiratDarabok.Statusz ugyiratDarab_statusz;
                Ugyiratok.Statusz ugyirat_statusz;

                IratPeldanyok.SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(drv
                    , out pld_statusz, out irat_statusz, out ugyiratDarab_statusz, out ugyirat_statusz);

                //string Pld_Csoport_Id_Felelos = (pld_statusz != null) ? pld_statusz.CsopId_Felelos : "";
                string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

                // �tvehet�?

                ErrorDetails errorDetail;

                bool atveheto = CheckVisszavehetoTUKWithFunctionRight(page, pld_statusz, ugyirat_statusz, out errorDetail);

                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
            }
        }

        public static void IratPeldanyokGridView_RowDataBound_CheckAtvetel(GridViewRowEventArgs e, Page page, List<string> FelettesSzervezetList)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
                IratPeldanyok.Statusz pld_statusz;
                Iratok.Statusz irat_statusz;
                UgyiratDarabok.Statusz ugyiratDarab_statusz;
                Ugyiratok.Statusz ugyirat_statusz;

                IratPeldanyok.SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(drv
                    , out pld_statusz, out irat_statusz, out ugyiratDarab_statusz, out ugyirat_statusz);

                //string Pld_Csoport_Id_Felelos = (pld_statusz != null) ? pld_statusz.CsopId_Felelos : "";
                string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

                // �tvehet�?

                //bool atveheto = false;
                ErrorDetails errorDetail;

                //if (IratPeldanyok.Atveheto(pld_statusz, ugyirat_statusz,UI.SetExecParamDefault(page, new ExecParam()), out errorDetail))
                //{
                //    if (Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(page), FelettesSzervezetList, Pld_Csoport_Id_Felelos))
                //    {
                //        atveheto = true;
                //    }
                //    else
                //    {
                //        errorDetail = pld_statusz.CreateErrorDetail("�n nem tagja az iratp�ld�ny kezel� csoportj�nak.", Statusz.ColumnTypes.CsopId_Felelos);
                //    }

                //    if (FelettesSzervezetList.Contains(Pld_Csoport_Id_Felelos) && !Contentum.eRecord.Utility.FunctionRights.GetFunkcioJog(page, "AtvetelSzervezettol"))
                //    {
                //        atveheto = false;
                //        errorDetail = pld_statusz.CreateErrorDetail("�nnek nincs joga szervezetnek k�ld�tt iratp�ld�nyt �tvenni.", Statusz.ColumnTypes.FelhCsopId_Orzo);
                //    }

                //    if (pld_statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && pld_statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                //    {
                //        atveheto = false;
                //        errorDetail = pld_statusz.CreateErrorDetail("Ez az iratp�ld�ny m�r �nn�l van.", Statusz.ColumnTypes.FelhCsopId_Orzo);
                //    }
                //}

                bool atveheto = CheckAtvehetoWithFunctionRight(page, pld_statusz, ugyirat_statusz, out errorDetail);

                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
            }
        }

        #region CheckVisszakuldheto
        public static bool CheckVisszakuldhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_PldIratPeldanyok, Id, null, null);
                return false;
            }

            EREC_PldIratPeldanyok erec_PldIratPeldany = (EREC_PldIratPeldanyok)result.Record;

            IratPeldanyok.Statusz statusz = GetAllapotByBusinessDocument(erec_PldIratPeldany);

            return CheckVisszakuldhetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckVisszakuldhetoWithFunctionRight(Page page, IratPeldanyok.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Visszak�ldhet�?
            bool visszakuldheto = IratPeldanyok.Visszakuldheto(UI.SetExecParamDefault(page), statusz, out errorDetail);

            if (visszakuldheto)
            {
                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                    {
                        visszakuldheto = false;
                        // Ez az iratp�ld�ny m�r �nn�l van!
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52863, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszak�ld�s
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);

                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount > 0)
                        {
                            visszakuldheto = false;
                            // A t�tel nem k�ldhet� vissza, mert azt visszak�ldt�k �nnek, mint eredeti felad�nak!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53705, Statusz.ColumnTypes.CsopId_Felelos);

                        }
                    }

                }
                else
                {
                    if (statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                    {
                        if (statusz.Allapot == KodTarak.IRATPELDANY_ALLAPOT.Tovabbitas_alatt)
                        {
                            visszakuldheto = false;
                            // A t�tel nem k�ldhet� vissza, mert azt �n adta �t �s csak a c�mzett k�ldheti vissza �nnek!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53702, Statusz.ColumnTypes.CsopId_Felelos);
                        }
                    }
                }

                if (visszakuldheto)
                {
                    visszakuldheto = CheckAtvehetoWithFunctionRight(page, statusz.Id, out errorDetail);
                    if (errorDetail != null)
                    {
                        // A t�tel nem k�ldhet� vissza, mert csak az �tvehet� t�telek visszak�ld�se megengedett!
                        errorDetail.Message = String.Format("{0}\\n{1}", Resources.Error.ErrorCode_53708, errorDetail.Message ?? "");
                    }
                }
            }

            return visszakuldheto;
        }
        #endregion CheckVisszakuldheto


        public static void IratPeldanyokGridView_RowDataBound_CheckUgyiratbaHelyezes(GridViewRowEventArgs e, Page page)
        {

            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
                IratPeldanyok.Statusz pld_statusz;
                Iratok.Statusz irat_statusz;
                UgyiratDarabok.Statusz ugyiratDarab_statusz;
                Ugyiratok.Statusz ugyirat_statusz;

                IratPeldanyok.SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(drv
                    , out pld_statusz, out irat_statusz, out ugyiratDarab_statusz, out ugyirat_statusz);

                string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

                ErrorDetails errorDetail;

                bool ugyiratbaHelyezheto = IratPeldanyok.UgyiratbaHelyezheto(pld_statusz, irat_statusz, ugyirat_statusz,
                    UI.SetExecParamDefault(page, new ExecParam()), out errorDetail);

                UI.SetRowCheckboxAndInfo(ugyiratbaHelyezheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
            }
        }


        /// <summary>
        /// Iratp�ld�ny GetAllWithExtension eredm�ny�nek egy sor�b�l �ssze�ll�tja a st�tuszokat
        /// Iratp�ld�ny GridView RowDataBound-n�l haszn�lhat�
        /// </summary>
        private static void SetStatusFromIratpeldanyGetAllWithExtensionDataRowView(DataRowView drv
            , out IratPeldanyok.Statusz pld_statusz, out Iratok.Statusz irat_statusz, out UgyiratDarabok.Statusz ugyiratDarab_statusz, out Ugyiratok.Statusz ugyirat_statusz)
        {

            #region IratP�ld�ny statusz

            if (drv != null && drv.Row != null)
            {
                pld_statusz = IratPeldanyok.GetAllapotByDataRow(drv.Row);
            }
            else
            {
                pld_statusz = null;
            }

            #endregion

            #region Irat statusz

            if (drv != null && drv.Row != null)
            {
                irat_statusz = IratPeldanyok.GetIratAllapotByDataRow(drv.Row);
            }
            else
            {
                irat_statusz = null;
            }

            #endregion

            #region �gyiratDarab statusz

            string UgyiratDarab_Allapot = "";
            if (drv != null && drv["EREC_UgyUgyiratdarabok_Allapot"] != null)
            {
                UgyiratDarab_Allapot = drv["EREC_UgyUgyiratdarabok_Allapot"].ToString();
            }

            string UgyiratDarab_Ugyirat_Id = "";
            if (drv != null && drv["EREC_UgyUgyiratdarabok_UgyUgyirat_Id"] != null)
            {
                UgyiratDarab_Ugyirat_Id = drv["EREC_UgyUgyiratdarabok_UgyUgyirat_Id"].ToString();
            }

            string Irat_UgyiratDarab_Id = "";
            if (drv != null && drv["EREC_IraIratok_UgyUgyIratDarab_Id"] != null)
            {
                Irat_UgyiratDarab_Id = drv["EREC_IraIratok_UgyUgyIratDarab_Id"].ToString();
            }

            ugyiratDarab_statusz = new Contentum.eRecord.BaseUtility.UgyiratDarabok.Statusz(
                Irat_UgyiratDarab_Id, UgyiratDarab_Allapot, UgyiratDarab_Ugyirat_Id);
            #endregion

            #region �gyirat statusz

            if (drv != null && drv.Row != null)
            {
                ugyirat_statusz = IratPeldanyok.GetUgyiratAllapotByDataRow(drv.Row);
            }
            else
            {
                ugyirat_statusz = null;
            }

            #endregion
        }


        public static void Atvetel(String IratPeldanyId, Page page
          , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(IratPeldanyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "IratPeldanyAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Atvetel(execParam, IratPeldanyId);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        public static void Visszakuldes(String iratPeldanyId, String visszakuldesIndoka, Page page
            , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(iratPeldanyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "IratPeldanyAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {
                if (!String.IsNullOrEmpty(iratPeldanyId))
                {
                    EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                    ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                    execParam.Record_Id = iratPeldanyId;

                    Result result = service.VisszakuldesByObject(execParam, iratPeldanyId, Constants.TableNames.EREC_PldIratPeldanyok, visszakuldesIndoka);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                        if (errorUpdatePanel != null)
                        {
                            errorUpdatePanel.Update();
                        }
                    }
                }
            }
        }

        public static void Postazas(String IratPeldanyId, Page page
       , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(IratPeldanyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "Postazas"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Postazas(execParam, IratPeldanyId);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        //// TODO:
        //public static Statusz GetAllapotFromDataRowView(DataRowView drv)
        //{

        //}

    }

    public class UgyiratDarabok : Contentum.eRecord.BaseUtility.UgyiratDarabok
    {
        public static string GetFullFoszam(String Ugyiratdarab_Id, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok = null;

            EREC_UgyUgyiratdarabokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
            execParam.Record_Id = Ugyiratdarab_Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result.Record;

                return GetFullFoszam(erec_UgyUgyiratdarabok, ParentPage, EErrorPanel, ErrorUpdatePanel);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                if (ErrorUpdatePanel != null)
                {
                    ErrorUpdatePanel.Update();
                }
                return "hiba!";
            }
        }

        public static string GetFullFoszam(EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, Page ParentPage, Contentum.eUIControls.eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            if (erec_UgyUgyiratdarabok != null)
            {
                EREC_IraIktatoKonyvekService IktatoKonyvekservice = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam IktatoKonyvekexecParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                IktatoKonyvekexecParam.Record_Id = erec_UgyUgyiratdarabok.IraIktatokonyv_Id;

                Result IktatoKonyvekresult = IktatoKonyvekservice.Get(IktatoKonyvekexecParam);
                if (String.IsNullOrEmpty(IktatoKonyvekresult.ErrorCode))
                {
                    EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)IktatoKonyvekresult.Record;
                    if (erec_IraIktatoKonyvek != null)
                    {
                        //return erec_IraIktatoKonyvek.MegkulJelzes + " /" + erec_UgyUgyiratdarabok.Foszam + " /" + erec_IraIktatoKonyvek.Ev;

                        return UgyiratDarabok.GetFullFoszam(erec_UgyUgyiratdarabok, erec_IraIktatoKonyvek);
                    }
                    else
                    {
                        return "hiba!";
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, IktatoKonyvekresult);
                    if (ErrorUpdatePanel != null)
                    {
                        ErrorUpdatePanel.Update();
                    }
                    return "hiba!";
                }
            }
            else
            {
                return "hiba!";
            }
        }


        public static Statusz GetAllapotFromDataRow(DataRow ugyiratDarabRow)
        {
            return new Statusz(ugyiratDarabRow["Id"].ToString(), ugyiratDarabRow["Allapot"].ToString(), ugyiratDarabRow["UgyUgyirat_Id"].ToString());
        }

    }

    public class Iratok : Contentum.eRecord.BaseUtility.Iratok
    {
        /// <summary>
        /// Megmondja, hogy egy�b be�ll�t�sok (rendszerparam�terekben felsorolt k�d�rt�kek) szerint egy adott irat a min�s�t�se bizalmasnak sz�m�t-e
        /// (ekkor pl. csatolm�nyait csak az �rz�je �s annak vezet�je l�thatja)
        /// </summary>
        /// <param name="ParentPage"></param>
        /// <param name="IraMinositesValue">A konkr�t irat min�s�t�se (IRAT_MINOSITES k�dcsoport k�dt�rbeli k�d)</param>
        /// <returns></returns>
        public static bool IsConfidential(Page ParentPage, string IratMinositesValue)
        {
            bool isConfidential = false;

            // BLG_577: ha az IRAT_MINOSITES_BIZALMAS rendszerparam�ter "MIND"-re van �ll�tva, akkor minden irat bizalmasnak tekintend�:
            if (MindenIratBizalmas(ParentPage))
            {
                return true;
            }

            if (!String.IsNullOrEmpty(IratMinositesValue))
            {
                string confidential = Rendszerparameterek.Get(ParentPage, Rendszerparameterek.IRAT_MINOSITES_BIZALMAS);

                if (!String.IsNullOrEmpty(confidential))
                {
                    string[] confidentialArray = confidential.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    int i = 0;
                    while (!isConfidential && i < confidentialArray.Length)
                    {
                        isConfidential |= (IratMinositesValue == confidentialArray[i]);
                        i++;
                    }
                }
            }
            return isConfidential;
        }

        /// <summary>
        /// Az "IRAT_MINOSITES_BIZALMAS" rendszerparam�ter be�ll�t�sa szerint minden irat bizalmasnak tekinthet�-e
        /// </summary>
        /// <param name="parentPage"></param>
        /// <returns></returns>
        public static bool MindenIratBizalmas(Page parentPage)
        {
            string confidential = Rendszerparameterek.Get(parentPage, Rendszerparameterek.IRAT_MINOSITES_BIZALMAS);

            if (!String.IsNullOrEmpty(confidential))
            {
                if (confidential == KodTarak.IRAT_MINOSITES.Mind
                    || confidential.ToUpper() == "MIND")
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, string id)
        {
            return CheckRights_Csatolmanyok(ParentPage, id, true);
        }



        public static bool CheckRights_Csatolmanyok(Page ParentPage, string id, bool checkRightForObj)
        {
            EREC_IraIratok erec_IraIratok = null;

            if (!String.IsNullOrEmpty(id))
            {
                ExecParam execParam = UI.SetExecParamDefault(ParentPage);
                execParam.Record_Id = id;
                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                Result result = null;
                if (checkRightForObj)
                {
                    result = service.GetWithRightCheck(execParam, 'O');
                }
                else
                {
                    result = service.Get(execParam);
                }
                if (!result.IsError && result.Record != null)
                {
                    erec_IraIratok = (EREC_IraIratok)result.Record;

                    if (erec_IraIratok != null)
                    {
                        return CheckRights_Csatolmanyok(ParentPage, erec_IraIratok);
                    }
                }
            }
            return false;
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, EREC_IraIratok erec_IraIratok, bool checkRightForObj)
        {
            if (erec_IraIratok != null)
            {
                if (checkRightForObj)
                {
                    return CheckRights_Csatolmanyok(ParentPage, erec_IraIratok.Id);
                }
                else
                {
                    return CheckRights_Csatolmanyok(ParentPage, erec_IraIratok);
                }
            }
            return false;
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, EREC_IraIratok erec_IraIratok)
        {
            if (erec_IraIratok != null)
            {
                if (FelhasznaloProfil.IsAdminInSzervezet(ParentPage)) // BUG_8360
                {
                    return true;
                }

                // iratmin�s�t�s ellen�rz�se
                if (!IsConfidential(ParentPage, erec_IraIratok.Minosites)) //(erec_IraIratok.Minosites != KodTarak.IRAT_MINOSITES.BelsoNemNyilvanos)
                {
                    return true;
                }

                // �rz� vizsg�lata az iratp�ld�nyok alapj�n
                // bizalmas irat csatolm�nyait a felhaszn�l� akkor l�thatja, ha l�tezik olyan iratp�ld�ny,
                // melynek �rz�je vagy az �rz� vezet�je a felhaszn�l�
                string Felhasznalo_Id = FelhasznaloProfil.FelhasznaloId(ParentPage);
                List<string> listSajatCsoportDolgozok = new List<string>();

                // aktu�lis felhaszn�l�
                listSajatCsoportDolgozok.Add(Felhasznalo_Id);

                // aktu�lis csoport dolgoz�i, ha az aktu�lis felhaszn�l� vezet�, szervezet asszisztense
                if (FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(ParentPage) == KodTarak.CsoprttagsagTipus.vezeto
                 || FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(ParentPage) == KodTarak.CsoprttagsagTipus.SzervezetAsszisztense) /*<-- BLG_577*/
                {
                    string FelhasznaloSzervezet_Id = FelhasznaloProfil.FelhasznaloSzerverzetId(ParentPage);
                    bool isFelelosSzervezet = true;
                    if (Rendszerparameterek.GetBoolean(ParentPage, "JOGOSULTSAG_FELELOS_SZERVEZET_ALAPJAN", false))
                    {

                        if (!String.IsNullOrEmpty(erec_IraIratok.Csoport_Id_Ugyfelelos))
                        {
                            if (!FelhasznaloSzervezet_Id.Equals(erec_IraIratok.Csoport_Id_Ugyfelelos, StringComparison.InvariantCultureIgnoreCase))
                                isFelelosSzervezet = false;
                        }
                        else
                        {
                            EREC_UgyUgyiratokService ugyiratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                            ExecParam xpmUgyiratok = UI.SetExecParamDefault(ParentPage);
                            xpmUgyiratok.Record_Id = erec_IraIratok.Ugyirat_Id;

                            Result resUgyirat = ugyiratokService.Get(xpmUgyiratok);

                            if (!resUgyirat.IsError)
                            {
                                EREC_UgyUgyiratok erec_UgyUgyiratok = resUgyirat.Record as EREC_UgyUgyiratok;

                                if (!String.IsNullOrEmpty(erec_UgyUgyiratok.Csoport_Id_Ugyfelelos))
                                {
                                    if (!FelhasznaloSzervezet_Id.Equals(erec_UgyUgyiratok.Csoport_Id_Ugyfelelos, StringComparison.InvariantCultureIgnoreCase))
                                        isFelelosSzervezet = false;
                                }
                            }
                        }
                    }

                    if (isFelelosSzervezet)
                    {
                        ExecParam execParam_csoport = UI.SetExecParamDefault(ParentPage);
                        Contentum.eAdmin.Service.KRT_CsoportokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                        KRT_CsoportokSearch krt_CsoportokSearch = new KRT_CsoportokSearch();
                        krt_CsoportokSearch.Tipus.Filter(KodTarak.CSOPORTTIPUS.Dolgozo);

                        Result result_csoport = service.GetAllSubCsoport(execParam_csoport, FelhasznaloSzervezet_Id, true, krt_CsoportokSearch);

                        if (!result_csoport.IsError)
                        {
                            foreach (DataRow row in result_csoport.Ds.Tables[0].Rows)
                            {
                                string csoport_id = row["Id"].ToString();
                                if (!String.IsNullOrEmpty(csoport_id) && !listSajatCsoportDolgozok.Contains(csoport_id))
                                {
                                    listSajatCsoportDolgozok.Add(csoport_id);
                                }
                            }
                        }
                    }
                }

                // �rz� ellen�rz�se
                ExecParam execParam_pld = UI.SetExecParamDefault(ParentPage);
                EREC_PldIratPeldanyokService service_pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                EREC_PldIratPeldanyokSearch search_pld = new EREC_PldIratPeldanyokSearch();
                search_pld.FelhasznaloCsoport_Id_Orzo.In(listSajatCsoportDolgozok);

                search_pld.IraIrat_Id.Filter(erec_IraIratok.Id);

                // el�g l�tez�st vizsg�lni, ez�rt csak az els�t k�rj�k le, ha van
                search_pld.TopRow = 1;

                Result result_pld = service_pld.GetAll(execParam_pld, search_pld);

                if (!result_pld.IsError && result_pld.GetCount == 1)
                {
                    return true;
                }

                // kezel� ellen�rz�se
                search_pld = new EREC_PldIratPeldanyokSearch();
                search_pld.Csoport_Id_Felelos.In(listSajatCsoportDolgozok);

                search_pld.IraIrat_Id.Filter(erec_IraIratok.Id);

                // el�g l�tez�st vizsg�lni, ez�rt csak az els�t k�rj�k le, ha van
                search_pld.TopRow = 1;

                result_pld = service_pld.GetAll(execParam_pld, search_pld);

                if (!result_pld.IsError && result_pld.GetCount == 1)
                {
                    return true;
                }

                List<string> lstObj_Ids = new List<string>();

                //// minden p�ld�ny lek�r�se objektumon kereszt�li jogosults�g ellen�rz�shez
                //ExecParam execParam_pld2 = UI.SetExecParamDefault(ParentPage);
                //EREC_PldIratPeldanyokSearch search_pld2 = new EREC_PldIratPeldanyokSearch();

                //search_pld2.IraIrat_Id.Filter(erec_IraIratok.Id);

                //Result result_pld2 = service_pld.GetAll(execParam_pld2, search_pld2);

                //if (!result_pld2.IsError)
                //{
                //        foreach (DataRow row in result_pld2.Ds.Tables[0].Rows)
                //        {
                //            lstObj_Ids.Add(row["Id"].ToString());
                //        }
                //}

                lstObj_Ids.Add(erec_IraIratok.Id);
                // BUG#4828: �gyirat jogosultakhoz felvettek is l�thass�k az irat csatolm�nyait:
                lstObj_Ids.Add(erec_IraIratok.Ugyirat_Id);    // �gyirat objektumon kereszt�li jogosults�g

                // objektumon kereszt�li jogosults�g ellen�rz�se az iratra
                Contentum.eAdmin.Service.KRT_JogosultakService service_jogosultak = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_JogosultakService();
                ExecParam execParam_jogosultak = UI.SetExecParamDefault(ParentPage);
                KRT_JogosultakSearch search_jogosultak = new KRT_JogosultakSearch();
                search_jogosultak.Obj_Id.In(lstObj_Ids);

                search_jogosultak.Csoport_Id_Jogalany.Filter(Felhasznalo_Id);

                search_jogosultak.TopRow = 1;

                Result result_jogosultak = service_jogosultak.GetAll(execParam_jogosultak, search_jogosultak);
                if (!result_jogosultak.IsError && result_jogosultak.GetCount == 1)
                {
                    return true;
                }


            }
            return false;
        }

        public static void SetIratPeldanyLetrehozasa_ImageButton(String IratId, ImageButton imageButton)
        {
            //imageButton.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IratId + "=" + IratId
            //, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);            
            // BUG_8852
            if (UtilityCsatolmanyok.HasKrCsatolmany(imageButton.Page, IratId))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
            else imageButton.Enabled = true;

            imageButton.OnClientClick = JavaScripts.SetOnClientClick("PldIratPeldanyokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IratId + "=" + IratId
            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refresh);
        }



        public static void SetAtIktatasFunkciogomb(String iratId, Iratok.Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz
           , ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "AtIktatas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(iratId) || iratStatusz == null || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Iratok.AtIktathato(iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.AtIktatas
                          + "&" + QueryStringVars.IratId + "=" + iratId
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refresh);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }

        public static void SetFelszabaditasFunkciogomb(string iratId, Statusz iratStatusz, Ugyiratok.Statusz ugyiratStatusz, ImageButton imageButton)
        {
            Page page = imageButton.Page;

            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "IratFelszabaditas"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(iratId) || iratStatusz == null || ugyiratStatusz == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Iratok.Felszabadithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("FelszabaditasForm.aspx"
                , QueryStringVars.IratId + "=" + iratId
                , Defaults.PopupWidth_Max, Defaults.PopupHeight, imageButton.ClientID, EventArgumentConst.refresh);
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }

        public static void SetMunkaIratBeiktatasFunkciogomb(string iratId, EREC_IraIratok obj_Irat, ImageButton imageButton)
        {
            imageButton.ToolTip = Resources.Buttons.Munkapeldany_Iktatas;
            Page page = imageButton.Page;

            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "MunkapeldanyBeiktatas_Bejovo")
                && !FunctionRights.GetFunkcioJog(page, "MunkapeldanyBeiktatas_Belso"))
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);

                return;
            }

            if (String.IsNullOrEmpty(iratId) || obj_Irat == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            // ha munkaanyag, �s beiktathat�:
            if (Iratok.Munkaanyag(obj_Irat) &&
                (obj_Irat.Allapot == KodTarak.IRAT_ALLAPOT.Megnyitott_Munkairat
                 || obj_Irat.Allapot == KodTarak.IRAT_ALLAPOT.Iktatando_Munkairat))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.ElokeszitettIratIktatas
                          + "&" + QueryStringVars.Id + "=" + iratId
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refresh);

                imageButton.Visible = true;
            }
            else
            {
                imageButton.Enabled = false;
                //UI.SetImageButtonStyleToDisabled(imageButton);
                return;
            }
        }


        public static Statusz GetAllapotFromDataRow(DataRow iratRow)
        {
            return new Statusz(iratRow["Id"].ToString()
                , iratRow["Allapot"].ToString()
                , iratRow["FelhasznaloCsoport_Id_Orzo"].ToString()
                , iratRow["UgyUgyIratDarab_Id"].ToString()
                , iratRow["Ugyirat_Id"].ToString()
                , iratRow["FelhasznaloCsoport_Id_Iktato"].ToString()
                , iratRow["ElsoIratPeldanyOrzo_Id"].ToString()
                , iratRow["ElsoIratPeldanyAllapot"].ToString()
                , iratRow["Alszam"].ToString());
        }

        #region Elint�zett�Nyilv�n�t�s
        public static bool CheckElintezhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_IraIratok, Id, null, null);
                return false;
            }

            EREC_IraIratok obj_EREC_IraIratok = result.Record as EREC_IraIratok;
            Statusz statusz = GetAllapotByBusinessDocument(obj_EREC_IraIratok);

            return CheckElintezhetoWithFunctionRight(page, statusz, obj_EREC_IraIratok, out errorDetail);
        }

        public static bool CheckElintezhetoWithFunctionRight(Page page, Statusz statusz, EREC_IraIratok obj_EREC_Iratok, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Elint�zhet�?
            bool elintezheto = false;

            if (Elintezheto(statusz, out errorDetail))
            {
                //Jogosults�gkezel�s
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "IratElintezes"))
                {
                    //""
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52240);
                }
                else
                {
                    elintezheto = true;
                }
                elintezheto = true;
            }

            return elintezheto;
        }

        //public static void Elintezes(String Erec_IraIratokId, Page page
        //  , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        //{
        //    if (String.IsNullOrEmpty(Erec_IraIratokId) || page == null || errorPanel == null)
        //    {
        //        return;
        //    }

        //    //Funkci�jogosults�g-ellen�rz�s:
        //    if (!FunctionRights.GetFunkcioJog(page, "IratElintezes"))
        //    {
        //        UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
        //        return;
        //    }
        //    else
        //    {

        //        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        //        ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

        //        Result result = service.Elintezes(execParam, Erec_IraIratokId, new EREC_HataridosFeladatok());
        //        if (!String.IsNullOrEmpty(result.ErrorCode))
        //        {
        //            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
        //            if (errorUpdatePanel != null)
        //            {
        //                errorUpdatePanel.Update();
        //            }
        //        }
        //    }
        //}
        #endregion
        #region Elint�zett�Nyilv�n�t�sVisszavon�sa
        public static bool CheckElintezhetoVisszavonWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_IraIratok, Id, null, null);
                return false;
            }

            EREC_IraIratok obj_EREC_IraIratok = result.Record as EREC_IraIratok;
            Statusz statusz = GetAllapotByBusinessDocument(obj_EREC_IraIratok);

            return CheckElintezhetoVisszavonWithFunctionRight(page, statusz, obj_EREC_IraIratok, out errorDetail);
        }

        public static bool CheckElintezhetoVisszavonWithFunctionRight(Page page, Statusz statusz, EREC_IraIratok obj_EREC_Iratok, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Elint�zhet�?
            bool elintezheto = false;

            if (ElintezesVisszavonhato(statusz, out errorDetail))
            {
                //Jogosults�gkezel�s
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "IratElintezesVisszavonas"))
                {
                    //""
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52240);
                }
                else
                {
                    elintezheto = true;
                }
                elintezheto = true;
            }

            return elintezheto;
        }

        public static void ElintezesVisszavon(String Erec_IraIratokId, Page page
          , Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(Erec_IraIratokId) || page == null || errorPanel == null)
            {
                return;
            }

            //Funkci�jogosults�g-ellen�rz�s:
            if (!FunctionRights.GetFunkcioJog(page, "IratElintezesVisszavonas"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.ElintezesVisszavonasa(execParam, Erec_IraIratokId);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }
        #endregion


        public static bool GetObjectsHierarchyByIrat(ExecParam execParam, string irat_Id
            , out EREC_IraIratok erec_IraIratok, out EREC_UgyUgyiratdarabok erec_UgyUgyiratdarabok, out EREC_UgyUgyiratok erec_UgyUgyiratok
            , out EREC_PldIratPeldanyok elsoIratPeldany
            , Contentum.eUIControls.eErrorPanel errorPanel)
        {
            erec_IraIratok = null;
            erec_UgyUgyiratdarabok = null;
            erec_UgyUgyiratok = null;
            elsoIratPeldany = null;

            EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam_irat = execParam.Clone();
            execParam_irat.Record_Id = irat_Id;

            Result result_iratHierarchia = service_iratok.GetIratHierarchia(execParam_irat);
            if (!String.IsNullOrEmpty(result_iratHierarchia.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_iratHierarchia);
                return false;
            }

            IratHierarchia iratHierarchia = (IratHierarchia)result_iratHierarchia.Record;

            erec_IraIratok = iratHierarchia.IratObj;
            erec_UgyUgyiratdarabok = iratHierarchia.UgyiratDarabObj;
            erec_UgyUgyiratok = iratHierarchia.UgyiratObj;

            #region Els� iratp�ld�ny Get:

            EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

            Result result_elsoPldGet = service_Pld.GetElsoIratPeldanyByIraIrat(execParam.Clone(), irat_Id);
            if (result_elsoPldGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_elsoPldGet);
                return false;
            }

            elsoIratPeldany = (EREC_PldIratPeldanyok)result_elsoPldGet.Record;

            #endregion


            //#region Irat Get
            //EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            //ExecParam execParam_irat = execParam.Clone();
            //execParam_irat.Record_Id = irat_Id;

            //Result result_irat = service_iratok.Get(execParam_irat);
            //if (!String.IsNullOrEmpty(result_irat.ErrorCode))
            //{
            //    // hiba:
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_irat);
            //    return false;
            //}

            //erec_IraIratok = (EREC_IraIratok)result_irat.Record;
            //#endregion

            //#region �gyiratdarab Get
            //EREC_UgyUgyiratdarabokService service_ugyiratdarab = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
            //ExecParam execParam_ugyiratDarab = execParam.Clone();
            //execParam_ugyiratDarab.Record_Id = erec_IraIratok.UgyUgyIratDarab_Id;

            //Result result_ugyiratDarab = service_ugyiratdarab.Get(execParam_ugyiratDarab);
            //if (!String.IsNullOrEmpty(result_ugyiratDarab.ErrorCode))
            //{
            //    // hiba:
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_ugyiratDarab);
            //    return false;
            //}

            //erec_UgyUgyiratdarabok = (EREC_UgyUgyiratdarabok)result_ugyiratDarab.Record;
            //#endregion

            //#region �gyirat Get
            //EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            //ExecParam execParam_ugyirat = execParam.Clone();
            //execParam_ugyirat.Record_Id = erec_UgyUgyiratdarabok.UgyUgyirat_Id;

            //Result result_ugyirat = service_ugyiratok.Get(execParam_ugyirat);
            //if (!String.IsNullOrEmpty(result_ugyirat.ErrorCode))
            //{
            //    // hiba:
            //    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result_ugyirat);
            //    return false;
            //}

            //erec_UgyUgyiratok = (EREC_UgyUgyiratok)result_ugyirat.Record;
            //#endregion

            return true;

        }


        //public static void Felszabaditas(string iratId, Page page, Contentum.eUIControls.eErrorPanel EErrorPanel1, UpdatePanel ErrorUpdatePanel)
        //{
        //    // Funkci�jog ellen�rz�s:
        //    if (!FunctionRights.GetFunkcioJog(page, "IratFelszabaditas"))
        //    {
        //        // nincs joga a m�velethez:
        //        UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        //        return;
        //    }
        //    else
        //    {
        //        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        //        ExecParam execParam = UI.SetExecParamDefault(page);

        //        Result result = service.Felszabaditas(execParam, iratId);
        //        if (result.IsError)
        //        {
        //            // hiba:
        //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //            if (ErrorUpdatePanel != null)
        //            {
        //                ErrorUpdatePanel.Update();
        //            }
        //        }
        //    }
        //}

        #region CR3175 - Kisherceg alkalmaz�sa
        private static string getIDFromDataRowView(DataRowView drv)
        {

            if (drv.Row.Table.Columns.Contains("Id"))
            {
                return drv.Row["Id"].ToString();
            }
            return null;
        }
        private static string getAlairasModFromDataRowView(DataRowView drv)
        {

            if (drv.Row.Table.Columns.Contains("AlairasMod"))
            {
                return drv.Row["AlairasMod"].ToString();
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>        
        public static string SetClientOnClickAlairas(string IratId, Page page)
        {
            string alairasVisszautasitasKezeles = Rendszerparameterek.Get(page, Rendszerparameterek.ALAIRAS_VISSZAUTASITAS_KEZELES);

            if (string.IsNullOrEmpty(IratId))
                return string.Format("alert('{0}'); return false;", Resources.List.UI_NoSelectedItem);

            ExecParam ep = UI.SetExecParamDefault(page, new ExecParam());
            EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();
            search.Obj_Id.Filter(IratId);

            EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            Result result = service.GetAll(ep, search);
            if (result.IsError || result.Ds == null)
            {
                return string.Format("alert('{0}'); return false;", "Hiba t�rt�nt az al��r�k lek�rdez�se k�zben!");
            }

            List<EREC_IratAlairok> resultList = new List<EREC_IratAlairok>();

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                var test = new EREC_IratAlairok();
                DataSetHelper.LoadUpdateBusinessDocumentFromDataRow(test, row);

                resultList.Add(test as EREC_IratAlairok);
            }

            List<EREC_IratAlairok> visszautasitottAlairok = resultList.Where(x => x.Allapot == KodTarak.IRATALAIRAS_ALLAPOT.Visszautasitott).OrderBy(x => x.AlairasSorrend).ToList();

            //Van al��r�s megtagad�s.
            if (visszautasitottAlairok.Any())
            {
                string kodtar_kod = result.Ds.Tables[0].Rows[0]["AlairoSzerep"].ToString();
                string strAlairoSzerep = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("ALAIRO_SZEREP", kodtar_kod, page);

                if (string.Equals(alairasVisszautasitasKezeles, "0"))
                {
                    return "alert('Figyelem! A csatolm�ny al��r�s�t a(z) " + strAlairoSzerep + " visszautas�totta, �gy ez az irat nem al��rhat�.'); return false;";
                }

                if (string.Equals(alairasVisszautasitasKezeles, "1"))
                {

                    return "if(confirm('Figyelem! A csatolm�ny al��r�s�t a(z) " + strAlairoSzerep + " visszautas�totta. Biztos, hogy al��rja az iratot?')) {} else { return false; }";
                }

                if (string.Equals(alairasVisszautasitasKezeles, "2"))
                {
                    if (visszautasitottAlairok.All(x => resultList.Any(r =>
                        r.Allapot == KodTarak.IRATALAIRAS_ALLAPOT.Alairt && r.AlairasSorrend == x.AlairasSorrend)))
                    {
                        return "if(confirm('Figyelem! A csatolm�ny al��r�s�t a(z) " + strAlairoSzerep + " visszautas�totta. Biztos, hogy al��rja az iratot?')) {} else { return false; }";
                    }
                    else
                    {
                        return "alert(''Figyelem! A csatolm�ny al��r�s�t a(z) " + strAlairoSzerep + " visszautas�totta, �gy ez az irat nem al��rhat�.'); return false;";
                    }
                }
            }

            // szerep visszakeres�s
            search = new EREC_IratAlairokSearch();
            search.FelhasznaloCsoport_Id_Alairo.Filter(ep.Felhasznalo_Id);
            search.FelhasznaloCsoport_Id_Alairo.OrGroup("alairo");
            search.FelhaszCsop_Id_HelyettesAlairo.Filter(ep.Felhasznalo_Id);
            search.FelhaszCsop_Id_HelyettesAlairo.OrGroup("alairo");
            search.Obj_Id.Filter(IratId);
            search.Obj_Id.GroupOperator = Query.Operators.and;
            search.AlairoSzerep.Filter(KodTarak.ALAIRO_SZEREP.Kiadmanyozo);
            search.TopRow = 1;
            result = service.GetAll(ep.Clone(), search);
            eUIControls.eErrorPanel eErrorPanel = new eUIControls.eErrorPanel();
            if (result.IsError || result.Ds == null)
            {
                return string.Format("alert('{0}'); return false;", "Hiba t�rt�nt az al��r�k lek�rdez�se k�zben!");
            }
            bool kiadmanyozo = result.GetCount > 0;
            if (kiadmanyozo)
            {
                ErrorDetails errorDetails = null;
                Iratok.Statusz statusz = Iratok.GetAllapotById(IratId, ep, eErrorPanel);
                if (!string.IsNullOrEmpty(eErrorPanel.ErrorMessage))
                {
                    return string.Format("alert('{0}'); return false;", eErrorPanel.ErrorMessage);
                }
                if (!Iratok.KiadmonyozoAlairhatja(statusz, ep, out errorDetails))
                    return string.Format("alert('{0}'); return false;", errorDetails.Message);
            }

            return string.Empty;

        }
        public static string SetClientOnClickAlairasVisszavonas(string IratId, Page page)
        {

            if (string.IsNullOrEmpty(IratId))
                return string.Format("alert('{0}'); return false;", Resources.List.UI_NoSelectedItem);

            ExecParam ep = UI.SetExecParamDefault(page, new ExecParam());
            EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            Result result = service.CheckAlairasVisszavonhato(ep, IratId);

            if (result.IsError)
            {
                return string.Format("alert('{0}'); return false;", ResultError.GetErrorMessageFromResultObject(result));
            }

            return string.Empty;

        }

        /// <summary>
        /// Ha Template-b�l van bet�ltve, megjel�li az adott sort �s ha ut�lagosan adminisztr�lt calendar-t jelen�t meg
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void AlairokGridView_RowDataBound_CheckTemplatesRowKijeloles(GridViewRowEventArgs e, Page page, string IratId)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                string id = getIDFromDataRowView(drv);
                string alairasMod = getAlairasModFromDataRowView(drv);
                //ErrorDetails errorDetail;
                // CheckBox lek�r�se:
                CheckBox checkBox = (CheckBox)e.Row.FindControl("check");

                if (string.IsNullOrEmpty(id))
                {
                    e.Row.BackColor = System.Drawing.Color.Coral;
                    if (checkBox != null)
                    {
                        checkBox.Checked = true;
                    }

                    if (alairasMod.Equals("M_UTO"))
                    {
                        TextBox datum = (TextBox)e.Row.FindControl("AlairasDatumaCalendar");
                        datum.Visible = true;
                        Label datumLabel = (Label)e.Row.FindControl("AlairasDatumaLabel");
                        datumLabel.Visible = false;
                        Image AlairasDatumaCalendarImageButton = (Image)e.Row.FindControl("AlairasDatumaCalendarImageButton");
                        AjaxControlToolkit.CalendarExtender AlairasDatumaCalendarExtender = (AjaxControlToolkit.CalendarExtender)e.Row.FindControl("AlairasDatumaCalendarExtender");
                        datum.ReadOnly = false;
                        AlairasDatumaCalendarImageButton.Visible = true;
                        AlairasDatumaCalendarExtender.Enabled = true;
                        AlairasDatumaCalendarExtender.SelectedDate = DateTime.Now;
                    }
                    else
                    {
                        TextBox datum = (TextBox)e.Row.FindControl("AlairasDatumaCalendar");
                        datum.Visible = false;
                        Label datumLabel = (Label)e.Row.FindControl("AlairasDatumaLabel");
                        datumLabel.Visible = true;
                    }
                    // Info Image �ll�t�sa, hogy template-b�l lett bet�ltve
                    ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                    if (infoImageButton != null)
                    {
                        infoImageButton.Visible = true;
                        string infoMessage = Resources.List.SablonbolBetoltveInfo;
                        infoImageButton.ToolTip = infoMessage;
                    }
                }
                else
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = true;
                }
            }
        }
        #endregion CR3175 - Kisherceg alkalmaz�sa

        #region CR3189 - Jogosultakon is legyen kisherceg
        /// <summary>
        /// Ha Template-b�l van bet�ltve, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void JogosultakGridView_RowDataBound_CheckTemplatesRowKijeloles(GridViewRowEventArgs e, Page page, string IratId)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                string id = getIDFromDataRowView(drv);
                //ErrorDetails errorDetail;
                // CheckBox lek�r�se:
                CheckBox checkBox = (CheckBox)e.Row.FindControl("check");

                if (string.IsNullOrEmpty(id))
                {
                    e.Row.BackColor = System.Drawing.Color.Coral;
                    if (checkBox != null)
                    {
                        checkBox.Checked = true;
                    }

                    // Info Image �ll�t�sa, hogy template-b�l lett bet�ltve
                    ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                    if (infoImageButton != null)
                    {
                        infoImageButton.Visible = true;
                        string infoMessage = Resources.List.SablonbolBetoltveInfo;
                        infoImageButton.ToolTip = infoMessage;
                    }
                }
                else
                {
                    checkBox.Checked = false;
                    checkBox.Enabled = true;
                }
            }
        }
        #endregion CR3189 - Jogosultakon is legyen kisherceg

        public class Hatarido
        {
            Page page;
            EREC_IraIratok irat;
            public Hatarido(Page page, EREC_IraIratok irat)
            {
                this.page = page;
                this.irat = irat;
            }
            public string GetHataridoIdoTartam()
            {
                string hataridoText = String.Empty;

                if (!irat.Typed.Hatarido.IsNull)
                {
                    DateTime hatarido = irat.Typed.Hatarido.Value;
                    DateTime ugyintezesKezdete = GetUgyintezesKezdete(irat);
                    string idoegyseg = irat.IntezesiIdoegyseg;

                    if (String.IsNullOrEmpty(idoegyseg))
                        idoegyseg = "1440"; //nap

                    int napokSzama = GetNapokSzama(ugyintezesKezdete, hatarido, idoegyseg);

                    string idoegysegNev = GetIdoegysegNev(idoegyseg);

                    hataridoText = String.Format("{0} {1}", napokSzama, idoegysegNev);
                }

                return hataridoText;
            }

            public int GetUzemzavarIdotartam()
            {
                if (!irat.Typed.UzemzavarKezdete.IsNull
                    && !irat.Typed.UzemzavarVege.IsNull)
                {
                    DateTime dtKezd = irat.Typed.UzemzavarKezdete.Value;
                    DateTime dtVege = irat.Typed.UzemzavarVege.Value;

                    return (dtVege - dtKezd).Days;
                }

                return 0;
            }

            public int GetHataridoMeghosszabitas()
            {
                int meghosszabitas = GetUzemzavarIdotartam();

                if (!irat.Typed.FelfuggesztettNapokSzama.IsNull)
                    meghosszabitas += irat.Typed.FelfuggesztettNapokSzama.Value;

                return meghosszabitas;
            }

            public string GetHataridoMeghosszabitasText()
            {
                return string.Format("{0} nap", GetHataridoMeghosszabitas());
            }

            public int GetHataridoTullepes()
            {
                if (irat.Typed.Hatarido.IsNull)
                {
                    return 0;
                }
                else
                {
                    DateTime hatarido = irat.Typed.Hatarido.Value;
                    int meghosszabitas = GetHataridoMeghosszabitas();
                    DateTime now = DateTime.Now;
                    int tullepes = (now - hatarido).Days;
                    tullepes -= meghosszabitas;

                    return Math.Max(0, tullepes);
                }
            }

            public string GetHataridoTullepessText()
            {
                return string.Format("{0} nap", GetHataridoTullepes());
            }

            DateTime GetUgyintezesKezdete(EREC_IraIratok irat)
            {
                //bej�v�
                if (irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                {
                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execParam = UI.SetExecParamDefault(page);
                    execParam.Record_Id = irat.KuldKuldemenyek_Id;

                    Result res = service.Get(execParam);

                    if (res.IsError)
                        throw new Contentum.eUtility.ResultException(res);

                    EREC_KuldKuldemenyek kuldemeny = res.Record as EREC_KuldKuldemenyek;

                    return kuldemeny.Typed.BeerkezesIdeje.Value;
                }
                else
                {
                    return irat.Base.Typed.LetrehozasIdo.Value;
                }
            }

            int GetNapokSzama(DateTime kezd, DateTime veg, string idoegyseg)
            {
                int napokSzama = 0;

                //munkanap
                if (idoegyseg == "-1440")
                {
                    Contentum.eAdmin.Service.KRT_Extra_NapokService extraNapokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();
                    ExecParam execParam = UI.SetExecParamDefault(page);

                    Result result = extraNapokService.MunkanapokSzama(execParam, kezd, veg);

                    if (result.IsError)
                    {
                        throw new Contentum.eUtility.ResultException(result);
                    }
                    else
                    {
                        napokSzama = (int)result.Record;
                    }
                }
                else
                {
                    napokSzama = (veg.Date - kezd.Date).Days;
                }

                return napokSzama;
            }

            string GetIdoegysegNev(string idoegyseg)
            {
                Dictionary<string, string> kodcsoport = KodTar_Cache.GetKodtarakByKodCsoport("IDOEGYSEG", page);

                if (kodcsoport.ContainsKey(idoegyseg))
                    return kodcsoport[idoegyseg];

                return idoegyseg;
            }
        }

        public static bool ValaszIratIktathato(EREC_IraIratok irat, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            if (irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
            {
                return true;
            }
            else
            {
                errorDetail = new ErrorDetails() { Message = "Csak bej�v� irathoz iktathat� v�laszirat!" };
                return false;
            }
        }

        public static void SetValaszIratFunkciogomb(String iratId, EREC_IraIratok irat, ImageButton imageButton, Page page)
        {
            // Funkci�jog:
            if (!FunctionRights.GetFunkcioJog(page, "BelsoIratIktatas"))
            {
                imageButton.Enabled = false;
                return;
            }

            if (String.IsNullOrEmpty(iratId) || irat == null)
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }

            ExecParam execParam = UI.SetExecParamDefault(page);
            ErrorDetails errorDetail = null;

            if (Iratok.ValaszIratIktathato(irat, out errorDetail))
            {
                imageButton.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New
                        + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                        + "&" + QueryStringVars.UgyiratId + "=" + irat.Ugyirat_Id
                        + "&" + QueryStringVars.IratId + "=" + iratId
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, imageButton.ClientID, EventArgumentConst.refresh);
            }
            else
            {
                imageButton.Enabled = false;
                return;
            }
        }

        public static void IratokGridView_RowDataBound_CheckTomegesOlvasasiJog(GridViewRowEventArgs e, Page page, List<string> jogosultIratok)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                Iratok.Statusz statusz = Iratok.GetAllapotFromDataRow(drv.Row);

                var errorDetails = new ErrorDetails { Message = "Az irathoz nincs joga jogosultat felvenni!" };
                bool vegrehajthato = jogosultIratok.Contains(statusz.Id);
                UI.SetRowCheckboxAndInfo(vegrehajthato, e, errorDetails, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, statusz.Id, page);
            }
        }
    }

    public class IktatoKonyvek : BaseUtility.IktatoKonyvek
    {
        public static void FillDropDownList(DropDownList dropDownList, bool filterForActualYear, bool filterForNotClosed, String iktatoerkezteto,
            Page page, Contentum.eUIControls.eErrorPanel errorPanel)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

            EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
            Search.IktatoErkezteto.Filter(iktatoerkezteto);
            if (filterForActualYear)
            {
                Search.Ev.Filter(DateTime.Today.Year.ToString());
            }

            if (filterForNotClosed)
            {
                Search.LezarasDatuma.IsNull();
            }
            Search.OrderBy = "Iktatohely ASC, Ev DESC";
            Result result = service.GetAllWithIktathat(execParam, Search);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                dropDownList.Items.Clear();

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    //String iktatokonyvNev = "";
                    //String megkulJel = "";
                    String iktatokonyvId = "";
                    try
                    {
                        //iktatokonyvNev = row["Nev"].ToString();
                        //megkulJel = row["MegkulJelzes"].ToString();
                        iktatokonyvId = row["Id"].ToString();
                        string text = IktatoKonyvek.GetIktatokonyvText(row);
                        dropDownList.Items.Add(new ListItem(text, iktatokonyvId));
                    }
                    catch
                    {
                        // hiba, pl. nem stimmel az oszlopn�v
                        dropDownList.Items.Clear();
                        dropDownList.Items.Add(new ListItem(Resources.Error.UIDropDownListFillingError, ""));
                        //IraIktatoKonyvek_DropDownList.Enabled = false;
                        break;
                    }

                }
            }
        }

        public static string GetIktatokonyvText(DataRow row)
        {
            string iktatohely = row["Iktatohely"].ToString();
            string megkulonboztetoJelzes = row["MegkulJelzes"].ToString();
            string nev = row["Nev"].ToString();

            string text = iktatohely;
            if (!String.IsNullOrEmpty(megkulonboztetoJelzes))
            {
                if (!String.IsNullOrEmpty(text))
                    text += txtDelimeter;

                text += megkulonboztetoJelzes;
            }

            if (!String.IsNullOrEmpty(nev))
            {
                text += nevBeginDelimeter + nev + nevEndDelimeter;
            }

            return text;
        }


        // Postak�nyvn�l nem �rjuk ki a megk�l�nb�ztet� jelz�st, meg ilyeneket...
        public static string GetIktatokonyvTextForPostakonyv(DataRow row)
        {
            return row["Nev"].ToString();
        }



        public static string GetIktatokonyvValue(DataRow row)
        {
            string iktatohely = row["Iktatohely"].ToString();
            //string megkulonboztetoJelzes = row["MegkulJelzes"].ToString();

            //if (String.IsNullOrEmpty(iktatohely.Trim()) && String.IsNullOrEmpty(megkulonboztetoJelzes.Trim()))
            //{
            //    return String.Empty;
            //}

            //string value = iktatohely.Trim() + valueDelimeter + megkulonboztetoJelzes.Trim();
            string value = iktatohely;

            return value;

        }

        public static string GetIktatokonyvText(EREC_IraIktatoKonyvek erecIktatokonyv)
        {
            DataTable t = new DataTable("temp");
            t.Columns.AddRange(new DataColumn[3] { new DataColumn("Iktatohely"), new DataColumn("MegkulJelzes"), new DataColumn("Nev") });
            DataRow row = t.NewRow();
            row["Iktatohely"] = erecIktatokonyv.Iktatohely;
            row["MegkulJelzes"] = erecIktatokonyv.MegkulJelzes;
            row["Nev"] = erecIktatokonyv.Nev;

            if (erecIktatokonyv.IktatoErkezteto == Constants.IktatoErkezteto.Postakonyv)
            {
                return GetIktatokonyvTextForPostakonyv(row);
            }
            else
            {
                return GetIktatokonyvText(row);
            }
        }

        public static string GetIktatokonyvValue(EREC_IraIktatoKonyvekSearch erecIktatokonyvSearch)
        {
            return GetIktatokonyvValue(erecIktatokonyvSearch.Iktatohely.Value, erecIktatokonyvSearch.MegkulJelzes.Value);
        }

        public static string GetIktatokonyvValue(string Iktatohely, string MegkulJelzes)
        {
            DataTable t = new DataTable("temp");
            t.Columns.AddRange(new DataColumn[2] { new DataColumn("Iktatohely"), new DataColumn("MegkulJelzes") });
            DataRow row = t.NewRow();
            row["Iktatohely"] = Iktatohely;
            row["MegkulJelzes"] = MegkulJelzes;
            return GetIktatokonyvValue(row);
        }


        public static void FillDropDownListByIrattariTetel(DropDownList dropDownList, bool filterForActualYear, bool filterForNotClosed, string iktatoerkezteto, string IrattariTetel_Id,
            Page page, Contentum.eUIControls.eErrorPanel errorPanel)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

            EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
            Search.IktatoErkezteto.Filter(iktatoerkezteto);
            if (filterForActualYear)
            {
                Search.Ev.Filter(DateTime.Today.Year.ToString());
            }

            if (filterForNotClosed)
            {
                Search.LezarasDatuma.IsNull();
            }
            Search.OrderBy = "Iktatohely ASC, Ev DESC";
            Result result = service.GetAllWithIktathatByIrattariTetel(execParam, Search, IrattariTetel_Id);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                dropDownList.Items.Clear();

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    String iktatokonyvId = "";
                    try
                    {
                        iktatokonyvId = row["Id"].ToString();
                        string text = IktatoKonyvek.GetIktatokonyvText(row);
                        dropDownList.Items.Add(new ListItem(text, iktatokonyvId));
                    }
                    catch
                    {
                        // hiba, pl. nem stimmel az oszlopn�v
                        dropDownList.Items.Clear();
                        dropDownList.Items.Add(new ListItem(Resources.Error.UIDropDownListFillingError, ""));
                        //IraIktatoKonyvek_DropDownList.Enabled = false;
                        break;
                    }

                }
            }
        }


        /// <summary>
        /// Ha nem jel�lhet� ki lez�r�sra, megjel�li az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void IraIktatokonyvekGridView_RowDataBound_CheckLezaras(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                //  ellen�rz�se:                
                bool bLezarhato = false;
                ErrorDetails errorDetail;

                ExecParam execParam = UI.SetExecParamDefault(page);

                if (IktatoKonyvek.Lezarhato(execParam, drv, out errorDetail))
                {
                    bLezarhato = true;
                }

                // CheckBox lek�r�se:
                CheckBox checkBox = (CheckBox)e.Row.FindControl("check");
                CheckBox cbUjIktatokonyv = (CheckBox)e.Row.FindControl("cbUjIktatokonyv");

                if (!bLezarhato)
                {
                    e.Row.BackColor = System.Drawing.Color.Coral;
                    if (checkBox != null)
                    {
                        checkBox.Checked = false;
                        checkBox.Enabled = false;

                        if (cbUjIktatokonyv != null)
                        {
                            cbUjIktatokonyv.Checked = false;
                            cbUjIktatokonyv.Enabled = false;
                        }
                    }

                    // Info Image �ll�t�sa, hogy mi�rt nem lehet �tadni
                    ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                    if (infoImageButton != null && errorDetail != null)
                    {
                        infoImageButton.Visible = true;
                        string infoMessage = ResultError.GetErrorDetailInfoMsg(errorDetail, page);
                        infoImageButton.ToolTip = infoMessage;
                        infoMessage = infoMessage.Replace("\n", ""); //LZS - BLG_2549
                        infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                            + Constants.ErrorDetails.ObjectTypes.EREC_IraIktatoKonyvek + "','" + drv["Id"].ToString() + "','"
                            + errorDetail.ObjectType + "','" + errorDetail.ObjectId + "'); return false;";
                    }
                }
                else
                {
                    checkBox.Checked = true;
                    checkBox.Enabled = true;

                    if (cbUjIktatokonyv != null)
                    {
                        cbUjIktatokonyv.Checked = true;
                        cbUjIktatokonyv.Enabled = true;
                    }
                }
            }
        }

        public class IktatokonyvItem
        {
            private string ev;

            public string Ev
            {
                get { return ev; }
                set { ev = value; }
            }

            private string id;

            public string Id
            {
                get { return id; }
                set { id = value; }
            }

            private string text;

            public string Text
            {
                get { return text; }
                set { text = value; }
            }

            private string value;

            public string Value
            {
                get { return this.value; }
                set { this.value = value; }
            }

            private string megkulJelzes;

            public string MegkulJelzes
            {
                get { return this.megkulJelzes; }
                set { this.megkulJelzes = value; }
            }

            private string lezarasDatuma;

            public string LezarasDatuma
            {
                get { return this.lezarasDatuma; }
                set { this.lezarasDatuma = value; }
            }

            private string iktSzamOsztas;

            public string IktSzamOsztas
            {
                get { return this.iktSzamOsztas; }
                set { this.iktSzamOsztas = value; }
            }

            public IktatokonyvItem()
            {
            }

            public IktatokonyvItem(DataRow row)
            {
                ev = row["Ev"].ToString();
                id = row["Id"].ToString();
                text = IktatoKonyvek.GetIktatokonyvText(row);
                value = IktatoKonyvek.GetIktatokonyvValue(row);
                megkulJelzes = row["MegkulJelzes"].ToString();
                iktSzamOsztas = row["IktSzamOsztas"].ToString();
                lezarasDatuma = row["LezarasDatuma"].ToString();
                var iktatoerkezteto = row["IktatoErkezteto"].ToString();

                // Postak�nyvn�l m�shogy van a ki�rand� sz�veg �ssze�ll�t�sa:
                if (iktatoerkezteto == Constants.IktatoErkezteto.Postakonyv)
                {
                    text = IktatoKonyvek.GetIktatokonyvTextForPostakonyv(row);
                }
            }
        }

        public static class IktatokonyvCache
        {
            private const string EveryEv = "EveryEv";
            private const string CacheKey_AllIktatokonyvList = "AllIktatokonyvList";
            private const string CacheKey_AllErkeztetokonyvList = "AllErkeztetokonyvList";
            private const string CacheKey_AllPostakonyvList = "AllPostakonyvList";

            private static Dictionary<string, List<IktatokonyvItem>> GetFromSession(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto)
            {
                string OrgId = FelhasznaloProfil.OrgId(session);

                if (iktatoerkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    if (session[Constants.SessionNames.ErkeztetokonyvekDictionary + OrgId] != null
                        && session[Constants.SessionNames.ErkeztetokonyvekDictionary + OrgId] is Dictionary<string, List<IktatokonyvItem>>)
                    {
                        return session[Constants.SessionNames.ErkeztetokonyvekDictionary + OrgId] as Dictionary<string, List<IktatokonyvItem>>;
                    }
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Iktato)
                {
                    if (session[Constants.SessionNames.IktatokonyvekDictionary + OrgId] != null
                        && session[Constants.SessionNames.IktatokonyvekDictionary + OrgId] is Dictionary<string, List<IktatokonyvItem>>)
                    {
                        return session[Constants.SessionNames.IktatokonyvekDictionary + OrgId] as Dictionary<string, List<IktatokonyvItem>>;
                    }
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Postakonyv)
                {
                    if (session[Constants.SessionNames.PostakonyvekDictionary + OrgId] != null
                        && session[Constants.SessionNames.PostakonyvekDictionary + OrgId] is Dictionary<string, List<IktatokonyvItem>>)
                    {
                        return session[Constants.SessionNames.PostakonyvekDictionary + OrgId] as Dictionary<string, List<IktatokonyvItem>>;
                    }
                }

                return null;
            }

            private static void AddToSession(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto, Dictionary<string, List<IktatokonyvItem>> IktatokonyvrkDictionary)
            {
                string OrgId = FelhasznaloProfil.OrgId(session);

                if (iktatoerkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    session[Constants.SessionNames.ErkeztetokonyvekDictionary + OrgId] = IktatokonyvrkDictionary;
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Iktato)
                {
                    session[Constants.SessionNames.IktatokonyvekDictionary + OrgId] = IktatokonyvrkDictionary;
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Postakonyv)
                {
                    session[Constants.SessionNames.PostakonyvekDictionary + OrgId] = IktatokonyvrkDictionary;
                }
            }

            private static Result FillSession(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto)
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam execParam = UI.SetExecParamDefault(session, new ExecParam());

                EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
                Search.IktatoErkezteto.Filter(iktatoerkezteto);
                Search.OrderBy = "Iktatohely ASC, Ev DESC";

                Result result = service.GetAllWithIktathat(execParam, Search);

                if (result.IsError)
                {
                    return result;
                }

                Dictionary<string, List<IktatokonyvItem>> IktatokonyvekDictionary = new Dictionary<string, List<IktatokonyvItem>>();

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    IktatokonyvItem item = new IktatokonyvItem(row);

                    List<IktatokonyvItem> IktatokonyvekList;

                    if (!IktatokonyvekDictionary.TryGetValue(item.Ev, out IktatokonyvekList))
                    {
                        IktatokonyvekList = new List<IktatokonyvItem>();
                        IktatokonyvekDictionary[item.Ev] = IktatokonyvekList;
                    }

                    IktatokonyvekList.Add(item);

                    List<IktatokonyvItem> IktatokonyvekListAll;

                    if (!IktatokonyvekDictionary.TryGetValue(EveryEv, out IktatokonyvekListAll))
                    {
                        IktatokonyvekListAll = new List<IktatokonyvItem>();
                        IktatokonyvekDictionary[EveryEv] = IktatokonyvekListAll;
                    }

                    IktatokonyvekListAll.Add(item);
                }

                AddToSession(session, iktatoerkezteto, IktatokonyvekDictionary);

                return new Result();
            }


            private static Dictionary<string, List<IktatokonyvItem>> GetAllIktatokonyvFromCache(System.Web.Caching.Cache cache, ExecParam execParam, string iktatoerkezteto)
            {
                string OrgId = execParam.Org_Id;

                if (iktatoerkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    if (cache[CacheKey_AllErkeztetokonyvList + OrgId] != null
                        && cache[CacheKey_AllErkeztetokonyvList + OrgId] is Dictionary<string, List<IktatokonyvItem>>)
                    {
                        return cache[CacheKey_AllErkeztetokonyvList + OrgId] as Dictionary<string, List<IktatokonyvItem>>;
                    }
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Iktato)
                {
                    if (cache[CacheKey_AllIktatokonyvList + OrgId] != null
                        && cache[CacheKey_AllIktatokonyvList + OrgId] is Dictionary<string, List<IktatokonyvItem>>)
                    {
                        return cache[CacheKey_AllIktatokonyvList + OrgId] as Dictionary<string, List<IktatokonyvItem>>;
                    }
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Postakonyv)
                {
                    if (cache[CacheKey_AllPostakonyvList + OrgId] != null
                        && cache[CacheKey_AllPostakonyvList + OrgId] is Dictionary<string, List<IktatokonyvItem>>)
                    {
                        return cache[CacheKey_AllPostakonyvList + OrgId] as Dictionary<string, List<IktatokonyvItem>>;
                    }
                }

                return null;
            }

            private static void AddToCache(System.Web.Caching.Cache cache, ExecParam execParam, string iktatoerkezteto, Dictionary<string, List<IktatokonyvItem>> IktatokonyvekDictionary)
            {
                int cacheDuration_hour = 10;

                string OrgId = execParam.Org_Id;

                if (iktatoerkezteto == Constants.IktatoErkezteto.Erkezteto)
                {
                    cache.Insert(CacheKey_AllErkeztetokonyvList + OrgId, IktatokonyvekDictionary, null
                    , DateTime.Now.AddHours(cacheDuration_hour), System.Web.Caching.Cache.NoSlidingExpiration
                    , System.Web.Caching.CacheItemPriority.Default, null);
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Iktato)
                {
                    cache.Insert(CacheKey_AllIktatokonyvList + OrgId, IktatokonyvekDictionary, null
                    , DateTime.Now.AddHours(cacheDuration_hour), System.Web.Caching.Cache.NoSlidingExpiration
                    , System.Web.Caching.CacheItemPriority.Default, null);
                }
                else if (iktatoerkezteto == Constants.IktatoErkezteto.Postakonyv)
                {
                    cache.Insert(CacheKey_AllPostakonyvList + OrgId, IktatokonyvekDictionary, null
                    , DateTime.Now.AddHours(cacheDuration_hour), System.Web.Caching.Cache.NoSlidingExpiration
                    , System.Web.Caching.CacheItemPriority.Default, null);
                }
            }

            private static Result FillAllIktatokonyvToCache(System.Web.Caching.Cache cache, ExecParam execParam, string iktatoerkezteto)
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                //ExecParam execParam = UI.SetExecParamDefault(session, new ExecParam());

                EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
                Search.IktatoErkezteto.Filter(iktatoerkezteto);
                Search.OrderBy = "Iktatohely ASC, Ev DESC";

                Result result = service.GetAll(execParam, Search);

                if (result.IsError)
                {
                    return result;
                }

                Dictionary<string, List<IktatokonyvItem>> IktatokonyvekDictionary = new Dictionary<string, List<IktatokonyvItem>>();

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    IktatokonyvItem item = new IktatokonyvItem(row);

                    List<IktatokonyvItem> IktatokonyvekList;

                    if (!IktatokonyvekDictionary.TryGetValue(item.Ev, out IktatokonyvekList))
                    {
                        IktatokonyvekList = new List<IktatokonyvItem>();
                        IktatokonyvekDictionary[item.Ev] = IktatokonyvekList;
                    }

                    IktatokonyvekList.Add(item);

                    List<IktatokonyvItem> IktatokonyvekListAll;

                    if (!IktatokonyvekDictionary.TryGetValue(EveryEv, out IktatokonyvekListAll))
                    {
                        IktatokonyvekListAll = new List<IktatokonyvItem>();
                        IktatokonyvekDictionary[EveryEv] = IktatokonyvekListAll;
                    }

                    IktatokonyvekListAll.Add(item);
                }

                AddToCache(cache, execParam, iktatoerkezteto, IktatokonyvekDictionary);

                return new Result();
            }

            private static bool Predicate_IsIktatoKonyvClosed(IktatokonyvItem item)
            {
                if (String.IsNullOrEmpty(item.LezarasDatuma))
                {
                    return false;
                }

                DateTime lezarasDatuma;
                var parsed = DateTime.TryParse(item.LezarasDatuma, out lezarasDatuma);
                return parsed ? lezarasDatuma < DateTime.Now : true;
            }

            // interface b�v�tve a filterForNotClosed param�terrel: ha true, a lez�rt iktat�k�nyveket t�r�lj�k a list�b�l
            public static Result GetIktatokonyvekList(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto, string EvTol, string EvIg, bool filterForActualYear, bool filterForNotClosed, out List<IktatokonyvItem> IktatokonyvekList)
            {
                IktatokonyvekList = new List<IktatokonyvItem>();

                Dictionary<string, List<IktatokonyvItem>> IktatokonyvekDictionary;

                IktatokonyvekDictionary = GetFromSession(session, iktatoerkezteto);

                if (IktatokonyvekDictionary == null)
                {
                    Result res = FillSession(session, iktatoerkezteto);
                    if (res.IsError)
                    {
                        return res;
                    }

                    IktatokonyvekDictionary = GetFromSession(session, iktatoerkezteto);
                }

                List<IktatokonyvItem> list;

                string ev;

                if (filterForActualYear)
                {
                    ev = DateTime.Now.Year.ToString();
                }
                else
                {
                    ev = EveryEv;
                }

                if (IktatokonyvekDictionary.TryGetValue(ev, out list))
                {
                }
                else
                {
                    return new Result();
                }


                if (String.IsNullOrEmpty(EvTol) && String.IsNullOrEmpty(EvIg) || filterForActualYear)
                {

                    if (filterForNotClosed)
                    {
                        // a referencia miatt csak a m�solatot sz�rhetj�k, List.RemoveAll itt nem megy
                        foreach (IktatokonyvItem item in list)
                        {
                            if (!Predicate_IsIktatoKonyvClosed(item))
                            {
                                IktatokonyvekList.Add(item);
                            }
                        }
                    }
                    else
                    {
                        IktatokonyvekList = list;
                    }
                    return new Result();
                }

                int iEvTol = 0;

                if (!String.IsNullOrEmpty(EvTol))
                {
                    Int32.TryParse(EvTol, out iEvTol);
                }

                int iEvIg = 3000;

                if (!String.IsNullOrEmpty(EvIg))
                {
                    Int32.TryParse(EvIg, out iEvIg);
                }

                if (iEvTol > iEvIg)
                {
                    return new Result();
                }

                if (iEvIg == iEvTol)
                {
                    if (IktatokonyvekDictionary.TryGetValue(iEvTol.ToString(), out list))
                    {
                        if (filterForNotClosed)
                        {
                            // a referencia miatt csak a m�solatot sz�rhetj�k, List.RemoveAll itt nem megy
                            foreach (IktatokonyvItem item in list)
                            {
                                if (!Predicate_IsIktatoKonyvClosed(item))
                                {
                                    IktatokonyvekList.Add(item);
                                }
                            }
                        }
                        else
                        {
                            IktatokonyvekList = list;
                        }
                    }

                    return new Result();
                }

                foreach (IktatokonyvItem item in list)
                {
                    int iEv;

                    if (Int32.TryParse(item.Ev, out iEv))
                    {
                        if (iEvTol <= iEv && iEv <= iEvIg)
                        {
                            IktatokonyvekList.Add(item);
                        }
                    }
                }

                // ez nem referencia
                if (filterForNotClosed)
                {
                    IktatokonyvekList.RemoveAll(Predicate_IsIktatoKonyvClosed);
                }
                return new Result();
            }

            public static Result GetIktatokonyvekList(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto, bool filterForActualYear, bool filterForNotClosed, out List<IktatokonyvItem> IktatokonyvekList)
            {
                return GetIktatokonyvekList(session, iktatoerkezteto, String.Empty, String.Empty, filterForActualYear, filterForNotClosed, out IktatokonyvekList);
            }

            public static Result GetIktatokonyvekList(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto, string EvTol, string EvIg, out List<IktatokonyvItem> IktatokonyvekList)
            {
                return GetIktatokonyvekList(session, iktatoerkezteto, EvTol, EvIg, false, false, out IktatokonyvekList);
            }

            public static Result GetIktatokonyvekList(System.Web.SessionState.HttpSessionState session, string iktatoerkezteto, out List<IktatokonyvItem> IktatokonyvekList)
            {
                return GetIktatokonyvekList(session, iktatoerkezteto, String.Empty, String.Empty, false, false, out IktatokonyvekList);
            }

            /// <summary>
            /// Az �sszes iktat�k�nyvet visszaadja, nem csak amihez hozz� van rendelve a felhaszn�l�
            /// </summary>
            /// <param name="cache"></param>
            /// <param name="iktatoerkezteto"></param>
            /// <param name="IktatokonyvekList"></param>
            /// <returns></returns>
            public static Result GetAllIktatokonyvekList(System.Web.Caching.Cache cache, ExecParam execParam, string iktatoerkezteto, string EvTol, string EvIg, bool filterForActualYear, out List<IktatokonyvItem> IktatokonyvekList)
            {
                IktatokonyvekList = new List<IktatokonyvItem>();

                Dictionary<string, List<IktatokonyvItem>> IktatokonyvekDictionary;

                IktatokonyvekDictionary = GetAllIktatokonyvFromCache(cache, execParam, iktatoerkezteto);

                if (IktatokonyvekDictionary == null)
                {
                    Result res = FillAllIktatokonyvToCache(cache, execParam, iktatoerkezteto);
                    if (res.IsError)
                    {
                        return res;
                    }

                    IktatokonyvekDictionary = GetAllIktatokonyvFromCache(cache, execParam, iktatoerkezteto);
                }

                List<IktatokonyvItem> list;

                string ev;

                if (filterForActualYear)
                {
                    ev = DateTime.Now.Year.ToString();
                }
                else
                {
                    ev = EveryEv;
                }

                if (IktatokonyvekDictionary.TryGetValue(ev, out list))
                {
                }
                else
                {
                    return new Result();
                }

                if (String.IsNullOrEmpty(EvTol) && String.IsNullOrEmpty(EvIg) || filterForActualYear)
                {
                    IktatokonyvekList = list;
                    return new Result();
                }

                int iEvTol = 0;

                if (!String.IsNullOrEmpty(EvTol))
                {
                    Int32.TryParse(EvTol, out iEvTol);
                }

                int iEvIg = 3000;

                if (!String.IsNullOrEmpty(EvIg))
                {
                    Int32.TryParse(EvIg, out iEvIg);
                }

                if (iEvTol > iEvIg)
                {
                    return new Result();
                }

                if (iEvIg == iEvTol)
                {
                    if (IktatokonyvekDictionary.TryGetValue(iEvTol.ToString(), out list))
                    {
                        IktatokonyvekList = list;
                    }

                    return new Result();
                }

                foreach (IktatokonyvItem item in list)
                {
                    int iEv;

                    if (Int32.TryParse(item.Ev, out iEv))
                    {
                        if (iEvTol <= iEv && iEv <= iEvIg)
                        {
                            IktatokonyvekList.Add(item);
                        }
                    }
                }

                return new Result();
            }

            public static Result GetAllIktatokonyvekList(System.Web.Caching.Cache cache, ExecParam execParam, string iktatoerkezteto, out List<IktatokonyvItem> IktatokonyvekList)
            {
                return GetAllIktatokonyvekList(cache, execParam, iktatoerkezteto, String.Empty, String.Empty, false, out IktatokonyvekList);
            }
        }

    }

    public class Dossziek
    {

        public static bool AtadasraKijelolheto(string Id, string Csoport_Id_Tulaj, string Tipus
           , ExecParam execParam, out ErrorDetails errorDetail)
        {
            KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();

            Result statuszResult = service.GetStatus(execParam, Id);
            if (statuszResult.IsError || statuszResult.GetCount < 1)
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "Hiba a dosszi� lek�r�se k�zben!";
                return false;
            }

            if (statuszResult.Ds.Tables[0].Rows[0]["Allapot"].ToString() == "1")
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "A dosszi� nem adhat� �t, mert m�r mozg�sban van!";
                return false;
            }

            if (execParam.Felhasznalo_Id != Csoport_Id_Tulaj)
            {
                errorDetail = new ErrorDetails("A dosszi� nem �nn�l van!", Constants.TableNames.KRT_Mappak, Id, Constants.ErrorDetails.ColumnTypes.FelhasznaloCsoport_Id_Orzo, Csoport_Id_Tulaj);
                return false;
            }
            else if (Tipus == KodTarak.MAPPA_TIPUS.Virtualis)
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "Csak fizikai dosszi�kat lehet �tadni!";
                return false;
            }
            else
            {
                errorDetail = null;
                return true;
            }

        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.KRT_Mappak, Id, null, null);
                return false;
            }

            KRT_Mappak krt_Mappak = (KRT_Mappak)result.Record;

            return CheckAtadasraKijelolhetoWithFunctionRight(page, krt_Mappak.Id, krt_Mappak.Csoport_Id_Tulaj, krt_Mappak.Tipus, out errorDetail);
        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, string Id, string Csoport_Id_Tulaj, string Tipus, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // �tadhat�?
            bool atadasraKijelolheto = false;

            if (AtadasraKijelolheto(Id, Csoport_Id_Tulaj, Tipus, UI.SetExecParamDefault(page), out errorDetail))
            {
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "MappakAtadas"))
                {
                    //"�nnek nincs joga dosszi�t �tadni."
                    errorDetail = new ErrorDetails();
                    errorDetail.Message = Resources.Error.ErrorCode_52869;
                }
                else
                {
                    atadasraKijelolheto = true;
                }
            }

            return atadasraKijelolheto;
        }

        public static void DossziekGridView_RowDataBound_CheckAtadasraKijeloles(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:     
                //bool atadasraKijelolheto = false;
                ErrorDetails errorDetail;

                //if (Dossziek.AtadasraKijelolheto(drv["Id"].ToString(),drv["Csoport_Id_Tulaj"].ToString(),drv["Tipus"].ToString(),
                //    UI.SetExecParamDefault(page, new ExecParam()), out errorDetail))
                //{
                //    atadasraKijelolheto = true;
                //}

                bool atadasraKijelolheto = CheckAtadasraKijelolhetoWithFunctionRight(page, drv["Id"].ToString(), drv["Csoport_Id_Tulaj"].ToString(), drv["Tipus"].ToString(), out errorDetail);

                UI.SetRowCheckboxAndInfo(atadasraKijelolheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.KRT_Mappak, drv["Id"].ToString(), page);
            }
        }


        public static bool Atveheto(string Id, string Csoport_Id_Tulaj, string Tipus
            , ExecParam execParam, Page page, out ErrorDetails errorDetail)
        {
            KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();

            Result statuszResult = service.GetStatus(execParam, Id);
            if (statuszResult.IsError || statuszResult.GetCount < 1)
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "Hiba a dosszi� lek�r�se k�zben!";
                return false;
            }

            if (statuszResult.Ds.Tables[0].Rows[0]["Allapot"].ToString() == "0")
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "A dosszi�t nem adt�k �t!";
                return false;
            }

            if (statuszResult.Ds.Tables[0].Rows[0]["CimzettUser"].ToString() != execParam.Felhasznalo_Id
                && (statuszResult.Ds.Tables[0].Rows[0]["CimzettUser"].ToString() != execParam.FelhasznaloSzervezet_Id
                    || !FunctionRights.GetFunkcioJog(page, "AtvetelSzervezettol")))
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "A dosszi�t nem veheti �t, mert nem �n a c�mzett!";
                return false;
            }

            if (Tipus == KodTarak.MAPPA_TIPUS.Virtualis)
            {
                errorDetail = new ErrorDetails();
                errorDetail.Message = "Csak fizikai dosszi�kat lehet �tvenni!";
                return false;
            }
            else
            {
                errorDetail = null;
                return true;
            }

        }

        public static void DossziekGridView_RowDataBound_CheckAtvetel(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tvehet�s�g kijel�lhet�s�g ellen�rz�se:     
                ErrorDetails errorDetail;

                bool atveheto = Dossziek.Atveheto(drv["Id"].ToString(), drv["Csoport_Id_Tulaj"].ToString(), drv["Tipus"].ToString(),
                    UI.SetExecParamDefault(page, new ExecParam()), page, out errorDetail);

                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.KRT_Mappak, drv["Id"].ToString(), page);
            }
        }
    }

    public class PostaKonyvek
    {

        private static string GetPostakonyvMegkulJelzes(string postakonyvId, Page page)
        {
            List<IktatoKonyvek.IktatokonyvItem> postaKonyvekList;

            // postak�nyv lista felt�lt�se:
            IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(page.Session, Constants.IktatoErkezteto.Postakonyv
                , out postaKonyvekList);

            if (postaKonyvekList != null)
            {
                // megkeress�k a postak�nyvet:
                foreach (IktatoKonyvek.IktatokonyvItem postaKonyvItem in postaKonyvekList)
                {
                    if (postaKonyvItem.Id == postakonyvId)
                    {
                        return postaKonyvItem.MegkulJelzes;
                    }
                }
            }

            return String.Empty;
        }

        private const string kcs_Postakonyv_Vevokodok = "Postakonyv_Vevokodok";
        private const string kcs_Postakonyv_MegallapodasAzonositok = "Postakonyv_MegallapodasAzonositok";

        /// <summary>
        /// Visszaadja a megadott postak�nyvh�z tartoz� vev�k�dot
        /// </summary>
        /// <param name="postakonyvId">Postak�nyv Id-ja (EREC_IraIktatokonyvek t�bl�b�l)</param>        
        public static string GetPostakonyvVevokod(string postakonyvId, Page page)
        {
            if (string.IsNullOrEmpty(postakonyvId)) { return String.Empty; }

            // postak�nyv MegkulJelzes-�nek lek�r�se:
            string postaKonyv_MegkulJelzes = GetPostakonyvMegkulJelzes(postakonyvId, page);

            if (string.IsNullOrEmpty(postaKonyv_MegkulJelzes)) { return String.Empty; }

            // Vev�k�dok lek�r�se a k�dcsoport-cache -b�l
            Dictionary<string, string> vevoKodokDict =
                KodTar_Cache.GetKodtarakByKodCsoport(kcs_Postakonyv_Vevokodok, page);
            if (vevoKodokDict != null
                && vevoKodokDict.ContainsKey(postaKonyv_MegkulJelzes))
            {
                return vevoKodokDict[postaKonyv_MegkulJelzes];
            }

            return String.Empty;
        }


        /// <summary>
        /// Visszaadja a megadott postak�nyvh�z tartoz� meg�llapod�s azonos�t�t
        /// </summary>
        /// <param name="postakonyvId">Postak�nyv Id-ja (EREC_IraIktatokonyvek t�bl�b�l)</param>   
        public static string GetPostakonyvMegallapodasAzonosito(string postakonyvId, Page page)
        {
            if (string.IsNullOrEmpty(postakonyvId)) { return String.Empty; }

            // postak�nyv MegkulJelzes-�nek lek�r�se:
            string postaKonyv_MegkulJelzes = GetPostakonyvMegkulJelzes(postakonyvId, page);

            if (string.IsNullOrEmpty(postaKonyv_MegkulJelzes)) { return String.Empty; }

            // Meg�llapod�s azonos�t�k lek�r�se a k�dcsoport-cache -b�l
            Dictionary<string, string> megallapodasAzonositokDict =
                KodTar_Cache.GetKodtarakByKodCsoport(kcs_Postakonyv_MegallapodasAzonositok, page);
            if (megallapodasAzonositokDict != null
                && megallapodasAzonositokDict.ContainsKey(postaKonyv_MegkulJelzes))
            {
                return megallapodasAzonositokDict[postaKonyv_MegkulJelzes];
            }

            return String.Empty;
        }
    }

    //BLG 1131 k�zbes�t�si t�telek ellen�rz�se
    public class IraKezbesitesiTetelek
    {
        public static void GridView_RowDataBound_CheckAtadasraKijeloles(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tad�sra kijel�lhet�s�g ellen�rz�se:                

                //IrattariKikero.Statusz statusz = IrattariKikero.GetAllapotFromDataRowView(drv);
                string allapotNev = string.Empty;
                string statusz = drv["Allapot"].ToString();
                if (!String.IsNullOrEmpty(statusz))
                {
                    allapotNev = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.KEZBESITESITETEL_ALLAPOT.KodcsoportKod, statusz, page);
                }

                //string statusz = "";
                string kezbesitesitetelId = "";

                bool atadasrakijelolt = !string.IsNullOrEmpty(statusz) && statusz.Equals(KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt);
                ErrorDetails errorDetail = new ErrorDetails();

                if (!atadasrakijelolt)
                {
                    errorDetail = new ErrorDetails("A k�zbes�t�si t�tel �llapota nem megfelel�.", Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek, String.Empty, Constants.ErrorDetails.ColumnTypes.Allapot, allapotNev);
                }

                UI.SetRowCheckboxAndInfo(atadasrakijelolt, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek, kezbesitesitetelId, page);
            }
        }

        #region 1130 �tvev� jegyz�k nyomtat�sa
        public static void GridView_RowDataBound_CheckAtvett(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                // �tvett st�tusz ellen�rz�se:                

                string allapotNev = string.Empty;
                string statusz = drv["Allapot"].ToString();
                if (!String.IsNullOrEmpty(statusz))
                {
                    allapotNev = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.KEZBESITESITETEL_ALLAPOT.KodcsoportKod, statusz, page);
                }

                string kezbesitesitetelId = "";

                bool atvett = !string.IsNullOrEmpty(statusz) && statusz.Equals(KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett);
                ErrorDetails errorDetail = new ErrorDetails();

                // CheckBox lek�r�se:
                CheckBox checkBox = (CheckBox)e.Row.FindControl("check");

                if (!atvett)
                {
                    errorDetail = new ErrorDetails("A k�zbes�t�si t�tel �llapota nem megfelel�.", Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek, String.Empty, Constants.ErrorDetails.ColumnTypes.Allapot, allapotNev);
                }

                UI.SetRowCheckboxAndInfo(atvett, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_IraKezbesitesiTetelek, kezbesitesitetelId, page);
            }
        }

        #endregion
    }

    public class Util
    {
        /// <summary>
        /// HiddenField mez� �rt�k�t n�veli 1-gyel (ha az �rt�ke sz�m egy�ltal�n)
        /// </summary>
        /// <param name="hiddenfield"></param> 
        public static void IncrementHiddenFieldValue(HiddenField hiddenfield)
        {
            try
            {
                int value = Int32.Parse(hiddenfield.Value);
                value++;
                hiddenfield.Value = value.ToString();
            }
            catch
            {
                // ha hiba van, marad a r�gi �rt�k
            }
        }

        /// <summary>
        /// BusinessDocument mez�inek felt�lt�se DataRow-b�l
        /// </summary>
        /// <param name="BusinessDocument"></param>
        /// <param name="row"></param>
        public static void LoadBusinessDocumentFromDataRow(object BusinessDocument, DataRow row)
        {
            try
            {
                if (row != null)
                {
                    System.Reflection.PropertyInfo[] Properties = BusinessDocument.GetType().GetProperties();
                    foreach (System.Reflection.PropertyInfo P in Properties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BusinessDocument, row[P.Name].ToString(), null);
                            }
                        }
                    }
                    System.Reflection.FieldInfo BaseField = BusinessDocument.GetType().GetField("Base");
                    object BaseObject = BaseField.GetValue(BusinessDocument);

                    System.Reflection.PropertyInfo[] BaseProperties = BaseObject.GetType().GetProperties();

                    foreach (System.Reflection.PropertyInfo P in BaseProperties)
                    {
                        if (row.Table.Columns.Contains(P.Name))
                        {
                            if (!(row.IsNull(P.Name)))
                            {
                                P.SetValue(BaseObject, row[P.Name].ToString(), null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


    public class CustomTextBox : Contentum.eUIControls.CustomTextBox
    {
    }

    [Serializable()]
    public class SzamlaErkeztetesFormTemplateObject
    {
        private EREC_KuldKuldemenyek kuldemenyComponents = null;

        public EREC_KuldKuldemenyek KuldemenyComponents
        {
            get { return kuldemenyComponents; }
            set { kuldemenyComponents = value; }
        }

        private EREC_Szamlak szamlaComponents = new EREC_Szamlak();
        public EREC_Szamlak SzamlaComponents
        {
            get { return szamlaComponents; }
            set { szamlaComponents = value; }
        }
    }

    [Serializable()]
    public class SzignalasTemplateObject
    {
        private string _szignalasTipus;

        public string SzignalasTipus
        {
            get { return _szignalasTipus; }
            set { _szignalasTipus = value; }
        }

        private EREC_KuldKuldemenyek kuldemenyComponents = new EREC_KuldKuldemenyek();

        public EREC_KuldKuldemenyek KuldemenyComponents
        {
            get { return kuldemenyComponents; }
            set { kuldemenyComponents = value; }
        }

        private EREC_UgyUgyiratok ugyiratComponents = new EREC_UgyUgyiratok();
        public EREC_UgyUgyiratok UgyiratComponents
        {
            get { return ugyiratComponents; }
            set { ugyiratComponents = value; }
        }


        private EREC_IraIratok iratComponents = new EREC_IraIratok();
        public EREC_IraIratok IratComponents
        {
            get { return iratComponents; }
            set { iratComponents = value; }
        }

        private EREC_HataridosFeladatok hataridosFeladat = null;
        public EREC_HataridosFeladatok HataridosFeladat
        {
            get { return hataridosFeladat; }
            set { hataridosFeladat = value; }
        }
    }

    [Serializable()]
    public class IktatasFormTemplateObject
    {
        private EREC_KuldKuldemenyek kuldemenyComponents = null;

        public EREC_KuldKuldemenyek KuldemenyComponents
        {
            get { return kuldemenyComponents; }
            set { kuldemenyComponents = value; }
        }

        private EREC_UgyUgyiratok ugyiratComponents = new EREC_UgyUgyiratok();
        public EREC_UgyUgyiratok UgyiratComponents
        {
            get { return ugyiratComponents; }
            set { ugyiratComponents = value; }
        }


        private EREC_IraIratok iratComponents = new EREC_IraIratok();
        public EREC_IraIratok IratComponents
        {
            get { return iratComponents; }
            set { iratComponents = value; }
        }


        private EREC_PldIratPeldanyok iratPeldanyComponents = new EREC_PldIratPeldanyok();
        public EREC_PldIratPeldanyok IratPeldanyComponents
        {
            get { return iratPeldanyComponents; }
            set { iratPeldanyComponents = value; }
        }

        private EREC_HataridosFeladatok hataridosFeladat = null;
        public EREC_HataridosFeladatok HataridosFeladat
        {
            get { return hataridosFeladat; }
            set { hataridosFeladat = value; }
        }

        private string agazatiJel_Id;
        public string AgazatiJel_Id
        {
            get { return agazatiJel_Id; }
            set { agazatiJel_Id = value; }
        }

        private string iraKezFeljegyz_KezelesTipus;
        public string IraKezFeljegyz_KezelesTipus
        {
            get { return iraKezFeljegyz_KezelesTipus; }
            set { iraKezFeljegyz_KezelesTipus = value; }
        }

        private string iraKezFeljegyz_Leiras;
        public string IraKezFeljegyz_Leiras
        {
            get { return iraKezFeljegyz_Leiras; }
            set { iraKezFeljegyz_Leiras = value; }
        }

        private bool ugyiratPeldanySzukseges;
        public bool UgyiratPeldanySzukseges
        {
            get { return ugyiratPeldanySzukseges; }
            set { ugyiratPeldanySzukseges = value; }
        }

        private bool keszitoPeldanya;
        public bool KeszitoPeldanya
        {
            get { return keszitoPeldanya; }
            set { keszitoPeldanya = value; }
        }

        public class ElozmenySearchcomponent
        {
            private string ev = null;

            public string Ev
            {
                get
                {
                    if (ev == null) return String.Empty;
                    return ev;
                }
                set
                {
                    if (!String.IsNullOrEmpty(value))
                    {
                        int iEv;
                        if (Int32.TryParse(value, out iEv))
                            ev = value;
                    }
                }
            }

            private string iktatokonyv = null;

            public string Iktatokonyv
            {
                get
                {
                    if (iktatokonyv == null) return iktatokonyv;
                    return iktatokonyv;
                }
                set { iktatokonyv = value; }
            }

            private string foszam = null;

            public string Foszam
            {
                get
                {
                    if (foszam == null) return String.Empty;
                    return foszam;
                }
                set
                {
                    if (!String.IsNullOrEmpty(value))
                    {
                        int iFoszam;
                        if (Int32.TryParse(value, out iFoszam))
                            foszam = value;
                    }
                }
            }


        }

        private ElozmenySearchcomponent elozmenySearch = null;

        public ElozmenySearchcomponent ElozmenySearch
        {
            get { return elozmenySearch; }
            set { elozmenySearch = value; }
        }

        private string _ErtkeztetesVagyIktatas;

        public string ErtkeztetesVagyIktatas
        {
            get { return _ErtkeztetesVagyIktatas; }
            set { _ErtkeztetesVagyIktatas = value; }
        }
    }

    public class HataridosFeladatok : BaseUtility.HataridosFeladatok
    {
        public class Forras : BaseUtility.HataridosFeladatok.Forras
        {
            public const string Kezi = "1";
            public const string Automatikus = "2";
            public const string Empty = "X";

            private static ListItem itemKezi = new ListItem("K�zi", Kezi);
            private static ListItem itemAuto = new ListItem("Automatikus", Automatikus);
            private static ListItem itemEmpty = new ListItem("Mind", Empty);


            public static void FillRadioButtonList(RadioButtonList radioButtonList)
            {
                FillRadioButtonList(radioButtonList, false);
            }
            public static void FillRadioButtonList(RadioButtonList radioButtonList, bool addEmptyValue)
            {
                radioButtonList.Items.Clear();
                radioButtonList.Items.Add(new ListItem(itemKezi.Text, itemKezi.Value));
                radioButtonList.Items.Add(new ListItem(itemAuto.Text, itemAuto.Value));
                if (addEmptyValue)
                {
                    radioButtonList.Items.Add(new ListItem(itemEmpty.Text, itemEmpty.Value));
                    radioButtonList.SelectedValue = itemEmpty.Value;
                }
                else
                {
                    radioButtonList.SelectedValue = itemKezi.Value;
                }
            }

            public static string GetTextFromValue(string value)
            {
                string text;

                switch (value)
                {
                    case Automatikus:
                        text = itemAuto.Text;
                        break;
                    case Kezi:
                        text = itemKezi.Text;
                        break;
                    default:
                        text = String.Empty;
                        break;
                }

                return text;
            }
        }

        public static void SetFeladatJelzoColumnVisibility(Page page, GridView gridView)
        {
            foreach (DataControlField column in gridView.Columns)
            {
                if (column.HeaderText == "F.")
                {
                    if (!IsFeladatJelzoIkonEnabledOnList(page))
                    {
                        column.Visible = false;
                        column.HeaderText = String.Empty;
                    }
                }
            }
        }

        public static void SetFeladatListColumnsVisibility(Page page, Contentum.eUIControls.TreeGridView treeGridView)
        {
            if (HataridosFeladatok.IsDisabled(page))
            {
                foreach (DataControlField column in treeGridView.Columns)
                {
                    switch (column.HeaderText)
                    {
                        case "Le�r�s":
                        case "Kezel�s":
                        case "L�trehoz�":
                        case "Azonos�t�":
                            break;
                        default:
                            column.Visible = false;
                            break;
                    }
                }

                treeGridView.Mode = Contentum.eUIControls.TreeGridView.TreeGridViewModes.GridView;
            }
        }

        public static bool IsKezelesiFeljegyzesPanelVisible(Page page)
        {
            string pageName = String.Empty;

            if (page.GetType().BaseType != null)
            {
                pageName = page.GetType().BaseType.Name;
            }

            if (IsDisabled(page))
            {
                if (pageName != "AtadasForm"
                    && pageName != "FeladatokForm")
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsFeladatTabEnabled(Page page)
        {
            return !HataridosFeladatok.IsDisabled(page);
        }
    }

    public class IratMetaDefinicio
    {
        #region Int�z�si id�
        private const string kcs_IDOEGYSEG = "IDOEGYSEG";
        /// <summary>
        /// Hat�rid�t tartalmaz� kontrol eszk�ztippj�nek be�ll�t�sa irat metadefin�ci� Id alapj�n
        /// </summary>
        /// <param name="IratMetadefinicio_Id"></param>
        /// <param name="Hatarido_CalendarControl"></param>
        public static void SetHataridoToolTipByIratMetaDefinicioId(Page page, string IratMetadefinicio_Id, WebControl Hatarido_Control)
        {
            EREC_IratMetaDefinicio erec_IratMetaDefinicio = GetBusinessObjectById(page, IratMetadefinicio_Id);

            SetHataridoToolTip(page, erec_IratMetaDefinicio, Hatarido_Control);
        }

        /// <summary>
        /// Hat�rid�t tartalmaz� kontrol eszk�ztippj�nek be�ll�t�sa irat metadefin�ci� objektum alapj�n
        /// </summary>
        /// <param name="IratMetadefinicio_Id"></param>
        /// <param name="Hatarido_CalendarControl"></param>
        public static void SetHataridoToolTipByIratMetaDefinicio(Page page, EREC_IratMetaDefinicio erec_IratMetaDefinicio, WebControl Hatarido_Control)
        {
            SetHataridoToolTip(page, erec_IratMetaDefinicio, Hatarido_Control);
        }

        public static EREC_IratMetaDefinicio GetBusinessObjectById(Page page, string IratMetadefinicio_Id)
        {
            EREC_IratMetaDefinicio erec_IratMetaDefinicio = null;
            // Int�z�si id� kiolvas�sa az EREC_IratMetaDefinicio t�bl�b�l
            if (!String.IsNullOrEmpty(IratMetadefinicio_Id))
            {
                EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                execParam.Record_Id = IratMetadefinicio_Id;
                Result result_get = service.Get(execParam);

                erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result_get.Record;
            }

            return erec_IratMetaDefinicio;
        }

        /// <summary>
        /// Hat�rid�t tartalmaz� kontrol eszk�ztippj�nek be�ll�t�sa irat metadefin�ci� alapj�n
        /// </summary>
        /// <param name="IratMetadefinicio_Id"></param>
        /// <param name="Hatarido_CalendarControl"></param>
        public static void SetHataridoToolTip(Page page, EREC_IratMetaDefinicio erec_IratMetaDefinicio, WebControl Hatarido_Control)
        {
            Hatarido_Control.ToolTip = GetHataridoToolTip(page, erec_IratMetaDefinicio);
        }

        /// <summary>
        /// Hat�rid�t tartalmaz� kontrol eszk�ztippj�nek meghat�roz�saa irat metadefin�ci� alapj�n
        /// </summary>
        /// <param name="IratMetadefinicio_Id"></param>
        public static string GetHataridoToolTip(Page page, EREC_IratMetaDefinicio erec_IratMetaDefinicio)
        {
            string ToolTip = "";
            if (erec_IratMetaDefinicio != null)
            {
                if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.UgyiratIntezesiIdo) && erec_IratMetaDefinicio.UgyiratIntezesiIdo != "0")
                {
                    string Idoegyseg;
                    if (!KodTar_Cache.GetKodtarakByKodCsoport(kcs_IDOEGYSEG, page).TryGetValue(erec_IratMetaDefinicio.Idoegyseg, out Idoegyseg))
                    {
                        if (!KodTar_Cache.GetKodtarakByKodCsoport(kcs_IDOEGYSEG, page).TryGetValue(KodTarak.IDOEGYSEG.DEFAULT, out Idoegyseg))
                        {
                            Idoegyseg = "???";
                        }
                    }

                    ToolTip = String.Format(Resources.Form.UI_IntezesiIdo_ToolTip, erec_IratMetaDefinicio.UgyiratIntezesiIdo, Idoegyseg);
                    if (!String.IsNullOrEmpty(erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott) && erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott == "1")
                    {
                        ToolTip += Resources.Form.UI_IntezesiIdoKotott_ToolTip;
                    }
                }

                if (erec_IratMetaDefinicio.UgyiratHataridoKitolas == "0")
                {
                    ToolTip += (ToolTip == "" ? "" : "\n") + Resources.Form.UI_UgyiratHataridoKitolas_Forbidden_ToolTip;
                }
                else
                {
                    ToolTip += (ToolTip == "" ? "" : "\n") + Resources.Form.UI_UgyiratHataridoKitolas_Allowed_ToolTip;
                }
            }
            else
            {
                ToolTip += Resources.Form.UI_IntezesiIdoDefault_ToolTip;
            }

            return ToolTip;
        }


        #endregion Int�z�si id�
    }

    public static class Extra_Napok_Hatarido
    {
        #region HataridoCache

        // private cache - most csak munkanapok eset�n haszn�ljuk, hogy ne kelljen mindig az adatb�zishoz fordulni

        private const String cache_key_hatarido = "Extra_Napok_HataridoDictionary";   // Context.Cache

        private static void AddToHataridoCache(System.Web.Caching.Cache cache, string Idoegyseg, string IntezesiIdo, string strDatumTol, DateTime dtHatarido)
        {
            // key: Idoegyseg|IntezesiIdo|strDatumTol �sszef�z�tt string
            string key = String.Format("{0}|{1}|{2}", Idoegyseg, IntezesiIdo, strDatumTol);
            Dictionary<string, DateTime> HataridoDictionary = null;

            if (cache[cache_key_hatarido] == null)
            {
                HataridoDictionary = new Dictionary<string, DateTime>();

                double dSlidingExpiration = 1.0;    // 1 napos sliding expiration
                // Dictionary felv�tele a Cache-be:
                cache.Insert(cache_key_hatarido, HataridoDictionary, null
                        , System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromDays(dSlidingExpiration)
                        , System.Web.Caching.CacheItemPriority.High, null);

                HataridoDictionary.Add(key, dtHatarido);
            }
            else
            {
                HataridoDictionary = (Dictionary<string, DateTime>)cache[cache_key_hatarido];

                if (HataridoDictionary.ContainsKey(key))
                {
                    HataridoDictionary[key] = dtHatarido;
                }
                else
                {
                    HataridoDictionary.Add(key, dtHatarido);
                }
            }

            Logger.Debug(String.Format("AddToHataridoCache - Key: {0}; Hat�rid�: {1}", key, dtHatarido));
        }

        private static bool TryGetFromHataridoCache(System.Web.Caching.Cache cache, string Idoegyseg, string IntezesiIdo, string strDatumTol, out DateTime dtHatarido)
        {
            dtHatarido = DateTime.MinValue;
            // key: Idoegyseg|IntezesiIdo|strDatumTol �sszef�z�tt string
            string key = String.Format("{0}|{1}|{2}", Idoegyseg, IntezesiIdo, strDatumTol);
            Logger.Debug(String.Format("TryGetFromHataridoCache - Key: {0}", key));

            Dictionary<string, DateTime> HataridoDictionary = null;

            if (cache[cache_key_hatarido] == null)
            {
                return false;
            }
            else
            {
                HataridoDictionary = (Dictionary<string, DateTime>)cache[cache_key_hatarido];
                if (!HataridoDictionary.ContainsKey(key))
                {
                    return false;
                }
                else
                {
                    dtHatarido = (DateTime)HataridoDictionary[key];
                    Logger.Debug(String.Format("TryGetFromHataridoCache - Hat�rid�: {0}", dtHatarido));
                    return true;
                }
            }

        }

        #endregion HataridoCache

        public static DateTime GetIntezesiHataridoByIdoegyseg(Page page, string Idoegyseg, string IntezesiIdo, DateTime DatumTol, out string ErrorMessage)
        {
            Logger.Debug("GetIntezesiHataridoByIdoegyseg (from page) - START");
            ExecParam ExecParam = UI.SetExecParamDefault(page);
            return Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(page.Cache, ExecParam, Idoegyseg, IntezesiIdo, DatumTol, out ErrorMessage);
        }

        public static DateTime GetIntezesiHataridoByIdoegyseg(System.Web.Caching.Cache cache, ExecParam ExecParam, string Idoegyseg, string IntezesiIdo, DateTime DatumTol, out string ErrorMessage)
        {
            Logger.Debug("GetIntezesiHataridoByIdoegyseg - START");
            Logger.Debug(String.Format("Bemen� param�terek: Id�egys�g: {0}; Int�z�si id�: {1}; D�tum: {2}", Idoegyseg, IntezesiIdo, DatumTol));

            ErrorMessage = String.Empty;
            DateTime dtHatarido = DateTime.MinValue;

            int nIdoegysegParsed = 0;
            int nIntezesiIdoParsed = 0;

            if (String.IsNullOrEmpty(Idoegyseg) || Idoegyseg == "0"
                || String.IsNullOrEmpty(IntezesiIdo) || IntezesiIdo == "0")
            {
                // default �rt�k haszn�lata
                int nDefaultIntezesiIdo = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO);
                int nDefaultIdoegyseg = Rendszerparameterek.GetInt(ExecParam, Rendszerparameterek.DEFAULT_INTEZESI_IDO_IDOEGYSEG);

                if (nDefaultIdoegyseg == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65101); //"Az alap�rtelmezett id�egys�g nem hat�rozhat� meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                if (nDefaultIntezesiIdo == 0)
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65102); //"Az alap�rtelmezett int�z�si id� nem hat�rozhat� meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                nIdoegysegParsed = nDefaultIdoegyseg;
                nIntezesiIdoParsed = nDefaultIntezesiIdo;
            }
            else
            {
                int nIdoegyseg;
                if (!Int32.TryParse(Idoegyseg, out nIdoegyseg))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65103); //"Az id�egys�g nem hat�rozhat� meg."
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                int nIntezesiIdo;
                if (!Int32.TryParse(IntezesiIdo, out nIntezesiIdo))
                {
                    ErrorMessage = ResultError.GetErrorMessageByErrorCode(65104); //"Az int�z�si id� nem hat�rozhat� meg!"
                    Logger.Debug(String.Format("Hiba: {0}", ErrorMessage));
                    Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
                    return dtHatarido;
                }

                nIdoegysegParsed = nIdoegyseg;
                nIntezesiIdoParsed = nIntezesiIdo;
            }

            Logger.Debug(String.Format("Elemzett id�egys�g: {0}; Elemzett int�z�si id�: {1}", nIdoegysegParsed, nIntezesiIdoParsed));

            // int�z�si hat�rid� meghat�roz�sa
            if (nIdoegysegParsed < 0)
            {
                switch (nIdoegysegParsed.ToString())
                {
                    case KodTarak.IDOEGYSEG.Munkanap:   // -1440
                        if (!Extra_Napok_Hatarido.TryGetFromHataridoCache(cache, Idoegyseg, IntezesiIdo, DatumTol.ToShortDateString(), out dtHatarido))
                        {
                            Contentum.eAdmin.Service.KRT_Extra_NapokService service_extra_napok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_Extra_NapokService();

                            Result result_extra_napok = service_extra_napok.KovetkezoMunkanap(ExecParam, DatumTol, nIntezesiIdoParsed);
                            if (result_extra_napok.IsError)
                            {
                                ErrorMessage = ResultError.GetErrorMessageFromResultObject(result_extra_napok);
                            }
                            else
                            {
                                try
                                {
                                    dtHatarido = (DateTime)result_extra_napok.Record;
                                }
                                catch
                                {
                                    dtHatarido = DateTime.MinValue;
                                    ErrorMessage = Resources.Error.UIDateTimeFormatError;
                                }
                            }
                            Extra_Napok_Hatarido.AddToHataridoCache(cache, Idoegyseg, IntezesiIdo, DatumTol.ToShortDateString(), dtHatarido);
                        }

                        // �ra, perc be�ll�t�s
                        dtHatarido = dtHatarido.AddHours(DatumTol.Hour);
                        dtHatarido = dtHatarido.AddMinutes(DatumTol.Minute);
                        break;
                    default:
                        ErrorMessage = ResultError.GetErrorMessageByErrorCode(65105); //"Ismeretlen id�egys�g. Az id�egys�gre vonatkoz� hat�rid�sz�m�t�s nincs implement�lva!"
                        break;
                }
            }
            else
            {
                dtHatarido = DatumTol.AddMinutes(nIdoegysegParsed * nIntezesiIdoParsed);
            }

            Logger.Debug(String.Format("Hat�rid�: {0}", dtHatarido));
            Logger.Debug("GetIntezesiHataridoByIdoegyseg - END");
            return dtHatarido;
        }

        public static DateTime GetDefaultIntezesiHatarido(Page page, DateTime DatumTol, out string ErrorMessage)
        {
            Logger.Debug("GetDefaultIntezesiHatarido (from page) - START");

            ExecParam ExecParam = UI.SetExecParamDefault(page);
            return Extra_Napok_Hatarido.GetDefaultIntezesiHatarido(page.Cache, ExecParam, DatumTol, out ErrorMessage);
        }

        public static DateTime GetDefaultIntezesiHatarido(System.Web.Caching.Cache cache, ExecParam ExecParam, DateTime DatumTol, out string ErrorMessage)
        {
            Logger.Debug("GetDefaultIntezesiHatarido - START");

            DateTime dtHatarido = DateTime.MinValue;

            // default �rt�k haszn�lata
            dtHatarido = Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(cache, ExecParam, "", "", DatumTol, out ErrorMessage);
            Logger.Debug(String.Format("Default hat�rid�: {0}", dtHatarido));
            Logger.Debug("GetDefaultIntezesiHatarido - END");
            return dtHatarido;
        }

    }

    public class ObjektumTipusok : eUtility.ObjektumTipusok
    {
    }

    public class Dokumentumok
    {
        public static string GetVerziokLink(object DokumentumId, object VerzioJel)
        {
            string ret = String.Empty;

            if (DokumentumId == null)
            {
                return ret;
            }

            if (!IsMoreVerzio(VerzioJel))
            {
                return ret;
            }

            ret = "javascript:var ret = " + JavaScripts.SetOnCLientClick_NoPostBack("DokumentumVerziokList.aspx", QueryStringVars.DokumentumId + "=" + DokumentumId.ToString(),
                Defaults.PopupWidth, Defaults.PopupHeight).TrimEnd("return false;".ToCharArray()) + ";";

            return ret;
        }

        public static bool IsMoreVerzio(object VerzioJel)
        {
            //return (VerzioJel == null || VerzioJel.ToString() != "1.0");
            return true;
        }
    }

    #region BLG_2597 HALK al��r�s ut�ni folymatatok
    public class HALKStatusz
    {
        /// <summary>
        /// A st�tusz lek�rdez�s�nek ideje
        /// </summary>
        public string StatusGetTime { get; set; }

        /// <summary>
        /// A h�tt�rfolyamat befejez�d�tt
        /// </summary>
        public bool BackgroundProcessEnded { get; set; }

        /// <summary>
        /// St�tusz
        /// </summary>
        public string BackgroundProcessStatus { get; set; }

        /// <summary>
        /// Hiba�zenet
        /// </summary>
        public string BackgroundProcessError { get; set; }
    }
    #endregion

    #region IRAT HATAS
    /// <summary>
    /// �irat hat�sa az �gyint�z�sre� �rt�k indul�s, meg�ll�t�s vagy v�ge, 
    /// </summary>
    public enum EnumIratHatasaUgyintezesre
    {
        START,
        PAUSE,
        STOP,
        ON_PROGRESS
    }
    #endregion
}
