﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using System;
using System.Data;
using System.Web.UI;

/// <summary>
/// Summary description for UtilityCsatolmanyok
/// </summary>
namespace Contentum.eRecord.Utility
{
    public static class UtilityCsatolmanyok
    {
        public static bool CheckRights_CsatolmanyokByDokumentum(Page ParentPage, string dokumentumId)
        {
            if (!String.IsNullOrEmpty(dokumentumId))
            {
                if (FelhasznaloProfil.IsAdminInSzervezet(ParentPage)) // BUG_8360
                {
                    return true;
                }

                ExecParam execParam = UI.SetExecParamDefault(ParentPage);
                EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();
                search_csatolmanyok.Dokumentum_Id.Filter(dokumentumId);

                Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(ParentPage), search_csatolmanyok);

                if (result_csatGetAll.IsError)
                {
                    Logger.Error("Hiba: EREC_CsatolmanyokService.GetAll :" + result_csatGetAll.ErrorMessage);
                    return false;
                }

                foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                {
                    string KuldKuldemeny_Id = null;
                    string IraIrat_Id = null;

                    if (row.Table.Columns.Contains("KuldKuldemeny_Id"))
                        KuldKuldemeny_Id = row["KuldKuldemeny_Id"].ToString();
                    if (row.Table.Columns.Contains("IraIrat_Id"))
                        IraIrat_Id = row["IraIrat_Id"].ToString();

                    if (!string.IsNullOrEmpty(IraIrat_Id))
                        return Iratok.CheckRights_Csatolmanyok(ParentPage, IraIrat_Id);
                    else if (!string.IsNullOrEmpty(KuldKuldemeny_Id))
                        return Kuldemenyek.CheckRights_Csatolmanyok(ParentPage, KuldKuldemeny_Id);
                }
            }

            return false;
        }

        // BUG_8852
        public static bool HasKrCsatolmany(Page ParentPage, string IratId)
        {
            if (String.IsNullOrEmpty(IratId)) { return false; }

            EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

            search.IraIrat_Id.Value = IratId;
            search.IraIrat_Id.Operator = Query.Operators.equals;

            search.WhereByManual = " and UPPER(RIGHT(KRT_Dokumentumok.FajlNev,3))='.KR'";


            Result result = service.GetAllWithExtension(UI.SetExecParamDefault(ParentPage), search);

            if (result.IsError)
            {
                // hiba:
                throw new ResultException(result);
               
            }

            return (result.Ds.Tables[0].Rows.Count > 0);

        }
    }
}