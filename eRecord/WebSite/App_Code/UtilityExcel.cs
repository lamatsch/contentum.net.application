﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using OfficeOpenXml;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;

/// <summary>
/// Summary description for UtilityTomegesIktatas
/// </summary>
namespace Contentum.eRecord.UtilityExcel
{
    public static class UtilityExcel
    {
        public static IEnumerable<T> ConvertSheetToObjects<T>(this ExcelWorksheet worksheet, CsvHelper.Configuration.CsvClassMap<T> classMap) where T : new()
        {

            var columns = classMap.PropertyMaps
             .Select(p => new
             {
                 Property = p.Data.Property,
                 Column = worksheet.Cells.Where(cell => cell.Start.Row == 1 && cell.Value.ToString() == p.Data.Names[0]).Select(cell => cell.Start.Column).FirstOrDefault()
             })
             .Where(col => col.Column > 0).ToList();

            var rows = worksheet.Cells
               .Select(cell => cell.Start.Row)
               .Distinct()
               .OrderBy(x => x);

            var collection = rows.Skip(1)
                .Select(row =>
                {
                    var tnew = new T();
                    columns.ForEach(col =>
                    {
                        //This is the real wrinkle to using reflection - Excel stores all numbers as double including int
                        var val = worksheet.Cells[row, col.Column];
                        //If it is numeric it is a double since that is how excel stores all numbers
                        if (val.Value == null)
                        {
                            col.Property.SetValue(tnew, String.Empty, null);
                            return;
                        }
                        if (col.Property.PropertyType == typeof(Int32))
                        {
                            col.Property.SetValue(tnew, val.GetValue<int>(), null);
                            return;
                        }
                        if (col.Property.PropertyType == typeof(double))
                        {
                            col.Property.SetValue(tnew, val.GetValue<double>(), null);
                            return;
                        }
                        if (col.Property.PropertyType == typeof(DateTime))
                        {
                            col.Property.SetValue(tnew, val.GetValue<DateTime>(), null);
                            return;
                        }
                        //Its a string
                        //BLG_5941
                        col.Property.SetValue(tnew, val.GetValue<string>().Replace("'", "''"), null);
                    });

                    return tnew;
                });


            //Send it back
            return collection;
        }

        public static void WriteHeader<T>(this ExcelWorksheet worksheet, CsvHelper.Configuration.CsvClassMap<T> classMap) where T : new()
        {
            List<object> header = new List<object>();

            foreach (var propertyMap in classMap.PropertyMaps.OrderBy<CsvHelper.Configuration.CsvPropertyMap, int>(p => p.Data.Index))
            {
                if (!propertyMap.Data.Ignore)
                {
                    header.Add(propertyMap.Data.Names[0]);
                }
            }

            List<object[]> data = new List<object[]>();
            data.Add(header.ToArray());

            worksheet.Cells["A1"].LoadFromArrays(data);
        }


        /// <summary>
        /// A CSV-ben megadott Igen/Nem-es érték konvertálása adatbázisbeli értékre (Igen: 1, Nem: 0)
        /// </summary>
        /// <param name="igenNemStr"></param>
        /// <returns></returns>
        public static string ConvertIgenNemErtekToDbIgenNem(string igenNemStr)
        {
            if (!String.IsNullOrEmpty(igenNemStr))
            {
                switch (igenNemStr.ToLower())
                {
                    case "igen":
                    case "i":
                    case "1":
                        return "1";
                    case "nem":
                    case "n":
                    case "0":
                        return "0";
                    default:
                        // Nem értelmezhető érték:
                        throw new ResultException(String.Format("Nem értelmezhető érték: '{0}' (Lehetséges értékek: Igen/Nem, I/N, 1/0)", igenNemStr));
                }
            }
            else
            {
                return String.Empty;
            }
        }
    }
}