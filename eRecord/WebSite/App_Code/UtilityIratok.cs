﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.BaseUtility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Common helper class for IraIratFormTab and EgyszerusitettIktatasForm
/// </summary>
public class IratForm
{
    private readonly Control _control;
    private readonly Contentum.eUtility.PageView _pageView;
    private readonly string _command;

    public bool IsUgyintezesKezdeteVisible = true;
    public bool IsIrat = false; // IraIratFormTab-ról hívva lesz true

    public IratForm(Control control, Contentum.eUtility.PageView pageView)
    {
        _control = control;
        _command = Page.Request.QueryString.Get(QueryStringVars.Command);
        _pageView = pageView;
    }

    #region Common properties for IraIratFormTab and EgyszerusitettIktatasForm

    public Page Page { get { return _control.Page; } }

    public Label Label_CalendarControl_UgyintezesKezdete;

    public ICalendarControl CalendarControl_UgyintezesKezdete;

    #endregion

    #region BLG_8826 Ügyintézési idő kezdete legyen módosítható

    public void SetUgyintezesKezdete(EREC_UgyUgyiratok erec_UgyUgyiratok, string defaultUgyintezesKezdete)
    {
        CalendarControl_UgyintezesKezdete.Text = erec_UgyUgyiratok == null || String.IsNullOrEmpty(erec_UgyUgyiratok.UgyintezesKezdete) ?
                defaultUgyintezesKezdete : erec_UgyUgyiratok.UgyintezesKezdete;
        CalendarControl_UgyintezesKezdete.Visible = IsUgyintezesKezdeteVisible;
        Label_CalendarControl_UgyintezesKezdete.Visible = IsUgyintezesKezdeteVisible;
        AllowChangeUgyintezesKezdete();
    }

    public void SetDefaultUgyintezesKezdete()
    {
        if (_command == CommandName.New && IsUgyintezesKezdeteVisible && String.IsNullOrEmpty(CalendarControl_UgyintezesKezdete.Text))
        {
            SetUgyintezesKezdete(null, DateTime.Now.ToString());
        }
    }

    public bool IsUgyiratUgyintezesKezdeteModosithato()
    {
        return Rendszerparameterek.GetBoolean(Page, Contentum.eRecord.Utility.Rendszerparameterek.UGYINT_IDO_MOD, false);
    }

    public void AllowChangeUgyintezesKezdete()
    {
        var rdOnly = _command == CommandName.View || !IsUgyiratUgyintezesKezdeteModosithato() 
            || (IsIrat && _command != CommandName.New);
        CalendarControl_UgyintezesKezdete.ReadOnly = rdOnly;
        CalendarControl_UgyintezesKezdete.Enabled = !rdOnly;
        CalendarControl_UgyintezesKezdete.Validate = !rdOnly;
    }

    public void UpdateUgyiratUgyintezesKezdete(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        if (erec_UgyUgyiratok != null && _pageView != null && IsUgyiratUgyintezesKezdeteModosithato() && IsUgyintezesKezdeteVisible)
        {
            erec_UgyUgyiratok.UgyintezesKezdete = CalendarControl_UgyintezesKezdete.Text;
            erec_UgyUgyiratok.Updated.UgyintezesKezdete = _pageView.GetUpdatedByView(CalendarControl_UgyintezesKezdete as Control);
        }
    }

    #endregion
}
