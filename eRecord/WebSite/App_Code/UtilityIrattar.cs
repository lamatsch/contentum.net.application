﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for UtilityIrattar
/// </summary>
public class UtilityIrattar
{
    public static Result GetIrattarList(Page page, ExecParam execParam, string startup, string orderBy, out EREC_UgyUgyiratokSearch search)
    {
        EREC_UgyUgyiratokService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        search = null;
        if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(page, new EREC_UgyUgyiratokSearch(true)
                , Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch);
        }
        else if (startup == Constants.Startup.FromKozpontiIrattar)
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(page, new EREC_UgyUgyiratokSearch(true)
                , Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);
        }
        else if (startup == Constants.Startup.FromSkontroIrattar)
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(page, new EREC_UgyUgyiratokSearch(true)
                , Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch);
        }

        search.OrderBy = orderBy;
        search.TopRow = UI.GetTopRow(page);

        // default szuresek:
        var defaultAllapotok = new string[0];
        if (startup == Constants.Startup.FromKozpontiIrattar)
        {
            defaultAllapotok = new string[] {
            KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
            KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert,
            KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
        };
        }
        else if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            defaultAllapotok = new string[] {
            KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott,
            KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert,
            KodTarak.UGYIRAT_ALLAPOT.Atmeneti_irattarbol_kikolcsonzott,
            KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo
        };
        }
        else if (startup == Constants.Startup.FromSkontroIrattar)
        {
            defaultAllapotok = new string[] {
            KodTarak.UGYIRAT_ALLAPOT.Skontroban,
            KodTarak.UGYIRAT_ALLAPOT.Skontrobol_elkert
        };
        }

        search.Manual_Allapot_DefaultFilter.In(defaultAllapotok);
        search.Manual_Allapot_DefaultFilter.OrGroup("99");

        //szereltek kezelése
        search.Manual_Szereles_Alatt.In(defaultAllapotok);
        search.Manual_Szereles_Alatt.OrGroup("99");

        if (startup == Constants.Startup.FromKozpontiIrattar)
        {
            string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToLower();

            // Központi irattárnál azokat hozzuk, ahol az irat helye (õrzõ) a Központi irattár:
            search.FelhasznaloCsoport_Id_Orzo.Filter(kozpontiIrattarId);

            //szûrés irattári helyre
            if (!Rendszerparameterek.GetBoolean(execParam, "IRATTAROZAS_HELY_FELH_ALTAL", true))
            {
                if (KellIrattarListaSzures(page, startup))
                {
                    var allJogosultIrattariHelyIds = Contentum.eRecord.BaseUtility.Irattar.GetAllJogosultIrattariHelyIds(execParam);
                    if (allJogosultIrattariHelyIds.Count > 0)
                    {
                        search.IrattarId.In(allJogosultIrattariHelyIds);
                    }
                    else
                    {
                        search.IrattarId.Filter(Guid.Empty.ToString());
                    }
                }
            }

            // Van-e szûrés kérés paraméterben?
            // (Csak elsõ kéréskor figyeljük)
            if (!page.IsPostBack)
            {
                string filter = page.Request.QueryString.Get(QueryStringVars.Filter);
                if (filter == Constants.JovahagyandokFilter.Kolcsonzesre)
                {
                    if (FunctionRights.GetFunkcioJog(page, "KolcsonzesJovahagyas"))
                    {
                        Ugyiratok.SetSearchObjectTo_JovahagyandokKolcsonzesre(search, page);
                    }
                    else
                    {
                        // ne lásson semmit
                        search.Id.IsNull();
                    }
                    Search.SetSearchObject_CustomSessionName(page, search, Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);
                }
                else if (filter == Constants.EngedelyezettKikeronLevo)
                {
                    if (FunctionRights.GetFunkcioJog(page, "IrattarKolcsonzesKiadasa"))
                    {
                        Ugyiratok.SetSearchObjectTo_EngedelyezettKikeronLevo(search, page);
                    }
                    else
                    {
                        // ne lásson semmit
                        search.Id.IsNull();
                    }
                    Search.SetSearchObject_CustomSessionName(page, search, Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);
                }
            }
        }
        else if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            if (String.IsNullOrEmpty(search.Csoport_Id_Felelos.Value))
            {
                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToLower();

                if (!String.IsNullOrEmpty(kozpontiIrattarId))
                {
                    search.Csoport_Id_Felelos.NotEquals(kozpontiIrattarId);
                }
            }

            if (!Rendszerparameterek.GetBoolean(execParam, "IRATTAROZAS_HELY_FELH_ALTAL", true))
            {
                if (KellIrattarListaSzures(page, startup))
                {
                    var allIrattar = Contentum.eRecord.BaseUtility.Irattar.GetAllIrattarIds(execParam);
                    if (allIrattar.Count > 0)
                    {
                        search.FelhasznaloCsoport_Id_Orzo.In(allIrattar);
                    }
                }
            }

            //LZS - BUG_6622
            search.Manual_SzereltekNelkul.NotEquals("60"); //Szerelt állapot

            //Van - e szûrés kérés paraméterben ?
            // (Csak elsõ kéréskor figyeljük)
            if (!page.IsPostBack)
            {
                string filter = page.Request.QueryString.Get(QueryStringVars.Filter);
                if (filter == Constants.JovahagyandokFilter.Kikeresre)
                {
                    if (FunctionRights.GetFunkcioJog(page, "AtmenetiIrattarKikeresJovahagyas"))
                    {
                        Ugyiratok.SetSearchObjectTo_JovahagyandokKikeresre(search, page);
                    }
                    else
                    {
                        // ne lásson semmit
                        search.Id.IsNull();
                    }
                    Search.SetSearchObject_CustomSessionName(page, search, Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch);
                }
            }
        }
        else if (startup == Constants.Startup.FromSkontroIrattar)
        {
            if (String.IsNullOrEmpty(search.Csoport_Id_Felelos.Value))
            {
                string skontroIrattarosId = KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam).Obj_Id.ToLower();
                search.Csoport_Id_Felelos.FilterIfNotEmpty(skontroIrattarosId);
            }

            // BUG_3842: skontróban ne jelenjenek meg a szereltek
            search.IsSzereltekExcluded = true;
        }

        return service.GetAllWithExtensionAndJogosultak(execParam, search, true);
    }

    static bool KellIrattarListaSzures(Page page, string startup)
    {
        //Rendszerparaméter
        if (Rendszerparameterek.GetBoolean(page, "IRATTAROZAS_LISTA_SZURES_IRATTAROS_ALTAL", false))
        {
            //Irattáros
            return IsIrattarosFelhasznalo(page, startup);
        }
        else
        {
            return false;
        }
    }

    static bool IsIrattarosFelhasznalo(Page page, string startup)
    {
        string funkcioKod = "";
        if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            funkcioKod = "AtmenetiIrattarAtvetel";
        }
        else if (startup == Constants.Startup.FromKozpontiIrattar)
        {
            funkcioKod = "KozpontiIrattarAtvetel";
        }
        else
        {
            return false;
        }

        return FunctionRights.GetFunkcioJog(page, funkcioKod);
    }
}