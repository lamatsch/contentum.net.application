﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUIControls;
using Contentum.eUtility;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

/// <summary>
/// Local Utility NameSpace
/// </summary>
// &&
namespace Contentum.eRecord.Utility
{
    // EZT NEM KELL FELTENNI A DLL-ekbe:
    public partial class Kuldemenyek : Contentum.eRecord.BaseUtility.Kuldemenyek
    {
        public static bool CheckRights_Csatolmanyok(Page ParentPage, string id)
        {
            return CheckRights_Csatolmanyok(ParentPage, id, true);
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, string id, bool checkRightForObj)
        {
            EREC_KuldKuldemenyek erec_KuldKuldemenyek = null;

            if (!String.IsNullOrEmpty(id))
            {
                ExecParam execParam = UI.SetExecParamDefault(ParentPage);
                execParam.Record_Id = id;
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

                Result result = null;
                if (checkRightForObj == true)
                {
                    result = service.GetWithRightCheck(execParam, 'O');
                }
                else
                {
                    result = service.Get(execParam);
                }
                if (!result.IsError && result.Record != null)
                {
                    erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result.Record;

                    if (erec_KuldKuldemenyek != null)
                    {
                        return Kuldemenyek.CheckRights_Csatolmanyok(ParentPage, erec_KuldKuldemenyek);
                    }
                }
            }

            return false;
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, EREC_KuldKuldemenyek erec_KuldKuldemenyek, bool checkRightForObj)
        {
            if (erec_KuldKuldemenyek != null)
            {
                if (checkRightForObj == true)
                {
                    return CheckRights_Csatolmanyok(ParentPage, erec_KuldKuldemenyek.Id);
                }
                else
                {
                    return CheckRights_Csatolmanyok(ParentPage, erec_KuldKuldemenyek);
                }
            }
            return false;
        }

        public static bool CheckRights_Csatolmanyok(Page ParentPage, EREC_KuldKuldemenyek erec_KuldKuldemenyek)
        {
            EREC_IraIratok erec_IraIratok = null;
            if (erec_KuldKuldemenyek != null)
            {
                if (FelhasznaloProfil.IsAdminInSzervezet(ParentPage)) // BUG_8360
                {
                    return true;
                }

                if (erec_KuldKuldemenyek.Allapot != KodTarak.KULDEMENY_ALLAPOT.Iktatva) // csak ha már iktatva van, akkor van hozzá irat
                {
                    // küldemény minősítés ellenőrzése (azonos az irattal)
                    if (!Iratok.IsConfidential(ParentPage, erec_KuldKuldemenyek.Minosites)) //(erec_KuldKuldemenyek.Minosites != KodTarak.IRAT_MINOSITES.BelsoNemNyilvanos)
                    {
                        return true;
                    }

                    // őrző vizsgálata
                    string Felhasznalo_Id = FelhasznaloProfil.FelhasznaloId(ParentPage);

                    if (erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo == Felhasznalo_Id)
                    {
                        return true;
                    }

                    if (erec_KuldKuldemenyek.Csoport_Id_Felelos == Felhasznalo_Id)
                    {
                        return true;
                    }

                    // aktuális csoport dolgozói között van-e az őrző, ha az aktuális felhasználó vezető vagy szervezet asszisztense
                    if (FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(ParentPage) == KodTarak.CsoprttagsagTipus.vezeto
                     || FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(ParentPage) == KodTarak.CsoprttagsagTipus.SzervezetAsszisztense) /*<-- BLG_577*/
                    {
                        ExecParam execParam_csoport = UI.SetExecParamDefault(ParentPage);
                        Contentum.eAdmin.Service.KRT_CsoportokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                        KRT_CsoportokSearch krt_CsoportokSearch = new KRT_CsoportokSearch();
                        krt_CsoportokSearch.Tipus.Filter(KodTarak.CSOPORTTIPUS.Dolgozo);

                        krt_CsoportokSearch.Id.Filter(erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Orzo);

                        // csak a létezést vizsgáljuk
                        krt_CsoportokSearch.TopRow = 1;

                        string FelhasznaloSzervezet_Id = FelhasznaloProfil.FelhasznaloSzerverzetId(ParentPage);
                        Result result_csoport = service.GetAllSubCsoport(execParam_csoport, FelhasznaloSzervezet_Id, true, krt_CsoportokSearch);

                        if (!result_csoport.IsError && result_csoport.GetCount > 0)
                        {
                            return true;
                        }
                    }

                    List<string> lstObj_Ids = new List<string>();
                    // objektumon keresztüli jogosultság ellenőrzése a küldeményre
                    Contentum.eAdmin.Service.KRT_JogosultakService service_jogosultak = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_JogosultakService();
                    ExecParam execParam_jogosultak = UI.SetExecParamDefault(ParentPage);
                    KRT_JogosultakSearch search_jogosultak = new KRT_JogosultakSearch();
                    search_jogosultak.Obj_Id.Filter(erec_KuldKuldemenyek.Id);
                    search_jogosultak.Csoport_Id_Jogalany.Filter(Felhasznalo_Id);

                    search_jogosultak.TopRow = 1;

                    Result result_jogosultak = service_jogosultak.GetAll(execParam_jogosultak, search_jogosultak);
                    if (!result_jogosultak.IsError && result_jogosultak.GetCount == 1)
                    {
                        return true;
                    }


                } // nem iktatott küldemény
                else
                {
                    ExecParam execParam_irat = UI.SetExecParamDefault(ParentPage);
                    EREC_IraIratokService service_irat = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    EREC_IraIratokSearch search_irat = new EREC_IraIratokSearch();
                    search_irat.KuldKuldemenyek_Id.Filter(erec_KuldKuldemenyek.Id);

                    Result result_irat = service_irat.GetAllWithExtensionAndJogosultak(execParam_irat, search_irat, true);

                    if (!result_irat.IsError && result_irat.GetCount > 0)
                    {
                        DataRow row_irat = result_irat.Ds.Tables[0].Rows[0];

                        erec_IraIratok = new EREC_IraIratok();

                        // objektum feltöltése a DataRow alapján
                        System.Reflection.PropertyInfo[] Properties = erec_IraIratok.GetType().GetProperties();
                        foreach (System.Reflection.PropertyInfo P in Properties)
                        {
                            if (row_irat.Table.Columns.Contains(P.Name))
                            {
                                if (!(row_irat.IsNull(P.Name)))
                                {
                                    P.SetValue(erec_IraIratok, row_irat[P.Name].ToString(), null);
                                }
                            }
                        }

                        return Iratok.CheckRights_Csatolmanyok(ParentPage, erec_IraIratok);
                    }
                }
            }
            return false;
        }

        public static void SetIktatas(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            image.ToolTip = Resources.Buttons.EmailIktatas_BejovoIratIktatasa;
            bool hasBejovoIratIktatas = FunctionRights.GetFunkcioJog(image.Page, CommandName.BejovoIratIktatas);
            bool hasEmailIktatas = FunctionRights.GetFunkcioJog(image.Page, "EmailIktatas");
            if (!hasBejovoIratIktatas && !hasEmailIktatas)
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }
            else
            {
                image.Enabled = true;
            }

            if (hasBejovoIratIktatas && !hasBejovoIratIktatas)
            {
                image.ToolTip = Resources.Buttons.Bejovo_irat_iktatasa;
            }
            if (hasEmailIktatas && !hasBejovoIratIktatas)
            {
                image.ToolTip = Resources.Buttons.EmailIktatasa;
            }

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }
            ExecParam execparam = UI.SetExecParamDefault(image.Page);
            ErrorDetails errorDetail = null;
            if (Iktathato(execparam, statusz, out errorDetail))
            {
                string emailBoritekId = String.Empty;
                if (hasEmailIktatas && statusz.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.E_mail)
                {
                    EREC_eMailBoritekokService _EREC_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
                    EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch = new EREC_eMailBoritekokSearch();
                    _EREC_eMailBoritekokSearch.KuldKuldemeny_Id.Filter(KuldemenyId);
                    Result res = _EREC_eMailBoritekokService.GetAll(execparam, _EREC_eMailBoritekokSearch);
                    if (!res.IsError && res.GetCount > 0)
                    {
                        emailBoritekId = res.Ds.Tables[0].Rows[0]["Id"].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(emailBoritekId))
                {
                    image.Enabled = hasEmailIktatas;
                    image.OnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.EmailBoritekokId + "=" + emailBoritekId
                        + "&" + QueryStringVars.KuldemenyId + "=" + KuldemenyId
                        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    image.Enabled = hasBejovoIratIktatas;
                    // TODO: Ide kell Andris iktato kepernyojenek meghivasa
                    image.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.KuldemenyId + "=" + KuldemenyId
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
            }
            else
            {
                image.OnClientClick = "alert('" + Resources.Error.UINemIktathatoKuldemeny
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page)
                    + "'); return false;";
            }

        }

        // CR 3110: Iktatás Munkapéldányra Rendszerparaméter alapján (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapján
        public static void SetIktatasMunkapeldany(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            // CR3221 Szignálás(előkészítés) kezelés 
            image.ToolTip = Resources.Buttons.SzignalasElokeszites_Munkapeldany_Iktatas;
            bool hasBejovoIratIktatas = FunctionRights.GetFunkcioJog(image.Page, CommandName.BejovoIratIktatas);
            bool hasMunkapeldanyIktatas = FunctionRights.GetFunkcioJog(image.Page, CommandName.MunkapeldanyBeiktatas_Bejovo);
            //            bool hasEmailIktatas = FunctionRights.GetFunkcioJog(image.Page, "EmailIktatas");
            if (!hasBejovoIratIktatas && !hasMunkapeldanyIktatas)
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }
            else
            {
                image.Enabled = true;
            }
            // Ez mi ez?
            //if (hasBejovoIratIktatas && !hasBejovoIratIktatas)
            //{
            //    image.ToolTip = Resources.Buttons.Bejovo_irat_iktatasa;
            //}
            //if (hasEmailIktatas && !hasBejovoIratIktatas)
            //{
            //    image.ToolTip = Resources.Buttons.EmailIktatasa;
            //}

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }
            ExecParam execparam = UI.SetExecParamDefault(image.Page);
            ErrorDetails errorDetail = null;
            if (Iktathato(execparam, statusz, out errorDetail))
            {
                // Megnézni hogy ez kell-e munkapéldánynál 
                //string emailBoritekId = String.Empty;
                //if (hasEmailIktatas && statusz.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.E_mail)
                //{
                //    EREC_eMailBoritekokService _EREC_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
                //    EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch = new EREC_eMailBoritekokSearch();
                //    _EREC_eMailBoritekokSearch.KuldKuldemeny_Id.Value = KuldemenyId;
                //    _EREC_eMailBoritekokSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;
                //    Result res = _EREC_eMailBoritekokService.GetAll(execparam, _EREC_eMailBoritekokSearch);
                //    if (!res.IsError && res.Ds.Tables[0].Rows.Count > 0)
                //    {
                //        emailBoritekId = res.Ds.Tables[0].Rows[0]["Id"].ToString();
                //    }
                //}
                //if (!String.IsNullOrEmpty(emailBoritekId))
                //{
                //    image.Enabled = hasEmailIktatas;
                //    image.OnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                //        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.EmailBoritekokId + "=" + emailBoritekId
                //        + "&" + QueryStringVars.KuldemenyId + "=" + KuldemenyId
                //        , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                //}
                //else
                //{
                //    image.Enabled = hasBejovoIratIktatas;
                //    // TODO: Ide kell Andris iktato kepernyojenek meghivasa
                //    image.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                //       , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.KuldemenyId + "=" + KuldemenyId
                //       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                //}
                image.Enabled = hasMunkapeldanyIktatas;
                // TODO: Ide kell Andris iktato kepernyojenek meghivasa
                image.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.MunkapeldanyBeiktatas_Bejovo + "&" + QueryStringVars.KuldemenyId + "=" + KuldemenyId
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                image.OnClientClick = "alert('" + Resources.Error.UINemIktathatoKuldemeny
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page)
                    + "'); return false;";
            }

        }

        public static void SetIktatasMegtagadas(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, CommandName.BejovoIratIktatas))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }
            ExecParam execparam = UI.SetExecParamDefault(image.Page, new ExecParam());

            if (IktatasMegtagadhato(execparam, statusz))
            {
                image.OnClientClick = JavaScripts.SetOnClientClick("EmailIktatasMegtagadasForm.aspx",
                                        QueryStringVars.Command + "=" + CommandName.IktatasMegtagadas + "&" +
                                        QueryStringVars.KuldemenyId + "=" + KuldemenyId + "&" +
                                        QueryStringVars.RefreshCallingWindow + "=1",
                                        Defaults.PopupWidth, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID,
                                        EventArgumentConst.refreshKuldemenyekPanel);
            }
            else
            {
                image.OnClientClick = JavaScripts.SetOnClientClickIktatasNemTagadhatoMegKuldemenyAlert();
            }

        }


        public static void SetLezaras(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Kuldemeny" + CommandName.Lezaras))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }

            ExecParam execparam = UI.SetExecParamDefault(image.Page, new ExecParam());
            ErrorDetails errorDetail = null;

            if (Lezarhato(execparam, statusz, out errorDetail))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickLezarasConfirm();
            }
            else
            {
                string javascript = "alert('" + Resources.Error.UINemLezarhatoKuldemeny
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page) + "'); return false;";

                image.OnClientClick = javascript;
            }

        }

        public static void SetAtadasraKijeloles(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "KuldemenyAtadas"))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }
            ExecParam execparam = UI.SetExecParamDefault(image.Page);
            ErrorDetails errorDetail;
            if (AtadasraKijelolheto(execparam, statusz, out errorDetail))
            {
                KuldKuldemenyekUpdatePanel.Page.Session["SelectedKuldemenyIds"] = KuldemenyId;

                KuldKuldemenyekUpdatePanel.Page.Session["SelectedBarcodeIds"] = null;
                KuldKuldemenyekUpdatePanel.Page.Session["SelectedUgyiratIds"] = null;
                KuldKuldemenyekUpdatePanel.Page.Session["SelectedIratPeldanyIds"] = null;
                KuldKuldemenyekUpdatePanel.Page.Session["SelectedDosszieIds"] = null;

                image.OnClientClick = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                //image.OnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx"
                //   , QueryStringVars.KuldemenyId + "=" + KuldemenyId
                //   , Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshKuldemenyekPanel);
            }
            else
            {
                image.OnClientClick = "alert('" + Resources.Error.ErrorCode_52381
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page)
                    + "'); return false;"; ;
            }

        }


        public static void SetSzignalas(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "KuldemenySzignalas"))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return;
            }

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return;
            }
            ExecParam execparam = UI.SetExecParamDefault(image.Page);
            ErrorDetails errorDetail = null;

            if (Kuldemenyek.Szignalhato(statusz, execparam, out errorDetail))
            {
                image.OnClientClick = JavaScripts.SetOnClientClick("SzignalasForm.aspx",
                   QueryStringVars.KuldemenyId + "=" + KuldemenyId
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight, KuldKuldemenyekUpdatePanel.ClientID, EventArgumentConst.refreshKuldemenyekPanel);
            }
            else
            {
                image.OnClientClick = "alert('" + Resources.Error.ErrorCode_52582
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page)
                    + "'); return false;";
            }

        }



        //public static Boolean Lezarhato(Statusz statusz)
        //{
        //    if (statusz.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        public static void Lezaras(String KuldemenyId, Page ParentPage, eUIControls.eErrorPanel EErrorPanel)
        {
            if (FunctionRights.GetFunkcioJog(ParentPage, "Kuldemeny" + CommandName.Lezaras))
            {
                if (!String.IsNullOrEmpty(KuldemenyId))
                {
                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execparam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                    execparam.Record_Id = KuldemenyId;
                    Result result = service.Lezaras(execparam);
                    if (result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                        //ErrorUpdatePanel.Update();
                    }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(ParentPage);
            }
        }


        public static void Atvetel(String KuldemenyId, Page page, eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(KuldemenyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkciójogosultság-ellenőrzés:
            if (!FunctionRights.GetFunkcioJog(page, "KuldemenyAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {

                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());

                Result result = service.Atvetel(execParam, KuldemenyId);
                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                    if (errorUpdatePanel != null)
                    {
                        errorUpdatePanel.Update();
                    }
                }
            }
        }

        public static void Visszakuldes(String kuldemenyId, String visszakuldesIndoka, Page page, eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
        {
            if (String.IsNullOrEmpty(kuldemenyId) || page == null || errorPanel == null)
            {
                return;
            }

            // Funkciójogosultság-ellenőrzés:
            if (!FunctionRights.GetFunkcioJog(page, "KuldemenyAtvetel"))
            {
                UI.DisplayDontHaveFunctionRights(errorPanel, errorUpdatePanel);
                return;
            }
            else
            {
                if (!String.IsNullOrEmpty(kuldemenyId))
                {
                    EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                    ExecParam execParam = UI.SetExecParamDefault(page, new ExecParam());
                    //execParam.Record_Id = kuldemenyId;

                    Result result = service.VisszakuldesByObject(execParam, kuldemenyId, Constants.TableNames.EREC_KuldKuldemenyek, visszakuldesIndoka);
                    if (result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                        if (errorUpdatePanel != null)
                        {
                            errorUpdatePanel.Update();
                        }
                    }
                }
            }
        }


        //CR 2121
        public static bool SetSztorno(String KuldemenyId, Statusz statusz, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            if (!FunctionRights.GetFunkcioJog(image.Page, "Kuldemeny" + CommandName.Sztorno))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");

                return false;
            }

            if (String.IsNullOrEmpty(KuldemenyId) || statusz == null)
            {
                image.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                return false;
            }

            ExecParam execparam = UI.SetExecParamDefault(image.Page);
            ErrorDetails errorDetail = null;

            if (Sztornozhato(execparam, statusz, out errorDetail))
            {
                image.OnClientClick = JavaScripts.SetOnClientClickSztornoConfirm();
                return true;
            }
            else
            {
                image.OnClientClick = "alert('" + Resources.Error.UINemSztornirozhatoKuldemeny
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, image.Page)
                    + "'); return false;";
            }

            return false;
        }

        //CR 2129
        public static void Sztorno(String KuldemenyId, String SztornozasIndoka, Page ParentPage, eErrorPanel EErrorPanel)
        {
            if (FunctionRights.GetFunkcioJog(ParentPage, "Kuldemeny" + CommandName.Sztorno))
            {
                if (!String.IsNullOrEmpty(KuldemenyId))
                {
                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execparam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                    execparam.Record_Id = KuldemenyId;
                    Result result = service.Sztorno(execparam, SztornozasIndoka);
                    if (result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                        //ErrorUpdatePanel.Update();
                    }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(ParentPage);
            }
        }

        public static void Sztorno(String KuldemenyId, Page ParentPage, eErrorPanel EErrorPanel)
        {
            Sztorno(KuldemenyId, String.Empty, ParentPage, EErrorPanel);
        }

        public static string Bontas(EREC_KuldKuldemenyek erec_KuldKuldemenyek, Page ParentPage, eErrorPanel EErrorPanel)
        {
            if (FunctionRights.GetFunkcioJog(ParentPage, "Kuldemeny" + CommandName.Bontas))
            {
                if (!String.IsNullOrEmpty(erec_KuldKuldemenyek.KuldKuldemeny_Id_Szulo))
                {
                    EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

                    ExecParam execParam = UI.SetExecParamDefault(ParentPage, new ExecParam());
                    execParam.Record_Id = erec_KuldKuldemenyek.KuldKuldemeny_Id_Szulo;

                    Result result = service.Bontas(execParam, erec_KuldKuldemenyek);

                    if (!result.IsError)
                    {
                        return (result.Record as ErkeztetesIktatasResult).KuldemenyAzonosito;
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel, result);
                    }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(ParentPage);
            }
            return "";
        }

        public static void SetZarolas(String GridViewClientId, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            // TODO: zarolas

            if (!FunctionRights.GetFunkcioJog(image.Page, "Kuldemeny" + CommandName.Lock))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");
                return;
            }

            image.OnClientClick = JavaScripts.SetOnClientClickLockConfirm(GridViewClientId);
        }

        public static void SetZarolas(ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            // TODO: zarolas

            if (!FunctionRights.GetFunkcioJog(image.Page, "Kuldemeny" + CommandName.Lock))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");
                return;
            }

            image.OnClientClick = JavaScripts.SetOnClientClickLockConfirm();
        }

        public static void Zarolas(GridView KuldKuldemenyekGridView, Page ParentPage, eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            LockManager.LockSelectedGridViewRecords(KuldKuldemenyekGridView, "EREC_KuldKuldemenyek"
                , "KuldemenyLock", "KuldemenyForceLock"
                 , ParentPage, EErrorPanel, ErrorUpdatePanel);
        }

        public static void Zarolas(String KuldemenyId, Page ParentPage, eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            LockManager.LockSelectedGridViewRecords(KuldemenyId, "EREC_KuldKuldemenyek"
                , "KuldemenyLock", "KuldemenyForceLock"
                 , ParentPage, EErrorPanel, ErrorUpdatePanel);
        }

        public static void SetZarolas_feloldasa(String GridViewClientId, ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            // TODO: zarolas_feloldasa
            if (!FunctionRights.GetFunkcioJog(image.Page, "Kuldemeny" + CommandName.Lock))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");
                return;
            }

            image.OnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(GridViewClientId);
        }

        public static void SetZarolas_feloldasa(ImageButton image, UpdatePanel KuldKuldemenyekUpdatePanel)
        {
            // TODO: zarolas_feloldasa
            if (!FunctionRights.GetFunkcioJog(image.Page, "Kuldemeny" + CommandName.Lock))
            {
                image.Enabled = false;
                //image.CssClass = (image.Enabled ? "highlightit" : "disableditem");
                return;
            }

            image.OnClientClick = JavaScripts.SetOnClientClickUnlockConfirm();

        }

        public static void Zarolas_feloldasa(GridView KuldKuldemenyekGridView, Page ParentPage, eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            LockManager.UnlockSelectedGridViewRecords(KuldKuldemenyekGridView, "EREC_KuldKuldemenyek"
                , "KuldemenyLock", "KuldemenyForceLock"
                , ParentPage, EErrorPanel, ErrorUpdatePanel);
        }

        public static void Zarolas_feloldasa(String KuldemenyId, Page ParentPage, eErrorPanel EErrorPanel, UpdatePanel ErrorUpdatePanel)
        {
            LockManager.UnlockSelectedGridViewRecords(KuldemenyId, "EREC_KuldKuldemenyek"
                , "KuldemenyLock", "KuldemenyForceLock"
                , ParentPage, EErrorPanel, ErrorUpdatePanel);
        }


        /// <summary>
        /// A keresési objektum beállítása a csak iktatható küldemények kereséséhez
        /// </summary>
        /// <param name="searchObject"></param>
        public static void SetSearchObjectTo_Iktathatok(EREC_KuldKuldemenyekSearch searchObject)
        {
            // Iktatható: iktatási kötelezettség==Iktatandó ÉS (Állapot==Érkeztetett Vagy Szignált Vagy Átvett Vagy Felszabadított)

            searchObject.Manual_Iktathatok_IktatniKell.Filter(KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando);

            searchObject.Manual_Iktathatok_Allapot.In(new string[] {
                KodTarak.KULDEMENY_ALLAPOT.Erkeztetve,
                KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt,
                KodTarak.KULDEMENY_ALLAPOT.Szignalva,
                KodTarak.KULDEMENY_ALLAPOT.Atveve
            });
        }


        /// <summary>
        /// (Küldemények listájához) alap szűrési feltétel beállítása:
        /// (Nem kell hozni a kimenő küldeményeket)
        /// </summary>
        /// <param name="erec_UgyUgyiratokSearch"></param>
        public static void SetDefaultFilter(EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch)
        {
            erec_KuldKuldemenyekSearch.Manual_PostazasIranya_DefaultFilter.NotIn(new string[] {
                KodTarak.POSTAZAS_IRANYA.Kimeno
            });
        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_KuldKuldemenyek, Id, null, null);
                return false;
            }

            EREC_KuldKuldemenyek erec_KuldKuldemeny = (EREC_KuldKuldemenyek)result.Record;
            Kuldemenyek.Statusz statusz = GetAllapotByBusinessDocument(erec_KuldKuldemeny);

            return CheckAtadasraKijelolhetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckAtadasraKijelolhetoWithFunctionRight(Page page, Kuldemenyek.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Átadható?
            bool atadasraKijelolheto = false;

            if (Kuldemenyek.AtadasraKijelolheto(UI.SetExecParamDefault(page), statusz, out errorDetail) == true)
            {
                if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "KuldemenyAtadas"))
                {
                    //"Önnek nincs joga küldeményt átadni."
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52867, Statusz.ColumnTypes.CsopId_Felelos);
                }
                else
                {
                    atadasraKijelolheto = true;
                }
            }

            return atadasraKijelolheto;
        }
        #region BLG1880 - expediált állapotú tételeknek lehet csak tömeges ragszámot osztani
        public static void KuldemenyekGridView_RowDataBound_CheckExpedialt(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Expediált állapot ellenőrzése
                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);

                bool Expedialt = false;
                ErrorDetails errorDetail = null;

                if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Expedialt)
                {
                    Expedialt = true;
                    // ha nem a felhasználó az őrző:
                    ExecParam execParam = UI.SetExecParamDefault(page);
                    if (statusz.FelhCsopId_Orzo != Csoportok.GetFelhasznaloSajatCsoportId(execParam))
                    {
                        errorDetail = statusz.CreateErrorDetail("A küldeménynek nem Ön az őrzője.", Statusz.ColumnTypes.FelhCsopId_Orzo);
                        Expedialt = false;
                    }
                }
                else
                {
                    errorDetail = statusz.CreateErrorDetail("A Küldemény állapota nem megfelelő.", Statusz.ColumnTypes.Allapot);
                }

                UI.SetRowCheckboxAndInfo(Expedialt, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, page);
            }
        }
        #endregion

        /// <summary>
        /// Ha nem jelölhető ki átadásra, megjelöli az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void KuldemenyekGridView_RowDataBound_CheckAtadasraKijeloles(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Átadásra kijelölhetőség ellenőrzése
                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);
                ErrorDetails errorDetail;
                bool atadasraKijelolheto = CheckAtadasraKijelolhetoWithFunctionRight(page, statusz, out errorDetail);
                UI.SetRowCheckboxAndInfo(atadasraKijelolheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, page);
            }
        }

        public static bool CheckAtvehetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_KuldKuldemenyek, Id, null, null);
                return false;
            }

            EREC_KuldKuldemenyek erec_KuldKuldemeny = (EREC_KuldKuldemenyek)result.Record;
            Kuldemenyek.Statusz statusz = GetAllapotByBusinessDocument(erec_KuldKuldemeny);

            return CheckAtvehetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckAtvehetoWithFunctionRight(Page page, Kuldemenyek.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Átvehető?
            bool atveheto = false;

            if (Kuldemenyek.Atveheto(UI.SetExecParamDefault(page), statusz, out errorDetail) == true)
            {
                //Felelős ellenőrzéshez kell
                List<string> FelettesSzervezetList = Csoportok.GetFelhasznaloFelettesCsoportjai(page, FelhasznaloProfil.FelhasznaloId(page));

                if (Csoportok.IsMember(FelhasznaloProfil.FelhasznaloId(page), FelettesSzervezetList, statusz.CsopId_Felelos))
                {
                    if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "KuldemenyAtvetel"))
                    {
                        //"Önnek nincs joga küldeményt átvenni."
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52852, Statusz.ColumnTypes.CsopId_Felelos);
                    }
                    else
                    {
                        atveheto = true;
                    }
                }
                else
                {
                    //"Ön nem tagja a küldemény kezelő csoportjának."
                    // ErrorCode_52393	A küldemény nem vehető át, mert Ön nem tagja a küldemény felelős csoportjának!
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52393, Statusz.ColumnTypes.CsopId_Felelos);
                }

                if (FelettesSzervezetList.Contains(statusz.CsopId_Felelos) && !Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "AtvetelSzervezettol"))
                {
                    atveheto = false;
                    //"Önnek nincs joga szervezetnek küldött küldeményt átvenni."
                    errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52236, Statusz.ColumnTypes.FelhCsopId_Orzo);
                }

                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt)
                    {
                        atveheto = false;
                        // Ez a küldemény már Önnél van!
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52862, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszaküldés
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);
                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount == 0)
                        {
                            atveheto = false;
                            // Ez a küldemény már Önnél van!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52862, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        }
                    }
                }
            }

            return atveheto;
        }

        /// <summary>
        /// TUK visszavételnél ellenőrzés elvégzése
        /// </summary>
        /// <param name="page"></param>
        /// <param name="statusz"></param>
        /// <param name="errorDetail"></param>
        /// <returns></returns>
        public static bool CheckVisszavehetoWithFunctionRight(Page page, Kuldemenyek.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Átvehető?
            bool atveheto = true;

            if (Kuldemenyek.Atveheto(UI.SetExecParamDefault(page), statusz, out errorDetail) == true)
            {

                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt)
                    {
                        atveheto = false;
                        // Ez a küldemény már Önnél van!
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52862, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszaküldés
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);
                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);

                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount == 0)
                        {
                            atveheto = false;
                            // Ez a küldemény már Önnél van!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52862, Statusz.ColumnTypes.FelhCsopId_Orzo);
                        }
                    }
                }
            }

            return atveheto;
        }

        /// <summary>
        /// TÜK visszavételnél ellenőrzi, hogy átvehető -e a sor. Itt csak státuszt ellenőrzünk, és azt, hogy esetleg már a TÜK kezelőnél van -e a küldemény
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void KuldemenyekGridView_RowDataBound_CheckVisszavehetoTUK(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Átvehető-e?
                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);
                ErrorDetails errorDetail;
                bool atveheto = CheckVisszavehetoWithFunctionRight(page, statusz, out errorDetail);
                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, page);
            }
        }

        /// <summary>
        /// Ha nem vehető át, megjelöli az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void KuldemenyekGridView_RowDataBound_CheckAtveheto(GridViewRowEventArgs e, Page page, List<string> FelettesSzervezetList)
        {
            if (e == null || page == null || FelettesSzervezetList == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Átvehető-e?
                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);
                ErrorDetails errorDetail;
                bool atveheto = CheckAtvehetoWithFunctionRight(page, statusz, out errorDetail);
                UI.SetRowCheckboxAndInfo(atveheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, page);
            }
        }

        #region CheckVisszakuldheto
        public static bool CheckVisszakuldhetoWithFunctionRight(Page page, string Id, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(page);
            execParam.Record_Id = Id;
            Result result = service.Get(execParam);

            if (result.IsError)
            {
                errorDetail = new ErrorDetails(result.ErrorMessage, Constants.TableNames.EREC_KuldKuldemenyek, Id, null, null);
                return false;
            }

            EREC_KuldKuldemenyek erec_KuldKuldemeny = (EREC_KuldKuldemenyek)result.Record;

            Kuldemenyek.Statusz statusz = GetAllapotByBusinessDocument(erec_KuldKuldemeny);

            return CheckVisszakuldhetoWithFunctionRight(page, statusz, out errorDetail);
        }

        public static bool CheckVisszakuldhetoWithFunctionRight(Page page, Kuldemenyek.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            // Visszaküldhető?
            bool visszakuldheto = Kuldemenyek.Visszakuldheto(UI.SetExecParamDefault(page), statusz, out errorDetail);

            if (visszakuldheto)
            {
                if (statusz.CsopId_Felelos == FelhasznaloProfil.FelhasznaloId(page) && statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                {
                    if (statusz.Allapot != KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt)
                    {
                        visszakuldheto = false;
                        // Ez a küldemény már Önnél van!
                        errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_52862, Statusz.ColumnTypes.FelhCsopId_Orzo);
                    }
                    else
                    {
                        // van-e visszaküldés
                        EREC_IraKezbesitesiTetelekService service_kezbtetelek = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                        ExecParam execParam_kezbtetelek = UI.SetExecParamDefault(page);

                        EREC_IraKezbesitesiTetelekSearch search_kezbtetelek = new EREC_IraKezbesitesiTetelekSearch();
                        search_kezbtetelek.Allapot.Filter(KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott);
                        search_kezbtetelek.Obj_Id.Filter(statusz.Id);
                        Result result_kezbtetelek = service_kezbtetelek.GetAll(execParam_kezbtetelek, search_kezbtetelek);

                        if (result_kezbtetelek.IsError || result_kezbtetelek.GetCount > 0)
                        {
                            visszakuldheto = false;
                            // A tétel nem küldhető vissza, mert azt visszaküldték Önnek, mint eredeti feladónak!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53705, Statusz.ColumnTypes.CsopId_Felelos);
                        }
                    }
                }
                else
                {
                    if (statusz.FelhCsopId_Orzo == FelhasznaloProfil.FelhasznaloId(page))
                    {
                        if (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Tovabbitas_alatt)
                        {
                            visszakuldheto = false;
                            // A tétel nem küldhető vissza, mert azt Ön adta át és csak a címzett küldheti vissza Önnek!
                            errorDetail = statusz.CreateErrorDetail(Resources.Error.ErrorCode_53702, Statusz.ColumnTypes.CsopId_Felelos);
                        }
                    }
                }

                if (visszakuldheto)
                {
                    visszakuldheto = CheckAtvehetoWithFunctionRight(page, statusz, out errorDetail);
                    if (errorDetail != null)
                    {
                        // A tétel nem küldhető vissza, mert csak az átvehető tételek visszaküldése megengedett!
                        errorDetail.Message = String.Format("{0}\\n{1}", Resources.Error.ErrorCode_53708, errorDetail.Message ?? "");
                    }
                }
            }

            return visszakuldheto;
        }
        #endregion CheckVisszakuldheto

        #region PostazasEFeladoJegyzek
        public static bool CheckModosithatoPostazashozWithFunctionRight(Page page, Kuldemenyek.Statusz statusz, out ErrorDetails errorDetail)
        {
            errorDetail = null;

            bool modosithato = false;

            if (!Contentum.eUtility.FunctionRights.GetFunkcioJog(page, "Postazas"))
            {
                //Nincs megfelelő jogosultsága a művelet végrehajtásához!
                //TODO: "Önnek nincs joga postázott küldeményt módosítani."
                errorDetail = statusz.CreateErrorDetail(Resources.Error.UIDontHaveFunctionRights, Statusz.ColumnTypes.CsopId_Felelos);
            }
            else
            {
                modosithato = true;
            }

            if (modosithato)
            {
                //// Módosítható?
                //modosithato = Kuldemenyek.Modosithato(statusz, UI.SetExecParamDefault(page), out errorDetail);
                modosithato = (statusz.Allapot == KodTarak.KULDEMENY_ALLAPOT.Postazott);

                if (!modosithato)
                {
                    errorDetail = statusz.CreateErrorDetail("A küldemény állapota nem megfelelő.", Statusz.ColumnTypes.Allapot);
                }
            }

            return modosithato;
        }

        /// <summary>
        /// Ha nem módosítható postázáshoz, megjelöli az adott sort
        /// </summary>
        /// <param name="e"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static void KuldemenyekGridView_RowDataBound_CheckModosithatoPostazashoz(GridViewRowEventArgs e, Page page)
        {
            if (e == null || page == null) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Módosíthatóság ellenőrzése
                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotFromDataRowView(e.Row.DataItem as DataRowView);
                ErrorDetails errorDetail;
                bool modosithato = CheckModosithatoPostazashozWithFunctionRight(page, statusz, out errorDetail);
                UI.SetRowCheckboxAndInfo(modosithato, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek, statusz.Id, page);
            }
        }
        #endregion PostazasEFeladoJegyzek

        public static Statusz GetAllapotFromDataRow(DataRow kuldemenyRow, string IratId)
        {
            return new Statusz(kuldemenyRow["Id"].ToString(), kuldemenyRow["Allapot"].ToString(), kuldemenyRow["IktatniKell"].ToString()
                , IratId, "", kuldemenyRow["FelhasznaloCsoport_Id_Orzo"].ToString(), kuldemenyRow["Zarolo_id"].ToString()
                , kuldemenyRow["KuldesMod"].ToString(), kuldemenyRow["Csoport_Id_Felelos"].ToString());
        }

        public static Statusz GetAllapotFromDataRowView(DataRowView drv)
        {
            string Id = "";
            if (drv != null && drv["Id"] != null)
            {
                Id = drv["Id"].ToString();
            }

            String Allapot = "";
            if (drv != null && drv["Allapot"] != null)
            {
                Allapot = drv["Allapot"].ToString();
            }
            else Allapot = "";


            String IktatniKell = "";
            if (drv != null && drv["IktatniKell"] != null)
            {
                IktatniKell = drv["IktatniKell"].ToString();
            }
            else IktatniKell = "";


            String FelhasznaloCsoport_Id_Orzo = "";
            if (drv != null && drv["FelhasznaloCsoport_Id_Orzo"] != null)
            {
                FelhasznaloCsoport_Id_Orzo = drv["FelhasznaloCsoport_Id_Orzo"].ToString();
            }
            else FelhasznaloCsoport_Id_Orzo = "";


            String Zarolo_id = "";
            if (drv != null && drv["Zarolo_id"] != null)
            {
                Zarolo_id = drv["Zarolo_id"].ToString();
            }
            else Zarolo_id = "";

            String KuldesMod = "";
            if (drv != null && drv["KuldesMod"] != null)
            {
                KuldesMod = drv["KuldesMod"].ToString();
            }
            else KuldesMod = "";

            string CsopId_Felelos = "";
            if (drv != null && drv["Csoport_Id_Felelos"] != null)
            {
                CsopId_Felelos = drv["Csoport_Id_Felelos"].ToString();
            }

            Kuldemenyek.Statusz statusz = new Contentum.eRecord.BaseUtility.Kuldemenyek.Statusz(
                Id, Allapot, IktatniKell, "", "", FelhasznaloCsoport_Id_Orzo, Zarolo_id, KuldesMod, CsopId_Felelos);

            return statusz;
        }

        #region BLG_577 CSATOLMANY JOGOSULTAK
        /// <summary>
        /// Csatolmany jogosultjai
        /// </summary>
        /// <param name="ParentPage"></param>
        /// <param name="erec_KuldKuldemenyek"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetUserRights_Csatolmanyok(Page ParentPage, EREC_KuldKuldemenyek erec_KuldKuldemenyek)
        {
            return CsatolmanyokUtility.GetUserRights_Csatolmanyok(ParentPage, erec_KuldKuldemenyek);
        }
        #endregion BLG_577 CSATOLMANY JOGOSULTSAG

        public static string[] GetIratpeldanyKuldemenyekForSSRS(Page page, string[] iratpeldanyIds)
        {
            if (iratpeldanyIds == null || iratpeldanyIds.Length < 1)
                return new string[0];

            var search = new EREC_KuldKuldemenyekSearch(true);

            var res = new List<string>();
            var iratpeldanyIdsWithoutZeros = new List<string>();
            foreach (var id in iratpeldanyIds)
            {
                if (id == Guid.Empty.ToString())
                {
                    res.Add(id); // NOTE: to preserve position due to compatibility
                }
                else
                {
                    iratpeldanyIdsWithoutZeros.Add(id);
                }
            }

            search.Extended_EREC_PldIratPeldanyokSearch.Id.In(iratpeldanyIdsWithoutZeros);

            var group = "531";
            var allapotok = new string[] {
                Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Expedialt,
                Contentum.eUtility.KodTarak.IRATPELDANY_ALLAPOT.Postazott
            };
            search.Extended_EREC_PldIratPeldanyokSearch.Allapot.In(allapotok);
            search.Extended_EREC_PldIratPeldanyokSearch.Allapot.OrGroup(group);

            // csak akkor keresünk a továbbítás alatti mezőre, ha az állapot továbbítás alatt
            string tovabbitasAlattAllapot = String.Join(",", allapotok.Select(allapot => 
                "case when EREC_PldIratPeldanyok.Allapot = '50' then '" + allapot + "' else 'xx' end").ToArray());
            search.Extended_EREC_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Value = tovabbitasAlattAllapot;
            search.Extended_EREC_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Operator = Query.Operators.inner;
            search.Extended_EREC_PldIratPeldanyokSearch.TovabbitasAlattAllapot.OrGroup(group);

            ExecParam execParam = UI.SetExecParamDefault(page);
            var service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            Result result = service.KimenoKuldGetAllWithExtension(execParam, search);

            if (result.IsError)
            {
                return null;
            }

            // BUG_4788_Task10268
            var iratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
            iratPeldanyokSearch.Id.In(iratpeldanyIdsWithoutZeros);

            var iratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            var iratPeldanyokResult = iratPeldanyokService.GetAll(execParam, iratPeldanyokSearch);
            if (iratPeldanyokResult.IsError)
            {
                return null;
            }
            var map_iratPeldanyAzonositoToId = new Dictionary<string, string>();
            foreach (DataRow row in iratPeldanyokResult.Ds.Tables[0].Rows)
            {
                map_iratPeldanyAzonositoToId[row["Azonosito"].ToString()] = row["Id"].ToString();
            }

            var map_posToResult = new SortedDictionary<int, string>();
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                var id = row["id"].ToString();
                if (!map_posToResult.ContainsValue(id))
                {
                    if (row["iratpeldanyok"] != DBNull.Value)
                    {
                        var iratpeldanyAzonosito = row["iratpeldanyok"].ToString().Trim();
                        if (map_iratPeldanyAzonositoToId.ContainsKey(iratpeldanyAzonosito))
                        {
                            var iratpeldanyId = map_iratPeldanyAzonositoToId[iratpeldanyAzonosito];
                            var pos = iratpeldanyIds.ToList().IndexOf(iratpeldanyId);
                            if (pos != -1)
                            {
                                map_posToResult[pos] = id;
                            }
                        }
                    }
                }
            }

            foreach (var kv in map_posToResult)
            {
                if (!res.Contains(kv.Value))
                {
                    res.Add(kv.Value);
                }
            }

            return res.ToArray();
        }
    }
}
