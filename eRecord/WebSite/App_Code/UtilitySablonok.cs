﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for UtilitySablonok
/// </summary>
namespace Contentum.eRecord.Utility
{
    public static class UtilitySablonok
    {
        public static void DownloadTemplateFile ( HttpResponse Response, HttpServerUtility Server)
        {

            string DirName = Server.MapPath ( "~\\Sablonok\\" );
            string FileName = "CimzettLista.xlsx";

            Response.Clear ( );
            Response.ClearContent ( );
            Response.ClearHeaders ( );

            // Content-Type és fájl nevének beállítása
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.CacheControl = "public";
            Response.HeaderEncoding = System.Text.Encoding.UTF8;
            Response.Charset = "utf-8";
            Response.AddHeader ( "Content-Disposition", String.Format ( "attachment;filename={0}", FileName ) );

            var fileByteContent = Contentum.eRecord.Utility.FileFunctions.Upload.GetFileContentFromTempDirectory ( DirName, FileName );

            HttpContext.Current.Response.BinaryWrite ( fileByteContent );

            HttpContext.Current.Response.End ( );

            HttpContext.Current.ApplicationInstance.CompleteRequest ( );

        }
    }
}