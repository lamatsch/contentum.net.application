﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// MicroSigner aláíráshoz a kliens oldali callback függvények által hívott metódusok
/// https://e-szigno.hu/uzleti-megoldasok/microsigner.html
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebSite")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class AVDHCallbackService : System.Web.Services.WebService
{

    static string GetTempPath()
    {
        return Path.GetTempPath();
    }

    static string GetTempDir(string erkeztetoSzam)
    {
        return Path.Combine(GetTempPath(), erkeztetoSzam);
    }

    static string GetInputDir(string erkeztetoSzam)
    {
        return Path.Combine(GetTempDir(erkeztetoSzam), "In");
    }

    //private Result Download(Dictionary<int,bool> downloadResults, string[] recordIdsArray, string[] externalLinksArray, string[] fileNamesArray, string[] iratIdsArray, string[] folyamatTetelekIdsArray)
    //{
    //    Result resVerify = new Result();

    //    Dictionary<int, bool> newDic = downloadResults.ToDictionary(x = true);
    //    var iterate = downloadResults.Where(x => x.Value == false).ToList();

    //    for (int i = 0; i < iterate.Count; i++)
    //    {
    //        int k = iterate[i].Key;
    //        try
    //        {
    //            Directory.CreateDirectory(GetInputDir(recordIdsArray[k]));

    //            string tempFile = Path.Combine(GetInputDir(recordIdsArray[k]), fileNamesArray[k]);

    //            using (var client = new WebClient())
    //            {
    //                client.DownloadFile(externalLinksArray[k], tempFile);
    //            }

    //            string filename = String.Empty;
    //            string documentSite = String.Empty;
    //            string documentStore = String.Empty;
    //            string documentFolder = String.Empty;
    //            Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

    //            ExecParam execParamUpl = UI.SetExecParamDefault(HttpContext.Current.Session);

    //            if (!string.IsNullOrEmpty(recordIdsArray[k]))
    //            {
    //                execParamUpl.Record_Id = recordIdsArray[k];
    //                char jogszint = 'O';

    //                Result dokGetResult = dokService.GetWithRightCheck(execParamUpl, jogszint);
    //                if (dokGetResult.IsError)
    //                {
    //                    throw new ResultException(dokGetResult);
    //                }

    //                filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
    //                documentStore = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 3);
    //                documentSite = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 2);
    //                documentFolder = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 4);
    //            }

    //            FileStream reader = File.OpenRead(tempFile);
    //            byte[] documentData = new byte[reader.Length];
    //            reader.Read(documentData, 0, (int)reader.Length);

    //            Result spUploadResult = eDocumentService.UploadSignedFile(execParamUpl.Clone(), iratIdsArray[i], recordIdsArray[i], documentSite, documentStore, documentFolder, filename, documentData);

    //            if (spUploadResult.IsError)
    //            {
    //                Logger.Error("AVDH hiba, spUploadResult.ErrorMessage: " + spUploadResult.ErrorMessage);
    //                return spUploadResult;
    //            }

    //            ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
    //            Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelekIdsArray[i], KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes, String.Empty);
    //            eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikeres_felulvizsgalat);
    //            if (resTetel.IsError)
    //            {
    //                Logger.Error("AVDH hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
    //                eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
    //                eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikertelen_felulvizsgalat);
    //                return resTetel;
    //            }
    //        }
    //        catch (Exception exp)
    //        {
    //            ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
    //            Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelekIdsArray[i], KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes, exp.Message);
    //            if (resTetel.IsError)
    //            {
    //                Logger.Error("PdfVerify hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
    //                eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
    //                return resTetel;
    //            }

    //            Logger.Error("AVDH hiba, externalLinksArray Exception: " + exp.Message);
    //            resVerify.ErrorMessage = "AVDH hiba, externalLinksArray Exception: " + exp.Message;
    //            resVerify.ErrorCode = exp.Message;
    //            eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
    //            return resVerify;
    //        }
    //    }
    //    return resVerify;
    //}

    [WebMethod(EnableSession = true, CacheDuration = 300)]
    public Result StarDownload(string iratIds, string recordIds, string externalLinks, string folyamatTetelekIds, string fileNames, string proc_Id)
    {
        Result resVerify = new Result();

        string[] iratIdsArray = iratIds.Split(';');
        string[] recordIdsArray = recordIds.Split(';');
        string[] externalLinksArray = externalLinks.Split('|');
        string[] folyamatTetelekIdsArray = folyamatTetelekIds.Split(';');
        string[] fileNamesArray = fileNames.Split(';');
        //List<string> verifiedPdfs = new List<string>();

        //Előjegyzett aláírás lekérése, ellenőrzés
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session);
        AlairasokService service_Alairasok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetAlairasokService();
        eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.ellenorzes_folyamatban);

        //int AVDH_RETRY = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_RETRY"));

        Dictionary<int, bool> downloadResult = new Dictionary<int, bool>();

        for (int i = 0; i < externalLinksArray.Length; i++)
        {
            try
            {
                Directory.CreateDirectory(GetInputDir(recordIdsArray[i]));

                string tempFile = Path.Combine(GetInputDir(recordIdsArray[i]), fileNamesArray[i]);

                using (var client = new WebClient())
                {
                    client.DownloadFile(externalLinksArray[i], tempFile);
                }

                string filename = String.Empty;
                string documentSite = String.Empty;
                string documentStore = String.Empty;
                string documentFolder = String.Empty;
                Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

                ExecParam execParamUpl = UI.SetExecParamDefault(HttpContext.Current.Session);

                if (!string.IsNullOrEmpty(recordIdsArray[i]))
                {
                    execParamUpl.Record_Id = recordIdsArray[i];
                    char jogszint = 'O';

                    Result dokGetResult = dokService.GetWithRightCheck(execParamUpl, jogszint);
                    if (dokGetResult.IsError)
                    {
                        throw new ResultException(dokGetResult);
                    }

                    filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                    documentStore = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 3);
                    documentSite = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 2);
                    documentFolder = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 4);
                }

                FileStream reader = File.OpenRead(tempFile);
                byte[] documentData = new byte[reader.Length];
                reader.Read(documentData, 0, (int)reader.Length);

                Result spUploadResult = eDocumentService.UploadSignedFile(execParamUpl.Clone(), iratIdsArray[i], recordIdsArray[i], documentSite, documentStore, documentFolder, filename, documentData);

                if (spUploadResult.IsError)
                {
                    Logger.Error("AVDH hiba, spUploadResult.ErrorMessage: " + spUploadResult.ErrorMessage);
                    return spUploadResult;
                }

                ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
                Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelekIdsArray[i], KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes, String.Empty);
                eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikeres_felulvizsgalat);
                if (resTetel.IsError)
                {
                    Logger.Error("AVDH hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
                    eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
                    eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikertelen_felulvizsgalat);
                    return resTetel;
                }
            }
            catch (Exception exp)
            {
                ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
                Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelekIdsArray[i], KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes, exp.Message);
                if (resTetel.IsError)
                {
                    Logger.Error("PdfVerify hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
                    eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
                    return resTetel;
                }

                Logger.Error("AVDH hiba, externalLinksArray Exception: " + exp.Message);
                resVerify.ErrorMessage = "AVDH hiba, externalLinksArray Exception: " + exp.Message;
                resVerify.ErrorCode = exp.Message;
                eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
                return resVerify;
            }
        }

        eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes);
        return resVerify;
    }



}