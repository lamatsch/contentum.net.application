using AjaxControlToolkit;
using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for Ajax
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebSite")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Ajax : System.Web.Services.WebService
{

    public Ajax()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetOrszagokList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_OrszagokService krt_OrszagokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_OrszagokService();

            return krt_OrszagokService.GetOrszagokList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetOrszagokList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int AjaxTeszt()
    {
        return 1;
    }


    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetFelhasznalokList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_FelhasznalokService krt_FelhasznalokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            return krt_FelhasznalokService.GetFelhasznalokList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetFelhasznalokList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetKozteruletekList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_KozteruletekService krt_KozteruletekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KozteruletekService();

            return krt_KozteruletekService.GetKozteruletekList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetKozteruletekList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetKozteruletTipusokList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_KozteruletTipusokService krt_KozteruletTipusokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KozteruletTipusokService();

            return krt_KozteruletTipusokService.GetKozteruletTipusokList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetKozteruletTipusokList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTelepulesekList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_TelepulesekService krt_TelepulesekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_TelepulesekService();

            return krt_TelepulesekService.GetTelepulesekList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetTelepulesekList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetIranyitoszamokList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_TelepulesekService krt_TelepulesekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_TelepulesekService();

            string[] list = krt_TelepulesekService.GetIranyitoszamokList(prefixText, count, contextKey);

            for (int i = 0; i < list.Length; i++)
            {
                list[i] = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(list[i], list[i]);
            }

            return list;
        }
        catch (Exception e)
        {
            Logger.Error("GetIranyitoszamokList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPartnerekList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_PartnerekService krt_PartnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            return krt_PartnerekService.GetPartnerekList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetPartnerekList", e);
            return null;
        }

    }
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetMinositoPartnerekList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_PartnerekService krt_PartnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            return krt_PartnerekService.GetMinositoPartnerekList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetMinositoPartnerekList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetCsoportokList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_CsoportokService krt_CsoportokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

            return krt_CsoportokService.GetCsoportokList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetCsoportokList", e);
            return null;
        }

    }

    //[System.Web.Services.WebMethod]
    //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public string[] GetFelhasznaloCsoportokList(string prefixText, int count, string contextKey)
    //{
    //    try
    //    {
    //        Contentum.eAdmin.Service.KRT_CsoportokService krt_CsoportokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

    //        return krt_CsoportokService.GetFelhasznaloCsoportokList(prefixText, count, contextKey);
    //    }
    //    catch (Exception e)
    //    { // TODO 
    //        return null;
    //    }

    //}

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetFelhasznaloList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_FelhasznalokService krt_felhasznalokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            return krt_felhasznalokService.GetFelhasznaloList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetFelhasznaloList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetFunkciokList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_FunkciokService krt_funkciokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FunkciokService();

            return krt_funkciokService.GetFunkciokList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetFunkciokList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetBankszamlaszamokList(string prefixText, int count, string contextKey)
    {
        // ha * van benne, akkor nem k�rj�k le a list�t
        if (prefixText.Contains("*"))
        {
            return null;
        }

        try
        {
            Contentum.eAdmin.Service.KRT_BankszamlaszamokService krt_BankszamlaszamokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();

            return krt_BankszamlaszamokService.GetBankszamlaszamokList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetBankszamlaszamokList", e);
            return null;
        }
    }


    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetAdoszamByPartner(string partnerId, string userId, string loginId)
    {
        try
        {
            Contentum.eAdmin.Service.KRT_PartnerekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = userId;
            execParam.LoginUser_Id = loginId;
            execParam.Record_Id = partnerId;

            //string adoszam = service.GetAdoszamByPartner(execParam).Record as string;
            //return adoszam;

            string[] result = service.GetAdoszamByPartner(execParam).Record as string[];
            return result;
        }
        catch (Exception e)
        {
            //return null;
            Logger.Warn("GetAdoszamByPartner hiba", e);
            throw new Exception("Hiba t�rt�nt a partner ad�sz�m�nak lek�r�sekor!");
        }
    }

    /// <summary>
    /// A Javascript futtat�sa k�zben fell�p� hib�k logol�s�t ell�t� webservice
    /// </summary>
    /// <param name="errorCode"></param>
    /// <param name="exception"></param>
    /// <param name="url"></param>
    /// <param name="referrer"></param>
    /// <param name="scripts"></param>
    /// <returns></returns>
    [System.Web.Services.WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool LogException(int errorCode, string exception, string url, string referrer, string[] scripts)
    {
        //Log4Net
        Logger.ErrorStart();
        Logger.Error("Javascript hiba");
        Logger.Error(String.Format("Hibak�d: {0},\nHiba: {1},\nUrl: {2}", errorCode, exception, url));
        Logger.ErrorEnd();

        try
        {
            //Audit log page bejegyz�sek k�z�
            KRT_Log_Page krtPage = new KRT_Log_Page();
            krtPage.Updated.SetValueAll(true);
            krtPage.Base.Updated.SetValueAll(false);

            krtPage.Date = Log.GetStaticDateID();
            string loginTranzId = (string)(Session[Constants.LoginTranzId] ?? String.Empty);
            if (!String.IsNullOrEmpty(loginTranzId))
            {
                krtPage.Login_Tranz_id = loginTranzId;
            }
            krtPage.Name = Path.GetFileName(url);
            //krtPage.ModulId t�rolt elj�r�s t�lti ki;
            krtPage.Url = url;
            krtPage.HibaKod = errorCode.ToString();
            krtPage.HibaUzenet = "Javascript hiba: " + exception;
            ExecParam xpm = UI.SetExecParamDefault(Session);
            krtPage.Base.Letrehozo_id = xpm.Felhasznalo_Id;
            krtPage.Status = Log.Status.KliensFut;

            krtPage.Base.Tranz_id = System.Guid.NewGuid().ToString();

            Log.AddPageLog(krtPage, log4net.Core.Level.Severe);
        }
        catch (Exception e)
        {
            //Log4Net
            Logger.ErrorStart();
            Logger.Error("Javascript audit log hiba:");
            Logger.Error(String.Format("Hiba�zenet: {0}", e.Message));
            Logger.ErrorEnd();
        }

        return true;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UpdateAlapertelmezettTemplate(string felhasznaloId, string templateId, string Alapertelmezett)
    {
        Logger.DebugStart();
        Logger.Debug("UpdateAlapertelmezettTemplate scriptmethod start");
        Logger.Debug("felhasznaloId: " + felhasznaloId);
        Logger.Debug("templateId:" + templateId);
        Logger.Debug("Alapertelmezett: " + Alapertelmezett);

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;
        execParam.Record_Id = templateId;

        ResultError res = UIMezoObjektumErtekek.UpdateAlapertelmezett(execParam, Alapertelmezett);

        if (res.IsError)
        {
            Logger.Error(String.Format("UpdateAlapertelmezettTemplate scriptmethod hiba: {0},{1}", res.ErrorMessage, res.ErrorMessage));
            throw new ResultException(res.ErrorMessage, res.ErrorMessage);
        }

        Logger.Debug("UpdateAlapertelmezettTemplate scriptmethod end");
    }

    [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetLoginFelhasznalok(string knownCategoryValues, string category, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }

        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        //CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //values.Add(emptyItem);

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = contextKey;
        execParam.LoginUser_Id = contextKey;

        KRT_FelhasznalokSearch search_felhasznalok = new KRT_FelhasznalokSearch();
        search_felhasznalok.Id.Value = contextKey;
        search_felhasznalok.Id.Operator = Contentum.eQuery.Query.Operators.equals;

        search_felhasznalok.Engedelyezett.Value = "1";
        search_felhasznalok.Engedelyezett.Operator = Contentum.eQuery.Query.Operators.equals;

        Contentum.eAdmin.Service.KRT_FelhasznalokService service_felhasznalok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        Result result_felhasznalok = service_felhasznalok.GetAll(execParam, search_felhasznalok);

        if (!String.IsNullOrEmpty(result_felhasznalok.ErrorCode))
        {
            // hiba:
            return null;
        }
        else
        {
            if (result_felhasznalok.Ds.Tables[0].Rows.Count > 0)
            {
                String Felhasznalo_Id = result_felhasznalok.Ds.Tables[0].Rows[0]["Id"].ToString();
                String Felhasznalo_Nev = result_felhasznalok.Ds.Tables[0].Rows[0]["Nev"].ToString();

                try
                {
                    CascadingDropDownNameValue item = new CascadingDropDownNameValue(Felhasznalo_Nev, contextKey + "|" + Felhasznalo_Id);
                    values.Add(item);
                    item.isDefaultValue = true;

                }
                catch
                {
                    values.Clear();
                    values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                }
            }
            else
            {
                // nincs ilyen felhasznalo
                return null;
            }
        }

        Contentum.eAdmin.Service.KRT_HelyettesitesekService service_helyettesitesek = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();

        KRT_HelyettesitesekSearch search_helyettesitesek = new KRT_HelyettesitesekSearch();

        search_helyettesitesek.Felhasznalo_ID_helyettesito.Value = contextKey;
        search_helyettesitesek.Felhasznalo_ID_helyettesito.Operator = Query.Operators.equals;

        string str_now = DateTime.Now.ToString();

        // kezd <= now <= vege

        search_helyettesitesek.HelyettesitesKezd.Value = str_now;
        search_helyettesitesek.HelyettesitesKezd.Operator = Query.Operators.lessorequal;

        search_helyettesitesek.HelyettesitesVege.Value = str_now;
        search_helyettesitesek.HelyettesitesVege.Operator = Query.Operators.greaterorequal;

        search_helyettesitesek.OrderBy = "Felhasznalo_ID_helyettesitett";

        Result result_helyettesitesek = service_helyettesitesek.GetAllWithExtension(execParam, search_helyettesitesek);

        if (!String.IsNullOrEmpty(result_helyettesitesek.ErrorCode))
        {
            // hiba:
            return null;
        }
        else
        {
            String lastHelyettesitett_Id = "";
            foreach (DataRow row in result_helyettesitesek.Ds.Tables[0].Rows)
            {
                String Helyettesitett_Id = row["Felhasznalo_ID_helyettesitett"].ToString();
                String Helyettesitett_Nev = row["Felhasznalo_Nev_helyettesitett"].ToString();

                if (lastHelyettesitett_Id != Helyettesitett_Id)
                {

                    try
                    {
                        values.Add(new CascadingDropDownNameValue(Helyettesitett_Nev, contextKey + "|" + Helyettesitett_Id));
                        lastHelyettesitett_Id = Helyettesitett_Id;
                    }
                    catch
                    {
                        values.Clear();
                        values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                    }

                }
            }

        }

        // rendez�s helyettes�tett n�v szerint
        values.Sort(new CascadingDropDownNameValueComparer());

        return values.ToArray();

        //return default(AjaxControlToolkit.CascadingDropDownNameValue[]);
    }

    [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetCsoportTagsagokByFelhasznalo(string knownCategoryValues, string category, string contextKey)
    {
        Logger.DebugStart("GetCsoportTagsagokByFelhasznalo");

        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }


        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
          knownCategoryValues);

        // A kiv�lasztott �gazati jel:
        string FelhasznaloHelyettesArray = "";

        if (kv.ContainsKey("Felhasznalo") == false)
        {
            return null;
        }
        else
        {
            FelhasznaloHelyettesArray = kv["Felhasznalo"];

        }

        if (String.IsNullOrEmpty(FelhasznaloHelyettesArray))
        {
            return null;
        }

        // ha van '|' --> k�t felhaszn�l�, �sszef�zve
        if (FelhasznaloHelyettesArray.Contains("|"))
        {
            // A felhaszn�l� �s a helyettes�tett felhaszn�l�:
            string[] array = FelhasznaloHelyettesArray.Split('|');
            string Helyettesito_Id = array[0];
            string Helyettesitett_Id = array[1];

            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = contextKey;
            execParam.LoginUser_Id = contextKey;

            if (Helyettesito_Id == Helyettesitett_Id)
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

                search_csoportok.Csoport_Id_Jogalany.Value = Helyettesito_Id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);

                string lastCsoport_Id = "";
                if (String.IsNullOrEmpty(result_csoportok.ErrorCode))
                {
                    foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                    {
                        String CsoportTag_Id = row["Id"].ToString();
                        String Csoport_Id = row["Csoport_Id"].ToString();
                        String Csoport_Nev = row["Csoport_Nev"].ToString();
                        String ItemValue = "|" + CsoportTag_Id + "|";

                        if (lastCsoport_Id != Csoport_Id)
                        {
                            try
                            {
                                values.Add(new CascadingDropDownNameValue(Csoport_Nev, ItemValue));
                                lastCsoport_Id = Csoport_Id;
                            }
                            catch
                            {
                                values.Clear();
                                values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                            }
                        }
                    }
                }
            }
            else
            {

                Contentum.eAdmin.Service.KRT_HelyettesitesekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_HelyettesitesekService();

                KRT_HelyettesitesekSearch search = new KRT_HelyettesitesekSearch();

                search.Felhasznalo_ID_helyettesito.Value = Helyettesito_Id;
                search.Felhasznalo_ID_helyettesito.Operator = Query.Operators.equals;

                search.Felhasznalo_ID_helyettesitett.Value = Helyettesitett_Id;
                search.Felhasznalo_ID_helyettesitett.Operator = Query.Operators.equals;

                string str_now = DateTime.Now.ToString();

                // kezd <= now <= vege

                search.HelyettesitesKezd.Value = str_now;
                search.HelyettesitesKezd.Operator = Query.Operators.lessorequal;

                search.HelyettesitesVege.Value = str_now;
                search.HelyettesitesVege.Operator = Query.Operators.greaterorequal;

                search.OrderBy = "KRT_CsoportTagok.Csoport_Id ASC, KRT_CsoportTagok.Tipus DESC";

                //Result result = service.GetAll(execParam, search);
                Result result = service.GetAllWithExtension(execParam, search);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {

                        String HelyettesitesMod = row["HelyettesitesMod"].ToString();
                        if (String.IsNullOrEmpty(HelyettesitesMod))
                        {
                            // ha nincs megadva (r�gi rekord), akkor helyettes�t�s
                            HelyettesitesMod = KodTarak.HELYETTESITES_MOD.Helyettesites;
                        }

                        String Helyettesites_Id = row["Id"].ToString();

                        String CsoportTag_ID_helyettesitett = row["CsoportTag_ID_helyettesitett"].ToString();
                        if (HelyettesitesMod == KodTarak.HELYETTESITES_MOD.Helyettesites && String.IsNullOrEmpty(CsoportTag_ID_helyettesitett))
                        {
                            // a helyettes�tettnek t�bb csoportja is lehet, ezek k�z�l is v�lasztani kell
                            #region helyettes�tett csoportjai

                            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

                            search_csoportok.Csoport_Id_Jogalany.Value = Helyettesitett_Id;
                            search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                            //Result result_csoportok = service_csoportok.GetAll(execParam, search_csoportok);
                            Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);
                            if (String.IsNullOrEmpty(result_csoportok.ErrorCode))
                            {
                                string lastCsoport_Id = "";
                                foreach (DataRow row_csoportok in result_csoportok.Ds.Tables[0].Rows)
                                {
                                    String CsoportTag_Id = row_csoportok["Id"].ToString();
                                    String Csoport_Id = row_csoportok["Csoport_Id"].ToString();
                                    //String Csoport_Nev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Csoport_Id, this.Context.Cache);
                                    String Csoport_Nev = row_csoportok["Csoport_Nev"].ToString(); ;

                                    String Helyettesites_ItemValue = Helyettesites_Id + "|" + CsoportTag_Id + "|" + HelyettesitesMod;

                                    if (lastCsoport_Id != Csoport_Id)
                                    {
                                        try
                                        {
                                            values.Add(new CascadingDropDownNameValue(Csoport_Nev, Helyettesites_ItemValue));
                                            lastCsoport_Id = Csoport_Id;
                                        }
                                        catch
                                        {
                                            values.Clear();
                                            values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                                        }
                                    }
                                }
                            }
                            #endregion helyettes�tett csoportjai
                        }
                        else
                        {
                            //String Csoport_Id_helyettesitett = row["Csoport_Id_helyettesitett"].ToString();
                            //String Csoport_Nev_helyettesitett = UI.RemoveEmailAddressFromCsoportNev(row["Csoport_Nev_helyettesitett"].ToString());
                            //String Helyettesites_ItemValue = Helyettesites_Id + "|" + CsoportTag_ID_helyettesitett + "|" + HelyettesitesMod;
                            //try
                            //{
                            //    values.Add(new CascadingDropDownNameValue(Csoport_Nev_helyettesitett, Helyettesites_ItemValue));
                            //}
                            //catch
                            //{
                            //    values.Clear();
                            //    values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                            //}

                            //t�bbf�le kapcsolatt�pussal is szerepelhet egy csoporttag, ebb�l kell kiv�lasztani a leger�sebb t�pust (vezet� eset�n)
                            #region csoportag t�pus kiv�laszt�sa

                            String Csoport_Id_helyettesitett = row["Csoport_Id_helyettesitett"].ToString();

                            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();

                            search_csoportok.Csoport_Id_Jogalany.Value = Helyettesitett_Id;
                            search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                            search_csoportok.Csoport_Id.Value = Csoport_Id_helyettesitett;
                            search_csoportok.Csoport_Id.Operator = Query.Operators.equals;

                            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                            Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);
                            if (String.IsNullOrEmpty(result_csoportok.ErrorCode))
                            {
                                string lastCsoport_Id = "";
                                foreach (DataRow row_csoportok in result_csoportok.Ds.Tables[0].Rows)
                                {
                                    String CsoportTag_Id = row_csoportok["Id"].ToString();
                                    String Csoport_Id = row_csoportok["Csoport_Id"].ToString();
                                    String Csoport_Nev = row_csoportok["Csoport_Nev"].ToString(); ;

                                    String Helyettesites_ItemValue = Helyettesites_Id + "|" + CsoportTag_Id + "|" + HelyettesitesMod;

                                    if (lastCsoport_Id != Csoport_Id)
                                    {
                                        try
                                        {
                                            values.Add(new CascadingDropDownNameValue(Csoport_Nev, Helyettesites_ItemValue));
                                            lastCsoport_Id = Csoport_Id;
                                        }
                                        catch
                                        {
                                            values.Clear();
                                            values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                                        }
                                    }
                                }
                            }
                            #endregion helyettes�tett csoportjai
                        }

                    }
                }

            }
        }


        //if (values.Count == 0)
        //{
        //    CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //    values.Add(emptyItem);
        //    emptyItem.isDefaultValue = true;
        //}

        Logger.DebugEnd("GetCsoportTagsagokByFelhasznalo");

        // rendez�s helyettes�tett n�v szerint
        values.Sort(new CascadingDropDownNameValueComparer());

        // a rendez�s ut�ni els� �rt�ket jel�lj�k ki
        if (values.Count > 0)
        {
            values[0].isDefaultValue = true;
        }

        return values.ToArray();
    }

    /// <summary>
    /// Seg�doszt�ly CascadingDropDown rendez�shez:
    /// ha van default �rt�k, azt el�re helyezi a rangsorban, egy�bk�nt a name szerint hasonl�t �ssze
    /// </summary>
    public class CascadingDropDownNameValueComparer : IComparer<CascadingDropDownNameValue>
    {
        public int Compare(CascadingDropDownNameValue obj1, CascadingDropDownNameValue obj2)
        {
            int result = 0;
            if (obj1.isDefaultValue || obj2.isDefaultValue)
            {
                // hogy n�vekv� sorrend (el�l a default) legyen, obj1-t hasonl�tjuk obj2-h�z
                result = obj2.isDefaultValue.CompareTo(obj1.isDefaultValue);
            }
            else
            {
                // hogy n�vekv� sorrend (el�l az ABC-ben el�r�bb l�v�) legyen, obj2-t hasonl�tjuk obj1-hez
                result = obj1.name.CompareTo(obj2.name);
            }

            //Logger.Debug(String.Format("Nev1: {0}; Nev2: {1}; Default1: {2}; Default2: {3}; result: {4}", obj1.name, obj2.name, obj1.isDefaultValue, obj2.isDefaultValue, result));

            return result;
        }
    }

    [System.Web.Services.WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetFuggokodtarak(string knownCategoryValues, string category, string contextKey)
    {
        Logger.DebugStart("GetFuggokodtarak");


        string vezerloKodcsoportKod = contextKey;
        string fuggoKodcsoportKod = category;

        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        //nincs f�gg�s�g
        if (String.IsNullOrEmpty(knownCategoryValues))
        {
            List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(fuggoKodcsoportKod, execParam, HttpContext.Current.Cache, null);

            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
            {
                values.Add(new CascadingDropDownNameValue(elem.Nev, elem.Kod));
            }
        }
        else
        {

            StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
              knownCategoryValues);

            if (kv.ContainsKey(vezerloKodcsoportKod))
            {
                string vezerloKodtarKod = kv[vezerloKodcsoportKod];
                string vezerloKodtarId = GetKodtarId(execParam, vezerloKodcsoportKod, vezerloKodtarKod);

                if (!String.IsNullOrEmpty(vezerloKodtarId))
                {
                    bool hasNincsItem;
                    List<string> fuggoKodtrarak = GetFuggoKodtarak(execParam, vezerloKodcsoportKod, fuggoKodcsoportKod, vezerloKodtarId, out hasNincsItem);

                    if (fuggoKodtrarak != null && fuggoKodtrarak.Count > 0)
                    {
                        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(fuggoKodcsoportKod, execParam, HttpContext.Current.Cache, null);

                        foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
                        {
                            if (fuggoKodtrarak.Contains(elem.Id.ToLower()))
                                values.Add(new CascadingDropDownNameValue(elem.Nev, elem.Kod));
                        }
                    }
                }
            }
        }

        return values.ToArray();
    }

    private List<string> GetFuggoKodtarak(ExecParam execParam, string vezerloKodcsoportKod, string fuggoKodcsoportKod, string vezerloKodtarId, out bool hasNincsItem)
    {
        hasNincsItem = false;

        Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", vezerloKodcsoportKod);
        search.Vezerlo_KodCsoport_Id.Operator = Query.Operators.inner;
        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", fuggoKodcsoportKod);
        search.Fuggo_KodCsoport_Id.Operator = Query.Operators.inner;

        Result res = service.GetAll(execParam, search);

        if (!res.IsError)
        {

            if (res.Ds.Tables[0].Rows.Count > 0)
            {
                string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();

                if (!String.IsNullOrEmpty(adat))
                {
                    KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                    List<string> fuggoKodtarak = new List<string>();

                    foreach (KodtarFuggosegDataItemClass item in fuggosegek.Items)
                    {
                        if (item.VezerloKodTarId.Equals(vezerloKodtarId, StringComparison.InvariantCultureIgnoreCase))
                        {
                            fuggoKodtarak.Add(item.FuggoKodtarId);
                        }
                    }

                    foreach (KodtarFuggosegDataNincsClass item in fuggosegek.ItemsNincs)
                    {
                        if (item.VezerloKodTarId.Equals(vezerloKodtarId, StringComparison.InvariantCultureIgnoreCase))
                        {
                            hasNincsItem = true;
                            break;
                        }
                    }

                    return fuggoKodtarak;
                }
            }
        }

        return null;
    }

    private string GetKodtarId(ExecParam execParam, string kodcsoportKod, string kodtarKod)
    {
        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(kodcsoportKod, execParam, HttpContext.Current.Cache, null);

        foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
        {
            if (elem.Kod == kodtarKod)
                return elem.Id;
        }

        return null;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetFelelosSzervezetVezeto(string prefixText, int count, string contextKey)
    {
        try
        {
            string[] values = contextKey.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (values == null || values.Length < 2)
                return null;

            string felhasznaloId = values[0];
            string felelosSzervezetId = values[1];
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;
            Contentum.eAdmin.Service.KRT_CsoportTagokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch src = new KRT_CsoportTagokSearch();

            src.Csoport_Id.Value = felelosSzervezetId;
            src.Csoport_Id.Operator = Query.Operators.equals;

            src.Tipus.Value = KodTarak.CsoprttagsagTipus.vezeto;
            src.Tipus.Operator = Query.Operators.equals;

            Result result = service.GetAllWithExtension(execParam, src);
            if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
                return "Nincs adat|b";

            return string.Format("{0}|{1}", result.Ds.Tables[0].Rows[0]["Csoport_Jogalany_Nev"].ToString(), result.Ds.Tables[0].Rows[0]["Csoport_Id_Jogalany"].ToString());
        }
        catch (Exception e)
        {
            Logger.Error("GetFelelosSzervezetVezeto", e);
            return null;
        }
    }

    /// <summary>
    /// Partnerhez rendelt email c�m lek�r�se.
    /// </summary>
    /// <param name="partnerId"></param>
    /// <param name="contextKey"></param>
    /// <returns></returns>
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string GetPartnerEmail(string partnerId, string contextKey)
    {
        if (string.IsNullOrEmpty(partnerId) || string.IsNullOrEmpty(contextKey))
            return null;
        string result = string.Empty;
        try
        {
            string[] values = contextKey.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (values == null || values.Length < 2)
                return null;

            string felhasznaloId = values[0];
            string felelosSzervezetId = values[1];
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;

            KRT_PartnerekSearch srcPart = new KRT_PartnerekSearch();
            srcPart.Id.Value = partnerId;
            srcPart.Id.Operator = Query.Operators.equals;
            srcPart.TopRow = 1;

            KRT_CimekSearch srcCim = new KRT_CimekSearch();
            srcCim.Tipus.Value = KodTarak.Cim_Tipus.Email;
            srcCim.Tipus.Operator = Query.Operators.equals;
            srcCim.TopRow = 1;

            Contentum.eAdmin.Service.KRT_PartnerekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            Result resultSvc = service.GetAllWithCim(execParam, srcPart, srcCim);

            if (!string.IsNullOrEmpty(resultSvc.ErrorCode) || resultSvc.Ds.Tables[0].Rows.Count < 1)
                return null;

            if (resultSvc.Ds.Tables[0].Rows[0]["CimTipus"].ToString() == KodTarak.Cim_Tipus.Email && !string.IsNullOrEmpty(resultSvc.Ds.Tables[0].Rows[0]["CimTobbi"].ToString()))
            {
                result = resultSvc.Ds.Tables[0].Rows[0]["CimTobbi"].ToString();
            }
        }
        catch (Exception e)
        {
            //return null;
            Logger.Warn("GetAllWithCim hiba", e);
            throw new Exception("Hiba t�rt�nt a partner email lek�r�sekor!");
        }
        return result;
    }

    private class FizikaiHelyekData
    {
        public string Id;
        public string Value;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string GetFizikaiHelyek(string partnerId, string contextKey)
    {
        if (string.IsNullOrEmpty(partnerId) || string.IsNullOrEmpty(contextKey))
            return null;
        string result;
        try
        {
            string[] values = contextKey.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (values == null || values.Length < 2)
                return null;

            string felhasznaloId = values[0];
            string felelosSzervezetId = values[1];
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;

            Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
            search_csoportok.Csoport_Id_Jogalany.Value = partnerId;
            search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

            Result result_csoportok = service_csoportok.GetAllWithExtension(execParam, search_csoportok);

            if (!string.IsNullOrEmpty(result_csoportok.ErrorCode))
                return null;

            List<string> Csoportok = new List<string>();

            foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
            {
                Csoportok.Add(row["Csoport_Id"].ToString());
            }
            Csoportok.Add(partnerId);
            EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
            EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
            search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
            var res = service.GetAllWithPartnerKapcsolatok(execParam, search);

            if (!string.IsNullOrEmpty(res.ErrorCode) || res.Ds.Tables[0].Rows.Count < 1)
                return null;

            List<FizikaiHelyekData> data = new List<FizikaiHelyekData>();
            if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    FizikaiHelyekData d = new FizikaiHelyekData();
                    d.Id = row["Id"].ToString();
                    d.Value = row["Ertek"].ToString();
                    //data.Add(row["Id"].ToString() + "||" + row["Ertek"].ToString());
                    if (!data.Any(x => (x.Id == d.Id && x.Value == d.Value)))
                    {
                        data.Add(d);
                    }
                }
            }
            else
            {
                return null;
            }
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //Context.Response.Write(js.Serialize(data.ToArray()));
            result = JsonConvert.SerializeObject(data.ToArray()).ToString();
        }
        catch (Exception e)
        {
            //return null;
            Logger.Warn("GetAllWithCim hiba", e);
            throw new Exception("Hiba t�rt�nt a partner email lek�r�sekor!");
        }
        return result;
    }

    /// <summary>
    /// IsExistsHivatkozasiSzam
    /// </summary>
    /// <param name="hivatkozasizzam"></param>
    /// <param name="contextKey"></param>
    /// <returns></returns>
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string IsExistsHivatkozasiSzam(string hivatkozasizzam, string contextKey)
    {
        if (string.IsNullOrEmpty(hivatkozasizzam) || string.IsNullOrEmpty(contextKey))
            return null;
        string result = string.Empty;
        try
        {
            string[] values = contextKey.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            if (values == null || values.Length < 2)
                return false.ToString();

            string felhasznaloId = values[0];
            string felelosSzervezetId = values[1];
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;

            EREC_IraIratokSearch src = new EREC_IraIratokSearch();
            src.HivatkozasiSzam.Value = hivatkozasizzam;
            src.HivatkozasiSzam.Operator = Query.Operators.equals;

            Contentum.eRecord.Service.EREC_IraIratokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
            Result resultSvc = service.GetAll(execParam, src);

            if (!string.IsNullOrEmpty(resultSvc.ErrorCode) || resultSvc.Ds.Tables[0].Rows.Count < 1)
                return false.ToString();

            if (resultSvc.Ds.Tables[0].Rows.Count > 0)
                return true.ToString();
        }
        catch (Exception e)
        {
            Logger.Warn("IsExistsHivatkozasiSzam", e);
            throw new Exception("Hiba t�rt�nt a hivatkozasi szam lek�r�sekor!");
        }
        return false.ToString(); ;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTUKFelhasznalokList(string prefixText, int count, string contextKey)
    {
        List<string> list = new List<string>();

        try
        {
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = contextKey;

            Contentum.eAdmin.Service.KRT_PartnerekService partnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
            KRT_PartnerekSearch search = new KRT_PartnerekSearch();
            search.Tipus.Value = KodTarak.Partner_Tipus.Szemely;
            search.Tipus.Operator = Query.Operators.equals;
            search.Belso.Value = Constants.Database.Yes;
            search.Belso.Operator = Query.Operators.equals;
            search.TopRow = 20;
            search.OrderBy = "KRT_Partnerek.Nev";

            search.Nev.Value = prefixText + '%';
            search.Nev.Operator = Query.Operators.like;

            Result result = partnerekService.GetAll(execParam, search);

            if (!result.IsError)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    list.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row["Nev"].ToString(), row["Id"].ToString()));
                }

                return list.ToArray();
            }
        }
        catch (Exception e)
        {
            Logger.Error("GetTUKFelhasznalokList", e);
        }

        return list.ToArray();
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetCimTipusok(string felhasznaloId, string kuldesMod)
    {
        Logger.DebugStart("GetCimTipusok");

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;

        string vezerloKodcsoportKod = "KULDEMENY_KULDES_MODJA";
        string fuggoKodcsoportKod = "CIM_TIPUS";

        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(fuggoKodcsoportKod, execParam, HttpContext.Current.Cache, null);
        bool addNincsItem = true;
        bool hasNincsItem = false;
        if (!String.IsNullOrEmpty(kuldesMod))
        {
            string vezerloKodtarKod = kuldesMod;
            string vezerloKodtarId = GetKodtarId(execParam, vezerloKodcsoportKod, vezerloKodtarKod);

            if (!String.IsNullOrEmpty(vezerloKodtarId))
            {
                
                List<string> fuggoKodtrarak = GetFuggoKodtarak(execParam, vezerloKodcsoportKod, fuggoKodcsoportKod, vezerloKodtarId, out hasNincsItem);

                if (fuggoKodtrarak != null && fuggoKodtrarak.Count > 0)
                {
                    if (!hasNincsItem)
                    {
                        addNincsItem = false;
                    }
                    foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
                    {
                        if (fuggoKodtrarak.Contains(elem.Id.ToLower()))
                            values.Add(new CascadingDropDownNameValue(elem.Nev, elem.Kod));
                    }
                }
            }
        }

        if (values.Count == 0 && !hasNincsItem)
        {
            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
            {
                values.Add(new CascadingDropDownNameValue(elem.Nev, elem.Kod));
            }
        }

        if (addNincsItem)
        {
            values.Insert(0, new CascadingDropDownNameValue("Nincs", ""));
        }

        string mindValue = String.Join(",", values.Select(v => v.value).ToArray());

        values.Insert(0, new CascadingDropDownNameValue("Mind", mindValue));

        return values.ToArray();
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue GetCimTipus(string felhasznaloId, string cimId)
    {
        Logger.DebugStart("GetCimTipusok");

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;
        execParam.Record_Id = cimId;

        Contentum.eAdmin.Service.KRT_CimekService cimekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CimekService();
        Result res = cimekService.Get(execParam);

        if (!res.IsError)
        {
            KRT_Cimek cim = res.Record as KRT_Cimek;
            string value = cim.Tipus;
            string name = KodTar_Cache.GetKodtarErtekByKodCsoportKodTar("CIM_TIPUS", value, execParam, HttpContext.Current.Cache);
            return new AjaxControlToolkit.CascadingDropDownNameValue(name, value);
        }

        return null;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetFuggoKodtarakList(string felhasznaloId, string vezerloKodcsoportKod, string fuggoKodcsoportKod, string parentValue)
    {
        Logger.DebugStart("GetFuggoKodtarakList");

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = felhasznaloId;

        List<CascadingDropDownNameValue> values = new List<CascadingDropDownNameValue>();

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarList = KodTar_Cache.GetKodtarakByKodCsoportList(fuggoKodcsoportKod, execParam, HttpContext.Current.Cache, null);
        bool addNincsItem = true;

        if (!String.IsNullOrEmpty(parentValue))
        {
            string vezerloKodtarKod = parentValue;
            string vezerloKodtarId = GetKodtarId(execParam, vezerloKodcsoportKod, vezerloKodtarKod);

            if (!String.IsNullOrEmpty(vezerloKodtarId))
            {
                bool hasNincsItem;
                List<string> fuggoKodtrarak = GetFuggoKodtarak(execParam, vezerloKodcsoportKod, fuggoKodcsoportKod, vezerloKodtarId, out hasNincsItem);

                if (fuggoKodtrarak != null && fuggoKodtrarak.Count > 0)
                {
                    if (!hasNincsItem)
                    {
                        addNincsItem = false;
                    }
                    foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
                    {
                        if (fuggoKodtrarak.Contains(elem.Id.ToLower()))
                            values.Add(new CascadingDropDownNameValue(elem.Nev, elem.Kod));
                    }
                }
            }
        }

        if (values.Count == 0)
        {
            foreach (Contentum.eUtility.KodTar_Cache.KodTarElem elem in kodtarList)
            {
                values.Add(new CascadingDropDownNameValue(elem.Nev, elem.Kod));
            }
        }

        if (addNincsItem)
        {
            values.Insert(0, new CascadingDropDownNameValue("Nincs", ""));
        }


        return values.ToArray();
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public bool IsElosztoIv(string felhasznaloId, string partnerId)
    {
        Logger.DebugStart("IsElosztoIv");

        if (!String.IsNullOrEmpty(partnerId))
        {
            Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = felhasznaloId;

            EREC_IraElosztoivekSearch search = new EREC_IraElosztoivekSearch();
            search.Id.Value = partnerId;
            search.Id.Operator = Query.Operators.equals;

            Result result = service.GetAll(execParam, search);

            if (!result.IsError && result.Ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    private KRT_Cimek GetCimById(string id, string felhasznaloId)
    {
        Contentum.eAdmin.Service.KRT_CimekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CimekService();
        ExecParam execParamCim = new ExecParam();
        execParamCim.Felhasznalo_Id = felhasznaloId;
        execParamCim.Record_Id = id;

        Result res = service.Get(execParamCim);
        if (res.IsError)
        {
            return null;
        }

        return (KRT_Cimek)res.Record;

    }

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(KRT_Cimek krt_cimek)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (krt_cimek.Tipus)
            {
                case KodTarak.Cim_Tipus.Postai:
                    cim.Add(krt_cimek.OrszagNev, delimeter);
                    cim.Add(krt_cimek.IRSZ, delimeter);
                    cim.Add(krt_cimek.TelepulesNev, delimeter);
                    // BLG_1347
                    //cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                    //cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                    if (String.IsNullOrEmpty(krt_cimek.KozteruletNev) && !String.IsNullOrEmpty(krt_cimek.HRSZ))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(krt_cimek.HRSZ, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(krt_cimek.KozteruletNev, delimeterSpace);
                        cim.Add(krt_cimek.KozteruletTipusNev, delimeterSpace);
                        string hazszam = krt_cimek.Hazszam;
                        string hazszamIg = krt_cimek.Hazszamig;
                        string hazszamBetujel = krt_cimek.HazszamBetujel;
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cim.Add(hazszam, delimeter);
                        string lepcsohaz = krt_cimek.Lepcsohaz;
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cim.Add(lepcsohaz, delimeter);
                        string szint = krt_cimek.Szint;
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cim.Add(szint, delimeter);
                        string ajto = krt_cimek.Ajto;
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = krt_cimek.AjtoBetujel;
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = krt_cimek.CimTobbi;
                    cim.Add(Cim, delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception e)
        {
            Logger.Error("GetAppendedCim", e);
            return "";
        }

        return text.ToString();
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string GetKRID(string felhasznaloId, string partnerId, string iratPeldanyId, string ugyiratId, string iratPeldanyFajta, string expedialasModja)
    {
        Logger.DebugStart("GetKRID");

        if (iratPeldanyFajta == Contentum.eUtility.KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
        {
            if (expedialasModja == Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu
               || expedialasModja == Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.Elhisz)
            {
                if (!String.IsNullOrEmpty(partnerId))
                {
                    #region Kor�bbi

                    if (!string.IsNullOrEmpty(iratPeldanyId) && !string.IsNullOrEmpty(ugyiratId))
                    {
                        Contentum.eRecord.Service.EREC_PldIratPeldanyokService pldService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        ExecParam execParamPld = new ExecParam();
                        execParamPld.Felhasznalo_Id = felhasznaloId;
                        execParamPld.Record_Id = iratPeldanyId;

                        Result pldResult = pldService.Get(execParamPld);
                        if (!pldResult.IsError)
                        {
                            EREC_PldIratPeldanyok pld = (EREC_PldIratPeldanyok)pldResult.Record;

                            if (!string.IsNullOrEmpty(pld.Partner_Id_Cimzett))
                            {
                                Contentum.eRecord.Service.EREC_IraIratokService iratService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                ExecParam execParamIrat = new ExecParam();
                                execParamIrat.Felhasznalo_Id = felhasznaloId;
                                EREC_IraIratokSearch iratSearch = new EREC_IraIratokSearch();
                                iratSearch.AdathordozoTipusa.Value = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
                                iratSearch.AdathordozoTipusa.Operator = Contentum.eQuery.Query.Operators.equals;
                                iratSearch.ExpedialasModja.Value = KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
                                iratSearch.ExpedialasModja.Operator = Contentum.eQuery.Query.Operators.equals;
                                iratSearch.Ugyirat_Id.Value = ugyiratId;
                                iratSearch.Ugyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                                iratSearch.WhereByManual = " EREC_KuldKuldemenyek.Partner_Id_Bekuldo='" + pld.Partner_Id_Cimzett + "' ";
                                Result iratResult = iratService.GetAllWithExtension(execParamIrat, iratSearch);
                                if (!iratResult.IsError && iratResult.Ds.Tables[0].Rows.Count > 0)
                                {
                                    string cimSTR_Bekuldo = iratResult.Ds.Tables[0].Rows[0]["CimSTR_Bekuldo"] == DBNull.Value ? null : iratResult.Ds.Tables[0].Rows[0]["CimSTR_Bekuldo"].ToString();
                                    if (!string.IsNullOrEmpty(cimSTR_Bekuldo))
                                    {
                                        return cimSTR_Bekuldo + "||";
                                    }
                                }
                            }
                        }
                        else
                        {
                            Logger.Error("GetKRID iratpeldany Get error: " + pldResult.ErrorMessage);
                        }
                    }
                    #endregion

                    #region 07 Tipusu cim
                    Contentum.eAdmin.Service.KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                    KRT_Partnerek p = new KRT_Partnerek();
                    p.Typed.Id = new Guid(partnerId);
                    KRT_PartnerCimekSearch pcSearch = new KRT_PartnerCimekSearch();

                    pcSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                    pcSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                    pcSearch.ErvKezd.Group = "100";
                    pcSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

                    pcSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                    pcSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                    pcSearch.ErvVege.Group = "100";
                    pcSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

                    ExecParam execParamCim = new ExecParam();
                    execParamCim.Felhasznalo_Id = felhasznaloId;
                    Result partnerCimekResult = cimekService.GetAllByPartner(execParamCim, p, "07", pcSearch);

                    if (!partnerCimekResult.IsError && partnerCimekResult.Ds.Tables[0].Rows.Count > 0)
                    {
                        string cimId = partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"] == DBNull.Value ? string.Empty : partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"].ToString();
                        if (!string.IsNullOrEmpty(cimId))
                        {
                            KRT_Cimek cim = GetCimById(cimId, felhasznaloId);
                            if (cim != null)
                            {
                                return GetAppendedCim(cim) + "||" + cimId;
                            }
                        }
                    }

                    #endregion

                    #region Hivatali kapu

                    if (expedialasModja == Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
                    {
                        Contentum.eAdmin.Service.KRT_PartnerekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
                        ExecParam execParamPartner = new ExecParam();
                        execParamPartner.Felhasznalo_Id = felhasznaloId;
                        execParamPartner.Record_Id = partnerId;
                        Result partnerResult = service.Get(execParamPartner);

                        if (!partnerResult.IsError)
                        {
                            Result result = service.GetKRID(execParamPartner, partnerId);

                            if (result.IsError)
                            {
                                string errorMessage = ResultError.GetErrorMessageFromResultObject(result);
                                Logger.Error("GetKRID hiba: " + errorMessage);
                            }

                            KRT_Partnerek partner = (KRT_Partnerek)partnerResult.Record;

                            if (partner.Tipus == KodTarak.Partner_Tipus.Szemely)
                            {
                                KRT_Szemelyek psz = ((KRT_PartnerSzemelyek)partner).Szemelyek;

                                if (psz != null)
                                {
                                    string krid = GetSzemelyKapcsolatiKodHivataliKapu(psz);

                                    if (!string.IsNullOrEmpty(krid))
                                    {
                                        return krid + "||";
                                    }
                                }
                            }

                            if (partner.Tipus == KodTarak.Partner_Tipus.Szervezet)
                            {
                                string krid = GetPartnerKRIDByNevHivataliKapu(partner.Nev, Contentum.eIntegrator.Service.HivatalTipus.HivataliKapu);

                                if (!string.IsNullOrEmpty(krid))
                                {
                                    return krid + "||";
                                }
                                else
                                {
                                    return result.Uid + "||";
                                }
                            }
                        }
                        else
                        {
                            Logger.Error("GetKRID partner Get error: " + partnerResult.ErrorMessage);
                        }
                    }

                    #endregion
                }
            }
            #region Elektronikus de nem 19 vagy 190
            else
            {
                Contentum.eAdmin.Service.KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

                KRT_Partnerek p = new KRT_Partnerek();
                p.Typed.Id = new Guid(partnerId);
                KRT_PartnerCimekSearch pcSearch = new KRT_PartnerCimekSearch();
                pcSearch.WhereByManual = " and (KRT_Cimek.Tipus ='03RNY' or KRT_Cimek.Tipus ='03') ";
                pcSearch.OrderBy = " KRT_PartnerCimek.LetrehozasIdo DESC ";

                pcSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                pcSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                pcSearch.ErvKezd.Group = "100";
                pcSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

                pcSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                pcSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                pcSearch.ErvVege.Group = "100";
                pcSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

                ExecParam execParamCim = new ExecParam();
                execParamCim.Felhasznalo_Id = felhasznaloId;
                Result partnerCimekResult = cimekService.GetAllByPartner(execParamCim, p, null, pcSearch);

                if (!partnerCimekResult.IsError && partnerCimekResult.Ds.Tables[0].Rows.Count > 0)
                {
                    string cimId = partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"] == DBNull.Value ? string.Empty : partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"].ToString();
                    if (!string.IsNullOrEmpty(cimId))
                    {
                        KRT_Cimek cim = GetCimById(cimId, felhasznaloId);
                        if (cim != null)
                        {
                            return GetAppendedCim(cim) + "||" + cimId;
                        }
                    }
                }
            }
            #endregion
        }
        else if (iratPeldanyFajta == Contentum.eUtility.KodTarak.UGYINTEZES_ALAPJA.Hagyomanyos_papir)
        {
            Contentum.eAdmin.Service.KRT_CimekService cimekService = eAdminService.ServiceFactory.GetKRT_CimekService();

            KRT_Partnerek p = new KRT_Partnerek();
            p.Typed.Id = new Guid(partnerId);
            KRT_PartnerCimekSearch pcSearch = new KRT_PartnerCimekSearch();
            pcSearch.WhereByManual = " and (KRT_Cimek.Tipus ='01RNY' or KRT_Cimek.Tipus ='01') ";
            pcSearch.OrderBy = " KRT_PartnerCimek.LetrehozasIdo DESC ";

            pcSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            pcSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
            pcSearch.ErvKezd.Group = "100";
            pcSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

            pcSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
            pcSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
            pcSearch.ErvVege.Group = "100";
            pcSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;

            ExecParam execParamCim = new ExecParam();
            execParamCim.Felhasznalo_Id = felhasznaloId;
            Result partnerCimekResult = cimekService.GetAllByPartner(execParamCim, p, null, pcSearch);

            if (!partnerCimekResult.IsError && partnerCimekResult.Ds.Tables[0].Rows.Count > 0)
            {
                string cimId = partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"] == DBNull.Value ? string.Empty : partnerCimekResult.Ds.Tables[0].Rows[0]["Cim_Id"].ToString();
                if (!string.IsNullOrEmpty(cimId))
                {
                    KRT_Cimek cim = GetCimById(cimId, felhasznaloId);
                    if (cim != null)
                    {
                        return GetAppendedCim(cim) + "||" + cimId;
                    }
                }
            }
        }
        return String.Empty;
    }
    public static string GetHivataliKapuFiok()
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        string[] fiokok = svc.GetHivataliKapuFiokok();

        if (fiokok != null && fiokok.Length > 0)
        {
            return fiokok[0];
        }
        return null;
    }

    public static string GetPartnerKRIDByNevHivataliKapu(string nev, Contentum.eIntegrator.Service.HivatalTipus hivatalTipus)
    {
        try
        {
            Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
            Contentum.eIntegrator.Service.HivatalSzuroParameterek szuroParameterk = new Contentum.eIntegrator.Service.HivatalSzuroParameterek();
            szuroParameterk.Nev = nev;
            //szuroParameterk.RovidNev = tbRovidNev.Text;
            //szuroParameterk.NevOperator = OperatorTipus.And;
            szuroParameterk.Tipus = hivatalTipus;
            //szuroParameterk.TamogatottSzolgaltatasok = cbTamogatottSzolgaltatasok.Checked;
            szuroParameterk.TamogatottSzolgaltatasok = false;

            Result result = svc.HivatalokListajaFeldolgozas_Szurt(GetHivataliKapuFiok(), szuroParameterk);

            Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

            if (resultError.IsError)
            {
                return null;
            }


            Contentum.eIntegrator.Service.HivatalokListajaValasz valasz = result.Record as Contentum.eIntegrator.Service.HivatalokListajaValasz;

            if (valasz != null)
            {
                if (valasz.HivatalokLista.Length == 1)
                {
                    return valasz.HivatalokLista[0].KRID;
                }
            }

        }
        catch (Exception e)
        {
            Logger.Error("GetPartnerKRIDByNevHivataliKapu error: " + e.Message);
        }
        return null;
    }


    public static string GetSzemelyKapcsolatiKodHivataliKapu(KRT_Szemelyek szemely)
    {
        try
        {
            Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
            Contentum.eIntegrator.Service.AzonositasKerdes kerdes = new Contentum.eIntegrator.Service.AzonositasKerdes();
            kerdes.TermeszetesSzemelyAzonosito = new Contentum.eIntegrator.Service.hkpTermeszetesSzemelyAzonosito();

            #region viselt n�v
            kerdes.TermeszetesSzemelyAzonosito.ViseltNeve = new Contentum.eIntegrator.Service.NevAdat();
            kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.CsaladiNev = szemely.UjCsaladiNev;
            kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev1 = szemely.UjUtonev;
            kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev2 = szemely.UjTovabbiUtonev;
            #endregion

            #region anyja neve
            kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve = new Contentum.eIntegrator.Service.NevAdat();
            kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.CsaladiNev = szemely.AnyjaNeveCsaladiNev;
            kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev1 = szemely.AnyjaNeveElsoUtonev;
            kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev2 = szemely.AnyjaNeveTovabbiUtonev;
            #endregion

            kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely = new Contentum.eIntegrator.Service.hkpSzuletesiHely();
            kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Telepules = szemely.SzuletesiHely;

            DateTime ido;
            if (DateTime.TryParse(szemely.SzuletesiIdo, out ido))
            {
                kerdes.TermeszetesSzemelyAzonosito.SzuletesiIdo = ido;
            }

            Result result = svc.Azonositas(GetHivataliKapuFiok(), kerdes);

            if (result.IsError)
            {
                return null;
            }

            Contentum.eIntegrator.Service.AzonositasValasz valasz = result.Record as Contentum.eIntegrator.Service.AzonositasValasz;

            if (valasz != null)
            {
                if (valasz.HibaUzenet != null && !String.IsNullOrEmpty(valasz.HibaUzenet.Tartalom))
                {
                    return null;
                }

                if (valasz.AzonositottList.Length == 1)
                {
                    return valasz.AzonositottList[0].KapcsolatiKod;
                }
            }
        }
        catch (Exception ex)
        {
            Logger.Error("GetSzemelyKapcsolatiKodHivataliKapu error: " + ex.Message);
        }
        return null;
    }

}

