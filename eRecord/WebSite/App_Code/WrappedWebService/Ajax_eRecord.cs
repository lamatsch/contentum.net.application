using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using AjaxControlToolkit;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Web.Script.Serialization;


/// <summary>
/// Summary description for Ajax_eRecord
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebSite")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class Ajax_eRecord : System.Web.Services.WebService
{

    public Ajax_eRecord()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }



    #region �gazati jel, �gyk�r, �gyt�pus - Cascading DropDown WebMethods



    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetAgazatiJelek(string knownCategoryValues, string category, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }

        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        //CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //values.Add(emptyItem);

        ExecParam execParam = UI.SetExecParamDefault(Session);


        EREC_AgazatiJelekService service_agazatiJelek = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();

        EREC_AgazatiJelekSearch search_agJel = new EREC_AgazatiJelekSearch();
        search_agJel.OrderBy = "Kod";

        Result result_agjelGetAll = service_agazatiJelek.GetAllWithIktathat(execParam, search_agJel);
        if (!String.IsNullOrEmpty(result_agjelGetAll.ErrorCode))
        {
            // hiba:
            return null;
        }
        else
        {
            foreach (DataRow row in result_agjelGetAll.Ds.Tables[0].Rows)
            {
                string Id = row["Id"].ToString();
                string Nev = row["Nev"].ToString();
                string Kod = row["Kod"].ToString();
                try
                {
                    values.Add(new CascadingDropDownNameValue("[" + Kod + "] " + Nev, Id));
                }
                catch
                {
                    values.Clear();
                    values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                }
            }

        }


        //if (values.Count == 0)
        //{
        //    CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //    values.Add(emptyItem);
        //    emptyItem.isDefaultValue = true;
        //}


        return values.ToArray();

        //return default(AjaxControlToolkit.CascadingDropDownNameValue[]);
    }

    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetUgykorokByAgazatiJel(string knownCategoryValues, string category, string contextKey)
    {
        Logger.DebugStart("GetUgykorokByAgazatiJel");

        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }


        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
          knownCategoryValues);


        // A kiv�lasztott �gazati jel:
        string agazatiJel_Id = "";

        if (kv.ContainsKey("AgazatiJel") == false)
        {
            return null;
        }
        else
        {
            agazatiJel_Id = kv["AgazatiJel"];

        }

        if (String.IsNullOrEmpty(agazatiJel_Id))
        {
            return null;
        }


        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

        search.AgazatiJel_Id.Value = agazatiJel_Id;
        search.AgazatiJel_Id.Operator = Query.Operators.equals;

        search.OrderBy = "IrattariTetelszam";

        Logger.DebugStart("service.GetAllWithIktathat");

        Result result = service.GetAllWithIktathat(execParam, search);

        Logger.DebugEnd("service.GetAllWithIktathat");

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        int resultCount = result.Ds.Tables[0].Rows.Count;

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string irattariTetel_Id = row["Id"].ToString();
            string irattariTetel_Ittsz = row["IrattariTetelszam"].ToString();

            string irattariTetel_Nev = row["Nev"].ToString();
            //if (irattariTetel_Nev.Length > 50)
            //{
            //    irattariTetel_Nev = irattariTetel_Nev.Substring(0, 50) + "...";
            //}

            CascadingDropDownNameValue item = new CascadingDropDownNameValue(irattariTetel_Ittsz + " (" + irattariTetel_Nev + ")", irattariTetel_Id);
            values.Add(item);
            if (resultCount == 1)
            {
                item.isDefaultValue = true;
            }
        }

        //if (values.Count == 0)
        //{
        //    CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //    values.Add(emptyItem);
        //    emptyItem.isDefaultValue = true;
        //}

        Logger.DebugEnd("GetUgykorokByAgazatiJel");

        return values.ToArray();
    }


    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetIrattariTetelszamList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("Ajax_eRecord\\GetIrattariTetelszamList elindul.");
        Logger.Debug(String.Format("Param�terek: prefixText: {0}, count: {1}, contextKey: {2}", prefixText, count, contextKey));
        try
        {
            string userId = contextKey;
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = userId;
            execParam.LoginUser_Id = userId;

            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();
            if (!String.IsNullOrEmpty(prefixText) && prefixText.Length > 1)
            {
                prefixText = prefixText.Replace("?", "").TrimEnd();
                search.IrattariTetelszam.Value = prefixText + "%";
                search.IrattariTetelszam.Operator = Query.Operators.like;
            }
            Result result = service.GetAllWithExtension(execParam, search);

            List<string> res = new List<string>();
            if (result != null && result.Ds != null)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string autoCompleteItem = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(
                        row["Merge_IrattariTetelszam"].ToString(),
                        //row["IrattariTetelszam"].ToString(), 
                        row["ID"].ToString() + row["Nev"].ToString());

                    res.Add(autoCompleteItem);
                }
            }

            Logger.Debug("Ajax_eRecord\\GetIrattariTetelszamList v�ge.");
            Logger.DebugEnd();
            return res.ToArray();
        }
        catch (Exception e)
        { // TODO
            Logger.Error("GetIrattariTetelszamList hiba: " + e.Message);
            return null;
        }
    }

    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetUgytipusokByUgykor(string knownCategoryValues, string category, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }

        List<CascadingDropDownNameValue> values =
           new List<CascadingDropDownNameValue>();

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
          knownCategoryValues);


        // A kiv�lasztott �gyk�r:
        string ugykor_Id = "";

        if (kv.ContainsKey("Ugykor") == false)
        {
            return null;
        }
        else
        {
            ugykor_Id = kv["Ugykor"];

        }

        if (String.IsNullOrEmpty(ugykor_Id))
        {
            return null;
        }



        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

        search.Ugykor_Id.Value = ugykor_Id;
        search.Ugykor_Id.Operator = Query.Operators.equals;

        search.Ugytipus.Value = "";
        search.Ugytipus.Operator = Query.Operators.notnull;

        search.EljarasiSzakasz.Operator = Query.Operators.isnull;
        search.Irattipus.Operator = Query.Operators.isnull;

        search.OrderBy = "UgytipusNev";

        Result result = service.GetAll(execParam, search);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            //string iratMetaDef_Id = row["Id"].ToString();
            string Ugytipus = row["Ugytipus"].ToString();
            string UgytipusNev = row["UgytipusNev"].ToString();

            if (!String.IsNullOrEmpty(UgytipusNev))
            {
                values.Add(new CascadingDropDownNameValue(UgytipusNev, Ugytipus));
            }
        }

        if (values.Count == 1)
        {
            values[0].isDefaultValue = true;
        }

        //if (values.Count == 0)
        //{
        //    CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //    values.Add(emptyItem);
        //    emptyItem.isDefaultValue = true;
        //}

        return values.ToArray();
    }





    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetIktatokonyvekByIrattaritetel(string knownCategoryValues, string category, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }

        string felhasznaloId = "";
        string ugykor_Id = "";



        List<CascadingDropDownNameValue> values =
           new List<CascadingDropDownNameValue>();

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
          knownCategoryValues);

        // �gazati jel van?
        string agazatiJel_Id = "";

        if (kv.ContainsKey("AgazatiJel") == false)
        {
            agazatiJel_Id = "";
        }
        else
        {
            agazatiJel_Id = kv["AgazatiJel"];
        }



        // ha van ';' --> meg van adva explicit az iratt�rit�telsz�m
        if (contextKey.Contains(";"))
        {
            string[] array = contextKey.Split(';');
            felhasznaloId = array[0];
            ugykor_Id = array[1];
        }
        else
        {
            felhasznaloId = contextKey;

            // �gyk�r (Iratt�ri t�telsz�m)

            if (kv.ContainsKey("Ugykor") == false)
            {
                ugykor_Id = "";
            }
            else
            {
                ugykor_Id = kv["Ugykor"];
            }
        }




        //if (String.IsNullOrEmpty(ugykor_Id))
        //{
        //    return null;
        //}

        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();

        //search.OrderBy = "MegkulJelzes";
        search.OrderBy = "Iktatohely";

        search.Ev.Value = DateTime.Today.Year.ToString();
        search.Ev.Operator = Query.Operators.equals;

        // csak a lez�ratlanok jelennek meg
        //search.LezarasDatuma.Value = "";
        //search.LezarasDatuma.Operator = Query.Operators.isnull;

        //LZS - BLG_2549 - Lez�rt iktat�k�nyv figyel�s.
        string sqlFormattedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
        search.WhereByManual = " and (EREC_IraIktatoKonyvek.LezarasDatuma >= '" + sqlFormattedDate + "' or EREC_IraIktatoKonyvek.LezarasDatuma is null)";

        search.IktatoErkezteto.Value = "I";
        search.IktatoErkezteto.Operator = Query.Operators.equals;

        Result result = service.GetAllWithIktathatByIrattariTetel(execParam, search, ugykor_Id);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        int resultCount = result.Ds.Tables[0].Rows.Count;

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string Id = row["Id"].ToString();

            CascadingDropDownNameValue item = new CascadingDropDownNameValue(IktatoKonyvek.GetIktatokonyvText(row), Id);
            values.Add(item);

            if (resultCount == 1)
            {
                item.isDefaultValue = true;
            }
        }

        //if (values.Count == 0)
        //{
        //    CascadingDropDownNameValue emptyItem = new CascadingDropDownNameValue(Resources.Form.EmptyListItem, "");
        //    values.Add(emptyItem);
        //    emptyItem.isDefaultValue = true;
        //}

        return values.ToArray();
    }

    #endregion

    #region �gyt�pus �gyk�rh�z, norm�l dropdown listhez (�gyirat m�dos�t�s k�perny�re)

    /// <summary>
    /// M�dos�t�shoz, keres�shez �gyt�pus lista az �gyk�r Id alapj�n. A contextKey-t haszn�lva param�terezhet�, a knownCategoryValues-t
    /// figyelmen k�v�l hagyja (�n�ll� dropdownlisthez, ahol nincs f�l�rendelt cascading dropdown)
    /// </summary>
    /// <param name="knownCategoryValues"></param>
    /// <param name="category"></param>
    /// <param name="contextKey">ugykor_id|selected_id|check_ervenyesseg szerkezetu, ahol az ugykor_id k�telez�,
    /// a selected_id �s a check_ervenyesseg opcionalis, de ha van check_ervenyesseg, akkor a selected_id-t legal�bb �res elemk�nt meg kell adni.
    /// A check_ervenyesseg erteke 0 vagy 1 lehet, ha 0, az �rv�nytelen t�teleket is lek�rj�k</param>
    /// <returns></returns>
    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetUgytipusokByUgykorForModify(string knownCategoryValues, string category, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }

        List<CascadingDropDownNameValue> values =
           new List<CascadingDropDownNameValue>();

        //StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
        //  knownCategoryValues);


        // A kiv�lasztott �gyk�r:
        char[] separators = new char[1] { '|' };
        //string[] contextKeySplitted = contextKey.Split(separators, StringSplitOptions.RemoveEmptyEntries);
        string[] contextKeySplitted = contextKey.Split(separators, StringSplitOptions.None);
        string ugykor_Id = "";
        string selectedUgytipus = "";
        bool bCheckErvenyeseg = true;
        string ErvenyessegDatum = "";
        if (contextKeySplitted.Length > 0)
        {
            ugykor_Id = contextKeySplitted[0];
        }

        if (String.IsNullOrEmpty(ugykor_Id))
        {
            return null;
        }

        if (contextKeySplitted.Length > 1)
        {
            selectedUgytipus = contextKeySplitted[1];
        }

        if (contextKeySplitted.Length > 2)
        {
            string strCheckErvenyesseg = contextKeySplitted[2];
            if (strCheckErvenyesseg == "0")
            {
                bCheckErvenyeseg = false;
            }
        }

        if (bCheckErvenyeseg)
        {
            if (contextKeySplitted.Length > 3)
            {
                ErvenyessegDatum = contextKeySplitted[3];
            }
        }

        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

        search.Ugykor_Id.Value = ugykor_Id;
        search.Ugykor_Id.Operator = Query.Operators.equals;

        search.Ugytipus.Value = "";
        search.Ugytipus.Operator = Query.Operators.notnull;

        search.EljarasiSzakasz.Operator = Query.Operators.isnull;
        search.Irattipus.Operator = Query.Operators.isnull;

        //if (!String.IsNullOrEmpty(selectedUgytipus) || bCheckErvenyeseg == false)
        if (bCheckErvenyeseg == false)
        {
            #region �rv�nyess�g ellen�rz�s kikapcsol�sa
            search.ErvKezd.Clear();
            search.ErvVege.Clear();
            #endregion �rv�nyess�g ellen�rz�s kikapcsol�sa
        }
        else if (!String.IsNullOrEmpty(ErvenyessegDatum))
        {
            #region �rv�nyess�g ellen�rz�s bekapcsol�sa
            search.ErvKezd.Group = "345";
            search.ErvKezd.GroupOperator = Query.Operators.and;
            search.ErvKezd.Value = ErvenyessegDatum;
            search.ErvKezd.Operator = Query.Operators.lessorequal;
            search.ErvVege.Group = "345";
            search.ErvVege.GroupOperator = Query.Operators.and;
            search.ErvVege.Value = ErvenyessegDatum;
            search.ErvVege.Operator = Query.Operators.greaterorequal;
            #endregion �rv�nyess�g ellen�rz�s bekapcsol�sa
        }
        else
        {
            #region Iratt�ri t�tel lek�r�se
            EREC_IraIrattariTetelek erec_IraIrattariTetelek = null;

            // meg kell vizsg�lni az iratt�ri t�tel �rv�nyess�g�nek tartom�ny�t
            EREC_IraIrattariTetelekService service_iit = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();

            ExecParam execParam_iit = UI.SetExecParamDefault(Session);
            execParam_iit.Record_Id = ugykor_Id;

            Result result_iit = service_iit.Get(execParam_iit);

            if (!result_iit.IsError)
            {
                erec_IraIrattariTetelek = (EREC_IraIrattariTetelek)result_iit.Record;
            }
            #endregion Iratt�ri t�tel lek�r�se

            if (erec_IraIrattariTetelek != null)
            {
                // �rv�nyess�g kezdete ne lehessen j�v�beni:
                DateTime ErvVege_iit;
                string strErvVegeCondition = erec_IraIrattariTetelek.ErvVege;
                if (DateTime.TryParse(erec_IraIrattariTetelek.ErvVege, out ErvVege_iit)
                    && DateTime.Now < ErvVege_iit)
                {
                    strErvVegeCondition = Query.SQLFunction.getdate;
                }

                #region �rv�nyess�g ellen�rz�s bekapcsol�sa
                search.ErvKezd.Group = "345";
                search.ErvKezd.GroupOperator = Query.Operators.and;
                // ne legyen j�v�beni: 
                search.ErvKezd.Value = strErvVegeCondition;
                search.ErvKezd.Operator = Query.Operators.less;
                search.ErvVege.Group = "345";
                search.ErvVege.GroupOperator = Query.Operators.and;
                search.ErvVege.Value = erec_IraIrattariTetelek.ErvKezd;
                search.ErvVege.Operator = Query.Operators.greater;
                #endregion �rv�nyess�g ellen�rz�s bekapcsol�sa
            }
        }

        search.OrderBy = "UgytipusNev";

        Result result = service.GetAll(execParam, search);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        string deletedValue = "[" + Resources.Form.UI_ErvenytelenitettTetel + "]";

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            #region �rv�nyess�g ellen�rz�s
            bool ervenyes = true;

            //if (bCheckErvenyeseg == true)
            //{
            DateTime ErvKezd;
            DateTime ErvVege;

            if (DateTime.TryParse(row["ErvVege"].ToString(), out ErvVege) && ErvVege < DateTime.Now)
            {
                ervenyes = false;
            }
            else if (DateTime.TryParse(row["ErvKezd"].ToString(), out ErvKezd) && ErvKezd > DateTime.Now)
            {
                ervenyes = false;
            }
            //}
            #endregion �rv�nyess�g ellen�rz�s

            string Ugytipus = row["Ugytipus"].ToString();
            string UgytipusNev = row["UgytipusNev"].ToString();

            // CR#2265: be�rjuk az �rv�nytelen t�teleket is, nem csak a kiv�lasztottat
            //if (ervenyes || bCheckErvenyeseg == false)
            //{
            if (ervenyes == false)
            {
                if (!String.IsNullOrEmpty(UgytipusNev))
                {
                    UgytipusNev += " " + deletedValue;
                }
                else
                {
                    UgytipusNev = Ugytipus + " " + deletedValue;
                }
            }

            if (!String.IsNullOrEmpty(UgytipusNev))
            {
                values.Add(new CascadingDropDownNameValue(UgytipusNev, Ugytipus, false));
            }
            else
            {
                values.Add(new CascadingDropDownNameValue(Ugytipus, Ugytipus, false));
            }

            // kiv�laszt�s
            if (!String.IsNullOrEmpty(selectedUgytipus) && Ugytipus == selectedUgytipus)
            {
                values[values.Count - 1].isDefaultValue = true;
            }
            //}
            //else if (Ugytipus == selectedUgytipus)
            //{
            //    string deletedValue = "[" + Resources.Form.UI_ErvenytelenitettTetel + "]";
            //    if (!String.IsNullOrEmpty(UgytipusNev))
            //    {
            //        values.Insert(0, new CascadingDropDownNameValue(UgytipusNev + " " + deletedValue, Ugytipus));
            //    }
            //    else
            //    {
            //        values.Insert(0, new CascadingDropDownNameValue(Ugytipus + " " + deletedValue, Ugytipus));
            //    }
            //}
        }


        if (!String.IsNullOrEmpty(selectedUgytipus))
        {
            if (!values.Exists(delegate (CascadingDropDownNameValue match) { return match.isDefaultValue; }))
            {
                string selectedUgytipusNev = selectedUgytipus;
                #region selectedUgytipus lek�r�se

                search.Ugytipus.Value = selectedUgytipus;
                search.Ugytipus.Operator = Query.Operators.equals;
                search.ErvKezd.Clear();
                search.ErvVege.Clear();
                result = service.GetAll(execParam, search);
                if (!result.IsError && result.Ds.Tables[0].Rows.Count > 0)
                {
                    selectedUgytipusNev = result.Ds.Tables[0].Rows[0]["UgytipusNev"].ToString();
                }

                #endregion

                values.Insert(0, new CascadingDropDownNameValue(selectedUgytipusNev + " " + deletedValue, selectedUgytipus));
            }
        }

        if (values.Count == 1 && String.IsNullOrEmpty(selectedUgytipus))
        {
            values[0].isDefaultValue = true;
        }
        return values.ToArray();
    }

    //[System.Web.Services.WebMethod(CacheDuration = 300, EnableSession=true)]
    //[System.Web.Script.Services.ScriptMethod]
    //public string[] GetUgytipusokByUgykorForDropDown(string ugykor_Id)
    //{
    //    if (String.IsNullOrEmpty(ugykor_Id))
    //    {
    //        return null;
    //    }

    //    List<string> values = new List<string>();

    //    EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

    //    ExecParam execParam = UI.SetExecParamDefault(Session);

    //    EREC_IratMetaDefinicioSearch search = new EREC_IratMetaDefinicioSearch();

    //    search.Ugykor_Id.Value = ugykor_Id;
    //    search.Ugykor_Id.Operator = Query.Operators.equals;

    //    search.Ugytipus.Value = "";
    //    search.Ugytipus.Operator = Query.Operators.notnull;

    //    search.EljarasiSzakasz.Operator = Query.Operators.isnull;
    //    search.Irattipus.Operator = Query.Operators.isnull;

    //    search.OrderBy = "UgytipusNev";

    //    Result result = service.GetAll(execParam, search);

    //    if (!String.IsNullOrEmpty(result.ErrorCode))
    //    {
    //        return null;
    //    }

    //    foreach (DataRow row in result.Ds.Tables[0].Rows)
    //    {
    //        //string iratMetaDef_Id = row["Id"].ToString();
    //        string Ugytipus = row["Ugytipus"].ToString();
    //        string UgytipusNev = row["UgytipusNev"].ToString();

    //        if (!String.IsNullOrEmpty(Ugytipus))
    //        {
    //            values.Add(Ugytipus + "|" + UgytipusNev);
    //        }
    //    }
    //    return values.ToArray();
    //    //return String.Join("|", values.ToArray());
    //} 

    #endregion �gyt�pus �gyk�rh�z, norm�l dropdown listhez (�gyirat m�dos�t�s k�perny�re)

    #region (�gy)felel�s meghat�roz�s (Iktat�s k�perny�re)

    [System.Web.Services.WebMethod(CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetFelelosCsoportokByUgykorUgytipus(string UgykorId, string Ugytipus, string UserId, string UserCsoportId)
    {
        Logger.Debug("Ajax - GetFelelosCsoportokByUgykorUgytipus START");
        Logger.Debug(String.Format("Bemen� param�terek: UgykorId: {0}; Ugytipus: {1}; UserId: {2}; UserCsoportId: {3}",
            new string[] { UgykorId, Ugytipus, UserId, UserCsoportId }));
        if (String.IsNullOrEmpty(UgykorId) || String.IsNullOrEmpty(UserId))
        {
            return "";
        }

        // Szign�l�s t�pus�nak �s a javasolt felel�snek a lek�r�se

        EREC_SzignalasiJegyzekekService service = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = UserId;
        execParam.LoginUser_Id = UserId;


        Result result = service.GetSzignalasTipusaByUgykorUgytipus(execParam, UgykorId, Ugytipus);
        if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
        {
            Logger.Debug(String.Format("Hiba: {0}", ResultError.GetErrorMessageFromResultObject(result)));
            Logger.Debug("Ajax - GetFelelosCsoportokByUgykorUgytipus END");
            // hiba:
            return "";
        }

        string szignalasTipusa = result.Record.ToString();
        string javasoltCsoport = result.Uid;

        Logger.Debug(String.Format("Szign�l�s t�pusa: {0}", szignalasTipusa));
        Logger.Debug(String.Format("Javasolt felel�s: {0}", javasoltCsoport));

        string strReturn = String.Empty;

        switch (szignalasTipusa)
        {
            case "1":
            case "2":
                {
                    // iktat�t �ll�tjuk be kezel�nek:

                    string iktatoNev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(UserId, Context.Cache);
                    if (String.IsNullOrEmpty(iktatoNev))
                    {
                        //return "";
                    }
                    else
                    {
                        //return ";;" + UserId + "," + iktatoNev;
                        strReturn = ";;" + UserId + "," + iktatoNev;
                    }
                }
                break;
            case "3":
                {
                    // kezel� az iktat� szervezet vezet�je lesz:

                    string vezetoNeve = "";
                    string vezetoId = GetCsoportVezeto(UserCsoportId, UserId, out vezetoNeve);
                    if (String.IsNullOrEmpty(vezetoId) || String.IsNullOrEmpty(vezetoNeve))
                    {
                        //return "";
                    }
                    else
                    {
                        //return ";;" + vezetoId + "," + vezetoNeve;
                        strReturn = ";;" + vezetoId + "," + vezetoNeve;
                    }
                }
                break;
            case "4":
                {
                    // �gyfelel�s a megadott csoport, �s kezel� a megadott csoport vezet�je lesz

                    string vezetoNeve = "";
                    string vezetoId = GetCsoportVezeto(javasoltCsoport, UserId, out vezetoNeve);

                    string javasoltCsoportNeve = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(javasoltCsoport, Context.Cache);
                    if ((String.IsNullOrEmpty(vezetoId) || String.IsNullOrEmpty(vezetoNeve))
                        && (String.IsNullOrEmpty(javasoltCsoport) || String.IsNullOrEmpty(javasoltCsoportNeve))
                       )

                    {
                        //return "";
                    }
                    else
                    {
                        ////return vezetoId + "," + vezetoNeve + ";;" + vezetoId + "," + vezetoNeve;
                        //return String.Format("{0},{1};;{2},{3}", new string[] { javasoltCsoport ?? ""
                        //    , javasoltCsoportNeve ?? "", vezetoId ?? "", vezetoNeve ?? ""});
                        strReturn = String.Format("{0},{1};;{2},{3}", new string[] { javasoltCsoport ?? ""
                            , javasoltCsoportNeve ?? "", vezetoId ?? "", vezetoNeve ?? ""});
                    }
                }
                break;
            case "5":
                {
                    string ugyintezoId = javasoltCsoport;
                    string ugyintezoNev = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(ugyintezoId, Context.Cache);
                    if (String.IsNullOrEmpty(ugyintezoNev))
                    {
                        //return "";
                    }
                    else
                    {
                        //return ";" + ugyintezoId + "," + ugyintezoNev + ";" + ugyintezoId + "," + ugyintezoNev;
                        strReturn = ";" + ugyintezoId + "," + ugyintezoNev + ";" + ugyintezoId + "," + ugyintezoNev; ;
                    }
                }
                break;
            case "6":
                // iktat� ad meg mindent a formon
                //return "";
                break;
            default:
                //return "";
                break;

        }

        Logger.Debug(String.Format("Return: {0}", strReturn));
        Logger.Debug("Ajax - GetFelelosCsoportokByUgykorUgytipus END");

        return strReturn;

    }


    private string GetCsoportVezeto(string csoportId, string UserId, out string CsoportNev)
    {
        CsoportNev = "";

        if (String.IsNullOrEmpty(csoportId))
        {
            return null;
        }

        // Megadott szervezet vezet�j�nek lek�rdez�se:
        Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok =
            Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

        ExecParam execParam_getLeader = new ExecParam();
        execParam_getLeader.Felhasznalo_Id = UserId;
        execParam_getLeader.LoginUser_Id = UserId;


        Result result_GetLeader = service_csoportok.GetLeader(execParam_getLeader, csoportId);
        if (!String.IsNullOrEmpty(result_GetLeader.ErrorCode)
            || result_GetLeader.Record == null)
        {
            return null;
        }

        KRT_Csoportok csoportVezeto = (KRT_Csoportok)result_GetLeader.Record;

        if (!String.IsNullOrEmpty(csoportVezeto.Id))
        {
            CsoportNev = csoportVezeto.Nev;
            return csoportVezeto.Id;
        }
        else
        {
            return null;
        }
    }

    #endregion

    #region Gener�lt t�rgy lek�r�se �gyk�r, �gyt�pus alapj�n
    [System.Web.Services.WebMethod(CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetGeneraltTargyByUgykorUgytipus(string UgykorId, string Ugytipus, string UserId)
    {
        Logger.Debug("GetGeneraltTargyByUgykorUgytipus START");
        Logger.Debug(string.Format("Bej�v� param�terek: UgykorId: {0}; Ugytipus: {1}", UgykorId, Ugytipus));
        if (String.IsNullOrEmpty(UgykorId) || String.IsNullOrEmpty(UserId))
        {
            Logger.Warn("Hi�nyz� bej�v� param�terek!");
            Logger.Debug("GetGeneraltTargyByUgykorUgytipus END");
            return "";
        }

        // Irat metadefin�ci� lek�r�se

        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = UserId;
        execParam.LoginUser_Id = UserId;

        Result result = service.GetIratMetaDefinicioByUgykorUgytipus(execParam, UgykorId, Ugytipus);
        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            // hiba:
            Logger.Error(String.Format("Hiba az irat metadefin�ci� lek�r�sn�l! Hiba�zenet: {0}", ResultError.GetErrorMessageFromResultObject(result)));
            Logger.Debug("GetGeneraltTargyByUgykorUgytipus END");
            return "";
        }

        EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
        if (erec_IratMetaDefinicio == null)
        {
            // hiba:
            Logger.Error("Az irat metadefin�ci� rekord �rt�ke NULL");
            Logger.Debug("GetGeneraltTargyByUgykorUgytipus END");
            return "";
        }

        Logger.Debug(String.Format("Gener�lt t�rgy: {0}", erec_IratMetaDefinicio.GeneraltTargy));
        Logger.Debug("GetGeneraltTargyByUgykorUgytipus END");
        return erec_IratMetaDefinicio.GeneraltTargy;
    }

    #endregion Gener�lt t�rgy lek�r�se �gyk�r, �gyt�pus alapj�n

    #region Iratt�ri t�telsz�m (�sszef�z�tt) meghat�roz�sa (iktat�s k�perny�re)

    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string Get_Merge_IrattariTetelszamByUgykor(string UgykorId, string UserId)
    {
        Logger.Debug("Get_Merge_IrattariTetelszamByUgykor start");

        if (String.IsNullOrEmpty(UgykorId) || String.IsNullOrEmpty(UserId))
        {
            return "";
        }

        string Merge_IrattariTetelszam = "";
        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

        search.Id.Value = UgykorId;
        search.Id.Operator = Query.Operators.equals;

        search.OrderBy = "IrattariTetelszam";

        Logger.DebugStart("Get_Merge_IrattariTetelszamByUgykor");
        Result result = service.GetAllWithExtension(execParam, search);
        Logger.DebugStart("Get_Merge_IrattariTetelszamByUgykor");

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return "";
        }

        // csak egy ilyen iratt�ri t�tel lehet
        if (result.Ds.Tables[0].Rows.Count > 0)
        {
            Merge_IrattariTetelszam = result.Ds.Tables[0].Rows[0]["Merge_IrattariTetelszam"].ToString();
        }

        Logger.Debug("Get_Merge_IrattariTetelszamByUgykor end");
        return Merge_IrattariTetelszam;
    }

    #endregion Iratt�ri t�telsz�m (�sszef�z�tt) meghat�roz�sa (iktat�s k�perny�re)

    #region IDOEGYSEG KodtarCache
    private const String cache_key_idoegyseg = "IdoegysegDictionary";   // Context.Cache, mert az Ajax_eRecord haszn�lja �s nincs Page

    public string Get_IDOEGYSEG_Nev(string UserId, string Kod)
    {
        Dictionary<string, string> IdoegysegDictionary = null;
        string name = "???";
        if (Context.Cache[cache_key_idoegyseg] == null)
        {
            IdoegysegDictionary = new Dictionary<string, string>();
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = UserId;
            execParam.LoginUser_Id = UserId;
            int KodCsoportokCache_RefreshPeriod_Minute =
                Rendszerparameterek.GetInt(execParam, Rendszerparameterek.KodCsoportokCache_RefreshPeriod_Minute);
            if (KodCsoportokCache_RefreshPeriod_Minute == 0)
            {
                KodCsoportokCache_RefreshPeriod_Minute = 600;
            }

            // Dictionary felv�tele a Cache-be:
            Context.Cache.Insert(cache_key_idoegyseg, IdoegysegDictionary, null
                    , DateTime.Now.AddMinutes(KodCsoportokCache_RefreshPeriod_Minute), System.Web.Caching.Cache.NoSlidingExpiration
            , System.Web.Caching.CacheItemPriority.NotRemovable, null);

            Contentum.eAdmin.Service.KRT_KodTarakService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
            Result result = service.GetAllByKodcsoportKod(execParam, "IDOEGYSEG");
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    String kodtarNev = row["Nev"].ToString();
                    String kodtarKod = row["Kod"].ToString();

                    // csak ha m�g nincs ilyen k�d:
                    if (IdoegysegDictionary.ContainsKey(kodtarKod) == false)
                    {
                        IdoegysegDictionary.Add(kodtarKod, kodtarNev);
                    }
                }

                if (IdoegysegDictionary.ContainsKey(Kod))
                {
                    name = IdoegysegDictionary[Kod];
                }

            }
        }
        else
        {
            // �rt�kek a Dictionary-b�l
            IdoegysegDictionary = (Dictionary<string, string>)Context.Cache[cache_key_idoegyseg];

            if (IdoegysegDictionary.ContainsKey(Kod))
            {
                name = IdoegysegDictionary[Kod];
            }
            else
            {
                // Nincs bent a Dictionary-ben, le kell k�rni az adatb�zisb�l:
                Contentum.eAdmin.Service.KRT_KodTarakService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
                ExecParam execParam = new ExecParam();
                execParam.Felhasznalo_Id = UserId;
                execParam.LoginUser_Id = UserId;
                Result result = service.GetAllByKodcsoportKod(execParam, "IDOEGYSEG");

                if (result != null && String.IsNullOrEmpty(result.ErrorCode))
                {
                    DataRow[] rows = result.Ds.Tables[0].Select("Kod = '" + Kod + "'");
                    if (rows.Length > 0)
                    {
                        name = rows[0]["Nev"].ToString();
                        IdoegysegDictionary.Add(Kod, name);
                    }
                }
            }
        }

        return name;
    }

    #endregion IDOEGYSEG KodtarCache

    #region D�tumform�z�s
    private string GetFormattedShortDateStringForUI(string UserId, DateTime date)
    {
        string shortDateStr = String.Empty;
        ExecParam xpm = new ExecParam();
        xpm.Felhasznalo_Id = UserId;
        string ShortDatePattern = Rendszerparameterek.Get(xpm, Rendszerparameterek.SHORT_DATE_PATTERN);
        if (!String.IsNullOrEmpty(ShortDatePattern))
        {
            try
            {
                shortDateStr = date.ToString(ShortDatePattern);
            }
            catch
            {
                shortDateStr = date.ToShortDateString();
            }
        }
        else
        {
            shortDateStr = date.ToShortDateString();
        }

        return shortDateStr;
    }
    #endregion D�tumform�z�s

    #region �gyirat int�z�si hat�rid� meghat�roz�s (irat metadefin�ci� alapj�n) (Iktat�s k�perny�re)

    /// <summary>
    /// A �tadott param�terek alapj�n lek�rdezi az el��rt int�z�si id�t (ha van ilyen) �s annak k�t�tts�g�t
    /// </summary>
    /// <returns>int�z�si id�, k�t�tts�g, sz�m�tott hat�rid�, esetleges tooltip
    /// �s �gyirat hat�rid� kitol�si flag sz�veg pontosvessz�vel (';') elv�lasztva</returns>
    [System.Web.Services.WebMethod(CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetIntezesiIdoWithToolTip(string UgykorId, string Ugytipus, string UserId, string IktatasDatuma)
    {
        DateTime dtIktatasDatuma;
        if (!DateTime.TryParse(IktatasDatuma, out dtIktatasDatuma))
        {
            dtIktatasDatuma = DateTime.Now;
        }

        string result = GetIntezesiIdoByUgykorUgytipus(UgykorId, Ugytipus, UserId);

        string tooltip = "";
        string shortDateStr = "";
        //int nIntezesiIdo = 30; // default int�z�si id�
        //int nIdoegysegMin = Int32.Parse(KodTarak.IDOEGYSEG.DEFAULT); //-1440; default id�egys�g percben (1 munkanap)
        string strIntezesiIdo = "";
        string strIntezesiIdoKotott = "";
        string strIdoegysegMin = "";
        string strUgyiratHataridoKitolas = "1";// default: a hat�rid� kitolhat�

        char[] separators = new char[1] { ';' };
        string[] ugyIntezesiHataridoArray = result.Split(separators);

        if (ugyIntezesiHataridoArray.Length == 4)
        {
            strIntezesiIdo = ugyIntezesiHataridoArray[0];
            strIdoegysegMin = ugyIntezesiHataridoArray[1];
            strIntezesiIdoKotott = ugyIntezesiHataridoArray[2];
            strUgyiratHataridoKitolas = ugyIntezesiHataridoArray[3];

            if (!String.IsNullOrEmpty(strIntezesiIdo) && strIntezesiIdo != "0")
            {
                string strIdoegysegNev = Get_IDOEGYSEG_Nev(UserId, strIdoegysegMin);
                tooltip = String.Format(Resources.Form.UI_IntezesiIdo_ToolTip, strIntezesiIdo, strIdoegysegNev);

                if (strIntezesiIdoKotott == "1")
                {
                    tooltip += Resources.Form.UI_IntezesiIdoKotott_ToolTip;
                }
            }
            else
            {
                tooltip += Resources.Form.UI_IntezesiIdoDefault_ToolTip;
            }

            if (strUgyiratHataridoKitolas == "0")
            {
                tooltip += (tooltip == "" ? "" : "\n") + Resources.Form.UI_UgyiratHataridoKitolas_Forbidden_ToolTip;
            }
            else
            {
                tooltip += (tooltip == "" ? "" : "\n") + Resources.Form.UI_UgyiratHataridoKitolas_Allowed_ToolTip;
            }
        }
        else
        {
            tooltip += Resources.Form.UI_IntezesiIdoDefault_ToolTip;
        }

        //DateTime SomeDaysAfterToday = DateTime.Now.AddDays(nIntezesiIdo);
        //shortDateStr = SomeDaysAfterToday.ToShortDateString();

        Logger.Debug("Hat�rid� meghat�roz�s  START");
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = UserId;
        execParam.LoginUser_Id = UserId;

        String ErrorMessage = String.Empty;
        DateTime dtHatarido = Contentum.eUtility.Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, dtIktatasDatuma, strIntezesiIdo, strIdoegysegMin, out ErrorMessage);
        //DateTime dtHatarido = Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(Context.Cache, execParam, strIdoegysegMin, strIntezesiIdo, dtIktatasDatuma, out ErrorMessage);
        shortDateStr = GetFormattedShortDateStringForUI(UserId, dtHatarido);

        #region SAKKORA
        DateTime ugyintKezdeteDateTime = DateTime.MinValue;
        string ugyintKezdeteDateTimeShortString = null;
        if (Contentum.eUtility.Rendszerparameterek.UseSakkora(execParam))
        {
            ugyintKezdeteDateTime = Contentum.eUtility.Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, strIntezesiIdo, shortDateStr, string.Empty);
            if (ugyintKezdeteDateTime != null)
            {
                ugyintKezdeteDateTimeShortString = GetFormattedShortDateStringForUI(UserId, ugyintKezdeteDateTime);
            }
        }
        #endregion

        if (!String.IsNullOrEmpty(ErrorMessage))
        {
            tooltip += (tooltip == "" ? "" : "\n") + ErrorMessage;

            // hiba eset�n kinull�zzuk
            shortDateStr = System.Text.RegularExpressions.Regex.Replace(shortDateStr, @"[0-9]", "0");

            //hourStr = 0.ToString("D2");
            //minuteStr = 0.ToString("D2");
        }
        else
        {
            //hourStr = dtHatarido.Hour.ToString("D2");
            //minuteStr = dtHatarido.Minute.ToString("D2");
        }

        Logger.Debug(String.Format("Iktat�s d�tum: {0}; Hat�rid� d�tum: {1}", dtIktatasDatuma, dtHatarido));
        Logger.Debug("Hat�rid� meghat�roz�s END");

        //shortDateStr = GetFormattedShortDateStringForUI(SomeMinutesAfterNow);

        if (ugyintKezdeteDateTime == DateTime.MinValue)
        {
            return strIntezesiIdo + ";" + strIntezesiIdoKotott + ";" + shortDateStr + ";" + tooltip + ";" + strUgyiratHataridoKitolas
                + ";" + dtHatarido.ToString("HH") + ";" + dtHatarido.ToString("ss");
        }
        else
        {
            return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}",
                strIntezesiIdo, strIntezesiIdoKotott, shortDateStr, tooltip, strUgyiratHataridoKitolas,
                dtHatarido.ToString("HH"), dtHatarido.ToString("ss"),
                ugyintKezdeteDateTimeShortString, ugyintKezdeteDateTime.ToString("HH"), ugyintKezdeteDateTime.ToString("ss"));
        }
    }


    [System.Web.Services.WebMethod(CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetIntezesiIdoByUgykorUgytipus(string UgykorId, string Ugytipus, string UserId)
    {
        Logger.Debug("GetIntezesiIdoByUgykorUgytipus start");
        if (String.IsNullOrEmpty(UgykorId) || String.IsNullOrEmpty(UserId))
        {
            return "";
        }

        // Int�z�si hat�rid� �s hat�rid� k�telez�s�g lek�r�se

        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = UserId;
        execParam.LoginUser_Id = UserId;

        Result result = service.GetIratMetaDefinicioByUgykorUgytipus(execParam, UgykorId, Ugytipus);
        if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
        {
            // hiba:
            return "";
        }

        EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
        string UgyiratIntezesiIdo = erec_IratMetaDefinicio.UgyiratIntezesiIdo.ToString();
        string UgyiratIntezesiIdoKotott = erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott.ToString();
        string Idoegyseg = erec_IratMetaDefinicio.Idoegyseg.ToString();
        string UgyiratHataridoKitolas = erec_IratMetaDefinicio.UgyiratHataridoKitolas.ToString();

        int nUgyiratIntezesiIdo;
        bool isIntegerUgyiratIntezesiIdo = Int32.TryParse(UgyiratIntezesiIdo, out nUgyiratIntezesiIdo);

        if (!isIntegerUgyiratIntezesiIdo || nUgyiratIntezesiIdo <= 0)
        {
            // nincs megadva
            //return "";
            return ";;;" + (UgyiratHataridoKitolas == "0" ? "0" : "1");
        }
        else
        {
            //return UgyiratIntezesiIdo + ";" + Idoegyseg + ";" + (UgyiratIntezesiIdoKotott == "1" ? "1" : "0");
            return UgyiratIntezesiIdo + ";" + Idoegyseg + ";" + (UgyiratIntezesiIdoKotott == "1" ? "1" : "0") + ";" + (UgyiratHataridoKitolas == "0" ? "0" : "1");
        }
    }


    [System.Web.Services.WebMethod(CacheDuration = 0)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetIntezesiIdoWithToolTipManual(string strIntezesiIdo, string strIdoegyseg, string UserId, string IktatasDatuma)
    {
        DateTime dtIktatasDatuma;
        if (!DateTime.TryParse(IktatasDatuma, out dtIktatasDatuma))
        {
            dtIktatasDatuma = DateTime.Now;
        }

        string strIntezesiIdoKotott = "0";
        string strUgyiratHataridoKitolas = "1";
        string tooltip = "";



        Logger.Debug("Hat�rid� meghat�roz�s  START");
        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = UserId;
        execParam.LoginUser_Id = UserId;

        String ErrorMessage = String.Empty;
        string shortDateStr = "";
        DateTime dtHatarido = DateTime.Now;
        DateTime ugyintKezdeteDateTime = DateTime.MinValue;
        string ugyintKezdeteDateTimeShortString = null;
        if (!String.IsNullOrEmpty(strIntezesiIdo) && strIntezesiIdo != "0")
        {
            #region SAKKORA
            if (Contentum.eUtility.Rendszerparameterek.UseSakkora(execParam))
            {
                ugyintKezdeteDateTime = Contentum.eUtility.Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, strIntezesiIdo, shortDateStr, string.Empty);
                if (ugyintKezdeteDateTime != null)
                {
                    dtHatarido = Contentum.eUtility.Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, ugyintKezdeteDateTime, strIntezesiIdo, strIdoegyseg, out ErrorMessage);
                    shortDateStr = GetFormattedShortDateStringForUI(UserId, dtHatarido);

                    ugyintKezdeteDateTimeShortString = GetFormattedShortDateStringForUI(UserId, ugyintKezdeteDateTime);
                }
            }
            else
            {
                dtHatarido = Contentum.eUtility.Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, dtIktatasDatuma, strIntezesiIdo, strIdoegyseg, out ErrorMessage);
                //DateTime dtHatarido = Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(Context.Cache, execParam, strIdoegyseg, strIntezesiIdo, dtIktatasDatuma, out ErrorMessage);
                shortDateStr = GetFormattedShortDateStringForUI(UserId, dtHatarido);
            }
            #endregion
        }
        if (!String.IsNullOrEmpty(ErrorMessage))
        {
            tooltip += (tooltip == "" ? "" : "\n") + ErrorMessage;

            // hiba eset�n kinull�zzuk
            shortDateStr = System.Text.RegularExpressions.Regex.Replace(shortDateStr, @"[0-9]", "0");

            //hourStr = 0.ToString("D2");
            //minuteStr = 0.ToString("D2");
        }
        else
        {
            //hourStr = dtHatarido.Hour.ToString("D2");
            //minuteStr = dtHatarido.Minute.ToString("D2");
        }

        Logger.Debug(String.Format("Iktat�s d�tum: {0}; Hat�rid� d�tum: {1}", dtIktatasDatuma, shortDateStr));
        Logger.Debug("Hat�rid� meghat�roz�s END");

        //shortDateStr = GetFormattedShortDateStringForUI(SomeMinutesAfterNow);

        return ugyintKezdeteDateTime == DateTime.MinValue
            ?
            strIntezesiIdo + ";" + strIntezesiIdoKotott + ";" + shortDateStr + ";" + tooltip + ";" + strUgyiratHataridoKitolas
                + ";" + dtHatarido.ToString("HH") + ";" + dtHatarido.ToString("ss")
            :
            string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}",
                strIntezesiIdo, strIntezesiIdoKotott, shortDateStr, tooltip, strUgyiratHataridoKitolas,
                dtHatarido.ToString("HH"), dtHatarido.ToString("ss"),
                ugyintKezdeteDateTimeShortString, ugyintKezdeteDateTime.ToString("HH"), ugyintKezdeteDateTime.ToString("ss"));
    }

    #endregion

    #region Irat hat�rid� meghat�roz�s (irat metadefin�ci� alapj�n) (Iktat�s k�perny�re)

    /// <summary>
    /// A �tadott param�terek alapj�n lek�rdezi az el��rt int�z�si id�t (ha van ilyen) �s annak k�t�tts�g�t, �s hogy kitolhatja-e az �gyirat hat�ridej�t (�gyirat szintt�l is f�gg)
    /// </summary>
    /// <returns>int�z�si id�, k�t�tts�g, sz�m�tott hat�rid�, esetleges tooltip sz�veg �s �gyirat hat�rid� kitolhat�s�g pontosvessz�vel (';') elv�lasztva</returns>
    [System.Web.Services.WebMethod(CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetIratIntezesiIdoWithToolTipFromUgyiratId(string Ugyirat_Id, string EljarasiSzakasz, string Irattipus, string UserId, string IktatasDatuma)
    {
        EREC_UgyUgyiratokService service_uu = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam_uu = new ExecParam();
        execParam_uu.Felhasznalo_Id = UserId;
        execParam_uu.LoginUser_Id = UserId;
        execParam_uu.Record_Id = Ugyirat_Id;

        Result result_uu = service_uu.Get(execParam_uu);
        if (!String.IsNullOrEmpty(result_uu.ErrorCode) || result_uu.Record == null)
        {
            // hiba:
            return "";
        }

        EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result_uu.Record;

        string Ugykor_Id = erec_UgyUgyiratok.IraIrattariTetel_Id;
        string Ugytipus = erec_UgyUgyiratok.UgyTipus;

        if (String.IsNullOrEmpty(Ugykor_Id) || String.IsNullOrEmpty(Ugytipus))
        {
            // hiba:
            return "";
        }

        return GetIratIntezesiIdoWithToolTip(Ugykor_Id, Ugytipus, EljarasiSzakasz, Irattipus, UserId, IktatasDatuma);
    }

    /// <summary>
    /// A �tadott param�terek alapj�n lek�rdezi az el��rt int�z�si id�t (ha van ilyen) �s annak k�t�tts�g�t
    /// </summary>
    /// <returns>int�z�si id�, k�t�tts�g, sz�m�tott hat�rid�, esetleges tooltip sz�veg �s �gyirat hat�rid� kitol�s pontosvessz�vel (';') elv�lasztva,
    /// illetve ugyanez az �gyirat szinten, az el�z�t�l pipe ('|') karakterrel elv�lasztva</returns>
    [System.Web.Services.WebMethod(CacheDuration = 60)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetIratIntezesiIdoWithToolTip(string UgykorId, string Ugytipus, string EljarasiSzakasz, string Irattipus, string UserId, string IktatasDatuma)
    {
        Logger.Debug("Ajax_WS - GetIratIntezesiIdoWithToolTip start");
        Logger.Debug("Param�terek: UgykorId: " + (UgykorId ?? "NULL") + " UserId: " + (UserId ?? "NULL") + " IktatasDatuma: " + (IktatasDatuma ?? "NULL"));
        Logger.Debug("Ugytipus: " + (Ugytipus ?? "NULL") + " EljarasiSzakasz: " + (EljarasiSzakasz ?? "NULL") + " Irattipus: " + (Irattipus ?? "NULL"));

        DateTime dtIktatasDatuma;
        if (!DateTime.TryParse(IktatasDatuma, out dtIktatasDatuma))
        {
            dtIktatasDatuma = DateTime.Now;
        }

        string result = GetIratIntezesiIdoByIrattipus(UgykorId, Ugytipus, EljarasiSzakasz, Irattipus, UserId);

        string tooltip = "";
        string shortDateStr = "";
        int nIntezesiIdo = 0; // default int�z�si id�
        //int nIdoegysegMin = Int32.Parse(KodTarak.IDOEGYSEG.DEFAULT); //-1440; default id�egys�g percben (1 munkanap)

        string strIntezesiIdo = "";
        string strIntezesiIdoKotott = "";
        string strIdoegysegMin = "";
        string hourStr = "";
        string minuteStr = "";
        string strUgyiratHataridoKitolas = "1";// default: a hat�rid� kitolhat�

        char[] separators = new char[1] { ';' };
        string[] iratIntezesiHataridoArray = result.Split(separators);

        if (iratIntezesiHataridoArray.Length == 4)
        {
            strIntezesiIdo = iratIntezesiHataridoArray[0];
            strIdoegysegMin = iratIntezesiHataridoArray[1];
            strIntezesiIdoKotott = iratIntezesiHataridoArray[2];
            strUgyiratHataridoKitolas = iratIntezesiHataridoArray[3];

            if (!String.IsNullOrEmpty(strIntezesiIdo) && strIntezesiIdo != "0")
            {
                int nIntezesiIdoParsed;

                if (Int32.TryParse(strIntezesiIdo, out nIntezesiIdoParsed))
                {
                    nIntezesiIdo = nIntezesiIdoParsed;
                }

                string strIdoegysegNev = Get_IDOEGYSEG_Nev(UserId, strIdoegysegMin);
                tooltip = String.Format(Resources.Form.UI_IntezesiIdo_ToolTip, strIntezesiIdo, strIdoegysegNev);

                if (strIntezesiIdoKotott == "1")
                {
                    tooltip += Resources.Form.UI_IntezesiIdoKotott_ToolTip;
                }
            }
            else
            {
                tooltip += Resources.Form.UI_IntezesiIdoDefault_ToolTip;
            }

            if (strUgyiratHataridoKitolas == "0")
            {
                tooltip += (tooltip == "" ? "" : "\n") + Resources.Form.UI_UgyiratHataridoKitolas_Forbidden_ToolTip;
            }
            else
            {
                tooltip += (tooltip == "" ? "" : "\n") + Resources.Form.UI_UgyiratHataridoKitolas_Allowed_ToolTip;
            }
        }
        else
        {
            tooltip += Resources.Form.UI_IntezesiIdoDefault_ToolTip;
        }

        //DateTime SomeDaysAfterToday = DateTime.Now.AddDays(nIntezesiIdo);
        //shortDateStr = SomeDaysAfterToday.ToShortDateString();
        DateTime ugyintKezdeteDateTime = DateTime.MinValue;
        string ugyintKezdeteDateTimeShortString = null;
        if (nIntezesiIdo != 0)
        {
            Logger.Debug("Hat�rid� meghat�roz�s  START");
            ExecParam execParam = new ExecParam();
            execParam.Felhasznalo_Id = UserId;
            execParam.LoginUser_Id = UserId;

            String ErrorMessage = String.Empty;
            DateTime dtHatarido = Contentum.eUtility.Sakkora.GetUgyUgyintezesHataridoGlobal(execParam, dtIktatasDatuma, strIntezesiIdo, strIdoegysegMin, out ErrorMessage);
            //DateTime dtHatarido = Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(Context.Cache, execParam, strIdoegysegMin, strIntezesiIdo, dtIktatasDatuma, out ErrorMessage);
            shortDateStr = GetFormattedShortDateStringForUI(UserId, dtHatarido);
            if (!String.IsNullOrEmpty(ErrorMessage))
            {
                tooltip += (tooltip == "" ? "" : "\n") + ErrorMessage;
                // hiba eset�n kinull�zzuk
                shortDateStr = System.Text.RegularExpressions.Regex.Replace(shortDateStr, @"[0-9]", "0");

                hourStr = 0.ToString("D2");
                minuteStr = 0.ToString("D2");
            }
            else
            {
                hourStr = dtHatarido.Hour.ToString("D2");
                minuteStr = dtHatarido.Minute.ToString("D2");
            }

            Logger.Debug(String.Format("Iktat�s d�tum: {0}; Hat�rid� d�tum: {1}", dtIktatasDatuma, dtHatarido));
            Logger.Debug("Hat�rid� meghat�roz�s END");

            #region SAKKORA
            if (Contentum.eUtility.Rendszerparameterek.UseSakkora(execParam))
            {
                ugyintKezdeteDateTime = Contentum.eUtility.Sakkora.SetUgyUgyintezesKezdeteGlobal(execParam, strIntezesiIdo, shortDateStr, string.Empty);
                if (ugyintKezdeteDateTime != null)
                {
                    ugyintKezdeteDateTimeShortString = GetFormattedShortDateStringForUI(UserId, ugyintKezdeteDateTime);
                }
            }
            #endregion
        }


        //return strIntezesiIdo + ";" + strIntezesiIdoKotott + ";" + shortDateStr + ";" + tooltip;

        //return strIntezesiIdo + ";" + strIntezesiIdoKotott + ";" + shortDateStr + ";" + hourStr + ";" + minuteStr + ";" + tooltip;



        string result_irat;
        if (ugyintKezdeteDateTime == DateTime.MinValue)
        {
            result_irat = strIntezesiIdo + ";" + strIntezesiIdoKotott + ";" + shortDateStr + ";" + hourStr + ";" + minuteStr + ";" + tooltip + ";" + strUgyiratHataridoKitolas;
        }
        else
        {
            result_irat = string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}",
               strIntezesiIdo, strIntezesiIdoKotott, shortDateStr, hourStr, minuteStr, tooltip, strUgyiratHataridoKitolas,
               ugyintKezdeteDateTimeShortString, ugyintKezdeteDateTime.ToString("HH"), ugyintKezdeteDateTime.ToString("ss"));
        }


        Logger.Debug("result_irat: " + result_irat);
        Logger.Debug("Ajax_WS - GetIratIntezesiIdoWithToolTip end");
        return result_irat;
    }


    [System.Web.Services.WebMethod(CacheDuration = 60)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetIratIntezesiIdoByIrattipus(string UgykorId, string Ugytipus, string EljarasiSzakasz, string Irattipus, string UserId)
    {
        Logger.Debug("Ajax_WS - GetIratIntezesiIdoByIrattipus start");
        Logger.Debug("Param�terek: UgykorId: " + (UgykorId ?? "NULL") + " UserId: " + (UserId ?? "NULL"));
        Logger.Debug("Ugytipus: " + (Ugytipus ?? "NULL") + " EljarasiSzakasz: " + (EljarasiSzakasz ?? "NULL") + " Irattipus: " + (Irattipus ?? "NULL"));
        if (String.IsNullOrEmpty(UgykorId) || String.IsNullOrEmpty(UserId))
        {
            return "";
        }

        // Int�z�si hat�rid� �s hat�rid� k�telez�s�g lek�r�se

        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = UserId;
        execParam.LoginUser_Id = UserId;

        Result result = service.GetIratMetaDefinicioByIrattipus(execParam, UgykorId, Ugytipus, EljarasiSzakasz, Irattipus);
        if (!String.IsNullOrEmpty(result.ErrorCode) || result.Record == null)
        {
            // hiba:
            return "";
        }

        EREC_IratMetaDefinicio erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
        string IratIntezesiIdo = erec_IratMetaDefinicio.UgyiratIntezesiIdo.ToString();
        string IratIntezesiIdoKotott = erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott.ToString();
        string Idoegyseg = erec_IratMetaDefinicio.Idoegyseg.ToString();
        string UgyiratHataridoKitolas = erec_IratMetaDefinicio.UgyiratHataridoKitolas.ToString();

        Logger.Debug(String.Format("GetIratIntezesiIdoByIrattipus IratIntezesiIdo: {0} Kotott: {1}", IratIntezesiIdo, IratIntezesiIdoKotott));

        int nIratIntezesiIdo;
        bool isIntegerIratIntezesiIdo = Int32.TryParse(IratIntezesiIdo, out nIratIntezesiIdo);

        if (!isIntegerIratIntezesiIdo || nIratIntezesiIdo <= 0)
        {
            //// nincs megadva
            //return "";
            ////string UgyiratIntezesiIdo = GetIntezesiIdoByUgykorUgytipus(UgykorId, Ugytipus, UserId);
            ////Logger.Debug("GetIratIntezesiIdoByIrattipus <- GetIntezesiIdoByUgykorUgytipus: " + UgyiratIntezesiIdo);
            ////return UgyiratIntezesiIdo;
            return ";;;" + (UgyiratHataridoKitolas == "0" ? "0" : "1");
        }
        else
        {
            return IratIntezesiIdo + ";" + Idoegyseg + ";" + (IratIntezesiIdoKotott == "1" ? "1" : "0") + ";" + (UgyiratHataridoKitolas == "0" ? "0" : "1");
        }
    }

    #endregion

    #region �rkeztet�k�nyvek lek�r�s �v alapj�n  (K�ldem�ny keres�si k�perny�re)

    [System.Web.Services.WebMethod(EnableSession = true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetErkeztetoKonyvekList(string evTol, string evIg, string userId, string userCsoportId, Constants.IktatoKonyvekDropDownListMode mode, bool valuesFilledWithId)
    {
        return GetErkeztetoKonyvekList(evTol, evIg, true, false, userId, userCsoportId, mode, valuesFilledWithId);
    }

    [System.Web.Services.WebMethod(EnableSession = true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public string GetAllErkeztetoKonyvekList(string evTol, string evIg, string userId, string userCsoportId, Constants.IktatoKonyvekDropDownListMode mode, bool valuesFilledWithId)
    {
        return GetErkeztetoKonyvekList(evTol, evIg, false, false, userId, userCsoportId, mode, valuesFilledWithId);
    }

    private string GetErkeztetoKonyvekList(string evTol, string evIg, bool filter_IdeIktathat, bool filterNotClosed, string userId, string userCsoportId, Constants.IktatoKonyvekDropDownListMode mode, bool valuesFilledWithId)
    {
        Logger.Info("Ajax_WS - GetErkeztetoKonyvekList");

        if (String.IsNullOrEmpty(userId))
        {
            Logger.Error("String.IsNullOrEmpty(userId)");
            throw new Exception("1");
        }



        ExecParam execParam = new ExecParam();
        execParam.Felhasznalo_Id = userId;
        execParam.LoginUser_Id = userId;
        execParam.FelhasznaloSzervezet_Id = userCsoportId;

        Result result = new Result();

        string iktatoerkezteto;

        List<IktatoKonyvek.IktatokonyvItem> IktatokonyvekList = null;


        // iktat�k�nyv:
        if (mode == Constants.IktatoKonyvekDropDownListMode.Iktatokonyvek || mode == Constants.IktatoKonyvekDropDownListMode.IktatokonyvekWithRegiAdatok)
        {
            iktatoerkezteto = Constants.IktatoErkezteto.Iktato;
        }
        // �rkeztet�k�nyv:
        else
        {
            iktatoerkezteto = Constants.IktatoErkezteto.Erkezteto;
        }

        bool filterByEv = true;

        if (!String.IsNullOrEmpty(evTol) || !String.IsNullOrEmpty(evIg))
        {

            if (mode == Constants.IktatoKonyvekDropDownListMode.IktatokonyvekWithRegiAdatok)
            {
                int iEvTol;
                if (Int32.TryParse(evTol, out iEvTol))
                {
                    int MigralasEve = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.MIGRALAS_EVE);
                    if (MigralasEve != 0 && MigralasEve > iEvTol)
                    {
                        filterByEv = false;
                    }
                }
            }
        }

        if (filterByEv)
        {
            if (filter_IdeIktathat)
            {
                result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, evTol, evIg, out IktatokonyvekList);
            }
            else
            {
                // Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre:
                result = IktatoKonyvek.IktatokonyvCache.GetAllIktatokonyvekList(Context.Cache, execParam
                , iktatoerkezteto, evTol, evIg, false, out IktatokonyvekList);
            }
        }
        else
        {
            if (filter_IdeIktathat)
            {
                result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, false, filterNotClosed, out IktatokonyvekList);
            }
            else
            {
                // Nincs sz�r�s a szervezet �ltal l�that� iktat�k�nyvekre:
                result = IktatoKonyvek.IktatokonyvCache.GetAllIktatokonyvekList(Context.Cache, execParam
                , iktatoerkezteto, evTol, evIg, false, out IktatokonyvekList);
            }
        }

        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            Dictionary<string, string> erkKonyvekDict = new Dictionary<string, string>();

            foreach (IktatoKonyvek.IktatokonyvItem item in IktatokonyvekList)
            {
                try
                {

                    var value = valuesFilledWithId ? item.Id : item.Value;
                    // ha m�g nincs ilyen megk�l�nb�ztet� jelz�s� elem a list�ban, hozz�adjuk:
                    if (erkKonyvekDict.ContainsKey(value) == false)
                    {
                        erkKonyvekDict.Add(value, item.Text);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error("Dictionary hiba: " + e.ToString());
                    throw new Exception("2");
                }
            }

            // return string �ssze�ll�t�sa: (form�tum: [megjelen�tend� n�v]&[megkulJelzes];[megjelen�tend� n�v]&[megkulJelzes];... )
            StringBuilder sb = new StringBuilder();

            JavaScriptSerializer jsSerialiter = new JavaScriptSerializer();
            jsSerialiter.Serialize(erkKonyvekDict, sb);

            //foreach (KeyValuePair<string, string> kvp in erkKonyvekDict)
            //{
            //    string megkulJelzes = kvp.Key;
            //    string erkeztetoKonyvNev = kvp.Value;

            //    if (sb.Length > 0)
            //    {
            //        sb.Append(";" + erkeztetoKonyvNev + "&" + megkulJelzes);
            //    }
            //    else
            //    {
            //        sb.Append(erkeztetoKonyvNev + "&" + megkulJelzes);
            //    }
            //}

            return sb.ToString();
        }

        Logger.Error("IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList hiba", execParam, result);
        throw new Exception(result.ErrorCode + ":" + result.ErrorMessage);
    }

    #endregion

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetTargyszavakList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("Ajax_eRecord\\GetTargyszavakList elindul.");
        Logger.Debug(String.Format("Param�terek: prefixText: {0}, count: {1}, contextKey: {2}", prefixText, count, contextKey));
        try
        {
            EREC_TargySzavakService service = eRecordService.ServiceFactory.GetEREC_TargySzavakService();
            Logger.Debug("Ajax_eRecord\\GetTargyszavakList v�ge.");
            Logger.DebugEnd();
            return service.GetTargyszavakList(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetTargyszavakList hiba: " + e.Message);
            return null;
        }

    }


    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetContentTypeList(string prefixText, int count, string contextKey)
    {
        Logger.DebugStart();
        Logger.Debug("Ajax_eRecord\\GetContentTypeList elindul.");
        Logger.Debug(String.Format("Param�terek: prefixText: {0}, count: {1}, contextKey: {2}", prefixText, count, contextKey));
        try
        {
            EREC_Obj_MetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_Obj_MetaDefinicioService();
            Logger.Debug("Ajax_eRecord\\GetContentTypeList v�ge.");
            Logger.DebugEnd();
            return service.GetContentTypeList(prefixText, count, contextKey);
        }
        catch (Exception e)
        { // TODO
            Logger.Error("GetContentTypeList hiba: " + e.Message);
            return null;
        }

    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetElosztoivekList(string prefixText, int count, string contextKey)
    {
        try
        {
            Contentum.eAdmin.Service.EREC_IraElosztoivekService elosztoivekService = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();

            return elosztoivekService.GetElosztoivekListForAutoComplete(prefixText, count, contextKey);
        }
        catch (Exception e)
        {
            Logger.Error("GetElosztoivekList", e);
            return null;
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string[] GetPartnerekUnionElosztoivekList(string prefixText, int count, string contextKey)
    {
        Contentum.eAdmin.Service.KRT_PartnerekService partnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
        Contentum.eAdmin.Service.EREC_IraElosztoivekService elosztoivekService = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();

        string[] partnerList = partnerekService.GetPartnerekList(prefixText, count, contextKey);
        string[] elosztoivList = elosztoivekService.GetElosztoivekListForAutoComplete(prefixText, count, contextKey);

        if (partnerList == null)
            partnerList = new string[0];
        if (elosztoivList == null)
            elosztoivList = new string[0];

        string[] ret = new string[partnerList.Length + elosztoivList.Length];

        for (int i = 0; i < partnerList.Length; i++)
            ret[i] = partnerList[i];

        for (int j = 0; j < elosztoivList.Length; j++)
            ret[j + partnerList.Length] = elosztoivList[j];

        return ret;
    }

    private class DokumentumVerzio
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _formatum;

        public string Formatum
        {
            get { return _formatum; }
            set { _formatum = value; }
        }

        private string _source;

        public string source
        {
            get { return _source; }
            set { _source = value; }
        }

        private string _version;

        public string version
        {
            get { return _version; }
            set { _version = value; }
        }

        private string _url;

        public string url
        {
            get { return _url; }
            set { _url = value; }
        }

        private string _size;

        public string size
        {
            get { return _size; }
            set { _size = value; }
        }

        private string _created;

        public string created
        {
            get { return _created; }
            set { _created = value; }
        }

        private string _comments;

        public string comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
    }
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public Result GetDokumentumVerziok(string FelhasznaloId, string DokumentumId)
    {
        ExecParam xpm = new ExecParam();
        xpm.Felhasznalo_Id = FelhasznaloId;
        xpm.Record_Id = DokumentumId;

        Contentum.eDocument.Service.DocumentService service = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
        Result result = service.GetDocumentVersionsFromMOSS(xpm);

        if (!result.IsError)
        {
            List<DokumentumVerzio> data = new List<DokumentumVerzio>();
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                DokumentumVerzio item = new DokumentumVerzio();
                item.Id = row["Id"].ToString();
                item.name = row["name"].ToString();
                item.Formatum = row["Formatum"].ToString();
                item.source = new System.Web.UI.WebControls.Image().ResolveUrl("~/images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(item.Formatum));
                item.version = row["version"].ToString();
                item.url = row["url"].ToString();
                item.size = row["size"].ToString();
                item.created = row["created"].ToString();
                item.comments = row["comments"].ToString();
                data.Add(item);
            }
            result.Record = data;
        }
        else
        {
            result.ErrorMessage = ResultError.GetErrorMessageFromResultObject(result);
        }

        result.Ds = null;
        return result;
    }

    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetAllAgazatiJelek(string knownCategoryValues, string category, string contextKey)
    {
        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }

        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        ExecParam execParam = UI.SetExecParamDefault(Session);


        EREC_AgazatiJelekService service_agazatiJelek = eRecordService.ServiceFactory.GetEREC_AgazatiJelekService();

        EREC_AgazatiJelekSearch search_agJel = new EREC_AgazatiJelekSearch();
        search_agJel.OrderBy = "Kod";

        Result result_agjelGetAll = service_agazatiJelek.GetAll(execParam, search_agJel);
        if (!String.IsNullOrEmpty(result_agjelGetAll.ErrorCode))
        {
            // hiba:
            return null;
        }
        else
        {
            foreach (DataRow row in result_agjelGetAll.Ds.Tables[0].Rows)
            {
                string Id = row["Id"].ToString();
                string Nev = row["Nev"].ToString();
                string Kod = row["Kod"].ToString();
                try
                {
                    values.Add(new CascadingDropDownNameValue("[" + Kod + "] " + Nev, Id));
                }
                catch
                {
                    values.Clear();
                    values.Add(new CascadingDropDownNameValue(Resources.Error.UIDropDownListFillingError, ""));
                }
            }

        }

        return values.ToArray();
    }

    [System.Web.Services.WebMethod(true, CacheDuration = 300)]
    [System.Web.Script.Services.ScriptMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetAllUgykorokByAgazatiJel(string knownCategoryValues, string category, string contextKey)
    {
        Logger.DebugStart("GetAllUgykorokByAgazatiJel");

        if (String.IsNullOrEmpty(contextKey))
        {
            return null;
        }


        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();

        StringDictionary kv = CascadingDropDown.ParseKnownCategoryValuesString(
          knownCategoryValues);


        // A kiv�lasztott �gazati jel:
        string agazatiJel_Id = "";

        if (kv.ContainsKey("AgazatiJel") == false)
        {
            return null;
        }
        else
        {
            agazatiJel_Id = kv["AgazatiJel"];

        }

        if (String.IsNullOrEmpty(agazatiJel_Id))
        {
            return null;
        }


        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();

        ExecParam execParam = UI.SetExecParamDefault(Session);

        EREC_IraIrattariTetelekSearch search = new EREC_IraIrattariTetelekSearch();

        search.AgazatiJel_Id.Value = agazatiJel_Id;
        search.AgazatiJel_Id.Operator = Query.Operators.equals;

        search.OrderBy = "IrattariTetelszam";

        Logger.DebugStart("service.GetAll");

        Result result = service.GetAll(execParam, search);

        Logger.DebugEnd("service.GetAll");

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            return null;
        }

        int resultCount = result.Ds.Tables[0].Rows.Count;

        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string irattariTetel_Id = row["Id"].ToString();
            string irattariTetel_Ittsz = row["IrattariTetelszam"].ToString();

            string irattariTetel_Nev = row["Nev"].ToString();

            CascadingDropDownNameValue item = new CascadingDropDownNameValue(irattariTetel_Ittsz + " (" + irattariTetel_Nev + ")", irattariTetel_Id);
            values.Add(item);
            if (resultCount == 1)
            {
                item.isDefaultValue = true;
            }
        }

        Logger.DebugEnd("GetAllUgykorokByAgazatiJel");

        return values.ToArray();
    }


}

