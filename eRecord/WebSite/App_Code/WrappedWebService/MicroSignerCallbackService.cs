﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eUtility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml;

/// <summary>
/// MicroSigner aláíráshoz a kliens oldali callback függvények által hívott metódusok
/// https://e-szigno.hu/uzleti-megoldasok/microsigner.html
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebSite")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class MicroSignerCallbackService : System.Web.Services.WebService
{
    private const string certfilename = "_signer_certificate.cer";
    //private const string serversidecertfilename = "minositett-male-seal-win7.pfx";
    private const string downloadedfilename = "_downloaded.pdf";
    private const string halfsignedfilename = "_halfsigned.pdf";
    //private const string verifiedfilename = "_verified.pdf";
    private const string verifiedlistxmlfilename = "_list.xml";
    private const string halfsignedbase64filename = "_halfsigned_base64_encoded.txt";
    private const string signedfilename = "_signed.pdf";
    private const string logpdfsignfilename = "_log_pdf_sign.txt";
    private const string logpdfverifyfilename = "_log_pdf_verify.txt";
    private const string logpdfgetunsignedhashfilename = "_log_pdf_get_unssigned_hash.txt";
    private const string logpdfsetsignedhashfilename = "_log_pdf_set_signed_hash.txt";
    private const string logServersideSignName = "_log_server_sode_sign.txt";

    private Process StartProcess(string fileName, string arguments)
    {
        ProcessStartInfo info = new ProcessStartInfo
        {
            FileName = fileName,
            Arguments = arguments,
            CreateNoWindow = false,
            UseShellExecute = false
        };

        return Process.Start(info);
    }

    [WebMethod(EnableSession = true, CacheDuration = 300)]
    public Result SetUnsignedHashes(string certificate, string sessionId, string[] documentIds, string iratIds, string recordIds, string externalLinks)
    {
        Result resUnsignedHashes = new Result();

        string[] iratIdsArray = iratIds.Split(';');
        string[] recordIdsArray = recordIds.Split(';');
        string[] externalLinksArray = externalLinks.Split('|');
        List<string> unsignedHashes = new List<string>();

        //Előjegyzett aláírás lekérése, ellenőrzés
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session);
        AlairasokService service_Alairasok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetAlairasokService();

        for (int i = 0; i < iratIdsArray.Length; i++)
        {
            Result result_ElojegyzettAlairas = service_Alairasok.GetAll_PKI_ElojegyzettAlairas(execParam.Clone(), String.Empty, "Irat", iratIdsArray[i]);

            if (result_ElojegyzettAlairas.IsError)
            {
                return result_ElojegyzettAlairas;
            }

            if (result_ElojegyzettAlairas.Ds.Tables[0].Rows.Count == 0
                || String.IsNullOrEmpty(result_ElojegyzettAlairas.Ds.Tables[0].Rows[0]["AlairasSzabaly_Id"].ToString()))
            {
                resUnsignedHashes.ErrorMessage = "Ön nincs felvéve az előjegyzett aláírók közé, vagy Önhöz nem található aláírószabály.";
                resUnsignedHashes.ErrorCode = "[52751]";
                return resUnsignedHashes;
            }
        }

        // Unsigned hash készítése eSzignó Automatával
        string eSzignoFilePath = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_eSzigno_FilePath");

        File.WriteAllText(eSzignoFilePath + @"out\" + sessionId + certfilename, certificate);

        bool isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsSignTestMode") == "true";
        string pdf_sign_params = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_pdf_sign_params");
        string pdf_get_unsigned_hash_params = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_pdf_get_unsigned_hash_params");
        string spUser = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string spPW = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string spDomain = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

        for (int i = 0; i < externalLinksArray.Length; i++)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Credentials = new System.Net.NetworkCredential(spUser, spPW, spDomain);
                    client.DownloadFile(externalLinksArray[i], eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename);
                }

                string testParams = @"-test yes -debug yes -debug_file_path " + eSzignoFilePath + @"out\" + recordIdsArray[i] + logpdfsignfilename + " ";
                if (!isTestMode) { testParams = ""; }
                Logger.Error("Hívott pdf_sign parancs: " + eSzignoFilePath + @"bin\eszigno3.exe " + testParams + @"pdf_sign -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename + @" -out " + eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedfilename + @" -signer_cert " + eSzignoFilePath + @"out\" + sessionId + certfilename + " " + pdf_sign_params);
                Process processUnsigned = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", testParams + @"pdf_sign -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename + @" -out " + eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedfilename + @" -signer_cert " + eSzignoFilePath + @"out\" + sessionId + certfilename + " " + pdf_sign_params);
                processUnsigned.WaitForExit();

                if (processUnsigned.ExitCode == 0)
                {
                    string testParamsGethash = @"-test yes -debug yes -debug_file_path " + eSzignoFilePath + @"out\" + recordIdsArray[i] + logpdfgetunsignedhashfilename + " ";
                    if (!isTestMode) { testParamsGethash = ""; }
                    Logger.Debug("Hívott pdf_get_unsigned_hash parancs: " + eSzignoFilePath + @"bin\eszigno3.exe" + testParamsGethash + @"pdf_get_unsigned_hash -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedfilename + @" -out " + eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedbase64filename + " " + pdf_get_unsigned_hash_params);
                    Process processGethash = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", testParamsGethash + @"pdf_get_unsigned_hash -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedfilename + @" -out " + eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedbase64filename + " " + pdf_get_unsigned_hash_params);
                    processGethash.WaitForExit();

                    if (processGethash.ExitCode == 0)
                    {
                        unsignedHashes.Add(File.ReadAllText(eSzignoFilePath + @"out\" + recordIdsArray[i] + halfsignedbase64filename).Trim(new char[] { '\n', '\r' }));
                    }
                    else
                    {
                        Logger.Error("SetUnsignedHashes hiba, processGethash.ExitCode: " + processGethash.ExitCode);
                        resUnsignedHashes.ErrorMessage = "SetUnsignedHashes hiba, processGethash.ExitCode: " + processGethash.ExitCode;
                        resUnsignedHashes.ErrorCode = processGethash.ExitCode.ToString();
                        return resUnsignedHashes;
                    }
                }
                else
                {
                    Logger.Error("SetUnsignedHashes hiba, processUnsigned.ExitCode: " + processUnsigned.ExitCode);
                    resUnsignedHashes.ErrorMessage = "SetUnsignedHashes hiba, processUnsigned.ExitCode: " + processUnsigned.ExitCode;
                    resUnsignedHashes.ErrorCode = processUnsigned.ExitCode.ToString();
                    return resUnsignedHashes;
                }
            }
            catch (Exception exp)
            {
                Logger.Error("SetUnsignedHashes hiba, externalLinksArray Exception: " + exp.Message);
                resUnsignedHashes.ErrorMessage = "SetUnsignedHashes hiba, externalLinksArray Exception: " + exp.Message;
                resUnsignedHashes.ErrorCode = exp.Message;
                return resUnsignedHashes;
            }
        }

        // JSON összeállítása
        StringBuilder sbUnsigned = new StringBuilder();
        StringWriter swUnsigned = new StringWriter(sbUnsigned);
        using (JsonWriter writerUnsigned = new JsonTextWriter(swUnsigned))
        {
            writerUnsigned.Formatting = Newtonsoft.Json.Formatting.Indented;
            writerUnsigned.WriteStartObject();
            writerUnsigned.WritePropertyName("sessionId");
            writerUnsigned.WriteValue(sessionId);
            writerUnsigned.WritePropertyName("hashAlgorithm");
            //sha1 lenyomat aláírása esetén: 1.3.14.3.2.26
            //sha256 lenyomat aláírása esetén: 2.16.840.1.101.3.4.2.1
            //sha384 lenyomat aláírása esetén: 2.16.840.1.101.3.4.2.2
            //sha512 lenyomat aláírása esetén: 2.16.840.1.101.3.4.2.3
            writerUnsigned.WriteValue("2.16.840.1.101.3.4.2.1");

            writerUnsigned.WritePropertyName("unsignedHashes");
            writerUnsigned.WriteStartArray();
            for (int i = 0; i < unsignedHashes.Count; i++)
            {
                writerUnsigned.WriteStartObject();
                writerUnsigned.WritePropertyName("documentId");
                writerUnsigned.WriteValue(documentIds[i]);
                writerUnsigned.WritePropertyName("unsignedHash");
                writerUnsigned.WriteValue(unsignedHashes[i]);
                writerUnsigned.WriteEndObject();
            }
            writerUnsigned.WriteEndArray();

            writerUnsigned.WriteEndObject();
        }

        var jsonUnsigned = sbUnsigned.ToString();

        // JSON elküldése a közvetítő szerverre
        string microsecUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsServerURL");
        string microsecUserName = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsServerUserName");
        string microsecPassword = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsServerPassword");
        string microsecCertFile = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsPfxFile");
        string microsecCertFilePassword = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsPfxFilePassword");
        
        HttpWebRequest requestUnsigned = (HttpWebRequest)WebRequest.Create(microsecUrl + "setUnsignedHashes");
        requestUnsigned.Credentials = new NetworkCredential(microsecUserName, microsecPassword);
        byte[] bytesUnsigned = System.Text.Encoding.ASCII.GetBytes(jsonUnsigned);

        X509Certificate2 signcert = new X509Certificate2(microsecCertFile, microsecCertFilePassword);
        byte[] bytesUnsignedPKCS7 = Crypt.SignPKCS7(bytesUnsigned, signcert);

        requestUnsigned.ContentType = "application/pkcs7-signature";
        requestUnsigned.ContentLength = bytesUnsignedPKCS7.Length;
        requestUnsigned.Method = "POST";

        Stream requestStreamUnsigned = requestUnsigned.GetRequestStream();
        requestStreamUnsigned.Write(bytesUnsignedPKCS7, 0, bytesUnsignedPKCS7.Length);
        requestStreamUnsigned.Close();

        try
        {

            HttpWebResponse responseUnsigned = (HttpWebResponse)requestUnsigned.GetResponse();

            if (responseUnsigned.StatusCode == HttpStatusCode.OK)
            {
                if (!isTestMode)
                {
                    File.Delete(eSzignoFilePath + @"out\" + sessionId + certfilename);
                }
            }
            else
            {
                Logger.Error("SetUnsignedHashes hiba, responseUnsigned.StatusCode: " + responseUnsigned.StatusCode.ToString());
                resUnsignedHashes.ErrorMessage = "SetUnsignedHashes hiba, responseUnsigned.StatusCode: " + responseUnsigned.StatusCode.ToString();
                resUnsignedHashes.ErrorCode = responseUnsigned.StatusCode.ToString();
                return resUnsignedHashes;
            }
        }
        catch (WebException ex)
        {
            String responseString = String.Empty;

            if (ex.Response != null)
            {
                using (Stream stream = ex.Response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                }
            }

            Logger.Error(String.Format("SetUnsignedHashes hiba: requestUnsigned.Url={0}, Exception = {1}, Response={2}", requestUnsigned.RequestUri.ToString(), ex.ToString(), responseString));
            resUnsignedHashes.ErrorMessage = "SetUnsignedHashes hiba: " + ex.Message;
            resUnsignedHashes.ErrorCode = "SetUnsignedHashes hiba: " + ex.Message;
            return resUnsignedHashes;
        }

        return resUnsignedHashes;
    }

    [WebMethod(EnableSession = true, CacheDuration = 300)]
    public Result SetSignedHashes(string[] signedHashes, string iratIds, string recordIds, string csatolmanyokIds, string folyamatTetelekIds)
    {
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session);
        Microsec.MicroSignerManager microSignerManager = new Microsec.MicroSignerManager(execParam);
        return microSignerManager.SetSignedHashes(signedHashes, iratIds, recordIds, csatolmanyokIds, folyamatTetelekIds);
    }

    #region BLG 2947

    private string getMicrosecErrorTextByCode(int errorCode)
    {
        string errorMessage = string.Empty;
        if (!Microsec.MicroSignerManager.ResultErrors.TryGetValue(errorCode, out errorMessage))
        {
            return "PdfVerify hiba, processListXml.ExitCode: " + errorCode.ToString();
        }
        return errorMessage;
    }

    [WebMethod(EnableSession = true, CacheDuration = 300)]
    public Result PdfVerify(string sessionId, string[] documentIds, string recordIds, string externalLinks, string folyamatTetelekIds, string proc_Id)
    {
        Result resVerify = new Result();

        //string[] iratIdsArray = iratIds.Split(';');
        string[] recordIdsArray = recordIds.Split(';');
        string[] externalLinksArray = externalLinks.Split(';');
        string[] folyamatTetelekIdsArray = folyamatTetelekIds.Split(';');
        //List<string> verifiedPdfs = new List<string>();

        ////Előjegyzett aláírás lekérése, ellenőrzés
        //ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session);
        //AlairasokService service_Alairasok = Contentum.eRecord.BaseUtility.eRecordService.ServiceFactory.GetAlairasokService();
        //eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.ellenorzes_folyamatban);

        //for (int i = 0; i < iratIdsArray.Length; i++)
        //{
        //    Result result_ElojegyzettAlairas = service_Alairasok.GetAll_PKI_ElojegyzettAlairas(execParam.Clone(), String.Empty, "Irat", iratIdsArray[i]);

        //    if (result_ElojegyzettAlairas.IsError)
        //    {
        //        eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
        //        eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikertelen_felulvizsgalat);
        //        return result_ElojegyzettAlairas;
        //    }

        //    if (result_ElojegyzettAlairas.Ds.Tables[0].Rows.Count == 0
        //        || String.IsNullOrEmpty(result_ElojegyzettAlairas.Ds.Tables[0].Rows[0]["AlairasSzabaly_Id"].ToString()))
        //    {
        //        resVerify.ErrorMessage = "Ön nincs felvéve az előjegyzett aláírók közé, vagy Önhöz nem található aláírószabály.";
        //        resVerify.ErrorCode = "[52751]";
        //        eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
        //        eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikertelen_felulvizsgalat);
        //        return resVerify;
        //    }
        //}

        //// Unsigned hash készítése eSzignó Automatával
        //string eSzignoFilePath = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_eSzigno_FilePath");

        //File.WriteAllText(eSzignoFilePath + @"out\" + sessionId + certfilename, certificate);



        string eSzignoFilePath = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_eSzigno_FilePath");

        bool isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("MsSignTestMode") == "true";
        string pdf_sign_params = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_pdf_sign_params");
        string pdf_get_unsigned_hash_params = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Ms_pdf_get_unsigned_hash_params");
        string spUser = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string spPW = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string spDomain = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");

        for (int i = 0; i < externalLinksArray.Length; i++)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Credentials = new System.Net.NetworkCredential(spUser, spPW, spDomain);
                    client.DownloadFile(externalLinksArray[i], eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename);
                }

                string testParams = @"-test yes -debug yes -debug_file_path " + eSzignoFilePath + @"out\" + recordIdsArray[i] + logpdfverifyfilename + " ";
                if (!isTestMode) { testParams = ""; }
                //Process processVerify = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", testParams + @"pdf_verify -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename + @" -out " + eSzignoFilePath + @"out\" + recordIdsArray[i] + verifiedfilename + @" -signer_cert " + eSzignoFilePath + @"out\" + sessionId + certfilename + " " + pdf_sign_params);
                string verifyParams = testParams + @"pdf_verify -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename + @" -trusted_cert_dir " + eSzignoFilePath + @"out\" + sessionId + certfilename + " " + pdf_sign_params;
                Process processVerify = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", verifyParams);
                processVerify.WaitForExit();

                if (processVerify.ExitCode == 0)
                {
                    //verifiedPdfs.Add(File.ReadAllText(eSzignoFilePath + @"out\" + recordIdsArray[i] + verifiedfilename));

                    //pdf_list 
                    string pdfListParams = testParams + @"pdf_list -in " + eSzignoFilePath + @"out\" + recordIdsArray[i] + downloadedfilename + @" -trusted_cert_dir " + eSzignoFilePath + @"out\" + sessionId + certfilename + " " + pdf_sign_params + @" -out " + eSzignoFilePath + @"out\" + recordIdsArray[i] + verifiedlistxmlfilename + @" -listform xml";
                    Process processListXml = StartProcess(eSzignoFilePath + @"bin\eszigno3.exe", pdfListParams);
                    processListXml.WaitForExit();
                    if (processListXml.ExitCode == 0)
                    {
                        //string filename = String.Empty;
                        //string documentSite = String.Empty;
                        //string documentStore = String.Empty;
                        //string documentFolder = String.Empty;
                        //Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

                        //ExecParam execParamUpl = UI.SetExecParamDefault(HttpContext.Current.Session);

                        //if (!string.IsNullOrEmpty(recordIdsArray[i]))
                        //{
                        //    execParamUpl.Record_Id = recordIdsArray[i];
                        //    char jogszint = 'O';

                        //    Result dokGetResult = dokService.GetWithRightCheck(execParamUpl, jogszint);
                        //    if (dokGetResult.IsError)
                        //    {
                        //        throw new ResultException(dokGetResult);
                        //    }

                        //    filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                        //    documentStore = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 3);
                        //    documentSite = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 2);
                        //    documentFolder = eDocumentService.getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 4);
                        //}

                        //byte[] documentData = File.ReadAllBytes(eSzignoFilePath + @"out\" + recordIdsArray[i] + verifiedlistxmlfilename);
                        //Result spUploadResult = eDocumentService.UploadSignedFile(execParamUpl.Clone(), iratIdsArray[i], recordIdsArray[i], documentSite, documentStore, documentFolder, filename, documentData);

                        //if (spUploadResult.IsError)
                        //{
                        //    Logger.Error("SetSignedHashes hiba, spUploadResult.ErrorMessage: " + spUploadResult.ErrorMessage);
                        //    return spUploadResult;
                        //}

                        //ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
                        //Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelekIdsArray[i], KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes, String.Empty);
                        //eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikeres_felulvizsgalat);
                        //if (resTetel.IsError)
                        //{
                        //    Logger.Error("PdfVerify hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
                        //    eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
                        //    eDocumentService.SetAlairasFelulvizsgalatEredmeny(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], KodTarak.TOMEGES_ALAIRAS_FELULVIZSGALAT_EREDMENY.sikertelen_felulvizsgalat);
                        //    return resTetel;
                        //}

                        XmlDocument doc = new XmlDocument();
                        doc.Load(eSzignoFilePath + @"out\" + recordIdsArray[i] + verifiedlistxmlfilename);
                        //XmlDocument docToWrite = new XmlDocument();

                        //XmlNodeList nl = doc.SelectNodes("pdf");
                        //XmlNode pdf = nl[0];
                        //XmlNodeList nl2 = pdf.SelectNodes("signatures");
                        //XmlNode root = nl2[0];
                        //XmlElement signatures = docToWrite.CreateElement("signatures");
                        //if (root != null)
                        //{
                        //    foreach (XmlNode xnode in root.ChildNodes)
                        //    {
                        //        XmlElement signature = docToWrite.CreateElement("signature");

                        //        signature.SetAttribute("name", xnode.Attributes["name"].Value);
                        //        signature.SetAttribute("issuer", xnode.Attributes["issuer"].Value);
                        //        signature.SetAttribute("issuerValidTo", xnode.Attributes["issuerValidTo"].Value);
                        //        signature.SetAttribute("signerTime", xnode.Attributes["signerTime"].Value);
                        //        signatures.AppendChild(signature);
                        //    }
                        //}
                        //docToWrite.AppendChild(signatures);

                        //string jsonTextEredmeny = JsonConvert.SerializeXmlNode(doc);
                        Result setResult = eDocumentService.SetAlairasFelulvizsgalatEredmenyFromXML(UI.SetExecParamDefault(HttpContext.Current.Session), recordIdsArray[i], doc);
                        if (setResult.IsError)
                        {
                            Logger.Error("PdfVerify Eredmény mentési hiba, setResult.ErrorMessage: " + setResult.ErrorMessage);
                            return setResult;
                        }

                        ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
                        Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelekIdsArray[i], KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes, String.Empty);
                        if (resTetel.IsError)
                        {
                            Logger.Error("PdfVerify hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
                            return resTetel;
                        }

                    }
                    else
                    {
                        return returnVerifyError(getMicrosecErrorTextByCode(processListXml.ExitCode), processListXml.ExitCode.ToString(), true, folyamatTetelekIdsArray[i], proc_Id, recordIdsArray[i], resVerify);
                    }

                }
                else
                {
                    return returnVerifyError(getMicrosecErrorTextByCode(processVerify.ExitCode), processVerify.ExitCode.ToString(), true, folyamatTetelekIdsArray[i], proc_Id, recordIdsArray[i], resVerify);
                }
            }
            catch (Exception exp)
            {
                return returnVerifyError("PdfVerify hiba, externalLinksArray Exception: " + exp.Message, string.Empty, false, folyamatTetelekIdsArray[i], proc_Id, recordIdsArray[i], resVerify);
            }
        }

        resVerify = eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikeres_ellenorzes);
        return resVerify;
    }

    private Result returnVerifyError(String errorMessage, String errorCode, Boolean seteDocument, string folyamatTetelId, string proc_Id, string recordId, Result resVerify)
    {
        ExecParam execParamTomeges = UI.SetExecParamDefault(HttpContext.Current.Session);
        Result resTetel = eDocumentService.SetFolyamatTetelStatus(execParamTomeges, folyamatTetelId, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes, errorCode);
        if (seteDocument)
        {
            eDocumentService.SetAlairasFelulvizsgalatEredmenyFromStatusCode(UI.SetExecParamDefault(HttpContext.Current.Session), recordId, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
        }
        if (resTetel.IsError)
        {
            Logger.Error("PdfVerify hiba, resTetel.ErrorMessage: " + resTetel.ErrorMessage);
            eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
            return resTetel;
        }

        Logger.Error(errorMessage + ": " + errorCode);
        resVerify.ErrorMessage = errorMessage;
        resVerify.ErrorCode = errorCode;
        eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_ELLENORZES_FOLYAMAT_STATUS.sikertelen_ellenorzes);
        return resVerify;
    }


    #endregion

    #region BLG 6473 - szerver oldali aláírás
    [WebMethod(EnableSession = true, CacheDuration = 300)]
    public Result ServersidePDFSign(string sessionId, string iratIds, string[] documentIds, string recordIds, string externalLinks, string folyamatTetelekIds, string proc_Id, string megjegyzes, string csatolmanyokIds)
    {
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session);
        Microsec.MicroSignerManager microSignerManager = new Microsec.MicroSignerManager(execParam);
        return microSignerManager.ServersidePDFSign(sessionId, iratIds, documentIds, recordIds, externalLinks, folyamatTetelekIds, proc_Id, megjegyzes, csatolmanyokIds);
    }
    #endregion

}