﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Utility;
using System.Web.UI;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for SilverLight
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebSite")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SilverLight : System.Web.Services.WebService {

    public class PartnerCim
    {
        private string _PartnerForras;

        public string PartnerForras
        {
            get { return _PartnerForras; }
            set { _PartnerForras = value; }
        }

        private string _PartnerNev;

        public string PartnerNev
        {
            get { return _PartnerNev; }
            set { _PartnerNev = value; }
        }
        private Guid? _PartnerId;

        public Guid? PartnerId
        {
            get { return _PartnerId; }
            set { _PartnerId = value; }
        }
        private string _CimNev;

        public string CimNev
        {
            get { return _CimNev; }
            set { _CimNev = value; }
        }
        private Guid? _CimId;

        public Guid? CimId
        {
            get { return _CimId; }
            set { _CimId = value; }
        }

        public PartnerCim()
        {
        }

        public PartnerCim(string PartnerForras, string PartnerNev, Guid? PartnerId, string CimNev, Guid? CimId)
        {
            this.PartnerForras = PartnerForras;
            this.PartnerNev = PartnerNev;
            this.PartnerId = PartnerId;
            this.CimNev = CimNev;
            this.CimId = CimId;
        }
    }

    public class KodTar
    {
        private string _Kod;

        public string Kod
        {
            get { return _Kod; }
            set { _Kod = value; }
        }
        private string _Nev;

        public string Nev
        {
            get { return _Nev; }
            set { _Nev = value; }
        }

        public KodTar()
        {
        }

        public KodTar(string Kod, string Nev)
        {
            this.Kod = Kod;
            this.Nev = Nev;
        }
    }

    public SilverLight () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [System.Web.Services.WebMethod(EnableSession=true)]
    public List<IktatoKonyvek.IktatokonyvItem> GetErkeztetoKonyvekList(ExecParam execParam, string evTol, string evIg, bool filter_IdeIktathat, bool filterNotClosed, Constants.IktatoKonyvekDropDownListMode mode)
    {
        Logger.Info("Ajax_WS - GetErkeztetoKonyvekList");


        Session["FelhasznaloId"] = execParam.Felhasznalo_Id;
        Session["FelhasznaloSzervezetId"] = execParam.FelhasznaloSzervezet_Id;
        Session["LoginUserId"] = execParam.LoginUser_Id;
        Session["FelhasznaloCsoportTagId"] = execParam.CsoportTag_Id;
        Session["HelyettesitesId"] = execParam.Helyettesites_Id;

        Result result = new Result();

        string iktatoerkezteto;

        List<IktatoKonyvek.IktatokonyvItem> IktatokonyvekList = null;


        // iktatókönyv:
        if (mode == Constants.IktatoKonyvekDropDownListMode.Iktatokonyvek || mode == Constants.IktatoKonyvekDropDownListMode.IktatokonyvekWithRegiAdatok)
        {
            iktatoerkezteto = Constants.IktatoErkezteto.Iktato;
        }
        // érkeztetőkönyv:
        else
        {
            iktatoerkezteto = Constants.IktatoErkezteto.Erkezteto;
        }

        bool filterByEv = true;

        if (!String.IsNullOrEmpty(evTol) || !String.IsNullOrEmpty(evIg))
        {

            if (mode == Constants.IktatoKonyvekDropDownListMode.IktatokonyvekWithRegiAdatok)
            {
                int iEvTol;
                if (Int32.TryParse(evTol, out iEvTol))
                {
                    int MigralasEve = Rendszerparameterek.GetInt(execParam, Rendszerparameterek.MIGRALAS_EVE);
                    if (MigralasEve != 0 && MigralasEve > iEvTol)
                    {
                        filterByEv = false;
                    }
                }
            }
        }

        if (filterByEv)
        {
            if (filter_IdeIktathat)
            {
                result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, evTol, evIg, out IktatokonyvekList);
            }
            else
            {
                // Nincs szűrés a szervezet által látható iktatókönyvekre:
                result = IktatoKonyvek.IktatokonyvCache.GetAllIktatokonyvekList(Context.Cache, execParam
                , iktatoerkezteto, evTol, evIg, false, out IktatokonyvekList);
            }
        }
        else
        {
            if (filter_IdeIktathat)
            {
                result = IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(Session, iktatoerkezteto, false, filterNotClosed, out IktatokonyvekList);
            }
            else
            {
                // Nincs szűrés a szervezet által látható iktatókönyvekre:
                result = IktatoKonyvek.IktatokonyvCache.GetAllIktatokonyvekList(Context.Cache, execParam
                , iktatoerkezteto, evTol, evIg, false, out IktatokonyvekList);
            }
        }

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            throw new Contentum.eUtility.ResultException(result);
        }

        return IktatokonyvekList;

    }

    [System.Web.Services.WebMethod]
    public List<PartnerCim> GetPartnerekList(ExecParam execParam, string prefixText, int count)
    {
        try
        {
            string contextKey = execParam.Felhasznalo_Id;

            Contentum.eAdmin.Service.KRT_PartnerekService krt_PartnerekService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

            string[] res =  krt_PartnerekService.GetPartnerekList(prefixText, count, contextKey);

            //"{\"First\":\"C:   Laborcz Ferenc Szobrászműhely Alapítvány | 1174 Budapest Bél Mátyás u. 41. \",\"Second\":\"48e6bf95-7caf-4114-aa04-578e0ef62e7d;a930a62f-b7ea-415c-8df1-c31834ca978a\"}"

            List<PartnerCim> PartnerCimekList = new List<PartnerCim>();

            foreach (string item in res)
            {
                PartnerCim pc = new PartnerCim();
                Pair pair = GetPairFromJs(item);
                string text = pair.First.ToString(); ;
                string[] textParts = text.Split('|');
                string partnerNevAndForras = textParts[0];
                string partnerNev = partnerNevAndForras;
                string partnerForras = String.Empty;

                if (partnerNevAndForras.IndexOf(":") == 1)
                {
                    partnerNev = partnerNevAndForras.Substring(partnerNevAndForras.IndexOf(":") + 1).Trim();
                    partnerForras = partnerNevAndForras.Substring(0, 1);
                }
                pc.PartnerNev = partnerNev;
                pc.PartnerForras = partnerForras;

                if (textParts.Length > 1)
                {
                    pc.CimNev = textParts[1].Trim();
                }
                else
                {
                    pc.CimNev = String.Empty;
                }

                string value = pair.Second.ToString();
                string[] valueParts = value.Split(';');
                string partnerId = valueParts[0].Trim();
                if (!String.IsNullOrEmpty(partnerId))
                    pc.PartnerId = new Guid(partnerId);
                if (valueParts.Length > 1)
                {
                    string cimId = valueParts[1].Trim();
                    if (!String.IsNullOrEmpty(cimId))
                        pc.CimId = new Guid(cimId);
                }

                PartnerCimekList.Add(pc);
            }

            return PartnerCimekList;
        }
        catch (Exception)
        { // TODO 
            return null;
        }

    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    public List<KodTar> GetKodtarakList(ExecParam execParam, string Kodcsoport)
    {
        Dictionary<String, String> kodtarak_Dictionary =
           Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(Kodcsoport, execParam, HttpContext.Current.Cache);

        List<KodTar> kodTarakList = new List<KodTar>();

        foreach (KeyValuePair<String, String> kvp in kodtarak_Dictionary)
        {
            String kodtarKod = kvp.Key; // Key a kódtár kód
            String kodtarNev = kvp.Value; // Value a kódtárérték neve
            kodTarakList.Add(new KodTar(kodtarKod,kodtarNev));
        }

        return kodTarakList;
    }

    [System.Web.Services.WebMethod]
    public List<KRT_Csoportok> GetCsoportokList(ExecParam execParam, string prefixText, int count, Contentum.eUtility.Csoportok.CsoportFilterObject filter)
    {
        try
        {
            filter.FelhasznaloId = execParam.Felhasznalo_Id;
            string contextKey = filter.JsSerialize();

            Contentum.eAdmin.Service.KRT_CsoportokService krt_csoportokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();

            string[] res = krt_csoportokService.GetCsoportokList(prefixText, count, contextKey);

            List<KRT_Csoportok> CsoportokList = new List<KRT_Csoportok>();

            foreach (string item in res)
            {
                KRT_Csoportok cs = new KRT_Csoportok();

                Pair pair = GetPairFromJs(item);

                cs.Nev = pair.First.ToString();
                cs.Id = pair.Second.ToString();

                CsoportokList.Add(cs);
            }

            return CsoportokList;
        }
        catch (Exception)
        { // TODO 
            return null;
        }

    }

    [System.Web.Services.WebMethod]
    public List<KRT_Felhasznalok> GetFelhasznalokList(ExecParam execParam, string prefixText, int count)
    {
        try
        {
            string contextKey = execParam.Felhasznalo_Id;

            Contentum.eAdmin.Service.KRT_FelhasznalokService krt_felhasznalokService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

            string[] res = krt_felhasznalokService.GetFelhasznaloList(prefixText, count, contextKey);

            List<KRT_Felhasznalok> FelhaszbalokList = new List<KRT_Felhasznalok>();

            foreach (string item in res)
            {
                KRT_Felhasznalok f = new KRT_Felhasznalok();

                Pair pair = GetPairFromJs(item);

                f.Nev = pair.First.ToString();
                f.Id = pair.Second.ToString();

                FelhaszbalokList.Add(f);
            }

            return FelhaszbalokList;
        }
        catch (Exception)
        { // TODO 
            return null;
        }

    }

    private Pair GetPairFromJs(string jsPair)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        Pair pair = serializer.Deserialize<Pair>(jsPair);
        return pair;
    }

    
}

