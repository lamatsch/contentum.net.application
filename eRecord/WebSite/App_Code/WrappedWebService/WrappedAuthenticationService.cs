using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using Contentum.eBusinessDocuments;
using System.Collections.Generic;
/// <summary>
/// Summary description for WrappedAuthenticationService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WrappedAuthenticationService : System.Web.Services.WebService {

    public WrappedAuthenticationService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public Result Login(ExecParam ExecParam, Contentum.eBusinessDocuments.Login Record)
    {

        Contentum.eAdmin.Service.AuthenticationService authenticationService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetAuthenticationService();

        return authenticationService.Login(ExecParam, Record);       
    }

    private const string appKey_eDocumentBusinessServiceUrl = "eDocumentBusinessServiceUrl";
    private const string appKey_eMigrationWebSiteUrl = "eMigrationWebSiteUrl";
    private const string appKey_eMigrationBusinessServiceUrl = "eMigrationBusinessServiceUrl";

    [WebMethod]
    public Result GetApplicationUrls(ExecParam execParam)
    {
        Logger.DebugStart("GetApplicationUrls");

        Result result = new Result();

        string eDocumentWsUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(appKey_eDocumentBusinessServiceUrl);
        string eMigrationWebSiteUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(appKey_eMigrationWebSiteUrl);
        string eMigrationWebServiceUrl = System.Web.Configuration.WebConfigurationManager.AppSettings.Get(appKey_eMigrationBusinessServiceUrl);

        //Dictionary<string, string> urlDict = new Dictionary<string, string>();

        string responseString = appKey_eDocumentBusinessServiceUrl + "," + eDocumentWsUrl
            + ";" + appKey_eMigrationWebSiteUrl + "," + eMigrationWebSiteUrl
            + ";" + appKey_eMigrationBusinessServiceUrl + "," + eMigrationWebServiceUrl;

        result.Record = responseString;

        Logger.DebugEnd("GetApplicationUrls");

        return result;
    }

    
}

