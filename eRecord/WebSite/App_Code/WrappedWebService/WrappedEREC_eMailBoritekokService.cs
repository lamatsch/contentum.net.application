using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using Contentum.eBusinessDocuments;

/// <summary>
/// Summary description for EREC_eMailBoritekokService
/// </summary>
[WebService(Namespace = "Contentum.eRecord.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WrappedEREC_eMailBoritekokService : System.Web.Services.WebService {

    public WrappedEREC_eMailBoritekokService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod()]
    [System.Xml.Serialization.XmlInclude(typeof(EREC_eMailBoritekCsatolmanyok))]
    public Result InsertAndAttachDocument(ExecParam ExecParam, EREC_eMailBoritekok erec_eMailBoritekok, Csatolmany[] csatolmanyok, String egyebParamsXml)
    {
        Contentum.eRecord.Service.EREC_eMailBoritekokService erec_eMailBoritekokService = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();

        return erec_eMailBoritekokService.InsertAndAttachDocument(ExecParam, erec_eMailBoritekok, csatolmanyok, "Felhasznalo", egyebParamsXml);
        
    }    
}

