using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Contentum.eBusinessDocuments;

/// <summary>
/// Summary description for WrappedAuthenticationService
/// </summary>
[WebService(Namespace = "Contentum.eAdmin.WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WrappedKRT_KodTarakService : System.Web.Services.WebService
{
    public WrappedKRT_KodTarakService()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    [WebMethod()]
    public Result KodCsoportChangedNotification(ExecParam ExecParam, String kodcsoport)
    {
        // TODO:
        System.Web.UI.Page page = new System.Web.UI.Page();
        
        Contentum.eUtility.KodTar_Cache.RefreshKodCsoportokCacheByKodcsoportKod(ExecParam, kodcsoport, Context.Cache);

        return new Result();
    }

}

