﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="AtadasForm.aspx.cs" Inherits="AtadasForm" %>


<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="uc11"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <!-- külön figyeljük a nem elektronikus kezelésű tételeket is (pl. küldeményeknél), és az üzenetet a papíron való átadásról majd ennek megfelelően mutatjuk -->
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            var elektronikusCbList = $("input[type=checkbox][name$='cbUgyintezesModja']:checked");
            $("input[type=checkbox][id$='check']").click(function (e) {
                var checkedList = $("input[type=checkbox][id$='check']:checked");
                $("span[id$='labelTetelekSzamaDb']").text(checkedList.length.toString());
                if (elektronikusCbList.length > 0) {
                    var checkedNemElektronikusList = [];
                    checkedList.each(function () {
                        var thisRowNemElektronikus = $(this).closest('tr').find("input[type=checkbox][name$='cbUgyintezesModja']:not(:checked)").first();
                        if (thisRowNemElektronikus.length != 0) {
                            checkedNemElektronikusList.push(thisRowNemElektronikus);
                        }
                    });
                    $("span[id$='labelNemElektronikusTetelekSzamaDb']").text(checkedNemElektronikusList.length.toString());
                } else {
                    $("span[id$='labelNemElektronikusTetelekSzamaDb']").text(checkedList.length.toString());
                }
            });
        }
    </script>
        
    <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
              
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <div>
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
    
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                      
                    <asp:Panel ID="KuldemenyekListPanel" runat="server" Visible="false">
                    
                 <asp:Label ID="Label_Kuldemenyek" runat="server" Text="Küldemények:" Visible="false"  CssClass="GridViewTitle"></asp:Label>          
                 <br />
                 
                        <asp:GridView ID="KuldKuldemenyekGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                            AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDataBound="KuldKuldemenyekGridView_RowDataBound">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                    
                                    <ItemTemplate>
                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                                                
                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                    <ItemTemplate>                                    
                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                            Visible="false" OnClientClick="return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>       
                              
                                <asp:BoundField DataField="EREC_IraIktatoKonyvek_MegkulJelzes" HeaderText="Érk.könyv"
                                    SortExpression="EREC_IraIktatoKonyvek_MegkulJelzes">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Erkezteto_Szam" HeaderText="Érk.szám" SortExpression="Erkezteto_Szam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="EREC_IraIktatoKonyvek_Ev" HeaderText="Év" SortExpression="EREC_IraIktatoKonyvek_Ev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="HivatkozasiSzam" HeaderText="Küldõ&nbsp;ikt.száma" SortExpression="HivatkozasiSzam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NevSTR_Bekuldo" HeaderText="Küldõ/feladó neve" SortExpression="NevSTR_Bekuldo">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Csoport_Id_FelelosNev" HeaderText="Kezelõ" SortExpression="Csoport_Id_FelelosNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="125px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="BeerkezesIdeje" HeaderText="Érk.dátuma" ItemStyle-HorizontalAlign="Center"
                                    SortExpression="ElsoAtvetIdeje">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AllapotNev" HeaderText="Állapot" SortExpression="AllapotNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IktatniKellNev" HeaderText="Iktatás" SortExpression="IktatniKellNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FelhasznaloCsoport_Id_OrzoNev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_OrzoNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Csoport_Id_CimzettNev" HeaderText="Címzett" SortExpression="Csoport_Id_CimzettNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>
                    
                    
                    <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                
                 <br />
                 <asp:Label ID="Label_Ugyiratok" runat="server" Text="Ügyiratok:" Visible="false"  CssClass="GridViewTitle"></asp:Label>          
                 <br />
                 
                <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                    OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle"/>  
                    
                    <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                               <%-- <HeaderTemplate>
                                    <div class="DisableWrap">
                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                    &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                    </div>
                                </HeaderTemplate>--%>
                                <ItemTemplate>
                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                </ItemTemplate>
                            </asp:TemplateField>
                                                                               
                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                    <ItemTemplate>                                    
                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                            Visible="false" OnClientClick="return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>  
                            
                           
                            <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám" 
                                SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezelõ" SortExpression="Felelos_Nev" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="Ügyindító" SortExpression="NevSTR_Ugyindito"  >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev"  >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;dátum" SortExpression="LetrehozasIdo" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Hatarido" HeaderText="Határidõ" SortExpression="Hatarido" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Lezarasdat" HeaderText="Lezárás" SortExpression="Lezarasdat" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Elintezesdat" HeaderText="Ügyirat elintézési idõpontja" SortExpression="Elintezesdat" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            <%--TODO: ide majd egy kis ikon kell, ami fölé húzva megjelenik az õrzõ neve--%>
                        </asp:BoundField>                        
                            <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                            <ItemTemplate>
                                <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                            </ItemTemplate>
                        </asp:TemplateField>                        
                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                <HeaderTemplate>
                                    <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>                    
                    
                    <PagerSettings Visible="False" />
                </asp:GridView>
                    </asp:Panel>
                    
                    <asp:Panel ID="IratPeldanyokListPanel" runat="server" Visible="false">
                    
                 <br />
                 <asp:Label ID="Label_IratPeldanyok" runat="server" Text="Iratpéldányok:" Visible="false" CssClass="GridViewTitle"></asp:Label>          
                 <br />  
                    <asp:GridView ID="PldIratPeldanyokGridView" runat="server"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" 
                                            OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <%--<HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>--%>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                                       
                                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                                <ItemTemplate>                                    
                                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                        Visible="false" OnClientClick="return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                                       
                                        <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám" 
                                            SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="160px"/>
                                        </asp:BoundField>             
                                        <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok_Targy">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="EREC_KuldKuldemenyek_PostazasIranya" HeaderText="Irány" SortExpression="EREC_KuldKuldemenyek_PostazasIranya">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField> --%>   
                                        <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Határidõ" SortExpression="VisszaerkezesiHatarido">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="Címzett" SortExpression="NevSTR_Cimzett">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KuldesMod_Nev" HeaderText="Küldésmód" SortExpression="KuldesMod_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="EREC_KuldKuldemenyek_Bekuldok" HeaderText="Küldõ/feladó nevek" SortExpression="EREC_KuldKuldemenyek_Bekuldok">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EREC_KuldKuldemenyek_HivatkozasiSzam" HeaderText="Hivatkozási szám" SortExpression="EREC_KuldKuldemenyek_HivatkozasiSzam">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField>--%>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>                   
                    
                    </asp:Panel>
                    
                    <asp:Panel ID="DosszieListPanel" runat="server" Visible="false">
                    
                 <br />
                 <asp:Label ID="Label_Dossziek" runat="server" Text="Dossziék:" Visible="false" CssClass="GridViewTitle"></asp:Label>          
                 <br />  
                    <asp:GridView ID="DossziekGridView" runat="server"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" 
                                            OnRowDataBound="DossziekGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                                       
                                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                                <ItemTemplate>                                    
                                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                        Visible="false" OnClientClick="return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                                       
                                        <asp:BoundField DataField="Nev" HeaderText="Név" 
                                            SortExpression="Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="300px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Barcode" HeaderText="Vonalkód" 
                                            SortExpression="Barcode">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="160px"/>
                                        </asp:BoundField>  
                                        <asp:BoundField DataField="Csoport_Id_Tulaj_Nev" HeaderText="Tulajdonos" 
                                            SortExpression="Csoport_Id_Tulaj_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="400px"/>
                                        </asp:BoundField>    
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>                   
                    
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                    <asp:Label ID="labelNemElektronikusTetelekSzamaDb" Text="0" runat="server" style="display:none;" />
                </td>
            </tr>
            <tr>
            <td>
            <br />
                <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">                
                <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                <br />
                </asp:Panel>
                <asp:Panel ID="Panel_Warning_Kuldemeny" runat="server" Visible="false">                
                <asp:Label ID="Label_Warning_Kuldemeny" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                <br />
                </asp:Panel>
                <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">     
                <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                <br />
                </asp:Panel>
                <asp:Panel ID="Panel_Warning_Dosszie" runat="server" Visible="false">     
                <asp:Label ID="Label_Warning_Dosszie" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                <br />
                </asp:Panel>
                <asp:Panel ID="Panel_Warning_NemAzonositottVonalkodok" runat="server" Visible="false" HorizontalAlign="Center">                    
                    <br />
                    <asp:Label ID="labelNotIdentifiedHeader" runat="server" Text="<%$Resources:Form,UI_NemAzonositottVonalkodok%>" 
                        Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                    <span style="position:relative;top:-10px; left:38%;">
                        <asp:Label ID="Label_Warning_NemAzonositottVonalkodok" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                    </span>
                </asp:Panel>
            </td></tr>
            <tr>
                <td>
                
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelUserNev" runat="server" Text="Átadás ide:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="CsoportId_Felelos_Kovetkezo_CsoportTextBox" runat="server" />
                                </td>
                            <td class="mrUrlapCaption_short">
                                <div class="DisableWrap">
                                    &nbsp;<asp:Label ID="label2" runat="server" Text="Átadó:"></asp:Label>
                                </div>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:FelhasznaloCsoportTextBox ID="Atado_FelhasznaloCsoportTextBox" runat="server" ReadOnly="true"
                                    ViewMode="true" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    <uc11:KezelesiFeljegyzesPanel ID="KezelesiFeljegyzesPanel1" FelelosControlID="CsoportId_Felelos_Kovetkezo_CsoportTextBox" runat="server"/>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />                 
                </td>
            </tr>            
        </table>
     </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upResult">
     <ContentTemplate>
      <asp:Panel ID="ResultPanel" runat="server" Visible="false" width="90%">
            <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
            <asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
            <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
            <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
            <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">A kijelölt tételek átadása sikeresen végrehajtódott.</div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/images/hu/ovalgomb/atadasilistanyomtatas.gif"
                                onmouseover="swapByName(this.id,'atadasilistanyomtatas2.gif')" onmouseout="swapByName(this.id,'atadasilistanyomtatas.gif')"
                               CommandName="Print"/>
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                               CommandName="Close" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>      
      </asp:Panel>
      </ContentTemplate>
     </asp:UpdatePanel>
     </div>
</asp:Content>