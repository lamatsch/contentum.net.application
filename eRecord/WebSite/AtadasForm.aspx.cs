using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class AtadasForm : Contentum.eUtility.UI.PageBase
{
    
    //private const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = KodTarak.FELADAT_ALTIPUS.kcsNev;
    //private const string kcs_KEZELESI_FELJEGYZES_KULDEMENY = KodTarak.FELADAT_ALTIPUS.kcsNev;

    private string Command = "";
    private AtadasTipus Mode = AtadasTipus.NincsMegadva;

    private UI ui = new UI();

    private String UgyiratId = "";
    private String IratPeldanyId = "";
    private String KuldemenyId = ""; // id-k �sszef�zve
    private String DosszieId = "";
    private String Vonalkodok = ""; // vonalk�dok vessz�vel elv�lasztva

    private String[] KuldemenyekArray;
    private String[] UgyiratokArray;
    private String[] IratPeldanyokArray;
    private String[] DossziekArray;
    private String[] VonalkodokArray;
    //BLG 1130
    private Boolean isTuk = false;

    private string nemAzonositottVonalkodok = "";


    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_UgyiratAtadas = "UgyiratAtadas";
    private const String FunkcioKod_IratPeldanyAtadas = "IratPeldanyAtadas";
    private const String FunkcioKod_KuldemenyAtadas = "KuldemenyAtadas";
    private const String FunkcioKod_DosszieAtadas = "MappakAtadas";
    private const String FunkcioKod_VonalkodosAtadas = "VonalkodosAtadas";

    private Result result_atadas = null;

    public string maxTetelszam = "0";

    //private string js = "";

    private enum AtadasTipus
    {
        NincsMegadva,
        UgyiratAtadas,
        IratPeldanyAtadas,
        KuldemenyAtadas,
        DosszieAtadas,
        VonalkodosAtadas
    }

    //teljes �tad�s, nem csak �tad�sra kijel�l�s
    protected bool IsAtadas
    {
        get
        {
            return Command == CommandName.Atadas;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString(); 
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        //BLG 1130
        isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        if (isTuk)
        {
            FormFooter1.Command = CommandName.RendbenEsNyomtat;
        }
        else
        {
            FormFooter1.Command = CommandName.Modify;
        }

        KezelesiFeljegyzesPanel1.Command = String.Empty;
        KezelesiFeljegyzesPanel1.Command = CommandName.New;
        #region CR3153 - BOPMH-ban az iktat�s visszajelz� k�perny�re kellene irat �tadas gomb is.
        bool? isUgyirat = null;

        if (!string.IsNullOrEmpty(Request.QueryString.Get("isUgyirat")))
        {
            isUgyirat = Request.QueryString.Get("isUgyirat") == "true" ? true : false;
        }
       
        
        #endregion


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session["SelectedUgyiratIds"] != null)
                UgyiratId = Session["SelectedUgyiratIds"].ToString();
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratPeldanyId)))
        {
            if (Session["SelectedIratPeldanyIds"] != null)
                IratPeldanyId = Session["SelectedIratPeldanyIds"].ToString();
        }
        else IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.KuldemenyId)))
        {
            if (Session["SelectedKuldemenyIds"] != null)
                KuldemenyId = Session["SelectedKuldemenyIds"].ToString();
        }
        else KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.Vonalkodok)))
        {
            if (Session["SelectedBarcodeIds"] != null)
                Vonalkodok = Session["SelectedBarcodeIds"].ToString();
        }
        else Vonalkodok = Request.QueryString.Get(QueryStringVars.Vonalkodok);

        if (Session["SelectedDosszieIds"] != null)
        {
            DosszieId = Session["SelectedDosszieIds"].ToString();
        }

        if (!String.IsNullOrEmpty(Vonalkodok))
        {
            // Vonalk�dos �tad�s
            Mode = AtadasTipus.VonalkodosAtadas;

            VonalkodokArray = Vonalkodok.Split(',');
        }//CR3153 - BOPMH-ban az iktat�s visszajelz� k�perny�re kellene irat �tadas gomb is.
        else if ((!String.IsNullOrEmpty(IratPeldanyId) && !isUgyirat.HasValue) || (!String.IsNullOrEmpty(IratPeldanyId) && isUgyirat.HasValue && !isUgyirat.Value))
        {
            // IratP�ld�ny �tad�sa
            Mode = AtadasTipus.IratPeldanyAtadas;

            IratPeldanyokArray = IratPeldanyId.Split(',');
        }
        else if (!String.IsNullOrEmpty(KuldemenyId))
        {
            // K�ldem�ny �tad�sa:
            Mode = AtadasTipus.KuldemenyAtadas;

            KuldemenyekArray = KuldemenyId.Split(',');
        }//CR3153 - BOPMH-ban az iktat�s visszajelz� k�perny�re kellene irat �tadas gomb is.
        else if ((!String.IsNullOrEmpty(UgyiratId) && !isUgyirat.HasValue ) || (!String.IsNullOrEmpty(UgyiratId) && isUgyirat.HasValue && isUgyirat.Value))
        {
            // �gyirat�tad�s
            Mode = AtadasTipus.UgyiratAtadas;

            UgyiratokArray = UgyiratId.Split(',');
        }
        else if (!String.IsNullOrEmpty(DosszieId))
        {
            Mode = AtadasTipus.DosszieAtadas;

            DossziekArray = DosszieId.Split(',');
        }

        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            case CommandName.RendbenEsNyomtat:
                //TODO - T�K jogok
                break;
            default:
                if (Mode == AtadasTipus.VonalkodosAtadas)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_VonalkodosAtadas);
                }
                else if (Mode == AtadasTipus.UgyiratAtadas)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratAtadas);
                }
                else if (Mode == AtadasTipus.KuldemenyAtadas)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_KuldemenyAtadas);
                }
                else if (Mode == AtadasTipus.DosszieAtadas)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_DosszieAtadas);
                }
                else if (Mode == AtadasTipus.NincsMegadva)
                {
                    // nem siker�lt azonos�tani az �tad�s m�dj�t, mert semmilyen adat nem �rkezett a sessionben
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UI_Popup_NoSessionData);
                    return;
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratPeldanyAtadas);
                }
                break;
        }

        if (!IsPostBack)
        {
            LoadFormComponents();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        if (Mode == AtadasTipus.VonalkodosAtadas)
        {
            FormHeader1.HeaderTitle = Resources.Form.VonalkodosAtadasHeaderTitle;
        }
        else if (Mode == AtadasTipus.UgyiratAtadas)
        {
            FormHeader1.HeaderTitle = Resources.Form.AtadasraKijelolFormHeaderTitle_Ugyiratok;
            KezelesiFeljegyzesPanel1.SetObjektumType(Constants.TableNames.EREC_UgyUgyiratok);
        }
        else if (Mode == AtadasTipus.IratPeldanyAtadas)
        {
            FormHeader1.HeaderTitle = Resources.Form.AtadasraKijelolFormHeaderTitle_IratPeldany;
            KezelesiFeljegyzesPanel1.SetObjektumType(Constants.TableNames.EREC_PldIratPeldanyok);
        }
        else if (Mode == AtadasTipus.KuldemenyAtadas)
        {
            FormHeader1.HeaderTitle = IsAtadas ? Resources.Form.AtadasFormHeaderTitle_Kuldemeny : Resources.Form.AtadasraKijelolFormHeaderTitle_Kuldemeny;
            KezelesiFeljegyzesPanel1.SetObjektumType(Constants.TableNames.EREC_KuldKuldemenyek);
        }
        else if (Mode == AtadasTipus.DosszieAtadas)
        {
            FormHeader1.HeaderTitle = Resources.Form.AtadasraKijelolFormHeaderTitle_Dosszie;
            KezelesiFeljegyzesPanel1.SetObjektumType(Constants.TableNames.KRT_Mappak);
        }
        else
        {
            FormHeader1.HeaderTitle = Resources.Form.AtadasraKijelolFormHeaderTitle;
        }

        FormHeader1.DisableModeLabel = true;        

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (Mode != AtadasTipus.NincsMegadva)
        {
            //FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            //    + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            //    + "else {alert('Amennyiben az �tadott objektumok tartalmaznak pap�ron l�tez� inform�ci�kat, az �tad�st fizikailag is hajtsa v�gre!');}";

            FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } "
                + "else { var countNemElektronikus = parseInt(document.getElementById('" + labelNemElektronikusTetelekSzamaDb.ClientID + "').innerHTML);if (countNemElektronikus > 0) { alert('" + Resources.List.UI_Atadas_AtadasFizikailag + "');} }";

            //labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count +
            //    ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count +
            //    ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null).Count).ToString();

            int cntKijeloltUgyiratok = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count;
            int cntKijeloltIratpeldanyok = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count;
            int cntKijeloltKuldemenyek = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null).Count;
            int cntKijeloltDossziek = ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null).Count;

            int cntKijelolt = cntKijeloltUgyiratok + cntKijeloltIratpeldanyok + cntKijeloltKuldemenyek + cntKijeloltDossziek;

            int cntKijeloltUgyiratokElektronikus = ui.GetGridViewSelectedAndExecutableRows(UgyUgyiratokGridView, "cbUgyintezesModja", FormHeader1.ErrorPanel, null).Count;
            int cntKijeloltIratpeldanyokElektronikus = ui.GetGridViewSelectedAndExecutableRows(PldIratPeldanyokGridView, "cbUgyintezesModja", FormHeader1.ErrorPanel, null).Count;
            int cntKijeloltKuldemenyekElektronikus = ui.GetGridViewSelectedAndExecutableRows(KuldKuldemenyekGridView, "cbUgyintezesModja", FormHeader1.ErrorPanel, null).Count;

            int cntKijeloltElektronikus = cntKijeloltUgyiratokElektronikus + cntKijeloltIratpeldanyokElektronikus + cntKijeloltKuldemenyekElektronikus;

            labelTetelekSzamaDb.Text = cntKijelolt.ToString();

            labelNemElektronikusTetelekSzamaDb.Text = (cntKijelolt - cntKijeloltElektronikus).ToString();

            // f�kusz az "�tad�s ide" mez�re
            JavaScripts.SetFocus(CsoportId_Felelos_Kovetkezo_CsoportTextBox.TextBox);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Mode == AtadasTipus.NincsMegadva)
        {
            FormFooter1.ImageButton_Save.Visible = false;
        }

        //bopmh
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            KezelesiFeljegyzesPanel1.KezelesiFeljegyzesTipusValidate = true;
        }
    }

    private void LoadFormComponents()
    {
        if (Mode == AtadasTipus.VonalkodosAtadas)
        {
            // Vonalk�dok alapj�n �gyirat, p�ld�ny, k�ldem�ny t�mb�k felt�lt�se:

            VonalkodElemzes();

            if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
            {
                Label_Warning_NemAzonositottVonalkodok.Text = nemAzonositottVonalkodok;
                Panel_Warning_NemAzonositottVonalkodok.Visible = true;
            }

            if (UgyiratokArray != null && UgyiratokArray.Length > 0)
            {
                UgyiratokListPanel.Visible = true;
                Label_Ugyiratok.Visible = true;

                FillUgyiratokGridView();
            }

            if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
            {
                IratPeldanyokListPanel.Visible = true;
                Label_IratPeldanyok.Visible = true;

                FillIratPeldanyGridView();
            }

            if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
            {
                KuldemenyekListPanel.Visible = true;
                Label_Kuldemenyek.Visible = true;

                FillKuldemenyGridView();
            }

            if (DossziekArray != null && DossziekArray.Length > 0)
            {
                DosszieListPanel.Visible = true;
                Label_Dossziek.Visible = true;

                FillDossziekGridView();
            }
        }
        else if (Mode == AtadasTipus.UgyiratAtadas)
        {
            #region UgyiratAtadas
            if (String.IsNullOrEmpty(UgyiratId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                UgyiratokListPanel.Visible = true;
                KuldemenyekListPanel.Visible = false;
                IratPeldanyokListPanel.Visible = false;
                DosszieListPanel.Visible = false;

                FillUgyiratokGridView();                               
    
            }
            #endregion
        }
        else if (Mode == AtadasTipus.IratPeldanyAtadas)
        {
            #region IratPeldanyAtadas
            if (String.IsNullOrEmpty(IratPeldanyId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                UgyiratokListPanel.Visible = false;
                KuldemenyekListPanel.Visible = false;
                IratPeldanyokListPanel.Visible = true;
                DosszieListPanel.Visible = false;

                FillIratPeldanyGridView();
               
            }
            #endregion
        }
        else if (Mode == AtadasTipus.KuldemenyAtadas)
        {
            #region KuldemenyAtadas
            if (String.IsNullOrEmpty(KuldemenyId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                UgyiratokListPanel.Visible = false;
                KuldemenyekListPanel.Visible = true;
                IratPeldanyokListPanel.Visible = false;
                DosszieListPanel.Visible = false;

                FillKuldemenyGridView();

            }
            #endregion
        }
        else if (Mode == AtadasTipus.DosszieAtadas)
        {
            #region DosszieAtadas
            if (String.IsNullOrEmpty(DosszieId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                MainPanel.Visible = false;
            }
            else
            {
                UgyiratokListPanel.Visible = false;
                KuldemenyekListPanel.Visible = false;
                IratPeldanyokListPanel.Visible = false;
                DosszieListPanel.Visible = true;

                FillDossziekGridView();

            }
            #endregion DosszieAtadas
        }

        Atado_FelhasznaloCsoportTextBox.Id_HiddenField = FelhasznaloProfil.GetFelhasznaloCsoport(
            UI.SetExecParamDefault(Page,new ExecParam()));
        Atado_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        // kezel�si feljegyz�s r�sz:
        if (Mode == AtadasTipus.KuldemenyAtadas)
        {
            //KezFelj_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZES_KULDEMENY, true, FormHeader1.ErrorPanel);
        }
        else
        {
            //KezFelj_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA, true, FormHeader1.ErrorPanel);
        }
    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adhat� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atadasrakijelolhetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_atadasraNEMkijelolhetok = UgyiratokArray.Length - count_atadasrakijelolhetok;

        if (count_atadasraNEMkijelolhetok > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_AtadasraKijeloltekWarning, count_atadasraNEMkijelolhetok);
            //Label_Warning_Ugyirat.Visible = true;
            Panel_Warning_Ugyirat.Visible = true;
        }
    }


    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adhat� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atadasrakijelolhetok = UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check");

        int count_atadasraNEMkijelolhetok = IratPeldanyokArray.Length - count_atadasrakijelolhetok;

        if (count_atadasraNEMkijelolhetok > 0)
        {
            Label_Warning_IratPeldany.Text = String.Format(Resources.List.UI_IratPeldany_AtadasraKijeloltekWarning, count_atadasraNEMkijelolhetok);
            //Label_Warning_IratPeldany.Visible = true;
            Panel_Warning_IratPeldany.Visible = true;
        }
    }


    private void FillKuldemenyGridView()
    {
        DataSet ds = KuldKuldemenyekGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != KuldemenyekArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adhat� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atadasrakijelolhetok = UI.GetGridViewSelectedCheckBoxesCount(KuldKuldemenyekGridView, "check");
        
        int count_atadasraNEMkijelolhetok = KuldemenyekArray.Length - count_atadasrakijelolhetok;

        if (count_atadasraNEMkijelolhetok > 0)
        {
            Label_Warning_Kuldemeny.Text = String.Format(Resources.List.UI_KuldAtadasraKijeloltekWarning, count_atadasraNEMkijelolhetok);
            //Label_Warning_Kuldemeny.Visible = true;
            Panel_Warning_Kuldemeny.Visible = true;
        }
    }

    private void FillDossziekGridView()
    {
        DataSet ds = DossziekGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != DossziekArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

         //Van-e olyan rekord, ami nem adhat� �t? 
         //CheckBoxok vizsg�lat�val:
        int count_atadasrakijelolhetok = UI.GetGridViewSelectedCheckBoxesCount(DossziekGridView, "check");

        int count_atadasraNEMkijelolhetok = DossziekArray.Length - count_atadasrakijelolhetok;

        if (count_atadasraNEMkijelolhetok > 0)
        {
            Label_Warning_Dosszie.Text = String.Format(Resources.List.UI_DosszieAtadasraKijeloltekWarning, count_atadasraNEMkijelolhetok);
            //Label_Warning_Dosszie.Visible = true;
            Panel_Warning_Dosszie.Visible = true;
        }
    }

    /// <summary>
    /// Vonalk�dok alapj�n objektumok beazonos�t�sa
    /// </summary>
    private void VonalkodElemzes()
    {
        nemAzonositottVonalkodok = "";

        if (VonalkodokArray != null && VonalkodokArray.Length > 0)
        {
            KRT_BarkodokService service = eRecordService.ServiceFactory.GetKRT_BarkodokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_BarkodokSearch search = new KRT_BarkodokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < VonalkodokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Kod.Value = "'" + VonalkodokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Kod.Value += ",'" + VonalkodokArray[i] + "'";
            //    }
            //}
            search.Kod.Value = Search.GetSqlInnerString(VonalkodokArray);
            search.Kod.Operator = Query.Operators.inner;

            Result res = service.GetAll(execParam, search);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
            }
            else
            {
                if (res.Ds == null) { return; }

                List<String> ugyiratokList = new List<string>();
                List<String> kuldemenyekList = new List<string>();
                List<String> iratPeldanyList = new List<string>();
                List<String> dossziekList = new List<string>();

                List<String> hasznaltVonalkodok = new List<string>();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    string Kod = row["Kod"].ToString();
                    string Obj_Id = row["Obj_Id"].ToString();
                    string Obj_type = row["Obj_type"].ToString();

                    switch (Obj_type)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            ugyiratokList.Add(Obj_Id);
                            hasznaltVonalkodok.Add(Kod);
                            break;
                        case Constants.TableNames.EREC_KuldKuldemenyek:
                            kuldemenyekList.Add(Obj_Id);
                            hasznaltVonalkodok.Add(Kod);
                            break;
                        case Constants.TableNames.EREC_PldIratPeldanyok:
                            iratPeldanyList.Add(Obj_Id);
                            hasznaltVonalkodok.Add(Kod);
                            break;
                        case Constants.TableNames.KRT_Mappak:
                            dossziekList.Add(Obj_Id);
                            hasznaltVonalkodok.Add(Kod);
                            break;
                    }
                }

                // A nem azonositott vonalkodok kigy�jt�se
                foreach (string vonalkod in VonalkodokArray)
                {
                    if (!hasznaltVonalkodok.Contains(vonalkod))
                    {
                        //if (String.IsNullOrEmpty(nemAzonositottVonalkodok))
                        //{
                        //    nemAzonositottVonalkodok = "'" + vonalkod + "'";                            
                        //}
                        //else
                        //{
                        //    nemAzonositottVonalkodok += ",'" + vonalkod + "'";
                        //}

                        nemAzonositottVonalkodok += "<li class=\"notIdentifiedText\">" + vonalkod + "</li>";
                    }
                }

                if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
                {
                    nemAzonositottVonalkodok = "<ul>" + nemAzonositottVonalkodok + "</ul>";
                }
                
                // List�kb�l t�mb�k felt�lt�se:
                UgyiratokArray = ugyiratokList.ToArray();
                KuldemenyekArray = kuldemenyekList.ToArray();
                IratPeldanyokArray = iratPeldanyList.ToArray();
                DossziekArray = dossziekList.ToArray();

            }
        }
    }



    protected DataSet KuldKuldemenyekGridViewBind()
    {
        if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < KuldemenyekArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + KuldemenyekArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + KuldemenyekArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(KuldemenyekArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);
            
            ui.GridViewFill(KuldKuldemenyekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;
            
            return ds;            
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void KuldKuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        Kuldemenyek.KuldemenyekGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);

        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");
    }



    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < UgyiratokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + UgyiratokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + UgyiratokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);
            
            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        
        Ugyiratok.UgyiratokGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);


        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");
    }



    protected DataSet DossziekGridViewBind()
    {
        if (DossziekArray != null && DossziekArray.Length > 0)
        {
            KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_MappakSearch search = new KRT_MappakSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < DossziekArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + DossziekArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + DossziekArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(DossziekArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(DossziekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void DossziekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Dossziek.DossziekGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);
    }



    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < IratPeldanyokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + IratPeldanyokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + IratPeldanyokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(IratPeldanyokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);


        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");
    }


    #region Kezel�si feljegyz�sek

    //private EREC_UgyKezFeljegyzesek GetBusinessObjectFromComponents_UgyKezFeljegyzesek()
    //{
    //    EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = new EREC_UgyKezFeljegyzesek();
    //    erec_UgyKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_UgyKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_UgyKezFeljegyzesek.KezelesTipus = KezFelj_KezelesTipus_KodtarakDropDownList.SelectedValue;
    //    erec_UgyKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezFelj_KezelesTipus_KodtarakDropDownList);

    //    erec_UgyKezFeljegyzesek.Leiras = KezFelj_Leiras_TextBox.Text;
    //    erec_UgyKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(KezFelj_Leiras_TextBox);

    //    return erec_UgyKezFeljegyzesek;
    //}


    //private EREC_IraKezFeljegyzesek GetBusinessObjectFromComponents_IraKezFeljegyzesek()
    //{
    //    EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = new EREC_IraKezFeljegyzesek();
    //    erec_IraKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_IraKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_IraKezFeljegyzesek.KezelesTipus = KezFelj_KezelesTipus_KodtarakDropDownList.SelectedValue;
    //    erec_IraKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezFelj_KezelesTipus_KodtarakDropDownList);

    //    erec_IraKezFeljegyzesek.Leiras = KezFelj_Leiras_TextBox.Text;
    //    erec_IraKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(KezFelj_Leiras_TextBox);

    //    return erec_IraKezFeljegyzesek;
    //}


    //private EREC_KuldKezFeljegyzesek GetBusinessObjectFromComponents_KuldKezFeljegyzesek()
    //{
    //    EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = new EREC_KuldKezFeljegyzesek();
    //    erec_KuldKezFeljegyzesek.Updated.SetValueAll(false);
    //    erec_KuldKezFeljegyzesek.Base.Updated.SetValueAll(false);

    //    erec_KuldKezFeljegyzesek.KezelesTipus = KezFelj_KezelesTipus_KodtarakDropDownList.SelectedValue;
    //    erec_KuldKezFeljegyzesek.Updated.KezelesTipus = pageView.GetUpdatedByView(KezFelj_KezelesTipus_KodtarakDropDownList);

    //    erec_KuldKezFeljegyzesek.Leiras = KezFelj_Leiras_TextBox.Text;
    //    erec_KuldKezFeljegyzesek.Updated.Leiras = pageView.GetUpdatedByView(KezFelj_Leiras_TextBox);

    //    return erec_KuldKezFeljegyzesek;
    //}
    #endregion


    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //ha valamelyik �gon val�ban megt�rt�nik az �tad�s, az UId -t elmentj�k a SaveAndPrint sz�m�ra
        String TomegesTetelAtadasUid = String.Empty;
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndPrint)
        {
            if (Mode == AtadasTipus.NincsMegadva)
            {
                // nem siker�lt azonos�tani az �tad�s m�dj�t, mert lej�rt a session
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UI_Popup_SessionTimeOut);
                return;
            }

            // saj�t mag�nak ne lehessen �tadni
            if (CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField.Equals(Atado_FelhasznaloCsoportTextBox.Id_HiddenField, StringComparison.InvariantCultureIgnoreCase))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel
                    , Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_52214); // Az �tad�s c�mzettje �s az �tad� nem lehet azonos!
                return;
            }

            if ((Mode == AtadasTipus.UgyiratAtadas
                 && FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtadas))
                || (Mode == AtadasTipus.IratPeldanyAtadas
                    && FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtadas))
                || (Mode == AtadasTipus.KuldemenyAtadas
                    && FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtadas))
                || (Mode == AtadasTipus.VonalkodosAtadas
                    && FunctionRights.GetFunkcioJog(Page, FunkcioKod_VonalkodosAtadas))
                || (Mode == AtadasTipus.DosszieAtadas
                    && FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtadas))
                )
            {
                int selectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count +
                    ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count +
                    ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null).Count +
                    ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null).Count;

                if (selectedIds > int.Parse(maxTetelszam))
                {
                    // ha a JavaScript v�g�n "return false;" van, nem jelenik meg az �zenet...
                    string javaS = "alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "');"; // return false; } ";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                    return;
                }

                EREC_HataridosFeladatok erec_HataridosFeladat = KezelesiFeljegyzesPanel1.GetBusinessObject();

                if (Mode == AtadasTipus.VonalkodosAtadas)
                {


                    String kovFelelosId = CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField;
                    if (String.IsNullOrEmpty(kovFelelosId))
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                            , Resources.Error.UINincsMegadvaFelelos);
                        LoadFormComponents();
                        return;
                    }

                    // �tad�sra kijel�l�s

                    KRT_BarkodokService service = eRecordService.ServiceFactory.GetKRT_BarkodokService();                                        
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
                    //String[] UgyiratIds = new String[selectedItemsList_ugyirat.Count];
                    //for (int i = 0; i < UgyiratIds.Length; i++)
                    //{
                    //    UgyiratIds[i] = selectedItemsList_ugyirat[i];
                    //}
                    String[] UgyiratIds = selectedItemsList_ugyirat.ToArray();

                    List<string> selectedItemsList_pld = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);
                    //String[] IratPeldanyIds = new String[selectedItemsList_pld.Count];
                    //for (int i = 0; i < IratPeldanyIds.Length; i++)
                    //{
                    //    IratPeldanyIds[i] = selectedItemsList_pld[i];
                    //}
                    String[] IratPeldanyIds = selectedItemsList_pld.ToArray();

                    List<string> selectedItemsList_kuld = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null);
                    //String[] KuldemenyIds = new String[selectedItemsList_kuld.Count];
                    //for (int i = 0; i < KuldemenyIds.Length; i++)
                    //{
                    //    KuldemenyIds[i] = selectedItemsList_kuld[i];
                    //}
                    String[] KuldemenyIds = selectedItemsList_kuld.ToArray();

                    List<string> selectedItemsList_dosszie = ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null);
                    //String[] DosszieIds = new String[selectedItemsList_dosszie.Count];
                    //for (int i = 0; i < DosszieIds.Length; i++)
                    //{
                    //    DosszieIds[i] = selectedItemsList_dosszie[i];
                    //}
                    String[] DosszieIds = selectedItemsList_dosszie.ToArray();

                    if (selectedItemsList_ugyirat.Count + selectedItemsList_pld.Count + selectedItemsList_kuld.Count + selectedItemsList_dosszie.Count == 0)
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }

                                        
                    //EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = GetBusinessObjectFromComponents_UgyKezFeljegyzesek();
                    //EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = GetBusinessObjectFromComponents_KuldKezFeljegyzesek();
                    //EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = GetBusinessObjectFromComponents_IraKezFeljegyzesek();
                    

                    result_atadas = service.Atadas_Tomeges(execParam, UgyiratIds, KuldemenyIds, IratPeldanyIds, DosszieIds, kovFelelosId
                        , erec_HataridosFeladat);

                    TomegesTetelAtadasUid = result_atadas == null ? String.Empty : result_atadas.Uid;
                    if (String.IsNullOrEmpty(result_atadas.ErrorCode))
                    {
                        //JavaScripts.RegisterCloseWindowClientScript(Page);
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                        ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result_atadas.Uid);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_atadas);
                        UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result_atadas);
                        UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result_atadas);
                        UI.MarkFailedRecordsInGridView(KuldKuldemenyekGridView, result_atadas);
                        UI.MarkFailedRecordsInGridView(DossziekGridView, result_atadas);
                    }


                }
                else if (Mode == AtadasTipus.UgyiratAtadas)
                {
                    if (String.IsNullOrEmpty(UgyiratId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        String kovFelelosId = CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField;
                        if (String.IsNullOrEmpty(kovFelelosId))
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                                , Resources.Error.UINincsMegadvaFelelos);
                            LoadFormComponents();
                            return;
                        }

                        // �tad�sra kijel�l�s

                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
                        //String[] UgyiratIds = new String[selectedItemsList.Count];
                        //for (int i = 0; i < UgyiratIds.Length; i++)
                        //{
                        //    UgyiratIds[i] = selectedItemsList[i];
                        //}
                        String[] UgyiratIds = selectedItemsList.ToArray();

                        //EREC_UgyKezFeljegyzesek erec_UgyKezFeljegyzesek = GetBusinessObjectFromComponents_UgyKezFeljegyzesek();

                        Result result = service.AtadasraKijeloles_Tomeges(execParam, UgyiratIds, kovFelelosId, erec_HataridosFeladat);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (UgyiratIds.Length > 0)
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratIds[0]);
                            }
                            JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                        }

                    }
                }
                else if (Mode == AtadasTipus.IratPeldanyAtadas)
                {
                    if (String.IsNullOrEmpty(IratPeldanyId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        String kovFelelosId = CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField;
                        if (String.IsNullOrEmpty(kovFelelosId))
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                                , Resources.Error.UINincsMegadvaFelelos);
                            LoadFormComponents();
                            return;
                        }

                        EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);
                        //String[] IratPeldanyIds = new String[selectedItemsList.Count];
                        //for (int i = 0; i < IratPeldanyIds.Length; i++)
                        //{
                        //    IratPeldanyIds[i] = selectedItemsList[i];
                        //}
                        String[] IratPeldanyIds = selectedItemsList.ToArray();


                        //EREC_IraKezFeljegyzesek erec_IraKezFeljegyzesek = GetBusinessObjectFromComponents_IraKezFeljegyzesek();

                        Result result = service.AtadasraKijeloles_Tomeges(execParam, IratPeldanyIds, kovFelelosId, erec_HataridosFeladat);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            JavaScripts.RegisterSelectedRecordIdToParent(Page, IratPeldanyId);
                            JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result);
                        }

                    }     
                }
                else if (Mode == AtadasTipus.KuldemenyAtadas)
                {
                    if (String.IsNullOrEmpty(KuldemenyId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        String kovFelelosId = CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField;
                        if (String.IsNullOrEmpty(kovFelelosId))
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                                , Resources.Error.UINincsMegadvaFelelos);
                            LoadFormComponents();
                            return;
                        }


                        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel,null);
                        //String[] KuldemenyIds = new String[selectedItemsList.Count];
                        //for (int i = 0; i < KuldemenyIds.Length; i++)
                        //{
                        //    KuldemenyIds[i] = selectedItemsList[i];
                        //}
                        String[] KuldemenyIds = selectedItemsList.ToArray();

                        //EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = GetBusinessObjectFromComponents_KuldKezFeljegyzesek();

                        //Result result = service.AtadasraKijeloles(execParam, KuldemenyId, kovFelelosId, erec_KuldKezFeljegyzesek);
                        Result result = null; 
                        
                        if(IsAtadas)
                        {
                            result = service.Atadas_Tomeges(execParam,
                            KuldemenyIds, kovFelelosId, erec_HataridosFeladat);
                            TomegesTetelAtadasUid = result == null ? String.Empty : result.Uid;
                        }
                        else
                        {
                            result = service.AtadasraKijeloles_Tomeges(execParam,
                            KuldemenyIds, kovFelelosId, erec_HataridosFeladat);
                        }

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {

                            if (KuldemenyIds.Length > 0)
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, KuldemenyIds[0]);
                            }
                            JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            UI.MarkFailedRecordsInGridView(KuldKuldemenyekGridView, result);
                        }

                    }
                }
                else if (Mode == AtadasTipus.DosszieAtadas)
                {
                    if (String.IsNullOrEmpty(DosszieId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        String kovFelelosId = CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField;
                        if (String.IsNullOrEmpty(kovFelelosId))
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                                , Resources.Error.UINincsMegadvaFelelos);
                            LoadFormComponents();
                            return;
                        }


                        KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null);
                        //String[] DosszieIds = new String[selectedItemsList.Count];
                        //for (int i = 0; i < DosszieIds.Length; i++)
                        //{
                        //    DosszieIds[i] = selectedItemsList[i];
                        //}
                        String[] DosszieIds = selectedItemsList.ToArray();

                        //EREC_KuldKezFeljegyzesek erec_KuldKezFeljegyzesek = GetBusinessObjectFromComponents_KuldKezFeljegyzesek();

                        //Result result = service.AtadasraKijeloles(execParam, KuldemenyId, kovFelelosId, erec_KuldKezFeljegyzesek);
                        Result result = service.AtadasraKijeloles_Tomeges(execParam,
                            DosszieIds, kovFelelosId, erec_HataridosFeladat);

                        if (String.IsNullOrEmpty(result.ErrorCode))
                        {
                            if (DosszieIds.Length > 0)
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, DosszieIds[0]);
                            }
                            JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            UI.MarkFailedRecordsInGridView(DossziekGridView, result);
                        }

                    }
                }
                    
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

            if(e.CommandName == CommandName.SaveAndPrint)
            {
                if (String.IsNullOrEmpty(TomegesTetelAtadasUid))
                {
                    Result result = Atadas();

                    if (result != null && !result.IsError)
                    {
                        TomegesTetelAtadasUid = result.Uid;
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(DossziekGridView, result);
                        return;
                    }
                }

                string js=String.Empty;
                string url = String.Empty;

                switch (Mode)
                {
                    case AtadasTipus.UgyiratAtadas:
                        string sendurl = "AtadoAtvevoListaPrintFormUgyiratSSRS.aspx?Atado=" + HttpUtility.UrlEncode(Atado_FelhasznaloCsoportTextBox.Text, System.Text.UnicodeEncoding.UTF8) + "&Atvevo=" + HttpUtility.UrlEncode(CsoportId_Felelos_Kovetkezo_CsoportTextBox.Text, System.Text.UnicodeEncoding.UTF8) + "&Ids=" + UgyiratId;
                        js = "javascript:window.open('"+sendurl+"')";
                        break;

                    case AtadasTipus.IratPeldanyAtadas:
                        string sendurl2 = "AtadoAtvevoListaPrintFormIratpeldanySSRS.aspx?Atado=" + HttpUtility.UrlEncode(Atado_FelhasznaloCsoportTextBox.Text, System.Text.UnicodeEncoding.UTF8) + "&Atvevo=" + HttpUtility.UrlEncode(CsoportId_Felelos_Kovetkezo_CsoportTextBox.Text, System.Text.UnicodeEncoding.UTF8) + "&Ids=" + IratPeldanyId;
                        js = "javascript:window.open('" + sendurl2 + "')";
                        break;

                    case AtadasTipus.KuldemenyAtadas:
                        string sendurl3 = "AtadoAtvevoListaPrintFormKimenoKuldemenySSRS.aspx?Atado=" + HttpUtility.UrlEncode(Atado_FelhasznaloCsoportTextBox.Text, System.Text.UnicodeEncoding.UTF8) + "&Atvevo=" + HttpUtility.UrlEncode(CsoportId_Felelos_Kovetkezo_CsoportTextBox.Text, System.Text.UnicodeEncoding.UTF8) + "&Ids=" + KuldemenyId;
                        js = "javascript:window.open('" + sendurl3 + "')";
                        break;
                }

                
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyExpedialas", js, true);
            }
        }
    }

    private Result Atadas()
    {
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtadas)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtadas)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtadas)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtadas))
        {
            
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            #region Lista id-k meghat�roz�sa
            List<string> selectedItemsList = new List<string>();
            if(Mode == AtadasTipus.UgyiratAtadas)
            {
                selectedItemsList =  ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
            }
            else if(Mode == AtadasTipus.IratPeldanyAtadas)
            {
                selectedItemsList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);
            }
            else if (Mode == AtadasTipus.KuldemenyAtadas)
            {
                //k�ldem�nyn�l k�l�n fucntion
                EREC_KuldKuldemenyekService kuldService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                selectedItemsList = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null);

                String[] kuldTetelIds = selectedItemsList.ToArray();
                Result kuldResult = null;

                if (kuldTetelIds.Length > 0)
                {
                    String kovFelelosId = CsoportId_Felelos_Kovetkezo_CsoportTextBox.Id_HiddenField;
                    EREC_HataridosFeladatok erec_HataridosFeladat = KezelesiFeljegyzesPanel1.GetBusinessObject();

                    kuldResult = kuldService.Atadas_Tomeges(execParam, kuldTetelIds, kovFelelosId , erec_HataridosFeladat);
                    if (!String.IsNullOrEmpty(kuldResult.ErrorCode))
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, kuldResult);
                    else
                        UgyiratokGridViewBind();
                }

                return kuldResult;
            }
            else if (Mode == AtadasTipus.DosszieAtadas)
            {
                selectedItemsList = ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null);
            }
            #endregion

            String[] tetelIds = selectedItemsList.ToArray();

            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            Result result = null;
            if (tetelIds.Length > 0)
            {
                result = service.Atadas_Tomeges(execParam, tetelIds);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                else
                    UgyiratokGridViewBind();
            }

            return result;
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            return null;
        }
    }

    protected void ImagePrint_Click(object sender, ImageClickEventArgs e)
    {

    }
}
