﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class AtadoAtvevoListaPrintForm : Contentum.eUtility.UI.PageBase
{
    private string fejId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        fejId = Request.QueryString.Get("fejId");

        if (String.IsNullOrEmpty(fejId))
        {
            // nincs Id megadva:
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(fejId))
        {
            EREC_IraKezbesitesiFejekService fej_service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiFejekService();
            EREC_IraKezbesitesiFejekSearch erec_IraKezbesitesiFejekSearch = new EREC_IraKezbesitesiFejekSearch();

            erec_IraKezbesitesiFejekSearch.Id.Value = fejId;
            erec_IraKezbesitesiFejekSearch.Id.Operator = Query.Operators.equals;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result fej_result = fej_service.GetAllWithExtension(execParam, erec_IraKezbesitesiFejekSearch);

            string irany = "";
            
            foreach (DataRow _row in fej_result.Ds.Tables[0].Rows)
            {
                irany = _row["Tipus"].ToString();
            }

            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

            /*if (String.Equals(irany, "A"))
            {
                erec_IraKezbesitesiTetelekSearch.WhereByManual = " and EREC_IraKezbesitesiTetelek.KezbesitesFej_Id='" + fejId + "'";
            }
            else if (String.Equals(irany, "V"))
            {
                erec_IraKezbesitesiTetelekSearch.WhereByManual = " and EREC_IraKezbesitesiTetelek.AtveteliFej_Id='" + fejId + "'";
            }
                
            erec_IraKezbesitesiTetelekSearch.OrderBy = " EREC_IraKezbesitesiTetelek.Obj_type, Azonosito";*/
            
            Result result = service.ForXml(execParam, erec_IraKezbesitesiTetelekSearch, irany, fejId);

            /*DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date";
            column.AutoIncrement = false;
            column.Caption = "Date";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "count";
            column.AutoIncrement = false;
            column.Caption = "count";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Date"] = System.DateTime.Now.ToString();
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            row["count"] = result.Ds.Tables[0].Rows.Count.ToString();
            table.Rows.Add(row);*/

            //string xml = result.Ds.GetXml();
            //string xsd = result.Ds.GetXmlSchema();

            string templateText = "";
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
            
            if (String.Equals(irany, "A"))
            {
                WebRequest wr = WebRequest.Create(SP_TM_site_url + "Atado lista.xml");
                wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                templateText = template.ReadToEnd();
                template.Close();
                //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"aaaalllll\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>VandorE</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>8</o:Revision><o:TotalTime>5</o:TotalTime><o:Created>2008-11-17T12:26:00Z</o:Created><o:LastSaved>2008-11-17T15:34:00Z</o:LastSaved><o:Pages>2</o:Pages><o:Words>25</o:Words><o:Characters>179</o:Characters><o:Company>Főpolgármesteri Hivatal</o:Company><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>203</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Tahoma\"><w:panose-1 w:val=\"020B0604030504040204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"61002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000101FF\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"66CF6869\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"A8680E16\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"00A420B8\"/><w:pPr><w:spacing w:before=\"120\" w:after=\"120\"/><w:ind w:left=\"482\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00A420B8\"/><w:pPr><w:ind w:left=\"482\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Dokumentumtrkp\"><w:name w:val=\"Document Map\"/><wx:uiName wx:val=\"Dokumentumtérkép\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"DokumentumtrkpChar\"/><w:rsid w:val=\"0070467B\"/><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Tahoma\" w:h-ansi=\"Tahoma\" w:cs=\"Tahoma\"/><wx:font wx:val=\"Tahoma\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"DokumentumtrkpChar\"><w:name w:val=\"Dokumentumtérkép Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"Dokumentumtrkp\"/><w:rsid w:val=\"0070467B\"/><w:rPr><w:rFonts w:ascii=\"Tahoma\" w:h-ansi=\"Tahoma\" w:cs=\"Tahoma\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszerű bekezdés\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"009F4233\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"11266\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"120\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00A420B8\"/><wsp:rsid wsp:val=\"00057107\"/><wsp:rsid wsp:val=\"00072547\"/><wsp:rsid wsp:val=\"00100B12\"/><wsp:rsid wsp:val=\"00231F33\"/><wsp:rsid wsp:val=\"002740B4\"/><wsp:rsid wsp:val=\"0029138D\"/><wsp:rsid wsp:val=\"002A6203\"/><wsp:rsid wsp:val=\"00422746\"/><wsp:rsid wsp:val=\"00476D90\"/><wsp:rsid wsp:val=\"004A67E8\"/><wsp:rsid wsp:val=\"004C5A6E\"/><wsp:rsid wsp:val=\"00573ABF\"/><wsp:rsid wsp:val=\"00594CCA\"/><wsp:rsid wsp:val=\"005A4082\"/><wsp:rsid wsp:val=\"005B1BFD\"/><wsp:rsid wsp:val=\"005B5E53\"/><wsp:rsid wsp:val=\"0070467B\"/><wsp:rsid wsp:val=\"007E1B0E\"/><wsp:rsid wsp:val=\"00852BAD\"/><wsp:rsid wsp:val=\"009904A4\"/><wsp:rsid wsp:val=\"009A21E8\"/><wsp:rsid wsp:val=\"009F4233\"/><wsp:rsid wsp:val=\"00A0138B\"/><wsp:rsid wsp:val=\"00A33968\"/><wsp:rsid wsp:val=\"00A420B8\"/><wsp:rsid wsp:val=\"00AD6CFF\"/><wsp:rsid wsp:val=\"00BA089C\"/><wsp:rsid wsp:val=\"00C678F6\"/><wsp:rsid wsp:val=\"00CC1B59\"/><wsp:rsid wsp:val=\"00CE0D44\"/><wsp:rsid wsp:val=\"00D81606\"/><wsp:rsid wsp:val=\"00DA5E33\"/><wsp:rsid wsp:val=\"00DB282F\"/><wsp:rsid wsp:val=\"00DD36CB\"/><wsp:rsid wsp:val=\"00FA1107\"/><wsp:rsid wsp:val=\"00FE13AE\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><Fejlec_Tabla ns0:repeater=\"true\"><w:p wsp:rsidR=\"00A0138B\" wsp:rsidRDefault=\"00A0138B\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr></w:p><w:p wsp:rsidR=\"00A0138B\" wsp:rsidRDefault=\"00100B12\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr><w:r><w:br w:type=\"page\"/></w:r></w:p><w:p wsp:rsidR=\"00A420B8\" wsp:rsidRDefault=\"00A33968\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr><w:r><w:tab/><w:t>átadó</w:t></w:r><w:r><w:tab/><w:t>átvevő</w:t></w:r></w:p><w:p wsp:rsidR=\"00A33968\" wsp:rsidRDefault=\"00A33968\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr><w:r><w:tab/><w:t>……………………………….</w:t></w:r><w:r><w:tab/><w:t>……………………………….</w:t></w:r></w:p><w:p wsp:rsidR=\"00A33968\" wsp:rsidRDefault=\"00A33968\"/><w:tbl><w:tblPr><w:tblW w:w=\"14368\" w:type=\"dxa\"/><w:tblInd w:w=\"482\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"2320\"/><w:gridCol w:w=\"2835\"/><w:gridCol w:w=\"2976\"/><w:gridCol w:w=\"3119\"/><w:gridCol w:w=\"3118\"/></w:tblGrid><w:tr wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidTr=\"00DD36CB\"><w:tc><w:tcPr><w:tcW w:w=\"2320\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Azonosító</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2835\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Küldő</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2976\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Tárgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"3119\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Címzett</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"3118\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Ügyintéző</w:t></w:r></w:p></w:tc></w:tr><Adat_Tabla ns0:repeater=\"true\"><w:tr wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidTr=\"00DD36CB\"><w:trPr><w:trHeight w:val=\"574\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"2320\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00057107\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr><Azonosito/><w:r wsp:rsidR=\"00DD36CB\"><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr><w:br/></w:r><Barkod/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2835\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00057107\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr><Kuldo/><w:r wsp:rsidR=\"00DD36CB\"><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr><w:br/></w:r><Kuldo_cim/></w:p></w:tc><Targy><w:tc><w:tcPr><w:tcW w:w=\"2976\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr></w:p></w:tc></Targy><w:tc><w:tcPr><w:tcW w:w=\"3119\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00057107\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr><CimzettNev/><w:r wsp:rsidR=\"00DD36CB\"><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr><w:br/></w:r><CimzettCim/></w:p></w:tc><Ugyintezo><w:tc><w:tcPr><w:tcW w:w=\"3118\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00DD36CB\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"00DD36CB\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr></w:p></w:tc></Ugyintezo></w:tr></Adat_Tabla></w:tbl><w:p wsp:rsidR=\"00A420B8\" wsp:rsidRDefault=\"00A420B8\"/><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"002A6203\"><w:r><w:t>Átadott tételek száma: </w:t></w:r><w:r><w:tab/></w:r><szam/><w:r wsp:rsidR=\"009F4233\"><w:t> </w:t></w:r><w:r><w:t>db</w:t></w:r></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"002A6203\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:r><w:t>Átvevő:</w:t></w:r><w:r><w:tab/></w:r><Cimzett_Nev/></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"002A6203\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:r><w:t>Átadó:</w:t></w:r><w:r><w:tab/></w:r><Atado_Nev/></w:p><w:p wsp:rsidR=\"0070467B\" wsp:rsidRDefault=\"0070467B\" wsp:rsidP=\"002A6203\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"0070467B\"><w:pPr><w:jc w:val=\"center\"/><w:outlineLvl w:val=\"0\"/></w:pPr><w:r><w:t>Dátum: </w:t></w:r><Date/></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRPr=\"007B74E0\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"0070467B\"><w:pPr><w:jc w:val=\"center\"/><w:outlineLvl w:val=\"0\"/><w:rPr><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"007B74E0\"><w:rPr><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr><w:t>Átadás-átvételi lista</w:t></w:r></w:p></Fejlec_Tabla></ns0:NewDataSet><w:sectPr wsp:rsidR=\"002A6203\" wsp:rsidRPr=\"007B74E0\" wsp:rsidSect=\"0070467B\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"426\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";
            }
            else if (String.Equals(irany, "V"))
            {
                WebRequest wr = WebRequest.Create(SP_TM_site_url + "Atvevo lista.xml");
                wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                templateText = template.ReadToEnd();
                template.Close();
                //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"aaaalllll\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>VandorE</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>4</o:Revision><o:TotalTime>3</o:TotalTime><o:Created>2008-11-17T12:39:00Z</o:Created><o:LastSaved>2008-11-17T13:18:00Z</o:LastSaved><o:Pages>2</o:Pages><o:Words>25</o:Words><o:Characters>179</o:Characters><o:Company>Főpolgármesteri Hivatal</o:Company><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>203</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Tahoma\"><w:panose-1 w:val=\"020B0604030504040204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"61002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000101FF\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"66CF6869\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"A8680E16\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"00A420B8\"/><w:pPr><w:spacing w:before=\"120\" w:after=\"120\"/><w:ind w:left=\"482\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00A420B8\"/><w:pPr><w:ind w:left=\"482\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Dokumentumtrkp\"><w:name w:val=\"Document Map\"/><wx:uiName wx:val=\"Dokumentumtérkép\"/><w:basedOn w:val=\"Norml\"/><w:link w:val=\"DokumentumtrkpChar\"/><w:rsid w:val=\"0070467B\"/><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Tahoma\" w:h-ansi=\"Tahoma\" w:cs=\"Tahoma\"/><wx:font wx:val=\"Tahoma\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"DokumentumtrkpChar\"><w:name w:val=\"Dokumentumtérkép Char\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:link w:val=\"Dokumentumtrkp\"/><w:rsid w:val=\"0070467B\"/><w:rPr><w:rFonts w:ascii=\"Tahoma\" w:h-ansi=\"Tahoma\" w:cs=\"Tahoma\"/><w:sz w:val=\"16\"/><w:sz-cs w:val=\"16\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszerű bekezdés\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"009F4233\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"10242\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"120\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00A420B8\"/><wsp:rsid wsp:val=\"00072547\"/><wsp:rsid wsp:val=\"00100B12\"/><wsp:rsid wsp:val=\"00195850\"/><wsp:rsid wsp:val=\"00231F33\"/><wsp:rsid wsp:val=\"002740B4\"/><wsp:rsid wsp:val=\"002A6203\"/><wsp:rsid wsp:val=\"0039104A\"/><wsp:rsid wsp:val=\"00422746\"/><wsp:rsid wsp:val=\"00476D90\"/><wsp:rsid wsp:val=\"004A67E8\"/><wsp:rsid wsp:val=\"004C5A6E\"/><wsp:rsid wsp:val=\"00573ABF\"/><wsp:rsid wsp:val=\"00594CCA\"/><wsp:rsid wsp:val=\"005A4082\"/><wsp:rsid wsp:val=\"005B1BFD\"/><wsp:rsid wsp:val=\"005B5E53\"/><wsp:rsid wsp:val=\"0070467B\"/><wsp:rsid wsp:val=\"007E1B0E\"/><wsp:rsid wsp:val=\"00852BAD\"/><wsp:rsid wsp:val=\"00931618\"/><wsp:rsid wsp:val=\"009904A4\"/><wsp:rsid wsp:val=\"009F4233\"/><wsp:rsid wsp:val=\"00A0138B\"/><wsp:rsid wsp:val=\"00A33968\"/><wsp:rsid wsp:val=\"00A420B8\"/><wsp:rsid wsp:val=\"00AD6CFF\"/><wsp:rsid wsp:val=\"00B36CBE\"/><wsp:rsid wsp:val=\"00BA089C\"/><wsp:rsid wsp:val=\"00CE0D44\"/><wsp:rsid wsp:val=\"00D55B39\"/><wsp:rsid wsp:val=\"00D81606\"/><wsp:rsid wsp:val=\"00DA5E33\"/><wsp:rsid wsp:val=\"00DB282F\"/><wsp:rsid wsp:val=\"00FA1107\"/><wsp:rsid wsp:val=\"00FE13AE\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><Fejlec_Tabla ns0:repeater=\"true\"><w:p wsp:rsidR=\"00A0138B\" wsp:rsidRDefault=\"00A0138B\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr></w:p><w:p wsp:rsidR=\"00A0138B\" wsp:rsidRDefault=\"00100B12\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr><w:r><w:br w:type=\"page\"/></w:r></w:p><w:p wsp:rsidR=\"00A420B8\" wsp:rsidRDefault=\"00A33968\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr><w:r><w:tab/><w:t>átadó</w:t></w:r><w:r><w:tab/><w:t>átvevő</w:t></w:r></w:p><w:p wsp:rsidR=\"00A33968\" wsp:rsidRDefault=\"00A33968\" wsp:rsidP=\"00A33968\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"3686\"/><w:tab w:val=\"center\" w:pos=\"10348\"/></w:tabs></w:pPr><w:r><w:tab/><w:t>……………………………….</w:t></w:r><w:r><w:tab/><w:t>……………………………….</w:t></w:r></w:p><w:p wsp:rsidR=\"00A33968\" wsp:rsidRDefault=\"00A33968\"/><w:tbl><w:tblPr><w:tblW w:w=\"14368\" w:type=\"dxa\"/><w:tblInd w:w=\"482\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"2320\"/><w:gridCol w:w=\"2835\"/><w:gridCol w:w=\"2976\"/><w:gridCol w:w=\"3119\"/><w:gridCol w:w=\"3118\"/></w:tblGrid><w:tr wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidTr=\"0039104A\"><w:tc><w:tcPr><w:tcW w:w=\"2320\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Azonosító</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2835\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Küldő</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2976\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Tárgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"3119\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Címzett</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"3118\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"A6A6A6\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"009F4233\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:b/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"009F4233\"><w:rPr><w:b/></w:rPr><w:t>Ügyintéző</w:t></w:r></w:p></w:tc></w:tr><Adat_Tabla ns0:repeater=\"true\"><w:tr wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidTr=\"0039104A\"><w:trPr><w:trHeight w:val=\"574\"/></w:trPr><w:tc><w:tcPr><w:tcW w:w=\"2320\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr><Azonosito/><w:r><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr><w:br/></w:r><Barkod/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2835\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr><Kuldo/><w:r><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr><w:br/></w:r><Kuldo_cim/></w:p></w:tc><Targy><w:tc><w:tcPr><w:tcW w:w=\"2976\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr></w:p></w:tc></Targy><w:tc><w:tcPr><w:tcW w:w=\"3119\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr><CimzettNev/><w:r><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr><w:br/></w:r><CimzettCim/></w:p></w:tc><Ugyintezo><w:tc><w:tcPr><w:tcW w:w=\"3118\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0039104A\" wsp:rsidRPr=\"009F4233\" wsp:rsidRDefault=\"0039104A\" wsp:rsidP=\"00100B12\"><w:pPr><w:ind w:left=\"0\"/><w:rPr><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/></w:rPr></w:pPr></w:p></w:tc></Ugyintezo></w:tr></Adat_Tabla></w:tbl><w:p wsp:rsidR=\"00A420B8\" wsp:rsidRDefault=\"00A420B8\"/><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"002A6203\"><w:r><w:t>Átadott tételek száma: </w:t></w:r><w:r><w:tab/></w:r><szam/><w:r wsp:rsidR=\"009F4233\"><w:t> </w:t></w:r><w:r><w:t>db</w:t></w:r></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"002A6203\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:r><w:t>Átvevő:</w:t></w:r><w:r><w:tab/></w:r><Atvevo_Nev/></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"002A6203\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:r><w:t>Átadó:</w:t></w:r><w:r><w:tab/></w:r><Atado_Nev/></w:p><w:p wsp:rsidR=\"0070467B\" wsp:rsidRDefault=\"0070467B\" wsp:rsidP=\"002A6203\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"0070467B\"><w:pPr><w:jc w:val=\"center\"/><w:outlineLvl w:val=\"0\"/></w:pPr><w:r><w:t>Dátum: </w:t></w:r><Date/></w:p><w:p wsp:rsidR=\"002A6203\" wsp:rsidRPr=\"007B74E0\" wsp:rsidRDefault=\"002A6203\" wsp:rsidP=\"0070467B\"><w:pPr><w:jc w:val=\"center\"/><w:outlineLvl w:val=\"0\"/><w:rPr><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"007B74E0\"><w:rPr><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr><w:t>Átadás-átvételi lista</w:t></w:r></w:p></Fejlec_Tabla></ns0:NewDataSet><w:sectPr wsp:rsidR=\"002A6203\" wsp:rsidRPr=\"007B74E0\" wsp:rsidSect=\"0070467B\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"426\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";
            }

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }

            result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

            string filename = "";

            if (String.Equals(irany, "A"))
            {
                filename = "Atado_lista_";
            }
            else
            {
                filename = "Atvevo_lista_";
            }
            
            if (pdf)
            {
                filename = filename +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
            }
            else
            {
                filename = filename +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
            }

            int priority = 1;
            bool prior = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

            if (string.IsNullOrEmpty(csop_result.ErrorCode))
            {
                foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
                {
                    if (_row["Tipus"].ToString().Equals("3"))
                    {
                        prior = true;
                    }
                }
            }

            if (prior)
            {
                priority++;
            }
            
            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
            result = tms.GetWordDocument_ForXml_Thread(templateText, result, pdf, priority, filename, 25);

            byte[] res = (byte[])result.Record;

            /*if (String.Equals(irany, "A"))
            {
                String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ')
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );

                Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                
                if (pdf)
                {
                    result = ds.UploadFromeRecordWithCTTCheckin(execParam
                        , Constants.DocumentStoreType.SharePoint
                        , "Atado lista " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf"
                        , res
                        , new KRT_Dokumentumok()
                        , uploadXmlStrParams);
                }
                else
                {
                    result = ds.UploadFromeRecordWithCTTCheckin(execParam
                        , Constants.DocumentStoreType.SharePoint
                        , "Atado lista " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc"
                        , res
                        , new KRT_Dokumentumok()
                        , uploadXmlStrParams);
                }
            }
            else if (String.Equals(irany, "V"))
            {
                String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ')
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                
                Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();

                if (pdf)
                {
                    result = ds.UploadFromeRecordWithCTTCheckin(execParam
                        , Constants.DocumentStoreType.SharePoint
                        , "Atvevo lista " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf"
                        , res
                        , new KRT_Dokumentumok()
                        , uploadXmlStrParams);
                }
                else
                {
                    result = ds.UploadFromeRecordWithCTTCheckin(execParam
                        , Constants.DocumentStoreType.SharePoint
                        , "Atvevo lista " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc"
                        , res
                        , new KRT_Dokumentumok()
                        , uploadXmlStrParams);
                }
            }

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                execParam.Record_Id = fejId;

                fej_result = fej_service.Get(execParam);

                EREC_IraKezbesitesiFejek erec_IraKezbesitesiFejek = (EREC_IraKezbesitesiFejek)fej_result.Record;

                erec_IraKezbesitesiFejek.Updated.SetValueAll(false);
                erec_IraKezbesitesiFejek.Base.Updated.SetValueAll(false);
                erec_IraKezbesitesiFejek.Base.Updated.Ver = true;

                erec_IraKezbesitesiFejek.UtolsoNyomtatas_Id = result.Uid;
                erec_IraKezbesitesiFejek.Updated.UtolsoNyomtatas_Id = true;

                Result res_update = fej_service.Update(execParam, erec_IraKezbesitesiFejek);
            }*/

            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                if (pdf)
                {
                    Response.ContentType = "application/pdf";
                }
                else
                {
                    Response.ContentType = "application/msword";
                }
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            else
            {
                if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
                {
                    string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                }
            }
        }
    }
}