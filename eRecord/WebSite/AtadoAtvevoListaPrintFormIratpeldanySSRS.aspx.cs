﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class AtadoAtvevoListaPrintForm : Contentum.eUtility.UI.PageBase
{
    private string Ids = String.Empty;
    private string[] IdsArray;
    private string Atado = String.Empty;
    private string Atvevo = String.Empty;
    private string Ids2 = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        Ids = Request.QueryString["Ids"];
        Ids2 = Ids.Replace(",", "','");
        IdsArray = Ids.Split(',');
        // IdsArray = new string[] {"6e88064c- 2f33 - e811 - 80c9 - 00155d020dd3","48a84560 - 2d33 - e811 - 80c9 - 00155d020dd3"};
        Atado = Request.QueryString["Atado"];
        Atvevo = Request.QueryString["Atvevo"];



        if (String.IsNullOrEmpty(Ids))
        {
            // nincs Id megadva:
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !String.IsNullOrEmpty(Ids))
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;




        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];


                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {


                        case "Ids":
                            ReportParameters[i].Values.AddRange(IdsArray);
                            break;

                        case "Szervezet":
                            // ReportParameters[i].Values.Add(Szervezet);
                            break;

                        case "Atado":

                            ReportParameters[i].Values.Add(Atado);

                            break;

                        case "Atvevo":

                            ReportParameters[i].Values.Add(Atvevo);

                            break;

                        case "Where":
                            ReportParameters[i].Values.Add("EREC_PldIratPeldanyok.ErvKezd <= getdate() and EREC_PldIratPeldanyok.ErvVege >= getdate() and dbo.fn_GetCsoportNev(EREC_PldIratPeldanyok.FelhasznaloCsoport_Id_Orzo) = '" + Atado + "' and EREC_PldIratPeldanyok.id in ('" + Ids2 + "')");
                            break;
                    }
                }

            }

            else
            {
                // Response.Write("ooo");
            }

        }
        return ReportParameters;
    }
}