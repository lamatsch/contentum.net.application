﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class AtadoAtvevoListaPrintForm : Contentum.eUtility.UI.PageBase
{
    private string fejId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        fejId = Request.QueryString.Get("fejId");

        if (String.IsNullOrEmpty(fejId))
        {
            // nincs Id megadva:
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && !String.IsNullOrEmpty(fejId))
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;


        EREC_IraKezbesitesiFejekService fej_service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiFejekService();
        EREC_IraKezbesitesiFejekSearch erec_IraKezbesitesiFejekSearch = new EREC_IraKezbesitesiFejekSearch();

        erec_IraKezbesitesiFejekSearch.Id.Value = fejId;
        erec_IraKezbesitesiFejekSearch.Id.Operator = Query.Operators.equals;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result fej_result = fej_service.GetAllWithExtension(execParam, erec_IraKezbesitesiFejekSearch);

        string irany = "";

        foreach (DataRow _row in fej_result.Ds.Tables[0].Rows)
        {
            irany = _row["Tipus"].ToString();
        }

        execParam.Fake = true;
        fej_result = fej_service.GetAllWithExtension(execParam, erec_IraKezbesitesiFejekSearch);

        EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
        EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

       //  Result result = service.GetAllWithExtension(execParam, erec_IraKezbesitesiTetelekSearch);
        Result result = service.ForXml(execParam, erec_IraKezbesitesiTetelekSearch, irany, fejId);

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < rpis.Count; i++)
                    {
                        ReportParameters[i] = new ReportParameter(rpis[i].Name);

                        switch (rpis[i].Name)
                        {
                            case "AtadasIrany":
                                ReportParameters[i].Values.Add(irany);
                                break;

                            case "Where":
                                ReportParameters[i].Values.Add("EREC_IraKezbesitesiFejek.ErvKezd <= getdate() and EREC_IraKezbesitesiFejek.ErvVege >= getdate() and EREC_IraKezbesitesiFejek.Tipus= '"+irany+"' and EREC_IraKezbesitesiFejek.Id = '"+ fejId +"'");
                                break;

                            case "TopRow":
                                //  ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                                break;

                            case "ExecutorUserId":
                                ReportParameters[i].Values.Add(fej_result.SqlCommand.GetParamValue("@ExecutorUserId"));
                                break;

                            case "FelhasznaloSzervezet_Id":
                                if (!string.IsNullOrEmpty(fej_result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id")))
                                {
                                    ReportParameters[i].Values.Add(fej_result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                                }

                                break;

                            case "fejId":
                                ReportParameters[i].Values.Add(fejId);
                                break;

                            case "Tipus":
                                ReportParameters[i].Values.Add(irany);
                                break;

                            case "Jogosultak":
                                if (!string.IsNullOrEmpty(fej_result.SqlCommand.GetParamValue("@Jogosultak")))
                                {
                                    //ReportParameters[i].Values.Add(fej_result.SqlCommand.GetParamValue("@Jogosultak"));  
                                }
                                break;
                        }
                    }
                }
                else
                {
                    // Response.Write("ooo");
                }
            }
        }
        return ReportParameters;
    }
}