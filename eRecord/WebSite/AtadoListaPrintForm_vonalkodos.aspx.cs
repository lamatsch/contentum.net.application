﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;

public partial class AtadoListaPrintForm_vonalkodos : System.Web.UI.Page
{
    private string tetelIds = String.Empty;
    private string qs_ugyiratIds = String.Empty;
    private string qs_iratpldIds = String.Empty;
    private string qs_kuldIds = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        /*tetelIds = Request.QueryString.Get("Id");

        if (String.IsNullOrEmpty(tetelIds))
        {
            // nincs Id megadva:
        }*/
    }

    private EREC_IraKezbesitesiTetelekSearch GetSearchObjectFromComponents()
    {
        EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

        erec_IraKezbesitesiTetelekSearch.Id.Value = tetelIds;
        erec_IraKezbesitesiTetelekSearch.Id.Operator = Query.Operators.inner;

        return erec_IraKezbesitesiTetelekSearch;
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents_ugy()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        erec_UgyUgyiratokSearch.Id.Value = qs_ugyiratIds;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_pld()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.Id.Value = qs_iratpldIds;
        erec_PldIratPeldanyokSearch.Id.Operator = Query.Operators.inner;

        return erec_PldIratPeldanyokSearch;
    }

    private EREC_KuldKuldemenyekSearch GetSearchObjectFromComponents_kuld()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        erec_KuldKuldemenyekSearch.Id.Value = qs_kuldIds;
        erec_KuldKuldemenyekSearch.Id.Operator = Query.Operators.inner;

        return erec_KuldKuldemenyekSearch;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack && Session["SelectedKezbesitesiTetelIds"] == null)
        {
            //a nyitó oldalról érkező paramétereket kiolvasó script regisztrálása
            string hiddenFieldID = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            string qs_ugyirat = Request.QueryString.Get("qs_ugyirat");
            string qs_iratpld = Request.QueryString.Get("qs_iratpld");
            string qs_kuld = Request.QueryString.Get("qs_kuld");
            if (!String.IsNullOrEmpty(hiddenFieldID))
            {
                Dictionary<string, string> ParentChildControl = new Dictionary<string, string>(4);
                ParentChildControl.Add(hiddenFieldID, hfAtadoLista.ClientID);
                ParentChildControl.Add(qs_ugyirat, hfugyiratIds.ClientID);
                ParentChildControl.Add(qs_iratpld, hfiratpldIds.ClientID);
                ParentChildControl.Add(qs_kuld, hfkuldIds.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl, "", EventArgumentConst.refreshValuesFromParent);

            }

            /*string qs_ugyirat = Request.QueryString.Get("qs_ugyirat");
            if (!String.IsNullOrEmpty(qs_ugyirat))
            {
                Dictionary<string, string> ParentChildControl_ugyirat = new Dictionary<string, string>(1);
                ParentChildControl_ugyirat.Add(qs_ugyirat, hfugyiratIds.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl_ugyirat, "", EventArgumentConst.refreshValuesFromParent);

            }

            string qs_iratpld = Request.QueryString.Get("qs_iratpld");
            if (!String.IsNullOrEmpty(qs_iratpld))
            {
                Dictionary<string, string> ParentChildControl_iratpld = new Dictionary<string, string>(1);
                ParentChildControl_iratpld.Add(qs_iratpld, hfiratpldIds.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl_iratpld, "", EventArgumentConst.refreshValuesFromParent);

            }

            string qs_kuld = Request.QueryString.Get("qs_kuld");
            if (!String.IsNullOrEmpty(qs_kuld))
            {
                Dictionary<string, string> ParentChildControl_kuld = new Dictionary<string, string>(1);
                ParentChildControl_kuld.Add(qs_kuld, hfkuldIds.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl_kuld, "", EventArgumentConst.refreshValuesFromParent);

            }*/
        }
        else
        {
            if (Session["SelectedKezbesitesiTetelIds"] != null)
            {
                tetelIds = Session["SelectedKezbesitesiTetelIds"].ToString();
            }
            else
            {
                string eventArgument = Request.Params.Get("__EVENTARGUMENT");
                if (!String.IsNullOrEmpty(eventArgument))
                {
                    if (eventArgument == EventArgumentConst.refreshValuesFromParent)
                    {
                        if (hfAtadoLista.Value.Trim() != String.Empty)
                        {
                            tetelIds = hfAtadoLista.Value;
                        }
                        if (hfugyiratIds.Value.Trim() != String.Empty)
                        {
                            qs_ugyiratIds = hfugyiratIds.Value;
                        }
                        if (hfiratpldIds.Value.Trim() != String.Empty)
                        {
                            qs_iratpldIds = hfiratpldIds.Value;
                        }
                        if (hfkuldIds.Value.Trim() != String.Empty)
                        {
                            qs_kuldIds = hfkuldIds.Value;
                        }
                    }
                }
            }
            tetelIds = tetelIds.Replace("$", "'");

            if (!String.IsNullOrEmpty(tetelIds))
            {

                EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = GetSearchObjectFromComponents();

                erec_IraKezbesitesiTetelekSearch.OrderBy = " EREC_IraKezbesitesiTetelek.Obj_type, Azonosito";

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.GetAllWithExtension(execParam, erec_IraKezbesitesiTetelekSearch);

                int ugyiratdb = 0;
                
                if (!String.IsNullOrEmpty(qs_ugyiratIds))
                {
                    EREC_UgyUgyiratokService ugyirat_service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    EREC_UgyUgyiratokSearch ugyirat_search = GetSearchObjectFromComponents_ugy();

                    Result ugy_result = ugyirat_service.GetAllWithExtension(execParam, ugyirat_search);

                    ugyiratdb = ugy_result.Ds.Tables[0].Rows.Count;

                    DataTable tempTable = new DataTable();
                    tempTable = ugy_result.Ds.Tables[0].Copy();
                    tempTable.TableName = "ugy_Table";
                    result.Ds.Tables.Add(tempTable);
                }

                int iratplddb = 0;
                
                if (!String.IsNullOrEmpty(qs_iratpldIds))
                {
                    EREC_PldIratPeldanyokService iratpld_service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    EREC_PldIratPeldanyokSearch iratpld_search = GetSearchObjectFromComponents_pld();

                    Result pld_result = iratpld_service.GetAllWithExtension(execParam, iratpld_search);

                    iratplddb = pld_result.Ds.Tables[0].Rows.Count;

                    DataTable tempTable = new DataTable();
                    tempTable = pld_result.Ds.Tables[0].Copy();
                    tempTable.TableName = "pld_Table";
                    result.Ds.Tables.Add(tempTable);
                }

                int kulddb = 0;
                
                if (!String.IsNullOrEmpty(qs_kuldIds))
                {
                    EREC_KuldKuldemenyekService kuld_service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    EREC_KuldKuldemenyekSearch kuld_search = GetSearchObjectFromComponents_kuld();

                    Result kuld_result = kuld_service.GetAllWithExtension(execParam, kuld_search);

                    kulddb = kuld_result.Ds.Tables[0].Rows.Count;
                    
                    DataTable tempTable = new DataTable();
                    tempTable = kuld_result.Ds.Tables[0].Copy();
                    tempTable.TableName = "kuld_Table";
                    result.Ds.Tables.Add(tempTable);
                }

                int ossz = ugyiratdb + iratplddb + kulddb;
                
                DataTable table = new DataTable("ParentTable");
                DataColumn column;
                DataRow row;
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.Int32");
                column.ColumnName = "id";
                column.ReadOnly = true;
                column.Unique = true;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Date";
                column.AutoIncrement = false;
                column.Caption = "Date";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "FelhNev";
                column.AutoIncrement = false;
                column.Caption = "FelhNev";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "count";
                column.AutoIncrement = false;
                column.Caption = "count";
                column.ReadOnly = false;
                column.Unique = false;
                table.Columns.Add(column);
                DataColumn[] PrimaryKeyColumns = new DataColumn[1];
                PrimaryKeyColumns[0] = table.Columns["id"];
                table.PrimaryKey = PrimaryKeyColumns;
                result.Ds.Tables.Add(table);

                row = table.NewRow();
                row["id"] = 0;
                row["Date"] = System.DateTime.Now.ToString();
                row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
                row["count"] = ossz.ToString();
                table.Rows.Add(row);

                string xml = result.Ds.GetXml();
                //string xsd = result.Ds.GetXmlSchema();
                string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"al\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>kiss.gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>7</o:Revision><o:TotalTime>28</o:TotalTime><o:Created>2008-01-11T14:51:00Z</o:Created><o:LastSaved>2008-02-06T07:58:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>30</o:Words><o:Characters>213</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>242</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"00000000000000000000\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"578A1BEE\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"7B8C329A\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"1\"><w:lsid w:val=\"61DA36D2\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"843461BC\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"1\"/></w:list><w:list w:ilfo=\"2\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"004C6AD9\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00C13B74\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"7170\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:relyOnVML/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00C13B74\"/><wsp:rsid wsp:val=\"00000191\"/><wsp:rsid wsp:val=\"00057B9F\"/><wsp:rsid wsp:val=\"000A7B4B\"/><wsp:rsid wsp:val=\"000B278F\"/><wsp:rsid wsp:val=\"000F4AC5\"/><wsp:rsid wsp:val=\"001273E3\"/><wsp:rsid wsp:val=\"00152388\"/><wsp:rsid wsp:val=\"00153272\"/><wsp:rsid wsp:val=\"001C2798\"/><wsp:rsid wsp:val=\"001E1C53\"/><wsp:rsid wsp:val=\"002A0192\"/><wsp:rsid wsp:val=\"00340259\"/><wsp:rsid wsp:val=\"00376EF4\"/><wsp:rsid wsp:val=\"003A5476\"/><wsp:rsid wsp:val=\"003F3219\"/><wsp:rsid wsp:val=\"00457A1F\"/><wsp:rsid wsp:val=\"00483D12\"/><wsp:rsid wsp:val=\"004C6AD9\"/><wsp:rsid wsp:val=\"004D7B2E\"/><wsp:rsid wsp:val=\"0067217C\"/><wsp:rsid wsp:val=\"00697D8E\"/><wsp:rsid wsp:val=\"007F4439\"/><wsp:rsid wsp:val=\"00892303\"/><wsp:rsid wsp:val=\"008C06BF\"/><wsp:rsid wsp:val=\"008F5F01\"/><wsp:rsid wsp:val=\"00916750\"/><wsp:rsid wsp:val=\"00946B74\"/><wsp:rsid wsp:val=\"00B72CB0\"/><wsp:rsid wsp:val=\"00B84CEF\"/><wsp:rsid wsp:val=\"00BA689D\"/><wsp:rsid wsp:val=\"00BB26A8\"/><wsp:rsid wsp:val=\"00C13B74\"/><wsp:rsid wsp:val=\"00C51C20\"/><wsp:rsid wsp:val=\"00C7526C\"/><wsp:rsid wsp:val=\"00CD2E3C\"/><wsp:rsid wsp:val=\"00CE1436\"/><wsp:rsid wsp:val=\"00CF35EA\"/><wsp:rsid wsp:val=\"00D172CF\"/><wsp:rsid wsp:val=\"00D338E7\"/><wsp:rsid wsp:val=\"00D52168\"/><wsp:rsid wsp:val=\"00D618EF\"/><wsp:rsid wsp:val=\"00DA202C\"/><wsp:rsid wsp:val=\"00DC1D7A\"/><wsp:rsid wsp:val=\"00DD6412\"/><wsp:rsid wsp:val=\"00EA511B\"/><wsp:rsid wsp:val=\"00EE3A55\"/><wsp:rsid wsp:val=\"00FF2092\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00C13B74\" wsp:rsidP=\"00BA689D\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00C13B74\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"32\"/><w:sz-cs w:val=\"32\"/></w:rPr><w:t>Átadó lista</w:t></w:r></w:p><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"008C06BF\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Dátum: </w:t></w:r><ParentTable><Date/></ParentTable></w:p><Table><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRDefault=\"00C13B74\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00C13B74\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Átadó:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t> </w:t></w:r><Atado_Nev/></w:p><w:p wsp:rsidR=\"00B72CB0\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00B72CB0\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Átvevő: </w:t></w:r><Cimzett_Nev/></w:p></Table><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRDefault=\"00C13B74\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00152388\" wsp:rsidRDefault=\"00152388\" wsp:rsidP=\"00BA689D\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Átadott tételek: </w:t></w:r><ParentTable><count/></ParentTable><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>db</w:t></w:r></w:p><w:p wsp:rsidR=\"00152388\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00152388\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14425\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"auto\"/></w:tblBorders><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"534\"/><w:gridCol w:w=\"1724\"/><w:gridCol w:w=\"1724\"/><w:gridCol w:w=\"2693\"/><w:gridCol w:w=\"2364\"/><w:gridCol w:w=\"2693\"/><w:gridCol w:w=\"2693\"/></w:tblGrid><w:tr wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidTr=\"00DA202C\"><w:tc><w:tcPr><w:tcW w:w=\"534\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Azonosító</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Vonalkód</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00A30C97\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Beküldő</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2364\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"004934AF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Beküldő</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t> címe</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Tárgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"BFBFBF\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"008C06BF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00D54DCC\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"008C06BF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:b/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Címzett</w:t></w:r></w:p></w:tc></w:tr><kuld_Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"003A5476\" wsp:rsidRPr=\"00C13B74\" wsp:rsidTr=\"00DA202C\"><w:tblPrEx><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"auto\"/></w:tblPrEx><w:tc><w:tcPr><w:tcW w:w=\"534\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"2\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:ind w:left=\"426\"/></w:pPr></w:p></w:tc><FullErkeztetoSzam><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></FullErkeztetoSzam><BarCode><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></BarCode><NevSTR_Bekuldo><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></NevSTR_Bekuldo><CimSTR_Bekuldo><w:tc><w:tcPr><w:tcW w:w=\"2364\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></CimSTR_Bekuldo><Targy><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></Targy><Csoport_Id_CimzettNev><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"003A5476\" wsp:rsidRDefault=\"003A5476\" wsp:rsidP=\"00C62C67\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></Csoport_Id_CimzettNev></w:tr></kuld_Table><ugy_Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"00C13B74\" wsp:rsidTr=\"00DA202C\"><w:tblPrEx><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"auto\"/></w:tblPrEx><w:tc><w:tcPr><w:tcW w:w=\"534\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00DA202C\"><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"2\"/><wx:t wx:val=\"2.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:ind w:left=\"426\"/></w:pPr></w:p></w:tc><Foszam_Merge><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00BF7DD5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></Foszam_Merge><BARCODE><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00BF7DD5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></BARCODE><NevSTR_Ugyindito><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00A30C97\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></NevSTR_Ugyindito><w:tc><w:tcPr><w:tcW w:w=\"2364\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"004934AF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc><Targy><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"00BF7DD5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></Targy><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00B84CEF\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00B84CEF\" wsp:rsidP=\"000F4AC5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc></w:tr></ugy_Table><pld_Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"000F4AC5\" wsp:rsidRPr=\"00C13B74\" wsp:rsidTr=\"00DA202C\"><w:tblPrEx><w:shd w:val=\"clear\" w:color=\"auto\" w:fill=\"auto\"/></w:tblPrEx><w:tc><w:tcPr><w:tcW w:w=\"534\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"00DA202C\"><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"2\"/><wx:t wx:val=\"3.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:ind w:left=\"426\"/></w:pPr></w:p></w:tc><FullErkeztetoSzam><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"00BF7DD5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></FullErkeztetoSzam><BarCode><w:tc><w:tcPr><w:tcW w:w=\"1724\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"00BF7DD5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></BarCode><NevSTR_Bekuldo><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"00A30C97\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></NevSTR_Bekuldo><w:tc><w:tcPr><w:tcW w:w=\"2364\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"004934AF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc><EREC_IraIratok_Targy><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"00BF7DD5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></EREC_IraIratok_Targy><w:tc><w:tcPr><w:tcW w:w=\"2693\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"000F4AC5\" wsp:rsidRDefault=\"000F4AC5\" wsp:rsidP=\"000F4AC5\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr></w:p></w:tc></w:tr></pld_Table></w:tbl><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00C13B74\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRDefault=\"00C13B74\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00B72CB0\" wsp:rsidRDefault=\"00B72CB0\" wsp:rsidP=\"00B72CB0\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"2835\"/><w:tab w:val=\"center\" w:pos=\"9923\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:tab/><w:t>……………………</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:tab/><w:t>……………………</w:t></w:r></w:p><w:p wsp:rsidR=\"00B72CB0\" wsp:rsidRDefault=\"00B72CB0\" wsp:rsidP=\"00B72CB0\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"2835\"/><w:tab w:val=\"center\" w:pos=\"9923\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:tab/><w:t>átadó</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:tab/><w:t>átvevő</w:t></w:r></w:p><w:p wsp:rsidR=\"00B72CB0\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00B72CB0\" wsp:rsidP=\"00B72CB0\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"9923\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00B72CB0\" wsp:rsidRDefault=\"00B72CB0\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00C13B74\" wsp:rsidRPr=\"00C13B74\" wsp:rsidRDefault=\"00C13B74\" wsp:rsidP=\"00C13B74\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00C13B74\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Készítette:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t> </w:t></w:r><ParentTable><FelhNev/></ParentTable></w:p></ns0:NewDataSet><w:sectPr wsp:rsidR=\"00C13B74\" wsp:rsidRPr=\"00C13B74\" wsp:rsidSect=\"00C13B74\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

                Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
                result = tms.GetWordDocument(templateText, xml, false);

                byte[] res = (byte[])result.Record;

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/msword";
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
        }

    }
}