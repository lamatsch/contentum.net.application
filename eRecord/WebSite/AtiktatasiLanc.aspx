﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="AtiktatasiLanc.aspx.cs" Inherits="AtiktatasiLanc" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
        
        <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
              
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    
    <asp:Panel ID="MainPanel" runat="server">
      
                 <table style="width: 95%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="IraIratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" PagerSettings-Visible="false" AutoGenerateColumns="False"
                                            DataKeyNames="Id" OnPreRender="IraIratokGridView_PreRender">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" Height="30px" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:BoundField DataField="HierarchyLevel" HeaderText="Sorrend">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Center" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="Iktatószám">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="labelIktatoszam" onclick='<%# this.GetIratViewClientClick(Eval("Id").ToString()) %>'
                                                            ToolTip="Megtekint">
                                                            <asp:HyperLink runat="server" ID="hplIktatoszam" NavigateUrl="" Text='<%#Eval("IktatoSzam_Merge") %>'>
                                                            </asp:HyperLink>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IktatasDatuma" HeaderText="Iktatva">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Iktato_Nev" HeaderText="Iktató">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                </asp:BoundField>                                                
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                    </tr>
                    </table>
     <uc2:FormFooter ID="FormFooter1" runat="server" />
    </asp:Panel>

</asp:Content>

