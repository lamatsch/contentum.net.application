﻿using System;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Text;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class AtiktatasiLanc : Contentum.eUtility.UI.PageBase
{
    private string IratId;
    private UI ui = new UI();

    protected void Page_Init(object sender, EventArgs e)
    {
        IratId = Request.QueryString.Get(QueryStringVars.IratId);

        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue=true;" +
"if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
+ "window.close();";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        if (!IsPostBack && !string.IsNullOrEmpty(IratId))
        {
            // Jogosultságellenőrzés az objektumra:
            CheckRights(IratId);

            LoadAtiktatasiLanc(IratId);
        }

        // HeaderText
        FormHeader1.FullManualHeaderTitle = Resources.Form.AtiktatasiLancHeaderTitle;
    }

    protected void IraIratokGridView_PreRender(object sender, EventArgs e)
    {
        #region Az eredeti sor megjelölése:

        for (int i = 0; i < IraIratokGridView.Rows.Count; i++)
        {
            string row_Id = IraIratokGridView.DataKeys[i].Value.ToString();

            if (row_Id == IratId)
            {
                // sor kiszínezése:
                IraIratokGridView.Rows[i].CssClass = "GridViewSelectedRowStyle";
                break;
            }

        }
        #endregion
    }

    private List<string> iratIdLista_jogosult = null;

    private void LoadAtiktatasiLanc(string IratId)
    {
        if (!string.IsNullOrEmpty(IratId))
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            Result result = service.GetAll_AtiktatasiLanc(UI.SetExecParamDefault(Page), IratId);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                MainPanel.Visible = false;
                return;
            }

            #region Jogosultság ellenőrzés a lekért iratokra (amihez nincs joga, nem lesz aláhúzva)

            // Id-lánc összeállítása
            StringBuilder sb_IratIds = new StringBuilder();
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string row_Id = row["Id"].ToString();

                if (sb_IratIds.Length == 0)
                {
                    sb_IratIds.Append("'" + row_Id + "'");
                }
                else
                {
                    sb_IratIds.Append(",'" + row_Id + "'");
                }
            }

            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
            // szűrés az id-kra:
            iratokSearch.Id.Value = sb_IratIds.ToString();
            iratokSearch.Id.Operator = Query.Operators.inner;

            // Lekérdezés jogosultságellenőrzéssel, csak azok maradnak, amihez van joga
            Result result_iratokGetAll = service.GetAllWithExtensionAndJogosultak(UI.SetExecParamDefault(Page), iratokSearch, true);
            if (result_iratokGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_iratokGetAll);
                return;
            }

            iratIdLista_jogosult = new List<string>();

            // Irat Id-k kigyűjtése, amihez van joga:
            foreach (DataRow row in result_iratokGetAll.Ds.Tables[0].Rows)
            {
                string row_Id = row["Id"].ToString();
                if (!iratIdLista_jogosult.Contains(row_Id))
                {
                    iratIdLista_jogosult.Add(row_Id);
                }
            }

            #endregion

            // GridViewBind eseményre feliratkozás:
            IraIratokGridView.RowDataBound += new System.Web.UI.WebControls.GridViewRowEventHandler(IraIratokGridView_RowDataBound);

            //BuildAtiktatasiLanc(result.Ds);
            ui.GridViewFill(IraIratokGridView, result, FormHeader1.ErrorPanel, null);

        }
    }

    private void IraIratokGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        // Ha az adott sorhoz nincs joga, akkor levesszük a linket az iktatószámról
        if (iratIdLista_jogosult == null) { return; }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string row_id = drv["Id"].ToString();

            // ha nincs a jogosult irat listában:
            if (iratIdLista_jogosult.Contains(row_id) == false)
            {
                // link levétele:
                Label labelIktatoszam = (Label) e.Row.FindControl("labelIktatoszam");
                if (labelIktatoszam != null)
                {
                    string iktatoSzam = drv["IktatoSzam_Merge"].ToString();
                    if (!String.IsNullOrEmpty(iktatoSzam))
                    {
                        // label alatt van egy hyperlink, ezt leszedjük, és beállítjuk a label szövegét, plusz az onclick-et töröljük, abban volt az ablak nyitás
                        labelIktatoszam.Controls.Clear();
                        labelIktatoszam.Text = iktatoSzam;
                        labelIktatoszam.Attributes["onclick"] = String.Empty;
                    }
                }
            }

        }
    }

//    private void BuildAtiktatasiLanc(System.Data.DataSet ds)
//    {
//        iratokSpan.Controls.Clear();

//        foreach (DataRow row in ds.Tables[0].Rows)
//        {
//            string row_HierarchyLevel = row["HierarchyLevel"].ToString();
//            string row_Id = row["Id"].ToString();
//            string row_IktatoSzam_Merge = row["IktatoSzam_Merge"].ToString();
//            string row_IktatasDatuma = row["IktatasDatuma"].ToString();
//            string row_Iktato_Nev = row["Iktato_Nev"].ToString();

//            // Új eFormPanel gombóc az iratnak:
//            Contentum.eUIControls.eFormPanel irateFormPanel = new Contentum.eUIControls.eFormPanel();
//            irateFormPanel.Style[HtmlTextWriterStyle.Width] = "10%";

//            HtmlGenericControl htmlControl = new HtmlGenericControl();
//            htmlControl.InnerHtml = @"<table>
//                        <tr><td style='text-decoration: underline;'>" + row_IktatoSzam_Merge.Replace(" ","&nbsp;") + @"</td></tr>//                        
//                    </table>";

//            irateFormPanel.Controls.Add(htmlControl);


//            iratokSpan.Controls.Add(irateFormPanel);
//        }        
//    }


    private void CheckRights(string IratId)
    {        
        char jogszint = 'O';
                
        EREC_IraIratokService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = IratId;

        Result result = service.GetWithRightCheck(execParam, jogszint);
        
        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            MainPanel.Visible = false;
            return;
        }
    }



    /// <summary>
    /// ASPX fájlból hívjuk
    /// </summary>
    /// <param name="oObj_Id">A megtekintendő objektum id-ja</param>
    /// <param name="oObj_type">A megtekintendő objektum típusa</param>
    /// <returns>Javascript kód</returns>
    protected string GetIratViewClientClick(string irat_Id)
    {
        string js = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&"
            + QueryStringVars.Id + "=" + irat_Id, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        return js;
    }


}
