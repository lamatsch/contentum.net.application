<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="AtvetelForm.aspx.cs" Inherits="AtvetelForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>


<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%--  CR3206 - Iratt�ri strukt�ra --%>
<%@ Register Src="eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>
<%--  CR3206 - Iratt�ri strukt�ra --%>
<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%-- CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa--%>
<%@ Register Src="eRecordComponent/IrattarStrukturaTerkep.ascx" TagName="IrattarStrukturaTerkep" TagPrefix="imdt" %>
<%@ Register Src="eRecordComponent/VonalkodTextBox.ascx" TagName="VonalkodTextBox" TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc16" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>
        
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:UpdatePanel runat="server" ID="FormHeader_UpdatePanel" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,AtvetelFormHeaderTitle %>" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <asp:Panel ID="MainPanel" runat="server">

            <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />

            <table cellpadding="0" cellspacing="0" width="90%">
                <tr>
                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                        <asp:Panel ID="KuldemenyekListPanel" runat="server" Visible="false">

                            <asp:Label ID="Label_Kuldemenyek" runat="server" Text="K�ldem�nyek:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                            <br />

                            <asp:GridView ID="KuldKuldemenyekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" OnRowDataBound="KuldKuldemenyekGridView_RowDataBound">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />

                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="EREC_IraIktatoKonyvek_MegkulJelzes" HeaderText="�rk.k�nyv"
                                        SortExpression="EREC_IraIktatoKonyvek_MegkulJelzes">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Erkezteto_Szam" HeaderText="�rk.sz�m" SortExpression="Erkezteto_Szam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREC_IraIktatoKonyvek_Ev" HeaderText="�v" SortExpression="EREC_IraIktatoKonyvek_Ev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="HivatkozasiSzam" HeaderText="Hivatkoz�si&nbsp;sz�m" SortExpression="HivatkozasiSzam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Bekuldo" HeaderText="K�ld�/felad� neve" SortExpression="NevSTR_Bekuldo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Csoport_Id_FelelosNev" HeaderText="Kezel�" SortExpression="Csoport_Id_FelelosNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="125px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BeerkezesIdeje" HeaderText="�rk.d�tuma" ItemStyle-HorizontalAlign="Center"
                                        SortExpression="ElsoAtvetIdeje">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllapotNev" HeaderText="�llapot" SortExpression="AllapotNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IktatniKellNev" HeaderText="Iktat�s" SortExpression="IktatniKellNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FelhasznaloCsoport_Id_OrzoNev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_OrzoNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Csoport_Id_CimzettNev" HeaderText="C�mzett" SortExpression="Csoport_Id_CimzettNev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Fizikai hely" Visible="false">
                                        <ItemTemplate>
                                            <%--<asp:DropDownList ID="ddlSorrend" runat="server" AutoPostBack="false"></asp:DropDownList>--%>
                                            <uc16:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>


                        <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">

                            <br />
                            <asp:Label ID="Label_Ugyiratok" runat="server" Text="�gyiratok:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                            <br />

                            <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                                AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />

                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Iratt�r" Visible="false">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label_IrattarTipus" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m"
                                        SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Fizikai hely" Visible="false">
                                        <ItemTemplate>
                                            <%--<asp:DropDownList ID="ddlSorrend" runat="server" AutoPostBack="false"></asp:DropDownList>--%>
                                            <uc16:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="IratPeldanyokListPanel" runat="server" Visible="false">

                            <br />
                            <asp:Label ID="Label_IratPeldanyok" runat="server" Text="Iratp�ld�nyok:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                            <br />
                            <asp:GridView ID="PldIratPeldanyokGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id"
                                OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktat�sz�m"
                                        SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="T�rgy" SortExpression="EREC_IraIratok_Targy">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="EREC_KuldKuldemenyek_PostazasIranya" HeaderText="Ir�ny" SortExpression="EREC_KuldKuldemenyek_PostazasIranya">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField> --%>
                                    <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Hat�rid�" SortExpression="VisszaerkezesiHatarido">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="C�mzett" SortExpression="NevSTR_Cimzett">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="KuldesMod_Nev" HeaderText="K�ld�sm�d" SortExpression="KuldesMod_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    </asp:BoundField>

                                    <asp:TemplateField HeaderText="Fizikai hely" Visible="false">
                                        <ItemTemplate>
                                            <%--<asp:DropDownList ID="ddlSorrend" runat="server" AutoPostBack="false"></asp:DropDownList>--%>
                                            <uc16:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </asp:Panel>

                        <asp:Panel ID="DosszieListPanel" runat="server" Visible="false">

                            <br />
                            <asp:Label ID="Label_Dossziek" runat="server" Text="Dosszi�k:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                            <br />
                            <asp:GridView ID="DossziekGridView" runat="server"
                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id"
                                OnRowDataBound="DossziekGridView_RowDataBound">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Nev" HeaderText="N�v"
                                        SortExpression="Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Barcode" HeaderText="Vonalk�d"
                                        SortExpression="Barcode">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Csoport_Id_Tulaj_Nev" HeaderText="Tulajdonos"
                                        SortExpression="Csoport_Id_Tulaj_Nev">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="400px" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>

                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                        <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="Panel_Warning_Kuldemeny" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Kuldemeny" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="Panel_Warning_Dosszie" runat="server" Visible="false">
                            <asp:Label ID="Label_Warning_Dosszie" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="Panel_Warning_NemAzonositottVonalkodok" runat="server" Visible="false"
                            HorizontalAlign="Center">
                            <br />
                            <asp:Label ID="labelNotIdentifiedHeader" runat="server" Text="<%$Resources:Form,UI_NemAzonositottVonalkodok%>"
                                Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                            <span style="position: relative; top: -10px; left: 38%;">
                                <asp:Label ID="Label_Warning_NemAzonositottVonalkodok" runat="server" Text="Label"
                                    CssClass="warningHeader"></asp:Label>
                            </span>
                        </asp:Panel>
                        <%--<asp:Panel ID="Panel_Warning_NemAzonositottVonalkodok" runat="server" Visible="false">    
                <asp:Label ID="Label_Warning_NemAzonositottVonalkodok" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                 <br />
                </asp:Panel>--%>
                    </td>
                </tr>
                <tr>
                    <%--  CR3206 - Iratt�ri strukt�ra --%>
                    <eUI:eFormPanel ID="Panel_IrattariStruktura" runat="server" Visible="false">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr class="urlapSor" runat="server">
                                <td style="text-align: left; vertical-align: top;" colspan="2">
                                    <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="Label7" runat="server" Text="Iratt�ri hely megad�sa: " CssClass="GridViewTitle"></asp:Label>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label1" runat="server" Text="Vonalk�d:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <%-- CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa--%>
                                    <asp:UpdatePanel runat="server" ID="Vonalkod_UpdatePanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <%--<uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDown" runat="server" />--%>
                                            <uc5:VonalkodTextBox ID="IrattariHely_VonalkodTextBox" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi" runat="server">
                                <td></td>
                                <td style="border: solid 1px Silver;">
                                    <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel1" runat="server" Visible="true">
                                                <ajaxToolkit:CollapsiblePanelExtender ID="MetaAdatokCPE" runat="server" TargetControlID="Panel1"
                                                    CollapsedSize="20" Collapsed="False"
                                                    AutoCollapse="false" AutoExpand="false"
                                                    ExpandedSize="150"
                                                    ScrollContents="true">
                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                <table style="width: 97%;">
                                                    <tr>
                                                        <td style="width: 100%;">
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td style="width: 100%; text-align: left;">
                                                                        <asp:Panel ID="Panel2" runat="server" Visible="true">
                                                                            <imdt:IrattarStrukturaTerkep ID="IrattarTerkep1" runat="server"
                                                                                OnSelectedNodeChanged="IrattarTerkep_SelectedNodeChanged" />
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <%--  CR3206 - Iratt�ri strukt�ra --%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <br />
                        <uc2:FormFooter ID="FormFooter1" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upResult">
            <ContentTemplate>
                <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
                    <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
                    <asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
                    <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
                    <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
                    <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                        <div class="mrResultPanelText">A kijel�lt t�telek �tv�tele sikeresen v�grehajt�dott.</div>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/images/hu/ovalgomb/atvetelilistanyomtatas.gif"
                                        onmouseover="swapByName(this.id,'atvetelilistanyomtatas2.gif')" onmouseout="swapByName(this.id,'atvetelilistanyomtatas.gif')"
                                        CommandName="Print" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                        onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                        CommandName="Close" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                </asp:Panel>
                <asp:Panel ID="ResultPanel_IrattarbaVetel" runat="server" Visible="false" Width="90%">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server" CssClass="mrResultPanel">
                        <div class="mrResultPanelText">
                            A kijel�lt t�telek iratt�rba v�tele sikeresen v�grehajt�dott.
                        </div>
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ImageClose_IrattarbaVetelResult" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                        onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                        CommandName="Close" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
