using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Linq;

public partial class AtvetelForm : Contentum.eUtility.UI.PageBase
{

    private string Command = "";
    private AtvetelTipus Mode = AtvetelTipus.NincsMegadva;
    private UI ui = new UI();
    //BLG1131 T�K visszav�tel
    private Boolean isTukVisszavetel = false;
    private Boolean isTukRendszer = false;

    private String UgyiratId = "";
    private String IratPeldanyId = "";
    private String KuldemenyId = ""; // id-k �sszef�zve
    private String DosszieId = "";
    private String Vonalkodok = ""; // vonalk�dok vessz�vel elv�lasztva

    private String[] KuldemenyekArray;
    private String[] UgyiratokArray;
    private String[] IratPeldanyokArray;
    private String[] DossziekArray;
    private String[] VonalkodokArray;

    private string nemAzonositottVonalkodok = "";

    private const string FunkcioKod_UgyiratAtvetel = "UgyiratAtvetel";
    private const string FunkcioKod_IratPeldanyAtvetel = "IratPeldanyAtvetel";
    private const string FunkcioKod_KuldemenyAtvetel = "KuldemenyAtvetel";
    private const String FunkcioKod_DosszieAtvetel = "MappakAtvetel";
    private const string FunkcioKod_VonalkodosAtvetel = "VonalkodosAtvetel";
    private const string FunkcioKod_VonalkodosIrattarbaVetel = "VonalkodosIrattarbaVetel";

    private Result result_atvetel = null;

    public string maxTetelszam = "0";

    //IsMember-hez kell
    private List<string> FelettesSzervezetList = null;

    private enum AtvetelTipus
    {
        NincsMegadva,
        UgyiratAtvetel,
        IratPeldanyAtvetel,
        KuldemenyAtvetel,
        DosszieAtvetel,
        VonalkodosAtvetel,
        VonalkodosIrattarbaVetel,
        #region CR3206 - Iratt�ri strukt�ra
        UgyiratIrattarAtvetel
        #endregion
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        //ebben az esetben a form T�K -b�l megh�vott visszav�telk�nt funkcion�l
        if (Rendszerparameterek.IsTUK(Page))
        {
            if (Session["TUKVisszavetel"] != null && "1".Equals(Session["TUKVisszavetel"].ToString()))
            {
                isTukVisszavetel = true;
            }
            isTukRendszer = true;
        }

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session["SelectedUgyiratIds"] != null)
                UgyiratId = Session["SelectedUgyiratIds"].ToString();
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratPeldanyId)))
        {
            if (Session["SelectedIratPeldanyIds"] != null)
                IratPeldanyId = Session["SelectedIratPeldanyIds"].ToString();
        }
        else IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.KuldemenyId)))
        {
            if (Session["SelectedKuldemenyIds"] != null)
                KuldemenyId = Session["SelectedKuldemenyIds"].ToString();
        }
        else KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);


        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.Vonalkodok)))
        {
            if (Session["SelectedBarcodeIds"] != null)
                Vonalkodok = Session["SelectedBarcodeIds"].ToString();
        }
        else Vonalkodok = Request.QueryString.Get(QueryStringVars.Vonalkodok);


        if (Session["SelectedDosszieIds"] != null)
        {
            DosszieId = Session["SelectedDosszieIds"].ToString();
        }


        if (Command == CommandName.UgyiratIrattarAtvetel && !String.IsNullOrEmpty(Vonalkodok))
        {
            // Vonalk�dos iratt�rba v�tel:
            Mode = AtvetelTipus.VonalkodosIrattarbaVetel;

            VonalkodokArray = Vonalkodok.Split(',');
        }
        else if (!String.IsNullOrEmpty(Vonalkodok))
        {
            // Vonalk�dos �tv�tel
            Mode = AtvetelTipus.VonalkodosAtvetel;

            VonalkodokArray = Vonalkodok.Split(',');
        }
        else if (!String.IsNullOrEmpty(IratPeldanyId))
        {
            // IratP�ld�ny �tv�tel
            Mode = AtvetelTipus.IratPeldanyAtvetel;

            IratPeldanyokArray = IratPeldanyId.Split(',');
        }
        else if (!String.IsNullOrEmpty(KuldemenyId))
        {
            // K�ldem�ny �tv�tel:
            Mode = AtvetelTipus.KuldemenyAtvetel;

            KuldemenyekArray = KuldemenyId.Split(',');
        }
        else if (!String.IsNullOrEmpty(UgyiratId))
        {
            #region  CR3206 - Iratt�ri strukt�ra
            if (Command == CommandName.UgyiratIrattarAtvetel)
            {
                Mode = AtvetelTipus.UgyiratIrattarAtvetel;
            }
            else
            {
                // �gyirat�tv�tel
                Mode = AtvetelTipus.UgyiratAtvetel;
            }
            #endregion
            UgyiratokArray = UgyiratId.Split(',');
        }
        else if (!String.IsNullOrEmpty(DosszieId))
        {
            Mode = AtvetelTipus.DosszieAtvetel;

            DossziekArray = DosszieId.Split(',');
        }

        else
        {

        }

        if (Rendszerparameterek.IsTUK(Page))
        {
            isTukRendszer = true;

            if (!IsPostBack)
            {
                if (Mode == AtvetelTipus.IratPeldanyAtvetel)
                {
                    SetFizikaiHelyekVisible(PldIratPeldanyokGridView);
                }
                else if (Mode == AtvetelTipus.UgyiratAtvetel)
                {
                    SetFizikaiHelyekVisible(UgyUgyiratokGridView);
                }
                else if (Mode == AtvetelTipus.KuldemenyAtvetel)
                {
                    SetFizikaiHelyekVisible(KuldKuldemenyekGridView);
                }

                ReloadFizikaiHelyek(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, null);
            }

        }

        // Funkci�jog-ellen�rz�s:
        if (Mode == AtvetelTipus.UgyiratIrattarAtvetel)
        {
            #region  CR3206 - Iratt�ri strukt�ra
            if (!FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtvetel") && !FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarAtvetel"))
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
            #endregion
        }
        else if (Mode == AtvetelTipus.VonalkodosIrattarbaVetel)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_VonalkodosIrattarbaVetel);
        }
        else if (Mode == AtvetelTipus.VonalkodosAtvetel)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_VonalkodosAtvetel);
        }
        else if (Mode == AtvetelTipus.UgyiratAtvetel)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratAtvetel);
        }
        else if (Mode == AtvetelTipus.KuldemenyAtvetel)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_KuldemenyAtvetel);
        }
        else if (Mode == AtvetelTipus.DosszieAtvetel)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_DosszieAtvetel);
        }
        else if (Mode == AtvetelTipus.NincsMegadva)
        {
            // nem siker�lt azonos�tani az �tv�tel m�dj�t, mert semmilyen adat nem �rkezett a sessionben
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UI_Popup_NoSessionData);
            return;
        }
        else
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratPeldanyAtvetel);
        }



        if (!IsPostBack)
        {
            LoadFormComponents();
        }

    }

    private void SetFizikaiHelyekVisible(GridView grid)
    {
        foreach (DataControlField column in grid.Columns)
        {
            if (column.HeaderText == "Fizikai hely")
            {
                column.Visible = true;
            }
        }
    }

    private void ReloadFizikaiHelyek(string id, string selectedValue)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode) || result_csoportok.Ds.Tables[0].Rows.Count < 1)
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }

                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    FizikaiHelyek = res;
                    //IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    //if (res.Ds.Tables[0].Rows.Count == 1)
                    //    IrattariHelyLevelekDropDownTUK.DropDownList.SelectedIndex = 1;
                    //else if (!string.IsNullOrEmpty(selectedValue))
                    //{
                    //    IrattariHelyLevelekDropDownTUK.SetSelectedValue(selectedValue);
                    //}

                }
            }
        }
    }

    private Result FizikaiHelyek;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        #region CR3206 - Iratt�ri strukt�ra
        // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
        // IrattariHelyLevelekDropDown.Validate = false;
        IrattariHely_VonalkodTextBox.Validate = false;
        #endregion
        if (Mode == AtvetelTipus.VonalkodosAtvetel)
        {
            FormHeader1.HeaderTitle = Resources.Form.VonalkodosAtvetelHeaderTitle;
        }
        else if (Mode == AtvetelTipus.VonalkodosIrattarbaVetel)
        {
            FormHeader1.HeaderTitle = Resources.Form.VonalkodosIrattarbaVetelHeaderTitle;

            // Iratt�r t�pus mez� megjelen�t�s:
            string headerText_irattar = "Iratt�r";
            foreach (DataControlField column in UgyUgyiratokGridView.Columns)
            {
                if (column.HeaderText == headerText_irattar)
                {
                    column.Visible = true;
                    break;
                }
            }
        }
        else if (Mode == AtvetelTipus.UgyiratIrattarAtvetel)
        {
            #region CR3206 - Iratt�ri strukt�ra
            FormHeader1.HeaderTitle = Resources.Form.UgyiratokIrattarbaVetelHeaderTitle;

            // Iratt�r t�pus mez� megjelen�t�s:
            string headerText_irattar = "Iratt�r";
            foreach (DataControlField column in UgyUgyiratokGridView.Columns)
            {
                if (column.HeaderText == headerText_irattar)
                {
                    column.Visible = true;
                    break;
                }
            }

            // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
            IrattariHely_VonalkodTextBox.TextBox.TextChanged += new EventHandler(IrattariHely_VonalkodTextBox_TextChanged);
            //IrattariHelyDropdownSor.Visible = true;

            #endregion
        }


        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Mode != AtvetelTipus.NincsMegadva)
        {
            ImageClose.OnClientClick = "window.returnValue=true;" +
            "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
       + "window.close();";
            ImageClose_IrattarbaVetelResult.OnClientClick = "window.returnValue=true;" +
            "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
       + "window.close();";

            FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

            labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count +
                ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count +
                ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null).Count +
                ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null).Count).ToString();
        }

        #region BLG1131 Visszav�tel
        if (isTukVisszavetel)
        {
            FormHeader1.HeaderTitle = Resources.Form.VisszavetelHeaderTitle;
        }
        #endregion

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Mode == AtvetelTipus.NincsMegadva)
        {
            FormFooter1.ImageButton_Save.Visible = false;
        }
    }

    // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {

        string javaS =
                "var ctrl = document.getElementById('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "'); " +
                "if(ctrl) { " +
                "document.getElementById('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "').onblur = function () " +
                "{if(document.getElementById('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "').value!='') __doPostBack('" + IrattariHely_VonalkodTextBox.TextBox.ClientID + "', '');};"
                + "};";
        ScriptManager.RegisterStartupScript(IrattariHely_VonalkodTextBox.TextBox, this.GetType(), "sendPostBack", javaS, true);

    }

    private void LoadFormComponents()
    {
        if (Mode == AtvetelTipus.VonalkodosAtvetel)
        {
            // Vonalk�dok alapj�n �gyirat, p�ld�ny, k�ldem�ny t�mb�k felt�lt�se:

            try
            {
                //Felel�s ellen�rz�shez kell
                FelettesSzervezetList = Contentum.eUtility.Csoportok.GetFelhasznaloFelettesCsoportjai(Page, FelhasznaloProfil.FelhasznaloId(Page));

                VonalkodElemzes();

                if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
                {
                    Label_Warning_NemAzonositottVonalkodok.Text = nemAzonositottVonalkodok;
                    Panel_Warning_NemAzonositottVonalkodok.Visible = true;
                }

                if (UgyiratokArray != null && UgyiratokArray.Length > 0)
                {
                    UgyiratokListPanel.Visible = true;
                    Label_Ugyiratok.Visible = true;

                    FillUgyiratokGridView();
                }

                if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
                {
                    IratPeldanyokListPanel.Visible = true;
                    Label_IratPeldanyok.Visible = true;

                    FillIratPeldanyGridView();
                }

                if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
                {
                    KuldemenyekListPanel.Visible = true;
                    Label_Kuldemenyek.Visible = true;

                    FillKuldemenyGridView();
                }

                if (DossziekArray != null && DossziekArray.Length > 0)
                {
                    DosszieListPanel.Visible = true;
                    Label_Dossziek.Visible = true;

                    FillDossziekGridView();
                }

            }
            catch (Contentum.eUtility.ResultException e)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
            }

        }
        else if (Mode == AtvetelTipus.VonalkodosIrattarbaVetel)
        {
            try
            {
                VonalkodElemzes(true, false, false, false);


                if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
                {
                    Label_Warning_NemAzonositottVonalkodok.Text = nemAzonositottVonalkodok;
                    Panel_Warning_NemAzonositottVonalkodok.Visible = true;
                }

                if (UgyiratokArray != null && UgyiratokArray.Length > 0)
                {
                    UgyiratokListPanel.Visible = true;
                    Label_Ugyiratok.Visible = true;

                    FillUgyiratokGridView();
                }

                //if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
                //{
                //    IratPeldanyokListPanel.Visible = true;
                //    Label_IratPeldanyok.Visible = true;

                //    FillIratPeldanyGridView();
                //}

                //if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
                //{
                //    KuldemenyekListPanel.Visible = true;
                //    Label_Kuldemenyek.Visible = true;

                //    FillKuldemenyGridView();
                //}
            }
            catch (Contentum.eUtility.ResultException e)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
            }
        }
        else if (Mode == AtvetelTipus.UgyiratIrattarAtvetel)
        {
            #region  CR3206 - Iratt�ri strukt�ra
            try
            {
                if (UgyiratokArray != null && UgyiratokArray.Length > 0)
                {
                    UgyiratokListPanel.Visible = true;
                    Label_Ugyiratok.Visible = true;

                    FillUgyiratokGridView();
                }
                #region CR3206 - Iratt�ri strukt�ra
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
                List<string> irattarIds = new List<string>();
                Label labelIrattarTipus = (Label)UgyUgyiratokGridView.Rows[0].FindControl("Label_IrattarTipus");
                if (labelIrattarTipus.Text == "K�zponti")
                {
                    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page)).Obj_Id;
                    irattarIds.Add(kozpontiIrattarId);
                }
                else
                {
                    Contentum.eAdmin.Service.KRT_CsoportokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                    Result result = service.GetAllIrattar(ExecParam);

                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string Id = row["Id"].ToString();
                        if (!String.IsNullOrEmpty(Id) && !irattarIds.Contains(Id))
                        {
                            irattarIds.Add(Id);
                        }
                    }

                    //saj�t szervezet�nek iratt�ri felyei is l�tsz�djanak
                    string felhasznaloSzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);

                    if (!irattarIds.Contains(felhasznaloSzervezetId))
                    {
                        irattarIds.Add(felhasznaloSzervezetId);
                    }
                }

                EREC_IrattariHelyekService serviceIrattariHelyek = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
                EREC_IrattariHelyekSearch IrattariHelyekSearch = new EREC_IrattariHelyekSearch();

                IrattariHelyekSearch.Felelos_Csoport_Id.Value = Contentum.eRecord.BaseUtility.Search.GetSqlInnerString(irattarIds.ToArray());
                IrattariHelyekSearch.Felelos_Csoport_Id.Operator = Query.Operators.inner;
                IrattarTerkep1.IrattariHelyekSearch = IrattariHelyekSearch;
                IrattarTerkep1.EErrorPanel1 = FormHeader1.ErrorPanel;
                Panel_IrattariStruktura.Visible = true;
                #endregion
            }
            catch (Contentum.eUtility.ResultException e)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
            }
            #endregion
        }

        else
        {
            FelettesSzervezetList = Contentum.eUtility.Csoportok.GetFelhasznaloFelettesCsoportjai(Page, FelhasznaloProfil.FelhasznaloId(Page));

            if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
            {
                KuldemenyekListPanel.Visible = true;
                Label_Kuldemenyek.Visible = true;

                FillKuldemenyGridView();
            }

            if (UgyiratokArray != null && UgyiratokArray.Length > 0)
            {
                UgyiratokListPanel.Visible = true;
                Label_Ugyiratok.Visible = true;

                FillUgyiratokGridView();
            }

            if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
            {
                IratPeldanyokListPanel.Visible = true;
                Label_IratPeldanyok.Visible = true;

                FillIratPeldanyGridView();
            }

            if (DossziekArray != null && DossziekArray.Length > 0)
            {
                DosszieListPanel.Visible = true;
                Label_Dossziek.Visible = true;

                FillDossziekGridView();
            }
        }
        //else if (Mode == AtadasTipus.UgyiratAtadas)
        //{
        //    #region UgyiratAtadas
        //    if (String.IsNullOrEmpty(UgyiratId))
        //    {
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //        MainPanel.Visible = false;
        //    }
        //    else
        //    {
        //        UgyiratokListPanel.Visible = true;
        //        KuldemenyekListPanel.Visible = false;
        //        IratPeldanyokListPanel.Visible = false;

        //        FillUgyiratokGridView();

        //    }
        //    #endregion
        //}
        //else if (Mode == AtadasTipus.IratPeldanyAtadas)
        //{
        //    #region IratPeldanyAtadas
        //    if (String.IsNullOrEmpty(IratPeldanyId))
        //    {
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //        MainPanel.Visible = false;
        //    }
        //    else
        //    {
        //        UgyiratokListPanel.Visible = false;
        //        KuldemenyekListPanel.Visible = false;
        //        IratPeldanyokListPanel.Visible = true;

        //        FillIratPeldanyGridView();

        //    }
        //    #endregion
        //}
        //else if (Mode == AtadasTipus.KuldemenyAtadas)
        //{
        //    #region KuldemenyAtadas
        //    if (String.IsNullOrEmpty(KuldemenyId))
        //    {
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //        MainPanel.Visible = false;
        //    }
        //    else
        //    {
        //        UgyiratokListPanel.Visible = false;
        //        KuldemenyekListPanel.Visible = true;
        //        IratPeldanyokListPanel.Visible = false;

        //        FillKuldemenyGridView();

        //    }
        //    #endregion
        //}


    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_NEMatvehetok = UgyiratokArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_AtvetelreKijeloltekWarning, count_NEMatvehetok);
            //Label_Warning_Ugyirat.Visible = true;
            Panel_Warning_Ugyirat.Visible = true;
        }
    }


    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check");

        int count_NEMatvehetok = IratPeldanyokArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_IratPeldany.Text = String.Format(Resources.List.UI_IratPeldany_AtvetelreKijeloltekWarning, count_NEMatvehetok);
            //Label_Warning_IratPeldany.Visible = true;
            Panel_Warning_IratPeldany.Visible = true;
        }
    }


    private void FillKuldemenyGridView()
    {
        DataSet ds = KuldKuldemenyekGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != KuldemenyekArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(KuldKuldemenyekGridView, "check");

        int count_NEMatvehetok = KuldemenyekArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_Kuldemeny.Text = String.Format(Resources.List.UI_KuldAtvetelreKijeloltekWarning, count_NEMatvehetok);
            //Label_Warning_Kuldemeny.Visible = true;
            Panel_Warning_Kuldemeny.Visible = true;
        }
    }

    private void FillDossziekGridView()
    {
        DataSet ds = DossziekGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != DossziekArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(DossziekGridView, "check");

        int count_NEMatvehetok = DossziekArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_Dosszie.Text = String.Format(Resources.List.UI_DosszieAtvetelreKijeloltekWarning, count_NEMatvehetok);
            //Label_Warning_Dosszie.Visible = true;
            Panel_Warning_Dosszie.Visible = true;
        }
    }


    /// <summary>
    /// Vonalk�dok alapj�n objektumok beazonos�t�sa
    /// </summary>
    private void VonalkodElemzes()
    {
        VonalkodElemzes(true, true, true, true);
    }

    private void VonalkodElemzes(bool includeUgyirat, bool includeIratPeldany, bool includeKuldemeny, bool includeDosszie)
    {
        nemAzonositottVonalkodok = "";

        if (VonalkodokArray != null && VonalkodokArray.Length > 0)
        {
            KRT_BarkodokService service = eRecordService.ServiceFactory.GetKRT_BarkodokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_BarkodokSearch search = new KRT_BarkodokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < VonalkodokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Kod.Value = "'" + VonalkodokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Kod.Value += ",'" + VonalkodokArray[i] + "'";
            //    }
            //}
            search.Kod.Value = Search.GetSqlInnerString(VonalkodokArray);
            search.Kod.Operator = Query.Operators.inner;

            Result res = service.GetAll(execParam, search);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
            }
            else
            {
                if (res.Ds == null) { return; }

                List<String> ugyiratokList = new List<string>();
                List<String> kuldemenyekList = new List<string>();
                List<String> iratPeldanyList = new List<string>();
                List<String> dosszieList = new List<string>();

                List<String> hasznaltVonalkodok = new List<string>();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    string Kod = row["Kod"].ToString();
                    string Obj_Id = row["Obj_Id"].ToString();
                    string Obj_type = row["Obj_type"].ToString();

                    switch (Obj_type)
                    {
                        case Constants.TableNames.EREC_UgyUgyiratok:
                            if (includeUgyirat)
                            {
                                ugyiratokList.Add(Obj_Id);
                                hasznaltVonalkodok.Add(Kod);
                            }
                            break;
                        case Constants.TableNames.EREC_KuldKuldemenyek:
                            if (includeKuldemeny)
                            {
                                kuldemenyekList.Add(Obj_Id);
                                hasznaltVonalkodok.Add(Kod);
                            }
                            break;
                        case Constants.TableNames.EREC_PldIratPeldanyok:
                            if (includeIratPeldany)
                            {
                                iratPeldanyList.Add(Obj_Id);
                                hasznaltVonalkodok.Add(Kod);
                            }
                            break;
                        case Constants.TableNames.KRT_Mappak:
                            if (includeDosszie)
                            {
                                dosszieList.Add(Obj_Id);
                                hasznaltVonalkodok.Add(Kod);
                            }
                            break;

                    }
                }

                // A nem azonositott vonalkodok kigy�jt�se
                foreach (string vonalkod in VonalkodokArray)
                {
                    if (!hasznaltVonalkodok.Contains(vonalkod))
                    {
                        //if (String.IsNullOrEmpty(nemAzonositottVonalkodok))
                        //{
                        //    nemAzonositottVonalkodok = "'" + vonalkod + "'";
                        //}
                        //else
                        //{
                        //    nemAzonositottVonalkodok += ",'" + vonalkod + "'";
                        //}

                        nemAzonositottVonalkodok += "<li class=\"notIdentifiedText\">" + vonalkod + "</li>";
                    }
                }

                if (!String.IsNullOrEmpty(nemAzonositottVonalkodok))
                {
                    nemAzonositottVonalkodok = "<ul>" + nemAzonositottVonalkodok + "</ul>";
                }

                // List�kb�l t�mb�k felt�lt�se:
                UgyiratokArray = ugyiratokList.ToArray();
                KuldemenyekArray = kuldemenyekList.ToArray();
                IratPeldanyokArray = iratPeldanyList.ToArray();
                DossziekArray = dosszieList.ToArray();
            }
        }
    }



    protected DataSet KuldKuldemenyekGridViewBind()
    {
        if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < KuldemenyekArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + KuldemenyekArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + KuldemenyekArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(KuldemenyekArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(KuldKuldemenyekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void KuldKuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        //Kuldemenyek.KuldemenyekGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);
        if (isTukVisszavetel)
        {
            //BLG 1131
            Kuldemenyek.KuldemenyekGridView_RowDataBound_CheckVisszavehetoTUK(e, Page);
        }
        else
        {
            Kuldemenyek.KuldemenyekGridView_RowDataBound_CheckAtveheto(e, Page, FelettesSzervezetList);
        }


        if (isTukRendszer && e.Row.RowType == DataControlRowType.DataRow && FizikaiHelyek != null)
        {
            eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)e.Row.FindControl("IrattariHelyLevelekDropDownTUK");
            ddp.FillDropDownList(FizikaiHelyek, "Id", "Ertek", "1", true, FormHeader1.ErrorPanel);

            var row = ((DataRowView)e.Row.DataItem);
            string fizikaiHely = row["IrattarId"] == DBNull.Value ? null : row["IrattarId"].ToString();

            if (!string.IsNullOrEmpty(fizikaiHely))
            {
                ddp.SetSelectedValue(fizikaiHely);
            }
        }
    }



    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < UgyiratokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + UgyiratokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + UgyiratokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            if (Mode == AtvetelTipus.VonalkodosIrattarbaVetel)
            {
                search.OrderBy = "Csoportok_FelelosNev.Nev";
            }

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (isTukVisszavetel)
        {
            //BLG1131
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckVisszavehetoTUK(e, Page);
        }
        else
        {
            if (Mode == AtvetelTipus.VonalkodosIrattarbaVetel)
            {
                // Iratt�rba v�teln�l m�sfajta ellen�rz�s:
                Ugyiratok.UgyiratokGridView_RowDataBound_CheckIrattarbaVetel(e, Page);

                #region Iratt�r t�pus mez� felt�lt�s:

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

                    string Csoport_Id_Felelos = String.Empty;
                    if (drv["Csoport_Id_Felelos"] != null)
                    {
                        Csoport_Id_Felelos = drv["Csoport_Id_Felelos"].ToString();
                    }

                    // K�zponti vagy �tmeneti iratt�r?
                    bool kozpontiIrattar = false;
                    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page)).Obj_Id;

                    if (Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                    {
                        kozpontiIrattar = true;
                    }

                    // Label lek�r�se:
                    Label labelIrattarTipus = (Label)e.Row.FindControl("Label_IrattarTipus");
                    if (labelIrattarTipus != null)
                    {
                        if (kozpontiIrattar)
                        {
                            labelIrattarTipus.Text = "K�zponti";
                        }
                        else
                        {
                            labelIrattarTipus.Text = "�tmeneti";
                        }
                    }

                }

                #endregion
            }
            else if (Mode == AtvetelTipus.UgyiratIrattarAtvetel)
            {
                #region CR3206 - Iratt�ri strukt�ra
                // Iratt�rba v�teln�l m�sfajta ellen�rz�s:
                Ugyiratok.UgyiratokGridView_RowDataBound_CheckIrattarbaVetel(e, Page);

                #region Iratt�r t�pus mez� felt�lt�s:

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

                    string Csoport_Id_Felelos = String.Empty;
                    if (drv["Csoport_Id_Felelos"] != null)
                    {
                        Csoport_Id_Felelos = drv["Csoport_Id_Felelos"].ToString();
                    }

                    // K�zponti vagy �tmeneti iratt�r?
                    bool kozpontiIrattar = false;
                    string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page)).Obj_Id;

                    if (Csoport_Id_Felelos.ToLower() == kozpontiIrattarId.ToLower())
                    {
                        kozpontiIrattar = true;
                    }

                    // Label lek�r�se:
                    Label labelIrattarTipus = (Label)e.Row.FindControl("Label_IrattarTipus");
                    if (labelIrattarTipus != null)
                    {
                        if (kozpontiIrattar)
                        {
                            labelIrattarTipus.Text = "K�zponti";
                        }
                        else
                        {
                            labelIrattarTipus.Text = "�tmeneti";
                        }
                    }
                }
                #endregion
                #endregion CR3206 - Iratt�ri strukt�ra
            }
            else
            {
                Ugyiratok.UgyiratokGridView_RowDataBound_CheckAtvetel(e, Page, FelettesSzervezetList);
            }
        }



        if (isTukRendszer && e.Row.RowType == DataControlRowType.DataRow && FizikaiHelyek != null)
        {
            eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)e.Row.FindControl("IrattariHelyLevelekDropDownTUK");
            ddp.FillDropDownList(FizikaiHelyek, "Id", "Ertek", "1", true, FormHeader1.ErrorPanel);

            var row = ((DataRowView)e.Row.DataItem);
            string fizikaiHely = row["IrattarId"] == DBNull.Value ? null : row["IrattarId"].ToString();

            if (!string.IsNullOrEmpty(fizikaiHely))
            {
                ddp.SetSelectedValue(fizikaiHely);
            }
        }
    }



    protected DataSet DossziekGridViewBind()
    {
        if (DossziekArray != null && DossziekArray.Length > 0)
        {
            KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_MappakSearch search = new KRT_MappakSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < DossziekArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + DossziekArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + DossziekArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(DossziekArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(DossziekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void DossziekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Dossziek.DossziekGridView_RowDataBound_CheckAtvetel(e, Page);
    }



    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            //for (int i = 0; i < IratPeldanyokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + IratPeldanyokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + IratPeldanyokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(IratPeldanyokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        if (isTukVisszavetel)
        {
            IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckTUKVisszavetel(e, Page);
        }
        else
        {
            //IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);
            IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckAtvetel(e, Page, FelettesSzervezetList);
        }



        if (isTukRendszer && e.Row.RowType == DataControlRowType.DataRow && FizikaiHelyek != null)
        {
            eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)e.Row.FindControl("IrattariHelyLevelekDropDownTUK");
            ddp.FillDropDownList(FizikaiHelyek, "Id", "Ertek", "1", true, FormHeader1.ErrorPanel);

            var row = ((DataRowView)e.Row.DataItem);
            string fizikaiHely = row["IrattarId"] == DBNull.Value ? null : row["IrattarId"].ToString();

            if (!string.IsNullOrEmpty(fizikaiHely))
            {
                ddp.SetSelectedValue(fizikaiHely);
            }
        }

    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (Mode == AtvetelTipus.NincsMegadva)
            {
                // nem siker�lt azonos�tani az �tv�tel m�dj�t, mert lej�rt a session
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UI_Popup_SessionTimeOut);
                return;
            }
            //T�K visszav�tel eset�n csak kezelo, irathely �ll�t�s
            if (isTukVisszavetel)
            {
                if (Mode == AtvetelTipus.UgyiratAtvetel)
                {
                    if (String.IsNullOrEmpty(UgyiratId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                        if (selectedItemsList.Count == 0)
                        {
                            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                            return;
                        }

                        foreach (string SelectedUgyiratId in selectedItemsList)
                        {
                            string version = String.Empty;

                            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            execParam.Record_Id = SelectedUgyiratId;
                            Result result_Search = service.Get(execParam);
                            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)result_Search.Record;

                            //verzi� ment�se, k�l�nben hib�t ad
                            version = ugyirat.Base.Ver;

                            ugyirat.Updated.SetValueAll(false);
                            ugyirat.Base.Updated.SetValueAll(false);

                            if (isTukRendszer)
                            {
                                for (int i = 0; i < UgyUgyiratokGridView.Rows.Count; i++)
                                {
                                    CheckBox cb = (CheckBox)UgyUgyiratokGridView.Rows[i].FindControl("check");
                                    if (cb.Text.ToLower().Trim() == SelectedUgyiratId.ToLower().Trim())
                                    {
                                        eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)UgyUgyiratokGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                        if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                        {
                                            string fizikaiHelyId = ddp.SelectedValue;
                                            string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                            ugyirat.IrattarId = fizikaiHelyId;
                                            ugyirat.Updated.IrattarId = true;
                                            ugyirat.IrattariHely = fizikaiHely;
                                            ugyirat.Updated.IrattariHely = true;
                                        }
                                    }
                                }
                            }

                            ugyirat.FelhasznaloCsoport_Id_Orzo = execParam.Felhasznalo_Id;
                            ugyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

                            ugyirat.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
                            ugyirat.Updated.Csoport_Id_Felelos = true;

                            ugyirat.Base.Ver = version;
                            ugyirat.Base.Updated.Ver = true;

                            ExecParam execParam_UgyiratUpdate = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_UgyiratUpdate.Record_Id = ugyirat.Id;

                            EREC_UgyUgyiratokService serviceUgyiratUpdate = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                            Result result_UgyiratlUpdate = serviceUgyiratUpdate.Update(execParam_UgyiratUpdate, ugyirat);

                            if (!string.IsNullOrEmpty(result_UgyiratlUpdate.ErrorMessage))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ResultError.GetErrorMessageByErrorCode(Convert.ToInt32(result_UgyiratlUpdate.ErrorMessage)));
                                return;
                            }
                        }
                    }
                }
                else if (Mode == AtvetelTipus.IratPeldanyAtvetel)
                {
                    if (String.IsNullOrEmpty(IratPeldanyId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);

                        if (selectedItemsList.Count == 0)
                        {
                            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                            return;
                        }

                        foreach (string SelectedIratPeldanyId in selectedItemsList)
                        {
                            string version = String.Empty;

                            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            execParam.Record_Id = SelectedIratPeldanyId;
                            Result result_Search = service.Get(execParam);
                            EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)result_Search.Record;

                            //verzi� ment�se, k�l�nben hib�t ad
                            version = iratPeldany.Base.Ver;

                            iratPeldany.Updated.SetValueAll(false);
                            iratPeldany.Base.Updated.SetValueAll(false);

                            if (isTukRendszer)
                            {
                                for (int i = 0; i < PldIratPeldanyokGridView.Rows.Count; i++)
                                {
                                    CheckBox cb = (CheckBox)PldIratPeldanyokGridView.Rows[i].FindControl("check");
                                    if (cb.Text.ToLower().Trim() == SelectedIratPeldanyId.ToLower().Trim())
                                    {
                                        eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)PldIratPeldanyokGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                        if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                        {
                                            string fizikaiHelyId = ddp.SelectedValue;
                                            string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                            iratPeldany.IrattarId = fizikaiHelyId;
                                            iratPeldany.Updated.IrattarId = true;
                                            iratPeldany.IrattariHely = fizikaiHely;
                                            iratPeldany.Updated.IrattariHely = true;
                                        }
                                    }
                                }
                            }

                            iratPeldany.FelhasznaloCsoport_Id_Orzo = execParam.Felhasznalo_Id;
                            iratPeldany.Updated.FelhasznaloCsoport_Id_Orzo = true;

                            iratPeldany.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
                            iratPeldany.Updated.Csoport_Id_Felelos = true;

                            iratPeldany.Base.Ver = version;
                            iratPeldany.Base.Updated.Ver = true;

                            ExecParam execParam_UgyiratUpdate = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_UgyiratUpdate.Record_Id = iratPeldany.Id;

                            EREC_PldIratPeldanyokService serviceIratPeldanyUpdate = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            Result result_IratpeldanyUpdate = serviceIratPeldanyUpdate.Update(execParam_UgyiratUpdate, iratPeldany);

                            if (!string.IsNullOrEmpty(result_IratpeldanyUpdate.ErrorMessage))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ResultError.GetErrorMessageByErrorCode(Convert.ToInt32(result_IratpeldanyUpdate.ErrorMessage)));
                                return;
                            }
                        }
                    }
                }
                else if (Mode == AtvetelTipus.KuldemenyAtvetel)
                {
                    if (String.IsNullOrEmpty(KuldemenyId))
                    {
                        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        return;
                    }
                    else
                    {
                        List<string> selectedItemsList = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null);

                        if (selectedItemsList.Count == 0)
                        {
                            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                            return;
                        }

                        foreach (string SelectedKuldemenyId in selectedItemsList)
                        {
                            string version = String.Empty;

                            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            execParam.Record_Id = SelectedKuldemenyId;
                            Result result_Search = service.Get(execParam);
                            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)result_Search.Record;

                            //verzi� ment�se, k�l�nben hib�t ad
                            version = kuldemeny.Base.Ver;

                            kuldemeny.Updated.SetValueAll(false);
                            kuldemeny.Base.Updated.SetValueAll(false);

                            if (isTukRendszer)
                            {
                                for (int i = 0; i < KuldKuldemenyekGridView.Rows.Count; i++)
                                {
                                    CheckBox cb = (CheckBox)KuldKuldemenyekGridView.Rows[i].FindControl("check");
                                    if (cb.Text.ToLower().Trim() == SelectedKuldemenyId.ToLower().Trim())
                                    {
                                        eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)KuldKuldemenyekGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                        if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                        {
                                            string fizikaiHelyId = ddp.SelectedValue;
                                            string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                            kuldemeny.IrattarId = fizikaiHelyId;
                                            kuldemeny.Updated.IrattarId = true;
                                            kuldemeny.IrattariHely = fizikaiHely;
                                            kuldemeny.Updated.IrattariHely = true;
                                        }
                                    }
                                }
                            }

                            kuldemeny.FelhasznaloCsoport_Id_Orzo = execParam.Felhasznalo_Id;
                            kuldemeny.Updated.FelhasznaloCsoport_Id_Orzo = true;

                            kuldemeny.Csoport_Id_Felelos = execParam.Felhasznalo_Id;
                            kuldemeny.Updated.Csoport_Id_Felelos = true;

                            kuldemeny.Base.Ver = version;
                            kuldemeny.Base.Updated.Ver = true;

                            ExecParam execParam_UgyiratUpdate = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam_UgyiratUpdate.Record_Id = kuldemeny.Id;

                            EREC_KuldKuldemenyekService serviceKuldemenyUpdate = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            Result result_KuldemenyUpdate = serviceKuldemenyUpdate.Update(execParam_UgyiratUpdate, kuldemeny);

                            if (!string.IsNullOrEmpty(result_KuldemenyUpdate.ErrorMessage))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ResultError.GetErrorMessageByErrorCode(Convert.ToInt32(result_KuldemenyUpdate.ErrorMessage)));
                                return;
                            }
                        }
                    }
                }
                //visszat�r�skor TUKVisszavetelt t�r�lj�k, nehogy k�s�bb gondot okozzon
                Session["TUKVisszavetel"] = null;
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
            else
            {


                if (
                    (Mode == AtvetelTipus.UgyiratAtvetel
                     && FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel))
                    || (Mode == AtvetelTipus.IratPeldanyAtvetel
                        && FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtvetel))
                    || (Mode == AtvetelTipus.KuldemenyAtvetel
                        && FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtvetel))
                    ||
                    (Mode == AtvetelTipus.VonalkodosAtvetel
                        && FunctionRights.GetFunkcioJog(Page, FunkcioKod_VonalkodosAtvetel))
                    ||
                    (Mode == AtvetelTipus.VonalkodosIrattarbaVetel
                        && FunctionRights.GetFunkcioJog(Page, FunkcioKod_VonalkodosIrattarbaVetel))
                    ||
                    (Mode == AtvetelTipus.DosszieAtvetel
                        && FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtvetel))
                    || // CR3206 - Iratt�ri strukt�ra
                    (Mode == AtvetelTipus.UgyiratIrattarAtvetel//if (!FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtvetel") && !FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarAtvetel"))
                        )
                    )
                {
                    if (Mode == AtvetelTipus.VonalkodosAtvetel)
                    {
                        #region VonalkodosAtvetel

                        KRT_BarkodokService service = eRecordService.ServiceFactory.GetKRT_BarkodokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
                        //String[] UgyiratIds = new String[selectedItemsList_ugyirat.Count];
                        //for (int i = 0; i < UgyiratIds.Length; i++)
                        //{
                        //    UgyiratIds[i] = selectedItemsList_ugyirat[i];
                        //}
                        String[] UgyiratIds = selectedItemsList_ugyirat.ToArray();

                        List<string> selectedItemsList_pld = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);
                        //String[] IratPeldanyIds = new String[selectedItemsList_pld.Count];
                        //for (int i = 0; i < IratPeldanyIds.Length; i++)
                        //{
                        //    IratPeldanyIds[i] = selectedItemsList_pld[i];
                        //}
                        String[] IratPeldanyIds = selectedItemsList_pld.ToArray();

                        List<string> selectedItemsList_kuld = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null);
                        //String[] KuldemenyIds = new String[selectedItemsList_kuld.Count];
                        //for (int i = 0; i < KuldemenyIds.Length; i++)
                        //{
                        //    KuldemenyIds[i] = selectedItemsList_kuld[i];
                        //}
                        String[] KuldemenyIds = selectedItemsList_kuld.ToArray();

                        List<string> selectedItemsList_dosszie = ui.GetGridViewSelectedRows(DossziekGridView, FormHeader1.ErrorPanel, null);
                        //String[] DosszieIds = new String[selectedItemsList_dosszie.Count];
                        //for (int i = 0; i < DosszieIds.Length; i++)
                        //{
                        //    DosszieIds[i] = selectedItemsList_dosszie[i];
                        //}
                        String[] DosszieIds = selectedItemsList_dosszie.ToArray();

                        if (selectedItemsList_ugyirat.Count + selectedItemsList_pld.Count + selectedItemsList_kuld.Count + selectedItemsList_dosszie.Count == 0)
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                            return;
                        }

                        // TODO:

                        result_atvetel = service.Atvetel_Tomeges(execParam, UgyiratIds, KuldemenyIds, IratPeldanyIds, DosszieIds);


                        if (String.IsNullOrEmpty(result_atvetel.ErrorCode))
                        {
                            //JavaScripts.RegisterCloseWindowClientScript(Page);
                            MainPanel.Visible = false;
                            ResultPanel.Visible = true;
                            ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result_atvetel.Uid);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_atvetel);
                            UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result_atvetel);
                            UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result_atvetel);
                            UI.MarkFailedRecordsInGridView(KuldKuldemenyekGridView, result_atvetel);
                            UI.MarkFailedRecordsInGridView(DossziekGridView, result_atvetel);
                        }

                        #endregion
                    }
                    else if (Mode == AtvetelTipus.VonalkodosIrattarbaVetel)
                    {
                        #region VonalkodosIrattarbaVetel

                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                        if (selectedItemsList_ugyirat.Count == 0)
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                            return;
                        }

                        // �gyiratok �sszef�z�se ( , -vel elv�lasztva)
                        //StringBuilder sb_UgyiratIds = new StringBuilder();

                        //foreach (string ugyiratId in selectedItemsList_ugyirat)
                        //{
                        //    if (sb_UgyiratIds.Length == 0)
                        //    {
                        //        sb_UgyiratIds.Append(ugyiratId);
                        //    }
                        //    else
                        //    {
                        //        sb_UgyiratIds.Append("," + ugyiratId);
                        //    }
                        //}

                        //result_atvetel = service.AtvetelIrattarba_Tomeges(execParam, sb_UgyiratIds.ToString());
                        string strUgyiratIds = String.Join(",", selectedItemsList_ugyirat.ToArray());
                        result_atvetel = service.AtvetelIrattarba_Tomeges(execParam, strUgyiratIds);

                        if (!result_atvetel.IsError)
                        {
                            //JavaScripts.RegisterCloseWindowClientScript(Page);
                            MainPanel.Visible = false;
                            ResultPanel_IrattarbaVetel.Visible = true;
                            //ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result_atvetel.Uid);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_atvetel);
                        }

                        #endregion
                    }
                    else if (Mode == AtvetelTipus.UgyiratIrattarAtvetel)
                    {
                        #region CR3206 - Iratt�ri strukt�ra

                        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                        if (selectedItemsList_ugyirat.Count == 0)
                        {
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                            return;
                        }

                        // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
                        // BUG_6107
                        if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTARI_HELY_KOTELEZO, true))
                        {
                            if ((IrattarTerkep1.TreeView.SelectedNode.ChildNodes.Count > 0) || (IrattarTerkep1.TreeView.SelectedNode.Parent == null))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, ResultError.GetErrorMessageByErrorCode(52355));
                                return;
                            }
                        }
                        string strUgyiratIds = String.Join(",", selectedItemsList_ugyirat.ToArray());
                        result_atvetel = service.AtvetelIrattarba_Tomeges(execParam, strUgyiratIds);

                        if (!result_atvetel.IsError)
                        {
                            //JavaScripts.RegisterCloseWindowClientScript(Page);
                            MainPanel.Visible = false;
                            // CR3246 Iratt�ri Helyek kezel�s�nek m�dos�t�sa
                            // CR kapcs�n: sikeres m�veletn�l ErrorPanel ne jelenjen meg..
                            FormHeader1.ErrorPanel.Visible = false;
                            ResultPanel_IrattarbaVetel.Visible = true;
                            //ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result_atvetel.Uid);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_atvetel);
                        }
                        // CR3246
                        //if (!string.IsNullOrEmpty(IrattariHelyLevelekDropDown.SelectedValue))
                        if (!string.IsNullOrEmpty(IrattarTerkep1.TreeView.SelectedValue))
                        {
                            using (EREC_IrattariHelyekService IrattarService = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
                            {
                                Result Res_IrattariHely = IrattarService.IrattarRendezes(execParam.Clone(), strUgyiratIds, IrattarTerkep1.TreeView.SelectedValue, IrattarTerkep1.TreeView.SelectedNode.Text);
                                if (!String.IsNullOrEmpty(Res_IrattariHely.ErrorCode))
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Res_IrattariHely);
                                }
                            }
                        }
                        #endregion
                    }
                    else if (Mode == AtvetelTipus.UgyiratAtvetel)
                    {
                        if (String.IsNullOrEmpty(UgyiratId))
                        {
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            return;
                        }
                        else
                        {
                            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                            if (selectedItemsList.Count == 0)
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                                return;
                            }

                            //String[] UgyiratIds = new String[selectedItemsList.Count];
                            //for (int i = 0; i < UgyiratIds.Length; i++)
                            //{
                            //    UgyiratIds[i] = selectedItemsList[i];
                            //}
                            String[] UgyiratIds = selectedItemsList.ToArray();

                            Result result = service.Atvetel_Tomeges(execParam.Clone(), UgyiratIds, execParam.Felhasznalo_Id);
                            if (!string.IsNullOrEmpty(result.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                            }
                            else
                            {
                                if (isTukRendszer)
                                {
                                    foreach (string SelectedUgyiratId in selectedItemsList)
                                    {
                                        for (int i = 0; i < UgyUgyiratokGridView.Rows.Count; i++)
                                        {
                                            CheckBox cb = (CheckBox)UgyUgyiratokGridView.Rows[i].FindControl("check");
                                            if (cb.Text.ToLower().Trim() == SelectedUgyiratId.ToLower().Trim())
                                            {
                                                eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)UgyUgyiratokGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                                if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                                {
                                                    string fizikaiHelyId = ddp.SelectedValue;
                                                    string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                                    ExecParam execParamAtvet = UI.SetExecParamDefault(Page, new ExecParam());
                                                    execParamAtvet.Record_Id = SelectedUgyiratId;
                                                    Result ugyiratResult = service.Get(execParamAtvet);

                                                    if (!ugyiratResult.IsError)
                                                    {
                                                        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratResult.Record;
                                                        ugyirat.Updated.SetValueAll(false);
                                                        ugyirat.Base.Updated.SetValueAll(false);
                                                        ugyirat.Base.Updated.Ver = true;
                                                        ugyirat.IrattarId = fizikaiHelyId;
                                                        ugyirat.Updated.IrattarId = true;
                                                        ugyirat.IrattariHely = fizikaiHely;
                                                        ugyirat.Updated.IrattariHely = true;
                                                        service.Update(execParamAtvet, ugyirat);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                MainPanel.Visible = false;
                                ResultPanel.Visible = true;
                                ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result.Uid);
                            }
                        }
                    }
                    else if (Mode == AtvetelTipus.IratPeldanyAtvetel)
                    {
                        if (String.IsNullOrEmpty(IratPeldanyId))
                        {
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            return;
                        }
                        else
                        {
                            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            List<string> selectedItemsList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);

                            if (selectedItemsList.Count == 0)
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                                return;
                            }

                            //String[] IratpeldanyIds = new String[selectedItemsList.Count];
                            //for (int i = 0; i < IratpeldanyIds.Length; i++)
                            //{
                            //    IratpeldanyIds[i] = selectedItemsList[i];
                            //}
                            String[] IratpeldanyIds = selectedItemsList.ToArray();

                            Result result = service.Atvetel_Tomeges(execParam.Clone(), IratpeldanyIds, execParam.Felhasznalo_Id);
                            if (!string.IsNullOrEmpty(result.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result);
                            }
                            else
                            {
                                if (isTukRendszer)
                                {
                                    foreach (string SelectedIratPeldanyId in selectedItemsList)
                                    {
                                        for (int i = 0; i < PldIratPeldanyokGridView.Rows.Count; i++)
                                        {
                                            CheckBox cb = (CheckBox)PldIratPeldanyokGridView.Rows[i].FindControl("check");
                                            if (cb.Text.ToLower().Trim() == SelectedIratPeldanyId.ToLower().Trim())
                                            {
                                                eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)PldIratPeldanyokGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                                if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                                {
                                                    string fizikaiHelyId = ddp.SelectedValue;
                                                    string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                                    ExecParam execParamAtvet = UI.SetExecParamDefault(Page, new ExecParam());
                                                    execParamAtvet.Record_Id = SelectedIratPeldanyId;
                                                    Result iratPeldanyResult = service.Get(execParamAtvet);

                                                    if (!iratPeldanyResult.IsError)
                                                    {
                                                        EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)iratPeldanyResult.Record;
                                                        iratPeldany.Updated.SetValueAll(false);
                                                        iratPeldany.Base.Updated.SetValueAll(false);
                                                        iratPeldany.Base.Updated.Ver = true;
                                                        iratPeldany.IrattarId = fizikaiHelyId;
                                                        iratPeldany.Updated.IrattarId = true;
                                                        iratPeldany.IrattariHely = fizikaiHely;
                                                        iratPeldany.Updated.IrattariHely = true;
                                                        service.Update(execParamAtvet, iratPeldany);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                MainPanel.Visible = false;
                                ResultPanel.Visible = true;
                                ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result.Uid);
                            }
                        }
                    }
                    else if (Mode == AtvetelTipus.KuldemenyAtvetel)
                    {
                        if (String.IsNullOrEmpty(KuldemenyId))
                        {
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            return;
                        }
                        else
                        {
                            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            List<string> selectedItemsList = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null);

                            if (selectedItemsList.Count == 0)
                            {
                                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                                return;
                            }

                            //String[] KuldemenyIds = new String[selectedItemsList.Count];
                            //for (int i = 0; i < KuldemenyIds.Length; i++)
                            //{
                            //    KuldemenyIds[i] = selectedItemsList[i];
                            //}
                            String[] KuldemenyIds = selectedItemsList.ToArray();

                            Result result = service.Atvetel_Tomeges(execParam.Clone(), KuldemenyIds, execParam.Felhasznalo_Id);
                            if (!string.IsNullOrEmpty(result.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                UI.MarkFailedRecordsInGridView(KuldKuldemenyekGridView, result);
                            }
                            else
                            {
                                if (isTukRendszer)
                                {
                                    foreach (string SelectedKuldemenyId in selectedItemsList)
                                    {
                                        for (int i = 0; i < KuldKuldemenyekGridView.Rows.Count; i++)
                                        {
                                            CheckBox cb = (CheckBox)KuldKuldemenyekGridView.Rows[i].FindControl("check");
                                            if (cb.Text.ToLower().Trim() == SelectedKuldemenyId.ToLower().Trim())
                                            {
                                                eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)KuldKuldemenyekGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                                if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                                {
                                                    string fizikaiHelyId = ddp.SelectedValue;
                                                    string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                                    ExecParam execParamAtvet = UI.SetExecParamDefault(Page, new ExecParam());
                                                    execParamAtvet.Record_Id = SelectedKuldemenyId;
                                                    Result kuldemenyResult = service.Get(execParamAtvet);

                                                    if (!kuldemenyResult.IsError)
                                                    {
                                                        EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)kuldemenyResult.Record;
                                                        kuldemeny.Updated.SetValueAll(false);
                                                        kuldemeny.Base.Updated.SetValueAll(false);
                                                        kuldemeny.Base.Updated.Ver = true;
                                                        kuldemeny.IrattarId = fizikaiHelyId;
                                                        kuldemeny.Updated.IrattarId = true;
                                                        kuldemeny.IrattariHely = fizikaiHely;
                                                        kuldemeny.Updated.IrattariHely = true;
                                                        service.Update(execParamAtvet, kuldemeny);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                MainPanel.Visible = false;
                                ResultPanel.Visible = true;
                                ImagePrint.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("AtadoAtvevoListaPrintForm.aspx?fejId=" + result.Uid);
                            }
                        }
                    }

                }
                else
                {
                    UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                }
            }

        }
    }

    protected void IrattariHely_VonalkodTextBox_TextChanged(object sender, EventArgs e)
    {
        ClearErrorPanel();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IrattariHelyekService serviceIrattariHelyek = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService();
        if (!String.IsNullOrEmpty(IrattariHely_VonalkodTextBox.Text))
        {
            Result result = serviceIrattariHelyek.GetByVonalkod(ExecParam, IrattariHely_VonalkodTextBox.Text);
            EREC_IrattariHelyek irattariHely = result.Record as EREC_IrattariHelyek;
            if (irattariHely == null)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, ResultError.GetErrorMessageByErrorCode(52356));

                IrattarTerkep1.TreeView.Nodes[0].Selected = true;
            }
            else
            {
                string selectedId = irattariHely.Id;
                TreeNode node = IrattarTerkep1.FindByValue(selectedId);
                if (node == null)
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, ResultError.GetErrorMessageByErrorCode(52356));
                    IrattarTerkep1.TreeView.Nodes[0].Selected = true;

                }
                else
                {
                    node.Selected = true;
                    IrattarTerkep1.ExpandAllParents(node);
                    //    IrattarTerkep1.TreeView.SelectedNode.Value = selectedId;

                }
            }
            TreeViewUpdatePanel.Update();
            FormHeader_UpdatePanel.Update();
        }
    }
    protected void IrattarTerkep_SelectedNodeChanged(object sender, EventArgs e)
    {
        ClearErrorPanel();

        IrattariHely_VonalkodTextBox.Text = "";
        Vonalkod_UpdatePanel.Update();
    }

    private void ClearErrorPanel()
    {
        if (FormHeader1.ErrorPanel.Visible == true)
        {
            FormHeader1.ErrorPanel.Visible = false;
            FormHeader1.ErrorPanel.IsWarning = false;
            FormHeader_UpdatePanel.Update();
        }
    }

}
