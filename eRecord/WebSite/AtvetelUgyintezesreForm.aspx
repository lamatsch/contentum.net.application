<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="AtvetelUgyintezesreForm.aspx.cs" Inherits="AtvetelUgyintezesreForm" Title="Untitled Page" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>

<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc3" %>

<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,AtvetelFormHeaderTitle %>" />
     <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    
                                <uc10:UgyiratMasterAdatok id="UgyiratMasterAdatok1" runat="server">
                                </uc10:UgyiratMasterAdatok>
                        &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUserNev" runat="server" Text="K�vetkez� �gyint�z�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:FelhasznaloCsoportTextBox ID="FelhCsopId_Ugyintezo_kovetkezo_FelhasznaloCsoportTextBox" runat="server" ReadOnly="true"
                                    ViewMode="true" />
                            </td>
                        </tr>--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="�gyirat k�vetkez� helye:"></asp:Label>
                                &nbsp;
                            </td>
                            <td class="mrUrlapMezo"><uc5:FelhasznaloCsoportTextBox ID="FelhCsopId_Orzo_Kovetkezo_FelhasznaloCsoportTextBox" runat="server" ReadOnly="true"
                                    ViewMode="true" />
                            </td>
                        </tr>
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="label2" runat="server" Text="�tv�tel d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:ReadOnlyTextBox ID="ReadOnlyTextBox1" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="label7" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="3">
                                <asp:TextBox ID="TextBox2" runat="server" Width="700px"></asp:TextBox></td>
                        </tr>  --%>                                   
                    </table>
                    </eUI:eFormPanel>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>            
        </table>
     </asp:Panel>
</asp:Content>

