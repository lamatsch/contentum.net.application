using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;


public partial class AtvetelUgyintezesreForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private String UgyiratId = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_UgyiratAtvetelUgyintezesre = "UgyiratAtvetelUgyintezesre";

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratAtvetelUgyintezesre);
                break;
        }
        
        UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
        
        if (!IsPostBack)
        {
            LoadFormComponents();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadFormComponents()
    {
        if (String.IsNullOrEmpty(UgyiratId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            UgyiratMasterAdatok1.UgyiratId = UgyiratId;
            EREC_UgyUgyiratok erec_UgyUgyiratok =
                UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

            if (erec_UgyUgyiratok == null)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                    ResultError.CreateNewResultWithErrorCode(50101));
                MainPanel.Visible = false;
                return;
            }
            else
            {
                ExecParam execParam = UI.SetExecParamDefault(Page);
                ErrorDetails errorDetail = null;

                // Ellen�rz�s, �tvehet�-e �gyint�z�sre?:
                Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
                execParam.Record_Id = UgyiratId;
                if (Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam, out errorDetail) == false)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                        ResultError.CreateNewResultWithErrorCode(52161, errorDetail));
                    MainPanel.Visible = false;
                    return;
                }
                
                String kovUgyintezoEsOrzo_Id = FelhasznaloProfil.GetFelhasznaloCsoport(execParam);

                //FelhCsopId_Ugyintezo_kovetkezo_FelhasznaloCsoportTextBox.Id_HiddenField = kovUgyintezoEsOrzo_Id;
                //FelhCsopId_Ugyintezo_kovetkezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

                FelhCsopId_Orzo_Kovetkezo_FelhasznaloCsoportTextBox.Id_HiddenField = kovUgyintezoEsOrzo_Id;
                FelhCsopId_Orzo_Kovetkezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }
        }               
    }

    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetelUgyintezesre))
            {
                if (String.IsNullOrEmpty(UgyiratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {                

                    // �tv�tel �gyint�z�sre

                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                   
                    Result result = service.AtvetelUgyintezesre(execParam, UgyiratId);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, UgyiratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }

                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel,null);
            }

        }
    }

  

}
