using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Linq;

public partial class AtvetelUgyintezesreTomegesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private string Command = "";
    private String UgyiratId = "";
    private String[] UgyiratokArray;

    private const string FunkcioKod_UgyiratAtvetel = "UgyiratAtvetelUgyintezesre";

    public string maxTetelszam = "0";
    private Boolean isTukRendszer = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        this.FormHeader1.HeaderTitle = "�tv�tel �gyint�z�sre";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        if (Rendszerparameterek.IsTUK(Page))
        {
            isTukRendszer = true;

            SetFizikaiHelyekVisible(UgyUgyiratokGridView);
            ReloadFizikaiHelyek(UI.SetExecParamDefault(Page, new ExecParam()).Felhasznalo_Id, null);
        }

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session["SelectedUgyiratIds"] != null)
                UgyiratId = Session["SelectedUgyiratIds"].ToString();
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (!String.IsNullOrEmpty(UgyiratId))
        {
            // �gyirat�tv�tel      
            UgyiratokArray = UgyiratId.Split(',');
        }

        // Funkci�jog-ellen�rz�s:               
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratAtvetel);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }

    }

    private void ReloadFizikaiHelyek(string id, string selectedValue)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode) || result_csoportok.Ds.Tables[0].Rows.Count < 1)
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }

                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    FizikaiHelyek = res;
                    //IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    //if (res.Ds.Tables[0].Rows.Count == 1)
                    //    IrattariHelyLevelekDropDownTUK.DropDownList.SelectedIndex = 1;
                    //else if (!string.IsNullOrEmpty(selectedValue))
                    //{
                    //    IrattariHelyLevelekDropDownTUK.SetSelectedValue(selectedValue);
                    //}

                }
            }
        }
    }

    private Result FizikaiHelyek;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        // ImageClose.OnClientClick = "window.returnValue=true;window.opener.__doPostBack(window.opener.postBackControl, window.opener.postBackArgument); window.close(); return false;";
         ImageClose.OnClientClick = JavaScripts.GetOnCloseClientClickScript();

        ImageClose_IrattarbaVetelResult.OnClientClick = "window.returnValue=true; window.__doPostBack(window.postBackControl, window.postBackArgument);window.close(); return false;";

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; }";
       
        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    private void LoadFormComponents()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UgyiratokListPanel.Visible = true;
            Label_Ugyiratok.Visible = true;

            FillUgyiratokGridView();
        }
    }

    private void SetFizikaiHelyekVisible(GridView grid)
    {
        foreach (DataControlField column in grid.Columns)
        {
            if (column.HeaderText == "Fizikai hely")
            {
                column.Visible = true;
            }
        }
    }

    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_NEMatvehetok = UgyiratokArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_AtvetelreKijeloltekWarning, count_NEMatvehetok);
            //Label_Warning_Ugyirat.Visible = true;
            Panel_Warning_Ugyirat.Visible = true;
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();

            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        Ugyiratok.UgyiratokGridView_RowDataBound_CheckAtvetelUgyintezesre(e, Page);



        if (isTukRendszer && e.Row.RowType == DataControlRowType.DataRow && FizikaiHelyek != null)
        {
            eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)e.Row.FindControl("IrattariHelyLevelekDropDownTUK");
            ddp.FillDropDownList(FizikaiHelyek, "Id", "Ertek", "1", true, FormHeader1.ErrorPanel);

            var row = ((DataRowView)e.Row.DataItem);
            string fizikaiHely = row["IrattarId"] == DBNull.Value ? null : row["IrattarId"].ToString();

            if (!string.IsNullOrEmpty(fizikaiHely))
            {
                ddp.SetSelectedValue(fizikaiHely);
            }
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel))
            {
                if (String.IsNullOrEmpty(UgyiratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }
                    String[] UgyiratIds = selectedItemsList.ToArray();

                    Result result = service.AtvetelUgyintezesre_Tomeges(execParam.Clone(), UgyiratIds);
                    if (!string.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                    }
                    else
                    {
                        #region T�K eset�ben fizikai helyeket is kezelni kell �gyirat �s irat�ld�ny szinten
                        if (isTukRendszer)
                        {
                            foreach (string SelectedUgyiratId in selectedItemsList)
                            {
                                for (int i = 0; i < UgyUgyiratokGridView.Rows.Count; i++)
                                {
                                    CheckBox cb = (CheckBox)UgyUgyiratokGridView.Rows[i].FindControl("check");
                                    if (cb.Text.ToLower().Trim() == SelectedUgyiratId.ToLower().Trim())
                                    {
                                        eRecordComponent_IrattariHelyLevelekDropDown ddp = (eRecordComponent_IrattariHelyLevelekDropDown)UgyUgyiratokGridView.Rows[i].FindControl("IrattariHelyLevelekDropDownTUK");

                                        if (!string.IsNullOrEmpty(ddp.SelectedValue))
                                        {
                                            #region �gyiratok update
                                            string fizikaiHelyId = ddp.SelectedValue;
                                            string fizikaiHely = ddp.DropDownList.SelectedItem.Text;

                                            ExecParam execParamAtvet = UI.SetExecParamDefault(Page, new ExecParam());
                                            execParamAtvet.Record_Id = SelectedUgyiratId;
                                            Result ugyiratResult = service.Get(execParamAtvet);

                                            string originalIrattarId = null;

                                            if (!ugyiratResult.IsError)
                                            {
                                                EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)ugyiratResult.Record;
                                                originalIrattarId = ugyirat.IrattarId;
                                                ugyirat.Updated.SetValueAll(false);
                                                ugyirat.Base.Updated.SetValueAll(false);
                                                ugyirat.Base.Updated.Ver = true;
                                                ugyirat.IrattarId = fizikaiHelyId;
                                                ugyirat.Updated.IrattarId = true;
                                                ugyirat.IrattariHely = fizikaiHely;
                                                ugyirat.Updated.IrattariHely = true;
                                                service.Update(execParamAtvet, ugyirat);
                                            }
                                            #endregion

                                            #region �gyiratokhoz tartoz� p�ld�nyok update
                                            EREC_PldIratPeldanyokService peldanyService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                                            EREC_UgyUgyiratokSearch ugyiratokSearch = new EREC_UgyUgyiratokSearch();
                                            ugyiratokSearch.Id.Value = SelectedUgyiratId;
                                            ugyiratokSearch.Id.Operator = Query.Operators.equals;
                                            EREC_PldIratPeldanyokSearch peldanyokSearch = new EREC_PldIratPeldanyokSearch(true);
                                            //peldanyokSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Value = KodTarak.POSTAZAS_IRANYA.Bejovo;
                                            //peldanyokSearch.Extended_EREC_IraIratokSearch.PostazasIranya.Operator = Query.Operators.equals;
                                            //iratSearch.Extended_EREC_IraIratokSearch.Alszam.Value=
                                            Result peldanyResult = peldanyService.GetAllByUgyirat(execParam, ugyiratokSearch, peldanyokSearch, null);
                                            if (peldanyResult.IsError)
                                            {
                                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, peldanyResult);
                                                return;
                                            }
                                            else if (peldanyResult.Ds.Tables[0].Rows.Count > 0)
                                            {
                                                List<string> modositandoPeldanyok = new List<string>();
                                                foreach (DataRow row in peldanyResult.Ds.Tables[0].Rows)
                                                {
                                                    String alszam = "";
                                                    string postazasIranya = "";
                                                    if (row["Irat_Alszam"] != DBNull.Value && row["Irat_PostazasIranya"] != DBNull.Value)
                                                    {
                                                        alszam = row["Irat_Alszam"].ToString();
                                                        postazasIranya = row["Irat_PostazasIranya"].ToString();

                                                        if (alszam == "1" && postazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                                                        {
                                                            modositandoPeldanyok.Add(row["Id"].ToString());
                                                            continue;
                                                        }
                                                    }

                                                    if (row["IrattarId"] != DBNull.Value)
                                                    {
                                                        string iid = row["IrattarId"].ToString();

                                                        if (!string.IsNullOrEmpty(originalIrattarId) && iid.ToLower() == originalIrattarId.ToLower())
                                                        {
                                                            modositandoPeldanyok.Add(row["Id"].ToString());
                                                        }
                                                    }
                                                }

                                                if (modositandoPeldanyok.Count > 0)
                                                {
                                                    EREC_PldIratPeldanyokSearch modositandoPeldanyokSearch = new EREC_PldIratPeldanyokSearch(false);
                                                    modositandoPeldanyokSearch.Id.Value = Search.GetSqlInnerString(modositandoPeldanyok.Distinct().ToArray());
                                                    modositandoPeldanyokSearch.Id.Operator = Query.Operators.inner;

                                                    Result modositandoPeldanyResult = peldanyService.GetAll(execParam, modositandoPeldanyokSearch);
                                                    if (modositandoPeldanyResult.IsError)
                                                    {
                                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, modositandoPeldanyResult);
                                                        return;
                                                    }

                                                    foreach (DataRow modositandoPeldanyRow in modositandoPeldanyResult.Ds.Tables[0].Rows)
                                                    {
                                                        EREC_PldIratPeldanyok peldany = new EREC_PldIratPeldanyok();
                                                        Contentum.eRecord.Utility.Util.LoadBusinessDocumentFromDataRow(peldany, modositandoPeldanyRow);
                                                        ExecParam execParamPeldany = UI.SetExecParamDefault(Page, new ExecParam());
                                                        execParamPeldany.Record_Id = peldany.Id;
                                                        peldany.Base.Updated.SetValueAll(false);
                                                        peldany.Updated.SetValueAll(false);
                                                        peldany.Base.Updated.Ver = true;
                                                        peldany.IrattarId = fizikaiHelyId;
                                                        peldany.Updated.IrattarId = true;
                                                        peldany.IrattariHely = fizikaiHely;
                                                        peldany.Updated.IrattariHely = true;
                                                        peldanyService.Update(execParamPeldany, peldany);
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                        ImagePrint.Visible = false;
                    }
                }

            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }

    }
}


