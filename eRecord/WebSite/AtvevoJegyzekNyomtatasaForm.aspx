﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true" CodeFile="AtvevoJegyzekNyomtatasaForm.aspx.cs" Inherits="AtvevojegyzekNyomtatasaForm" %>


<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc2" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>



    <asp:Panel ID="MainPanel" runat="server" Visible="true" Width="100%">

        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="AdminDataForm" runat="server">
                        <uc4:CustomUpdateProgress ID="CustomUpdateProgress2" runat="server" />

                        <uc1:FormHeader ID="FormHeader1" runat="server" />
                        <table cellpadding="0" cellspacing="0" border="1" width="90%">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <br />
                                    <asp:Panel ID="IraKezbesitesiTetelekPanel" runat="server">
                                        <asp:Label ID="Label_Grid" runat="server" Text="Kézbesítési tételek:" Visible="false" CssClass="GridViewTitle"></asp:Label>
                                        <br />
                                        <asp:GridView ID="IraKezbesitesiTetelekGridView" runat="server"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id"
                                            OnRowDataBound="IraKezbesitesiTetelekGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                            Visible="false" OnClientClick="return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Atado_Nev" HeaderText="Átadó" SortExpression="Atado_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField DataField="Cimzett_Nev" HeaderText="Címzett" SortExpression="Cimzett_Nev" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="Azonosító" SortExpression="Azonosito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelAzonosito" runat="server" Text='<%#                                        
                                                                string.Concat(                                                                                                        
                                                                  (Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok ? "<a href=\"UgyUgyiratokList.aspx?Id="+ Eval("Obj_Id") as string + "\" style=\"text-decoration:underline\">" : ""
                                                                 ,(Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok ? "<a href=\"PldIratPeldanyokList.aspx?Id="+ Eval("Obj_Id") as string + "\" style=\"text-decoration:underline\">" : ""
                                                                 ,(Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek ? "<a href=\"KuldKuldemenyekList.aspx?Id="+ Eval("Obj_Id") as string + "\" style=\"text-decoration:underline\">" : ""
                                                                 , Eval("Azonosito")
                                                                 , ((Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok
                                                                   || (Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok
                                                                   || (Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek) ? "<a />" : "") 
                                                                 %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Barkod" HeaderText="Vonalkód" SortExpression="Barkod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="110px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Típus" SortExpression="Obj_type">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelTipus" runat="server" Text='<%#Eval("Obj_type")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- Rejtett információk --%>
                                                <asp:BoundField DataField="Obj_type">
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Obj_Id">
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField DataField="Megjegyzes" HeaderText="Tárgy" SortExpression="Targy"  >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>  --%>
                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Átir.&nbsp;dátuma" SortExpression="LetrehozasIdo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--                                                <asp:BoundField DataField="Atvevo_Nev" HeaderText="Átvevő" SortExpression="Atvevo_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelAllapot" runat="server" Text='<%#Eval("Allapot")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Állapot" SortExpression="Allapot">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%#Eval("Allapot")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <table cellpadding="0" cellspacing="0" runat="server" id="MainTable">
                            <tr>
                                <td>
                                    <uc3:FormFooter ID="FormFooter1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
