﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using System.Data;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class AtvevojegyzekNyomtatasaForm : Contentum.eUtility.UI.PageBase
{
    #region properties
    String selectedItemIds;
    String[] selectedItemsArray;
    bool isTuk = false;
    public string maxTetelszam = "0";
    private UI ui = new UI();

    public const string kuldemeny = "Küldemény";
    public const string iratpeldany = "Iratpéldány";
    public const string ugyirat = "Ügyirat";
    public const string dosszie = "Dosszié";
    #endregion

    #region Oldal betöltése
    protected void Page_Load(object sender, EventArgs e)
    {
        //csak TÜK kezelőhasználhatja a funkciót!
        isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);

        //sessionben átadott adatok kinyerése
        selectedItemIds = Session["selectedIraKezbesitesiTetelekIds"] == null ? String.Empty : Session["selectedIraKezbesitesiTetelekIds"].ToString();
        if (!String.IsNullOrEmpty(selectedItemIds))
        {
            selectedItemsArray = selectedItemIds.Split(',');
        }

        if (isTuk)
        {
            FormFooter1.Command = CommandName.Nyomtat;
        }
        else
        {
            FormFooter1.Command = CommandName.Cancel;
        }

        FormFooter1.Visible = true;
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (!IsPostBack)
        {
            FillGridView();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FormFooter1.Command = CommandName.Modify;
        FormHeader1.DisableModeLabel = true;
        FormHeader1.HeaderTitle = Resources.Form.IratMozgasAdministralasHeaderTitle;
        IraKezbesitesiTetelekGridView.Visible = true;
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        int cntKijelolt_Lezaras = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, FormHeader1.ErrorPanel, null).Count;

        labelTetelekSzamaDb.Text = cntKijelolt_Lezaras.ToString();
    }
    #endregion

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

        if (e.CommandName == CommandName.SaveAndPrint)
        {
            //csak TÜK
            if (isTuk)
            {
                List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, FormHeader1.ErrorPanel, null);
                String[] tetelIds = new String[selectedItemsList.Count];

                for (int i = 0; i < tetelIds.Length; i++)
                {
                    tetelIds[i] = selectedItemsList[i];
                    //}

                    //Result result = null;
                    if (tetelIds.Length > 0)
                    {
                        //TODO - result meghatározása
                        EREC_IraKezbesitesiTetelekService tetel_service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam.Record_Id = tetelIds[i];

                        Result tetelResult = tetel_service.Get(execParam);

                        if (String.IsNullOrEmpty(tetelResult.ErrorCode))
                        {
                            EREC_IraKezbesitesiTetelek erec_KezbesitesiTetelek = (EREC_IraKezbesitesiTetelek)tetelResult.Record;

                            if (!string.IsNullOrEmpty(erec_KezbesitesiTetelek.AtveteliFej_Id))
                            {
                                //string js = "javascript:window.open('AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + erec_KezbesitesiTetelek.AtveteliFej_Id + "')";
                                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AtadoAtvevoListaNyomtatas", js, true);
                                //Session["fejId"] = erec_KezbesitesiTetelek.AtveteliFej_Id;
                                string js = JavaScripts.SetOnClientClickWithTimeout("AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + erec_KezbesitesiTetelek.AtveteliFej_Id, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, null, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AtadoAtvevoJegyzekNyomtatas", js, true);
                            }
                            else
                            {
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, tetelResult);
                        }

                        /*if (result != null && !result.IsError)
                        {
                            string js = "javascript:window.open('AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + result.Uid + "')";
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyExpedialas", js, true);
                        }
                        else
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            UI.MarkFailedRecordsInGridView(IraKezbesitesiTetelekGridView, result);
                        }*/
                    }
                }
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_NotTUK, "");
            }
        }
    }

    #region gridView
    private void FillGridView()
    {
        try
        {
            DataSet ds = GridViewBind();

            // ellenőrzés:
            if (ds != null && ds.Tables[0].Rows.Count != selectedItemsArray.Length)
            {
                // nem annyi rekord jött, mint amennyit vártunk:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            }
        }
        catch (Contentum.eUtility.ResultException e)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
        }
    }

    protected DataSet GridViewBind()
    {
        if (selectedItemsArray != null && selectedItemsArray.Length > 0)
        {
            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            for (int i = 0; i < selectedItemsArray.Length; i++)
            {
                if (i == 0)
                {
                    search.Id.Value = "'" + selectedItemsArray[i] + "'";
                }
                else
                {
                    search.Id.Value += ",'" + selectedItemsArray[i] + "'";
                }
            }
            search.Id.Value = Search.GetSqlInnerString(selectedItemsArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(IraKezbesitesiTetelekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void IraKezbesitesiTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Típus kiírása
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelTipus = (Label)e.Row.FindControl("labelTipus");
            if (labelTipus != null && labelTipus.Text.Trim() != String.Empty)
            {
                switch (labelTipus.Text.Trim())
                {
                    case Constants.TableNames.EREC_KuldKuldemenyek:
                        labelTipus.Text = kuldemeny;
                        break;
                    case Constants.TableNames.EREC_PldIratPeldanyok:
                        labelTipus.Text = iratpeldany;
                        break;
                    case Constants.TableNames.EREC_UgyUgyiratok:
                        labelTipus.Text = ugyirat;
                        break;
                    case Constants.TableNames.KRT_Mappak:
                        labelTipus.Text = dosszie;
                        break;
                }
            }
        }

        //GridView_RowDataBound_SetVegyesInfo(e);
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        //IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);
        IraKezbesitesiTetelek.GridView_RowDataBound_CheckAtvett(e, Page);
        
        //BaseEREC_IraKezbesitesiTetelek.

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");

        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");
        UI.SetFeladatInfo(e, GetObjType(e), GetObjId(e));

        GridView_RowDataBound_SetKezbesitesiTetelAllapot(e);

    }

    private void GridView_RowDataBound_SetKezbesitesiTetelAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;


            // megjelöljük a kézbesítési tételekk sorait
            bool isKezbesitesiTetel = false;
            isKezbesitesiTetel = (drw.Row.Table.Columns.Contains("Id") && !String.IsNullOrEmpty(drw["Id"].ToString()));

            if (isKezbesitesiTetel)
            {
                string allapotnev = GetKezbesitesiTetelAllapot(e);
                Label lbAllapotNev = (Label)e.Row.FindControl("labelAllapotNev");
                lbAllapotNev.Text = allapotnev;
            }
        }
    }

    private string GetKezbesitesiTetelAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string allapot = null;
                string allapot_nev = null;
                if (drw.Row.Table.Columns.Contains("Allapot"))
                {
                    allapot = drw["Allapot"].ToString();
                    if (!String.IsNullOrEmpty(allapot))
                    {
                        allapot_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.KEZBESITESITETEL_ALLAPOT.KodcsoportKod, allapot, Page);
                    }
                }

                return allapot_nev;
            }
        }

        return String.Empty;
    }

    private string GetObjType(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string Obj_type = String.Empty;
                if (drw.Row.Table.Columns.Contains("Obj_type"))
                {
                    Obj_type = drw["Obj_type"].ToString();
                }

                return Obj_type;
            }
        }

        return String.Empty;
    }

    private string GetObjId(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string Obj_Id = String.Empty;
                if (drw.Row.Table.Columns.Contains("Obj_Id"))
                {
                    Obj_Id = drw["Obj_Id"].ToString();
                }

                return Obj_Id;
            }
        }

        return String.Empty;
    }
    #endregion
}