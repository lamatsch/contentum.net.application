using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BankszamlaszamokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        PartnerTextBox_Partner.ReadOnly = true;
        BankszamlaszamTextBox1.ReadOnly = true;
        PartnerTextBox_Bank.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        labelBankszamlaszam.CssClass = "mrUrlapInputWaterMarked";
        labelBank.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        PartnerTextBox_Partner.ReadOnly = true;

        labelPartner.CssClass = "mrUrlapInputWaterMarked";

        PartnerTextBox_Partner.Validate = true;
        BankszamlaszamTextBox1.Validate = true;
    }

    /// <summary>
    /// New m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetNewControls()
    {
        PartnerTextBox_Partner.Validate = true;
        BankszamlaszamTextBox1.Validate = true;
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Bankszamlaszam" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (!result.IsError)
                {
                    KRT_Bankszamlaszamok krt_Bankszamlaszamok = (KRT_Bankszamlaszamok)result.Record;
                    LoadComponentsFromBusinessObject(krt_Bankszamlaszamok);                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            //a querystringben kapott partner be�ll�t�sa, ha volt
            string Partner_Id = Request.QueryString.Get(QueryStringVars.PartnerId);
            if (!String.IsNullOrEmpty(Partner_Id))
            {
                PartnerTextBox_Partner.Id_HiddenField = Partner_Id;
                PartnerTextBox_Partner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

                PartnerTextBox_Partner.ReadOnly = true;
                labelPartner.CssClass = "mrUrlapInputWaterMarked";
            }

            SetNewControls();
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }

        // bank csak szervezet lehet
        PartnerTextBox_Bank.SetFilterType_Szervezet();
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Bankszamlaszamok krt_Bankszamlaszamok)
    {
        if (krt_Bankszamlaszamok != null)
        {
            PartnerTextBox_Partner.Id_HiddenField = krt_Bankszamlaszamok.Partner_Id;
            PartnerTextBox_Partner.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

            BankszamlaszamTextBox1.Id_HiddenField = krt_Bankszamlaszamok.Id;
            BankszamlaszamTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

            PartnerTextBox_Bank.Id_HiddenField = krt_Bankszamlaszamok.Partner_Id_Bank;
            PartnerTextBox_Bank.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

            ErvenyessegCalendarControl1.ErvKezd = krt_Bankszamlaszamok.ErvKezd;
            ErvenyessegCalendarControl1.ErvVege = krt_Bankszamlaszamok.ErvVege;

            //aktu�lis verzi� elt�rol�sa
            FormHeader1.Record_Ver = krt_Bankszamlaszamok.Base.Ver;
            //A m�dosit�s,l�trehoz�s form be�ll�t�sa
            FormPart_CreatedModified1.SetComponentValues(krt_Bankszamlaszamok.Base);
        }
    }

    // form --> business object
    private KRT_Bankszamlaszamok GetBusinessObjectFromComponents()
    {
        KRT_Bankszamlaszamok krt_Bankszamlaszamok = new KRT_Bankszamlaszamok();
        krt_Bankszamlaszamok.Updated.SetValueAll(false);
        krt_Bankszamlaszamok.Base.Updated.SetValueAll(false);

        krt_Bankszamlaszamok.Partner_Id = PartnerTextBox_Partner.Id_HiddenField;
        krt_Bankszamlaszamok.Updated.Partner_Id = pageView.GetUpdatedByView(PartnerTextBox_Partner);

        krt_Bankszamlaszamok.Bankszamlaszam = BankszamlaszamTextBox1.Text;
        krt_Bankszamlaszamok.Updated.Bankszamlaszam = pageView.GetUpdatedByView(BankszamlaszamTextBox1);

        krt_Bankszamlaszamok.Partner_Id_Bank = PartnerTextBox_Bank.Id_HiddenField;
        krt_Bankszamlaszamok.Updated.Partner_Id_Bank = pageView.GetUpdatedByView(PartnerTextBox_Bank);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Bankszamlaszamok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_Bankszamlaszamok.Base.Ver = FormHeader1.Record_Ver;
        krt_Bankszamlaszamok.Base.Updated.Ver = true;

        return krt_Bankszamlaszamok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(PartnerTextBox_Partner);
            compSelector.Add_ComponentOnClick(BankszamlaszamTextBox1);
            compSelector.Add_ComponentOnClick(PartnerTextBox_Bank);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Bankszamlaszam" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
                            KRT_Bankszamlaszamok krt_Bankszamlaszamok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_Bankszamlaszamok);

                            if (!result.IsError)
                            {
                                // ha t�bb textbox is meg van adva, feldaraboljuk
                                string tb1ClientId = Request.QueryString.Get(QueryStringVars.TextBoxId);
                                string tb2ClientId = Request.QueryString.Get("TextBoxId2");
                                string tb3ClientId = Request.QueryString.Get("TextBoxId3");

                                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                                string Bankszamlaszam1 = null;
                                string Bankszamlaszam2 = null;
                                string Bankszamlaszam3 = null;
                                
                                if (!String.IsNullOrEmpty(tb2ClientId))
                                {
                                    string txt = krt_Bankszamlaszamok.Bankszamlaszam;
                                    Bankszamlaszam1 = txt.Substring(0, Math.Min(8, txt.Length));
                                    txt = txt.Remove(0, Math.Min(txt.Length, 8));
                                    Bankszamlaszam2 = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
                                    if (!String.IsNullOrEmpty(tb3ClientId))
                                    {
                                        txt = txt.Remove(0, Math.Min(txt.Length, 8));
                                        Bankszamlaszam3 = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
                                    }
                                }
                                else
                                {
                                    Bankszamlaszam1 = krt_Bankszamlaszamok.Bankszamlaszam;
                                }

                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Bankszamlaszam1, true, tryFireChangeEvent))
                                {

                                    if (!String.IsNullOrEmpty(tb2ClientId))
                                    {
                                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                                        // Sorrend fontos!!!
                                        // el�bb hiddenfield, azt�n textbox (mert m�sk�pp a change lefut�sakor m�g nincs �t�rva a hiddenfield id,
                                        // �s az esetleges kl�noz�sn�l helytelen (a m�dos�t�s el�tti) �rt�k ker�l be
                                        parameters.Add(tb2ClientId, Bankszamlaszam2);
                                        if (!String.IsNullOrEmpty(tb3ClientId))
                                        {
                                            parameters.Add(tb3ClientId, Bankszamlaszam3);
                                        }
                                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "BankszamlaszamReturnValues", true, tryFireChangeEvent);
                                    }

                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
                                KRT_Bankszamlaszamok krt_Bankszamlaszamok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_Bankszamlaszamok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
