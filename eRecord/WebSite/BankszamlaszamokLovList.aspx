<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="BankszamlaszamokLovList.aspx.cs" Inherits="BankszamlaszamokLovList" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,BankszamlaszamokLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Bankszámlaszám:"></asp:Label><br />
                                            <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%"></asp:TextBox>&nbsp;
                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                AlternateText="Keresés"></asp:ImageButton>
                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                AlternateText="Részletes keresés"></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: top;">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                            <!-- Kliens oldali kiválasztás kezelése -->
                                                            <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                            <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                            <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                                PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                                DataKeyNames="Id">
                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="Id" SortExpression="Id">
                                                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Bankszamlaszam" HeaderText="Bankszámlaszám" SortExpression="Bankszamlaszam">
                                                                        <HeaderStyle CssClass="GridViewInvisibleColumnStyle" Width="100px" />
                                                                        <ItemStyle CssClass="GridViewInvisibleColumnStyle" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderTemplate>
                                                                            <asp:LinkButton ID="linkBankszamlaszam" runat="server" CommandName="sort" CommandArgument="Bankszamlaszam">Bankszámlaszám</asp:LinkButton>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="labelBankszamlaszam" runat="server" Text='<%# string.Format("{0}{1}{2}", Eval("Bankszamlaszam1"), (string.IsNullOrEmpty(Eval("Bankszamlaszam2") as string) ? "" : string.Format("-{0}",Eval("Bankszamlaszam2"))), (string.IsNullOrEmpty(Eval("Bankszamlaszam3") as string) ? "" : string.Format("-{0}",Eval("Bankszamlaszam3"))))  %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Partner_Nev_Bank" HeaderText="Bank" SortExpression="KRT_Partnerek_Bank.Nev">
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <PagerSettings Visible="False" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <%--<asp:ListBox ID="ListBoxSearchResult" runat="server" Width="95%" Rows="20" />>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" AlternateText="Kijelölt tétel részletes adatainak megtekintése"
                                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png" onmouseover="swapByName(this.id,'reszletesadatok2.png')"
                                                onmouseout="swapByName(this.id,'reszletesadatok.png')"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
