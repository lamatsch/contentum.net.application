using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;

public partial class BankszamlaszamokLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    private string Partner_Id = null;
 
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BankszamlaszamokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        Partner_Id = Request.QueryString.Get(QueryStringVars.PartnerId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        string qsSearch = String.Empty;

        if (!string.IsNullOrEmpty(Partner_Id))
        {
            qsSearch = QueryStringVars.PartnerId + "=" + Partner_Id;
        }

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("BankszamlaszamokSearch.aspx", qsSearch
            , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "BankszamlaszamokForm.aspx", "", GridViewSelectedId.ClientID, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        if (!IsPostBack)
        {
            FillGridViewSearchResult(TextBoxSearch.Text, false);
        }

        Form.DefaultButton = ButtonSearch.UniqueID;
        Form.DefaultFocus = ButtonSearch.UniqueID;
    }


    protected void ClearForm()
    {
        TextBoxSearch.Text = "";
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillGridViewSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
        KRT_BankszamlaszamokSearch krt_BankszamlaszamokSearch = null;

        if (searchObjectFromSession == true)
        {
            krt_BankszamlaszamokSearch = (KRT_BankszamlaszamokSearch)Search.GetSearchObject(Page, new KRT_BankszamlaszamokSearch());
            krt_BankszamlaszamokSearch.OrderBy = "Bankszamlaszam";

            ClearForm();
        }
        else
        {
            krt_BankszamlaszamokSearch = new KRT_BankszamlaszamokSearch();
            krt_BankszamlaszamokSearch.Bankszamlaszam.Value = SearchKey;
            krt_BankszamlaszamokSearch.Bankszamlaszam.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            krt_BankszamlaszamokSearch.OrderBy = "Bankszamlaszam";
        }

        if (!String.IsNullOrEmpty(Partner_Id))
        {
            krt_BankszamlaszamokSearch.Partner_Id.Value = Partner_Id;
            krt_BankszamlaszamokSearch.Partner_Id.Operator = Query.Operators.equals;
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        krt_BankszamlaszamokSearch.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(execParam, krt_BankszamlaszamokSearch);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, krt_BankszamlaszamokSearch.TopRow);
    }
        
    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                    // ha t�bb textbox is meg van adva, feldaraboljuk
                    string tb1ClientId = Request.QueryString.Get(QueryStringVars.TextBoxId);
                    string tb2ClientId = Request.QueryString.Get("TextBoxId2");
                    string tb3ClientId = Request.QueryString.Get("TextBoxId3");

                    string Bankszamlaszam1 = null;
                    string Bankszamlaszam2 = null;
                    string Bankszamlaszam3 = null;

                    if (!String.IsNullOrEmpty(tb2ClientId))
                    {
                        string txt = selectedText;
                        Bankszamlaszam1 = txt.Substring(0, Math.Min(8, txt.Length));
                        txt = txt.Remove(0, Math.Min(txt.Length, 8));
                        Bankszamlaszam2 = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
                        if (!String.IsNullOrEmpty(tb3ClientId))
                        {
                            txt = txt.Remove(0, Math.Min(txt.Length, 8));
                            Bankszamlaszam3 = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
                        }
                    }
                    else
                    {
                        Bankszamlaszam1 = selectedText;
                    }

                    if (JavaScripts.SendBackResultToCallingWindow(Page, selectedId, Bankszamlaszam1, true, tryFireChangeEvent))
                    {
                        if (!String.IsNullOrEmpty(tb2ClientId))
                        {
                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                            // Sorrend fontos!!!
                            // el�bb hiddenfield, azt�n textbox (mert m�sk�pp a change lefut�sakor m�g nincs �t�rva a hiddenfield id,
                            // �s az esetleges kl�noz�sn�l helytelen (a m�dos�t�s el�tti) �rt�k ker�l be
                            parameters.Add(tb2ClientId, Bankszamlaszam2);
                            if (!String.IsNullOrEmpty(tb3ClientId))
                            {
                                parameters.Add(tb3ClientId, Bankszamlaszam3);
                            }
                            JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "BankszamlaszamReturnValues", true, tryFireChangeEvent);
                        }

                        string refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow == "1" ? true : false);
                    }
                    else
                    {
                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    }
                    
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult("", true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}
