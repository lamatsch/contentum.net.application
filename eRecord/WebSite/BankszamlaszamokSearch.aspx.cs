using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class BankszamlaszamokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_BankszamlaszamokSearch);

    string Partner_Id = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

        Partner_Id = Request.QueryString.Get(QueryStringVars.PartnerId);

        BankszamlaszamTextBox1.PartnerTextBox = PartnerTextBox_Partner;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_BankszamlaszamokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_BankszamlaszamokSearch)Search.GetSearchObject(Page, new KRT_BankszamlaszamokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_BankszamlaszamokSearch krt_BankszamlaszamokSearch = null;
        if (searchObject != null) krt_BankszamlaszamokSearch = (KRT_BankszamlaszamokSearch)searchObject;

        if (krt_BankszamlaszamokSearch != null)
        {
            if (!String.IsNullOrEmpty(Partner_Id))
            {
                PartnerTextBox_Partner.Id_HiddenField = Partner_Id;
                PartnerTextBox_Partner.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
                PartnerTextBox_Partner.ReadOnly = true;
            }
            else
            {
                PartnerTextBox_Partner.Id_HiddenField = krt_BankszamlaszamokSearch.Partner_Id.Value;
                PartnerTextBox_Partner.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
            }

            // ha sz�veg volt megadva, de nem volt kit�ltve az Id, a sz�veget t�ltj�k csak vissza
            if (String.IsNullOrEmpty(krt_BankszamlaszamokSearch.Id.Value))
            {
                BankszamlaszamTextBox1.Text = krt_BankszamlaszamokSearch.Bankszamlaszam.Value;
                BankszamlaszamTextBox1.Id_HiddenField = "";
            }
            else
            {
                BankszamlaszamTextBox1.Id_HiddenField = krt_BankszamlaszamokSearch.Id.Value;
                BankszamlaszamTextBox1.SetTextBoxById(SearchHeader1.ErrorPanel);
            }

            PartnerTextBox_Bank.Id_HiddenField = krt_BankszamlaszamokSearch.Partner_Id_Bank.Value;
            PartnerTextBox_Bank.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);

            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_BankszamlaszamokSearch.ErvKezd, krt_BankszamlaszamokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private KRT_BankszamlaszamokSearch SetSearchObjectFromComponents()
    {
        KRT_BankszamlaszamokSearch krt_BankszamlaszamokSearch = (KRT_BankszamlaszamokSearch)SearchHeader1.TemplateObject;
        if (krt_BankszamlaszamokSearch == null)
        {
            krt_BankszamlaszamokSearch = new KRT_BankszamlaszamokSearch();
        }

        if (!String.IsNullOrEmpty(PartnerTextBox_Partner.Id_HiddenField))
        {
            krt_BankszamlaszamokSearch.Partner_Id.Value = PartnerTextBox_Partner.Id_HiddenField;
            krt_BankszamlaszamokSearch.Partner_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(BankszamlaszamTextBox1.Id_HiddenField))
        {
            krt_BankszamlaszamokSearch.Id.Value = BankszamlaszamTextBox1.Id_HiddenField;
            krt_BankszamlaszamokSearch.Id.Operator = Query.Operators.equals;
        }
        else if (!String.IsNullOrEmpty(BankszamlaszamTextBox1.Text))
        {
            krt_BankszamlaszamokSearch.Bankszamlaszam.Value = BankszamlaszamTextBox1.Text;
            krt_BankszamlaszamokSearch.Bankszamlaszam.Operator = Search.GetOperatorByLikeCharater(BankszamlaszamTextBox1.Text);
        }

        if (!String.IsNullOrEmpty(PartnerTextBox_Bank.Id_HiddenField))
        {
            krt_BankszamlaszamokSearch.Partner_Id_Bank.Value = PartnerTextBox_Bank.Id_HiddenField;
            krt_BankszamlaszamokSearch.Partner_Id_Bank.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_BankszamlaszamokSearch.ErvKezd, krt_BankszamlaszamokSearch.ErvVege);

        return krt_BankszamlaszamokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject()); 
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_BankszamlaszamokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_BankszamlaszamokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_BankszamlaszamokSearch();
    }



   

}
