<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="BarkodSavokForm.aspx.cs" Inherits="BarkodSavokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true" >
	</asp:ScriptManager>
	<uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,BarkodSavokNewHeaderTitle %>" />
	<br />
	<div class="popupBody">
		<eUI:eFormPanel ID="EFormPanel1" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%">
				<tbody>
					<tr class="urlapNyitoSor">
						<td class="mrUrlapCaption">
							<img alt=" " src="images/hu/design/spacertrans.gif" />
						</td>
						<td class="mrUrlapMezo">
							<img alt=" " src="images/hu/design/spacertrans.gif" />
						</td>
					</tr>
                    <tr class="urlapSor">
						<td class="mrUrlapCaption">
							<asp:Label ID="Label0" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelFelelos" runat="server" Text="S�vtulajdonos:"></asp:Label>
						</td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="FelelosCsoportTextBox1" runat="server" ViewMode="true" Validate="false"/>
                        </td>
                    </tr>
                    <tr class="urlapSor" ID="TRKovetkezoCsoportFelelos" runat="server">
						<td class="mrUrlapCaption">
							<asp:Label ID="LabelKovetkezoCsoportFelelos0" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelKovetkezoCsoportFelelos1" runat="server" Text="K�vetkez� csoport felel�s:"></asp:Label>
						</td>
                        <td class="mrUrlapMezo">
                            <uc7:CsoportTextBox ID="FelelosCsoportTextBox2" runat="server" Validate="false"/>
                        </td>
                    </tr>
                    <tr class="urlapSor" ID="TRIgenylendoDarab" runat="server" >
						<td class="mrUrlapCaption">
							<asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelIgenylendoDarab" runat="server" Text="Ig�nylend� darab:"></asp:Label>
						</td>
                        <td class="mrUrlapMezo">
                            <uc9:RequiredNumberBox ID="Igenylendo_Darab_RequiredNumberBox" runat="server" Validate="true" />
                        </td>
                    </tr>

                    <tr class="urlapSor" ID="TRReszIntervallum" runat="server" >
						<td class="mrUrlapCaption">
							<asp:Label ID="LabelReszSav1" runat="server" CssClass="ReqStar" Text=""></asp:Label>
							<asp:Label ID="labelReszSav2" runat="server" Text="R�sz s�v felt�lt�s:"></asp:Label>
						</td>
                        <td class="mrUrlapMezo">
                            <asp:CheckBox ID="CheckBoxReszSav" runat="server" Text="" />
                        </td>
                    </tr>
<%-- .................... --%>
					<tr class="urlapSor" ID="TRSavKezdete" runat="server" >
						<td class="mrUrlapCaption">
							<asp:Label ID="LabelSavKezdete1" runat="server" CssClass="ReqStar" Text=""></asp:Label>
							<asp:Label ID="labelSavKezdete2" runat="server" Text="S�v kezdete:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<uc8:VonalKodTextBox ID="SavKezdete" runat="server" Validate="false" />
						</td>
					</tr>
					<tr class="urlapSor" ID="TRReszSavKezdete" runat="server" >
						<td class="mrUrlapCaption">
							<asp:Label ID="LabelReszSavKezdete1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelReszSavKezdete2" runat="server" Text="R�sz s�v kezdete:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<uc8:VonalKodTextBox ID="ReszSavKezdete" runat="server" Validate="true" />
						</td>
					</tr>
<%-- .................... --%>
					<tr class="urlapSor" ID="TRSavVege" runat="server" >
						<td class="mrUrlapCaption">
							<asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text=""></asp:Label>
							<asp:Label ID="labelSavVege" runat="server" Text="S�v v�ge:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<uc8:VonalKodTextBox ID="SavVege" runat="server" Validate="false" />
						</td>
					</tr>
					<tr class="urlapSor" ID="TRReszSavVege" runat="server" >
						<td class="mrUrlapCaption">
							<asp:Label ID="LabelReszSavVege1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelReszSavVege2" runat="server" Text="R�sz s�v v�ge:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<uc8:VonalKodTextBox ID="ReszSavVege" runat="server" Validate="true" />
						</td>
					</tr>


					<tr class="urlapSor">
						<td class="mrUrlapCaption" >
							<asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelSavTipus" runat="server" Text="S�v t�pus:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:DropDownList ID="ddownSavTipus" runat="server">
								<%-- asp:ListItem Text="Gener�lt" Value="G"></asp:ListItem --%>
								<asp:ListItem Text="Nyomdai" Value="N"></asp:ListItem>
								<asp:ListItem Text="Printelt" Value="P"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr class="urlapSor">
						<td class="mrUrlapCaption" >
							<asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelSavAllapot" runat="server" Text="S�v �llapot:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<asp:DropDownList ID="ddownSavAllapot" runat="server">
								<asp:ListItem Text="Allok�lt" Value="A"></asp:ListItem>
								<asp:ListItem Text="Felhaszn�lt" Value="F"></asp:ListItem>
								<asp:ListItem Text="Haszn�lhat�" Value="H"></asp:ListItem>
								<asp:ListItem Text="T�r�lt" Value="T"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr class="urlapSor" runat="server" id="Ervenyesseg_tr">
						<td class="mrUrlapCaption" >
							<asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
							<asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
						</td>
						<td class="mrUrlapMezo">
							<uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" Enabled="false" />
						</td>
					</tr>
				</tbody>
			</table>
		</eUI:eFormPanel>
		<uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
		<uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
	</div>
</asp:Content>
