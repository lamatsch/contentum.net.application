using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BarkodSavokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
    	FelelosCsoportTextBox1.Enabled = false;
        SavKezdete.ViewEnabled = false;
        //SavVege.TextBox.Text = krt_BarkodSavok.SavVege;
        //ddownSavTipus.SelectedValue = krt_BarkodSavok.SavType;
		//ddownSavAllapot.SelectedValue = krt_BarkodSavok.SavAllapot;
        ErvenyessegCalendarControl1.Enable_ErvKezd = false;
        ErvenyessegCalendarControl1.Enable_ErvVege = false;
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
 
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        //---requiredTextBoxNev.TextBox.Enabled = false;
        //---ddownKarbantarthato.Enabled = false;

        //---labelNev.CssClass = "mrUrlapInputWaterMarked";
        //---labelKarbantarthato.CssClass = "mrUrlapInputWaterMarked";
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //SavKezdete.Width = new Unit(80);
        //SavVege.Width = new Unit(80);
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                ErvenyessegCalendarControl1.Enabled = true;
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BarkodSav" + Command);
                //TODO: a val�di jogokhoz k�tni! (ld. el�z� sor, de a list�n is figyelni kellene)
                // csak ideiglenes biztons�gi int�zked�s, majd 
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BarkodSavokList");
                break;
        }
        FormHeader1.HeaderTitle = "Vonalk�d s�v";
      
            if ( Command == CommandName.New )
        {
        	FormHeader1.ModeText = "Ig�nyl�s";
        	Igenylendo_Darab_RequiredNumberBox.Visible = true;

            FelelosCsoportTextBox1.Enabled = false;
        	TRKovetkezoCsoportFelelos.Visible = false;

			TRReszIntervallum.Visible = false;
        	TRSavKezdete.Visible = false;
        	TRReszSavKezdete.Visible = false;
        	TRSavVege.Visible = false;
        	TRReszSavVege.Visible = false;

        	ddownSavTipus.SelectedValue = "P";
        	ddownSavAllapot.SelectedValue = "A";
        	ddownSavAllapot.Enabled = false;
        }
		else
		{
			registerJavascripts();
			if (Command == CommandName.View)
			{
				FormHeader1.ModeText = "Felt�lt�s";
				labelIgenylendoDarab.Text = "Ig�nyelt darab:";
				TRKovetkezoCsoportFelelos.Visible = false;
				Igenylendo_Darab_RequiredNumberBox.Enabled = false;
				FelelosCsoportTextBox1.Enabled = false;
				//SavKezdete.Enabled = false;
                SavKezdete.ReadOnly = true;
				//SavVege.Enabled = false;
                SavVege.ReadOnly = true;
				ddownSavTipus.Enabled = false;
				ddownSavAllapot.Enabled = false;
				String id = Request.QueryString.Get(QueryStringVars.Id);
				if (String.IsNullOrEmpty(id))
				{
					// nincs Id megadva:
					ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
				}
				else
				{
					KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
					ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
					execParam.Record_Id = id;

					Result result = service.Get(execParam);
					if (String.IsNullOrEmpty(result.ErrorCode))
					{
						KRT_BarkodSavok krt_BarkodSavok = (KRT_BarkodSavok)result.Record;
						LoadComponentsFromBusinessObject(krt_BarkodSavok);
					}
					else
					{
						ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
					}
				}
			}

			if (Command == CommandName.Modify)
			{
				//TRKovetkezoCsoportFelelos.Visible = true;
				FelelosCsoportTextBox1.Enabled = false;
				Igenylendo_Darab_RequiredNumberBox.Enabled = false;
				TRReszIntervallum.Visible = false;
				SavKezdete.ReadOnly = true;
				SavVege.ReadOnly = true;
				ddownSavTipus.Enabled = false;
				ddownSavAllapot.Enabled = false;

                FelelosCsoportTextBox2.Validate = true;// k�vetkez� felel�s megad�sa itt k�telez�
				String id = Request.QueryString.Get(QueryStringVars.Id);
				if (String.IsNullOrEmpty(id))
				{
					// nincs Id megadva:
					ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
				}
				else
				{
					KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
					ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
					execParam.Record_Id = id;

					Result result = service.Get(execParam);
					if (String.IsNullOrEmpty(result.ErrorCode))
					{
						KRT_BarkodSavok krt_BarkodSavok = (KRT_BarkodSavok)result.Record;
						LoadComponentsFromBusinessObject(krt_BarkodSavok);
					}
					else
					{
						ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
					}
				}
			}
		}
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
			if( Command == CommandName.New ) 
			{
                FormHeader1.FullManualHeaderTitle = "Vonalk�d s�v - Ig�nyl�s";
				// felel�s defaultb�l be�ll�tva a saj�t csoportra:
				FelelosCsoportTextBox1.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
					FelhasznaloProfil.FelhasznaloId(Page));
				FelelosCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
			}
			if( Command == CommandName.View ) 
			{
                FormHeader1.FullManualHeaderTitle = "Vonalk�d s�v - Felt�lt�s";
                //// felel�s defaultb�l be�ll�tva a saj�t csoportra:
                //FelelosCsoportTextBox1.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
                //    FelhasznaloProfil.FelhasznaloId(Page));
                //FelelosCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
				FormFooter1.Command = CommandName.New;
			}
			if( Command == CommandName.Modify ) 
			{
                FormHeader1.FullManualHeaderTitle = "Vonalk�d s�v - Sz�toszt�s";
                //// felel�s defaultb�l be�ll�tva a saj�t csoportra:
                //FelelosCsoportTextBox1.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
                //    FelhasznaloProfil.FelhasznaloId(Page));
                //FelelosCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
			}
            pageView.SetViewOnPage(Command);
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="krt_KodTarak"></param>
    private void LoadComponentsFromBusinessObject(KRT_BarkodSavok krt_BarkodSavok)
    {
    	FelelosCsoportTextBox1.Id_HiddenField = krt_BarkodSavok.Csoport_Id_Felelos;
        FelelosCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        // BLG_1940
        //Igenylendo_Darab_RequiredNumberBox.Text = ""+(
        //	System.Convert.ToInt64(krt_BarkodSavok.SavVege.Substring(0, 12))-
        //	System.Convert.ToInt64(krt_BarkodSavok.SavKezd.Substring(0, 12))+1
        //);

        Igenylendo_Darab_RequiredNumberBox.Text = "" + (
        System.Convert.ToInt64(krt_BarkodSavok.SavVege.Substring(krt_BarkodSavok.SavVege.Length-13, 12)) -
        System.Convert.ToInt64(krt_BarkodSavok.SavKezd.Substring(krt_BarkodSavok.SavVege.Length - 13, 12)) + 1
    );

        SavKezdete.TextBox.Text = krt_BarkodSavok.SavKezd;
        SavVege.TextBox.Text = krt_BarkodSavok.SavVege;

        //ReszSavKezdete.TextBox.Text = krt_BarkodSavok.SavKezd;
        //ReszSavVege.TextBox.Text = krt_BarkodSavok.SavVege;

        ddownSavTipus.SelectedValue = krt_BarkodSavok.SavType;
		ddownSavAllapot.SelectedValue = krt_BarkodSavok.SavAllapot;
        ErvenyessegCalendarControl1.ErvKezd = krt_BarkodSavok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_BarkodSavok.ErvVege;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = krt_BarkodSavok.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        FormPart_CreatedModified1.SetComponentValues(krt_BarkodSavok.Base);
    }

    /// <summary>
    /// �zleti objektum felt�lt�se komponensekb�l
    /// </summary>
    /// <returns></returns>
    private KRT_BarkodSavok GetBusinessObjectFromComponents()
    {
        KRT_BarkodSavok krt_BarkodSavok = new KRT_BarkodSavok();
        krt_BarkodSavok.Updated.SetValueAll(false);
        krt_BarkodSavok.Base.Updated.SetValueAll(false);
        krt_BarkodSavok.Csoport_Id_Felelos = FelelosCsoportTextBox1.Id_HiddenField;
        krt_BarkodSavok.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(FelelosCsoportTextBox1.TextBox);

        if (Command == CommandName.View)
        {
            if (CheckBoxReszSav.Checked == true)
            {
                krt_BarkodSavok.SavKezd = ReszSavKezdete.TextBox.Text;
                krt_BarkodSavok.Updated.SavKezd = pageView.GetUpdatedByView(ReszSavKezdete.TextBox);

                krt_BarkodSavok.SavVege = ReszSavVege.TextBox.Text;
                krt_BarkodSavok.Updated.SavVege = pageView.GetUpdatedByView(ReszSavVege.TextBox);
            }
            else
            {
                krt_BarkodSavok.SavKezd = SavKezdete.TextBox.Text;
                krt_BarkodSavok.Updated.SavKezd = pageView.GetUpdatedByView(SavKezdete.TextBox);

                krt_BarkodSavok.SavVege = SavVege.TextBox.Text;
                krt_BarkodSavok.Updated.SavVege = pageView.GetUpdatedByView(SavVege.TextBox);
            }
        }
        else if (Command == CommandName.Modify)
        {
            krt_BarkodSavok.SavKezd = ReszSavKezdete.TextBox.Text;
            krt_BarkodSavok.Updated.SavKezd = pageView.GetUpdatedByView(ReszSavKezdete.TextBox);

            krt_BarkodSavok.SavVege = ReszSavVege.TextBox.Text;
            krt_BarkodSavok.Updated.SavVege = pageView.GetUpdatedByView(ReszSavVege.TextBox);
        }
        else
        {
            krt_BarkodSavok.SavKezd = SavKezdete.TextBox.Text;
            krt_BarkodSavok.Updated.SavKezd = pageView.GetUpdatedByView(SavKezdete.TextBox);

            krt_BarkodSavok.SavVege = SavVege.TextBox.Text;
            krt_BarkodSavok.Updated.SavVege = pageView.GetUpdatedByView(SavVege.TextBox);
        }
                
        krt_BarkodSavok.SavType = ddownSavTipus.SelectedValue;
        krt_BarkodSavok.Updated.SavType = pageView.GetUpdatedByView(ddownSavTipus);

		krt_BarkodSavok.SavAllapot = ddownSavAllapot.SelectedValue;
        krt_BarkodSavok.Updated.SavAllapot = pageView.GetUpdatedByView(ddownSavAllapot);

        krt_BarkodSavok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        krt_BarkodSavok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);
        krt_BarkodSavok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        krt_BarkodSavok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //a megnyit�si verzi� megad�sa ellen�rz�s miatt
        krt_BarkodSavok.Base.Ver = FormHeader1.Record_Ver;
        krt_BarkodSavok.Base.Updated.Ver = true;

        return krt_BarkodSavok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            //---compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            //---compSelector.Add_ComponentOnClick(textErtek);
            //---compSelector.Add_ComponentOnClick(ddownKarbantarthato);
            //---compSelector.Add_ComponentOnClick(textNote);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //if (FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + Command))
            //{
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
                            KRT_BarkodSavok krt_BarkodSavok = GetBusinessObjectFromComponents();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.BarkodSav_Igenyles(execParam, krt_BarkodSavok.SavType, System.Convert.ToInt32(Igenylendo_Darab_RequiredNumberBox.Text));
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.View:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
                                KRT_BarkodSavok krt_BarkodSavok = GetBusinessObjectFromComponents();
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;
                                Result result = service.BarkodSav_Feltoltes(
	                                execParam, 
	                                recordId,
	                                krt_BarkodSavok.SavKezd,
	                                krt_BarkodSavok.SavVege,
	                                (CheckBoxReszSav.Checked == true ? "I" : "N"));
                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
                                KRT_BarkodSavok krt_BarkodSavok = GetBusinessObjectFromComponents();
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;
                                //Result result = service.Update(execParam, krt_BarkodSavok);
                                Result result = service.BarkodSav_Szetosztas(
	                                execParam, 
	                                recordId,
	                                krt_BarkodSavok.SavKezd,
	                                krt_BarkodSavok.SavVege,
	                                FelelosCsoportTextBox1.Id_HiddenField,
	                                FelelosCsoportTextBox2.Id_HiddenField
	                            );
								if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            //}
            //else
            //{
            //    UI.RedirectToAccessDeniedErrorPage(Page);
            //}
        }
    }
    
    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
		
        //CheckBoxReszSav.Attributes["onclick"] = 
        //"if(document.forms[0]."+CheckBoxReszSav.ClientID+".checked == true){}else{}return true;";
        if (Command == CommandName.View)
        {
            string js_onclick = " showOrHideComponents('" + CheckBoxReszSav.ClientID
                    + "',0,['"
                    + TRReszSavKezdete.ClientID
                    + "','"
                    + TRReszSavVege.ClientID
                    + "']);";

            string[] reszsavKezdeteValidators = (ReszSavKezdete.ValidatorClientIDs ?? new string[] { "" });
            string[] reszsavVegeValidators = (ReszSavVege.ValidatorClientIDs ?? new string[] { "" });
            string ValidatorIDs = "";
            string reszsavKezdeteValidatorsConcat = String.Join("','", reszsavKezdeteValidators);
            string reszsavVegeValidatorsConcat = String.Join("','", reszsavVegeValidators);

            ValidatorIDs = String.Join("','", new string[] { reszsavKezdeteValidatorsConcat, reszsavVegeValidatorsConcat });

            if (ValidatorIDs.Length > 0)
            {
                js_onclick += " enableOrDisableValidator('" + CheckBoxReszSav.ClientID
                     + "',0,['"
                     + ValidatorIDs
                     + "']);";
            }

            CheckBoxReszSav.Attributes["onclick"] = js_onclick;

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "hideComponents", CheckBoxReszSav.Attributes["onclick"], true);
        }
    }
}
