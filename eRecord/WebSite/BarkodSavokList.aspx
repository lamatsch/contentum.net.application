<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BarkodSavokList.aspx.cs" Inherits="BarkodSavokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--HiddenFields--%>
<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%>

	<!--F� lista-->
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="text-align: left; vertical-align: top; width: 0px;">
				<asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeBarkodSavok" OnClientClick="return false;" />
			</td>
			<td style="text-align: left; vertical-align: top; width: 100%;">
				<asp:UpdatePanel ID="updatePanelBarkodSavok"
				                 runat="server"
				                 OnLoad="updatePanelBarkodSavok_Load">
					<ContentTemplate>
						<ajaxToolkit:CollapsiblePanelExtender ID="cpeBarkodSavok"
						                                      runat="server"
						                                      TargetControlID="panelBarkodSavok"
						                                      CollapsedSize="20"
						                                      Collapsed="False"
						                                      ExpandControlID="btnCpeBarkodSavok"
						                                      CollapseControlID="btnCpeBarkodSavok"
						                                      ExpandDirection="Vertical"
						                                      AutoCollapse="false"
						                                      AutoExpand="false"
						                                      CollapsedImage="images/hu/Grid/plus.gif"
						                                      ExpandedImage="images/hu/Grid/minus.gif"
						                                      ImageControlID="btnCpeBarkodSavok"
						                                      ExpandedSize="0"
						                                      ExpandedText="Vonalk�ds�vok list�ja"
						                                      CollapsedText="Vonalk�ds�vok list�ja">
						</ajaxToolkit:CollapsiblePanelExtender>
						<asp:Panel ID="panelBarkodSavok" runat="server" Width="100%">
							<table style="width: 100%;" cellpadding="0" cellspacing="0">
								<tr>
									<td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
										<asp:GridView id="gridViewBarkodSavok"
										              runat="server"
										              GridLines="None"
										              BorderWidth="1px"
										              AllowSorting="True"
										              PagerSettings-Visible="false"
										              CellPadding="0"
										              DataKeyNames="Id"
										              AutoGenerateColumns="False"
										              AllowPaging="true"
										              OnRowCommand="gridViewBarkodSavok_RowCommand"
										              OnPreRender="gridViewBarkodSavok_PreRender"
										              OnRowDataBound="gridViewBarkodSavok_RowDataBound"
										              OnSorting="gridViewBarkodSavok_Sorting">
											<RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
											<SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
											<HeaderStyle CssClass="GridViewHeaderStyle"/>
											<Columns>
												<asp:TemplateField>
													<HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
													<ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
													<HeaderTemplate>
														<asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
														&nbsp;&nbsp;
														<asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
													</HeaderTemplate>
													<ItemTemplate>
														<asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
													<HeaderStyle Width="25px" />
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
												</asp:CommandField>
												<asp:BoundField DataField="Csoport_Felelos" HeaderText="Csoport felel�s" SortExpression="Csoport_Felelos">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
											<asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
												<ItemTemplate>
												<asp:Label ID="SzervezetId" runat="server" CssClass="ReqStar" Text='<%# Eval("Csoport_Id_Felelos") %>'></asp:Label>
												</ItemTemplate>
											</asp:TemplateField>
												<asp:BoundField DataField="SavKezd" HeaderText="S�v kezdete" SortExpression="SavKezd">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="SavVege" HeaderText="S�v v�ge" SortExpression="SavVege">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="IgenyeltDarab" HeaderText="Darabsz�m" SortExpression="IgenyeltDarab">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="75px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="SavTypeName" HeaderText="S�v t�pus" SortExpression="SavTypeName">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="90px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="SavAllapotName" HeaderText="S�v �llapot" SortExpression="SavAllapotName">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="90px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
													<ItemTemplate>
													<asp:Label ID="Labelx" runat="server" CssClass="ReqStar" Text='<%# Eval("SavAllapot") %>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>

												<%-- asp:TemplateField>
													<HeaderStyle  CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
													<HeaderTemplate>
														<asp:LinkButton ID="linkKarbantarthato" runat="server">Karbantarthat�</asp:LinkButton>
													</HeaderTemplate>
													<ItemTemplate>
														<asp:CheckBox ID="cbKarbantarthato" runat="server" Enabled="false"/>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
													<HeaderTemplate>
														<asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
													</HeaderTemplate>
													<ItemTemplate>
														<asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
													</ItemTemplate>
												</asp:TemplateField --%>

											</Columns>
											<PagerSettings Visible="False" />
										</asp:GridView>
									</td>
								</tr>
							</table>
						</asp:Panel>
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
	</table>

</asp:Content>
