using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;

public partial class BarkodSavokList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    #region Utils
    static class BoolString
    {
        public const string YesOld = "I";
        public const string Yes = "1";
        public const string NoOld = "N";
        public const string No = "0";
    }

    //Az I/N �rt�k �tkonvert�l�sa checkbox �llapotaira
    protected void SetCheckBox(CheckBox cb, string value)
    {
        if (value == BoolString.Yes || value == BoolString.YesOld)
        {
            cb.Checked = true;
        }
        else
        {
            cb.Checked = false;
        }
    }

    protected string getSavAllapot(GridView gridView)
    {
        return getSavAllapot(gridView, -1);
    }

    protected string getSavAllapot(GridView gridView, int rowIndex)
    {
        string SavAllapot = String.Empty;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            Label x = (Label)selectedRow.FindControl("Labelx");
            if (x != null)
            {
                SavAllapot = x.Text;
            }
        }
        return SavAllapot;
    }

    protected string getSavFelelos(GridView gridView)
    {
        return getSavFelelos(gridView, -1);
    }

    protected string getSavFelelos(GridView gridView, int rowIndex)
    {
        string SavFelelos = String.Empty;
        int index;
        if (rowIndex == -1)
        {
            index = gridView.SelectedIndex;
        }
        else
        {
            index = rowIndex;
        }
        GridViewRow selectedRow = gridView.Rows[index];
        if (selectedRow != null)
        {
            Label x = (Label)selectedRow.FindControl("SzervezetId");
            if (x != null)
            {
                SavFelelos = x.Text;
            }
        }
        return SavFelelos;
    }

    readonly string jsNemModosithato = "alert('Vonalk�ds�v sz�toszt�sa csak felt�lt�s ut�n lehets�ges.');return false;";
    protected bool getModosithatosag(GridView gridView)
    {
        return getModosithatosag(gridView, -1);
    }
    protected bool getModosithatosag(GridView gridView, int rowIndex)
    {
        bool modosithato = false;

        if (getSavAllapot(gridView, rowIndex) == "H")
        {
            modosithato = true;
        }

        return modosithato;
    }

    readonly string jsNemFeltoltheto = "alert('" + Resources.List.UI_NotLoadable + "');return false;";
    protected bool getFeltoltheto(GridView gridView)
    {
        return getFeltoltheto(gridView, -1);
    }
    protected bool getFeltoltheto(GridView gridView, int rowIndex)
    {
        bool feltoltheto = false;

        if (getSavAllapot(gridView, rowIndex) == "A")
        {
            feltoltheto = true;
        }

        return feltoltheto;
    }

    readonly string jsNemListazhato = "alert('Vonalk�ds�v csak allok�lt �llapotban list�zhat�.');return false;";
    protected bool getListazhato(GridView gridView)
    {
        return getListazhato(gridView, -1);
    }
    protected bool getListazhato(GridView gridView, int rowIndex)
    {
        bool listazhato = false;

        if (getSavAllapot(gridView, rowIndex) == "A")
        {
            listazhato = true;
        }
        return listazhato;
    }

    readonly string jsFelhasznaloNemJogosult = "alert('�n nem jogosult a m�velet v�grehajt�s�ra!\\n\\nA m�veletet csak a vonalk�ds�v�rt felel�s szervezet tagjai, vagy ha a felel�s egy szem�ly, ezen szem�ly szervezet�nek tagjai hajthatj�k v�gre.');return false;";
    protected bool getFelhasznaloJogosult(GridView gridView)
    {
        return getFelhasznaloJogosult(gridView, -1);
    }
    protected bool getFelhasznaloJogosult(GridView gridView, int rowIndex)
    {
        bool felhasznalo_jogosult = false;

        string savFelelos = getSavFelelos(gridView, rowIndex);
        if (savFelelos == FelhasznaloProfil.FelhasznaloId(Page)
            || (ListOfCsoportTagIds != null && ListOfCsoportTagIds.Contains(savFelelos)))
        {
            felhasznalo_jogosult = true;
        }

        return felhasznalo_jogosult;
    }
    #endregion

    #region csoporttagok
    private List<string> ListOfCsoportTagIds = null;

    private List<string> GetCsoportTagokListToSzervezet(string Szervezet_Id)
    {
        List<string> CsoporttagIds = new List<string>();

        if (!String.IsNullOrEmpty(Szervezet_Id))
        {
            CsoporttagIds.Add(Szervezet_Id);
            ExecParam execParam = UI.SetExecParamDefault(Page);
            KRT_CsoportokSearch search = new KRT_CsoportokSearch();

            string[] ids = Contentum.eUtility.Csoportok.GetAllSubCsoport(execParam, Szervezet_Id, true, search);

            if (ids != null)
            {
                foreach (string id in ids)
                {
                    if (!String.IsNullOrEmpty(id))
                    {
                        CsoporttagIds.Add(id);
                    }
                }
            }
        }
        
        return CsoporttagIds;
    }
    #endregion csoporttagok

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BarkodSavokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(KRT_ParameterekSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.SearchVisible = false;
        ListHeader1.HeaderLabel = Resources.List.BarkodSavokLisHeader;
        ListHeader1.ExportVisible = true;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        ListHeader1.PrintVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("BarkodSavokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelBarkodSavok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("BarkodSavokForm.aspx", CommandName.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelBarkodSavok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ExportOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewBarkodSavok.ClientID, "", "check");
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewBarkodSavok.ClientID);
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewBarkodSavok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewBarkodSavok.ClientID);
        //   ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("BarkodSavokListajaPrintForm.aspx");
        ListHeader1.PrintOnClientClick = "javascript:window.open('BarkodSavokListajaPrintForm.aspx?')";
        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewBarkodSavok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewBarkodSavok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewBarkodSavok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, ez nem tudom mire j� */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_BarkodSavokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            BarkodSavokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        // jogosults�g vizsg�lathoz csoporttagok lek�r�se
        if (ListOfCsoportTagIds == null)
        {
            if (ViewState["Csoporttagok"] != null)
            {
                ListOfCsoportTagIds = (List<string>)ViewState["Csoporttagok"];
            }
            else
            {
                ListOfCsoportTagIds = GetCsoportTagokListToSzervezet(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                ViewState["Csoporttagok"] = ListOfCsoportTagIds;
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
//---------------------------------
        /*ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.ViewHistory);
        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "BarkodSavok" + CommandName.Lock);*/

		ListHeader1.NewToolTip = "Vonalk�d s�v ig�nyl�s";
		ListHeader1.ViewToolTip = "Vonalk�d s�v felt�lt�s";
		ListHeader1.ModifyToolTip = "Vonalk�d s�v sz�toszt�sa m�s szervezeteknek";
		ListHeader1.ExportToolTip = "A kiv�lasztott s�v vonalk�djainak export�l�sa";
		
		ListHeader1.InvalidateVisible = false;
		ListHeader1.HistoryVisible = false;

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewBarkodSavok);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }
    #endregion Base Page

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void BarkodSavokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewBarkodSavok", ViewState, "");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewBarkodSavok", ViewState);

        BarkodSavokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void BarkodSavokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_BarkodSavokSearch search = (KRT_BarkodSavokSearch)Search.GetSearchObject(Page, new KRT_BarkodSavokSearch());
     
        search.OrderBy = Search.GetOrderBy("gridViewBarkodSavok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        //Result res = service.GetAll(ExecParam, search);
        Result res = service.GetAllWithExtensions(ExecParam, search);

        UI.GridViewFill(gridViewBarkodSavok, res,ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewBarkodSavok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //checkbox oszlopok be�ll�t�sa
        /*if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox cbKarbantarthato = (CheckBox)e.Row.FindControl("cbKarbantarthato");
            DataRowView drw = (DataRowView)e.Row.DataItem;

            SetCheckBox(cbKarbantarthato, drw["Karbantarthato"].ToString());
        }*/

        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewBarkodSavok_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewBarkodSavok.PageIndex;

        //oldalsz�mok be�ll�t�sa
        gridViewBarkodSavok.PageIndex = ListHeader1.PageIndex;

        //lapoz�s eset�n adatok friss�t�se
        if (prev_PageIndex != gridViewBarkodSavok.PageIndex )
        {
            //scroll �llapot�nak t�rl�se
            //----JavaScripts.ResetScroll(Page, cpeBarkodSavok);
            BarkodSavokGridViewBind();
        }
        else
        {
            //----UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeBarkodSavok);
        }

        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewBarkodSavok);

        //oldalsz�mok be�ll�t�sa
        ListHeader1.PageCount = gridViewBarkodSavok.PageCount;
        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(gridViewBarkodSavok);

    }
    
    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        BarkodSavokGridViewBind();
    }

    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewBarkodSavok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewBarkodSavok, selectedRowNumber, "check");

            string id = gridViewBarkodSavok.DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);
            ////gridViewBarkodSavok_SelectRowCommand(id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            bool felhasznalo_jogosult = getFelhasznaloJogosult(gridViewBarkodSavok);

            //m�dos�that�s�g ellen�rz�se
            bool modosithato = getModosithatosag(gridViewBarkodSavok);
            if (!felhasznalo_jogosult)
            {
                ListHeader1.ModifyOnClientClick = jsFelhasznaloNemJogosult;
            }
            else if (modosithato)
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("BarkodSavokForm.aspx"
                ,CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                ,Defaults.PopupWidth, Defaults.PopupHeight, updatePanelBarkodSavok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ModifyOnClientClick = jsNemModosithato;
            }

            bool feltoltheto = getFeltoltheto(gridViewBarkodSavok);
            if (!felhasznalo_jogosult)
            {
                ListHeader1.ViewOnClientClick = jsFelhasznaloNemJogosult;
            }
            else if (feltoltheto)
            {
				ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("BarkodSavokForm.aspx"
					 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                     , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelBarkodSavok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ViewOnClientClick = jsNemFeltoltheto;
            }
            
            bool listazhato = getListazhato(gridViewBarkodSavok);
            if (!felhasznalo_jogosult)
            {
                ListHeader1.ExportOnClientClick = jsFelhasznaloNemJogosult;
            }
            else if (listazhato)
            {
				/*ListHeader1.ExportOnClientClick = JavaScripts.SetOnClientClick("BarkodSavokListazas.aspx"
					 , QueryStringVars.Id + "=" + id
					 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelBarkodSavok.ClientID);
				*/
				ListHeader1.ExportOnClientClick = 
					"window.open('BarkodSavokListazas.aspx?Id="+id+"','external');return false;";
            }
            else
            {
                ListHeader1.ExportOnClientClick = jsNemListazhato;
            }

            /*string tableName = "Krt_Parameterek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelBarkodSavok.ClientID);*/

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelBarkodSavok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    BarkodSavokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewBarkodSavok));                    
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            //deleteSelectedBarkodSavok();
            BarkodSavokGridViewBind();
        }
    }

    //kiv�lasztott elemek t�rl�se
    private void deleteSelectedBarkodSavok()
    {

        if (FunctionRights.GetFunkcioJog(Page, "BarkodSavokInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewBarkodSavok, EErrorPanel1, ErrorUpdatePanel);

            KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }

        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

    }

    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedBarkodSavokRecords();
                BarkodSavokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedBarkodSavokRecords();
                BarkodSavokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedBarkodSavok();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedBarkodSavokRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewBarkodSavok, "KRT_BarkodSavok"
            , "BarkodSavokLock", "BarkodSavokForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedBarkodSavokRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewBarkodSavok, "KRT_BarkodSavok"
            , "BarkodSavokLock", "BarkodSavokForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewBarkodSavok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedBarkodSavok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewBarkodSavok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_BarkodSavok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewBarkodSavok_Sorting(object sender, GridViewSortEventArgs e)
    {
        BarkodSavokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewBarkodSavok", ViewState, e.SortExpression));
    }

    #endregion

}
