﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.IO;
using System.Net;

public partial class BarkodSavokListajaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("BarkodSavokListajaPrintFormSSRS.aspx");
    }
}
