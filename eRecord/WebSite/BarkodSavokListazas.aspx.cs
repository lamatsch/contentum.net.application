using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;

public partial class BarkodSavokListazas : Contentum.eUtility.UI.PageBase
{
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
    	FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BarkodSavokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
	}

    protected void Page_Load(object sender, EventArgs e)
    {
    	KRT_BarkodSavokService service = eRecordService.ServiceFactory.GetKRT_BarkodSavokService();
    	ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
    	KRT_BarkodSavokSearch search = (KRT_BarkodSavokSearch)Search.GetSearchObject(Page, new KRT_BarkodSavokSearch());
    	
    	String id = Request.QueryString.Get(QueryStringVars.Id);
        //string str = ""+service.BarkodSav_Listazas(execParam, id, execParam.Felhasznalo_Id);

        //System.IO.Stream iStream = null;
        //int bufferSize = 10240; //10K szeletekben
        //byte[] buffer = new Byte[bufferSize];
        //int length;
        //long dataToRead;
        //string filepath = ""+str;
        //string filename = System.IO.Path.GetFileName(filepath);
        //try
        //{
        //    iStream = new System.IO.FileStream(filepath, System.IO.FileMode.Open, System.IO.FileAccess.Read,System.IO.FileShare.Read);
        //    dataToRead = iStream.Length;
        //    Response.ContentType = "application/octet-stream";
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    while(dataToRead > 0)
        //    {
        //        if (Response.IsClientConnected) 
        //        {
        //            length = iStream.Read(buffer, 0, bufferSize);
        //            Response.OutputStream.Write(buffer, 0, length);
        //            Response.Flush();
        //            buffer= new Byte[bufferSize];
        //            dataToRead = dataToRead - length;
        //        }
        //        else
        //        {
        //            dataToRead = -1;
        //        }
        //    }
        //    //kuldemeny_id = ''
        //    /*Notify.SendAnswerEmail(execParam, 
        //                           "528D5080-3916-462A-AF77-1380496F74AE",
        //                           "00000000-0000-0000-0000-000000000000");*/
        //}
        //catch (Exception ex) 
        //{
        //    Response.Write("Error : " + ex.Message);
        //}
        //finally
        //{
        //    if (iStream != null) 
        //    {
        //        iStream.Close();
        //    }
        //    Response.Close();
        //}
        //JavaScripts.RegisterCloseWindowClientScript(Page);

        if (String.IsNullOrEmpty(id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(EErrorPanel1);
        }
        else
        {
            Result result = service.BarkodSav_Listazas(execParam, id, execParam.Felhasznalo_Id);
            if (string.IsNullOrEmpty(result.ErrorCode))
            {
                string filename = "KRT_Barkodok.txt";

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                //Response.ContentType = "application/octet-stream";
                Response.ContentType = "text/html";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    string barkod = row["Kod"].ToString();
                    Response.Output.WriteLine(barkod);
                }
                Response.End();
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
	}
}
