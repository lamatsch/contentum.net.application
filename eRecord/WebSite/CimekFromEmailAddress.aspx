<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true" CodeFile="CimekFromEmailAddress.aspx.cs"
    Inherits="CimekFromEmailAddress" Title="Email c�mek" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="ucFH" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="ucFF" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ucFH:FormHeader ID="FormHeader1" runat="server" />
    <%--<uc1:ListHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,PartnercimekSearchHeaderTitle%>" />--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <%--Hiba megjelenites--%>
    <div style="width: 95%">
        <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <asp:ListBox runat="server" ID="ListBoxEmailAddresses" Rows="10" Width="98%" AutoPostBack="true"/>
                    <div style="height: 10px"></div>
                    <ucFF:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

