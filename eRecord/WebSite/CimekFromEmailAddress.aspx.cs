using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class CimekFromEmailAddress : Contentum.eUtility.UI.PageBase
{
    private string SessionNameEmailAddressFromEmailBody = "SessionValue_EmailAddressFromEmailBody";

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        FormHeader1.HeaderTitle = Resources.Form.FormHeader_EmailAddressFromEmailBody;
        FormFooter1.ImageButton_Accept.Visible = true;
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);

        string partnerId = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.PartnerId);
        string emailBoritekId = Request.QueryString.Get(Contentum.eRecord.Utility.QueryStringVars.EmailBoritekokId);
        if (string.IsNullOrEmpty(emailBoritekId))
            return;

        if (!IsPostBack)
        {
            string partnerEmail = LoadPartnerEmailAddress(partnerId);

            EREC_eMailBoritekokService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
            execparam_emailGet.Record_Id = emailBoritekId;

            Result result_emailGet = service.Get(execparam_emailGet);
            if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                ErrorUpdatePanel1.Update();
                return;
            }
            Dictionary<string, string> resultEmails = new Dictionary<string, string>();

            EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)result_emailGet.Record;

            #region Mail From
            if (!string.IsNullOrEmpty(erec_eMailBoritekok.Felado) && !resultEmails.ContainsKey(erec_eMailBoritekok.Felado))
                resultEmails.Add(erec_eMailBoritekok.Felado, erec_eMailBoritekok.Felado);
            #endregion

            #region Mail Content
            Dictionary<string, string> resultFromContent = GetEmailAddressesFromEmailBody(erec_eMailBoritekok.Uzenet);
            if (resultFromContent != null && resultFromContent.Count > 0)
            {
                foreach (var key in resultFromContent.Keys)
                {
                    if (!string.IsNullOrEmpty(key) && !resultEmails.ContainsKey(key))
                        resultEmails.Add(key, key);
                }
            }
            #endregion

            #region Mail Subject
            Dictionary<string, string> resultSubject = GetEmailAddressesFromEmailBody(erec_eMailBoritekok.Targy);
            if (resultSubject != null && resultSubject.Count > 0)
            {
                foreach (var key in resultSubject.Keys)
                {
                    if (!string.IsNullOrEmpty(key) && !resultEmails.ContainsKey(key))
                        resultEmails.Add(key, key);
                }
            }
            #endregion

            #region Mail CC
            Dictionary<string, string> resultCC = GetEmailAddressesFromEmailBody(erec_eMailBoritekok.CC);
            if (resultCC != null && resultCC.Count > 0)
            {
                foreach (var key in resultCC.Keys)
                {
                    if (!string.IsNullOrEmpty(key) && !resultEmails.ContainsKey(key))
                        resultEmails.Add(key, key);
                }
            }
            #endregion

            #region Mail Cimzett
            Dictionary<string, string> resultCimzett = GetEmailAddressesFromEmailBody(erec_eMailBoritekok.Cimzett);
            if (resultCimzett != null && resultCimzett.Count > 0)
            {
                foreach (var key in resultCimzett.Keys)
                {
                    if (!string.IsNullOrEmpty(key) && !resultEmails.ContainsKey(key))
                        resultEmails.Add(key, key);
                }
            }
            #endregion

            FillListBox(resultEmails, partnerEmail);
        }
    }
    /// <summary>
    /// Felt�lti a listbox kontrolt az email c�mekkel
    /// </summary>
    /// <param name="result"></param>
    private void FillListBox(Dictionary<string, string> result, string partnerEmail)
    {
        if (!string.IsNullOrEmpty(partnerEmail))
            ListBoxEmailAddresses.Items.Add(new ListItem()
            {
                Text = partnerEmail,
                Value = partnerEmail,
                Selected = true
            });
        foreach (var item in result.Keys)
        {
            ListBoxEmailAddresses.Items.Add(new ListItem()
            {
                Text = item,
                Value = item
            });
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (!string.IsNullOrEmpty(ListBoxEmailAddresses.SelectedValue))
        {
            Session[SessionNameEmailAddressFromEmailBody] = ListBoxEmailAddresses.SelectedValue;
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    protected void registerJavascripts()
    {
        //zIndex
        JavaScripts.SetCompletionListszIndex(Page);
    }
    /// <summary>
    /// Visszaadja a megtal�lt email c�meket
    /// </summary>
    /// <param name="body"></param>
    /// <returns></returns>
    private Dictionary<string, string> GetEmailAddressesFromEmailBody(string body)
    {
        string pattern = Rendszerparameterek.Get(Page, Rendszerparameterek.REGEXP_PATTERN_EMAIL_ADDRESS);
        if (string.IsNullOrEmpty(pattern))
            return null;
        return Contentum.eUtility.RegExp.EmailAddressMatchesAll(pattern, body);
    }
    /// <summary>
    /// LoadPartnerEmailAddress
    /// </summary>
    /// <param name="partnerId"></param>
    /// <returns></returns>
    private string LoadPartnerEmailAddress(string partnerId)
    {
        if (string.IsNullOrEmpty(partnerId))
            return null;

        KRT_PartnerekService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
        ExecParam execparam_Get = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_PartnerekSearch src = new KRT_PartnerekSearch();
        src.Id.Value = partnerId;
        src.Id.Operator = Query.Operators.equals;
        src.TopRow = 5;

        Result result_Get = service.GetReallyAllWithCim(execparam_Get, src, new KRT_CimekSearch(), "1");
       
        if (!string.IsNullOrEmpty(result_Get.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Get);
            ErrorUpdatePanel1.Update();
            return null;
        }
        for (int i = 0; i < result_Get.Ds.Tables[0].Rows.Count; i++)
        {
            if (string.IsNullOrEmpty(result_Get.Ds.Tables[0].Rows[i]["CimNev"].ToString()))
                continue;
            if (result_Get.Ds.Tables[0].Rows[i]["CimNev"].ToString().Contains("@"))
                return result_Get.Ds.Tables[0].Rows[i]["CimNev"].ToString();
        }

        return null;
    }
}
