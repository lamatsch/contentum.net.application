<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CimekList.aspx.cs" Inherits="CimekList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
<%--Hiba megjelenites--%>
<eUI:eErrorPanel id="EErrorPanel1" runat="server">
    </eUI:eErrorPanel>
</contenttemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--/Hiba megjelenites--%>
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
<%--Tablazat / Grid--%> 

    <table width="100%" cellpadding="0" cellspacing="0">   
    <tr>
    <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="CimekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
    <td style="text-align: left; vertical-align: top; width: 100%;">
            <ajaxToolkit:CollapsiblePanelExtender ID="CimekCPE" runat="server" TargetControlID="PanelCimek"
                CollapsedSize="20" Collapsed="False" ExpandControlID="CimekCPEButton" CollapseControlID="CimekCPEButton"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="CimekCPEButton"
                ExpandedSize="0" ExpandedText="Szerepk�r�k list�ja" CollapsedText="Szerepk�r�k list�ja">
            </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="PanelCimek" runat="server">
                    <ajaxToolkit:TabContainer ID="TabContainerMaster" runat="server"
                        OnActiveTabChanged="TabContainerMaster_ActiveTabChanged" AutoPostBack="true" Width="100%">
                        <!--Postai-->
                        <ajaxToolkit:TabPanel ID="TabPanelPostai" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelPostaiCimek" runat="server">
                                    <ContentTemplate>                              
                                        <asp:Label ID="labelPostai" runat="server" Text="Postai c�mek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                         
                            </HeaderTemplate>
                             <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelPostai" runat="server" OnLoad="updatePanelPostai_Load">
                                <ContentTemplate>
                                <!-- scrollozhat�s�g miatt -->  
                                <ajaxToolkit:CollapsiblePanelExtender ID="PostaiCPE" runat="server" TargetControlID="panelPostai"
                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                    AutoCollapse="false" AutoExpand="false" ExpandedSize="0"> 
                                </ajaxToolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="panelPostai" runat="server">                      
                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                 <tr>
                                 <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <asp:GridView ID="gridViewPostai" runat="server" OnRowCommand="gridViewPostai_RowCommand"
                                    CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" 
                                    AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="gridViewPostai_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="gridViewPostai_Sorting"
                                    OnRowDataBound="gridViewPostai_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" 
                                                ShowSelectButton="True" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="TelepulesNev" HeaderText="Telep&#252;l&#233;s" SortExpression="TelepulesNev" >
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="IRSZ" HeaderText="Ir&#225;ny&#237;t&#243;sz&#225;m" SortExpression="IRSZ" >
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KozteruletNev" HeaderText="K&#246;zter&#252;let" SortExpression="KozteruletNev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KozteruletTipusNev" HeaderText="K&#246;zter&#252;let t&#237;pus" SortExpression="KozteruletTipusNev" >
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="H�zsz�m" SortExpression="Hazszam">
                                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                            
                                            <ItemTemplate>
                                                <asp:Label ID="labelHazszam" runat="server" Text='<%# Eval("Hazszam") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Lepcsohaz" HeaderText="L&#233;pcs�h&#225;z" SortExpression="Lepcsohaz">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Szint" HeaderText="Szint" SortExpression="Szint">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Ajt�" SortExpression="Ajto">
                                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                            
                                            <ItemTemplate>
                                                <asp:Label ID="labelAjto" runat="server" Text='<%# Eval("Ajto") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--BLG_1347--%>
                                        <asp:TemplateField HeaderText="Hrsz." SortExpression="Hrsz">
                                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="60px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                            
                                            <ItemTemplate>
                                                <asp:Label ID="labelHrsz" runat="server" Text='<%# Eval("Hrsz") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                            <ItemStyle CssClass="GridViewLockedImage" />
                                            <HeaderStyle CssClass="GridViewLockedImage" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                        <EmptyDataTemplate>
                                            <b style="padding-left:10px">A lista tartalma �res!</b>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                 </td>
                                 </tr>
                                 </table>
                                </asp:Panel>  
                               </ContentTemplate>
                               </asp:UpdatePanel>
                            </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <!-- Egy�b -->
                            <ajaxToolkit:TabPanel ID="TabPanelEgyeb" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelEgyebCimek" runat="server">
                                    <ContentTemplate>                                    
                                        <asp:Label ID="labelEgyeb" runat="server" Text="Egy�b c�mek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                     
                            </HeaderTemplate>
                             <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelEgyeb" runat="server" OnLoad="updatePanelEgyeb_Load">
                                <ContentTemplate>
                                <!-- scrollozhat�s�g miatt -->  
                                <ajaxToolkit:CollapsiblePanelExtender ID="EgyebCPE" runat="server" TargetControlID="panelEgyeb"
                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                    AutoCollapse="false" AutoExpand="false" ExpandedSize="0"> 
                                </ajaxToolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="panelEgyeb" runat="server">            
                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                 <tr>
                                 <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <asp:GridView ID="gridViewEgyeb" runat="server" OnRowCommand="gridViewEgyeb_RowCommand"
                                    CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                    AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" OnPreRender="gridViewEgyeb_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="gridViewEgyeb_Sorting"
                                    OnRowDataBound="gridViewEgyeb_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="TipusNev" HeaderText="C�m T�pusa" SortExpression="KodTarakCimTipus.Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>   
                                        <asp:BoundField DataField="CimTobbi" HeaderText="C�m" SortExpression="CimTobbi">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        </asp:BoundField>   
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <b style="padding-left:10px">A lista tartalma �res!</b>
                                    </EmptyDataTemplate>
                                    <PagerSettings Visible="False" />
                                  </asp:GridView>
                                 </td>
                                 </tr>
                                 </table>
                               </asp:Panel>   
                               </ContentTemplate>
                               </asp:UpdatePanel>
                            </ContentTemplate> 
                         </ajaxToolkit:TabPanel>    
                  </ajaxToolkit:TabContainer>
                </asp:Panel>
          </td>                            
    </tr>
    <tr>
        <td style="text-align: left; height: 8px;" colspan="2">
        </td>
    </tr>
    <tr>   
        <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
        </td>
        <td style="text-align: left; vertical-align: top; width: 100%;"> 
        <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">  
                <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton" CollapseControlID="DetailCPEButton" ExpandDirection="Vertical"
                AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                </ajaxToolkit:CollapsiblePanelExtender>
            
                <asp:Panel ID="panelDetail" runat="server">
                    <ajaxToolkit:TabContainer ID="TabContainerDetail" runat="server" 
                        OnActiveTabChanged="TabContainerDetail_ActiveTabChanged" Width="100%">                     
                        <ajaxToolkit:TabPanel ID="TabPanelPartner" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelPartnerek" runat="server">
                                    <ContentTemplate>                               
                                        <asp:Label ID="labelPartnerHeader" runat="server" Text="Partnerek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                          
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="PartnerekUpdatePanel" runat="server" OnLoad="PartnerekUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="PartnerekPanel" runat="server" Visible="false" Width="100%">

                                            <uc1:SubListHeader ID="PartnerekSubListHeader" runat="server" />

                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                                            <ajaxToolkit:CollapsiblePanelExtender ID="PartnerekCPE" runat="server" TargetControlID="panelPartnerekList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0">         
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            
                                            <asp:Panel ID="panelPartnerekList" runat="server">

                                            <asp:GridView ID="PartnerekGridView" runat="server"
                                             CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6"
                                             AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                             AutoGenerateColumns="false" OnSorting="PartnerekGridView_Sorting" OnPreRender="PartnerekGridView_PreRender" 
                                             OnRowCommand="PartnerekGridView_RowCommand" DataKeyNames="Id" OnRowDataBound="PartnerekGridView_RowDataBound">        
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>  
                                                <asp:BoundField DataField="Partner_Nev" HeaderText="Partner neve" SortExpression="Partner_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>             
                                                <asp:BoundField DataField="Fajta_Nev" HeaderText="C�m fajt�ja" SortExpression="Fajta_Nev">
                                                      <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                      <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>                                               
                                                </Columns>                
                                            </asp:GridView> 
                                            </asp:Panel>
                                            </td></tr></table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>                                                    
                </ajaxToolkit:TabContainer>                
                </asp:Panel>      
            </td>
           </tr>
          </table>                      
        </td>
        </tr>
    </table>
</asp:Content>
