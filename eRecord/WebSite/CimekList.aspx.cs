/* $Header: CimekList.aspx.cs, 30, 2010.05.03. 9:30:23, Boda Eszter$ 
 */
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Data;

public partial class CimekList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    public const int tabIndexPostai = 0;
    public const int tabIndexEgyeb = 1;

    private void SetActiveTabBySearchObject(KRT_CimekSearch cimekSearch)
    {
        switch (cimekSearch.Tipus.Value)
        {
            case KodTarak.Cim_Tipus.Postai:
                if (cimekSearch.Tipus.Operator == Query.Operators.equals)
                    TabContainerMaster.ActiveTabIndex = tabIndexPostai;
                else
                    goto case KodTarak.Cim_Tipus.Egyeb;
                break;

            case KodTarak.Cim_Tipus.Postafiok:
            case KodTarak.Cim_Tipus.Email:
            case KodTarak.Cim_Tipus.Telefon:
            case KodTarak.Cim_Tipus.Fax:
            case KodTarak.Cim_Tipus.Web:
                goto case KodTarak.Cim_Tipus.Egyeb;

            case KodTarak.Cim_Tipus.Egyeb:
                TabContainerMaster.ActiveTabIndex = tabIndexEgyeb;
                break;

            default:
                TabContainerMaster.ActiveTabIndex = tabIndexPostai;
                break;
        }
    }

    private string GetSelectedMasterRecordID()
    {
        if (TabContainerMaster.ActiveTabIndex == tabIndexPostai)
        {
            return UI.GetGridViewSelectedRecordId(gridViewPostai);
        }
        else
        {
            return UI.GetGridViewSelectedRecordId(gridViewEgyeb);
        }
    }

    public Field SaveSearchField(Field field)
    {
        Field ret = new Field();
        ret.Group = field.Group;
        ret.GroupOperator = field.GroupOperator;
        ret.Name = field.Name;
        ret.Operator = field.Operator;
        ret.Type = field.Type;
        ret.Value = field.Value;
        ret.ValueTo = field.ValueTo;
        return ret;
    }

    public string PostaiCimekSelectedId
    {
        get
        {
            object o = ViewState["PostaiCimekSelectedId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["PostaiCimekSelectedId"] = value;
        }
    }

    public string EgyebCimekSelectedId
    {
        get
        {
            object o = ViewState["EgyebCimekSelectedId"];
            if (o != null)
                return (string)o;
            return String.Empty;
        }
        set
        {
            ViewState["EgyebCimekSelectedId"] = value;
        }
    }

    public void SetSelectedId()
    {
        switch (TabContainerMaster.ActiveTab.TabIndex)
        {
            case tabIndexPostai:
                PostaiCimekSelectedId = ListHeader1.SelectedRecordId;
                break;
            case tabIndexEgyeb:
                EgyebCimekSelectedId = ListHeader1.SelectedRecordId;
                break;
        }
    }

    public string GetSelectedId()
    {
        switch (TabContainerMaster.ActiveTab.TabIndex)
        {
            case tabIndexPostai:
                return PostaiCimekSelectedId;
            case tabIndexEgyeb:
                return EgyebCimekSelectedId;
        }

        return String.Empty;
    }

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "CimekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingButtonsClick);
        ListHeader1.SelectedRecordChanged += new EventHandler(ListHeader1_SelectedRecordChanged);
        PartnerekSubListHeader.RowCount_Changed += new EventHandler(PartnerekSubListHeader_RowCount_Changed);
        PartnerekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(PartnerekSubListHeader_ErvenyessegFilter_Changed);
    }

    void ListHeader1_SelectedRecordChanged(object sender, EventArgs e)
    {
        SetSelectedId();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.CimekListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(KRT_CimekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        //TabContainer
        JavaScripts.RegisterActiveTabChangedClientScript(Page,TabContainerMaster);
        //Master TabContainer
        ScriptManager1.RegisterAsyncPostBackControl(TabContainerMaster);
        //Detail TabContainer
        ScriptManager1.RegisterAsyncPostBackControl(TabContainerDetail);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TabContainerMaster.ClientID);

        //selectedRecordId kezel�se
        if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
        {
            ListHeader1.AttachedGridView = gridViewPostai;
        }
        else
        {
            ListHeader1.AttachedGridView = gridViewEgyeb;
        }


        PartnerekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        PartnerekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        PartnerekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        PartnerekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(PartnerekGridView.ClientID);
        PartnerekSubListHeader.ButtonsClick += new CommandEventHandler(PartnerekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        PartnerekSubListHeader.AttachedGridView = PartnerekGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewPostai);
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEgyeb);
        ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerekGridView);

        /* Breaked Records, email eset�n */
        Search.SetIdsToSearchObject(Page, "Id", new KRT_CimekSearch());
        KRT_CimekSearch search = (KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch());
        if (!String.IsNullOrEmpty(Request.QueryString.Get("Id")))
        {
            TabContainerMaster.ActiveTabIndex = tabIndexPostai;
        }
        

        if (!IsPostBack)
        {
            SetActiveTabBySearchObject((KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch()));
            ActiveTabRefreshMaster(TabContainerMaster);
        }

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Cim" + CommandName.Lock);

        PartnerekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "PartnerCimekList");

        PartnerekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerCim" + CommandName.New);
        PartnerekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerCim" + CommandName.View);
        PartnerekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerCim" + CommandName.Modify);
        PartnerekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "PartnerCim" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = "";
            if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewPostai);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewPostai(MasterListSelectedRowId);
            }
            else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyeb))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEgyeb);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewEgyeb(MasterListSelectedRowId);
            }

            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClearDetail();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainerMaster.ActiveTab);
        }
    }

    #endregion

    #region Master Tab

    protected void TabContainerMaster_ActiveTabChanged(object sender, EventArgs e)
    {
        ListHeader1.PageIndex = 0;
        ActiveTabRefreshMaster(sender as AjaxControlToolkit.TabContainer);

    }

    private void ActiveTabRefreshMaster(AjaxControlToolkit.TabContainer sender)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case tabIndexPostai:
                ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.CimTipusKod + "=" + KodTarak.Cim_Tipus.Postai
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID, EventArgumentConst.refreshMasterList, TabContainerMaster.ClientID);
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekSearch.aspx", QueryStringVars.CimTipusKod + "=" + KodTarak.Cim_Tipus.Postai
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID, EventArgumentConst.refreshMasterList, TabContainerMaster.ClientID);
                ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewPostai.ClientID);
                ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewPostai.ClientID);
                ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewPostai.ClientID);
                
                ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewPostai.ClientID);

                ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
                QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID, "", true);
                
                gridViewPostaiBind();

                if (!String.IsNullOrEmpty(PostaiCimekSelectedId))
                    ActiveTabRefreshDetail(TabContainerDetail, PostaiCimekSelectedId);
                else
                    ActiveTabClearDetail();

                break;
            case tabIndexEgyeb:
                ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.CimTipusKod + "=" + KodTarak.Cim_Tipus.EgyebDefault
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEgyeb.ClientID, EventArgumentConst.refreshMasterList, TabContainerMaster.ClientID);
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekSearch.aspx", QueryStringVars.CimTipusKod + "=" + KodTarak.Cim_Tipus.Egyeb
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID, EventArgumentConst.refreshMasterList, TabContainerMaster.ClientID);
                ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEgyeb.ClientID);
                ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewEgyeb.ClientID);
                ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewEgyeb.ClientID);

                ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewEgyeb.ClientID);

                ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
                QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEgyeb.ClientID, "", true);

                gridViewEgyebBind();

                if (!String.IsNullOrEmpty(EgyebCimekSelectedId))
                    ActiveTabRefreshDetail(TabContainerDetail, EgyebCimekSelectedId);
                else
                    ActiveTabClearDetail();

                break;
        }
    }

    #endregion

    #region Master List

    #region Postai

    protected void gridViewPostaiBind()
    {
        //string defaultSortExpression = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev";
        string defaultSortExpression = "TelepulesNev, KozteruletNev";
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewPostai", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewPostai", ViewState);

        gridViewPostaiBind(sortExpression, sortDirection);
    }

    protected void gridViewPostaiBind(String SortExpression, SortDirection SortDirection)
    {
        KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_CimekSearch search = (KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch());

        //T�pus elment�se
        Field oldTipus = SaveSearchField(search.Tipus);

        //Egy�b c�mek mez�inek elment�s
        Field oldCimTobbi = SaveSearchField(search.CimTobbi);

        //Keres�si objektum igaz�t�sa postai c�mkekre
        search.CimTobbi.Value = "";
        search.CimTobbi.Operator = "";

        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
        search.Tipus.Operator = Query.Operators.equals;

        search.OrderBy = Search.GetOrderBy("gridViewPostai", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        ListHeader1.SelectedRecordId = PostaiCimekSelectedId;

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtension(ExecParam, search);

        //T�pus vissza�ll�t�sa
        search.Tipus = oldTipus;

        //Egy�b c�mel mez�inek vissza�ll�t�sa
        search.CimTobbi = oldCimTobbi;

        //email eset�n ha nem postai cimekre sz�l a hivatkoz�s, tabv�lt�s
        if (!String.IsNullOrEmpty(Request.QueryString.Get("Id")))
        {
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                TabContainerMaster.ActiveTabIndex = tabIndexEgyeb;
                ActiveTabRefreshMaster(TabContainerMaster);
            }
        }

        UI.GridViewFill(gridViewPostai, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        UI.SetTabHeaderRowCountText(res, labelPostai);

        PostaiCimekSelectedId = ListHeader1.SelectedRecordId;

    }

    protected void gridViewPostai_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //a t�bb oszlopb�l vett mez�k be�ll�t�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            string hazszamIg = drv["Hazszamig"].ToString();
            string hazszamBetujel = drv["HazszamBetujel"].ToString();
            string ajtoBetujel = drv["AjtoBetujel"].ToString();

            Label hazszam = (Label)e.Row.FindControl("labelHazszam");
            Label ajto = (Label)e.Row.FindControl("labelAjto");

            if (!String.IsNullOrEmpty(hazszamIg))
            {
                if (hazszam != null)
                    hazszam.Text += "-" + hazszamIg;
            }
            if(!String.IsNullOrEmpty(hazszamBetujel))
            {
                if(hazszam != null)
                    hazszam.Text += "/" + hazszamBetujel;
            }
            if (!String.IsNullOrEmpty(ajtoBetujel))
            {
                if (ajto != null)
                    ajto.Text += "/" + ajtoBetujel;
            }

        }

        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void gridViewPostai_PreRender(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, PostaiCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, EgyebCPE);

            ListHeader1.RefreshPagerLabel();

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewPostai);

            PostaiCimekSelectedId = ListHeader1.SelectedRecordId;
        }

    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
        {
            gridViewPostaiBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyeb))
        {
            gridViewEgyebBind();
        }
    }

    void ListHeader1_PagingButtonsClick(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, PostaiCPE);
        JavaScripts.ResetScroll(Page, EgyebCPE);

        if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
        {
            gridViewPostaiBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyeb))
        {
            gridViewEgyebBind();
        }
    }


    protected void gridViewPostai_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewPostai, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewPostai_SelectRowCommand(id);
        }
    }

    private void gridViewPostai_SelectRowCommand(string cimId)
    {
        string id = cimId;
        if (!String.IsNullOrEmpty(id))
        {
            ActiveTabRefreshDetail(TabContainerDetail, id);

        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewPostai(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("CimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("CimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "KRT_Cimek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelPostai.ClientID);

            PartnerekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerCimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.CimId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void updatePanelPostai_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
                    {
                        gridViewPostaiBind();
                        gridViewPostai_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewPostai));
                    }
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedCimek();
            ActiveTabRefreshMaster(TabContainerMaster);
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedCimRecords();
                ActiveTabRefreshMaster(TabContainerMaster);
                break;

            case CommandName.Unlock:
                UnlockSelectedCimRecords();
                ActiveTabRefreshMaster(TabContainerMaster);
                break;
            case CommandName.SendObjects:
                SendMailSelectedCimek();
                break;
        }
    }

    private void LockSelectedCimRecords()
    {
        switch (TabContainerMaster.ActiveTabIndex)
        {
            case tabIndexPostai:
                LockManager.LockSelectedGridViewRecords(gridViewPostai, "KRT_Cimek"
                    , "CimLock", "CimForceLock"
                     , Page, EErrorPanel1, ErrorUpdatePanel);
                break;
            case tabIndexEgyeb:
                LockManager.LockSelectedGridViewRecords(gridViewEgyeb, "KRT_Cimek"
                 , "CimLock", "CimForceLock"
                , Page, EErrorPanel1, ErrorUpdatePanel);
                break;
        }
    }

    private void UnlockSelectedCimRecords()
    {
        switch (TabContainerMaster.ActiveTabIndex)
        {
            case tabIndexPostai:
                LockManager.UnlockSelectedGridViewRecords(gridViewPostai, "KRT_Cimek"
                , "CimLock", "CimForceLock"
                , Page, EErrorPanel1, ErrorUpdatePanel);
                break;
            case tabIndexEgyeb:
                LockManager.UnlockSelectedGridViewRecords(gridViewEgyeb, "KRT_Cimek"
                , "CimLock", "CimForceLock"
                , Page, EErrorPanel1, ErrorUpdatePanel);
                break;
        }

    }

    /// <summary>
    /// T�rli a gridViewPostai -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedCimek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "CimInvalidate"))
        {
            List<string> deletableItemsList = new List<string>();

            switch (TabContainerMaster.ActiveTabIndex)
            {
                case tabIndexPostai:
                    deletableItemsList = ui.GetGridViewSelectedRows(gridViewPostai, EErrorPanel1, ErrorUpdatePanel);
                    break;
                case tabIndexEgyeb:
                    deletableItemsList = ui.GetGridViewSelectedRows(gridViewEgyeb, EErrorPanel1, ErrorUpdatePanel);
                    break;
            }

            KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a gridViewPostai -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedCimek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
            {
                Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewPostai, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Cimek");
            }
            else
            {
                Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEgyeb, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "KRT_Cimek");
            }
        }
    }
    protected void gridViewPostai_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewPostaiBind(e.SortExpression, UI.GetSortToGridView("gridViewPostai", ViewState, e.SortExpression));
        ActiveTabClearDetail();
    }

    #endregion

    #region Egyeb

    protected void gridViewEgyebBind()
    {
        string defultSortExpression = "KodTarakCimTipus.Nev, CimTobbi";
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEgyeb", ViewState, defultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEgyeb", ViewState);

        gridViewEgyebBind(sortExpression, sortDirection);
    }

    protected void gridViewEgyebBind(String SortExpression, SortDirection SortDirection)
    {

        KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        KRT_CimekSearch search = (KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch());

        //Keres�si objektum konfigur�l�sa egy�b c�mekre
        KRT_CimekSearch searchEgyeb = new KRT_CimekSearch();
        searchEgyeb.Id = search.Id;
        searchEgyeb.CimTobbi = search.CimTobbi;
        searchEgyeb.ErvKezd = search.ErvKezd;
        searchEgyeb.ErvVege = search.ErvVege;

        //Tipus be�ll�t�s egy�b c�mekre
        if (String.IsNullOrEmpty(search.Tipus.Value) || (search.Tipus.Value == KodTarak.Cim_Tipus.Postai && search.Tipus.Operator == Query.Operators.equals))
        {
            searchEgyeb.Tipus.Value = KodTarak.Cim_Tipus.Postai;
            searchEgyeb.Tipus.Operator = Query.Operators.notequals;
        }
        else
        {
            searchEgyeb.Tipus = search.Tipus;
        }


        search.OrderBy = Search.GetOrderBy("gridViewEgyeb", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        searchEgyeb.OrderBy = search.OrderBy;
        searchEgyeb.TopRow = UI.GetTopRow(Page);

        ListHeader1.SelectedRecordId = EgyebCimekSelectedId;

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtension(ExecParam, searchEgyeb);

        UI.GridViewFill(gridViewEgyeb, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        UI.SetTabHeaderRowCountText(res, labelEgyeb);

        EgyebCimekSelectedId = ListHeader1.SelectedRecordId;
    }

    protected void gridViewEgyeb_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void gridViewEgyeb_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEgyeb, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewEgyeb_SelectRowCommand(id);
        }
    }

    private void gridViewEgyeb_SelectRowCommand(string cimId)
    {
        string id = cimId;
        if (!String.IsNullOrEmpty(id))
        {
            ActiveTabRefreshDetail(TabContainerDetail, id);

        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewEgyeb(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("CimekForm.aspx"
                 , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("CimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelPostai.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "KRT_Cimek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelPostai.ClientID);

            PartnerekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("PartnerCimekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.CimId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void gridViewEgyeb_PreRender(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyeb))
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, PostaiCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, EgyebCPE);

            ListHeader1.RefreshPagerLabel();

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEgyeb);

            EgyebCimekSelectedId = ListHeader1.SelectedRecordId;
        }

    }


    protected void updatePanelEgyeb_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyeb))
                    {
                        gridViewEgyebBind();
                        gridViewEgyeb_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewEgyeb));
                    }
                    break;
            }
        }
    }

    protected void gridViewEgyeb_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewEgyebBind(e.SortExpression, UI.GetSortToGridView("gridViewEgyeb", ViewState, e.SortExpression));
        ActiveTabClearDetail();
    }

    #endregion

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainerMaster.ActiveTab.Equals(tab))
        {
            ActiveTabRefreshDetail(TabContainerDetail, GetSelectedMasterRecordID());
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainerDetail_ActiveTabChanged(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelPostai))
        {
            if (gridViewPostai.SelectedIndex == -1)
            {
                return;
            }
        }
        if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyeb))
        {
            if (gridViewEgyeb.SelectedIndex == -1)
            {
                return;
            }
        }
        string cimId = GetSelectedMasterRecordID();
        ActiveTabRefreshDetail(sender as AjaxControlToolkit.TabContainer, cimId);
    }

    private void ActiveTabRefreshDetail(AjaxControlToolkit.TabContainer sender, string cimId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                PartnerekGridViewBind(cimId);
                PartnerekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClearDetail()
    {
        switch (TabContainerDetail.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(PartnerekGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        PartnerekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainerDetail.ActiveTab.Equals(TabPanelPartner))
        {
            PartnerekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(PartnerekGridView),GetSelectedMasterRecordID());
        }
    }

    #endregion


    #region Partnerek Detail

    private void PartnerekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_PartnerCimek();
            PartnerekGridViewBind(GetSelectedMasterRecordID());
        }
    }

    protected void PartnerekGridViewBind(string cimId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("PartnerekGridView", ViewState, "Partner_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("PartnerekGridView", ViewState);

        PartnerekGridViewBind(cimId, sortExpression, sortDirection);
    }

    protected void PartnerekGridViewBind(string cimId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(cimId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Cimek cim = new KRT_Cimek();
            cim.Id = cimId;
            KRT_PartnerCimekSearch search = new KRT_PartnerCimekSearch();
            search.OrderBy = Search.GetOrderBy("PartnerekGridView", ViewState, SortExpression, SortDirection);

            PartnerekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByCim(ExecParam, cim, search);

            UI.GridViewFill(PartnerekGridView, res, PartnerekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, labelPartnerHeader);

            ui.SetClientScriptToGridViewSelectDeSelectButton(PartnerekGridView);
        }
        else
        {
            ui.GridViewClear(PartnerekGridView);
        }
    }

    void PartnerekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        PartnerekGridViewBind(GetSelectedMasterRecordID());
    }

    protected void PartnerekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainerDetail.ActiveTab.Equals(TabPanelPartner))
                    //{
                    //    PartnerekGridViewBind(GetSelectedMasterRecordID());
                    //}
                    ActiveTabRefreshDetailList(TabPanelPartner);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a PartnerekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_PartnerCimek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PartnerCimInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(PartnerekGridView, EErrorPanel1, ErrorUpdatePanel);
            KRT_PartnerCimekService service = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void PartnerekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(PartnerekGridView, selectedRowNumber, "check");
        }
    }

    private void PartnerekGridView_RefreshOnClientClicks(string partnerCim_Id, string cimID)
    {
        string id = partnerCim_Id;
        if (!String.IsNullOrEmpty(id))
        {
            PartnerekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("PartnerCimekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            PartnerekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("PartnerCimekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id + "&" + QueryStringVars.CimId + "=" + cimID
                , Defaults.PopupWidth, Defaults.PopupHeight, PartnerekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void PartnerekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page , "cb_UtolsoHasznalatSikeres", "UtolsoHasznalatSiker");
    }

    protected void PartnerekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = PartnerekGridView.PageIndex;

        PartnerekGridView.PageIndex = PartnerekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != PartnerekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, PartnerekCPE);
            PartnerekGridViewBind(GetSelectedMasterRecordID());
        }
        else
        {
            UI.GridViewSetScrollable(PartnerekSubListHeader.Scrollable, PartnerekCPE);
        }
        PartnerekSubListHeader.PageCount = PartnerekGridView.PageCount;
        PartnerekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(PartnerekGridView);
    }


    void PartnerekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(PartnerekSubListHeader.RowCount);
        PartnerekGridViewBind(GetSelectedMasterRecordID());
    }

    protected void PartnerekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        PartnerekGridViewBind(GetSelectedMasterRecordID()
            , e.SortExpression, UI.GetSortToGridView("PartnerekGridView", ViewState, e.SortExpression));
    }

    #endregion

}
