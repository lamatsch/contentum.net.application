/* $Header: CimekLovList.aspx.cs, 24, 2017.06.09. 11:43:09, Németh Krisztina$ 
 */
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Text;
using System.Collections.Generic;
using Contentum.eUIControls;
using Contentum.eIntegrator.Service;
using Contentum.eIntegrator.Service.NAP.BusinessObjects;
using Contentum.eIntegrator.Service.NAP.Results;

public partial class CimekLovList : Contentum.eUtility.UI.PageBase
{
    private bool disable_refreshLovList = false;

    private Type _type = typeof(KRT_CimekSearch);

    private const string kodcsoportCim = "CIM_TIPUS";

    private string filterType = null;

    public const int tabIndexPostai = 0;
    public const int tabIndexEgyeb = 1;

    public const int paneIndexFull = 0;
    public const int paneIndexFast = 1;
    public const int paneIndexDetail = 2;

    private const string searchIndicatorText = "Keres�s v�grehajtva!";
    private const string noSerchResult = "A keres�snek nincs eredm�nye";

    string CimTipus { get; set; }

    private class Cim
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        private string delimeter;

        public string Delimeter
        {
            get { return delimeter; }
            set { delimeter = value; }
        }
        public Cim()
        {
        }
        public Cim(string text)
        {
            Text = text;
            Delimeter = ", ";
        }
        public Cim(string text, string delimeter)
        {
            Text = text;
            Delimeter = delimeter;
        }
    }

    private class CimCollection : IEnumerable
    {
        private List<Cim> items;
        public CimCollection()
        {
            items = new List<Cim>();
        }
        public void Add(string text, string delimeter)
        {
            items.Add(new Cim(text, delimeter));
        }
        public void Add(string text)
        {
            items.Add(new Cim(text));
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        #endregion
    }

    private string GetAppendedCim(DataRow row)
    {
        CimCollection cim = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";


        try
        {
            switch (row["Tipus"].ToString())
            {
                case KodTarak.Cim_Tipus.Postai:
                    string orszag = row["OrszagNev"].ToString();
                    cim.Add(orszag, delimeter);
                    string iranyitoszam = row["IRSZ"].ToString();
                    cim.Add(iranyitoszam, delimeter);
                    string telepules = row["TelepulesNev"].ToString();
                    cim.Add(telepules, delimeter);
                    // BLG_1347
                    //cim.Add(kozterulet, delimeterSpace);
                    string kozterulet = row["KozteruletNev"].ToString();
                    string hrsz = row["HRSZ"].ToString();
                    if (String.IsNullOrEmpty(kozterulet) && !String.IsNullOrEmpty(hrsz))
                    {
                        cim.Add("HRSZ.", delimeterSpace);
                        cim.Add(hrsz, delimeterSpace);
                    }
                    else
                    {
                        cim.Add(kozterulet, delimeterSpace);
                        string kozteruletTipus = row["KozteruletTipusNev"].ToString();
                        cim.Add(kozteruletTipus, delimeterSpace);
                        string hazszam = row["Hazszam"].ToString();
                        string hazszamIg = row["Hazszamig"].ToString();
                        string hazszamBetujel = row["HazszamBetujel"].ToString();
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cim.Add(hazszam, delimeter);
                        string lepcsohaz = row["Lepcsohaz"].ToString();
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cim.Add(lepcsohaz, delimeter);
                        string szint = row["Szint"].ToString();
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cim.Add(szint, delimeter);
                        string ajto = row["Ajto"].ToString();
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = row["AjtoBetujel"].ToString();
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cim.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Egyeb:
                    string Cim = row["CimTobbi"].ToString();
                    cim.Add(Cim, delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }

            string lastDelimeter = "";

            foreach (Cim item in cim)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception e)
        {
            throw e;
        }

        return text.ToString();
    }

    private void SetDefaultValues()
    {
        TelepulesTextBox1.Text = "";
        IranyitoszamTextBox1.Text = "";
        KozteruletTextBox1.Text = "";
        KozteruletTipusTextBox1.Text = "";

        KodtarakDropDownListTipus.SetSelectedValue("");
        textEgyebCim.Text = "";

        TabContainerDetail.ActiveTabIndex = tabIndexPostai;
        Accordion1.SelectedIndex = paneIndexFull;
    }

    private void SetNoSearchResultText()
    {
        if (ListBoxSearchResult.Items.Count == 0)
        {
            ListBoxSearchResult.Items.Add(new ListItem(noSerchResult, ""));
        }
        else
        {
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "CimekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //Accordion panel vez�rl�inek inicializ�l�sa, enn�lk�l nem hajland� rendesen m�k�dni, de hogy m�rt az rejt�ly
        ControlCollection controls = paneFullText.Controls;
        controls = paneDetail.Controls;
        controls = paneFast.Controls;

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        KodtarakDropDownListTipus.DropDownList.AutoPostBack = true;
        KodtarakDropDownListTipus.DropDownList.SelectedIndexChanged += new EventHandler(ButtonSearch_Click);

        registerJavascript();


        // CR3321 Partner r�gz�t�s csak keres�s ut�n
        string query = "&" + QueryStringVars.HiddenFieldId + "=" + CimHiddenField.ClientID
                  + "&" + QueryStringVars.TextBoxId + "=" + CimTextBox.ClientID;


        if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.TryFireChangeEvent)))
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        string jsdoPostback = "function NewCimCallback(){__doPostBack('" + CimTextBox.ClientID + @"','');}";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NewCimCallback", jsdoPostback, true);

        CimTipus = Request.QueryString.Get("CimTipus");

        //sz�r�s
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterType != null)
        {
            // CR3321 Partner r�gz�t�s csak keres�s ut�n
            ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx",
                                                QueryStringVars.Command + "=" + CommandName.New +
                                                "&" + QueryStringVars.Filter + "=" + filterType + query,
                                                Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID,
                                                EventArgumentConst.refresh, TabContainerDetail.ClientID);

            //ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType
            //, Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
          + JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType + query + "&" +
          QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);
            //ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            //+ JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Filter + "=" + filterType + "&" +
            //QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("CimekSearch.aspx", QueryStringVars.Filter + "=" + filterType, Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch, true);

            switch (filterType)
            {
                case KodTarak.Cim_Tipus.Postai:
                    LovListHeader1.HeaderTitle = Resources.LovList.CimekLovListHeaderTitle_Filtered_Postai;
                    TabContainerDetail.Tabs[tabIndexEgyeb].Visible = false;
                    labelEgyebHeader.Visible = false;
                    break;
                case KodTarak.Cim_Tipus.Egyeb:
                    LovListHeader1.HeaderTitle = Resources.LovList.CimekLovListHeaderTitle_Filtered_Egyeb;
                    TabContainerDetail.Tabs[tabIndexPostai].Visible = false;
                    labelPostaiHeader.Visible = false;
                    paneDetail.Visible = false;
                    break;
                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }
        }
        else
        {
            // CR3321 Partner r�gz�t�s csak keres�s ut�n
            //ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            //, Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonNew.OnClientClick = JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx",
                                                QueryStringVars.Command + "=" + CommandName.New +
                                                query,
                                                Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID,
                                                EventArgumentConst.refresh, TabContainerDetail.ClientID);

            //ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            //    + JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + "&" +
            //    QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);
            ButtonNewOnSelected.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
               + JavaScripts.SetOnClientClickWithActivePanelSet("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.New + query + "&" +
               QueryStringVars.Id + "=' + $get('" + ListBoxSearchResult.ClientID + "').value + '", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refresh, TabContainerDetail.ClientID);

            ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("CimekSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch, true);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
        + JavaScripts.SetOnClientClick("CimekForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);


        if (!IsPostBack)
        {
            KodtarakDropDownListTipus.FillAndSetEmptyValue(kodcsoportCim, LovListHeader1.ErrorPanel);
            ListItem itemPostai = KodtarakDropDownListTipus.DropDownList.Items.FindByValue(KodTarak.Cim_Tipus.Postai);
            KodtarakDropDownListTipus.DropDownList.Items.Remove(itemPostai);
            FillFiokokDropDownList();
        }

        if (!String.IsNullOrEmpty(CimTipus))
        {
            if (!IsPostBack)
            {
                List<string> cimTipusok = new List<string>(CimTipus.Split(','));

                //Accordion1.SelectedIndex = paneIndexFast;
                if (cimTipusok.Contains(KodTarak.Cim_Tipus.Postai))
                {
                    TabContainerDetail.ActiveTabIndex = tabIndexPostai;

                    if (CimTipus == KodTarak.Cim_Tipus.Postai)
                    {
                        TabPanelEgyeb.Enabled = false;
                    }
                }
                else
                {
                    TabContainerDetail.ActiveTabIndex = tabIndexEgyeb;
                    TabPanelPostai.Enabled = false;
                }

                FilterCimTipusok(KodtarakDropDownListTipus.DropDownList);
            }
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        labelSearchIndicator.Text = "";
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        if (Accordion1.SelectedIndex == paneHivataliKapu.TabIndex)
        {
            FillHivataliKapuSearchResult();
            return;
        }

        if (Accordion1.SelectedIndex == paneRNY.TabIndex)
        {
            FillRNYSearchResult();
            return;
        }

        IsHivataliKapuSearch = false;
        IsRNYSearch = false;

        ListBoxSearchResult.Items.Clear();

        KRT_CimekService service = eAdminService.ServiceFactory.GetKRT_CimekService();
        KRT_CimekSearch search = null;

        //a keres� formon eszk�zlt r�szletes keres�s eredm�nye
        if (searchObjectFromSession == true)
        {
            search = (KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch());
            TextBoxSearch.Text = "";
        }
        else
        {
            search = new KRT_CimekSearch();
            switch (Accordion1.SelectedIndex)
            {
                case paneIndexFull:
                    search.Tipus.Value = "";
                    search.Tipus.Operator = "";
                    search.TelepulesNev.Value = SearchKey;
                    search.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.TelepulesNev.Group = "1";
                    search.TelepulesNev.GroupOperator = Query.Operators.or;
                    search.OrszagNev.Value = SearchKey;
                    search.IRSZ.Value = SearchKey;
                    search.IRSZ.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.IRSZ.Group = "1";
                    search.IRSZ.GroupOperator = Query.Operators.or;
                    search.OrszagNev.Value = SearchKey;
                    search.OrszagNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.OrszagNev.Group = "1";
                    search.OrszagNev.GroupOperator = Query.Operators.or;
                    search.KozteruletNev.Value = SearchKey;
                    search.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.KozteruletNev.Group = "1";
                    search.KozteruletNev.GroupOperator = Query.Operators.or;
                    search.KozteruletTipusNev.Value = SearchKey;
                    search.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.KozteruletTipusNev.Group = "1";
                    search.KozteruletTipusNev.GroupOperator = Query.Operators.or;
                    search.Hazszam.Value = SearchKey;
                    search.Hazszam.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.Hazszam.Group = "1";
                    search.Hazszam.GroupOperator = Query.Operators.or;
                    search.CimTobbi.Value = SearchKey;
                    search.CimTobbi.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                    search.CimTobbi.Group = "1";
                    search.CimTobbi.GroupOperator = Query.Operators.or;
                    break;

                //r�szletes keres�s
                case paneIndexFast:
                    int deltaTabIndex = 0;
                    if (filterType != null)
                    {
                        if (filterType != KodTarak.Cim_Tipus.Postai)
                            deltaTabIndex = 1;

                    }
                    switch (TabContainerDetail.ActiveTabIndex + deltaTabIndex)
                    {
                        case tabIndexPostai:
                            search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                            search.Tipus.Operator = Query.Operators.equals;
                            if (true)
                            {
                                if (!String.IsNullOrEmpty(TelepulesTextBox1.Text))
                                {
                                    search.TelepulesNev.Value = TelepulesTextBox1.Text;
                                    search.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(TelepulesTextBox1.Text);
                                }
                                if (!String.IsNullOrEmpty(IranyitoszamTextBox1.Text))
                                {
                                    search.IRSZ.Value = IranyitoszamTextBox1.Text;
                                    search.IRSZ.Operator = Search.GetOperatorByLikeCharater(IranyitoszamTextBox1.Text);
                                }
                                if (!String.IsNullOrEmpty(KozteruletTextBox1.Text))
                                {
                                    search.KozteruletNev.Value = KozteruletTextBox1.Text;
                                    search.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTextBox1.Text);
                                }
                                if (!String.IsNullOrEmpty(KozteruletTipusTextBox1.Text))
                                {
                                    search.KozteruletTipusNev.Value = KozteruletTipusTextBox1.Text;
                                    search.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTipusTextBox1.Text);
                                }
                            }
                            break;
                        case tabIndexEgyeb:
                            if (String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
                            {
                                search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                                search.Tipus.Operator = Query.Operators.notequals;
                            }
                            else
                            {
                                search.Tipus.Value = KodtarakDropDownListTipus.SelectedValue;
                                search.Tipus.Operator = Query.Operators.equals;
                            }
                            if (!String.IsNullOrEmpty(textEgyebCim.Text))
                            {
                                search.CimTobbi.Value = textEgyebCim.Text;
                                search.CimTobbi.Operator = Search.GetOperatorByLikeCharater(textEgyebCim.Text);
                            }
                            break;
                    }
                    break;
            }
        }

        // ha kell, sz�r�nk a t�pusra, ak�r fel�lv�gva a r�szletes keres�sn�l megadott t�pust is
        if (filterType != null)
        {
            switch (filterType)
            {
                case KodTarak.Cim_Tipus.Postai:
                    search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                    search.Tipus.Operator = Query.Operators.equals;
                    break;
                case KodTarak.Cim_Tipus.Egyeb:
                    if (search.Tipus.Value == "" || search.Tipus.Value == KodTarak.Cim_Tipus.Postai)
                    {
                        search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                        search.Tipus.Operator = Query.Operators.notequals;
                    }
                    break;
                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }
        }

        if (!String.IsNullOrEmpty(this.CimTipus))
        {
            if (String.IsNullOrEmpty(search.Tipus.Operator))
            {
                search.Tipus.Value = GetCimTipusFilter();
                search.Tipus.Operator = Query.Operators.inner;
            }
            else
            {
                search.WhereByManual = String.Format(" and KRT_Cimek.Tipus in ({0})", GetCimTipusFilter());
            }
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        string defaultSortExpression = "TelepulesNev, KozteruletNev";

        if (String.IsNullOrEmpty(search.Tipus.Value))
        {
            search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
            search.Tipus.Operator = Query.Operators.equals;
            //search.OrderBy = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev,Hazszam";
            search.OrderBy = defaultSortExpression;
            Result result = service.GetAll(execParam, search);
            ListBoxFill(ListBoxSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, true);
            search.Tipus.Value = KodTarak.Cim_Tipus.Postai;
            search.Tipus.Operator = Query.Operators.notequals;
            search.OrderBy = "KodTarakCimTipus.Nev, CimTobbi";//"TipusNev, CimTobbi";
            result = service.GetAllWithExtension(execParam, search);
            ListBoxFill(ListBoxSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, false);

            trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        }
        else
        {
            Result result = new Result();
            if (search.Tipus.Value == KodTarak.Cim_Tipus.Postai && search.Tipus.Operator == Query.Operators.equals)
            {
                //search.OrderBy = "OrszagNev, TelepulesNev, KozteruletNev, KozteruletTipusNev,Hazszam";
                search.OrderBy = defaultSortExpression;
                result = service.GetAllWithExtension(execParam, search);
            }
            else
            {
                search.OrderBy = "KodTarakCimTipus.Nev, CimTobbi";//"TipusNev, CimTobbi";
                result = service.GetAllWithExtension(execParam, search);
            }
            ListBoxFill(ListBoxSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, true);

            trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        }

        SetNoSearchResultText();
    }

    public void ListBoxFill(ListBox listBox, Contentum.eBusinessDocuments.Result result, Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel, bool clear)
    {
        //if (listBox == null || result.Ds == null) return;
        if (listBox == null) return;

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
        }
        else
        {
            try
            {
                if (clear)
                    listBox.Items.Clear();

                DataSet ds = result.Ds;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string id = ((Guid)row["Id"]).ToString();
                    string text = GetAppendedCim(row);
                    listBox.Items.Add(new ListItem(text, id));
                }
            }
            catch (Exception e)
            {
                Logger.Error("ListBoxFill", e);
                ResultError.DisplayErrorOnErrorPanel(errorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), "");
            }
        }
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;
                string partnerNev = String.Empty;

                if (IsHivataliKapuSearch)
                {
                    selectedText = ParseHivataliKapuCimItemValue(selectedId, out partnerNev);
                    selectedId = String.Empty;
                }

                if (IsRNYSearch)
                {                    
                    string tipus = ParseRNYCimItemValue(selectedId, out selectedText);

                    using (KRT_CimekService svc = eAdminService.ServiceFactory.GetKRT_CimekService())
                    {
                        // Lek�rdezz�k van-e m�r ilyen c�m
                        KRT_CimekSearch krt_cimeksearch = new KRT_CimekSearch();
                        krt_cimeksearch.Tipus.Value = tipus;
                        krt_cimeksearch.Tipus.Operator = Query.Operators.equals;

                        krt_cimeksearch.CimTobbi.Value = selectedText;
                        krt_cimeksearch.CimTobbi.Operator = Query.Operators.equals;

                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result result = svc.GetAll(execParam, krt_cimeksearch);

                        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

                        if (resultError.IsError)
                        {
                            // Hiba kezeles
                            ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
                            if (LovListHeader1.ErrorUpdatePanel != null)
                            {
                                LovListHeader1.ErrorUpdatePanel.Update();
                            }
                            return;
                        }

                        if (result.HasData())
                        {
                            // Megtal�ltuk
                            selectedId = result.Ds.Tables[0].Rows[0]["Id"].ToString();
                        }
                        else
                        {
                            // Felvessz�k
                            KRT_Cimek krt_cimek = new KRT_Cimek();
                            krt_cimek.Kategoria = KodTarak.Cim_Kategoria.K;
                            krt_cimek.Tipus = tipus;
                            krt_cimek.CimTobbi = selectedText;
                            krt_cimek.Forras = KodTarak.Cim_Forras.RNY;

                            result = svc.Insert(execParam, krt_cimek);

                            resultError = new Contentum.eUtility.ResultError(result);

                            if (resultError.IsError)
                            {
                                // Hiba kezeles
                                ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
                                if (LovListHeader1.ErrorUpdatePanel != null)
                                {
                                    LovListHeader1.ErrorUpdatePanel.Update();
                                }
                                return;
                            }
                            else
                            {
                                selectedId = result.Uid; // Be�ll�tjuk az �jonnan felvett record azonos�t�j�t, ezzel t�r�nk majd vissza a h�v� oldalra
                            }
                        }

                    }
                }

                bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);

                if (IsHivataliKapuSearch)
                {
                    string partnerTextBoxId = Request.QueryString.Get("PartnerTextBoxId");
                    string partnerHiddenFieldId = Request.QueryString.Get("PartnerHiddenFieldId");
                    if (!String.IsNullOrEmpty(partnerTextBoxId) && !String.IsNullOrEmpty(partnerHiddenFieldId))
                    {
                        string partnerId = String.Empty;
                        string partnerText = partnerNev;
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(partnerHiddenFieldId, partnerId);
                        parameters.Add(partnerTextBoxId, partnerText);
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "PartnerReturnValues", true, tryFireChangeEvent, true);
                    }
                }
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        //SetDefaultValues();
                        labelSearchIndicator.Text = "";
                        FillListBoxSearchResult("", true);
                        labelSearchIndicator.Text = "Keres�s v�grehajtva.";
                    }
                    break;
                case EventArgumentConst.refresh:
                    if (!disable_refreshLovList)
                    {
                        FillListBoxSearchResult(TextBoxSearch.Text, false);
                    }
                    break;
            }
        }

        if (IsHivataliKapuSearch || IsRNYSearch)
        {
            ImageButtonReszletesAdatok.Enabled = false;
            ButtonNewOnSelected.Enabled = false;
            ButtonNew.Enabled = false;
        }
        else
        {
            ImageButtonReszletesAdatok.Enabled = true;
            ButtonNewOnSelected.Enabled = true;
            ButtonNew.Enabled = true;
        }
    }
    protected void TabContainerDetail_ActiveTabChanged(object sender, EventArgs e)
    {
        //FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void registerJavascript()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //autocomplete-ek f�gg�s�geinek be�ll�t�sa, postback ut�n contextkey �jra be�ll�t�sa
        JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //ir�ny�t�sz�m kit�lt�se
        //JavaScripts.SetTextWithFirstResult(TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //accordion postback ut�n helyes header st�lus megjelen�t�s
        JavaScripts.FixAccordionPane(Page, Accordion1.ClientID);

        //textbox-ok eset�n enter lenyom�s�ra keres�s, f�kusz meg�rz�se, kurzor a sz�veg v�g�re �ll�t�sa
        string jsOnKeyDown = "function CancelPopulating(sender,e){sender.remove_populating(CancelPopulating);e.set_cancel(true);}\n" +
            "function SetFocus(sender,e){sender.remove_endRequest(SetFocus);window.setTimeout(function() {var text = $get(sender.lastFocus); text.focus();\n" +
            "text.value = text.value;if(sender.autoComplete)$find(sender.autoComplete).add_populating(CancelPopulating);},1);};\n" +
            "function OnKeyDown(sender,ev) {var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;\n" +
            "if(AjaxControlToolkit.AutoCompleteBehavior) var autoComplete = Sys.UI.Behavior.getBehaviorsByType(sender,AjaxControlToolkit.AutoCompleteBehavior)[0];\n" +
            "if(k === Sys.UI.Key.enter && (!autoComplete || !autoComplete._popupBehavior.get_visible()))\n" +
            "{var prm = Sys.WebForms.PageRequestManager.getInstance();prm.lastFocus = sender.id;if(autoComplete) prm.autoComplete = autoComplete.get_id();\n" +
            "prm.add_endRequest(SetFocus);\n" +
            "" + ClientScript.GetPostBackEventReference(ButtonSearch, "") + ";}}";

        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "OnKeyDown", jsOnKeyDown, true);

        TextBoxSearch.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        textEgyebCim.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        TelepulesTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        IranyitoszamTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        KozteruletTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");
        KozteruletTipusTextBox1.TextBox.Attributes.Add("onkeydown", "OnKeyDown(this,event)");

    }


    // CR3321 Partner r�gz�t�s csak keres�s ut�n
    protected void CimTextBox_ValueChanged(object sender, EventArgs e)
    {
        string selectedId = CimHiddenField.Value;
        string selectedText = CimTextBox.Text;

        bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

        JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);
        JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, false);
        disable_refreshLovList = true;

    }

    #region Hivatali kapu

    bool IsHivataliKapuSearch
    {
        get
        {
            object o = ViewState["IsHivataliKapuSearch"];

            if (o != null)
            {
                return (bool)ViewState["IsHivataliKapuSearch"];
            }
            else
            {
                return false;
            }
        }
        set
        {
            ViewState["IsHivataliKapuSearch"] = value;
        }
    }

    protected void FillHivataliKapuSearchResult()
    {
        IsHivataliKapuSearch = true;
        if (TabContainerHivataliKapu.ActiveTabIndex == TabPanelHivatal.TabIndex)
        {
            FillHivatalSearchResult();
        }
        else
        {
            FillSzemelySearchResult();
        }


    }

    protected void FillHivatalSearchResult()
    {
        ListBoxSearchResult.Items.Clear();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        HivatalSzuroParameterek szuroParameterk = new HivatalSzuroParameterek();
        szuroParameterk.Nev = tbNev.Text;
        szuroParameterk.RovidNev = tbRovidNev.Text;
        szuroParameterk.NevOperator = ParseEnum<OperatorTipus>(ddNevOperator.SelectedValue, OperatorTipus.And);
        szuroParameterk.Tipus = ParseEnum<HivatalTipus>(ddCimTipus.SelectedValue, HivatalTipus.Mind);
        //szuroParameterk.TamogatottSzolgaltatasok = cbTamogatottSzolgaltatasok.Checked;
        szuroParameterk.TamogatottSzolgaltatasok = false;
        Result result = svc.HivatalokListajaFeldolgozas_Szurt(KR_Fiok.SelectedValue, szuroParameterk);

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (LovListHeader1.ErrorUpdatePanel != null)
            {
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
        else
        {
            try
            {
                HivatalokListajaValasz valasz = result.Record as HivatalokListajaValasz;

                if (valasz != null)
                {
                    foreach (var hivatal in valasz.HivatalokLista)
                    {
                        string id = CreateHivataliKapuCimItemValue(hivatal.KRID, hivatal.Nev);
                        string text = String.Format("{0} - {1} - {2}", hivatal.KRID, hivatal.Nev, hivatal.RovidNev);
                        ListBoxSearchResult.Items.Add(new ListItem(text, id));
                    }
                }

                SetNoSearchResultText();

            }
            catch (Exception e)
            {
                Logger.Error("FillHivatalSearchResult", e);
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), "");
            }
        }
    }

    protected void FillSzemelySearchResult()
    {
        ListBoxSearchResult.Items.Clear();

        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        AzonositasKerdes kerdes = new AzonositasKerdes();
        kerdes.TermeszetesSzemelyAzonosito = new hkpTermeszetesSzemelyAzonosito();

        #region sz�let�si n�v
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve = new NevAdat();
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.CsaladiNev = SzuletesiNevVezetekNev.Text;
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.UtoNev1 = SzuletesiNevUtonev1.Text;
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.UtoNev2 = SzuletesiNevUtonev2.Text;
        #endregion

        #region viselt n�v
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve = new NevAdat();
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.CsaladiNev = ViseltNevVezetekNev.Text;
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev1 = ViseltNevUtonev1.Text;
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev2 = ViseltNevUtonev2.Text;
        #endregion

        #region anyja neve
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve = new NevAdat();
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.CsaladiNev = AnyjaNeveVezetekNev.Text;
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev1 = AnyjaNeveUtonev1.Text;
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev2 = AnyjaNeveUtonev2.Text;
        #endregion  

        kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely = new hkpSzuletesiHely();
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Telepules = SzuletesiHely.Text;

        kerdes.TermeszetesSzemelyAzonosito.SzuletesiIdo = SzuletesDatuma.SelectedDate;

        Result result = svc.Azonositas(KR_Fiok.SelectedValue, kerdes);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result);
            return;
        }

        AzonositasValasz valasz = result.Record as AzonositasValasz;

        if (valasz != null)
        {
            if (valasz.HibaUzenet != null && !String.IsNullOrEmpty(valasz.HibaUzenet.Tartalom))
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, "Hiba", valasz.HibaUzenet.Tartalom);
                return;
            }

            foreach (Azonositott azonositott in valasz.AzonositottList)
            {
                string id = CreateHivataliKapuCimItemValue(azonositott.KapcsolatiKod, azonositott.Nev);
                string text = String.Format("{0} - {1} - {2}", azonositott.KapcsolatiKod, azonositott.Nev, azonositott.Email);
                ListBoxSearchResult.Items.Add(new ListItem(text, id));
            }

            SetNoSearchResultText();
        }
    }

    T ParseEnum<T>(string value, T defaultValue)
    {
        return (T)Enum.Parse(typeof(T), value);
    }

    void FillFiokokDropDownList()
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        string[] fiokok = svc.GetHivataliKapuFiokok();

        KR_Fiok.Items.Clear();
        foreach (string fiok in fiokok)
        {
            KR_Fiok.Items.Add(new ListItem(fiok, fiok));
        }
    }

    string CreateHivataliKapuCimItemValue(string cim, string nev)
    {
        return String.Format("{0}${1}", cim, nev);
    }

    string ParseHivataliKapuCimItemValue(string value, out string nev)
    {
        string cim = String.Empty;
        nev = String.Empty;

        if (!String.IsNullOrEmpty(value))
        {
            string[] parts = value.Split('$');
            cim = parts[0];

            if (parts.Length > 1)
                nev = parts[1];
        }

        return cim;
    }

    string CreateRNYCimItemValue(string tipus, string nev)
    {
        return String.Format("{0}${1}", tipus, nev);
    }

    string ParseRNYCimItemValue(string value, out string nev)
    {
        string tipus = String.Empty;
        nev = String.Empty;

        if (!String.IsNullOrEmpty(value))
        {
            string[] parts = value.Split('$');
            tipus = parts[0];

            if (parts.Length > 1)
                nev = parts[1];
        }

        return tipus;
    }

    #endregion

    #region RNY

    bool IsRNYSearch
    {
        get
        {
            object o = ViewState["IsRNYSearch"];

            if (o != null)
            {
                return (bool)ViewState["IsRNYSearch"];
            }
            else
            {
                return false;
            }
        }
        set
        {
            ViewState["IsRNYSearch"] = value;
        }
    }

    protected void FillRNYSearchResult()
    {
        ListBoxSearchResult.Items.Clear();

        IsRNYSearch = true;

        INT_NAPService service = eIntegratorService.ServiceFactory.GetINT_NAPService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        Szemely4TAdatok szemelyAdatok = SzemelyAdatokPanel.Get4TAdatok();

        AzonositoAdat azonositoAdat = new AzonositoAdat
        {
            SzemelyAdatok = szemelyAdatok            
        };

        AlapRendelkezesekResult result = service.GetAlapRendelkezesek(execParam, azonositoAdat);

        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (LovListHeader1.ErrorUpdatePanel != null)
            {
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
        else
        {
            try
            {
                List<KRT_Cimek> cimek = result.ElerhetosegekList;

                if (cimek != null)
                {
                    foreach (var cim in cimek)
                    {
                        string id = CreateRNYCimItemValue(cim.Tipus, cim.CimTobbi);
                        string text = GetRNYCimText(cim);
                        ListBoxSearchResult.Items.Add(new ListItem(text, id));
                    }
                }

                SetNoSearchResultText();

            }
            catch (Exception e)
            {
                Logger.Error("FillRNYSearchResult", e);
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"), "");
            }
        }
    }

    string GetRNYCimText(KRT_Cimek cim)
    {
        CimCollection cimCollection = new CimCollection();
        StringBuilder text = new StringBuilder("");
        string delimeter = ", ";
        string delimeterSpace = " ";

        try
        {
            switch (cim.Tipus)
            {
                case KodTarak.Cim_Tipus.Postai_Cim_RNY:
                    string iranyitoszam = cim.IRSZ;
                    cimCollection.Add(iranyitoszam, delimeter);
                    string telepules = cim.TelepulesNev;
                    cimCollection.Add(telepules, delimeter);
                    // BLG_1347
                    //cim.Add(kozterulet, delimeterSpace);
                    string kozterulet = cim.KozteruletNev;
                    string hrsz = cim.HRSZ;
                    if (String.IsNullOrEmpty(kozterulet) && !String.IsNullOrEmpty(hrsz))
                    {
                        cimCollection.Add("HRSZ.", delimeterSpace);
                        cimCollection.Add(hrsz, delimeterSpace);
                    }
                    else
                    {
                        cimCollection.Add(kozterulet, delimeterSpace);
                        string kozteruletTipus = cim.KozteruletTipusNev;
                        cimCollection.Add(kozteruletTipus, delimeterSpace);
                        string hazszam = cim.Hazszam;
                        string hazszamIg = cim.Hazszamig;
                        string hazszamBetujel = cim.HazszamBetujel;
                        if (!String.IsNullOrEmpty(hazszamIg))
                            hazszam += "-" + hazszamIg;
                        if (!String.IsNullOrEmpty(hazszamBetujel))
                            hazszam += "/" + hazszamBetujel;
                        cimCollection.Add(hazszam, delimeter);
                        string lepcsohaz = cim.Lepcsohaz;
                        if (!String.IsNullOrEmpty(lepcsohaz))
                            lepcsohaz += " l�pcs�h�z";
                        cimCollection.Add(lepcsohaz, delimeter);
                        string szint = cim.Szint;
                        if (!String.IsNullOrEmpty(szint))
                            szint += ". emelet";
                        cimCollection.Add(szint, delimeter);
                        string ajto = cim.Ajto;
                        if (!String.IsNullOrEmpty(ajto))
                        {
                            string ajtoBetujel = cim.AjtoBetujel;
                            if (!String.IsNullOrEmpty(ajtoBetujel))
                                ajto += "/" + ajtoBetujel;
                            ajto += " ajt�";
                        }
                        cimCollection.Add(ajto, delimeter);
                    }
                    break;

                case KodTarak.Cim_Tipus.Telefonszam_RNY:
                case KodTarak.Cim_Tipus.Email_Cim_RNY:
                    cimCollection.Add(cim.CimTobbi, delimeter);
                    break;

                default:
                    goto case KodTarak.Cim_Tipus.Telefonszam_RNY;
            }

            string lastDelimeter = "";

            foreach (Cim item in cimCollection)
            {
                if (!String.IsNullOrEmpty(item.Text))
                {
                    text.Append(item.Text);
                    text.Append(item.Delimeter);
                    lastDelimeter = item.Delimeter;
                }
            }

            if (text.Length >= lastDelimeter.Length)
                text = text.Remove(text.Length - lastDelimeter.Length, lastDelimeter.Length);
        }
        catch (Exception e)
        {
            throw e;
        }

        return text.ToString();
    }

    #endregion

    string GetCimTipusFilter()
    {
        string filter = String.Empty;

        if (!String.IsNullOrEmpty(CimTipus))
        {
            foreach (string tipus in CimTipus.Split(','))
            {
                if (!String.IsNullOrEmpty(filter))
                    filter += ",";

                filter += String.Format("'{0}'", tipus);
            }
        }

        return filter;
    }

    void FilterCimTipusok(DropDownList dropDownList)
    {
        if (!String.IsNullOrEmpty(CimTipus))
        {
            List<string> cimTipusok = new List<string>(CimTipus.Split(','));

            for (int i = dropDownList.Items.Count - 1; i >= 0; i--)
            {
                ListItem item = dropDownList.Items[i];

                if (!String.IsNullOrEmpty(item.Value))
                {
                    if (!cimTipusok.Contains(item.Value))
                    {
                        dropDownList.Items.Remove(item);
                    }
                }
            }
        }
    }
}
