using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class PartnercimekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_CimekSearch);

    private const string kodcsoportCim = "CIM_TIPUS";
    public const int tabIndexPostai = 0;
    public const int tabIndexEgyeb = 1;

    public const int paneIndexFull = 0;
    public const int paneIndexDetail = 1;

    private string filterType = null;

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");
        public static readonly ListItem NotSet = new ListItem(Resources.Form.EmptyListItem, "X");
        public static void FillDropDownList(DropDownList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(NotSet);
            list.Items.Add(Yes);
            list.Items.Add(No);
            SetSelectedValue(list, selectedValue);
        }
        public static void FillDropDownList(DropDownList list)
        {
            FillDropDownList(list, NotSet.Value);
        }
        public static bool isBoolStringValue(string value)
        {
            if (value == Yes.Value || value == No.Value || value == NotSet.Value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SetSelectedValue(DropDownList list, string selectedValue)
        {
            if (isBoolStringValue(selectedValue))
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = NotSet.Value;
            }
        }
        public static void SetSearchField(Field field, string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                field.Value = value;
                field.Operator = Query.Operators.equals;
            }
            else
            {
                field.Value = "";
                field.Operator = "";
            }
        }
    }
    
    protected void SetActivePanel(KRT_CimekSearch cimekSearch)
    {
        string cimTipus = Request.QueryString.Get(QueryStringVars.CimTipusKod) ?? KodTarak.Cim_Tipus.Default;

        switch (cimekSearch.Tipus.Value)
        {
            case KodTarak.Cim_Tipus.Postai:
                if (cimekSearch.Tipus.Operator == Query.Operators.equals)
                {
                    Accordion1.SelectedIndex = paneIndexDetail;
                    TabContainerDetail.ActiveTabIndex = tabIndexPostai;
                }
                else
                {
                    goto case KodTarak.Cim_Tipus.Egyeb;
                }
                break;

            case KodTarak.Cim_Tipus.Egyeb:
                Accordion1.SelectedIndex = paneIndexDetail;
                if (filterType == null)
                    TabContainerDetail.ActiveTabIndex = tabIndexEgyeb;
                break;

            //ha �res a cimekSearch objektum, akkor a h�v� form �ll�tja az akt�v tab-ot
            case "":
                Accordion1.SelectedIndex = paneIndexFull;
                switch (cimTipus)
                {
                    case KodTarak.Cim_Tipus.Postai:
                        TabContainerDetail.ActiveTabIndex = tabIndexPostai;
                        break;

                    case KodTarak.Cim_Tipus.Egyeb:
                        if (filterType == null)
                            TabContainerDetail.ActiveTabIndex = tabIndexEgyeb;
                        break;

                    default:
                        goto case KodTarak.Cim_Tipus.Egyeb;
                }
                break;

            default:
                goto case KodTarak.Cim_Tipus.Egyeb;
        }
    }


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
        
        //Accordion panel vez�rl�inek inicializ�l�sa, enn�lk�l nem hajland� rendesen m�k�dni, de hogy m�rt az rejt�ly
        ControlCollection controls = paneFullText.Controls;
        controls = paneDetail.Controls;

        registerJavascripts();

        //sz�r�s
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterType != null)
        {

            switch (filterType)
            {
                case KodTarak.Cim_Tipus.Postai:
                    SearchHeader1.HeaderTitle = Resources.Search.PartnercimekSearchHeaderTitle_Filtered_Postai;
                    TabContainerDetail.Tabs[tabIndexEgyeb].Visible = false;
                    labelEgyebHeader.Visible = false;
                    break;
                case KodTarak.Cim_Tipus.Egyeb:
                    SearchHeader1.HeaderTitle = Resources.Search.PartnercimekSearchHeaderTitle_Filtered_Egyeb;
                    TabContainerDetail.Tabs[tabIndexPostai].Visible = false;
                    labelPostaiHeader.Visible = false;
                    break;
                default:
                    goto case KodTarak.Cim_Tipus.Egyeb;
            }
        }

    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KRT_CimekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_CimekSearch)Search.GetSearchObject(Page, new KRT_CimekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject,bool resetState)
    {
        KRT_CimekSearch _KRT_CimekSearch = (KRT_CimekSearch)searchObject;

        if (_KRT_CimekSearch != null)
        {
            if (!resetState)
            {
                SetActivePanel(_KRT_CimekSearch);
                //A c�m t�pus dropdown lista felt�lt�se
                KodtarakDropDownListTipus.FillAndSetEmptyValue(kodcsoportCim, SearchHeader1.ErrorPanel);
                ListItem itemPostai = KodtarakDropDownListTipus.DropDownList.Items.FindByValue(KodTarak.Cim_Tipus.Postai);
                KodtarakDropDownListTipus.DropDownList.Items.Remove(itemPostai);
                //Mindk�t oldal drop down list-j�nek felt�lt�se
                BoolString.FillDropDownList(ddownMinketOldal);
            }
            switch (Accordion1.SelectedIndex)
            {
                case paneIndexFull:
                    {
                        textKifejezes.Text = _KRT_CimekSearch.Nev.Value;

                        if (resetState && Accordion1.SelectedIndex == paneIndexFull)
                        {
                            goto case paneIndexDetail;
                        }

                        break;
                    }
                case paneIndexDetail:
                    {
                        //postai tab
                        OrszagTextBox1.Text = _KRT_CimekSearch.OrszagNev.Value;
                        TelepulesTextBox1.Text = _KRT_CimekSearch.TelepulesNev.Value;
                        IranyitoszamTextBox1.Text = _KRT_CimekSearch.IRSZ.Value;
                        KozteruletTextBox1.Text = _KRT_CimekSearch.KozteruletNev.Value;
                        KozteruletTipusTextBox1.Text = _KRT_CimekSearch.KozteruletTipusNev.Value;
                        BoolString.SetSelectedValue(ddownMinketOldal, _KRT_CimekSearch.MindketOldal.Value);
                        RequiredNumberBoxHsz.Text = _KRT_CimekSearch.Hazszam.Value;
                        textHazszamBetujel.Text = _KRT_CimekSearch.HazszamBetujel.Value;
                        textLepcsohaz.Text = _KRT_CimekSearch.Lepcsohaz.Value;
                        textEmelet.Text = _KRT_CimekSearch.Szint.Value;
                        textAjto.Text = _KRT_CimekSearch.Ajto.Value;
                        textAjtoBetujel.Text = _KRT_CimekSearch.AjtoBetujel.Value;
                        textHelyrajziSzam.Text = _KRT_CimekSearch.HRSZ.Value;
                        textEgyeb.Text = _KRT_CimekSearch.Tobbi.Value;
                        RequiredNumberBoxHszIg.Text = _KRT_CimekSearch.Hazszamig.Value;
                        //egy�b tab
                        if (_KRT_CimekSearch.Tipus.Value == KodTarak.Cim_Tipus.Postai)
                            KodtarakDropDownListTipus.SetSelectedValue("");
                        else
                            KodtarakDropDownListTipus.SetSelectedValue(_KRT_CimekSearch.Tipus.Value);
                        textEgyebCimek.Text = _KRT_CimekSearch.CimTobbi.Value;

                        //k�z�s keres� felt�tel, �rv�nyess�g
                        Ervenyesseg_SearchFormComponent1.SetDefault(
                           _KRT_CimekSearch.ErvKezd, _KRT_CimekSearch.ErvVege);

                        if (resetState && Accordion1.SelectedIndex == paneIndexDetail)
                        {
                             goto case paneIndexFull;
                        }

                        break;
                    }
            }

        }
    }

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        LoadComponentsFromSearchObject(searchObject, false);
    }
    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_CimekSearch SetSearchObjectFromComponents()
    {
        KRT_CimekSearch _KRT_CimekSearch = (KRT_CimekSearch)SearchHeader1.TemplateObject;

        if (_KRT_CimekSearch == null)
        {
            _KRT_CimekSearch = new KRT_CimekSearch();
        }



        switch(Accordion1.SelectedIndex)
        {
            case paneIndexFull:
                {
                    _KRT_CimekSearch.Tipus.Value = "";
                    _KRT_CimekSearch.Tipus.Operator = "";
                    _KRT_CimekSearch.Nev.Value = textKifejezes.Text;
                    string SearchKey = textKifejezes.Text;
                     _KRT_CimekSearch.TelepulesNev.Value = SearchKey;
                     _KRT_CimekSearch.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.TelepulesNev.Group = "1";
                     _KRT_CimekSearch.TelepulesNev.GroupOperator = Query.Operators.or;
                     _KRT_CimekSearch.OrszagNev.Value = SearchKey;
                     _KRT_CimekSearch.IRSZ.Value = SearchKey;
                     _KRT_CimekSearch.IRSZ.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.IRSZ.Group = "1";
                     _KRT_CimekSearch.IRSZ.GroupOperator = Query.Operators.or;
                     _KRT_CimekSearch.OrszagNev.Value = SearchKey;
                     _KRT_CimekSearch.OrszagNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.OrszagNev.Group = "1";
                     _KRT_CimekSearch.OrszagNev.GroupOperator = Query.Operators.or;
                     _KRT_CimekSearch.KozteruletNev.Value = SearchKey;
                     _KRT_CimekSearch.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.KozteruletNev.Group = "1";
                     _KRT_CimekSearch.KozteruletNev.GroupOperator = Query.Operators.or;
                     _KRT_CimekSearch.KozteruletTipusNev.Value = SearchKey;
                     _KRT_CimekSearch.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.KozteruletTipusNev.Group = "1";
                     _KRT_CimekSearch.KozteruletTipusNev.GroupOperator = Query.Operators.or;
                     _KRT_CimekSearch.Hazszam.Value = SearchKey;
                     _KRT_CimekSearch.Hazszam.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.Hazszam.Group = "1";
                     _KRT_CimekSearch.Hazszam.GroupOperator = Query.Operators.or;
                     _KRT_CimekSearch.CimTobbi.Value = SearchKey;
                     _KRT_CimekSearch.CimTobbi.Operator = Search.GetOperatorByLikeCharater(SearchKey);
                     _KRT_CimekSearch.CimTobbi.Group = "1";
                     _KRT_CimekSearch.CimTobbi.GroupOperator = Query.Operators.or;
                    break;
                }
            case paneIndexDetail:
                {
                    //postai tab
                    if (filterType == null || filterType == KodTarak.Cim_Tipus.Postai)
                    {
                        _KRT_CimekSearch.OrszagNev.Value = OrszagTextBox1.Text;
                        _KRT_CimekSearch.OrszagNev.Operator = Search.GetOperatorByLikeCharater(OrszagTextBox1.Text);
                        _KRT_CimekSearch.TelepulesNev.Value = TelepulesTextBox1.Text;
                        _KRT_CimekSearch.TelepulesNev.Operator = Search.GetOperatorByLikeCharater(TelepulesTextBox1.Text);
                        _KRT_CimekSearch.IRSZ.Value = IranyitoszamTextBox1.Text;
                        if (!String.IsNullOrEmpty(IranyitoszamTextBox1.Text))
                            _KRT_CimekSearch.IRSZ.Operator = Query.Operators.equals;
                        _KRT_CimekSearch.KozteruletNev.Value = KozteruletTextBox1.Text;
                        _KRT_CimekSearch.KozteruletNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTextBox1.Text);
                        _KRT_CimekSearch.KozteruletTipusNev.Value = KozteruletTipusTextBox1.Text;
                        _KRT_CimekSearch.KozteruletTipusNev.Operator = Search.GetOperatorByLikeCharater(KozteruletTipusTextBox1.Text);
                        BoolString.SetSearchField(_KRT_CimekSearch.MindketOldal, ddownMinketOldal.SelectedValue);
                        _KRT_CimekSearch.Hazszam.Value = RequiredNumberBoxHsz.Text;
                        if (!String.IsNullOrEmpty(RequiredNumberBoxHsz.Text))
                            _KRT_CimekSearch.Hazszam.Operator = Query.Operators.equals;
                        _KRT_CimekSearch.Hazszamig.Value = RequiredNumberBoxHszIg.Text;
                        if (!String.IsNullOrEmpty(RequiredNumberBoxHszIg.Text))
                            _KRT_CimekSearch.Hazszamig.Operator = Query.Operators.equals;
                        _KRT_CimekSearch.HazszamBetujel.Value = textHazszamBetujel.Text;
                        if (!String.IsNullOrEmpty(textHazszamBetujel.Text))
                            _KRT_CimekSearch.HazszamBetujel.Operator = Query.Operators.equals;
                        _KRT_CimekSearch.Lepcsohaz.Value = textLepcsohaz.Text;
                        _KRT_CimekSearch.Lepcsohaz.Operator = Search.GetOperatorByLikeCharater(textLepcsohaz.Text);
                        _KRT_CimekSearch.Szint.Value = textEmelet.Text;
                        if (!String.IsNullOrEmpty(textEmelet.Text))
                            _KRT_CimekSearch.Szint.Operator = Query.Operators.equals;
                        _KRT_CimekSearch.Ajto.Value = textAjto.Text;
                        _KRT_CimekSearch.Ajto.Operator = Search.GetOperatorByLikeCharater(textAjto.Text);
                        _KRT_CimekSearch.AjtoBetujel.Value = textAjtoBetujel.Text;
                        if (!String.IsNullOrEmpty(textAjtoBetujel.Text))
                            _KRT_CimekSearch.AjtoBetujel.Operator = Query.Operators.equals;
                        _KRT_CimekSearch.HRSZ.Value = textHelyrajziSzam.Text;
                        _KRT_CimekSearch.HRSZ.Operator = Search.GetOperatorByLikeCharater(textHelyrajziSzam.Text);
                        _KRT_CimekSearch.Tobbi.Value = textEgyeb.Text;
                        _KRT_CimekSearch.Tobbi.Operator = Search.GetOperatorByLikeCharater(textEgyeb.Text);
                    }


                    //egy�b tab
                    if (filterType == null || filterType != KodTarak.Cim_Tipus.Postai)
                    {
                        _KRT_CimekSearch.CimTobbi.Value = textEgyebCimek.Text;
                        _KRT_CimekSearch.CimTobbi.Operator = Search.GetOperatorByLikeCharater(textEgyebCimek.Text);
                    }

                    //k�z�s r�sz
                    Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
                         _KRT_CimekSearch.ErvKezd, _KRT_CimekSearch.ErvVege);

                    //t�pus be�ll�t�sa
                    if (TabContainerDetail.ActiveTab.Equals(TabPanelPostai))
                    {
                        if (!Search.IsIdentical(_KRT_CimekSearch, GetDefaultSearchObject(false)))
                        {
                            _KRT_CimekSearch.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                            _KRT_CimekSearch.Tipus.Operator = Query.Operators.equals;
                        }
                        else
                        {
                            _KRT_CimekSearch.Tipus.Value = "";
                            _KRT_CimekSearch.Tipus.Operator = "";
                        }
                    }
                    else
                    {
                        if (!Search.IsIdentical(_KRT_CimekSearch, GetDefaultSearchObject(false)))
                        {
                            if (!String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
                            {
                                _KRT_CimekSearch.Tipus.Value = KodtarakDropDownListTipus.SelectedValue;
                                _KRT_CimekSearch.Tipus.Operator = Query.Operators.equals;
                            }
                            else
                            {
                                _KRT_CimekSearch.Tipus.Value = KodTarak.Cim_Tipus.Postai;
                                _KRT_CimekSearch.Tipus.Operator = Query.Operators.notequals;
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(KodtarakDropDownListTipus.SelectedValue))
                            {
                                _KRT_CimekSearch.Tipus.Value = KodtarakDropDownListTipus.SelectedValue;
                                _KRT_CimekSearch.Tipus.Operator = Query.Operators.equals;
                            }
                            else
                            {
                                _KRT_CimekSearch.Tipus.Value = "";
                                _KRT_CimekSearch.Tipus.Operator = "";
                            }
                        }
                    }
                }
                break;

        }


        return _KRT_CimekSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject(),true);
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_CimekSearch searchObject = SetSearchObjectFromComponents();
            int returnValue = -1;
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            //A kiv�lasztott keres�si panel �tad�sa visszat�t�si �rt�kk�nt
            if (Accordion1.SelectedIndex == paneIndexDetail)
                returnValue = TabContainerDetail.ActiveTabIndex;

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();

            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);

            //Ha van �tadva TabContainer query string param�ter, akkor visszat�r a kiv�lasztott tab �rt�k�vel
            if (Request.QueryString["TabContainer"] == null)
                JavaScripts.RegisterCloseWindowClientScript(Page);
            else
                JavaScripts.RegisterCloseWindowClientScript(Page, returnValue);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_CimekSearch GetDefaultSearchObject(bool resetTemplate)
    {
        if (resetTemplate)
            SearchHeader1.TemplateReset();
        return new KRT_CimekSearch();
    }

    private KRT_CimekSearch GetDefaultSearchObject()
    {
        return GetDefaultSearchObject(true);
    }


    protected void registerJavascripts()
    {
        //f�gg�s�gek
        JavaScripts.SetAutoCompleteContextKey(Page, OrszagTextBox1.TextBox, TelepulesTextBox1.TextBox, TelepulesTextBox1.AutoCompleteExtenderClientID);
        JavaScripts.SetAutoCompleteContextKey(Page, TelepulesTextBox1.TextBox,IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //az els� ir�ny�t�sz�m automatikus be�r�sa
        //JavaScripts.SetTextWithFirstResult(TelepulesTextBox1.TextBox, IranyitoszamTextBox1.TextBox, IranyitoszamTextBox1.AutoCompleteExtenderClientID);

        //zIndex
        JavaScripts.SetCompletionListszIndex(Page);

        //fix accordion pane
        JavaScripts.FixAccordionPane(Page, Accordion1.ClientID);

    }
}
