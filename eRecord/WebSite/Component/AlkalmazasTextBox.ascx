﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlkalmazasTextBox.ascx.cs" Inherits="Component_AlkalmazasTextBox" %>

<div class="DisableWrap">
<asp:TextBox ID="AlkalmazasNev" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="AlkalmazasNev" Display="None" 
ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>" Enabled="false">
</asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>