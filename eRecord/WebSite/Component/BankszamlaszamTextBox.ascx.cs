using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

public partial class Component_BankszamlaszamTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    private bool isSixteenCharsFormatEnabled = true;

    private const string BANKSZAMLASZAM_ELLENORZO_VEKTOR_PART1 = "9731973";
    private const string BANKSZAMLASZAM_ELLENORZO_VEKTOR_PART2 = "973197319731973";

    private const string ValidChars_Input = "0123456789";
    private const string ValidChars_Select = "0123456789";
    private const string ValidChars_Search = "0123456789*";

    #region public properties

    public enum ModeType { Input, Select, Search };

    public ModeType Mode {
        get
        {
            if (ViewState["Mode"] != null)
            {
                return (ModeType)ViewState["Mode"];
            }
            return ModeType.Input;
        }

        set {
            ViewState["Mode"] = value;
        }
    }

    private bool customTextEnabled = false;

    public bool CustomTextEnabled
    {
        get { return customTextEnabled; }
        set { customTextEnabled = value; }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public bool ValidateFormat
    {
        set { CustomValidator_Format.Enabled = value; }
        get { return CustomValidator_Format.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            TextBox1.ValidationGroup = value;
            TextBox2.ValidationGroup = value;
            TextBox3.ValidationGroup = value;
        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public string Text
    {
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                string txt = value;
                TextBox1.Text = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
                txt = txt.Remove(0, Math.Min(txt.Length, 8));
                TextBox2.Text = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
                txt = txt.Remove(0, Math.Min(txt.Length, 8));
                TextBox3.Text = txt.Substring(0, Math.Min(txt.Length, 8)).Trim();
            }
            else {
                TextBox1.Text = String.Empty;
                TextBox2.Text = String.Empty;
                TextBox3.Text = String.Empty;
            }
        }
        get
        {
            char paddingChar = ' ';
            StringBuilder sb = new StringBuilder();
            if (Mode != ModeType.Search)
            {
                if (!String.IsNullOrEmpty(TextBox3.Text))
                {
                    sb.Append(TextBox1.Text.PadRight(8, paddingChar));
                    sb.Append(TextBox2.Text.PadRight(8, paddingChar));
                    sb.Append(TextBox3.Text);
                }
                else if (!String.IsNullOrEmpty(TextBox2.Text))
                {
                    sb.Append(TextBox1.Text.PadRight(8, paddingChar));
                    sb.Append(TextBox2.Text);
                }
                else
                {
                    sb.Append(TextBox1.Text);
                }
            }
            else
            {
                // Search m�dban lehetnek benne *-ok, �s nem kell padding
                sb.Append(TextBox1.Text);
                sb.Append(TextBox2.Text);
                sb.Append(TextBox3.Text);
            }
            return sb.ToString();
        }
    }

    public TextBox TextBox
    {
        get { return TextBox1; }
    }

    public bool Enabled
    {
        set
        {
            TextBox1.Enabled = value;
            TextBox2.Enabled = value;
            TextBox3.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return TextBox1.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            TextBox1.ReadOnly = value;
            TextBox2.ReadOnly = value;
            TextBox3.ReadOnly = value;
            LovImageButton.Visible = !value;
            NewImageButton.Visible = !value;
            ResetImageButton.Visible = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;

        }
        get { return TextBox1.ReadOnly; }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    Component_EditablePartnerTextBox _PartnerTextBox = null;
    public Component_EditablePartnerTextBox PartnerTextBox
    {
        set {_PartnerTextBox = value; }
        get { return _PartnerTextBox; }
    }

    public string Partner_Id
    {
        set {
            if (this._PartnerTextBox != null)
                this._PartnerTextBox.Id_HiddenField = value;
        }
        get {
            if (this._PartnerTextBox != null)
                return this._PartnerTextBox.Id_HiddenField;
            return null;
        }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }


    public string CssClass
    {
        get
        {
           return TextBox1.CssClass;
        }
        set
        {
            TextBox1.CssClass = value;
            TextBox2.CssClass = value;
            TextBox3.CssClass = value;
        }
    }

    private bool _tryFireChangeEvent = true;

    public bool TryFireChangeEvent
    {
        get { return _tryFireChangeEvent; }
        set { _tryFireChangeEvent = value; }
    }

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }
    #endregion

    //public bool isBankszamlaszamChanged
    //{
    //    get { return TextBox1.Text != HiddenField1.Value; }
    //}

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }

    #endregion
    /// <summary>
    /// Ha meg volt adva partner Id, lek�ri az ahhoz tartoz� banksz�mlasz�mot
    /// </summary>
    /// <param name="errorPanel"></param>
    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_BankszamlaszamokService service = eAdminService.ServiceFactory.GetKRT_BankszamlaszamokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);

            if (result.IsError && errorPanel != null)
            {
                // ha hiba volt, �res lesz
                Text = HiddenField1.Value = "";
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
            else
            {
                KRT_Bankszamlaszamok krt_Bankszamlaszamok = (KRT_Bankszamlaszamok)result.Record;
                Text = krt_Bankszamlaszamok.Bankszamlaszam;            
            }

        }
        else
        {
            Text = HiddenField1.Value = "";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        switch (this.Mode)
        {
            case ModeType.Search:
                fteNumber1.ValidChars = ValidChars_Search;
                fteNumber2.ValidChars = ValidChars_Search;
                fteNumber3.ValidChars = ValidChars_Search;
                break;
            case ModeType.Input:
                fteNumber1.ValidChars = ValidChars_Input;
                fteNumber2.ValidChars = ValidChars_Input;
                fteNumber3.ValidChars = ValidChars_Input;
                break;
            case ModeType.Select:
                fteNumber1.ValidChars = ValidChars_Select;
                fteNumber2.ValidChars = ValidChars_Select;
                fteNumber3.ValidChars = ValidChars_Select;
                break;
        }


        switch (this.Mode)
        {   case ModeType.Search:
                ValidateFormat = false;
                ResetImageButton.Visible = true;

                customTextEnabled = true;
                goto case ModeType.Select;
            case ModeType.Input:
                NewImageButton.Visible = false;
                LovImageButton.Visible = false;
                ViewImageButton.Visible = false;
                ResetImageButton.Visible = false;

                ajaxEnabled = false;
                AutoCompleteExtender1.Enabled = false;
                customTextEnabled = true;
                break;
            case ModeType.Select:
                {
                    ResetImageButton.Visible = true;

                    string queryString = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                    + "&" + QueryStringVars.TextBoxId + "=" + TextBox.ClientID
                    + "&" + QueryStringVars.TextBoxId + "2=" + TextBox2.ClientID
                    + "&" + QueryStringVars.TextBoxId + "3=" + TextBox3.ClientID;

                    if (_tryFireChangeEvent)
                    {
                        queryString += "&" + QueryStringVars.TryFireChangeEvent + "=1";
                    }

                    if (PartnerTextBox != null)
                    {
                        queryString += "&" + QueryStringVars.PartnerId + "=" + "'+ $get('" + PartnerTextBox.Control_HiddenField.ClientID + "').value +'";
                    }

                    OnClick_Lov = JavaScripts.SetOnClientClick("BankszamlaszamokLovList.aspx", queryString
                                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                    string queryString_New = CommandName.Command + "=" + CommandName.New
                       + "&" + queryString;

                    OnClick_New = JavaScripts.SetOnClientClick("BankszamlaszamokForm.aspx",
                        CommandName.Command + "=" + CommandName.New + "&" + queryString
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                    OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                            "BankszamlaszamokForm.aspx", "", HiddenField1.ClientID);

                    ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
                    + TextBox1.ClientID + "').value = '';$get('"
                    + TextBox2.ClientID + "').value = '';$get('"
                    + TextBox3.ClientID + "').value = '';return false";

                    if (!ajaxEnabled)
                    {
                        //ASP.NET 2.0 bug work around
                        TextBox.Attributes.Add("readonly", "readonly");
                        TextBox2.Attributes.Add("readonly", "readonly");
                        TextBox3.Attributes.Add("readonly", "readonly");
                        AutoCompleteExtender1.Enabled = false;
                        WorldJumpEnabled = false;
                    }
                    else
                    {
                        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

                        string[] arrayContextKey = new string[3];

                        arrayContextKey[0] = FelhasznaloProfil.FelhasznaloId(Page);
                        arrayContextKey[1] = FelhasznaloProfil.LoginUserId(Page);
                        arrayContextKey[2] = this.Partner_Id ?? "";

                        AutoCompleteExtender1.ContextKey = string.Join(";", arrayContextKey);

                        //Manu�lis be�r�s eset�n az id t�rl�se
                        string jsClearHiddenField = "var clear=true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
                            + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

                        TextBox.Attributes.Add("onchange", jsClearHiddenField);
                        TextBox2.Attributes.Add("onchange", jsClearHiddenField);
                        TextBox3.Attributes.Add("onchange", jsClearHiddenField);
                    }
                }
                break;

        }

        ////ASP.NET 2.0 bug work around
        //TextBox.Attributes.Add("readonly", "readonly");
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);
        componentList.Add(TextBox2);
        componentList.Add(TextBox3);
 
        // Lekell tiltani a ClientValidatort
        Validator1.Enabled = false;
        CustomValidator_Format.Enabled = false;

        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);

            if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
            {
                Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                if (c != null)
                {
                    c.Visible = Validate;
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.BankszamlaszamBehavior", this.TextBox.ClientID);

        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("TextBoxPart2ClientId", this.TextBox2.ClientID);
        descriptor.AddProperty("TextBoxPart3ClientId", this.TextBox3.ClientID);

        string[] CheckVektor = {BANKSZAMLASZAM_ELLENORZO_VEKTOR_PART1,BANKSZAMLASZAM_ELLENORZO_VEKTOR_PART2 };
        if (CheckVektor != null && CheckVektor.Length > 0)
            descriptor.AddProperty("CheckVektor", CheckVektor);

        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("ChecksumValidatorId", CustomValidator_Format.ClientID);
        descriptor.AddProperty("ChecksumValidatorCalloutId", ValidatorCalloutExtender_Format.ClientID);
        descriptor.AddProperty("IsSixteenCharsFormatEnabled", isSixteenCharsFormatEnabled);

        descriptor.AddProperty("UserId", FelhasznaloProfil.FelhasznaloId(Page));
        descriptor.AddProperty("LoginId", FelhasznaloProfil.LoginUserId(Page));

        descriptor.AddProperty("CustomTextEnabled", this.CustomTextEnabled);

        if (this._PartnerTextBox != null)
        {
            descriptor.AddProperty("PartnerBehaviorId", _PartnerTextBox.BehaviorID);
        }

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/BankszamlaszamBehavior.js";

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
