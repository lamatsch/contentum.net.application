﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BizottsagTextBox.ascx.cs" Inherits="Component_BizottsagTextBox" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>

<div class="DisableWrap">
    <asp:TextBox ID="CsoportMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
    <asp:ImageButton TabIndex="-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif"
        onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
        CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" />
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true"
        MinimumPrefixLength="2" TargetControlID="CsoportMegnevezes" CompletionSetCount="20"
        ContextKey="" UseContextKey="true" FirstRowSelected="true" ServicePath="http://localhost:100/eAdminWebService/KRT_CsoportokService.asmx"
        ServiceMethod="GetCsoportokList" CompletionListItemCssClass="GridViewRowStyle"
        CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle">
    </ajaxToolkit:AutoCompleteExtender>
    <eUI:WorldJumpExtender ID="WorldJumpExtender1" runat="server" TargetControlID="CsoportMegnevezes"
        AutoCompleteExtenderId="AutoCompleteExtender1" />
</div>
