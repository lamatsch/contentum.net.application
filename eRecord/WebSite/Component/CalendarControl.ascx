<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarControl.ascx.cs" Inherits="Component_CalendarControl" %>
<%@ Register Src="~/Component/DateTimeCompareValidator.ascx" TagName="DateTimeCompareValidator" TagPrefix="uc"%>
<div class="DisableWrap">
<table cellpadding="0" cellspacing="0">
  <tr>
   <td>
    <asp:TextBox ID="DateTextBox" runat="server" CssClass="mrUrlapCalendar"></asp:TextBox>
   </td>
   <td>
    <asp:ImageButton TabIndex = "-1"
        ID="CalendarImageButton1" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
        OnClientClick="return false;" CausesValidation="false"/>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
        runat="server" ControlToValidate="DateTextBox" Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
        <ajaxToolkit:CalendarExtender ID="CalendarExtender1"
            runat="server" PopupButtonID="CalendarImageButton1" TargetControlID="DateTextBox" PopupPosition="BottomLeft">
        </ajaxToolkit:CalendarExtender>
       <asp:CustomValidator ID="DateFormatValidator" runat="server" 
           ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
           ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"  
           EnableClientScript="true" ControlToValidate="DateTextBox" Display="None"
           onservervalidate="DateFormatValidator_ServerValidate">
       </asp:CustomValidator>
       <ajaxToolkit:ValidatorCalloutExtender ID="DateFormatValidatorExtender" runat="server" TargetControlID="DateFormatValidator">
       <Animations>
        <OnShow>
        <Sequence>
            <HideAction Visible="true" />
            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
        </Sequence>    
        </OnShow>
       </Animations>
       </ajaxToolkit:ValidatorCalloutExtender>
       <uc:DateTimeCompareValidator runat="server" ControlToValidate="DateTextBox"
       HourControlToValidate="HourTextBox" MinuteControlToValidate="MinuteTextBox" ID="DateTimeCompareValidator1" Enabled="false"/>
        <%--LZS--BUG_5036
            �j DateTimeCompareValidator hozz�ad�sa amiatt, hogy a mai napra is �s a felad�s d�tum�ra is tudjunk valid�ci�t csin�lni.--%> 
       <uc:DateTimeCompareValidator runat="server" ControlToValidate="DateTextBox"
       HourControlToValidate="HourTextBox" MinuteControlToValidate="MinuteTextBox" ID="DateTimeCompareValidator2" Enabled="false"/>
        <%--LZS--BUG_5036--%>
    </td>
    <td>
        <asp:Panel runat="server" ID="TimePanel" CssClass="mrUrlapInputTimePanel" Visible="false">
	  <table cellspacing="0" cellpadding="0" border="0" rules="none" style="display:inline;">
		<tr>
		<td style="white-space:nowrap">
	   <span class="mrUrlapTimeWrapper">
            	<asp:TextBox ID="HourTextBox" runat="server" CssClass="mrUrlapInputTime" MaxLength="2"></asp:TextBox>
             	<asp:CustomValidator 
			ID="HourCustomValidator"
			runat="server"
			Display="None"
			EnableClientScript="true"
            		ControlToValidate="HourTextBox"	
			ErrorMessage="Az �ra megad�s helytelen form�tum�.">
		</asp:CustomValidator>
            	<span class="mrUrlapTimeSeparator">:</span>
            	<asp:TextBox ID="MinuteTextBox" runat="server" CssClass="mrUrlapInputTime" MaxLength="2"></asp:TextBox>
            	<asp:CustomValidator
			ID="MinuteCustomValidator"
			runat="server"
			Display="None"
			EnableClientScript="true"
            		ControlToValidate="MinuteTextBox"
			ErrorMessage="A perc megad�sa helytelen form�tum�.">
		</asp:CustomValidator>
            </span>
	    </td>
		<td class="mrUrlapTimeArrowWrapper">
                <asp:ImageButton TabIndex = "-1" ID="ArrowUpImageButton" runat="server" cssclass="mrUrlapTimeArrowUp" ImageUrl="~/images/hu/egyeb/time_up.gif" OnClientClick="return false;" BorderStyle="None" BorderWidth="1px"/><br/>
                <asp:ImageButton TabIndex = "-1" ID="ArrowDownImageButton" runat="server" cssclass="mrUrlapTimeArrowDown" ImageUrl="~/images/hu/egyeb/time_down.gif" OnClientClick="return false;" BorderStyle="None" BorderWidth="1px"/>
		</td>
		</tr>
            </table>
        </asp:Panel>
     </td>
    </tr>
</table>
            

    <ajaxToolkit:ValidatorCalloutExtender  ID="ValidatorCalloutExtender2" runat="server" TargetControlID="HourCustomValidator">
    <Animations>
        <OnShow>
        <Sequence>
            <HideAction Visible="true" />
            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
        </Sequence>    
        </OnShow>
    </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:ValidatorCalloutExtender  ID="ValidatorCalloutExtender3" runat="server" TargetControlID="MinuteCustomValidator">
    <Animations>
        <OnShow>
        <Sequence>
            <HideAction Visible="true" />
            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
        </Sequence>    
        </OnShow>
    </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>                        
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
        TargetControlID="RequiredFieldValidator1">
          <Animations>
                <OnShow>
                <Sequence>
                    <HideAction Visible="true" />
                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                </Sequence>    
                </OnShow>
          </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>