using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eUIControls;

public partial class Component_DataSetDropDownList : System.Web.UI.UserControl
{
    private readonly ListItem emptyListItem = new ListItem(Resources.Form.EmptyListItem, "");
    private string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public eDropDownList DropDownList
    {
        get { return DataSet_DropDownList; }
    }

    public String SelectedValue
    {
        get { return DataSet_DropDownList.SelectedValue; }
    }

    public bool Enabled
    {
        get { return DataSet_DropDownList.Enabled; }
        set { DataSet_DropDownList.Enabled = value; }
    }

    public Unit Width
    {
        get { return DataSet_DropDownList.Width; }
        set { DataSet_DropDownList.Width = value; }
    }

    public string CssClass
    {
        get { return DataSet_DropDownList.CssClass; }
        set { DataSet_DropDownList.CssClass = value; }
    }

    public string ValidationGroup
    {
        set
        {
            DataSet_DropDownList.ValidationGroup = value;
        }
        get { return DataSet_DropDownList.ValidationGroup; }
    }

    /// <summary>
    /// Felt�lti a DropDownList-et a megadott k�dcsoporthoz tartoz� (�rv�nyes) k�dt�r�rt�kekkel
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="errorPanel"></param>
    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        DataSet_DropDownList.Items.Clear();

        if (Res.Ds.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < Res.Ds.Tables[0].Rows.Count; i++)
            {
                DataSet_DropDownList.Items.Add(new ListItem(Res.Ds.Tables[0].Rows[i][DisplayColumnName].ToString(), Res.Ds.Tables[0].Rows[i][IdColumnName].ToString()));
            }

            if (addEmptyItem)
            {
                DataSet_DropDownList.Items.Insert(0, emptyListItem);
            }            
        }
    }

    public void FillDropDownList(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName, false, errorPanel);
    }

/*    public void FillWithOneValue(string kodcsoport, string kodtar, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(kodtar))
        {
            KRT_KodTarakService service = eAdminService.ServiceFactory.GetKRT_KodTarakService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = service.GetAllByKodcsoportKod(execParam, kodcsoport);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                DataSet_DropDownList.Items.Clear();
                if (result.Ds.Tables[0].Rows.Count > 0)
                {
                    bool isEqualKodtar = false;
                    foreach (DataRow row in result.Ds.Tables[0].Rows)
                    {
                        String kodtarKod = row["Kod"].ToString();
                        if (kodtarKod == kodtar)
                        {
                            String kodtarNev = row["Nev"].ToString();
                            DataSet_DropDownList.Items.Add(new ListItem(kodtarNev, kodtarKod));
                            isEqualKodtar = true;
                            break;
                        }
                    }
                    if (!isEqualKodtar)
                    {
                        DataSet_DropDownList.Items.Insert(0, new ListItem(kodtar + " " + deletedValue, kodtar));
                    }
                }
                else
                {
                    DataSet_DropDownList.Items.Insert(0, new ListItem(kodtar + " " + deletedValue, kodtar));
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            DataSet_DropDownList.Items.Insert(0, emptyListItem);
        }

    }*/

    /// <summary>
    /// Felt�lti a list�t, �s be�ll�tja az �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="kodcsoport"></param>
    /// <param name="selectedValue"></param>
    /// <param name="addEmptyItem"></param>
    /// <param name="errorPanel"></param>
    
    public void FillAndSetSelectedValue(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, String selectedValue, bool addEmptyItem, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillDropDownList(Res, IdColumnName, DisplayColumnName, addEmptyItem, errorPanel);
        ListItem selectedListItem = DataSet_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            DataSet_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            DataSet_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
        }
    }

    public void FillAndSetSelectedValue(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, String selectedValue, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(Res, IdColumnName, DisplayColumnName, selectedValue, false, errorPanel);
    }

    public void FillAndSetEmptyValue(Contentum.eBusinessDocuments.Result Res, String IdColumnName, String DisplayColumnName, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        FillAndSetSelectedValue(Res, IdColumnName, DisplayColumnName, emptyListItem.Value, true, errorPanel);
    }


    /// <summary>
    /// Be�ll�tja a m�r felt�lt�tt lista �rt�k�t a megadottra, ha az l�tezik. 
    /// Ha nem l�tezik, �j elemk�nt felv�ve, �s megjel�lve, hogy ez egy nem l�tez� k�dt�r�rt�k
    /// </summary>
    /// <param name="selectedValue"></param>
    public void SetSelectedValue(String selectedValue)
    {
        ListItem selectedListItem = DataSet_DropDownList.Items.FindByValue(selectedValue);
        if (selectedListItem != null)
        {
            DataSet_DropDownList.SelectedValue = selectedValue;
        }
        else
        {
            DataSet_DropDownList.Items.Insert(0, new ListItem(selectedValue + " " + deletedValue, selectedValue));
        }
    }




    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(DropDownList);

        return componentList;
    }

    private bool _ViewEnabled = true;

    public bool ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            if (!_ViewEnabled)
            {
                Enabled = _ViewEnabled;
                DropDownList.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    bool _ViewVisible = true;

    public bool ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                DropDownList.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion
}
