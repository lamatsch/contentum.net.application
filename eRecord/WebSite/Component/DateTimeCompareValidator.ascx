﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateTimeCompareValidator.ascx.cs" Inherits="Component_DateTimeCompareValidator" %>
<asp:CustomValidator ID="DateTimeCompareCustomValidator" runat="server"
    SetFocusOnError="true" EnableClientScript="true"   ClientValidationFunction="Utility.Calendar.ValidateDateTimeCompare"
    Display="None" OnServerValidate="DateTimeCompareCustomValidator_ServerValidate" ValidateEmptyText="false" ErrorMessage="A dátum összehasonlítás hiba!">
</asp:CustomValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="DateTimeCompareCustomValidatorExtender" runat="server"
    TargetControlID="DateTimeCompareCustomValidator">
    <Animations>
        <OnShow>
        <Sequence>
            <HideAction Visible="true" />
            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
        </Sequence>    
        </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
