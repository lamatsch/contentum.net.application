﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;

public partial class Component_DateTimeCompareValidator : System.Web.UI.UserControl, IScriptControl
{
    public string ControlToValidate
    {
        get
        {
            return DateTimeCompareCustomValidator.ControlToValidate;
        }
        set
        {
            DateTimeCompareCustomValidator.ControlToValidate = value;
        }
    }

    private string _HourControlToValidate;

    public string HourControlToValidate
    {
        get { return _HourControlToValidate; }
        set { _HourControlToValidate = value; }
    }

    private string _MinuteControlToValidate;

    public string MinuteControlToValidate
    {
        get { return _MinuteControlToValidate; }
        set { _MinuteControlToValidate = value; }
    }

    public bool Enabled
    {
        get
        {
            return DateTimeCompareCustomValidator.Visible;
        }
        set
        {
            DateTimeCompareCustomValidator.Visible = value;
        }
    }

    public bool IsValid
    {
        get
        {
            return DateTimeCompareCustomValidator.IsValid;
        }
        set
        {
            DateTimeCompareCustomValidator.IsValid = value;
        }
    }

    public void Validate()
    {
        DateTimeCompareCustomValidator.Validate();
    }

    private ValidationCompareOperator _operator = ValidationCompareOperator.GreaterThanEqual;

    public ValidationCompareOperator Operator
    {
        get
        {
            return _operator;
        }
        set
        {
            _operator = value;
        }
    }

    public CustomValidator DateCompareCustomValdator
    {
        get
        {
            return DateTimeCompareCustomValidator;
        }
    }

    private string _valueToCompare = DateTime.Now.ToShortDateString();

    public string ValueToCompare
    {
        get
        {
            return _valueToCompare;
        }
        set
        {
            DateTime dt;
            if (DateTime.TryParse(value, out dt))
            {
                _valueToCompare = value;
            }
        }
    }

    public string ValidationGroup
    {
        get
        {
            return DateTimeCompareCustomValidator.ValidationGroup;
        }
        set
        {
            DateTimeCompareCustomValidator.ValidationGroup = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Contentum.eUtility.JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //LZS - BUG_5036
        if (!string.IsNullOrEmpty(this.DateCompareCustomValdator.ErrorMessage))
            return;

        switch (Operator)
        {
            case ValidationCompareOperator.GreaterThan:
                DateTimeCompareCustomValidator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_Future;
                break;
            case ValidationCompareOperator.GreaterThanEqual:
                DateTimeCompareCustomValidator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_PresentFuture;
                break;
            case ValidationCompareOperator.LessThan:
                DateTimeCompareCustomValidator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_Past;
                break;
            case ValidationCompareOperator.LessThanEqual:
                DateTimeCompareCustomValidator.ErrorMessage = Resources.Form.DateTimeCompareValidationMessage_PresentPast;
                break;
        }
    }

    protected void DateTimeCompareCustomValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string inputValue = args.Value;
        string compareValue = ValueToCompare;
        if (!String.IsNullOrEmpty(inputValue) && !String.IsNullOrEmpty(compareValue))
        {
            DateTime inputDate;
            DateTime compareDate;
            if (DateTime.TryParse(inputValue, out inputDate) && DateTime.TryParse(compareValue, out compareDate))
            {
                args.IsValid = CompareValues(inputDate, compareDate, Operator);
            }
        }
    }

    private bool CompareValues(DateTime op1, DateTime op2, ValidationCompareOperator _operator)
    {
        switch (_operator)
        {
            case ValidationCompareOperator.NotEqual:
                return op1 != op2;
            case ValidationCompareOperator.GreaterThan:
                return op1 > op2;
            case ValidationCompareOperator.GreaterThanEqual:
                return op1 >= op2;
            case ValidationCompareOperator.LessThan:
                return op1 < op2;
            case ValidationCompareOperator.LessThanEqual:
                return op1 <= op2;
            default:
                return op1 == op2;
        }
    }

    public override Control FindControl(string id)
    {
        Control c = null;
        c = base.FindControl(id);
        if (c == null)
        {
            c = base.NamingContainer.FindControl(id);
        }
        return c;
    }

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (Enabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");

                sm.RegisterScriptControl(this);

            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (Enabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        string tempControlToValidate = ControlToValidate;
        ControlToValidate = String.Empty;
        base.Render(writer);
        ControlToValidate = tempControlToValidate;

        if (Enabled)
        {
            if (!this.DesignMode)
            {
                Control controlToValidate = FindControl(ControlToValidate);
                string controlToValidateClientID = String.Empty;
                if (controlToValidate != null)
                {
                    controlToValidateClientID = controlToValidate.ClientID;
                }

                ScriptManager.RegisterExpandoAttribute(DateTimeCompareCustomValidator, DateTimeCompareCustomValidator.ClientID, "controltovalidate", controlToValidateClientID, true);
                ScriptManager.RegisterExpandoAttribute(DateTimeCompareCustomValidator, DateTimeCompareCustomValidator.ClientID, "operator", Operator.ToString(), true);
                ScriptManager.RegisterExpandoAttribute(DateTimeCompareCustomValidator, DateTimeCompareCustomValidator.ClientID, "valuetocompare", ValueToCompare, true);

                string js = "ValidatorValidate(" + DateTimeCompareCustomValidator.ClientID + ");";

                if (!String.IsNullOrEmpty(HourControlToValidate))
                {
                    Control hourControl = FindControl(HourControlToValidate);
                    if (hourControl != null && hourControl.Visible)
                    {

                        ScriptManager.RegisterExpandoAttribute(DateTimeCompareCustomValidator, DateTimeCompareCustomValidator.ClientID, "HourControlToValidate", hourControl.ClientID, true);
                        if (hourControl is WebControl)
                        {
                            //(hourControl as WebControl).Attributes["onchange"] = js;
                            //(hourControl as WebControl).Attributes["onfocus"] = js;
                        }
                    }
                }

                if (!String.IsNullOrEmpty(MinuteControlToValidate))
                {
                    Control minuteControl = FindControl(MinuteControlToValidate);
                    if (minuteControl != null && minuteControl.Visible)
                    {
                        ScriptManager.RegisterExpandoAttribute(DateTimeCompareCustomValidator, DateTimeCompareCustomValidator.ClientID, "MinuteControlToValidate", minuteControl.ClientID, true);
                        if (minuteControl is WebControl)
                        {
                            //(minuteControl as WebControl).Attributes["onchange"] = js;
                            //(minuteControl as WebControl).Attributes["onfocus"] = js;
                        }
                    }
                }
            }
        }
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        return null;
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!Enabled)
            return null;

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Calendar.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}