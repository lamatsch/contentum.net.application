﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatumIdoIntervallum_SearchCalendarControl.ascx.cs" Inherits="Component_DatumIdoIntervallum_SearchCalendarControl" %>
<%@ Register Src="CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>

<span class="DisableWrap">
    <table>
        <tr>
            <td>
                <cc:CalendarControl ID="DatumKezd" runat="server" ReadOnly="false" Validate="false" TimeVisible="true"
                    InitHourValueWhenEmpty="0" InitMinuteValueWhenEmpty="01" />
            </td>
            <td>
                <asp:ImageButton TabIndex="-1" ID="datumKezd_masol_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/right-arrow.jpg" AlternateText="Másol" /></td>
            <td>
                <cc:CalendarControl ID="DatumVege" runat="server" ReadOnly="false" Validate="false" TimeVisible="true"
                    InitHourValueWhenEmpty="23" InitMinuteValueWhenEmpty="59" />
            </td>
        </tr>
    </table>
</span>