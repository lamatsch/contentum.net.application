<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatumIntervallum_SearchCalendarControl.ascx.cs"
    Inherits="Component_DatumIntervallum_SearchCalendarControl" %>
<span class="DisableWrap">
    <asp:TextBox ID="datumKezd_TextBox" runat="server" CssClass="datummezo" MaxLength="11"></asp:TextBox>
    <asp:ImageButton TabIndex = "-1" ID="datumKezd_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
        OnClientClick="return false;" />
    <asp:ImageButton TabIndex = "-1" ID="datumKezd_masol_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/right-arrow.jpg"
        AlternateText="M�sol" />
    <asp:TextBox ID="datumVege_TextBox" runat="server" CssClass="datummezo" MaxLength="11"></asp:TextBox>
    <asp:ImageButton TabIndex = "-1" ID="datumVege_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
        OnClientClick="return false;" />
    <asp:CheckBox ID="AktualisEv_ChBox" runat="server" Text="Aktu�lis �v" Visible="false"/>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="datumKezd_ImageButton"
        TargetControlID="datumKezd_TextBox" OnClientShown="OnClientShown">
    </ajaxToolkit:CalendarExtender>
    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="datumVege_ImageButton"
        TargetControlID="datumVege_TextBox" OnClientShown="OnClientShown">
    </ajaxToolkit:CalendarExtender>
    <%--BUG_4651--%>
    <%--Kirakva common.js-be--%>
    <%--<script type="text/javascript">
        function OnClientShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10005;
        }
    </script>--%>
    <asp:RequiredFieldValidator ID="DatumKezd_RequiredFieldValidator" runat="server" SetFocusOnError="true" ControlToValidate="datumKezd_TextBox"
        Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
        TargetControlID="DatumKezd_RequiredFieldValidator">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow></Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    <asp:RequiredFieldValidator ID="DatumVege_RequiredFieldValidator" runat="server" SetFocusOnError="true" ControlToValidate="datumVege_TextBox"
        Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
        TargetControlID="DatumVege_RequiredFieldValidator">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow></Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="datumKezd_TextBox" SetFocusOnError="true"
        ControlToValidate="datumVege_TextBox" Display="None" ErrorMessage="A d�tum v�ge nem lehet a d�tum kezdete el�tti id�pont!"
        Operator="GreaterThanEqual" Type="Date" Visible="False"></asp:CompareValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
        TargetControlID="CompareValidator1">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>   

    <asp:CompareValidator ID="CompareValidator_OnlyPast" runat="server" SetFocusOnError="true"
        ControlToValidate="datumVege_TextBox" Display="None" ErrorMessage="A d�tum v�g�nek kisebbnek kell lennie a jelenlegi d�tumn�l!"
        Operator="LessThan" Type="Date" Visible="False" Enabled="false"></asp:CompareValidator>
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="server"
        TargetControlID="CompareValidator_OnlyPast">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>   
    
<%--    <asp:CompareValidator ID="CompareValidator_DateFormat_Kezd" runat="server"
    Type="Date" Operator="DataTypeCheck"
    ControlToValidate="datumKezd_TextBox" ErrorMessage="A d�tum form�tuma nem megfelel�!"
    Display="None" Visible="false" Enabled="false" SetFocusOnError="true" />--%>
    <asp:CustomValidator ID="CustomValidator_DateFormat_Kezd" runat="server" 
           ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
           ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"  
           EnableClientScript="true" ControlToValidate="datumKezd_TextBox" Display="None"
    />
        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_DateFormat_Kezd" runat="server"
        TargetControlID="CustomValidator_DateFormat_Kezd">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
<%--    <asp:CompareValidator ID="CompareValidator_DateFormat_Vege" runat="server"
    Type="Date" Operator="DataTypeCheck"
    ControlToValidate="datumVege_TextBox" ErrorMessage="A d�tum form�tuma nem megfelel�!"
    Display="None" Visible="false" Enabled="false" SetFocusOnError="true" />--%>
    <asp:CustomValidator ID="CustomValidator_DateFormat_Vege" runat="server" 
           ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
           ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"  
           EnableClientScript="true" ControlToValidate="datumVege_TextBox" Display="None"
    />
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_DateFormat_Vege" runat="server"
        TargetControlID="CustomValidator_DateFormat_Vege">
            <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
        </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</span>
