﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DownloadDocumentAsZip.ascx.cs" Inherits="Component_DownloadDocumentAsZip" %>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-ui.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/JavaScripts/json2.js") %>"></script>

<script type="text/javascript">
    //$(function () {
    //    downloadDocumentsAsZip('58cc58fa-36eb-e911-80db-00155d027e33', "almafa");
    //});
    /**
     * downloadDocumentsAsZip
     * @param inIdList
     * @param inFileName
     */
    function downloadDocumentsAsZip(inIdList, inFileName) {
        if (inIdList === null || inIdList === '' || inIdList === 'undefined')
            return;

        if (inFileName === null || inFileName === '') {
            inFileName = getFormattedTime();
        }
        setProgressbar('block');
        try {
            // use this transport for "binary" data type
            $.ajaxTransport("+binary", function (options, originalOptions, jqXHR) {
                // check for conditions and support for blob / arraybuffer response type
                if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob))))) {
                    return {
                        // create new XMLHttpRequest
                        send: function (headers, callback) {
                            // setup all variables
                            var xhr = new XMLHttpRequest(),
                                url = options.url,
                                type = options.type,
                                async = options.async || true,
                                // blob or arraybuffer. Default is blob
                                dataType = options.responseType || "blob",
                                data = options.data || null,
                                username = options.username || null,
                                password = options.password || null;

                            xhr.addEventListener('load', function () {
                                var data = {};
                                data[options.dataType] = xhr.response;
                                // make callback and send data
                                callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                            });

                            xhr.open(type, url, async, username, password);

                            // setup custom headers
                            for (var i in headers) {
                                xhr.setRequestHeader(i, headers[i]);
                            }

                            xhr.responseType = dataType;
                            xhr.send(data);
                        },
                        abort: function () {
                            jqXHR.abort();
                        }
                    };
                }
            });

            $.ajax({
                url: '<%=ResolveUrl("~/GetDocumentContent.aspx/GetAllDocumentsInZip") %>' +'?' + $.now(),
                data: JSON.stringify({
                    expFelhasznaloId: $("[id$=HiddenField_Svc_FelhasznaloId]").val(),
                    expSzervezetId: $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val(),
                    ids: inIdList,
                    name: inFileName,
                    outZipFileName: null,
                }),
                dataType: "binary",
                responseType: 'blob',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                processData: false,
                beforeSend: function (jqXHR) {
                    // set request headers here rather than in the ajax 'headers' object
                    //jqXHR.setRequestHeader('Accept-Encoding', 'gzip');
                },
                success: function (data, textStatus, jqXHR) {
                    //debugger; 
                    //console.log(data);
                    //console.log(textStatus);
                    //console.log(jqXHR);

                    if (!data || data.size < 1) {
                        setProgressbar('none');
                        console.log(data);
                        alert('Sikertelen letöltés !');
                        return;
                    }
                    var downloadName = inFileName + getFormattedTime() + '.zip';
                    if (window.navigator.msSaveOrOpenBlob) {
                        //alert('IE ' + data.size);
                        try {
                            var blob = getBlobFromResponse(data);

                            $('#aDownloadDocumentAsZip').click(function () {
                                window.navigator.msSaveOrOpenBlob(blob, downloadName);
                            });
                            $('#aDownloadDocumentAsZip').click();

                            $('#aDownloadDocumentAsZip').unbind('click');
                        } catch (e) {
                            //console.log(e);
                        }

                    } else {
                        //console.log('OTHER BROWSER');
                        try {

                            var blob = getBlobFromResponse(data);

                            var a = window.document.createElement('a');
                            url = window.URL.createObjectURL(blob);
                            a.href = url;
                            a.download = downloadName;
                            document.body.appendChild(a);
                            a.click();
                            document.body.removeChild(a);
                        } catch (e) {
                            //console.log(e);
                        }
                    }
                    setProgressbar('none');
                },
                complete: function (jqXHR, textStatus) {
                    //console.log(textStatus);
                    //console.log(jqXHR.status);
                    //console.log(jqXHR.responseText);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    setProgressbar('none');
                    alert(errorThrown);
                },
                failure: function (response) {
                    setProgressbar('none');
                    showResponseError(response);
                }
            });
        } catch (e) {
            setProgressbar('none');
            console.log(e);
        }
    }

    /**
     * getBlobFromResponse
     * @param datad
     */
    function getBlobFromResponse(datad) {
        var blob = new Blob([datad]);
        return blob;
    }

    function showResponseError(response) {
        if (response != null && response.responseJSON != null && response.responseJSON.Message != null) {
            alert(response.responseJSON.Message);
        }
        else {
            alert(response);
        }
    }
    /**
     * getFormattedTime
     * */
    function getFormattedTime() {
        var today = new Date();
        var y = today.getFullYear();
        // JavaScript months are 0-based.
        var m = today.getMonth() + 1;
        var d = today.getDate();
        var h = today.getHours();
        var mi = today.getMinutes();
        var s = today.getSeconds();
        return "_" + y + "-" + m + "-" + d + "-" + h + "-" + mi + "-" + s;
    }
    /**
     * setProgressbar
     * @param display
     */
    function setProgressbar(display) {
        var progress = $get('ctl00_ContentPlaceHolder1_CustomUpdateProgress1_UpdateProgress1');
        if (progress != null && progress != 'undefined')
            progress.style.display = display;
    }
    /**
     * convert BASE64 string to Byte{} array
     * @param base64
     */
    function base64ToArrayBuffer(base64) {
        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
            var ascii = binaryString.charCodeAt(i);
            bytes[i] = ascii;
        }
        return bytes;
    }

    function str2bytes(str) {
        var bytes = new Uint8Array(str.length);
        for (var i = 0; i < str.length; i++) {
            bytes[i] = str.charCodeAt(i);
        }
        return bytes;
    }
</script>
<a id="aDownloadDocumentAsZip" download="" href="#"></a>
<asp:HiddenField ID="HiddenField_Svc_FelhasznaloId" runat="server" Value="" />
<asp:HiddenField ID="HiddenField_Svc_FelhasznaloSzervezetId" runat="server" Value="" />