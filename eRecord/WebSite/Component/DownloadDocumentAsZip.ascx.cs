﻿using Contentum.eRecord.BaseUtility;
using System;

public partial class Component_DownloadDocumentAsZip : System.Web.UI.UserControl
{
    public string HiddenFieldFelelosSzervezetId
    {
        set { HiddenField_Svc_FelhasznaloSzervezetId.Value = value; }
        get { return HiddenField_Svc_FelhasznaloSzervezetId.Value; }
    }
    public string HiddenFieldSvcFelhasznaloId
    {
        set { HiddenField_Svc_FelhasznaloId.Value = value; }
        get { return HiddenField_Svc_FelhasznaloId.Value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
        HiddenField_Svc_FelhasznaloSzervezetId.Value = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
    }
}