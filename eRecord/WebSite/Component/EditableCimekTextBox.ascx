<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditableCimekTextBox.ascx.cs" Inherits="Component_EditableCimekTextBox" %>

<div class="DisableWrap">
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Panel ID="panelCimTipus" runat="server" style="margin-top:5px" Visible="false">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="mrUrlapCaption_nowidth">
                                <asp:Label ID="labelCimTipus" runat="server" Text="C�m t�pusa:" />
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCimTipus" runat="server" Width="180px"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
                <asp:ImageButton TabIndex="-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
                    CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" />
                <asp:ImageButton TabIndex="-1" ID="NewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
                    ImageUrl="~/images/hu/lov/hozzaad.gif" onmouseover="swapByName(this.id,'hozzaad_keret.gif')" onmouseout="swapByName(this.id,'hozzaad.gif')" AlternateText="�j" />
                <asp:ImageButton TabIndex="-1" ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
                    ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" />
                <asp:ImageButton TabIndex="-1" ID="ModifyImageButton" runat="server" Visible="false" CssClass="mrUrlapInputImageButton"
                    ImageUrl="~/images/hu/egyeb/modositas.gif" onmouseover="swapByName(this.id,'modositas_keret.gif')" onmouseout="swapByName(this.id,'modositas.gif')" AlternateText="M�dos�t" />
                <asp:ImageButton TabIndex="-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Alap�llapot" />
                <asp:ImageButton TabIndex="-1" ID="ImageButtonEmailAddressFromEmailBody" runat="server" Visible="false"
                    ImageUrl="~/images/hu/egyeb/lista.gif"
                    onmouseover="swapByName(this.id,'lista_keret.gif')" onmouseout="swapByName(this.id,'lista.gif')"
                    Height="20" AlternateText="Email c�mek az email sz�veg�ben" CssClass="mrUrlapInputImageButton" ToolTip="Email c�mek az email sz�veg�ben" />
                <asp:ImageButton TabIndex="-1" ID="LoadAddressImageButton" runat="server" Visible="false"
                    ImageUrl="~/images/hu/egyeb/refresh.png" AlternateText="Automatikus kit�lt�s" CssClass="mrUrlapInputImageButton" ToolTip="Automatikus kit�lt�s" OnClientClick="return false;"
                    style="margin-top: 3px;"/>
                <asp:HiddenField
                    ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenFieldCustom" runat="server" />
                <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="TextBox1"
                    Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender
                    ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
                    <Animations>
                        <OnShow>
                        <Sequence>
                            <HideAction Visible="true" />
                            <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                        </Sequence>    
                        </OnShow>
                    </Animations>
                </ajaxToolkit:ValidatorCalloutExtender>
                <asp:HiddenField ID="HiddenfieldExternalPartnerControlId" runat="server"/>
            </td>
        </tr>
    </table>
</div>
