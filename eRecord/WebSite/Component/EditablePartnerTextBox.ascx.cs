using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Specialized;

public partial class Component_EditablePartnerTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl, Contentum.eUtility.Test.ITestComponent, UI.ILovListTextBox

{
    private string _Type = "All";

    #region events
    public event EventHandler TextChanged;

    public virtual void TextBox_TextChanged(object sender, EventArgs e)
    {
        if (TextChanged != null)
        {
            TextChanged(this, e);
        }
    }
    #endregion events

    #region public properties

    private bool _TryFireChangeEvent = false;

    public bool TryFireChangeEvent
    {
        get { return _TryFireChangeEvent; }
        set { _TryFireChangeEvent = value; }
    }

    private bool _CreateToolTipOnItems = false;

    public bool CreateToolTipOnItems
    {
        get { return _CreateToolTipOnItems; }
        set { _CreateToolTipOnItems = value; }
    }

    public string Type
    {
        get { return _Type; }
    }

    /// <summary>
    /// Sz�r�s a szervezet ('Organization') t�pus� partnerekre
    /// </summary>
    public void SetFilterType_Szervezet()
    {
        _Type = "Szervezet";
    }

    /// <summary>
    /// Nincs sz�r�s a partnerekre
    /// </summary>
    public void SetFilterType_All()
    {
        _Type = "All";
    }

    private bool _withElosztoivek;

    public bool WithElosztoivek
    {
        get { return _withElosztoivek; }
        set { _withElosztoivek = value; }
    }

    private bool _withKuldemeny = true;

    public bool WithKuldemeny
    {
        get { return _withKuldemeny; }
        set { _withKuldemeny = value; }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            PartnerMegnevezes.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public bool Enabled
    {
        set
        {
            PartnerMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            cbWithAllCim.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return PartnerMegnevezes.Enabled; }
    }
    //LZS - BUG_4731 - Szabadk�zi g�pel�s enged�lyez�se/letilt�sa.
    public bool FreeTextEnabled
    {
        set
        {
            if (value == false)
            {
                PartnerMegnevezes.Attributes.Add("readonly", "readonly");
                PartnerMegnevezes.CssClass += " mrUrlapInputWaterMarked";
            }
            else
            {
                PartnerMegnevezes.Attributes.Remove("readonly");
                PartnerMegnevezes.CssClass = PartnerMegnevezes.CssClass.Replace(" mrUrlapInputWaterMarked", "");
            }

        }
        get { return PartnerMegnevezes.Attributes["readonly"] == "readonly" ? false : true; }
    }
    public bool ReadOnly
    {
        set
        {
            PartnerMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;
            cbWithAllCim.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }


        }
        get { return PartnerMegnevezes.ReadOnly; }
    }

    public string Text
    {
        set { PartnerMegnevezes.Text = value; }
        get
        {
            if (IsOnTestPage)
            {
                if (String.IsNullOrEmpty(Id_HiddenField))
                    return PartnerMegnevezes.Text.Trim();
                else
                    return PartnerMegnevezes.Text;
            }

            return PartnerMegnevezes.Text;
        }
    }

    public TextBox TextBox
    {
        get { return PartnerMegnevezes; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public HiddenField Control_HiddenField
    {
        get { return HiddenField1; }
    }

    // Az aktu�lis partnertextboxban kiv�lasztott partner
    // "kl�noz�sa" a "ClonePartner"-be, ha meg van adva
    #region ClonePartner

    public string ClonePartnerTextBoxClientId
    {
        get
        {
            if (clonePartnerTextBox != null)
                return clonePartnerTextBox.TextBox.ClientID;
            else
                return String.Empty;
        }
    }

    public string ClonePartnerHiddenFieldClientId
    {
        get
        {
            if (clonePartnerTextBox != null)
                return clonePartnerTextBox.HiddenField1.ClientID;
            else
                return String.Empty;
        }
    }

    private Component_EditablePartnerTextBox clonePartnerTextBox = null;

    public Component_EditablePartnerTextBox ClonePartnerTextBox
    {
        set { this.clonePartnerTextBox = value; }

    }

    private TextBox cloneCimTextBox = null;

    public TextBox CloneCimTextBox
    {
        get { return cloneCimTextBox; }
        set { cloneCimTextBox = value; }
    }

    public string CloneCimTextBoxClientId
    {
        get
        {
            if (cloneCimTextBox != null)
                return cloneCimTextBox.ClientID;
            else
                return String.Empty;
        }
    }

    private HiddenField cloneCimHiddenField = null;

    public HiddenField CloneCimHiddenField
    {
        get { return cloneCimHiddenField; }
        set { cloneCimHiddenField = value; }
    }

    public string CloneCimHiddenFieldClientId
    {
        get
        {
            if (cloneCimHiddenField != null)
                return cloneCimHiddenField.ClientID;
            else
                return String.Empty;
        }
    }

    #endregion Clone

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }
    
    public Boolean ViewButtonEnabled
    {
        get
        {
            return ViewImageButton.Enabled;
        }
        set
        {
            ViewImageButton.Enabled = value;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                PartnerMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }


    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                PartnerMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                PartnerMegnevezes.ReadOnly = value;
                AutoCompleteExtender1.Enabled = !value;
                cbWithAllCim.Visible = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    public string CssClass
    {
        get
        {
            return TextBox.CssClass;
        }
        set
        {
            TextBox.CssClass = value;
        }
    }

    public bool WithAllCimCheckBoxVisible
    {
        get
        {
            return cbWithAllCim.Visible;
        }
        set
        {
            cbWithAllCim.Visible = value;
        }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    #endregion

    #region Cim

    private TextBox cimTextBox = null;

    public TextBox CimTextBox
    {
        get { return cimTextBox; }
        set { cimTextBox = value; SetCimTextBoxPartnerReference(); }
    }

    public string CimTextBoxClientId
    {
        get
        {
            if (cimTextBox != null)
                return cimTextBox.ClientID;
            else
                return String.Empty;
        }
    }

    private HiddenField cimHiddenField = null;

    public HiddenField CimHiddenField
    {
        get { return cimHiddenField; }
        set { cimHiddenField = value; }
    }

    public string CimHiddenFieldClientId
    {
        get
        {
            if (cimHiddenField != null)
                return cimHiddenField.ClientID;
            else
                return String.Empty;
        }
    }

    void SetCimTextBoxPartnerReference()
    {
        if (this.CimTextBox != null)
        {
            Component_EditableCimekTextBox cimTextBoxComponent = this.CimTextBox.Parent as Component_EditableCimekTextBox;

            if (cimTextBoxComponent != null)
            {
                cimTextBoxComponent.PartnerTextBox = this.TextBox;
                cimTextBoxComponent.PartnerHiddenField = this.HiddenField1;
            }
        }
    }

    public string CimTipusFilter
    {
        get
        {
            if (this.CimTextBox != null)
            {
                Component_EditableCimekTextBox cimTextBoxComponent = this.CimTextBox.Parent as Component_EditableCimekTextBox;

                if (cimTextBoxComponent != null)
                {
                    return cimTextBoxComponent.CimTipusFilter;
                }
            }

            return String.Empty;
        }
    }

    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        // CR3321 Partner r�gz�t�s csak keres�s ut�n
        //bopmh
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            NewImageButton.Visible = false;
        }

        if (TryFireChangeEvent == false)
        {
            ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
            + TextBox.ClientID + "').value = '';return false;";
        }
        else
        {
            ResetImageButton.OnClientClick = "var hf=$get('" + HiddenField1.ClientID + "'); hf.value = '';var tbox = $get('"
            + TextBox.ClientID + "'); tbox.value = '';$common.tryFireEvent(tbox, 'change'); return false;";
        }

        if (ajaxEnabled)
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            if (_withElosztoivek)
            {
                AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax_eRecord.asmx";
                AutoCompleteExtender1.ServiceMethod = "GetPartnerekUnionElosztoivekList";
            }

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            // userId;WithAllCim;WithKuldemeny
            AutoCompleteExtender1.ContextKey = String.Format("{0};;{1};", felh_Id, WithKuldemeny ? "1" : "0");

            Contentum.eUtility.Configuration.Ajax.ConfigurePartnerAutoComplete(AutoCompleteExtender1);
        }
        else
        {
            AutoCompleteExtender1.Enabled = false;
            WorldJumpExtender1.Enabled = false;
        }

        string query = "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID;

        if (TryFireChangeEvent)
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        if (!String.IsNullOrEmpty(CimTextBoxClientId) && !String.IsNullOrEmpty(CimHiddenFieldClientId))
        {
            query += "&CimTextBoxId=" + CimTextBoxClientId + "&CimHiddenFieldId=" + CimHiddenFieldClientId;
        }

        if (!String.IsNullOrEmpty(CimTipusFilter))
        {
            query += "&" + CimTipusFilter;
        }

        switch (_Type)
        {
            case "All":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx", QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                 + "&" + QueryStringVars.ViewImageButtonId + "=" + ViewImageButton.ClientID
                                , Defaults.PopupWidth + 150, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                        , CommandName.Command + "=" + CommandName.New
                        + query
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                    "PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;
            case "Szervezet":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;

            case "Szemely":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;

            case Constants.FilterType.Partnerek.BelsoSzemely:
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;
        }

        //Manu�lis be�r�s eset�n az id t�rl�se
        //string jsClearHiddenField = "$get('" + HiddenField1.ClientID + "').value = '';";
        string jsClearHiddenField = "var clear=true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
            + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

        TextBox.Attributes.Add("onchange", jsClearHiddenField);

        TextBox.TextChanged += this.TextChanged;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //LZS - BUG_7820
        //ResetImageButton.Visible = !Validate && !ViewMode;
        ResetImageButton.Visible = true;
    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        SetPartnerTextBoxById(errorPanel);
    }

    public void SetPartnerTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Partnerek KRT_Partnerek = (KRT_Partnerek)result.Record;
                Text = KRT_Partnerek.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }

    public void SetPartnerTextBoxByStringOrId(string partnerId, string partnerString, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        Id_HiddenField = partnerId;

        if (!String.IsNullOrEmpty(partnerString))
        {
            Text = partnerString;
        }
        else
        {
            SetPartnerTextBoxById(errorPanel);
        }
    }

    public void SetStringAndIdSearchFields(Field fieldId, Field fieldString)
    {
        fieldId.Value = Id_HiddenField;
        fieldString.Value = Text;

        if (String.IsNullOrEmpty(Id_HiddenField))
        {
            fieldId.Operator = String.Empty;
            fieldString.Operator = Search.GetOperatorByLikeCharater(Text);
        }
        else
        {
            fieldId.Operator = Query.Operators.equals;
            fieldString.Operator = String.Empty;
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>
        {
            PartnerMegnevezes,
            LovImageButton,
            NewImageButton,
            ViewImageButton,
            ResetImageButton,
            cbWithAllCim
        };

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);

                if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
                {
                    Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                    if (c != null)
                    {
                        c.Visible = Validate;
                    }
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        if (!ajaxEnabled)
            return null;

        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.PartnerAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("CimTextBoxClientId", this.CimTextBoxClientId);
        descriptor.AddProperty("CimHiddenFieldClientId", this.CimHiddenFieldClientId);
        descriptor.AddProperty("Delimeter", Constants.AutoComplete.delimeter);
        descriptor.AddProperty("CheckBoxClientId", this.cbWithAllCim.ClientID);
        descriptor.AddProperty("CreateToolTipOnItems", CreateToolTipOnItems ? "1" : "0");
        descriptor.AddProperty("CustomTextEnabled", WithKuldemeny);
        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("HiddenValue", "");

        // kl�noz�s
        descriptor.AddProperty("ClonePartnerTextBoxClientId", ClonePartnerTextBoxClientId);
        descriptor.AddProperty("ClonePartnerHiddenFieldClientId", ClonePartnerHiddenFieldClientId);
        descriptor.AddProperty("CloneCimTextBoxClientId", CloneCimTextBoxClientId);
        descriptor.AddProperty("CloneCimHiddenFieldClientId", CloneCimHiddenFieldClientId);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!ajaxEnabled)
            return null;

        ScriptReference reference = new ScriptReference
        {
            Path = "~/JavaScripts/AutoComplete.js?t=20190607"
        };

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ITestComponent Members

    public bool IsOnTestPage
    {
        get
        {
            if (Page is Contentum.eUtility.Test.BaseTestPage)
                return true;
            return false;
        }
    }

    #endregion
}