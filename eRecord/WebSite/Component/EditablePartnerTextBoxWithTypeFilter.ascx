<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditablePartnerTextBoxWithTypeFilter.ascx.cs"
    Inherits="Component_EditablePartnerTextBoxWithTypeFilter" %>
<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="~/Component/EditableCimekTextBox.ascx" TagName="EditableCimekTextBox" TagPrefix="uc" %>

<%--<script src="../Scripts/jquery-1.12.4.min.js" type="text/javascript"></script>--%>

        <asp:DropDownList ID="DropDownList_Partner_Tipus_Filter" runat="server" AutoPostBack="true"
            OnSelectedIndexChanged="DropDownList_Partner_Tipus_Filter_SelectedIndexChanged" Width="250px" />
        <div class="DisableWrap">
            <%--BUG_5197 (nekrisz)--%>
<%--            <asp:TextBox ID="PartnerMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true" AutoPostBack="true" ToolTip="Partner"></asp:TextBox>--%>
			<asp:TextBox ID="PartnerMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true" ToolTip="Partner" AutoPostBack="True"></asp:TextBox>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:ImageButton TabIndex="-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif"
                onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
                CssClass="mrUrlapInputImageButton" AlternateText="Kiv�laszt" ToolTip="Kiv�laszt" />
            <asp:ImageButton TabIndex="-1" ID="NewImageButton" runat="server" CssClass="mrUrlapInputImageButton" ImageUrl="~/images/hu/lov/hozzaad.gif"
                onmouseover="swapByName(this.id,'hozzaad_keret.gif')" onmouseout="swapByName(this.id,'hozzaad.gif')"
                AlternateText="�j partner" ToolTip="�j partner" />
            <asp:ImageButton TabIndex="-1" ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton" ImageUrl="~/images/hu/egyeb/nagyito.gif"
                onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')"
                AlternateText="Megtekint" ToolTip="Megtekint" />
            <asp:ImageButton TabIndex="-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png"
                onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')"
                AlternateText="Alap�llapot" ToolTip="Alap�llapot" />
            <asp:CheckBox ID="cbWithAllCim" runat="server" Checked="true" ToolTip="Minden c�m" TabIndex="-1" />
            <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="PartnerMegnevezes" SetFocusOnError="true"
                Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender
                ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
                <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
                </Animations>
            </ajaxToolkit:ValidatorCalloutExtender>
            <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true" MinimumPrefixLength="2"
                TargetControlID="PartnerMegnevezes" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true"
                ServicePath="http://localhost:100/eAdminWebService/KRT_PartnerekService.asmx" ServiceMethod="GetPartnerekList"
                CompletionListItemCssClass="GridViewRowStyle" CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle"
                CompletionInterval="2000">
            </ajaxToolkit:AutoCompleteExtender>

            <eUI:WorldJumpExtender ID="WorldJumpExtender1" runat="server" TargetControlID="PartnerMegnevezes"
                AutoCompleteExtenderId="AutoCompleteExtender1" FreeType="true" />

        </div>
        
        <div class="DisableWrap">
            <asp:DropDownList ID="KapcsoltPartnerDropDown" runat="server" AutoPostBack="false" ToolTip="Kapcsolt partner" Width="250px" />
            <asp:HiddenField ID="HiddenFieldKapPart1" runat="server" />
            <asp:ImageButton TabIndex="-1" ID="ImageButtonNewMinositoOrKapcsolattartoPartner" runat="server" CssClass="mrUrlapInputImageButton"
                ImageUrl="~/images/hu/lov/hozzaad2.gif" Visible="false"
                onmouseover="swapByName(this.id,'hozzaad2_keret.gif')" onmouseout="swapByName(this.id,'hozzaad2.gif')"
                ToolTip="<%$Resources:Form,UI_NewMinositoOrKapcsolattartoTitle%>"
                AlternateText="<%$Resources:Form,UI_NewMinositoOrKapcsolattartoTitle%>" />
        </div>