using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_EditablePartnerTextBoxWithTypeFilter : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl, Contentum.eUtility.Test.ITestComponent, UI.ILovListTextBox
{
    private string _Type = "All";

    #region events
    public event EventHandler TextChanged;

    public virtual void TextBox_TextChanged(object sender, EventArgs e)
    {
        if (TextChanged != null)
        {
            TextChanged(this, e);
        }
    }
    #endregion events

    #region public properties

    private bool _TryFireChangeEvent = false;

    public bool TryFireChangeEvent
    {
        get { return _TryFireChangeEvent; }
        set { _TryFireChangeEvent = value; }
    }

    private bool _CreateToolTipOnItems = false;

    public bool CreateToolTipOnItems
    {
        get { return _CreateToolTipOnItems; }
        set { _CreateToolTipOnItems = value; }
    }

    public string Type
    {
        get { return _Type; }
    }

    /// <summary>
    /// Sz�r�s a szervezet ('Organization') t�pus� partnerekre
    /// </summary>
    public void SetFilterType_Szervezet()
    {
        _Type = "Szervezet";
    }

    /// <summary>
    /// Nincs sz�r�s a partnerekre
    /// </summary>
    public void SetFilterType_All()
    {
        _Type = "All";
    }

    private bool _withElosztoivek;

    public bool WithElosztoivek
    {
        get { return _withElosztoivek; }
        set { _withElosztoivek = value; }
    }

    private bool _withKuldemeny = true;

    public bool WithKuldemeny
    {
        get { return _withKuldemeny; }
        set { _withKuldemeny = value; }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return Validator1;
        }
    }

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            PartnerMegnevezes.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }


    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public bool Enabled
    {
        set
        {
            PartnerMegnevezes.Enabled = value;
            //BUG 5197
            KapcsoltPartnerDropDown.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            cbWithAllCim.Enabled = value;
            ImageButtonNewMinositoOrKapcsolattartoPartner.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
                ImageButtonNewMinositoOrKapcsolattartoPartner.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
                ImageButtonNewMinositoOrKapcsolattartoPartner.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return PartnerMegnevezes.Enabled; }
    }

    //LZS - BUG_5340 - Szabadk�zi g�pel�s enged�lyez�se/letilt�sa.
    public bool FreeTextEnabled
    {
        set
        {
            if (value == false)
            {
                PartnerMegnevezes.Attributes.Add("readonly", "readonly");
                PartnerMegnevezes.CssClass += " mrUrlapInputWaterMarked";
            }
            else
            {
                PartnerMegnevezes.Attributes.Remove("readonly");
                PartnerMegnevezes.CssClass = PartnerMegnevezes.CssClass.Replace(" mrUrlapInputWaterMarked", "");
            }

        }
        get { return PartnerMegnevezes.Attributes["readonly"] == "readonly" ? false : true; }
    }

    public bool ReadOnly
    {
        set
        {
            PartnerMegnevezes.ReadOnly = value;
            //BUG 5197
            KapcsoltPartnerDropDown.Enabled = !value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            // BUG_4731
            // BUG kapcs�n jav�tva
            DropDownList_Partner_Tipus_Filter.Visible = !value;

            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;
            cbWithAllCim.Enabled = !value;
            ImageButtonNewMinositoOrKapcsolattartoPartner.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
                ImageButtonNewMinositoOrKapcsolattartoPartner.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
                ImageButtonNewMinositoOrKapcsolattartoPartner.CssClass = "mrUrlapInputImageButton";
            }


        }
        get { return PartnerMegnevezes.ReadOnly; }
    }

    public string Text
    {
        set { PartnerMegnevezes.Text = value; }
        get
        {
            if (IsOnTestPage)
            {
                if (String.IsNullOrEmpty(Id_HiddenField))
                    return PartnerMegnevezes.Text.Trim();
                else
                    return PartnerMegnevezes.Text;
            }

            return PartnerMegnevezes.Text;
        }
    }

    public TextBox TextBox
    {
        get { return PartnerMegnevezes; }
    }

    public string Id_HiddenField
    {
        set
        {
            HiddenField1.Value = value;
            //LZS - BUG_5197 - Let�roljuk Session v�ltoz�ba a partnerId-t.
            if (!string.IsNullOrEmpty(value) && !string.IsNullOrEmpty(Session["partnerId"] as string))
            {
                if (value != Session["partnerId"].ToString())
                    Session["partnerId"] = value;
            }
        }
        get { return HiddenField1.Value; }
    }

    #region BUG 5197
    public string KapcsoltPartnerDropDownValue
    {
        set
        {
            // BUG_5197 (nekrisz)
            if (KapcsoltPartnerDropDown.Items.Count == 0)
                SetKapcsoltPartner();
            KapcsoltPartnerDropDown.SelectedValue = value;
        }
        get { return KapcsoltPartnerDropDown.SelectedValue; }
    }
    #endregion

    public HiddenField Control_HiddenField
    {
        get { return HiddenField1; }
    }

    // Az aktu�lis partnertextboxban kiv�lasztott partner
    // "kl�noz�sa" a "ClonePartner"-be, ha meg van adva
    #region ClonePartner

    private string _ClonePartnerTextBoxClientId = String.Empty;

    public string ClonePartnerTextBoxClientId
    {
        get
        {
            if (clonePartnerTextBox != null)
                return clonePartnerTextBox.TextBox.ClientID;
            else
                return _ClonePartnerTextBoxClientId;
        }
        set
        {
            _ClonePartnerTextBoxClientId = value;
        }
    }

    private string _ClonePartnerHiddenFieldClientId = String.Empty;

    public string ClonePartnerHiddenFieldClientId
    {
        get
        {
            if (clonePartnerTextBox != null)
                return clonePartnerTextBox.HiddenField1.ClientID;
            else
                return _ClonePartnerHiddenFieldClientId;
        }
        set
        {
            _ClonePartnerHiddenFieldClientId = value;
        }
    }

    private Component_EditablePartnerTextBoxWithTypeFilter clonePartnerTextBox = null;

    public Component_EditablePartnerTextBoxWithTypeFilter ClonePartnerTextBox
    {
        set { this.clonePartnerTextBox = value; }

    }

    private TextBox cloneCimTextBox = null;

    public TextBox CloneCimTextBox
    {
        get { return cloneCimTextBox; }
        set { cloneCimTextBox = value; }
    }

    public string CloneCimTextBoxClientId
    {
        get
        {
            if (cloneCimTextBox != null)
                return cloneCimTextBox.ClientID;
            else
                return String.Empty;
        }
    }

    private HiddenField cloneCimHiddenField = null;

    public HiddenField CloneCimHiddenField
    {
        get { return cloneCimHiddenField; }
        set { cloneCimHiddenField = value; }
    }

    public string CloneCimHiddenFieldClientId
    {
        get
        {
            if (cloneCimHiddenField != null)
                return cloneCimHiddenField.ClientID;
            else
                return String.Empty;
        }
    }

    #endregion Clone

    public string BehaviorID
    {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_NewMinositoOrKapcsolattartoPartner
    {
        set { ImageButtonNewMinositoOrKapcsolattartoPartner.OnClientClick = value; }
        get { return ImageButtonNewMinositoOrKapcsolattartoPartner.OnClientClick; }
    }

    public Boolean ViewButtonEnabled
    {
        get
        {
            return ViewImageButton.Enabled;
        }
        set
        {
            ViewImageButton.Enabled = value;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                PartnerMegnevezes.CssClass = "ViewReadOnlyWebControl";
                //BUG 5197
                KapcsoltPartnerDropDown.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
                ImageButtonNewMinositoOrKapcsolattartoPartner.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }


    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                PartnerMegnevezes.CssClass = "ViewDisabledWebControl";
                //BUG 5197
                KapcsoltPartnerDropDown.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
                ImageButtonNewMinositoOrKapcsolattartoPartner.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
            //ImageButtonNewMinositoOrKapcsolattartoPartner.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                PartnerMegnevezes.ReadOnly = value;
                //BUG 5197
                KapcsoltPartnerDropDown.Enabled = !value;
                DropDownList_Partner_Tipus_Filter.Enabled = !value;
                AutoCompleteExtender1.Enabled = !value;
                cbWithAllCim.Visible = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
                //  ImageButtonNewMinositoOrKapcsolattartoPartner.Visible = !value;
            }
        }
    }

    public string CssClass
    {
        get
        {
            return TextBox.CssClass;
        }
        set
        {
            TextBox.CssClass = value;
        }
    }

    public bool WithAllCimCheckBoxVisible
    {
        get
        {
            return cbWithAllCim.Visible;
        }
        set
        {
            cbWithAllCim.Visible = value;
        }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    #endregion

    #region Cim

    private TextBox cimTextBox = null;

    public TextBox CimTextBox
    {
        get { return cimTextBox; }
        set { cimTextBox = value; SetCimTextBoxPartnerReference(); }
    }

    public string CimTextBoxClientId
    {
        get
        {
            if (cimTextBox != null)
                return cimTextBox.ClientID;
            else
                return String.Empty;
        }
    }

    private HiddenField cimHiddenField = null;

    public HiddenField CimHiddenField
    {
        get { return cimHiddenField; }
        set { cimHiddenField = value; }
    }

    public string CimHiddenFieldClientId
    {
        get
        {
            if (cimHiddenField != null)
                return cimHiddenField.ClientID;
            else
                return String.Empty;
        }
    }

    void SetCimTextBoxPartnerReference()
    {
        if (this.CimTextBox != null)
        {
            Component_EditableCimekTextBox cimTextBoxComponent = this.CimTextBox.Parent as Component_EditableCimekTextBox;

            if (cimTextBoxComponent != null)
            {
                cimTextBoxComponent.PartnerTextBox = this.TextBox;
                cimTextBoxComponent.PartnerHiddenField = this.HiddenField1;
                cimTextBoxComponent.dropDownList = this.DropDownList_Partner_Tipus_Filter;
            }
        }
    }

    public string CimTipusFilter
    {
        get
        {
            if (this.cimTextBox != null)
            {
                Component_EditableCimekTextBox cimTextBoxComponent = this.CimTextBox.Parent as Component_EditableCimekTextBox;

                if (cimTextBoxComponent != null)
                {
                    return cimTextBoxComponent.CimTipusFilter;
                }
            }

            return String.Empty;
        }
    }

    #endregion

    #region BUG 5197 - partner c�m ellen�rz�s kapcsolat partnerhez
    public void CheckPartnerCimek(string partnerId, string cimId, string ver, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!string.IsNullOrEmpty(partnerId) && !string.IsNullOrEmpty(cimId))
        {
            //C�m ellen�rz�se
            KRT_PartnerCimekService cimService = eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
            KRT_PartnerCimekSearch cimSearch = new KRT_PartnerCimekSearch();

            cimSearch.Partner_id.Value = partnerId;
            cimSearch.Partner_id.Operator = Query.Operators.equals;

            cimSearch.Cim_Id.Value = cimId;
            cimSearch.Cim_Id.Operator = Query.Operators.equals;

            Result cimRes = cimService.GetAll(new ExecParam(), cimSearch);
            if (!cimRes.IsError)
            {
                if (cimRes.Ds.Tables[0].Rows.Count == 0)
                {
                    KRT_PartnerCimekService cimServiceInsert = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_PartnerCimekService();
                    //TODO insert!
                    KRT_PartnerCimek partnerCimek = new KRT_PartnerCimek();

                    partnerCimek.Updated.SetValueAll(false);
                    partnerCimek.Base.Updated.SetValueAll(false);

                    partnerCimek.Cim_Id = cimId;
                    partnerCimek.Updated.Cim_Id = true;

                    partnerCimek.Partner_id = partnerId;
                    partnerCimek.Updated.Partner_id = true;

                    partnerCimek.Base.Ver = ver;
                    partnerCimek.Base.Updated.Ver = true;

                    Result result = cimServiceInsert.Insert(new ExecParam(), partnerCimek);
                    if (result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                        return;
                    }
                }
            }
        }
    }
    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }
    #endregion

    #region BUG 5197 - kapcsolt partner meghat�roz�sa
    private string getPartnerIdByKapcsolatAndName(string partnerIdKapcsolt, string name)
    {
        if(string.IsNullOrEmpty(partnerIdKapcsolt) || string.IsNullOrEmpty(name))
        {
            return string.Empty;
        }
        KRT_PartnerKapcsolatokService pkservice = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
        KRT_PartnerKapcsolatokSearch pksearch = new KRT_PartnerKapcsolatokSearch();

        pksearch.Partner_id_kapcsolt.Value = partnerIdKapcsolt;
        pksearch.Partner_id_kapcsolt.Operator = Query.Operators.equals;

        Result respk = pkservice.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), pksearch);
        if (!respk.IsError)
        {
            if (respk.Ds.Tables.Count > 0 && respk.Ds.Tables[0].Rows.Count > 0)
            {
                DataRow rowpk = respk.Ds.Tables[0].Rows[0];
                string partnerId = rowpk["Partner_id"].ToString();

                KRT_PartnerekService partnerekService1 = eAdminService.ServiceFactory.GetKRT_PartnerekService();

                ExecParam execParam1 = UI.SetExecParamDefault(Page, new ExecParam());
                execParam1.Record_Id = partnerId;

                Result respartner1 = partnerekService1.Get(execParam1);
                if (!respartner1.IsError)
                {
                    KRT_Partnerek partner = (KRT_Partnerek)respartner1.Record;
                    if (name.Equals(partner.Nev))
                    {
                        return partnerId;
                    }
                }
            }
        }
        return string.Empty;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        // CR3321 Partner r�gz�t�s csak keres�s ut�n
        //bopmh
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            NewImageButton.Visible = false;
        }
        if (IsPostBack)
        {
            if (!string.IsNullOrEmpty(Id_HiddenField))
            {
                if (Session["partnerId"] == null || Id_HiddenField != Session["partnerId"].ToString())
                    Session["partnerId"] = Id_HiddenField;
            }
            SetType();
        }

        InitializePartnerTipusFilter();
        SetControlsOnPageLoad();

        TextBox.TextChanged += this.TextChanged;
        //LZS - BUG_5197 - Change met�dus a doPostBack-hez.
        //PartnerMegnevezes.Attributes.Add("onblur", "PartnerMegnevezesChanged(this);");

        //string PartnerMegnevezesChanged = Request["__EVENTARGUMENT"];

        //if (!string.IsNullOrEmpty(PartnerMegnevezesChanged))
        //{
        //    if (string.IsNullOrEmpty(Session["partnerId"] as string))
        //    {
        //        Session["partnerId"] = "";
        //    }

        //    SetPartnerTextBoxByString();
        //    if (!string.IsNullOrEmpty(Id_HiddenField))
        //    {
        //        if (Id_HiddenField != Session["partnerId"].ToString())
        //            Session["partnerId"] = Id_HiddenField;
        //    }
        //}
        //string js = "$(document).ready(function () { " +
        //        "$('#" + PartnerMegnevezes.ClientID + "').change(function() { " +
        //        //"    alert(\"Handler for .change() called.\"); " +
        //        "    }); " +
        //        "});";

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);



        //BUG 5197
        //partnerChanged
        // BUG_5197 (nekrisz)
        string separatedPartnerNev = String.Empty;
        if (KapcsoltPartnerDropDown.SelectedItem != null)
            separatedPartnerNev = KapcsoltPartnerDropDown.SelectedItem.Text;

        SetKapcsoltPartner();
      //  if (IsPostBack){
            //string target = Request["__EVENTTARGET"];
            //if (target == "partnerChanged")
            //{
            //    string v = Request["__EVENTARGUMENT"];
            //    if (!string.IsNullOrEmpty(v) && "1".Equals(v))
            //    {
            //        SetKapcsoltPartner();
            //    }
            //}
            // BUG_5197 (nekrisz)
            //if (_Type == Constants.FilterType.Partnerek.SzervezetKapcsolattartoja)
            {
                string[] nevArray = PartnerMegnevezes.Text.Split(new[] { " - " }, StringSplitOptions.None);
                if (nevArray != null)
                {
                    string separatedNev = nevArray[0];
                    Text = separatedNev;
                    
                    if (nevArray.Length > 1)
                    {
                        separatedPartnerNev = nevArray[1];
                        
                    }
                    ListItem li = KapcsoltPartnerDropDown.Items.FindByText(separatedPartnerNev);
                    if(li == null)
                    {
                        string id = getPartnerIdByKapcsolatAndName(Id_HiddenField, separatedPartnerNev);
                        if (!string.IsNullOrEmpty(id))
                        {
                            li = new ListItem(separatedPartnerNev, Id_HiddenField);
                            KapcsoltPartnerDropDown.Items.Add(li);
                        }
                    }
                    if(li != null)
                    {
                        li.Selected = true;
                    }
                }
            }
      //  }
        
        //SetKapcsoltPartner();
    }
    private void SetControlsOnPageLoad()
    {
        if (TryFireChangeEvent == false)
        {
            ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
            + TextBox.ClientID + "').value = '';return false;";
        }
        else
        {
            ResetImageButton.OnClientClick = "var hf=$get('" + HiddenField1.ClientID + "'); hf.value = '';var tbox = $get('"
            + TextBox.ClientID + "'); tbox.value = '';$common.tryFireEvent(tbox, 'change'); return false;";
        }

        if (ajaxEnabled)
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            var type = _Type;
            if (_withElosztoivek)
            {
                AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax_eRecord.asmx";
                AutoCompleteExtender1.ServiceMethod = "GetPartnerekUnionElosztoivekList";
            }
            else
            {
                if (FelhasznaloProfil.OrgKod(Page) == "CSPH") // BUG_8419
                {
                    type += "WithSeparators"; // address parts separated with ", "
                }
            }

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            // userId;WithAllCim;WithKuldemeny;Type
            // WithAllCim set by SetContextKey in AutoComplete.js
            AutoCompleteExtender1.ContextKey = String.Format("{0};;{1};{2};", felh_Id, WithKuldemeny ? "1" : "0", type);

            Contentum.eUtility.Configuration.Ajax.ConfigurePartnerAutoComplete(AutoCompleteExtender1);
        }
        else
        {
            AutoCompleteExtender1.Enabled = false;
            WorldJumpExtender1.Enabled = false;
        }

        string query = "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID;

        if (TryFireChangeEvent)
        {
            query += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }

        if (!String.IsNullOrEmpty(CimTextBoxClientId) && !String.IsNullOrEmpty(CimHiddenFieldClientId))
        {
            query += "&CimTextBoxId=" + CimTextBoxClientId + "&CimHiddenFieldId=" + CimHiddenFieldClientId;
        }

        if (!String.IsNullOrEmpty(CimTipusFilter))
        {
            query += "&" + CimTipusFilter;
        }

        switch (_Type)
        {
            case "All":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx", QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                 + "&" + QueryStringVars.ViewImageButtonId + "=" + ViewImageButton.ClientID
                                , Defaults.PopupWidth + 150, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                        , CommandName.Command + "=" + CommandName.New
                        + query
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                    "PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;
            case "Szervezet":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                SetNewSzemelyWithMinositoOrKapcsolattartoUrl(query);

                break;

            case "Szemely":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;

            case Constants.FilterType.Partnerek.BelsoSzemely:
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

                break;
            case Constants.FilterType.Partnerek.SzervezetKapcsolattartoja:
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + query + "&" + QueryStringVars.WithElosztoivek + "=" + _withElosztoivek.ToString()
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + query
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);
                //BUG 5197 - 3. as pont:
                query = "&" + QueryStringVars.HiddenFieldId + "=" + HiddenFieldKapPart1.ClientID
                       + "&" + QueryStringVars.KapcsoltPartnerDropDownId + "=" + KapcsoltPartnerDropDown.ClientID;
                SetNewSzemelyWithMinositoOrKapcsolattartoUrl(query);

                break;
        }

        //Manu�lis be�r�s eset�n az id t�rl�se
        string jsClearHiddenField = "var clear=true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
            + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";
            //+ "__doPostBack('partnerChanged','1')";
            //+ "$common.tryFireEvent("+ this.CimTextBoxClientId + ", 'change');";

        TextBox.Attributes.Add("onchange", jsClearHiddenField);
    }
    /// <summary>
    /// New szemely partber, with minosito or kapcsolattarto
    /// </summary>
    /// <param name="query"></param>
    private void SetNewSzemelyWithMinositoOrKapcsolattartoUrl(string query)
    {
        OnClick_NewMinositoOrKapcsolattartoPartner =
            JavaScripts.SetPartnerOnClientClick("PartnerekForm.aspx",
            HiddenField1.ClientID
           , CommandName.Command + "=" + CommandName.New
           + "&" + QueryStringVars.Filter + "=" + Constants.FilterType.Partnerek.Szemely
           + "&" + QueryStringVars.NewSzemelyPartnerWithMinositoOrKapcsolattarto + "=true"
           + query
           , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //LZS - BUG_7820
        //ResetImageButton.Visible = !Validate && !ViewMode;
        ResetImageButton.Visible = true;
    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel, UpdatePanel errorUpdatePanel)
    {
        SetPartnerTextBoxById(errorPanel);
    }

    public void SetPartnerTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Partnerek KRT_Partnerek = (KRT_Partnerek)result.Record;
                Text = KRT_Partnerek.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }

    //LZS - BUG_5197 - A PartnerId megkeres�se n�v alapj�n.
    public void SetPartnerTextBoxByString()
    {
        if (!String.IsNullOrEmpty(Text))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_PartnerekSearch kRT_PartnerekSearch = new KRT_PartnerekSearch();
            kRT_PartnerekSearch.Nev.Value = Text;
            kRT_PartnerekSearch.Nev.Operator = Query.Operators.equals;


            Result result = service.GetAllWithExtnesions(execParam, kRT_PartnerekSearch);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    Id_HiddenField = row["Id"].ToString();
                }
            }
            else
            {
                //ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
                Id_HiddenField = "";
            }
        }
        //else
        //{
        //    HiddenField1.Value = "";
        //}
    }

    public void SetPartnerTextBoxByStringOrId(string partnerId, string partnerString, Contentum.eUIControls.eErrorPanel errorPanel)
    {
        Id_HiddenField = partnerId;

        if (!String.IsNullOrEmpty(partnerString))
        {
            Text = partnerString;
        }
        else
        {
            SetPartnerTextBoxById(errorPanel);
        }
    }
    
    public void SetStringAndIdSearchFields(Field fieldId, Field fieldString)
    {
        fieldId.Value = Id_HiddenField;
        fieldString.Value = Text;

        if (String.IsNullOrEmpty(Id_HiddenField))
        {
            fieldId.Operator = String.Empty;
            fieldString.Operator = Search.GetOperatorByLikeCharater(Text);
        }
        else
        {
            fieldId.Operator = Query.Operators.equals;
            fieldString.Operator = String.Empty;
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>
        {
            PartnerMegnevezes,
            LovImageButton,
            NewImageButton,
            ViewImageButton,
            ResetImageButton,
            cbWithAllCim,
            ImageButtonNewMinositoOrKapcsolattartoPartner,
            KapcsoltPartnerDropDown
        };

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";
        ImageButtonNewMinositoOrKapcsolattartoPartner.OnClientClick = "";
        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);

                if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
                {
                    Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

                    if (c != null)
                    {
                        c.Visible = Validate;
                    }
                }
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        if (!ajaxEnabled)
            return null;

        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.PartnerAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("CimTextBoxClientId", this.CimTextBoxClientId);
        descriptor.AddProperty("CimHiddenFieldClientId", this.CimHiddenFieldClientId);
        descriptor.AddProperty("Delimeter", Constants.AutoComplete.delimeter);
        descriptor.AddProperty("CheckBoxClientId", this.cbWithAllCim.ClientID);
        descriptor.AddProperty("CreateToolTipOnItems", CreateToolTipOnItems ? "1" : "0");
        descriptor.AddProperty("CustomTextEnabled", WithKuldemeny);
        descriptor.AddProperty("id", this.BehaviorID);
        descriptor.AddProperty("HiddenValue", "");

        // kl�noz�s
        descriptor.AddProperty("ClonePartnerTextBoxClientId", ClonePartnerTextBoxClientId);
        descriptor.AddProperty("ClonePartnerHiddenFieldClientId", ClonePartnerHiddenFieldClientId);
        descriptor.AddProperty("CloneCimTextBoxClientId", CloneCimTextBoxClientId);
        descriptor.AddProperty("CloneCimHiddenFieldClientId", CloneCimHiddenFieldClientId);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!ajaxEnabled)
            return null;

        ScriptReference reference = new ScriptReference
        {
            Path = "~/JavaScripts/AutoComplete.js?t=20190607"
        };

        return new ScriptReference[] { reference };
    }

    #endregion

    #region ITestComponent Members

    public bool IsOnTestPage
    {
        get
        {
            if (Page is Contentum.eUtility.Test.BaseTestPage)
                return true;
            return false;
        }
    }

    #endregion

    #region PARTNER TIPUSOK 2 CONTROL
    /// <summary>
    /// GetPartnerTipusokFilter
    /// </summary>
    /// <returns></returns>
    private ListItem[] GetPartnerTipusokFilter(bool useDefaultEmpty)
    {
        List<ListItem> items = new List<ListItem>();
        items.Add(new ListItem(KodTarak.Partner_Tipusok_Filter.Mind.Description, KodTarak.Partner_Tipusok_Filter.Mind.Key));
        items.Add(new ListItem(KodTarak.Partner_Tipusok_Filter.Szemely.Description, KodTarak.Partner_Tipusok_Filter.Szemely.Key));
        items.Add(new ListItem(KodTarak.Partner_Tipusok_Filter.Szervezet.Description, KodTarak.Partner_Tipusok_Filter.Szervezet.Key));
        items.Add(new ListItem(KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Description, KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Key));
        return items.ToArray();
    }

    protected void DropDownList_Partner_Tipus_Filter_SelectedIndexChanged(object sender, EventArgs e)
    {
        OnPartnerTipusFilterChanged();
    }

    private void OnPartnerTipusFilterChanged()
    {
        HiddenField1.Value = string.Empty;
        //BUG 5197
        PartnerMegnevezes.Text = string.Empty;
        ResetPKDropDownList();
        SetType();
        SetControlsOnPageLoad();
    }

    private void SetType()
    {
        ImageButtonNewMinositoOrKapcsolattartoPartner.Visible = DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.Szervezet.Key || DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Key;
        if (DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.Mind.Key)
        {
            _Type = "All";
        }
        else if (DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.Szervezet.Key)
        {
            _Type = KodTarak.Partner_Tipusok_Filter.Szervezet.Name;
        }
        else if (DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.Szemely.Key)
        {
            _Type = KodTarak.Partner_Tipusok_Filter.Szemely.Name;
        }
        else if (DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Key)
        {
            _Type = KodTarak.Partner_Tipusok_Filter.SzervezetKapcsolattartoja.Name;
        }
        else
        {
            _Type = "All";
        }
    }
    private void InitializePartnerTipusFilter()
    {
        // BUG_4731
        // BUG kapcs�n jav�tva
        if (ReadOnly)
        {
            DropDownList_Partner_Tipus_Filter.Visible = false;
        }
        else
        {
            string PARTNERKAPCSOLATOK_ERKEZTETESKOR_ENABLED = Contentum.eUtility.Rendszerparameterek.Get(Page, Contentum.eUtility.Rendszerparameterek.PARTNERKAPCSOLATOK_ERKEZTETESKOR_ENABLED);
            if (string.Equals(PARTNERKAPCSOLATOK_ERKEZTETESKOR_ENABLED, "1"))
            {
                DropDownList_Partner_Tipus_Filter.Visible = true;
                if (!Page.IsPostBack)
                {
                    DropDownList_Partner_Tipus_Filter.Items.Clear();
                    DropDownList_Partner_Tipus_Filter.Items.AddRange(GetPartnerTipusokFilter(true));
                }
            }
            else
            {
                DropDownList_Partner_Tipus_Filter.Visible = false;
            }
        }
    }
    #endregion

    #region BUG 5197 / 3 - Kapcsolt partenrek list�z�sa
    protected void SetKapcsoltPartner()
    {
        try
        {
            ResetPKDropDownList();
            //if (DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.Szervezet.Key)
            if (DropDownList_Partner_Tipus_Filter.SelectedValue != KodTarak.Partner_Tipusok_Filter.Szemely.Key)
            {
                // BUG_5197 (nekrisz)
                if (!String.IsNullOrEmpty(Id_HiddenField))
                {
                    #region partner lista meghat�roz�sa

                    KRT_PartnerKapcsolatokService pkservice = eAdminService.ServiceFactory.GetKRT_PartnerKapcsolatokService();
                    KRT_PartnerKapcsolatokSearch pksearch = new KRT_PartnerKapcsolatokSearch();

                    pksearch.Partner_id_kapcsolt.Value = Id_HiddenField;
                    pksearch.Partner_id_kapcsolt.Operator = Query.Operators.equals;
                    //pksearch.Partner_id_kapcsolt.Group = "1111";
                    //pksearch.Partner_id_kapcsolt.GroupOperator = Contentum.eQuery.Query.Operators.or;

                    //pksearch.Partner_id.Value = Id_HiddenField;
                    //pksearch.Partner_id.Operator = Query.Operators.equals;
                    //pksearch.Partner_id.Group = "1111";
                    //pksearch.Partner_id.GroupOperator = Contentum.eQuery.Query.Operators.or;

                    //BUG_13213
                    pksearch.Tipus.Value = KodTarak.PartnerKapcsolatTipus.Szervezet_Kapcsolattartoja;
                    pksearch.Tipus.Operator = Query.Operators.equals;

                    Result respk = pkservice.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), pksearch);
                    if (!respk.IsError)
                    {
                        if (respk.Ds.Tables.Count > 0 && respk.Ds.Tables[0].Rows.Count > 0)
                        {
                            List<string> partnerIds = new List<string>();
                            foreach (DataRow rowpk in respk.Ds.Tables[0].Rows)
                            {
                                partnerIds.Add(rowpk["Partner_id"].ToString());
                                // BUG_13213
                                //  partnerIds.Add(rowpk["Partner_id_kapcsolt"].ToString());                           
                            }
                            if (partnerIds != null && partnerIds.Count > 0)
                            {
                                KRT_PartnerekService partnerekService1 = eAdminService.ServiceFactory.GetKRT_PartnerekService();
                                KRT_PartnerekSearch partnerekSearch1 = new KRT_PartnerekSearch();

                                partnerekSearch1.Id.Value = Search.GetSqlInnerString(partnerIds.ToArray());
                                partnerekSearch1.Id.Operator = Query.Operators.inner;

                                Result res = partnerekService1.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), partnerekSearch1);
                                if (!res.IsError)
                                {
                                    FillKPDropDownList(res);

                                }
                            }
                        }
                    }
                    //if (DropDownList_Partner_Tipus_Filter.SelectedValue == KodTarak.Partner_Tipusok_Filter.Szervezet.Key)
                    //{
                    //    KapcsoltPartnerDropDown.Enabled = true;
                    //}
                    #endregion

                }
            }
        }catch(Exception ex)
        {
            Logger.Error("EditablePartnerTextBoxWithTypeFilter.PartnerMegnevezes_TextChanged error: " + ex.Message);
        }
        
    }

    #region BUG 5197
    private void ResetPKDropDownList()
    {
        KapcsoltPartnerDropDown.ClearSelection();
        KapcsoltPartnerDropDown.Items.Clear();
        KapcsoltPartnerDropDown.Items.Add(new ListItem("Nincs kapcsolt partner megadva", string.Empty));
        //KapcsoltPartnerDropDown.Enabled = false;
    }

    public void FillKPDropDownList(Contentum.eBusinessDocuments.Result Res)
    {
        try
        {
            if (Res != null && Res.Ds != null && Res.Ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < Res.Ds.Tables[0].Rows.Count; i++)
                {
                    KapcsoltPartnerDropDown.Items.Add(new ListItem(Res.Ds.Tables[0].Rows[i]["Nev"].ToString(), Res.Ds.Tables[0].Rows[i]["Id"].ToString()));
                }
                //KapcsoltPartnerDropDown.Items.Insert(0, string.Empty);

            }
        }
        catch (Exception ex)
        {
            Logger.Error("EditablePartnerTextBoxWithTypeFilter.FillKPDropDownList error: " + ex.Message);
        }
    }
    #endregion

    #endregion
}