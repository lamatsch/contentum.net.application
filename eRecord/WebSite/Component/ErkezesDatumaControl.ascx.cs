using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_ErkezesDatumaControl : System.Web.UI.UserControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    #region public properties

    /// <summary>
    /// Sets a value indicating whether [enable_ erv kezd].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv kezd]; otherwise, <c>false</c>.</value>
    public bool Enable_DatumKezd
    {
        set
        {
            erkKezd_TextBox.Enabled = value;
            erkKezd_ImageButton.Enabled = value;
            ErkKezd_RequiredFieldValidator.Enabled = value;
            CalendarExtender1.Enabled = value;
            if (!value)
            {
                erkKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_DatumKezd
    {
        set
        {
            erkKezd_TextBox.ReadOnly = value;
            erkKezd_ImageButton.Enabled = !value;
            ErkKezd_RequiredFieldValidator.Enabled = !value;
            CalendarExtender1.Enabled = !value;
            if (value)
            {
                erkKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv vege].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv vege]; otherwise, <c>false</c>.</value>
    public bool Enable_DatumVege
    {
        set
        {
            erkVege_TextBox.Enabled = value;
            erkVege_ImageButton.Enabled = value;
            ErkVege_RequiredFieldValidator.Enabled = value;
            masol_ImageButton.Enabled = value;
            CalendarExtender2.Enabled = value;
            if (!value)
            {
                masol_ImageButton.CssClass = "disabledCalendarImage";
                erkVege_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_DatumVege
    {
        set
        {
            erkVege_TextBox.ReadOnly = value;
            erkVege_ImageButton.Enabled = !value;
            ErkVege_RequiredFieldValidator.Enabled = !value;
            masol_ImageButton.Enabled = !value;
            CalendarExtender2.Enabled = !value;
            if (value)
            {
                masol_ImageButton.CssClass = "disabledCalendarImage";
                erkVege_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            Enable_DatumKezd = _Enabled;
            Enable_DatumVege = _Enabled;
        }
    }

    Boolean _Readonly = false;

    public Boolean Readonly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            Readonly_DatumKezd = _Readonly;
            Readonly_DatumVege = _Readonly;
        }
    }

    public string ErkKezd
    {
        get
        {
            if (!String.IsNullOrEmpty(erkKezd_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(erkKezd_TextBox.Text);
                    return dt.ToShortDateString();
                }
                catch (FormatException)
                {
                    return erkKezd_TextBox.Text;
                }
            }
            else return "";
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    erkKezd_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    erkKezd_TextBox.Text = value;
                }
            }
            else erkKezd_TextBox.Text = "";
        }
    }

    public string ErkVege
    {
        get
        {
            if (!String.IsNullOrEmpty(erkVege_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(erkVege_TextBox.Text);
                    dt = dt.AddHours(23 - dt.Hour);
                    dt = dt.AddMinutes(59 - dt.Minute);
                    dt = dt.AddSeconds(59 - dt.Second);
                    dt = dt.AddMilliseconds(999 - dt.Millisecond);
                    return dt.ToString();
                }
                catch (FormatException)
                {
                    return erkVege_TextBox.Text;
                }
            }
            else return "";
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    erkVege_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    erkVege_TextBox.Text = value;
                }
            }
            else erkVege_TextBox.Text = "";
        }
    }

    public bool Validate
    {
        set 
        { 
            ErkKezd_RequiredFieldValidator.Enabled = value;
            ErkVege_RequiredFieldValidator.Enabled = value;
        }
        get {
            return ErkKezd_RequiredFieldValidator.Enabled || ErkVege_RequiredFieldValidator.Enabled;
        }
    }

    #endregion

    public void SetDefault()
    {
        //erkKezd_TextBox.Text = DefaultDates.Today;
        //erkVege_TextBox.Text = DefaultDates.EndDate;
    }
}
