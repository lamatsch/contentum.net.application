<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ErvenyessegCalendarControl.ascx.cs"
    Inherits="Component_ErvenyessegCalendarControl" %>

<asp:TextBox ID="ervKezd_TextBox" runat="server" CssClass="mrUrlapCalendar_Erv"></asp:TextBox>
<asp:ImageButton TabIndex="-1"
    ID="ervKezd_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
    OnClientClick="return false;" />&nbsp;
<asp:TextBox ID="ervVege_TextBox" runat="server" CssClass="mrUrlapCalendar_Erv"></asp:TextBox>
<asp:ImageButton TabIndex="-1"
    ID="ervVege_ImageButton" runat="server" ImageUrl="~/images/hu/egyeb/Calendar.png"
    OnClientClick="return false;" />
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="ervKezd_ImageButton"
    TargetControlID="ervKezd_TextBox">
</ajaxToolkit:CalendarExtender>
<ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="ervVege_ImageButton"
    TargetControlID="ervVege_TextBox">
</ajaxToolkit:CalendarExtender>
<asp:RequiredFieldValidator ID="ErvKezd_RequiredFieldValidator" runat="server" SetFocusOnError="true"
    ControlToValidate="ervKezd_TextBox" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
    TargetControlID="ErvKezd_RequiredFieldValidator">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<%--<asp:RequiredFieldValidator ID="ErvVege_RequiredFieldValidator" runat="server" SetFocusOnError="true" Enabled="false" Visible="false"
    ControlToValidate="ervVege_TextBox" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
    TargetControlID="ErvVege_RequiredFieldValidator">
        <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>--%>
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="ervKezd_TextBox" SetFocusOnError="true"
    ControlToValidate="ervVege_TextBox" Display="None" ErrorMessage="<%$Resources:Form,Erv_kezd_vege_CompareValidatorMessage%>"
    Operator="GreaterThanEqual" Type="Date" Visible="False"></asp:CompareValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="server"
    TargetControlID="CompareValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>

<%--    <asp:CompareValidator ID="CompareValidator_DateFormat_Kezd" runat="server"
    Type="Date" Operator="DataTypeCheck"
    ControlToValidate="ervKezd_TextBox" ErrorMessage="A d�tum form�tuma nem megfelel�!"
    Display="None" Visible="false" Enabled="false" SetFocusOnError="true" />--%>
<asp:CustomValidator ID="CustomValidator_DateFormat_Kezd" runat="server"
    ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
    ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"
    EnableClientScript="true" ControlToValidate="ervKezd_TextBox" Display="None" />
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_DateFormat_Kezd" runat="server"
    TargetControlID="CustomValidator_DateFormat_Kezd">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<%--    <asp:CompareValidator ID="CompareValidator_DateFormat_Vege" runat="server"
    Type="Date" Operator="DataTypeCheck"
    ControlToValidate="ervVege_TextBox" ErrorMessage="A d�tum form�tuma nem megfelel�!"
    Display="None" Visible="false" Enabled="false" SetFocusOnError="true" />--%>
<asp:CustomValidator ID="CustomValidator_DateFormat_Vege" runat="server"
    ErrorMessage="<%$Resources:Form,DateTimeFormatValidationMessage%>" SetFocusOnError="true"
    ClientValidationFunction="Utility.Calendar.ValidateDateTimeFormat"
    EnableClientScript="true" ControlToValidate="ervVege_TextBox" Display="None" />
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_DateFormat_Vege" runat="server"
    TargetControlID="CustomValidator_DateFormat_Vege">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
    </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<script type="text/javascript">
    function calendarShown(sender, args) {
        sender._popupBehavior._element.style.zIndex = 10005;
    }
</script>
