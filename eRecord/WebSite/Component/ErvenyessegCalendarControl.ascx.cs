using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class Component_ErvenyessegCalendarControl : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        //Bug 10530 - Mindig a t0bbi control el� ny�ljon fel a calendar
        CalendarExtender1.OnClientShown = "calendarShown";
        CalendarExtender2.OnClientShown = "calendarShown";
    }

    #region public properties

    public AjaxControlToolkit.CalendarPosition PopupPosition
    {
        get { return CalendarExtender1.PopupPosition; }
        set { CalendarExtender1.PopupPosition = value; CalendarExtender2.PopupPosition = value; }
    }

    public bool bOpenDirectionTop
    {
        get
        {
            if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == true)
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                }
            }
            else
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                }
            }
        }
    }

    public bool bOpenDirectionLeft
    {
        get
        {
            if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft
                || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomLeft)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == true)
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.TopLeft)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopLeft;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomLeft;
                }
            }
            else
            {
                if (CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomRight
                    || CalendarExtender1.PopupPosition == AjaxControlToolkit.CalendarPosition.BottomLeft)
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
                }
                else
                {
                    CalendarExtender1.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                    CalendarExtender2.PopupPosition = AjaxControlToolkit.CalendarPosition.TopRight;
                }
            }
        }
    }


    // Keres�si formok miatt kell (Ervenyesseg_SearchFormComponent-en haszn�latos)
    // Ha searchMode true, akkor az ErvVege mez� nem rejti el a default ErvVege �rt�ket,
    // illetve kiolvas�sn�l az �res ErvVege nem adja vissza alapb�l a default ErvVege �rt�ket (4700.12.31)
    private bool searchMode = false;
    public bool SearchMode 
    {
        get { return searchMode; }
        set { searchMode = value; }
    }

    public string ErvKezd
    {
        get 
        {
            if (!String.IsNullOrEmpty(ervKezd_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(ervKezd_TextBox.Text);
                    if (dt == DateTime.Today)
                        //return DateTime.Now.ToString();
                        return DateTime.Today.ToString();  // mai nap 0:0 -ra �ll�tjuk
                    else
                        return dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    return ervKezd_TextBox.Text;
                }
            }
            else return "";            
        }
        set 
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    ervKezd_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                }
                catch (FormatException)
                {
                    ervKezd_TextBox.Text = value;
                }
            }
            else ervKezd_TextBox.Text = "";
        }
    }    

    public string ErvVege
    {
        get
        {
            if (!String.IsNullOrEmpty(ervVege_TextBox.Text))
            {
                try
                {
                    DateTime dt = DateTime.Parse(ervVege_TextBox.Text);
                    dt = dt.AddHours(23 - dt.Hour);
                    dt = dt.AddMinutes(59 - dt.Minute);
                    dt = dt.AddSeconds(59 - dt.Second);
                    dt = dt.AddMilliseconds(999 - dt.Millisecond);
                    return dt.ToString();
                }
                catch (FormatException)
                {
                    return ervVege_TextBox.Text;
                }
            }
            else
            {
                if (!searchMode)
                {
                    // Ha az �rv�nyess�g v�ge mez� �res, akkor a default �rv�nyess�g v�ge d�tumot vessz�k (adatb�zisban ez a 4700.12.31)
                    return new DateTime(4700, 12, 31).ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        set
        {
            if (!String.IsNullOrEmpty(value))
            {
                try
                {
                    DateTime dt = DateTime.Parse(value);
                    // ha a d�tum a default �rv�nyess�g v�ge, �resen hagyjuk a textboxot
                    // t�bbf�le default �rv�nyess�g v�ge volt haszn�latban: 4700.12.31, 4700.01.01, 2099.12.31, 9999.12.31
                    if (!searchMode &&
                            (dt.Year == 4700
                            || dt.Year == 2099 && dt.Month == 12 && dt.Day == 31
                            || dt.Year == 9999)
                       )
                    {
                        ervVege_TextBox.Text = "";
                    }
                    else
                    {
                        ervVege_TextBox.Text = dt.ToString("yyyy.MM.dd.");
                    }
                }
                catch (FormatException)
                {
                    ervVege_TextBox.Text = value;
                }
            }
            else ervVege_TextBox.Text = "";
        }
    }
    
    //public bool ReadOnly_ErvKezd
    //{
    //    get { return ErvKezd_TextBox.ReadOnly; }
    //    set 
    //    { 
    //        ErvKezd_TextBox.ReadOnly = value;                  
    //    }
    //}

    //public bool ReadOnly_ErvVege
    //{
    //    get { return ErvVege_TextBox.ReadOnly; }
    //    set 
    //    { 
    //        ErvVege_TextBox.ReadOnly = value;
    //        //if (value == true)
    //        //{
    //        //    ervVegeDatum_TextBox.CssClass = "mrUrlapCalendarDis_Erv";
    //        //}
    //        //else
    //        //{
    //        //    ervVegeDatum_TextBox.CssClass = "mrUrlapCalendar_Erv";
    //        //}
    //    }
    //}

    public bool ReadOnly_ErvKezd
    {
        set
        {
            ervKezd_TextBox.ReadOnly = value;
            ervKezd_ImageButton.Enabled = !value;
            ErvKezd_RequiredFieldValidator.Enabled = !value;
            CalendarExtender1.Enabled = !value;
            if (value)
            {
                ervKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                ervKezd_ImageButton.CssClass = "";
            }
        }
    }

    public bool ReadOnly_ErvVege
    {
        set
        {
            ervVege_TextBox.ReadOnly = value;
            ervVege_ImageButton.Enabled = !value;
            //ErvVege_RequiredFieldValidator.Enabled = !value;
            CalendarExtender2.Enabled = !value;
            if (value)
            {
                ervVege_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                ervVege_ImageButton.CssClass = "";
            }
        }
    }

    public bool Enable_ErvKezd
    {
        set
        {
            ervKezd_TextBox.Enabled = value;
            ervKezd_ImageButton.Enabled = value;
            ErvKezd_RequiredFieldValidator.Enabled = value;
            CalendarExtender1.Enabled = value;
            if (!value)
            {
                ervKezd_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                ervKezd_ImageButton.CssClass = "";
            }
        }
    }

    public bool Enable_ErvVege
    {
        set
        {
            ervVege_TextBox.Enabled = value;
            ervVege_ImageButton.Enabled = value;
            //ErvVege_RequiredFieldValidator.Enabled = value;
            CalendarExtender2.Enabled = value;
            if (!value)
            {
                ervVege_ImageButton.CssClass = "disabledCalendarImage";
            }
            else
            {
                ervVege_ImageButton.CssClass = "";
            }
        }
    }
	

    public TextBox ErvKezd_TextBox
    {
        get { return ervKezd_TextBox; }        
    }

    public TextBox ErvVege_TextBox
    {
        get { return ervVege_TextBox; }
    }

    public ImageButton ErvKezd_ImageButton
    {
        get { return ervKezd_ImageButton; }
    }

    public ImageButton ErvVege_ImageButton
    {
        get { return ervVege_ImageButton; }
    }

    public bool Validate
    {
        set
        {
            ErvKezd_RequiredFieldValidator.Enabled = value;
            ErvKezd_RequiredFieldValidator.Visible = value;
            //ErvVege_RequiredFieldValidator.Enabled = value;
            //ErvVege_RequiredFieldValidator.Visible = value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether this CustomValidator_DateFormat_Kezd and CustomValidator_DateFormat_Vege will validate.
    /// </summary>
    /// <value><c>true</c> if validate; otherwise, <c>false</c>.</value>
    public bool ValidateDateFormat
    {
        set
        {
            CustomValidator_DateFormat_Kezd.Enabled = value;
            CustomValidator_DateFormat_Kezd.Visible = value;
            CustomValidator_DateFormat_Vege.Enabled = value;
            CustomValidator_DateFormat_Vege.Visible = value;
        }
    }

    public void SetDefault()
    {
        ervKezd_TextBox.Text = DateTime.Now.ToString("yyyy.MM.dd.");
        //ervVege_TextBox.Text = DefaultDates.EndDate;
        ervVege_TextBox.Text = "";
    }

    Boolean _Enabled = true;

    public Boolean Enabled
    {
        get { return _Enabled; }
        set { 
            _Enabled = value;
            Enable_ErvKezd = _Enabled;
            Enable_ErvVege = _Enabled;
        }
    }

    Boolean _Readonly = false;

    public Boolean ReadOnly
    {
        get { return _Readonly; }
        set
        {
            _Readonly= value;
            ReadOnly_ErvKezd = _Readonly;
            ReadOnly_ErvVege = _Readonly;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            
            if (!_ViewEnabled)
            {
                ervKezd_TextBox.CssClass += " ViewReadOnlyWebControl";
                ervKezd_ImageButton.CssClass = "ViewReadOnlyWebControl";

                ervVege_TextBox.CssClass += " ViewReadOnlyWebControl";
                ervVege_ImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            
            if (!_ViewVisible)
            {
                ervKezd_TextBox.CssClass += " ViewDisabledWebControl";
                ervKezd_ImageButton.CssClass = "ViewDisabledWebControl";

                ervVege_TextBox.CssClass += " ViewDisabledWebControl";
                ervVege_ImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    public void SetErvenyessegFields(object BusinessDocument, bool updated)
    {
        if (BusinessDocument == null)
        {
            return;
        }

        System.Reflection.PropertyInfo Property_ErvKezd = BusinessDocument.GetType().GetProperty("ErvKezd");
        System.Reflection.PropertyInfo Property_ErvVege = BusinessDocument.GetType().GetProperty("ErvVege");

        System.Reflection.FieldInfo UpdatedField = BusinessDocument.GetType().GetField("Updated");
        object UpdatedObject = UpdatedField.GetValue(BusinessDocument);

        if (UpdatedObject == null) { return; }

        System.Reflection.PropertyInfo Property_Updated_ErvKezd = UpdatedObject.GetType().GetProperty("ErvKezd");
        System.Reflection.PropertyInfo Property_Updated_ErvVege = UpdatedObject.GetType().GetProperty("ErvVege");

        bool ervKezdMaiNap = false;
        try
        {
            DateTime dt_ErvKezd = DateTime.Parse(ervKezd_TextBox.Text);
            if (dt_ErvKezd == DateTime.Today)
            {
                ervKezdMaiNap = true;
            }
        }
        catch
        {
            ervKezdMaiNap = false;
        }

        bool insert = false;
        // Insert m�dban vagyunk-e? (Command=New)
        string commandName = Page.Request.QueryString.Get(QueryStringVars.Command);
        if (commandName == CommandName.New)
        {
            insert = true;
        }

        
        
        // Updated mez�k �ll�t�sa:
        // Ha az �rv�nyess�g kezdete a mai nap, �s insert m�velet van, az ErvKezd mez�t null-ra �ll�tjuk,
        // hogy majd az SQL szerveren be�ll�t�djon a getdate() default �rt�k
        if (ervKezdMaiNap && insert)
        {
            Property_ErvKezd.SetValue(BusinessDocument, "<null>", null);
        }
        else
        {
            Property_ErvKezd.SetValue(BusinessDocument, ErvKezd, null);
        }
        
        Property_ErvVege.SetValue(BusinessDocument, ErvVege, null);

        Property_Updated_ErvKezd.SetValue(UpdatedObject, updated, null);
        Property_Updated_ErvVege.SetValue(UpdatedObject, updated, null);

    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(ErvKezd_TextBox);
        componentList.Add(ErvKezd_ImageButton);
        componentList.Add(ErvVege_TextBox);
        componentList.Add(ErvVege_ImageButton);

        // Lekell tiltani a ClientValidator
        ErvKezd_RequiredFieldValidator.Enabled = false;
        //ErvVege_RequiredFieldValidator.Enabled = false;
        CompareValidator1.Enabled = false;
        CustomValidator_DateFormat_Kezd.Enabled = false;
        CustomValidator_DateFormat_Vege.Enabled = false;
        
        CalendarExtender1.Enabled = false;
        ErvKezd_ImageButton.OnClientClick = "";
        CalendarExtender2.Enabled = false;
        ErvVege_ImageButton.OnClientClick = "";

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        if (!String.IsNullOrEmpty(ErvKezd) && !String.IsNullOrEmpty(ErvVege))
        {
            text = ErvKezd + Query.OperatorNames[Query.Operators.between] + ErvVege;
        }
        else
        {
            if (!String.IsNullOrEmpty(ErvKezd))
            {
                text = Query.OperatorNames[Query.Operators.greaterorequal] + ErvKezd;
            }

            if (!String.IsNullOrEmpty(ErvVege))
            {
                text = Query.OperatorNames[Query.Operators.lessorequal] + ErvVege;
            }
        }

        return text;
    }

    #endregion
    #region IScriptControl Members

    private ScriptManager sm;
    private bool ajaxEnabled = true;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        return null;
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!ajaxEnabled)
            return null;

        List<ScriptReference> listOfScripRefernces = new List<ScriptReference>();

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/Calendar.js";
        listOfScripRefernces.Add(reference);

        return listOfScripRefernces.ToArray();
    }

    #endregion

}
