using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class Component_EvIntervallumControl : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{
    private bool isIntervallumMode = true;
    /// <summary>
    /// Intervallumk�nt(default) viselkedik vagy egy mez�k�nt
    /// </summary>
    public bool IsIntervallumMode
    {
        get { return isIntervallumMode; }
        set { isIntervallumMode = value; }
    }

    private bool autoCopy = true;

    public bool AutoCopy
    {
        get { return autoCopy; }
        set { autoCopy = value; }
    }

   

    /// <summary>
    /// Handles the Init event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Enabled && isIntervallumMode)
        {
            EvTol_masol_ImageButton.OnClientClick = "JavaScript: var controlEvIg = document.getElementById('" + EvIgTextBox.ClientID + "');controlEvIg.value = " +
               " document.getElementById('" + EvTolTextBox.ClientID + "').value;$common.tryFireEvent(controlEvIg, 'change');" +
               "return false;";
        }

        if (autoCopy)
        {
            string jsCopy = ";var evIg = $get('" + EvIgTextBox.ClientID + "');var evTol = $get('" + EvTolTextBox.ClientID + "');"
                          + "if(evTol && evIg) evIg.value = evTol.value;";

            EvTolTextBox.Attributes["onkeyup"] += jsCopy;
        }


        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!isIntervallumMode)
        {
            EvIgTextBox.Visible = false;
            FilteredTextBoxExtender2.Enabled = false;
            EvTol_masol_ImageButton.Visible = false;
        }
    }

    /// <summary>
    /// EvTol �s EvIg be�ll�t�sa a megadott keres�si mez�nek
    /// </summary>
    public void SetSearchObjectFields(Field SearchField)
    {
        if (!String.IsNullOrEmpty(EvTol) || !String.IsNullOrEmpty(EvIg))
        {
            if (String.IsNullOrEmpty(EvTol))
            {
                SearchField.Value = EvIg;
                SearchField.Operator = Query.Operators.lessorequal;
            }
            else if (String.IsNullOrEmpty(EvIg))
            {
                SearchField.Value = EvTol;
                SearchField.Operator = Query.Operators.greaterorequal;
            }
            else
            {
                SearchField.Value = EvTol;
                SearchField.ValueTo = EvIg;
                SearchField.Operator = Query.Operators.between;
            }
        }
    }

    /// <summary>
    /// TextBoxok be�ll�t�sa a keres�si objektum mez�je alapj�n
    /// </summary>
    /// <param name="SearchField"></param>
    public void SetComponentFromSearchObjectFields(Field SearchField)
    {
        if (SearchField.Operator == Query.Operators.lessorequal)
        {
            EvIg = SearchField.Value;
        }
        else if (SearchField.Operator == Query.Operators.greaterorequal)
        {
            EvTol = SearchField.Value;
        }
        else
        {
            EvTol = SearchField.Value;
            EvIg = SearchField.ValueTo;
        }
    }

    #region public properties

    /// <summary>
    /// Gets or sets the erv kezd.
    /// </summary>
    /// <value>The erv kezd.</value>
    public string EvTol
    {
        get
        {
            return EvTol_TextBox.Text;
        }
        set
        {
            EvTol_TextBox.Text = value;
        }
    }

    public int EvTol_Integer
    {
        get
        {
            int evTolInt;
            if (Int32.TryParse(EvTol, out evTolInt))
            {
                return evTolInt;
            }
            else
            {
                throw new FormatException("�v form�tuma nem megfelel�");
            }
        }
    }

    /// <summary>
    /// Gets or sets the erv vege.
    /// </summary>
    /// <value>The erv vege.</value>
    public string EvIg
    {
        get
        {

            return EvIg_TextBox.Text;
        }
        set
        {
            EvIg_TextBox.Text = value;
        }
    }

    public int EvIg_Integer
    {
        get
        {
            int evIgInt;
            if (Int32.TryParse(EvIg, out evIgInt))
            {
                return evIgInt;
            }
            else
            {
                throw new FormatException("�v form�tuma nem megfelel�");
            }
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv kezd].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv kezd]; otherwise, <c>false</c>.</value>
    public bool Enable_EvTol
    {
        set
        {
            EvTol_TextBox.Enabled = value;
            FilteredTextBoxExtender1.Enabled = value;
        }
    }

    public bool Readonly_EvTol
    {
        set
        {
            EvTol_TextBox.ReadOnly = value;
            FilteredTextBoxExtender1.Enabled = !value;
        }
    }

    /// <summary>
    /// Sets a value indicating whether [enable_ erv vege].
    /// </summary>
    /// <value><c>true</c> if [enable_ erv vege]; otherwise, <c>false</c>.</value>
    public bool Enable_EvIg
    {
        set
        {
            EvIg_TextBox.Enabled = value;
            FilteredTextBoxExtender2.EnableClientState = value;
            EvTol_masol_ImageButton.Enabled = value;
            if (!value)
            {
                EvTol_masol_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }

    public bool Readonly_EvIg
    {
        set
        {
            EvIg_TextBox.ReadOnly = value;
            FilteredTextBoxExtender2.Enabled = !value;
            EvTol_masol_ImageButton.Enabled = !value;
            if (value)
            {
                EvTol_masol_ImageButton.CssClass = "disabledCalendarImage";
            }
        }
    }


    /// <summary>
    /// Gets the erv kezd_ text box.
    /// </summary>
    /// <value>The erv kezd_ text box.</value>
    public TextBox EvTol_TextBox
    {
        get { return EvTolTextBox; }
    }

    /// <summary>
    /// Gets the erv vege_ text box.
    /// </summary>
    /// <value>The erv vege_ text box.</value>
    public TextBox EvIg_TextBox
    {
        get
        {
            if (isIntervallumMode)
                return EvIgTextBox;
            else
                return EvTolTextBox;
        }
    }

    public ImageButton ImageButton_Masol
    {
        get { return EvTol_masol_ImageButton; }
    }

    private Boolean _setDefaultEvTol;
    /// <summary>
    /// Kit�ltse-e az �rv�nyess�g kezdete mez�t.
    /// </summary>
    /// <value><c>true</c> if [set default erv kezd]; otherwise, <c>false</c>.</value>
    public bool SetDefaultEvTol
    {
        set
        {
            _setDefaultEvTol = value;
        }

    }

    private Boolean _setDefaultEvIg;
    /// <summary>
    /// Kit�ltse-e az �rv�nyess�g v�ge mez�t.
    /// </summary>
    /// <value><c>true</c> if [set default erv vege]; otherwise, <c>false</c>.</value>
    public bool SetDefaultEvIg
    {
        set
        {
            _setDefaultEvIg = value;
        }

    }

    /// <summary>
    /// Sets the default.
    /// </summary>
    public void SetDefault()
    {
        if (_setDefaultEvTol)
            EvTol_TextBox.Text = DateTime.Now.Year.ToString();
        if (_setDefaultEvIg)
            EvIg_TextBox.Text = DateTime.Now.Year.ToString();
    }

    Boolean _Enabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="Component_ErvenyessegCalendarControl"/> is enabled.
    /// </summary>
    /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            Enable_EvTol = _Enabled;
            Enable_EvIg = _Enabled;
        }
    }

    Boolean _Readonly = false;

    public Boolean ReadOnly
    {
        get { return _Readonly; }
        set
        {
            _Readonly = value;
            Readonly_EvTol = _Readonly;
            Readonly_EvIg = _Readonly;
        }
    }

    Boolean _ViewEnabled = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view enabled].
    /// </summary>
    /// <value><c>true</c> if [view enabled]; otherwise, <c>false</c>.</value>
    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;

            if (!_ViewEnabled)
            {
                ReadOnly = !_ViewEnabled;
                EvTol_TextBox.CssClass += "ViewReadOnlyWebControl";
                EvTol_masol_ImageButton.CssClass = "ViewReadOnlyWebControl";
                EvIg_TextBox.CssClass += "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    /// <summary>
    /// Gets or sets a value indicating whether [view visible].
    /// </summary>
    /// <value><c>true</c> if [view visible]; otherwise, <c>false</c>.</value>
    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;

            if (!_ViewVisible)
            {
                Enabled = _ViewVisible;
                EvTol_TextBox.CssClass += "ViewDisabledWebControl";
                EvTol_masol_ImageButton.CssClass = "ViewDisabledWebControl";
                EvIg_TextBox.CssClass += "ViewDisabledWebControl";
            }
        }
    }

    #endregion

    #region ISelectableUserComponent method implementations

    /// <summary>
    /// Gets the component list.
    /// </summary>
    /// <returns></returns>
    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(EvTol_TextBox);
        componentList.Add(EvIg_TextBox);
        componentList.Add(EvTol_masol_ImageButton);


        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;
        Field field = new Field();
        this.SetSearchObjectFields(field);
        switch (field.Operator)
        {
            case Query.Operators.greaterorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.lessorequal:
                text = Query.OperatorNames[field.Operator] + field.Value;
                break;
            case Query.Operators.between:
                text = field.Value + Query.OperatorNames[field.Operator] + field.ValueTo;
                break;

        }

        return text;
    }

    #endregion
}
