﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FelelosSzervezetVezetoTextBox.ascx.cs" Inherits="Component_FelelosSzervezetVezetoTextBox" %>

<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-ui.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/JavaScripts/json2.js") %>"></script>
<script type="text/javascript">
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function () {
        loadFelelosSzervezetVezetoData();
    });
    $(function () {
        loadFelelosSzervezetVezetoData();
    });
    function loadFelelosSzervezetVezetoData() {
        if ($("[id$=HiddenField_FelelosSzervezetId]").val() != null) {
            $.ajax({
                url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetFelelosSzervezetVezeto") %>',
                data: JSON.stringify({
                    prefixText: "",
                    count: $("[id$=HiddenField_Svc_RowsCount]").val(),
                    contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ";"
                    + $("[id$=HiddenField_FelelosSzervezetId]").val()
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    fillFelelosSzervezetVezetoTextBox(data)
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }
        else {
            $("inputFelelosSzervezetVezeto").val("");
        }
    }
    function fillFelelosSzervezetVezetoTextBox(data) {
        if (data === null || data.d === null || data.d.length < 1) {
            $("inputFelelosSzervezetVezeto").val("");
            return;
        }
      
        var name = data.d.split('|')[0];
        var value = data.d.split('|')[1];
        //console.log(name);
        document.getElementById("inputFelelosSzervezetVezeto").value = name;

        $("[id$=HiddenField_FelelosSzervezetId]").val(value);
        $("[id$=HiddenField_FelelosSzervezetNev]").val(name);
    };

    function SetFelelosSzervezetVezeto(id, name) {
        $("[id$=HiddenField_FelelosSzervezetId]").val(id);
        $("[id$=HiddenField_FelelosSzervezetNev]").val(name);
        loadFelelosSzervezetVezetoData();
    }
</script>

<input type="text" id="inputFelelosSzervezetVezeto" readonly="readonly" disabled="disabled" style="width:250px;" />

<asp:HiddenField ID="HiddenField_FelelosSzervezetId" runat="server" />
<asp:HiddenField ID="HiddenField_FelelosSzervezetNev" runat="server" />

<asp:HiddenField ID="HiddenField_FelelosSzervezetVezetoId" runat="server" />
<asp:HiddenField ID="HiddenField_FelelosSzervezetVezetoNev" runat="server" />

<asp:HiddenField ID="HiddenField_Svc_FelhasznaloId" runat="server" Value="" />
<asp:HiddenField ID="HiddenField_Svc_RowsCount" runat="server" Value="1" />
