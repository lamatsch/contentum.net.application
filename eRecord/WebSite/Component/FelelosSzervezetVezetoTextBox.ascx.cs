﻿using Contentum.eAdmin.Utility;
using System;

public partial class Component_FelelosSzervezetVezetoTextBox : System.Web.UI.UserControl
{
    public string HiddenFieldFelelosSzervezetId
    {
        set { HiddenField_FelelosSzervezetId.Value = value; }
        get { return HiddenField_FelelosSzervezetId.Value; }
    }
    public string HiddenFieldFelelosSzervezetNev
    {
        set { HiddenField_FelelosSzervezetNev.Value = value; }
        get { return HiddenField_FelelosSzervezetNev.Value; }
    }

    public string HiddenFieldFelelosSzervezetVezetoId
    {
        set { HiddenField_FelelosSzervezetVezetoId.Value = value; }
        get { return HiddenField_FelelosSzervezetVezetoId.Value; }
    }
    public string HiddenFieldFelelosSzervezetVezetoNev
    {
        set { HiddenField_FelelosSzervezetVezetoNev.Value = value; }
        get { return HiddenField_FelelosSzervezetVezetoNev.Value; }
    }

    public string HiddenFieldSvcFelhasznaloId
    {
        set { HiddenField_Svc_FelhasznaloId.Value = value; }
        get { return HiddenField_Svc_FelhasznaloId.Value; }
    }
    public string HiddenFieldSvcRowsCount
    {
        set { HiddenField_Svc_RowsCount.Value = value; }
        get { return HiddenField_Svc_RowsCount.Value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
        HiddenField_Svc_RowsCount.Value = "1";
    }

    public void SetSzervezet(string id, string name)
    {
        HiddenFieldFelelosSzervezetId = id;
        HiddenFieldFelelosSzervezetNev = name;
    }
    public void SetSzervezet(string id)
    {
        HiddenFieldFelelosSzervezetId = id;
    }
}