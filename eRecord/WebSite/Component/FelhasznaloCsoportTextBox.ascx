<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FelhasznaloCsoportTextBox.ascx.cs" Inherits="Component_FelhasznaloCsoportTextBox" %>

<%@ Register Namespace="Contentum.eUIControls" TagPrefix="eUI" %>
<%@ Register Src="CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="cst" %>
<div class="DisableWrap">
<asp:TextBox ID="CsoportMegnevezes" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
 <asp:HiddenField ID="HiddenField1" runat="server" />
<asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')" CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" ToolTip="Kiválaszt" />
<asp:ImageButton TabIndex = "-1" ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton" ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" ToolTip="Megtekint" />
<asp:ImageButton TabIndex = "-1" ID="ResetImageButton" runat="server" Visible="false" ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')" onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Alapállapot" ToolTip="Alapállapot"/>
<asp:CheckBox ID="cbAllDolgozo" runat="server" Visible="false" Text="Összes dolgozó" Checked="false" CausesValidation="false"/>
<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="CsoportMegnevezes"
    Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true" MinimumPrefixLength="2" 
      TargetControlID="CsoportMegnevezes" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true" 
      ServicePath="http://localhost:100/eAdminWebService/KRT_CsoportokService.asmx" ServiceMethod="GetCsoportokList"
      CompletionListItemCssClass="GridViewRowStyle" CompletionListHighlightedItemCssClass="GridViewLovListSelectedRowStyle"
      CompletionListCssClass="AutoCompleteExtenderCompletionList"
      ></ajaxToolkit:AutoCompleteExtender>
      
      <eUI:WorldJumpExtender ID="WorldJumpExtender1" runat="server" TargetControlID="CsoportMegnevezes" AutoCompleteExtenderId="AutoCompleteExtender1" />
</div>