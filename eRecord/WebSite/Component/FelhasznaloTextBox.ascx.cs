using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_FelhasznaloTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{

    #region public properties

    private string filterType;

    public string FilterType
    {
        get 
        {
            if (String.IsNullOrEmpty(filterType))
                return String.Empty;
            else
                return filterType; 
        }
        set { filterType = value; }
    }

    public bool WithEmail
    {
        get
        {
            return (filterType == Constants.FilterType.Felhasznalok.WithEmail);
        }
        set
        {
            FilterType = Constants.FilterType.Felhasznalok.WithEmail;
        }
    }



    public bool Validate
    {
        set 
        { 
            Validator1.Enabled = value;            
        }
        get 
        { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        get
        {
            return Validator1.ValidationGroup;
        }
        set
        {
            Validator1.ValidationGroup = value;
        }
    }

    public bool Enabled
    {
        set
        {
            FelhasznaloMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return FelhasznaloMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set { 
            FelhasznaloMegnevezes.ReadOnly = value;
            LovImageButton.Visible = !value;
            NewImageButton.Visible = !value;
            ResetImageButton.Visible = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;
            //if (value == true)
            //{
            //    LovImageButton.CssClass = "disabledLovListItem";
            //    NewImageButton.CssClass = "disabledLovListItem";
            //    ResetImageButton.CssClass = "disabledLovListItem";
            //}
            //else
            //{
            //    LovImageButton.CssClass = "mrUrlapInputImageButton";
            //    NewImageButton.CssClass = "mrUrlapInputImageButton";
            //    ResetImageButton.CssClass = "mrUrlapInputImageButton";
            //}

        }
        get { return FelhasznaloMegnevezes.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { FelhasznaloMegnevezes.Text = value; }
        get { return FelhasznaloMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return FelhasznaloMegnevezes; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                FelhasznaloMegnevezes.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                FelhasznaloMegnevezes.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    /* Gombok l�that�s�ga */
    public Boolean LovImageButtonVisible
    {
        set
        {
            LovImageButton.Visible = value;
        }
    }
    public Boolean NewImageButtonVisible
    {
        set
        {
            NewImageButton.Visible = value;
        }
    }
    public Boolean ViewImageButtonVisible
    {
        set
        {
            ViewImageButton.Visible = value;
        }
    }
    public Boolean ResetImageButtonVisible
    {
        set
        {
            ResetImageButton.Visible = value;
        }
    }
    /* ---- */

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    public string CssClass
    {
        get
        {
            return TextBox.CssClass;
        }
        set
        {
            TextBox.CssClass = value;
        }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }

    private bool customTextEnabled = false;

    public bool CustomTextEnabled
    {
        get { return customTextEnabled; }
        set { customTextEnabled = value; }
    }

    public string BehaviorID {
        get
        {
            return this.TextBox.ClientID + "_Behavior";
        }
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {  
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        OnClick_New = JavaScripts.SetOnClientClick("FelhasznalokForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + FelhasznaloMegnevezes.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "FelhasznalokForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

        OnClick_Reset = "$get('" + TextBox.ClientID + "').value = '';$get('" +
            HiddenField1.ClientID + "').value = '';return false;";

        if (!String.IsNullOrEmpty(filterType))
        {
            OnClick_Lov = JavaScripts.SetOnClientClick("FelhasznalokLovList.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + FelhasznaloMegnevezes.ClientID
               + "&" + QueryStringVars.Filter + "=" + filterType
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }
        else
        {
            OnClick_Lov = JavaScripts.SetOnClientClick("FelhasznalokLovList.aspx",
               QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
               + "&" + QueryStringVars.TextBoxId + "=" + FelhasznaloMegnevezes.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        if (!ReadOnly)
        {
            ResetImageButton.Visible = !Validate;
        }

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
            WorldJumpEnabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            AutoCompleteExtender1.ContextKey = felh_Id + ";";
            if (!String.IsNullOrEmpty(filterType))
            {
                AutoCompleteExtender1.ContextKey += filterType;
            }

            //JavaScripts.RegisterCsoportAutoCompleteScript(Page, AutoCompleteExtender1.ClientID, HiddenField1.ClientID);
        }

        if (ajaxEnabled)
        {
            if (!customTextEnabled)
            {
                //Manu�lis be�r�s eset�n az id t�rl�se
                string jsClearHiddenField = "$get('" + HiddenField1.ClientID + @"').value = '';";

                TextBox.Attributes.Add("onchange", jsClearHiddenField);
            }
        }

    }

    public void SetFelhasznaloTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            // Cache-b�l szedj�k ki:
            string felhasznaloNev = eAdminService.GetFelhasznaloNevById(Id_HiddenField, Page);

            if (!string.IsNullOrEmpty(felhasznaloNev))
            {
                Text = felhasznaloNev;
            }
            else
            {
                KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = Id_HiddenField;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Felhasznalok KRT_Felhasznalok = (KRT_Felhasznalok)result.Record;
                    Text = KRT_Felhasznalok.Nev;
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                }
            }
        }
        else
        {
            Text = "";
        }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(FelhasznaloMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.CsoportAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("CustomTextEnabled", customTextEnabled);
        descriptor.AddProperty("HiddenValue", "");

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AutoComplete.js?t=20190607";

        return new ScriptReference[] { reference };
    }

    #endregion
}
