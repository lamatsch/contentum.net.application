using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_FelhasznalokMultiSelectPanel : System.Web.UI.UserControl
{
    private const string Felhasznalo_Id_Prefix = "f";
    private const string Csoport_Id_Prefix = "c";
    private const string Prefix_Separator = ":";

    #region Properties

    private bool? _CustomTextEnabled = null;
    public bool CustomTextEnabled
    {
        get
        {
            if (TextBoxSearch == null)
            {
                return _CustomTextEnabled ?? false;
            }
            else
            {
                return TextBoxSearch.CustomTextEnabled;
            }
        }
        set
        {
            if (TextBoxSearch == null)
            {
                _CustomTextEnabled = value;
            }
            else
            {
                TextBoxSearch.CustomTextEnabled = value;
            }
        }
    }

    private bool? _CsoporSelectionEnabled = null;
    public bool CsoporSelectionEnabled
    {
        get
        {
            if (rblFelhasznaloCsoport == null)
            {
                return rblFelhasznaloCsoport.Visible;
            }
            else
            {
                return _CsoporSelectionEnabled ?? false;
            }
        }

        set
        {
            if (rblFelhasznaloCsoport == null)
            {
                _CsoporSelectionEnabled = value;
            }
            else
            {
                rblFelhasznaloCsoport.Visible = value; ;
            }
        }
    }

    private int? _Rows = null;
    public int Rows
    {
        get
        {
            if (ListBoxSearchResult == null)
            {
                return _Rows ?? 5;
            }
            else
            {
                return ListBoxSearchResult.Size;
            }
        }

        set
        {
            if (ListBoxSearchResult == null)
            {
                _Rows = value;
            }
            else
            {
                ListBoxSearchResult.Size = value;
            }
        }
    }

    private string _Width = null;
    public string Width
    {
        get
        {
            if (ListBoxSearchResult == null)
            {
                return _Width ?? "100%";
            }
            else
            {
                return ListBoxSearchResult.Style["width"];
            }
        }

        set
        {
            if (ListBoxSearchResult == null)
            {
                _Width = value;
            }
            else
            {
                ListBoxSearchResult.Style["width"] = value;
            }
        }
    }

    public int Count
    {
        get { return ListBoxSearchResult.Items.Count; }
    }

    public string[] SelectedIds
    {
        get { return GetSelectedIdsByPrefix(null); }
    }

    public string[] FelhasznaloIds
    {
        get { return GetSelectedIdsByPrefix(Felhasznalo_Id_Prefix); }
    }

    public string[] CsoportIds
    {
        get { return GetSelectedIdsByPrefix(Csoport_Id_Prefix); }
    }

    public string[] SelectedTexts
    {
        get
        {
            if (ListBoxSearchResult.Items.Count == 0)
            {
                return null;
            }
            else
            {
                string[] selectedTexts = new string[ListBoxSearchResult.Items.Count];
                for (int i = 0; i < ListBoxSearchResult.Items.Count; i++)
                {
                    selectedTexts[i] = ListBoxSearchResult.Items[i].Text;
                }
                return selectedTexts;
            }
        }
    }

    public TextBox FelhasznaloTextBox
    {
        get { return this.TextBoxSearch.TextBox; }
    }

    #region BLG_2956
    //LZS
    public bool PanelEnabled
    {
        get { return Panel1.Enabled; }
        set { Panel1.Enabled = value; }
    }
    #endregion
    #endregion Properties

    #region  Utils
    private string[] GetSelectedIdsByPrefix(string prefix)
    {
        if (ListBoxSearchResult.Items.Count == 0)
        {
            return null;
        }
        else
        {
            string[] selectedIds = new string[ListBoxSearchResult.Items.Count];
            bool bIgnorePrefix = String.IsNullOrEmpty(prefix);
            int index = 0;
            foreach (ListItem item in ListBoxSearchResult.Items)
            {
                string[] elements = item.Value.Split(Prefix_Separator.ToCharArray());

                if ((bIgnorePrefix || elements[0] == prefix) && elements.Length > 1)
                {
                    selectedIds[index] = elements[1];
                    index++;
                }
            }
            if (index == 0)
            {
                return null;
            }
            else
            {
                Array.Resize(ref selectedIds, index);
                return selectedIds;
            }
        }
    }

    private void AddStyleToListItem(ListItem item)
    {
        // ListItem: fontra vonatkozó jellemzők nem használhatók, csak a HtmlSelect szintjén!
        if (item != null)
        {
            string[] elements = item.Value.Split(Prefix_Separator.ToCharArray());

            switch (elements[0])
            {
                case Csoport_Id_Prefix:
                    item.Attributes.CssStyle.Add("color", "#2D75B1");
                    break;
                case Felhasznalo_Id_Prefix:
                    // default style
                    break;

            }
        }
    }

    #endregion Utils

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        RegisterJavaScrips();

        foreach (ListItem item in rblFelhasznaloCsoport.Items)
        {
            item.Attributes.Add("onclick", "setFelhasznaloCsoportInputVisibility(this)");
        }

        #region Apply properties here (not in the setters, otherwise null reference exception when calling from markup)
        // CustomTextEnabled
        if (_CustomTextEnabled != null)
        {
            TextBoxSearch.CustomTextEnabled = (bool)_CustomTextEnabled;
        }

        // CsoportSelectionEnabled
        if (_CsoporSelectionEnabled != null)
        {
            if (_CsoporSelectionEnabled == false)
            {
                rblFelhasznaloCsoport.SelectedValue = "f"; // felhasználó
            }
            rblFelhasznaloCsoport.Visible = (bool)_CsoporSelectionEnabled;
        }

        // Rows
        if (_Rows != null)
        {
            ListBoxSearchResult.Size = (int)_Rows;
        }

        // Width
        if (_Width != null)
        {
            ListBoxSearchResult.Style["width"] = _Width;
        }

        #endregion Apply properties here (not in the setters, otherwise null reference exception when calling from markup)
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListBoxSearchResult.Items.Clear();
            TextBoxSearch.TextBox.Width = Unit.Pixel(300);
            TextBoxSearchCsoport.TextBox.Width = Unit.Pixel(300);
        }
        else
        {
            foreach (ListItem item in ListBoxSearchResult.Items)
            {
                AddStyleToListItem(item);
            }

            switch (rblFelhasznaloCsoport.SelectedValue)
            {
                case "f":
                    tr_felhasznaloselector.Style.Add("display", "");
                    tr_csoportselector.Style.Add("display", "none");
                    break;
                case "c":
                    tr_felhasznaloselector.Style.Add("display", "none");
                    tr_csoportselector.Style.Add("display", "");
                    break;
            }
        }

        TextBoxSearch.TextBox.AutoPostBack = true;
        TextBoxSearch.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);

        TextBoxSearchCsoport.TextBox.AutoPostBack = true;
        TextBoxSearchCsoport.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        RegisterJavaScrips();

        foreach (ListItem item in rblFelhasznaloCsoport.Items)
        {
            item.Attributes.Add("onclick", "setFelhasznaloCsoportInputVisibility(this)");
        }
    }

    #region Event Handlers
    void TextBox_TextChanged(object sender, EventArgs e)
    {
        AddTextBoxSearchToListBoxSearchResult();
    }

    #endregion Event Handlers

    #region Public methods
    public void Clear()
    {
        TextBoxSearch.Text = "";
        TextBoxSearch.Id_HiddenField = "";
        TextBoxSearchCsoport.Text = "";
        TextBoxSearchCsoport.Id_HiddenField = "";
        ListBoxSearchResult.Items.Clear();
    }
    #endregion Public methods

    private void RegisterJavaScrips()
    {
        string js = @"
function show(item, bShowIt)
{
    if (item && item.style) item.style.display=(bShowIt?'':'none');
}
function setFelhasznaloCsoportInputVisibility(radiobutton) {
    if (radiobutton)
    {
        var cssel = $get('" + tr_csoportselector.ClientID + @"');
        var fsel = $get('" + tr_felhasznaloselector.ClientID + @"');
        if (cssel && fsel) {
            show(fsel, radiobutton.checked && radiobutton.value == 'f');
            show(cssel, radiobutton.checked && radiobutton.value == 'c');
        }
    }
    return false;
}
";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);

    }


    private void AddTextBoxSearchToListBoxSearchResult()
    {
        bool isFelhasznalo = (rblFelhasznaloCsoport.SelectedValue == "f");
        if ((isFelhasznalo && TextBoxSearch.Text.Trim() != "")
            || (!isFelhasznalo && TextBoxSearchCsoport.Text.Trim() != "")
            )
        {

            string text = (isFelhasznalo ? TextBoxSearch.Text : TextBoxSearchCsoport.Text);
            string value = (isFelhasznalo ? TextBoxSearch.Id_HiddenField : TextBoxSearchCsoport.Id_HiddenField);

            if (isFelhasznalo && TextBoxSearch.CustomTextEnabled == true && String.IsNullOrEmpty(value))
            {
                value = text;
            }
            if (!String.IsNullOrEmpty(value))
            {
                value = String.Concat(isFelhasznalo ? Felhasznalo_Id_Prefix : Csoport_Id_Prefix, Prefix_Separator, value);
                ListItem item = new ListItem(text, value);

                AddStyleToListItem(item);

                if (!ListBoxSearchResult.Items.Contains(item))
                {
                    ListBoxSearchResult.Items.Add(item);
                }
                if (isFelhasznalo)
                {
                    TextBoxSearch.Text = "";
                    TextBoxSearch.Id_HiddenField = "";
                }
                else
                {
                    TextBoxSearchCsoport.Text = "";
                    TextBoxSearchCsoport.Id_HiddenField = "";
                }
            }
        }
    }
    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        AddTextBoxSearchToListBoxSearchResult();
    }

    protected void ButtonRemove_Click(object sender, ImageClickEventArgs e)
    {
        if (ListBoxSearchResult.Multiple)
        {
            for (int i = ListBoxSearchResult.Items.Count - 1; i > -1; i--)
            {
                if (ListBoxSearchResult.Items[i].Selected)
                {
                    ListBoxSearchResult.Items.RemoveAt(i);
                }
            }
        }
        else
        {
            if (ListBoxSearchResult.SelectedIndex > -1 && ListBoxSearchResult.Items[ListBoxSearchResult.SelectedIndex] != null)
            {
                ListBoxSearchResult.Items.Remove(ListBoxSearchResult.Items[ListBoxSearchResult.SelectedIndex]);
            }
        }
    }
}
