<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormFooter.ascx.cs" Inherits="Component_FormFooter" %>
<table style="width: 100%;">
    <tr>
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelSave" runat="server"></asp:Label>
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelSaveAndNew" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelSaveAndPrint" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelAccept" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelDecline" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelClose" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelCancel" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelBack" runat="server"></asp:Label>
        </td>        
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelResultList_ExportExcel" runat="server"></asp:Label>
        </td>    
        <td style="text-align: center; width: 10%;">
            <asp:Label ID="LabelDefault" runat="server"></asp:Label>
        </td>  
    </tr>
    <tr>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "1" ID="ImageSave" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" 
                OnClick="ImageButton_Click" CommandName="Save" Visible="False" />         
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex="2" ID="ImageSaveAndNew" runat="server"
                ImageUrl="~/images/hu/ovalgomb/rendben_es_uj.png"
                onmouseover="swapByName(this.id,'rendben_es_uj2.png')" onmouseout="swapByName(this.id,'rendben_es_uj.png')"
                OnClick="ImageButton_Click" CommandName="SaveAndNew" Visible="False" />     
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex="2" ID="ImageSaveAndPrint" runat="server"
                ImageUrl="~/images/hu/ovalgomb/rendben_es_nyomtat.png"
                onmouseover="swapByName(this.id,'rendben_es_nyomtat2.png')" onmouseout="swapByName(this.id,'rendben_es_nyomtat.png')"
                OnClick="ImageButton_Click" CommandName="SaveAndPrint" Visible="False" />     
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "3" ID="ImageAccept" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/jovahagyas.gif" 
                onmouseover="swapByName(this.id,'jovahagyas2.gif')" onmouseout="swapByName(this.id,'jovahagyas.gif')" 
                CommandName="KolcsonzesJovahagyas" OnClick="ImageButton_Click" Visible="False" />         
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "4" ID="ImageDecline" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/elutasitas.gif" 
                onmouseover="swapByName(this.id,'elutasitas2.gif')" onmouseout="swapByName(this.id,'elutasitas.gif')" 
                CommandName="KolcsonzesVisszautasitas" OnClick="ImageButton_Click" Visible="False" />         
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "5" ID="ImageClose" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/bezar.jpg" 
                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')" 
                OnClientClick="window.close(); return false;"                
                OnClick="ImageButton_Click" CommandName="Close" />         
        </td>      
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "6" ID="ImageCancel" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/megsem.jpg" 
                onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')" 
                OnClientClick="window.close(); return false;" CommandName="Cancel" 
                OnClick="ImageButton_Click" Visible="False" />         
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "7" ID="ImageBack" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/vissza.jpg" 
                onmouseover="swapByName(this.id,'vissza2.jpg')" onmouseout="swapByName(this.id,'vissza.jpg')" 
                CommandName="Back" OnClick="ImageButton_Click" Visible="False" CausesValidation="False" />         
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "8" ID="ImageResultList_ExportExcel" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/export.jpg" 
                onmouseover="swapByName(this.id,'export2.jpg')" onmouseout="swapByName(this.id,'export.jpg')" 
                CommandName="ResultList_ExportExcel" OnClick="ImageButton_Click" Visible="False" CausesValidation="False" />         
        </td>
        <td style="text-align: center; width: 10%;">
            <asp:ImageButton TabIndex = "9" ID="ImageDefault" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/alapallapot.jpg" 
                onmouseover="swapByName(this.id,'alapallapot2.jpg')" onmouseout="swapByName(this.id,'alapallapot.jpg')" 
                CommandName="Default" OnClick="ImageButton_Click" Visible="False" CausesValidation="False" />         
        </td>    
    </tr>
</table>