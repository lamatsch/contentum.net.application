using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_FormFooter : System.Web.UI.UserControl
{
    private string _Command = "";

    public bool SaveEnabled
    {
        get { return ImageSave.Enabled; }
        set { ImageSave.Enabled = value; }
    }

    public ImageButton ImageButton_Save
    {
        get { return ImageSave; }
    }
    public ImageButton ImageButton_SaveAndNew
    {
        get { return ImageSaveAndNew; }
    }
    //BLG_1130
    public ImageButton ImageButton_SaveAndPrint
    {
        get { return ImageSaveAndPrint; }
    }
    public ImageButton ImageButton_Close
    {
        get { return ImageClose; }
    }
    public ImageButton ImageButton_Cancel
    {
        get { return ImageCancel; }
    }
    public ImageButton ImageButton_Back
    {
        get { return ImageBack; }
    }
    public ImageButton ImageButton_Accept
    {
        get { return ImageAccept; }
    }
    public ImageButton ImageButton_Decline
    {
        get { return ImageDecline; }
    }
    public ImageButton ImageButton_ResultList_ExportExcel
    {
        get { return ImageResultList_ExportExcel; }
    }

    public ImageButton ImageButton_Default
    {
        get { return ImageDefault; }
    }

    public Label Label_Accept
    {
        get { return LabelAccept; }
    }

    public Label Label_Back
    {
        get { return LabelBack; }
    }

    public Label Label_Cancel
    {
        get { return LabelCancel; }
    }

    public Label Label_Close
    {
        get { return LabelClose; }
    }

    public Label Label_Decline
    {
        get { return LabelDecline; }
    }

    public Label Label_ResultList_ExportExcel
    {
        get { return LabelResultList_ExportExcel; }
    }

    public Label Label_Save
    {
        get { return LabelSave; }
    }

    public Label Label_SaveAndNew
    {
        get { return LabelSaveAndNew; }
    }

    public Label Label_SaveAndPrint
    {
        get { return LabelSaveAndPrint; }
    }

    public Label Label_Default
    {
        get { return LabelDefault; }
    }

    public string Command
    {
        get { return _Command; }
        set { _Command = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Command == "")
        {
            Command = Request.QueryString.Get("Command");
        }
        setButtonsVisibleMode(Command);

        //ne lehessen t?bbsz?r kattintani szkript
        ImageButton_Save.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_Save);
        ImageButton_SaveAndNew.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_SaveAndNew);
        //BLG_1130
        ImageButton_SaveAndPrint.OnClientClick += JavaScripts.SetDisableButtonOnClientClick(Page, ImageButton_SaveAndPrint);
        //UI.SwapImageToDisabled(ImageSave);

        //if (!ImageSave.Enabled)
        //{
        //    int pont = ImageSave.ImageUrl.IndexOf('.');
        //    ImageSave.ImageUrl = ImageSave.ImageUrl.Substring(0, pont) + "2" + ImageSave.ImageUrl.Substring(pont);
        //}

    }

    public event CommandEventHandler ButtonsClick;
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    private void setButtonsVisibleMode(string mode)
    {
        switch (mode)
        {
            case CommandName.New:
                ImageSave.Visible = true;
                ImageClose.Visible = false;
                ImageCancel.Visible = true;
                ImageBack.Visible = false;
                break;
            case CommandName.View:
                ImageSave.Visible = false;
                ImageClose.Visible = true;
                ImageCancel.Visible = false;
                ImageBack.Visible = false;
                break;
            case CommandName.Modify:
                ImageSave.Visible = true;
                ImageClose.Visible = false;
                ImageCancel.Visible = true;
                ImageBack.Visible = false;
                break;
            case CommandName.KolcsonzesJovahagyas:
                ImageAccept.Visible = true;
                ImageDecline.Visible = true;
                ImageClose.Visible = true;
                break;
            case CommandName.RendbenEsNyomtat:
                ImageSave.Visible = true;
                ImageSaveAndPrint.Visible = true;
                ImageClose.Visible = true;
                break;
            case CommandName.Nyomtat:
                ImageSaveAndPrint.Visible = true;
                ImageClose.Visible = true;
                break;
            default:
                ImageSave.Visible = false;
                ImageClose.Visible = true;
                ImageCancel.Visible = false;
                ImageBack.Visible = false;
                break;
        }
    }


    public string ValidationGroup
    {
        set
        {
            ImageSave.ValidationGroup = value;
        }
        get
        {
            return ImageSave.ValidationGroup;
        }
    }
}
