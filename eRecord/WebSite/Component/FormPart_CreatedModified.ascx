<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormPart_CreatedModified.ascx.cs" Inherits="Component_FormPart_CreatedModified" %>
<%@ Register Src="~/Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc" %>
<br />
<eUI:eFormPanel ID="EFormPanel1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%">
            <tr class="urlapSor" runat="server" id="trLetrehozo">
                <td class="mrUrlapCaption">
                    <asp:Label ID="Label1" runat="server" Text="Létrehozó:" CssClass="mrUrlapCaptionDisable"></asp:Label></td>
                <td class="mrUrlapMezo">
                    <asp:TextBox ID="Letrehozo_TextBox" runat="server" CssClass="mrUrlapInputLetrehozoDisable" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="mrUrlapCaption_shorter">
                    <asp:Label ID="Label4" runat="server" Text="Létrehozás ideje:" CssClass="mrUrlapCaptionDisable"></asp:Label></td>
                <td class="mrUrlapMezo">
                    <asp:TextBox ID="Letrehozas_ido_TextBox" runat="server" CssClass="mrUrlapInputLetrehozasIdejeDisable" ReadOnly="True"></asp:TextBox></td>
            </tr>
            <tr class="urlapSor" runat="server" id="trModosito">
                <td class="mrUrlapCaption">
                    <asp:Label ID="Label2" runat="server" Text="Módosító:" CssClass="mrUrlapCaptionDisable"></asp:Label></td>
                <td class="mrUrlapMezo">
                    <asp:TextBox ID="Modosito_TextBox" runat="server" CssClass="mrUrlapInputModositoDisable" ReadOnly="True"></asp:TextBox></td>
                <td class="mrUrlapCaption_shorter">
                    <asp:Label ID="Label3" runat="server" Text="Módosítás ideje:" CssClass="mrUrlapCaptionDisable"></asp:Label></td>
                <td class="mrUrlapMezo">
                    <asp:TextBox ID="Modositas_ido_TextBox" runat="server" CssClass="mrUrlapInputModositasIdejeDisable" ReadOnly="True"></asp:TextBox></td>
            </tr>
    </table>
</eUI:eFormPanel>
