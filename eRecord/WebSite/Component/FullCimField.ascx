<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FullCimField.ascx.cs" Inherits="Component_FullCimField" %>
<%@ Register Src="OrszagTextBox.ascx" TagName="OrszagTextBox" TagPrefix="uc1" %>
<%@ Register Src="IranyitoszamTextBox.ascx" TagName="IranyitoszamTextBox" TagPrefix="uc2" %>
<%@ Register Src="TelepulesTextBox.ascx" TagName="TelepulesTextBox" TagPrefix="uc3" %>
<%@ Register Src="KozteruletTextBox.ascx" TagName="KozteruletTextBox" TagPrefix="uc4" %>
<%@ Register Src="KozteruletTipusTextBox.ascx" TagName="KozteruletTipusTextBox" TagPrefix="uc5" %>
<div class="CimField">
    <uc1:OrszagTextBox ID="OrszagTextBox1" runat="server" ImageVisible="false" CssClass="OrszagTextBox" WatermarkEnabled="true" Validate="false" Visible="false"/>
    <uc2:IranyitoszamTextBox ID="IranyitoszamTextBox1" runat="server" CssClass="IranyitoszamTextBox" WatermarkEnabled="true" Validate="false"/>
    <uc3:TelepulesTextBox ID="TelepulesTextBox1" runat="server" ImageVisible="false" CssClass="TelepulesTextBox" WatermarkEnabled="true" Validate="false"/>
    <uc4:KozteruletTextBox ID="KozteruletTextBox1" runat="server" ImageVisible="false" CssClass="KozteruletTextBox" WatermarkEnabled="true" Validate="false"/>
    <uc5:KozteruletTipusTextBox ID="KozteruletTipusTextBox1" runat="server" ImageVisible="false" CssClass="KozteruletTipusTextBox" WatermarkEnabled="true" Validate="false"/>
    <asp:TextBox ID="TobbiTextBox" runat="server" CssClass="TobbiTextBox"></asp:TextBox>
    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="TobbiTextBox" runat="server"
     WatermarkText="H�zsz�m" WatermarkCssClass="TobbiTextBox watermarked"></ajaxtoolkit:TextBoxWatermarkExtender>
</div>