﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;

public partial class FunctionKeysManager : System.Web.UI.UserControl,IScriptControl
{
    public string ObjektumId
    {
        get
        {
            return hfObjektumId.Value;
        }
        set
        {
            if (!IsFinal)
            {
                _isSet = true;
                hfObjektumId.Value = value;
            }
        }
    }

    public string ObjektumType
    {
        get
        {
            return hfObjektumType.Value;
        }
        set
        {
            if (!IsFinal)
            {
                _isSet = true;
                hfObjektumType.Value = value;
            }
        }
    }

    public readonly string[] ValidObjektumTypes = new string[] { "EREC_UgyUgyiratok" ,
    "EREC_IraIratok","EREC_PldIratPeldanyok","EREC_KuldKuldemenyek"};

    public bool IsValidObjektumType()
    {
        if (String.IsNullOrEmpty(ObjektumType))
            return false;

        if (Array.IndexOf(ValidObjektumTypes, ObjektumType) > -1)
        {
            return true;
        }

        return false;
    }

    private bool _isFinal = false;
    public bool IsFinal
    {
        get
        {
            return _isFinal;
        }
        set
        {
            _isFinal = value;
        }
    }

    private bool _isSet = false;

    public bool IsSet
    {
        get { return _isSet; }
        set { _isSet = value; }
    }

    public static FunctionKeysManager GetCurrent(Page page)
    {
        if (page == null)
        {
            throw new ArgumentNullException("page");
        }

        return page.Items[typeof(FunctionKeysManager)] as FunctionKeysManager;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!DesignMode)
        {
            if (Visible)
            {
                FunctionKeysManager existingInstance = FunctionKeysManager.GetCurrent(Page);

                if (existingInstance != null)
                {
                    throw new InvalidOperationException("FunctionKeysManager only one instance is allowed on page!");
                }

                Page.Items[typeof(FunctionKeysManager)] = this;


                Control parent = this.Parent;
                if (parent != null)
                {
                    parent.Load += new EventHandler(parent_Load);
                }

            }
        }
    }

    #region Parent UpdatePanel

    private UpdatePanel _parentUpdatePanel;

    private bool _parentUpdatePanelAdded = false;

    void parent_Load(object sender, EventArgs e)
    {
        AddParentUpdatePanel(this.Parent);
    }

    private void AddParentUpdatePanel(Control parent)
    {
        if (parent != null && !_parentUpdatePanelAdded)
        {
            InitParentUpdatePanel();
            if (this._parentUpdatePanel != null)
            {
                int index = parent.Controls.IndexOf(this);
                this._parentUpdatePanel.ContentTemplateContainer.Controls.Add(this);
                if (index > -1)
                {
                    parent.Controls.AddAt(index, this._parentUpdatePanel);
                }
                else
                {
                    parent.Controls.Add(this._parentUpdatePanel);
                }
                _parentUpdatePanelAdded = true;
            }
        }
    }

    private void InitParentUpdatePanel()
    {
        if (this._parentUpdatePanel == null)
        {
            ScriptManager sm = ScriptManager.GetCurrent(Page);
            if (sm != null)
            {
                this._parentUpdatePanel = new UpdatePanel();
                this._parentUpdatePanel.ID = "FunkcionKeysManagerUpdatePanel";
                this._parentUpdatePanel.RenderMode = UpdatePanelRenderMode.Inline;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #endregion

    public void Clear()
    {
        ObjektumId = String.Empty;
        ObjektumType = String.Empty;
    }

    #region IScriptControl Members

    private ScriptManager sm;

    public bool Enabled
    {
        get
        {
            object o = ViewState["Enabled"];
            if (o != null)
                return (bool)ViewState["Enabled"];
            return true;
        }
        set
        {
            ViewState["Enabled"] = value;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (Enabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (Enabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.FunctionKeysBehavior", MainPanel.ClientID);
        descriptor.AddProperty("HfObjektumIdClientID", hfObjektumId.ClientID);
        descriptor.AddProperty("HfObjektumTypeClientID", hfObjektumType.ClientID);
        descriptor.AddProperty("ValidObjektumTypesArray", ValidObjektumTypes);
        descriptor.AddProperty("IsFeladatFunctionKeyEnabled", Contentum.eUtility.HataridosFeladatok.IsFeladatFunctionKeyEnabled(Page));
        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/FunctionKeys.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}
