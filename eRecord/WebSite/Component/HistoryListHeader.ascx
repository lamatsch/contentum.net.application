<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HistoryListHeader.ascx.cs" Inherits="Component_HistoryListHeader" %>

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<table class="mrkFejlec" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 85px; vertical-align: top;">
            <img runat="server" id="kepernyo_lista_ikon" src="../images/hu/fejlec/kepernyo_lista_ikon.jpg" alt=" " />
            </td>
            <td style="vertical-align: top; text-align: left;">
                <table cellspacing="0" cellpadding="0" style="width: 100%">
                    <tr>
                        <td style="height: 46px; width: 300px; text-align: left; vertical-align: middle;">
                            <asp:Label ID="Header" runat="server" Text="Header" CssClass="ListHeaderText"></asp:Label>   
                        </td>
                        <td align="right" style="height: 46px; width: 400px; vertical-align: bottom; position: static; text-align: right;">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                           <table cellspacing="0" cellpadding="0" border="0" style="height: 46px;">
                                <tr>
                                    <td valign="middle" style="height: 22px;" >
                                           <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="../images/hu/egyeb/rewall.jpg" onmouseover="swapByName(this.id,'rewall2.jpg');" onmouseout="swapByName(this.id,'rewall.jpg');" OnClick="ImageButton1_Click" CommandName="First" AlternateText="Els� oldal" /></td>
                                    <td style="height: 22px; width: 16px;">
                                        &nbsp;</td>
                                    <td valign="middle" style="height: 22px">
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../images/hu/egyeb/rew.jpg" onmouseover="swapByName(this.id,'rew2.jpg')" onmouseout="swapByName(this.id,'rew.jpg')" OnClick="ImageButton1_Click" CommandName="Prev" AlternateText="El�z� oldal"/></td>
                                    <td style="height: 22px; width: 16px;">
                                        &nbsp;</td>
                                    <td valign="middle" align="center" style="height: 22px; width: 40px; background-repeat: no-repeat; background-position: center; font-weight: bold;">
                                        <asp:Label ID="Pager" runat="server" Text="Pager"></asp:Label></td>
                                    <td style="height: 22px; width: 16px;">
                                        &nbsp;</td>
                                    <td valign="middle" style="height: 22px">                                        
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../images/hu/egyeb/ffw.jpg" onmouseover="swapByName(this.id,'ffw2.jpg')" onmouseout="swapByName(this.id,'ffw.jpg')" OnClick="ImageButton1_Click" CommandName="Next" AlternateText="K�vetkez� oldal"/></td>
                                    <td style="height: 22px; width: 16px;">
                                        &nbsp;</td>
                                    <td valign="middle" style="height: 22px">
                                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="../images/hu/egyeb/ffwd.jpg" onmouseover="swapByName(this.id,'ffwd2.jpg')" onmouseout="swapByName(this.id,'ffwd.jpg')" OnClick="ImageButton1_Click" CommandName="Last" AlternateText="Utols� oldal"/></td>
                                    <td style="height: 22px; width: 16px;">
                                        &nbsp;</td>
                                </tr>
                            </table>                
                                </ContentTemplate>
                            </asp:UpdatePanel>  
                            </td>                         
                    </tr>
                    <tr>
                        <td style="height: 3px;" colspan="3">
                         <%-- Spacer--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; text-align: left; height: 32px;">
							<table runat="server" id="LeftFunctionButtonsTable" width="*" cellpadding="0" cellspacing="0">
							<tr>
									<td class="ikonSorElem" id="LeftFunctionButtonsTableButtonKereses" style="width: 31px">
                                        <asp:ImageButton ID="ImageSearch" runat="server" ImageUrl="../images/hu/ikon/kereses1_keret_nelkul.jpg" onmouseover="swapByName(this.id,'kereses1.jpg')" onmouseout="swapByName(this.id,'kereses1_keret_nelkul.jpg')" OnClick="Left_FunkcionButtons_OnClick" CommandName="kereses" AlternateText="Keres�s"/>									
									</td>
							
								<td class="ikonSorElem" id="LeftFunctionButtonsTableButtonMegnez">
                                    <asp:ImageButton ID="ImageView" runat="server" ImageUrl="../images/hu/ikon/megtekintes1_keret_nelkul.jpg" onmouseover="swapByName(this.id,'megtekintes1.jpg')" onmouseout="swapByName(this.id,'megtekintes1_keret_nelkul.jpg')" OnClick="Left_FunkcionButtons_OnClick" CommandName="View" AlternateText="Megtekint�s"/>
																
								</td>

								<td class="ikonSorElem" id="LeftFunctionButtonsTableButtonExportalas">
                                    <asp:ImageButton ID="ImageExport" runat="server" ImageUrl="../images/hu/ikon/export1_keret_nelkul.jpg" onmouseover="swapByName(this.id,'export1.jpg')" onmouseout="swapByName(this.id,'export1_keret_nelkul.jpg')" OnClick="Left_FunkcionButtons_OnClick"  OnClientClick="aspnetForm.target ='_blank';" CommandName="ExcelExport" AlternateText="Export�l�s"/>
								</td>
								<td class="ikonSorElem" id="LeftFunctionButtonsTableButtonNyomtatas">
                                    <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="../images/hu/ikon/nyomtatas1_keret_nelkul.jpg" onmouseover="swapByName(this.id,'nyomtatas1.jpg')" onmouseout="swapByName(this.id,'nyomtatas1_keret_nelkul.jpg')" OnClick="Left_FunkcionButtons_OnClick" CommandName="Print" AlternateText="Nyomtat�s"/>
								</td>
     
							</tr>
							</table>
					    </td>						
				        <td colspan="2" style="height: 32px; vertical-align: top; text-align: right;">
<%--Jobboldali funkcio gombok--%>
						<table runat="server" id="RightFunctionButtonsTable" width="*" cellpadding="0" cellspacing="0" style="height: 32px;">
						<tr>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonErkeztet">
                                        &nbsp;<%--									<a href="!bejovo_kuld_erk1.html" onmouseover="swap('erkeztet', 'images/hu/ikon/erkeztetes1.jpg');" onmouseout="swap('erkeztet', 'images/hu/ikon/erkeztetes1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/erkeztetes1_keret_nelkul.jpg" class="ikon" id="erkeztet" AlternateText="�rkeztet�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonBejovoIratIktatas">
                                        &nbsp;<%--										<a href="./bejovo-irat-ikt-1.html" onmouseover="swap('iktatas1', './images/ikon/bejovo_irat_iktatasa1.jpg');" onmouseout="swap('iktatas1', './images/ikon/bejovo_irat_iktatasa1_keret.jpg');">
										<img src="./images/ikon/bejovo_irat_iktatasa1_keret.jpg" class="ikon" id="iktatas1" alt="Bej�v� irat iktat�sa"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonSztorno">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('sztorno', 'images/hu/ikon/storno1.jpg');" onmouseout="swap('sztorno', 'images/hu/ikon/storno1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/storno1_keret_nelkul.jpg" class="ikon" id="sztorno" AlternateText="Sztorn�"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonIktatas" style="width: 5px">
                                        &nbsp;<%--									<a href="./bejovo-irat-ikt-1.html" onmouseover="swap('iktatas', 'images/hu/ikon/iktatas1.jpg');" onmouseout="swap('iktatas', 'images/hu/ikon/iktatas1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/iktatas1_keret_nelkul.jpg" class="ikon" id="iktatas" AlternateText="Iktat�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonBelsoIratIktatas">
                                        &nbsp;<%--										<a href="#" onmouseover="swap('iktatas2', './images/ikon/belso_irat_iktatasa.jpg');" onmouseout="swap('iktatas2', './images/ikon/belso_irat_iktatasa_ker_nel.jpg');">
										<img src="./images/ikon/belso_irat_iktatasa_ker_nel.jpg" class="ikon" id="iktatas2" alt="Bels� irat iktat�sa"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtIktatas">
                                        &nbsp;<%--										<a href="#" onmouseover="swap('iktatas3', './images/ikon/atiktatas1.jpg');" onmouseout="swap('iktatas3', './images/ikon/atiktatas1_keret_nelkul.jpg');">
										<img src="./images/ikon/atiktatas1_keret_nelkul.jpg" class="ikon" id="iktatas3" alt="�tiktat�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadasra" style="width: 31px">
                                        </td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadas">
                                        &nbsp;<%--										<a href="#" onmouseover="swap('atadas', './images/ikon/atadas1.jpg');" onmouseout="swap('atadas', './images/ikon/atadas1_keret_nelkul.jpg');">
										<img src="./images/ikon/atadas1_keret_nelkul.jpg" class="ikon" id="atadas" alt="�tad�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadasVissza">
                                        &nbsp;<%--										<a href="#" onmouseover="swap('atadas', './images/ikon/atadas1.jpg');" onmouseout="swap('atadas', './images/ikon/atadas1_keret_nelkul.jpg');">
										<img src="./images/ikon/atadas1_keret_nelkul.jpg" class="ikon" id="atadas" alt="�tad�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonTovabbitasra">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('tovabbitasra', 'images/hu/ikon/tovabbitasra_kijelol1.jpg');" onmouseout="swap('tovabbitasra', 'images/hu/ikon/tovabbitasra_kijelol1_keret.jpg');">
									<img src="images/hu/ikon/tovabbitasra_kijelol1_keret.jpg" class="ikon" id="tovabbitasra" AlternateText="Tov�bb�t�sra kijel�l"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonTovabbitas">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('tovabbitas', 'images/hu/ikon/tovabbitas1.jpg');" onmouseout="swap('tovabbitas', 'images/hu/ikon/tovabbitas1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/tovabbitas1_keret_nelkul.jpg" class="ikon" id="tovabbitas" AlternateText="Tov�bb�t�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonTovabbitasVissza" style="width: 5px">
                                        </td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonLezaras">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('lezaras', 'images/hu/ikon/lezaras1.jpg');" onmouseout="swap('lezaras', 'images/hu/ikon/lezaras1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/lezaras1_keret_nelkul.jpg" class="ikon" id="lezaras" AlternateText="Lez�r�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonLezarasvissza">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('lezarasvissza', 'images/hu/ikon/lezaras_visszavonasa1.jpg');" onmouseout="swap('lezarasvissza', 'images/hu/ikon/lezaras_visszavonasa1_keret.jpg');">
									<img src="images/hu/ikon/lezaras_visszavonasa1_keret.jpg" class="ikon" id="lezarasvissza" AlternateText="Lez�r�s visszavon�sa"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtvetel">
                                        &nbsp;<%--										<a href="#" onmouseover="swap('atvetel', './images/ikon/atvetel1.jpg');" onmouseout="swap('atvetel', './images/ikon/atvetel1_keret_nelkul.jpg');">
										<img src="./images/ikon/atvetel1_keret_nelkul.jpg" class="ikon" id="atvetel" alt="�tv�tel"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonRendezes">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('rendezes', 'images/hu/ikon/rendezes1.jpg');" onmouseout="swap('rendezes', 'images/hu/ikon/rendezes1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/rendezes1_keret_nelkul.jpg" class="ikon" id="rendezes" AlternateText="Rendez�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonIrattarozasrakijeloles">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('irattarozasrakij', 'images/hu/ikon/irattarozasra_kijeloles1.jpg');" onmouseout="swap('irattarozasrakij', 'images/hu/ikon/irattarozasra_kijeloles1_ke.jpg');">
									<img src="images/hu/ikon/irattarozasra_kijeloles1_ke.jpg" class="ikon" id="irattarozasrakij" AlternateText="Iratt�roz�sra kijel�l�s"/></a>
--%></td>
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonUgyiratMegtekintese">
                                        &nbsp;<%--										<a href="#" onmouseover="swap('ugyirat_megtekintese', './images/ikon/ugyirat_megtekintese1.jpg');" onmouseout="swap('ugyirat_megtekintese', './images/ikon/ugyirat_megtekintese1_keret.jpg');">
										<img src="./images/ikon/ugyirat_megtekintese1_keret.jpg" class="ikon" id="ugyirat_megtekintese" alt="�gyirat megtekint�se"/></a>
--%></td>
						
									<td class="ikonSorElem" id="RightFunctionButtonsTableButtonBoritonyomtatas">
                                        &nbsp;<%--									<a href="#" onmouseover="swap('boritonyomtatas', 'images/hu/ikon/borito_nyomtatasa1.jpg');" onmouseout="swap('boritonyomtatas', 'images/hu/ikon/borito_nyomtatasa1_keret_ne.jpg');">
									<img src="images/hu/ikon/borito_nyomtatasa1_keret_ne.jpg" class="ikon" id="boritonyomtatas" AlternateText="Bor�t� nyomtat�sa"/></a>
--%></td>														        
							        <td style="width: 10px;">
							        &nbsp;
							        </td>
						</tr>
						</table>												                              							
<%--/Jobboldali funkcio gombok--%>							                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</ContentTemplate>    
</asp:UpdatePanel>    