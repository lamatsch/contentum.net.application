<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IranyitoszamTextBox.ascx.cs" Inherits="Component_IranyitoszamTextBox" %>
<div class="DisableWrap">
<asp:TextBox ID="Iranyitoszam_TextBox" runat="server" CssClass="mrUrlapInput"
    Enabled="true"></asp:TextBox>
<asp:RequiredFieldValidator
    ID="RequiredFieldValidator1" runat="server" ControlToValidate="Iranyitoszam_TextBox" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
    TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());" />
            </Sequence>    
            </OnShow>
        </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"  
TargetControlID="Iranyitoszam_TextBox"/>
<ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="false" MinimumPrefixLength="1" 
TargetControlID="Iranyitoszam_TextBox" CompletionSetCount="20" ContextKey="" UseContextKey="true" FirstRowSelected="true" 
 ServicePath="http://localhost:100/eAdminWebService/KRT_TelepulesekService.asmx" ServiceMethod="GetIranyitoszamokList" CompletionInterval="2000"
></ajaxToolkit:AutoCompleteExtender>
<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="Iranyitoszam_TextBox" runat="server"
WatermarkText="Ir�ny�t�sz�m" WatermarkCssClass="watermarked" Enabled="false"></ajaxtoolkit:TextBoxWatermarkExtender>

</div>