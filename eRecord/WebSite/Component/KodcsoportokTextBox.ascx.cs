using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

public partial class Component_KodcsoportokTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    #region public properties

    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set { textKodcsoport.Text = value; }
        get { return textKodcsoport.Text; }
    }

    public TextBox TextBox
    {
        get { return textKodcsoport; }
    }

    public string Kodcsoport_Kod
    {
        get { return Kodcsoport_Kod_HiddenField.Value; }
    }


    public bool Enabled
    {
        set
        {
            textKodcsoport.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return textKodcsoport.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            textKodcsoport.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return textKodcsoport.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { hiddenKodcsoportID.Value = value; }
        get { return hiddenKodcsoportID.Value; }
    }

    public HiddenField HiddenField
    {
        get { return hiddenKodcsoportID; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                textKodcsoport.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                textKodcsoport.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {

        OnClick_Lov = JavaScripts.SetOnClientClick("KodcsoportokLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + hiddenKodcsoportID.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + textKodcsoport.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("KodcsoportokForm.aspx"
            , CommandName.Command + "=" + CommandName.New
            + "&" + QueryStringVars.HiddenFieldId + "=" + hiddenKodcsoportID.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + textKodcsoport.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "KodcsoportokForm.aspx", "", hiddenKodcsoportID.ClientID);

        OnClick_Reset = "$get('" + textKodcsoport.ClientID + "').value = '';$get('" +
            hiddenKodcsoportID.ClientID + "').value = '';return false;";


    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        ResetImageButton.Visible = !Validate;
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
        

    }

    public void SetTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_KodCsoportokService service = eAdminService.ServiceFactory.GetKRT_KodCsoportokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_KodCsoportok KRT_Kodcsoportok = (KRT_KodCsoportok)result.Record;
                Text = KRT_Kodcsoportok.Nev;
                // Id alapj�n val� KodCsoport visszaolvas�shoz - ObjMetaDefinicioForm haszn�lja
                // TODO: LOV-listr�l stb. vissza�rni, ha sz�ks�ges
                Kodcsoport_Kod_HiddenField.Value = KRT_Kodcsoportok.Kod;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
            Kodcsoport_Kod_HiddenField.Value = "";
        }
    }

    #region ISelectableUserComponent Members


    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(textKodcsoport);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
