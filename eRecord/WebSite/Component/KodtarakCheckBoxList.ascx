<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KodtarakCheckBoxList.ascx.cs" Inherits="Component_KodtarakCheckBoxList" %>

<div runat="server" id="listboxDiv" style="overflow-y: scroll; overflow-x:auto; POSITION: relative; HEIGHT: 80px;">
    <asp:CheckBoxList ID="KodtarakCheckBoxList" runat="server" RepeatColumns="1" RepeatDirection="Vertical"></asp:CheckBoxList>
</div>

<asp:CustomValidator runat="server" ID="RequiredFieldValidator1" ClientValidationFunction="ValidateList" Enabled="false" 
    Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:CustomValidator>

<script type="text/javascript">
function ValidateList(source, args) {
    var chkList = document.getElementById('<%= KodtarakCheckBoxList.ClientID %>');
    var chks = chkList.getElementsByTagName("input");
    for (var i = 0; i < chks.length; i++) {
        if (chks[i].checked) {
            args.IsValid = true;
            return;
        }
    }
    args.IsValid = false;
}
</script>

<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="KodtarakListBox"
    Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>--%>
<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>
            </OnShow>
     </Animations>
</ajaxToolkit:ValidatorCalloutExtender>
