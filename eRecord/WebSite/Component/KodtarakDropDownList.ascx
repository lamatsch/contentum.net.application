<%@ Control Language="C#" AutoEventWireup="true" CodeFile="KodtarakDropDownList.ascx.cs" Inherits="Component_KodtarakDropDownList" %>

<asp:DropDownList ID="Kodtarak_DropDownList" runat="server" CssClass="mrUrlapInputComboBox"></asp:DropDownList>
<asp:RadioButtonList ID="Kodtarak_RadioButtonList" runat="server" CssClass="mrUrlapInputRadioButtonList" Visible="false" RepeatDirection="Horizontal"></asp:RadioButtonList>

<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Kodtarak_DropDownList"
    Display="None" SetFocusOnError="true" Enabled="false" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>

