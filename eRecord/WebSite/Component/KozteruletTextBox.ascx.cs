using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_KozteruletTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{

    public bool WatermarkEnabled
    {
        get { return TextBoxWatermarkExtender1.Enabled; }
        set { TextBoxWatermarkExtender1.Enabled = value; }
    }

    public bool ImageVisible
    {
        get { return KozteruletImageButton.Visible; }
        set { KozteruletImageButton.Visible = value; }
    }

    public string Text
    {
        set { KozteruletNev_TextBox.Text = value; }
        get { return KozteruletNev_TextBox.Text; }
    }

    public bool Validate
    {
        set
        {
            RequiredFieldValidator1.Enabled = value;
            RequiredFieldValidator1.Visible = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public string OnClick
    {
        set { KozteruletImageButton.OnClientClick = value; }
        get { return KozteruletImageButton.OnClientClick; }
    }

    public TextBox TextBox
    {
        get { return KozteruletNev_TextBox; }
    }

    public bool Enabled
    {
        set
        {
            KozteruletNev_TextBox.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            KozteruletImageButton.Enabled = value;
            if (!value)
            {
                KozteruletImageButton.CssClass = "disabledLovListItem";
            }
            else
            { 
                KozteruletImageButton.CssClass = "mrUrlapInputImageButton";
            }
        }
        get { return KozteruletNev_TextBox.Enabled; }
    }

    public bool ReadOnly
    {
        set 
        { 
            KozteruletNev_TextBox.ReadOnly = value;
            KozteruletImageButton.Enabled = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (value)
            {
                KozteruletImageButton.CssClass = "disabledLovListItem";
            }
            else
            {                
                KozteruletImageButton.CssClass = "mrUrlapInputImageButton";
            }
        
        }
        get { return KozteruletNev_TextBox.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    public bool AutoCompleteEnabled
    {
        set { AutoCompleteExtender1.Enabled = value; }
        get { return AutoCompleteExtender1.Enabled; }
    }

    public string CssClass
    {
        get { return KozteruletNev_TextBox.CssClass; }
        set 
        { 
            KozteruletNev_TextBox.CssClass = value;
            TextBoxWatermarkExtender1.WatermarkCssClass = value + " " + TextBoxWatermarkExtender1.WatermarkCssClass;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                KozteruletNev_TextBox.CssClass += " ViewReadOnlyWebControl";
                KozteruletImageButton.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                KozteruletNev_TextBox.CssClass += " ViewDisabledWebControl";
                KozteruletImageButton.CssClass += " ViewDisabledWebControl";
            }
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //OnClick = JavaScripts.SetOnClientClick("KozteruletekLovList.aspx","", Defaults.PopupWidth, Defaults.PopupHeight,KozteruletImageButton.ClientID);

        //AutoCompleteExtender1.ServicePath = UI.GetAppSetting("eAdminBusinessServiceUrl") + "KRT_KozteruletekService.asmx";

        AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

        // �tt�ve a Page_Loadba, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        //String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //AutoCompleteExtender1.ContextKey = felh_Id + ";";

        OnClick = JavaScripts.SetOnClientClick("KozteruletekLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + KozteruletNev_TextBox.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // autentik�ci� ut�n, m�sk�pp �res marad a FelhasznaloId, ha lej�rt a session
        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        AutoCompleteExtender1.ContextKey = felh_Id + ";";

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(KozteruletNev_TextBox);
        componentList.Add(KozteruletImageButton);

        KozteruletImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion


    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
