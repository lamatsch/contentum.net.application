﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eAdmin.Utility;

public partial class Component_Letrehozas_SearchFormComponent : System.Web.UI.UserControl
{
    public bool TartomanyVisible
    {
        set
        {
            trLetrehozasTartomany.Visible = value;
        }
        get
        {
            return trLetrehozasTartomany.Visible;
        }
    }

    public bool ValidateDateFormat
    {
        set { LetrehozasCalendarControl1.ValidateDateFormat = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            SetDefault();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Keresési objektum Letrehozasido
    /// </summary>
    /// <param name="letrehozasKezd"></param>
    /// <param name="letrehozasVege"></param>
    public void SetSearchObjectFields(Field letrehozasKezd, Field letrehozasVege)
    {
        // érvényességtartomány van kiválasztva:
        // A feltétel: LetrehozasKezd <= LetrehozasCalendarControl1.LetrehozasVege && LetrehozasVege >= LetrehozasCalendarControl1.LetrehozasKezd            
        if (!String.IsNullOrEmpty(LetrehozasCalendarControl1.letrehozasKezd))
        {
            letrehozasKezd.Value = LetrehozasCalendarControl1.letrehozasKezd;
            letrehozasKezd.Operator = Query.Operators.greaterorequal;
            letrehozasKezd.Group = "100";
            letrehozasKezd.GroupOperator = Query.Operators.and;
        }
        else
        {
            letrehozasKezd.Value = DateTime.MinValue.ToString();
        }
        if (!String.IsNullOrEmpty(LetrehozasCalendarControl1.letrehozasVege))
        {
            letrehozasVege.Value = LetrehozasCalendarControl1.letrehozasVege;
        }
        else
        {
            letrehozasVege.Value = DateTime.Now.ToString();
        }
        letrehozasVege.Operator = Query.Operators.lessorequal;
        letrehozasVege.Group = "100";
        letrehozasVege.GroupOperator = Query.Operators.and;
    }

    public void SetDefault()
    {
        LetrehozasCalendarControl1.SetDefault();
        LetrehozasTartomany_Label.Enabled = false;
    }

    public void SetDefault(Field letrehozasKezd, Field letrehozasVege)
    {
        SetDefault();

        if (letrehozasKezd != null && letrehozasVege != null)
        {
            // érvényességtartomány van megadva

            LetrehozasCalendarControl1.letrehozasKezd = letrehozasVege.Value;
            LetrehozasCalendarControl1.letrehozasVege = letrehozasKezd.Value;

            LetrehozasTartomany_Label.Enabled = true;
        }
        //LetrehozasCalendarControl1.letrehozasVege_ImageButton.Attributes["onload"] = LetrehozasTartomany_CheckBox.Attributes["onclick"];
    }

    //public void LoadFromSearchObject(Field letrehozasKezd, Field letrehozasVege)
    //{
    //    if (letrehozasKezd.Value == Query.SQLFunction.getdate)
    //    {
    //        LetrehozasCalendarControl1.letrehozasKezd = DefaultDates.Today;
    //    }
    //    else
    //    {
    //        LetrehozasCalendarControl1.letrehozasKezd = letrehozasKezd.Value;
    //    }

    //    if (letrehozasVege.Value == Query.SQLFunction.getdate)
    //    {
    //        LetrehozasCalendarControl1.letrehozasVege = DefaultDates.Today;
    //    }
    //    else
    //    {
    //        LetrehozasCalendarControl1.letrehozasVege = letrehozasVege.Value;
    //    }
    //}

    #region public properties

    public Boolean Enabled
    {
        set { Panel1.Enabled = value; }
        get { return Panel1.Enabled; }
    }
    public string LetrehozasKezd
    {
        set { LetrehozasCalendarControl1.letrehozasKezd = value; }
        get { return LetrehozasCalendarControl1.letrehozasKezd; }
    }

    public string LetrehozasVege
    {
        set { LetrehozasCalendarControl1.letrehozasVege = value; }
        get { return LetrehozasCalendarControl1.letrehozasVege; }
    }

    public TextBox LetrehozasKezd_TextBox
    {
        get { return LetrehozasCalendarControl1.LetrehozasKezd_TextBox; }
    }

    public TextBox LetrehozasVege_TextBox
    {
        get { return LetrehozasCalendarControl1.LetrehozasVege_TextBox; }
    }

    //public bool Validate
    //{
    //    set
    //    {
    //        LetrehozasCalendarControl1.Validate = value;
    //    }
    //}

    #endregion

    private void registerJavascripts()
    {
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        string text = String.Empty;

        text = Search.GetQueryText(LetrehozasTartomany_Label.Text, LetrehozasCalendarControl1.GetSearchText()).TrimEnd(Search.whereDelimeter.ToCharArray());

        return text;
    }

    #endregion
}