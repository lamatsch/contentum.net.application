<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListHeader.ascx.cs" Inherits="Component_ListHeader" %>
<%@ Register Src="SearchPopup.ascx" TagName="SearchPopup" TagPrefix="uc1" %>
<%@ Register Src="ListSortPopup.ascx" TagName="ListSortPopup" TagPrefix="uc2" %>
<%@ Register Src="ListColumnsPopup.ascx" TagName="ListColumnsPopup" TagPrefix="uc3" %>
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager" TagPrefix="fm" %>
<%@ Register Src="PrintHtml.ascx" TagName="PrintHtml" TagPrefix="print" %>

<asp:UpdatePanel ID="UpdatePanel2" RenderMode="Inline" runat="server">
    <ContentTemplate>
        <!-- A kiv�lasztand� record Id-j�nak t�rol�sa -->
        <asp:HiddenField ID="selectedRecordId" runat="server" />
        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
            TargetControlID="Panel2" CollapsedSize="12" Collapsed="False" ExpandControlID="ListHeaderCPEButton"
            CollapseControlID="ListHeaderCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
            AutoExpand="false" ScrollContents="false" CollapsedImage="~/images/hu/Grid/plus.gif"
            ExpandedImage="~/images/hu/Grid/minus.gif" ImageControlID="ListHeaderCPEButton">
        </ajaxToolkit:CollapsiblePanelExtender>
        <asp:Panel ID="Panel2" runat="server">
            <table class="mrkFejlec" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="width: 85px; vertical-align: top;">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="vertical-align: top; padding-left: 0px; padding-top: 0px;">
                                    <asp:ImageButton runat="server" ImageUrl="~/images/hu/Grid/minus.gif" ID="ListHeaderCPEButton"
                                        OnClientClick="return false;" />
                                </td>
                                <td>
                                    <img runat="server" id="kepernyo_lista_ikon" src="../images/hu/fejlec/kepernyo_lista_ikon.jpg"
                                        alt=" " />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align: top; text-align: left;">
                        <table cellspacing="0" cellpadding="0" style="width: 100%">
                            <tr>
                                <td colspan="3" style="width: 100%">
                                    <table cellspacing="0" cellpadding="0" style="width: 100%">
                                        <tr>
                                            <td style="height: 46px; text-align: left; vertical-align: middle; white-space: nowrap; width: 100%;"
                                                printable="true">
                                                <asp:Label ID="Header" runat="server" Text="Header" CssClass="ListHeaderText"></asp:Label>&nbsp;
                                                <asp:Label ID="IsFilteredLabel" runat="server" CssClass="ListHeaderText"></asp:Label>
                                                <asp:Label ID="labelFilter" runat="server" CssClass="ListHeaderText"></asp:Label>
                                                <uc1:SearchPopup ID="spSearchPanel" runat="server" />
                                            </td>
                                            <td align="right" style="height: 46px; vertical-align: bottom; text-align: right;">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <table id="tablePager" runat="server" cellspacing="0" cellpadding="0" border="0"
                                                            style="height: 46px;">
                                                            <tr>
                                                                <td valign="middle" style="height: 22px; padding-right: 20px;" class="listHeaderElement">
                                                                    <%--<asp:TextBox runat="server" Width="30" MaxLength="4" ID="TopRowTextBox" Text= ToolTip="Maxim�lis rekordsz�m"></asp:TextBox>&nbsp;&nbsp;--%>
                                                                    <asp:TextBox runat="server" Width="30" MaxLength="4" ID="RowCountTextBox" ToolTip="Oldalank�nti rekordok sz�ma"
                                                                        AutoPostBack="True" OnTextChanged="RowCountTextBox_TextChanged"></asp:TextBox>
                                                                    <ajaxToolkit:FilteredTextBoxExtender ID="filteredExtenderRowCount" runat="server" FilterType="Numbers" TargetControlID="RowCountTextBox">
                                                                    </ajaxToolkit:FilteredTextBoxExtender>
                                                                    <asp:CheckBox runat="server" ID="ScrollableCheckBox" Checked="false" ToolTip="Scrollozhat� lista"
                                                                        OnCheckedChanged="ScrollableCheckBox_CheckedChanged" AutoPostBack="true" />
                                                                    <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server" TargetControlID="ScrollableCheckBox"
                                                                        UncheckedImageUrl="../images/hu/ikon/scrolling_off.jpg" UncheckedImageAlternateText="Scrollozhat�s�g bekapcsol�sa"
                                                                        CheckedImageUrl="../images/hu/ikon/scrolling_on.jpg" CheckedImageAlternateText="Scrollozhat�s�g kikapcsol�sa"
                                                                        ImageHeight="24" ImageWidth="26">
                                                                    </ajaxToolkit:ToggleButtonExtender>
                                                                </td>
                                                                <td valign="middle" style="height: 22px; padding-right: 10px">
                                                                    <asp:ImageButton ID="ImageButton_FirstPage" runat="server" ImageUrl="../images/hu/egyeb/rewall.jpg"
                                                                        onmouseover="swapByName(this.id,'rewall2.jpg');" onmouseout="swapByName(this.id,'rewall.jpg');"
                                                                        OnClick="ImageButton1_Click" CommandName="First" AlternateText="<%$Resources:Buttons,First_page %>"
                                                                        ToolTip="<%$Resources:Buttons,First_page %>" /></td>
                                                                <td style="height: 22px; width: 16px;">&nbsp;</td>
                                                                <td valign="middle" style="height: 22px;">
                                                                    <asp:ImageButton ID="ImageButton_PrevPage" runat="server" ImageUrl="../images/hu/egyeb/rew.jpg"
                                                                        onmouseover="swapByName(this.id,'rew2.jpg')" onmouseout="swapByName(this.id,'rew.jpg')"
                                                                        OnClick="ImageButton1_Click" CommandName="Prev" AlternateText="<%$Resources:Buttons,Prev_page %>"
                                                                        ToolTip="<%$Resources:Buttons,Prev_page %>" /></td>
                                                                <td valign="middle" align="center" style="height: 22px; background-repeat: no-repeat; background-position: center; font-weight: bold; padding-left: 10px; padding-right: 10px;">
                                                                    <span style="white-space: nowrap">
                                                                        <asp:Label ID="Pager" runat="server" Text="Pager"></asp:Label>
                                                                        <asp:HiddenField ID="PageIndex_HiddenField" runat="server" />
                                                                        <asp:HiddenField ID="PageCount_HiddenField" runat="server" />
                                                                        <asp:Label ID="labelRecordNumber" runat="server" Text="(0)"></asp:Label>
                                                                    </span>
                                                                </td>
                                                                <td valign="middle" style="height: 22px; padding-right: 10px">
                                                                    <%--<a href="#" onmouseover="swap('ffw','images/hu/egyeb/ffw2.jpg');" onmouseout="swap('ffw', 'images/hu/egyeb/ffw.jpg');"><img runat="server" src="images/hu/egyeb/ffw.jpg" id="ffw" AlternateText=  /></a>--%>
                                                                    <asp:ImageButton ID="ImageButton_NextPage" runat="server" ImageUrl="../images/hu/egyeb/ffw.jpg"
                                                                        onmouseover="swapByName(this.id,'ffw2.jpg')" onmouseout="swapByName(this.id,'ffw.jpg')"
                                                                        OnClick="ImageButton1_Click" CommandName="Next" AlternateText="<%$Resources:Buttons,Next_page %>"
                                                                        ToolTip="<%$Resources:Buttons,Next_page %>" /></td>
                                                                <td valign="middle" style="height: 22px; padding-right: 10px;">
                                                                    <asp:ImageButton ID="ImageButton_LastPage" runat="server" ImageUrl="../images/hu/egyeb/ffwd.jpg"
                                                                        onmouseover="swapByName(this.id,'ffwd2.jpg')" onmouseout="swapByName(this.id,'ffwd.jpg')"
                                                                        OnClick="ImageButton1_Click" CommandName="Last" AlternateText="<%$Resources:Buttons,Last_page %>"
                                                                        ToolTip="<%$Resources:Buttons,Last_page %>" /></td>

                                                                <td valign="middle" style="height: 22px; padding-right: 10px;">
                                                                    <asp:Image ID="ImageSort" runat="server" ImageUrl="../images/hu/egyeb/sort.gif" onmouseover="swapByName(this.id,'sort2.gif')"
                                                                        onmouseout="swapByName(this.id,'sort.gif')" AlternateText="Rendez�s megad�sa..."
                                                                        ToolTip="Rendez�s megad�sa..." Style="cursor: pointer;" />
                                                                </td>
                                                                <td valign="middle" style="height: 22px; padding-right: 10px;" id="td_ImageListColumns" runat="server">
                                                                    <asp:Image ID="ImageListColumns" runat="server" ImageUrl="../images/hu/egyeb/column_on.gif" onmouseover="swapByName(this.id,'column_on2.gif')"
                                                                        onmouseout="swapByName(this.id,'column_on.gif')" AlternateText="Oszlopok megad�sa..."
                                                                        ToolTip="Oszlopok megad�sa..." Style="cursor: pointer;" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <uc2:ListSortPopup ID="ListSortPopup1" runat="server" TargetControlID="ImageSort" />
                                                        <uc3:ListColumnsPopup ID="ListColumnsPopup1" runat="server" TargetControlID="ImageListColumns" OnGridViewBind="RowCountTextBox_TextChanged"
                                                            OnDefaultColumnsVisibilitySaved="ListColumnsPopup_DefaultColumnsVisibilitySaved" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td runat="server" id="tdLinkDefaultPage" style="vertical-align: top; text-align: right; background-image: url('images/hu/fejlec/kepernyo_lista_fill.jpg'); background-repeat: repeat-x; width: 128px; height: 46px;">
                                                <a href="Default.aspx">
                                                    <img runat="server" src="../images/hu/fejlec/nyitooldal.jpg" onmouseover="swapByName(this.id,'nyitooldal2.jpg')"
                                                        onmouseout="swapByName(this.id,'nyitooldal.jpg')" id="nyitooldal" style="vertical-align: top;" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 3px;" colspan="3">
                                    <%-- Spacer--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; text-align: left; height: 32px;">
                                    <table runat="server" id="LeftFunctionButtonsTable" width="*" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonKereses" style="width: 31px">
                                                <asp:ImageButton ID="ImageSearch" runat="server" ImageUrl="../images/hu/ikon/kereses1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kereses1.jpg')" onmouseout="swapByName(this.id,'kereses1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="kereses" AlternateText="Keres�s"
                                                    ToolTip="Keres�s" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonNumerikusKereses" style="width: 31px">
                                                <asp:ImageButton ID="ImageNumericSearch" runat="server" ImageUrl="../images/hu/ikon/numerikus_kereses_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'numerikus_kereses.jpg')" onmouseout="swapByName(this.id,'numerikus_kereses_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="kereses" AlternateText="Gyors keres�s"
                                                    ToolTip="Gyors keres�s" Visible="false" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonFTKereses" style="width: 31px">
                                                <asp:ImageButton ID="ImageFTSearch" runat="server" ImageUrl="../images/hu/ikon/ftkereses1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ftkereses1.jpg')" onmouseout="swapByName(this.id,'ftkereses1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="kereses" AlternateText="Teljes sz�veges teres�s"
                                                    ToolTip="Teljes sz�veges keres�s" Visible="false" />
                                                <%--									<a href="!bejovo_kuld_keres.html" onmouseover="swap('kereses', 'images/hu/ikon/kereses1.jpg');" onmouseout="swap('kereses', 'images/hu/ikon/kereses1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/ftkereses1_keret_nelkul.jpg" class="ikon" id="kereses" AlternateText="Teljes sz�veges keres�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonMETA_List" style="width: 31px">
                                                <asp:ImageButton ID="ImageButtonMETA_List" runat="server" ImageUrl="../images/hu/ikon/META_list.jpg"
                                                    onmouseover="swapByName(this.id,'META_list.jpg')" onmouseout="swapByName(this.id,'META_list.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="metalist" AlternateText="Irat META adatainak list�ja"
                                                    ToolTip="Irat META adatainak list�ja" Visible="false" />

                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonFuggoseg" style="width: 31px">
                                                <asp:ImageButton ID="ImageButtonFuggoseg" runat="server" ImageUrl="../images/hu/ikon/Fuggoseg.jpg"
                                                    onmouseover="swapByName(this.id,'Fuggoseg.jpg')" onmouseout="swapByName(this.id,'Fuggoseg.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="fuggoseg" AlternateText="Sablonok iratt�pushoz rendel�se"
                                                    ToolTip="Sablonok iratt�pushoz rendel�se" Visible="false" />

                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonUj">
                                                <asp:ImageButton ID="ImageNew" runat="server" ImageUrl="../images/hu/ikon/uj1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'uj1.jpg')" onmouseout="swapByName(this.id,'uj1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="New" AlternateText="<%$Resources:Buttons,New %>"
                                                    ToolTip="<%$Resources:Buttons,New %>" />
                                                <%--									<a href="#" onmouseover="swap('uj', 'images/hu/ikon/uj1.jpg');" onmouseout="swap('uj', 'images/hu/ikon/uj1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/uj1_keret_nelkul.jpg" class="ikon" id="uj" AlternateText="�j"/>
									</a>								
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonHozzaad">
                                                <asp:ImageButton ID="ImageAdd" runat="server" ImageUrl="../images/hu/ikon/hozzaadas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'hozzaadas1.jpg')" onmouseout="swapByName(this.id,'hozzaadas1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Add" AlternateText="Hozz�ad"
                                                    ToolTip="Hozz�ad" Visible="false" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonMegnez">
                                                <asp:ImageButton ID="ImageView" runat="server" ImageUrl="../images/hu/ikon/megtekintes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'megtekintes1.jpg')" onmouseout="swapByName(this.id,'megtekintes1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="View" AlternateText="<%$Resources:Buttons,View %>"
                                                    ToolTip="<%$Resources:Buttons,View %>" />
                                                <%--									<a href="./!bejovo_kuld_megtekint1.html" onmouseover="swap('megnez', 'images/hu/ikon/megtekintes1.jpg');" onmouseout="swap('megnez', 'images/hu/ikon/megtekintes1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/megtekintes1_keret_nelkul.jpg" class="ikon" id="megnez" AlternateText="Megtekint�s"/>
									</a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonModosit">
                                                <asp:ImageButton ID="ImageModify" runat="server" ImageUrl="../images/hu/ikon/modositas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'modositas1.jpg')" onmouseout="swapByName(this.id,'modositas1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Modify" AlternateText="M�dos�t�s"
                                                    ToolTip="M�dos�t�s" />
                                                <%--									<a href="./!bejovo_kuld_modosit1.html" onmouseover="swap('modosit', 'images/hu/ikon/modositas1.jpg');" onmouseout="swap('modosit', 'images/hu/ikon/modositas1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/modositas1_keret_nelkul.jpg" class="ikon" id="modosit" AlternateText="M�dos�t�s"/>
								</a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonUgyiratTerkep">
                                                <asp:ImageButton ID="ImageUgyiratTerkep" runat="server" ImageUrl="../images/hu/ikon/ugyiratterkep_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyiratterkep.jpg')" onmouseout="swapByName(this.id,'ugyiratterkep_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="UgyiratTerkep" AlternateText="�gyiratt�rk�p"
                                                    ToolTip="�gyiratt�rk�p" Visible="false" />
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonExportalas">
                                                <asp:ImageButton ID="ImageExport" runat="server" ImageUrl="../images/hu/ikon/export1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'export1.jpg')" onmouseout="swapByName(this.id,'export1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="ExcelExport" AlternateText="<%$Resources:Buttons,Export %>"
                                                    ToolTip="<%$Resources:Buttons,Export %>" Visible="False" />
                                            </td>
                                            <%-- CR3155 - CSV export --%>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonCSVExportalas">
                                                <asp:ImageButton ID="ImageCSVExport" runat="server" ImageUrl="../images/hu/ikon/csvExport_keret_nelkul.png"
                                                    onmouseover="swapByName(this.id,'csvExport.png')" onmouseout="swapByName(this.id,'csvExport_keret_nelkul.png')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="CSVExport" AlternateText="<%$Resources:Buttons,CSVExport %>"
                                                    ToolTip="<%$Resources:Buttons,CSVExport %>" Visible="False" />
                                            </td>
                                            <%-- CR3155 - CSV export --%>

                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonNyomtatas">
                                                <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="../images/hu/ikon/nyomtatas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'nyomtatas1.jpg')" onmouseout="swapByName(this.id,'nyomtatas1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Print" AlternateText="<%$Resources:Buttons,Print %>"
                                                    ToolTip="<%$Resources:Buttons,Print %>" Visible="False" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonSSRSNyomtatas">
                                                <asp:ImageButton ID="ImageSSRSPrint" runat="server" ImageUrl="../images/hu/ikon/nyomtatas1_keret_nelkul_rs.jpg"
                                                    onmouseover="swapByName(this.id,'nyomtatas1_rs.jpg')" onmouseout="swapByName(this.id,'nyomtatas1_keret_nelkul_rs.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Print" AlternateText="<%$Resources:Buttons,Print %>"
                                                    ToolTip="<%$Resources:Buttons,Print %>" Visible="False" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonSSRSNyomtatasFutar">
                                                <asp:ImageButton ID="ImageSSRSPrintFutar" runat="server" ImageUrl="../images/hu/ikon/nyomtatas1_keret_nelkul_ssrs_futar.jpg"
                                                    onmouseover="swapByName(this.id,'nyomtatas1_ssrs_futar.jpg')" onmouseout="swapByName(this.id,'nyomtatas1_keret_nelkul_ssrs_futar.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="PrintSSRSFutar" AlternateText="<%$Resources:Buttons,Print_futarjegyzek %>"
                                                    ToolTip="<%$Resources:Buttons,Print_futarjegyzek %>" Visible="False" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonErvenytelenites">
                                                <asp:ImageButton ID="ImageInvalidate" runat="server" ImageUrl="../images/hu/ikon/ervenytelenites1_keret_nelk.jpg"
                                                    onmouseover="swapByName(this.id,'ervenytelenites1.jpg')" onmouseout="swapByName(this.id,'ervenytelenites1_keret_nelk.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Invalidate" AlternateText="�rv�nytelen�t�s"
                                                    ToolTip="�rv�nytelen�t�s" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonErvenyesites">
                                                <asp:ImageButton ID="ImageRevalidate" runat="server" ImageUrl="../images/hu/ikon/felhasznalo_revidialasa_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'felhasznalo_revidialasa.jpg')" onmouseout="swapByName(this.id,'felhasznalo_revidialasa_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Revalidate" AlternateText="<%$Resources:Buttons,Print %>"
                                                    ToolTip="<%$Resources:Buttons,Revalidate %>" Visible="false" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonHistory">
                                                <asp:ImageButton ID="ImageHistory" runat="server" ImageUrl="~/images/hu/ikon/history1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'history1.jpg')" onmouseout="swapByName(this.id,'history1_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="ViewHistory" AlternateText="<%$Resources:Buttons,View_history %>"
                                                    ToolTip="<%$Resources:Buttons,View_history %>" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonRefresh">
                                                <asp:ImageButton ID="ImageRefresh" runat="server" ImageUrl="../images/hu/ikon/refresh_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'refresh.jpg')" onmouseout="swapByName(this.id,'refresh_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="Refresh" AlternateText="<%$Resources:Buttons,Refresh %>"
                                                    ToolTip="<%$Resources:Buttons,Refresh %>" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonDefaultFilter">
                                                <asp:ImageButton ID="ImageDefaultFilter" runat="server" ImageUrl="../images/hu/ikon/alapallapot_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'alapallapot.jpg')" onmouseout="swapByName(this.id,'alapallapot_keret_nelkul.jpg')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="DefaultFilter" AlternateText="<%$Resources:Buttons,DefaultFilter %>"
                                                    ToolTip="<%$Resources:Buttons,DefaultFilter %>" />
                                            </td>
                                            <td class="ikonSorElem" id="LeftFunctionButtonsTableButtonSzlaExport">
                                                <asp:ImageButton ID="ImageSzlaExport" runat="server" ImageUrl="../images/hu/ikon/szlaexport.png"
                                                    onmouseover="swapByName(this.id,'szlaexport1.png')" onmouseout="swapByName(this.id,'szlaexport.png')"
                                                    OnClick="Left_FunkcionButtons_OnClick" CommandName="SzlaExport" AlternateText="<%$Resources:Buttons,SzlaExport %>"
                                                    ToolTip="<%$Resources:Buttons,SzlaExport %>" />
                                            </td>
                                            <td style="width: 100%"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td colspan="2" style="height: 32px; vertical-align: top; text-align: right;">
                                    <%--Jobboldali funkcio gombok--%>
                                    <table runat="server" id="RightFunctionButtonsTable" width="*" cellpadding="0" cellspacing="0"
                                        style="height: 32px;">
                                        <tr>
                                            <td style="width: 100%;"></td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonUgyintezesiIdoUjraSzamolas">
                                                <asp:ImageButton ID="ImageUgyintezesiIdoUjraSzamolas" runat="server" ImageUrl="../images/hu/ikon/UgyintIdoUjraSzamolas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'UgyintIdoUjraSzamolas.jpg')" onmouseout="swapByName(this.id,'UgyintIdoUjraSzamolas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="UgyintezesiIdoUjraSzamolas" AlternateText="<%$Resources:Buttons,UgyintezesiIdoUjraSzamolas %>"
                                                    ToolTip="<%$Resources:Buttons,UgyintezesiIdoUjraSzamolas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonBontas">
                                                <asp:ImageButton ID="ImageBontas" runat="server" ImageUrl="../images/hu/ikon/bontas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'bontas1.jpg')" onmouseout="swapByName(this.id,'bontas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Bontas" AlternateText="<%$Resources:Buttons,Bontas %>"
                                                    ToolTip="<%$Resources:Buttons,Bontas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonEloadoiIv">
                                                <asp:ImageButton ID="ImageEloadoiIv" runat="server" ImageUrl="../images/hu/ikon/eloadiiv_tomges_nyomtatas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'eloadiiv_tomges_nyomtatas.jpg')" onmouseout="swapByName(this.id,'eloadiiv_tomges_nyomtatas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="EloadoiIv" AlternateText="<%$Resources:Buttons,EloadoiIv %>"
                                                    ToolTip="<%$Resources:Buttons,EloadoiIv %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonPeresEloadoiIv">
                                                <asp:ImageButton ID="ImagePeresEloadoiIv" runat="server" ImageUrl="../images/hu/ikon/peres_eloadiiv_tomges_nyomtatas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'peres_eloadiiv_tomges_nyomtatas.jpg')" onmouseout="swapByName(this.id,'peres_eloadiiv_tomges_nyomtatas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="PeresEloadoiIv" AlternateText="<%$Resources:Buttons,PeresEloadoiIv %>"
                                                    ToolTip="<%$Resources:Buttons,PeresEloadoiIv %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonErkeztet">
                                                <asp:ImageButton ID="ImageErkeztetes" runat="server" ImageUrl="../images/hu/ikon/erkeztetes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'erkeztetes1.jpg')" onmouseout="swapByName(this.id,'erkeztetes1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Erkeztetes" AlternateText="<%$Resources:Buttons,Erkeztetes %>"
                                                    ToolTip="<%$Resources:Buttons,Erkeztetes %>" />
                                                <%--									<a href="!bejovo_kuld_erk1.html" onmouseover="swap('erkeztet', 'images/hu/ikon/erkeztetes1.jpg');" onmouseout="swap('erkeztet', 'images/hu/ikon/erkeztetes1_keret_nelkul.jpg');">
									<img src="images/hu/ikon/erkeztetes1_keret_nelkul.jpg" class="ikon" id="erkeztet" AlternateText="�rkeztet�s"/></a>
                                                --%>
                                            </td>
                                            <%-- Email iktat�s megtagad�s --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIktatasMegtagadas">
                                                <asp:ImageButton ID="ImageIktatasMegtagadas" runat="server" ImageUrl="../images/hu/ikon/email_megtagadas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'email_megtagadas.jpg')" onmouseout="swapByName(this.id,'email_megtagadas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IktatasMegtagadas" AlternateText="<%$Resources:Buttons,Iktatas_Megtagadas %>"
                                                    ToolTip="<%$Resources:Buttons,Iktatas_Megtagadas %>" />
                                            </td>
                                            <%-- / Email iktat�s megtagad�s --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonBejovoIratIktatas">
                                                <asp:ImageButton ID="ImageBejovoIratIktatas" runat="server" ImageUrl="../images/hu/ikon/bejovo_irat_iktatasa1_keret.jpg"
                                                    onmouseover="swapByName(this.id,'bejovo_irat_iktatasa1.jpg')" onmouseout="swapByName(this.id,'bejovo_irat_iktatasa1_keret.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="BejovoIratIktatas" AlternateText="<%$Resources:Buttons,Bejovo_irat_iktatasa %>"
                                                    ToolTip="<%$Resources:Buttons,Bejovo_irat_iktatasa %>" />
                                                <%--										<a href="./bejovo-irat-ikt-1.html" onmouseover="swap('iktatas1', './images/ikon/bejovo_irat_iktatasa1.jpg');" onmouseout="swap('iktatas1', './images/ikon/bejovo_irat_iktatasa1_keret.jpg');">
										<img src="./images/ikon/bejovo_irat_iktatasa1_keret.jpg" class="ikon" id="iktatas1" alt="Bej�v� irat iktat�sa"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonBelsoIratIktatas">
                                                <asp:ImageButton ID="ImageBelsoIratIktatas" runat="server" ImageUrl="../images/hu/ikon/belso_irat_iktatasa_ker_nel.jpg"
                                                    onmouseover="swapByName(this.id,'belso_irat_iktatasa.jpg')" onmouseout="swapByName(this.id,'belso_irat_iktatasa_ker_nel.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="BelsoIratIktatas" AlternateText="<%$Resources:Buttons,Belso_irat_iktatasa %>"
                                                    ToolTip="<%$Resources:Buttons,Belso_irat_iktatasa %>" />
                                            </td>
                                            <%-- BLG 1131 Iratmozg�s adminisztr�l�sa --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIratmozgasAdminisztralasa">
                                                <asp:ImageButton ID="ImageIratmozgasAdminisztralasa" runat="server" ImageUrl="~/images/hu/ikon/iratmozgas_adminisztralasa_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'iratmozgas_adminisztralasa.jpg')" onmouseout="swapByName(this.id,'iratmozgas_adminisztralasa_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IratmozgasAdminisztralasa"
                                                    AlternateText="<%$Resources:Buttons,Iratmozgas_adminisztralasa %>" ToolTip="<%$Resources:Buttons,Iratmozgas_adminisztralasa %>" Visible="false" />
                                            </td>
                                            <%-- BEJ�V� IRAT IKTAT�SA ALSZ�MRA --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonBejovoIratIktatasAlszamra">
                                                <asp:ImageButton ID="ImageBejovoIratIktatasAlszamra" runat="server" ImageUrl="~/images/hu/ikon/bejovo_irat_iktatasa_alszamra_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'bejovo_irat_iktatasa_alszamra.jpg')" onmouseout="swapByName(this.id,'bejovo_irat_iktatasa_alszamra_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="BejovoIratIktatasAlszamra"
                                                    AlternateText="<%$Resources:Buttons,Bejovo_irat_iktatasa_alszamra %>" ToolTip="<%$Resources:Buttons,Bejovo_irat_iktatasa_alszamra %>" />
                                            </td>
                                            <%-- bELS� IRAT IKTAT�SA ALSZ�MRA --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonBelsoIratIktatasAlszamra">
                                                <asp:ImageButton ID="ImageBelsoIratIktatasAlszamra" runat="server" ImageUrl="~/images/hu/ikon/belso_irat_iktatasa_alszamra_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'belso_irat_iktatasa_alszamra.jpg')" onmouseout="swapByName(this.id,'belso_irat_iktatasa_alszamra_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="BelsoIratIktatasAlszamra"
                                                    AlternateText="<%$Resources:Buttons,Belso_irat_iktatasa_alszamra %>" ToolTip="<%$Resources:Buttons,Belso_irat_iktatasa_alszamra %>" />
                                            </td>
                                            <%-- Iktat�s el�k�sz�tett dokumentumb�l	--%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIktatas">
                                                <asp:ImageButton ID="ImageIktatas" runat="server" ImageUrl="../images/hu/ikon/iktatas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'iktatas1.jpg')" onmouseout="swapByName(this.id,'iktatas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Iktatas" AlternateText="<%$Resources:Buttons,Iktatas %>"
                                                    ToolTip="Iktat�s el�k�sz�tett dokumentumb�l" />
                                            </td>
                                            <td class="ikonSorElem" id="Td1">
                                                <asp:ImageButton ID="ImageMunkapeldanyIktatasa" runat="server" ImageUrl="~/images/hu/ikon/munkapeldany_iktatasa_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'munkapeldany_iktatasa.jpg')" onmouseout="swapByName(this.id,'munkapeldany_iktatasa_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IktatasElokeszites" AlternateText="<%$Resources:Buttons,Munkapeldany_Iktatas %>"
                                                    ToolTip="<%$Resources:Buttons,Munkapeldany_Iktatas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtIktatas">
                                                <asp:ImageButton ID="ImageAtIktatas" runat="server" ImageUrl="../images/hu/ikon/atiktatas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atiktatas1.jpg')" onmouseout="swapByName(this.id,'atiktatas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Atiktatas" AlternateText="<%$Resources:Buttons,Atiktatas %>"
                                                    ToolTip="<%$Resources:Buttons,Atiktatas %>" />
                                                <%--										<a href="#" onmouseover="swap('iktatas3', './images/ikon/atiktatas1.jpg');" onmouseout="swap('iktatas3', './images/ikon/atiktatas1_keret_nelkul.jpg');">
										            <img src="./images/ikon/atiktatas1_keret_nelkul.jpg" class="ikon" id="iktatas3" alt="�tiktat�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonValaszIrat">
                                                <asp:ImageButton ID="ImageValaszIrat" runat="server" ImageUrl="~/images/hu/ikon/valasz_irat_iktatasa_alszamra_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'valasz_irat_iktatasa_alszamra.jpg')" onmouseout="swapByName(this.id,'valasz_irat_iktatasa_alszamra_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="ValaszIrat" AlternateText="<%$Resources:Buttons,ValaszIrat %>"
                                                    ToolTip="<%$Resources:Buttons,ValaszIrat %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadasra">
                                                <asp:ImageButton ID="ImageAtadasra" runat="server" ImageUrl="../images/hu/ikon/atadaskijelol_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atadaskijelol.jpg')" onmouseout="swapByName(this.id,'atadaskijelol_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtadasraKijeloles" AlternateText="<%$Resources:Buttons,Atadasra_kijelol %>"
                                                    ToolTip="<%$Resources:Buttons,Atadasra_kijelol %>" />
                                                <%--										<a href="#" onmouseover="swap('atadasra', './images/ikon/atadaskijelol.jpg');" onmouseout="swap('atadasra', './images/ikon/atadaskijelol_keret_nelkul.jpg');">
										            <img src="./images/ikon/atadaskijelol_keret_nelkul.jpg" class="ikon" id="atadasra" alt="�tad�sra kijel�l"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadas">
                                                <asp:ImageButton ID="ImageAtadas" runat="server" ImageUrl="../images/hu/ikon/atadas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atadas1.jpg')" onmouseout="swapByName(this.id,'atadas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Atadas" AlternateText="<%$Resources:Buttons,Atadas %>"
                                                    ToolTip="<%$Resources:Buttons,Atadas %>" />
                                                <%--										<a href="#" onmouseover="swap('atadas', './images/ikon/atadas1.jpg');" onmouseout="swap('atadas', './images/ikon/atadas1_keret_nelkul.jpg');">
										            <img src="./images/ikon/atadas1_keret_nelkul.jpg" class="ikon" id="atadas" alt="�tad�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadasListaNyomtatassl">
                                                <asp:ImageButton ID="ImageAtadasListaval" runat="server" ImageUrl="../images/hu/ikon/atadasi_lista_nyomtatasa1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atadasi_lista_nyomtatasa1.jpg')" onmouseout="swapByName(this.id,'atadasi_lista_nyomtatasa1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtadasListaval" AlternateText="<%$Resources:Buttons,AtadasListaval %>"
                                                    ToolTip="<%$Resources:Buttons,AtadasListaval %>" />
                                                <%--										<a href="#" onmouseover="swap('atadaslistaval', './images/ikon/atadasi_lista_nyomtatasa1.jpg');" onmouseout="swap('atadaslistaval', './images/ikon/atadasi_lista_nyomtatasa1_keret_nelkul.jpg');">
										            <img src="./images/ikon/atadasi_lista_nyomtatasa1_keret_nelkul.jpg.jpg" class="ikon" id="atadaslistaval" alt="�tad�s + �tad�si lista nyomtat�sa"/></a>
                                                --%>
                                            </td>
                                            <%--BLG 1130 --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtvevoJegyzekNyomtatasa">
                                                <asp:ImageButton ID="ImageAtvevoJegyzekNyomtatasa" runat="server" ImageUrl="../images/hu/ikon/atvevo_jegyzek_nyomtatasa1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atvevo_jegyzek_nyomtatasa1.jpg')" onmouseout="swapByName(this.id,'atvevo_jegyzek_nyomtatasa1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtvevoJegyzekNyomtatasa" AlternateText="<%$Resources:Buttons,AtvevoJegyzekNyomtatasa %>"
                                                    ToolTip="<%$Resources:Buttons,AtvevoJegyzekNyomtatasa %>" />
                                                <%--										<a href="#" onmouseover="swap('atvevojegyzek', './images/ikon/atvevo_jegyzek_nyomtatasa1.jpg');" onmouseout="swap('atadaslistaval', './images/ikon/atvevo_jegyzek_nyomtatasa1_keret_nelkul.jpg');">
										            <img src="./images/ikon/atvevo_jegyzek_nyomtatasa1_keret_nelkul.jpg.jpg" class="ikon" id="atvevojegyzek" alt="�tvev� jegyz�k nyomtat�sa"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtvevoJegyzekNyomtatasaTomeges">
                                                <asp:ImageButton ID="ImageAtvevoJegyzekNyomtatasaTomeges" runat="server" ImageUrl="../images/hu/ikon/atvevo_jegyzek_nyomtatasa1_tomeges_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atvevo_jegyzek_nyomtatasa1_tomeges.jpg')" onmouseout="swapByName(this.id,'atvevo_jegyzek_nyomtatasa1_tomeges_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtvevoJegyzekNyomtatasaTomeges" AlternateText="<%$Resources:Buttons,AtvevoJegyzekNyomtatasaTomeges %>"
                                                    ToolTip="<%$Resources:Buttons,AtvevoJegyzekNyomtatasaTomeges %>" />
                                                <%--										<a href="#" onmouseover="swap('atvevojegyzek', './images/ikon/atvevo_jegyzek_nyomtatasa1_tomeges.jpg');" onmouseout="swap('atvevojegyzek', './images/ikon/atvevo_jegyzek_nyomtatasa1_tomeges_keret_nelkul.jpg');">
										            <img src="./images/ikon/atvevo_jegyzek_nyomtatasa1_tomeges_keret_nelkul.jpg.jpg" class="ikon" id="atvevojegyzek" alt="�tvev� jegyz�k nyomtat�sa t�meges"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtadasVissza">
                                                <asp:ImageButton ID="ImageAtadasVissza" runat="server" ImageUrl="../images/hu/ikon/atadas_visszavonasa1_keret_.jpg"
                                                    onmouseover="swapByName(this.id,'atadas_visszavonasa1.jpg')" onmouseout="swapByName(this.id,'atadas_visszavonasa1_keret_.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtadasVissza" AlternateText="<%$Resources:Buttons,Atadas_visszavonasa %>"
                                                    ToolTip="<%$Resources:Buttons,Atadas_visszavonasa %>" />
                                                <%--										<a href="#" onmouseover="swap('atadas', './images/ikon/atadas1.jpg');" onmouseout="swap('atadas', './images/ikon/atadas1_keret_nelkul.jpg');">
										            <img src="./images/ikon/atadas1_keret_nelkul.jpg" class="ikon" id="atadas" alt="�tad�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTovabbitasra">
                                                <asp:ImageButton ID="ImageTovabbitasra" runat="server" ImageUrl="../images/hu/ikon/tovabbitasra_kijelol1_keret.jpg"
                                                    onmouseover="swapByName(this.id,'tovabbitasra_kijelol1.jpg')" onmouseout="swapByName(this.id,'tovabbitasra_kijelol1_keret.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Atadas" AlternateText="<%$Resources:Buttons,Tovabbitasra_kijelol %>"
                                                    ToolTip="<%$Resources:Buttons,Tovabbitasra_kijelol %>" />
                                                <%--									<a href="#" onmouseover="swap('tovabbitasra', 'images/hu/ikon/tovabbitasra_kijelol1.jpg');" onmouseout="swap('tovabbitasra', 'images/hu/ikon/tovabbitasra_kijelol1_keret.jpg');">
									            <img src="images/hu/ikon/tovabbitasra_kijelol1_keret.jpg" class="ikon" id="tovabbitasra" AlternateText="Tov�bb�t�sra kijel�l"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTovabbitas">
                                                <asp:ImageButton ID="ImageSzignalas" runat="server" ImageUrl="../images/hu/ikon/tovabbitas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'tovabbitas1.jpg')" onmouseout="swapByName(this.id,'tovabbitas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Szignalas" AlternateText="Szign�l�s"
                                                    ToolTip="Szign�l�s" />
                                                <%--									<a href="#" onmouseover="swap('tovabbitas', 'images/hu/ikon/tovabbitas1.jpg');" onmouseout="swap('tovabbitas', 'images/hu/ikon/tovabbitas1_keret_nelkul.jpg');">
									            <img src="images/hu/ikon/tovabbitas1_keret_nelkul.jpg" class="ikon" id="tovabbitas" AlternateText="Tov�bb�t�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTomegesSzignalas">
                                                <asp:ImageButton ID="ImageTomegesSzignalas" runat="server" ImageUrl="../images/hu/ikon/ugyirat_szignalas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_szignalas.jpg')" onmouseout="swapByName(this.id,'ugyirat_szignalas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="TomegesSzignalas" AlternateText="T�meges szign�l�s"
                                                    ToolTip="T�meges szign�l�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTovabbitasVissza">
                                                <asp:ImageButton ID="ImageTovabbitasVissza" runat="server" ImageUrl="../images/hu/ikon/tovabbitas_visszavonasa1_ke.jpg"
                                                    onmouseover="swapByName(this.id,'tovabbitas_visszavonasa1.jpg')" onmouseout="swapByName(this.id,'tovabbitas_visszavonasa1_ke.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="TovabbitasVissza" AlternateText="<%$Resources:Buttons,Tovabbitas_visszavonasa %>"
                                                    ToolTip="<%$Resources:Buttons,Tovabbitas_visszavonasa %>" />
                                                <%--									<a href="#" onmouseover="swap('tovabbitas', 'images/hu/ikon/tovabbitas1.jpg');" onmouseout="swap('tovabbitas', 'images/hu/ikon/tovabbitas1_keret_nelkul.jpg');">
									            <img src="images/hu/ikon/tovabbitas1_keret_nelkul.jpg" class="ikon" id="tovabbitas" AlternateText="Tov�bb�t�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonDossziebaHelyez">
                                                <asp:ImageButton ID="ImageDossziebaHelyez" runat="server" ImageUrl="../images/hu/ikon/dosszieba_helyez_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'dosszieba_helyez.jpg')" onmouseout="swapByName(this.id,'dosszieba_helyez_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="DossziebaHelyez" AlternateText="<%$Resources:Buttons,DossziebaHelyez %>"
                                                    ToolTip="<%$Resources:Buttons,DossziebaHelyez %>" />
                                                <%--									<a href="#" onmouseover="swap('tovabbitas', 'images/hu/ikon/tovabbitas1.jpg');" onmouseout="swap('tovabbitas', 'images/hu/ikon/tovabbitas1_keret_nelkul.jpg');">
									            <img src="images/hu/ikon/tovabbitas1_keret_nelkul.jpg" class="ikon" id="tovabbitas" AlternateText="Tov�bb�t�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonDossziebolKivesz">
                                                <asp:ImageButton ID="ImageDossziebolKivesz" runat="server" ImageUrl="../images/hu/ikon/dosszieba_helyez_vissza_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'dosszieba_helyez_vissza.jpg')" onmouseout="swapByName(this.id,'dosszieba_helyez_vissza_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="DossziebolKivesz" AlternateText="<%$Resources:Buttons,DossziebolKivesz %>"
                                                    ToolTip="<%$Resources:Buttons,DossziebolKivesz %>" />
                                                <%--									<a href="#" onmouseover="swap('tovabbitas', 'images/hu/ikon/tovabbitas1.jpg');" onmouseout="swap('tovabbitas', 'images/hu/ikon/tovabbitas1_keret_nelkul.jpg');">
									            <img src="images/hu/ikon/tovabbitas1_keret_nelkul.jpg" class="ikon" id="tovabbitas" AlternateText="Tov�bb�t�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSkontrobaHelyezes">
                                                <asp:ImageButton ID="ImageSkontrobaHelyez" runat="server" ImageUrl="../images/hu/ikon/skontro.jpg"
                                                    onmouseover="swapByName(this.id,'skontro_keret.jpg')" onmouseout="swapByName(this.id,'skontro.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="SkontrobaHelyez" AlternateText="Skontr�ba helyez�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSkontrobolKivetel">
                                                <asp:ImageButton ID="ImageSkontrobolKivetel" runat="server" ImageUrl="../images/hu/ikon/skontrobol_vissza.jpg"
                                                    onmouseover="swapByName(this.id,'skontrobol_vissza_keret.jpg')" onmouseout="swapByName(this.id,'skontrobol_vissza.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="SkontrobolKivesz" AlternateText="Skontr�b�l kiv�tel"
                                                    ToolTip="Skontr�b�l kiv�tel" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtvetel">
                                                <asp:ImageButton ID="ImageAtvetel" runat="server" ImageUrl="../images/hu/ikon/atvetel2_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atvetel2.jpg')" onmouseout="swapByName(this.id,'atvetel2_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Atvetel" AlternateText="<%$Resources:Buttons,Atvetel %>"
                                                    ToolTip="<%$Resources:Buttons,Atvetel %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtvetelListaNyomtatassal">
                                                <asp:ImageButton ID="ImageAtvetelListaval" runat="server" ImageUrl="../images/hu/ikon/atveteli_lista_nyomtatasa1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atveteli_lista_nyomtatasa1.jpg')" onmouseout="swapByName(this.id,'atveteli_lista_nyomtatasa1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtvetelListaval" AlternateText="<%$Resources:Buttons,AtvetelListaval %>"
                                                    ToolTip="<%$Resources:Buttons,AtvetelListaval %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAtvetelUgyintezesre">
                                                <asp:ImageButton ID="ImageAtvetelUgyintezesre" runat="server" ImageUrl="../images/hu/ikon/atvetel1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atvetel1.jpg')" onmouseout="swapByName(this.id,'atvetel1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="AtvetelUgyintezesre" AlternateText="<%$Resources:Buttons,AtvetelUgyintezesre %>"
                                                    ToolTip="<%$Resources:Buttons,AtvetelUgyintezesre %>" />
                                            </td>
                                            <%-- BLG 1131 Visszav�tel --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonVisszavetel">
                                                <asp:ImageButton ID="ImageVisszavetel" runat="server" ImageUrl="~/images/hu/ikon/visszavetel_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'visszavetel.jpg')" onmouseout="swapByName(this.id,'visszavetel_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Visszavetel"
                                                    AlternateText="<%$Resources:Buttons,Visszavetel %>" ToolTip="<%$Resources:Buttons,Visszavetel %>" Visible="false" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonVisszakuldes">
                                                <asp:ImageButton ID="ImageVisszakuldes" runat="server" ImageUrl="../images/hu/ikon/visszakuldes2_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'visszakuldes2.jpg')" onmouseout="swapByName(this.id,'visszakuldes2_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Visszakuldes" AlternateText="<%$Resources:Buttons,Visszakuldes %>"
                                                    ToolTip="<%$Resources:Buttons,Visszakuldes %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonFelfuggeszt">
                                                <asp:ImageButton ID="ImageButtonFelfuggeszt" runat="server" ImageUrl="../images/hu/ikon/felfuggeszt_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'felfuggeszt_kerettel.jpg')" onmouseout="swapByName(this.id,'felfuggeszt_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Felfuggesztes" AlternateText="<%$Resources:Buttons,Felfuggesztes %>"
                                                    ToolTip="<%$Resources:Buttons,Felfuggesztes %>" />
                                            </td>
                                            <%-- CR3128 Iratp�ld�ny l�trehoz�s nyom�gomb az iratok list�j�ra --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIratpeldanyLetrehozas">
                                                <asp:ImageButton ID="ImageIratpeldanyLetrehozas" runat="server" ImageUrl="../images/hu/ikon/iratpeldanyletrehozas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'iratpeldanyletrehozas.jpg')" onmouseout="swapByName(this.id,'iratpeldanyletrehozas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IratpeldanyLetrehozas" AlternateText="<%$Resources:Buttons,IratPeldanyLetrehozas %>"
                                                    ToolTip="<%$Resources:Buttons,IratPeldanyLetrehozas %>" />
                                            </td>
                                            <%-- CR3128 Iratp�ld�ny l�trehoz�s nyom�gomb az iratok list�j�ra --%>
                                            <%-- CR3129 CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIratCsatolmanyAlairas">
                                                <asp:ImageButton ID="ImageIratCsatolmanyAlairas" runat="server" ImageUrl="../images/hu/ikon/alairasSzimpla1_keret_nelkul.png"
                                                    onmouseover="swapByName(this.id,'alairasSzimpla1.png')" onmouseout="swapByName(this.id,'alairasSzimpla1_keret_nelkul.png')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Alairas" AlternateText="<%$Resources:Buttons,IratCsatolmanyAlairas %>"
                                                    ToolTip="<%$Resources:Buttons,IratCsatolmanyAlairas %>" />
                                            </td>
                                            <%-- CR3129 Iratp�ld�ny l�trehoz�s nyom�gomb az iratok list�j�ra --%>
                                            <%-- BLItem 63.Task:299 - 20. t�meges elektronikus al��r�s --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTomegesIratCsatolmanyAlairas">
                                                <asp:ImageButton ID="ImageTomegesIratCsatolmanyAlairas" runat="server" ImageUrl="../images/hu/ikon/alairasTomeges1_keret_nelkul.png"
                                                    onmouseover="swapByName(this.id,'alairasTomeges1.png')" onmouseout="swapByName(this.id,'alairasTomeges1_keret_nelkul.png')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="TomegesAlairas" AlternateText="<%$Resources:Buttons,TomegesIratCsatolmanyAlairas %>"
                                                    ToolTip="<%$Resources:Buttons,TomegesIratCsatolmanyAlairas %>" />
                                            </td>
                                            <%-- BLItem 1880. t�meges ragsz�m gener�l�s al��r�s --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTomegesRagszamGeneralas">
                                                <asp:ImageButton ID="ImageTomegesRagszamGeneralas" runat="server" ImageUrl="../images/hu/ikon/tomeges_ragszam_gen_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'tomeges_ragszam_gen.jpg')" onmouseout="swapByName(this.id,'tomeges_ragszam_gen_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="TomegesRagszamGeneralas" AlternateText="<%$Resources:Buttons,TomegesRagszamOsztas %>"
                                                    ToolTip="<%$Resources:Buttons,TomegesRagszamOsztas %>" Visible="false" />
                                            </td>
                                            <%-- BLItem 63.Task:299 - 20. t�meges elektronikus al��r�s --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonAthelyezes">
                                                <asp:ImageButton ID="ImageAthelyezes" runat="server" ImageUrl="../images/hu/ikon/athelyezes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'athelyezes1.jpg')" onmouseout="swapByName(this.id,'athelyezes1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Athelyezes" AlternateText="<%$Resources:Buttons,IratpeldanyUgyiratbaHelyezese %>"
                                                    ToolTip="<%$Resources:Buttons,IratpeldanyUgyiratbaHelyezese %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonRendezes">
                                                <asp:ImageButton ID="ImageRendezes" runat="server" ImageUrl="../images/hu/ikon/rendezes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'rendezes1.jpg')" onmouseout="swapByName(this.id,'rendezes1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Rendezes" AlternateText="<%$Resources:Buttons,Rendezes %>"
                                                    ToolTip="<%$Resources:Buttons,Rendezes %>" />
                                                <%--									<a href="#" onmouseover="swap('rendezes', 'images/hu/ikon/rendezes1.jpg');" onmouseout="swap('rendezes', 'images/hu/ikon/rendezes1_keret_nelkul.jpg');">
									            <img src="images/hu/ikon/rendezes1_keret_nelkul.jpg" class="ikon" id="rendezes" AlternateText="Rendez�s"/></a>
                                                --%>
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIrattarozasrakijeloles">
                                                <asp:ImageButton ID="ImageIrattarozasra" runat="server" ImageUrl="../images/hu/ikon/irattarozasra_kijeloles1_ke.jpg"
                                                    onmouseover="swapByName(this.id,'irattarozasra_kijeloles1.jpg')" onmouseout="swapByName(this.id,'irattarozasra_kijeloles1_ke.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Irattarozasra" AlternateText="Iratt�rba k�ld�s"
                                                    ToolTip="Iratt�rba k�ld�s" />
                                            </td>
                                            <%-- CR3206 - Iratt�ri strukt�ra --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIrattarbarendezes">
                                                <asp:ImageButton ID="ImageIrattarbarendezes" runat="server" ImageUrl="../images/hu/ikon/Irattarba_rendezes_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Irattarba_rendezes.jpg')" onmouseout="swapByName(this.id,'Irattarba_rendezes_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Irattarbarendezes" AlternateText="<%$Resources:Buttons,Irattarbarendezes %>"
                                                    ToolTip="<%$Resources:Buttons,Irattarbarendezes %>" />
                                            </td>
                                            <%-- CR3206 - Iratt�ri strukt�ra --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKiadasOsztalyra">
                                                <asp:ImageButton ID="ImageKiadasOsztalyra" runat="server" ImageUrl="../images/hu/ikon/kiadasosztalyra1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kiadasosztalyra1.jpg')" onmouseout="swapByName(this.id,'kiadasosztalyra1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="KiadasOsztalyra" AlternateText="Kiad�s oszt�lyra"
                                                    ToolTip="Kiad�s oszt�lyra" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonUgyiratpotlonyomtatas">
                                                <asp:ImageButton ID="ImageUgyiratpotlonyomtatas" runat="server" ImageUrl="../images/hu/ikon/ugyiratpotlo_nyomtatasa_keret_ne.jpg"
                                                    onmouseover="swapByName(this.id,'ugyiratpotlo_nyomtatasa.jpg')" onmouseout="swapByName(this.id,'ugyiratpotlo_nyomtatasa_keret_ne.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="UgyiratpotloNyomtatasa" AlternateText="<%$Resources:Buttons,Ugyiratpotlo_nyomtatasa %>"
                                                    ToolTip="<%$Resources:Buttons,Ugyiratpotlo_nyomtatasa %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKolcsonzes">
                                                <asp:ImageButton ID="ImageKolcsonzes" runat="server" ImageUrl="../images/hu/ikon/kolcsonzes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kolcsonzes1.jpg')" onmouseout="swapByName(this.id,'kolcsonzes1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Kolcsonzes" AlternateText="<%$Resources:Buttons,Kolcsonzes %>"
                                                    ToolTip="<%$Resources:Buttons,Kolcsonzes %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKolcsonzesJovahagyas">
                                                <asp:ImageButton ID="ImageKolcsonzesJovahagyas" runat="server" ImageUrl="../images/hu/ikon/kolcsonzesjovahagyas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kolcsonzesjovahagyas1.jpg')" onmouseout="swapByName(this.id,'kolcsonzesjovahagyas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="KolcsonzesJovahagyas" AlternateText="<%$Resources:Buttons,KolcsonzesJovahagyas %>"
                                                    ToolTip="<%$Resources:Buttons,KolcsonzesJovahagyas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKolcsonzesVisszautasitas">
                                                <asp:ImageButton ID="ImageKolcsonzesVisszautasitas" runat="server" ImageUrl="../images/hu/ikon/kolcsonzesvisszautasitas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kolcsonzesvisszautasitas1.jpg')" onmouseout="swapByName(this.id,'kolcsonzesvisszautasitas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="KolcsonzesVisszautasitas"
                                                    AlternateText="<%$Resources:Buttons,KolcsonzesVisszautasitas %>" ToolTip="<%$Resources:Buttons,KolcsonzesVisszautasitas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKolcsonzesSztorno">
                                                <asp:ImageButton ID="ImageKolcsonzesSztorno" runat="server" ImageUrl="../images/hu/ikon/kolcsonzessztorno1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kolcsonzessztorno1.jpg')" onmouseout="swapByName(this.id,'kolcsonzessztorno1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="KolcsonzesSztorno" AlternateText="<%$Resources:Buttons,KolcsonzesSztorno %>"
                                                    ToolTip="<%$Resources:Buttons,KolcsonzesSztorno %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKolcsonzesKiadas">
                                                <asp:ImageButton ID="ImageKolcsonzesKiadas" runat="server" ImageUrl="~/images/hu/ikon/kolcsonzeskiadas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kolcsonzeskiadas1.jpg')" onmouseout="swapByName(this.id,'kolcsonzeskiadas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="KolcsonzesKiadas" AlternateText="<%$Resources:Buttons,KolcsonzesKiadas %>"
                                                    ToolTip="<%$Resources:Buttons,KolcsonzesKiadas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonKolcsonzesVissza">
                                                <asp:ImageButton ID="ImageKolcsonzesVissza" runat="server" ImageUrl="../images/hu/ikon/kolcsonzesvissza1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'kolcsonzesvissza1.jpg')" onmouseout="swapByName(this.id,'kolcsonzesvissza1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="KolcsonzesVissza" AlternateText="<%$Resources:Buttons,KolcsonzesVissza %>"
                                                    ToolTip="<%$Resources:Buttons,KolcsonzesVissza %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonJegyzekrehelyezes">
                                                <asp:ImageButton ID="ImageJegyzekrehelyezes" runat="server" ImageUrl="../images/hu/ikon/Jegyzekrehelyezes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Jegyzekrehelyezes1.jpg')" onmouseout="swapByName(this.id,'Jegyzekrehelyezes1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Jegyzekrehelyezes" AlternateText="<%$Resources:Buttons,Jegyzekrehelyezes %>"
                                                    ToolTip="<%$Resources:Buttons,Jegyzekrehelyezes %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSzereles">
                                                <asp:ImageButton ID="ImageSzereles" runat="server" ImageUrl="../images/hu/ikon/ugyirat_szereles_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_szereles.jpg')" onmouseout="swapByName(this.id,'ugyirat_szereles_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Szereles" AlternateText="Szerel�s"
                                                    ToolTip="Szerel�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSzerelesVisszavon">
                                                <asp:ImageButton ID="ImageSzerelesVisszavon" runat="server" ImageUrl="../images/hu/ikon/ugyirat_szerelesvisszavon_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_szerelesvisszavon.jpg')" onmouseout="swapByName(this.id,'ugyirat_szerelesvisszavon_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="SzerelesVisszavonas" AlternateText="Szerel�s visszavon�sa"
                                                    ToolTip="Szerel�s visszavon�sa" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonCsatolas">
                                                <asp:ImageButton ID="ImageCsatolas" runat="server" ImageUrl="../images/hu/ikon/ugyirat_csatolas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_csatolas.jpg')" onmouseout="swapByName(this.id,'ugyirat_csatolas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Csatolas" AlternateText="Csatol�s"
                                                    ToolTip="Csatol�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonElintezetteNyilvanitas">
                                                <asp:ImageButton ID="ImageElintezetteNyilvanitas" runat="server" ImageUrl="../images/hu/ikon/ugyirat_elintezett_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_elintezett.jpg')" onmouseout="swapByName(this.id,'ugyirat_elintezett_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="ElintezetteNyilvanitas"
                                                    AlternateText="Elint�zett� nyilv�n�t�s" ToolTip="Elint�zett� nyilv�n�t�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonElintezettVisszavon">
                                                <asp:ImageButton ID="ImageElintezetteNyilvanitasVisszavon" runat="server" ImageUrl="../images/hu/ikon/ugyirat_elintezettvissza_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_elintezettvissza.jpg')" onmouseout="swapByName(this.id,'ugyirat_elintezettvissza_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="ElintezetteNyilvanitasVisszavon"
                                                    AlternateText="Elint�zett� nyilv�n�t�s visszavon�sa" ToolTip="Elint�zett� nyilv�n�t�s visszavon�sa" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonElintezetteNyilvanitasTomeges">
                                                <asp:ImageButton ID="ImageElintezetteNyilvanitasTomeges" runat="server" ImageUrl="../images/hu/ikon/ugyirat_elintezett_tomeges_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_elintezett_tomeges.jpg')" onmouseout="swapByName(this.id,'ugyirat_elintezett_tomeges_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="ElintezetteNyilvanitasTomeges"
                                                    AlternateText="T�meges elint�zett� nyilv�n�t�s" ToolTip="T�meges elint�zett� nyilv�n�t�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonLezaras">
                                                <asp:ImageButton ID="ImageLezaras" runat="server" ImageUrl="../images/hu/ikon/zarolas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'zarolas1.jpg')" onmouseout="swapByName(this.id,'zarolas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Lezaras" AlternateText="Lez�r�s"
                                                    ToolTip="Lez�r�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonLezarasVisszavonas">
                                                <asp:ImageButton ID="ImageLezarasVisszavonas" runat="server" ImageUrl="../images/hu/ikon/zarolas_visszavonasa1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'zarolas_visszavonasa1.jpg')" onmouseout="swapByName(this.id,'zarolas_visszavonasa1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="LezarasVisszavonas" AlternateText="Lez�r�s visszavon�sa"
                                                    ToolTip="Lez�r�s visszavon�sa" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonUgyiratMegtekintese">
                                                <asp:ImageButton ID="ImageUgyiratIrattarAtvetel" runat="server" ImageUrl="../images/hu/ikon/ugyirat_megtekintese1_keret.jpg"
                                                    onmouseover="swapByName(this.id,'ugyirat_megtekintese1.jpg')" onmouseout="swapByName(this.id,'ugyirat_megtekintese1_keret.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="UgyiratIrattarAtvetel" AlternateText="Iratt�rba �tv�tel"
                                                    ToolTip="Iratt�rba �tv�tel" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonExpedialas">
                                                <asp:ImageButton ID="ImageExpedialas" runat="server" ImageUrl="../images/hu/ikon/feltoltes1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'feltoltes1.jpg')" onmouseout="swapByName(this.id,'feltoltes1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Expedialas" AlternateText="<%$Resources:Buttons,Expedialas %>"
                                                    ToolTip="<%$Resources:Buttons,Expedialas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonPostazas">
                                                <asp:ImageButton ID="ImagePostazas" runat="server" ImageUrl="../images/hu/ikon/email_send_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'email_send.jpg')" onmouseout="swapByName(this.id,'email_send_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Postazas" AlternateText="<%$Resources:Buttons,Postazas %>"
                                                    ToolTip="<%$Resources:Buttons,Postazas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonPostazasTomeges">
                                                <asp:ImageButton ID="ImagePostazasTomeges" runat="server" ImageUrl="../images/hu/ikon/email_send_t_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'email_send_t.jpg')" onmouseout="swapByName(this.id,'email_send_t_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Postazas" AlternateText="<%$Resources:Buttons,PostazasTomeges %>"
                                                    ToolTip="<%$Resources:Buttons,PostazasTomeges %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTeljessegEllenorzes">
                                                <asp:ImageButton ID="ImageTeljessegEllenorzes" runat="server" ImageUrl="../images/hu/ikon/teljessegellenorzes_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'teljessegellenorzes.jpg')" onmouseout="swapByName(this.id,'teljessegellenorzes_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="TeljessegEllenorzes" AlternateText="<%$Resources:Buttons,TeljessegEllenorzes %>"
                                                    ToolTip="<%$Resources:Buttons,TeljessegEllenorzes %>" />
                                            </td>

                                            <td class="ikonSorElem">
                                                <asp:ImageButton ID="ImageFelszabaditas" runat="server" ImageUrl="../images/hu/ikon/felszabaditas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'felszabaditas.jpg')" onmouseout="swapByName(this.id,'felszabaditas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Felszabaditas" AlternateText="<%$Resources:Buttons,Felszabaditas %>"
                                                    ToolTip="<%$Resources:Buttons,Felszabaditas %>" />
                                            </td>

                                            <%-- BLG2102 - megsemmis�t�si jegyz�k nyomtat�sa --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonMegsJegyzNyomtatas">
                                                <asp:ImageButton Visible="false" ID="ImageMegsJegyzNyomtatas" runat="server" ImageUrl="../images/hu/ikon/megs_jegyz_nyomtatasa1_keret_nelkul.png"
                                                    onmouseover="swapByName(this.id,'megs_jegyz_nyomtatasa1.png')" onmouseout="swapByName(this.id,'megs_jegyz_nyomtatasa1_keret_nelkul.png')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="MegsJegyzNyomtatas" AlternateText="<%$Resources:Buttons,MegsJegyzNyomtatas %>"
                                                    ToolTip="<%$Resources:Buttons,MegsJegyzNyomtatas %>" />
                                                <%--									<a href="#" onmouseover="swap('MegsJegyzNyomtatas', 'images/hu/ikon/megs_jegyz_nyomtatasa1.png');" onmouseout="swap('MegsJegyzNyomtatas', 'images/hu/ikon/megs_jegyz_nyomtatasa1_keret_nelkul.png');">
									            <img src="images/hu/ikon/megs_jegyz_nyomtatasa1_keret_nelkul.png" class="ikon" id="MegsJegyzNyomtatas" AlternateText="Megsemmis�t� jegyz�k nyomtat�sa"/></a>
                                                --%>
                                            </td>

                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSztorno">
                                                <asp:ImageButton ID="ImageSztorno" runat="server" ImageUrl="../images/hu/ikon/storno1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'storno1.jpg')" onmouseout="swapByName(this.id,'storno1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Sztorno" AlternateText="<%$Resources:Buttons,Sztorno %>"
                                                    ToolTip="<%$Resources:Buttons,Sztorno %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonFelulvizsgalat">
                                                <asp:ImageButton ID="ImageFelulvizsgalat" runat="server" ImageUrl="../images/hu/ikon/felulvizsgalat_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'felulvizsgalat.jpg')" onmouseout="swapByName(this.id,'felulvizsgalat_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Felulvizsgalat" AlternateText="<%$Resources:Buttons,Felulvizsgalat %>"
                                                    ToolTip="<%$Resources:Buttons,Felulvizsgalat %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonJegyzekZarolas">
                                                <asp:ImageButton ID="ImageJegyzekZarolas" runat="server" ImageUrl="../images/hu/ikon/Jegyzekzarolas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Jegyzekzarolas1.jpg')" onmouseout="swapByName(this.id,'Jegyzekzarolas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="JegyzekZarolas" AlternateText="<%$Resources:Buttons,JegyzekZarolas %>"
                                                    ToolTip="<%$Resources:Buttons,JegyzekZarolas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonJegyzekVegrehajtas">
                                                <asp:ImageButton ID="ImageJegyzekVegrehajtas" runat="server" ImageUrl="../images/hu/ikon/Jegyzekvegrehajtas1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Jegyzekvegrehajtas1.jpg')" onmouseout="swapByName(this.id,'Jegyzekvegrehajtas1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="JegyzekVegrehajtas" AlternateText="<%$Resources:Buttons,JegyzekVegrehajtas %>"
                                                    ToolTip="<%$Resources:Buttons,JegyzekVegrehajtas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonJegyzekMegsemmisites">
                                                <asp:ImageButton ID="ImageJegyzekMegsemmisites" runat="server" ImageUrl="../images/hu/ikon/Jegyzekmegsemmisites_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Jegyzekmegsemmisites.jpg')" onmouseout="swapByName(this.id,'Jegyzekmegsemmisites_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="JegyzekMegsemmisites" AlternateText="<%$Resources:Buttons,JegyzekMegsemmisites %>"
                                                    ToolTip="<%$Resources:Buttons,JegyzekMegsemmisites %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSelejtezesreKijeloles">
                                                <asp:ImageButton ID="ImageSelejtezesreKijeloles" runat="server" ImageUrl="../images/hu/ikon/Selejtezesrekijeloles_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Selejtezesrekijeloles.jpg')" onmouseout="swapByName(this.id,'Selejtezesrekijeloles_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="SelejtezesreKijeloles" AlternateText="<%$Resources:Buttons,SelejtezesreKijeloles %>"
                                                    ToolTip="<%$Resources:Buttons,SelejtezesreKijeloles %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSelejtezesreKijelolesVisszavonasa">
                                                <asp:ImageButton ID="ImageSelejtezesreKijelolesVisszavonasa" runat="server" ImageUrl="../images/hu/ikon/Selejtezesrekijelolesvisszavonasa_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Selejtezesrekijelolesvisszavonasa.jpg')" onmouseout="swapByName(this.id,'Selejtezesrekijelolesvisszavonasa_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="SelejtezesreKijelolesVisszavonasa" AlternateText="<%$Resources:Buttons,SelejtezesreKijelolesVisszavonasa %>"
                                                    ToolTip="<%$Resources:Buttons,SelejtezesreKijelolesVisszavonasa %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonSelejtezes">
                                                <asp:ImageButton ID="ImageSelejtezes" runat="server" ImageUrl="../images/hu/ikon/Selejtezes_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'Selejtezes.jpg')" onmouseout="swapByName(this.id,'Selejtezes_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Selejtezes" AlternateText="<%$Resources:Buttons,Selejtezes %>"
                                                    ToolTip="<%$Resources:Buttons,Selejtezes %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonLomtarbaHelyezes">
                                                <asp:ImageButton ID="ImageLomtarbaHelyezes" runat="server" ImageUrl="../images/hu/ikon/lomtar_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'lomtar.jpg')" onmouseout="swapByName(this.id,'lomtar_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="LomtarbaHelyezes" AlternateText="<%$Resources:Buttons,LomtarbaHelyezes %>"
                                                    ToolTip="<%$Resources:Buttons,LomtarbaHelyezes %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonBoritonyomtatas">
                                                <asp:ImageButton ID="ImageBoritonyomtatas" runat="server" ImageUrl="../images/hu/ikon/borito_nyomtatasa1_keret_ne.jpg"
                                                    onmouseover="swapByName(this.id,'borito_nyomtatasa1.jpg')" onmouseout="swapByName(this.id,'borito_nyomtatasa1_keret_ne.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Boritonyomtatas" AlternateText="<%$Resources:Buttons,Borito_nyomtatasa %>"
                                                    ToolTip="<%$Resources:Buttons,Borito_nyomtatasa %>" />
                                                <%--									<a href="#" onmouseover="swap('boritonyomtatas', 'images/hu/ikon/borito_nyomtatasa1.jpg');" onmouseout="swap('boritonyomtatas', 'images/hu/ikon/borito_nyomtatasa1_keret_ne.jpg');">
									            <img src="images/hu/ikon/borito_nyomtatasa1_keret_ne.jpg" class="ikon" id="boritonyomtatas" AlternateText="Bor�t� nyomtat�sa"/></a>
                                                --%>
                                            </td>

                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIktatokonyvAlairas">
                                                <asp:ImageButton ID="ImageIktatokonyvAlairas" runat="server" ImageUrl="../images/hu/ikon/iratcsatolmanyalairas_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'iratcsatolmanyalairas.jpg')" onmouseout="swapByName(this.id,'iratcsatolmanyalairas_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IktatokonyvAlairas"
                                                    AlternateText="Al��r�s"
                                                    ToolTip="Al��r�s" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIktatokonyvLezaras">
                                                <asp:ImageButton ID="ImageIktatokonyvLezaras" runat="server" ImageUrl="../images/hu/ikon/IktatokonyvLezaras_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'IktatokonyvLezaras.jpg')" onmouseout="swapByName(this.id,'IktatokonyvLezaras_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IktatokonyvLezaras" AlternateText="<%$Resources:Buttons,Lezaras %>"
                                                    ToolTip="<%$Resources:Buttons,Lezaras %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsCsoportHierarchia">
                                                <asp:ImageButton ID="ImageButtonCsoportHierarchia" runat="server" ImageUrl="../images/hu/ikon/csoport_hierarchia_megtekint_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'csoport_hierarchia_megtekint.jpg')" onmouseout="swapByName(this.id,'csoport_hierarchia_megtekint_keret_nelkul.jpg')"
                                                    AlternateText="Csoport hierarchia"
                                                    ToolTip="Csoport hierarchia" />
                                            </td>
                                            <%-- HTML nyomtat�s --%>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonPrintHtml">
                                                <print:PrintHtml ID="PrintHtml" runat="server" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonTomegesOlvasasiJog">
                                                <asp:ImageButton ID="ImageTomegesOlvasasiJog" runat="server" ImageUrl="../images/hu/ikon/tomeges_olvasasi_jog_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'tomeges_olvasai_jog.jpg')" onmouseout="swapByName(this.id,'tomeges_olvasasi_jog_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="TomegesOlvasasiJog" AlternateText="<%$Resources:Buttons,TomegesOlvasasiJog %>"
                                                    ToolTip="<%$Resources:Buttons,TomegesOlvasasiJog %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableSendObjects">
                                                <asp:ImageButton ID="ImageSendObjects" runat="server" ImageUrl="../images/hu/ikon/valasz1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'valasz1.jpg')" onmouseout="swapByName(this.id,'valasz1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="SendObjects" AlternateText="<%$Resources:Buttons,Sendobjects %>"
                                                    ToolTip="<%$Resources:Buttons,Sendobjects %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonLock">
                                                <asp:ImageButton ID="ImageLock" runat="server" ImageUrl="../images/hu/ikon/lezaras1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'lezaras1.jpg')" onmouseout="swapByName(this.id,'lezaras1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Lock" AlternateText="<%$Resources:Buttons,Zarolas %>"
                                                    ToolTip="<%$Resources:Buttons,Zarolas %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonUnLock">
                                                <asp:ImageButton ID="ImageUnLock" runat="server" ImageUrl="../images/hu/ikon/lezaras_visszavonasa1_keret.jpg"
                                                    onmouseover="swapByName(this.id,'lezaras_visszavonasa1.jpg')" onmouseout="swapByName(this.id,'lezaras_visszavonasa1_keret.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="Unlock" AlternateText="<%$Resources:Buttons,Zarolas_feloldasa %>"
                                                    ToolTip="<%$Resources:Buttons,Zarolas_feloldasa %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonHatosagiTomeges">
                                                <asp:ImageButton ID="ImageHatosagiTomeges" runat="server" ImageUrl="../images/hu/ikon/hatosagi_tomeges_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'hatosagi_tomeges.jpg')" onmouseout="swapByName(this.id,'hatosagi_tomeges_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="HatosagiAdatokTomegesModositasa" AlternateText="<%$Resources:Buttons,Hatosagi_tomeges %>"
                                                    ToolTip="<%$Resources:Buttons,Hatosagi_tomeges %>" />
                                            </td>
                                            <td class="ikonSorElem" id="RightFunctionButtonsTableButtonIratAtvetelIntezkedesre">
                                                <asp:ImageButton ID="ImageIratAtvetelIntezkedesre" runat="server" ImageUrl="../images/hu/ikon/atvetel1_keret_nelkul.jpg"
                                                    onmouseover="swapByName(this.id,'atvetel1.jpg')" onmouseout="swapByName(this.id,'atvetel1_keret_nelkul.jpg')"
                                                    OnClick="Right_FunkcionButtons_OnClick" CommandName="IratAtvetelIntezkedesre" AlternateText="<%$Resources:Buttons,IratAtvetelIntezkedesre %>"
                                                    ToolTip="<%$Resources:Buttons,IratAtvetelIntezkedesre %>" />
                                            </td>
                                            <td style="width: 10px;">&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <%--/Jobboldali funkcio gombok--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
    <%-- CR3155 - CSV export --%>
    <Triggers>
        <asp:PostBackTrigger ControlID="ImageCSVExport"></asp:PostBackTrigger>
    </Triggers>
    <%-- CR3155 - CSV export --%>
</asp:UpdatePanel>
