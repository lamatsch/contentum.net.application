<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LovListFooter.ascx.cs" Inherits="Component_LovListFooter" %>
<table style="width: 70%;">
        <tr>
        <td style="text-align: center;">
            <asp:ImageButton TabIndex = "1" ID="ImageOk" runat="server" 
               ImageUrl="~/images/hu/ovalgomb/kivalaszt.png" 
               onmouseover="swapByName(this.id,'kivalaszt2.png')" 
               onmouseout="swapByName(this.id,'kivalaszt.png')" 
               OnClick="ImageButton_Click" CommandName="Ok" Visible="True" />         
        </td>  
        <td style="text-align: center;">
            <asp:ImageButton TabIndex = "2" ID="ImageCancel" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/megsem.png" 
                onmouseover="swapByName(this.id,'megsem2.png')" 
                onmouseout="swapByName(this.id,'megsem.png')" 
                OnClientClick="window.close(); return false;" CommandName="Cancel" 
                OnClick="ImageButton_Click" Visible="True" />         
        </td>
        </tr>
</table>