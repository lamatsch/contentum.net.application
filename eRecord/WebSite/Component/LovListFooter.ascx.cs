using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_LovListFooter : System.Web.UI.UserControl
{
    public bool OkEnabled
    {
        get { return ImageOk.Enabled; }
        set { ImageOk.Enabled = value; }
    }

    public String OkOnClientClick
    {
        get { return ImageOk.OnClientClick; }
        set { ImageOk.OnClientClick = value; }
    }

    public ImageButton ImageButton_Ok
    {
        get { return ImageOk; }
    }

    public ImageButton ImageButton_Cancel
    {
        get { return ImageCancel; }
    }

    public event CommandEventHandler ButtonsClick;
    protected void ImageButton_Click(object sender, ImageClickEventArgs e)
    {
        if (ButtonsClick != null)
        {
            CommandEventArgs args = new CommandEventArgs((sender as ImageButton).CommandName, "");
            ButtonsClick(this, args);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {        
        UI.SwapImageToDisabled(ImageOk);
    }
}
