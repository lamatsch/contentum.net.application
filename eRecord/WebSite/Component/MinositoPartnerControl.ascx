﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeFile="MinositoPartnerControl.ascx.cs" Inherits="Component_MinositoPartnerControl" %>

<%@ Register Src="~/Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>


<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-ui.min.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/JavaScripts/json2.js") %>"></script>
<%--<link href="../Content/jquery-ui.min.css" rel="stylesheet" />--%>
<%--<link href="Content/bootstrapfullscreen.css" media="all" type="text/css" rel="stylesheet" />
<link href="Content/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="Content/font-awesome.min.css" type="text/css" rel="stylesheet" />--%>

<script type="text/javascript">
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_endRequest(function () {
        loadMinositoData();
        setMinositoDdlChangeEvent();
        documentReady();
    });

    $(function () {
        loadMinositoData();
        setMinositoDdlChangeEvent();
    });
    function loadMinositoData() {        

        if ($("[id$=HiddenField_MinositoSzuloPartnerId]").val() != null & $("[id$=HiddenField_MinositoSzuloPartnerId]").val() != '') {            
            $.ajax({
                url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetMinositoPartnerekList") %>',
                data: JSON.stringify({
                    prefixText: "",
                    count: $("[id$=HiddenField_Svc_RowsCount]").val(),
                    contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ";"
                        + $("[id$=HiddenField_MinositoSzuloPartnerId]").val()
                }),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    fillMinositoDropdownlist(data)
                },
                error: function (response) {
                    $("#divLoading").hide();
                    alert(response.responseText);
                },
                failure: function (response) {
                    $("#divLoading").hide();
                    alert(response.responseText);
                }
            });
        }
        else {            
            var ddlMinosito = $("[id$=DropDownListMinositoPartner]");
            ddlMinosito.empty().append('<option>' + $("[id$=HiddenFieldTranslation_JS_MissingSzuloMinositoPartner]").val() + '</option>');
        }
    }
    function fillMinositoDropdownlist(data) {
        if (data === null || data.d === null || data.d.length < 1) {
            var ddlMinosito = $("[id$=DropDownListMinositoPartner]");
            ddlMinosito.empty().append('<option>' + $("[id$=HiddenFieldTranslation_JS_NotFound]").val() + '.</option>');
            return;
        }
        $("#divLoading").hide();
        var ddlMinosito = $("[id$=DropDownListMinositoPartner]");
        ddlMinosito.empty().append('<option>' + $("[id$=HiddenFieldTranslation_JS_PleaseSelect]").val() + '</option>');
        $.each(data.d, function () {
                // BUG_13213
            //if (this.split('|')[3] != null & this.split('|')[3] != '') {
            //    ddlMinosito.append($("<option />")
            //        .val(this.split('|')[0])
            //        .text(this.split('|')[1] + ' (' + this.split('|')[3] + ')'));
            //}
            //else {
                ddlMinosito.append($("<option/>")
                    .val(this.split('|')[0])
                    .text(this.split('|')[1]));
            //}
        });
        if (GetMinositoPartner() != null && GetMinositoPartner() != '') {
            $("[id$=DropDownListMinositoPartner]").val(GetMinositoPartner());
        }
        else {
            $("DropDownListMinositoPartner select").val("0");
        }
    };

    function setMinositoDdlChangeEvent() {
        $("[id$=DropDownListMinositoPartner]").change(function () {
            var optionSelected = $("option:selected", this).text();
            var valueSelected = this.value;
            SetMinositoPartner(valueSelected, optionSelected);
        });
    }
    function SetMinositoPartner(id, name) {
        //console.log('SetMinositoPartner');
        $("[id$=HiddenField_MinositoId]").val(id);
        $("[id$=HiddenField_MinositoNev]").val(name);
        $("DropDownListMinositoPartner select").val(id);
    }
    function SetMinositoSzuloPartner(id, name) {
        //console.log('SetMinositoSzuloPartner');
        $("[id$=HiddenField_MinositoId]").val('');
        $("[id$=HiddenField_MinositoNev]").val('');
        $("[id$=HiddenField_MinositoSzuloPartnerId]").val(id);
        $("[id$=HiddenField_MinositoSzuloPartnerNev]").val(name);
        loadMinositoData();
    }
    function GetMinositoPartner() {
        return $("[id$=HiddenField_MinositoId]").val();
    }
    function GetMinositoSzuloPartner() {
        return $("[id$=HiddenField_MinositoSzuloPartnerId]").val();
    }
    function TestMinosito(id) {
        alert('OK ' + id);
    }

     function setMinosito() {
                try {
                    var id = $('#<%= MinositoSzervezet_PartnerTextBox.ClientID %>' + '_HiddenField1').val();

                    if (id != '') {
                        if (typeof SetMinositoSzuloPartner !== 'undefined')
                            SetMinositoSzuloPartner(id, null);
                    }

                } catch (e) {
                    //console.log(e);
                }
    }

    function documentReady() {
         try {                    
                            $('#<%= MinositoSzervezet_PartnerTextBox.ClientID %>' + '_PartnerMegnevezes').on('change', function () {                        
                                if (typeof setTimeout !== 'undefined' && typeof setMinosito !== 'undefined')
                                    setTimeout(setMinosito, 1500);
                            });
                    
                            $('#<%= MinositoSzervezet_PartnerTextBox.ClientID %>' + '_HiddenField1').on('change', function () {
                                if (typeof SetMinositoSzuloPartner !== 'undefined')
                                    SetMinositoSzuloPartner($(this).val(), null);
                            });
                        } catch (e) {
                            console.log(e);
                        }
    }

    $(document).ready(function () {
        documentReady();
    });


</script>

<table cellspacing="0" cellpadding="0" width="100%">
    <tr class="urlapSor_kicsi" runat="server">
  <%--      <td class="mrUrlapCaption_short">
            <asp:Label ID="Label31" runat="server" Text="Minõsítõ szervezet:"></asp:Label>
        </td>--%>
        <td class="mrUrlapMezo" width="0%">
            <uc2:partnertextbox id="MinositoSzervezet_PartnerTextBox" validate="false" tryfirechangeevent="true"
                runat="server" withallcimcheckboxvisible="false" />
        </td>
    </tr>
    <tr class="urlapSor_kicsi" runat="server">
    <%--    <td class="mrUrlapCaption_short">
            <asp:Label ID="Label14" runat="server" Text="Minõsítõ:"></asp:Label>
        </td>--%>
        <td class="mrUrlapMezo" colspan="1" width="0%">
            <select runat="server" id="DropDownListMinositoPartner" class="mrUrlapInputComboBox" visible="true">
            </select>
            <asp:ImageButton TabIndex="-1"
    ID="ViewImageButton" runat="server" CssClass="mrUrlapInputImageButton"
    ImageUrl="~/images/hu/egyeb/nagyito.gif" onmouseover="swapByName(this.id,'nagyito_keret.gif')" onmouseout="swapByName(this.id,'nagyito.gif')" AlternateText="Megtekint" />
                    <asp:RequiredFieldValidator ID="ValidatorMinosito" runat="server"
    ControlToValidate="DropDownListMinositoPartner" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>" />
        <asp:TextBox ID="TextBoxMinositoPartner" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Visible="false"></asp:TextBox>
        </td>
    </tr>
</table>



<img id="divLoading" style="display: none; height: 15px;"
    src="<%= Page.ResolveClientUrl("~/images/hu/egyeb/activity_indicator.gif") %>" />

<asp:HiddenField ID="HiddenField_MinositoId" runat="server" />
<asp:HiddenField ID="HiddenField_MinositoNev" runat="server" />

<asp:HiddenField ID="HiddenField_MinositoSzuloPartnerId" runat="server" Value="" />
<asp:HiddenField ID="HiddenField_MinositoSzuloPartnerNev" runat="server" />

<asp:HiddenField ID="HiddenField_Svc_FelhasznaloId" runat="server" Value="" />
<asp:HiddenField ID="HiddenField_Svc_RowsCount" runat="server" Value="" />

<asp:HiddenField ID="HiddenFieldTranslation_JS_MissingSzuloMinositoPartner" runat="server" Value="" />
<asp:HiddenField ID="HiddenFieldTranslation_JS_NotFound" runat="server" Value="" />
<asp:HiddenField ID="HiddenFieldTranslation_JS_PleaseSelect" runat="server" Value="" />




