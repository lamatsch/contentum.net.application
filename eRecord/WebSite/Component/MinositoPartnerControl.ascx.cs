﻿using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Resources;
using System;

public partial class Component_MinositoPartnerControl : System.Web.UI.UserControl
{
    private bool isReadOnly;

    private bool IsReadOnly
    {
        get { return isReadOnly; }
        set { isReadOnly = value; }
    }

    public bool Validate
    {
        set { ValidatorMinosito.Enabled = value; }
        get { return ValidatorMinosito.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            ValidatorMinosito.ValidationGroup = value;
            //TextBoxMinositoPartner.ValidationGroup = value;

        }
        get { return ValidatorMinosito.ValidationGroup; }
    }
    public int Count = 10;
    public string Text
    {
        set
        {
            HiddenField_MinositoNev.Value = value;
            TextBoxMinositoPartner.Text = value;
        }
        get { return HiddenField_MinositoNev.Value; }
    }

    public string HiddenFieldMinositoPartnerId
    {
        set { HiddenField_MinositoId.Value = value; }
        get { return HiddenField_MinositoId.Value; }
    }
    public string HiddenFieldMinositoSzuloPartnerId
    {
        set { HiddenField_MinositoSzuloPartnerId.Value = value; MinositoSzervezet_PartnerTextBox.Id_HiddenField = value; }
        get { return HiddenField_MinositoSzuloPartnerId.Value; }
    }

    public void SetReadOnly(bool value)
    {        
        IsReadOnly = value;

        // Ha ReadOnly, akkor a textbox-ot tesszük ki, egyébként a dropdownlistát:        
        this.MinositoSzervezet_PartnerTextBox.ReadOnly = IsReadOnly;
        this.TextBoxMinositoPartner.Visible = IsReadOnly;
        this.DropDownListMinositoPartner.Visible = !IsReadOnly;
    }

    public void SetPartnerTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        MinositoSzervezet_PartnerTextBox.SetPartnerTextBoxById(errorPanel);
        if (!String.IsNullOrEmpty(HiddenFieldMinositoPartnerId))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = HiddenFieldMinositoPartnerId;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Partnerek KRT_Partnerek = (KRT_Partnerek)result.Record;
                Text = KRT_Partnerek.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }        
        else
        {
            Text = String.Empty;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
        HiddenField_Svc_RowsCount.Value = Count.ToString();
        //HiddenField_MinositoSzuloPartnerId.Value = "67ce5eda-1b15-e711-a947-000d3a2773cf";
        JavaScripts.RegisterPopupWindowClientScript(Page);
        SetLinkButtons();
        SetTranslation();
    }

    private void SetLinkButtons()
    {
        ViewImageButton.OnClientClick =
            JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "",
            HiddenField_MinositoId.ClientID);
    }

    #region GET / SET IDS
    /// <summary>
    /// Szulo partner megadása
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public void SetSzuloPartner(string id, string name)
    {
        HiddenField_MinositoSzuloPartnerId.Value = id;
        HiddenField_MinositoSzuloPartnerNev.Value = name;
    }
    /// <summary>
    /// Minosito partner megadása
    /// </summary>
    /// <param name="id"></param>
    /// <param name="name"></param>
    public void SetMinositoPartner(string id, string name)
    {
        HiddenField_MinositoId.Value = id;
        HiddenField_MinositoNev.Value = name;
        TextBoxMinositoPartner.Text = name;
    }
    /// <summary>
    /// Szulo partner lekérése
    /// </summary>
    public string GetSzuloPartner(out string name)
    {
        name = HiddenField_MinositoSzuloPartnerNev.Value;
        return HiddenField_MinositoSzuloPartnerId.Value;
    }
    /// <summary>
    /// Minosito partner lekérése
    /// </summary>
    public string GetMinositoPartner(out string name)
    {
        name = HiddenField_MinositoNev.Value;
        return HiddenField_MinositoId.Value;
    }
    #endregion
    #region TRANSLATION
    /// <summary>
    /// Set translation
    /// </summary>
    private void SetTranslation()
    {
        HiddenFieldTranslation_JS_MissingSzuloMinositoPartner.Value = Resources.Form.JS_MissingSzuloMinositoPartner;
        HiddenFieldTranslation_JS_NotFound.Value = Resources.Form.JS_NotFound;
        HiddenFieldTranslation_JS_PleaseSelect.Value = Resources.Form.JS_PleaseSelect;
    }
    #endregion
}