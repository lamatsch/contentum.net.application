﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiGridViewPager.ascx.cs"
    Inherits="Component_MultiGridViewPager" %>
<!-- A kiválasztandó record Id-jának tárolása, updatepanel-en belül legyen -->
<asp:HiddenField ID="selectedRecordId" runat="server" />
<asp:HiddenField ID="selectedGridViewUniqueID" runat="server" />
<table id="MainTable" runat="server" cellpadding="0" cellspacing="0" border="0" width="0%">
    <tr>
        <td style="text-align: right; white-space: nowrap; padding-right: 20px;" class="sublistHeaderElement">
            <asp:TextBox ID="RowCountTextBox" runat="server" AutoPostBack="True" MaxLength="4"
                OnTextChanged="RowCountTextBox_TextChanged" Text="" ToolTip="Oldalankénti rekordok száma"
                Width="30" CssClass="sublistTextBox"></asp:TextBox>
            <asp:CheckBox ID="ScrollableCheckBox" runat="server" AutoPostBack="true" Checked="false"
                OnCheckedChanged="ScrollableCheckBox_CheckedChanged" ToolTip="Scrollozható lista" />
            <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server" TargetControlID="ScrollableCheckBox"
                UncheckedImageUrl="../images/hu/ikon/scrolling_off.jpg" UncheckedImageAlternateText="Scrollozhatóság bekapcsolása"
                CheckedImageUrl="../images/hu/ikon/scrolling_on.jpg" CheckedImageAlternateText="Scrollozhatóság kikapcsolása"
                ImageHeight="24" ImageWidth="26">
            </ajaxToolkit:ToggleButtonExtender>
        </td>
        <td valign="top" style="padding-right: 10px;">
            <asp:ImageButton runat="server" ImageUrl="../images/hu/egyeb/rewall.jpg" ID="rewall"
                onmouseover="swapByName(this.id,'rewall2.jpg')" onmouseout="swapByName(this.id,'rewall.jpg')"
                OnClick="ImageButton1_Click" CommandName="First" AlternateText="Első oldal" />
        </td>
        <td valign="top">
            <asp:ImageButton runat="server" ImageUrl="../images/hu/egyeb/rew.jpg" ID="rew" onmouseover="swapByName(this.id,'rew2.jpg')"
                onmouseout="swapByName(this.id,'rew.jpg')" OnClick="ImageButton1_Click" CommandName="Prev"
                AlternateText="Előző oldal" />
        </td>
        <td valign="middle" align="center" class="tlSzamlalo">
            <span style="white-space: nowrap">
                <asp:Label ID="Pager" runat="server" Text="Pager"></asp:Label>
                <asp:HiddenField ID="PageIndex_HiddenField" runat="server" />
                <asp:HiddenField ID="PageCount_HiddenField" runat="server" />
                <asp:Label ID="labelRecordNumber" runat="server" Text="(0)"></asp:Label>
            </span>
        </td>
        <td valign="top" style="padding-right: 10px;">
            <asp:ImageButton runat="server" ImageUrl="../images/hu/egyeb/ffw.jpg" ID="ffw" onmouseover="swapByName(this.id,'ffw2.jpg')"
                onmouseout="swapByName(this.id,'ffw.jpg')" OnClick="ImageButton1_Click" CommandName="Next"
                AlternateText="Következő oldal" />
        </td>
        <td valign="top">
            <asp:ImageButton runat="server" ImageUrl="../images/hu/egyeb/ffwd.jpg" ID="ffwd"
                onmouseover="swapByName(this.id,'ffwd2.jpg')" onmouseout="swapByName(this.id,'ffwd.jpg')"
                OnClick="ImageButton1_Click" CommandName="Last" AlternateText="Utolsó oldal" />
        </td>
        <td id="td_title" runat="server" width="90%" style="margin:-15px;">
            <asp:Label ID="labelTitle" runat="server" Text="" />
        </td>
    </tr>
</table>
