﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultiLineTextBox.ascx.cs" Inherits="Component_MultiLineTextBox" %>
<span class="DisableWrap">
<asp:TextBox ID="TextBox1" runat="server" CssClass="mrUrlapInput" Enabled="true" TextMode="MultiLine" Rows="4"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
    Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</span>