<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjektumTipusTextBox.ascx.cs" Inherits="Component_ObjektumTipusTextBox" %>
<div class="DisableWrap">
<asp:TextBox ID="ObjektumTipusNev" runat="server" CssClass="mrUrlapInput" Enabled="true"></asp:TextBox>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:ImageButton TabIndex = "-1" ID="LovImageButton" runat="server" ImageUrl="~/images/hu/lov/kivalaszt1.gif" onmouseover="swapByName(this.id,'kivalaszt1_keret.gif')" onmouseout="swapByName(this.id,'kivalaszt1.gif')"
 CssClass="mrUrlapInputImageButton" AlternateText="Kiválaszt" />

<asp:ImageButton TabIndex = "-1" ID="ResetImageButton" runat="server" Visible="false" 
ImageUrl="~/images/hu/egyeb/reset_icon.png" 
onmouseover="swapByName(this.id,'reset_icon_keret.png')" 
onmouseout="swapByName(this.id,'reset_icon.png')" 
AlternateText="Alapállapot"/>

<asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="ObjektumTipusNev" SetFocusOnError="true"
    Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
    <ajaxToolkit:ValidatorCalloutExtender
        ID="ValidatorCalloutExtender1" runat="server" TargetControlID="Validator1">
    <Animations>
            <OnShow>
            <Sequence>
                <HideAction Visible="true" />
                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
            </Sequence>    
            </OnShow>
     </Animations>
    </ajaxToolkit:ValidatorCalloutExtender>
</div>