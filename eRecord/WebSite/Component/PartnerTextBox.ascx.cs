using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class Component_PartnerTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    
    private string _Type = "All";

    #region public properties

    public string Type
    { 
        get { return _Type; }
        set { _Type = value; }
    }

    /// <summary>
    /// Sz�r�s a szervezet ('Organization') t�pus� partnerekre
    /// </summary>
    public void SetFilterType_Szervezet()
    {
        _Type = "Szervezet";
    }

    /// <summary>
    /// Nincs sz�r�s a partnerekre
    /// </summary>
    public void SetFilterType_All()
    {
        _Type = "All";
    }

    public bool Validate
    {
        set { Validator1.Enabled = value;}
        get { return Validator1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            PartnerMegnevezes.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public bool Enabled
    {
        set
        {
            PartnerMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return PartnerMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            PartnerMegnevezes.ReadOnly = value;
            LovImageButton.Enabled = !value;
            NewImageButton.Enabled = !value;
            ResetImageButton.Enabled = !value;
            if (value == true)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }


        }
        get { return PartnerMegnevezes.ReadOnly; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string Text
    {
        set { PartnerMegnevezes.Text = value; }
        get { return PartnerMegnevezes.Text; }
    }

    public TextBox TextBox
    {
        get { return PartnerMegnevezes; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }
    
    public HiddenField Control_HiddenField
    {
        get { return HiddenField1; }
    }

    public ImageButton ImageButton_Lov
    {
        get { return LovImageButton; }
    }

    public ImageButton ImageButton_New
    {
        get { return NewImageButton; }
    }

    public ImageButton ImageButton_View
    {
        get { return ViewImageButton; }
    }
       

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                PartnerMegnevezes.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    
    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                PartnerMegnevezes.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;            
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekint�s gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        ResetImageButton.Visible = !Validate && !ViewMode;
        ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
        + TextBox.ClientID + "').value = '';return false";

        switch (_Type)
        { 
            case "All":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx", QueryStringVars.Filter + "=" + _Type
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                        , CommandName.Command + "=" + CommandName.New
                        + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                        + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                    "PartnerekForm.aspx","", HiddenField1.ClientID);

                break;
            case "Szervezet":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID);

                break;

            case "Szemely":
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID);

                break;

            case Constants.FilterType.Partnerek.BelsoSzemely:
                OnClick_Lov = JavaScripts.SetOnClientClick("PartnerekLovList.aspx"
                        , QueryStringVars.Filter + "=" + _Type
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                                , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

                OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
                       , CommandName.Command + "=" + CommandName.New
                       + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
                       + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
                       + "&" + QueryStringVars.Filter + "=" + _Type
                       , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);


                OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField("PartnerekForm.aspx", "", HiddenField1.ClientID);

                break;

        }

        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");
    }

    public void SetPartnerTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Partnerek KRT_Partnerek = (KRT_Partnerek)result.Record;
                Text = KRT_Partnerek.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
                //if (errorUpdatePanel != null)
                //{
                //    errorUpdatePanel.Update();
                //}
            }
        }
        else
        {
            Text = "";
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(PartnerMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
