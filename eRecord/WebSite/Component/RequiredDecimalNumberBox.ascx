﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequiredDecimalNumberBox.ascx.cs" Inherits="Component_RequiredDecimalNumberBox" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>

<uc:RequiredNumberBox ID="DecimalNumberBox" runat="server" FilterType="NumbersWithDecimalSeparator" ValidateFormat="true"/>