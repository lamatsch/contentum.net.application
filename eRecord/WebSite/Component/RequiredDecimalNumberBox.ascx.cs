﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_RequiredDecimalNumberBox : System.Web.UI.UserControl
{
    public bool Enabled
    {
        get
        {
            return DecimalNumberBox.Enabled;
        }
        set
        {
            DecimalNumberBox.Enabled = value;
        }
    }

    public bool ReadOnly
    {
        get
        {
            return DecimalNumberBox.ReadOnly;
        }
        set
        {
            DecimalNumberBox.ReadOnly = value;
        }
    }

    public bool Validate
    {
        get
        {
            return DecimalNumberBox.Validate;
        }
        set
        {
            DecimalNumberBox.Validate = value;
        }
    }

    public string CssClass
    {
        get
        {
            return DecimalNumberBox.CssClass;
        }
        set
        {
            DecimalNumberBox.CssClass = value;
        }
    }

    public string Text
    {
        get
        {
            return DecimalNumberBox.Text;
        }
        set
        {
            DecimalNumberBox.Text = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}