using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_RequiredNumberBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    public bool Enabled
    {
        set 
        { 
            TextBox1.Enabled = value;
            FilteredTextBoxExtender1.Enabled = value;
        }
        get { return TextBox1.Enabled; }
    }

    public string ValidationGroup
    {
        set
        {
            RequiredFieldValidator1.ValidationGroup = value;
            RegExpNumberValidator.ValidationGroup = value;
            TextBox1.ValidationGroup = value;
        }
        get { return RequiredFieldValidator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public bool ReadOnly
    {
        set 
        { 
            TextBox1.ReadOnly = value;
            FilteredTextBoxExtender1.Enabled = !value;
        }
        get { return TextBox1.ReadOnly; }
    }

    public string Text
    {
        set { TextBox1.Text = value; }
        get { return TextBox1.Text; }
    }

    public int Number
    {
        get
        {
            if (String.IsNullOrEmpty(Text))
                return 0;

            int number;

            if (Int32.TryParse(Text, out number))
            {
                return number;
            }
            else
            {
                Text = "0";
                return 0;
            }
        }
        set
        {
            Text = value.ToString();
        }
    }

    public Double DecimalNumber
    {
        get
        {
            if (String.IsNullOrEmpty(Text))
                return 0.0;

            Double dnumber;

            if (Double.TryParse(Text, out dnumber))
            {
                return dnumber;
            }
            else
            {
                Text = String.Concat("0", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, "0");
                return 0.0;
            }
        }
        set
        {
            Text = value.ToString();
        }
    }

    public TextBox TextBox
    {
        get { return TextBox1; }
    }

    public int MaxLength
    {
        get { return TextBox1.MaxLength; }
        set { TextBox1.MaxLength = value; }
    }

    public bool Validate
    {
        set 
        { 
            RequiredFieldValidator1.Enabled = value;
            RequiredFieldValidator1.Visible = value;
        }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public bool ValidateFormat
    {
        set
        {
            RegExpNumberValidator.Enabled = value;
            RegExpNumberValidator.Visible = value;
        }
        get { return RegExpNumberValidator.Enabled; }
    }

    public string CssClass
    {
        set { TextBox1.CssClass = value; }
        get { return TextBox1.CssClass; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    public Boolean AutoPostBack
    {
        get { return TextBox.AutoPostBack; }
        set { TextBox.AutoPostBack = value; }
    }

    public string ToolTip
    {
        get { return TextBox.ToolTip; }
        set { TextBox.ToolTip = value; }
    }

    public Unit Width
    {
        get { return TextBox.Width; }
        set { TextBox.Width = value; }
    }

    public event EventHandler TextChanged
    {
        add { TextBox.TextChanged += value; }
        remove { TextBox.TextChanged -= value; }
    }

    public bool EnableNegativeNumbers
    {
        get
        {
            if (ViewState["EnableNegativeNumbers"] != null)
            {
                return (bool)ViewState["EnableNegativeNumbers"];
            }
            else
            {
                return false;
            }
        }

        set
        {
            ViewState["EnableNegativeNumbers"] = value;
            this.SetFilteredTextBox(this.FilterType, value);
        }
    }

    public enum NumberTextBoxFilterTypes
    {
        Numbers,
        NumbersWithDot,
        NumbersWithDecimalSeparator,
        NumberWithGroupSeperator,
        NumbersWithGroupSeparator,
        NumbersWithSlashDot
    }
    public NumberTextBoxFilterTypes FilterType
    {
        get
        {
            if (ViewState["NumberTextBoxFilterType"] != null)
            {
                return (NumberTextBoxFilterTypes)ViewState["NumberTextBoxFilterType"];
            }
            else
            {
                return NumberTextBoxFilterTypes.Numbers;
            }
        }

        set
        {
            ViewState["NumberTextBoxFilterType"] = value;
            this.SetFilteredTextBox(value, this.EnableNegativeNumbers);
        }
    }

    public bool RightAlign
    {
        set
        {
            if (value)
            {
                TextBox1.Style[HtmlTextWriterStyle.TextAlign] = "right";
            }
        }
    }

    public bool FilteredTextBoxExtenderEnabled
    {
        get
        {
            return FilteredTextBoxExtender1.Enabled;
        }
        set
        {
            FilteredTextBoxExtender1.Enabled = value;
        }
    }

    private void SetFilteredTextBox(NumberTextBoxFilterTypes ntbtype, bool enableNegativeNumbers)
    {
        string validCharsSign = EnableNegativeNumbers ? "-" : "";
        string regexpSign = EnableNegativeNumbers ? "-?" : "";
        FilteredTextBoxExtender1.FilterType = AjaxControlToolkit.FilterTypes.Numbers | AjaxControlToolkit.FilterTypes.Custom;
        FilteredTextBoxExtender1.ValidChars = validCharsSign;
        switch (ntbtype)
        {
            case NumberTextBoxFilterTypes.Numbers:
                RegExpNumberValidator.ValidationExpression = String.Concat(@"^", regexpSign, @"[0-9]+$");
                break;
            case NumberTextBoxFilterTypes.NumbersWithDot:
                FilteredTextBoxExtender1.ValidChars += ".";
                RegExpNumberValidator.ValidationExpression = String.Concat(@"^", regexpSign, @"[0-9]+(.[0-9]+)?$");
                break;
            case NumberTextBoxFilterTypes.NumbersWithSlashDot:
                FilteredTextBoxExtender1.ValidChars += ".";
                FilteredTextBoxExtender1.ValidChars += "/";
                RegExpNumberValidator.ValidationExpression = String.Concat(@"^", regexpSign, @"[0-9]+(.[0-9]+)?$");
                break;
            case NumberTextBoxFilterTypes.NumbersWithDecimalSeparator:
                FilteredTextBoxExtender1.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                RegExpNumberValidator.ValidationExpression = String.Concat(@"^", regexpSign, @"[0-9]+(", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, @"[0-9]+)?$");
                break;
            case NumberTextBoxFilterTypes.NumbersWithGroupSeparator:
                FilteredTextBoxExtender1.ValidChars += System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator;
                break;
        }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public bool FormatNumber
    {
        get;
        set;
    }

    public int NumberMaxLength
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }


    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;
        RegExpNumberValidator.Enabled = false;
        FilteredTextBoxExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.DesignMode)
        {
            // Test for ScriptManager and register if it exists
            sm = ScriptManager.GetCurrent(Page);

            if (sm == null)
                throw new HttpException("A ScriptManager control must exist on the current page.");


            sm.RegisterScriptControl(this);
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (!this.DesignMode)
        {
            sm.RegisterScriptDescriptors(this);
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        if (!FormatNumber)
            return null;

        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.NumberFormatBehavior", this.TextBox.ClientID);
        descriptor.AddProperty("NumberMaxLength", this.NumberMaxLength);

        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        if (!FormatNumber)
            return null;

        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/NumberFormatBehavior.js";

        return new ScriptReference[] { reference };
    }

    #endregion
}
