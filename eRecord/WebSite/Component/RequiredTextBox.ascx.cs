﻿using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;

public partial class Component_RequiredTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent
{
    public bool ReadOnly
    {
        set 
        { 
            TextBox1.ReadOnly = value;
            if (Validate)
            {
                RequiredFieldValidator1.Enabled = !value;
            }
        }
        get { return TextBox1.ReadOnly; }
    }

    public string Text
    {
        set { TextBox1.Text = value; }
        get { return TextBox1.Text; }
    }

    public TextBox TextBox
    {
        get {return TextBox1;}
    }

    public TextBoxMode TextBoxMode
    {
        get { return TextBox1.TextMode; }
        set { TextBox1.TextMode = value; }
    }

    public TextBoxMode TextMode
    {
        get { return TextBox1.TextMode; }
        set { TextBox1.TextMode = value; }
    }

    public int Rows
    {
        get { return TextBox1.Rows; }
        set { TextBox1.Rows = value; }
    }

    public int MaxLength
    {
        get { return TextBox1.MaxLength; }
        set { TextBox1.MaxLength = value; }
    }

    public bool Validate
    {
        set 
        { 
            RequiredFieldValidator1.Enabled = value;
            ViewState["Validate"] = value;
        }
        get 
        {
            object o = ViewState["Validate"];
            if (o != null)
                return (bool)o;
            return true; 
        }
    }

    public string ValidationGroup
    {
        set { RequiredFieldValidator1.ValidationGroup = value; }
        get { return RequiredFieldValidator1.ValidationGroup; }
    }

    public Unit Width
    {
        get { return TextBox1.Width; }
        set { TextBox1.Width = value; }
    }

    public string CssClass
    {
        set { TextBox1.CssClass = value; }
        get { return TextBox1.CssClass; }
    }

    Boolean _Enabled = true;

    public Boolean Enabled
    {
        get { return _Enabled; }
        set
        {
            _Enabled = value;
            TextBox1.Enabled = _Enabled;
            RequiredFieldValidator1.Enabled = _Enabled;
        }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass += " ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass += " ViewDisabledWebControl";
            }
        }
    }

    public RequiredFieldValidator Validator
    {
        get
        {
            return this.RequiredFieldValidator1;
        }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();
        componentList.Add(TextBox1);

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;

        return componentList;
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterOnValidatorOverClientScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }
    }

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion
}
