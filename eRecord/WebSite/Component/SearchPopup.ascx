﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchPopup.ascx.cs" Inherits="Component_SearchPopup" %>
 <script type="text/javascript">
 var showingTimeout = null;
 function showSearchPanel()
 {
    var ext = $find('<%=popupExtender.ClientID %>');
    if(ext)
    {
        showingTimeout = window.setTimeout("$find('<%=popupExtender.ClientID %>').showPopup();",500);
    }
 }
 
 function clearShowSearchPanel()
 {
    if(showingTimeout)
    {
        window.clearTimeout(showingTimeout);
        showingTimeout = null;
    }
 }
 
 </script>
<asp:Panel ID="pnlSearchPopup" runat="server" CssClass="sp_Panel" Style="display: none">
    <div style="padding: 8px">
        <div id="searchText" class="sp_SearchText">
            <asp:Label ID="labelSearchText" runat="server" Text=""></asp:Label>
        </div>      
    </div>
</asp:Panel>

<ajaxToolkit:PopupControlExtender ID="popupExtender" runat="server" TargetControlID="pnlSearchPopup" PopupControlID="pnlSearchPopup" Position="Bottom" > 
</ajaxToolkit:PopupControlExtender>
