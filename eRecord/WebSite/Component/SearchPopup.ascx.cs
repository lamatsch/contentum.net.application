﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Component_SearchPopup : System.Web.UI.UserControl
{
    #region public properties

    public AjaxControlToolkit.PopupControlExtender PopupControlExtender
    {
        get
        {
            return this.popupExtender;
        }
    }

    public string SearchText
    {
        get
        {
            return this.labelSearchText.Text;
        }
        set
        {
            this.labelSearchText.Text = value;
        }
    }

    public Label Search
    {
        get
        {
            return this.labelSearchText;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
