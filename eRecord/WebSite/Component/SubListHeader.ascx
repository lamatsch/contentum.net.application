<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubListHeader.ascx.cs"
    Inherits="Component_SubListHeader" %>
<table class="kUrlap" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="text-align: left; height:0px; vertical-align: top;">
                        <!-- A kiv�lasztand� record Id-j�nak t�rol�sa, updatepanel-en bel�l legyen -->
                        <asp:HiddenField ID="selectedRecordId" runat="server" />
                        <table style="width:438px" cellpadding="0" cellspacing="0">
                            <tr>
                                    <td>
                                        <asp:ImageButton  runat="server" ImageUrl="~/images/hu/trapezgomb/uj_trap.jpg"  ID="ImageNew" OnClick="Buttons_OnClick" CommandName="New"
                                             AlternateText="�j" onmouseover="swapByName(this.id,'uj_trap2.jpg')" onmouseout="swapByName(this.id,'uj_trap.jpg')" />
                                    </td>
                                    <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/megtekintes_trap.jpg"  ID="ImageView" OnClick="Buttons_OnClick" CommandName="View"
                                             AlternateText="Megtekint�s" onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')"/>
                                    </td>
                                    <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/modositas_trap.jpg" ID="ImageModify" OnClick="Buttons_OnClick" CommandName="Modify"
                                             AlternateText="M�dos�t�s" onmouseover="swapByName(this.id,'modositas_trap2.jpg')" onmouseout="swapByName(this.id,'modositas_trap.jpg')"/>
                                    </td>
                                    <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/ervenytelenites_trap.jpg" ID="ImageInvalidate" OnClick="Buttons_OnClick" CommandName="Invalidate"
                                             AlternateText="�rv�nytelen�t�s" onmouseover="swapByName(this.id,'ervenytelenites_trap2.jpg')" onmouseout="swapByName(this.id,'ervenytelenites_trap.jpg')"/>
                                    </td>
                                    <%-- <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/skontorba_tesz_trap.jpg" ID="ImageSkontrobaHelyez" OnClick="Buttons_OnClick" CommandName="SkontrobaHelyez"
                                             AlternateText="Skontr�ba helyez�s" onmouseover="swapByName(this.id,'skontorba_tesz_trap2.jpg')" onmouseout="swapByName(this.id,'skontorba_tesz_trap.jpg')" Visible="false"/>
                                    </td>
                                    <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/skontorbol_kivesz_trap.jpg" ID="ImageSkontrobolKivetel" OnClick="Buttons_OnClick" CommandName="SkontrobolKivesz"
                                             AlternateText="Skontr�b�l kiv�tel" onmouseover="swapByName(this.id,'skontorbol_kivesz_trap2.jpg')" onmouseout="swapByName(this.id,'skontorbol_kivesz_trap.jpg')" Visible="false"/>
                                    </td>
                                    <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/irattar_trap.jpg" ID="ImageIrattarozasra" OnClick="Buttons_OnClick" CommandName="Irattarozasra"
                                             AlternateText="Iratt�rba k�ld�s" onmouseover="swapByName(this.id,'irattar_trap2.jpg')" onmouseout="swapByName(this.id,'irattar_trap.jpg')" Visible="false"/>
                                    </td>
                                     <td>
                                        <asp:ImageButton  runat="server" ImageUrl="../images/hu/trapezgomb/kiadas_osztalyra_trap.jpg" ID="ImageKiadasOsztalyra" OnClick="Buttons_OnClick" CommandName="KiadasOsztalyra"
                                             AlternateText="Kiad�s oszt�lyra" onmouseover="swapByName(this.id,'kiadas_osztalyra_trap2.jpg')" onmouseout="swapByName(this.id,'kiadas_osztalyra_trap.jpg')" Visible="false"/>
                                    </td> --%>
                                    <td style="width: 100%">
                                        &nbsp;
                                    </td>                                    
                            </tr>
                        </table>
                    </td>
                    <td runat="server" id="tdRightButtons" style="text-align: right; vertical-align: top; height:0px; width:100%;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td style="text-align: right; vertical-align: top;">                                 
                                    <asp:RadioButtonList ID="ValidFilter" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="ValidFilter_SelectedIndexChanged" CssClass="sublistHeaderElement">
                                        <asp:ListItem Text="&#201;rv." Value="Valid" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="&#201;rv.telen" Value="Invalid"></asp:ListItem>
                                        <asp:ListItem Text="Mind" Value="All"></asp:ListItem>
                                    </asp:RadioButtonList>    
                                </td>
                                <td style="width:100%">
                                </td>
                                <td style="text-align: right;white-space:nowrap;padding-right:20px;" class="sublistHeaderElement">
                                    <asp:TextBox ID="RowCountTextBox" runat="server" AutoPostBack="True" MaxLength="4" OnTextChanged="RowCountTextBox_TextChanged" Text="" ToolTip="Oldalank�nti rekordok sz�ma" Width="30" CssClass="sublistTextBox"></asp:TextBox>
                                    <asp:CheckBox ID="ScrollableCheckBox" runat="server" AutoPostBack="true" Checked="false" OnCheckedChanged="ScrollableCheckBox_CheckedChanged" ToolTip="Scrollozhat� lista" />
                                    <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender1" runat="server"
                                    TargetControlID="ScrollableCheckBox" UncheckedImageUrl="../images/hu/ikon/scrolling_off.jpg" UncheckedImageAlternateText="Scrollozhat�s�g bekapcsol�sa"
                                    CheckedImageUrl="../images/hu/ikon/scrolling_on.jpg" CheckedImageAlternateText="Scrollozhat�s�g kikapcsol�sa" ImageHeight="24" ImageWidth="26">
                                    </ajaxToolkit:ToggleButtonExtender>
                                </td>
                                <td valign="top" style="padding-right:10px;">
                                    <asp:ImageButton  runat="server" ImageUrl="../images/hu/egyeb/rewall.jpg" id="rewall" onmouseover="swapByName(this.id,'rewall2.jpg')" onmouseout="swapByName(this.id,'rewall.jpg')" OnClick="ImageButton1_Click" CommandName="First" AlternateText="Els� oldal"/>
                                </td>
                                <td valign="top">
                                    <asp:ImageButton  runat="server" ImageUrl="../images/hu/egyeb/rew.jpg" id="rew" onmouseover="swapByName(this.id,'rew2.jpg')" onmouseout="swapByName(this.id,'rew.jpg')" OnClick="ImageButton1_Click" CommandName="Prev" AlternateText="El�z� oldal"/>                                        
                                </td>
                                <td valign="middle" align="center" class="tlSzamlalo">
                                    <span style="white-space:nowrap">
                                        <asp:Label ID="Pager" runat="server" Text="Pager"></asp:Label>
                                        <asp:HiddenField ID="PageIndex_HiddenField" runat="server" />
                                        <asp:HiddenField ID="PageCount_HiddenField" runat="server" />
                                        <asp:Label ID="labelRecordNumber" runat="server" Text="(0)"></asp:Label>
                                    </span>     
                                </td>
                                <td valign="top" style="padding-right:10px;">
                                     <asp:ImageButton  runat="server" ImageUrl="../images/hu/egyeb/ffw.jpg" id="ffw" onmouseover="swapByName(this.id,'ffw2.jpg')" onmouseout="swapByName(this.id,'ffw.jpg')" OnClick="ImageButton1_Click" CommandName="Next" AlternateText="K�vetkez� oldal"/>
                                        </td>
                                <td valign="top">
                                    <asp:ImageButton  runat="server" ImageUrl="../images/hu/egyeb/ffwd.jpg" id="ffwd" onmouseover="swapByName(this.id,'ffwd2.jpg')" onmouseout="swapByName(this.id,'ffwd.jpg')" OnClick="ImageButton1_Click" CommandName="Last" AlternateText="Utols� oldal"/>                                        
                                        </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
