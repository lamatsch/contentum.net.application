﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TUKFelhasznaloMultiSelect.ascx.cs" Inherits="Component_TUKFelhasznaloMultiSelect" %>

<%@ Register Src="~/Component/TUKFelhasznaloTextBox.ascx" TagName="TUKFelhasznaloTextBox" TagPrefix="uc" %>

<asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always" RenderMode="Block">
    <ContentTemplate>
        <asp:Panel ID="ActivePanel" runat="server">
            <table border="0" cellpadding="0" cellspacing="0">

                <tr style="text-align: left;">
                    <td class="mrUrlapMezo" style="font-weight: normal;" colspan="2">
                        <uc:TUKFelhasznaloTextBox id="TextBoxSearch" runat="server" validate="false" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; vertical-align: top; padding-top: 5px">
                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="5" SelectionMode="Multiple" CssClass="mrUrlapInputComboBox" />
                    </td>
                    <td style="padding-top: 5px;padding-left:2px">
<%--                        <div>
                            <asp:ImageButton ID="ButtonAdd" runat="server" ImageUrl="~/images/hu/trapezgomb/hozzaad_trap.jpg"
                            onmouseover="swapByName(this.id,'hozzaad_trap2.jpg')" onmouseout="swapByName(this.id,'hozzaad_trap.jpg')"
                            AlternateText="Hozzáad" Style="padding-right: 5px;" OnClick="ButtonAdd_Click" CausesValidation="false"/>
                        </div>--%>
                        <div>
                            <asp:ImageButton ID="ButtonRemove" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg"
                            onmouseover="swapByName(this.id,'torles_trap2.jpg')" onmouseout="swapByName(this.id,'torles_trap.jpg')"
                            AlternateText="Töröl" OnClick="ButtonRemove_Click" CausesValidation="false" />
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="ReadonlyPanel" runat="server" Visible="false">
              <asp:TextBox ID="ReadonlyText" runat="server" Rows="5" TextMode="MultiLine" CssClass="mrUrlapInput" ReadOnly="true"/>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
