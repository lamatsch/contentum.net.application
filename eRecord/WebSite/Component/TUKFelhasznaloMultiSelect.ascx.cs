﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_TUKFelhasznaloMultiSelect : System.Web.UI.UserControl
{

    public bool ReadOnly
    {
        get
        {
            return !ActivePanel.Visible;
        }
        set
        {
            ActivePanel.Visible = !value;
            ReadonlyPanel.Visible = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        TextBoxSearch.TextBox.AutoPostBack = true;
        TextBoxSearch.TextBox.TextChanged += new EventHandler(TextBox_TextChanged);
    }

    void TextBox_TextChanged(object sender, EventArgs e)
    {
        AddItemToSearchResult();
    }

    protected void ButtonAdd_Click(object sender, ImageClickEventArgs e)
    {
        AddItemToSearchResult();
    }

    protected void ButtonRemove_Click(object sender, ImageClickEventArgs e)
    {
        RemoveItemFromSearchResult();
    }

    private void AddItemToSearchResult()
    {
        string id = TextBoxSearch.Id_HiddenField;
        string text = TextBoxSearch.Text;

        if (!String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(text))
        {
            ListItem li = ListBoxSearchResult.Items.FindByValue(id);

            if (li == null)
            {
                ListBoxSearchResult.Items.Add(new ListItem(text, id));
            }

            TextBoxSearch.Id_HiddenField = String.Empty;
            TextBoxSearch.Text = String.Empty;
            SetFocus(TextBoxSearch.TextBox);
        }
    }

    private void RemoveItemFromSearchResult()
    {
        for (int i = ListBoxSearchResult.Items.Count - 1; i > -1; i--)
        {
            if (ListBoxSearchResult.Items[i].Selected)
            {
                ListBoxSearchResult.Items.RemoveAt(i);
            }
        }
    }

    void SetFocus(Control control)
    {
        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm != null)
        {
            sm.SetFocus(control);
        }
        else
        {
            Page.SetFocus(control);
        }
    }

    public string GetSelectedText()
    {
        List<string> texts = new List<string>();

        foreach (ListItem item in ListBoxSearchResult.Items)
        {
            texts.Add(item.Text);
        }

        return String.Join(", ", texts.ToArray());
    }

    public void SetSelectedText(string text)
    {
        ListBoxSearchResult.Items.Clear();
        if (!String.IsNullOrEmpty(text))
        {
            string[] items = text.Split(',');

            foreach (string item in items)
            {
                if (!String.IsNullOrEmpty(item) && !String.IsNullOrEmpty(item.Trim()))
                {
                    string itemText = item.Trim();
                    ListBoxSearchResult.Items.Add(new ListItem(itemText));
                }
            }
        }

        ReadonlyText.Text = text;
    }
}