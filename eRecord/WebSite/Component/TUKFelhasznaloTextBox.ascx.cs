﻿using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_TUKFelhasznaloTextBox : System.Web.UI.UserControl, ISelectableUserComponent, Contentum.eUtility.ISearchComponent, IScriptControl
{
    #region public properties


    public bool Validate
    {
        set { Validator1.Enabled = value; }
        get { return Validator1.Enabled; }
    }


    public string ValidationGroup
    {
        set
        {
            Validator1.ValidationGroup = value;
            PartnerMegnevezes.ValidationGroup = value;

        }
        get { return Validator1.ValidationGroup; }
    }

    public AjaxControlToolkit.ValidatorCalloutExtender ValidatorCalloutExtender
    {
        set
        {
            ValidatorCalloutExtender1 = value;
        }
        get { return ValidatorCalloutExtender1; }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string OnClick_View
    {
        set { ViewImageButton.OnClientClick = value; }
        get { return ViewImageButton.OnClientClick; }
    }

    public string OnClick_New
    {
        set { NewImageButton.OnClientClick = value; }
        get { return NewImageButton.OnClientClick; }
    }

    public string OnClick_Reset
    {
        get { return ResetImageButton.OnClientClick; }
        set { ResetImageButton.OnClientClick = value; }
    }

    public string Text
    {
        set
        {
            PartnerMegnevezes.Text = value;
        }
        get
        {
            return PartnerMegnevezes.Text;
        }
    }

    public TextBox TextBox
    {
        get { return PartnerMegnevezes; }
    }

    public string BehaviorID
    {
        get { return TextBox.ClientID + "_Behavior"; }
    }

    public string FilterBehaviorID
    {
        get { return TextBox.ClientID + "_FilterBehavior"; }
    }

    public string CssClass
    {
        get { return PartnerMegnevezes.CssClass; }
        set { PartnerMegnevezes.CssClass = value; }
    }

    public HiddenField HiddenField
    {
        get { return HiddenField1; }
    }



    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    private string _LabelRequiredIndicatorID;

    public string LabelRequiredIndicatorID
    {
        get { return _LabelRequiredIndicatorID; }
        set { _LabelRequiredIndicatorID = value; }
    }

    public bool Enabled
    {
        set
        {
            PartnerMegnevezes.Enabled = value;
            LovImageButton.Enabled = value;
            NewImageButton.Enabled = value;
            ResetImageButton.Enabled = value;
            AutoCompleteExtender1.Enabled = value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = value;
            if (value == false)
            {
                LovImageButton.CssClass = "disabledLovListItem";
                NewImageButton.CssClass = "disabledLovListItem";
                ResetImageButton.CssClass = "disabledLovListItem";
            }
            else
            {
                LovImageButton.CssClass = "mrUrlapInputImageButton";
                NewImageButton.CssClass = "mrUrlapInputImageButton";
                ResetImageButton.CssClass = "mrUrlapInputImageButton";
            }

        }
        get { return PartnerMegnevezes.Enabled; }
    }

    public bool ReadOnly
    {
        set
        {
            PartnerMegnevezes.ReadOnly = value;
            LovImageButton.Visible = !value;
            NewImageButton.Visible = !value;
            ResetImageButton.Visible = !value;
            AutoCompleteExtender1.Enabled = !value;
            if (_worldJumpEnabled)
                WorldJumpExtender1.Enabled = !value;

        }
        get { return PartnerMegnevezes.ReadOnly; }
    }


    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            ReadOnly = !_ViewEnabled;
            if (!_ViewEnabled)
            {
                PartnerMegnevezes.CssClass += " ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
                NewImageButton.CssClass = "ViewReadOnlyWebControl";
                ResetImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                PartnerMegnevezes.CssClass += " ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
                NewImageButton.CssClass = "ViewDisabledWebControl";
                ResetImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }

    private bool searchMode = false;
    public bool SearchMode
    {
        get
        {
            return searchMode;
        }
        set
        {
            searchMode = value;
            Validate = !value;
            NewImageButton.Visible = !value;
        }
    }

    private bool viewMode = false;
    /// <summary>
    /// Csak a megtekintés gomb lesz fenn
    /// </summary>
    public bool ViewMode
    {
        get
        {
            return viewMode;
        }
        set
        {
            if (value == true)
            {
                viewMode = value;
                Validate = !value;
                LovImageButton.Visible = !value;
                NewImageButton.Visible = !value;
                ViewImageButton.Visible = value;
                ResetImageButton.Visible = !value;
            }
        }
    }

    private bool _worldJumpEnabled = true;

    public bool WorldJumpEnabled
    {
        get
        {
            return this._worldJumpEnabled;
        }
        set
        {
            this._worldJumpEnabled = value;
            WorldJumpExtender1.Enabled = value;
        }
    }

    private bool _tryFireChangeEvent = true;

    public bool TryFireChangeEvent
    {
        get { return _tryFireChangeEvent; }
        set { _tryFireChangeEvent = value; }
    }

    #endregion

    #region AJAX
    private bool ajaxEnabled = true;
    public bool AjaxEnabled
    {
        get
        {
            return ajaxEnabled;
        }
        set
        {
            ajaxEnabled = value;
        }
    }
    #endregion

    private string queryString_Lov = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterOnValidatorOverClientScript(Page);

        if (!ReadOnly)
        {
            ResetImageButton.Visible = !Validate && !ViewMode;
        }

        if (!ajaxEnabled)
        {
            //ASP.NET 2.0 bug work around
            TextBox.Attributes.Add("readonly", "readonly");
            AutoCompleteExtender1.Enabled = false;
            WorldJumpEnabled = false;
        }
        else
        {
            AutoCompleteExtender1.ServicePath = "~/WrappedWebService/Ajax.asmx";

            String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
            AutoCompleteExtender1.ContextKey = felh_Id;
        }

        queryString_Lov = QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
            + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID;


        if (_tryFireChangeEvent)
        {
            queryString_Lov += "&" + QueryStringVars.TryFireChangeEvent + "=1";
        }


        OnClick_Lov = JavaScripts.SetOnClientClick("TUKFelhasznalokLovList.aspx", queryString_Lov
                             , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_New = JavaScripts.SetOnClientClick("PartnerekForm.aspx"
           , CommandName.Command + "=" + CommandName.New
           + "&" + QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + PartnerMegnevezes.ClientID
            , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

        OnClick_View = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "PartnerekForm.aspx", "", HiddenField1.ClientID, this.BehaviorID);

        ResetImageButton.OnClientClick = "$get('" + HiddenField1.ClientID + "').value = '';$get('"
        + TextBox.ClientID + "').value = '';return false";

        if (ajaxEnabled)
        {
            //Manuális beírás esetén az id törlése
            string jsClearHiddenField = "var clear =true; if (typeof(event) != 'undefined' && event){if(event.lovlist && event.lovlist == 1) clear = false;}"
                + "if(clear) $get('" + HiddenField1.ClientID + @"').value = '';";

            TextBox.Attributes.Add("onchange", jsClearHiddenField);
        }
    }

    public void SetPartnerTextBoxById(Contentum.eUIControls.eErrorPanel errorPanel)
    {
        if (!String.IsNullOrEmpty(Id_HiddenField))
        {
            KRT_PartnerekService service = eAdminService.ServiceFactory.GetKRT_PartnerekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id_HiddenField;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                KRT_Partnerek KRT_Partnerek = (KRT_Partnerek)result.Record;
                Text = KRT_Partnerek.Nev;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result);
            }
        }
        else
        {
            Text = "";
        }
    }

    public void Page_PreRender(object sender, EventArgs e)
    {
    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(PartnerMegnevezes);
        componentList.Add(LovImageButton);
        componentList.Add(NewImageButton);
        componentList.Add(ViewImageButton);
        componentList.Add(ResetImageButton);

        LovImageButton.OnClientClick = "";
        NewImageButton.OnClientClick = "";
        ViewImageButton.OnClientClick = "";
        ResetImageButton.OnClientClick = "";

        // Lekell tiltani a ClientValidator
        Validator1.Enabled = false;
        AutoCompleteExtender1.Enabled = false;

        return componentList;
    }

    #endregion

    #region ISearchComponent Members

    public string GetSearchText()
    {
        return this.Text;
    }

    #endregion

    #region IScriptControl Members

    private ScriptManager sm;

    protected override void OnPreRender(EventArgs e)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                // Test for ScriptManager and register if it exists
                sm = ScriptManager.GetCurrent(Page);

                if (sm == null)
                    throw new HttpException("A ScriptManager control must exist on the current page.");


                sm.RegisterScriptControl(this);
            }
        }

        if (!String.IsNullOrEmpty(LabelRequiredIndicatorID))
        {
            Control c = this.NamingContainer.FindControl(LabelRequiredIndicatorID);

            if (c != null)
            {
                c.Visible = Validate;
            }
        }

        base.OnPreRender(e);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (ajaxEnabled)
        {
            if (!this.DesignMode)
            {
                sm.RegisterScriptDescriptors(this);
            }
        }

        base.Render(writer);
    }

    public System.Collections.Generic.IEnumerable<ScriptDescriptor> GetScriptDescriptors()
    {
        ScriptControlDescriptor descriptor = new ScriptControlDescriptor("Utility.CsoportAutoComplete", this.TextBox.ClientID);
        descriptor.AddProperty("AutoCompleteExtenderClientId", this.AutoCompleteExtender1.ClientID);
        descriptor.AddProperty("HiddenFieldClientId", this.HiddenField1.ClientID);
        descriptor.AddProperty("id", this.BehaviorID);
        return new ScriptDescriptor[] { descriptor };
    }

    public System.Collections.Generic.IEnumerable<ScriptReference> GetScriptReferences()
    {
        ScriptReference reference = new ScriptReference();
        reference.Path = "~/JavaScripts/AutoComplete.js?t=20190607";
        return new ScriptReference[] { reference };
    }

    #endregion
}