<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TreeViewHeader.ascx.cs"
    Inherits="Component_TreeViewHeader" %>
<%@ Register Src="SearchPopup.ascx" TagName="SearchPopup" TagPrefix="uc1" %>
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <!-- A kiv�lasztand� record Id-j�nak t�rol�sa -->
        <asp:HiddenField ID="selectedRecordId" runat="server" />
        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
            TargetControlID="Panel2" CollapsedSize="12" Collapsed="False" ExpandControlID="TreeViewHeaderCPEButton"
            CollapseControlID="TreeViewHeaderCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
            AutoExpand="false" ScrollContents="false" CollapsedImage="~/images/hu/Grid/plus.gif"
            ExpandedImage="~/images/hu/Grid/minus.gif" ImageControlID="TreeViewHeaderCPEButton">
        </ajaxToolkit:CollapsiblePanelExtender>
        <asp:Panel ID="Panel2" runat="server">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="vertical-align: top; padding-left: 0px; padding-top: 0px; width: 0%;">
                        <asp:ImageButton runat="server" ImageUrl="~/images/hu/Grid/minus.gif" ID="TreeViewHeaderCPEButton"
                            OnClientClick="return false;" />
                    </td>
                    <td style="width: 0%; text-align: left; vertical-align: top;">
                        <asp:Panel ID="FilterFunctionButtonsPanel" runat="server">
                            <table cellpadding="0" cellspacing="0" style="height: 80px;">
                                <tr style="height: 46px;">
                                    <td colspan="4" style="height: 46px; text-align: left; vertical-align: middle; white-space: nowrap; width: 100%;">
                                        <asp:Label ID="Header" runat="server" Text="Header" CssClass="ListHeaderText"></asp:Label>&nbsp;
                                        <asp:Label ID="IsFilteredLabel" runat="server" CssClass="ListHeaderText"></asp:Label>
                                        <asp:Label ID="labelFilter" runat="server" CssClass="ListHeaderText"></asp:Label>
                                        <uc1:SearchPopup ID="spSearchPanel" runat="server" />
                                    </td>
                                </tr>
                                <tr style="height: 32px; text-align: left;">
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%; height: 100%;">
                                            <tr style="height: 16px; vertical-align: top;">
                                                <td>
                                                    <asp:ImageButton ID="ImageSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap.jpg"
                                                        onmouseover="swapByName(this.id,'kereses_trap2.jpg')" onmouseout="swapByName(this.id,'kereses_trap.jpg')"
                                                        OnClick="Filter_FunctionButtons_OnClick" CommandName="kereses" AlternateText="<%$Resources:Buttons,Search %>"
                                                        ToolTip="<%$Resources:Buttons,Search %>" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageRefresh" runat="server" ImageUrl="~/images/hu/trapezgomb/frissites_trap.jpg"
                                                        onmouseover="swapByName(this.id,'frissites_trap2.jpg')" onmouseout="swapByName(this.id,'frissites_trap.jpg')"
                                                        OnClick="Filter_FunctionButtons_OnClick" CommandName="Refresh" AlternateText="<%$Resources:Buttons,Refresh %>"
                                                        ToolTip="<%$Resources:Buttons,Refresh %>" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" style="vertical-align: top; text-align: center">
                                                    <asp:ImageButton ID="ImageDefaultFilter" runat="server" ImageUrl="~/images/hu/trapezgomb/alapallapot_trap.jpg"
                                                        onmouseover="swapByName(this.id,'alapallapot_trap2.jpg')" onmouseout="swapByName(this.id,'alapallapot_trap.jpg')"
                                                        OnClick="Filter_FunctionButtons_OnClick" CommandName="DefaultFilter" AlternateText="<%$Resources:Buttons,DefaultFilter %>"
                                                        ToolTip="<%$Resources:Buttons,DefaultFilter %>" />
                                                </td>
                                                <td colspan="1" style="vertical-align: top; text-align: center">
                                                    <asp:ImageButton ID="ImageButton_TreeViewPrint" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'nyomtatas_trap2.jpg')" onmouseout="swapByName(this.id,'nyomtatas_trap.jpg')" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td style="width: 10px;"></td>
                    <td style="width: 0%; text-align: center; vertical-align: top;">
                        <asp:Panel ID="NodeFunctionButtonsPanel" runat="server">
                            <table runat="server" id="NodeFunctionButtonsTable" cellpadding="0" cellspacing="0"
                                style="height: 80px;">
                                <tr style="height: 46px;">
                                    <td colspan="7" style="vertical-align: middle;">
                                        <asp:Label ID="HeaderNodeButtons" runat="server" Text="Node" CssClass="ListHeaderText"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr style="height: 32px;">
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%; height: 100%;">
                                            <tr style="height: 16px; vertical-align: top;">
                                                <td style="width: 0%">
                                                    <asp:ImageButton ID="ImageView" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                        onmouseover="swapByName(this.id,'megtekintes_trap2.jpg')" onmouseout="swapByName(this.id,'megtekintes_trap.jpg')"
                                                        OnClick="Node_FunctionButtons_OnClick" CommandName="View" AlternateText="<%$Resources:Buttons,View %>"
                                                        ToolTip="<%$Resources:Buttons,View %>" />
                                                </td>
                                                <td style="width: 0%">
                                                    <asp:ImageButton ID="ImageModify" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                        onmouseover="swapByName(this.id,'modositas_trap2.jpg')" onmouseout="swapByName(this.id,'modositas_trap.jpg')"
                                                        OnClick="Node_FunctionButtons_OnClick" CommandName="Modify" AlternateText="<%$Resources:Buttons,Modositas %>"
                                                        ToolTip="<%$Resources:Buttons,Modositas %>" />
                                                </td>
                                                <td style="width: 0%">
                                                    <asp:ImageButton ID="ImageInvalidate" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg"
                                                        onmouseover="swapByName(this.id,'torles_trap2.jpg')" onmouseout="swapByName(this.id,'torles_trap.jpg')"
                                                        OnClick="Node_FunctionButtons_OnClick" CommandName="Invalidate" AlternateText="<%$Resources:Buttons,Invalidate %>"
                                                        ToolTip="<%$Resources:Buttons,Invalidate %>" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:ImageButton ID="ImageExpandNode" runat="server" ImageUrl="~/images/hu/trapezgomb/csomopont_kibontasa_trap.jpg"
                                                        onmouseover="swapByName(this.id,'csomopont_kibontasa_trap2.jpg')" onmouseout="swapByName(this.id,'csomopont_kibontasa_trap.jpg')"
                                                        OnClick="Node_FunctionButtons_OnClick" CommandName="ExpandNode" AlternateText="<%$Resources:Buttons,ExpandNode %>"
                                                        ToolTip="<%$Resources:Buttons,ExpandNode %>" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageNodeRights" runat="server" ImageUrl="~/images/hu/trapezgomb/jogosultak_trap.jpg"
                                                        onmouseover="swapByName(this.id,'jogosultak_trap2.jpg')" onmouseout="swapByName(this.id,'jogosultak_trap.jpg')"
                                                        OnClick="Node_FunctionButtons_OnClick" CommandName="NodeRights" AlternateText="<%$Resources:Buttons,NodeRights %>"
                                                        ToolTip="<%$Resources:Buttons,NodeRights %>" Visible="false"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100%"></td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td style="width: 10px;"></td>
                    <td style="width: 30%; text-align: center; vertical-align: top;">
                        <asp:Panel ID="ChildFunctionButtonsPanel" runat="server">
                            <%--Alarendelt csomopont funkcio gombok--%>
                            <table id="ChildFunctionButtonsTable" cellpadding="0" cellspacing="0" style="height: 80px;">
                                <tr style="height: 46px;">
                                    <td colspan="2" style="vertical-align: middle;">
                                        <asp:Label ID="HeaderChildButtons" runat="server" Text="Child" CssClass="ListHeaderText"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr style="height: 32px;">
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%; height: 100%;">
                                            <tr style="height: 16px; vertical-align: top;">
                                                <td>
                                                    <asp:ImageButton ID="ImageNew" runat="server" ImageUrl="~/images/hu/trapezgomb/felvitel_trap.jpg"
                                                        onmouseover="swapByName(this.id,'felvitel_trap2.jpg')" onmouseout="swapByName(this.id,'felvitel_trap.jpg')"
                                                        OnClick="Child_FunctionButtons_OnClick" CommandName="New" AlternateText="<%$Resources:Buttons,New %>"
                                                        ToolTip="<%$Resources:Buttons,New %>" />
                                                </td>
                                                <td><%-- CR3206 - Iratt�ri strukt�ra --%>
                                                    <asp:ImageButton ID="ImageUgyiratokList" runat="server" ImageUrl="~/images/hu/trapezgomb/ugyirat_fa_trap.jpg"
                                                        onmouseover="swapByName(this.id,'ugyirat_fa_trap2.jpg')" onmouseout="swapByName(this.id,'ugyirat_fa_trap.jpg')"
                                                        OnClick="Child_FunctionButtons_OnClick" CommandName="UgyiratokList" AlternateText="<%$Resources:Buttons,UgyiratokListaja %>"
                                                        ToolTip="<%$Resources:Buttons,UgyiratokListaja %>" />
                                                </td>
                                                <%-- CR3206 - Iratt�ri strukt�ra --%>

                                                <td>
                                                    <asp:ImageButton ID="ImageVonalkodNyomtatasa" runat="server" 
                                                        ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg" OnClick="Child_FunctionButtons_OnClick"
                                                        CommandName="VonalkodNyomtatasa" AlternateText="<%$Resources:Buttons,Vonalkod_Nyomtatasa %>" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                  
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--/Alarendelt csomopont funkcio gombok--%>
                        </asp:Panel>
                    </td>
                    <td style="width: 10px;"></td>
                    <td style="width: 30%; text-align: center; vertical-align: top;">
                        <asp:Panel ID="Panel1" runat="server">
                            <%--Uj csomopont kijeloles funkcio gombok--%>
                            <table id="SelectNewChildFunctionButtonsTable" cellpadding="0" cellspacing="0" style="height: 80px;">
                                <tr style="height: 46px;">
                                    <td colspan="2" style="vertical-align: middle;">&nbsp;
                                    </td>
                                </tr>
                                <tr style="height: 32px;">
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%; height: 100%;">
                                            <tr style="height: 16px; vertical-align: top;">
                                                <td style="vertical-align: top; width: 0%">
                                                    <asp:CheckBox ID="cbSelectNewChild" ToolTip="<%$Resources:Buttons,SelectNewChild %>"
                                                        runat="server" />
                                                    <ajaxToolkit:ToggleButtonExtender ID="ToggleButtonExtender2" runat="server" TargetControlID="cbSelectNewChild"
                                                        UncheckedImageUrl="../images/hu/ikon/ujelemkijelol_off.jpg" UncheckedImageAlternateText="Felvitt elem kijel�l�s bekapcsol�sa"
                                                        CheckedImageUrl="../images/hu/ikon/ujelemkijelol_on.jpg" CheckedImageAlternateText="Felvitt elem kijel�l�s kikapcsol�sa"
                                                        ImageHeight="24" ImageWidth="26">
                                                    </ajaxToolkit:ToggleButtonExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--/Uj csomopont kijeloles funkcio gombok--%>
                        </asp:Panel>
                    </td>
                    <td style="width: 10px;"></td>
                    <td style="width: 30%; text-align: center; vertical-align: top;">
                        <asp:Panel ID="CustomNodeFunctionButtonsPanel" runat="server">
                            <%--Felhasznaloi csomopont funkcio gombok--%>
                            <table id="CustomNodeFunctionButtonsTable" cellpadding="0" cellspacing="0" style="height: 80px;">
                                <tr style="height: 46px;">
                                    <td colspan="1" style="vertical-align: middle;">
                                        <asp:Label ID="HeaderCustomNodeButtons" runat="server" Text="CustomNode" CssClass="ListHeaderText"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                                <tr style="height: 32px;">
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 0%; height: 100%">
                                            <tr style="height: 16px; vertical-align: top;">
                                                <td>
                                                    <asp:ImageButton ID="ImageNewCustomNode" runat="server" ImageUrl="~/images/hu/trapezgomb/felvitel_trap.jpg"
                                                        onmouseover="swapByName(this.id,'felvitel_trap2.jpg')" onmouseout="swapByName(this.id,'felvitel_trap.jpg')"
                                                        OnClick="CustomNode_FunctionButtons_OnClick" CommandName="New" AlternateText="<%$Resources:Buttons,New %>"
                                                        ToolTip="<%$Resources:Buttons,New %>" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1">&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--/Felhasznaloi csomopont funkcio gombok--%>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
