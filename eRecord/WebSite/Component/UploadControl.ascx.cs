using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Contentum.eAdmin.Utility;

public partial class Component_UploadControl : System.Web.UI.UserControl
{


    #region public properties

    public bool Validate
    {
        set { RequiredFieldValidator1.Enabled = value; }
        get { return RequiredFieldValidator1.Enabled; }
    }

    public String ValidationGroup
    {
        set
        {
            TextBox1.ValidationGroup = value;
            RequiredFieldValidator1.ValidationGroup = value;
        }
        get
        {
            return RequiredFieldValidator1.ValidationGroup;
        }
    }

    public string OnClick_Lov
    {
        set { LovImageButton.OnClientClick = value; }
        get { return LovImageButton.OnClientClick; }
    }

    public string Text
    {
        set { TextBox1.Text = value; }
        get { return TextBox1.Text; }
    }

    public System.Web.UI.WebControls.TextBox TextBox
    {
        get { return TextBox1; }
    }

    public bool Enabled
    {
        set
        {
            TextBox1.Enabled = value;
            LovImageButton.Enabled = value;
            if (value == false)
            {
                UI.SwapImageToDisabled(LovImageButton);
            }
        }
        get { return TextBox1.Enabled; }
    }

    public bool ReadOnly
    {
        set { TextBox1.ReadOnly = value; }
        get { return TextBox1.ReadOnly; }
    }

    public string Id_HiddenField
    {
        set { HiddenField1.Value = value; }
        get { return HiddenField1.Value; }
    }

    Boolean _ViewEnabled = true;

    public Boolean ViewEnabled
    {
        get { return _ViewEnabled; }
        set
        {
            _ViewEnabled = value;
            Enabled = _ViewEnabled;
            if (!_ViewEnabled)
            {
                TextBox1.CssClass = "ViewReadOnlyWebControl";
                LovImageButton.CssClass = "ViewReadOnlyWebControl";
            }
        }
    }

    Boolean _ViewVisible = true;

    public Boolean ViewVisible
    {
        get { return _ViewVisible; }
        set
        {
            _ViewVisible = value;
            Enabled = _ViewVisible;
            if (!_ViewVisible)
            {
                TextBox1.CssClass = "ViewDisabledWebControl";
                LovImageButton.CssClass = "ViewDisabledWebControl";
            }
        }
    }


    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        OnClick_Lov = JavaScripts.SetOnClientClick("UploadLovList.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID
           + "&" + QueryStringVars.TextBoxId + "=" + TextBox1.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);
        //ASP.NET 2.0 bug work around
        TextBox.Attributes.Add("readonly", "readonly");

    }

    #region ISelectableUserComponent method implementations

    public System.Collections.Generic.List<WebControl> GetComponentList()
    {
        System.Collections.Generic.List<WebControl> componentList = new System.Collections.Generic.List<WebControl>();

        componentList.Add(TextBox1);
        componentList.Add(LovImageButton);

        // Lekell tiltani a ClientValidator
        RequiredFieldValidator1.Enabled = false;

        return componentList;
    }

    #endregion

}
