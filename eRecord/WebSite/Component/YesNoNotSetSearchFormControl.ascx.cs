﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Component_YesNoNotSetSearchFormControl : System.Web.UI.UserControl, Contentum.eUtility.ISearchComponent
{

    public static class BoolString
    {
        public const string YesValue = "Yes";
        public const string NoValue = "No";
        public const string NotSetValue = "NotSet";

        public static string ConvertValueToBoolString(string Value)
        {
            switch (Value)
            {
                case BoolString.YesValue:
                    return Boolean.TrueString;
                case BoolString.NoValue:
                    return Boolean.FalseString;
                case BoolString.NotSetValue:
                    return string.Empty;
                default:
                    bool bValue;
                    if (Boolean.TryParse(Value, out bValue))
                    {
                        return Value;
                    }
                    return string.Empty;
            }
        }

        public static string ConvertBooleanStringToValue(string strBoolValue)
        {
            bool bValue;

            if (Boolean.TryParse(strBoolValue, out bValue))
            {
                return (bValue ? BoolString.YesValue : BoolString.NoValue);
            }
            else
            {
                return BoolString.NotSetValue;
            }
        }

        public static string ConvertBooleanToValue(bool? bValue)
        {
            if (bValue == null)
            {
                return BoolString.NotSetValue;
            }
            return (bValue == true ? BoolString.YesValue : BoolString.NoValue);

        }
    }

    #region Properties

    public string YesItemText
    {
        get
        {
            ListItem rb = rbl.Items.FindByValue(BoolString.YesValue);
            if (rb != null)
            {
                return rb.Text;
            }

            return String.Empty;
        }
        set
        {
            ListItem rb = rbl.Items.FindByValue(BoolString.YesValue);
            if (rb != null)
            {
                rb.Text = value;
            }
        }
    }

    public string NoItemText
    {
        get
        {
            ListItem rb = rbl.Items.FindByValue(BoolString.NoValue);
            if (rb != null)
            {
                return rb.Text;
            }

            return String.Empty;
        }
        set
        {
            ListItem rb = rbl.Items.FindByValue(BoolString.NoValue);
            if (rb != null)
            {
                rb.Text = value;
            }
        }
    }

    public string NotSetItemText
    {
        get
        {
            ListItem rb = rbl.Items.FindByValue(BoolString.NotSetValue);
            if (rb != null)
            {
                return rb.Text;
            }

            return String.Empty;
        }
        set
        {
            ListItem rb = rbl.Items.FindByValue(BoolString.NotSetValue);
            if (rb != null)
            {
                rb.Text = value;
            }
        }
    }

    public int RepeatColumns
    {
        get { return rbl.RepeatColumns; }
        set { rbl.RepeatColumns = value; }
    }

    public RepeatDirection RepeatDirection
    {
        get { return rbl.RepeatDirection; }
        set { rbl.RepeatDirection = value; }
    }

    public RepeatLayout RepeatLayout
    {
        get { return rbl.RepeatLayout; }
        set { rbl.RepeatLayout = value; }
    }

    public string SelectedValue
    {
        get { return BoolString.ConvertValueToBoolString(rbl.SelectedValue); }
        set { rbl.SelectedValue = BoolString.ConvertBooleanStringToValue(value); }
    }


    public string SelectedText
    {
        get
        {
            if (rbl.SelectedItem != null)
            {
                return rbl.SelectedItem.Text;
            } return String.Empty;
        }
    }

    #endregion Properties

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #region ISearchComponent Members

    string Contentum.eUtility.ISearchComponent.GetSearchText()
    {
        return SelectedText;
    }

    #endregion
}
