<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="CsatolasForm.aspx.cs" Inherits="CsatolasForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/IratTextBox.ascx" TagName="IratTextBox" TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/PldIratPeldanyokTextBox.ascx" TagName="IratPeldanyTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/KuldemenyTextBox.ascx" TagName="KuldemenyTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/IratMasterAdatok.ascx" TagName="IratMasterAdatok"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/IratPeldanyMasterAdatok.ascx" TagName="IratPeldanyMasterAdatok"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/KuldemenyMasterAdatok.ascx" TagName="KuldemenyMasterAdatok"
    TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <script type="text/javascript">

        function SaveActiveTab(activeTab) {
            var hiddenField = $get('<%=hfActiveTab.ClientID %>');
            if (hiddenField) {
                var uniqueId;
                if (activeTab.get_id() == '<%=tabPanelUgyirat.ClientID %>') {
                    uniqueId = '<%=tabPanelUgyirat.UniqueID %>';
                }

                if (activeTab.get_id() == '<%=tabPanelIrat.ClientID %>') {
                    uniqueId = '<%=tabPanelIrat.UniqueID %>';
                }

                if (activeTab.get_id() == '<%=tabPanelIratPeldany.ClientID %>') {
                    uniqueId = '<%=tabPanelIratPeldany.UniqueID %>';
                }

                if (activeTab.get_id() == '<%=tabPanelKuldemeny.ClientID %>') {
                    uniqueId = '<%=tabPanelKuldemeny.UniqueID %>';
                }

                if (uniqueId != '')
                    hiddenField.value = uniqueId;
            }
        }

        function SetActiveObjektum(sender, args) {
            if (sender && typeof (AjaxControlToolkit) != 'undefined' && AjaxControlToolkit.TabContainer != undefined) {
                if (AjaxControlToolkit.TabContainer.isInstanceOfType(sender)) {
                    var activeTab = sender.get_activeTab();
                    if (activeTab) {
                        SaveActiveTab(activeTab);
                        var label = $get('<%=labelCsatolando.ClientID %>');
                        var text = null;
                        if (label && label.firstChild)
                            text = label.firstChild;

                        var validatorUgyirat = $get('<%=Csatolando_UgyiratTextBox.Validator.ClientID %>');
                        var validatorIrat = $get('<%=Csatolando_IratTextBox.Validator.ClientID %>');
                        var validatorIratPeldany = $get('<%=Csatolando_IratPeldanyTextBox.Validator.ClientID %>');
                        var validatorKuldemeny = $get('<%=Csatolando_KuldemenyTextBox.Validator.ClientID %>');

                        if (activeTab.get_id() == '<%=tabPanelUgyirat.ClientID %>') {
                            if (text)
                                text.nodeValue = 'Csatoland� �gyirat:';

                            if (validatorUgyirat)
                                validatorUgyirat.enabled = true;

                            if (validatorIrat)
                                validatorIrat.enabled = false;

                            if (validatorIratPeldany)
                                validatorIratPeldany.enabled = false;

                            if (validatorKuldemeny)
                                validatorKuldemeny.enabled = false;

                            return true;
                        }

                        if (activeTab.get_id() == '<%=tabPanelIrat.ClientID %>') {
                            if (text)
                                text.nodeValue = 'Csatoland� irat:';

                            if (validatorUgyirat)
                                validatorUgyirat.enabled = false;

                            if (validatorIrat)
                                validatorIrat.enabled = true;

                            if (validatorIratPeldany)
                                validatorIratPeldany.enabled = false;

                            if (validatorKuldemeny)
                                validatorKuldemeny.enabled = false;

                            return true;
                        }

                        if (activeTab.get_id() == '<%=tabPanelIratPeldany.ClientID %>') {
                            if (text)
                                text.nodeValue = 'Csatoland� iratp�ld�ny:';

                            if (validatorUgyirat)
                                validatorUgyirat.enabled = false;

                            if (validatorIrat)
                                validatorIrat.enabled = false;

                            if (validatorIratPeldany)
                                validatorIratPeldany.enabled = true;

                            if (validatorKuldemeny)
                                validatorKuldemeny.enabled = false;

                            return true;
                        }

                        if (activeTab.get_id() == '<%=tabPanelKuldemeny.ClientID %>') {
                            if (text)
                                text.nodeValue = 'Csatoland� k�ldem�ny:';

                            if (validatorUgyirat)
                                validatorUgyirat.enabled = false;

                            if (validatorIrat)
                                validatorIrat.enabled = false;

                            if (validatorIratPeldany)
                                validatorIratPeldany.enabled = false;

                            if (validatorKuldemeny)
                                validatorKuldemeny.enabled = true;

                            return true;
                        }
                    }
                }
            }
        }

        function pageLoad() {
            var tabContainer = $find('<%=TabContainerCsatolando.ClientID %>');
            if (tabContainer)
                SetActiveObjektum(tabContainer, Sys.EventArgs.Empty);
        }
    </script>

    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,CsatolasFormHeaderTitle %>" />
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <uc10:UgyiratMasterAdatok ID="Ugy_UgyiratMasterAdatok" runat="server" Visible="false">
                    </uc10:UgyiratMasterAdatok>
                    <uc10:IratMasterAdatok ID="IratMasterAdatok1" runat="server" Visible="false"></uc10:IratMasterAdatok>
                    <uc10:IratPeldanyMasterAdatok ID="IratPeldanyMasterAdatok1" runat="server" Visible="false">
                    </uc10:IratPeldanyMasterAdatok>
                    <uc10:KuldemenyMasterAdatok ID="KuldemenyMasterAdatok1" runat="server" Visible="false">
                    </uc10:KuldemenyMasterAdatok>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption" style="vertical-align: middle; padding-top: 20px;">
                                    <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="labelCsatolando" runat="server" Text="Csatoland� �gyirat:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:HiddenField ID="hfActiveTab" runat="server" />
                                    <ajaxToolkit:TabContainer ID="TabContainerCsatolando" OnClientActiveTabChanged="SetActiveObjektum"
                                        runat="server" Width="350px">
                                        <ajaxToolkit:TabPanel runat="server" ID="tabPanelUgyirat" TabIndex="0" HeaderText="�gyirat">
                                            <ContentTemplate>
                                                <uc6:UgyiratTextBox ID="Csatolando_UgyiratTextBox" runat="server" />
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel runat="server" ID="tabPanelIrat" TabIndex="1" HeaderText="&nbsp;&nbsp;Irat&nbsp;&nbsp;">
                                            <ContentTemplate>
                                                <asp:RadioButtonList runat="server" ID="rbListOkozatisag" RepeatDirection="Horizontal"
                                                    Visible="false">
                                                    <asp:ListItem Text="El�zm�ny" Value="0"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Text="K�vetkezm�ny" Value="1"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <uc6:IratTextBox ID="Csatolando_IratTextBox" runat="server" />
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel runat="server" ID="tabPanelIratPeldany" TabIndex="2" HeaderText="Iratp�ld�ny">
                                            <ContentTemplate>
                                                <uc6:IratPeldanyTextBox ID="Csatolando_IratPeldanyTextBox" runat="server" />
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                        <ajaxToolkit:TabPanel runat="server" ID="tabPanelKuldemeny" TabIndex="3" HeaderText="K�ldem�ny">
                                            <ContentTemplate>
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr class="urlapSor" runat="server" id="trKuldemenyKapcsolatIrany" visible="false">
                                                        <td class="mrUrlapCaption_bal">
                                                            <asp:Label ID="labelKuldemenyKapcsolatTipus" runat="server" Text="Kapcsolat ir�nya:" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlKuldemenyKapcsolatIrany" CssClass="mrUrlapInputComboBoxKozepes" runat="server">
                                                                <asp:ListItem Text="Bor�t�kja" Value="B" />
                                                                <asp:ListItem Text="Tartalma" Value="T" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="2">
                                                            <uc6:KuldemenyTextBox ID="Csatolando_KuldemenyTextBox" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>
                                    </ajaxToolkit:TabContainer>
                                </td>
                            </tr>
                            <tr style="height: 5px">
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label2" runat="server" Text="Megjegyz�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="UgyiratKapcs_Leiras_TextBox" runat="server" Width="344px" Rows="2"
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
