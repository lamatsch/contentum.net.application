using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CsatolasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private String ObjektumId = "";
    private String Startup = String.Empty;
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private static class Okozatisag
    {
        public const string Elozmeny = "0";
        public const string Utozmany = "1";
    }

    private const String FunkcioKod_UgyiratCsatolasNew = "UgyiratCsatolasNew";
    private const String FunkcioKod_KuldemenyCsatolasNew = "KuldemenyCsatolasNew";


    private void SetKapcsolatBusinessObject(EREC_UgyiratObjKapcsolatok objKapcsolatok)
    {
        switch (Startup)
        {
            case Constants.Startup.FromUgyirat:
                {
                    if (TabContainerCsatolando.ActiveTab == tabPanelUgyirat)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_UgyUgyiratok;

                        objKapcsolatok.Obj_Id_Elozmeny = Csatolando_UgyiratTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_UgyUgyiratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelIratPeldany)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_UgyUgyiratok;

                        objKapcsolatok.Obj_Id_Elozmeny = Csatolando_IratPeldanyTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_PldIratPeldanyok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelKuldemeny)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = Csatolando_KuldemenyTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_KuldKuldemenyek;

                        objKapcsolatok.Obj_Id_Elozmeny = ObjektumId;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_UgyUgyiratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyiratKuldemeny;
                    }
                    break;
                }

            case Constants.Startup.FromIrat:
                {
                    
                    if (TabContainerCsatolando.ActiveTab == tabPanelIrat)
                    {
                        if (rbListOkozatisag.SelectedValue == Okozatisag.Elozmeny)
                        {
                            objKapcsolatok.Obj_Id_Elozmeny = Csatolando_IratTextBox.Id_HiddenField;
                            objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                        }
                        else
                        {
                            objKapcsolatok.Obj_Id_Elozmeny = ObjektumId;
                            objKapcsolatok.Obj_Id_Kapcsolt = Csatolando_IratTextBox.Id_HiddenField;
                        }
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_IraIratok;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_IraIratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Iratfordulat;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelIratPeldany)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_IraIratok;
                        objKapcsolatok.Obj_Id_Elozmeny = Csatolando_IratPeldanyTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_PldIratPeldanyok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelKuldemeny)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = Csatolando_KuldemenyTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_KuldKuldemenyek;
                        objKapcsolatok.Obj_Id_Elozmeny = ObjektumId;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_IraIratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.IratKuldemeny;
                    }
                    break;
                }

            case Constants.Startup.FromIratPeldany:
                {
                    objKapcsolatok.Obj_Id_Elozmeny = ObjektumId;
                    objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_PldIratPeldanyok;

                    if (TabContainerCsatolando.ActiveTab == tabPanelUgyirat)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = Csatolando_UgyiratTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_UgyUgyiratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelIrat)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = Csatolando_IratTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_IraIratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz;
                    }
                    break;
                }
            case Constants.Startup.FromKuldemeny:
                {
                    switch (ddlKuldemenyKapcsolatIrany.SelectedValue)
                    {
                        case "B":
                            objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                            objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_KuldKuldemenyek;
                            break;
                        case "T":
                            objKapcsolatok.Obj_Id_Elozmeny = ObjektumId;
                            objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_KuldKuldemenyek;
                            break;
                        default:
                            goto case "B";
                    }

                    if (TabContainerCsatolando.ActiveTab == tabPanelKuldemeny)
                    {

                        switch (ddlKuldemenyKapcsolatIrany.SelectedValue)
                        {
                            case "B":
                                objKapcsolatok.Obj_Id_Elozmeny = Csatolando_KuldemenyTextBox.Id_HiddenField;
                                objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_KuldKuldemenyek;
                                break;
                            case "T":
                                objKapcsolatok.Obj_Id_Kapcsolt = Csatolando_KuldemenyTextBox.Id_HiddenField;
                                objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_KuldKuldemenyek;
                                break;
                            default:
                                goto case "B";
                        }
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.KuldemenyBoritekTartalom;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelIrat)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_KuldKuldemenyek;
                        objKapcsolatok.Obj_Id_Elozmeny = Csatolando_IratTextBox.Id_HiddenField;;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_IraIratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.IratKuldemeny;
                    }
                    else if (TabContainerCsatolando.ActiveTab == tabPanelUgyirat)
                    {
                        objKapcsolatok.Obj_Id_Kapcsolt = ObjektumId;
                        objKapcsolatok.Obj_Type_Kapcsolt = Constants.TableNames.EREC_KuldKuldemenyek;

                        objKapcsolatok.Obj_Id_Elozmeny = Csatolando_UgyiratTextBox.Id_HiddenField;
                        objKapcsolatok.Obj_Type_Elozmeny = Constants.TableNames.EREC_UgyUgyiratok;
                        objKapcsolatok.KapcsolatTipus = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyiratKuldemeny;
                    }

                    break;
                }

            default:
                //error
                break;
        }

        objKapcsolatok.Updated.Obj_Id_Elozmeny = true;
        objKapcsolatok.Updated.Obj_Type_Elozmeny = true;
        objKapcsolatok.Updated.Obj_Id_Kapcsolt = true;
        objKapcsolatok.Updated.Obj_Type_Kapcsolt = true;
        objKapcsolatok.Updated.KapcsolatTipus = true;
    }
    
    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        Startup = Request.QueryString.Get(Constants.Startup.StartupName);
        // Jogosults�gellen�rz�s:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                if (Startup == Constants.Startup.FromKuldemeny)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_KuldemenyCsatolasNew);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratCsatolasNew);
                }
                break;
        }

        ObjektumId = Request.QueryString.Get(QueryStringVars.Id);
        //Startup = Request.QueryString.Get(Constants.Startup.StartupName);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        SetActiveTabFromClient();

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void SetActiveTabFromClient()
    {
        if (IsPostBack)
        {
            if (!String.IsNullOrEmpty(hfActiveTab.Value))
            {
                Control control = FindControl(hfActiveTab.Value);
                if (control != null && control is AjaxControlToolkit.TabPanel)
                {
                    AjaxControlToolkit.TabPanel activeTab = control as AjaxControlToolkit.TabPanel;
                    TabContainerCsatolando.ActiveTab = activeTab;
                    //TabContainerCsatolando.ActiveTabIndex = activeTab.TabIndex;
                }
            }
        }
    }



    private void LoadFormComponents()
    {
        if (String.IsNullOrEmpty(ObjektumId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            SetComponentVisiblity();

            switch (Startup)
            {
                case Constants.Startup.FromUgyirat:
                    {
                        Ugy_UgyiratMasterAdatok.UgyiratId = ObjektumId;
                        EREC_UgyUgyiratok erec_UgyUgyiratok =
                           Ugy_UgyiratMasterAdatok.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);

                        if (erec_UgyUgyiratok == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            // Csatolhat�-e hozz� �gyirat?

                            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
                            ErrorDetails errorDetail = null;
                            if (Ugyiratok.Csatolhato(ugyiratStatusz, out errorDetail) == false)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                    ResultError.CreateNewResultWithErrorCode(55010, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }
                        }
                        break;
                    }

                case Constants.Startup.FromIrat:
                    {
                        IratMasterAdatok1.IratId = ObjektumId;
                        EREC_IraIratok erec_IraIratok =
                           IratMasterAdatok1.SetIratMasterAdatokById(FormHeader1.ErrorPanel);

                        if (erec_IraIratok == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            // Csatolhat�-e hozz� irat?

                            Iratok.Statusz IratStatusz = Iratok.GetAllapotByBusinessDocument(erec_IraIratok);
                            ErrorDetails errorDetail = null;
                            if (Iratok.Csatolhato(IratStatusz, out errorDetail) == false)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                    ResultError.CreateNewResultWithErrorCode(55020, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }
                        }
                        break;
                    }

                case Constants.Startup.FromIratPeldany:
                    {
                        IratPeldanyMasterAdatok1.IratPeldanyId = ObjektumId;
                        EREC_PldIratPeldanyok erec_PldIratPeldanyok =
                           IratPeldanyMasterAdatok1.SetIratPeldanyMasterAdatokById(FormHeader1.ErrorPanel);

                        if (erec_PldIratPeldanyok == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            // Csatolhat�-e hozz� iratp�ld�ny?

                            IratPeldanyok.Statusz IratPeldanyStatusz = IratPeldanyok.GetAllapotByBusinessDocument(erec_PldIratPeldanyok);
                            ErrorDetails errorDetail = null;
                            if (IratPeldanyok.Csatolhato(IratPeldanyStatusz, out errorDetail) == false)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                        ResultError.CreateNewResultWithErrorCode(55030, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }
                        }
                        break;
                    }

                case Constants.Startup.FromKuldemeny:
                    {
                        //KuldemenyMasterAdatok1.KuldemenyId = ObjektumId;
                        EREC_KuldKuldemenyek erec_KuldKuldemenyek =
                           KuldemenyMasterAdatok1.SetKuldemenyMasterAdatok(ObjektumId, FormHeader1.ErrorPanel, null);

                        if (erec_KuldKuldemenyek == null)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                ResultError.CreateNewResultWithErrorCode(50101));
                            MainPanel.Visible = false;
                            return;
                        }
                        else
                        {
                            // Csatolhat�-e hozz� k�ldem�ny?

                            Kuldemenyek.Statusz KuldemenyStatusz = Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);
                            ErrorDetails errorDetail = null;
                            if (Kuldemenyek.Csatolhato(KuldemenyStatusz, out errorDetail) == false)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                                        ResultError.CreateNewResultWithErrorCode(55040, errorDetail));
                                MainPanel.Visible = false;
                                return;
                            }
                        }
                        break;
                    }

                default:
                    //error
                    break;
            }
        }
    }

    private void SetComponentVisiblity()
    {
        switch (Startup)
        {
            case Constants.Startup.FromUgyirat:
                {
                    Ugy_UgyiratMasterAdatok.Visible = true;
                    IratMasterAdatok1.Visible = false;
                    IratPeldanyMasterAdatok1.Visible = false;
                    KuldemenyMasterAdatok1.Visible = false;
                    rbListOkozatisag.Visible = false;
                    //tabf�lek
                    tabPanelUgyirat.Enabled = true;
                    tabPanelIrat.Enabled = false;
                    tabPanelIratPeldany.Enabled = true;
                    tabPanelKuldemeny.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyCsatolasNew);
                    trKuldemenyKapcsolatIrany.Visible = false;
                    TabContainerCsatolando.ActiveTab = tabPanelUgyirat;
                    //sz�r�sek
                    Csatolando_UgyiratTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Csatolt;
                    Csatolando_UgyiratTextBox.ObjektumId = ObjektumId;
                    Csatolando_IratPeldanyTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany;
                    Csatolando_IratPeldanyTextBox.ObjektumId = ObjektumId;
                    Csatolando_KuldemenyTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyiratKuldemeny;
                    Csatolando_KuldemenyTextBox.ObjektumId = ObjektumId;
                    break;
                }

            case Constants.Startup.FromIrat:
                {
                    Ugy_UgyiratMasterAdatok.Visible = false;
                    IratMasterAdatok1.Visible = true;
                    IratPeldanyMasterAdatok1.Visible = false;
                    KuldemenyMasterAdatok1.Visible = false;
                    rbListOkozatisag.Visible = true;
                    //tabf�lek
                    tabPanelUgyirat.Enabled = false;
                    tabPanelIrat.Enabled = true;
                    tabPanelIratPeldany.Enabled = true;
                    tabPanelKuldemeny.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyCsatolasNew); ;
                    trKuldemenyKapcsolatIrany.Visible = false;
                    TabContainerCsatolando.ActiveTab = tabPanelIrat;
                    //sz�r�sek
                    Csatolando_IratTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Iratfordulat;
                    Csatolando_IratTextBox.ObjektumId = ObjektumId;
                    Csatolando_IratPeldanyTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz;
                    Csatolando_IratPeldanyTextBox.ObjektumId = ObjektumId;
                    Csatolando_KuldemenyTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.IratKuldemeny;
                    Csatolando_KuldemenyTextBox.ObjektumId = ObjektumId;
                    break;
                }

            case Constants.Startup.FromIratPeldany:
                {
                    Ugy_UgyiratMasterAdatok.Visible = false;
                    IratMasterAdatok1.Visible = false;
                    IratPeldanyMasterAdatok1.Visible = true;
                    KuldemenyMasterAdatok1.Visible = false;
                    rbListOkozatisag.Visible = false;
                    //tabf�lek
                    tabPanelUgyirat.Enabled = true;
                    tabPanelIrat.Enabled = true;
                    tabPanelIratPeldany.Enabled = false;
                    tabPanelKuldemeny.Enabled = false;
                    TabContainerCsatolando.ActiveTab = tabPanelUgyirat;
                    //sz�r�sek
                    Csatolando_UgyiratTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyinditoPeldany;
                    Csatolando_UgyiratTextBox.ObjektumId = ObjektumId;
                    Csatolando_IratTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz;
                    Csatolando_IratTextBox.ObjektumId = ObjektumId;
                    break;
                }
            case Constants.Startup.FromKuldemeny:
                {
                    Ugy_UgyiratMasterAdatok.Visible = false;
                    IratMasterAdatok1.Visible = false;
                    IratPeldanyMasterAdatok1.Visible = false;
                    KuldemenyMasterAdatok1.Visible = true;
                    rbListOkozatisag.Visible = false;
                    //tabf�lek
                    tabPanelUgyirat.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratCsatolasNew);
                    tabPanelIrat.Enabled = FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratCsatolasNew);
                    tabPanelIratPeldany.Enabled = false;
                    tabPanelKuldemeny.Enabled = true;
                    trKuldemenyKapcsolatIrany.Visible = true;
                    TabContainerCsatolando.ActiveTab = tabPanelKuldemeny;
                    //sz�r�sek
                    Csatolando_KuldemenyTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.KuldemenyBoritekTartalom;
                    Csatolando_KuldemenyTextBox.ObjektumId = ObjektumId;
                    Csatolando_IratTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.IratKuldemeny;
                    Csatolando_IratTextBox.ObjektumId = ObjektumId;
                    Csatolando_UgyiratTextBox.FilterByObjektumKapcsolat = KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.UgyiratKuldemeny;
                    Csatolando_UgyiratTextBox.ObjektumId = ObjektumId;
                    break;
                }

            default:
                //error
                break;
        }
    }


    private EREC_UgyiratObjKapcsolatok GetBusinessObjectFromComponents()
    {
        EREC_UgyiratObjKapcsolatok objkapcsolat = new EREC_UgyiratObjKapcsolatok();
        objkapcsolat.Updated.SetValueAll(false);
        objkapcsolat.Base.Updated.SetValueAll(false);

        this.SetKapcsolatBusinessObject(objkapcsolat);
        
        objkapcsolat.Leiras = UgyiratKapcs_Leiras_TextBox.Text;
        objkapcsolat.Updated.Leiras = pageView.GetUpdatedByView(UgyiratKapcs_Leiras_TextBox);

        return objkapcsolat;
    }

    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            string FunkcioKod = null;
            if (Startup == Constants.Startup.FromKuldemeny)
            {
                FunkcioKod = FunkcioKod_KuldemenyCsatolasNew;
            }
            else
            {
                FunkcioKod = FunkcioKod_UgyiratCsatolasNew;
            }


            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod))
            {
                if (String.IsNullOrEmpty(ObjektumId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    // Csatol�s

                    EREC_UgyiratObjKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    EREC_UgyiratObjKapcsolatok objKapcsolatok = GetBusinessObjectFromComponents();

                    Result result = service.Insert(execParam, objKapcsolatok);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, ObjektumId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }

                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }


    
}
