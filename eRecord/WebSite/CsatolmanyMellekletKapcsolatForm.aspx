<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="CsatolmanyMellekletKapcsolatForm.aspx.cs" Inherits="CsatolmanyMellekletKapcsolatForm"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%@ register src="Component/FormHeader.ascx" tagname="FormHeader" tagprefix="uc1" %>
    <%@ register src="Component/FormFooter.ascx" tagname="FormFooter" tagprefix="uc2" %>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <asp:Panel ID="MainPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                <eUI:eFormPanel ID="Melleklet_EFormPanel" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">                                                
                                                <asp:Label ID="Label1" runat="server" Text="Mell�kletek (adathordoz�) t�pusa:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Melleklet_AdathordozoJellege_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                            </td>                                      
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label2" runat="server" Text="Mennyis�g:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Melleklet_Mennyiseg_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label6" runat="server" Text="Megjegyz�s:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Melleklet_Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox></td>
                                                                                  
                                            <td class="mrUrlapCaption">                                                
                                                <asp:Label ID="Label3" runat="server" Text="Vonalk�d:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Melleklet_BarCode_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </eUI:eFormPanel>
                                <eUI:eFormPanel ID="Csatolmany_EFormPanel" runat="server" Visible="false">
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label4" runat="server" Text="�llom�ny:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Csatolmany_FajlNev_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                            </td>
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label5" runat="server" Text="Form�tum:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Csatolmany_Formatum_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label7" runat="server" Text="Megjegyz�s:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Csatolmany_Leiras_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox></td>
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label8" runat="server" Text="Verzi�:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Csatolmany_VerzioJel_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </eUI:eFormPanel>
                                <asp:Panel ID="CsatolmanyokListPanel" runat="server" Visible="false">
                                    <br />
                                    <asp:Label ID="Label_Csatolmanyok" runat="server" Text="Csatolm�nyok kiv�laszt�sa:"
                                        CssClass="GridViewTitle"></asp:Label>
                                    <br />
                                    <asp:GridView ID="CsatolmanyokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                        AllowSorting="False" DataKeyNames="Id" AutoGenerateColumns="False" Width="100%"
                                        OnRowDataBound="CsatolmanyokGridView_RowDataBound">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                    &nbsp;&nbsp;
                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                        CssClass="HideCheckBoxText" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FajlNev" HeaderText="�llom�ny" SortExpression="FajlNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Leiras" HeaderText="Megjegyz�s" SortExpression="Leiras">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="VerzioJel" HeaderText="Verzi�" SortExpression="VerzioJel">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tipus" HeaderText="T�pus" SortExpression="Tipus">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Formatum" HeaderText="Form�tum" SortExpression="Formatum">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ElektronikusAlairasNev" HeaderText="Elektronikus al��r�s"
                                                SortExpression="ElektronikusAlairasNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </asp:Panel>
                                <asp:Panel ID="MellekletekListPanel" runat="server" Visible="false">
                                    <br />
                                    <asp:Label ID="Label9" runat="server" Text="Mell�kletek kiv�laszt�sa:" CssClass="GridViewTitle"></asp:Label>
                                    <br />
                                    <asp:GridView ID="MellekletekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                        AllowSorting="False" DataKeyNames="Id" AutoGenerateColumns="False" Width="100%"
                                        OnRowDataBound="MellekletekGridView_RowDataBound">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                    &nbsp;&nbsp;
                                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Id") %>'
                                                        CssClass="HideCheckBoxText" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AdathordozoTipusNev" HeaderText="Mell�kletek (adathordoz�) t�pusa"
                                                SortExpression="AdathordozoTipusNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Mennyiseg" HeaderText="Mennyis�g" SortExpression="Mennyiseg">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MennyisegiEgysegNev" HeaderText="Egys�g" SortExpression="MennyisegiEgysegNev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyz�s" SortExpression="Megjegyzes">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="BarCode" HeaderText="Vonalk�d" SortExpression="BarCode">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBorder" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
