using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;

public partial class CsatolmanyMellekletKapcsolatForm : System.Web.UI.Page
{
    private string Command = "";

    private string CsatolmanyId = String.Empty;
    private string MellekletId = String.Empty;
    private string IratId = String.Empty;
    private string KuldemenyId = String.Empty;
    private UI ui = new UI();

    private const string KodCsoportAdathordozoTipus = "ADATHORDOZO_TIPUSA";
    private const string KodCsoportMennyisegiEgyseg = "MENNYISEGI_EGYSEG";
    private const string Funkcio_CsatolmanyMellekletKapcsolatNew = "CsatolmanyMellekletKapcsolatNew";

    private KapcsolatMod Mode = KapcsolatMod.CsatolmanyokKoteseMelleklethez;

    enum KapcsolatMod
    {
        MellekletekKoteseCsatolmanyhoz
       , CsatolmanyokKoteseMelleklethez
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(QueryStringVars.Command);

        MellekletId = Request.QueryString.Get(QueryStringVars.MellekletId);
        CsatolmanyId = Request.QueryString.Get(QueryStringVars.CsatolmanyId);
        IratId = Request.QueryString.Get(QueryStringVars.IratId);
        KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);

        if (!string.IsNullOrEmpty(MellekletId))
        {
            Mode = KapcsolatMod.CsatolmanyokKoteseMelleklethez;
        }
        else if (!string.IsNullOrEmpty(CsatolmanyId))
        {
            Mode = KapcsolatMod.MellekletekKoteseCsatolmanyhoz;
        }
        else
        {
            // nincs megadva valamilyen param�ter
            MainPanel.Visible = false;
        }


        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        // TODO: Jogosults�gellen�rz�s:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, Funkcio_CsatolmanyMellekletKapcsolatNew);


        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
          System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        // C�m be�ll�t�sa:
        if (Mode == KapcsolatMod.CsatolmanyokKoteseMelleklethez)
        {
            FormHeader1.HeaderTitle = Resources.Form.CsatolmanyokKoteseMelleklethezFormHeaderTitle;
        }
        else if (Mode == KapcsolatMod.MellekletekKoteseCsatolmanyhoz)
        {
            FormHeader1.HeaderTitle = Resources.Form.MellekletekKoteseCsatolmanyhozFormHeaderTitle;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ui.SetClientScriptToGridViewSelectDeSelectButton(CsatolmanyokGridView);
    }

    private void LoadFormComponents()
    {
        // Mell�klet vagy csatolm�ny panel felt�lt�se:
        if (!string.IsNullOrEmpty(MellekletId))
        {
            Melleklet_EFormPanel.Visible = true;
            FillMellekletPanel();
        }
        else if (!string.IsNullOrEmpty(CsatolmanyId))
        {
            Csatolmany_EFormPanel.Visible = true;
            FillCsatolmanyPanel();
        }
        else
        {
            MainPanel.Visible = false;
            return;
        }

        if (Mode == KapcsolatMod.CsatolmanyokKoteseMelleklethez)
        {
            FillCsatolmanyokGrid();

            CsatolmanyokListPanel.Visible = true;
        }
        else if (Mode == KapcsolatMod.MellekletekKoteseCsatolmanyhoz)
        {
            FillMellekletekGridView();

            MellekletekListPanel.Visible = true;
        }
    }


    #region seg�dfv. sorok letilt�s�hoz

    const string disabledRowStyle = "GridViewLovListDisabledRowStyle";

    protected void DisableRowInList(GridViewRowEventArgs e, List<string> disabledIdList, string checkBoxServerId)
    {
        if (disabledIdList != null && disabledIdList.Count > 0)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

                string row_Id = "";
                if (drv != null && drv["Id"] != null)
                {
                    row_Id = drv["Id"].ToString();
                }

                if (disabledIdList.Contains(row_Id))
                {
                    e.Row.CssClass = disabledRowStyle;

                    // CheckBox lek�r�se:
                    CheckBox checkBox = (CheckBox)e.Row.FindControl(checkBoxServerId);
                    if (checkBox != null)
                    {
                        checkBox.Checked = false;
                        checkBox.Enabled = false;
                    }
                }
            }
        }
    }

    #endregion

    #region CsatolmanyokGridView

    // azok a csatolm�nyok, amik m�r hozz� vannak rendelve a mell�klethez (majd le kell azokat a sorokat tiltani)
    private List<string> letiltandoCsatolmanyokListaja = null;


    private void FillCsatolmanyokGrid()
    {
        if (!string.IsNullOrEmpty(IratId) || !string.IsNullOrEmpty(KuldemenyId))
        {
            #region Letiltand� csatolm�nyok list�j�nak �ssze�ll�t�sa

            EREC_IratelemKapcsolatokService service_iratelemKapcs = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();

            EREC_IratelemKapcsolatokSearch search_iratelemKapcs = new EREC_IratelemKapcsolatokSearch();
            if (!String.IsNullOrEmpty(MellekletId))
            {
                search_iratelemKapcs.Melleklet_Id.Value = MellekletId;
                search_iratelemKapcs.Melleklet_Id.Operator = Query.Operators.equals;
            }
            else
            {
                // valami nem ok:
                Logger.Error("Nincs megadva a MellekletId!");
                return;
            }

            Result result_iratelemKapcsGetAll = service_iratelemKapcs.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), search_iratelemKapcs);
            if (result_iratelemKapcsGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_iratelemKapcsGetAll);
                return;
            }

            // Lista felt�lt�se:
            letiltandoCsatolmanyokListaja = new List<string>();
            foreach (DataRow row in result_iratelemKapcsGetAll.Ds.Tables[0].Rows)
            {
                string csatolmanyId = row["Csatolmany_Id"].ToString();
                if (!letiltandoCsatolmanyokListaja.Contains(csatolmanyId))
                {
                    letiltandoCsatolmanyokListaja.Add(csatolmanyId);
                }
            }

            #endregion

            EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

            // ha az iratId meg van adva, arra sz�r�nk, egy�bk�nt a kuldemenyId-ra:
            if (!String.IsNullOrEmpty(IratId))
            {
                search.IraIrat_Id.Value = IratId;
                search.IraIrat_Id.Operator = Query.Operators.equals;
            }
            else
            {
                search.KuldKuldemeny_Id.Value = KuldemenyId;
                search.KuldKuldemeny_Id.Operator = Query.Operators.equals;
            }

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAllWithExtension(execParam, search);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            ui.GridViewFill(CsatolmanyokGridView, result, FormHeader1.ErrorPanel, null);
        }
    }

    protected void CsatolmanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DisableRowInList(e, letiltandoCsatolmanyokListaja, "check");
    }


    #endregion


    #region MellekletekGridView

    // azok a mell�kletek, amik m�r hozz� vannak rendelve a csatolm�nyhoz (majd le kell azokat a sorokat tiltani)
    private List<string> letiltandoMellekletekListaja = null;

    private void FillMellekletekGridView()
    {
        if (!string.IsNullOrEmpty(IratId) || !string.IsNullOrEmpty(KuldemenyId))
        {
            #region Letiltand� mell�kletek list�j�nak �ssze�ll�t�sa

            EREC_IratelemKapcsolatokService service_iratelemKapcs = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();

            EREC_IratelemKapcsolatokSearch search_iratelemKapcs = new EREC_IratelemKapcsolatokSearch();
            if (!String.IsNullOrEmpty(CsatolmanyId))
            {
                search_iratelemKapcs.Csatolmany_Id.Value = CsatolmanyId;
                search_iratelemKapcs.Csatolmany_Id.Operator = Query.Operators.equals;
            }
            else
            {
                // valami nem ok:
                Logger.Error("Nincs megadva a Csatolmany_Id!");
                return;
            }

            Result result_iratelemKapcsGetAll = service_iratelemKapcs.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), search_iratelemKapcs);
            if (result_iratelemKapcsGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_iratelemKapcsGetAll);
                return;
            }

            // Lista felt�lt�se:
            letiltandoMellekletekListaja = new List<string>();
            foreach (DataRow row in result_iratelemKapcsGetAll.Ds.Tables[0].Rows)
            {
                string row_mellekletId = row["Melleklet_Id"].ToString();
                if (!letiltandoMellekletekListaja.Contains(row_mellekletId))
                {
                    letiltandoMellekletekListaja.Add(row_mellekletId);
                }
            }

            #endregion

            EREC_MellekletekService service = eRecordService.ServiceFactory.GetEREC_MellekletekService();

            EREC_MellekletekSearch search = new EREC_MellekletekSearch();

            // ha az iratId meg van adva, arra sz�r�nk, egy�bk�nt a kuldemenyId-ra:
            if (!String.IsNullOrEmpty(IratId))
            {
                search.IraIrat_Id.Value = IratId;
                search.IraIrat_Id.Operator = Query.Operators.equals;
            }
            else
            {
                search.KuldKuldemeny_Id.Value = KuldemenyId;
                search.KuldKuldemeny_Id.Operator = Query.Operators.equals;
            }

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service.GetAllWithExtension(execParam, search);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            ui.GridViewFill(MellekletekGridView, result, FormHeader1.ErrorPanel, null);
        }

    }

    protected void MellekletekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DisableRowInList(e, letiltandoMellekletekListaja, "check");
    }


    #endregion

    /// <summary>
    /// Mell�klet panel felt�lt�se
    /// </summary> 
    private void FillMellekletPanel()
    {
        if (!String.IsNullOrEmpty(MellekletId))
        {
            // Melleklet lek�r�se:
            EREC_MellekletekService service_Mellekletek = eRecordService.ServiceFactory.GetEREC_MellekletekService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = MellekletId;

            Result result = service_Mellekletek.Get(execParam);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            EREC_Mellekletek melleklet = (EREC_Mellekletek)result.Record;

            string kodtarErtek_AdathordozoTipus =
                Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodCsoportAdathordozoTipus, melleklet.AdathordozoTipus, Page);
            Melleklet_AdathordozoJellege_TextBox.Text = kodtarErtek_AdathordozoTipus;

            string kodtarErtek_MennyisegiEgyseg =
                Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodCsoportMennyisegiEgyseg, melleklet.MennyisegiEgyseg, Page);

            Melleklet_Mennyiseg_TextBox.Text = melleklet.Mennyiseg + " " + kodtarErtek_MennyisegiEgyseg;

            Melleklet_Megjegyzes_TextBox.Text = melleklet.Megjegyzes;

            Melleklet_BarCode_TextBox.Text = melleklet.BarCode;
        }
    }


    /// <summary>
    /// Csatolm�ny panel felt�lt�se
    /// </summary>
    private void FillCsatolmanyPanel()
    {
        if (!String.IsNullOrEmpty(CsatolmanyId))
        {
            // Csatolm�ny adatok lek�r�se (GetAllWithExtension-nel)
            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();
            search.Id.Value = CsatolmanyId;
            search.Id.Operator = Query.Operators.equals;

            Result result = service_csatolmanyok.GetAllWithExtension(UI.SetExecParamDefault(Page, new ExecParam()), search);
            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            // Egy tal�latnak kell lennie:
            if (result.Ds.Tables[0].Rows.Count != 1)
            {
                // hiba: rekord lek�r�se sikertelen
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_50101);
                return;
            }

            DataRow row = result.Ds.Tables[0].Rows[0];

            Csatolmany_FajlNev_TextBox.Text = row["FajlNev"].ToString();
            Csatolmany_Formatum_TextBox.Text = row["Formatum"].ToString();
            Csatolmany_Leiras_TextBox.Text = row["Leiras"].ToString();
            Csatolmany_VerzioJel_TextBox.Text = row["VerzioJel"].ToString();
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //Jogosults�gellen�rz�s:
            if (FunctionRights.GetFunkcioJog(Page, Funkcio_CsatolmanyMellekletKapcsolatNew))
            {

                if (Mode == KapcsolatMod.CsatolmanyokKoteseMelleklethez)
                {
                    List<string> selectedItemsList_Csatolmanyok = ui.GetGridViewSelectedRows(CsatolmanyokGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList_Csatolmanyok.Count == 0)
                    {
                        // Nincs kiv�lasztva elem:
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    string[] csatolmanyokIds = new String[selectedItemsList_Csatolmanyok.Count];
                    for (int i = 0; i < csatolmanyokIds.Length; i++)
                    {
                        csatolmanyokIds[i] = selectedItemsList_Csatolmanyok[i];
                    }

                    EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();

                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    Result result = service.CsatolmanyokKoteseMelleklethez(execParam, MellekletId, csatolmanyokIds);

                    if (result.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }
                    else
                    {
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }


                }
                else if (Mode == KapcsolatMod.MellekletekKoteseCsatolmanyhoz)
                {
                    List<string> selectedItemsList_Mellekletek = ui.GetGridViewSelectedRows(MellekletekGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList_Mellekletek.Count == 0)
                    {
                        // Nincs kiv�lasztva elem:
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    string[] mellekletekIds = new String[selectedItemsList_Mellekletek.Count];
                    for (int i = 0; i < mellekletekIds.Length; i++)
                    {
                        mellekletekIds[i] = selectedItemsList_Mellekletek[i];
                    }

                    EREC_IratelemKapcsolatokService service = eRecordService.ServiceFactory.GetEREC_IratelemKapcsolatokService();

                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    Result result = service.MellekletekKoteseCsatolmanyhoz(execParam, CsatolmanyId, mellekletekIds);

                    if (result.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }
                    else
                    {
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }


                }



            }

        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }


    }

}
