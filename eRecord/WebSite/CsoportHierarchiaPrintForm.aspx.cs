﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eQuery.BusinessDocuments;

public partial class CsoportHierarchiaPrintForm : System.Web.UI.Page
{
    private bool printMode = false;
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        printMode = Request.QueryString.Get("PrintMode") == "1";
        if (!printMode)
        {
            this.MasterPageFile = "~/MasterPage.master";
        }
        else
        {
            string js = "window.print();";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Print", js, true);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!printMode)
        {
            ButtonNyomtatas.Visible = true;
        }

        string js = "popup('CsoportHierarchiaPrintForm.aspx?PrintMode=1";
        if (!string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.Id)))
            js += "&" + QueryStringVars.Id + "=" + Request.QueryString.Get(QueryStringVars.Id);

        js += "'," + Defaults.PopupWidth + "," + Defaults.PopupHeight + ");";

        ButtonNyomtatas.Attributes.Add("onclick", js);


        this.LoadTreeView(Request.QueryString.Get(QueryStringVars.Id));
    }

    private void LoadTreeView(string SelectedId)
    {
        KRT_CsoportTagokService service = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();

        Result result = service.GetAll_ForTree(UI.SetExecParamDefault(Page), SelectedId);

        if (result.IsError)
        {
            Response.Write("<font style='color: #FC5050; font-size:25px; font-weight:bold; font-style:italic;'> Hiba lépett fel a lekérdezés közben! Hibaüzenet: " + ResultError.GetErrorMessageFromResultObject(result) + "</font>");
            return;
        }
        
        ArrayList nodeList = new ArrayList();

        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
        {
            if (string.IsNullOrEmpty(result.Ds.Tables[0].Rows[i]["Csoport_Id"].ToString()))
            {
                TreeNode tn = this.GetDefaultTreeNode(result.Ds.Tables[0].Rows[i]["Csoport_Jogalany_Nev"].ToString(),
                    result.Ds.Tables[0].Rows[i]["Csoport_Id_Jogalany"].ToString(),
                    result.Ds.Tables[0].Rows[i]["Csoport_Jogalany_Tipus"].ToString());
                CsoportHierarchiTreeView.Nodes.Add(tn);

                if (!string.IsNullOrEmpty(SelectedId) && SelectedId.ToUpper() == tn.Value.ToUpper())
                    tn.Selected = true;

                nodeList.Add(tn);

                continue;
            }
        }

        while (nodeList.Count != 0)
        {
            TreeNode tn = (TreeNode)nodeList[0];

            nodeList.RemoveAt(0);

            for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
            {
                if (result.Ds.Tables[0].Rows[i]["Csoport_Id"].ToString() == tn.Value)
                {
                    TreeNode cn = this.GetDefaultTreeNode(result.Ds.Tables[0].Rows[i]["Csoport_Jogalany_Nev"].ToString(),
                        result.Ds.Tables[0].Rows[i]["Csoport_Id_Jogalany"].ToString(),
                        result.Ds.Tables[0].Rows[i]["Csoport_Jogalany_Tipus"].ToString());
                    tn.ChildNodes.Add(cn);

                    if (!string.IsNullOrEmpty(SelectedId) && cn.Value.ToUpper() == SelectedId.ToUpper())
                        cn.Text = "<b>" + cn.Text + "</b>";

                    nodeList.Add(cn);
                }
            }
        }

        CsoportHierarchiTreeView.ExpandAll();

    }

    private TreeNode GetDefaultTreeNode(string Text, string Id, string Tipus)
    {
        TreeNode tn = new TreeNode(Text, Id, "", "javascript:void(0);", "");

        switch (Tipus)
        {
            case KodTarak.CSOPORTTIPUS.Szervezet:
            default:
                tn.ImageUrl = "images/hu/egyeb/szervezet.gif";
            break;
            case KodTarak.CSOPORTTIPUS.HivatalVezetes:
                tn.ImageUrl = "images/hu/egyeb/hivatal_vezeto.gif";
            break;
            case KodTarak.CSOPORTTIPUS.Projekt:
                tn.ImageUrl = "images/hu/egyeb/projekt.gif";
            break;
            case KodTarak.CSOPORTTIPUS.Testulet_Bizottsag:
                tn.ImageUrl = "images/hu/egyeb/bizottsag.gif";
            break;
            case KodTarak.CSOPORTTIPUS.Dolgozo:
            tn.ImageUrl = "images/hu/egyeb/dolgozo.gif";
            break;
        }

        return tn;
    }

}
