<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CsoportokList.aspx.cs" Inherits="CsoportokList" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
<%--Hiba megjelenites--%>
<eUI:eErrorPanel id="EErrorPanel1" runat="server">
    </eUI:eErrorPanel>
</contenttemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--/Hiba megjelenites--%>

<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
   
    
<%--Tablazat / Grid--%> 
 <table width="100%" cellpadding="0" cellspacing="0">   
    <tr>
    <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="CsoportokCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
    <td style="text-align: left; vertical-align: top; width: 100%;">                           
  
        <asp:UpdatePanel ID="CsoportokUpdatePanel" runat="server" OnLoad="CsoportokUpdatePanel_Load" >
            <ContentTemplate>            
            <ajaxToolkit:CollapsiblePanelExtender ID="CsoportokCPE" runat="server" TargetControlID="Panel1"
            CollapsedSize="20" Collapsed="False" ExpandControlID="CsoportokCPEButton" CollapseControlID="CsoportokCPEButton" ExpandDirection="Vertical"
            AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="CsoportokCPEButton"             
             ExpandedSize="0" ExpandedText="Csoportok list�ja" CollapsedText="Csoportok list�ja" 
              >            
            </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="Panel1" runat="server">
            <table style="width: 98%;" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">            
                 <asp:GridView ID="CsoportokGridView" runat="server" OnRowCommand="CsoportokGridView_RowCommand"
                                    CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" AllowPaging="True"
                                    PagerSettings-Visible="false" AllowSorting="True" OnPreRender="CsoportokGridView_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="CsoportokGridView_Sorting"
                                    OnRowDataBound="CsoportokGridView_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_Csoportok.Nev" HeaderStyle-Width="200px" HeaderStyle-CssClass="GridViewBorderHeader"/>
                                        <%--BLG_619--%>
                                         <asp:BoundField DataField="Kod" HeaderText="Szk" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_Csoportok.Kod" HeaderStyle-Width="20px" HeaderStyle-CssClass="GridViewBorderHeader" Visible="False" />
                                        <asp:BoundField DataField="Csoport_Tipus" HeaderText="T�pus" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="KRT_KodTarak.Nev" HeaderStyle-Width="150px" />
                                        <asp:TemplateField>
                                                <HeaderStyle  CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="labelRendszer" runat="server" Text="Rendszer" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbSystem" runat="server" Enabled="false"/>
                                                </ItemTemplate>
                                        </asp:TemplateField>    
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>               
        </td></tr></table>        
                 </asp:Panel>    
        </ContentTemplate>    
        </asp:UpdatePanel>        
    </td>                            
    </tr>

    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
    <tr>    
    <td style="text-align: left; vertical-align: top; width: 0px;">
    <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
    </td>
    <td style="text-align: left; vertical-align: top; width: 100%;">                           
    <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">                               

            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel8"
            CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton" CollapseControlID="DetailCPEButton" ExpandDirection="Vertical"
            AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton"
            >
            
            </ajaxToolkit:CollapsiblePanelExtender>
            
                <asp:Panel ID="Panel8" runat="server">

                
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" 
                        OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        
                        <ajaxToolkit:TabPanel ID="CsoporttagokTabPanel" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelCsoportTagok" runat="server">
                                    <ContentTemplate>                                   
                                        <asp:Label ID="Header1" runat="server" Text="Csoporttagok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                          
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="CsoporttagokUpdatePanel" runat="server" OnLoad="CsoporttagokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="CsoporttagokPanel" runat="server" Visible="false" Width="100%">

                                            <uc1:SubListHeader ID="CsoporttagokSubListHeader" runat="server" />

                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CsoporttagokCPE" runat="server" TargetControlID="Panel2"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0"
                                              >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="Panel2" runat="server">

                                        <asp:GridView ID="CsoporttagokGridView" runat="server"
                                             CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                             AutoGenerateColumns="false" OnSorting="CsoporttagokGridView_Sorting" OnPreRender="CsoporttagokGridView_PreRender" OnRowCommand="CsoporttagokGridView_RowCommand" DataKeyNames="Id" >        
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>  
                                                <asp:BoundField DataField="Csoport_Nev" HeaderText="Csoporttag neve" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Csoport_Nev" HeaderStyle-Width="200px" /> 
                                                <asp:BoundField DataField="CsoportTag_Tipus" HeaderText="Kapcsolat t�pusa" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Csoport_Tipus" HeaderStyle-Width="150px" /> 
                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s ideje" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />             
                                                              
                                                </Columns>                
                                            </asp:GridView> 
                                            </asp:Panel>

                                                                                       
                                            </td></tr></table>
                                            
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                              
                
               <ajaxToolkit:TabPanel ID="FelhasznaloSzerepkorTabPanel" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltSzerepkorok" runat="server">
                                    <ContentTemplate>                                
                                        <asp:Label ID="Label1" runat="server" Text="Felhaszn�l� - szerepk�r hozz�rendel�sek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                           
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="FelhasznaloSzerepkorUpdatePanel" runat="server" OnLoad="FelhasznaloSzerepkorUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="FelhasznaloSzerepkorPanel" runat="server" Visible="false" Width="100%">

                                            <uc1:SubListHeader ID="FelhasznaloSzerepkorSubListHeader" runat="server" />

                                            <table style="height: 200px; width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">

                                        <ajaxToolkit:CollapsiblePanelExtender ID="FelhasznaloSzerepkorCPE" runat="server" TargetControlID="Panel3"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0"
                                              >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="Panel3" runat="server">

                                        <asp:GridView ID="FelhasznaloSzerepkorGridView" runat="server"
                                             CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                             AutoGenerateColumns="false" OnSorting="FelhasznaloSzerepkorGridView_Sorting" OnPreRender="FelhasznaloSzerepkorGridView_PreRender" OnRowCommand="FelhasznaloSzerepkorGridView_RowCommand" DataKeyNames="Id" >        
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>                                                  
                                                <asp:BoundField DataField="Felhasznalo_Nev" HeaderText="Felhaszn�l� neve" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Felhasznalo_Nev" HeaderStyle-Width="200px" /> 
                                                <asp:BoundField DataField="Szerepkor_Nev" HeaderText="Szerepk�r neve" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Szerepkor_Nev" HeaderStyle-Width="200px" />
                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s ideje" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />             
                                                <asp:TemplateField HeaderText="Megb�z�sban kapott" SortExpression="Helyettesites_Id">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbMegbizasbanKapott" runat="server" Checked='<%# string.IsNullOrEmpty(Eval("Helyettesites_Id").ToString()) ? false : true  %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                </Columns>                
                                            </asp:GridView> 

                                            </asp:Panel>
                                            
                                            </td></tr></table>
                                            
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        </ajaxToolkit:TabContainer>                         
                
                
            </asp:Panel>     
                
                </td>
            </tr>
        </table>
        
        
                </td>
            </tr>
        </table>
        <br />    

</asp:Content>

