<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="CsoportokLovList.aspx.cs" Inherits="CsoportokLovList" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" ScriptMode="Release"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,CsoportokLovListHeaderTitle%>" />
    &nbsp;<br />    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                            <br />                    
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td>
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">                                                
                                        <asp:Label ID="Label1" runat="server" Text="Csoport megnevez�s:" Font-Bold="True"></asp:Label><br />
                                        <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%"></asp:TextBox>&nbsp;
                                        <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"></asp:ImageButton>
                                        <asp:ImageButton id="ButtonAdvancedSearch" runat="server" AlternateText="R�szletes keres�s" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"></asp:ImageButton> 
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">                    
                                        <div class="listaFulFelsoCsikKicsi"><img src="images/hu/design/spacertrans.gif" alt="" /></div>                            
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="20" Width="95%">
                                        </asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:ImageButton id="ImageButtonReszletesAdatok" runat="server" 
                                        AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" 
                                        ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                        onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')">
                                        </asp:ImageButton>
                                    </td>
                                </tr>
                            </table>  
                          </eUI:eFormPanel>     
                        </ContentTemplate>
                    </asp:UpdatePanel>
                
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />

</asp:Content>

