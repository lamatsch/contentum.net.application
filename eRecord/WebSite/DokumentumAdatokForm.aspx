<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DokumentumAdatokForm.aspx.cs" Inherits="DokumentumAdatokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    <Scripts>
        <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
    </Scripts>
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKapcsolatiKod" runat="server" Text="Kapcsolati k�d:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="KapcsolatiKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelNev" runat="server" Text="Felad� neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Nev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelEmail" runat="server" Text="Felad� e-mail c�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Email" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelRovidNev" runat="server" Text="Hivatal r�vid neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="RovidNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelMAKKod" runat="server" Text="Hivatal M�K k�dja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="MAKKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKRID" runat="server" Text="Hivatal KRID-je:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="KRID" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelErkeztetesiSzam" runat="server" Text="�rkeztet�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="ErkeztetesiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelDokTipusHivatal" runat="server" Text="Hivatal:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="DokTipusHivatal" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelDokTipusAzonosito" runat="server" Text="T�pus:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="DokTipusAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelDokTipusLeiras" runat="server" Text="T�pus n�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="DokTipusLeiras" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelHivatkozasiSzam" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="HivatkozasiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Megjegyzes" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelFileNev" runat="server" Text="F�jln�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="FileNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelErvenyessegiDatum" runat="server" Text="�rv�nyess�gi d�tum:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="ErvenyessegiDatum" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelErkeztetesiDatum" runat="server" Text="�rkeztet�si d�tum:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="ErkeztetesiDatum" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKezbesitettseg" runat="server" Text="K�zbes�tetts�g:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="Kezbesitettseg" runat="server" />
                            </td>
                        </tr>
<%--                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelIdopecset" runat="server" Text="Id�pecs�t:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Idopecset" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelValaszTitkositas" runat="server" Text="V�lasz titkos�t�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="ValaszTitkositas" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelValaszUtvonal" runat="server" Text="V�lasz �tvonal:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="ValaszUtvonal" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelRendszeruzenet" runat="server" Text="Rendszer�zenet:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Rendszeruzenet" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelTarterulet" runat="server" Text="T�rter�let:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="Tarterulet" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelETertiveveny" runat="server" Text="ET�rtivev�ny:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="ETertiveveny" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
