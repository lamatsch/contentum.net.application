using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class DokumentumAdatokForm : Contentum.eUtility.UI.PageBase
{
    private const string kcs_Kezbesittettseg = "HKP_KEZBESITETTSEG";
    private const string kcs_Valaszutvonal = "HKP_VALASZUTVONAL";
    private const string kcs_Tarterulet = "HKP_TARTERULET";
    private const string kcs_Allapot = "HKP_ALLAPOT";

    private string Command = "";

    private void SetViewControls()
    {
        KapcsolatiKod.ReadOnly = true;
        Nev.ReadOnly = true;
        Email.ReadOnly = true;
        RovidNev.ReadOnly = true;
        MAKKod.ReadOnly = true;
        KRID.ReadOnly = true;
        ErkeztetesiSzam.ReadOnly = true;
        DokTipusHivatal.ReadOnly = true;
        DokTipusAzonosito.ReadOnly = true;
        DokTipusLeiras.ReadOnly = true;
        HivatkozasiSzam.ReadOnly = true;
        Megjegyzes.ReadOnly = true;
        FileNev.ReadOnly = true;
        ErvenyessegiDatum.ReadOnly = true;
        ErkeztetesiDatum.ReadOnly = true;
        Kezbesitettseg.ReadOnly = true;
        ValaszTitkositas.ReadOnly = true;
        ValaszUtvonal.ReadOnly = true;
        Rendszeruzenet.ReadOnly = true;
        Tarterulet.ReadOnly = true;
        ETertiveveny.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "DokumentumAdatok" + Command);

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                HKP_DokumentumAdatokService service = eRecordService.ServiceFactory.GetHKP_DokumentumAdatokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    HKP_DokumentumAdatok hkp_DokumentumAdatok = (HKP_DokumentumAdatok)result.Record;
                    LoadComponentsFromBusinessObject(hkp_DokumentumAdatok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Hivatali kapu �zenet";

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(HKP_DokumentumAdatok hkp_DokumentumAdatok)
    {
        KapcsolatiKod.Text = hkp_DokumentumAdatok.KapcsolatiKod;
        Nev.Text = hkp_DokumentumAdatok.Nev;
        Email.Text = hkp_DokumentumAdatok.Email;
        RovidNev.Text = hkp_DokumentumAdatok.RovidNev;
        MAKKod.Text = hkp_DokumentumAdatok.MAKKod;
        KRID.Text = hkp_DokumentumAdatok.KRID;
        ErkeztetesiSzam.Text = hkp_DokumentumAdatok.ErkeztetesiSzam;
        DokTipusHivatal.Text = hkp_DokumentumAdatok.DokTipusHivatal;
        DokTipusAzonosito.Text = hkp_DokumentumAdatok.DokTipusAzonosito;
        DokTipusLeiras.Text = hkp_DokumentumAdatok.DokTipusLeiras;
        HivatkozasiSzam.Text = hkp_DokumentumAdatok.HivatkozasiSzam;
        Megjegyzes.Text = hkp_DokumentumAdatok.Megjegyzes;
        FileNev.Text = hkp_DokumentumAdatok.FileNev;
        ErvenyessegiDatum.Text = hkp_DokumentumAdatok.ErvenyessegiDatum;
        ErkeztetesiDatum.Text = hkp_DokumentumAdatok.ErkeztetesiDatum;
        Kezbesitettseg.FillAndSetSelectedValue(kcs_Kezbesittettseg, hkp_DokumentumAdatok.Kezbesitettseg, FormHeader1.ErrorPanel);
        ValaszTitkositas.Text = DisplayBoolean(hkp_DokumentumAdatok.ValaszTitkositas);
        ValaszUtvonal.FillAndSetSelectedValue(kcs_Valaszutvonal, hkp_DokumentumAdatok.ValaszUtvonal, FormHeader1.ErrorPanel);
        Rendszeruzenet.Text = DisplayBoolean(hkp_DokumentumAdatok.Rendszeruzenet);
        Tarterulet.FillAndSetSelectedValue(kcs_Tarterulet, hkp_DokumentumAdatok.Tarterulet, FormHeader1.ErrorPanel);
        ETertiveveny.Text = DisplayBoolean(hkp_DokumentumAdatok.ETertiveveny);
 
        FormHeader1.Record_Ver = hkp_DokumentumAdatok.Base.Ver;
    }

    // form --> business object
    private HKP_DokumentumAdatok GetBusinessObjectFromComponents()
    {
        HKP_DokumentumAdatok hkp_DokumentumAdatok = new HKP_DokumentumAdatok();
        hkp_DokumentumAdatok.Updated.SetValueAll(false);
        hkp_DokumentumAdatok.Base.Updated.SetValueAll(false);

        hkp_DokumentumAdatok.KapcsolatiKod = KapcsolatiKod.Text;
        hkp_DokumentumAdatok.Updated.KapcsolatiKod = true;
        hkp_DokumentumAdatok.Nev = Nev.Text;
        hkp_DokumentumAdatok.Updated.Nev = true;
        hkp_DokumentumAdatok.Email = Email.Text;
        hkp_DokumentumAdatok.Updated.Email = true;
        hkp_DokumentumAdatok.RovidNev = RovidNev.Text;
        hkp_DokumentumAdatok.Updated.RovidNev = true;
        hkp_DokumentumAdatok.MAKKod = MAKKod.Text;
        hkp_DokumentumAdatok.Updated.MAKKod = true;
        hkp_DokumentumAdatok.KRID = KRID.Text;
        hkp_DokumentumAdatok.Updated.KRID = true;
        hkp_DokumentumAdatok.ErkeztetesiSzam = ErkeztetesiSzam.Text;
        hkp_DokumentumAdatok.Updated.ErkeztetesiSzam = true;
        hkp_DokumentumAdatok.DokTipusHivatal = DokTipusHivatal.Text;
        hkp_DokumentumAdatok.Updated.DokTipusHivatal = true;
        hkp_DokumentumAdatok.DokTipusAzonosito = DokTipusAzonosito.Text;
        hkp_DokumentumAdatok.Updated.DokTipusAzonosito = true;
        hkp_DokumentumAdatok.DokTipusLeiras = DokTipusLeiras.Text;
        hkp_DokumentumAdatok.Updated.DokTipusLeiras = true;
        hkp_DokumentumAdatok.HivatkozasiSzam = HivatkozasiSzam.Text;
        hkp_DokumentumAdatok.Updated.HivatkozasiSzam = true;
        hkp_DokumentumAdatok.Megjegyzes = Megjegyzes.Text;
        hkp_DokumentumAdatok.Updated.Megjegyzes = true;
        hkp_DokumentumAdatok.FileNev = FileNev.Text;
        hkp_DokumentumAdatok.Updated.FileNev = true;
        hkp_DokumentumAdatok.ErvenyessegiDatum = ErvenyessegiDatum.Text;
        hkp_DokumentumAdatok.Updated.ErvenyessegiDatum = true;
        hkp_DokumentumAdatok.ErkeztetesiDatum = ErkeztetesiDatum.Text;
        hkp_DokumentumAdatok.Updated.ErkeztetesiDatum = true;
        hkp_DokumentumAdatok.Kezbesitettseg = Kezbesitettseg.Text;
        hkp_DokumentumAdatok.Updated.Kezbesitettseg = true;
        hkp_DokumentumAdatok.ValaszTitkositas = ValaszTitkositas.Text;
        hkp_DokumentumAdatok.Updated.ValaszTitkositas = true;
        hkp_DokumentumAdatok.ValaszUtvonal = ValaszUtvonal.Text;
        hkp_DokumentumAdatok.Updated.ValaszUtvonal = true;
        hkp_DokumentumAdatok.Rendszeruzenet = Rendszeruzenet.Text;
        hkp_DokumentumAdatok.Updated.Rendszeruzenet = true;
        hkp_DokumentumAdatok.Tarterulet = Tarterulet.Text;
        hkp_DokumentumAdatok.Updated.Tarterulet = true;
        hkp_DokumentumAdatok.ETertiveveny = ETertiveveny.Text;
        hkp_DokumentumAdatok.Updated.ETertiveveny = true;


        hkp_DokumentumAdatok.Base.Ver = FormHeader1.Record_Ver;
        hkp_DokumentumAdatok.Base.Updated.Ver = true;

        return hkp_DokumentumAdatok;
    }



    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "DokumentumAdatok" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {

                            HKP_DokumentumAdatokService service = eRecordService.ServiceFactory.GetHKP_DokumentumAdatokService();
                            HKP_DokumentumAdatok hkp_DokumentumAdatok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Csatolmany csatolmany = null;

                            Result result = service.InsertAndAttachDocument(execParam, hkp_DokumentumAdatok, csatolmany);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    protected string DisplayBoolean(string value)
    {
        if (value != null)
        {
            if ("1".Equals(value.ToString()))
            {
                return "Igen";
            }
        }

        return "Nem";
    }
}
