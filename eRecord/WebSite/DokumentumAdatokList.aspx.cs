using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;
using Contentum.eQuery;

public partial class DokumentumAdatokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();


    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "DokumentumAdatokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(HKP_DokumentumAdatokSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = "Hivatali kapus �zenetek";

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.BejovoIratIktatasVisible = true;
        //ListHeader1.ErkeztetesVisible = true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("DokumentumAdatokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelHKP_DokumentumAdatok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = "";
        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("DokumentumAdatokForm.aspx"
                 , CommandName.Command + "=" + CommandName.New
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelHKP_DokumentumAdatok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = "";
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewHKP_DokumentumAdatok.ClientID,"", "check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewHKP_DokumentumAdatok.ClientID);
        ListHeader1.HistoryOnClientClick = "";

        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.BejovoIratIktatas.ToolTip = "Hivatali kapus �zenet �rkeztet�s, iktat�s";
        //ListHeader1.ErkeztetesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewHKP_DokumentumAdatok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(gridViewHKP_DokumentumAdatok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewHKP_DokumentumAdatok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelHKP_DokumentumAdatok.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewHKP_DokumentumAdatok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewHKP_DokumentumAdatok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, e-mail megtekint�s */
        Search.SetIdsToSearchObject(Page, "Id", new HKP_DokumentumAdatokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            HKP_DokumentumAdatokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        //ScriptManager1.RegisterAsyncPostBackControl(btnPostaFiokFeldolgozas);

        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = false;
        ListHeader1.NewVisible = false;

        //teszt
        //ListHeader1.NewEnabled = true;
        //ListHeader1.NewVisible = true;

        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdatok" + CommandName.View);
        ListHeader1.ModifyEnabled = false;
        ListHeader1.ModifyVisible = false;
        //ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdat" + CommandName.Invalidate);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdatok" + CommandName.ViewHistory);

        // erkeztetes
        //ListHeader1.ErkeztetesEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.New);
        // Email �rkeztet�s/iktat�s:
        ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdatokErkeztetes")
                                    || FunctionRights.GetFunkcioJog(Page, "DokumentumAdatokIktatas");

        //ListHeader1.BejovoIratIktatasEnabled = false;

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdatok" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdatok" + CommandName.Lock);

        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewHKP_DokumentumAdatok);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);

        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "DokumentumAdatok" + CommandName.ExcelExport);
    }

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void HKP_DokumentumAdatokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewHKP_DokumentumAdatok", ViewState, "HKP_DokumentumAdatok.ErkeztetesiDatum");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewHKP_DokumentumAdatok", ViewState, SortDirection.Descending);

        HKP_DokumentumAdatokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void HKP_DokumentumAdatokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        HKP_DokumentumAdatokService service = eRecordService.ServiceFactory.GetHKP_DokumentumAdatokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        HKP_DokumentumAdatokSearch search = (HKP_DokumentumAdatokSearch)Search.GetSearchObject(Page, Search.GetDefaultSearchObject_HKP_DokumentumAdatokSearch());
        search.OrderBy = Search.GetOrderBy("gridViewHKP_DokumentumAdatok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        search.Irany.Value = "0"; //Bej�v�
        search.Irany.Operator = Query.Operators.equals;

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(gridViewHKP_DokumentumAdatok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewHKP_DokumentumAdatok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //�rkeztetve checkbox be�ll�t�sa a kuldem�ny id alapj�n
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton imageButtonErkeztetve = (ImageButton)e.Row.FindControl("imageButtonErkeztetve");
            ImageButton imageButtonIktatva = (ImageButton)e.Row.FindControl("ImageButtonIktatva");
            DataRowView drw = (DataRowView)e.Row.DataItem;
            
            #region �rkeztetve ikon be�ll�t�sa
            if (drw != null && drw["KuldKuldemeny_Id"] != null)
            {
                string kuldemenyId = drw["KuldKuldemeny_Id"].ToString();
                
                if (String.IsNullOrEmpty(kuldemenyId))
                {
                    imageButtonErkeztetve.Visible = false;
                    imageButtonErkeztetve.OnClientClick = String.Empty;
                }
                else
                {
                    string erkeztetoSzam = String.Empty;
                    if (drw["ErkeztetoSzam"] != null)
                    {
                        erkeztetoSzam = drw["ErkeztetoSzam"].ToString();
                    }

                    imageButtonErkeztetve.Visible = true;
                    imageButtonErkeztetve.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + kuldemenyId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    if (!String.IsNullOrEmpty(erkeztetoSzam))
                    {
                        // �rkeztet�sz�m megjelen�t�se a Tooltip-ben
                        imageButtonErkeztetve.ToolTip = erkeztetoSzam + " k�ldem�ny megtekint�se";
                        imageButtonErkeztetve.AlternateText = imageButtonErkeztetve.ToolTip;
                    }
                }
            }
            else
            {
                imageButtonErkeztetve.Visible = false;
                imageButtonErkeztetve.OnClientClick = String.Empty;
            }
            #endregion

            #region Iktatva ikon be�ll�t�sa

            if (drw != null && drw["IratIktatoszamEsId"] != null)
            {
                string IratIktatoszamEsId = drw["IratIktatoszamEsId"].ToString();

                if (String.IsNullOrEmpty(IratIktatoszamEsId))
                {
                    imageButtonIktatva.Visible = false;
                    imageButtonIktatva.OnClientClick = String.Empty;
                }
                else
                {
                    string iktatoszam = this.GetIktatoszamFrom_IratIktatoszamEsId(IratIktatoszamEsId);
                    string iratId = this.GetIdFrom_IratIktatoszamEsId(IratIktatoszamEsId);

                    imageButtonIktatva.Visible = true;
                    imageButtonIktatva.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + iratId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    if (!String.IsNullOrEmpty(iktatoszam))
                    {
                        // �rkeztet�sz�m megjelen�t�se a Tooltip-ben
                        imageButtonIktatva.ToolTip = iktatoszam + " irat megtekint�se";
                        imageButtonIktatva.AlternateText = imageButtonIktatva.ToolTip;
                    }
                }
            }
            else
            {
                imageButtonErkeztetve.Visible = false;
                imageButtonErkeztetve.OnClientClick = String.Empty;
            }

            #endregion

            #region cbIsIktathatoKuldemeny be�ll�t�sa (�rkeztetett, nem iktatott)
            CheckBox cbIsIktathatoKuldemeny = (CheckBox)e.Row.FindControl("cbIsIktathatoKuldemeny");
            if (cbIsIktathatoKuldemeny != null && imageButtonErkeztetve != null && imageButtonIktatva != null)
            {
                // �rkeztetett, nem iktatott:
                cbIsIktathatoKuldemeny.Checked = imageButtonErkeztetve.Visible && !imageButtonIktatva.Visible;
            }
            #endregion cbIsIktathatoKuldemeny be�ll�t�sa (�rkeztetett, nem iktatott)
        }
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewHKP_DokumentumAdatok_PreRender(object sender, EventArgs e)
    {
        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewHKP_DokumentumAdatok);

        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeHKP_DokumentumAdatok);
        ListHeader1.RefreshPagerLabel();

    }


    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        HKP_DokumentumAdatokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeHKP_DokumentumAdatok);
        HKP_DokumentumAdatokGridViewBind();
    }


    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewHKP_DokumentumAdatok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewHKP_DokumentumAdatok, selectedRowNumber, "check");

            string id = gridViewHKP_DokumentumAdatok.DataKeys[selectedRowNumber].Value.ToString();

        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("DokumentumAdatokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelHKP_DokumentumAdatok.ClientID);
            ListHeader1.ModifyOnClientClick = "";
            string tableName = "HKP_DokumentumAdatok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelHKP_DokumentumAdatok.ClientID);


            HKP_DokumentumAdatokService HKP_DokumentumAdatokService = eRecordService.ServiceFactory.GetHKP_DokumentumAdatokService();
            ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
            execparam_emailGet.Record_Id = id;

            Result result_emailGet = HKP_DokumentumAdatokService.Get(execparam_emailGet);
            if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                ErrorUpdatePanel.Update();
                return;
            }

            HKP_DokumentumAdatok HKP_DokumentumAdatok = (HKP_DokumentumAdatok)result_emailGet.Record;

            // ha lehet �rkeztetni:
            if (String.IsNullOrEmpty(HKP_DokumentumAdatok.KuldKuldemeny_Id) && String.IsNullOrEmpty(HKP_DokumentumAdatok.IraIrat_Id))
            {
                //ListHeader1.ErkeztetesOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                //    , CommandName.Command + "=" + CommandName.New + "&" + QueryStringVars.HKP_DokumentumAdatokId + "=" + id
                //    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, updatePanelHKP_DokumentumAdatok.ClientID, EventArgumentConst.refreshMasterList);

                ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New + "&" + "DokumentumAdatokId" + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelHKP_DokumentumAdatok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // alert('Az email �rkeztetett');
                //ListHeader1.ErkeztetesOnClientClick = "alert('" + Resources.Error.ErrorCode_52651 + "'); return false;";
                if (!String.IsNullOrEmpty(HKP_DokumentumAdatok.IraIrat_Id))
                {
                    ListHeader1.BejovoIratIktatasOnClientClick = "alert('Az email m�r iktatva van!'); return false;";
                }
                else
                {
                    if (gridViewHKP_DokumentumAdatok.SelectedIndex > -1 && gridViewHKP_DokumentumAdatok.SelectedIndex < gridViewHKP_DokumentumAdatok.Rows.Count)
                    {
                        GridViewRow gvrow = gridViewHKP_DokumentumAdatok.Rows[gridViewHKP_DokumentumAdatok.SelectedIndex];

                        if (gvrow != null)
                        {
                            CheckBox cbIsIktathatoKuldemeny = (CheckBox)gvrow.FindControl("cbIsIktathatoKuldemeny");

                            if (cbIsIktathatoKuldemeny.Checked)
                            {

                                // Allapot szerint beallitja a headerben a gombok viselkedeset:
                                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(cbIsIktathatoKuldemeny.Text, Page, EErrorPanel1);

                                if (statusz == null)
                                {
                                    ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                                }
                                else
                                {
                                    ExecParam execparam = UI.SetExecParamDefault(Page);
                                    ErrorDetails errorDetail = null;
                                    if (Kuldemenyek.Iktathato(execparam, statusz, out errorDetail))
                                    {
                                        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                                            , QueryStringVars.Command + "=" + CommandName.New + "&" + "DokumentumAdatokId" + "=" + id
                                            + "&" + QueryStringVars.KuldemenyId + "=" + cbIsIktathatoKuldemeny.Text
                                            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelHKP_DokumentumAdatok.ClientID, EventArgumentConst.refreshMasterList);
                                    }
                                    else
                                    {
                                        ListHeader1.BejovoIratIktatasOnClientClick = "alert('" + Resources.Error.UINemIktathatoKuldemeny
                                            + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                                            + "'); return false;";
                                    }
                                }

                            }
                            else
                            {
                                ListHeader1.BejovoIratIktatasOnClientClick = "alert('Az email m�r iktatva van!'); return false;";
                            }

                        }
                    }
                    else
                    {
                        ListHeader1.BejovoIratIktatasOnClientClick = "alert('" + Resources.Error.ErrorCode_52651 + "'); return false;";
                    }
                }
            }


        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelHKP_DokumentumAdatok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    HKP_DokumentumAdatokGridViewBind();
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            bool success = deleteSelectedEmailBoritek();
            if (success)
            {
                HKP_DokumentumAdatokGridViewBind();
            }
        }

        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, gridViewHKP_DokumentumAdatok, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
    }

    //kiv�lasztott elemek t�rl�se
    private bool deleteSelectedEmailBoritek()
    {        
        if (FunctionRights.GetFunkcioJog(Page, "EmailBoritekInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewHKP_DokumentumAdatok, EErrorPanel1, ErrorUpdatePanel);

            int markedRows = MarkSelectedRowsWhichCannotInvalidate();

            if (markedRows > 0)
            {
                string errorMessage = (deletableItemsList.Count == 1) ? Resources.Error.UIEmailBoritekNemTorolheto
                    : Resources.Error.UIEmailBoritekokNemTorolhetok;
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, errorMessage);
                ErrorUpdatePanel.Update();
                return false;
            }

            HKP_DokumentumAdatokService service = eRecordService.ServiceFactory.GetHKP_DokumentumAdatokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return false;
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        return true;
    }

    private int MarkSelectedRowsWhichCannotInvalidate()
    {
        int markedRows = 0;
 
        foreach (GridViewRow row in gridViewHKP_DokumentumAdatok.Rows)
        {
            CheckBox checkB = (CheckBox)row.FindControl("check");
            // ha ki van jel�lve a sor:
            if (checkB.Checked)
            {
                // �rkeztetve vagy iktatva van-e m�r?

                Label label_KuldemenyId = (Label)row.FindControl("Label_KuldemenyId");
                Label label_IratId = (Label)row.FindControl("Label_IratId");

                // ha m�r �rkeztetve vagy iktatva van:
                if ((label_KuldemenyId != null && !String.IsNullOrEmpty(label_KuldemenyId.Text))
                    || (label_IratId != null && !String.IsNullOrEmpty(label_IratId.Text)))
                {
                    markedRows++;

                    //row.Attributes["style"] += " background-color: Coral; ";
                    row.BackColor = System.Drawing.Color.Coral;
                }
                else
                {
                    row.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                // Csak hogy elt�nj�n a sz�nez�s a k�vetkez� pr�b�lkoz�skor:
                row.BackColor = System.Drawing.Color.White;
            }
        }

        return markedRows;
    }



    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedEmailBoritekRecords();
                HKP_DokumentumAdatokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedEmailBoritekRecords();
                HKP_DokumentumAdatokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedHKP_DokumentumAdatok();
                HKP_DokumentumAdatokGridViewBind();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedEmailBoritekRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewHKP_DokumentumAdatok, "HKP_DokumentumAdatok"
            , "DokumentumAdatokLock", "DokumentumAdatokForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedEmailBoritekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewHKP_DokumentumAdatok, "HKP_DokumentumAdatok"
            , "DokumentumAdatokLock", "DokumentumAdatokForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewHKP_DokumentumAdatok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedHKP_DokumentumAdatok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewHKP_DokumentumAdatok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "HKP_DokumentumAdatok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewHKP_DokumentumAdatok_Sorting(object sender, GridViewSortEventArgs e)
    {
        HKP_DokumentumAdatokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewHKP_DokumentumAdatok", ViewState, e.SortExpression));
    }

    #endregion

    public string GetDocumentLink(Guid? dokumentumId, string fileNev)
    {
        if (dokumentumId != null)
        {
            return String.Format("<a href=\"javascript:void(0);\" onclick=\"window.open('GetDocumentContent.aspx?id={0}'); return false;\" style=\"text-decoration:underline\">{1}</a>", dokumentumId, fileNev);
        }
        else
        {
            return fileNev;
        }
    }

    #region segedFv

    // Segedfv.

    private const string IratIktatoszamEsId_Delimitter = "***";

    protected string GetIktatoszamFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return String.Empty;
        }
    }

    protected string GetIdFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return String.Empty;
            }
        }
        else
        {
            return String.Empty;
        }
    }

    protected string DisplayBoolean(object value)
    {
        if (value != null)
        {
            if("1".Equals(value.ToString()))
            {
                return "Igen";
            }
        }

        return "Nem";
    }

    #endregion

    protected void btnPostaFiokFeldolgozas_Click(object sender, EventArgs e)
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        try
        {
            svc.PostaFiokFeldolgozas();
            HKP_DokumentumAdatokGridViewBind();
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", ex.Message);
            ErrorUpdatePanel.Update();
        }
    }

}
