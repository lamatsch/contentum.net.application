<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="DokumentumAdatokSearch.aspx.cs" Inherits="DokumentunAdatokSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>


<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>


<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor" runat="server" id="KapcsolatiKodRow" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKapcsolatiKod" runat="server" Text="Kapcsolati k�d:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="KapcsolatiKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelNev" runat="server" Text="Felad� neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Nev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="EmailRow" runat="server">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelEmail" runat="server" Text="Felad� e-mail c�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Email" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="RovidNevRow" runat="server">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelRovidNev" runat="server" Text="Hivatal r�vid neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="RovidNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="MAKKodRow" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelMAKKod" runat="server" Text="Hivatal M�K k�dja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="MAKKod" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="KRIDRow" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKRID" runat="server" Text="Hivatal KRID-je:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:RequiredNumberBox ID="KRID" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:RequiredNumberBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelErkeztetesiSzam" runat="server" Text="�rkeztet�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="ErkeztetesiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="AllapotRow" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAllapot" runat="server" Text="�llapot:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:KodtarakDropDownList ID="Allapot" runat="server" />
                            </td>
                        </tr>   
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelDokTipusHivatal" runat="server" Text="Hivatal:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="DokTipusHivatal" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelDokTipusAzonosito" runat="server" Text="T�pus:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="DokTipusAzonosito" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="TipusNevRow" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelDokTipusLeiras" runat="server" Text="T�pus n�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="DokTipusLeiras" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="HivatkozasiSzamRow" >
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelHivatkozasiSzam" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="HivatkozasiSzam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="Megjegyzes" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelFileNev" runat="server" Text="F�jln�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:TextBox ID="FileNev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trErvenyessegiDatum" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelErvenyessegiDatum" runat="server" Text="�rv�nyess�gi d�tum:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:DatumIntervallum_SearchCalendarControl ID="ErvenyessegiDatum" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:DatumIntervallum_SearchCalendarControl>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelErkeztetesiDatum" runat="server" Text="�rkeztet�si d�tum:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:DatumIntervallum_SearchCalendarControl ID="ErkeztetesiDatum" runat="server" CssClass="mrUrlapInput" Validate="false"></uc:DatumIntervallum_SearchCalendarControl>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="KezbesitettsegRow" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelKezbesitettseg" runat="server" Text="K�zbes�tetts�g:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="Kezbesitettseg" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="ValasztTitkositasRow" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelValaszTitkositas" runat="server" Text="V�lasz titkos�t�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:DropDownList ID="ValaszTitkositas" runat="server" CssClass="mrUrlapInputComboBox">
                                <asp:ListItem Text="Igen" Value="1" />
                                <asp:ListItem Text="Nem" Value="0" />
                                <asp:ListItem Text="�sszes" Value="" Selected="True" />
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="ValaszUtvonalRow" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelValaszUtvonal" runat="server" Text="V�lasz �tvonal:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="ValaszUtvonal" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="RendszeruzenetRow" runat="server" >
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelRendszeruzenet" runat="server" Text="Rendszer�zenet:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:DropDownList ID="Rendszeruzenet" runat="server" CssClass="mrUrlapInputComboBox">
                                <asp:ListItem Text="Igen" Value="1" />
                                <asp:ListItem Text="Nem" Value="0" />
                                <asp:ListItem Text="�sszes" Value="" Selected="True" />
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="TarteruletRow" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelTarterulet" runat="server" Text="T�rter�let:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <uc:KodtarakDropDownList ID="Tarterulet" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="ETertivevenyRow" runat="server">
                            <td class="mrUrlapCaption">
                            <asp:Label ID="labelETertiveveny" runat="server" Text="ET�rtivev�ny:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                            <asp:DropDownList ID="ETertiveveny" runat="server" CssClass="mrUrlapInputComboBox">
                                <asp:ListItem Text="Igen" Value="1" />
                                <asp:ListItem Text="Nem" Value="0" />
                                <asp:ListItem Text="�sszes" Value="" Selected="True" />
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="ErkeztetettIktatottRow" runat="server">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="ErkeztetettIktatott" runat="server" CssClass="mrUrlapInputComboBox" Text="�rkeztetett / Iktatott" />
                            </td>
                        </tr>           
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
