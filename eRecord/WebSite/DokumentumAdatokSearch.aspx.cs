using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class DokumentunAdatokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(HKP_DokumentumAdatokSearch);

    private const string kcs_Kezbesittettseg = KodTarak.EBEADVANY_KODCSOPORTOK.KEZBETITETTSEG;
    private const string kcs_Valaszutvonal = KodTarak.EBEADVANY_KODCSOPORTOK.VALASZUTVONAL;
    private const string kcs_Tarterulet = KodTarak.EBEADVANY_KODCSOPORTOK.TARTERULET;
    private const string kcs_Allapot = KodTarak.EBEADVANY_KODCSOPORTOK.ALLAPOT;

    private string Mode
    {
        get
        {
            return Request.QueryString.Get(QueryStringVars.Mode);
        }
    }

    protected bool IsKimeno
    {
        get
        {
            return "Kimeno".Equals(this.Mode);
        }
    }

    const string customSessionName = "KimenoDokumentumAdatokSearch";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

        if (IsKimeno)
        {
            SearchHeader1.CustomTemplateTipusNev = customSessionName;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = "Hivatali kapus �zenetek keres�se";
       
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            HKP_DokumentumAdatokSearch searchObject = null;
            if (IsKimeno)
            {
                if (Search.IsSearchObjectInSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev))
                {
                    searchObject = (HKP_DokumentumAdatokSearch)Search.GetSearchObject_CustomSessionName(Page, new HKP_DokumentumAdatokSearch()
                        , SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }
            else
            {
                if (Search.IsSearchObjectInSession(Page, _type))
                {
                    searchObject = (HKP_DokumentumAdatokSearch)Search.GetSearchObject(Page, new HKP_DokumentumAdatokSearch());
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }
            LoadComponentsFromSearchObject(searchObject);
        }

        ErkeztetettIktatottRow.Visible = !IsKimeno;
        AllapotRow.Visible = IsKimeno;
        ETertivevenyRow.Visible = IsKimeno;
        EmailRow.Visible = !IsKimeno;
        RovidNevRow.Visible = !IsKimeno;
        if (IsKimeno)
        {
            labelNev.Text = "C�mzett";
        }
        HivatkozasiSzamRow.Visible = !IsKimeno;
        RendszeruzenetRow.Visible = !IsKimeno;
    }

    private void FilterAllapotValues()
    {
        for (int i = Allapot.DropDownList.Items.Count - 1; i >= 0; i--)
        {
            ListItem item = Allapot.DropDownList.Items[i];

            if (!String.IsNullOrEmpty(item.Value))
            {
                int j;
                if (Int32.TryParse(item.Value, out j))
                {
                    if (j < 11)
                    {
                        Allapot.DropDownList.Items.RemoveAt(i);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        HKP_DokumentumAdatokSearch hkp_DokumentumAdatokSearch = null;
        if (searchObject != null) hkp_DokumentumAdatokSearch = (HKP_DokumentumAdatokSearch)searchObject;

        if (hkp_DokumentumAdatokSearch != null)
        {
            KapcsolatiKod.Text = hkp_DokumentumAdatokSearch.KapcsolatiKod.Value;
            Nev.Text = hkp_DokumentumAdatokSearch.Nev.Value;
            Email.Text = hkp_DokumentumAdatokSearch.Email.Value;
            RovidNev.Text = hkp_DokumentumAdatokSearch.RovidNev.Value;
            MAKKod.Text = hkp_DokumentumAdatokSearch.MAKKod.Value;
            KRID.Text = hkp_DokumentumAdatokSearch.KRID.Value;
            ErkeztetesiSzam.Text = hkp_DokumentumAdatokSearch.ErkeztetesiSzam.Value;
            DokTipusHivatal.Text = hkp_DokumentumAdatokSearch.DokTipusHivatal.Value;
            DokTipusAzonosito.Text = hkp_DokumentumAdatokSearch.DokTipusAzonosito.Value;
            DokTipusLeiras.Text = hkp_DokumentumAdatokSearch.DokTipusLeiras.Value;
            HivatkozasiSzam.Text = hkp_DokumentumAdatokSearch.HivatkozasiSzam.Value;
            Megjegyzes.Text = hkp_DokumentumAdatokSearch.Megjegyzes.Value;
            FileNev.Text = hkp_DokumentumAdatokSearch.FileNev.Value;
            ErvenyessegiDatum.SetComponentFromSearchObjectFields(hkp_DokumentumAdatokSearch.ErvenyessegiDatum);
            ErkeztetesiDatum.SetComponentFromSearchObjectFields(hkp_DokumentumAdatokSearch.ErkeztetesiDatum);
            Kezbesitettseg.FillAndSetSelectedValue(kcs_Kezbesittettseg, hkp_DokumentumAdatokSearch.Kezbesitettseg.Value, true, SearchHeader1.ErrorPanel);
            ValaszTitkositas.Text = hkp_DokumentumAdatokSearch.ValaszTitkositas.Value;
            ValaszUtvonal.FillAndSetSelectedValue(kcs_Valaszutvonal, hkp_DokumentumAdatokSearch.ValaszUtvonal.Value, true, SearchHeader1.ErrorPanel);
            Rendszeruzenet.Text = hkp_DokumentumAdatokSearch.Rendszeruzenet.Value;
            Tarterulet.FillAndSetSelectedValue(kcs_Tarterulet, hkp_DokumentumAdatokSearch.Tarterulet.Value, true, SearchHeader1.ErrorPanel);
            ETertiveveny.Text = hkp_DokumentumAdatokSearch.ETertiveveny.Value;
            Allapot.FillAndSetSelectedValue(kcs_Allapot, hkp_DokumentumAdatokSearch.Allapot.Value, true, SearchHeader1.ErrorPanel);
            FilterAllapotValues();
            


            if (String.IsNullOrEmpty(hkp_DokumentumAdatokSearch.KuldKuldemeny_Id.Operator))
            {
                ErkeztetettIktatott.Checked = true;
            }
            else
            {
                ErkeztetettIktatott.Checked = false;
            }
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private HKP_DokumentumAdatokSearch SetSearchObjectFromComponents()
    {
        HKP_DokumentumAdatokSearch hkp_DokumentumAdatokSearch = (HKP_DokumentumAdatokSearch)SearchHeader1.TemplateObject;
        if (hkp_DokumentumAdatokSearch == null)
        {
            hkp_DokumentumAdatokSearch = GetDefaultSearchObject();
        }

        if (!String.IsNullOrEmpty(KapcsolatiKod.Text))
        {
            hkp_DokumentumAdatokSearch.KapcsolatiKod.Value = KapcsolatiKod.Text;
            hkp_DokumentumAdatokSearch.KapcsolatiKod.Operator = Search.GetOperatorByLikeCharater(KapcsolatiKod.Text);
        }
        if (!String.IsNullOrEmpty(Nev.Text))
        {
            hkp_DokumentumAdatokSearch.Nev.Value = Nev.Text;
            hkp_DokumentumAdatokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev.Text);
        }
        if (!String.IsNullOrEmpty(Email.Text))
        {
            hkp_DokumentumAdatokSearch.Email.Value = Email.Text;
            hkp_DokumentumAdatokSearch.Email.Operator = Search.GetOperatorByLikeCharater(Email.Text);
        }
        if (!String.IsNullOrEmpty(RovidNev.Text))
        {
            hkp_DokumentumAdatokSearch.RovidNev.Value = RovidNev.Text;
            hkp_DokumentumAdatokSearch.RovidNev.Operator = Search.GetOperatorByLikeCharater(RovidNev.Text);
        }
        if (!String.IsNullOrEmpty(MAKKod.Text))
        {
            hkp_DokumentumAdatokSearch.MAKKod.Value = MAKKod.Text;
            hkp_DokumentumAdatokSearch.MAKKod.Operator = Search.GetOperatorByLikeCharater(MAKKod.Text);
        }
        if (!String.IsNullOrEmpty(KRID.Text))
        {
            hkp_DokumentumAdatokSearch.KRID.Value = KRID.Text;
            hkp_DokumentumAdatokSearch.KRID.Operator = Search.GetOperatorByLikeCharater(KRID.Text);
        }
        if (!String.IsNullOrEmpty(ErkeztetesiSzam.Text))
        {
            hkp_DokumentumAdatokSearch.ErkeztetesiSzam.Value = ErkeztetesiSzam.Text;
            hkp_DokumentumAdatokSearch.ErkeztetesiSzam.Operator = Search.GetOperatorByLikeCharater(ErkeztetesiSzam.Text);
        }
        if (!String.IsNullOrEmpty(DokTipusHivatal.Text))
        {
            hkp_DokumentumAdatokSearch.DokTipusHivatal.Value = DokTipusHivatal.Text;
            hkp_DokumentumAdatokSearch.DokTipusHivatal.Operator = Search.GetOperatorByLikeCharater(DokTipusHivatal.Text);
        }
        if (!String.IsNullOrEmpty(DokTipusAzonosito.Text))
        {
            hkp_DokumentumAdatokSearch.DokTipusAzonosito.Value = DokTipusAzonosito.Text;
            hkp_DokumentumAdatokSearch.DokTipusAzonosito.Operator = Search.GetOperatorByLikeCharater(DokTipusAzonosito.Text);
        }
        if (!String.IsNullOrEmpty(DokTipusLeiras.Text))
        {
            hkp_DokumentumAdatokSearch.DokTipusLeiras.Value = DokTipusLeiras.Text;
            hkp_DokumentumAdatokSearch.DokTipusLeiras.Operator = Search.GetOperatorByLikeCharater(DokTipusLeiras.Text);
        }
        if (!String.IsNullOrEmpty(HivatkozasiSzam.Text))
        {
            hkp_DokumentumAdatokSearch.HivatkozasiSzam.Value = HivatkozasiSzam.Text;
            hkp_DokumentumAdatokSearch.HivatkozasiSzam.Operator = Search.GetOperatorByLikeCharater(HivatkozasiSzam.Text);
        }
        if (!String.IsNullOrEmpty(Megjegyzes.Text))
        {
            hkp_DokumentumAdatokSearch.Megjegyzes.Value = Megjegyzes.Text;
            hkp_DokumentumAdatokSearch.Megjegyzes.Operator = Search.GetOperatorByLikeCharater(Megjegyzes.Text);
        }
        if (!String.IsNullOrEmpty(FileNev.Text))
        {
            hkp_DokumentumAdatokSearch.FileNev.Value = FileNev.Text;
            hkp_DokumentumAdatokSearch.FileNev.Operator = Search.GetOperatorByLikeCharater(FileNev.Text);
        }
        ErvenyessegiDatum.SetSearchObjectFields(hkp_DokumentumAdatokSearch.ErvenyessegiDatum);
        ErkeztetesiDatum.SetSearchObjectFields(hkp_DokumentumAdatokSearch.ErkeztetesiDatum);
        if (!String.IsNullOrEmpty(Kezbesitettseg.SelectedValue))
        {
            hkp_DokumentumAdatokSearch.Kezbesitettseg.Value = Kezbesitettseg.SelectedValue;
            hkp_DokumentumAdatokSearch.Kezbesitettseg.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(ValaszTitkositas.Text))
        {
            hkp_DokumentumAdatokSearch.ValaszTitkositas.Value = ValaszTitkositas.Text;
            hkp_DokumentumAdatokSearch.ValaszTitkositas.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(ValaszUtvonal.SelectedValue))
        {
            hkp_DokumentumAdatokSearch.ValaszUtvonal.Value = ValaszUtvonal.SelectedValue;
            hkp_DokumentumAdatokSearch.ValaszUtvonal.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Rendszeruzenet.Text))
        {
            hkp_DokumentumAdatokSearch.Rendszeruzenet.Value = Rendszeruzenet.Text;
            hkp_DokumentumAdatokSearch.Rendszeruzenet.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Tarterulet.SelectedValue))
        {
            hkp_DokumentumAdatokSearch.Tarterulet.Value = Tarterulet.SelectedValue;
            hkp_DokumentumAdatokSearch.Tarterulet.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(ETertiveveny.Text))
        {
            hkp_DokumentumAdatokSearch.ETertiveveny.Value = ETertiveveny.Text;
            hkp_DokumentumAdatokSearch.ETertiveveny.Operator = Query.Operators.equals;
        }
        if (!String.IsNullOrEmpty(Allapot.SelectedValue))
        {
            hkp_DokumentumAdatokSearch.Allapot.Value = Allapot.SelectedValue;
            hkp_DokumentumAdatokSearch.Allapot.Operator = Query.Operators.equals;
        }

        if (ErkeztetettIktatott.Checked)
        {
            hkp_DokumentumAdatokSearch.KuldKuldemeny_Id.Operator = String.Empty;
        }
        else
        {
            hkp_DokumentumAdatokSearch.KuldKuldemeny_Id.Operator = Query.Operators.isnull;
        }
              
        return hkp_DokumentumAdatokSearch;
    }

  

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            HKP_DokumentumAdatokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);

                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private HKP_DokumentumAdatokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        if (IsKimeno)
        {
            return new HKP_DokumentumAdatokSearch();
        }
        else
        {
            return Search.GetDefaultSearchObject_HKP_DokumentumAdatokSearch();
        }
    }

}
