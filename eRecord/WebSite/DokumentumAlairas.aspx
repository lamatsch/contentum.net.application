﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DokumentumAlairas.aspx.cs" Inherits="DokumentumAlairas" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc2" %>
<%@ Register Src="~/eRecordComponent/NetlockSignAssistSigner.ascx" TagName="NetlockSignAssistSigner" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/MicroSigner.ascx" TagName="MicroSigner" TagPrefix="uc3" %>
<%@ Register Src="~/eRecordComponent/AVDH.ascx" TagName="AVDH" TagPrefix="uc5" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/json2.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/libs/jquery/jquery.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/esign_v2.js" />
            <asp:ScriptReference Path="~/JavaScripts/Microsec.js?t=20191015" />
            <asp:ScriptReference Path="~/JavaScripts/AVDH.js" />
        </Scripts>
        <Services>
            <asp:ServiceReference Path="~/WrappedWebService/MicroSignerCallbackService.asmx" />
            <asp:ServiceReference Path="~/WrappedWebService/AVDHCallbackService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,DokumentumAlairasFormHeaderTitle %>" />
    <asp:HiddenField ID="hfIratId" runat="server" />
    <asp:HiddenField ID="hfIratIdList" runat="server" />
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%" id="dataTable">
            <tr>
                <td>
                    <br/>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">

                                    <asp:Label ID="Label1" runat="server" Text="Kijelölt aláíró:"></asp:Label>

                                </td>
                                <td class="mrUrlapMezo">
                                    <uc2:FelhasznaloCsoportTextBox ID="Alairo_FelhasznaloCsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="Aláírás típusa:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:DropDownList ID="AlairasTipusa_TextBox" runat="server" CssClass="mrUrlapInputComboBox" AutoPostBack="true">
                                        <asp:ListItem Value="1">Kártyás aláírás</asp:ListItem>
                                        <asp:ListItem Value="2">Központi aláírás</asp:ListItem>
                                        <asp:ListItem Value="3">Manuális aláírás</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="MegjegyzesLabel" runat="server" Text="<%$Forditas:MegjegyzesLabel|Megjegyzés:%>"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="MegjegyzesTextBox" runat="server" class="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;</td>
                                <td class="mrUrlapMezo">&nbsp;</td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <table style="width: 60%;">
                        <tr>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="1" ID="ImageAlairas" runat="server" ImageUrl="~/images/hu/ovalgomb/alairas.gif"
                                    onmouseover="swapByName(this.id,'alairas2.gif')" onmouseout="swapByName(this.id,'alairas.gif')"
                                    OnClick="ImageAlairas_Click" CommandName="Alairas" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="2" ID="ImageElutasitas" runat="server" ImageUrl="~/images/hu/ovalgomb/elutasitas.gif"
                                    onmouseover="swapByName(this.id,'elutasitas2.gif')" onmouseout="swapByName(this.id,'elutasitas.gif')"
                                    OnClick="ImageElutasitas_Click" CommandName="Elutasitas" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="6" ID="ImageCancel" runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                    onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                    OnClientClick="window.close(); return false;" CommandName="Cancel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <uc:NetlockSignAssistSigner ID="NetlockSigner" runat="server"/>
    </asp:Panel>

    <asp:UpdateProgress ID="processPanel" runat="server" DisplayAfter="30">
        <ProgressTemplate>
            <div class="updateProgress" id="UpdateProgressPanelA" runat="server">
                <eUI:eFormPanel ID="EFormPanelProgress" runat="server">
                    <table>
                        <tr>
                            <td id="imageTD1">
                                <div id="IMGDIV1">
                                    <%--<img src="images/hu/egyeb/activity_indicator.gif" alt="" />--%>
                                    <asp:Image runat="server" ImageUrl="~/images/hu/egyeb/activity_indicator.gif" />
                                </div>
                            </td>
                            <td class="updateProgressText">
                                <span id="progressLabel">Feldolgozás folyamatban...</span>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
            </div>
            <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtenderA" runat="server" TargetControlID="processPanel"
                VerticalSide="Middle" HorizontalSide="Center">
            </ajaxToolkit:AlwaysVisibleControlExtender>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField ID="halkProcessId" runat="server" Value="" />
    <asp:HiddenField ID="PostbackClientID" runat="server" Value="" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function () {
            //$('#IMGDIV1').remove();
            $('#<%=processPanel.ClientID %>').hide();
        });
        var HALKTimer;
        var lastStatusGetTime;
        function checkStatus() {
            $('#<%=processPanel.ClientID %>').show();
            //$("#dataTable").find("input,button,textarea,select").attr("disabled", "disabled");
            HALKTimer = setInterval(checkStatusFromBG, 1000);
            //$('#imageTD1').append('<div id=\"IMGDIV1\"><img src=\"images/hu/egyeb/activity_indicator.gif\" /></div>');
        }

        function checkStatusFromBG() {
            var bgProcessId = $('#<%=halkProcessId.ClientID %>').val();
            if (bgProcessId) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: 'DokumentumAlairas.aspx/GetHALKStatusz',
                    data: '{"processId":"' + bgProcessId + '", "lastStatusGetTime":"' + lastStatusGetTime + '"}',
                    success:
                        function (response) {
                            processResult(response.d);
                        }
                    , error: function (request, status, error) {
                        disableTimer();
                    }
                });
            }
        }

        function processResult(statusResult) {
            if (statusResult) {
                $('#progressLabel').html(statusResult.BackgroundProcessStatus);
                lastStatusGetTime = statusResult.StatusGetTime;
                if (statusResult.BackgroundProcessEnded) {
                    disableTimer();
                }
            }
        };

        function disableTimer() {
               $('#<%=processPanel.ClientID %>').hide();
                    //$('#IMGDIV1').remove();
                    //$("#dataTable").find("input,button,textarea,select").removeAttr("disabled");
                    clearInterval(HALKTimer);
                    window.returnValue = true;
                    var ua = window.navigator.userAgent;
                    var postbackID = $('#<%=PostbackClientID.ClientID %>').val();

            var isIE = /MSIE|Trident/.test(ua);

            if (isIE) {
                __doPostBack(postbackID, "refreshMasterList");
            } else {
                window.opener.__doPostBack(postbackID, window.opener.postBackArgument);
            }

            window.close();
        }
    </script>

    <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
        <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
            <div class="mrResultPanelText">
                <asp:Label ID="Label_Result" runat="server" Text="Az aláírás sikeresen végrehajtódott."></asp:Label>
            </div>
            <table>
                <tr>
                    <td>
                        <asp:ImageButton ID="ImageClose_ResultPanel" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                            onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                            CommandName="Close" OnClick="ImageClose_ResultPanel_Click" />
                    </td>
                </tr>
            </table>
        </eUI:eFormPanel>
    </asp:Panel>

    <%--Microsec ESign aláíráshoz--%>
    <uc3:MicroSigner ID="MicroSigner1" runat="server" />
    <uc5:AVDH ID="AVDH1" runat="server" />
</asp:Content>
