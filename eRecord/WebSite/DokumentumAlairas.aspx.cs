﻿using System;
using System.Linq;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Text;
using System.Xml;
using System.Net;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Contentum.eUtility.NetLockSignAssist;
using Contentum.eUtility.Netlock;

public partial class DokumentumAlairas : Contentum.eUtility.UI.PageBase
{

    private const string FunkcioKod_IraIratCsatolmanyModify = "IraIratCsatolmanyModify";
    //BUG 4755
    //private const string FuggoKodtarUgyiratJelleg = "FAB03917-45E4-49B3-8547-0E29446CB8A4";
    private const string FuggoKodtarAlairasFajta = "2A37FBA6-A3C9-4ED8-8C89-A0969E38DE9B";
    /*private const string UgyiratJellegElektronikus = "D5DED3B2-98E0-DD11-8975-005056C00008";
    private const string UgyiratJellegPapir = "D6DED3B2-98E0-DD11-8975-005056C00008";
    private const string UgyiratJellegVegyes = "D7DED3B2-98E0-DD11-8975-005056C00008";*/
    private const string AdatHordozoTipusElektronikus = "82F2FAFC-A312-4C13-A1FF-E86DB76B131F";
    private const string AdatHordozoTipusPapir = "22E7EDE2-BFCA-483A-975B-FFDB17B93272";
    private const string AdatHordozoTipusVegyes = "9DDC5255-0AC3-4FFF-AC94-E8089682B020";

    private string csatolmanyId = String.Empty; // Id-k összefűzve
    private string[] csatolmanyokArray;
    private string proc_Id = String.Empty;

    string IratId
    {
        get
        {
            return hfIratId.Value;
        }
        set
        {
            hfIratId.Value = value;
        }
    }

    string PKI_VENDOR
    {
        get
        {
            return Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), "PKI_VENDOR");
        }
    }

    List<string> IratIdList
    {
        get
        {
            return hfIratIdList.Value.Split(';').ToList();
        }
        set
        {
            hfIratIdList.Value = String.Join(";", value.ToArray());
        }
    }

    #region BLG 6473
    bool KozpontiAlairas
    {
        get
        {
            return AlairasTipusa_TextBox.Text == "2";
        }
    }
    #endregion
    bool ManualisAlairas
    {
        get
        {
            //BL 7148 - KR Kiküldés is manuális aláírásnak minősül
            return AlairasTipusa_TextBox.Text == "3" || AlairasTipusa_TextBox.Text == "4";
        }

    }
    bool EmptyValue
    {
        get
        {
            return String.IsNullOrEmpty(AlairasTipusa_TextBox.Text);
        }
    }
    bool KartyasAlairas
    {
        get
        {
            return AlairasTipusa_TextBox.Text.Equals("1");
        }
    }

    #region BL 7148
    bool KrKikuldes
    {
        get
        {
            return AlairasTipusa_TextBox.Text.Equals("4");
        }
    }
    #endregion

    bool KozpontiAlairasEnabled
    {
        get
        {
            return FunctionRights.GetFunkcioJog(Page, "KozpontiElektronikusAlairas");
        }
    }

    bool KartyasAlairasEnabled
    {
        get
        {
            return FunctionRights.GetFunkcioJog(Page, "KartyasElektronikusAlairas");
        }
    }

    bool AlairasTipusEnabled
    {
        get
        {
            return KozpontiAlairasEnabled || KartyasAlairasEnabled;
        }
    }

    bool AlairasEnabled
    {
        get
        {
            return KozpontiAlairasEnabled || KartyasAlairasEnabled;
        }
    }


    protected void Page_Init(object sender, EventArgs e)
    {

        // Jogosultságellenőrzés:
        if (!AlairasEnabled)
        {
            FunctionRights.RedirectErrorPage(Page);
        }

        csatolmanyId = Request.QueryString.Get(QueryStringVars.CsatolmanyId);
        proc_Id = Request.QueryString.Get("Proc_Id");
        PostbackClientID.Value =   Request.QueryString.Get("PostBackClientID"); // TODO use QueryStringVars.PostBackClientID

        //BUG 2509 - ha van proc_Id, akkor nincs külön csatolmanyId a requestben, proc_Id alapján töltjük ki
        //BUG 2509 - egyszerűbb itt átírni a csatolmanyokArray forrását, mint később vizsgálni a proc_Id -t
        if (!string.IsNullOrEmpty(proc_Id))
        {
            ExecParam execParamRP = UI.SetExecParamDefault(Page, new ExecParam());
            string csakFodok = Rendszerparameterek.Get(execParamRP, "ALAIRAS_CSAK_FODOKUMENTUM");
            if ("1".Equals(csakFodok) || string.IsNullOrEmpty(csatolmanyId)) {
                #region csatolmanyId meghatározása procId alapján
                csatolmanyId = string.Empty;

                ExecParam xpm = UI.SetExecParamDefault(Page);
                EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                EREC_Alairas_Folyamat_TetelekSearch search = new EREC_Alairas_Folyamat_TetelekSearch();
                search.AlairasFolyamat_Id.Value = proc_Id;
                search.AlairasFolyamat_Id.Operator = Query.Operators.equals;

                Result res = service.GetAll(xpm, search);

                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                    return;
                }
                else
                {
                    int rowInd = 0;
                    foreach (DataRow row in res.Ds.Tables[0].Rows)
                    {
                        string selectedCsatolmanyId = row["Csatolmany_Id"].ToString();
                        if (!string.IsNullOrEmpty(selectedCsatolmanyId))
                        {
                            csatolmanyId += rowInd == 0 ? selectedCsatolmanyId : "," + selectedCsatolmanyId;
                            rowInd++;
                        }
                    }
                }
                #endregion
            }
            else if (!string.IsNullOrEmpty(csatolmanyId))
            {
                //BUG 5695 - logika megfordítása - ha van csatolmanyId is, akkor az alapján folyamat tételek rendberakása
                ExecParam xpm = UI.SetExecParamDefault(Page);
                EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                EREC_Alairas_Folyamat_TetelekSearch search = new EREC_Alairas_Folyamat_TetelekSearch();
                search.AlairasFolyamat_Id.Value = proc_Id;
                search.AlairasFolyamat_Id.Operator = Query.Operators.equals;

                Result res = service.GetAll(xpm, search);

                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                    return;
                }
                else
                {
                    //int rowInd = 0;
                    foreach (DataRow row in res.Ds.Tables[0].Rows)
                    {
                        string selectedCsatolmanyId = row["Csatolmany_Id"].ToString();
                        if (!string.IsNullOrEmpty(selectedCsatolmanyId))
                        {
                            //csatolmanyId += rowInd == 0 ? selectedCsatolmanyId : "," + selectedCsatolmanyId;
                            //rowInd++;
                            if (!csatolmanyId.Contains(selectedCsatolmanyId))
                            {
                                EREC_Alairas_Folyamat_TetelekService serviceDelete = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                                ExecParam xpmDelete = UI.SetExecParamDefault(Page);
                                xpmDelete.Record_Id = row["Id"].ToString();

                                serviceDelete.Delete(xpmDelete);
                            }
                        }
                    }
                }
            }
            
        }
        if (!String.IsNullOrEmpty(csatolmanyId))
        {
            // csatolmány Id-k szétszedése, ha több is volt:
            csatolmanyokArray = csatolmanyId.Split(',');
        }
        else
        {
            //BUG 3638 - nincs csatolmány hibaüzenet!
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel,"",Resources.Error.TomegesAlairasNincsCsatolmany);
            MainPanel.Visible = false;
        }

    }

    #region BUG 4755 - legördülő lista töltése kódtár alapján
    private String GetIdListFromResult(Result result, String idName)
    {
        StringBuilder retSB = new StringBuilder();
        int counter = 0;
        foreach (DataRow row in result.Ds.Tables[0].Rows)
        {
            string iratId = row[idName].ToString();
            String addToBuilder = counter == 0 ? "'" + iratId + "'" : ",'" + iratId + "'";
            retSB.Append(addToBuilder);
            counter++;
        }
        return retSB.ToString();
    }

    private String GetIdListFromArray(string[] array)
    {
        StringBuilder retSB = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            string addToBuilder = (i == 0) ? "'" + array[i] + "'" : ",'" + array[i] + "'";
            retSB.Append(addToBuilder);
        }
        return retSB.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private void FillAlairasTipusaDropDown()
    {
        AlairasTipusa_TextBox.Items.Clear();
        Result result = Contentum.eUtility.KodtarFuggoseg.GetFuggoKodtarById(UI.SetExecParamDefault(Page, new ExecParam()), FuggoKodtarAlairasFajta);
        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }
        KRT_KodtarFuggoseg kodtarfg = (KRT_KodtarFuggoseg)result.Record;

        Contentum.eAdmin.BaseUtility.KodtarFuggoseg.KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(kodtarfg.Adat);
        if (fuggosegek == null)
            return;

        if (fuggosegek.Items != null && fuggosegek.Items.Count > 0)
        {
            List<KRT_KodTarak> kodtarList = new List<KRT_KodTarak>();
            KRT_KodTarak kodtar = null;
            //Ügyirat jelleg meghatározása
            //string jelleg = string.Empty;
            string adathTipus = string.Empty;
            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            if (csatolmanyokArray != null)
            {
                search_csatolmanyok.Id.Value = GetIdListFromArray(csatolmanyokArray);
                search_csatolmanyok.Id.Operator = Query.Operators.inner;
                Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);

                if (result_csatGetAll.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_csatGetAll);
                    return;
                } else {
                    EREC_IraIratokSearch search_iratok = new EREC_IraIratokSearch();
                    EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                    search_iratok.Id.Value = GetIdListFromResult(result_csatGetAll, "IraIrat_Id");
                    search_iratok.Id.Operator = Query.Operators.inner;
                    Result result_iratGetAll = service_iratok.GetAll(UI.SetExecParamDefault(Page), search_iratok);
                    if (result_iratGetAll.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_iratGetAll);
                        return;
                    }
                    else
                    {
                        foreach (DataRow row in result_iratGetAll.Ds.Tables[0].Rows)
                        {
                            adathTipus = row["AdathordozoTipusa"].ToString();
                        }
                        /*EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        EREC_UgyUgyiratokSearch search_ugyiratok = new EREC_UgyUgyiratokSearch();

                        search_ugyiratok.Id.Value = GetIdListFromResult(result_iratGetAll, "Ugyirat_Id");
                        search_ugyiratok.Id.Operator = Query.Operators.inner;
                        Result result_ugyiratGetAll = service_ugyiratok.GetAll(UI.SetExecParamDefault(Page), search_ugyiratok);
                        if (result_ugyiratGetAll.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGetAll);
                            return;
                        }
                        else
                        {
                            foreach (DataRow row in result_ugyiratGetAll.Ds.Tables[0].Rows)
                            {
                                jelleg = row["Jelleg"].ToString();
                            }
                        }*/
                    }
                }
            }

            if (!string.IsNullOrEmpty(adathTipus))
            {

                /*string vezerlokodtarId = UgyiratJellegElektronikus;
                switch (jelleg)
                {
                    case "01": vezerlokodtarId = UgyiratJellegElektronikus;break;
                    case "02": vezerlokodtarId = UgyiratJellegPapir;break;
                    case "03": vezerlokodtarId = UgyiratJellegVegyes; break;
                }*/
                
                string vezerlokodtarId = AdatHordozoTipusElektronikus;
                switch (adathTipus)
                {
                    case "1": vezerlokodtarId = AdatHordozoTipusElektronikus; break;
                    case "0": vezerlokodtarId = AdatHordozoTipusPapir; break;
                    case "V": vezerlokodtarId = AdatHordozoTipusVegyes; break;
                }
                foreach (var item in fuggosegek.Items)
                {
                    //szűrés vezérlő küdtár alapján
                    if (item.VezerloKodTarId.Equals(vezerlokodtarId, StringComparison.CurrentCultureIgnoreCase))
                    {
                        kodtar = GetKodtarElem(UI.SetExecParamDefault(Page, new ExecParam()), fuggosegek.FuggoKodCsoportId, item.FuggoKodtarId);
                        if (kodtar != null/* && !string.IsNullOrEmpty(kodtar.Id) && kodtar.Id.Equals(Constants.FuggoKodtarAlairasFajta,StringComparison.CurrentCultureIgnoreCase)*/)
                        {
                            kodtarList.Add(kodtar);
                        }
                    }
                }
            }

            //kodtarList rendezése, hogy a dropdownlista tartalma megeggyezzen a korábbival
            kodtarList.OrderBy(X => X.Kod).ToList();
            foreach(KRT_KodTarak kt in kodtarList)
            {
                AlairasTipusa_TextBox.Items.Add(new ListItem(kt.Nev, kt.Kod));
            }
        }
    }

    /// <summary>
    /// GetKodtarElem
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="kodcsoportKod"></param>
    /// <param name="kodtarKod"></param>
    /// <returns></returns>
    private KRT_KodTarak GetKodtarElem(ExecParam execParam, string kodcsoportKod, string kodtarId)
    {
        Contentum.eAdmin.Service.KRT_KodTarakService ktService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
        ExecParam ktExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        ktExecParam.Record_Id = kodtarId;
        Result resultFind = ktService.Get(ktExecParam);
        if (resultFind.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultFind);
            return null;
        }
        return (KRT_KodTarak)resultFind.Record;
    }
    #endregion

    #region BLG 2597 - HALK aláírás utáni folyamatok
    [System.Web.Services.WebMethod]
    public static HALKStatusz GetHALKStatusz(string processId, string lastStatusGetTime)
    {
        try
        {
            const string dtFormat = "yyyy.MM.dd HH.mm.ss.fff";
            DateTime currentStatusGetTime = DateTime.Now;
            DateTime? lastStatusGetTimeDt = null;
            DateTime dtSeged;
            if (DateTime.TryParseExact(lastStatusGetTime, dtFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AllowWhiteSpaces
                    , out dtSeged))
            {
                lastStatusGetTimeDt = dtSeged;
            }

            string errorString = string.Empty;
            string statusString = string.Empty;

            bool finished = HALKProcessFinished(processId, out errorString, out statusString);
            
            var statusz = new HALKStatusz()
            {
                BackgroundProcessEnded = finished,
                BackgroundProcessError = errorString,
                BackgroundProcessStatus = statusString,
                StatusGetTime = currentStatusGetTime.ToString(dtFormat)
            };

            return statusz;
        }
        catch (Exception exc)
        {
            Logger.Error(exc.ToString());

            return new HALKStatusz()
            {
                BackgroundProcessError = exc.Message,
                BackgroundProcessEnded = true
            };
        }
    }

    public static bool HALKProcessFinished(string processId, out string errorString, out string statusString)
    {
        errorString = string.Empty;
        statusString = string.Empty;

        try
        {
            EREC_Alairas_FolyamatokService folyamatoService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();

            ExecParam ep = UI.SetExecParamDefault(HttpContext.Current.Session);
            ep.Record_Id = processId;

            Result resultAFGet = folyamatoService.Get(ep);
            if (!String.IsNullOrEmpty(resultAFGet.ErrorCode))
            {
                //hiba lépett fel, de a folyamat végére ért
                errorString = resultAFGet.ErrorMessage;
                return true;
            }
            EREC_Alairas_Folyamatok eREC_Alairas_Folyamatok = (EREC_Alairas_Folyamatok)resultAFGet.Record;
            //statusString = //Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS., eREC_Alairas_Folyamatok.AlairasStatus, ep, HttpContext.Current.Cache);  
            switch (eREC_Alairas_Folyamatok.AlairasStatus)
            {
                case KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.alairas_folyamatban: statusString = "Aláírás folyamatban..."; break;
                case KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.folyamat_rogzitve: statusString = "Folyamat rögzítve..."; break;
                case KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas: statusString = "Sikeres Aláírás..."; break;
                case KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas: statusString = "Sikertelen aláírás..."; break;
                default: statusString = "Ismeretlen állapot..."; break;
            }
            if (eREC_Alairas_Folyamatok.AlairasStatus == KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas || eREC_Alairas_Folyamatok.AlairasStatus == KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas)
            {
                return true;
            }
        }
        catch (Exception exc)
        {
            Logger.Error("DokumentumAlairas.aspx HALKProcessFinished(" + processId + ") error:" + exc.Message);
            errorString = exc.Message;
            return true;
        }

        return false;
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.ErrorPanel.Visible = false;

        if (!IsPostBack)
        {
            LoadFormComponents();
            FillAlairasTipusaDropDown();
        }
        Boolean hasEnabledFormat = true;
        if (KartyasAlairas)
        {
            hasEnabledFormat = false;
            if(checkFileFormat("PDF", KartyasAlairas))
            {
                hasEnabledFormat = true;
            }
            else if (checkFileFormat("KR", KartyasAlairas))
            {
                hasEnabledFormat = true;
            }
            else if (checkFileFormat("KRX", KartyasAlairas))
            {
                hasEnabledFormat = true;
            }
        }


        if (!hasEnabledFormat)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.AlairasFormatumHiba);
            ImageAlairas.Enabled = false;
        }
        else
        {
            ImageAlairas.Enabled = true;
        }

        #region BL_7148 - onclick előtt selected állítás
        if (!IsPostBack)
        {
            if (KartyasAlairasEnabled)
            {
                AlairasTipusa_TextBox.SelectedValue = "1";
            }
            else if (KozpontiAlairasEnabled)
            {
                AlairasTipusa_TextBox.SelectedValue = "2";
            }
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), "PKI_INTEGRACIO") == "Nem")
            {
                AlairasTipusa_TextBox.SelectedValue = "3";
            }
        }
        if (!KartyasAlairasEnabled)
        {
            DisableSelectedAlairasTipusItem("1");
        }
        if (!KozpontiAlairasEnabled)
        {
            DisableSelectedAlairasTipusItem("2");
        }
        if (checkFileFormat("KR", true))
        {
            DisableSelectedAlairasTipusItem("1");
            DisableSelectedAlairasTipusItem("2");
            DisableSelectedAlairasTipusItem("3");
        }
        else
        {
            DisableSelectedAlairasTipusItem("4");
        }

        //ha minden elem ki van kapcsolava, akkor + üres elem beszúrása
        try
        {
            int enabledElements = 0;

            foreach (ListItem li in AlairasTipusa_TextBox.Items)
            {
                if (li.Attributes["disabled"] != "disabled")
                {
                    enabledElements++;
                }
                else
                {
                    //ha kikapcsolt, de mégis ki van választva, akkor kiválasztás léptetése
                    if (li.Selected && AlairasTipusa_TextBox.SelectedIndex < AlairasTipusa_TextBox.Items.Count - 1)
                    {
                        AlairasTipusa_TextBox.SelectedIndex++;
                    }
                }
            }
            if (enabledElements == 0)
            {
                AlairasTipusa_TextBox.Items.Insert(0, new ListItem());
                AlairasTipusa_TextBox.SelectedIndex = 0;
            }
        }
        catch (Exception)
        {

        }
        #endregion

        FormHeader1.DisableModeLabel = true;

        ImageAlairas.OnClientClick = JavaScripts.SetDisableButtonOnClientClick(Page, ImageAlairas);
        ImageElutasitas.OnClientClick = JavaScripts.SetDisableButtonOnClientClick(Page, ImageElutasitas);
        if (PKI_VENDOR.ToUpper() == "HALK" && !String.IsNullOrEmpty(proc_Id) && KartyasAlairas)
        {
            var queryString = "Proc_Id=" + proc_Id + "&Felh_Id=" + FelhasznaloProfil.FelhasznaloId(Page) + "&FelhSz_Id=" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page) + "&Muvelet=Sign"; ;
            string url = UI.GetAppSetting("RunHalkAppUrl") + "?" + queryString;
            #region BUG 6380
            Boolean HALKEnabled = true;
            if (!String.IsNullOrEmpty(proc_Id))
            {
                List<IratCsatolmanyai> tetelek = GetFolyamatTetelek();

                if (tetelek != null)
                {
                    foreach (IratCsatolmanyai tetel in tetelek)
                    {
                        if (!IratAlairokcheck(tetel.IratId))
                        {
                            HALKEnabled = false;
                        }
                    }
                }
                else
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", "Hiányoznak a folyamat tételek!");
                    HALKEnabled = false;
                }
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", "Hiányzó folyamat Id!");
                HALKEnabled = false;
            }

            #endregion
            #region BLG 2597
            if (HALKEnabled)
            {
                halkProcessId.Value = proc_Id;
                //ImageAlairas.OnClientClick = String.Format("window.open('{0}');window.close();return false;",url);

                ImageAlairas.OnClientClick = String.Format("checkStatus();window.open('{0}');return false;", url);
            }
            else
            {
                ImageAlairas.Enabled = false;
            }
            #endregion
        }

        
    }

    #region BUG 4755 - csak a létező elemeket tilthatjuk le!
    private void DisableSelectedAlairasTipusItem(String itemId)
    {
        try
        {
            if (AlairasTipusa_TextBox.Items.FindByValue(itemId) != null)
            {
                AlairasTipusa_TextBox.Items.FindByValue(itemId).Attributes["disabled"] = "disabled";
            }
        }
        catch (Exception)
        {

        }
    }
    #endregion

    protected void Page_PreRender(object sender, EventArgs e)
    {
        AlairasTipusa_TextBox.ReadOnly = !AlairasTipusEnabled;

        if (KartyasAlairas && KartyasAlairasEnabled && PKI_VENDOR.ToUpper() == "NETLOCK")
        {
            NetlockSigner.AsyncPostBackUrl = this.ResolveClientUrl("~/DokumentumAlairas.aspx/IratSigned");
            NetlockSigner.LoadFiles(this.proc_Id, csatolmanyokArray);
            ImageAlairas.OnClientClick = NetlockSigner.SignJSFunction + ";return false;";
            NetlockSigner.Visible = true;
        }
        else
        {
            NetlockSigner.Visible = false;
        }

        AlertIktatas();
    }

    #region BUG 3638
    private Boolean checkFileFormat(String fileFormat, bool needCheck)
    {
        if (needCheck && csatolmanyokArray != null && csatolmanyokArray.Length != 0)
        {
            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            search_csatolmanyok.Id.Value = GetIdListFromArray(csatolmanyokArray);
            search_csatolmanyok.Id.Operator = Query.Operators.inner;
            Result result_csatGetAll = service_csatolmanyok.GetAllWithExtension(UI.SetExecParamDefault(Page), search_csatolmanyok);
            if (!result_csatGetAll.IsError)
            {
                foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                {
                    string fileName = row["FajlNev"].ToString();
                    string ext = Path.GetExtension(fileName).Replace(".", "").Trim();
                    //BUG 7296 - ha van legalább egy pdf, true -t ad vissza
                    if (ext.Equals(fileFormat, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }
    #endregion

    private void LoadFormComponents()
    {
        if (csatolmanyokArray == null || csatolmanyokArray.Length == 0)
        {
            //ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            //BUG 3638
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.TomegesAlairasNincsCsatolmany);
            MainPanel.Visible = false;
        }
        else
        {
            #region Csatolmányok lekérése

            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            StringBuilder sb_csatIds = new StringBuilder();
            for (int i = 0; i < csatolmanyokArray.Length; i++)
            {
                if (i == 0)
                {
                    sb_csatIds.Append("'" + csatolmanyokArray[i] + "'");
                }
                else
                {
                    sb_csatIds.Append(",'" + csatolmanyokArray[i] + "'");
                }
            }

            search_csatolmanyok.Id.Value = sb_csatIds.ToString();
            search_csatolmanyok.Id.Operator = Query.Operators.inner;

            Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);

            if (result_csatGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_csatGetAll);
                MainPanel.Visible = false;
                return;
            }

            if (result_csatGetAll.Ds.Tables[0].Rows.Count != csatolmanyokArray.Length)
            {
                // hiba a lekérdezés során:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a csatolmányok lekérdezése során!");
                MainPanel.Visible = false;
                return;
            }

            // Ellenőrzés, minden csatolmánynak ugyanaz-e az iratId-ja:
            string iratId = result_csatGetAll.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

            //Csak akkor van ellenőrzés, ha nem tömeges aláírás (nincs process Id)
            if (String.IsNullOrEmpty(proc_Id))
            {
                foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                {
                    string row_IraIrat_Id = row["IraIrat_Id"].ToString();
                    if (row_IraIrat_Id != iratId)
                    {
                        // hiba:
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a csatolmányok lekérdezése során!");
                        MainPanel.Visible = false;
                        return;
                    }
                }
            }
            else
            {
                List<string> iratIdList = new List<string>();
                foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                {
                    string row_IraIrat_Id = row["IraIrat_Id"].ToString();
                    if (!iratIdList.Contains(row_IraIrat_Id))
                        iratIdList.Add(row_IraIrat_Id);
                }
                IratIdList = iratIdList;
            }

            IratId = iratId;

            #endregion

            Alairo_FelhasznaloCsoportTextBox.Id_HiddenField = UI.SetExecParamDefault(Page).Felhasznalo_Id;
            Alairo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        }
    }

    /// <summary>
    /// Aláírás
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImageAlairas_Click(object sender, ImageClickEventArgs e)
    {
        if (EmptyValue)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", "Nincs kiválasztva az aláírás típusa!");
            return;
        }

        if (PKI_VENDOR.ToUpper() == "HALK" && KartyasAlairas)
        {
            return;
        }

        if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "MICROSEC")
        {
            MicroSigner1.iratIds = new List<string>();
            MicroSigner1.fileNames = new List<string>();
            MicroSigner1.externalLinks = new List<string>();
            MicroSigner1.recordIds = new List<string>();
            MicroSigner1.folyamatTetelekIds = new List<string>();
            MicroSigner1.csatolmanyokIds = new List<string>();
        }

        if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "AVDH")
        {
            AVDH1.iratIds = new List<string>();
            AVDH1.fileNames = new List<string>();
            AVDH1.externalLinks = new List<string>();
            AVDH1.recordIds = new List<string>();
            AVDH1.folyamatTetelekIds = new List<string>();
            AVDH1.csatolmanyokIds = new List<string>();
        }

        //if (KozpontiAlairasEnabled)
        //{
        //    if (csatolmanyokArray == null || csatolmanyokArray.Length == 0)
        //    {
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //        return;
        //    }
        //    else
        //    {
        //        #region Kiadmányozó szerepű aláíró csak akkor írhatja a alá, ha egyik iratpéldánya az iratnak sincs továbbitás alatt

        //        ExecParam ep = UI.SetExecParamDefault(Page);
        //        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        //        EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();

        //        search.FelhasznaloCsoport_Id_Alairo.Value = ep.Felhasznalo_Id;
        //        search.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
        //        search.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.and;
        //        search.Obj_Id.Value = IratId;
        //        search.Obj_Id.Operator = Query.Operators.equals;
        //        search.Obj_Id.GroupOperator = Query.Operators.and;
        //        search.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
        //        search.AlairoSzerep.Operator = Query.Operators.equals;
        //        search.TopRow = 1;

        //        Result result = service.GetAll(ep.Clone(), search); ;

        //        if (result.IsError || result.Ds == null)
        //        {

        //            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", "Hiba történt az aláírók lekérdezése közben!"));
        //        }

        //        bool kiadmanyozo = result.Ds.Tables[0].Rows.Count > 0;

        //        if (kiadmanyozo)
        //        {
        //            ErrorDetails errorDetails = null;
        //            Iratok.Statusz statusz = Iratok.GetAllapotById(IratId, ep, FormHeader1.ErrorPanel);
        //            if (!Iratok.KiadmonyozoAlairhatja(statusz, UI.SetExecParamDefault(Page), out errorDetails))
        //                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", errorDetails.Message));
        //        }
        //        #endregion

        //        //bernat.laszlo added               
        //        #region Aláírás
        //        System.Collections.Generic.List<string> csatList = new System.Collections.Generic.List<string>(csatolmanyokArray);
        //        bool ret = NETLOCK_ElektronikusAlairas(UI.SetExecParamDefault(Page), csatList);
        //        if (ret)
        //            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        //        return;
        //        #endregion
        //        //bernat.laszlo eddig
        //    }
        //}
        //else
        //{
        //    System.Collections.Generic.List<string> csatList = new System.Collections.Generic.List<string>(csatolmanyokArray);
        //    bool ret = NETLOCK_ElektronikusAlairas(UI.SetExecParamDefault(Page), csatList);
        //    if (ret)
        //        JavaScripts.RegisterCloseWindowClientScript(Page, true);
        //    return;
        //    //   UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        //}

        bool ret = true;

        if (!String.IsNullOrEmpty(proc_Id))
        {
            List<IratCsatolmanyai> tetelek = GetFolyamatTetelek();

            if (tetelek != null)
            {
                foreach (IratCsatolmanyai tetel in tetelek)
                {
                    ret = Alairas(tetel.IratId, tetel.CsatolmanyIds.ToArray(), tetel.FolyamatTetelIds);

                    if (!ret)
                    {
                        break;
                    }
                }
            }
            else
            {
                ret = false;
            }

            if (ret)
            {
                eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas);
            }
            else
            {
                eDocumentService.SetFolyamatStatus(UI.SetExecParamDefault(HttpContext.Current.Session), proc_Id, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas);
            }
        }
        else
        {
            ret = Alairas(IratId, csatolmanyokArray, null);
        }

        if (ret)
        {
            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "MICROSEC")
            {
                //BLG 6473 Server oldali aláírás külön véve
                if (KozpontiAlairas)
                {
                    MicroSigner1.megjegyzes = MegjegyzesTextBox.Text;
                    MicroSigner1.StartNewServerSideSession();
                    
                }
                else
                {
                    MicroSigner1.StartNewSigningSession();
                }
            }
            else
            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "AVDH")
            {
                AVDH1.StartNewSigningSession();
            }else
            {
                JavaScripts.RegisterCloseWindowClientScript(Page, true);
            }
        }
    }


    protected void ImageClose_ResultPanel_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.RegisterCloseWindowClientScript(Page, true);
        return;
    }

    /// <summary>
    /// Elutasítás
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImageElutasitas_Click(object sender, ImageClickEventArgs e)
    {
        //LZS - BLG_2961
        if (MegjegyzesLabel.Text == "Megjegyzés:")
        {
            MegjegyzesLabel.Text = "Elutasítás oka:"; //Contentum.eUtility.ForditasExpressionBuilder.GetForditas(MegjegyzesLabel.ID, "Elutasítás oka:", "DokumentumAlairas.aspx");
            ImageAlairas.Visible = false;
            ImageAlairas.Enabled = false;
            MegjegyzesTextBox.Focus();
            return;
        }
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_IraIratCsatolmanyModify))
        {
            bool ret = true;
            if (!String.IsNullOrEmpty(proc_Id))
            {
                List<IratCsatolmanyai> tetelek = GetFolyamatTetelek();

                if (tetelek != null)
                {
                    foreach (IratCsatolmanyai tetel in tetelek)
                    {
                        ret = Elutasitas(tetel.CsatolmanyIds.ToArray());

                        if (!ret)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    ret = false;
                }
            }
            else
            {
                ret = Elutasitas(csatolmanyokArray);
            }

            if (ret)
            {
                JavaScripts.RegisterCloseWindowClientScript(Page, true);
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }


    #region NETLOCK, MicroSec Elektronikus Aláírás metódusok
    protected bool ElektronikusAlairas(ExecParam execParam_Input, string iratId, System.Collections.Generic.List<string> csatolmanyokIdList, Dictionary<string, string> folyamatTetelIds)
    {
        if (csatolmanyokIdList.Count > 0)
        {
            System.Collections.Generic.List<string> dokumentumokIdList = new System.Collections.Generic.List<string>();
            Dictionary<string, string> dokCsatkDict = new Dictionary<string, string>();
            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            StringBuilder sb_csatIds = new StringBuilder();
            for (int i = 0; i < csatolmanyokIdList.Count; i++)
            {
                if (i == 0)
                {
                    sb_csatIds.Append("'" + csatolmanyokIdList[i] + "'");
                }
                else
                {
                    sb_csatIds.Append(",'" + csatolmanyokIdList[i] + "'");
                }
            }

            search_csatolmanyok.Id.Value = sb_csatIds.ToString();
            search_csatolmanyok.Id.Operator = Query.Operators.inner;

            Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);

            if (result_csatGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_csatGetAll);
                MainPanel.Visible = false;
                return false;
            }
            else
            {
                foreach (DataRow _row in result_csatGetAll.Ds.Tables[0].Rows)
                {
                    dokumentumokIdList.Add(_row["Dokumentum_Id"].ToString());
                    dokCsatkDict.Add(_row["Dokumentum_Id"].ToString(), _row["Id"].ToString());
                }
            }

            if (!ManualisAlairas)
            {
                Contentum.eDocument.Service.KRT_DokumentumokService svcDokumentumok = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
                schDokumentumok.Id.Value = Search.GetSqlInnerString(dokumentumokIdList.ToArray());
                schDokumentumok.Id.Operator = Query.Operators.inner;

                Result resDokumentumok = svcDokumentumok.GetAll(execParam_Input.Clone(), schDokumentumok);

                if (resDokumentumok.IsError)
                {
                    Logger.Error("Dokumentumok.GetAll hiba", execParam_Input, resDokumentumok);
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resDokumentumok);
                    return false;
                }

                foreach (DataRow row in resDokumentumok.Ds.Tables[0].Rows)
                {
                    string recordId = row["Id"].ToString();
                    string externalLink = String.Empty;
                    string filename = String.Empty;
                    string guid = String.Empty;
                    string currentVersion = String.Empty;
                    string documentSite = String.Empty;
                    string documentStore = String.Empty;
                    string documentFolder = String.Empty;
                    string version = row["VerzioJel"].ToString();
                    string formatum = row["Formatum"].ToString();

                    if (IsPkiFile(formatum))
                    {
                        try
                        {

                            #region GUID alapján externalLink lekérése
                            Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                            ExecParam ep = execParam_Input.Clone();

                            if (!string.IsNullOrEmpty(recordId))
                            {
                                ep.Record_Id = recordId;
                                char jogszint = 'O';

                                Result dokGetResult = dokService.GetWithRightCheck(ep, jogszint);
                                if (dokGetResult.IsError)
                                {
                                    throw new Contentum.eUtility.ResultException(dokGetResult);
                                }

                                externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;
                                filename = (dokGetResult.Record as KRT_Dokumentumok).FajlNev;
                                // BLG_1639 
                                // MP javaslatára kiszedve (nem használjuk később)
                                //documentStore = getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 3);
                                //documentSite = getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 2);
                                //documentFolder = getDataFromExternalInfo((dokGetResult.Record as KRT_Dokumentumok).External_Info, 4);
                                guid = ep.Record_Id;
                                currentVersion = (dokGetResult.Record as KRT_Dokumentumok).VerzioJel;
                            }
                            #endregion GUID alapján externalLink lekérése

                            //verziók kezelése
                            //ha van megadva verzió és nem az aktuális, akkor le kell kérni a verzió url-jét
                            if (!String.IsNullOrEmpty(version) && version != currentVersion)
                            {
                                ExecParam xpmVersion = UI.SetExecParamDefault(Page);
                                xpmVersion.Record_Id = guid;
                                Contentum.eDocument.Service.DocumentService serviceVersion = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();
                                Result resVersion = serviceVersion.GetDocumentVersionsFromMOSS(xpmVersion);

                                if (resVersion.IsError)
                                {
                                    Logger.Error(String.Format("Verzio ({0}) lekerese hiba!", version), xpmVersion, resVersion);
                                    throw new Contentum.eUtility.ResultException(resVersion);
                                }

                                bool isVersion = false;
                                foreach (DataRow rowVersion in resVersion.Ds.Tables[0].Rows)
                                {
                                    if (version == rowVersion["version"].ToString())
                                    {
                                        externalLink = rowVersion["url"].ToString();
                                        isVersion = true;
                                        break;
                                    }
                                }

                                if (!isVersion)
                                {
                                    Logger.Error(String.Format("A verzio ({0}) nem talalhato!", version));
                                    throw new Contentum.eUtility.ResultException(String.Format("A kért verzió ({0}) nem található!", version));
                                }
                            }

                            externalLink = HttpUtility.UrlPathEncode(externalLink);
                            string csatolmanyId = dokCsatkDict[recordId];

                            byte[] documentData;

                            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "NETLOCK")
                            {
                                #region NETLOCK Aláírás
                                var netlockService = new NetLockSignAssistService();
                                try
                                {
                                    var contentOriginalPdf = NetLockHelper.DownloadDocument(externalLink, UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
                                    documentData = netlockService.Sign(null, filename, contentOriginalPdf);
                                    eDocumentService.UploadSignedFile(execParam_Input.Clone(), iratId, recordId, documentSite, documentStore, documentFolder, filename, documentData);
                                }
                                catch (Exception x)
                                {
                                    var errorMessage = x.Message;
                                    Logger.Warn("NETLOCKSignatureService hiba: " + errorMessage);
                                    netlockService.ValidationReport.Dump();
                                    throw new Contentum.eUtility.ResultException("NETLOCKSignatureService hiba: " + errorMessage);
                                }

                                if (folyamatTetelIds != null)
                                {
                                    string folyamatTetelId = folyamatTetelIds[csatolmanyId];

                                    eDocumentService.SetFolyamatTetelStatus(UI.SetExecParamDefault(HttpContext.Current.Session), folyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas, String.Empty);
                                }
                                #endregion NETLOCK Aláírás
                            }
                            // BLG_1639
                            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "AVDH")
                            {
                                #region AVDH Aláírás

                                string AVDH_Mode = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("AVDH_MODE");

                                // TODO: fileméret ellenőrzés, elágaztatás                         
                                //string AVDH_URL = AVDH_DHSZ_SSL_URL + AVDH_PAdES;

                                Contentum.eUtility.AVDHSignatureService signatureService = new Contentum.eUtility.AVDHSignatureService();
                                signatureService.UploadPDF(filename, formatum, externalLink, UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));

                                if (signatureService.RaiseError)
                                {
                                    Logger.Warn("AVDHSignatureService hiba: " + signatureService.errorMessage);
                                    throw new Contentum.eUtility.ResultException(String.Format("AVDHSignatureService hiba: " + signatureService.errorMessage));
                                }

                                if (folyamatTetelIds != null)
                                {
                                    string folyamatTetelId = folyamatTetelIds[csatolmanyId];

                                    eDocumentService.SetFolyamatTetelNote(UI.SetExecParamDefault(HttpContext.Current.Session), folyamatTetelId, signatureService.UploadResponseXML);

                                }

                                if (AVDH_Mode == "NORMAL")
                                {

                                    string loginURL = signatureService.extractDataFromXMLNode("response", "avdhKauLoginRequestUrl", signatureService.UploadResponseXML);

                                    string downloadURL = signatureService.extractDataFromXMLNode("response", "avdhDownloadURL", signatureService.UploadResponseXML);
                                    //signatureService.DownloadSignedPDF(downloadURL);

                                    //documentData = Convert.FromBase64String(signatureService.signedDataBase64);
                                    //eDocumentService.UploadSignedFile(execParam_Input.Clone(), iratId, recordId, documentSite, documentStore, documentFolder, filename, documentData);


                                    if (folyamatTetelIds != null)
                                    {
                                        string folyamatTetelId = folyamatTetelIds[csatolmanyId];

                                        eDocumentService.SetFolyamatTetelStatus(UI.SetExecParamDefault(HttpContext.Current.Session), folyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikeres_alairas, String.Empty);
                                    }

                                    AVDH1.iratIds.Add(iratId);
                                    AVDH1.fileNames.Add(filename);
                                    AVDH1.externalLinks.Add(loginURL);
                                    AVDH1.recordIds.Add(recordId);
                                    AVDH1.csatolmanyokIds.Add(downloadURL);

                                }
                                else if (AVDH_Mode == "DHSZ")
                                { }



                                #endregion
                            }

                            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "MICROSEC")
                            {
                                MicroSigner1.iratIds.Add(iratId);
                                MicroSigner1.fileNames.Add(filename);
                                MicroSigner1.externalLinks.Add(externalLink);
                                MicroSigner1.recordIds.Add(recordId);
                                MicroSigner1.csatolmanyokIds.Add(csatolmanyId);
                            }


                            if (folyamatTetelIds != null)
                            {
                                string folyamatTetelId = folyamatTetelIds[csatolmanyId];
                                Logger.Debug("Folyamat tétel ID hozzáadva: " + folyamatTetelId);
                                MicroSigner1.folyamatTetelekIds.Add(folyamatTetelId);
                                AVDH1.folyamatTetelekIds.Add(folyamatTetelId);
                            }
                        }

                        catch (Contentum.eUtility.ResultException resEx)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resEx.GetResult());
                            if (folyamatTetelIds != null)
                            {
                                string csatolmanyId = dokCsatkDict[recordId];
                                string folyamatTetelId = folyamatTetelIds[csatolmanyId];

                                eDocumentService.SetFolyamatTetelStatus(UI.SetExecParamDefault(HttpContext.Current.Session), folyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, resEx.Message);
                            }
                            return false;
                        }
                        catch (Exception exp)
                        {
                            // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
                            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", Resources.Error.ErrorCode_52795);
                            Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
                            if (folyamatTetelIds != null)
                            {
                                string csatolmanyId = dokCsatkDict[recordId];
                                string folyamatTetelId = folyamatTetelIds[csatolmanyId];

                                eDocumentService.SetFolyamatTetelStatus(UI.SetExecParamDefault(HttpContext.Current.Session), folyamatTetelId, KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.sikertelen_alairas, exp.Message);
                            }
                            return false;
                        }
                        finally
                        {

                        }

                    }
                }
            }

            #region Aláírás Microsec
            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "MICROSEC")
            {
                return true;
            }
            #endregion
            // BLG_1639
            #region Aláírás AVDH
            if (!ManualisAlairas && PKI_VENDOR.ToUpper() == "AVDH")
            {
                return true;
            }
            #endregion
            //LZS - BLG_2961
            string megjegyzes = MegjegyzesTextBox.Text;
            Contentum.eDocument.Service.DokumentumAlairasService service = eDocumentService.ServiceFactory.GetDokumentumAlairasService();
            Result result = service.IratCsatolmanyokAlairas(UI.SetExecParamDefault(Page), csatolmanyokIdList.ToArray(), megjegyzes);

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return false;
            }
        }

        return true;
    }
    #endregion

    #region privát metódusok
    public string getDataFromExternalInfo(string External_Info, int part)
    {
        string[] parts = External_Info.Split(new string[1] { ";" }, StringSplitOptions.None);
        string _partReturn = "";
        if (parts.Length != 5)
        {
            throw new Exception("Hibás formátumú External_Info mező");
        }
        if (part < parts.Length)
        {
            _partReturn = parts[part];
        }
        /*
        spsMachineName = parts[0];
        rootSiteCollectionUrl = parts[1];
        doktarSitePath = parts[2];
        doktarDocLibPath = parts[3];
        doktarFolderPath = parts[4];
        fileName = fileName;
         */
        return _partReturn;
    }
    #endregion

    #region BUG 6380
    public bool EnableSignForLoggedUser(string iratId)
    {
        try
        {
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IratAlairokService service = null;
            EREC_IratAlairokSearch search = null;

            service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            search = (EREC_IratAlairokSearch)Search.GetSearchObject(Page, new EREC_IratAlairokSearch());

            search.Obj_Id.Value = iratId;
            search.Obj_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            search.AlairasDatuma.Operator = Contentum.eQuery.Query.Operators.isnull;


            search.OrderBy = "AlairasSorrend, Alairo_Nev ASC";
            Result res = service.GetAllWithExtension(ExecParam, search);
            if (res.IsError)
            {
                //7289 - error on errorpanel
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", res.ErrorMessage);
                Logger.Error("DokumentumAlairas.EnableSignForLoggedUser() error (errorcode: " + res.ErrorCode + "):" + res.ErrorMessage);
            }
            else
            {
                DataSet ds = res.Ds;
                string sorrend = null;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string alairasSorrend = row["AlairasSorrend"].ToString();
                    if (String.IsNullOrEmpty(sorrend))
                    {
                        sorrend = alairasSorrend;
                    }

                    if (alairasSorrend == sorrend)
                    {
                        string AlairoId = row["FelhasznaloCsoport_Id_Alairo"].ToString();
                        string HelyettesAlairoId = row["FelhaszCsop_Id_HelyettesAlairo"].ToString();
                        string LoggedUserId = ExecParam.Felhasznalo_Id;
                        if (!string.IsNullOrEmpty(AlairoId) && AlairoId.Equals(LoggedUserId, StringComparison.CurrentCultureIgnoreCase))
                        {
                            return true;
                        }
                        else if (!string.IsNullOrEmpty(HelyettesAlairoId) && HelyettesAlairoId.Equals(LoggedUserId, StringComparison.CurrentCultureIgnoreCase))
                        {
                            return true;
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            //7289 - error on errorpanel
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", ex.Message);
            Logger.Error("DokumentumAlairas.EnableSignForLoggedUser() error:" + ex.Message);
        }
        return false;
    }

    bool IratAlairokcheck(string iratId)
    {
        #region Csak a soron következő aláíró, vagy annak helyetese írhatja alá.
        if (!EnableSignForLoggedUser(iratId))
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", "Csak a soron következő aláíró írhat alá!");
            return false;
        }
        #endregion
        #region Kiadmányozó szerepű aláíró csak akkor írhatja a alá, ha egyik iratpéldánya az iratnak sincs továbbitás alatt

        ExecParam ep = UI.SetExecParamDefault(Page);
        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();

        search.FelhasznaloCsoport_Id_Alairo.Value = ep.Felhasznalo_Id;
        search.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
        search.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
        search.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;
        search.FelhaszCsop_Id_HelyettesAlairo.Value = ep.Felhasznalo_Id;
        search.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
        search.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
        search.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;
        search.Obj_Id.Value = iratId;
        search.Obj_Id.Operator = Query.Operators.equals;
        search.Obj_Id.GroupOperator = Query.Operators.and;
        search.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
        search.AlairoSzerep.Operator = Query.Operators.equals;
        search.TopRow = 1;

        Result result = service.GetAll(ep.Clone(), search);

        if (result.IsError || result.Ds == null)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", "Hiba történt az aláírók lekérdezése közben!"));
            return false;
        }

        bool kiadmanyozo = result.Ds.Tables[0].Rows.Count > 0;

        if (kiadmanyozo)
        {
            ErrorDetails errorDetails = null;
            Iratok.Statusz statusz = Iratok.GetAllapotById(iratId, ep, FormHeader1.ErrorPanel);
            if (!Iratok.KiadmonyozoAlairhatja(statusz, UI.SetExecParamDefault(Page), out errorDetails))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", errorDetails.Message));
                return false;
            }
        }
        #endregion
        return true;
    }
    #endregion

    bool Alairas(string iratId, string[] csatolmanyokArray, Dictionary<string, string> folyamatTetelIds)
    {
        if (AlairasEnabled)
        {
            if (csatolmanyokArray == null || csatolmanyokArray.Length == 0)
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return false;
            }
            else
            {
                //#region Kiadmányozó szerepű aláíró csak akkor írhatja a alá, ha egyik iratpéldánya az iratnak sincs továbbitás alatt

                //ExecParam ep = UI.SetExecParamDefault(Page);
                //EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
                //EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();

                //search.FelhasznaloCsoport_Id_Alairo.Value = ep.Felhasznalo_Id;
                //search.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
                //search.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
                //search.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;
                //search.FelhaszCsop_Id_HelyettesAlairo.Value = ep.Felhasznalo_Id;
                //search.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
                //search.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
                //search.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;
                //search.Obj_Id.Value = iratId;
                //search.Obj_Id.Operator = Query.Operators.equals;
                //search.Obj_Id.GroupOperator = Query.Operators.and;
                //search.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
                //search.AlairoSzerep.Operator = Query.Operators.equals;
                //search.TopRow = 1;

                //Result result = service.GetAll(ep.Clone(), search); ;

                //if (result.IsError || result.Ds == null)
                //{
                //    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", "Hiba történt az aláírók lekérdezése közben!"));
                //    return false;
                //}

                //bool kiadmanyozo = result.Ds.Tables[0].Rows.Count > 0;

                //if (kiadmanyozo)
                //{
                //    ErrorDetails errorDetails = null;
                //    Iratok.Statusz statusz = Iratok.GetAllapotById(iratId, ep, FormHeader1.ErrorPanel);
                //    if (!Iratok.KiadmonyozoAlairhatja(statusz, UI.SetExecParamDefault(Page), out errorDetails))
                //    {
                //        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", errorDetails.Message));
                //        return false;
                //    }
                //}
                //#endregion
                if (IratAlairokcheck(iratId)) {
                    //bernat.laszlo added               
                    #region Aláírás
                    System.Collections.Generic.List<string> csatList = new System.Collections.Generic.List<string>(csatolmanyokArray);
                    bool ret = ElektronikusAlairas(UI.SetExecParamDefault(Page), iratId, csatList, folyamatTetelIds);
                    return ret;
                    #endregion
                    //bernat.laszlo eddig
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            System.Collections.Generic.List<string> csatList = new System.Collections.Generic.List<string>(csatolmanyokArray);
            bool ret = ElektronikusAlairas(UI.SetExecParamDefault(Page), iratId, csatList, folyamatTetelIds);
            return ret;
            //   UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }

    bool Elutasitas(string[] csatolmanyokArray)
    {
        if (csatolmanyokArray == null || csatolmanyokArray.Length == 0)
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return false;
        }
        else
        {
            #region Aláírás elutasítása

            EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();

            string csatolmanyId = csatolmanyokArray[0];
            string megjegyzes = ((String.IsNullOrEmpty(MegjegyzesTextBox.Text)) ? "" : MegjegyzesTextBox.Text);

            Result result = service.AlairasElutasitasByCsatolmanyId(UI.SetExecParamDefault(Page)
                , csatolmanyId, megjegyzes);

            if (result.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return false;
            }

            #endregion
        }

        return true;
    }
    List<IratCsatolmanyai> GetFolyamatTetelek()
    {
        ExecParam xpm = UI.SetExecParamDefault(Page);
        EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
        EREC_Alairas_Folyamat_TetelekSearch search = new EREC_Alairas_Folyamat_TetelekSearch();
        search.AlairasFolyamat_Id.Value = proc_Id;
        search.AlairasFolyamat_Id.Operator = Query.Operators.equals;

        Result res = service.GetAll(xpm, search);

        if (res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
            return null;
        }
        else
        {
            Dictionary<string, IratCsatolmanyai> dict = new Dictionary<string, IratCsatolmanyai>();
            List<IratCsatolmanyai> list = new List<IratCsatolmanyai>();
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string iratId = row["IraIrat_Id"].ToString();
                string csatolmanyId = row["Csatolmany_Id"].ToString();

                IratCsatolmanyai iratCsat;
                if (!dict.ContainsKey(iratId))
                {
                    iratCsat = new IratCsatolmanyai(iratId);
                    dict.Add(iratId, iratCsat);
                    list.Add(iratCsat);
                }
                else
                {
                    iratCsat = dict[iratId];
                }

                iratCsat.CsatolmanyIds.Add(csatolmanyId);
                iratCsat.FolyamatTetelIds.Add(csatolmanyId, id);
            }

            return list;
        }
    }
    class IratCsatolmanyai
    {
        public string IratId { get; set; }

        public List<string> CsatolmanyIds { get; set; }

        public Dictionary<string, string> FolyamatTetelIds { get; set; }

        public IratCsatolmanyai(string iratId)
        {
            this.IratId = iratId;
            CsatolmanyIds = new List<string>();
            FolyamatTetelIds = new Dictionary<string, string>();
        }
    }

    bool IsPkiFile(string formatum)
    {
        if (!String.IsNullOrEmpty(formatum))
        {
            string pkiFileTypes = Rendszerparameterek.Get(Page, "PKI_FILETYPES");

            if (String.IsNullOrEmpty(pkiFileTypes))
            {
                pkiFileTypes = "pdf";
            }

            if (pkiFileTypes.ToLower().Contains(formatum.ToLower()))
                return true;
        }

        return false;
    }
    static Result SetFolyamatTetelStatus(string id, string status, string hiba)
    {
        if (String.IsNullOrEmpty(id))
            return null;

        Result res = new Result();
        try
        {
            ExecParam xpm = UI.SetExecParamDefault(HttpContext.Current.Session);
            EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
            xpm.Record_Id = id;

            res = service.Get(xpm);

            if (!res.IsError)
            {
                EREC_Alairas_Folyamat_Tetelek record = res.Record as EREC_Alairas_Folyamat_Tetelek;

                record.Updated.SetValueAll(false);
                record.Base.Updated.SetValueAll(false);
                record.Base.Updated.Ver = true;

                record.AlairasStatus = status;
                record.Updated.AlairasStatus = true;

                record.Hiba = hiba;
                record.Updated.Hiba = true;

                res = service.Update(xpm, record);

            }
        }
        catch (Exception ex)
        {
            res = Contentum.eUtility.ResultException.GetResultFromException(ex);
        }

        return res;
    }

    static Result SetFolyamatStatus(string id, string status)
    {
        if (String.IsNullOrEmpty(id))
            return null;

        Result res = new Result();
        try
        {
            ExecParam xpm = UI.SetExecParamDefault(HttpContext.Current.Session);
            EREC_Alairas_FolyamatokService service = eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();
            xpm.Record_Id = id;

            res = service.Get(xpm);

            if (!res.IsError)
            {
                EREC_Alairas_Folyamatok record = res.Record as EREC_Alairas_Folyamatok;

                record.Updated.SetValueAll(false);
                record.Base.Updated.SetValueAll(false);
                record.Base.Updated.Ver = true;

                record.AlairasStatus = status;
                record.Updated.AlairasStatus = true;

                record.AlairasStopped = DateTime.Now.ToString();
                record.Updated.AlairasStopped = true;

                res = service.Update(xpm, record);

            }
        }
        catch (Exception ex)
        {
            res = Contentum.eUtility.ResultException.GetResultFromException(ex);
        }

        return res;
    }

    static Result SetFolyamatTetelStatus(string procId, string iratId, string csatolmanyId, string status, string hiba)
    {
        if (String.IsNullOrEmpty(procId) || String.IsNullOrEmpty(iratId) || String.IsNullOrEmpty(csatolmanyId))
            return null;

        Result res = new Result();
        try
        {
            ExecParam xpm = UI.SetExecParamDefault(HttpContext.Current.Session);
            EREC_Alairas_Folyamat_TetelekService service = eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
            EREC_Alairas_Folyamat_TetelekSearch search = new EREC_Alairas_Folyamat_TetelekSearch();
            search.AlairasFolyamat_Id.Value = procId;
            search.AlairasFolyamat_Id.Operator = Query.Operators.equals;
            search.IraIrat_Id.Value = iratId;
            search.IraIrat_Id.Operator = Query.Operators.equals;
            search.Csatolmany_Id.Value = csatolmanyId;
            search.Csatolmany_Id.Operator = Query.Operators.equals;

            res = service.GetAll(xpm, search);

            if (!res.IsError)
            {
                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    EREC_Alairas_Folyamat_Tetelek record = new EREC_Alairas_Folyamat_Tetelek();

                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);

                    record.Base.Ver = row["Ver"].ToString();
                    record.Base.Updated.Ver = true;

                    record.AlairasStatus = status;
                    record.Updated.AlairasStatus = true;

                    record.Hiba = hiba;
                    record.Updated.Hiba = true;

                    xpm.Record_Id = row["Id"].ToString();


                    res = service.Update(xpm, record);
                }

            }
        }
        catch (Exception ex)
        {
            res = Contentum.eUtility.ResultException.GetResultFromException(ex);
        }

        return res;
    }

    bool IsKiadmanyozo()
    {
        ExecParam ep = UI.SetExecParamDefault(Page);
        EREC_IratAlairokService service = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        EREC_IratAlairokSearch search = new EREC_IratAlairokSearch();

        search.FelhasznaloCsoport_Id_Alairo.Value = ep.Felhasznalo_Id;
        search.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
        search.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
        search.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;
        search.FelhaszCsop_Id_HelyettesAlairo.Value = ep.Felhasznalo_Id;
        search.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
        search.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
        search.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;
        search.Obj_Id.Value = IratId;
        search.Obj_Id.Operator = Query.Operators.equals;
        search.Obj_Id.GroupOperator = Query.Operators.and;
        search.AlairoSzerep.Value = KodTarak.ALAIRO_SZEREP.Kiadmanyozo;
        search.AlairoSzerep.Operator = Query.Operators.equals;
        search.TopRow = 1;

        Result result = service.GetAll(ep.Clone(), search); ;

        if (result.IsError || result.Ds == null)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", "Hiba történt az aláírók lekérdezése közben!"));
            return false;
        }

        bool kiadmanyozo = result.Ds.Tables[0].Rows.Count > 0;
        return kiadmanyozo;
    }

    bool IsMunkairat()
    {
        ExecParam ep = UI.SetExecParamDefault(Page);
        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ep.Record_Id = IratId;

        Result result = service.Get(ep);

        if (result.IsError)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", string.Format("alert('{0}'); return false;", "Hiba történt az irat lekérdezése közben!"));
            return false;
        }

        EREC_IraIratok iratObj = result.Record as EREC_IraIratok;
        return Iratok.Munkaanyag(iratObj);
    }

    void AlertIktatas()
    {
        if (String.IsNullOrEmpty(IratId))
            return;

        if (Rendszerparameterek.GetBoolean(Page, "MUNKAIRAT_AUTO_IKTATAS_KIADMANYOZASKOR", false))
        {
            if (IsMunkairat() && IsKiadmanyozo())
            {
                ImageAlairas.OnClientClick = "alert('Figyelem! A munkairat beiktatásra fog kerülni!');" + ImageAlairas.OnClientClick;
            }
        }
    }

    #region NetlockSignAssist

    [System.Web.Services.WebMethod]
    public static string IratSigned(eRecordComponent_NetlockSignAssistSigner.IratFiles signedIrat)
    {
        return eRecordComponent_NetlockSignAssistSigner.IratSigned(signedIrat);
    }

    #endregion

}
