﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DokumentumAlairasVisszavonas.aspx.cs" Inherits="DokumentumAlairasVisszavonas" Title="Aláírás visszavonás megerősítése" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register src="Component/FelhasznaloCsoportTextBox.ascx" tagname="FelhasznaloCsoportTextBox" tagprefix="uc2" %>
<%@ Register Src="~/eRecordComponent/JavaSigner.ascx" TagName="JavaSigner" TagPrefix="uc" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,DokumentumAlairasFormHeaderTitle %>" />
    <asp:HiddenField ID="hfIratId" runat="server" />
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <br />
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    
                                    <asp:Label ID="Label1" runat="server" Text="Kijelölt aláíró:"></asp:Label>
                                    
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc2:FelhasznaloCsoportTextBox ID="Alairo_FelhasznaloCsoportTextBox" runat="server" ViewMode="true" ReadOnly="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="Aláíró szerepe:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                <uc3:KodtarakDropDownList Visible ="true" ReadOnly = "true" ID="AlairoSzerepDropDownList" runat="server"/>                                
                                </td>                                
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label3" runat="server" Text="Aláírás állapota:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                <uc3:KodtarakDropDownList Visible ="true" ReadOnly = "true" ID="AlairasAllapotKodtarakDropDownList" runat="server"/>                                
                                </td>                                
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="VisszavonasIndokaLabel" runat="server" Text="Visszavonás Indoka:" ></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">                                
                                <asp:TextBox  ID="VisszavonasIndokaTextBox" runat="server" class="mrUrlapInput"></asp:TextBox>
                                </td>
                             </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;</td>
                                <td class="mrUrlapMezo">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <table style="width: 60%;">
                        <tr>
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="1" ID="ImageRendben" runat="server" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                    onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')"
                                    OnClick="ImageRendben_Click" CommandName="AlairasVisszavonas" />
                            </td>                           
                            <td style="text-align: center;">
                                <asp:ImageButton TabIndex="6" ID="ImageCancel" runat="server" ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                    onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                    OnClientClick="window.close(); return false;" CommandName="Cancel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>        
    </asp:Panel>
    
        <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
            <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">
                    <asp:Label ID="Label_Result" runat="server" Text="Az aláírás sikeresen végrehajtódott."></asp:Label>
                </div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageClose_ResultPanel" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                CommandName="Close" OnClick="ImageClose_ResultPanel_Click" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
        </asp:Panel> 
</asp:Content>
