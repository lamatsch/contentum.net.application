﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.Text;
using System.Web.UI;


public partial class DokumentumAlairasVisszavonas : Contentum.eUtility.UI.PageBase
{
    private const string FunkcioKod_IraIratCsatolmanyModify = "IraIratCsatolmanyModify";
    
    private string csatolmanyId = String.Empty; // Id-k összefűzve
    private string[] csatolmanyokArray;

    private string AlairokId;

    string IratId
    {
        get
        {
            return hfIratId.Value;
        }
        set
        {
            hfIratId.Value = value;
        }
    }    

    bool KozpontiAlairasEnabled
    {
        get
        {
            return FunctionRights.GetFunkcioJog(Page, "KozpontiElektronikusAlairas") ;
        }
    }

    bool KartyasAlairasEnabled
    {
        get
        {
            return FunctionRights.GetFunkcioJog(Page, "KartyasElektronikusAlairas");
        }
    }

    bool AlairasTipusEnabled
    {
        get
        {
            return KozpontiAlairasEnabled && KartyasAlairasEnabled ;
        }
    }

    bool AlairasEnabled
    {
        get
        {
            return KozpontiAlairasEnabled || KartyasAlairasEnabled;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
      
        // Jogosultságellenőrzés:
        if (!AlairasEnabled)
        {
            FunctionRights.RedirectErrorPage(Page);
        }

        csatolmanyId = Request.QueryString.Get(QueryStringVars.CsatolmanyId);

        if (!String.IsNullOrEmpty(csatolmanyId))
        {
            // csatolmány Id-k szétszedése, ha több is volt:
            csatolmanyokArray = csatolmanyId.Split(',');
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadFormComponents();
            VisszavonasIndokaLabel.Visible = true;
            VisszavonasIndokaTextBox.Visible = true;

        }
        
        FormHeader1.DisableModeLabel = true;

        ImageRendben.OnClientClick = JavaScripts.SetDisableButtonOnClientClick(Page, ImageRendben);        
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {        
    }

    private void LoadFormComponents()
    {
        if (csatolmanyokArray == null || csatolmanyokArray.Length == 0)
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            #region Csatolmányok lekérése

            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            StringBuilder sb_csatIds = new StringBuilder();
            for (int i = 0; i < csatolmanyokArray.Length; i++)
            {
                if (i == 0)
                {
                    sb_csatIds.Append("'" + csatolmanyokArray[i] + "'");
                }
                else
                {
                    sb_csatIds.Append(",'" + csatolmanyokArray[i] + "'");
                }
            }

            search_csatolmanyok.Id.Value = sb_csatIds.ToString();
            search_csatolmanyok.Id.Operator = Query.Operators.inner;

            Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);

            if (result_csatGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_csatGetAll);
                MainPanel.Visible = false;
                return;
            }

            if (result_csatGetAll.Ds.Tables[0].Rows.Count != csatolmanyokArray.Length)
            {
                // hiba a lekérdezés során:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a csatolmányok lekérdezése során!");
                MainPanel.Visible = false;
                return;
            }


            #endregion

            // Ellenőrzés, minden csatolmánynak ugyanaz-e az iratId-ja:
            string iratId = result_csatGetAll.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

            foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
            {
                string row_IraIrat_Id = row["IraIrat_Id"].ToString();
                if (row_IraIrat_Id != iratId)
                {
                    // hiba:
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a csatolmányok lekérdezése során!");
                    MainPanel.Visible = false;
                    return;
                }
            }

            IratId = iratId;

            #region Aláírás rekord megkeresése
            EREC_IratAlairokService service_alairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            EREC_IratAlairokSearch search_alairok = new EREC_IratAlairokSearch();
            ExecParam execParam_alairok = UI.SetExecParamDefault(Page);

            search_alairok.Obj_Id.Value = iratId;
            search_alairok.Obj_Id.Operator = Query.Operators.equals;
            search_alairok.Obj_Id.GroupOperator = Query.Operators.and;
            search_alairok.FelhasznaloCsoport_Id_Alairo.Value = execParam_alairok.Felhasznalo_Id;
            search_alairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
            search_alairok.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
            search_alairok.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;
            search_alairok.FelhaszCsop_Id_HelyettesAlairo.Value = execParam_alairok.Felhasznalo_Id;
            search_alairok.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
            search_alairok.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
            search_alairok.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;
            search_alairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
            search_alairok.Allapot.Operator = Query.Operators.notequals;
            search_alairok.TopRow = 1;

            Result res_alairok = service_alairok.GetAll(execParam_alairok, search_alairok);

            if (res_alairok.IsError)
            {
                if (IsPostBack)
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res_alairok);                
            }
            if (res_alairok.Ds.Tables[0].Rows.Count < 1)
            {
                
                    FormHeader1.ErrorPanel.ErrorMessage = "Nincs ilyen előjegyzett aláíró bejegyezve!";
                    FormHeader1.ErrorPanel.Visible = !FormHeader1.ErrorPanel.Visible;
                    return;
            }

            #endregion

            DataRow AlairokRow =res_alairok.Ds.Tables[0].Rows[0]; 
            AlairokId = AlairokRow["Id"].ToString();            
            
            Alairo_FelhasznaloCsoportTextBox.Id_HiddenField = UI.SetExecParamDefault(Page).Felhasznalo_Id;
            Alairo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            AlairoSzerepDropDownList.FillWithOneValue("ALAIRO_SZEREP", AlairokRow["AlairoSzerep"].ToString(), FormHeader1.ErrorPanel);
            AlairasAllapotKodtarakDropDownList.FillWithOneValue("IRATALAIRAS_ALLAPOT", AlairokRow["Allapot"].ToString(), FormHeader1.ErrorPanel);                       

        }
    }

    /// <summary>
    /// AláírásVisszavonása
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ImageRendben_Click(object sender, ImageClickEventArgs e)
    {
        if (csatolmanyokArray == null || csatolmanyokArray.Length == 0)
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return;
        }
        #region Csatolmányok lekérése

        EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

        EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

        StringBuilder sb_csatIds = new StringBuilder();
        for (int i = 0; i < csatolmanyokArray.Length; i++)
        {
            if (i == 0)
            {
                sb_csatIds.Append("'" + csatolmanyokArray[i] + "'");
            }
            else
            {
                sb_csatIds.Append(",'" + csatolmanyokArray[i] + "'");
            }
        }

        search_csatolmanyok.Id.Value = sb_csatIds.ToString();
        search_csatolmanyok.Id.Operator = Query.Operators.inner;

        Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);

        if (result_csatGetAll.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_csatGetAll);
            MainPanel.Visible = false;
            return;
        }

        if (result_csatGetAll.Ds.Tables[0].Rows.Count != csatolmanyokArray.Length)
        {
            // hiba a lekérdezés során:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a csatolmányok lekérdezése során!");
            MainPanel.Visible = false;
            return;
        }


        #endregion

        // Ellenőrzés, minden csatolmánynak ugyanaz-e az iratId-ja:
        string iratId = result_csatGetAll.Ds.Tables[0].Rows[0]["IraIrat_Id"].ToString();

        foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
        {
            string row_IraIrat_Id = row["IraIrat_Id"].ToString();
            if (row_IraIrat_Id != iratId)
            {
                // hiba:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a csatolmányok lekérdezése során!");
                MainPanel.Visible = false;
                return;
            }
        }

        IratId = iratId;

        #region Aláírás rekord megkeresése
        EREC_IratAlairokService service_alairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
        EREC_IratAlairokSearch search_alairok = new EREC_IratAlairokSearch();
        ExecParam execParam_alairok = UI.SetExecParamDefault(Page);

        search_alairok.Obj_Id.Value = iratId;
        search_alairok.Obj_Id.Operator = Query.Operators.equals;
        search_alairok.Obj_Id.GroupOperator = Query.Operators.and;
        search_alairok.FelhasznaloCsoport_Id_Alairo.Value = execParam_alairok.Felhasznalo_Id;
        search_alairok.FelhasznaloCsoport_Id_Alairo.Operator = Query.Operators.equals;
        search_alairok.FelhasznaloCsoport_Id_Alairo.Group = "alairo";
        search_alairok.FelhasznaloCsoport_Id_Alairo.GroupOperator = Query.Operators.or;
        search_alairok.FelhaszCsop_Id_HelyettesAlairo.Value = execParam_alairok.Felhasznalo_Id;
        search_alairok.FelhaszCsop_Id_HelyettesAlairo.Operator = Query.Operators.equals;
        search_alairok.FelhaszCsop_Id_HelyettesAlairo.Group = "alairo";
        search_alairok.FelhaszCsop_Id_HelyettesAlairo.GroupOperator = Query.Operators.or;
        search_alairok.Allapot.Value = KodTarak.IRATALAIRAS_ALLAPOT.Alairando;
        search_alairok.Allapot.Operator = Query.Operators.notequals;
        search_alairok.OrderBy = "AlairasSorrend DESC";
        search_alairok.TopRow = 1;

        Result res_alairok = service_alairok.GetAll(execParam_alairok, search_alairok);

        if (res_alairok.IsError)
        {
            if (IsPostBack)
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res_alairok);
        }
        if (res_alairok.Ds.Tables[0].Rows.Count < 1)
        {

            FormHeader1.ErrorPanel.ErrorMessage = "Nincs ilyen előjegyzett aláíró bejegyezve!";
            FormHeader1.ErrorPanel.Visible = !FormHeader1.ErrorPanel.Visible;
            return;
        }

        #endregion

        DataRow AlairokRow = res_alairok.Ds.Tables[0].Rows[0];
        AlairokId = AlairokRow["Id"].ToString();            

        System.Collections.Generic.List<string> csatList = new System.Collections.Generic.List<string>(csatolmanyokArray);
        bool ret = AlairasVisszavonas(UI.SetExecParamDefault(Page), csatList,iratId,AlairokId);

        //Minden oké
        if (ret)
        {
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
        else
            return;
    }


    protected void ImageClose_ResultPanel_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.RegisterCloseWindowClientScript(Page, true);
        return;
    }   
        
    protected bool AlairasVisszavonas(ExecParam execParam_Input, System.Collections.Generic.List<string> csatolmanyokIdList, string IratId, string AlairokId)
    {
        if (csatolmanyokIdList.Count > 0)
        {
            #region Csatolmányok lekérése
            System.Collections.Generic.List<string> dokumentumokIdList = new System.Collections.Generic.List<string>();
            EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();

            EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

            StringBuilder sb_csatIds = new StringBuilder();
            for (int i = 0; i < csatolmanyokIdList.Count; i++)
            {
                if (i == 0)
                {
                    sb_csatIds.Append("'" + csatolmanyokIdList[i] + "'");
                }
                else
                {
                    sb_csatIds.Append(",'" + csatolmanyokIdList[i] + "'");
                }
            }

            search_csatolmanyok.Id.Value = sb_csatIds.ToString();
            search_csatolmanyok.Id.Operator = Query.Operators.inner;

            Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);

            if (result_csatGetAll.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_csatGetAll);
                MainPanel.Visible = false;
                return false;
            }

            else
            {
                foreach (DataRow _row in result_csatGetAll.Ds.Tables[0].Rows)
                {
                    dokumentumokIdList.Add(_row["Dokumentum_Id"].ToString());
                }
            }
            #endregion

            #region Dokumentumok lekérése
            Contentum.eDocument.Service.KRT_DokumentumokService svcDokumentumok = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            KRT_DokumentumokSearch schDokumentumok = new KRT_DokumentumokSearch();
            schDokumentumok.Id.Value = Search.GetSqlInnerString(dokumentumokIdList.ToArray());
            schDokumentumok.Id.Operator = Query.Operators.inner;

            Result resDokumentumok = svcDokumentumok.GetAll(execParam_Input.Clone(), schDokumentumok);

            if (resDokumentumok.IsError)
            {
                Logger.Error("Dokumentumok.GetAll hiba", execParam_Input, resDokumentumok);
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resDokumentumok);
                return false;
            }
            #endregion

            foreach (DataRow row in resDokumentumok.Ds.Tables[0].Rows)
            {
                string recordId = row["Id"].ToString();                               

                try
                {
                    #region Aláírás visszaírása a csatolmánynál
                    ExecParam docExecParam = UI.SetExecParamDefault(Page);
                    docExecParam.Record_Id = recordId;
                    Result dokResult = svcDokumentumok.Get(docExecParam);

                    if (!dokResult.IsError)
                    {
                        KRT_Dokumentumok docRecord = (KRT_Dokumentumok)dokResult.Record;
                        docRecord.Updated.SetValueAll(false);
                        docRecord.Base.Updated.SetValueAll(false);
                        docRecord.Base.Updated.Ver = true;

                        docRecord.AlairasFelulvizsgalat = DateTime.Now.ToString();
                        docRecord.Updated.AlairasFelulvizsgalat = true;

                        docRecord.ElektronikusAlairas = string.Empty;
                        docRecord.Updated.ElektronikusAlairas = true;

                        Result result_krt_doc_alairas = svcDokumentumok.Update(docExecParam, docRecord);
                        if (!String.IsNullOrEmpty(result_krt_doc_alairas.ErrorCode))
                        {
                            Logger.Error(String.Format("KRT_Dokumentumok update hiba: " + result_krt_doc_alairas.ErrorMessage));
                            throw new Contentum.eUtility.ResultException(String.Format("KRT_Dokumentumok update hiba: " + result_krt_doc_alairas.ErrorMessage));
                        }
                                
                    }
                    #endregion                                        
                }
                catch (Contentum.eUtility.ResultException resEx)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resEx.GetResult());
                    return false;
                }
                catch (Exception exp)
                {
                    // A kért dokumentum nem érhető el! Kérem, próbálja meg később.
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", exp.Message);
                    Logger.Warn("GetDocumentumContent hiba: " + exp.Message);
                    return false;
                }
                finally
                {

                }
            }//foreach

            #region Aláírás visszavonása (aláírók)
            ExecParam execParam = UI.SetExecParamDefault(Page);
            EREC_IratAlairokService svcAlairok = eRecordService.ServiceFactory.GetEREC_IratAlairokService();
            execParam.Record_Id = AlairokId;
            Result result = svcAlairok.AlairasVisszavonas(execParam,IratId,VisszavonasIndokaTextBox.Text);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return false;
            }
            #endregion

        }

        return true;
    }
    
   
}
    