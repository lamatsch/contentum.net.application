﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class DokumentumGeneralas : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string iratId = Request.QueryString.Get(QueryStringVars.IratId);
        string dokumentumId = Request.QueryString.Get(QueryStringVars.DokumentumId);

        if (!String.IsNullOrEmpty(iratId) && !String.IsNullOrEmpty(dokumentumId))
        {
            EREC_IraIratokService iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page);
            Result iratokResult = iratokService.GetForSablon(execParam, iratId);

            if (iratokResult.IsError)
                return;

            KRT_DokumentumokService dokService = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
            ExecParam dokExecParam = execParam.Clone();
            dokExecParam.Record_Id = dokumentumId;

            Result dokResult = dokService.Get(dokExecParam);

            if (dokResult.IsError)
                return;

            string externalLink = (dokResult.Record as KRT_Dokumentumok).External_Link;
            byte[] sablon = GetDocContent(externalLink);
            string fileName = GetFileName();
            Generalas(sablon, iratokResult.Ds, fileName);
        }
    }

    string GetFileName()
    {
        string fileName = "doku.docx";

        string iratId = Request.QueryString.Get(QueryStringVars.IratId);
        EREC_IraIratokService iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = iratId;
        Result iratokResult = iratokService.Get(execParam);

        if (!iratokResult.IsError)
        {
            string azonosito = (iratokResult.Record as EREC_IraIratok).Azonosito;
            fileName = String.Format("{0}.docx", azonosito);
        }

        return fileName;
    }

    byte[] GetDocContent(string externalLink)
    {
        using (WebClient wc = new WebClient())
        {
            wc.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
            return wc.DownloadData(externalLink);
        }
    }

     void Generalas(byte[] sablon, DataSet iratAdatok, string fileName)
    {
        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        Result res = tms.DokumentumGeneralas_IratSablon(sablon, iratAdatok);

        if (res.IsError)
            return;

        byte[] generaltDoku = res.Record as byte[];

        Response.Clear();
        Response.ClearContent();
        Response.ClearHeaders();
        Response.AddHeader("Content-Disposition", "inline;filename=\"" + fileName + "\";");
        Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        Response.OutputStream.Write(generaltDoku, 0, generaltDoku.Length);
        Response.OutputStream.Flush();
        Response.End();

    }
}