﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="DokumentumVerziokList.aspx.cs" Inherits="DokumentumVerziokList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent"
 TagPrefix="uc" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Teszt</title>
    <base target="_self" />
    <style type="text/css">
        .sys-template
        {
            display: none;
        }
    </style>
</head>
<body xmlns:sys="javascript:Sys" xmlns:dataview="javascript:Sys.UI.DataView" sys:activate="*">
    <form id="form1" runat="server" style="padding-left:10px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptLocalization="false">
        <Scripts>
            <asp:ScriptReference Name="MicrosoftAjax.js" Path="~/JavaScripts/MicrosoftAjax.js"/>
            <asp:ScriptReference ScriptMode="Inherit" Path="~/JavaScripts/MicrosoftAjaxTemplates.js" />
            <asp:ScriptReference Path="~/JavaScripts/JSLINQ.js" />
            <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
        </Scripts>
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Style="display: none;">
    </eUI:eErrorPanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc:DokumentumVizualizerComponent ID="DokumentumVizualizerComponent" runat="server" />

    <script type="text/javascript">
        function swapByName(obj, newImageName) {
            var myobj = document.getElementById(obj);
            if (myobj) {
                var fileNameStartIndex = myobj.src.lastIndexOf("/");
                var newUrl = myobj.src.substring(0, fileNameStartIndex + 1) + newImageName;

                myobj.src = newUrl;
            }
        }
        
        var dokumentumVerziokData;
        var dokumentumVerziokDataView;
        var dokumentumVerziokList;
        var filterVerziokData;
        var prevOrderBy = 'version';
        var prevDirection = 'DESC';
        var prevFilterVerzioTol = '';
        var prevFilterVerzioIg = '';
        var updateProgress = $('#<%=CustomUpdateProgress1.UpdateProgress.ClientID %>');
        var errorPanel = $('#<%=EErrorPanel1.ClientID %>');
        var IsError = false;

        function DisplayResultErrorOnErrorPanel(errorPanel, result) {
            var hibaHeader = $(errorPanel).find('.hibaHeader');
            var hibaBody = $(errorPanel).find('.hibaBody');
            if (result.ErrorCode) {
                hibaHeader.html('Hiba!');
                hibaBody.html(result.ErrorMessage);
                $(errorPanel).show();
                return true;
            }
            else {
                $(errorPanel).hide();
                return false;
            }
        }

        function dataLoading(sender, args) {
            if (!dokumentumVerziokData) {
                var result = args.get_data();
                if (!DisplayResultErrorOnErrorPanel(errorPanel, result)) {
                    dokumentumVerziokList = sender;
                    dokumentumVerziokData = result.Record;
                    dokumentumVerziokDataView = new Array();
                    Array.addRange(dokumentumVerziokDataView, dokumentumVerziokData);
                    filterVerziokData = JSLINQ(dokumentumVerziokData).OrderBy(function(item) { return item.version; }).Select(function(item) { return { version: item.version }; }).ToArray();
                    prevFilterVerzioTol = filterVerziokData[0].version;
                    prevFilterVerzioIg = filterVerziokData[filterVerziokData.length - 1].version;
                    SetTitle(dokumentumVerziokData[0].name);
                    Sys.Observer.makeObservable(dokumentumVerziokDataView);
                    dokumentumVerziokList.set_data(dokumentumVerziokDataView);
                    dokumentumVerziokListSort(prevOrderBy, prevDirection);
                }
                args.set_cancel(true);
            }
        }

        function dokumentumVerziokListFilter(filterVerzioTol, filterVerzioIg) {
            if (filterVerzioTol <= filterVerzioIg) {
                prevFilterVerzioTol = filterVerzioTol;
                prevFilterVerzioIg = filterVerzioIg;
                dokumentumVerziokListSort(prevOrderBy, prevDirection);
            }
            else {
                $('#filterVersionIg>option[text=' + prevFilterVerzioIg + ']').attr('selected', 'true');
                $('#filterVersionTol>option[text=' + prevFilterVerzioTol + ']').attr('selected', 'true');
            }
            
        }

        function IsFilter(dokumentumVersion) {
            if (prevFilterVerzioTol) {
                if (dokumentumVersion.version < prevFilterVerzioTol)
                    return false;
            }

            if (prevFilterVerzioIg) {
                if (dokumentumVersion.version > prevFilterVerzioIg)
                    return false;
            }

            return true;
        }


        function dokumentumVerziokListSort(orderBy, direction) {
            prevOrderBy = orderBy;
            prevDirection = direction;

            OnDataChanging();
            
            dokumentumVerziokDataView.beginUpdate();
            var sortedList;

            if (direction == 'ASC')
                sortedList = JSLINQ(dokumentumVerziokData).Where(IsFilter).OrderBy(function(item) { return item[orderBy]; }).ToArray();
            else
                sortedList = JSLINQ(dokumentumVerziokData).Where(IsFilter).OrderByDescending(function(item) { return item[orderBy]; }).ToArray();

            dokumentumVerziokDataView.clear();
            dokumentumVerziokDataView.addRange(sortedList);
            dokumentumVerziokDataView.endUpdate();

            OnDataChanged();

            if (!IsVersionFilterHidden) {
                SwapVersionFilter();
            }

        }

        function onDokumentumVerziokListViewCommand(sender, args) {
            switch (args.get_commandName()) {
                case "Sort":
                    var orderBy = args.get_commandArgument();
                    var direction = 'ASC';
                    if (orderBy == prevOrderBy) {
                        direction = (prevDirection == 'DESC') ? 'ASC' : 'DESC';
                    }
                    dokumentumVerziokListSort(orderBy, direction);
                    break;
                case "Filter":
                    var filterVerzioTol = $('#filterVersionTol option:selected').text();
                    var filterVerzioIg = $('#filterVersionIg option:selected').text();
                    if (prevFilterVerzioTol != filterVerzioTol || prevFilterVerzioIg != filterVerzioIg) {
                        dokumentumVerziokListFilter(filterVerzioTol, filterVerzioIg);
                    }
                    break;
                default:
                    break;
            }
        }

        function IsUpdating(updating) {
            if (updating) {
                updateProgress.show();
            }
            else {
                updateProgress.hide();
            }
        }

        function onPropertyChanged(sender, args) {
            var prop = args.get_propertyName();
            switch (prop) {
                case "isFetching":
                    IsUpdating(sender.get_isFetching());
                    break;
                default:
                    break;
            }
        }

        function SetTitle(dokumentName) {
            var titleText = String.format(document.title, dokumentName);
            document.title = titleText;
            $('#ListHeader1_Header').text(titleText);
        }

        function pageLoad() {
            if (IsError) {
                $(errorPanel).show();
                SetTitle('hiba');
            }
            else {
                $find('dokumentumVerziokListView').fetchData();
            }
        };

        var IsVersionFilterHidden = true;
        function SwapVersionFilter(e) {
            if ($('#VersionFilterPanel').is(':hidden')) {
                $('#VersionFilterPanel').show();
                IsVersionFilterHidden = false;
            }
            else {
                $('#VersionFilterPanel').hide();
                IsVersionFilterHidden = true;
            }
        }

        function OnDataChanging() {
            var dokumentumVizualizerComponent = $find('<%=DokumentumVizualizerComponent.BehaviorID %>');

            if (dokumentumVizualizerComponent) {
                dokumentumVizualizerComponent.disposeElements();
            }
        }
        
        function OnDataChanged() {
            var dokumentumVizualizerComponent = $find('<%=DokumentumVizualizerComponent.BehaviorID %>');

            if (dokumentumVizualizerComponent) {
                var elementsList = new Array();
                for (var i = 0; i < dokumentumVerziokDataView.length; i++) {
                    var item = dokumentumVerziokDataView[i];
                    var dokumentElement = new Object();
                    dokumentElement.ClientID = String.format('fileIcon_{0}', item.version);
                    dokumentElement.DokumentumId = item.Id;
                    dokumentElement.FileName = item.name;
                    dokumentElement.Version = item.version;
                    dokumentElement.ExternalLink = item.url;
                    Array.add(elementsList, dokumentElement);
                }
                dokumentumVizualizerComponent.elementsList = elementsList;
                dokumentumVizualizerComponent.initializeElements();
            }
        }
        
    </script>

    <div>
        <table id="dokumentumVerziokListView" cellpadding="0" cellspacing="0" class="sys-template"
            sys:attach="dataview" dataview:autofetch="false" dataview:dataprovider="WrappedWebService/Ajax_eRecord.asmx"
            dataview:fetchoperation="GetDokumentumVerziok" dataview:ondataloading="{{ dataLoading }}"
            dataview:fetchparameters="{{ {FelhasznaloId: felhasznaloId, DokumentumId:dokumentumId} }}"
            dataview:oncommand="{{ onDokumentumVerziokListViewCommand }}" dataview:onpropertychanged="{{ onPropertyChanged }}">
            <thead code:if="$index==0" class="GridViewHeaderStyle">
                <tr>
                    <th class="GridViewBorderHeader" style="width: 200px;">
                        <span>Állomány</span>
                    </th>
                    <th class="GridViewBorderHeader" style="width: 110px;">
                    <div style="position:relative;width:100%">
                    <div id="VersionFilterPanel" style="display:none;position:absolute;top:-30px;left:0px;width:100%;text-align:center;">
                        <select id="filterVersionTol" class="sys-template" sys:attach="dataview" dataview:data="{{filterVerziokData}}"
                            sys:command="Filter">
                            <option sys:selected="{{prevFilterVerzioTol==version}}">{{version}}</option>
                        </select>
                        &nbsp;-&nbsp;
                        <select id="filterVersionIg" class="sys-template" sys:attach="dataview" dataview:data="{{filterVerziokData}}"
                            sys:command="Filter">
                            <option sys:selected="{{prevFilterVerzioIg==version}}">{{version}}</option>
                        </select>
                    </div>
                        <span class="GridViewSortingHeader" sys:command="Sort" sys:commandargument="version">
                            Verzió</span>
                        <span title="szűrés" style="padding-left:2px;cursor:pointer;position:relative;top:2px;" onclick="SwapVersionFilter();">
                            <img src="images/hu/egyeb/search.jpg"/>
                        </span>
                    </div>         
                    </th>
                    <th class="GridViewBorderHeader" style="width: 80px;">
                        <span class="GridViewSortingHeader" sys:command="Sort" sys:commandargument="size">Méret</span>
                    </th>
                    <th class="GridViewBorderHeader" style="width: 120px;">
                        <span class="GridViewSortingHeader" sys:command="Sort" sys:commandargument="created">
                            Létrehozva</span>
                    </th>
                    <th class="GridViewBorderHeader" style="width: 150px;">
                        <span class="GridViewSortingHeader" sys:command="Sort" sys:commandargument="comments">
                            Megjegyzés</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="GridViewRowStyle" sys:command="Select">
                    <td class="GridViewBoundFieldItemStyle">
                        {{ name }}
                        <input id="{{String.format('fileIcon_{0}', version)}}" type="image" sys:src="{binding source}" onclick="{{String.format('window.open(\'GetDocumentContent.aspx?id={0}&version={1}\');return false;',Id, version)}}" />
                    </td>
                    <td class="GridViewBoundFieldItemStyle">
                        {{ version }}
                    </td>
                    <td class="GridViewBoundFieldItemStyle">
                        {{ size/1000 }}&nbsp;Kb
                    </td>
                    <td class="GridViewBoundFieldItemStyle">
                        {{ created }}
                    </td>
                    <td class="GridViewBoundFieldItemStyle">
                        {{ comments }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
