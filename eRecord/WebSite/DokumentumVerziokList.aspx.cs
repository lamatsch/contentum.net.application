﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;

public partial class DokumentumVerziokList : System.Web.UI.Page
{
    private const string funkcio_DokumentumokList = "DokumentumokList";

    private string DokumentumId
    {
        get
        {
            return QueryStringVars.Get<Guid>(Page, QueryStringVars.DokumentumId);
        }
    }

    private bool isError = false;

    public bool IsError
    {
        get { return isError; }
        set { isError = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        // funkciójogok
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_DokumentumokList);

        Response.CacheControl = "no-cache";

        ListHeader1.DafaultPageLinkVisible = false;
        ListHeader1.PagerVisible = false;
        ListHeader1.SearchVisible = false;
        ListHeader1.RefreshVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;

        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.HeaderLabel = Resources.List.DokumentumVerziokListHeaderTitle;
        ListHeader1.CustomSearchObjectSessionName = "-----";

        if (String.IsNullOrEmpty(DokumentumId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(EErrorPanel1);
            IsError = true;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsError) return;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsError)
        {
            string jsError = "var IsError = true;";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "IsError", jsError, true);
        }

        ExecParam xpm = UI.SetExecParamDefault(Page);
        string dokumentumId = Request.QueryString.Get(QueryStringVars.DokumentumId);
        string js = String.Format("var felhasznaloId = '{0}'; var dokumentumId = '{1}';", xpm.Felhasznalo_Id, dokumentumId);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "initParameters", js, true);
    }
}
