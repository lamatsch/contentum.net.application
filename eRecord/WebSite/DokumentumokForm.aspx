﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DokumentumokForm.aspx.cs" Inherits="DokumentumokForm" Title="Dokumentumok karbantartása" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/FileUploadComponent.ascx" TagName="FileUploadComponent"
    TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>
<%@ Register Src="eRecordComponent/TabFooter.ascx" TagName="TabFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
         </Scripts>
    </asp:ScriptManager>
    <style>
        .ajax__calendar .ajax__calendar_container {
            z-index:1001;
        }

         div[id$="listboxDiv"] {
             height: auto !important;
             overflow-y: hidden !important;
         }
         div[id$="listboxDiv"] > select {
             min-width: 150px;
             width: auto !important;
             
             height: 80px !important;
         }
         table[id$='DynamicValueControl_TargyszoErtek_Control_KodtarakCheckBoxList'] {
             width: inherit !important;
         }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            try {
                $(window).scrollTop(0);
            } catch (e) {
            }
        });
    </script>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:Panel ID="FileUploadPanel" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                    <asp:Label ID="labelAllomanyStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="labelAllomany" runat="server" Text="Állomány:"></asp:Label>
                                    &nbsp;
                                </td>
                                <td style="text-align: left; vertical-align: middle;">
                                    <%--                        <uc3:FileUploadComponent ID="FileUploadComponent1" runat="server" SingleMode="true"
                                    Visible="false" />
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="mrUrlapInput" Width="350" />
    --%>
                                    <asp:TextBox ID="TextBoxFileName" runat="server" Visible="false" />
                                    <asp:FileUpload ID="fileUpload"  runat="server" CssClass="mrUrlapInput" Width="350" />
                                    <asp:RequiredFieldValidator ID="Validator1" runat="server" ControlToValidate="fileUpload"
                                        SetFocusOnError="true" Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                                        TargetControlID="Validator1">
                                        <Animations>
                                            <OnShow>
                                            <Sequence>
                                                <HideAction Visible="true" />
                                                <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                            </Sequence>    
                                            </OnShow>
                                        </Animations>
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:UpdatePanel ID="DokumentumUpdatePanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <%--<eUI:eFormPanel ID="EFormPanel1" runat="server">--%>
                            <asp:Panel ID="AllomanyAdatokPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="height: 30px">
                                        </td>
                                        <td class="mrUrlapMezo" style="height: 30px">
                                            <asp:CheckBox ID="cbMegnyithato" Text="Megnyitható" TextAlign="Right" runat="server" />
                                            <asp:CheckBox ID="cbOlvashato" Text="Olvasható" TextAlign="Right" runat="server" />
                                            <asp:CheckBox ID="cbTitkositott" Text="Titkosított" TextAlign="Right" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:HiddenField ID="hf_confirmUploadByDocumentHash" runat="server" Value="" />
                            <asp:Panel ID="TipusPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="labelTipus" runat="server" Text="Típus:" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_Tipus" runat="server" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelErvenyessegStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                            </uc6:ErvenyessegCalendarControl>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="ObjektumTargyszavaiPanel" runat="server">
                            </asp:Panel>
                            <otp:ObjektumTargyszavaiPanel ID="ObjektumTargyszavaiPanel1" RepeatColumns="1" RepeatDirection="Vertical" Width="100%"
                                runat="server" Validate="false" />
                            <%--</eUI:eFormPanel>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                &nbsp; &nbsp;&nbsp; &nbsp;
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
