﻿using Contentum.eBusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DokumentumokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string SITE_PATH = "standalone";
    private const string DOC_LIB_PATH = "docs";
    private const string FOLDER_PATH = "fph";

    private const string kcs_DOKUMENTUMTIPUS = "DOKUMENTUMTIPUS";

    private string Startup = String.Empty;

    // érvényes path: ^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$
    // ellenőrzött esetek:
    // 1. A : előtt, ha van, csak a-zA-Z karakter szerepelhet
    // 2. A \\ előtt, ha van, nem lehet más karakter
    // 3. A : előtt, ha van, max. 1 karakter szerepelhet
    // 4. : vagy \ után nem állhat közvetlenül .
    // 5. Az útvonalban/fájlnévben nem állhat ..
    // 6. Útvonal nem kezdődhet . vagy : karakterrel
    // 7. Ha az útvonal meghajtójel: alakban kezdődik, és nem követi közvetlenül \, akkor hátrébb sem állhat \ (azaz csak fájlnév követheti)
    // 8. A fájlnévben/útvonalban nem állhat a # % & * < > ? / { | } karakterek egyike sem
    private const string regexpCheckPathErrors = @"(^(.*[^a-zA-Z].*:|.+\\\\{2,}|.{2,}:))|((:|\\\\)\\.)|^(\\.|:)|(\\.{2,})|(^[a-zA-Z]:[^\\\\]+\\\\)|([#%&\\*\\<\\>\\?\\/\\{{\\|\\}}])";
    private string js_FileNameCheckFunction = String.Format(@"function checkFileName(fileName)
{{
    if (fileName) {{
        var re=new RegExp('{0}');
        var m = re.exec(fileName);
        if (m && m.length > 0) {{
            alert('{1}' + m[0]);
            return false;
        }}
    }}
    return true;
}}", regexpCheckPathErrors, Resources.Error.UIFileNameFormatError);

    #region Utility
    /// <summary>
    /// View módban a vezérlők engedélyezése és megjelenése
    /// </summary>
    private void SetViewControls()
    {
        //// FileUpload1 helyett FileUploadComponent1 megjelenítése, ami mutatja a feltöltött fájlt:
        //FileUpload1.Enabled = false;
        //FileUpload1.Visible = false;
        //FileUploadComponent1.Visible = true;
        //FileUploadComponent1.Enabled = false;

        fileUpload.Visible = false;
        TextBoxFileName.Visible = true;
        TextBoxFileName.ReadOnly = true;

        cbMegnyithato.Enabled = false;
        cbOlvashato.Enabled = false;
        cbTitkositott.Enabled = false;

        KodtarakDropDownList_Tipus.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        ObjektumTargyszavaiPanel1.ReadOnly = true;
        ObjektumTargyszavaiPanel1.ViewMode = true;

        labelAllomanyStar.CssClass = "mrUrlapInputWaterMarked";
        labelAllomany.CssClass = "mrUrlapInputWaterMarked";
        labelTipus.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }

    /// <summary>
    /// Modify módban a vezérlők engedélyezése és megjelenése
    /// </summary>
    private void SetModifyControls()
    {
        //// FileUpload1 helyett FileUploadComponent1 megjelenítése, ami mutatja a feltöltött fájlt:
        //FileUpload1.Enabled = false;
        //FileUpload1.Visible = false;
        //FileUploadComponent1.Visible = true;
        //FileUploadComponent1.Enabled = false;

        if (TextBoxFileName.Text == "-")
        {
            fileUpload.Visible = true;
            TextBoxFileName.Visible = false;
            labelAllomanyStar.Visible = false;
            Validator1.Enabled = false;
        }
        else
        {
            fileUpload.Visible = false;
            TextBoxFileName.Visible = true;
            TextBoxFileName.ReadOnly = true;
        }

        KodtarakDropDownList_Tipus.ReadOnly = true;
    }

    private void SetNewControls()
    {
        //// FileUploadComponent1 helyett FileUpload1 megjelenítése:
        //FileUpload1.Enabled = true;
        //FileUpload1.Visible = true;
        //FileUploadComponent1.Visible = false;
        //FileUploadComponent1.Enabled = false;

        if (Startup == Constants.Startup.FromPartnerekList)
        {
            labelAllomanyStar.Visible = false;
            Validator1.Enabled = false;
        }

        fileUpload.Visible = true;
        TextBoxFileName.Visible = false;
    }

    #endregion Utility

    private bool checkFileName = false;

    #region Base Page


    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                if (Startup == Constants.Startup.Standalone)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "StandaloneDokumentumok" + Command);
                }
                else if (Startup == Constants.Startup.Szakagi)
                {
                    //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SzakagiDokumentumok" + Command);
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Dokumentumok" + Command);
                }
                else if (Startup == Constants.Startup.Sablonok)
                {
                    //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SzakagiDokumentumok" + Command);
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Sablonok" + Command);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Dokumentumok" + Command);
                }
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_DokumentumokService service = Contentum.eRecord.Utility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)result.Record;
                    LoadComponentsFromBusinessObject(krt_Dokumentumok);

                    FillObjektumTargyszavaiPanel(id, krt_Dokumentumok.Tipus);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (Startup == Constants.Startup.Standalone || Startup == Constants.Startup.Szakagi)
            {
                KodtarakDropDownList_Tipus.FillAndSetEmptyValue(kcs_DOKUMENTUMTIPUS, FormHeader1.ErrorPanel, true);
            }
            else if (Startup == Constants.Startup.Sablonok)
            {
                //TODO
                KodtarakDropDownList_Tipus.FillAndSetEmptyValue(kcs_DOKUMENTUMTIPUS, FormHeader1.ErrorPanel, true);
            }
            else
            {
                //...
                KodtarakDropDownList_Tipus.FillAndSetEmptyValue(kcs_DOKUMENTUMTIPUS, FormHeader1.ErrorPanel, true);
            }
        }

        if (Command == CommandName.New)
        {
            SetNewControls();
        }
        else if (Command == CommandName.View)
        {
            SetViewControls();
        }
        else if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Startup == Constants.Startup.Standalone)
        {
            FormHeader1.HeaderTitle = Resources.Form.StandaloneDokumentumokFormHeaderTitle;
        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            FormHeader1.HeaderTitle = Resources.Form.SzakagiDokumentumokFormHeaderTitle;
        }
        else if (Startup == Constants.Startup.FromPartnerekList)
        {
            FormHeader1.HeaderTitle = Resources.Form.PartnerDokumentumokFormHeaderTitle;
            if (Command == CommandName.New && Contentum.eUtility.Rendszerparameterek.IsTUK(Page))
            {
                FormFooter1.ImageButton_SaveAndNew.Visible = true;
                FormFooter1.ImageButton_Default.Visible = true;
                FormFooter1.Label_SaveAndNew.Text = Resources.Form.DokumentumokUjTipusInfo;
            }
        }

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Startup == Constants.Startup.Standalone ||
            Startup == Constants.Startup.Szakagi ||
            Startup == Constants.Startup.FromPartnerekList)
        {
            KodtarakDropDownList_Tipus.DropDownList.SelectedIndexChanged += new EventHandler(DropDownList_SelectedIndexChanged);
            KodtarakDropDownList_Tipus.DropDownList.AutoPostBack = true;
        }

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (Command == CommandName.New)
        {
            if (checkFileName)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "checkFileName", js_FileNameCheckFunction, true);

                if (!IsPostBack || !FormFooter1.ImageButton_Save.OnClientClick.Contains("fileUpload"))
                {
                    /// fileUpload:
                    /// onchange: a fájlnév ellenőrzése (a nevükben ..-t tartalmazó fájlok sem tölthetők fel)
                    fileUpload.Attributes["onchange"] = "if (!checkFileName(this.value)) return false;";

                    string js_Check = String.Format(@"var fileUpload = $get('{0}');
if (fileUpload && !checkFileName(fileUpload.value)) return false;"
                        , fileUpload.ClientID);

                    FormFooter1.ImageButton_Save.OnClientClick = FormFooter1.ImageButton_SaveAndNew.OnClientClick = String.Format("{0}{1}", js_Check, FormFooter1.ImageButton_Save.OnClientClick);
                }
            }
            else
            {
                FormFooter1.ImageButton_Save.OnClientClick = FormFooter1.ImageButton_SaveAndNew.OnClientClick = SetOnClientClickConfirm(fileUpload.ClientID, "Nincs fájl kiválasztva", "Biztosan fájl nélkül szeretné létrehozni a rekordot?");
            }
        }
    }

    protected void DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillObjektumTargyszavaiPanel(Request.QueryString.Get(QueryStringVars.Id), ((DropDownList)sender).SelectedValue);
    }

    protected void FillObjektumTargyszavaiPanel(String id, String tipus)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        ObjektumTargyszavaiPanel1.FillObjektumTargyszavai(search, id, null
        , Constants.TableNames.KRT_Dokumentumok, Constants.ColumnNames.KRT_Dokumentumok.Tipus, new string[] { tipus }, false
        , null, false, false, FormHeader1.ErrorPanel);

        ObjektumTargyszavaiPanel1.SetDefaultValues();
        FormFooter1.ImageButton_Save.OnClientClick = ObjektumTargyszavaiPanel1.GetCheckErtekJavaScript() + FormFooter1.ImageButton_Save.OnClientClick;
    }

    //protected DataTable GetAllMetaByDokumentumTipus(String id, String tipus)
    //{
    //    EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
    //    ExecParam execParam_ot = UI.SetExecParamDefault(Page);

    //    EREC_ObjektumTargyszavaiSearch search_ot = new EREC_ObjektumTargyszavaiSearch();

    //    Result result_ot = service_ot.GetAllMetaByObjMetaDefinicio(execParam_ot, search_ot, id, null
    //        , Constants.TableNames.KRT_Dokumentumok, Constants.ColumnNames.KRT_Dokumentumok.Tipus, new string[] {tipus}, false
    //        , null, false, false);

    //    if (result_ot.IsError)
    //    {
    //        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot);
    //    }

    //    return result_ot.Ds.Tables[0];
    //}

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Dokumentumok krt_Dokumentumok)
    {
        //FileUploadComponent1.Value_HiddenField = ";" + krt_Dokumentumok.FajlNev;
        //FileUploadComponent1.Label.Text = krt_Dokumentumok.FajlNev;

        TextBoxFileName.Text = krt_Dokumentumok.FajlNev;

        cbMegnyithato.Checked = krt_Dokumentumok.Megnyithato == "1";
        cbOlvashato.Checked = krt_Dokumentumok.Olvashato == "1";
        cbTitkositott.Checked = krt_Dokumentumok.Titkositas == "1";

        KodtarakDropDownList_Tipus.FillAndSetSelectedValue(kcs_DOKUMENTUMTIPUS, krt_Dokumentumok.Tipus, FormHeader1.ErrorPanel);

        ErvenyessegCalendarControl1.ErvKezd = krt_Dokumentumok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_Dokumentumok.ErvVege;

        FormHeader1.Record_Ver = krt_Dokumentumok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_Dokumentumok.Base);
    }

    // form --> business object
    private KRT_Dokumentumok GetBusinessObjectFromComponents()
    {
        KRT_Dokumentumok krt_Dokumentumok = new KRT_Dokumentumok();
        // összes mező update-elhetőségét kezdetben letiltani:
        krt_Dokumentumok.Updated.SetValueAll(false);
        krt_Dokumentumok.Base.Updated.SetValueAll(false);

        //krt_Dokumentumok.FajlNev = fileUpload.PostedFile.FileName;
        //krt_Dokumentumok.Updated.FajlNev = pageView.GetUpdatedByView(fileUpload);

        krt_Dokumentumok.Megnyithato = (cbMegnyithato.Checked ? "1" : "0");
        krt_Dokumentumok.Updated.Megnyithato = pageView.GetUpdatedByView(cbMegnyithato);

        krt_Dokumentumok.Olvashato = (cbOlvashato.Checked ? "1" : "0");
        krt_Dokumentumok.Updated.Olvashato = pageView.GetUpdatedByView(cbOlvashato);

        krt_Dokumentumok.Titkositas = cbTitkositott.Checked ? "1" : "0";
        krt_Dokumentumok.Updated.Titkositas = pageView.GetUpdatedByView(cbTitkositott);

        krt_Dokumentumok.Tipus = KodtarakDropDownList_Tipus.SelectedValue;
        krt_Dokumentumok.Updated.Tipus = pageView.GetUpdatedByView(KodtarakDropDownList_Tipus);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_Dokumentumok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_Dokumentumok.Base.Ver = FormHeader1.Record_Ver;
        krt_Dokumentumok.Base.Updated.Ver = true;

        return krt_Dokumentumok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(FileUpload1);

            compSelector.Add_ComponentOnClick(cbMegnyithato);
            compSelector.Add_ComponentOnClick(cbOlvashato);
            compSelector.Add_ComponentOnClick(cbTitkositott);

            compSelector.Add_ComponentOnClick(KodtarakDropDownList_Tipus);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            compSelector.Add_ComponentOnClick(ObjektumTargyszavaiPanel1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        Startup = Startup = Request.QueryString.Get(QueryStringVars.Startup);

        string funkcioJog = "Dokumentumok";

        if (Startup == Constants.Startup.Standalone)
        {
            funkcioJog = "StandaloneDokumentumok";
        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            //funkcioJog = "SzakagiDokumentumok";
        }

        switch (e.CommandName)
        {
            //TODO: feltöltést megcsinálni
            case CommandName.Save:
            case CommandName.SaveAndNew:
                if (FunctionRights.GetFunkcioJog(Page, funkcioJog + Command))
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {

                                if (fileUpload.PostedFile == null || string.IsNullOrEmpty(fileUpload.PostedFile.FileName))
                                {
                                    if (Startup == Constants.Startup.FromPartnerekList)
                                    {

                                    }
                                    else
                                    {
                                        return;
                                    }
                                }

                                Result result = FileUpload();

                                // TODO: tranzakció
                                //KRT_DokumentumokService service = Contentum.eRecord.Utility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                                //KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents();

                                //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                //Result result = service.Insert(execParam, krt_Dokumentumok);

                                //LZS - BLG_51
                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    #region objektumfüggő tárgyszavak mentése a dokumentumhoz
                                    EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = ObjektumTargyszavaiPanel1.GetEREC_ObjektumTargyszavaiList(false, true);

                                    if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                                    {
                                        ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                        EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                        Result result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                                                    , EREC_ObjektumTargyszavaiList
                                                    , result.Uid
                                                    , null
                                                    , Constants.TableNames.KRT_Dokumentumok
                                                    , "*"
                                                    , null
                                                    , false
                                                    , null
                                                    , false
                                                    , true
                                                    );

                                        if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                        {
                                            // hiba
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot);
                                            return;
                                        }
                                    }
                                    #endregion objektumfüggő tárgyszavak mentése a dokumentumhoz

                                    KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents();
                                    // ha egy másik formról hívták meg, adatok visszaküldése:
                                    // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, krt_Dokumentumok.FajlNev))
                                    {
                                        if (e.CommandName == CommandName.SaveAndNew)
                                        {
                                            JavaScripts.RegisterRefreshOpenerClientScript(Page);
                                        }
                                        else
                                        {
                                            String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                            if (!String.IsNullOrEmpty(refreshCallingWindow) && refreshCallingWindow == "1")
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                            }
                                            else
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                        if (e.CommandName == CommandName.SaveAndNew)
                                            JavaScripts.RegisterRefreshOpenerClientScript(Page);
                                        else
                                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                                break;
                            }
                        case CommandName.Modify:
                            {
                                String recordId = Request.QueryString.Get(QueryStringVars.Id);

                                if (String.IsNullOrEmpty(recordId))
                                {
                                    // nincs Id megadva:
                                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                                }
                                else
                                {
                                    KRT_DokumentumokService service = Contentum.eRecord.Utility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                                    KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents();

                                    if (TextBoxFileName.Text == "-")
                                    {
                                        Result resultFileUpload = FileUpload();

                                        if (resultFileUpload.IsError)
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultFileUpload);
                                            return;
                                        }
                                        else
                                        {
                                            krt_Dokumentumok = (KRT_Dokumentumok)resultFileUpload.Record;
                                        }
                                    }

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    krt_Dokumentumok.Id = execParam.Record_Id;
                                    krt_Dokumentumok.Dokumentum_Id = execParam.Record_Id;
                                    Result result = service.Update(execParam, krt_Dokumentumok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        // verzió növelése, ha hiba történik a tárgyszavak mentésénél
                                        FormHeader1.Record_Ver = (Int32.Parse(FormHeader1.Record_Ver) + 1).ToString();

                                        #region objektumfüggő tárgyszavak mentése a dokumentumhoz
                                        EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = ObjektumTargyszavaiPanel1.GetEREC_ObjektumTargyszavaiList(true, false);

                                        if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                                        {
                                            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                            Result result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                                                        , EREC_ObjektumTargyszavaiList
                                                        , recordId
                                                        , null
                                                        , Constants.TableNames.KRT_Dokumentumok
                                                        , "*"
                                                        , null
                                                        , false
                                                        , null
                                                        , false
                                                        , false
                                                        );

                                            if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                            {
                                                // hiba
                                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ot);
                                                return;
                                            }
                                        }
                                        #endregion objektumfüggő tárgyszavak mentése a dokumentumhoz

                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                                break;
                            }
                    }
                }
                else
                {
                    UI.RedirectToAccessDeniedErrorPage(Page);
                }

                break;

            case CommandName.Default:
                TextBoxFileName.Text = string.Empty;
                TextBoxFileName.Visible = false;
                cbMegnyithato.Checked =
                cbOlvashato.Checked =
                cbTitkositott.Checked = false;
                KodtarakDropDownList_Tipus.SetSelectedValue(string.Empty);
                ErvenyessegCalendarControl1.SetDefault();

                FillObjektumTargyszavaiPanel(Request.QueryString.Get(QueryStringVars.Id), string.Empty);

                break;
        }
    }

    private Result FileUpload()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);

        string documentStoreType = Contentum.eUtility.Rendszerparameterek.Get(Page, Contentum.eUtility.Rendszerparameterek.IRAT_CSATOLMANY_DOKUMENTUMTARTIPUS);
        if (documentStoreType == Contentum.eUtility.Constants.DocumentStoreType.UCM)
            return new Result();


        #region Feltöltött file beolvasása byte[]-ba

        //System.IO.StreamReader sr = new System.IO.StreamReader(fileUpload.PostedFile.InputStream);
        //string s = sr.ReadToEnd();

        //byte[] buf = System.Text.ASCIIEncoding.ASCII.GetBytes(s);

        byte[] buf = fileUpload.FileBytes;

        #endregion Feltöltött file beolvasása byte[]-ba


        string fileName = fileUpload.PostedFile.FileName.Substring(fileUpload.PostedFile.FileName.LastIndexOf('\\') + 1);
        DocumentService documentService = Contentum.eUtility.eDocumentService.ServiceFactory.GetDocumentService();

        #region Feltöltötték-e már a fájlt
        if (!String.IsNullOrEmpty(fileName) && !FormHeader1.ErrorPanel.Visible)
        {
            Result result_fileexists = documentService.FileExists(SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName);

            if (!result_fileexists.IsError && (bool)result_fileexists.Record)
            {
                //A fájlt már feltöltötték, nem engedjük felülírni
                //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, ResultError.CreateNewResultWithErrorCode(52755));

                //return result_fileexists;

                System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "alert('Létezik ilyen nevű dokumentum, amelyet a rendszer felülírt, és új verziószámot kapott.');", true);

            }
        }
        #endregion Feltöltötték-e már a fájlt

        #region FileUpload
        KRT_Dokumentumok krt_Dokumentumok = GetBusinessObjectFromComponents();
        string checkinmode = "1";
        if (TextBoxFileName.Text == "-")
        {
            checkinmode = "2";
            krt_Dokumentumok.Id = Request.QueryString.Get(QueryStringVars.Id);
        }
        Result result = documentService.DirectUploadAndDocumentInsert(execParam, documentStoreType, SITE_PATH, DOC_LIB_PATH, FOLDER_PATH, fileName, buf, checkinmode, krt_Dokumentumok);
        if (result.IsError)
        {
            return result;
        }
        else
        {
            if (Startup != Constants.Startup.Sablonok)
            {
                string PartnerId = Request.QueryString.Get(QueryStringVars.PartnerId);

                if (!string.IsNullOrEmpty(PartnerId) &&
                    Request.QueryString.Get(CommandName.Command) == CommandName.New)
                {
                    KRT_Partner_Dokumentumok krt_Partner_Dokumentumok = new KRT_Partner_Dokumentumok
                    {
                        Partner_id = PartnerId,
                        Dokumentum_Id = result.Uid
                    };
                    KRT_Partner_DokumentumokService krt_Partner_DokumentumokService = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_Partner_DokumentumokService();
                    Result resultPartnerDok = krt_Partner_DokumentumokService.Insert(execParam, krt_Partner_Dokumentumok);

                    if (resultPartnerDok.IsError)
                    {
                        return resultPartnerDok;
                    }

                }

                return result;
            }

            else //Sablonok
            {

                //LZS - BLG_51 - Ez  az Id itt a sablon id. 
                string SablonId = Request.QueryString.Get(QueryStringVars.Id);

                //LZS - BLG_51 - A dokumentum feltöltése utan a KRT_Dokumentumok.Id értéket 
                //a sablon kódtár KRT_Kodtarak.Obj_Id mezőjében kell eltárolni. 
                if (!string.IsNullOrEmpty(SablonId) &&
                    Request.QueryString.Get(CommandName.Command) == CommandName.New)
                {
                    Contentum.eAdmin.Service.KRT_KodTarakService krt_KodtarakService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
                    ExecParam execParamUpdate = UI.SetExecParamDefault(Page);
                    execParamUpdate.Record_Id = SablonId;

                    KRT_KodTarak Sablon = (KRT_KodTarak)krt_KodtarakService.Get(execParamUpdate).Record;

                    Sablon.Updated.SetValueAll(false);
                    Sablon.Base.Updated.SetValueAll(false);

                    Sablon.Obj_Id = result.Uid;
                    Sablon.Updated.Obj_Id = true;

                    Result resultKRT_KodtarakUpdate = krt_KodtarakService.Update(execParamUpdate, Sablon);


                    if (resultKRT_KodtarakUpdate.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultKRT_KodtarakUpdate);
                    }

                }

                return result;

            }
        }


    }

    #endregion FileUpload


    private static string SetOnClientClickConfirm(string fileUploadClientID, string confirmHeader, string confirmQuestion)
    {
        string javascript = "";

        javascript =
        " if (" + fileUploadClientID + ".value.length > 0) {"
        + " return true;} "
        + "else if (confirm('" + confirmHeader + " \\n \\n"
        + confirmQuestion
         + "')) { return true; } else return false;";
        return javascript;
    }
    #endregion Base Page
}
