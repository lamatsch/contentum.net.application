<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DokumentumokListPopupForm.aspx.cs" Inherits="DokumentumokListPopupForm" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register src="eRecordComponent/DokumentumokList.ascx" tagname="DokumentumokList" tagprefix="dl" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="ff" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <dl:DokumentumokList ID="DokumentumokLista" runat="server" />
    <ff:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
