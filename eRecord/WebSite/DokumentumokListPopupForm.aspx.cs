using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eDocument.Service;
using Contentum.eRecord.Utility;

public partial class DokumentumokListPopupForm : Contentum.eUtility.UI.PageBase
{

    protected void Page_Init(object sender, EventArgs e)
    {
        DokumentumokLista.ScriptManager = ScriptManager1;
        DokumentumokLista.ListHeader = ListHeader1;

        DokumentumokLista.InitPage();

        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refresh + "'; window.close(); return false;";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        DokumentumokLista.LoadPage();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        DokumentumokLista.PreRenderPage();
    }


    
}




