﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="DokumentumokLovList.aspx.cs" Inherits="DokumentumokLovList" Title="Untitled Page" %>


<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc5" %>


<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,DokumentumokLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td>
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                  <td>                                    
                                    <table>  
                                    <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label4" runat="server" Text="Fájlnév:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="FajlNev_TextBox" runat="server" CssClass="mrUrlapInput">*</asp:TextBox>
                                            </td>      
                                            <td></td>                                      
                                            <td style="width: 30%"></td>
                                        </tr>                                      
                                        <tr class="urlapSor">                                            
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="Label3" runat="server" Text="Fájl tartalom:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="FajlTartalom_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                            </td>                                          
                                            <td></td>                                      
                                            <td></td>  
                                        </tr>
                                        <tr class="urlapSor">
                                            <td colspan="4">
                                                <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keresés" OnClick="ButtonSearch_Click" />
                                                <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="Részletes keresés" /></td>
                                        </tr>
                                    </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                        <div class="listaFulFelsoCsikKicsi">
                                            <img src="images/hu/design/spacertrans.gif" alt="" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        
                                                        <!-- Kliens oldali kiválasztás kezelése -->
                                                        <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                        <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                        
                                                        <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                            DataKeyNames="Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" SortExpression="Id">
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FajlNev" HeaderText="Állomány">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Formatum_Nev" HeaderText="Formátum">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Meret" HeaderText="Méret">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Feltöltve">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <%--
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        &nbsp;
                                        <br />
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" 
                                        ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                        onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"
                                        AlternateText="Kijelölt tétel részletes adatainak megtekintése" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
