﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Collections.Generic;

public partial class DokumentumokLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    private string IratId = String.Empty;
    private string KuldemenyId = String.Empty;

    private string Startup = String.Empty;

    private string DokumentumTipus = String.Empty;
    private string DokumentumTipusNev = String.Empty;

    private const string kcs_DOKUMENTUMTIPUS = "DOKUMENTUMTIPUS";

    protected string[] EnabledValues_DokumentumTipus
    {
        get
        {
            // ha még nem volt (sikeres) lekérés
            if (ViewState["EnabledValues_DokumentumTipus"] == null && ViewState["ForbiddenValues_DokumentumTipus"] == null)
            {
                FillViewStateEnabledAndForbiddenValues_Tipus("EnabledValues_DokumentumTipus", "ForbiddenValues_DokumentumTipus");
            }

            return ViewState["EnabledValues_DokumentumTipus"] as string[];
        }
    }

    protected string[] ForbiddenValues_DokumentumTipus
    {
        get
        {
            // ha még nem volt (sikeres) lekérés
            if (ViewState["EnabledValues_DokumentumTipus"] == null && ViewState["ForbiddenValues_DokumentumTipus"] == null)
            {
                FillViewStateEnabledAndForbiddenValues_Tipus("EnabledValues_DokumentumTipus", "ForbiddenValues_DokumentumTipus");
            }

            return ViewState["ForbiddenValues_DokumentumTipus"] as string[];
        }
    }

    #region Enabled és Forbidden tipusok ViewState-be mentése

    private void FillViewStateEnabledAndForbiddenValues_Tipus(string keyEnabled, string keyForbidden)
    {
        Dictionary<String, String> kodtarak_Dictionary =
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_DOKUMENTUMTIPUS, Page);

        if (kodtarak_Dictionary != null)
        {
            KRT_FunkciokSearch search = new KRT_FunkciokSearch();
            search.Kod.Value = String.Format("KT_{0}_*_*abled", kcs_DOKUMENTUMTIPUS);  // "KT_<kódcsoport kód>_<kódtár kód>_Enabled", "KT_<kódcsoport kód>_<kódtár kód>_Disabled";
            search.Kod.Operator = Query.Operators.like;
            List<string> lstFunkcioKodok = FunctionRights.GetAllFunkcioKod(Page, search);

            List<string> lstEnabledTipusok = new List<string>();
            List<string> lstForbiddenTipusok = new List<string>();

            foreach (string kod in kodtarak_Dictionary.Keys)
            {
                string enabledFunkcio = String.Format("KT_{0}_{1}_Enabled", kcs_DOKUMENTUMTIPUS, kod);
                string disabledFunkcio = String.Format("KT_{0}_{1}_Disabled", kcs_DOKUMENTUMTIPUS, kod);

                if ((lstFunkcioKodok.Contains(enabledFunkcio) && !FunctionRights.GetFunkcioJog(Page, enabledFunkcio))
                    || (lstFunkcioKodok.Contains(disabledFunkcio) && FunctionRights.GetFunkcioJog(Page, disabledFunkcio)))
                {
                    lstForbiddenTipusok.Add(kod);
                }
                else
                {
                    lstEnabledTipusok.Add(kod);
                }
            }

            ViewState[keyEnabled] = lstEnabledTipusok.ToArray();
            ViewState[keyForbidden] = lstForbiddenTipusok.ToArray();
        }

    }
    #endregion Enabled és Forbidden tipusok ViewState-be mentése

    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Standalone vagy csatolmány üzemmód
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        DokumentumTipus = Request.QueryString.Get(QueryStringVars.Tipus);
        if (!String.IsNullOrEmpty(DokumentumTipus))
        {
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_DOKUMENTUMTIPUS, Page).TryGetValue(DokumentumTipus, out DokumentumTipusNev);
        }

        if (Startup == Constants.Startup.Standalone)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "StandaloneDokumentumokList");
        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SzakagiDokumentumokList");
        }
        else
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "DokumentumokList");
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        if (Startup != Constants.Startup.Standalone)
        {
            // Szűrési feltétel van-e iratra:
            IratId = Request.QueryString.Get(QueryStringVars.IratId);
            // Küldeményre:
            KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);
        }
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        string queryString = String.Empty;
        if (!String.IsNullOrEmpty(Startup))
        {
            queryString = String.Format("{0}={1}", QueryStringVars.Startup, Startup);

            if (!String.IsNullOrEmpty(DokumentumTipus))
            {
                queryString += String.Format("&{0}={1}", QueryStringVars.Tipus, HttpUtility.UrlEncode(DokumentumTipus));
            }
        }

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "DokumentumokForm.aspx", queryString, GridViewSelectedId.ClientID, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("DokumentumokSearch.aspx", queryString
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {

            if (this.Startup == Constants.Startup.Standalone)
            {
                #region Header címének beállítása
                LovListHeader1.HeaderTitle = Resources.LovList.StandaloneDokumentumokLovListHeaderTitle;
                #endregion
            }
            else if (this.Startup == Constants.Startup.Szakagi)
            {
                #region Header címének beállítása
                LovListHeader1.HeaderTitle = Resources.LovList.SzakagiDokumentumokLovListHeaderTitle;

                if (!String.IsNullOrEmpty(IratId))
                {
                    #region Header címének beállítása (Irat iktatószámának kiíratása)

                    // Irat GET:
                    EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam_iratGet = UI.SetExecParamDefault(Page);
                    execParam_iratGet.Record_Id = IratId;

                    Result result_iratGet = service_iratok.Get(execParam_iratGet);
                    if (result_iratGet.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result_iratGet);
                        LovListHeader1.ErrorUpdatePanel.Update();
                    }

                    EREC_IraIratok iratObj = (EREC_IraIratok)result_iratGet.Record;

                    LovListHeader1.HeaderTitle += String.Format(" ({0} irat csatolmányai{1})", iratObj.Azonosito
                        , String.IsNullOrEmpty(DokumentumTipusNev) ? "" : String.Format(", {0}", DokumentumTipusNev));

                    #endregion
                }
                else if (!String.IsNullOrEmpty(KuldemenyId))
                {
                    #region Header címének beállítása

                    // Küldemény GET:
                    EREC_KuldKuldemenyekService service_kuldemeny = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execParam_kuldemenyGet = UI.SetExecParamDefault(Page);
                    execParam_kuldemenyGet.Record_Id = KuldemenyId;

                    Result result_kuldemenyGet = service_kuldemeny.Get(execParam_kuldemenyGet);
                    if (result_kuldemenyGet.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result_kuldemenyGet);
                        LovListHeader1.ErrorUpdatePanel.Update();
                    }

                    EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

                    LovListHeader1.HeaderTitle += String.Format(" ({0} küldemény csatolmányai{1})", kuldemenyObj.Azonosito
                        , String.IsNullOrEmpty(DokumentumTipusNev) ? "" : String.Format(", {0}", DokumentumTipusNev));

                    #endregion
                }
                else if (!String.IsNullOrEmpty(DokumentumTipus))
                {
                    LovListHeader1.HeaderTitle += String.Format(" ({0})", DokumentumTipusNev);
                }
                #endregion
            }
            else
            {
                if (!String.IsNullOrEmpty(IratId))
                {
                    #region Header címének beállítása (Irat iktatószámának kiíratása)

                    // Irat GET:
                    EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam_iratGet = UI.SetExecParamDefault(Page);
                    execParam_iratGet.Record_Id = IratId;

                    Result result_iratGet = service_iratok.Get(execParam_iratGet);
                    if (result_iratGet.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result_iratGet);
                        LovListHeader1.ErrorUpdatePanel.Update();
                    }

                    EREC_IraIratok iratObj = (EREC_IraIratok)result_iratGet.Record;

                    LovListHeader1.HeaderTitle = Resources.LovList.DokumentumokLovListHeaderTitle + " (" + iratObj.Azonosito + " irat csatolmányai)";

                    #endregion
                }
                else if (!String.IsNullOrEmpty(KuldemenyId))
                {
                    #region Header címének beállítása

                    // Küldemény GET:
                    EREC_KuldKuldemenyekService service_kuldemeny = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    ExecParam execParam_kuldemenyGet = UI.SetExecParamDefault(Page);
                    execParam_kuldemenyGet.Record_Id = KuldemenyId;

                    Result result_kuldemenyGet = service_kuldemeny.Get(execParam_kuldemenyGet);
                    if (result_kuldemenyGet.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(LovListHeader1.ErrorPanel, result_kuldemenyGet);
                        LovListHeader1.ErrorUpdatePanel.Update();
                    }

                    EREC_KuldKuldemenyek kuldemenyObj = (EREC_KuldKuldemenyek)result_kuldemenyGet.Record;

                    LovListHeader1.HeaderTitle = Resources.LovList.DokumentumokLovListHeaderTitle + " (" + kuldemenyObj.Azonosito + " küldemény csatolmányai)";

                    #endregion
                }
            }
        }

        // Ha van szűrés az iratra vagy a küldeményre, rögtön feltöltjük:
        if (!IsPostBack && (!String.IsNullOrEmpty(IratId) || !String.IsNullOrEmpty(KuldemenyId)))
        {
            FillGridViewSearchResult(false);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhatóság állítása
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        if (IsPostBack)
        {
            String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
            RefreshOnClientClicks(selectedRowId);
        }
    }

    private void RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {

            ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick(
                "Dokumentumok.aspx", QueryStringVars.Command + "=" + CommandName.View
                + "&" + QueryStringVars.Id + "=" + id
                + (String.IsNullOrEmpty(Startup) ? "" : String.Format("&{0}={1}", QueryStringVars.Startup, Startup))
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, null);
        }
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        Contentum.eDocument.Service.KRT_DokumentumokService service = Contentum.eUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        KRT_DokumentumokSearch search = null;

        if (searchObjectFromSession == true)
        {
            if (Startup == Constants.Startup.Standalone)
            {
                search = (KRT_DokumentumokSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new KRT_DokumentumokSearch(), Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch);
            }
            if (Startup == Constants.Startup.Szakagi)
            {
                search = (KRT_DokumentumokSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new KRT_DokumentumokSearch(), Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch);
            }
            else
            {
                search = (KRT_DokumentumokSearch)Search.GetSearchObject(Page, new KRT_DokumentumokSearch());
            }
        }
        else
        {
            search = new KRT_DokumentumokSearch();
            
            if (!String.IsNullOrEmpty(FajlNev_TextBox.Text))
            {
                search.FajlNev.Value = FajlNev_TextBox.Text;
                search.FajlNev.Operator = Search.GetOperatorByLikeCharater(FajlNev_TextBox.Text);
            }

            if (!String.IsNullOrEmpty(FajlTartalom_TextBox.Text))
            {
                // Operator-t nem állítjuk, csak a Value-t:
                search.Manual_FajlTartalom.Filter = FajlTartalom_TextBox.Text;
            }
           
        }

        // Ha van szűrés iratra vagy küldeményre:
        if (!String.IsNullOrEmpty(IratId))
        {
            search.Manual_Irat_Id.Value = IratId;
            search.Manual_Irat_Id.Operator = Query.Operators.equals;
        }
        else if (!String.IsNullOrEmpty(KuldemenyId))
        {
            search.Manual_Kuldemeny_Id.Value = KuldemenyId;
            search.Manual_Kuldemeny_Id.Operator = Query.Operators.equals;
        }
        
        // Rendezés:
        search.OrderBy = "KRT_Dokumentumok.LetrehozasIdo Desc";

        ExecParam execParam = UI.SetExecParamDefault(Page);

        //search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);
        search.TopRow = Rendszerparameterek.Get_LOVLIST_MAX_ROW(Page);

        // Adatok lekérése jogosultságellenőrzéssel:
        Result result = null;
        if (Startup == Constants.Startup.Standalone)
        {
            if (ForbiddenValues_DokumentumTipus != null && ForbiddenValues_DokumentumTipus.Length > 0)
            {
                search.Tipus.Value = Search.GetSqlInnerString(ForbiddenValues_DokumentumTipus);
                search.Tipus.Operator = Query.Operators.notinner;
            }

            result = service.GetAllWithExtensionAndMetaAndJogosultak(execParam, search
                    , null, null
                    , Constants.ColumnNames.KRT_Dokumentumok.Tipus, ForbiddenValues_DokumentumTipus, true
                    , null, false, false, true // bStandaloneMode = true
                    , Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED));

        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            if (!String.IsNullOrEmpty(DokumentumTipus)
                && !Array.Exists<string>(ForbiddenValues_DokumentumTipus
                , delegate(string item) { return item == DokumentumTipus; }))
            {
                search.Tipus.Value = DokumentumTipus;
                search.Tipus.Operator = Query.Operators.equals;
            }
            else
            {
                ui.GridViewClear(GridViewSearchResult);
                return;
            }

            result = service.GetAllWithExtensionAndMetaAndJogosultak(execParam, search
                , null, null
                , Constants.ColumnNames.KRT_Dokumentumok.Tipus, ForbiddenValues_DokumentumTipus, true
                , null, false, false, false // bStandaloneMode = false
                , Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED));

        }
        else
        {
            result = service.GetAllWithExtensionAndJogosultak(execParam, search
                , Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.DOKUMENTUMLISTA_JOGOSULTSAGSZURES_ENABLED));
        }

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }



    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}

