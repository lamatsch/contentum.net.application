﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DokumentumokSearch.aspx.cs" Inherits="DokumentumokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc7" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc4" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc8" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc9" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="objtb" %>
<%@ Register Src="eRecordComponent/IratTextBox.ascx" TagName="IratTextBox" TagPrefix="objtb" %>
<%@ Register Src="eRecordComponent/KuldemenyTextBox.ascx" TagName="KuldemenyTextBox"
    TagPrefix="objtb" %>
<%@ Register Src="eRecordComponent/ObjektumTargyszavakMultiSearch.ascx" TagName="ObjektumTargyszavakMultiSearch"
    TagPrefix="tszms" %>
<%@ Register Src="eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="~/Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--<asp:UpdatePanel ID="MainUpdatePanel" runat="server">
    <ContentTemplate>  --%>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,CsoportokSearchHeaderTitle%>" />
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td style="width: 15%">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="FajlNev_Label" runat="server" Text="Állomány neve:"></asp:Label>&nbsp;
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="FajlNev_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" Text="Méret:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc4:SzamIntervallum_SearchFormControl ID="Meret_SzamIntervallum_SearchFormControl1"
                                            runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Formatum_Label" runat="server" Text="Formátum:"></asp:Label>&nbsp;
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:KodtarakDropDownList ID="Formatum_KodtarakDropDownList" runat="server"></uc7:KodtarakDropDownList>
                                    </td>
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label2" runat="server" Text="Feltöltve:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc5:DatumIntervallum_SearchCalendarControl ID="LetrehozasIdo_DatumIntervallum_SearchCalendarControl1"
                                            runat="server" Validate="false" ValidateDateFormat="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption" style="height: 23px">
                                        <asp:Label ID="Label3" runat="server" Text="Fájl tartalom:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo" style="height: 23px">
                                        <asp:TextBox ID="FajlTartalom_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <asp:UpdatePanel ID="ObjektumTargySzavaiUpdatePanel" runat="server" UpdateMode="Conditional"
                        ChildrenAsTriggers="false">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="KodtarakDropDownList_Tipus" EventName="SelectedIndexChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <eUI:eFormPanel ID="EFormPanelTipus" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption" style="text-align: right; vertical-align: middle;">
                                            <asp:Label ID="labelTipus" runat="server" Text="Típus:" />
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_Tipus" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                            <eUI:eFormPanel ID="EFormPanelObjektumTargyszavai" runat="server" Visible="true">
                                <otp:ObjektumTargyszavaiPanel ID="ObjektumTargyszavaiPanel1" runat="server" SearchMode="true"
                                    RepeatColumns="2" RepeatDirection="Vertical" />
                            </eUI:eFormPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <eUI:eFormPanel ID="EFormPanelOTMultiSearch" runat="server">
                        <tszms:ObjektumTargyszavakMultiSearch ID="tszmsEgyebTargyszavak" runat="server" />
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="AltalanosKeresesPanel" runat="server">
                        <table cellspacing="0" cellpadding="0" width="90%">
                            <tr>
                                <td colspan="4">
                                    <table>
                                        <tr class="urlapSor">
                                            <td class="mrUrlapCaption">
                                                <asp:Label ID="labelAltalanos" runat="server" CssClass="mrUrlapCaption" Text="Ügy/irat/küld.tárgy, ügyindító, beküldő tartalmaz:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="Altalanos_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                            </td>
                                            <td class="mrUrlapCaption">
                                            </td>
                                            <td class="mrUrlapMezo">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                                <td style="width: 15%">
                                </td>
                            </tr>
                            <%--                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaptionBal_nowidth">
                                    <asp:Label ID="Label4" runat="server" Text="Küldemény:"></asp:Label>
                                </td>
                            </tr>--%>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelKuldemeny" runat="server" CssClass="mrUrlapCaption" Text="Küldemény:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <%-- csak bejövő küldemények --%>
                                    <objtb:KuldemenyTextBox ID="KuldemenyTextBox1" SearchMode="true" PostazasIranya="1"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="label7" runat="server" CssClass="mrUrlapCaption" Text="Év:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc10:EvIntervallum_SearchFormControl ID="ErkKonyv_Ev_Evintervallum_searchformcontrol1"
                                        runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Kuld_Label2" runat="server" Text="Érkeztetőkönyv:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc8:IraIktatoKonyvekDropDownList ID="Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList"
                                        EvIntervallum_SearchFormControlId="ErkKonyv_Ev_Evintervallum_searchformcontrol1"
                                        Mode="ErkeztetoKonyvek" Filter_IdeIktathat="true" runat="server" IsMultiSearchMode="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Kuld_Label16" runat="server" Text="Érkeztetési azonosító:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc9:SzamIntervallum_SearchFormControl ID="Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl"
                                        runat="server" />
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <%--                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaptionBal_nowidth">
                                    <asp:Label ID="Label5" runat="server" Text="Irat:"></asp:Label>
                                </td>
                            </tr>--%>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelIrat" runat="server" CssClass="mrUrlapCaption" Text="Irat:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <objtb:IratTextBox ID="IratTextBox1" SearchMode="true" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelEv" runat="server" CssClass="mrUrlapCaption" Text="Év:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc10:EvIntervallum_SearchFormControl ID="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                        runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Ugyirat_Label11" runat="server" Text="Iktatókönyv:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc8:IraIktatoKonyvekDropDownList ID="Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList"
                                        EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                        runat="server" Mode="Iktatokonyvek" Filter_IdeIktathat="true" IsMultiSearchMode="true"/>
                                </td>
                            </tr>
                            <tr class="urlapSor_kicsi">
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="labelUgyirat" runat="server" CssClass="mrUrlapCaption" Text="Ügyirat:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <objtb:UgyiratTextBox ID="UgyiratTextBox1" SearchMode="true" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Ugyirat_Label14" runat="server" Text="Főszám:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc9:SzamIntervallum_SearchFormControl ID="Ugyirat_Foszam_SzamIntervallum_SearchFormControl1"
                                        runat="server" />
                                </td>
                                <td class="mrUrlapCaption_short">
                                    <asp:Label ID="Label6" runat="server" Text="Alszám:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc9:SzamIntervallum_SearchFormControl ID="Irat_Alszam_SzamIntervallum_SearchFormControl2"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="ErvenyessegSearchPanel" runat="server" Visible="false">
                        <table>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                         <uc:PartnerTextBox ID="Partner" runat="server" SearchMode="true" WithAllCimCheckBoxVisible="false" />
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                            <tr>
                                <td colspan="4">
                                     <uc:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </eUI:eFormPanel>
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td>
                                <uc3:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server"></uc3:TalalatokSzama_SearchFormComponent>
                            </td>
                            <td style="width: 50%">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
