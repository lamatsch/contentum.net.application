﻿using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Utility;
using System.Data;
using Contentum.eQuery.FullTextSearch;
using System.Collections.Generic;

public partial class DokumentumokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_DokumentumokSearch);

    private const string kcsKod_DOKUMENTUM_FORMATUM = "DOKUMENTUM_FORMATUM";
    private const string kcs_DOKUMENTUMTIPUS = "DOKUMENTUMTIPUS";

    private string Startup = String.Empty;
    private string DokumentumTipus = String.Empty;
    private string DokumentumTipusNev = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

        Startup = Request.QueryString.Get(QueryStringVars.Startup);
        DokumentumTipus = Request.QueryString.Get(QueryStringVars.Tipus);

        if (!String.IsNullOrEmpty(DokumentumTipus))
        {
            Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_DOKUMENTUMTIPUS, Page).TryGetValue(DokumentumTipus, out DokumentumTipusNev);
        }

        if (Startup == Constants.Startup.Standalone)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch;

            AltalanosKeresesPanel.Visible = false;

            if (Rendszerparameterek.IsTUK(Page))
            {
                ErvenyessegSearchPanel.Visible = true;
            }

            if (!IsPostBack)
            {
                KodtarakDropDownList_Tipus.FillAndSetEmptyValue(kcs_DOKUMENTUMTIPUS, SearchHeader1.ErrorPanel, true);
            }
        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch;

            if (!IsPostBack)
            {
                KodtarakDropDownList_Tipus.FillAndSetEmptyValue(kcs_DOKUMENTUMTIPUS, SearchHeader1.ErrorPanel, true);
            }
            
        }
        else
        {
            EFormPanelOTMultiSearch.Visible = false;
            EFormPanelObjektumTargyszavai.Visible = false;
            EFormPanelTipus.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Startup == Constants.Startup.Standalone)
        {
            SearchHeader1.HeaderTitle = Resources.Search.StandaloneDokumentumokSearchHeaderTitle;
        }
        else if (Startup == Constants.Startup.Szakagi)
        {
            SearchHeader1.HeaderTitle = Resources.Search.SzakagiDokumentumokSearchHeaderTitle;

            if (!String.IsNullOrEmpty(DokumentumTipus))
            {
                SearchHeader1.HeaderTitle += String.Format(" ({0})", DokumentumTipusNev);
            }
        }
        else
        {
            SearchHeader1.HeaderTitle = Resources.Search.DokumentumokSearchHeaderTitle;
        }

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        SearchFooter1.ImageButton_Search.OnClientClick = "$get('" + CustomUpdateProgress1.UpdateProgress.ClientID + "').style.display = 'block';";

        if (Startup == Constants.Startup.Standalone) // || Startup == Constants.Startup.Szakagi)
        {
            KodtarakDropDownList_Tipus.DropDownList.SelectedIndexChanged += new EventHandler(DropDownList_SelectedIndexChanged);
            KodtarakDropDownList_Tipus.DropDownList.AutoPostBack = true;
        }

        if (!IsPostBack)
        {
            KRT_DokumentumokSearch searchObject = null;
            if (Startup == Constants.Startup.Standalone
                && Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch))
            {
                searchObject = (KRT_DokumentumokSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new KRT_DokumentumokSearch(), Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch);
            }
            else if (Startup == Constants.Startup.Szakagi
                && Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch))
            {
                searchObject = (KRT_DokumentumokSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new KRT_DokumentumokSearch(), Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch);
            }
            else if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_DokumentumokSearch)Search.GetSearchObject(Page, new KRT_DokumentumokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }

            LoadComponentsFromSearchObject(searchObject);
        }
    }


    protected void DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        EFormPanelObjektumTargyszavai.Visible = true;
        FillObjektumTargyszavaiPanelForSearch(((DropDownList)sender).SelectedValue);
        if (ObjektumTargyszavaiPanel1.Count == 0)
        {
            EFormPanelObjektumTargyszavai.Visible = false;
        }
    }

    protected void FillObjektumTargyszavaiPanelForSearch(String tipus)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        ObjektumTargyszavaiPanel1.FillObjektumTargyszavai(search, null, null
        , Constants.TableNames.KRT_Dokumentumok, Constants.ColumnNames.KRT_Dokumentumok.Tipus, new string [] {tipus}, false
        , null, false, false, SearchHeader1.ErrorPanel);
    }

    //protected DataTable GetAllMetaByDokumentumTipus(String id, String tipus)
    //{
    //    Contentum.eRecord.Service.EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
    //    ExecParam execParam_ot = UI.SetExecParamDefault(Page);

    //    EREC_ObjektumTargyszavaiSearch search_ot = new EREC_ObjektumTargyszavaiSearch();

    //    Result result_ot = service_ot.GetAllMetaByObjMetaDefinicio(execParam_ot, search_ot, id, null
    //        , Constants.TableNames.KRT_Dokumentumok, Constants.ColumnNames.KRT_Dokumentumok.Tipus, new string[] {tipus}, false
    //        , null, false, false);

    //    if (result_ot.IsError)
    //    {
    //        ResultError.DisplayResultErrorOnErrorPanel(SearchHeader1.ErrorPanel, result_ot);
    //    }

    //    return result_ot.Ds.Tables[0];
    //}

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_DokumentumokSearch krt_DokumentumokSearch = null;
        if (searchObject != null) krt_DokumentumokSearch = (KRT_DokumentumokSearch)searchObject;

        if (krt_DokumentumokSearch != null)
        {
            FajlNev_TextBox.Text = krt_DokumentumokSearch.FajlNev.Value;

            Formatum_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_DOKUMENTUM_FORMATUM
                , krt_DokumentumokSearch.Formatum.Value, true, SearchHeader1.ErrorPanel);

            Meret_SzamIntervallum_SearchFormControl1.SetComponentFromSearchObjectFields(krt_DokumentumokSearch.Meret);

            LetrehozasIdo_DatumIntervallum_SearchCalendarControl1.SetComponentFromSearchObjectFields(krt_DokumentumokSearch.Manual_LetrehozasIdo);

            FajlTartalom_TextBox.Text = krt_DokumentumokSearch.Manual_FajlTartalom.Filter;

            if (krt_DokumentumokSearch.Fts_altalanos != null)
            {
                Altalanos_TextBox.Text = krt_DokumentumokSearch.Fts_altalanos.Filter;
            }
            else
            {
                Altalanos_TextBox.Text = "";
            }

            // Év:
            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                              krt_DokumentumokSearch.Manual_Iktatokonyv_Ev);

            // Iktatókönyv feltöltés:
            Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
               , true, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
               , true, false
               , IktatoKonyvek.GetIktatokonyvValue(krt_DokumentumokSearch.Manual_Iktatokonyv_Iktatohely.Value
                    , krt_DokumentumokSearch.Manual_Iktatokonyv_Megkuljelzes.Value)
               , SearchHeader1.ErrorPanel);

            // Főszám:
            Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SetComponentFromSearchObjectFields(
                            krt_DokumentumokSearch.Manual_Ugyirat_Foszam);

            // Alszám:
            Irat_Alszam_SzamIntervallum_SearchFormControl2.SetComponentFromSearchObjectFields(
                krt_DokumentumokSearch.Manual_Irat_Alszam);

            // Érkeztetőkönyv év:
            ErkKonyv_Ev_Evintervallum_searchformcontrol1.SetComponentFromSearchObjectFields(
                krt_DokumentumokSearch.Manual_Erkeztetokonyv_Ev);

            // Érkköny feltöltés:
            Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                , true, ErkKonyv_Ev_Evintervallum_searchformcontrol1.EvTol, ErkKonyv_Ev_Evintervallum_searchformcontrol1.EvIg
                , true, false,
                    IktatoKonyvek.GetIktatokonyvValue(krt_DokumentumokSearch.Manual_Erkeztetokonyv_Iktatohely.Value
                        , krt_DokumentumokSearch.Manual_Erkeztetokonyv_Megkuljelzes.Value)
                , SearchHeader1.ErrorPanel);

            // Érkeztetési azonosító:
            Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                krt_DokumentumokSearch.Manual_Kuldemeny_ErkeztetoSzam);

            // objektumok
            KuldemenyTextBox1.Id_HiddenField = krt_DokumentumokSearch.Manual_Kuldemeny_Id.Value;
            KuldemenyTextBox1.SetKuldemenyTextBoxById(SearchHeader1.ErrorPanel, null);

            IratTextBox1.Id_HiddenField = krt_DokumentumokSearch.Manual_Irat_Id.Value;
            IratTextBox1.SetIratTextBoxById(SearchHeader1.ErrorPanel, null);

            UgyiratTextBox1.Id_HiddenField = krt_DokumentumokSearch.Manual_Ugyirat_Id.Value;
            UgyiratTextBox1.SetUgyiratTextBoxById(SearchHeader1.ErrorPanel, null);

            #region FTS

            #region objektum tárgyszavai
            if (Startup == Constants.Startup.Standalone)
            {
                KodtarakDropDownList_Tipus.SetSelectedValue(krt_DokumentumokSearch.Tipus.Value);
                DropDownList_SelectedIndexChanged(KodtarakDropDownList_Tipus.DropDownList, new EventArgs());
            }
            else if (Startup == Constants.Startup.Szakagi)
            {
                if (!String.IsNullOrEmpty(DokumentumTipus))
                {
                    KodtarakDropDownList_Tipus.SetSelectedValue(DokumentumTipus);
                    DropDownList_SelectedIndexChanged(KodtarakDropDownList_Tipus.DropDownList, new EventArgs());
                    EFormPanelTipus.Visible = false;
                }
                else
                {
                    KodtarakDropDownList_Tipus.SetSelectedValue(krt_DokumentumokSearch.Tipus.Value);
                    DropDownList_SelectedIndexChanged(KodtarakDropDownList_Tipus.DropDownList, new EventArgs());
                }

            }

            // dokumentum hierarchikus auto
            //ObjektumTargyszavaiPanel1.FillObjektumTargyszavai(null, null, null, Constants.TableNames.KRT_Dokumentumok
            //    , "*" , null, null, false, true, SearchHeader1.ErrorPanel);
            

            //if (ObjektumTargyszavaiPanel1.Count > 0)
            //{
            //    EFormPanelObjektumTargyszavai.Visible = true;
                ObjektumTargyszavaiPanel1.FillFromFTSTree(krt_DokumentumokSearch.fts_tree_objektumtargyszavai);
            //}
            //else
            //{
            //    EFormPanelObjektumTargyszavai.Visible = false;
            //}

            #endregion objektum tárgyszavai

            #region egyéb tárgyszavak
            tszmsEgyebTargyszavak.FillFromFTSTree(krt_DokumentumokSearch.fts_tree_egyeb);
            #endregion egyéb tárgyszavak

            #endregion FTS

            if (!String.IsNullOrEmpty(krt_DokumentumokSearch.Partner_id))
            {
                Partner.Id_HiddenField = krt_DokumentumokSearch.Partner_id;
                Partner.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
            }

            Ervenyesseg_SearchFormComponent1.SetDefault(krt_DokumentumokSearch.ErvKezd, krt_DokumentumokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_DokumentumokSearch SetSearchObjectFromComponents()
    {
        KRT_DokumentumokSearch krt_DokumentumokSearch = (KRT_DokumentumokSearch)SearchHeader1.TemplateObject;
        if (krt_DokumentumokSearch == null)
        {
            krt_DokumentumokSearch = new KRT_DokumentumokSearch();
        }

        if (!String.IsNullOrEmpty(FajlNev_TextBox.Text))
        {
            krt_DokumentumokSearch.FajlNev.Value = FajlNev_TextBox.Text;
            krt_DokumentumokSearch.FajlNev.Operator = Search.GetOperatorByLikeCharater(FajlNev_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Formatum_KodtarakDropDownList.SelectedValue))
        {
            krt_DokumentumokSearch.Formatum.Value = Formatum_KodtarakDropDownList.SelectedValue;
            krt_DokumentumokSearch.Formatum.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(FajlTartalom_TextBox.Text))
        {
            // Operator-t nem állítjuk, csak a Value-t:
            krt_DokumentumokSearch.Manual_FajlTartalom.Filter = FajlTartalom_TextBox.Text;            
        }

        Meret_SzamIntervallum_SearchFormControl1.SetSearchObjectFields(krt_DokumentumokSearch.Meret);

        LetrehozasIdo_DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(krt_DokumentumokSearch.Manual_LetrehozasIdo);
        
        if (!String.IsNullOrEmpty(Altalanos_TextBox.Text))
        {
            krt_DokumentumokSearch.Fts_altalanos.Filter = Altalanos_TextBox.Text;
        }
                
        // Iktatókönyv év:
        Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(krt_DokumentumokSearch.Manual_Iktatokonyv_Ev);
        
        // Megkül.jelzés
        if (!String.IsNullOrEmpty(Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue))
        {
            Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SetSearchObject(
                krt_DokumentumokSearch.Manual_Iktatokonyv_Iktatohely, krt_DokumentumokSearch.Manual_Iktatokonyv_Megkuljelzes);
        }

        // Főszám:
        Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SetSearchObjectFields(krt_DokumentumokSearch.Manual_Ugyirat_Foszam);

        // Alszám:
        Irat_Alszam_SzamIntervallum_SearchFormControl2.SetSearchObjectFields(krt_DokumentumokSearch.Manual_Irat_Alszam);

        // Érkeztetőkönyv év:
        ErkKonyv_Ev_Evintervallum_searchformcontrol1.SetSearchObjectFields(krt_DokumentumokSearch.Manual_Erkeztetokonyv_Ev);
        
        // Megkül.jelzés
        if (!String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue))
        {
            Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SetSearchObject(
                krt_DokumentumokSearch.Manual_Erkeztetokonyv_Iktatohely, krt_DokumentumokSearch.Manual_Erkeztetokonyv_Megkuljelzes);
        }

        // Érkeztetési azonosító:
        Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(krt_DokumentumokSearch.Manual_Kuldemeny_ErkeztetoSzam);

        // objektumok
        if (!String.IsNullOrEmpty(KuldemenyTextBox1.Id_HiddenField))
        {
            krt_DokumentumokSearch.Manual_Kuldemeny_Id.Value = KuldemenyTextBox1.Id_HiddenField;
            krt_DokumentumokSearch.Manual_Kuldemeny_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(IratTextBox1.Id_HiddenField))
        {
            krt_DokumentumokSearch.Manual_Irat_Id.Value = IratTextBox1.Id_HiddenField;
            krt_DokumentumokSearch.Manual_Irat_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(UgyiratTextBox1.Id_HiddenField))
        {
            krt_DokumentumokSearch.Manual_Ugyirat_Id.Value = UgyiratTextBox1.Id_HiddenField;
            krt_DokumentumokSearch.Manual_Ugyirat_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_Tipus.SelectedValue))
        {
            krt_DokumentumokSearch.Tipus.Value = KodtarakDropDownList_Tipus.SelectedValue;
            krt_DokumentumokSearch.Tipus.Operator = Query.Operators.equals;
        }

        #region FTS

        #region objektum tárgyszavai
        // dokumentum hierarchikus auto
        try
        {
            krt_DokumentumokSearch.fts_tree_objektumtargyszavai = ObjektumTargyszavaiPanel1.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion objektum tárgyszavai

        #region egyéb tárgyszavak
        try
        {
            krt_DokumentumokSearch.fts_tree_egyeb = tszmsEgyebTargyszavak.BuildFTSTree(SearchHeader1.ErrorPanel);
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion egyéb tárgyszavak

        #region merge objektum tárgyszavai és egyéb tárgyszavak
        try
        {
            FullTextSearchTree mergedTree = FullTextSearchTree.MergeFTSTrees(krt_DokumentumokSearch.fts_tree_objektumtargyszavai
                , krt_DokumentumokSearch.fts_tree_egyeb, FullTextSearchTree.Operator.Intersect);

            if (mergedTree != null)
            {
                krt_DokumentumokSearch.ObjektumTargyszavai_ObjIdFilter = mergedTree.TransformToFTSContainsConditions();
            }
            else
            {
                krt_DokumentumokSearch.ObjektumTargyszavai_ObjIdFilter = "";
            }
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion merge objektum tárgyszavai és egyéb tárgyszavak
        #endregion FTS

        if (!String.IsNullOrEmpty(Partner.Id_HiddenField))
        {
            krt_DokumentumokSearch.Partner_id = Partner.Id_HiddenField;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(krt_DokumentumokSearch.ErvKezd, krt_DokumentumokSearch.ErvVege);

        return krt_DokumentumokSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added: Találati listák mentése
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            SearchHeader1.NewResultList(SetSearchObjectFromComponents(), execParam);
        }
        //bernat.laszlo eddig
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_DokumentumokSearch searchObject = SetSearchObjectFromComponents();

            #region Fájltartalom-ra keresés esetén keresés Sharepoint-ból (kikommentezve, már tárolt eljárásból megy)
            //if (!string.IsNullOrEmpty(searchObject.Manual_FajlTartalom.Value))
            //{            

            //    // Keresés tartalomra Sharepointból
            //    // Eredményül KRT_Dokumentumok.External_Id -kat kapunk --> erre állítjuk be a szűrést

            //    Contentum.eDocument.Service.DocumentService documentService = eDocumentService.ServiceFactory.GetDocumentService();

            //    Result result_fajlTartalom = documentService.MossKeresEx(
            //        UI.SetExecParamDefault(Page),
            //        searchObject.Manual_FajlTartalom.Value,
            //        Guid.NewGuid().ToString(),
            //        1,
            //        0);

            //    if (result_fajlTartalom.IsError)
            //    {
            //        // hiba:
            //        ResultError.DisplayResultErrorOnErrorPanel(SearchHeader1.ErrorPanel, result_fajlTartalom);
            //        return;
            //    }

            //    string externalIds = String.Empty;

            //    //Label_FajlTartalomResult.Text = "";

            //    foreach (DataRow row in result_fajlTartalom.Ds.Tables[0].Rows)
            //    {
            //        string fileName = row["Title"].ToString();
            //        string url = row["Path"].ToString();

            //        string uniqueId = row["UniqueId"].ToString();

            //        //Label_FajlTartalomResult.Text += "<br/><span onclick=\"window.open('" + url + "'); return;\" >" + fileName + "</span><br/>"
            //        //        + url + "<br/>" + uniqueId + "<br/>";

            //        if (!string.IsNullOrEmpty(uniqueId))
            //        {
            //            if (string.IsNullOrEmpty(externalIds))
            //            {
            //                externalIds = "'" + uniqueId + "'";
            //            }
            //            else
            //            {
            //                externalIds += ",'" + uniqueId + "'";
            //            }
            //        }
            //    }

            //    //return;

            //    if (!string.IsNullOrEmpty(externalIds))
            //    {
            //        searchObject.External_Id.Value = externalIds;
            //        searchObject.External_Id.Operator = Query.Operators.inner;
            //    }
            //    else
            //    {
            //        // ha nincs találat a sharepointban, akkor mindenképpen üres listát kell hozni:
            //        // összerakunk egy HAMIS feltételt:
            //        searchObject.External_Id.Name = "1";
            //        searchObject.External_Id.Type = "String";
            //        searchObject.External_Id.Value = "2";
            //        searchObject.External_Id.Operator = Query.Operators.equals;
            //    }

                        
            //}
                #endregion

            // A searchObject.Manual_FajlTartalom -at külön kell nézni, mert az nem megy bele a Where-be (és az alapján hasonlítunk)
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()) && string.IsNullOrEmpty(searchObject.Manual_FajlTartalom.Filter)
                && string.IsNullOrEmpty(searchObject.Fts_altalanos.Filter)
                && searchObject.fts_tree_objektumtargyszavai == null
                && searchObject.fts_tree_egyeb == null
                && String.IsNullOrEmpty(searchObject.Partner_id))
            {
                // default searchobject
                if (Startup == Constants.Startup.Standalone)
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(
                        Page, Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch);
                }
                else if (Startup == Constants.Startup.Szakagi)
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(
                        Page, Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                //Mivel van UpdatePanelen belül is eFormPanel, egyenként adjuk hozzá
                searchObject.ReadableWhere = Search.GetReadableWhere(EFormPanel1)
                    + Search.GetReadableWhere(EFormPanelTipus)
                    + Search.GetReadableWhere(EFormPanelObjektumTargyszavai)
                    + Search.GetReadableWhere(EFormPanelOTMultiSearch)
                    + Search.GetReadableWhere(AltalanosKeresesPanel)
                    + Search.GetReadableWhere(ErvenyessegSearchPanel);

                if (Startup == Constants.Startup.Standalone)
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.StandaloneDokumentumokSearch);
                }
                else if (Startup == Constants.Startup.Szakagi)
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.SzakagiDokumentumokSearch);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_DokumentumokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_DokumentumokSearch();
    }

}
