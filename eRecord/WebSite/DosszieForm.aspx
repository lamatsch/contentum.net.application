﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="DosszieForm.aspx.cs" Inherits="DosszieForm" Title="Untitled form" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/VonalkodTextBox.ascx" TagName="VonalkodTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/JogosultakTab.ascx" TagName="JogosultakTab" TagPrefix="tp7" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="cstb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <ajaxToolkit:TabContainer ID="FormTabcontainer" runat="server">
                            <ajaxToolkit:TabPanel ID="DossziePanel" runat="server">
                                <HeaderTemplate>
                                    Dosszié</HeaderTemplate>
                                <ContentTemplate>
                                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                        <table cellspacing="5" cellpadding="0" width="100%">
                                            <tr>
                                                <td valign="top" align="right">
                                                    <table>
                                                        <tr class="urlapSor">
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label9" runat="server" Text="Szülő dosszié neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:HiddenField ID="SzuloDosszie_IdHiddenField" runat="server" />
                                                                <asp:TextBox ID="SzuloDosszieNeve_TextBox" runat="server" ReadOnly="true" />
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="SetRootButton" runat="server" OnClick="SetRootButton_Clicked"
                                                                    ImageUrl="images/hu/trapezgomb/dossziebol_kivesz_trap.gif" onmouseover="swapByName(this.id,'dossziebol_kivesz_trap2.gif')"
                                                                    onmouseout="swapByName(this.id,'dossziebol_kivesz_trap.gif')" CausesValidation="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor">
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label8" runat="server" Text="Dosszié útja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="2">
                                                                <asp:Panel ID="SzuloDosszie_TreeViewPanel" runat="server" Height="300" ScrollBars="Both"
                                                                    BackColor="White">
                                                                    <asp:TreeView ID="SzuloDosszie_TreeView" runat="server" Style="background-color: White;"
                                                                        CssClass="mrUrlapInput" ExpandImageToolTip="Kinyitás" CollapseImageToolTip="Összecsukás">
                                                                        <SelectedNodeStyle Font-Bold="False" BackColor="#E0E0E0" />
                                                                    </asp:TreeView>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" align="left">
                                                    <table>
                                                        <tr class="urlapSor">
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label1" runat="server" Text="Dosszié neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc3:RequiredTextBox ID="Nev_TextBox" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label3" runat="server" Text="Vonalkód:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc4:VonalkodTextBox ID="Vonalkod_TextBox" runat="server" RequiredValidate="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label7" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label2" runat="server" Text="Típus:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc5:KodtarakDropDownList ID="Tipus_DropDown" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label10" runat="server" Text="Tulajdonos:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <cstb:CsoportTextBox ID="CsoportTextBox" runat="server" ReadOnly="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mrUrlapCaption">
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:CheckBox ID="HasznalatiMod_CheckBox" runat="server" Text="Saját szervezeten belül publikus:"
                                                                    TextAlign="Left" Visible="false" Checked="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mrUrlapCaption">
                                                                <asp:Label ID="Label5" runat="server" Text="Leírás:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="Leiras_TextBox" runat="server" Rows="4" TextMode="MultiLine" CssClass="mrUrlapInput" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </eUI:eFormPanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabJogosultakPanel" runat="server" TabIndex="6">
                                <HeaderTemplate>
                                    <%-- <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
				                <ContentTemplate>--%>
                                    <asp:Label ID="Label6" runat="server" Text="Jogosultak" />
                                    <%--</ContentTemplate>
					        </asp:UpdatePanel>       --%>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <tp7:JogosultakTab ID="JogosultakTab1" runat="server" />
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
