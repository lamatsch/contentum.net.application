﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eUtility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;

public partial class DosszieForm : System.Web.UI.Page
{
    private const string imageUrlVirtualis = "images/hu/egyeb/virtualis_dosszie.gif"; //"~/images/hu/egyeb/virtualis_dosszie.gif";
    private const string imageUrlFizikai = "images/hu/egyeb/fizikai_dosszie.gif"; // "~/images/hu/egyeb/fizikai_dosszie.gif";

    private string Command = "";
    
    private PageView pageView = null;

    private const string kcsKod_Tipus = "MAPPA_TIPUS";

    //private const string confirmText = "A rendben gomb megnyomása után a dosszié helye megváltozik! Biztos, hogy folytatja?";
    private const string returnFalse = "return false;";

    private bool IsFizikaiDosszie(TreeNode tn)
    {
        // workaround: a checkbox állapot alapján vezetjük vissza típust
        return tn != null && tn.Checked;
        //tn.ImageUrl == imageUrlFizikai;
    }

    private bool HasDosszieFizikaiSzulo(TreeNode tn)
    {
        bool hasPhysicalParent = false;
        if (tn != null)
        {
            tn = tn.Parent;
            while (tn != null && !hasPhysicalParent)
            {
                hasPhysicalParent = IsFizikaiDosszie(tn);
                tn = tn.Parent;
            }
        }
        return hasPhysicalParent;
    }

    #region Base Tree Dictionary

    #region Statusz
    [Serializable]
    protected class Statusz
    {
        #region Properties
        private string id;
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nev;
        public string Nev
        {
            get { return nev; }
            set { nev = value; }
        }

        private string tipus;
        public string Tipus
        {
            get { return tipus; }
            set { tipus = value; }
        }

        private string allapot;
        public string Allapot
        {
            get { return allapot; }
            set { allapot = value; }
        }

        private string csoport_id_tulaj;
        public string Csoport_Id_Tulaj
        {
            get { return csoport_id_tulaj; }
            set { csoport_id_tulaj = value; }
        }

        private string szulomappa_id;
        public string SzuloMappa_Id
        {
            get { return szulomappa_id; }
            set { szulomappa_id = value; }
        }
        #endregion Properties

        #region Constructors
        public Statusz(string _id, string _nev)
        {
            this.id = _id;
            this.nev = _nev;
        }

        public Statusz(System.Data.DataRow row)
        {
            if (row.Table.Columns.Contains("Id"))
            {
                this.id = row["Id"].ToString();
            }
            if (row.Table.Columns.Contains("Nev"))
            {
                this.nev = row["Nev"].ToString();
            }
            if (row.Table.Columns.Contains("Tipus"))
            {
                this.tipus = row["Tipus"].ToString();
            }
            if (row.Table.Columns.Contains("Allapot"))
            {
                this.allapot = row["Allapot"].ToString();
            }
            if (row.Table.Columns.Contains("Csoport_Id_Tulaj"))
            {
                this.csoport_id_tulaj = row["Csoport_Id_Tulaj"].ToString();
            }
            if (row.Table.Columns.Contains("SzuloMappa_Id"))
            {
                this.szulomappa_id = row["SzuloMappa_Id"].ToString();
            }
        }
        #endregion Constructors
    }
    #endregion Statusz

    System.Collections.Generic.Dictionary<String, Statusz> MappakDictionary
    {
        get
        {
            if (ViewState["MappakDictionary"] == null)
            {
                ViewState["MappakDictionary"] = new System.Collections.Generic.Dictionary<string, Statusz>();
            }
            return (System.Collections.Generic.Dictionary<String, Statusz>)ViewState["MappakDictionary"];
        }
        set { ViewState["MappakDictionary"] = value; }
    }
        

    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.DossziekFormHeaderTitle;
        
        FormFooter1.ButtonsClick += new System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (!IsPostBack)
        {
            Tipus_DropDown.FillAndSetSelectedValue(kcsKod_Tipus, KodTarak.MAPPA_TIPUS.Virtualis, FormHeader1.ErrorPanel);
        }

        if (Command == CommandName.New)
        {

            SzuloDosszie_TreeView.SelectedNodeChanged += new EventHandler(SzuloDosszie_TreeView_SelectedNodeChanged);//=OnSelectedNodeChanged="SzuloDosszie_TreeView_SelectedNodeChanged"

            if (!IsPostBack)
            {
                this.FillSzuloTreeView_ForNewForm(Request.QueryString["SzuloMappa_Id"]);

                this.CsoportTextBox.Id_HiddenField = UI.SetExecParamDefault(Page).Felhasznalo_Id;
                this.CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }

        }
        else if (Command == CommandName.View || Command == CommandName.Modify)
        {
            if (!IsPostBack)
            {
                KRT_MappakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_MappakService();

                if (Command == CommandName.Modify)
                {
                    ExecParam execParam_statusz = UI.SetExecParamDefault(Page);

                    execParam_statusz.Record_Id = Request.QueryString["Id"];
                    Result result_statusz = service.GetStatus(execParam_statusz, Request.QueryString["Id"]);

                    if (result_statusz.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_statusz);
                        UpdatePanel1.Visible = false;
                        FormFooter1.Command = CommandName.View;
                    }
                    else if (result_statusz.Ds.Tables[0].Rows[0]["Tipus"].ToString() == KodTarak.MAPPA_TIPUS.Fizikai
                        && result_statusz.Ds.Tables[0].Rows[0]["Allapot"].ToString() == "1")
                    {
                        // A fizikai dosszié nem módosítható, mert mozgásban van!
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_64035);
                        UpdatePanel1.Visible = false;
                        FormFooter1.Command = CommandName.View;
                        return;
                    }
                }

                ExecParam execParam = UI.SetExecParamDefault(Page);
                execParam.Record_Id = Request.QueryString["Id"];

                Result result = service.Get(execParam);

                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UpdatePanel1.Visible = false;
                    return;
                }
                else
                {
                    KRT_Mappak krt_Mappak = result.Record as KRT_Mappak;
                    this.LoadComponentsFromBusinessObject(krt_Mappak);
                }

            }
        }

        if (Command == CommandName.View)
        {
            if (!IsPostBack)
            {
                this.FillSzuloTreeView_ForViewForm(Request.QueryString["Id"]);

                Result result_forView = this.FillSzuloTreeView_ForViewForm(Request.QueryString["Id"]);

                if (result_forView.IsError)
                {
                    UpdatePanel1.Visible = false;
                    return;
                }
            }
        }
        else if (Command == CommandName.Modify)
        {
            SzuloDosszie_TreeView.SelectedNodeChanged += new EventHandler(SzuloDosszie_TreeView_SelectedNodeChanged);//=OnSelectedNodeChanged="SzuloDosszie_TreeView_SelectedNodeChanged"

            //this.SetRootButton.OnClientClick = "if (!confirm('" + confirmText + "')) return false;";

            if (!IsPostBack)
            {
                Result result_forModify = this.FillSzuloTreeView_ForModifyForm(Request.QueryString["Id"]);

                if (result_forModify.IsError)
                {
                    UpdatePanel1.Visible = false;
                    return;
                }

                // A szülő fában a mappa szülő mappáját jelöljük ki
                this.SetNewParentNode(Request.QueryString["Id"], Request.QueryString["Id"]);
            }
        }

        if (!IsPostBack)
            this.SetSzuloDosszie_TextBox_Text();

        this.SetControlsStyle();

        registerJavaScripts();

    }

    private void SetControlsStyle()
    {
        switch (Command)
        {
            case CommandName.New:
                this.TabJogosultakPanel.Visible = false;
                this.HasznalatiMod_CheckBox.Visible = true;
                break;
            case CommandName.Modify:
                break;
            case CommandName.View:
                this.SzuloDosszie_TreeView.Enabled = false;
                this.Nev_TextBox.ReadOnly = true;
                this.Vonalkod_TextBox.ReadOnly = true;
                this.Tipus_DropDown.ReadOnly = true;
                this.Leiras_TextBox.ReadOnly = true;
                this.SetRootButton.Visible = false;
                break;
        }
    }



    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Mappak krt_mappak)
    {
        this.Nev_TextBox.Text = krt_mappak.Nev;
        this.Tipus_DropDown.SelectedValue = krt_mappak.Tipus;
        this.Vonalkod_TextBox.Text = krt_mappak.BarCode;
        
        this.CsoportTextBox.Id_HiddenField = krt_mappak.Csoport_Id_Tulaj;
        this.CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        
        this.Leiras_TextBox.Text = krt_mappak.Leiras;

        this.FormHeader1.Record_Ver = krt_mappak.Base.Ver;
    }

    // form --> business object
    private KRT_Mappak GetBusinessObjectFromComponents()
    {
        KRT_Mappak krt_mappak = new KRT_Mappak();
        krt_mappak.Updated.SetValueAll(false);
        krt_mappak.Base.Updated.SetValueAll(false);

        krt_mappak.SzuloMappa_Id = SzuloDosszie_IdHiddenField.Value;
        krt_mappak.Updated.SzuloMappa_Id = true;

        krt_mappak.Nev = this.Nev_TextBox.Text;
        krt_mappak.Updated.Nev = pageView.GetUpdatedByView(this.Nev_TextBox);

        krt_mappak.Tipus = this.Tipus_DropDown.SelectedValue;
        krt_mappak.Updated.Tipus = pageView.GetUpdatedByView(this.Tipus_DropDown);

        if (krt_mappak.Tipus == KodTarak.MAPPA_TIPUS.Fizikai)
        {
            krt_mappak.BarCode = this.Vonalkod_TextBox.Text;
            krt_mappak.Updated.BarCode = pageView.GetUpdatedByView(this.Vonalkod_TextBox);
        }
        else
        {
            krt_mappak.BarCode = "";
            krt_mappak.Updated.BarCode = krt_mappak.Updated.Tipus;
        }

        krt_mappak.Leiras = this.Leiras_TextBox.Text;
        krt_mappak.Updated.Leiras = pageView.GetUpdatedByView(this.Leiras_TextBox);

        // Csoport_Id_Tulaj-t nem formról olvassuk be
        if (Command == CommandName.New)
        {
            // nem módosítjuk! (azt az átadás végezheti)
            krt_mappak.Csoport_Id_Tulaj = UI.SetExecParamDefault(Page).Felhasznalo_Id;
            krt_mappak.Updated.Csoport_Id_Tulaj = true;
        }

        krt_mappak.Base.Ver = FormHeader1.Record_Ver;
        krt_mappak.Base.Updated.Ver = true;

        return krt_mappak;
    }


    #region FormTab

    protected void UgyUgyiratokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        //JogosultakTab1.Active = sende
        //ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    #endregion FormTab


    #region Tipus dropdown
    private void FillDropDownByTipus(string Tipus)
    {
        Tipus_DropDown.FillDropDownList(kcsKod_Tipus, FormHeader1.ErrorPanel);

        if (Tipus == KodTarak.MAPPA_TIPUS.Fizikai)
        {
            // fizikai alatt nem hozható létre virtuális
            ListItem liVirtualis = Tipus_DropDown.DropDownList.Items.FindByValue(KodTarak.MAPPA_TIPUS.Virtualis);
            if (liVirtualis != null)
            {
                Tipus_DropDown.DropDownList.Items.Remove(liVirtualis);
            }
        }
    }

    #endregion Tipus dropdown

    #region TreeView methodok

    private Result FillSzuloTreeView_ForNewForm(string SzuloMappaId)
    {
        KRT_MappakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_MappakService();

        ExecParam execParam = new ExecParam();
        execParam = UI.SetExecParamDefault(Page, execParam);
        execParam.Record_Id = SzuloMappaId;

        //Result result = service.GetWithTreeAndRightCheck(execParam);
        Result result = service.GetAllWithExtensionAndJogosultak(execParam, new KRT_MappakSearch());

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return result;
        }

        this.FillSzuloTreeView_FromResult(result, SzuloMappaId, CommandName.New);

        System.Data.DataRow[] rows = result.Ds.Tables[0].Select(String.Format("Id='{0}'", SzuloMappaId));

        if (rows.Length > 0)
        {
            String Tipus = rows[0]["Tipus"].ToString();
            FillDropDownByTipus(Tipus);

            // Command = New
            string csoport_id_tulaj = rows[0].Table.Columns.Contains("Csoport_Id_Tulaj") ? rows[0]["Csoport_Id_Tulaj"].ToString() : null;
            if (csoport_id_tulaj != FelhasznaloProfil.FelhasznaloId(Page))
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "jsAlertOwnerMisMatch", "alertOwnerMismatch();", true);
            }
        }

        return result;
    }

    private Result FillSzuloTreeView_ForViewForm(string MappaId)
    {
        KRT_MappakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_MappakService();

        ExecParam execParam = new ExecParam();
        execParam = UI.SetExecParamDefault(Page, execParam);
        execParam.Record_Id = Request.QueryString.Get(QueryStringVars.Id);


        //Result result = service.GetAllWithExtensionAndJogosultak(execParam, new KRT_MappakSearch());
        Result result = service.GetWithTreeAndRightCheck(execParam);


        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return result;
        }

        this.FillSzuloTreeView_FromResult(result, MappaId, CommandName.View);

        return result;
    }

    private Result FillSzuloTreeView_ForModifyForm(string MappaId)
    {
        KRT_MappakService service = Contentum.eUtility.eRecordService.ServiceFactory.GetKRT_MappakService();

        ExecParam execParam = new ExecParam();
        execParam = UI.SetExecParamDefault(Page, execParam);

        Result result = service.GetAllWithExtensionAndJogosultak(execParam, new KRT_MappakSearch());

        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return result;
        }

        this.FillSzuloTreeView_FromResult(result, MappaId, CommandName.Modify);

        return result;
    }

    private void FillSzuloTreeView_FromResult(Result result, string selectedId, string command)
    {
        // státusz Dictionary-k törlése:
        MappakDictionary.Clear();
        // load selected item to Dictionary
        System.Data.DataRow[] selectedRows = result.Ds.Tables[0].Select(String.Format("Id = '{0}'", selectedId));
        foreach (System.Data.DataRow row in selectedRows)
        {
            string id = row["Id"].ToString();
            MappakDictionary.Add(id, new Statusz(row));
        }

        SzuloDosszie_TreeView.Nodes.Clear();

        Stack<TreeNode> nodeList = new Stack<TreeNode>(result.Ds.Tables[0].Rows.Count);

        // 1. top level nodes
        System.Data.DataRow[] topLevelRows = result.Ds.Tables[0].Select("IsNull(SzuloMappa_Id, '') = ''", "LetrehozasIdo");

        foreach (System.Data.DataRow topLevelRow in topLevelRows)
        {
            TreeNode tn = GetDefaultTreeNode(topLevelRow, selectedId, command);

            SzuloDosszie_TreeView.Nodes.Add(tn);

            tn.Selected = tn.Value == selectedId;

            nodeList.Push(tn);
            
            string id = tn.Value;
            if (MappakDictionary.ContainsKey(id) == false)
            {
                MappakDictionary.Add(id, new Statusz(topLevelRow));
            }

        }

        // child nodes (deep search)
        while (nodeList.Count != 0)
        {
            TreeNode tn = nodeList.Pop();

            System.Data.DataRow[] childRows = result.Ds.Tables[0].Select(String.Format("SzuloMappa_Id='{0}'", tn.Value), "LetrehozasIdo");

            foreach (System.Data.DataRow childRow in childRows)
            {
                TreeNode cn = GetDefaultTreeNode(childRow, selectedId, command);
                tn.ChildNodes.Add(cn);
                cn.Selected = cn.Value == selectedId;

                nodeList.Push(cn);

                // load to Dictionary
                string id = cn.Value;
                if (MappakDictionary.ContainsKey(id) == false)
                {
                    MappakDictionary.Add(id, new Statusz(childRow));
                }
            }
        }

        SzuloDosszie_TreeView.ExpandAll();
    }


    private TreeNode GetDefaultTreeNode(System.Data.DataRow rowMappakGetAll, string selectedId, string command)
    {
        if (rowMappakGetAll != null)
        {
            string nev = rowMappakGetAll.Table.Columns.Contains("Nev") ? rowMappakGetAll["Nev"].ToString() : "???";
            string id = rowMappakGetAll.Table.Columns.Contains("Id") ? rowMappakGetAll["Id"].ToString() : "???";
            string tipus = rowMappakGetAll.Table.Columns.Contains("Tipus") ? rowMappakGetAll["Tipus"].ToString() : "???";
            string csoport_id_tulaj = rowMappakGetAll.Table.Columns.Contains("Csoport_Id_Tulaj") ? rowMappakGetAll["Csoport_Id_Tulaj"].ToString() : null;

            TreeNode tn = new TreeNode();//new TreeNode(Text, Id);
            tn.Value = id;

            // WorkAround: az OnClientClick javascriptek miatt, amiket az image-re is ráteszünk,
            // nem az ImegaUrl-t állítjuk, hanem a textbe tesszük a képet
            //switch (Tipus)
            //{
            //    case KodTarak.MAPPA_TIPUS.Virtualis:
            //    default:
            //        tn.ImageUrl = imageUrlVirtualis; // "~/images/hu/egyeb/virtualis_dosszie.gif";
            //        break;
            //    case KodTarak.MAPPA_TIPUS.Fizikai:
            //        tn.ImageUrl = imageUrlFizikai; // ".~/images/hu/egyeb/fizikai_dosszie.gif";
            //        break;
            //}

            string formatColorizedNode = "<span onclick=\"{0}\"><img src=\"{1}\" style=\"vertical-align:text-bottom;\" /><font style=\"background-color: #A3FA6B\">{2}</font></span>";
            string formatNormalNode = "<span onclick=\"{0}\"><img src=\"{1}\" style=\"vertical-align:text-bottom;\" />{2}</span>";


            string jsOnClick = String.Empty; // default
            string imgSrc = null;
            string formatString = formatNormalNode; // default

            switch (tipus)
            {
                case KodTarak.MAPPA_TIPUS.Virtualis:
                default:
                    imgSrc = imageUrlVirtualis; // "images/hu/egyeb/virtualis_dosszie.gif";
                    tn.Checked = false;
                    if (command == CommandName.Modify && id == selectedId)
                    {
                        formatString = formatColorizedNode;
                        jsOnClick = returnFalse;
                    }
                    else if (command == CommandName.New || command == CommandName.Modify)
                    {
                        jsOnClick = "return callConfirm();";
                    }
                    break;
                case KodTarak.MAPPA_TIPUS.Fizikai:
                    imgSrc = imageUrlFizikai; // "images/hu/egyeb/fizikai_dosszie.gif";
                    tn.Checked = true;
                    if (command == CommandName.New)
                    {
                        if (csoport_id_tulaj != FelhasznaloProfil.FelhasznaloId(Page))
                        {
                            jsOnClick = "return alertOwnerMismatch();";
                        }
                    }
                    else if (command == CommandName.Modify)
                    {
                        if (id == selectedId)
                        {
                            jsOnClick = returnFalse;
                            formatString = formatColorizedNode;
                        }
                        else if (MappakDictionary.ContainsKey(selectedId)
                            && MappakDictionary[selectedId].Tipus == KodTarak.MAPPA_TIPUS.Fizikai
                            && MappakDictionary[selectedId].Csoport_Id_Tulaj != csoport_id_tulaj)
                        {
                            jsOnClick = "return alertOwnerMismatch();";
                        }
                        else
                        {
                            jsOnClick = "return checkTipus();";
                        }
                    }
                    break;
            }

            tn.Text = String.Format(formatString, jsOnClick, imgSrc, nev);
            return tn;
        }

        return null;
    }

    private string GetNameFromTreeNode(TreeNode tn)
    {
        if (tn != null)
        {
            if (MappakDictionary.ContainsKey(tn.Value))
            {
                return MappakDictionary[tn.Value].Nev;
            }
            else
            {
                string text = tn.Text;

                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                try
                {
                    xmlDoc.LoadXml(text);

                    System.Xml.XmlNode ndDivFont = xmlDoc.SelectSingleNode("span/font");
                    if (ndDivFont != null)
                    {

                        text = ndDivFont.InnerText;
                    }
                    else
                    {
                        System.Xml.XmlNode ndDiv = xmlDoc.SelectSingleNode("span");
                        if (ndDiv != null)
                        {

                            text = ndDiv.InnerText;
                        }
                    }
                }
                catch (Exception)
                {
                    // NOTHING TO DO - just not the right xml format
                }

                return text;
            }
        }

        return String.Empty;
    }

    private void SetNewParentNode(string childId, string newParentId)
    {
        Stack<TreeNode> al = new Stack<TreeNode>();

        TreeNode childNode = null;
        TreeNode newParentNode = null;

        foreach (TreeNode rootNode in SzuloDosszie_TreeView.Nodes)
        {
            al.Push(rootNode);
        }

        while (al.Count != 0)
        {
            TreeNode tn = al.Pop();

            if (tn.Value == childId)
            {
                if (tn.Parent == null && childId == newParentId)
                {
                    tn.Selected = false;
                    return;
                }

                if (childId == newParentId)
                {
                    tn.Parent.Selected = true;
                    return;
                }

                childNode = tn;
            }

            if (tn.Value == newParentId)
            {
                newParentNode = tn;
            }

            if (childNode != null && newParentNode != null)
                break;

            foreach (TreeNode cn in tn.ChildNodes)
                al.Push(cn);
        }

        if (childNode.Parent == null)
        {
            SzuloDosszie_TreeView.Nodes.Remove(childNode);
        }
        else
        {
            childNode.Parent.ChildNodes.Remove(childNode);
        }

        if (newParentNode == null)
        {
            SzuloDosszie_TreeView.Nodes.Add(childNode);
            childNode.Select();
            childNode.Selected = false;
        }
        else
        {
            newParentNode.ChildNodes.Add(childNode);
        }
    }

    protected void SzuloDosszie_TreeView_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            this.SetNewParentNode(Request.QueryString.Get(QueryStringVars.Id), SzuloDosszie_TreeView.SelectedValue);
        }

        if (Command == CommandName.New)
        {
            if (SzuloDosszie_TreeView.SelectedNode != null)
            {
                if (IsFizikaiDosszie(SzuloDosszie_TreeView.SelectedNode))
                {
                    FillDropDownByTipus(KodTarak.MAPPA_TIPUS.Fizikai);
                }
                else
                {
                    FillDropDownByTipus(null);
                }


            }
        }

        SetSzuloDosszie_TextBox_Text();
    }

    protected void SetRootButton_Clicked(object sender, EventArgs e)
    {
        if (Command == CommandName.Modify)
        {
            FillDropDownByTipus(null);
            this.SetNewParentNode(Request.QueryString.Get(QueryStringVars.Id), string.Empty);
        }

        if (Command == CommandName.New)
        {
            FillDropDownByTipus(null);
            if (SzuloDosszie_TreeView.SelectedNode != null)
            {
                SzuloDosszie_TreeView.SelectedNode.Selected = false;
            }
        }

        SetSzuloDosszie_TextBox_Text();
    }

    private void SetSzuloDosszie_TextBox_Text()
    {
        if (Command == CommandName.Modify || Command == CommandName.New)
        {
            if (SzuloDosszie_TreeView.SelectedNode == null)
            {
                SzuloDosszieNeve_TextBox.Text = "";
                SzuloDosszie_IdHiddenField.Value = "";
                this.SetRootButton.Enabled = false;
            }
            else
            {
                SzuloDosszieNeve_TextBox.Text = GetNameFromTreeNode(SzuloDosszie_TreeView.SelectedNode);
                SzuloDosszie_IdHiddenField.Value = SzuloDosszie_TreeView.SelectedNode.Value;
                this.SetRootButton.Enabled = true;
            }
        }
        else
        {
            if (SzuloDosszie_TreeView.SelectedNode == null || SzuloDosszie_TreeView.SelectedNode.Parent == null)
            {
                SzuloDosszieNeve_TextBox.Text = "";
                this.SetRootButton.Enabled = false;
            }
            else
            {
                SzuloDosszieNeve_TextBox.Text = GetNameFromTreeNode(SzuloDosszie_TreeView.SelectedNode.Parent);
                SzuloDosszie_IdHiddenField.Value = SzuloDosszie_TreeView.SelectedNode.Parent.Value;
            }
        }
            
    }

    #endregion TreeView methodok


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            //if (FunctionRights.GetFunkcioJog(Page, "Csoport" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
                            KRT_Mappak krt_mappak = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            bool isPublic = HasznalatiMod_CheckBox.Checked;

                            Result result = service.InsertAndAttachBarcode(execParam, krt_mappak, isPublic);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
                                KRT_Mappak krt_mappak = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;


                                Result result = service.UpdateWithCheck(execParam, krt_mappak);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            //else
            {
                //UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    private void registerJavaScripts()
    {
        string js = String.Format(@"var ddlTipus = $get('{0}');var txtVonalkod = $get('{1}');
if (ddlTipus && txtVonalkod && ddlTipus.value == '{2}') {{
    txtVonalkod.disabled = false;
}}
else
{{
    txtVonalkod.value = '';
    txtVonalkod.disabled = true;
}}"
            , Tipus_DropDown.DropDownList.ClientID, Vonalkod_TextBox.TextBox.ClientID, KodTarak.MAPPA_TIPUS.Fizikai);

        Tipus_DropDown.DropDownList.Attributes.Add("onchange", js);

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Tipus_DropDownInit", js, true);

       string jsFunctions = String.Format(@"function callConfirm() {{ if (!confirm('{0}')) return false; }}
function checkTipus() {{if ($get('{1}').value == '{2}') {{ alert('{3}'); return false; }} else {{ return callConfirm();}} }}
function alertOwnerMismatch() {{ alert('{4}'); return false; }}"
    , Resources.Question.UI_Dossziek_AthelyezesQuestion // "A rendben gomb megnyomása után a dosszié helye megváltozik! Biztos, hogy folytatja?"
    , Tipus_DropDown.DropDownList.ClientID, KodTarak.MAPPA_TIPUS.Virtualis
    , Resources.Error.ErrorCode_64040 // Fizikai dossziéban nem hozható létre virtuális dosszié!"
    , Resources.Error.ErrorCode_64038); // A dosszié nem helyezhető a szülőnek kiválaszott fizikai dossziéba, mert a dossziék tulajdonosai különböznek!


        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "jsFunctions", jsFunctions, true);

        this.SetRootButton.OnClientClick = "return callConfirm();";
    }

}
