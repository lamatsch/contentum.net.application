﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;

public partial class DosszieKapcsolatokForm : System.Web.UI.Page
{
    public string maxTetelszam = "0";

    private UI ui = new UI();

    private string Command = "";

    private PageView pageView = null;
    //private ComponentSelectControl compSelector = null;

    private KRT_Mappak _krt_mappa;

    private KRT_Mappak krt_mappa
    {
        get
        {
            if (this._krt_mappa == null)
            {
                KRT_MappakService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetKRT_MappakService();

                ExecParam execParam = UI.SetExecParamDefault(Page);
                execParam.Record_Id = Request.QueryString[QueryStringVars.Id];

                Result result = service.Get(execParam);

                if (!result.IsError)
                    this._krt_mappa = (KRT_Mappak)result.Record;
                else
                {
                    this._krt_mappa = new KRT_Mappak();
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }

            return this._krt_mappa;
        }
    }


    private String[] KuldemenyekArray;
    private String[] UgyiratokArray;
    private String[] IratPeldanyokArray;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        if (Command == CommandName.DossziebaHelyez)
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MappaTartalmakNew");
        else
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MappaTartalmakInvalidate");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        if (Session["SelectedUgyiratIds"] != null)
        {
            string UgyiratId = Session["SelectedUgyiratIds"].ToString();

            UgyiratokArray = UgyiratId.Split(',');
        }

        if (Session["SelectedKuldemenyIds"] != null)
        {
            string KuldemenyId = Session["SelectedKuldemenyIds"].ToString();

            KuldemenyekArray = KuldemenyId.Split(',');
        }

        if (Session["SelectedIratPeldanyIds"] != null)
        {
            string IratPeldanyId = Session["SelectedIratPeldanyIds"].ToString();

            IratPeldanyokArray = IratPeldanyId.Split(',');
        }


        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        if (Command == CommandName.DossziebolKivesz)
        {
            FormHeader1.HeaderTitle = Resources.Form.DossziebolKiveszFormHeaderTitle;
        }
        else
        {
            FormHeader1.HeaderTitle = Resources.Form.DossziebaHelyezFormHeaderTitle;
        }

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        //ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        pageView.SetViewOnPage(Command);

        #region Ellenőrzés: fizikai mappa mozgásban
        KRT_MappakService service = eRecordService.ServiceFactory.GetKRT_MappakService();
        ExecParam execParam_statusz = UI.SetExecParamDefault(Page);

        execParam_statusz.Record_Id = Request.QueryString["Id"];
        Result result_statusz = service.GetStatus(execParam_statusz, Request.QueryString["Id"]);

        if (result_statusz.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_statusz);
            MainPanel.Visible = false;
            FormFooter1.Command = CommandName.View;
        }
        else if (result_statusz.Ds.Tables[0].Rows[0]["Tipus"].ToString() == KodTarak.MAPPA_TIPUS.Fizikai
            && result_statusz.Ds.Tables[0].Rows[0]["Allapot"].ToString() == "1")
        {
            //// A fizikai dosszié nem módosítható, mert mozgásban van!
            //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_64035);
            // A dosszié tartalma nem módosítható, mert mozgásban van!
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_64030);
            MainPanel.Visible = false;
            FormFooter1.Command = CommandName.View;
            return;
        }
        #endregion Ellenőrzés: fizikai mappa mozgásban

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; }";

        int cntKijeloltUgyiratok = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count;
        int cntKijeloltIratpeldanyok = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count;
        int cntKijeloltKuldemenyek = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null).Count;

        int cntKijelolt = cntKijeloltUgyiratok + cntKijeloltIratpeldanyok + cntKijeloltKuldemenyek;

        labelTetelekSzamaDb.Text = cntKijelolt.ToString();


    }

    private KRT_MappaTartalmak GetKRT_MappaTartalmakFromComponents()
    {
        KRT_MappaTartalmak krt_mappaTartalmak = new KRT_MappaTartalmak();

        krt_mappaTartalmak.Updated.SetValueAll(false);
        krt_mappaTartalmak.Base.Updated.SetValueAll(false);
        krt_mappaTartalmak.Base.Updated.Ver = true;

        krt_mappaTartalmak.Mappa_Id = Request.QueryString[QueryStringVars.Id];
        krt_mappaTartalmak.Updated.Mappa_Id = true;

        krt_mappaTartalmak.Leiras = MegjegyzesTextBox.Text;
        krt_mappaTartalmak.Updated.Leiras = pageView.GetUpdatedByView(MegjegyzesTextBox);

        return krt_mappaTartalmak;
    }

    private void LoadFormComponents()
    {
        if (this.krt_mappa == null)
            return;

        KRT_Mappak krt_mappak = this.krt_mappa;

        DosszieHelyeTextBox.Id_HiddenField = krt_mappak.Csoport_Id_Tulaj;
        DosszieHelyeTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        DosszieNeveTextBox.Text = krt_mappak.Nev;
        MegjegyzesTextBox.Text = krt_mappak.Leiras;

        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UgyiratokListPanel.Visible = true;
            Label_Ugyiratok.Visible = true;

            FillUgyiratokGridView();
        }

        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            IratPeldanyokListPanel.Visible = true;
            Label_IratPeldanyok.Visible = true;

            FillIratPeldanyGridView();
        }

        if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
        {
            KuldemenyekListPanel.Visible = true;
            Label_Kuldemenyek.Visible = true;

            FillKuldemenyGridView();
        }
    }


    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }
    }


    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        //// ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }
    }


    private void FillKuldemenyGridView()
    {
        DataSet ds = KuldKuldemenyekGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != KuldemenyekArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }
    }



    protected DataSet KuldKuldemenyekGridViewBind()
    {
        if (KuldemenyekArray != null && KuldemenyekArray.Length > 0)
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_KuldKuldemenyekSearch search = new EREC_KuldKuldemenyekSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            //for (int i = 0; i < KuldemenyekArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + KuldemenyekArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + KuldemenyekArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(KuldemenyekArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(KuldKuldemenyekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void KuldKuldemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");

        CheckBox cb = (CheckBox)e.Row.FindControl("check");

        if (cb != null)
            cb.Checked = true;

        if (e.Row.RowType == DataControlRowType.DataRow && this.krt_mappa.Tipus == KodTarak.MAPPA_TIPUS.Fizikai)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            if (drv["FelhasznaloCsoport_Id_Orzo"].ToString() != krt_mappa.Csoport_Id_Tulaj
                || drv["Csoport_Id_Felelos"].ToString() != krt_mappa.Csoport_Id_Tulaj)
            {
                e.Row.BackColor = System.Drawing.Color.Coral;
                if (cb != null)
                {
                    cb.Checked = false;
                    cb.Enabled = false;
                }

                // Info Image állítása, hogy miért nem lehet átadni
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null)
                {
                    infoImageButton.Visible = true;
                    string infoMessage = Resources.Error.ErrorCode_64013;
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                        + Constants.ErrorDetails.ObjectTypes.EREC_KuldKuldemenyek + "','" + drv["Id"].ToString() + "','"
                        + "EREC_KuldKuldemenyek" + "','" + drv["Id"].ToString() + "'); return false;";
                }

            }
        }
    }



    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            //for (int i = 0; i < UgyiratokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + UgyiratokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + UgyiratokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(UgyiratokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);


        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");

        CheckBox cb = (CheckBox)e.Row.FindControl("check");

        if (cb != null)
            cb.Checked = true;

        if (e.Row.RowType == DataControlRowType.DataRow && this.krt_mappa.Tipus == KodTarak.MAPPA_TIPUS.Fizikai)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            if (drv["FelhasznaloCsoport_Id_Orzo"].ToString() != krt_mappa.Csoport_Id_Tulaj
                || drv["Csoport_Id_Felelos"].ToString() != krt_mappa.Csoport_Id_Tulaj)
            {
                e.Row.BackColor = System.Drawing.Color.Coral;
                if (cb != null)
                {
                    cb.Checked = false;
                    cb.Enabled = false;
                }

                // Info Image állítása, hogy miért nem lehet átadni
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null)
                {
                    string infoMessage = null;

                    if (drv["FelhasznaloCsoport_Id_Orzo"].ToString() != drv["Csoport_Id_Felelos"].ToString())
                    {
                        // Az ügyirat mozgásban van, nem lehet dossziéba helyezni!
                        infoMessage = Resources.Error.ErrorCode_64009;
                    }
                    else
                    {
                        // 1. a dosszié a felhasználónál van, a belehelyezendő tétel nem:
                        if (krt_mappa.Csoport_Id_Tulaj == FelhasznaloProfil.FelhasznaloId(Page))
                        {
                            // az ügyirat nem Önnél van!
                            infoMessage = Resources.Error.ErrorCode_64003;
                        }
                        else
                        {
                            // A dosszié nincs Önnél, csak a dosszié tulajdonosánál lévő tétel helyezhető bele!
                            infoMessage = Resources.Error.ErrorCode_64034;
                        }
                    }


                    infoImageButton.Visible = true;
                    //string infoMessage = Resources.Error.ErrorCode_64003;
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                        + Constants.ErrorDetails.ObjectTypes.EREC_UgyUgyiratok + "','" + drv["Id"].ToString() + "','"
                        + "EREC_UgyUgyiratok" + "','" + drv["Id"].ToString() + "'); return false;";
                }

            }
        }
    }






    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            //for (int i = 0; i < IratPeldanyokArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + IratPeldanyokArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + IratPeldanyokArray[i] + "'";
            //    }
            //}
            search.Id.Value = Search.GetSqlInnerString(IratPeldanyokArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        CheckBox cbUgyintezesModja = (CheckBox)e.Row.FindControl("cbUgyintezesModja");

        if (cb != null)
            cb.Checked = true;

        if (e.Row.RowType == DataControlRowType.DataRow && this.krt_mappa.Tipus == KodTarak.MAPPA_TIPUS.Fizikai)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            if (drv["FelhasznaloCsoport_Id_Orzo"].ToString() != krt_mappa.Csoport_Id_Tulaj
                || drv["Csoport_Id_Felelos"].ToString() != krt_mappa.Csoport_Id_Tulaj)
            {
                e.Row.BackColor = System.Drawing.Color.Coral;
                if (cb != null)
                {
                    cb.Checked = false;
                    cb.Enabled = false;
                }

                // Info Image állítása, hogy miért nem lehet átadni
                ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
                if (infoImageButton != null)
                {
                    infoImageButton.Visible = true;
                    string infoMessage = Resources.Error.ErrorCode_64023;
                    infoImageButton.ToolTip = infoMessage;
                    infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                        + Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok + "','" + drv["Id"].ToString() + "','"
                        + "EREC_PldIratPeldanyok" + "','" + drv["Id"].ToString() + "'); return false;";
                }

            }
        }
    }



    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            int selectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count +
                ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count +
                ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null).Count;

            if (selectedIds > int.Parse(maxTetelszam))
            {
                // ha a JavaScript végén "return false;" van, nem jelenik meg az üzenet...
                string javaS = "alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "');";// return false;";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                return;
            }
            
            KRT_MappaTartalmakService service = eRecordService.ServiceFactory.GetKRT_MappaTartalmakService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<string> selectedItemsList_ugyirat = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);
            //String[] UgyiratIds = new String[selectedItemsList_ugyirat.Count];
            //for (int i = 0; i < UgyiratIds.Length; i++)
            //{
            //    UgyiratIds[i] = selectedItemsList_ugyirat[i];
            //}

            List<string> selectedItemsList_pld = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);
            //String[] IratPeldanyIds = new String[selectedItemsList_pld.Count];
            //for (int i = 0; i < IratPeldanyIds.Length; i++)
            //{
            //    IratPeldanyIds[i] = selectedItemsList_pld[i];
            //}

            List<string> selectedItemsList_kuld = ui.GetGridViewSelectedRows(KuldKuldemenyekGridView, FormHeader1.ErrorPanel, null);
            //String[] KuldemenyIds = new String[selectedItemsList_kuld.Count];
            //for (int i = 0; i < KuldemenyIds.Length; i++)
            //{
            //    KuldemenyIds[i] = selectedItemsList_kuld[i];
            //}

            if (selectedItemsList_ugyirat.Count + selectedItemsList_pld.Count + selectedItemsList_kuld.Count == 0)
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                return;
            }

            string Ugyirat_Ids = Search.GetSqlInnerString(selectedItemsList_ugyirat.ToArray());//Contentum.eUtility.Utils.GetStringFromArray(UgyiratIds, "','");
            //Ugyirat_Ids = "'" + Ugyirat_Ids + "'";

            string Kuldemeny_Ids = Search.GetSqlInnerString(selectedItemsList_kuld.ToArray());//Contentum.eUtility.Utils.GetStringFromArray(KuldemenyIds, "','");
            //Kuldemeny_Ids = "'" + Kuldemeny_Ids + "'";

            string Iratpeldany_Ids = Search.GetSqlInnerString(selectedItemsList_pld.ToArray());//Contentum.eUtility.Utils.GetStringFromArray(IratPeldanyIds, "','");
            //Iratpeldany_Ids = "'" + Iratpeldany_Ids + "'";


            if (Command == CommandName.DossziebaHelyez)
            {
                Result result = service.InsertTomeges(execParam, GetKRT_MappaTartalmakFromComponents(), Ugyirat_Ids, Iratpeldany_Ids, Kuldemeny_Ids);

                if (!result.IsError)
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                    UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result);
                    UI.MarkFailedRecordsInGridView(KuldKuldemenyekGridView, result);
                }
            }

            else if (Command == CommandName.DossziebolKivesz)
            {
                Result result = service.InvalidateTomeges(execParam, Request.QueryString[QueryStringVars.Id], Ugyirat_Ids, Iratpeldany_Ids, Kuldemeny_Ids);

                if (!result.IsError)
                {
                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                    UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result);
                    UI.MarkFailedRecordsInGridView(KuldKuldemenyekGridView, result);
                }
            }
        }
    }

}
