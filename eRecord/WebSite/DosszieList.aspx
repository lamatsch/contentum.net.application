﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DosszieList.aspx.cs" Inherits="DosszieList" Title="Dossziék listája" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="eRecordComponent/UgyiratokList.ascx" TagName="UgyiratokList" TagPrefix="ul" %>
<%@ Register Src="eRecordComponent/KuldemenyekList.ascx" TagName="KuldemenyekList"
    TagPrefix="kl" %>
<%@ Register Src="eRecordComponent/IratpeldanyokList.ascx" TagName="IratpeldanyokList"
    TagPrefix="il" %>
<%@ Register Src="eRecordComponent/DosszieTreeView.ascx" TagName="DosszieTreeView"
    TagPrefix="dtv" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table>
        <tr>
            <td width="100%">
                <lh:ListHeader ID="ListHeader" runat="server" />
                <lh:ListHeader ID="ListHeader1" runat="server" Visible="false" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td rowspan="2" valign="top" align="left" height="100%">
                <div id="PanelDossziekDiv" style="position: relative;">
                    <dtv:DosszieTreeView ID="DosszieTreeView" runat="server" />
                </div>
            </td>
            <td width="100%" valign="top" align="left">
                <ajaxToolkit:TabContainer ID="ListTabContainer" runat="server" Width="100%" OnActiveTabChanged="ListTabContainer_ActiveTabChanged"
                    ActiveTabIndex="0" AutoPostBack="true">
                    <ajaxToolkit:TabPanel ID="TabUgyiratPanel" runat="server" TabIndex="0">
                        <HeaderTemplate>
                            <asp:Label ID="Label1" Text="Ügyiratok" runat="server" />
                        </HeaderTemplate>
                        <ContentTemplate>
                            <ul:UgyiratokList ID="UgyiratokList" runat="server" />
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabKuldemenyekPanel" runat="server" TabIndex="1">
                        <HeaderTemplate>
                            <asp:Label ID="Label3" Text="Küldemények" runat="server" />
                        </HeaderTemplate>
                        <ContentTemplate>
                            <kl:KuldemenyekList ID="KuldemenyekList" runat="server" />
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabIratpeldanyokPanel" runat="server" TabIndex="2">
                        <HeaderTemplate>
                            <asp:Label ID="Label4" Text="Iratpéldányok" runat="server" />
                        </HeaderTemplate>
                        <ContentTemplate>
                            <il:IratpeldanyokList ID="IratpeldanyokList" runat="server" />
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <asp:UpdatePanel ID="DosszieListGridViewUpdatePanel" runat="server">
                    <ContentTemplate>
                        <asp:HiddenField ID="hfLastSelectedMasterRecordId" runat="server" />
                        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%" ActiveTabIndex="0"
                            AutoPostBack="true">
                            <ajaxToolkit:TabPanel ID="DossziekTabPanel" runat="server" TabIndex="0">
                                <HeaderTemplate>
                                    <asp:UpdatePanel ID="LabelUpdatePanelCsoportTagok" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="Header1" runat="server" Text="Egyéb dossziék" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="DosszieListPanel" runat="server">
                                        <uc1:SubListHeader ID="DosszieListSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                    <ajaxToolkit:CollapsiblePanelExtender ID="DosszieListCPE" runat="server" TargetControlID="Panel2"
                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                        AutoExpand="false" ExpandedSize="100">
                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                    <asp:Panel ID="Panel2" runat="server">
                                                        <asp:GridView ID="DosszieListGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                            AllowSorting="True" AutoGenerateColumns="false" OnRowCommand="DosszieListGridView_RowCommand"
                                                            OnPreRender="DosszieListGridView_PreRender" OnSorting="DosszieListGridView_Sorting"
                                                            DataKeyNames="Id,Mappa_Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="Mappa_Nev" HeaderText="Név" SortExpression="Mappa_Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="300px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Mappa_Barcode" HeaderText="Vonalkód" SortExpression="Mappa_Barcode">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="300px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Mappa_Tulajdonos_Nev" HeaderText="Tulajdonos" SortExpression="Mappa_Tulajdonos_Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="400px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="400px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Mappa_Tipus" HeaderText="Típus" SortExpression="Mappa_Tipus">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="100px" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
