﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eUtility;

public partial class DosszieList : System.Web.UI.Page
{
    private enum StartupMode
    {
        Ugyirat,
        Kuldemeny,
        Iratpeldany
    }

    private StartupMode startupMode;

    UI ui = new UI();


    protected void Page_Init(object sender, EventArgs e)
    {
        this.GetStartupMode();

        ScriptManager1.RegisterAsyncPostBackControl(ListTabContainer);

        this.TabIratpeldanyokPanel.Visible = this.TabUgyiratPanel.Visible = this.TabKuldemenyekPanel.Visible = false;

        if (FunctionRights.GetFunkcioJog(Page,"UgyiratokList"))
        {
            TabUgyiratPanel.Visible = true;
            UgyiratokList.ListHeader = ListHeader1;
            UgyiratokList.ScriptManager = ScriptManager1;
            UgyiratokList.DossziePanelVisible = false;
            UgyiratokList.AttachedDosszieTreeView = this.DosszieTreeView;
        }

        if (FunctionRights.GetFunkcioJog(Page, "KuldemenyList"))
        {
            TabKuldemenyekPanel.Visible = true;
            KuldemenyekList.ListHeader = ListHeader1;
            KuldemenyekList.ScriptManager = ScriptManager1;
            KuldemenyekList.DossziePanelVisible = false;
            KuldemenyekList.AttachedDosszieTreeView = this.DosszieTreeView;
        }

        if (FunctionRights.GetFunkcioJog(Page, "IratPeldanyokList"))
        {
            TabIratpeldanyokPanel.Visible = true;
            IratpeldanyokList.ListHeader = ListHeader1;
            IratpeldanyokList.ScriptManager = ScriptManager1;
            IratpeldanyokList.DossziePanelVisible = false;
            IratpeldanyokList.AttachedDosszieTreeView = this.DosszieTreeView;
        }

        DosszieListSubListHeader.RowCount_Changed += new EventHandler(DosszieListSubListHeader_RowCount_Changed);

        this.InitPage();

        UgyiratokList.DosszieId_HiddenField_ClientId = this.DosszieTreeView.SelectedId_HiddenField;
        KuldemenyekList.DosszieId_HiddenField_ClientId = this.DosszieTreeView.SelectedId_HiddenField;
        IratpeldanyokList.DosszieId_HiddenField_ClientId = this.DosszieTreeView.SelectedId_HiddenField;
    }

    private void InitPage()
    {
        switch (this.startupMode)
        {
            case StartupMode.Ugyirat:
            default:
                KuldemenyekList.IsActive = false;
                IratpeldanyokList.IsActive = false;
                UgyiratokList.ListHeader = ListHeader;
                if (FunctionRights.GetFunkcioJog(Page, "UgyiratokList"))
                {
                    UgyiratokList.InitPage();
                }
                break;
            case StartupMode.Kuldemeny:
                UgyiratokList.IsActive = false;
                IratpeldanyokList.IsActive = false;
                KuldemenyekList.ListHeader = ListHeader;
                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyList"))
                {
                    KuldemenyekList.InitPage();
                }
                break;
            case StartupMode.Iratpeldany:
                UgyiratokList.IsActive = false;
                KuldemenyekList.IsActive = false;
                IratpeldanyokList.ListHeader = ListHeader;
                if (FunctionRights.GetFunkcioJog(Page, "IratPeldanyokList"))
                {
                    IratpeldanyokList.InitPage();
                }
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.LoadPage();

        #region DosszieListGridView

        DosszieListSubListHeader.AttachedGridView = DosszieListGridView;
        DosszieListSubListHeader.NewVisible = false;
        DosszieListSubListHeader.ViewVisible = false;
        DosszieListSubListHeader.ModifyVisible = false;
        DosszieListSubListHeader.InvalidateVisible = false;

        DosszieListSubListHeader.ValidFilterVisible = false;       

        #endregion DosszieListGridView

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        //ListHeader.SelectedRecordChanged += new EventHandler(ListHeader_SelectedRecordChanged);
    }

    protected void ListHeader_SelectedRecordChanged(object sender, EventArgs e)
    {
        DosszieListGridViewBind();
    }

    private void LoadPage()
    {
        switch (this.startupMode)
        {
            case StartupMode.Ugyirat:
            default:
                if (FunctionRights.GetFunkcioJog(Page, "UgyiratokList"))
                {
                    UgyiratokList.LoadPage();
                }
                break;
            case StartupMode.Kuldemeny:
                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyList"))
                {
                    KuldemenyekList.LoadPage();
                }
                break;
            case StartupMode.Iratpeldany:
                if (FunctionRights.GetFunkcioJog(Page, "IratPeldanyokList"))
                {
                    IratpeldanyokList.LoadPage();
                }
                break;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.PreRenderPage();

        this.Title = Resources.List.DossziekListHeaderTitle; //"Dossziék listája";

        string idSelected = String.Empty;
        switch (this.startupMode)
        {
            case StartupMode.Ugyirat:
                idSelected = UI.GetGridViewSelectedRecordId(UgyiratokList.UgyiratokGridView);
                break;
            case StartupMode.Kuldemeny:
                idSelected = UI.GetGridViewSelectedRecordId(KuldemenyekList.KuldemenyekGridView);
                break;
            case StartupMode.Iratpeldany:
                idSelected = UI.GetGridViewSelectedRecordId(IratpeldanyokList.IratPeldanyokGridView);
                break;
        }

        if (!IsPostBack || hfLastSelectedMasterRecordId.Value != idSelected)
        {
            DosszieListGridViewBind(idSelected);
            hfLastSelectedMasterRecordId.Value = idSelected;
        }
    }

    private void PreRenderPage()
    {
        switch (this.startupMode)
        {
            case StartupMode.Ugyirat:
            default:
                if (FunctionRights.GetFunkcioJog(Page, "UgyiratokList"))
                {
                    UgyiratokList.PreRenderPage();
                }
                break;
            case StartupMode.Kuldemeny:
                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyList"))
                {
                    KuldemenyekList.PreRenderPage();
                }
                break;
            case StartupMode.Iratpeldany:
                if (FunctionRights.GetFunkcioJog(Page, "IratPeldanyokList"))
                {
                    IratpeldanyokList.PreRenderPage();
                }
                break;
        }
    }

    protected void ListTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        switch (startupMode)
        {
            case StartupMode.Ugyirat:
            default:
                UgyiratokList.DosszieId = this.DosszieTreeView.SelectedId;
                UgyiratokList.UgyUgyiratokGridViewBind();
                break;
            case StartupMode.Kuldemeny:
                KuldemenyekList.DosszieId = this.DosszieTreeView.SelectedId;
                KuldemenyekList.KuldKuldemenyekGridViewBind();
                break;
            case StartupMode.Iratpeldany:
                IratpeldanyokList.DosszieId = this.DosszieTreeView.SelectedId;
                IratpeldanyokList.PldIratPeldanyokGridViewBind();
                break;
        }

        DosszieListGridViewBind();
    }

    #region DosszieListGridView

    protected void DosszieListGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = DosszieListGridView.PageIndex;

        DosszieListGridView.PageIndex = DosszieListSubListHeader.PageIndex;

        if (prev_PageIndex != DosszieListGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, DosszieListCPE);
            DosszieListGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(DosszieListSubListHeader.Scrollable, DosszieListCPE);
        }
        DosszieListSubListHeader.PageCount = DosszieListGridView.PageCount;
        DosszieListSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(DosszieListGridView);
         
    }

    protected void DosszieListGridViewBind()
    {
        switch (startupMode)
        {
            case StartupMode.Ugyirat:
            default:
                DosszieListGridViewBind(UI.GetGridViewSelectedRecordId(UgyiratokList.UgyiratokGridView));
                break;
            case StartupMode.Kuldemeny:
                DosszieListGridViewBind(UI.GetGridViewSelectedRecordId(KuldemenyekList.KuldemenyekGridView));
                break;
            case StartupMode.Iratpeldany:
                DosszieListGridViewBind(UI.GetGridViewSelectedRecordId(IratpeldanyokList.IratPeldanyokGridView));
                break;
        }
    }

    protected void DosszieListGridViewBind(string SelectedId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("DosszieListGridView", ViewState, "Mappa_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("DosszieListGridView", ViewState);

        DosszieListGridViewBind(SelectedId, sortExpression, sortDirection);
    }

    protected void DosszieListGridViewBind(string SelectedId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(SelectedId))
        {
            KRT_MappaTartalmakService service = eRecordService.ServiceFactory.GetKRT_MappaTartalmakService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            KRT_MappaTartalmakSearch search = new KRT_MappaTartalmakSearch();

            if (!string.IsNullOrEmpty(SelectedId))
            {
                search.Obj_Id.Filter(SelectedId);
            }
            else
            {
                search.Obj_Id.Operator = Query.Operators.isnull;
            }

            search.OrderBy = Search.GetOrderBy("DosszieListGridView", ViewState, SortExpression, SortDirection);

            Result res = service.GetAllWithExtensionAndJogosultak(ExecParam, search);

            UI.GridViewFill(DosszieListGridView, res, DosszieListSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header1);
        }
        else
        {
            ui.GridViewClear(DosszieListGridView);
            UI.ClearTabHeaderRowCountText(Header1);
        }
    }

    protected void DosszieListGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        DosszieListGridViewBind(UI.GetGridViewSelectedRecordId(UgyiratokList.UgyiratokGridView)
            , e.SortExpression, UI.GetSortToGridView("DosszieListGridView", ViewState, e.SortExpression));
    }

    protected void DosszieListGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName != "Select")
        {
            return;
        }
        int selectedRowNumber = int.Parse(e.CommandArgument.ToString());

        string selectedMappaId = (sender as GridView).DataKeys[selectedRowNumber].Values["Mappa_Id"].ToString();


        ArrayList nodeList = new ArrayList();

        foreach (TreeNode tn in this.DosszieTreeView.TreeView.Nodes)
            nodeList.Add(tn);

        while (nodeList.Count != 0)
        {
            TreeNode tn = (TreeNode)nodeList[0];
            nodeList.Remove(tn);

            if (tn.Value == selectedMappaId)
            {
                tn.Select();
                break;
            }

            foreach (TreeNode cn in tn.ChildNodes)
                nodeList.Add(cn);
        }

        this.DosszieTreeView.SelectedId = selectedMappaId;
    }

    void DosszieListSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        DosszieListGridViewBind();
    }

    #endregion DosszieListGridView

    private void GetStartupMode()
    {
        string activeTab = Request.Params[ListTabContainer.ClientID + "_ClientState"];
        if (!string.IsNullOrEmpty(activeTab) && activeTab.LastIndexOf("\"ActiveTabIndex\":") > 0)
        {
            switch (activeTab[(activeTab.LastIndexOf("\"ActiveTabIndex\":") + "\"ActiveTabIndex\":".Length)])
            {
                case '0':
                default:
                    this.startupMode = StartupMode.Ugyirat;
                    break;
                case '1':
                    this.startupMode = StartupMode.Kuldemeny;
                    break;
                case '2':
                    this.startupMode = StartupMode.Iratpeldany;
                    break;
            }
        }
        else
        {
            this.startupMode = StartupMode.Ugyirat;
        }
    }

}
