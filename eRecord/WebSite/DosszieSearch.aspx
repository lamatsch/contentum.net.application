﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="DosszieSearch.aspx.cs" Inherits="DosszieSearch" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/VonalkodTextBox.ascx" TagName="VonalkodTextBox" TagPrefix="uc" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,DossziekSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                     </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelNev" runat="server" Text="Dosszié neve:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="tbNev" runat="server" CssClass="mrUrlapInput"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelVonalkod" runat="server" Text="Vonalkód:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc:VonalkodTextBox ID="vtbVonalkod" runat="server" RequiredValidate="false" CssClass="mrUrlapInput"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelTipus" runat="server" Text="Típus:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc:KodtarakDropDownList ID="ktddTipus" runat="server" CssClass="mrUrlapInputComboBox"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelTulajdonos" runat="server" Text="Tulajdonos:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc:CsoportTextBox ID="cstbTulajdonos" runat="server" SearchMode="true" CssClass="mrUrlapInput"/>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelLeiras" runat="server" Text="Leírás:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                         <asp:TextBox ID="tbLeiras" runat="server" Rows="4" TextMode="MultiLine" CssClass="mrUrlapInput" style="margin-top:6px;"/>
                                    </td>
                                </tr>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" />
                                     </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

