﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class DosszieSearch : System.Web.UI.Page
{
    private Type _type = typeof(KRT_MappakSearch);

    private const string kcsKod_MappaTipus = "MAPPA_TIPUS";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            KRT_MappakSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_MappakSearch)Search.GetSearchObject(Page, new KRT_MappakSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_MappakSearch krt_MappakSearch = null;
        if (searchObject != null) krt_MappakSearch = (KRT_MappakSearch)searchObject;

        if (krt_MappakSearch != null)
        {
            tbNev.Text = krt_MappakSearch.Nev.Value;
            vtbVonalkod.Text = krt_MappakSearch.BarCode.Value;
            ktddTipus.FillAndSetSelectedValue(kcsKod_MappaTipus, krt_MappakSearch.Tipus.Value,
                true, SearchHeader1.ErrorPanel);
            cstbTulajdonos.Id_HiddenField = krt_MappakSearch.Csoport_Id_Tulaj.Value;
            cstbTulajdonos.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            tbLeiras.Text = krt_MappakSearch.Leiras.Value;
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_MappakSearch SetSearchObjectFromComponents()
    {
        KRT_MappakSearch krt_MappakSearch = (KRT_MappakSearch)SearchHeader1.TemplateObject;

        if (krt_MappakSearch == null)
        {
            krt_MappakSearch = new KRT_MappakSearch();
        }

        if (!String.IsNullOrEmpty(tbNev.Text))
        {
            krt_MappakSearch.Nev.Value = tbNev.Text;
            krt_MappakSearch.Nev.Operator = Search.GetOperatorByLikeCharater(tbNev.Text);
        }

        if (!String.IsNullOrEmpty(vtbVonalkod.Text))
        {
            krt_MappakSearch.BarCode.Value = vtbVonalkod.Text;
            krt_MappakSearch.BarCode.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(ktddTipus.SelectedValue))
        {
            krt_MappakSearch.Tipus.Value = ktddTipus.SelectedValue;
            krt_MappakSearch.Tipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(cstbTulajdonos.Id_HiddenField))
        {
            krt_MappakSearch.Csoport_Id_Tulaj.Value = cstbTulajdonos.Id_HiddenField;
            krt_MappakSearch.Csoport_Id_Tulaj.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(tbLeiras.Text))
        {
            krt_MappakSearch.Leiras.Value = tbLeiras.Text;
            krt_MappakSearch.Leiras.Operator = Search.GetOperatorByLikeCharater(tbLeiras.Text);
        }


        return krt_MappakSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_MappakSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_MappakSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_MappakSearch();
    }
}
