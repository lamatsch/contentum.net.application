﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using Contentum.eRecord.Utility;
using System.IO;


public class Handler : IHttpHandler
{

   public void ProcessRequest ( HttpContext context )
   {
      UtilitySablonok.DownloadTemplateFile ( context.Response, context.Server );
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }

}