﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="EgyszerusitettIktatasForm.aspx.cs" Inherits="EgyszerusitettIktatasForm"
    Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc15" %>
<%@ Register Src="eRecordComponent/IktatokonyvTextBox.ascx" TagName="IktatokonyvTextBox"
    TagPrefix="uc14" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc9" %>
<%@ Register Src="Component/KodcsoportokTextBox.ascx" TagName="KodcsoportokTextBox"
    TagPrefix="uc10" %>
<%@ Register Src="Component/DataSetDropDownList.ascx" TagName="DataSetDropDownList"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/TabFooter.ascx" TagName="TabFooter" TagPrefix="uc7" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc1" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc10" %>
<%--<%@ Register Src="eRecordComponent/StandardObjektumTargyszavak.ascx" TagName="StandardObjektumTargyszavak"
    TagPrefix="sot" %>--%>
<%@ Register Src="eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel"
    TagPrefix="otp" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc11" %>
<%@ Register Src="eRecordComponent/UgyiratSzerelesiLista.ascx" TagName="UgyiratSzerelesiLista"
    TagPrefix="uc17" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel"
    TagPrefix="kp" %>
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager"
    TagPrefix="fm" %>
<%@ Register Src="~/eRecordComponent/MellekletekPanel.ascx" TagName="MellekletekPanel"
    TagPrefix="uc" %>
<%@ Register Src="eRecordComponent/IrattariTetelszamDropDownList.ascx" TagName="IrattariTetelszamDropDownList"
    TagPrefix="uc18" %>

<%@ Register Src="~/eRecordComponent/TerjedelemPanel.ascx" TagName="TerjedelemPanel" TagPrefix="uc15" %>

<%@ Register Src="Component/MinositoPartnerControl.ascx" TagName="MinositoPartnerControl" TagPrefix="ucMP" %>

<%@ Register Src="~/eRecordComponent/BejovoPeldanyPanel.ascx" TagName="BejovoPeldanyPanel" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/BejovoPeldanyListPanel.ascx" TagName="BejovoPeldanyListPanel" TagPrefix="uc" %>
<%@ Register Src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" TagName="EditablePartnerTextBoxWithTypeFilter" TagPrefix="EPARTTBWF" %>
<%@ Register Src="~/eRecordComponent/HivatkozasiSzamUserControl.ascx" TagPrefix="hszc" TagName="HivatkozasiSzamUserControl" %>
<%--BUG_10649--%>
<%@ Register Src="~/eRecordComponent/RagszamTextBox.ascx" TagName="RagszamTextBox" TagPrefix="uc5" %>
<%@ Register Src="~/eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc15" %>

<%@ Register Src="eRecordComponent/eBeadvanyAdatokUserControl.ascx" TagName="eBeadvanyAdatokUserControl" TagPrefix="ucEBAD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
            width: 220px;
        }
        /*body, html {
        overflow : auto;
    }*/
    </style>
    <uc1:FormHeader ID="FormHeader1" runat="server" FullManualHeaderTitle="<%$Resources:Form,EgyszerusitettIktatasFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <div style="width: 95%">
        <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="IraIratUpdatePanel" runat="server" OnLoad="IraIratUpdatePanel_Load">
        <ContentTemplate>
            <asp:Panel ID="MainPanel" runat="server">
                <asp:HiddenField ID="Record_Ver_HiddenField" runat="server" />
                <table cellpadding="0" cellspacing="0" width="95%">
                    <tr>
                        <td valign="top">
                            <asp:Panel ID="FoPanel" runat="server">
                                <table cellspacing="0" cellpadding="0" width="95%">
                                    <tr class="urlapSor_kicsi">
                                        <td colspan="6">
                                            <asp:RadioButtonList ID="ErkeztetesVagyIktatasRadioButtonList" runat="server" AutoPostBack="True"
                                                RepeatDirection="Horizontal" Visible="False">
                                                <%--                                                <asp:ListItem Selected="True" Value="0">Érkeztetés és iktatás</asp:ListItem>
                                                <asp:ListItem Value="1">Csak érkeztetés</asp:ListItem>
                                                <asp:ListItem Value="2">Érkeztetés és munkapéldány létrehozás</asp:ListItem>--%>
                                            </asp:RadioButtonList>
                                            <eUI:eFormPanel ID="KuldemenyPanel" runat="server">
                                                <table cellspacing="0" cellpadding="0" width="80%">
                                                    <tbody>
                                                        <tr class="urlapSor" runat="server" id="trKuldemenyPanelVonalkod">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:UpdatePanel ID="starUpdate" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="labelVonalkodPirosCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                        <asp:Label ID="Label10" runat="server" Text="Vonalkód:"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <uc10:VonalKodTextBox ID="VonalkodTextBoxVonalkod" runat="server" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="cbMegorzesJelzo" runat="server" Checked="false" ToolTip="<%$Forditas:labelBoritoTipusMegorzese|Borító típusának megőrzése%>"
                                                                    TabIndex="-1" />
                                                                <asp:Label ID="labelBoritoTipus" runat="server" Text="<%$Forditas:labelBoritoTipus|Borító típusa:%>"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="KodtarakDropDownListBoritoTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelErkeztetokonyv">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label19" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label20" runat="server" Text="Érkeztetőkönyv:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                                                <uc11:IraIktatoKonyvekDropDownList ID="ErkeztetoKonyvekDropDrownList" runat="server" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelKuldoAdoszama" runat="server" Text="Küldő adószáma:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="textAdoszam" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelErkeztetoSzam" visible="false">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="labelErlkeztetoSzam" runat="server" Text="Érkeztetési azonosító:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" style="color: red;" colspan="5">
                                                                <asp:TextBox ID="ErkeztetoSzam_TextBox" runat="server" CssClass="mrUrlapInputRed"
                                                                    ReadOnly="True"></asp:TextBox>
                                                                <%--</td>
                                                            <td class="mrUrlapCaption_short">
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="4" width="0%">--%>
                                                                <asp:HiddenField ID="Kuldemeny_Id_HiddenField" runat="server"></asp:HiddenField>
                                                                <asp:ImageButton ID="ImageButton_KuldemenyMegtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                    CommandName="" AlternateText="Megtekintés" CssClass="highlightit"></asp:ImageButton>
                                                                <%--                                                            </td>
                                                            <td style="padding-left: 2px; padding-right: 2px;">--%>
                                                                <asp:ImageButton ID="ImageButton_KuldemenyModositas" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                    CommandName="" AlternateText="Módosítás" CssClass="highlightit"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelKuldo">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label5" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label22" runat="server" Text="Küldő/feladó neve:"></asp:Label><br />
                                                                <br style="height: auto" />
                                                                <asp:Label ID="Label34" runat="server" Text="Kapcsolt partner:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                                                <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="Bekuldo_PartnerTextBox" runat="server" TryFireChangeEvent="true"
                                                                    CreateToolTipOnItems="true" OnTextChanged="Bekuldo_PartnerTextBox_TextChanged1" />
                                                                <%--  CR3113 Ügyindító nevét, címét automatikusan töltse a küldő/feladó alapján --%>
                                                                <%--<uc2:PartnerTextBox ID="Bekuldo_PartnerTextBox" runat="server" CreateToolTipOnItems="true" TryFireChangeEvent="True"></uc2:PartnerTextBox>--%>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label3" runat="server" Text="Küldő/feladó címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <%--  CR3113 Ügyindító nevét, címét automatikusan töltse a küldő/feladó alapján--%>
                                                                <uc3:CimekTextBox ID="Kuld_CimId_CimekTextBox" runat="server" Validate="false" TryFireChangeEvent="True" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBeerkezesModja">
                                                            <td class="mrUrlapCaption_short" id="tdKuld1" runat="server">
                                                                <asp:Label ID="Label12" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="lblBeerkezesModja" runat="server" Text="<%$Forditas:labelBeerkezesModja|Beérkezés módja:%>"></asp:Label>&nbsp;
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3" id="tdKuld2" runat="server">
                                                                <ktddl:KodtarakDropDownList ID="KuldesMod_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label59" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="lblKezbesitesModja" runat="server" Text="<%$Forditas:lblKezbesitesModja|Kézbesítés módja:%>"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="Kezbesites_modja_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--BUG_7678--%>
                                                        <tr class="urlapSor_kicsi" runat="server" id="tr_feladasiIdo">
                                                            <td class="mrUrlapCaption_short">
                                                                <%--<asp:CheckBox ID="CheckBox1" runat="server" ToolTip="Beérkezés időpontjának megőrzése"
                                                                    TabIndex="-1" />--%>
                                                                <asp:CheckBox ID="cbFeladasIdoMegorzes" runat="server" ToolTip="Feladás időpontjának megőrzése"
                                                                    TabIndex="-1" />
                                                                <asp:Label ID="Label_FeladasiIdoRequired" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label62" runat="server" Text="Feladási idő:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <cc:CalendarControl ID="FeladasiIdo_CalendarControl" runat="server" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short"></td>
                                                            <td class="mrUrlapMezo" width="0%"></td>
                                                        </tr>
                                                        <%-- bernat.laszlo added : Beérkezés időpontja és Érkeztetés időpontja különválik--%>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBeerkezesIdopontja">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="cbBeerkezesIdopontjaMegorzes" runat="server" ToolTip="Beérkezés időpontjának megőrzése"
                                                                    TabIndex="-1" />
                                                                <asp:Label ID="Label14" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label82" runat="server" Text="Beérkezés időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <cc:CalendarControl ID="BeerkezesIdeje_CalendarControl" runat="server" TimeVisible="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label11" runat="server" Text="Érkeztetés időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="ErkeztetesIdeje_CalendarControl" runat="server" ReadOnly="true"></cc:CalendarControl>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBonto">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label15" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label4" runat="server" Text="Bontó:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                                                <uc15:FelhasznaloCsoportTextBox ID="Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox" Validate="false" TryFireChangeEvent="true"
                                                                    runat="server" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label16" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label6" runat="server" Text="Bontás időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="Kuld_FelbontasDatuma_CalendarControl" runat="server" TimeVisible="true" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelKuldoIktatoszama">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label102" runat="server" Text="Hivatkozási szám:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <hszc:HivatkozasiSzamUserControl runat="server" ID="HivatkozasiSzamUserControl_Kuldemeny_TextBox" />
                                                                <%--<asp:TextBox ID="HivatkozasiSzam_Kuldemeny_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>--%>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="cbRagszamMegorzes" runat="server" ToolTip="Postai azonosító megőrzése" TabIndex="-1" />
                                                                <%--BUG_10649--%>
                                                                <asp:Label ID="LabelReqRagszam" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="labelRagszam" runat="server" Text="Postai azonosító:"></asp:Label>
                                                                <%--BLG_452 START--%>
                                                                <asp:Label ID="LabelFutarjegyzekListaSzama" runat="server" Text="Futárjegyzék lista száma:" Visible="false" />
                                                                <%--BLG_452 STOP--%>
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <%--BUG_10649--%>
                                                                <%--<uc10:VonalKodTextBox ID="RagszamRequiredTextBox" CssClass="mrUrlapInput" runat="server"/>--%>
                                                                <%--BUG_11273--%>
                                                                <%--<uc5:RagszamTextBox ID="RagszamRequiredTextBox" runat="server" CssClass="mrUrlapInput" RequiredValidate="false" EnableViewState="True"/>--%>
                                                                <uc5:RagszamTextBox ID="RagszamRequiredTextBox" runat="server" CssClass="mrUrlapInput" RequiredValidate="false" EnableViewState="True" Validate="False" />

                                                                <%--BLG_452 START--%>
                                                                <asp:TextBox ID="TextBoxFutarjegyzekListaSzama" CssClass="mrUrlapInput" runat="server" Visible="false" />
                                                                <%--BLG_452 STOP--%>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelAdathordozoTipusa">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label17" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label112" runat="server" Text="Küldemény típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <ktddl:KodtarakDropDownList ID="AdathordozoTipusa_DropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="AdathordozoTipusa_DropDownList_SelectedIndexChanged"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_shorter" rowspan="2">
                                                                <asp:CheckBox ID="cbBontasiMegjegyzesMegorzes" runat="server" ToolTip="Bontással kapcsolatos megjegyzés megőrzése"
                                                                    TabIndex="-1" />
                                                                <asp:Label ID="Label38" runat="server" Text="Bontással kapcsolatos megjegyzés:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" rowspan="2">
                                                                <asp:TextBox ID="bontasMegjegyzesTextBox" Rows="3" TextMode="MultiLine" runat="server"
                                                                    CssClass="mrUrlapInput" />
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelElsodlegesAdathordozoTipusa">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label39" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label57" runat="server" Text="Elsődleges adathordozó típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <ktddl:KodtarakDropDownList ID="UgyintezesModja_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trKuldemenyPanelBelsoCimzett">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:CheckBox ID="CheckBox_CimzettMegorzes" runat="server" TabIndex="-1" ToolTip="Címzett megőrzése" />
                                                                <asp:Label ID="Label13" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label122" runat="server" Text="Címzett neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <uc5:CsoportTextBox ID="CsoportId_CimzettCsoportTextBox1" runat="server"></uc5:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label18" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label132" runat="server" Text="Kézbesítés prioritása:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="Surgosseg_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--MAIN bernat.laszlo added--%>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trIktatasExtra">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label154" runat="server" Text="Munkaállomás:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <asp:TextBox ID="Munkaallomas_TextBox" runat="server" CssClass="mrUrlapInput" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label181" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <asp:Label ID="Label182" runat="server" Text="Címzés típusa:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="CimzesTipusa_DropDownList" runat="server"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--BLG_452 START--%>
                                                        <tr id="trFutarJegyzekParametersB" class="urlapSor" runat="server">
                                                            <td class="mrUrlapCaption_middle">
                                                                <asp:Label ID="LabelMinositesErvenyessegIdeje" runat="server" Text="Min érv. ideje:" />
                                                            </td>
                                                            <td class="mrUrlapMezo" colspan="3">
                                                                <cc:CalendarControl ID="CalendarControlMinositesErvenyessegIdeje" runat="server" Validate="false" />
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="LabelMinositoSzervezet" runat="server" Text="Minõsítõ szervezet:" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="LabelMinosito" runat="server" Text="Minõsítõ:" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ucMP:MinositoPartnerControl ID="MinositoPartnerControlEgyszerusitettIktatas" runat="server" />
                                                            </td>

                                                        </tr>
                                                        <%--BLG_452 STOP--%>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trMunkaallomas">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label190" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                <%--// CR 3113 : Iktatást nem igényel -> iktatást igényel--%>
                                                                <asp:Label ID="Label151" runat="server" Text="Iktatást igényel?"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <%--// CR 3113 : Iktatást nem igényel -> iktatást igényel--%>
                                                                <%--Itt nincs értelme kiválasztani, ezért disabled --%>
                                                                <asp:RadioButton ID="Ikt_n_igeny_Igen_RadioButton" runat="server" GroupName="Ikt_n_igeny_Selector" Text="Igen" Checked="true" Enabled="false" />
                                                                <asp:RadioButton ID="Ikt_n_igeny_Nem_RadioButton" runat="server" GroupName="Ikt_n_igeny_Selector" Text="Nem" Enabled="false" />
                                                            </td>

                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label156" runat="server" Text="Sérült küldemény?"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:RadioButton ID="serult_kuld_Igen_RadioButton" runat="server" GroupName="serult_kuld_Selector" Text="Igen" />
                                                                <asp:RadioButton ID="serult_kuld_Nem_RadioButton" runat="server" GroupName="serult_kuld_Selector" Text="Nem" Checked="true" />
                                                            </td>
                                                        </tr>

                                                        <tr class="urlapSor_kicsi" runat="server" id="trTevedesSerules">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label157" runat="server" Text="Téves címzés?"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                <asp:RadioButton ID="teves_cim_Igen_RadioButton" runat="server" GroupName="teves_cim_Selector" Text="Igen" />
                                                                <asp:RadioButton ID="teves_cim_Nem_RadioButton" runat="server" GroupName="teves_cim_Selector" Text="Nem" Checked="true" />
                                                            </td>

                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label158" runat="server" Text="Téves érkeztetés?"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:RadioButton ID="teves_erk_Igen_RadioButton" runat="server" GroupName="teves_erk_Selector" Text="Igen" />
                                                                <asp:RadioButton ID="teves_erk_Nem_RadioButton" runat="server" GroupName="teves_erk_Selector" Text="Nem" Checked="true" />
                                                            </td>
                                                        </tr>
                                                        <%--MAIN bernat.laszlo eddig--%>

                                                        <tr>
                                                            <td colspan="6" style="padding-top: 7px;">
                                                                <uc:MellekletekPanel ID="mellekletekPanel" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <%--BLG_452 START--%>
                                                        <tr id="trTerjedelemMainPanel" runat="server">
                                                            <td colspan="4" style="padding-top: 5px;">
                                                                <uc15:TerjedelemPanel ID="TerjedelemPanelA" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <%--BLG_452 STOP--%>
                                                    </tbody>
                                                </table>
                                            </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="EmailKuldemenyPanel" runat="server">
                                                <asp:HiddenField ID="eMailBoritekok_Ver_HiddenField" runat="server" />
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelErkeztetokonyv">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label23" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label24" runat="server" Text="Érkeztetőkönyv:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc11:IraIktatoKonyvekDropDownList ID="Email_IraIktatoKonyvekDropDownList1" runat="server"></uc11:IraIktatoKonyvekDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label25" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label26" runat="server" Text="Iktatási kötelezettség:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <ktddl:KodtarakDropDownList ID="Email_IktatasiKotelezettsegKodtarakDropDownList"
                                                                    runat="server" Width="150"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelKuldo">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label29" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label32" runat="server" Text="Küldő/feladó neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:PartnerTextBox ID="Email_Bekuldo_PartnerTextBox" runat="server" CreateToolTipOnItems="true" TryFireChangeEvent="true"></uc2:PartnerTextBox>
                                                                <%--  CR3113 Ügyindító nevét, címét automatikusan töltse a küldő/feladó alapján--%>
                                                                <%--Ahhoz hogy az ügyindító címét is automatikusan töltsük, kell a címmező is (itt alapból nem volt megjelenítve), viszont ha invisible, akkor nem működik, ezért átverjük :(--%>
                                                                <div style="visibility: hidden; width: 1px; height: 1px">
                                                                    <uc3:CimekTextBox ID="Email_CimekTextBox" runat="server" Validate="false" />
                                                                </div>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label33" runat="server" Text="Küldő szervezete:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc2:PartnerTextBox ID="Email_BekuldoSzervezete_PartnerTextBox" runat="server" ReadOnly="true"
                                                                    ViewMode="true"></uc2:PartnerTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelEmailCim">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label35" runat="server" Text="Küldő e-mail címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc3:CimekTextBox ID="Email_KuldCim_CimekTextBox" runat="server" Validate="false"></uc3:CimekTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label115" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label37" runat="server" Text="E-mail tárgya:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="Email_Targy_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelBelsoCimzett">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label40" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label123" runat="server" Text="Címzett neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc5:CsoportTextBox ID="Email_CsoportIdCimzett_CsoportTextBox" runat="server"></uc5:CsoportTextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label124" runat="server" Text="Címzett szervezete:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc5:CsoportTextBox ID="Email_CsoportId_CimzettSzervezete" runat="server" ReadOnly="true"
                                                                    ViewMode="true"></uc5:CsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelCimzettEmail">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label_cimzett" runat="server" Text="Címzett e-mail címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="Email_CimzettEmailcime" runat="server" CssClass="mrUrlapInput" Validate="false"></asp:TextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label42" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label43" runat="server" Text="Érkeztető:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc15:FelhasznaloCsoportTextBox ID="Email_ErkeztetoFelhasznaloCsoportTextBox1" runat="server"
                                                                    ReadOnly="true" ViewMode="true"></uc15:FelhasznaloCsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelBeerkezesIdopontja">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label46" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label47" runat="server" Text="Érkeztetés időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="Email_BeerkezesIdeje_CalendarControl" runat="server" TimeVisible="true"
                                                                    ReadOnly="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label48" runat="server" Text="Beérkezés módja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <ktddl:KodtarakDropDownList ID="Email_BeerkezesMod_DropDownList" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi" runat="server" id="trEmailKuldemenyPanelFeladasIdopontja">
                                                            <td class="mrUrlapCaption_short">
                                                                <%--BUG_7678--%>
                                                                <asp:Label ID="Label_EmailFeladasiIdoRequired" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label53" runat="server" Text="Feladás időpontja:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <cc:CalendarControl ID="Email_FeladasiIdo_CalendarControl" runat="server" TimeVisible="true"
                                                                    ReadOnly="true"></cc:CalendarControl>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label54" runat="server" Text="Hivatkozási szám:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <asp:TextBox ID="Email_HivatkozasiSzam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" style="padding-top: 7px;">
                                                                <uc:MellekletekPanel ID="mellekletekPanelEmail" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <%--<tr id="trKezelesiFeljegyzesKuldemeny" runat="server" visible="false">
                                                            <td colspan="4">
                                                            <div style="padding-top:5px;">
                                                                <kp:KezelesiFeljegyzesPanel runat="server" ID="KezelesiFeljegyzesPanelKuldemeny" Collapsed="true" RenderMode="Panel"/>
                                                            </div>
                                                            </td>
                                                        </tr>--%>
                                                    </tbody>
                                                </table>
                                            </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="eBeadvanyokPanel" runat="server" Visible="false">
                                                <asp:HiddenField ID="eBeadvanyokPanel_Ver" runat="server" />
                                                <asp:HiddenField ID="HiddenField_PR_ErkeztetesiSzam" runat="server" />
                                                <asp:HiddenField ID="HiddenField_Partner_Id" runat="server" />
                                                <asp:HiddenField ID="HiddenField_Cim_Id" runat="server" />
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tbody>
                                                        <tr class="urlapSor_kicsi" runat="server" id="treBeadvanyokPanelErkeztetokonyv">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label7" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label21" runat="server" Text="Érkeztetőkönyv:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo" width="0%">
                                                                <uc11:IraIktatoKonyvekDropDownList ID="eBeadvanyokPanel_IraIktatoKonyvekDropDownList" runat="server"></uc11:IraIktatoKonyvekDropDownList>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label63" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label64" runat="server" Text="Iktatási kötelezettség:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <ktddl:KodtarakDropDownList ID="eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList"
                                                                    runat="server" Width="150"></ktddl:KodtarakDropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="eBeadvanyokPanel_labelNev" runat="server" Text="Feladó neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="eBeadvanyokPanel_Nev" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="eBeadvanyokPanel_labelErkeztetesiSzam" runat="server" Text="Hivatkozási szám:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="eBeadvanyokPanel_ErkeztetesiSzam" runat="server" CssClass="mrUrlapInput" ReadOnly="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="eBeadvanyokPanel_labelFeladoCim" runat="server" Text="Feladó címe:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="eBeadvanyokPanel_FeladoCim" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="Label60" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                <asp:Label ID="Label61" runat="server" Text="Címzett neve:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <uc5:CsoportTextBox ID="eBeadvanyokPanel_CsoportIdCimzett_CsoportTextBox" runat="server"></uc5:CsoportTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr class="urlapSor_kicsi">
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="eBeadvanyokPanel_labelDokTipusHivatal" runat="server" Text="Hivatal:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="eBeadvanyokPanel_DokTipusHivatal" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                            <td class="mrUrlapCaption_short">
                                                                <asp:Label ID="eBeadvanyokPanel_labelDokTipusAzonosito" runat="server" Text="Típus:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezo">
                                                                <asp:TextBox ID="eBeadvanyokPanel_DokTipusAzonosito" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                            <tr class="urlapSor_kicsi">
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="eBeadvanyokPanel_labelDokTipusLeiras" runat="server" Text="Típus név:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo">
                                                                    <asp:TextBox ID="eBeadvanyokPanel_DokTipusLeiras" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="eBeadvanyokPanel_labelFileNev" runat="server" Text="Fájlnév:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo">
                                                                    <asp:TextBox ID="eBeadvanyokPanel_FileNev" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr class="urlapSor_kicsi">
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="eBeadvanyokPanel_labelMegjegyzes" runat="server" Text="Megjegyzés:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo">
                                                                    <asp:TextBox ID="eBeadvanyokPanel_Megjegyzes" runat="server" CssClass="mrUrlapInput" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                                <%--BUG_7678--%>
                                                                <td class="mrUrlapCaption_short">
                                                                    <%--<asp:CheckBox ID="CheckBox1" runat="server" ToolTip="Beérkezés időpontjának megőrzése"
                                                                    TabIndex="-1" />--%>
                                                                    <asp:Label ID="Label_eBeadvanyokFeladasiIdoRequired" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                    <asp:Label ID="Label69" runat="server" Text="Feladási idő:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%" colspan="3">
                                                                    <cc:CalendarControl ID="eBeadvanyokPanel_FeladasiIdo" runat="server" ReadOnly="true" TimeVisible="true"></cc:CalendarControl>
                                                                </td>
                                                            </tr>
                                                            <tr class="urlapSor_kicsi">
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="Label44" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                                    <asp:Label ID="Label45" runat="server" Text="Beérkezés/Érkeztetés időpontja:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <cc:CalendarControl ID="eBeadvanyokPanel_BeerkezesIdeje" runat="server" TimeVisible="true"
                                                                        ReadOnly="true"></cc:CalendarControl>
                                                                </td>
                                                                <td class="mrUrlapCaption_short">
                                                                    <asp:Label ID="Label58" runat="server" Text="Beérkezés módja:"></asp:Label>
                                                                </td>
                                                                <td class="mrUrlapMezo" width="0%">
                                                                    <ktddl:KodtarakDropDownList ID="eBeadvanyokPanel_BeerkezesMod" runat="server" ReadOnly="true"></ktddl:KodtarakDropDownList>
                                                                </td>
                                                            </tr>
                                                    </tbody>
                                                </table>

                                                <ucEBAD:eBeadvanyAdatokUserControl ID="eBeadvanyAdatokUserControl1" runat="server" />

                                            </eUI:eFormPanel>
                                            <kp:KezelesiFeljegyzesPanel runat="server" ID="KezelesiFeljegyzesPanelKuldemeny" />
                                            <%-- Rejtett textbox tárolja a régi ügyirat irattári tételszámát és megkülönböztető jelzését --%>
                                            <asp:TextBox ID="IrattariTetelTextBox" Style="display: none;" Text="" runat="server" />
                                            <asp:TextBox ID="MegkulJelzesTextBox" Style="display: none;" Text="" runat="server" />
                                            <asp:TextBox ID="IRJ2000TextBox" Style="display: none;" Text="" runat="server" />
                                            <eUI:CustomCascadingDropDown ID="CascadingDropDown_AgazatiJel" runat="server" UseContextKey="true"
                                                ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetAgazatiJelek"
                                                Category="AgazatiJel" Enabled="False" TargetControlID="AgazatiJelek_DropDownList"
                                                EmptyText="---" EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]"
                                                PromptText="<Ágazati jel kiválasztása>" PromptValue="">
                                            </eUI:CustomCascadingDropDown>
                                            <eUI:CustomCascadingDropDown ID="CascadingDropDown_Ugykor" runat="server" TargetControlID="Ugykor_DropDownList"
                                                ParentControlID="AgazatiJelek_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                                ServiceMethod="GetUgykorokByAgazatiJel" Category="Ugykor" Enabled="False" EmptyText="---"
                                                EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Irattári tételszám kiválasztása>"
                                                PromptValue="">
                                            </eUI:CustomCascadingDropDown>
                                            <eUI:CustomCascadingDropDown ID="CascadingDropDown_Ugytipus" runat="server" TargetControlID="UgyUgyirat_Ugytipus_DropDownList"
                                                ParentControlID="Ugykor_DropDownList" UseContextKey="true" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx"
                                                ServiceMethod="GetUgytipusokByUgykor" Category="Ugytipus" Enabled="False" EmptyText="---"
                                                EmptyValue="" SelectedValue="" LoadingText="[Feltöltés folyamatban...]" PromptText="<Ügy típusának kiválasztása>"
                                                PromptValue="">
                                            </eUI:CustomCascadingDropDown>
                                            <eUI:CustomCascadingDropDown ID="CascadingDropDown_IktatoKonyv" runat="server" TargetControlID="Iktatokonyvek_DropDownLis_Ajax"
                                                ParentControlID="IrattariTetelszam_DropDownList" UseContextKey="true" Category="Iktatokonyv"
                                                Enabled="false" ServicePath="~/WrappedWebService/Ajax_eRecord.asmx" ServiceMethod="GetIktatokonyvekByIrattaritetel"
                                                EmptyText="---" EmptyValue="" LoadingText="[Feltöltés folyamatban...]" SelectedValue=""
                                                PromptText="<Iktatókönyv kiválasztása>" PromptValue="">
                                            </eUI:CustomCascadingDropDown>
                                            <uc17:UgyiratSzerelesiLista runat="server" ID="UgyiratSzerelesiLista1" />
                                            <eUI:eFormPanel ID="Ugyirat_Panel" runat="server">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr id="tr_lezartazUgyirat" runat="server" visible="false" class="urlapSor_kicsi">
                                                        <td colspan="2" style="text-align: center; color: Red; font-weight: bold;">
                                                            <asp:Label ID="Label2" runat="server" Text="Figyelem: Lezárt az előzmény ügyirat!"></asp:Label>
                                                        </td>
                                                        <td colspan="2" style="text-align: left;">
                                                            <asp:RadioButtonList ID="RadioButtonList_Lezartbaiktat_vagy_Ujranyit" runat="server"
                                                                RepeatDirection="Horizontal">
                                                                <asp:ListItem Selected="True" Value="0">Lez&#225;rt &#252;gyiratba iktat&#225;s</asp:ListItem>
                                                                <asp:ListItem Value="1">&#220;gyirat &#250;jranyit&#225;sa</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_lezartazIktatokonyv" runat="server" visible="false" class="urlapSor_kicsi">
                                                        <td colspan="4" style="text-align: center; color: Red; font-weight: bold;">
                                                            <asp:Label ID="labelLezart" runat="server" Text="Figyelem: Az előzmény lezárt iktatókönyvben van! Az előzmény az új ügyiratba lesz szerelve!" />
                                                            <asp:Label ID="labelLezartFolyamatos" runat="server" Text="Figyelem: Az előzmény lezárt, folyósorszámos iktatókönyvben van! (iktatás csak alszámra megengedett)" />
                                                            <asp:HiddenField ID="LezartIktatokonyv_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="LezartFolyosorszamosIktatokonyv_HiddenField" runat="server" />
                                                        </td>
                                                        <%--                                                        <td colspan="2" style="text-align: left;">
                                                        </td>--%>
                                                    </tr>
                                                    <tr runat="server" id="trFoszam" visible="false" class="foszamRow">
                                                        <td class="foszamLabel" colspan="2">
                                                            <asp:Label ID="labelFoszam" runat="server" CssClass="DisableWrap" Text="Ügyirat iktatószáma:"> </asp:Label>
                                                        </td>
                                                        <td class="foszamText" colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td class="foszamText">
                                                                        <asp:Label ID="UgyiratFoszam_Label" runat="server" CssClass="DisableWrap" Text="[Főszám]"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 100%"></td>
                                                                    <td style="text-align: right;">
                                                                        <asp:ImageButton ID="FoszamraIktat_ImageButton" runat="server" Visible="false" CausesValidation="false"
                                                                            ImageUrl="~/images/hu/egyeb/reset_icon.png" onmouseover="swapByName(this.id,'reset_icon_keret.png')"
                                                                            onmouseout="swapByName(this.id,'reset_icon.png')" AlternateText="Előzmény törlése"
                                                                            ToolTip="Előzmény törlése" OnClick="FoszamraIktat_ImageButton_Click"></asp:ImageButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trRegiAdatAzonosito" visible="false" class="foszamRow">
                                                        <td class="foszamLabel" colspan="2">
                                                            <asp:Label ID="labelRegiAdatAzonosito" runat="server" CssClass="DisableWrap" Text="Régi azonosító:"> </asp:Label>
                                                        </td>
                                                        <td class="foszamText" colspan="2">
                                                            <asp:Label ID="RegiAdatAzonosito_Label" runat="server" CssClass="DisableWrap" Text="[Régi azonosító]"> </asp:Label>
                                                            <asp:Label ID="RegiAdatIRJ2000_Label" runat="server" CssClass="DisableWrap" Text="" Visible="false"> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trElozmenySearch" style="background-color: #F2E3FB;">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelEv" runat="server" Text="Év:"></asp:Label>
                                                        </td>
                                                        <td colspan="3" class="mrUrlapMezo">
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="mrUrlapMezo" style="width: 90px;">
                                                                        <uc11:EvIntervallum_SearchFormControl ID="evIktatokonyvSearch" IsIntervallumMode="false"
                                                                            runat="server" />
                                                                    </td>
                                                                    <td class="mrUrlapCaption_nowidth" style="width: 80px;">
                                                                        <asp:Label ID="labelIktatokonyvSearch" runat="server" Text="Iktatókönyv:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="width: 110px">
                                                                        <uc11:IraIktatoKonyvekDropDownList ID="IktatoKonyvekDropDownList_Search" runat="server"
                                                                            Mode="IktatokonyvekWithRegiAdatok" EvIntervallum_SearchFormControlId="evIktatokonyvSearch" />
                                                                    </td>
                                                                    <td class="mrUrlapCaption_nowidth" style="width: 80px">
                                                                        <asp:Label ID="labelFoszamSearch" runat="server" Text="Főszám:"></asp:Label>
                                                                    </td>
                                                                    <td class="mrUrlapMezo" style="width: 120px">
                                                                        <uc11:RequiredNumberBox ID="numberFoszamSearch" Validate="false" runat="server" />
                                                                    </td>
                                                                    <td style="padding-top: 5px">
                                                                        <%--<asp:ImageButton ID="ImageButtonElozmenySearch" runat="server" CausesValidation="false"
                                                        CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif"                                            
                                                        ToolTip="Előzmény" Visible="True" CssClass="highlightit" OnClick="ImageButtonElozmenySearch_Click" />--%>
                                                                        <asp:ImageButton ID="ImageButton_Elozmeny" runat="server" CausesValidation="false"
                                                                            CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif" ToolTip="Előzmény"
                                                                            Visible="True" CssClass="highlightit" OnClick="ImageButton_Elozmeny_Click" />
                                                                    </td>
                                                                    <td style="padding-top: 5px">
                                                                        <asp:ImageButton ID="ImageButton_MigraltKereses" runat="server" CausesValidation="false"
                                                                            CommandName="" ImageUrl="~/images/hu/trapezgomb/migraltkereses_trap1.jpg" ToolTip="Régi adatok"
                                                                            Visible="True" CssClass="highlightit" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_agazatiJel" runat="server" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_agazatiJel" runat="server" Text="Ágazati jel:"></asp:Label>
                                                            <%--BLG_44--%>
                                                            <asp:Label ID="labelUgyFajtaja" runat="server" Text="Ügy fajtája:"></asp:Label>

                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:DropDownList ID="AgazatiJelek_DropDownList" runat="server" CssClass="mrUrlapInputComboBox"
                                                                Width="97%">
                                                            </asp:DropDownList>
                                                            <%--BLG_44--%>
                                                            <ktddl:KodtarakDropDownList ID="UGY_FAJTAJA_KodtarakDropDownList" runat="server" CssClass="" Width="98%" />

                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <%--                                                    <asp:Lable ID="labelMerge_IrattariTetelszam" runat="server" Text="Összefűzött itsz.:" />--%>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="Merge_IrattariTetelszamTextBox" runat="server" ReadOnly="true" CssClass="mrUrlapLabelLike"
                                                                TabIndex="-1" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelIrattariTetelszam">
                                                        <td class="mrUrlapCaption_short">
                                                            <%--BLG_44--%>
                                                            <%--<asp:Label ID="label_IrattariTetelszam_DropDownList" runat="server" Text="Irattári tételszám:"></asp:Label>  
                                                            --%>
                                                            <%--CR3119 : Mindenhol irattári tételszám jelenjen meg --%>
                                                            <asp:Label ID="labelIrattariTetelszam" runat="server" Text="Irattári tételszám:"></asp:Label>
                                                            <%-- <asp:Label ID="labelUgykorKod" runat="server" Text="Irattári tételszám:"></asp:Label>--%>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:DropDownList ID="Ugykor_DropDownList" runat="server" CssClass="mrUrlapInputComboBox"
                                                                Width="97%">
                                                            </asp:DropDownList>
                                                            <uc12:IraIrattariTetelTextBox ID="UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox"
                                                                runat="server" Validate="false" Visible="false" />
                                                            <%--BLG_44--%>
                                                            <uc18:IrattariTetelszamDropDownList ID="IrattariTetelszam_DropDownList" runat="server"></uc18:IrattariTetelszamDropDownList>


                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*">&nbsp;</asp:Label><asp:Label
                                                                ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc11:IraIktatoKonyvekDropDownList ID="ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1"
                                                                runat="server" />
                                                            <asp:DropDownList ID="Iktatokonyvek_DropDownLis_Ajax" runat="server" CssClass="mrUrlapInputComboBox"
                                                                Visible="False">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Iktatokonyvek_DropDownLis_Ajax"
                                                                Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                                                                TargetControlID="RequiredFieldValidator1">
                                                                <Animations>
                                                                    <OnShow>
                                                                    <Sequence>
                                                                        <HideAction Visible="true" />
                                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                                    </Sequence>    
                                                                    </OnShow>
                                                                </Animations>
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_ugytipus" runat="server" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label27" runat="server" Text="Ügy típusa:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:DropDownList ID="UgyUgyirat_Ugytipus_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" OnSelectedIndexChanged="UgyUgyirat_Ugytipus_DropDownList_SelectedIndexChanged"
                                                                Width="97%">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="UgyUgyirat_Ugytipus_DropDownList"
                                                                Display="None" SetFocusOnError="true" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                                                                TargetControlID="RequiredFieldValidator2">
                                                                <Animations>
                                                                    <OnShow>
                                                                    <Sequence>
                                                                        <HideAction Visible="true" />
                                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                                    </Sequence>    
                                                                    </OnShow>
                                                                </Animations>
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        </td>
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo"></td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelTargy">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label55" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label30" runat="server" Text="Ügyirat tárgya:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc8:RequiredTextBox ID="UgyUgyirat_Targy_RequiredTextBox" Width="95%" runat="server"
                                                                Rows="2" TextBoxMode="MultiLine" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short" colspan="2">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td></td>
                                                                    <td style="padding-left: 2px;"></td>
                                                                    <td></td>
                                                                    <td style="width: 100%"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Megtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                            CommandName="" AlternateText="Megtekintés" CssClass="highlightit" />
                                                                        <asp:HiddenField ID="ElozmenyUgyiratID_HiddenField" runat="server" />
                                                                        <asp:HiddenField ID="MigraltUgyiratID_HiddenField" runat="server" />
                                                                    </td>
                                                                    <td style="padding-left: 2px;">
                                                                        <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Modositas" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                            CommandName="" AlternateText="Módosítás" CssClass="highlightit" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="ImageButton_Ugyiratterkep" runat="server" CausesValidation="false"
                                                                            CssClass="highlightit" ImageUrl="~/images/hu/trapezgomb/ugyirat_fa_trap.jpg"
                                                                            ToolTip="Ügyirat-fa" Visible="True" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label runat="server" Text="Ügyirat ügyintézési ideje:" ID="trUgyiratIntezesiIdoLabel" Visible="false" />
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">

                                                            <input id="inputUgyUgyintezesiNapok" runat="server" visible="false"
                                                                list='datalistUgyUgyintezesiNapok'
                                                                name="inputNameUgyUgyintezesiNapok"
                                                                min="0" max="10000000"
                                                                style="width: 150px;" autopostback="true" />
                                                            <datalist id="datalistUgyUgyintezesiNapok" runat="server">
                                                            </datalist>

                                                            <%--<ktddl:KodtarakDropDownList ID="IntezesiIdo_KodtarakDropDownList" runat="server" CssClass="" />--%>
                                                            <ktddl:KodtarakDropDownList ID="IntezesiIdoegyseg_KodtarakDropDownList" runat="server" CssClass="" Visible="false" />
                                                        </td>

                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_CalendarControl_UgyintezesKezdete" runat="server" Text="Ügyirat ügyintézési kezdete:" />
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="CalendarControl_UgyintezesKezdete" runat="server" Validate="false" TimeVisible="true" Enabled="false" ReadOnly="true" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelIntezesiHatarido">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label28" runat="server" Text="Ügyirat ügyintézési határideje:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="360px">
                                                            <cc:CalendarControl ID="UgyUgyirat_Hatarido_CalendarControl" runat="server" Visible="true"
                                                                Validate="false" TimeVisible="true" />
                                                            <%-- az alszámra iktatásnál a határidő módosulásának figyeléséhez --%>
                                                            <asp:HiddenField ID="ElozmenyUgyiratHatarido_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="UgyiratHataridoKitolas_Ugyirat_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="IktatasDatuma_Ugyirat_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="FelhasznaloCsoportIdOrzo_HiddenField" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelSkontroVege" runat="server" Text="Határidőbe tétel lejárata:" Visible="False"></asp:Label>
                                                            <asp:Label ID="labelIrattarba" runat="server" Text="Irattárba helyezés dátuma:" Visible="False"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="360px">
                                                            <asp:TextBox ID="txtSkontroVege" runat="server" Style="display: none;" />
                                                            <asp:TextBox ID="txtIrattarba" runat="server" Style="display: none;" />
                                                            <cc:CalendarControl ID="UgyUgyirat_SkontroVege_CalendarControl" runat="server" Visible="false"
                                                                Validate="false" ReadOnly="True" />
                                                            <cc:CalendarControl ID="UgyUgyirat_IrattarbaHelyezes_CalendarControl" runat="server"
                                                                Visible="false" Validate="false" ReadOnly="True" />
                                                        </td>
                                                    </tr>
                                                    <tr id="migraltUgyiratAzonositoMezo" class="urlapSor_kicsi" runat="server" style="display: none;">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="regiAzonositoLabel" runat="server" Text="Régi azonosító"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="regiAzonositoTextBox" runat="server" Validate="false" CssClass="ReadOnlyWebControl" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_JavasoltElozmeny" class="urlapSor_kicsi" runat="server" visible="false">
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelJavasoltElozmeny" runat="server" Text="Javasolt előzmény:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <asp:TextBox ID="JavasoltElozmenyTextBox" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelFelelos">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="ReqStar_Ugyfelelos_CsoportTextBox" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label9" runat="server" Text="Szervezeti egység:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc5:CsoportTextBox ID="Ugyfelelos_CsoportTextBox" runat="server" SzervezetCsoport="true"
                                                                Validate="false" LabelRequiredIndicatorID="ReqStar_Ugyfelelos_CsoportTextBox" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelUgyUgyirat_UgyintezoStar" runat="server" CssClass="ReqStar" Text="*" />
                                                            <%--BLG_1014--%>
                                                            <%--<asp:Label ID="Label34" runat="server" Text="Ügyintéző:"></asp:Label>--%>
                                                            <asp:Label ID="labelUgyiratUgyintezo" runat="server" Text="<%$Forditas:labelUgyiratUgyintezo|Ügyintéző:%>"></asp:Label>

                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                            <asp:UpdatePanel ID="UpdateUgyintezo" runat="server" UpdateMode="Always">
                                                                <ContentTemplate>
                                                                    <uc15:FelhasznaloCsoportTextBox ID="UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox"
                                                                        runat="server" Validate="false" LabelRequiredIndicatorID="labelUgyUgyirat_UgyintezoStar" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelKezelo">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label51" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label31" runat="server" Text="Kezelő:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc5:CsoportTextBox ID="UgyUgyiratok_CsoportId_Felelos" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="lblReqUgyindito" runat="server" Text="*" Visible="false" CssClass="ReqStar"></asp:Label>
                                                            <asp:Label ID="lblUgyindito" runat="server" Text="Ügyfél, ügyindító:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                            <uc2:PartnerTextBox ID="Ugy_PartnerId_Ugyindito_PartnerTextBox" Validate="false"
                                                                runat="server" CreateToolTipOnItems="true" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trUgyirat_PanelIratHelye">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelIratHelye_Ugyirat" runat="server" Text="Irat helye:" Visible="false"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <uc15:FelhasznaloCsoportTextBox ID="UgyiratOrzo_FelhasznaloCsoportTextBox" runat="server"
                                                                Validate="false" ViewMode="true" Visible="false" ReadOnly="true" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="lblReqUgyinditoCime" runat="server" Text="*" Visible="false" CssClass="ReqStar"></asp:Label>
                                                            <asp:Label ID="labelUgyinditoCime" runat="server" Text="Ügyindító címe:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                            <uc3:CimekTextBox ID="CimekTextBoxUgyindito" runat="server" Validate="false"></uc3:CimekTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_fizikaihely" runat="server" visible="false" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="label36" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                            <asp:Label ID="LabelFizikai" runat="server" Text="Fizikai hely:" Visible="true"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDownTUK" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo"></td>
                                                    </tr>
                                                </table>
                                            </eUI:eFormPanel>
                                            <%-- Standard objektumfüggő tárgyszavak ügyirathoz --%>
                                            <eUI:eFormPanel ID="StandardTargyszavakPanel" runat="server" Visible="true">
                                                <%--<sot:StandardObjektumTargyszavak ID="standardObjektumTargyszavak" runat="server" />--%>
                                                <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak" RepeatColumns="2" RepeatDirection="Horizontal"
                                                    runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                            </eUI:eFormPanel>
                                            <asp:HiddenField ID="UgyiratdarabEljarasiSzakasz_HiddenField" runat="server" />
                                            <eUI:eFormPanel ID="IratPanel" runat="server">
                                                <table cellspacing="0" cellpadding="0" width="100%">
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratPanelTargy">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label41" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                            <asp:Label ID="Label50" runat="server" Text="Irat tárgya:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <uc8:RequiredTextBox ID="IraIrat_Targy_RequiredTextBox" runat="server" Rows="2" TextBoxMode="MultiLine"
                                                                Width="95%" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short" width="0%">
                                                            <%--<asp:Label ID="Label38" runat="server" Text="Irat kategória:"></asp:Label>--%>
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="Label_Pld_IratTipusCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                    <asp:Label ID="Label56" runat="server" Text="Irat típus:"></asp:Label>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <%--<ktddl:KodtarakDropDownList ID="IraIrat_Kategoria_KodtarakDropDownList" runat="server" />--%>
                                                            <ktddl:KodtarakDropDownList ID="IraIrat_Irattipus_KodtarakDropDownList" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_Kiadmanyozas" runat="server" class="urlapSor_kicsi">
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:CheckBox ID="IraIrat_KiadmanyozniKell_CheckBox" runat="server" Text="Kiadmányozni kell" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_Kiadmanyozo" runat="server" Text="Kiadmányozó:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <uc15:FelhasznaloCsoportTextBox ID="IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox"
                                                                runat="server" Validate="false" ReadOnly="true" ViewMode="true" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratPanelIratMinosites">
                                                        <td class="mrUrlapCaption_short">
                                                            <%--BLG_1053--%>
                                                            <%--<asp:Label ID="labelIratMinosites" runat="server" Text="Kezelési utasítások"></asp:Label>--%>
                                                            <asp:Label ID="labelIratMinosites" runat="server" Text="<%$Forditas:labelIratMinosites|Irat minősítése:%>"></asp:Label>

                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <ktddl:KodtarakDropDownList ID="ktDropDownListIratMinosites" runat="server" Width="97%" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:UpdatePanel ID="StarUpdate1" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="Label_UgyintezoCsillag" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                                    <%--BLG_1014--%>
                                                                    <%--<asp:Label ID="Label133" runat="server" Text="Ügyintéző:"></asp:Label>--%>
                                                                    <asp:Label ID="labelIratUgyintezo" runat="server" Text="<%$Forditas:labelIratUgyintezo|Ügyintéző:%>"></asp:Label>


                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc15:FelhasznaloCsoportTextBox ID="Irat_Ugyintezo_FelhasznaloCsoportTextBox" runat="server"
                                                                Validate="false" />
                                                        </td>
                                                    </tr>
                                                    <%--BLG_1020--%>
                                                    <tr class="urlapSor_kicsi" runat="server" id="trIratIntezesiIdo" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label8" runat="server" Text="Irat ügyintézési ideje:" />
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <input id="inputUgyintezesiNapok" runat="server"
                                                                list='datalistUgyintezesiNapok'
                                                                name="inputNameUgyintezesiNapok"
                                                                min="0" max="10000000"
                                                                style="width: 150px;" autopostback="true" />
                                                            <datalist id="datalistUgyintezesiNapok" runat="server">
                                                            </datalist>
                                                            <%--<ktddl:KodtarakDropDownList ID="IratIntezesiIdo_KodtarakDropDownList" runat="server" CssClass="" />--%>
                                                            <ktddl:KodtarakDropDownList ID="IratIntezesiIdoegyseg_KodtarakDropDownList" runat="server" CssClass="" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_Irat_Ugyfelelos_CsoportTextBox" runat="server" Text="<%$Resources:Form,Irat_Csoport_Id_Ugyfelelos %>" Visible="false" />
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <uc5:CsoportTextBox ID="Irat_Ugyfelelos_CsoportTextBox" runat="server" SzervezetCsoport="true" Validate="false" Visible="false" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_IratHatarido" class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short">&nbsp;<asp:Label ID="labelIratHatarido" runat="server" Text="Irat ügyintézési határideje:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <cc:CalendarControl ID="IraIrat_Hatarido_CalendarControl" runat="server" Visible="true"
                                                                Validate="false" PopupPosition="TopLeft" TimeVisible="true" />
                                                            <%-- az alszámra iktatásnál a határidő módosulásának figyeléséhez --%>
                                                            <asp:HiddenField ID="UgyiratHataridoKitolas_Irat_HiddenField" runat="server" />
                                                            <asp:HiddenField ID="IktatasDatuma_Irat_HiddenField" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="LabelIratHatasaUgyintezesre" runat="server"
                                                                Text="<%$Forditas:LabelIratHatasaUgyintezesre|Irat hatása az ügyintézésre (sakkóra)%>" />
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <ktddl:KodtarakDropDownList ID="IratHatasaUgyintezesre_KodtarakDropDownList" runat="server" CssClass="mrUrlapInput" AutoPostBack="true" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr1001" class="urlapSor_kicsi" runat="server">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_EljarasiSzakaszFok" runat="server" Text="Eljárási szakasz:"></asp:Label>
                                                        </td>
                                                        <td></td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1">
                                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownList_EljarasiSzakaszFok" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="LabelEljarasFefuggesztesenekOka" runat="server" Text="<%$Forditas:LabelEljarasFefuggesztesenekOka|Eljárás felfüggesztésének, szünetelésének oka%>"
                                                                Visible="false" />
                                                            <asp:Label ID="LabelEljarasLezarasOka" runat="server" Text="<%$Forditas:LabelEljarasLezarasOka|Eljárás lezárásának oka%>"
                                                                Visible="false" />
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="1" width="0%">
                                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownListUgyiratFelfuggesztesOka" runat="server" CssClass="mrUrlapInput" AutoPostBack="true" Visible="false" />
                                                            <asp:TextBox runat="server" ID="TextBoxFelfuggesztesOka" CssClass="mrUrlapInput" AutoPostBack="true" Visible="false" />
                                                            <ktddl:KodtarakDropDownList ID="KodtarakDropDownListUgyiratLezarasOka" runat="server" CssClass="mrUrlapInput" AutoPostBack="true" Visible="false" />
                                                        </td>

                                                    </tr>
                                                    <tr id="tr_EgyebMuvelet" runat="server" class="urlapSor_kicsi" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="Label_EgyebMuvelet" runat="server" Text="Egyéb művelet:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="0%">
                                                            <asp:RadioButtonList ID="RadioButtonList_BejovoIktatasnal" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0" Selected="True">Semmi</asp:ListItem>
                                                                <asp:ListItem Value="1">Skontr&#243;b&#243;l kivesz</asp:ListItem>
                                                                <asp:ListItem Value="2">Lez&#225;r</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:RadioButtonList ID="RadioButtonList_BelsoIratIktatasnal" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="0" Selected="True">Semmi</asp:ListItem>
                                                                <asp:ListItem Value="1">Skontr&#243;ba helyez</asp:ListItem>
                                                                <asp:ListItem Value="2">Elint&#233;z</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <td class="mrUrlapCaption_short"></td>
                                                        <td class="mrUrlapMezo" width="0%" colspan="1"></td>
                                                    </tr>
                                                    <tr id="tr_IratJelleg" runat="server" class="urlapSor_kicsi" visible="false">
                                                        <td class="mrUrlapCaption_short">
                                                            <asp:Label ID="labelIratJelleg" runat="server" Text="Irat jellege:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo" width="0%"></td>
                                                        <td class="mrUrlapMezo" width="40%">
                                                            <ktddl:KodtarakDropDownList ID="IratJellegKodtarakDropDown" runat="server" ReadOnly="true" />
                                                        </td>

                                                    </tr>
                                                    <tr id="tr_kezelesifeljegyzes" runat="server">
                                                        <td colspan="5">
                                                            <div style="padding-top: 2px;">
                                                                <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" Collapsed="true"
                                                                    RenderMode="Panel" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="StandardTargyszavakPanel_Iratok" runat="server" Visible="true">
                                                <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak_Iratok" RepeatColumns="2" RepeatDirection="Horizontal"
                                                    runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                            </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="TipusosTargyszavakPanel_Iratok" runat="server" Visible="true">
                                                <otp:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_Iratok" RepeatColumns="2" RepeatDirection="Horizontal"
                                                    runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                                <otp:ObjektumTargyszavaiPanel ID="otpTipusosTargyszavak_IratokPostazasIranya" RepeatColumns="2" RepeatDirection="Horizontal"
                                                    runat="server" RepeatLayout="Table" CssClass="mrUrlapInputShort" />
                                            </eUI:eFormPanel>
                                            <uc:BejovoPeldanyListPanel ID="BejovoPeldanyListPanel1" runat="server" />
                                            <eUI:eFormPanel ID="ErkeztetesResultPanel" runat="server" Visible="false" CssClass="mrResultPanelErkeztetes">
                                                <div style="padding-bottom: 20px;">
                                                    <div class="mrResultPanelText">
                                                        Az érkeztetés sikeresen végrehajtódott.
                                                    </div>
                                                    <table cellspacing="0" cellpadding="0" width="800px" class="mrResultTableErkeztetes">
                                                        <tr class="urlapSorBigSize">
                                                            <td class="mrUrlapCaptionBigSize">
                                                                <asp:Label ID="labelKuldemeny" runat="server" Text="Érkeztetési azonosító:"></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapMezoBigSize">
                                                                <asp:Label ID="labelKuldemenyErkSzam" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td class="mrUrlapCaption" style="padding-top: 5px">
                                                                <asp:ImageButton ID="imgKuldemenyMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                            </td>
                                                            <td class="mrUrlapMezo" style="padding-top: 5px; width: 350px;">
                                                                <span style="padding-right: 0px;">
                                                                    <asp:ImageButton ID="imgKuldemenyModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                        onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                                    <asp:ImageButton ID="imgSendEmailErkeztetve" runat="server" ImageUrl="~/images/hu/trapezgomb/valasz_uzenet_trap.jpg"
                                                                        onmouseover="swapByName(this.id,'valasz_uzenet_trap2.jpg');" onmouseout="swapByName(this.id,'valasz_uzenet_trap.jpg');"
                                                                        OnCommand="imgSendEmailErkeztetve_Command" CausesValidation="false" />
                                                                    <!--CR3058-->
                                                                    <asp:ImageButton ID="ImageButtonVonalkodNyomtatasKuldemeny" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg"
                                                                        onmouseover="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" onmouseout="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" />                                                            </td>
                                                        </tr>
                                                        <%if (KuldemenyAtadasErtesitesVisible)
                                                            { %>
                                                        <tr class="urlapSorBigSize">
                                                            <td class="mrUrlapCaptionBigSize"></td>
                                                            <td class="mrUrlapMezoBigSize"></td>
                                                            <td class="mrUrlapCaption" style="padding-top: 5px">
                                                                <asp:ImageButton ID="imgAtadas" runat="server" ImageUrl="~/images/hu/trapezgomb/atadas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'atadas_trap2.jpg');" onmouseout="swapByName(this.id,'atadas_trap.jpg');" />
                                                            </td>
                                                            <td class="mrUrlapMezo" style="padding-top: 5px">
                                                                <asp:HiddenField ID="hfEmailAddresses" runat="server" />
                                                                <asp:HiddenField ID="hfEmailMessage" runat="server" />
                                                                <span style="padding-right: 0px;">
                                                                    <asp:ImageButton ID="imgEmailErtesites" runat="server" ImageUrl="~/images/hu/trapezgomb/email_ertesites_trap.jpg"
                                                                        onmouseover="swapByName(this.id,'email_ertesites_trap2.jpg');" onmouseout="swapByName(this.id,'email_ertesites_trap.jpg');"
                                                                        OnCommand="imgEmailErtesites_Command" />
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                    </table>
                                                </div>
                                            </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false" CssClass="mrResultPanel">
                                                <div class="mrResultPanelText">
                                                    <asp:Label ID="Label_ResultPanelText" CssClass="mrResultPanelText" runat="server"
                                                        Text="<%$Resources:Form,IktatasResultText %>" />
                                                </div>
                                                <table cellspacing="0" cellpadding="0" width="800" class="mrResultTable">
                                                    <tr class="urlapSorBigSize">
                                                        <td class="mrUrlapCaptionBigSize">
                                                            <asp:Label ID="labelUgyirat" runat="server" Text="Ügyirat iktatószáma:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezoBigSize">
                                                            <asp:Label ID="labelUgyiratFoszam" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapCaption" style="padding-top: 5px">
                                                            <asp:ImageButton ID="imgUgyiratMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                        </td>
                                                        <td class="mrUrlapMezo" style="padding-top: 5px">
                                                            <span style="padding-right: 80px">
                                                                <asp:ImageButton ID="imgUgyiratModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                                <asp:ImageButton ID="imgEloadoiIv" runat="server" ImageUrl="~/images/hu/trapezgomb/eloadoi_iv_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'eloadoi_iv_trap2.jpg');" onmouseout="swapByName(this.id,'eloadoi_iv_trap.jpg');" />
                                                                <!--CR 3058-->
                                                                <asp:ImageButton ID="ImageButtonVonalkodNyomtatasUgyirat" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg"
                                                                    onmouseover="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" onmouseout="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" />
                                                                <!--CR3140-->
                                                                <asp:ImageButton ID="Atadasra_kijelolesUgyirat" runat="server" ImageUrl="~/images/hu/trapezgomb/atadas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'atadas_trap2.jpg');" onmouseout="swapByName(this.id,'atadas_trap.jpg');" />

                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                                                        <td class="mrUrlapCaptionBigSize">
                                                            <asp:Label ID="labelIrat" runat="server" Text="Irat iktatószáma:"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezoBigSize">
                                                            <asp:Label ID="labelIratFoszam" runat="server" Text=""></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapCaption" style="padding-top: 5px">
                                                            <asp:ImageButton ID="imgIratMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                                onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                                                        </td>
                                                        <td class="mrUrlapMezo" style="padding-top: 5px;">
                                                            <span style="padding-right: 80px">
                                                                <asp:ImageButton ID="imgIratModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');" />
                                                                <asp:ImageButton ID="imgIratModositHatosagi" runat="server" ImageUrl="~/images/hu/trapezgomb/hatosagiadatok_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'hatosagiadatok_trap2.jpg');" onmouseout="swapByName(this.id,'hatosagiadatok_trap.jpg');" />
                                                                <asp:ImageButton ID="imgeMailNyomtatas" runat="server" ImageUrl="~/images/hu/trapezgomb/email_nyomtatas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'email_nyomtatas_trap2.jpg');" onmouseout="swapByName(this.id,'email_nyomtatas_trap.jpg');" />
                                                                <!--CR 3058-->
                                                                <asp:ImageButton ID="ImageButtonVonalkodNyomtatasIrat" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_vonalkod_trap2.jpg"
                                                                    onmouseover="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" onmouseout="swapByName(this.id,'nyomtatas_vonalkod_trap2.jpg');" />
                                                                <!--CR3153-->
                                                                <asp:ImageButton ID="Atadasra_kijelolesIrat" runat="server" ImageUrl="~/images/hu/trapezgomb/atadas_trap.jpg"
                                                                    onmouseover="swapByName(this.id,'atadas_trap2.jpg');" onmouseout="swapByName(this.id,'atadas_trap.jpg');" />      
                                                        </td>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr id="tr_resultPanel_UgyiratUjHatarido" runat="server" class="urlapSor" style="padding-bottom: 20px"
                                        visible="false">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelUgyiratUjHatarido" runat="server" Text="Ügyirat új ügyintézési határideje:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:Label ID="labelResultUgyiratUjHatarido" runat="server" />
                                        </td>
                                        <td class="mrUrlapCaption" style="padding-top: 5px"></td>
                                        <td class="mrUrlapMezo" style="padding-top: 5px"></td>
                                    </tr>
                                </table>
                                </eUI:eFormPanel>
                                            <eUI:eFormPanel ID="ResultPanel_KuldemenyElokeszites" runat="server" Visible="false"
                                                CssClass="mrResultPanel">
                                                <div class="mrResultPanelText">
                                                    A küldemény iktatási adatainak rögzítése megtörtént.
                                                    <br />
                                                    A küldemény még nem került iktatásra.
                                                    <br />
                                                    <br />
                                                </div>
                                            </eUI:eFormPanel>
                        </td>
                    </tr>
                    <tr class="urlapSor_kicsi">
                        <td class="mrUrlapCaption_short" valign="top"></td>
                        <td class="mrUrlapMezo">
                            <uc7:TabFooter ID="TabFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            </td>
                    </tr>
                </table>
             
            </asp:Panel>

            <script type="text/javascript">

                // a módosítás után ennek a gombnak a clickjét újra kell hívni
                var btnSaveToClick;

                function qmp_show(bUgyiratModosithato, bIratModosithato) {
                    var rblChoices = document.getElementById('<%=rblChoices.ClientID %>');
                    var radioArray = rblChoices.getElementsByTagName('input');

                    if (radioArray && radioArray.length > 0) {
                        bCheckedUndisplayed = false;
                        for (var i = 0; i < radioArray.length; i++) {
                            if (!bUgyiratModosithato) {
                                if (radioArray[i].value == "UgyiratHataridoToIrat") {
                                    radioArray[i].parentElement.style.display = 'none';
                                    if (radioArray[i].checked) { bCheckedUndisplayed = true; }
                                }
                            }
                            else {
                                if (radioArray[i].value == "UgyiratHataridoToIrat") {
                                    radioArray[i].parentElement.style.display = '';
                                }
                            }

                            if (!bIratModosithato) {
                                if (radioArray[i].value == "IratHataridoToUgyirat") {
                                    radioArray[i].parentElement.style.display = 'none';
                                    if (radioArray[i].checked) { bCheckedUndisplayed = true; }
                                }
                            }
                            else {
                                if (radioArray[i].value == "IratHataridoToUgyirat") {
                                    radioArray[i].parentElement.style.display = '';
                                }
                            }
                        }
                        if (bCheckedUndisplayed) {
                            radioArray[radioArray.length - 1].checked = true;
                        }

                    }


                    $find('<%=mdeQuestion.ClientID %>').show();
                    return false;
                }

                function qmp_onContinue() {
                    var bReturnValue = qmp_setHatarido();
                    $find('<%=mdeQuestion.ClientID %>').hide();
                    if (bReturnValue && btnSaveToClick) {
                        btnSaveToClick.click();
                    }
                    return false;
                }

                function qmp_onCancel() {
                    $find('<%=mdeQuestion.ClientID %>').hide();
                    return false;
                }

                function qmp_setUgyiratHataridoToIrat() {
                    var ugyiratHataridoTextBox = $get('<%=UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID %>');
                    var iratHataridoTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.TextBox.ClientID %>');
                    var iratIntezesiHataridoHourTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID %>');
                    var iratIntezesiHataridoMinuteTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID %>');
                    if (ugyiratHataridoTextBox && iratHataridoTextBox) {
                        ugyiratHataridoTextBox.value = iratHataridoTextBox.value;
                    }
                    return true;
                }

                function qmp_setIratHataridoToUgyirat() {
                    var ugyiratHataridoTextBox = $get('<%=UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID %>');
                    var iratHataridoTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.TextBox.ClientID %>');
                    var iratIntezesiHataridoHourTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID %>');
                    var iratIntezesiHataridoMinuteTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID %>');
                    if (ugyiratHataridoTextBox && iratHataridoTextBox) {
                        iratHataridoTextBox.value = ugyiratHataridoTextBox.value;
                    }
                    if (iratIntezesiHataridoHourTextBox) {
                        iratIntezesiHataridoHourTextBox.value = "00";
                    }
                    if (iratIntezesiHataridoMinuteTextBox) {
                        iratIntezesiHataridoMinuteTextBox.value = "00";
                    }
                    return true;
                }

                function qmp_clearIratHatarido() {
                    var iratHataridoTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.TextBox.ClientID %>');
                    var iratIntezesiHataridoHourTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID %>');
                    var iratIntezesiHataridoMinuteTextBox = $get('<%=IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID %>');
                    if (iratHataridoTextBox) {
                        iratHataridoTextBox.value = "";
                    }
                    if (iratIntezesiHataridoHourTextBox) {
                        iratIntezesiHataridoHourTextBox.value = "00";
                    }
                    if (iratIntezesiHataridoMinuteTextBox) {
                        iratIntezesiHataridoMinuteTextBox.value = "00";
                    }
                    return true;
                }

                function qmp_setHatarido() {
                    var rblChoices = document.getElementById('<%=rblChoices.ClientID %>');
                    var radioArray = rblChoices.getElementsByTagName('input');

                    if (radioArray && radioArray.length > 0) {
                        var selectedExpr = "";
                        for (var i = 0; i < radioArray.length; i++) {
                            if (radioArray[i].checked) {
                                selectedExpr = radioArray[i].value;
                            }
                        }
                        if (selectedExpr == 'IratHataridoToUgyirat') {
                            return qmp_setIratHataridoToUgyirat();
                        }
                        else if (selectedExpr == 'UgyiratHataridoToIrat') {
                            return qmp_setUgyiratHataridoToIrat();
                        }
                        else if (selectedExpr == 'ClearIratHatarido') {
                            return qmp_clearIratHatarido();
                        }
                        else if (selectedExpr == 'BackToForm') {
                            return false;
                        }
                    }
                    return false;
                }

            </script>

            <asp:Panel ID="pnlQuestion" runat="server" CssClass="emp_Panel" Style="display: none">
                <div style="padding: 8px">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <h2 id="header" class="emp_HeaderWrapper">
                                    <asp:Label ID="labelHeader" runat="server" Text="Az ügyirat határideje korábbi, mint az iraté. Mi történjen?"
                                        CssClass="emp_Header"></asp:Label>
                                </h2>
                                <br />
                                <asp:RadioButtonList ID="rblChoices" runat="server">
                                    <asp:ListItem Value="UgyiratHataridoToIrat" Selected="true" Text="Ügyirat ügyintézési határidejének kitolása az irat határidőhöz és mentés" />
                                    <asp:ListItem Value="IratHataridoToUgyirat" Selected="false" Text="Irat határidő igazítása az ügyirat határidőhöz és mentés" />
                                    <asp:ListItem Value="ClearIratHatarido" Selected="false" Text="Irat határidő törlése és mentés" />
                                    <asp:ListItem Value="BackToForm" Selected="false" Text="Visszatérés az űrlaphoz, kézi módosítás" />
                                </asp:RadioButtonList>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="btnContinue" runat="server" CausesValidation="false" ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                    onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" />
                            </td>
                        </tr>
                    </table>
                    <%--        <asp:Button CssClass="emp_OkButton" ID="btnContinue" runat="server" CausesValidation="false" 
            Text="Mentés a fentiek szerint"/> --%>
                    <%--        <asp:Button CssClass="emp_CancelButton" ID="btnCancel" runat="server" CausesValidation="false" 
            Text="Vissza az űrlaphoz"/>--%>
                    <%-- Erre csak azért van szükség, hogy meg tudjunk adni egy TargetControlID-t a ModalPopupExtender-nek --%>
                    <asp:Button ID="btnHidden" runat="server" Style="display: none" CausesValidation="false" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="SzignalasTipusa" runat="server" />
            <asp:HiddenField ID="JavasoltUgyintezoId" runat="server" />
            <asp:HiddenField ID="HataridosFeladatId" runat="server" />
            <ajaxToolkit:ModalPopupExtender ID="mdeQuestion" runat="server" TargetControlID="btnHidden"
                PopupControlID="pnlQuestion" OkControlID="btnContinue" BackgroundCssClass="qmp_modalBackground"
                OnOkScript="qmp_onContinue()" />
            <%--        CancelControlID="btnCancel" OnCancelScript="qmp_onCancel()"--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HiddenField_Svc_FelhasznaloId" runat="server" />
    <asp:HiddenField ID="HiddenField_Svc_FelhasznaloSzervezetId" runat="server" />

    <asp:HiddenField ID="HiddenField_EBeadvanyFeladoNeveCtrlId" runat="server" />
    <asp:HiddenField ID="HiddenField_EBeadvanyFeladoCimeCtrlId" runat="server" />
    <asp:HiddenField ID="HiddenField_UgyIratUgyinditoCtrlId" runat="server" />
    <asp:HiddenField ID="HiddenField_UgyIratUgyinditoCimeCtrlId" runat="server" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript">
        var EBeadvanyFeladoNeveCtrlId = '<%= eBeadvanyokPanel_Nev.ClientID %>';
        var EBeadvanyFeladoCimeCtrlId = '<%= eBeadvanyokPanel_FeladoCim.ClientID %>'
        var UgyIratUgyinditoCtrlId =  '<%= Ugy_PartnerId_Ugyindito_PartnerTextBox.TextBox.ClientID %>'
        var UgyIratUgyinditoCimeCtrlId = '<%= CimekTextBoxUgyindito.TextBox.ClientID %>'

        InitEvents();

        Sys.Application.add_load(function () {
            try {
                //BUG_6042 Ügyintézo másolása autocomplete esetén is (Bejövo irat,Belso keletkezésu irat)
                $('#ctl00_ContentPlaceHolder1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_HiddenField1').on('change', function () {
                    //console.log('ctl00_ContentPlaceHolder1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_HiddenField1 changed to ' + $("#ctl00_ContentPlaceHolder1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_HiddenField1").val());
                    $("#ctl00_ContentPlaceHolder1_Irat_Ugyintezo_FelhasznaloCsoportTextBox_HiddenField1").val($("#ctl00_ContentPlaceHolder1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_HiddenField1").val());
                    $("#ctl00_ContentPlaceHolder1_Irat_Ugyintezo_FelhasznaloCsoportTextBox_CsoportMegnevezes").val($("#ctl00_ContentPlaceHolder1_UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox_CsoportMegnevezes").val());
                });

                //BUG_4366
                $(window).scrollTop(0);
            } catch (e) {
                //console.log(e);
            }

            try {
                $('#ctl00_ContentPlaceHolder1_Email_Bekuldo_PartnerTextBox_PartnerMegnevezes').on('change', function () {
                    //console.log('valtozott a partner');
                    if (typeof setTimeout !== 'undefined')
                        setTimeout(loadPartnerEmails, 2000);
                });

                $('#ctl00_ContentPlaceHolder1_Email_Bekuldo_PartnerTextBox_HiddenField1').on('change', function () {
                    loadPartnerEmails();
                });

            } catch (e) {
                //console.log(e);
            }

             <%if (isTUKRendszer && Mode == ErkeztetesTipus.KuldemenyErkeztetes)
        { %>
            try {

                $('#<%=UgyUgyiratok_CsoportId_Felelos.HiddenField.ClientID%>').on('change', function () {

                    loadFizikaihelyek();
                });

                $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>').on('change', function () {
                    var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');

                            var dVal = dropdown.val();
                            var dText = dropdown.find('option:selected').text();
                            if (dVal != undefined && dText != undefined) {
                                $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                                $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                    }
                    //$('#ctl00_ContentPlaceHolder1_IraIratokTabContainer_TabIratPanel_IratTab1_HiddenFieldFizikaiHely').val(event.target.options[event.target.selectedIndex].value);
                });

                loadFizikaihelyek();

            } catch (e) {
                //console.log(e);
            }
             <%} %>
        });

        function loadFizikaihelyek() {
            var inPartnerId = $('#<%=UgyUgyiratok_CsoportId_Felelos.HiddenField.ClientID%>').val();
            if (inPartnerId === null || inPartnerId === '' || inPartnerId === undefined || inPartnerId === 'undefined') {
                inPartnerId = '1';
            }
            try {
                var fel = $("[id$=HiddenField_Svc_FelhasznaloId]").val();
                var szerv = $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val();
                if (fel != undefined || szerv != undefined) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetFizikaiHelyek") %>',
                        data: JSON.stringify({
                            partnerId: inPartnerId,
                            contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ';' + $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val()
                        }),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            fillFizikaihelyek(data);
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                }
            } catch (e) {
                //console.log(e);
            }
            //}
            //else {
            //    //console.log('partner id nem stimmel');
            //}
        }

        function fillFizikaihelyek(data) {
            //console.log('data');
            var dropdown = $('#<%= IrattariHelyLevelekDropDownTUK.DropDownList.ClientID %>');

            if (dropdown != null) {
                var dVal = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val();
                            //var dText = dropdown.find('option:selected').text();
                            var dText = $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val();
                            dropdown.empty();
                            dropdown.html('<option value="' + '' + '">' + '[Nincs megadva elem]' + '</option>');
                            //<option selected="selected" value="">[Nincs megadva elem]</option>

                            if (data === null || data.d === null) {
                                return;
                            }
                            if (dropdown !== null) {
                                var k = JSON.parse(data.d);

                                var s = '';
                                for (var i = 0; i < k.length; i++) {
                                    s += '<option value="' + k[i].Id + '">' + k[i].Value + '</option>';
                                }
                                dropdown.html(s);

                                if (dVal != undefined && dText != undefined) {
                                    dropdown.val(dVal);
                                    $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyIdHiddenField.ClientID%>').val(dVal);
                            $('#<%= IrattariHelyLevelekDropDownTUK.FizikaiHelyTextHiddenField.ClientID%>').val(dText);
                    }
                    //if (k.length == 1) {
                    //    dropdown.
                    //}

                }
            }
        }

        /**
         *  loadPartnerEmails
        */
        function loadPartnerEmails() {
            var inPartnerId = $('#ctl00_ContentPlaceHolder1_Email_Bekuldo_PartnerTextBox_HiddenField1').val();
            if (inPartnerId !== null && inPartnerId !== '' && inPartnerId !== undefined && inPartnerId !== 'undefined') {

                try {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WrappedWebService/Ajax.asmx/GetPartnerEmail") %>',
                        data: JSON.stringify({
                            partnerId: inPartnerId,
                            contextKey: $("[id$=HiddenField_Svc_FelhasznaloId]").val() + ';' + $("[id$=HiddenField_Svc_FelhasznaloSzervezetId]").val()
                        }),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            fillPartnerEmails(data);
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                } catch (e) {
                    //console.log(e);
                }
            }
            else {
                //console.log('partner id nem stimmel');
            }
        }

        /**
         * fillPartnerEmails
         * @param {string} data - fillPartnerEmails
         */
        function fillPartnerEmails(data) {
            //console.log('data');
            if (data === null || data.d === null) {
                return;
            }
            if ($("#ctl00_ContentPlaceHolder1_Email_KuldCim_CimekTextBox_TextBox1") !== null
                &&
                $("#ctl00_ContentPlaceHolder1_Email_KuldCim_CimekTextBox_TextBox1").val() === '') {

                $("#ctl00_ContentPlaceHolder1_Email_KuldCim_CimekTextBox_TextBox1").val(data.d);
            }
        }
        /**
         * GetControlValue
         * @param controlID
         */
        function GetControlValue(controlID) {
            console.log('GetControlValue');
            console.log(controlID);
            if (controlID !== null && controlID !== '' && controlID !== undefined && controlID !== 'undefined') {
                console.log('GetControlValue not bull');
                return document.getElementById(controlID).value;
            }
            else
                return null;
        }
        /**
         * SetControlValue
         * @param controlID
         * @param newValue
         */
        function SetControlValue(controlID, newValue) {
            try {
                var ctrl = document.getElementById(controlID);
                console.log(ctrl);
                if (checkIsNotNullObject(ctrl))
                    ctrl.value = newValue;
            }
            catch (e) {
                console.log(e);
            }
            finally {
                console.log('SetControlValue.stop');
            }
        }

        function InitEvents() {
            /*
             * currently diasabled js copy
             * */
            /*  InitEventsForEBeadvanyFelado();*/
        }
        function InitEventsForEBeadvanyFelado() {

            $("#" + EBeadvanyFeladoNeveCtrlId).on("change paste keyup", function () {
                copyEBeadvanyFeladoToUgyiratFelado();
            });

            $("#" + EBeadvanyFeladoCimeCtrlId).on("change paste keyup", function () {
                copyEBeadvanyFeladoCimeToUgyiratFeladoCime();
            });
        }
        /**
         * copyEBeadvanyFeladoToUgyiratFelado
         * */
        function copyEBeadvanyFeladoToUgyiratFelado() {
            var EBeadvanyFeladoNeveCtrlValue = GetControlValue(EBeadvanyFeladoNeveCtrlId);
            //var UgyIratUgyinditoCtrlValue = GetControlValue(UgyIratUgyinditoCtrlId);

            SetControlValue(UgyIratUgyinditoCtrlId, EBeadvanyFeladoNeveCtrlValue);
        }
        /**
         * copyEBeadvanyFeladoCimeToUgyiratFeladoCime
         * */
        function copyEBeadvanyFeladoCimeToUgyiratFeladoCime() {

            var EBeadvanyFeladoCimeCtrlValue = GetControlValue(EBeadvanyFeladoCimeCtrlId);
            //var UgyIratUgyinditoCimeCtrlValue = GetControlValue(UgyIratUgyinditoCimeCtrlId);

            SetControlValue(UgyIratUgyinditoCimeCtrlId, EBeadvanyFeladoCimeCtrlValue);
        }
        /**
         * checkIsNotNullObject
         * @param obj
         */
        function checkIsNotNullObject(obj) {
            console.log(obj);
            if (obj !== null && obj !== undefined && obj !== 'undefined') {
                return true;
            }
            return false;
        }
        /**
         * checkIsEmptyValueObject
         * @param obj
         */
        function checkIsEmptyValueObject(obj) {
            console.log(obj);
            if (obj !== null && obj !== undefined && obj !== 'undefined' && obj === '') {
                return true;
            }
            return false;
        }
    </script>

</asp:Content>
