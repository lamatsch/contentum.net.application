﻿/* $Header: EgyszerusitettIktatasForm.aspx.cs, 177, 2017.08.22. 15:59:30, Lamatsch András$ 
 */
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eDocument.Service;
using System.Linq;
using Newtonsoft.Json;

public partial class EgyszerusitettIktatasForm : Contentum.eUtility.UI.PageBase
{
    Contentum.eUtility.PageView pageView = null;

    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    //private const string kcs_IKTATOSZAM_KIEG = "IKTATOSZAM_KIEG";
    //private const string kcs_IRATKATEGORIA = "IRATKATEGORIA";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_KEZELESI_FELJEGYZESEK_TIPUSA = "KEZELESI_FELJEGYZESEK_TIPUSA";
    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_VISSZAVAROLAG = "VISSZAVAROLAG";
    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";
    //bernat.laszlo added
    private const string kcs_KULD_KEZB_MODJA = "KULD_KEZB_MODJA";
    private const string kcs_KULD_CIMZES_TIPUS = "KULD_CIMZES_TIPUS";
    //bernat.laszlo eddig

    private const string kcs_ErkeztetoKonyv = "ERKEZTETOKONYV";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
    private const string kcs_SURGOSSEG = "SURGOSSEG";
    private const string kcs_BoritoTipus = "KULDEMENY_BORITO_TIPUS";
    private const string kcs_IKTATASI_KOTELEZETTSEG = "IKTATASI_KOTELEZETTSEG";
    private const string kcs_IRAT_HATASA_UGYINTEZESRE = "IRAT_HATASA_UGYINTEZESRE";
    // BLG_44
    private const string kcs_UGY_FAJTAJA = "UGY_FAJTAJA";
    // BLG_361
    private const string kcs_ELSODLEGES_ADATHORDOZO = "ELSODLEGES_ADATHORDOZO";

    private const string kcs_UGYIRAT_INTEZESI_IDO = "UGYIRAT_INTEZESI_IDO"; // Iratnál is ezt használjuk

    private const string funkcio_EgyszerusitettIktatas = "EgyszerusitettIktatas";
    private const string funkcio_BejovoIratIktatas = "BejovoIratIktatas";
    private const string funkcio_Erkeztetes = "KuldemenyNew";
    private const string funkcio_BejovoIratIktatasCsakAlszamra = "BejovoIratIktatasCsakAlszamra";
    private const string funkcio_EmailErkeztetes = "EmailErkeztetes";
    private const string funkcio_EmailIktatas = "EmailIktatas";
    private const string funkcio_IktatasElokeszites = "IktatasElokeszites";

    private const string funkcio_eBeadvanyokErkeztetes = "eBeadvanyokErkeztetes";
    private const string funkcio_eBeadvanyokIktatas = "eBeadvanyokIktatas";


    private const string session_CimzettChecked = "CimzettChecked";
    private const string session_CimzettId = "CimzettId";

    private string Command;
    private Boolean _Load = false;
    public bool isTUKRendszer = false;

    #region Email kezelés mód kiválasztása segédosztály: EmailKezelesSelector
    private static class EmailKezelesSelector
    {
        public enum EmailKezelesTipus
        {
            Undefined = -1,
            ErkeztetesIktatas,
            CsakErkeztetes,
            Munkapeldany
        }

        public static EmailKezelesTipus ParseValue(string value)
        {
            if (String.IsNullOrEmpty(value))
                return EmailKezelesTipus.Undefined;

            if (!Enum.IsDefined(typeof(EmailKezelesTipus), value))
                return EmailKezelesTipus.Undefined;

            return (EmailKezelesTipus)Enum.Parse(typeof(EmailKezelesTipus), value);
        }

        public static void FillRadioList(RadioButtonList list, EmailBoritekTipus emailBoritekTipus, ErkeztetesTipus erkeztetesTipus)
        {
            if (list != null)
            {
                list.Items.Clear();

                if (emailBoritekTipus == EmailBoritekTipus.Kuldemeny)
                {
                    list.Items.Add(new ListItem("Iktatás", EmailKezelesTipus.ErkeztetesIktatas.ToString()));
                    list.Items.Add(new ListItem("Munkapéldány létrehozás", EmailKezelesTipus.Munkapeldany.ToString()));
                }
                else
                {
                    list.Items.Add(new ListItem("Érkeztetés és iktatás", EmailKezelesTipus.ErkeztetesIktatas.ToString()));
                    list.Items.Add(new ListItem("Csak érkeztetés", EmailKezelesTipus.CsakErkeztetes.ToString()));
                    if (erkeztetesTipus == ErkeztetesTipus.HivataliKapusErkeztetes)
                    {
                        list.Items.Add(new ListItem("Érkeztetés és fõszám-kezdeményezés", EmailKezelesTipus.Munkapeldany.ToString()));
                    }
                    else
                    {
                        list.Items.Add(new ListItem("Érkeztetés és munkapéldány létrehozás", EmailKezelesTipus.Munkapeldany.ToString()));
                    }
                }

                SelectDefault(list); // default kiválasztás
            }
        }

        public static void FillRadioList(RadioButtonList list, EmailKezelesTipus selectedValue, EmailBoritekTipus emailBoritekTipus, ErkeztetesTipus erkeztetesTipus)
        {
            if (list != null)
            {
                FillRadioList(list, emailBoritekTipus, erkeztetesTipus); // alapfeltöltés
                SetSelectedValue(list, selectedValue); // kiválasztás
            }
        }

        // default kiválasztás: elsõ elem
        public static void SelectDefault(RadioButtonList list)
        {
            if (list != null && list.Items.Count > 0)
            {
                list.Items[0].Selected = true;
            }
        }

        public static void SetSelectedValue(RadioButtonList list, EmailKezelesTipus selectedValue)
        {
            if (list != null)
            {
                ListItem li = list.Items.FindByValue(selectedValue.ToString());
                if (li != null)
                {
                    li.Selected = true;
                }
                else
                {
                    // default kiválasztás
                    SelectDefault(list);
                }
            }
        }

        public static void SetSelectedValue(RadioButtonList list, string selectedValue)
        {
            try
            {
                EmailKezelesTipus emailKezelesTipus = EmailKezelesSelector.ParseValue(selectedValue);
                SetSelectedValue(list, emailKezelesTipus);
            }
            catch
            {
                // default kiválasztás
                SelectDefault(list);
            }
        }

        public static ListItem GetListItemByValue(RadioButtonList list, EmailKezelesTipus selectedValue)
        {
            if (list != null)
            {
                ListItem li = list.Items.FindByValue(selectedValue.ToString());
                if (li != null)
                {
                    return li;
                }
            }

            return null;
        }

        public static EmailKezelesTipus GetSelectedValue(RadioButtonList list)
        {
            if (list != null)
            {
                if (!String.IsNullOrEmpty(list.SelectedValue))
                {
                    return EmailKezelesSelector.ParseValue(list.SelectedValue);
                }
            }

            return EmailKezelesTipus.Undefined;
        }

        public static bool IsSelectedValue(RadioButtonList list, EmailKezelesTipus value)
        {
            if (list != null)
            {
                return (GetSelectedValue(list) == value);
            }

            return false;
        }

        public static string GetSelectedValueString(RadioButtonList list)
        {
            if (list != null)
            {
                list.SelectedValue.ToString();
            }

            return String.Empty;
        }
    }
    #endregion Email kezelés mód kiválasztása segédosztály: EmailKezelesSelector

    private bool bUgyiratHataridoModositasiJog = false;

    protected bool IsRequireOverwriteTemplateDatas()
    {
        if (!IsPostBack && Command == CommandName.New && _Load)
        {
            return true;
        }

        return false;
    }

    private string EmailBoritekokId = "";

    public string EmailBoritekokKuldemenyId
    {
        get { return Kuldemeny_Id_HiddenField.Value; }
        set { Kuldemeny_Id_HiddenField.Value = value; }
    }

    public ErkeztetesTipus Mode = ErkeztetesTipus.KuldemenyErkeztetes;

    public enum ErkeztetesTipus
    {
        KuldemenyErkeztetes,
        EmailErkeztetes,
        HivataliKapusErkeztetes
    }

    private EmailBoritekTipus SubMode = EmailBoritekTipus.EmailBoritek;

    private enum EmailBoritekTipus
    {
        EmailBoritek,
        Kuldemeny
    }

    private IktatasiParameterek iktatasiParameterek = null;

    protected IktatasiParameterek IktatasiParameterek
    {
        get
        {
            if (iktatasiParameterek == null)
                iktatasiParameterek = new IktatasiParameterek();
            return iktatasiParameterek;
        }
        set { iktatasiParameterek = value; }
    }

    string eBeadvanyokId = "";

    bool isHivataliKapusErkeztetes
    {
        get
        {
            return Mode == ErkeztetesTipus.HivataliKapusErkeztetes;
        }
    }

    private bool Uj_Ugyirat_Hatarido_Kezeles
    {
        get
        {
            return Ugyiratok.Uj_Ugyirat_Hatarido_Kezeles(Page);
        }
    }

    private bool trUgyiratIntezesiIdoVisible
    {
        get
        {
            return trUgyiratIntezesiIdoLabel.Visible;
        }
        set
        {
            trUgyiratIntezesiIdoLabel.Visible = value;
            inputUgyUgyintezesiNapok.Visible = value;
            IntezesiIdoegyseg_KodtarakDropDownList.Visible = value;
        }
    }

    #region page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Funkciójog ellenõrzés: Page_Init-be áttéve    
    }

    private IratForm _iratForm;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);
        isTUKRendszer = Rendszerparameterek.IsTUK(Page);
        _iratForm = new IratForm(this, pageView)
        {
            CalendarControl_UgyintezesKezdete = CalendarControl_UgyintezesKezdete,
            Label_CalendarControl_UgyintezesKezdete = Label_CalendarControl_UgyintezesKezdete
        };
        InitSakkoraDateControls();
        #region ORG függõ megjelenítés
        // CR 3054
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            labelBoritoTipus.Text = "Vonalkód helye:";
        }

        #endregion

        Command = Request.QueryString.Get(QueryStringVars.Command);
        EmailBoritekokId = Request.QueryString.Get(QueryStringVars.EmailBoritekokId);
        eBeadvanyokId = Request.QueryString.Get("eBeadvanyokId");
        EmailBoritekokKuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);

        if (!String.IsNullOrEmpty(eBeadvanyokId))
        {
            Mode = ErkeztetesTipus.HivataliKapusErkeztetes;

            if (!String.IsNullOrEmpty(EmailBoritekokKuldemenyId))
            {
                SubMode = EmailBoritekTipus.Kuldemeny;
            }
        }
        else
        if (!String.IsNullOrEmpty(EmailBoritekokId))
        {
            Mode = ErkeztetesTipus.EmailErkeztetes;

            Label181.Visible = false;
            Label182.Visible = false;
            CimzesTipusa_DropDownList.Visible = false;

            if (!String.IsNullOrEmpty(EmailBoritekokKuldemenyId))
            {
                SubMode = EmailBoritekTipus.Kuldemeny;
            }
        }
        else
        {
            Mode = ErkeztetesTipus.KuldemenyErkeztetes;
        }

        #region Funkciójog ellenõrzés
        if (Command == CommandName.DesignView)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
        }
        else
        {
            if (Mode == ErkeztetesTipus.HivataliKapusErkeztetes)
            {
                if (SubMode == EmailBoritekTipus.Kuldemeny)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_eBeadvanyokIktatas);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_eBeadvanyokErkeztetes);
                }
            }
            else
            if (Mode == ErkeztetesTipus.EmailErkeztetes)
            {
                if (SubMode == EmailBoritekTipus.Kuldemeny)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_EmailIktatas);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_EmailErkeztetes);
                }
            }
            else
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcio_Erkeztetes);
            }
        }
        #endregion

        // alszámra iktatásnál jogosultság az ügyirat határidõ módosítására
        bUgyiratHataridoModositasiJog = FunctionRights.GetFunkcioJog(Page, "UgyiratModify");


        TabFooter1.ButtonsClick += new CommandEventHandler(TabFooterButtonsClick);

        ImageButton_KuldemenyMegtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "KuldKuldemenyekForm.aspx", "", Kuldemeny_Id_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        #region Küldemény módosítás gomb

        string javascript = "";

        javascript = " var hidden = getElementById('" + Kuldemeny_Id_HiddenField.ClientID + "'); "
            + " if (hidden.value!=null && hidden.value!='') "
            + " { if (popup('KuldKuldemenyekForm.aspx?" + QueryStringVars.Id + "='+hidden.value"
            + "+'&" + CommandName.Command + "=" + CommandName.Modify + "', " + Defaults.PopupWidth_Max.ToString()
            + ", " + Defaults.PopupHeight_Max
            + " )){__doPostBack('" + ImageButton_KuldemenyModositas.ClientID + "','" + EventArgumentConst.refreshKuldemenyekPanel
            + "');} } else { alert('" + Contentum.eUtility.Resources.Error.GetString("UINoSelectedItem") + "'); } "
            + " return false;";
        ImageButton_KuldemenyModositas.OnClientClick = javascript;

        #endregion


        ImageButton_Elozmeny.OnClientClick = GetElozmenyKeresesOnClientClick();

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
           "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_MigraltKereses.OnClientClick = GetElozmenyKeresesOnClientClick();

        #region Cascading DropDown
        // Contextkey-ben megadjuk a felhasználó id-t
        CascadingDropDown_AgazatiJel.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_Ugykor.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_Ugytipus.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);
        CascadingDropDown_IktatoKonyv.ContextKey = FelhasznaloProfil.FelhasznaloId(Page);

        #region BLG_232
        //LZS - BLG_335 - korábban ez ki volt kommentezve
        //#region CR956 fix, ügyirat tárgy ne másolódjon át irathoz
        // BUG_13049
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
        {
            UgyUgyirat_Targy_RequiredTextBox.TextBox.Attributes["onblur"] +=
           @" var iratTargyObj = $get('" + IraIrat_Targy_RequiredTextBox.TextBox.ClientID + @"');
                        if (iratTargyObj.value == '') 
                        {                  
                            iratTargyObj.value = this.value;
                        }  ";
        }   
        //#endregion
        #endregion

        if (Command == CommandName.New)
        {
            CascadingDropDown_AgazatiJel.Enabled = true;
            CascadingDropDown_Ugykor.Enabled = true;
            CascadingDropDown_Ugytipus.Enabled = true;

            CascadingDropDown_IktatoKonyv.Enabled = true;
        }

        Iktatokonyvek_DropDownLis_Ajax.Visible = true;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;

        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
        {
            RequiredFieldValidator2.Enabled = true;
            // BUG_1499
            //CascadingDropDown_IktatoKonyv.ParentControlID = "Ugykor_DropDownList";
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;
            }
            else
                CascadingDropDown_IktatoKonyv.ParentControlID = "Ugykor_DropDownList";

        }
        else
        {
            // BUG_1499
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                if (CascadingDropDown_Ugykor.SelectedValue == String.Empty)
                {
                    CascadingDropDown_IktatoKonyv.ParentControlID = "";
                }
                else CascadingDropDown_IktatoKonyv.ParentControlID = "Ugykor_DropDownList";
            }
            else
            {
                if (CascadingDropDown_Ugykor.SelectedValue == String.Empty)
                {
                    CascadingDropDown_IktatoKonyv.ParentControlID = "";
                }
                else CascadingDropDown_IktatoKonyv.ParentControlID = "Ugykor_DropDownList";
            }
            RequiredFieldValidator2.Enabled = false;
        }
        #endregion

        SetIrattariTetelUI();

        //FormHeader1.TemplateObjectType = typeof(EREC_UgyUgyiratok);
        FormHeader1.TemplateObjectType = typeof(IktatasFormTemplateObject);
        FormHeader1.FormTemplateLoader1_Visibility = true;


        //Partner és cím összekötése
        Bekuldo_PartnerTextBox.CimTextBox = Kuld_CimId_CimekTextBox.TextBox;
        Bekuldo_PartnerTextBox.CimHiddenField = Kuld_CimId_CimekTextBox.HiddenField;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimTextBox = CimekTextBoxUgyindito.TextBox;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.CimHiddenField = CimekTextBoxUgyindito.HiddenField;

        // CR3113 Ügyindító nevét, címét automatikusan töltse a küldõ/feladó alapján
        // Ahhoz hogy az ügyindító címét is automatikusan töltsük, kell a címmezõ is, de nincs megjelenítve
        Email_Bekuldo_PartnerTextBox.CimTextBox = Email_CimekTextBox.TextBox;
        Email_Bekuldo_PartnerTextBox.CimHiddenField = Email_CimekTextBox.HiddenField;

        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            EmailKezelesSelector.FillRadioList(ErkeztetesVagyIktatasRadioButtonList
                , (SubMode == EmailBoritekTipus.Kuldemeny) ? EmailBoritekTipus.Kuldemeny : EmailBoritekTipus.EmailBoritek, Mode);

            Email_IktatasiKotelezettsegKodtarakDropDownList.DropDownList.AutoPostBack = true;
            Email_IktatasiKotelezettsegKodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(EmailIktatasiKotelezettseg_SelectedIndexChanged);

            ErkeztetesVagyIktatasRadioButtonList.Visible = true;
            ErkeztetesVagyIktatasRadioButtonList.SelectedIndexChanged += new EventHandler(ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged);
        }

        if (Mode == ErkeztetesTipus.HivataliKapusErkeztetes)
        {
            EmailKezelesSelector.FillRadioList(ErkeztetesVagyIktatasRadioButtonList
                , (SubMode == EmailBoritekTipus.Kuldemeny) ? EmailBoritekTipus.Kuldemeny : EmailBoritekTipus.EmailBoritek, Mode);

            eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.DropDownList.AutoPostBack = true;
            eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(EmailIktatasiKotelezettseg_SelectedIndexChanged);


            ErkeztetesVagyIktatasRadioButtonList.Visible = true;
            ErkeztetesVagyIktatasRadioButtonList.SelectedIndexChanged += new EventHandler(ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged);
        }

        //DropDownList ListSearchExtender konfigurálása
        AgazatiJelek_DropDownList.LSEX_RaiseImmediateOnChange = true;
        Ugykor_DropDownList.LSEX_RaiseImmediateOnChange = true;

        // Irat határidõ idõpontja is látható
        IraIrat_Hatarido_CalendarControl.TimeVisible = true;

        RegisterJavascripts();

        FeljegyzesPanel.ErrorPanel = EErrorPanel1;
        FeljegyzesPanel.ErrorUpdatePanel = ErrorUpdatePanel1;

        FeljegyzesPanel.ValidateIfFeladatIsNotEmpty = true;

        //CR3058
        // BLG_1392
        //if (!Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
        //{
        //    Label_Pld_IratTipusCsillag.Visible = false;
        //    IraIrat_Irattipus_KodtarakDropDownList.Validate = false;
        //    starUpdate.Update();
        //    Label_UgyintezoCsillag.Visible = false;
        //    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = false;
        //    StarUpdate1.Update();
        //}
        //else
        //{
        //    Label_Pld_IratTipusCsillag.Visible = true;
        //    IraIrat_Irattipus_KodtarakDropDownList.Validate = true;
        //    starUpdate.Update();
        //    Label_UgyintezoCsillag.Visible = true;
        //    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = true;
        //    StarUpdate1.Update();
        //}
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_KOTELEZO_IRATTIPUS, false))
        {
            Label_Pld_IratTipusCsillag.Visible = true;
            IraIrat_Irattipus_KodtarakDropDownList.Validate = true;
            starUpdate.Update();
        }
        else
        {
            Label_Pld_IratTipusCsillag.Visible = false;
            IraIrat_Irattipus_KodtarakDropDownList.Validate = false;
            starUpdate.Update();
        }
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_KOTELEZO_IRATUGYINTEZO, false))
        {
            Label_UgyintezoCsillag.Visible = true;
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = true;
            StarUpdate1.Update();
        }
        else
        {
            Label_UgyintezoCsillag.Visible = false;
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.Validate = false;
            StarUpdate1.Update();
        }


        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
        {
            //VonalkodTextBoxVonalkod.Validate = true;
            VonalkodTextBoxVonalkod.ReadOnly = true;
            VonalkodTextBoxVonalkod.RequiredValidate = false;
            starUpdate.Update();
        }

        //LZS - BUG_6309
        if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.UGYFEL_ADATOK_KOTELEZOSEG) == 1)
        {
            lblReqUgyindito.Visible =
            lblReqUgyinditoCime.Visible =
            Ugy_PartnerId_Ugyindito_PartnerTextBox.Validate =
            CimekTextBoxUgyindito.Validate = true;
        }

        // BUG_7678
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.PAGE_FELADASIIDO_KOTELEZO, false))
        {
            if ((Mode == ErkeztetesTipus.EmailErkeztetes) && (SubMode != EmailBoritekTipus.Kuldemeny))
            {
                Label_EmailFeladasiIdoRequired.Visible = true;
                Email_FeladasiIdo_CalendarControl.Validate = true;

                Label_eBeadvanyokFeladasiIdoRequired.Visible = false;
                eBeadvanyokPanel_FeladasiIdo.Validate = false;


                Label_FeladasiIdoRequired.Visible = false;
                FeladasiIdo_CalendarControl.Validate = false;

            }
            else if ((Mode == ErkeztetesTipus.HivataliKapusErkeztetes) && (SubMode != EmailBoritekTipus.Kuldemeny))
            {
                Label_eBeadvanyokFeladasiIdoRequired.Visible = true;
                eBeadvanyokPanel_FeladasiIdo.Validate = true;

                Label_EmailFeladasiIdoRequired.Visible = false;
                Email_FeladasiIdo_CalendarControl.Validate = false;

                Label_FeladasiIdoRequired.Visible = false;
                FeladasiIdo_CalendarControl.Validate = false;
            }
            else
            {
                Label_eBeadvanyokFeladasiIdoRequired.Visible = false;
                eBeadvanyokPanel_FeladasiIdo.Validate = false;

                Label_EmailFeladasiIdoRequired.Visible = false;
                Email_FeladasiIdo_CalendarControl.Validate = false;

                Label_FeladasiIdoRequired.Visible = true;
                FeladasiIdo_CalendarControl.Validate = true;
            }
        }
        else
        {
            Label_eBeadvanyokFeladasiIdoRequired.Visible = false;
            eBeadvanyokPanel_FeladasiIdo.Validate = false;

            Label_EmailFeladasiIdoRequired.Visible = false;
            Email_FeladasiIdo_CalendarControl.Validate = false;

            Label_FeladasiIdoRequired.Visible = false;
            FeladasiIdo_CalendarControl.Validate = false;
        }
        #region CR3083 iktatáskor van egy olyan mezõ, hogy "ügyfél, ügyindító", innen kerüljön le az ügyindító, csak "ügyfél" maradjon
        // és CR3082 iktatáskor van egy olyan mezõ, hogy irat jellege ez a mezõ nem szükséges, eltüntethetõ a felületrõl
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            lblUgyindito.Text = "Ügyfél:";
            labelIratJelleg.Visible = false;
            IratJellegKodtarakDropDown.Validate = false;
            IratJellegKodtarakDropDown.Enabled = false;
            IratJellegKodtarakDropDown.Visible = false;
        }
        #endregion

        BejovoPeldanyListPanel1.ErrorPanel = EErrorPanel1;
        KodtarakDropDownList_EljarasiSzakaszFok.FillDropDownList(KodTarak.ELJARASI_SZAKASZ_FOK.KCS, true, EErrorPanel1);
        // BUG_3435
        //LZS - BUG_8854
        /* 
         * if (!Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
         *   KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue = KodTarak.ELJARASI_SZAKASZ_FOK.I;
         */

        /*ctl00_ContentPlaceHolder1_Email_Bekuldo_PartnerTextBox_HiddenField1*/
        Email_KuldCim_CimekTextBox.Hiddenfield_ExternalPartnerControl_Value = Email_Bekuldo_PartnerTextBox.Control_HiddenField.ClientID;

        InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));

        // BUG_5197 (nekrisz)
        Bekuldo_PartnerTextBox.TextBox.AutoPostBack = true;

        IktatoKonyvekDropDownList_Search.ValuesFilledWithId = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

    }

    private void SetIrattariTetelUI()
    {
        // BLG_44
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {
            Label_agazatiJel.Visible = true;
            AgazatiJelek_DropDownList.Visible = true;
            labelIrattariTetelszam.Visible = true;
            Ugykor_DropDownList.Visible = true;
            tr_ugytipus.Visible = true;
            labelUgyFajtaja.Visible = false;
            UGY_FAJTAJA_KodtarakDropDownList.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
            //label_IrattariTetelszam_DropDownList.Visible = false;
            IrattariTetelszam_DropDownList.Visible = false;
        }
        else
        {
            Label_agazatiJel.Visible = false;
            AgazatiJelek_DropDownList.Visible = false;
            labelIrattariTetelszam.Visible = true;
            Ugykor_DropDownList.Visible = false;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
            tr_ugytipus.Visible = false;
            labelUgyFajtaja.Visible = true;
            UGY_FAJTAJA_KodtarakDropDownList.Visible = true;
            UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = false;
            //label_IrattariTetelszam_DropDownList.Visible = true;
            IrattariTetelszam_DropDownList.Visible = true;
            IrattariTetelszam_DropDownList.ReadOnly = false;

            if (IrattariTetelszam_DropDownList.SelectedValue == String.Empty)
            {
                CascadingDropDown_IktatoKonyv.ParentControlID = "";
            }
            //BUG_1499
            //else CascadingDropDown_IktatoKonyv.ParentControlID = "IrattariTetelszam_DropDownList";
            else CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;


            RequiredFieldValidator2.Enabled = false;
        }
    }
    // ezzel a külsõ hívással biztosítható, hogy csak egy metódusban végrehajtott mûveletek
    // lefutása után hívódjon meg 
    private void RegisterStartupScript(string key, string script)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), key, script, true);
    }

    // ha az egyik határidõ textbox tartalmát kézzel módosítják, a tooltip ennek megfelelõre vált,
    // a másik határidõ textbox tooltipje pedig törlõdik
    //LZS - BLG_335 - Új isClone paraméter hozzáadva, a határidő másolás funkció miatt.
    private string HataridoTextBoxManualSetJavaScript(TextBox changedHataridoTextBox, TextBox alternateHataridoTextBox, bool isCloneable)
    {
        string js = @"var txt_changedhatarido = $get('" + changedHataridoTextBox.ClientID + @"');
                    if (txt_changedhatarido)
                    {
                        txt_changedhatarido.title = '" + Resources.Form.UI_IntezesiIdo_ManualSet_ToolTip + @"';
                    }
                    var txt_alternatehatarido = $get('" + alternateHataridoTextBox.ClientID + @"');
                    if (txt_alternatehatarido)
                    {
                        txt_alternatehatarido.title = '';
                    }
                    ";

        //LZS - BLG_335
        //Határidő másolása a klón objektumba
        if (isCloneable)
        {
            if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
            {
                //Dátum
                js += @"var txt_ugyirathatarido = $get('" + changedHataridoTextBox.ClientID + @"');
                        var txt_irathatarido = $get('" + alternateHataridoTextBox.ClientID + @"');
                    
                        txt_irathatarido.value = txt_ugyirathatarido.value;
                      ";
            }
        }

        return js;
    }

    private void RegisterJavascripts()
    {
        string ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
        string mode = Request.QueryString.Get(QueryStringVars.Mode);
        // Bejövõ és belsõ iktatásnál:
        if (Command == CommandName.New)
        {
            RegisterJavascriptsForWSCall(CommandName.BejovoIratIktatas);
        }
        if (Command == CommandName.New)
        {
            #region GombCsere

            string jsOnEvKeyUp = @"function OnEvKeyUp(ev)
            {
               var k= null;
               if(ev) var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
               if (k == null || k != Sys.UI.Key.enter)
               {
                   var textBoxEv = $get('" + evIktatokonyvSearch.EvTol_TextBox.ClientID + @"');
                   if(textBoxEv)
                    {
                       var evValue = textBoxEv.value;
                       var elozmenyVisible = true;
                       if(evValue != '')
                       {
                          evValue = parseInt(evValue);
                          if(!isNaN(evValue) && evValue < " + MigralasEve + @")
                          {
                             elozmenyVisible = false;
                          }
                           var imageElozmeny = $get('" + ImageButton_Elozmeny.ClientID + @"');
                           var imageMigralt = $get('" + ImageButton_MigraltKereses.ClientID + @"');
                           if(imageElozmeny && imageMigralt)
                           {
                               if(elozmenyVisible)
                               {
                                   imageElozmeny.style.display = '';
                                   imageMigralt.style.display = 'none';
                               }
                               else
                               {
                                   imageElozmeny.style.display = 'none';
                                   imageMigralt.style.display = '';
                               }
                           }  
                       }
                       else
                       {
                           var imageElozmeny = $get('" + ImageButton_Elozmeny.ClientID + @"');
                           var imageMigralt = $get('" + ImageButton_MigraltKereses.ClientID + @"');
                           if(imageElozmeny && imageMigralt)
                           {
                               imageElozmeny.style.display = '';
                               imageMigralt.style.display = '';
                           } 
                       }
                   }
               }     
             };OnEvKeyUp(null);";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnEvKeyUp", jsOnEvKeyUp, true);

            evIktatokonyvSearch.EvTol_TextBox.Attributes.Add("onkeyup", "OnEvKeyUp(event)");

            #endregion

            #region Enter figyelese

            string jsOnElozmenyKeyDown = @"function OnElozmenyKeyDown(ev)
            {
                var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
                if (k === Sys.UI.Key.enter)
                {
                    " + ImageButton_Elozmeny.OnClientClick + @"
                }
            return false;
            }";

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "OnElozmenyKeyDown", jsOnElozmenyKeyDown, true);

            numberFoszamSearch.TextBox.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            evIktatokonyvSearch.EvTol_TextBox.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            IktatoKonyvekDropDownList_Search.DropDownList.Attributes.Add("onkeydown", "OnElozmenyKeyDown(event)");

            #endregion
        }

        #region intézési határidõk


        //irat határidõ textbox változás figyelés
        IraIrat_Hatarido_CalendarControl.TextBox.Attributes.Add("onchange"
            , HataridoTextBoxManualSetJavaScript(IraIrat_Hatarido_CalendarControl.TextBox, UgyUgyirat_Hatarido_CalendarControl.TextBox, false));

        //LZS - BLG_335 - Másolandó (clone) textbox-ok átadása az ügyirat határidő calendar controlnak. Óra, perc külön.
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
        {
            //ügyirat határidõ textbox változás figyelés
            UgyUgyirat_Hatarido_CalendarControl.TextBox.Attributes.Add("onchange"
                , HataridoTextBoxManualSetJavaScript(UgyUgyirat_Hatarido_CalendarControl.TextBox, IraIrat_Hatarido_CalendarControl.TextBox, true));

            //LZS - BLG_335 - Óra textbox figyelés
            UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.Attributes.Add("onchange"
                , HataridoTextBoxManualSetJavaScript(UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox, IraIrat_Hatarido_CalendarControl.GetHourTextBox, true));

            //LZS - BLG_335 - Perc textbox figyelés
            UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.Attributes.Add("onchange"
                , HataridoTextBoxManualSetJavaScript(UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox, IraIrat_Hatarido_CalendarControl.GetMinuteTextBox, true));

            UgyUgyirat_Hatarido_CalendarControl.CloneHourTextId = IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID;
            UgyUgyirat_Hatarido_CalendarControl.CloneMinuteTextId = IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID;
        }
        #endregion intézési határidõk
    }

    // szétválasztjuk a bejövõ (kell szignálási jegyzék) és a belsõ (csak intézési határidõ kell) iktatást
    // (megjegyzés: egyszerûsített iktatásnál csak bejövõ lehet)
    private void RegisterJavascriptsForWSCall(String Mode)
    {

        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm == null)
        {
            throw new Exception("ScriptManager not found.");
        }

        sm.Services.Add(new ServiceReference("~/WrappedWebService/Ajax_eRecord.asmx"));

        ScriptReference sr = new ScriptReference();
        sr.Path = "~/JavaScripts/WsCalls.js?t=20190918";
        sm.Scripts.Add(sr);

        // var eSetTextBoxesByUgykorUgytipusType = { All:0, OnlySzignalas:1, OnlyIntezesiIdo:2 };
        string eSetTextBoxesByUgykorUgytipusType = "eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo";
        switch (Mode)
        {
            case CommandName.BejovoIratIktatas:
            case CommandName.AtIktatas:
                eSetTextBoxesByUgykorUgytipusType = "eSetTextBoxesByUgykorUgytipusType.All";
                break;
        }

        // WorkAround: irattípus változásnál nem töltjük a szignálási jegyzék szerinti felelõst,
        // mert akkor a már szignálás alapjén kitöltött és írásvédett értékek is felülíródnak a képernyõn
        string eSetTextBoxesByUgykorUgytipusIrattipusChangedType = "eSetTextBoxesByUgykorUgytipusType.OnlyIntezesiIdo";

        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            eSetTextBoxesByUgykorUgytipusType = "eSetTextBoxesByUgykorUgytipusType.None";
        }

        string js_variables = @"
                    
                    var userId = '" + FelhasznaloProfil.FelhasznaloId(Page) + @"';
                    var userCsoportId = '" + FelhasznaloProfil.FelhasznaloSzerverzetId(Page) + @"';
                    var UgykorDropDown_Id = '" + Ugykor_DropDownList.ClientID + @"'; 
                    var UgytipusDropDown_Id = '" + UgyUgyirat_Ugytipus_DropDownList.ClientID + @"';                    
                    var Ugyfelelos_TextBox_Id = '" + Ugyfelelos_CsoportTextBox.TextBox.ClientID + @"';
                    var Ugyfelelos_HiddenField_Id = '" + Ugyfelelos_CsoportTextBox.HiddenField.ClientID + @"';
                    var Ugyintezo_TextBox_Id = '" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.TextBox.ClientID + @"';
                    var Ugyintezo_HiddenField_Id = '" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.HiddenField.ClientID + @"';
                    var Kezelo_TextBox_Id = '" + UgyUgyiratok_CsoportId_Felelos.TextBox.ClientID + @"';
                    var Kezelo_HiddenField_Id = '" + UgyUgyiratok_CsoportId_Felelos.HiddenField.ClientID + @"';
                    var UgyTargy_TextBox_Id = '" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_TextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var UgyUgyiratHatarido_HourTextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_MinuteTextBox_Id = '" + UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var UgyUgyiratHatarido_CalendarImage_Id = '" + UgyUgyirat_Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IrattipusDropDown_Id = '" + IraIrat_Irattipus_KodtarakDropDownList.DropDownList.ClientID + @"';
                    var IraIratHatarido_TextBox_Id = '" + IraIrat_Hatarido_CalendarControl.TextBox.ClientID + @"';
                    var IraIratHatarido_HourTextBox_Id = '" + IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"';
                    var IraIratHatarido_MinuteTextBox_Id = '" + IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"';
                    var IraIratHatarido_CalendarImage_Id = '" + IraIrat_Hatarido_CalendarControl.CalendarImage.ClientID + @"';
                    var IraIratHatarido_ArrowUpImage_Id = '" + IraIrat_Hatarido_CalendarControl.ArrowUpImage.ClientID + @"';
                    var IraIratHatarido_ArrowDownImage_Id = '" + IraIrat_Hatarido_CalendarControl.ArrowDownImage.ClientID + @"';
                    var EljarasiSzakaszHiddenField_Id = '" + UgyiratdarabEljarasiSzakasz_HiddenField.ClientID + @"';
                    var UgyiratHataridoKitolas_Ugyirat_HiddenField_Id = '" + UgyiratHataridoKitolas_Ugyirat_HiddenField.ClientID + @"';
                    var UgyiratHataridoKitolas_Irat_HiddenField_Id = '" + UgyiratHataridoKitolas_Irat_HiddenField.ClientID + @"';
                    var IktatasDatumUgyirat_HiddenFieldId = '" + IktatasDatuma_Ugyirat_HiddenField.ClientID + @"';
                    // BLG_1020
                    var Irat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + BeerkezesIdeje_CalendarControl.TextBox.ClientID + @"';
                    var IktatasDatumIrat_HiddenFieldId = '" + IktatasDatuma_Irat_HiddenField.ClientID + @"';
                     // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                    var IktatokonyvDropDown_Id = '" + Iktatokonyvek_DropDownLis_Ajax.ClientID + @"';
                    // BLG_44
                    //var IrattariTetelszamDropDown_Id = '" + IrattariTetelszam_DropDownList.DropDownList.ClientID + @"';
                    var Ugyirat_Ugyintezes_Kezdete_Datum_HiddenfieldId = '" + BeerkezesIdeje_CalendarControl.TextBox.ClientID + @"';
                    var IntezesiIdo_DropDownList_Id = '" + inputUgyUgyintezesiNapok.ClientID + @"';
                    var IntezesiIdoegyseg_DropDownList_Id = '" + IntezesiIdoegyseg_KodtarakDropDownList.DropDownList.ClientID + @"';
                    // BLG_1020
                    var IratIntezesiIdo_DropDownList_Id = '" + inputUgyintezesiNapok.ClientID + @"';
                    var IratIntezesiIdoegyseg_DropDownList_Id = '" + IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.ClientID + @"';
                    var inputUgyintezesiNapok_Id = '" + inputUgyintezesiNapok.ClientID + @"';
                    var inputUgyUgyintezesiNapok_Id = '" + inputUgyUgyintezesiNapok.ClientID + @"';
                    var UgyintezesKezdete_TextBox_Id = '" + CalendarControl_UgyintezesKezdete.TextBox.ClientID + @"';
                    var UgyintezesKezdete_HourTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetHourTextBox.ClientID + @"';
                    var UgyintezesKezdete_MinuteTextBox_Id = '" + CalendarControl_UgyintezesKezdete.GetMinuteTextBox.ClientID + @"';
                    var UgyintezesModja_DropDownList_Id = '" + UgyintezesModja_DropDownList.DropDownList.ClientID + "';";

        js_variables += @"var Merge_IrattariTetelszamTextBox_Id = '" + Merge_IrattariTetelszamTextBox.ClientID + @"';";

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "FelelosCsoportVariables", js_variables, true);
        string js_AddHandler = "";
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {
            // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
            js_AddHandler += @"
        function UgyTipusChangeHandler()
            {
                //SetTextBoxesFromSzignalasiJegyzek();
                SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusType + @");
            }    
        ";
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
            {
                js_AddHandler += @"
            function UgykorChangeHandler()
            {
                SetMerge_IrattariTetelszamTextBox();
                //SetTextBoxesFromSzignalasiJegyzek();
                SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusType + @");
            }";
            }
            else
            {
                js_AddHandler += @"
            function UgykorChangeHandler()
            {
                SetMerge_IrattariTetelszamTextBox();
                //SetTextBoxesFromSzignalasiJegyzek();
                SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusType + @");

                // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                var UgykorDropDown = $get(UgykorDropDown_Id);
                if (UgykorDropDown)
                {
                    if (UgykorDropDown.selectedIndex > 0)
                    {
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior.set_ParentControlID(UgykorDropDown_Id);
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior._onParentChange(null, true);
                    }
                    else
                    {
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior.set_ParentControlID('');
                        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior._onParentChange(null, true);
                    }
                }
            }
            ";
            }
        }
        else
        {
            js_AddHandler += @"
            function ItszChangeHandler()
            {
                //SetMerge_IrattariTetelszamTextBox();
                //var IrattariTetelekDropDown = $get(IrattariTetelekDropdown_Id);
                //if (IrattariTetelekDropDown)
                //{
                //    if (IrattariTetelekDropDown.selectedIndex > 0)
                //    {
                //        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior.set_ParentControlID(IrattariTetelekDropdown_Id);
                //        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior._onParentChange(null, true);
                //    }
                //    else
                //    {
                //        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior.set_ParentControlID('');
                //        $get(IktatokonyvDropDown_Id).CascadingDropDownBehavior._onParentChange(null, true);
                //    }
                //}

            }";


        }
        js_AddHandler += @"
        function IrattipusOnChange()
            {
                var elozmenyHiddenField = $get('" + ElozmenyUgyiratID_HiddenField.ClientID + @"');
                if (!elozmenyHiddenField || elozmenyHiddenField.value == '')
                {
                    SetTextBoxesByIratMetaDefinicio(" + eSetTextBoxesByUgykorUgytipusIrattipusChangedType + @");
                }
                else
                {
                    SetTextBoxesIrattipusByUgyirat(elozmenyHiddenField.value);
                }
            }

        function callCreateItemToolTips(sender, args)
            {
                var dropdown = sender.get_element();

                if (dropdown)
                {
                    createItemToolTips(dropdown);
                }
            }        

         function pageLoad(){//be called automatically.
            
            var cascadingDropDown_ugytipus = $find('" + CascadingDropDown_Ugytipus.ClientID + @"');
            if (cascadingDropDown_ugytipus)
            {
                cascadingDropDown_ugytipus.add_selectionChanged(UgyTipusChangeHandler);
                //cascadingDropDown_ugytipus.add_populated(createItemToolTips);
                cascadingDropDown_ugytipus.add_populated(callCreateItemToolTips);
            }
            var cascadingDropDown_ugykor = $find('" + CascadingDropDown_Ugykor.ClientID + @"');
            if (cascadingDropDown_ugykor)
            {
                cascadingDropDown_ugykor.add_selectionChanged(UgykorChangeHandler);
                cascadingDropDown_ugykor.add_populated(callCreateItemToolTips);   
            }

            var cascadingDropDown_agazatijel = $find('" + CascadingDropDown_AgazatiJel.ClientID + @"');
            if (cascadingDropDown_agazatijel)
            {
                cascadingDropDown_agazatijel.add_populated(callCreateItemToolTips);   
            }

            var cascadingDropDown_iktatokonyv = $find('" + CascadingDropDown_IktatoKonyv.ClientID + @"');
            if (cascadingDropDown_iktatokonyv)
            {
                cascadingDropDown_iktatokonyv.add_populated(callCreateItemToolTips);   
            }

            //var dropDown_irattaritetelszam = $find('" + IrattariTetelszam_DropDownList.DropDownList.ClientID + @"');
            //if (dropDown_irattaritetelszam)
            //{
            //    dropDown_irattaritetelszam.add_selectionChanged(ItszChangeHandler);
            //}
         }
        
        var migraltUgyirat = document.getElementById('" + migraltUgyiratAzonositoMezo.ClientID + @"');
        if (migraltUgyirat)
        {
            migraltUgyirat.style.display = 'none';
        }

        ";

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "cascadingDropDown_handler", js_AddHandler, true);

        if (!Uj_Ugyirat_Hatarido_Kezeles)
        {
            IraIrat_Irattipus_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "IrattipusOnChange()");
        }

        // ágazati jel váltásakor irattári tételszám törlése
        string jsClearIrattariTetelszam = @"var Merge_IrattariTetelszamTextBox = $get('" + Merge_IrattariTetelszamTextBox.ClientID + @"');
if (Merge_IrattariTetelszamTextBox)
{
    Merge_IrattariTetelszamTextBox.value = '';
}
";
        AgazatiJelek_DropDownList.Attributes.Add("onchange", jsClearIrattariTetelszam);

        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            inputUgyintezesiNapok.Attributes.Add("onchange", "SetIratIntezesiIdoManualv2()");
            trIratIntezesiIdo.Visible = true;

            //IntezesiIdo_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIntezesiIdoManual()");
            inputUgyUgyintezesiNapok.Attributes.Add("onchange", "SetIntezesiIdoManualV2()");
            IntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIntezesiIdoManualV2()");

            // BLG_1020
            //IratIntezesiIdo_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIratIntezesiIdoManual()");
            IratIntezesiIdoegyseg_KodtarakDropDownList.DropDownList.Attributes.Add("onchange", "SetIratIntezesiIdoManualv2()");

            InitUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
        }

    }

    private string SetCompareUgyiratIratHataridoJavaScript(ImageButton btnSaveToClick)
    {
        string js_IsIratModosithato = "";
        if (Command == CommandName.Modify)
        {
            // ha irat módosítás módban vagyunk, megengedjük, hogy a (számítottnál korábbi) ügyirat határidõt vegye át
            js_IsIratModosithato = "true";
        }
        else
        {
            // iktatásnál a kötött irat határidõ nem módosítható
            js_IsIratModosithato = "!bIratIntezesiIdoKotott";
        }
        // ha módosíthatja az ügyirat határidõt (fõszámra iktatás), akkor kitoljuk azt
        string js_ToDoIfDateMisMatch_AllowedToModify = @"
            var bUgyiratModosithato = bAllowedToModify && bUgyiratHataridoKitolas;
            var bIratModosithato = " + js_IsIratModosithato + @";
            btnSaveToClick = $get('" + btnSaveToClick.ClientID + @"');
            qmp_show(bUgyiratModosithato, bIratModosithato);
            return false;
";

        string js_AllowedToModify = "";
        if (Command == CommandName.Modify)
        {
            // irat módosításkor az ügyirat nem módosítható (TODO?)
            js_AllowedToModify = "false";
        }
        else if (Command == CommandName.New && bUgyiratHataridoModositasiJog)
        {
            // fõszámra iktatás vagy alszámra, és a határidõ kitolható
            js_AllowedToModify = "(!elozmenyHiddenField || elozmenyHiddenField.value == '' || (elozmenyHiddenField.value != '' && bUgyiratHataridoKitolas && ((bIsUgyintezo && bIsOrzo) || (bIsIktatokonyvLezart && !bIsIktatokonyvFSLezart))))";
        }
        else
        {
            // csak fõszámra iktatáskor
            js_AllowedToModify = "(!elozmenyHiddenField || elozmenyHiddenField.value == '')";
        }

        string js = @"var ugyiratHataridoTextBox = $get('" + UgyUgyirat_Hatarido_CalendarControl.TextBox.ClientID + @"');
var iratHataridoTextBox = $get('" + IraIrat_Hatarido_CalendarControl.TextBox.ClientID + @"');
if (ugyiratHataridoTextBox && iratHataridoTextBox)
{
    var iratIntezesiHataridoHourTextBox = $get('" + IraIrat_Hatarido_CalendarControl.GetHourTextBox.ClientID + @"');
    var iratIntezesiHataridoMinuteTextBox = $get('" + IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.ClientID + @"');
    var elozmenyHiddenField = $get('" + ElozmenyUgyiratID_HiddenField.ClientID + @"');
    var ugyiratHataridoKitolasUgyiratHiddenField = $get('" + UgyiratHataridoKitolas_Ugyirat_HiddenField.ClientID + @"');
    var ugyiratHataridoKitolasIratHiddenField = $get('" + UgyiratHataridoKitolas_Irat_HiddenField.ClientID + @"');
    var ugyintezoHiddenField = $get('" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.HiddenField.ClientID + @"');
    var orzoHiddenField = $get('" + FelhasznaloCsoportIdOrzo_HiddenField.ClientID + @"');
    var lezartIktatokonyvHiddenField = $get('" + LezartIktatokonyv_HiddenField.ClientID + @"');
    var lezartIktatokonyvFSHiddenField = $get('" + LezartFolyosorszamosIktatokonyv_HiddenField.ClientID + @"');
    var bUgyiratHataridoKitolas = true;
    var bIsUgyintezo = false;
    var bIsOrzo = false;
    var bIsIktatokonyvLezart = false;
    var bIsIktatokonyvFSLezart = false;
    var bIratIntezesiIdoKotott = false;
    if (ugyiratHataridoKitolasUgyiratHiddenField && ugyiratHataridoKitolasIratHiddenField)
    {
        if (ugyiratHataridoKitolasUgyiratHiddenField.value == '0')
        {
            bUgyiratHataridoKitolas = false;
        }
        else if (ugyiratHataridoKitolasIratHiddenField.value == '0|0' || ugyiratHataridoKitolasIratHiddenField.value == '0|1')
       {
            bUgyiratHataridoKitolas = false;
       }

        if (ugyiratHataridoKitolasIratHiddenField.value == '0|1' || ugyiratHataridoKitolasIratHiddenField.value == '1|1')
        {
            bIratIntezesiIdoKotott = true;
        }
    }

    if (ugyintezoHiddenField && ugyintezoHiddenField.value == '" + FelhasznaloProfil.FelhasznaloId(Page) + @"')
    {
        bIsUgyintezo = true;
    }
    if (orzoHiddenField && orzoHiddenField.value == '" + FelhasznaloProfil.FelhasznaloId(Page) + @"')
    {
        bIsOrzo = true;
    }
    if (lezartIktatokonyvHiddenField && lezartIktatokonyvHiddenField.value != '')
    {
        bIsIktatokonyvLezart = true;
    }
    if (lezartIktatokonyvFSHiddenField && lezartIktatokonyvFSHiddenField.value != '')
    {
        bIsIktatokonyvLezart = true;
        bIsIktatokonyvFSLezart = true;
    }

    var ugyiratHatarido = Date.parseLocale(ugyiratHataridoTextBox.value);
    var iratHatarido = Date.parseLocale(iratHataridoTextBox.value);
    if (ugyiratHataridoTextBox.value != '' && iratHataridoTextBox.value != '' && ugyiratHatarido < iratHatarido)
    {
            var bAllowedToModify = " + js_AllowedToModify + @";
    " + js_ToDoIfDateMisMatch_AllowedToModify + @"
    }
}";

        return js;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.ButtonsClick +=
            new CommandEventHandler(FormHeader1_ButtonsClick);

        // Cím:
        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.EmailErkeztetesIktatasFormHeaderTitle;
        }

        if (Mode == ErkeztetesTipus.HivataliKapusErkeztetes)
        {
            FormHeader1.FullManualHeaderTitle = "Elektronikus üzenet érkeztetés/iktatás";
        }

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

            #region BLG 232
            //LZS
            //A Page_Load() metódusban dolgozzuk fel a speciális „ForceSave” postback - et.Ilyenkor állítjuk „1” re a TabFooter.ConfirmationResult.Value - t, 
            //amely tárolja majd, hogy a confirmation ablakban OK - t válaszoltunk.
            //Ezt követően meghívjuk a TabFooterButtonsClick() metódust újra „Save” paraméterrel.
            string eventArgument = Request.Params["__EVENTARGUMENT"];

            if (eventArgument == "ForceSave")
            {
                try
                {
                    TabFooter1.ConfirmationResult.Value = "1";
                    CommandEventArgs eventArgs = new CommandEventArgs(CommandName.Save, "New");
                    TabFooterButtonsClick(TabFooter1, eventArgs);
                }
                catch (Exception ex)
                {
                    Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                }

            }
            #endregion

        }
        SetIratCsoportIdUgyFelelos_Visibility(false);
        if (!IsPostBack)
        {
            ReLoadTab();
            UgyUgyirat_Ugytipus_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");

            Ugykor_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            AgazatiJelek_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            Iktatokonyvek_DropDownLis_Ajax.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
            // BLG_44
            IrattariTetelszam_DropDownList.Attributes.Add("onmouseover", "if (this.options && this.selectedIndex>-1 && this.options[this.selectedIndex]) {this.title = this.options[this.selectedIndex].title;}");
        }


        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            #region Rádiógombok engedélyezése funkciójog alapján:

            // Munkapéldány létrehozás gomb funkciójoghoz kötve:
            ListItem liMunkapeldany = EmailKezelesSelector.GetListItemByValue(ErkeztetesVagyIktatasRadioButtonList
                , EmailKezelesSelector.EmailKezelesTipus.Munkapeldany);
            if (liMunkapeldany != null)
            {
                liMunkapeldany.Enabled = FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites);
            }
            //ErkeztetesVagyIktatasRadioButtonList.Items[2].Enabled = FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites);

            // Érkeztetés és iktatás:
            bool vanEmailIktatasJog = FunctionRights.GetFunkcioJog(Page, funkcio_EmailIktatas);
            ListItem liErkeztetesIktatas = EmailKezelesSelector.GetListItemByValue(ErkeztetesVagyIktatasRadioButtonList, EmailKezelesSelector.EmailKezelesTipus.ErkeztetesIktatas);
            if (liErkeztetesIktatas != null)
            {
                //ErkeztetesVagyIktatasRadioButtonList.Items[0].Enabled = vanEmailIktatasJog;
                liErkeztetesIktatas.Enabled = vanEmailIktatasJog;
            }

            if (SubMode == EmailBoritekTipus.Kuldemeny)
            {
                // ha nincs emailiktató joga, át kell küldeni másik rádiógombra, mert alapból az iktatóra menne:
                if (!IsPostBack && !vanEmailIktatasJog && liMunkapeldany != null)
                {
                    // Az emailÉrkeztetés rádiógombra kell állni:
                    liMunkapeldany.Selected = true;
                    ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(sender, e);
                }
            }
            else
            {
                // ha nincs emailiktató joga, át kell küldeni másik rádiógombra, mert alapból az iktatóra menne:
                if (!IsPostBack && !vanEmailIktatasJog)
                {
                    // Az emailÉrkeztetés rádiógombra kell állni:
                    //ErkeztetesVagyIktatasRadioButtonList.SelectedValue = "1";
                    EmailKezelesSelector.SetSelectedValue(ErkeztetesVagyIktatasRadioButtonList
                        , EmailKezelesSelector.EmailKezelesTipus.CsakErkeztetes);
                    ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(sender, e);
                }
            }
            #endregion
        }


        if (Mode == ErkeztetesTipus.HivataliKapusErkeztetes)
        {
            #region Rádiógombok engedélyezése funkciójog alapján:

            // Munkapéldány létrehozás gomb funkciójoghoz kötve:
            ListItem liMunkapeldany = EmailKezelesSelector.GetListItemByValue(ErkeztetesVagyIktatasRadioButtonList
                , EmailKezelesSelector.EmailKezelesTipus.Munkapeldany);
            if (liMunkapeldany != null)
            {
                liMunkapeldany.Enabled = FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites);
            }
            //ErkeztetesVagyIktatasRadioButtonList.Items[2].Enabled = FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites);

            // Érkeztetés és iktatás:
            bool vanEmailIktatasJog = FunctionRights.GetFunkcioJog(Page, funkcio_eBeadvanyokIktatas);
            ListItem liErkeztetesIktatas = EmailKezelesSelector.GetListItemByValue(ErkeztetesVagyIktatasRadioButtonList, EmailKezelesSelector.EmailKezelesTipus.ErkeztetesIktatas);
            if (liErkeztetesIktatas != null)
            {
                //ErkeztetesVagyIktatasRadioButtonList.Items[0].Enabled = vanEmailIktatasJog;
                liErkeztetesIktatas.Enabled = vanEmailIktatasJog;
            }

            if (SubMode == EmailBoritekTipus.Kuldemeny)
            {
                // ha nincs emailiktató joga, át kell küldeni másik rádiógombra, mert alapból az iktatóra menne:
                if (!IsPostBack && !vanEmailIktatasJog && liMunkapeldany != null)
                {
                    // Az emailÉrkeztetés rádiógombra kell állni:
                    liMunkapeldany.Selected = true;
                    ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(sender, e);
                }
            }
            else
            {
                // ha nincs emailiktató joga, át kell küldeni másik rádiógombra, mert alapból az iktatóra menne:
                if (!IsPostBack && !vanEmailIktatasJog)
                {
                    // Az emailÉrkeztetés rádiógombra kell állni:
                    //ErkeztetesVagyIktatasRadioButtonList.SelectedValue = "1";
                    EmailKezelesSelector.SetSelectedValue(ErkeztetesVagyIktatasRadioButtonList
                        , EmailKezelesSelector.EmailKezelesTipus.CsakErkeztetes);
                    ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(sender, e);
                }
            }
            #endregion
        }

        IraIrat_Irattipus_KodtarakDropDownList.AutoPostBack = true;
        IraIrat_Irattipus_KodtarakDropDownList.DropDownList.SelectedIndexChanged += IraIrat_Irattipus_KodtarakDropDownList_SelectedIndexChanged;

        // BLG_44
        IrattariTetelszam_DropDownList.DropDownList.AutoPostBack = true;
        IrattariTetelszam_DropDownList.DropDownList.SelectedIndexChanged += new EventHandler(IrattariTetelszam_DropDownList_SelectedIndexChanged);

        KuldesMod_DropDownList.AutoPostBack = true;
        KuldesMod_DropDownList.DropDownList.SelectedIndexChanged += KuldesModja_DropDownList_SelectedIndexChanged;

        UGY_FAJTAJA_KodtarakDropDownList.DropDownList.AutoPostBack = true;
        UGY_FAJTAJA_KodtarakDropDownList.DropDownList.SelectedIndexChanged += new EventHandler(UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged);

        #region BLG_452
        Bekuldo_PartnerTextBox.TextBox.TextChanged += Bekuldo_PartnerTextBox_TextChanged;
        SetFutarJegyzekVisibility();
        #endregion

        #region EMAIL ADDRESS FROM EMAIL BODY
        SetKuldoCimEmailAddressFromSession();
        #endregion

        //LZS - BLG_335 - Ugyintező clone mezők beállítása
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZO_MASOLAS_UGYROL) == "1")
        {
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.CloneHiddenfieldId = Irat_Ugyintezo_FelhasznaloCsoportTextBox.HiddenField.ClientID;
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.CloneTextboxId = Irat_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.ClientID;
        }


        BejovoPeldanyListPanel1.Visible = PeldanyokSorszamaMegadhato;

        #region SAKKORA
        if (!Page.IsPostBack)
        {
            if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.SIMPLE)
            {
                SetVisibilityIratHatasa(false);
                HideFelfuggesztesOka();
                HideLezarasOka();
            }
            else if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
            {
                InitializeIratHatasaDDL(null);
                SetVisibilityIratHatasa(true);

                KodtarakDropDownListUgyiratFelfuggesztesOka.FillDropDownList(KodTarak.UGYIRAT_FELFUGGESZTES_OKA.KCS, true, EErrorPanel1);
                ShowFelfuggesztesOka();

                InitializeLezarasOkaDDL(null, null);
                ShowLezarasOka();
            }
        }
        else
        {
            ShowFelfuggesztesOka();
            InitializeLezarasOkaDDL(null, null);
            ShowLezarasOka();
        }
        #endregion

        //LZS - BLG_232
        //A Page_Load() végén beállítjuk, hogy ha a ConfirmationResult értéke „1”, akkor látszódjon az ErrorPanel.
        if (TabFooter1.ConfirmationResult.Value == "1")
        {
            EErrorPanel1.Visible = true;
        }
        HiddenField_Svc_FelhasznaloId.Value = FelhasznaloProfil.FelhasznaloId(Page);
        HiddenField_Svc_FelhasznaloSzervezetId.Value = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);

        // BUG_10649
        if (Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.POSTAI_RAGSZAM_ERKEZTETESKOR, false))
        {
            RagszamRequiredTextBox.RequiredValidate = LabelReqRagszam.Visible = RagszamRequiredTextBox.CheckRagszamRequired(KuldesMod_DropDownList.SelectedValue);
        }
        else
            RagszamRequiredTextBox.RequiredValidate = LabelReqRagszam.Visible = false;
       
    }

    private void IrattariTetelszam_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetMergeItszFromDDL();
    }

    // BUG_1499
    private void SetMergeItszFromDDL()
    {
        Merge_IrattariTetelszamTextBox.Visible = true;
        int elv = IrattariTetelszam_DropDownList.DropDownList.SelectedItem.Text.IndexOf('_');
        if (elv > -1)
        {
            Merge_IrattariTetelszamTextBox.Text = IrattariTetelszam_DropDownList.DropDownList.SelectedItem.Text.Substring(0, elv);

            CascadingDropDown_IktatoKonyv.ContextKey = FelhasznaloProfil.FelhasznaloId(Page) + ';' + IrattariTetelszam_DropDownList.DropDownList.SelectedValue;
        }
        if (IrattariTetelszam_DropDownList.SelectedValue == String.Empty)
        {
            CascadingDropDown_IktatoKonyv.ParentControlID = "";
            Merge_IrattariTetelszamTextBox.Text = String.Empty;
        }
        else CascadingDropDown_IktatoKonyv.ParentControlID = IrattariTetelszam_DropDownList.DropDownList.UniqueID;
    }

    protected void IraIratUpdatePanel_Load(object sender, EventArgs e)
    {
        // csak iktatáskor van értelme ezt figyelni (nehogy feleslegesen lefusson a tabfülek kattintgatásakor)

        if (Command == CommandName.New)
        {
            if (Request.Params["__EVENTARGUMENT"] != null)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
                switch (eventArgument)
                {
                    case EventArgumentConst.refreshKuldemenyekPanel:
                        if (!String.IsNullOrEmpty(EmailBoritekokKuldemenyId))
                        {
                            LoadKuldemenyComponents(EmailBoritekokKuldemenyId);
                        }
                        break;
                    case EventArgumentConst.refreshUgyiratokPanel:
                        if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
                        {
                            SetElozmenyUgyirat();
                        }
                        else if (!String.IsNullOrEmpty(MigraltUgyiratID_HiddenField.Value))
                        {
                            SetElozmenyRegiUgyirat();
                        }
                        break;
                }
            }
        }
    }

    #region Elozmeny Ugyirat
    /// <summary>
    /// Ügyirat adatainak feltöltése adatbázisból
    /// </summary>
    private void SetElozmenyUgyirat()
    {
        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
        if (!String.IsNullOrEmpty(UgyUgyirat_Id))
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = UgyUgyirat_Id;
            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;
                SetElozmenyUgyirat(erec_UgyUgyiratok);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel1.Update();
            }
        }
    }

    private void SetElozmenyUgyirat(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = UgyUgyirat_Id;
        if (erec_UgyUgyiratok != null)
        {
            /// Ellenõrzés, hogy lehet-e alszámra iktatni az ügyiratba
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(erec_UgyUgyiratok);
            ErrorDetails errorDetail = null;

            /// CR#1305: (FPH_TITKARSAGNAL) Ellenõrzés: Ha BejovoIratIktatasCsakAlszamra vagy BelsoIratIktatasCsakAlszamra joga van, akkor 
            /// nem iktathat bizonyos állapotú ügyiratokba ()
            if (!Ugyiratok.LehetAlszamraIktatni(execParam, ugyiratStatusz, out errorDetail)
                    || !Ugyiratok.CheckAlszamraIktatasSkontroIrattar_Funkciojog(Page, erec_UgyUgyiratok, CommandName.BejovoIratIktatas))
            {
                // nem lehet alszámra iktatni, hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(52109, errorDetail));
                ErrorUpdatePanel1.Update();

                // hiddenfield értékének törlése
                ElozmenyUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();
                return;
            }

            // ha van elõzmény, nem kell javasolt elõzmény mezõ
            if (Command == CommandName.New && IsMunkaanyagLetrehozas)
            {
                JavasoltElozmenyTextBox.Text = String.Empty;
                tr_JavasoltElozmeny.Visible = false;
            }

            //// Egyéb mûveletek sor megjelenítése alszámra iktatásnál:
            //tr_EgyebMuvelet.Visible = true;

            // elõrébb hoztuk, mert az elõzmény ügyirat esetleg felszabadíthatja a LoadUgyiratComponentsben
            UgyUgyirat_Hatarido_CalendarControl.ReadOnly = true;
            trUgyiratIntezesiIdoVisible = false;

            EREC_IraIktatoKonyvek erec_IraIktatokonyvek = LoadUgyiratComponents(erec_UgyUgyiratok);

            #region BLG_335
            //LZS – Előzményirat betöltésekor rendszerparaméterek alapján történik a tárgy, ügyintéző és az ügyintézési határidő másolása ügyiratról iratra.
            //LZS most meg mégis kell?
            // Mégsem kell (CR#1052)
            //// Ha üres az Iratok tárgy, segítségül beírjuk oda az Ügyiratok tárgyat
            if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
            {
                if (String.IsNullOrEmpty(IraIrat_Targy_RequiredTextBox.Text))
                {
                    IraIrat_Targy_RequiredTextBox.Text = UgyUgyirat_Targy_RequiredTextBox.Text;
                }
            }

            if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == "1")
            {
                if (String.IsNullOrEmpty(IraIrat_Hatarido_CalendarControl.Text))
                {
                    IraIrat_Hatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
                    //BUG 5911 - hibát okoz
                    //BeerkezesIdeje_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
                }
            }

            if (Rendszerparameterek.Get(Page, Rendszerparameterek.UGYINTEZO_MASOLAS_UGYROL) == "1")
            {
                if (String.IsNullOrEmpty(Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
                {
                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
                    Irat_Ugyintezo_FelhasznaloCsoportTextBox.Text = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text;
                }
            }
            #endregion

            // Iktatóköny vizsgálata, lezárt-e esetleg:
            bool iktatoKonyvLezart = false;
            bool iktatokonyvFolyosorszamos = false;
            if (Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatokonyvek))
            {
                // Lezárt az iktatókönyv:
                iktatoKonyvLezart = true;

                if (erec_IraIktatokonyvek.IktSzamOsztas == KodTarak.IKT_SZAM_OSZTAS.Folyosorszamos)
                {
                    iktatokonyvFolyosorszamos = true;
                }

                if (iktatokonyvFolyosorszamos)
                {
                    LezartIktatokonyv_HiddenField.Value = "";
                    LezartFolyosorszamosIktatokonyv_HiddenField.Value = erec_IraIktatokonyvek.Id;
                }
                else
                {
                    LezartIktatokonyv_HiddenField.Value = erec_IraIktatokonyvek.Id;
                    LezartFolyosorszamosIktatokonyv_HiddenField.Value = "";
                }
            }
            else
            {
                LezartIktatokonyv_HiddenField.Value = "";
                LezartFolyosorszamosIktatokonyv_HiddenField.Value = "";
            }

            // Ügyirat lezárt-e?
            bool ugyiratLezart = false;
            if (Ugyiratok.Lezart(ugyiratStatusz))
            {
                ugyiratLezart = true;
            }

            //Lezárt iktatókönyvû elõzménybe nem iktathat, ha csak alszámra iktathat joga van
            if (RaiseErrorLezartIktatokonyvbeNemIktathat(Mode))
            {
                // hiddenfield értékének törlése
                ElozmenyUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();
                return;
            }

            // Felelõs, Kezelõ sor látszódjon:
            UgyUgyiratok_CsoportId_Felelos.Visible = true;

            // attól függ, hogy lezárt-e az iktatókönyv és van-e joga fõszámra iktatni
            bool bFoszamraIktathat = true;
            bFoszamraIktathat &= !(FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra));
            if (iktatoKonyvLezart && !iktatokonyvFolyosorszamos && bFoszamraIktathat)
            {
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = false;
                if (Command == CommandName.New)
                {
                    UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate =
                        Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYINTEZO_KOTELEZO_ENABLED);
                }
                Ugyfelelos_CsoportTextBox.ReadOnly = false;

                // ügyindító, ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = false;
                CimekTextBoxUgyindito.ReadOnly = false;

                UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = false;
                UgyUgyirat_Targy_RequiredTextBox.ReadOnly = false;

                UgyUgyiratok_CsoportId_Felelos.ReadOnly = false;

                // irat helye:
                labelIratHelye_Ugyirat.Visible = false;
                UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = false;

                // kezelõ a jelenlegi felhasználó lesz
                UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;
                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

                // kezdõdátumot nem adunk meg
                IktatasDatuma_Ugyirat_HiddenField.Value = "";
                IktatasDatuma_Irat_HiddenField.Value = "";

                if (isTUKRendszer)
                {
                    IrattariHelyLevelekDropDownTUK.Enabled = true;
                }
            }
            else
            {
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = true;
                Ugyfelelos_CsoportTextBox.ReadOnly = true;

                // ügyindító, ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = true;
                CimekTextBoxUgyindito.ReadOnly = true;

                UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = true;
                UgyUgyirat_Targy_RequiredTextBox.ReadOnly = true;

                UgyUgyiratok_CsoportId_Felelos.ReadOnly = true;

                // irat helye:
                labelIratHelye_Ugyirat.Visible = true;
                UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = true;

                if (isTUKRendszer)
                {
                    IrattariHelyLevelekDropDownTUK.Enabled = false;
                }
            }

            labelSkontroVege.Visible = false;
            UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            labelIrattarba.Visible = false;
            UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Skontroban)
            {
                labelSkontroVege.Visible = true;
                UgyUgyirat_SkontroVege_CalendarControl.Visible = true;
                labelIrattarba.Visible = false;
                UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
            }
            if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott)
            {
                labelIrattarba.Visible = true;
                UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = true;
                labelSkontroVege.Visible = false;
                UgyUgyirat_SkontroVege_CalendarControl.Visible = false;
            }

            // BUG_1499         
            IrattariTetelszam_DropDownList.FillAndSetSelectedValue(erec_UgyUgyiratok.IraIrattariTetel_Id, true, EErrorPanel1);
            UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, erec_UgyUgyiratok.Ugy_Fajtaja, true, EErrorPanel1);
            SetIrattariTetelUI();
            UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged(UGY_FAJTAJA_KodtarakDropDownList.DropDownList, EventArgs.Empty);
            InitializeIratHatasaDDL(null);

            if (!iktatoKonyvLezart || iktatokonyvFolyosorszamos)
            {
                #region Cascading cuccok letiltása:
                CascadingDropDown_AgazatiJel.Enabled = false;
                CascadingDropDown_Ugykor.Enabled = false;
                CascadingDropDown_Ugytipus.Enabled = false;
                CascadingDropDown_IktatoKonyv.Enabled = false;

                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
                {
                    // BUG_1499
                    //// BLG_44
                    tr_agazatiJel.Visible = false;
                    //Label_agazatiJel.Visible = false;
                    AgazatiJelek_DropDownList.Visible = false;
                    Ugykor_DropDownList.Visible = false;
                    Merge_IrattariTetelszamTextBox.Visible = false;
                    UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = true;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    //labelIrattariTetelszam.Visible = true;
                    tr_ugytipus.Visible = false;
                    UgyUgyirat_Ugytipus_DropDownList.Visible = false;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    ////labelUgykorKod.Visible = false;
                }

                Iktatokonyvek_DropDownLis_Ajax.Visible = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = true;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = true;

                RequiredFieldValidator1.Enabled = false;
                // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
                //RequiredFieldValidator2.Enabled = false;
                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
                {
                    RequiredFieldValidator2.Enabled = true;
                }
                else RequiredFieldValidator2.Enabled = false;

                // BLG_44
                UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = true;
                // BUG_1499
                //IrattariTetelszam_DropDownList.Visible = false;
                IrattariTetelszam_DropDownList.ReadOnly = true;
                #endregion
            }
            else
            {
                #region Cascading cuccok engedélyezése:
                CascadingDropDown_AgazatiJel.Enabled = true;
                CascadingDropDown_Ugykor.Enabled = true;
                CascadingDropDown_Ugytipus.Enabled = true;
                CascadingDropDown_IktatoKonyv.Enabled = true;

                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
                {
                    // BUG_1499
                    tr_agazatiJel.Visible = true;
                    AgazatiJelek_DropDownList.Visible = true;
                    Ugykor_DropDownList.Visible = true;
                    Merge_IrattariTetelszamTextBox.Visible = true;
                    UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    ////labelIrattariTetelszam.Visible = false;
                    tr_ugytipus.Visible = true;
                    UgyUgyirat_Ugytipus_DropDownList.Visible = true;
                    //// CR3119: Mindenhol irattári tételszám jelenjen meg
                    ////labelUgykorKod.Visible = true;
                }

                Iktatokonyvek_DropDownLis_Ajax.Visible = true;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = false;
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

                RequiredFieldValidator1.Enabled = true;
                RequiredFieldValidator2.Enabled = true;

                #endregion

                UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
                if (Uj_Ugyirat_Hatarido_Kezeles)
                {
                    trUgyiratIntezesiIdoVisible = true;
                }
                SetIrattariTetelIktatokonyvElozmenyUgyirat(erec_UgyUgyiratok);

                // BUG_1499
                UGY_FAJTAJA_KodtarakDropDownList.ReadOnly = false;
                IrattariTetelszam_DropDownList.ReadOnly = false;
            }

            #region Módosítás gomb állítása

            if (!Ugyiratok.Modosithato(ugyiratStatusz, execParam, out errorDetail))
            {
                ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = "alert('" + Resources.Error.UIUgyiratNemModosithato
                   + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }
            else
            {
                ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + UgyUgyirat_Id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, ImageButton_ElozmenyUgyirat_Modositas.ClientID, EventArgumentConst.refreshUgyiratokPanel);
            }

            #endregion

            // iktatókönyv dropdown readonly, kivéve ha lezárt az elõzmény iktatókönyve
            if (iktatoKonyvLezart && !iktatokonyvFolyosorszamos)
            {
            }
            else
            {
                ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = true;
            }

            // Figyelmeztetõ üzenetek megjelenítése:
            if (iktatoKonyvLezart)
            {
                tr_lezartazIktatokonyv.Visible = true;
                tr_lezartazUgyirat.Visible = false;
                if (iktatokonyvFolyosorszamos)
                {
                    labelLezart.Visible = false;
                    labelLezartFolyamatos.Visible = true;
                }
                else
                {
                    labelLezart.Visible = true;
                    labelLezartFolyamatos.Visible = false;
                }
            }
            else if (ugyiratLezart)
            {
                tr_lezartazIktatokonyv.Visible = false;
                tr_lezartazUgyirat.Visible = true;
            }
            else
            {
                tr_lezartazIktatokonyv.Visible = false;
                tr_lezartazUgyirat.Visible = false;
            }

            #region standard objektumfüggõ tárgyszavak ügyirathoz

            FillObjektumTargyszavaiPanel(UgyUgyirat_Id);
            if (iktatoKonyvLezart && !iktatokonyvFolyosorszamos && bFoszamraIktathat)
            {
                otpStandardTargyszavak.ReadOnly = false;
            }
            else
            {
                otpStandardTargyszavak.ReadOnly = true;
            }

            if (otpStandardTargyszavak.Count > 0)
            {
                StandardTargyszavakPanel.Visible = true;
            }
            else
            {
                StandardTargyszavakPanel.Visible = false;
            }

            #endregion standard objektumfüggõ tárgyszavak ügyirathoz

            SetElozmenyUgyiratUI();

            #region irat határidõ
            RegisterStartupScript("setHatarido", "IrattipusOnChange();");
            #endregion irat határidõ

        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "Rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
        }
    }


    protected void FillObjektumTargyszavaiPanel(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak.FillObjektumTargyszavai(search, id, null
        , Constants.TableNames.EREC_UgyUgyiratok, null, null, false
        , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, false, EErrorPanel1);
    }

    protected void FillObjektumTargyszavaiPanel_Iratok(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak_Iratok.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_IraIratok, null, null, false
            , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, false, EErrorPanel1);
    }

    private void SetElozmenyUgyiratUI()
    {
        // elõzményezés után már nem lehet régi adatot keresni
        //ImageButton_MigraltKereses.Enabled = false;
        //UI.SetImageButtonStyleToDisabled(ImageButton_MigraltKereses);
        // Régi adat azonosító sor eltüntetése:
        trRegiAdatAzonosito.Visible = false;

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
    }

    private void ClearElozmenyUgyirat()
    {
        ElozmenyUgyiratID_HiddenField.Value = String.Empty;
        MigraltUgyiratID_HiddenField.Value = String.Empty;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.ReadOnly = false;
        UgyUgyirat_Targy_RequiredTextBox.ReadOnly = false;
        UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            trUgyiratIntezesiIdoVisible = true;
        }
        UgyUgyiratok_CsoportId_Felelos.ReadOnly = false;

        labelSkontroVege.Visible = false;
        UgyUgyirat_SkontroVege_CalendarControl.Visible = false;

        labelIrattarba.Visible = false;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;

        // Felelõs, Kezelõ sor látszódjon:
        UgyUgyiratok_CsoportId_Felelos.Visible = true;

        if (isTUKRendszer)
        {
            IrattariHelyLevelekDropDownTUK.Enabled = true;
        }

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.ReadOnly = false;
        if (Command == CommandName.New)
        {
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate =
                Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYINTEZO_KOTELEZO_ENABLED);

            // ha nincs elõzmény, itt kell javasolt elõzmény mezõ
            if (IsMunkaanyagLetrehozas)
            {
                tr_JavasoltElozmeny.Visible = true;
            }
        }
        Ugyfelelos_CsoportTextBox.ReadOnly = false;

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly = false;
        CimekTextBoxUgyindito.ReadOnly = false;

        // Irat helye:
        labelIratHelye_Ugyirat.Visible = false;
        UgyiratOrzo_FelhasznaloCsoportTextBox.Visible = false;

        #region Cascading cuccok engedélyezése:
        CascadingDropDown_AgazatiJel.Enabled = true;
        CascadingDropDown_Ugykor.Enabled = true;
        CascadingDropDown_Ugytipus.Enabled = true;
        CascadingDropDown_IktatoKonyv.Enabled = true;

        // Cascading cuccok kiürítése:
        CascadingDropDown_AgazatiJel.SelectedValue = String.Empty;
        CascadingDropDown_Ugykor.SelectedValue = String.Empty;
        CascadingDropDown_Ugytipus.SelectedValue = String.Empty;
        CascadingDropDown_IktatoKonyv.SelectedValue = String.Empty;

        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {
            tr_agazatiJel.Visible = true;
            AgazatiJelek_DropDownList.Visible = true;
            Ugykor_DropDownList.Visible = true;
            Merge_IrattariTetelszamTextBox.Visible = true;
            UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Visible = false;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelIrattariTetelszam.Visible = false;
            tr_ugytipus.Visible = true;
            UgyUgyirat_Ugytipus_DropDownList.Visible = true;
            // CR3119: Mindenhol irattári tételszám jelenjen meg
            //labelUgykorKod.Visible = true;
        }

        Iktatokonyvek_DropDownLis_Ajax.Visible = true;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.ReadOnly = false;
        ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        RequiredFieldValidator1.Enabled = true;
        // CR3348 Iktatókönyv vlasztás ITSZ nélkül (ITSZ_ELORE rendszerparamétertõl függ)
        //RequiredFieldValidator2.Enabled = true;
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ITSZ_ELORE, true))
        {
            RequiredFieldValidator2.Enabled = true;
        }
        else RequiredFieldValidator2.Enabled = false;

        // BLG_44
        UGY_FAJTAJA_KodtarakDropDownList.FillAndSetEmptyValue(kcs_UGY_FAJTAJA, EErrorPanel1);
        IrattariTetelszam_DropDownList.FillAndSetEmptyValue(EErrorPanel1);
        SetIrattariTetelUI();
        UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged(UGY_FAJTAJA_KodtarakDropDownList.DropDownList, EventArgs.Empty);
        #endregion

        // Figyelmeztetõ üzenetek megjelenítése:
        tr_lezartazIktatokonyv.Visible = false;
        tr_lezartazUgyirat.Visible = false;

        // lezárt iktatókönyv Id törlése
        LezartIktatokonyv_HiddenField.Value = "";

        #region standard objektumfüggõ tárgyszavak ügyirathoz

        otpStandardTargyszavak.ClearTextBoxValues();
        otpStandardTargyszavak.ReadOnly = false;
        StandardTargyszavakPanel.Visible = true;

        #endregion standard objektumfüggõ tárgyszavak ügyirathoz

        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.OnClientClick = JavaScripts.SetOnClientClick_FormModifyByHiddenField(
            "UgyUgyiratokForm.aspx", "", ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
            "UgyUgyiratokForm.aspx", QueryStringVars.Mode + "=" + Constants.UgyiratTerkep, ElozmenyUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.Enabled = true;
        // elõzményezés után már nem lehet régi adatot keresni
        ImageButton_MigraltKereses.Enabled = true;
        UI.SetImageButtonStyleToEnabled(ImageButton_MigraltKereses);
        // Régi adat azonosító sor eltüntetése:
        trRegiAdatAzonosito.Visible = false;
        RegiAdatAzonosito_Label.Text = String.Empty;
        IrattariTetelTextBox.Text = String.Empty;
        MegkulJelzesTextBox.Text = String.Empty;
        IRJ2000TextBox.Text = String.Empty;
        UgyiratSzerelesiLista1.Reset();

        ClearUgyiratComponents();

        // BLG_8826
        _iratForm.SetDefaultUgyintezesKezdete();
    }

    private void SetElozmenyRegiUgyirat()
    {
        string qsUgyiratterkep = QueryStringVars.Mode + "=" + Constants.UgyiratTerkep;
        ImageButton_Ugyiratterkep.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(
                                    eMigration.migralasFormUrl, qsUgyiratterkep, MigraltUgyiratID_HiddenField.ClientID, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        string qsView = String.Empty;
        ImageButton_ElozmenyUgyirat_Megtekintes.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField(eMigration.migralasFormUrl, qsView, MigraltUgyiratID_HiddenField.ClientID,
                          Defaults.PopupWidth_Max, Defaults.PopupHeight_Max);

        ImageButton_ElozmenyUgyirat_Modositas.Enabled = false;

        SetRegiadatAzonositoRow();

        SetIratHelyeCalendarControls();

        SetIrattariTetelIktatokonyvRegiUgyirat();

        // ha van elõzmény, nem kell javasolt elõzmény mezõ
        if (Command == CommandName.New && IsMunkaanyagLetrehozas)
        {
            JavasoltElozmenyTextBox.Text = String.Empty;
            tr_JavasoltElozmeny.Visible = false;
        }

        #region irat határidõ
        RegisterStartupScript("setHataridoRegiUgyirat", "UgyTipusChangeHandler();");
        #endregion irat határidõ
    }

    private void SetIratHelyeCalendarControls()
    {
        labelIrattarba.Visible = false;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = false;
        labelSkontroVege.Visible = false;
        UgyUgyirat_SkontroVege_CalendarControl.Visible = false;

        // valamiért nem mûködik, ha közvetlenül a DateTextBoxba írunk
        // (Visible = "true" Style="display: none;" mellett)
        // ezért másik textboxba írással kerüljük meg a problémát
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = txtIrattarba.Text;
        UgyUgyirat_SkontroVege_CalendarControl.Text = txtSkontroVege.Text;

        if (!String.IsNullOrEmpty(UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text))
        {
            labelIrattarba.Visible = true;
            UgyUgyirat_IrattarbaHelyezes_CalendarControl.Visible = true;
        }
        else if (!String.IsNullOrEmpty(UgyUgyirat_SkontroVege_CalendarControl.Text))
        {
            labelSkontroVege.Visible = true;
            UgyUgyirat_SkontroVege_CalendarControl.Visible = true;
        }
    }

    private void SetRegiadatAzonositoRow()
    {
        trRegiAdatAzonosito.Visible = true;
        RegiAdatAzonosito_Label.Text = regiAzonositoTextBox.Text;
        if (FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            if (!String.IsNullOrEmpty(IRJ2000TextBox.Text))
            {
                RegiAdatIRJ2000_Label.Text = " - " + IRJ2000TextBox.Text;
            }
            else
            {
                RegiAdatIRJ2000_Label.Text = String.Empty;
            }
            RegiAdatIRJ2000_Label.Visible = true;
        }
    }

    private void SetIrattariTetelIktatokonyvRegiUgyirat()
    {
        // elõkészítés: töröljük a mezõértékeket
        CascadingDropDown_AgazatiJel.SelectedValue = "";
        CascadingDropDown_IktatoKonyv.SelectedValue = "";

        // ezt nem kapjuk vissza, azért töröljük
        CascadingDropDown_Ugytipus.SelectedValue = "";

        if (!String.IsNullOrEmpty(IrattariTetelTextBox.Text))
        {
            #region irattári tétel
            string irattaritetelszam = IrattariTetelTextBox.Text;

            // Ágazati jel és azonosító meghatározása az irattári tételbõl
            EREC_IraIrattariTetelekService service_itsz = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch search_itsz = new EREC_IraIrattariTetelekSearch();
            ExecParam execParam_itsz = UI.SetExecParamDefault(Page, new ExecParam());
            search_itsz.IrattariTetelszam.Value = irattaritetelszam;
            search_itsz.IrattariTetelszam.Operator = Query.Operators.equals;
            Result result_itsz = service_itsz.GetAll(execParam_itsz, search_itsz);

            if (String.IsNullOrEmpty(result_itsz.ErrorCode))
            {
                if (result_itsz.Ds.Tables[0].Rows.Count == 1)
                {
                    string Id_itsz = result_itsz.Ds.Tables[0].Rows[0]["Id"].ToString();
                    string AgazatiJel_Id = result_itsz.Ds.Tables[0].Rows[0]["AgazatiJel_Id"].ToString();

                    CascadingDropDown_AgazatiJel.SelectedValue = AgazatiJel_Id;
                    CascadingDropDown_Ugykor.SelectedValue = Id_itsz;

                    #region iktatókönyv
                    if (!String.IsNullOrEmpty(MegkulJelzesTextBox.Text))
                    {
                        string megkuljelzes = MegkulJelzesTextBox.Text;

                        // Iktatókönyv
                        EREC_IraIktatoKonyvekService service_iktato = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                        EREC_IraIktatoKonyvekSearch search_iktato = new EREC_IraIktatoKonyvekSearch();
                        ExecParam execParam_iktato = UI.SetExecParamDefault(Page, new ExecParam());
                        search_iktato.Iktatohely.Value = megkuljelzes;
                        search_iktato.Iktatohely.Operator = Query.Operators.likestart;
                        search_iktato.Ev.Value = System.DateTime.Now.Year.ToString();
                        search_iktato.Ev.Operator = Query.Operators.equals;
                        Result result_iktato = service_iktato.GetAll(execParam_iktato, search_iktato);

                        if (String.IsNullOrEmpty(result_iktato.ErrorCode))
                        {
                            if (result_iktato.Ds.Tables[0].Rows.Count == 1)
                            {
                                string Id_iktato = result_iktato.Ds.Tables[0].Rows[0]["Id"].ToString();

                                CascadingDropDown_IktatoKonyv.SelectedValue = Id_iktato;
                            }
                        }

                        // végül kiürítjük a textboxot
                        MegkulJelzesTextBox.Text = "";
                    }

                    #endregion iktatókönyv

                    Ajax_eRecord ajax_eRecord = new Ajax_eRecord();
                    string merge_itsz = ajax_eRecord.Get_Merge_IrattariTetelszamByUgykor(Id_itsz, FelhasznaloProfil.FelhasznaloId(Page));
                    Merge_IrattariTetelszamTextBox.Text = merge_itsz;
                }
            }

            // végül kiürítjük a textboxot
            IrattariTetelTextBox.Text = "";
            #endregion irattári tétel
        }
    }

    private void SetIrattariTetelIktatokonyvElozmenyUgyirat(EREC_UgyUgyiratok erec_UgyUgyirat)
    {
        // elõkészítés: töröljük a mezõértékeket
        CascadingDropDown_AgazatiJel.SelectedValue = "";
        CascadingDropDown_IktatoKonyv.SelectedValue = "";
        CascadingDropDown_Ugytipus.SelectedValue = "";

        CascadingDropDown_IktatoKonyv.SelectedValue = "";

        string IraIrattariTetel_Id = erec_UgyUgyirat.IraIrattariTetel_Id;
        string IraIktatokonyv_Id = erec_UgyUgyirat.IraIktatokonyv_Id;
        string Ugytipus = erec_UgyUgyirat.UgyTipus;

        if (!String.IsNullOrEmpty(IraIrattariTetel_Id))
        {
            // Ágazati jel és azonosító meghatározása az irattári tételbõl
            EREC_IraIrattariTetelekService service_itsz = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch search_itsz = new EREC_IraIrattariTetelekSearch();
            ExecParam execParam_itsz = UI.SetExecParamDefault(Page, new ExecParam());
            search_itsz.Id.Value = IraIrattariTetel_Id;
            search_itsz.Id.Operator = Query.Operators.equals;

            #region érvényesség ellenõrzés kikapcsolása
            search_itsz.ErvKezd.Clear();
            search_itsz.ErvVege.Clear();
            #endregion érvényesség ellenõrzés kikapcsolása

            Result result_itsz = service_itsz.GetAll(execParam_itsz, search_itsz);

            if (String.IsNullOrEmpty(result_itsz.ErrorCode))
            {
                if (result_itsz.Ds.Tables[0].Rows.Count == 1)
                {
                    string AgazatiJel_Id = result_itsz.Ds.Tables[0].Rows[0]["AgazatiJel_Id"].ToString();

                    CascadingDropDown_AgazatiJel.SelectedValue = AgazatiJel_Id;
                    CascadingDropDown_Ugykor.SelectedValue = IraIrattariTetel_Id;

                    CascadingDropDown_Ugytipus.SelectedValue = Ugytipus;

                    #region iktatókönyv

                    // Iktatókönyv
                    EREC_IraIktatoKonyvekService service_iktato = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                    ExecParam execParam_iktato_get = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam_iktato_get.Record_Id = IraIktatokonyv_Id;

                    Result result_iktato_get = service_iktato.Get(execParam_iktato_get);
                    if (String.IsNullOrEmpty(result_iktato_get.ErrorCode))
                    {
                        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result_iktato_get.Record;

                        // elvileg csak lezártakra hívjuk...
                        if (!Contentum.eRecord.BaseUtility.IktatoKonyvek.Lezart(erec_IraIktatoKonyvek))
                        {
                            CascadingDropDown_IktatoKonyv.SelectedValue = IraIktatokonyv_Id;
                        }
                        else
                        {
                            EREC_IraIktatoKonyvekSearch search_iktato = new EREC_IraIktatoKonyvekSearch();
                            ExecParam execParam_iktato = UI.SetExecParamDefault(Page, new ExecParam());
                            if (!String.IsNullOrEmpty(erec_IraIktatoKonyvek.MegkulJelzes))
                            {
                                search_iktato.MegkulJelzes.Value = erec_IraIktatoKonyvek.MegkulJelzes;
                                search_iktato.MegkulJelzes.Operator = Query.Operators.equals;
                            }
                            else
                            {
                                search_iktato.MegkulJelzes.Value = "";
                                search_iktato.MegkulJelzes.Operator = Query.Operators.isnull;
                            }
                            search_iktato.Iktatohely.Value = erec_IraIktatoKonyvek.Iktatohely;
                            search_iktato.Iktatohely.Operator = Query.Operators.equals;
                            search_iktato.Ev.Value = System.DateTime.Now.Year.ToString();
                            search_iktato.Ev.Operator = Query.Operators.equals;
                            Result result_iktato = service_iktato.GetAll(execParam_iktato, search_iktato);

                            if (String.IsNullOrEmpty(result_iktato.ErrorCode))
                            {
                                if (result_iktato.Ds.Tables[0].Rows.Count == 1)
                                {
                                    string Id_iktato = result_iktato.Ds.Tables[0].Rows[0]["Id"].ToString();

                                    CascadingDropDown_IktatoKonyv.SelectedValue = Id_iktato;
                                }
                            }
                        }


                    }
                    #endregion iktatókönyv
                }
            }
        }

        Ajax_eRecord ajax_eRecord = new Ajax_eRecord();
        string merge_itsz = ajax_eRecord.Get_Merge_IrattariTetelszamByUgykor(IraIrattariTetel_Id, FelhasznaloProfil.FelhasznaloId(Page));
        Merge_IrattariTetelszamTextBox.Text = merge_itsz;

    }

    #endregion

    public void ReLoadTab()
    {
        if (Command == CommandName.New)
        {
            #region (Iktatás)

            // Components Init
            InitElozmenySearchComponent();
            SetDefaultHataridok();

            // EB 2010.01.22, CR#2284: ügyintézõ megadása rendszerparamétertõl függõen kötelezõ
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Validate =
                Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYINTEZO_KOTELEZO_ENABLED);

            // Kezelõ kitöltve alapból a felhasználóra:
            UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
            UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);
            // eljárási szakasz irat határidõhöz (most csak alapértelmezett lehet)
            UgyiratdarabEljarasiSzakasz_HiddenField.Value = KodTarak.ELJARASI_SZAKASZ.Osztatlan;

            if (Uj_Ugyirat_Hatarido_Kezeles)
            {
                trUgyiratIntezesiIdoVisible = true;

                InitUgyUgyintezesiNapok(UI.SetExecParamDefault(Page, new ExecParam()));
                //IntezesiIdo_KodtarakDropDownList.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, EErrorPanel1);

                List<string> filterList = new List<string>();
                filterList.Add(KodTarak.IDOEGYSEG.Nap);
                filterList.Add(KodTarak.IDOEGYSEG.Munkanap);
                IntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);

                // BLG_1020
                trIratIntezesiIdo.Visible = true;

                //IratIntezesiIdo_KodtarakDropDownList.FillDropDownList(kcs_UGYIRAT_INTEZESI_IDO, true, EErrorPanel1);

                IratIntezesiIdoegyseg_KodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, false, EErrorPanel1);
            }

            if (!IsPostBack)
            {
                #region Template betöltés, ha kell

                if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
                {
                    List<string> filterList = new List<string>();
                    filterList.Add(KodTarak.BoritoTipus.Irat);
                    KodtarakDropDownListBoritoTipus.FillAndSetSelectedValue(kcs_BoritoTipus, KodTarak.BoritoTipus.Irat, filterList, false, EErrorPanel1);
                }

                string templateId = Request.QueryString.Get(QueryStringVars.TemplateId);
                if (!String.IsNullOrEmpty(templateId))
                {
                    _Load = true;
                    FormHeader1.LoadTemplateObjectById(templateId);
                    LoadComponentsFromTemplate((IktatasFormTemplateObject)FormHeader1.TemplateObject);
                    _Load = false;

                }

                #endregion

                bool vanUgyirat = false;

                // Van-e megadva ügyirat alszámra iktatáshoz?
                String ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
                if (!String.IsNullOrEmpty(ugyiratId))
                {
                    ElozmenyUgyiratID_HiddenField.Value = ugyiratId;
                    SetElozmenyUgyirat();
                    vanUgyirat = true;
                }

                // komponensek inicializálása:
                if (vanUgyirat)
                {
                    ImageButton_Elozmeny.Enabled = false;
                    UI.SetImageButtonStyleToDisabled(ImageButton_Elozmeny);
                }

                #region standard objektumfüggõ tárgyszavak ügyirathoz
                if (!vanUgyirat)
                {
                    FillObjektumTargyszavaiPanel(null);
                    if (otpStandardTargyszavak.Count > 0)
                    {
                        StandardTargyszavakPanel.Visible = true;
                        otpStandardTargyszavak.SetDefaultValues();
                        TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                        TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpStandardTargyszavak.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
                    }
                    else
                    {
                        StandardTargyszavakPanel.Visible = false;
                    }
                }
                #endregion standard objektumfüggõ tárgyszavak ügyirathoz

                #region standard objektumfüggõ tárgyszavak irathoz
                FillObjektumTargyszavaiPanel_Iratok(null);
                if (otpStandardTargyszavak_Iratok.Count > 0)
                {
                    StandardTargyszavakPanel_Iratok.Visible = true;
                    otpStandardTargyszavak_Iratok.SetDefaultValues();
                    TabFooter1.ImageButton_Save.OnClientClick = otpStandardTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                    TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpStandardTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
                }
                else
                {
                    StandardTargyszavakPanel_Iratok.Visible = false;
                }

                #endregion standard objektumfüggõ tárgyszavak irathoz

                //IraIrat_Kategoria_KodtarakDropDownList.FillDropDownList(kcs_IRATKATEGORIA, true, EErrorPanel1);

                IraIrat_Irattipus_KodtarakDropDownList.FillDropDownList(kcs_IRATTIPUS, true, EErrorPanel1);

                string IratJelleg = KodTarak.IRAT_JELLEG.Webes; // default
                if (Mode == ErkeztetesTipus.EmailErkeztetes)
                {
                    IratJelleg = KodTarak.IRAT_JELLEG.E_mail_uzenet;
                }
                IratJellegKodtarakDropDown.FillWithOneValue("IRAT_JELLEG", IratJelleg, EErrorPanel1);

                //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillDropDownList(kcs_KEZELESI_FELJEGYZESEK_TIPUSA, true, EErrorPanel1);

                ktDropDownListIratMinosites.FillDropDownList(kcs_IRATMINOSITES, true, EErrorPanel1);

                IratHatasaUgyintezesre_KodtarakDropDownList.FillDropDownList(kcs_IRAT_HATASA_UGYINTEZESRE, false, EErrorPanel1);

                // BLG_44
                UGY_FAJTAJA_KodtarakDropDownList.FillDropDownList(kcs_UGY_FAJTAJA, true, EErrorPanel1);
                IrattariTetelszam_DropDownList.FillDropDownList(true, EErrorPanel1);
                FillTipusosObjektumTargyszavaiPanel_IratokPostazasIranya(null, KodTarak.POSTAZAS_IRANYA.Bejovo);
            }

            if (!IsPostBack)
            {
                #region Hataridok
                TabFooter1.ImageButton_Save.OnClientClick += SetCompareUgyiratIratHataridoJavaScript(TabFooter1.ImageButton_Save);
                TabFooter1.ImageButton_SaveAndClose.OnClientClick += SetCompareUgyiratIratHataridoJavaScript(TabFooter1.ImageButton_SaveAndClose);
                #endregion Hataridok
            }

            #endregion           

            if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
            {
                #region küldemény

                ErkeztetoKonyvekDropDrownList.FillDropDownList(true, true, Constants.IktatoErkezteto.Erkezteto, EErrorPanel1);
                KuldesMod_DropDownList.FillDropDownList(kcs_KULDEMENY_KULDES_MODJA, EErrorPanel1);

                // BLG_361
                //UgyintezesModja_DropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, EErrorPanel1);
                UgyintezesModja_DropDownList.FillDropDownList(kcs_ELSODLEGES_ADATHORDOZO, EErrorPanel1);
                AdathordozoTipusa_DropDownList.FillDropDownList(kcs_UGYINTEZES_ALAPJA, EErrorPanel1);

                Surgosseg_DropDownList.FillDropDownList(kcs_SURGOSSEG, EErrorPanel1);
                Surgosseg_DropDownList.SetSelectedValue(KodTarak.SURGOSSEG.Normal);
                Surgosseg_DropDownList.Validate = false;
                Label18.Visible = false;

                //bernat.laszlo added
                Kezbesites_modja_DropDownList.FillDropDownList(kcs_KULD_KEZB_MODJA, EErrorPanel1);
                CimzesTipusa_DropDownList.FillDropDownList(kcs_KULD_CIMZES_TIPUS, EErrorPanel1);
                tr_Kiadmanyozas.Visible = false;
                //bernat.laszlo eddig

                if (FunctionRights.GetFunkcioJog(Page, "KuldemenyFizikaiBontas"))
                {
                    Kuld_FelbontasDatuma_CalendarControl.SetToday();
                    Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                    Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                }

                if (!IsPostBack)
                {
                    if (isTUKRendszer)
                    {
                        ReloadIrattariHelyLevelekDropDownTUK(UgyUgyiratok_CsoportId_Felelos.HiddenField.Value, null);
                    }
                }

                BeerkezesIdeje_CalendarControl.SetToday();

                //bernat.laszlo added
                ErkeztetesIdeje_CalendarControl.SetToday();

                LoadSessionData();
                #endregion
            }
            else if (Mode == ErkeztetesTipus.EmailErkeztetes)
            {
                if (SubMode == EmailBoritekTipus.Kuldemeny)
                {
                    // Küldemény lekérdezése:
                    LoadKuldemenyComponents(EmailBoritekokKuldemenyId);
                }
                else
                {
                    #region Email

                    Email_IraIktatoKonyvekDropDownList1.FillDropDownList(true, true, Constants.IktatoErkezteto.Erkezteto, true, true, EErrorPanel1);

                    Email_IktatasiKotelezettsegKodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATASI_KOTELEZETTSEG, KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando
                        , false, EErrorPanel1);

                    //bernat.laszlo added
                    Kezbesites_modja_DropDownList.FillDropDownList(kcs_KULD_KEZB_MODJA, EErrorPanel1);
                    //  BUG_4809
                    //CimzesTipusa_DropDownList.FillDropDownList(kcs_KULD_CIMZES_TIPUS, EErrorPanel1);
                    CimzesTipusa_DropDownList.FillAndSetSelectedValue(kcs_KULD_CIMZES_TIPUS, KodTarak.KULD_CIMZES_TIPUS.nevre_szolo, EErrorPanel1);

                    ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
                    Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;
                    //bernat.laszlo eddig

                    // Emailboríték lekérdezése:
                    EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
                    ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
                    execparam_emailGet.Record_Id = EmailBoritekokId;

                    Result result_emailGet = erec_eMailBoritekokService.Get(execparam_emailGet);
                    if (result_emailGet.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                        ErrorUpdatePanel1.Update();
                        return;
                    }

                    EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)result_emailGet.Record;

                    #region Ellenõrzés, lehet-e érkeztetni:

                    if (!String.IsNullOrEmpty(erec_eMailBoritekok.KuldKuldemeny_Id))
                    {
                        // Már érkeztetve van:
                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_52651);
                        ErrorUpdatePanel1.Update();
                        MainPanel.Visible = false;

                        return;
                    }

                    #endregion

                    // verzió lementése:
                    eMailBoritekok_Ver_HiddenField.Value = erec_eMailBoritekok.Base.Ver;

                    // cimzett és feladóból a partneradatok lekérése:
                    Contentum.eAdmin.Service.KRT_PartnerekService service_partnerek =
                        Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_PartnerekService();

                    ExecParam execParam_partnerAdatok = UI.SetExecParamDefault(Page, new ExecParam());

                    Result result_partnerAdatok = service_partnerek.GetEmailPartnerAdatok(execParam_partnerAdatok,
                        erec_eMailBoritekok.Felado, erec_eMailBoritekok.Cimzett);
                    if (!String.IsNullOrEmpty(result_partnerAdatok.ErrorCode))
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_partnerAdatok);
                        ErrorUpdatePanel1.Update();
                        return;
                    }

                    // Beküldõ megállapítása az emailborítékból
                    DataTable table_felado = result_partnerAdatok.Ds.Tables["Felado"];
                    if (table_felado != null && table_felado.Rows.Count > 0)
                    {
                        DataRow row = table_felado.Rows[0];

                        Email_Bekuldo_PartnerTextBox.Id_HiddenField = row["Id"].ToString();
                        Email_Bekuldo_PartnerTextBox.Text = row["Nev"].ToString();

                        // Beküldõ címe:
                        Email_KuldCim_CimekTextBox.ID = row["CimId"].ToString();
                        Email_KuldCim_CimekTextBox.Text = row["CimNev"].ToString();
                    }

                    // Beküldõ szervezetének megállapítása:
                    DataTable table_feladoSzervezet = result_partnerAdatok.Ds.Tables["FeladoSzervezet"];
                    if (table_feladoSzervezet != null && table_feladoSzervezet.Rows.Count > 0)
                    {
                        DataRow row = table_feladoSzervezet.Rows[0];

                        Email_BekuldoSzervezete_PartnerTextBox.Id_HiddenField = row["Partner_id_kapcsolt"].ToString();
                        Email_BekuldoSzervezete_PartnerTextBox.Text = row["Nev"].ToString();
                    }

                    // Címzett megállapítása:
                    DataTable table_cimzett = result_partnerAdatok.Ds.Tables["Cimzett"];
                    if (table_cimzett != null && table_cimzett.Rows.Count > 0)
                    {
                        DataRow row = table_cimzett.Rows[0];

                        Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField = row["Id"].ToString();
                        Email_CsoportIdCimzett_CsoportTextBox.Text = row["Nev"].ToString();
                    }

                    // Címzett szervezetének megállapítása
                    DataTable table_cimzettSzervezet = result_partnerAdatok.Ds.Tables["CimzettSzervezet"];
                    if (table_cimzettSzervezet != null && table_cimzettSzervezet.Rows.Count > 0)
                    {
                        DataRow row = table_cimzettSzervezet.Rows[0];

                        Email_CsoportId_CimzettSzervezete.Id_HiddenField = row["Id"].ToString();
                        Email_CsoportId_CimzettSzervezete.Text = row["Nev"].ToString();
                    }

                    // ha még nem töltöttük ki:
                    if (string.IsNullOrEmpty(Email_KuldCim_CimekTextBox.Text))
                    {
                        Email_KuldCim_CimekTextBox.Text = erec_eMailBoritekok.Felado;
                    }

                    Email_Targy_TextBox.Text = erec_eMailBoritekok.Targy;

                    // Tárgy bemásolása az irat tárgyához is:
                    IraIrat_Targy_RequiredTextBox.Text = erec_eMailBoritekok.Targy;

                    Email_CimzettEmailcime.Text = erec_eMailBoritekok.Cimzett;

                    Email_ErkeztetoFelhasznaloCsoportTextBox1.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                    Email_ErkeztetoFelhasznaloCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

                    // BUG_7678
                    Email_FeladasiIdo_CalendarControl.Text = erec_eMailBoritekok.ErkezesDatuma;

                    Email_BeerkezesIdeje_CalendarControl.SetTodayAndTime();

                    Email_BeerkezesMod_DropDownList.FillWithOneValue(kcs_KULDEMENY_KULDES_MODJA, KodTarak.KULDEMENY_KULDES_MODJA.E_mail, EErrorPanel1);

                    Email_FeladasiIdo_CalendarControl.Text = erec_eMailBoritekok.FeladasDatuma;

                    #endregion

                    #region EMAIL ADDRESS FROM EMAIL BODY
                    Email_KuldCim_CimekTextBox.InitializeEmailAddressFromBody(erec_eMailBoritekok.Id);

                    SetKuldoCimEmailAddressFromSession();
                    #endregion
                }
            }
            else if (isHivataliKapusErkeztetes)
            {
                if (SubMode == EmailBoritekTipus.Kuldemeny)
                {
                    // Küldemény lekérdezése:
                    LoadKuldemenyComponents(EmailBoritekokKuldemenyId);
                }
                else
                {
                    #region Hivatali kapus

                    eBeadvanyokPanel_IraIktatoKonyvekDropDownList.FillDropDownList(true, true, Constants.IktatoErkezteto.Erkezteto, true, true, EErrorPanel1);

                    eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATASI_KOTELEZETTSEG, KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando
                        , false, EErrorPanel1);

                    Kezbesites_modja_DropDownList.FillDropDownList(kcs_KULD_KEZB_MODJA, EErrorPanel1);
                    CimzesTipusa_DropDownList.FillDropDownList(kcs_KULD_CIMZES_TIPUS, EErrorPanel1);
                    ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
                    Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;

                    // eBeadvanyok lekérdezése:
                    EREC_eBeadvanyokService _EREC_eBeadvanyokService = eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
                    ExecParam execparam_dokGet = UI.SetExecParamDefault(Page, new ExecParam());
                    execparam_dokGet.Record_Id = eBeadvanyokId;

                    Result result_dokGet = _EREC_eBeadvanyokService.Get(execparam_dokGet);
                    if (result_dokGet.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_dokGet);
                        ErrorUpdatePanel1.Update();
                        return;
                    }

                    EREC_eBeadvanyok _EREC_eBeadvanyok = (EREC_eBeadvanyok)result_dokGet.Record;

                    #region Ellenõrzés, lehet-e érkeztetni:

                    if (!String.IsNullOrEmpty(_EREC_eBeadvanyok.KuldKuldemeny_Id))
                    {
                        // Már érkeztetve van:
                        ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_52651);
                        ErrorUpdatePanel1.Update();
                        MainPanel.Visible = false;

                        return;
                    }

                    #endregion

                    // verzió lementése:
                    eBeadvanyokPanel_Ver.Value = _EREC_eBeadvanyok.Base.Ver;

                    eBeadvanyokPanel_Nev.Text = _EREC_eBeadvanyok.PartnerNev;
                    Ugy_PartnerId_Ugyindito_PartnerTextBox.Text = _EREC_eBeadvanyok.PartnerNev;
                    //természetes személy
                    if (_EREC_eBeadvanyok.FeladoTipusa == "0")
                    {
                        eBeadvanyokPanel_FeladoCim.Text = _EREC_eBeadvanyok.PartnerKapcsolatiKod;
                        CimekTextBoxUgyindito.Text = _EREC_eBeadvanyok.PartnerKapcsolatiKod;
                    }//hivatal
                    else
                    {
                        eBeadvanyokPanel_FeladoCim.Text = _EREC_eBeadvanyok.PartnerKRID;
                        CimekTextBoxUgyindito.Text = _EREC_eBeadvanyok.PartnerKRID;
                    }

                    eBeadvanyokPanel_ErkeztetesiSzam.Text = _EREC_eBeadvanyok.KR_ErkeztetesiSzam;

                    eBeadvanyokPanel_DokTipusHivatal.Text = _EREC_eBeadvanyok.KR_DokTipusHivatal;

                    eBeadvanyokPanel_DokTipusLeiras.Text = _EREC_eBeadvanyok.KR_DokTipusLeiras;

                    eBeadvanyokPanel_DokTipusAzonosito.Text = _EREC_eBeadvanyok.KR_DokTipusAzonosito;

                    eBeadvanyokPanel_FileNev.Text = _EREC_eBeadvanyok.KR_FileNev;

                    eBeadvanyokPanel_Megjegyzes.Text = _EREC_eBeadvanyok.KR_Megjegyzes;

                    // Tárgy bemásolása az irat tárgyához is:
                    IraIrat_Targy_RequiredTextBox.Text = _EREC_eBeadvanyok.KR_FileNev;

                    // BUG_7678
                    eBeadvanyokPanel_FeladasiIdo.Text = _EREC_eBeadvanyok.KR_ErkeztetesiDatum;

                    eBeadvanyokPanel_BeerkezesIdeje.Text = EBeadvanyKuldemenyBeerkezesIdoMeghatarozas(_EREC_eBeadvanyok);//_EREC_eBeadvanyok.Base.LetrehozasIdo;

                    if (FelhasznaloProfil.OrgIsNMHH(Page))
                    {
                        eBeadvanyokPanel_BeerkezesMod.FillWithOneValue(kcs_KULDEMENY_KULDES_MODJA, KodTarak.KULDEMENY_KULDES_MODJA.Elhisz, EErrorPanel1);
                    }
                    else
                    {
                        eBeadvanyokPanel_BeerkezesMod.FillWithOneValue(kcs_KULDEMENY_KULDES_MODJA, KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu, EErrorPanel1);
                    }
                    #endregion

                    #region BLG 232
                    //LZS
                    //Hivatali kapus (elektronikus üzenetek) érkeztetésekor/iktatásakor a kiválasztott eBeadvany-t kell lekérni, 
                    //majd a benne található PR_Erkeztetoszam mezőt, vagy ha az üres, akkor a KR_Erkeztetoszam mezőt kell kiírni a felületre a eBeadvanyokPanel_ErkeztetesiSzam.Text-be.
                    //eBeadvanyokPanel_ErkeztetesiSzam.Text = !string.IsNullOrEmpty(_EREC_eBeadvanyok.PR_ErkeztetesiSzam) ?
                    //                                        _EREC_eBeadvanyok.PR_ErkeztetesiSzam : _EREC_eBeadvanyok.KR_ErkeztetesiSzam;
                    #endregion

                    #region krx

                    string metaForrasKRID = Rendszerparameterek.Get(Page, "KRX_META_FORRAS_KRID");

                    if (!string.IsNullOrEmpty(metaForrasKRID))
                    {
                        string[] ids = metaForrasKRID.Split(',');

                        if (ids.Contains(_EREC_eBeadvanyok.PartnerKRID) || ids.Contains(_EREC_eBeadvanyok.PartnerKapcsolatiKod))
                        {
                            LoadKRXMetaAdatok();
                        }
                    }
                    else
                    {
                        LoadKRXMetaAdatok();
                    }

                    #endregion

                    if (!string.IsNullOrEmpty(_EREC_eBeadvanyok.PR_ErkeztetesiSzam))
                    {
                        HiddenField_PR_ErkeztetesiSzam.Value = _EREC_eBeadvanyok.PR_ErkeztetesiSzam;
                    }

                    if (!string.IsNullOrEmpty(_EREC_eBeadvanyok.Partner_Id))
                    {
                        HiddenField_Partner_Id.Value = _EREC_eBeadvanyok.Partner_Id;
                    }
                    if (!string.IsNullOrEmpty(_EREC_eBeadvanyok.Cim_Id))
                    {
                        HiddenField_Cim_Id.Value = _EREC_eBeadvanyok.Cim_Id;
                    }
                    LoadEBeadvanyElozmeny(_EREC_eBeadvanyok);
                }
            }

        }

        // Ez az egyes tab fulek belso commandjat hatarozza meg! pl: Kuldemeny modositasakor meg kell kulonboztetni gridview felvetel/modositast!!!
        TabFooter1.CommandArgument = Command;

        // Komponensek láthatóságának beállítása:
        SetComponentsVisibility(Command);

        #region BLG_452
        MinositoPartnerControlEgyszerusitettIktatas.SetMinositoPartner(MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoPartnerId, null);
        #endregion
    }

    private void ReloadIrattariHelyLevelekDropDownTUK(string id, string selectedValue)
    {
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            if (!string.IsNullOrEmpty(id))
            {
                Contentum.eAdmin.Service.KRT_CsoportTagokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Csoport_Id_Jogalany.Value = id;
                search_csoportok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

                search_csoportok.OrderBy = "Csoport_Id ASC, Tipus DESC";

                Result result_csoportok = service_csoportok.GetAllWithExtension(ExecParam, search_csoportok);

                if (!string.IsNullOrEmpty(result_csoportok.ErrorCode))
                    return;

                List<string> Csoportok = new List<string>();

                foreach (DataRow row in result_csoportok.Ds.Tables[0].Rows)
                {
                    Csoportok.Add(row["Csoport_Id"].ToString());
                }
                Csoportok.Add(id);
                EREC_IrattariHelyekSearch search = new EREC_IrattariHelyekSearch();
                search.WhereByManual = " and KRT_PartnerKapcsolatok.Tipus='5' and KRT_PartnerKapcsolatok.Partner_id in " +
                 "(select Partner_id_kapcsolt from KRT_PartnerKapcsolatok where Partner_id in(" + Search.GetSqlInnerString(Csoportok.ToArray()) + ")) ";
                var res = service.GetAllWithPartnerKapcsolatok(ExecParam, search);
                if (res != null && res.Ds.Tables != null && res.Ds.Tables[0].Rows.Count > 0)
                {
                    IrattariHelyLevelekDropDownTUK.FillDropDownList(res, "Id", "Ertek", "1", true, EErrorPanel1);
                    if (!string.IsNullOrEmpty(selectedValue))
                    {
                        IrattariHelyLevelekDropDownTUK.SetSelectedValue(selectedValue);
                    }
                }
            }
        }
    }

    private void InitElozmenySearchComponent()
    {
        bool isTUK = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            //TÜK esetén az id lesz az érték mezőnek feltöltve, mivel itt Id szerinti keresés lesz
            IktatoKonyvekDropDownList_Search.FillDropDownList_SetIdToValues(Constants.IktatoErkezteto.Iktato
                  , evIktatokonyvSearch.EvTol, evIktatokonyvSearch.EvIg, true, false, EErrorPanel1);
        }
        else
        {
            IktatoKonyvekDropDownList_Search.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                  , evIktatokonyvSearch.EvTol, evIktatokonyvSearch.EvIg, true, false, EErrorPanel1);
        }
    }

    private void LoadSessionData()
    {
        if (UI.GetSession(Page, "BoritoTipusChecked") == "true")
        {
            KodtarakDropDownListBoritoTipus.SetSelectedValue(UI.GetSession(Page, "BoritoTipusValue"));
            cbMegorzesJelzo.Checked = true;
        }

        // Címzett megõrzése:
        if (UI.GetSession(Page, session_CimzettChecked) == "true")
        {
            CsoportId_CimzettCsoportTextBox1.Id_HiddenField = UI.GetSession(Page, session_CimzettId);
            CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);
            CheckBox_CimzettMegorzes.Checked = true;
            BejovoPeldanyListPanel1.Partner_Id_Cimzett = UI.GetSession(Page, session_CimzettId);
        }
        //Ragszám
        if (UI.IsStoredValueInSession(Page, RagszamRequiredTextBox.ID))
        {
            RagszamRequiredTextBox.Text = UI.GetSessionStoredValue(Page, RagszamRequiredTextBox.ID, cbRagszamMegorzes);
        }

        if (Command == CommandName.New)
        {
            //Beerkezes Ido
            if (UI.IsStoredValueInSession(Page, BeerkezesIdeje_CalendarControl.ID))
            {
                BeerkezesIdeje_CalendarControl.Text = UI.GetSessionStoredValue(Page, BeerkezesIdeje_CalendarControl.ID, cbBeerkezesIdopontjaMegorzes);
            }

            if (UI.IsStoredValueInSession(Page, FeladasiIdo_CalendarControl.ID))
            {
                FeladasiIdo_CalendarControl.Text = UI.GetSessionStoredValue(Page, FeladasiIdo_CalendarControl.ID, cbFeladasIdoMegorzes);
            }

            //Bontasi Megjegyzes
            if (UI.IsStoredValueInSession(Page, bontasMegjegyzesTextBox.ID))
            {
                bontasMegjegyzesTextBox.Text = UI.GetSessionStoredValue(Page, bontasMegjegyzesTextBox.ID, cbBontasiMegjegyzesMegorzes);
            }

            //bernat.laszlo added
            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            Munkaallomas_TextBox.Text = execParam_Munkaallomas.UserHostAddress;
        }
    }

    #endregion

    #region private methods

    #region Hataridok

    private string GetDefaultIntezesiHataridoUgyirat(out string ErrorMessage)
    {
        ErrorMessage = String.Empty;

        DateTime dtUgyintezesKezdete = GetUgyintezesKezdete();

        DateTime dtHatarido = Extra_Napok_Hatarido.GetDefaultIntezesiHatarido(Page, dtUgyintezesKezdete, out ErrorMessage);
        // hiba esetén kinullázzuk
        if (dtHatarido == DateTime.MinValue)
        {
            return System.Text.RegularExpressions.Regex.Replace(dtHatarido.ToShortDateString(), @"[0-9]", "0");
        }
        return dtHatarido.ToShortDateString();
    }

    private string GetDefaultVisszaerkezesiHataridoPld(out string ErrorMessage)
    {
        // jelenleg ugyanúgy számoljuk
        return GetDefaultIntezesiHataridoUgyirat(out ErrorMessage);
    }
    private string GetIntezesiHataridoUgyirat(string Idoegyseg, string IntezesiIdo)
    {
        string ErrorMessage = String.Empty;

        DateTime dtUgyintezesKezdete = GetUgyintezesKezdete();

        ExecParam execParam = UI.SetExecParamDefault(Page);
        DateTime dtHatarido = Extra_Napok_Hatarido.GetIntezesiHataridoByIdoegyseg(Page.Cache, execParam, Idoegyseg, IntezesiIdo, dtUgyintezesKezdete, out ErrorMessage);

        return dtHatarido.ToShortDateString();
    }

    private void SetDefaultHataridok()
    {
        //string _30DaysAfterToday = DateTime.Now.AddDays(30).ToShortDateString();
        //UgyUgyirat_Hatarido_CalendarControl.Text = _30DaysAfterToday;
        if (!Uj_Ugyirat_Hatarido_Kezeles)
        {
            string errorMessage = String.Empty;
            UgyUgyirat_Hatarido_CalendarControl.Text = GetDefaultIntezesiHataridoUgyirat(out errorMessage);
            UgyUgyirat_Hatarido_CalendarControl.TextBox.ToolTip = (String.IsNullOrEmpty(errorMessage) ? "" : errorMessage + "\n") + Resources.Form.UI_IntezesiIdoDefault_ToolTip;

            // BLG_1020
            IraIrat_Hatarido_CalendarControl.Text = GetDefaultIntezesiHataridoUgyirat(out errorMessage);
            IraIrat_Hatarido_CalendarControl.TextBox.ToolTip = (String.IsNullOrEmpty(errorMessage) ? "" : errorMessage + "\n") + Resources.Form.UI_IntezesiIdoDefault_ToolTip;

        }

        //IraIrat_Hatarido_CalendarControl.Text = UgyUgyirat_Hatarido_CalendarControl.Text;
        //IraIrat_Hatarido_CalendarControl.TextBox.ToolTip = Resources.Form.UI_IntezesiIdoUgyiratbol_ToolTip;
        // BLG_1020
        //IraIrat_Hatarido_CalendarControl.Text = "";
        //IraIrat_Hatarido_CalendarControl.TextBox.ToolTip = "";

        UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = "1";
        // kitolhat? | kötött?
        UgyiratHataridoKitolas_Irat_HiddenField.Value = "1|0";

        IktatasDatuma_Ugyirat_HiddenField.Value = "";
        IktatasDatuma_Irat_HiddenField.Value = "";
    }
    #endregion Hataridok

    /// <summary>
    /// Komponensek láthatóságának beállítása
    /// </summary>
    /// <param name="_command">New, Modify, View</param>
    /// <param name="_mode">BejovoIratIktatas, BelsoIratIktatas, stb...</param>
    private void SetComponentsVisibility(String _command)
    {
        #region TabFooter gombok állítása

        if (_command == CommandName.Modify || _command == CommandName.New)
        {
            // A tomeges felvitelt segito gombok megjelenitese:
            TabFooter1.ImageButton_SaveAndClose.Visible = true;
            // ez csak New-nál kell
            if (_command == CommandName.New)
            {
                TabFooter1.ImageButton_SaveAndNew.Visible = false;
                TabFooter1.ImageButton_SaveAndClose.Visible = false;
                TabFooter1.ImageButton_Cancel.Visible = true;
                TabFooter1.ImageButton_Cancel.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            }
            else
            {
                // ezen a formon le kell plusszban tiltani a Cancel gombot, mert nincsen ertelme, mert alul van bezar gomb!!!!
                TabFooter1.ImageButton_Cancel.Visible = false;
            }
        }

        #endregion

        if (Mode == ErkeztetesTipus.EmailErkeztetes || isHivataliKapusErkeztetes)
        {
            if (SubMode == EmailBoritekTipus.Kuldemeny)
            {
                KuldemenyPanel.Visible = true;
                EmailKuldemenyPanel.Visible = false;
                eBeadvanyokPanel.Visible = false;

                VonalkodTextBoxVonalkod.ReadOnly = true;
                cbMegorzesJelzo.Visible = false;
                labelBoritoTipus.Visible = false;
                KodtarakDropDownListBoritoTipus.Visible = false; //ReadOnly = true;

                trKuldemenyPanelErkeztetokonyv.Visible = false; //?
                //ErkeztetoKonyvekDropDrownList.ReadOnly = true;
                //textAdoszam.ReadOnly = true;

                trKuldemenyPanelErkeztetoSzam.Visible = true;
                ErkeztetoSzam_TextBox.ReadOnly = true;

                trKuldemenyPanelKuldo.Visible = true; //?
                Bekuldo_PartnerTextBox.ReadOnly = true;
                Kuld_CimId_CimekTextBox.ReadOnly = true;

                // BUG_7678
                tr_feladasiIdo.Visible = true;

                trKuldemenyPanelBeerkezesModja.Visible = true; //?
                trKuldemenyPanelBeerkezesIdopontja.Visible = true; //bernat.laszlo added
                KuldesMod_DropDownList.ReadOnly = true;
                cbBeerkezesIdopontjaMegorzes.Visible = false;
                cbFeladasIdoMegorzes.Visible = false;
                BeerkezesIdeje_CalendarControl.ReadOnly = true;

                trKuldemenyPanelBonto.Visible = false;
                //Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.ReadOnly = true;
                //Kuld_FelbontasDatuma_CalendarControl.ReadOnly = true;

                trKuldemenyPanelKuldoIktatoszama.Visible = true; //?
                HivatkozasiSzamUserControl_Kuldemeny_TextBox.ReadOnly = true;
                cbRagszamMegorzes.Visible = false;
                labelRagszam.Visible = false;
                RagszamRequiredTextBox.Visible = false;//.ReadOnly = true;

                trKuldemenyPanelAdathordozoTipusa.Visible = false; //?
                //AdathordozoTipusa_DropDownList.ReadOnly = true;
                //cbBontasiMegjegyzesMegorzes.Visible = false;
                //bontasMegjegyzesTextBox.ReadOnly = true;

                trKuldemenyPanelElsodlegesAdathordozoTipusa.Visible = false; //?
                                                                             //UgyintezesModja_DropDownList.ReadOnly = true;

                trKuldemenyPanelBelsoCimzett.Visible = true; //?
                CheckBox_CimzettMegorzes.Visible = false;
                CsoportId_CimzettCsoportTextBox1.ReadOnly = true;
                Surgosseg_DropDownList.ReadOnly = true;

                //bernat.laszlo added
                trIktatasExtra.Visible = true;
                trMunkaallomas.Visible = true;
                trTevedesSerules.Visible = true;
                //bernat laszlo eddig

                mellekletekPanel.Visible = false; //?
            }
            else
            {
                //bernat.laszlo modified+added
                KuldemenyPanel.Visible = true; //KuldemenyPanel.Visible = false;
                trKuldemenyPanelVonalkod.Visible = false;
                trKuldemenyPanelErkeztetokonyv.Visible = false;
                trKuldemenyPanelErkeztetoSzam.Visible = false;
                trKuldemenyPanelKuldo.Visible = false;
                // BUG_7678
                tr_feladasiIdo.Visible = false;

                trKuldemenyPanelBeerkezesModja.Visible = true;
                trKuldemenyPanelBeerkezesIdopontja.Visible = false;
                trKuldemenyPanelBonto.Visible = false;
                trKuldemenyPanelKuldoIktatoszama.Visible = false;
                trKuldemenyPanelAdathordozoTipusa.Visible = false;
                trKuldemenyPanelElsodlegesAdathordozoTipusa.Visible = false;
                trKuldemenyPanelBelsoCimzett.Visible = false;
                trIktatasExtra.Visible = true;
                trMunkaallomas.Visible = false;
                trTevedesSerules.Visible = false;
                mellekletekPanel.Visible = false;
                tdKuld1.Visible = false;
                tdKuld2.Visible = false;
                if (isHivataliKapusErkeztetes)
                {
                    /// BUG#5432:
                    /// Hivatali kapus érkeztetésnél nem minden környezetben létezik ez a kódtárelem: KodTarak.KULD_KEZB_MODJA.e_ugyintezes,
                    /// és akár több érték is szóbajöhet a kódcsoporton belül (pl. NMHH-ban).
                    /// Ha nem található ez a kódtár, akkor szerkeszthetővé tesszük a legördülő listát, az alapértelmezett érték pedig 
                    /// a KodTarak.KULD_KEZB_MODJA.hivataliKapun lesz.
                    /// 
                    if (Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(kcs_KULD_KEZB_MODJA, this.Page).ContainsKey(KodTarak.KULD_KEZB_MODJA.e_ugyintezes))
                    {
                        Kezbesites_modja_DropDownList.SelectedValue = KodTarak.KULD_KEZB_MODJA.e_ugyintezes;
                        Kezbesites_modja_DropDownList.ReadOnly = true;
                    }
                    else
                    {
                        // Nem található a default KodTarak.KULD_KEZB_MODJA.e_ugyintezes érték, szerkeszthető módban hagyjuk a mezőt
                        Kezbesites_modja_DropDownList.SelectedValue = KodTarak.KULD_KEZB_MODJA.hivataliKapun;
                    }
                }
                else
                {
                    // EmailErkeztetes:
                    Kezbesites_modja_DropDownList.ReadOnly = true;
                    Kezbesites_modja_DropDownList.SelectedValue = KodTarak.KULD_KEZB_MODJA.email;
                }
                CimzesTipusa_DropDownList.ReadOnly = true;
                // BUG_4809
                //CimzesTipusa_DropDownList.Clear();
                //bernat.laszlo eddig
                EmailKuldemenyPanel.Visible = !isHivataliKapusErkeztetes;
                eBeadvanyokPanel.Visible = isHivataliKapusErkeztetes;
                ImageButton_MigraltKereses.Visible = false;
                // Emailbõl érkeztetés esetén adathordozó típusa elektronikus
                AdathordozoTipusa_DropDownList.SelectedValue = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            }
        }
        else
        {
            KuldemenyPanel.Visible = true;
            EmailKuldemenyPanel.Visible = false;
            eBeadvanyokPanel.Visible = false;
        }

        SetElektronikusIratVonalkodControl();

        if (isTUKRendszer)
        {
            tr_fizikaihely.Visible = true;
            IrattariHelyLevelekDropDownTUK.Visible = true;
            IrattariHelyLevelekDropDownTUK.DropDownList.AutoPostBack = false;
            //JavaScripts.SetFocus(IrattariHelyLevelekDropDownTUK);              
        }
    }

    private void SetAllPanelVisibility(bool value)
    {
        KuldemenyPanel.Visible = value;
        EmailKuldemenyPanel.Visible = value;
        Ugyirat_Panel.Visible = value;
        StandardTargyszavakPanel.Visible = value;
        StandardTargyszavakPanel_Iratok.Visible = value;
        IratPanel.Visible = value;
        ResultPanel.Visible = value;
        ErkeztetesResultPanel.Visible = value;
        UgyiratSzerelesiLista1.Visible = value;
        eBeadvanyokPanel.Visible = value;
        TipusosTargyszavakPanel_Iratok.Visible = value;
        BejovoPeldanyListPanel1.Visible = value;
    }

    /// <summary>
    /// Ha nem Iktatandó-t választ ki, csak az érkeztetõpanel látszódik:
    /// </summary>    
    private void EmailIktatasiKotelezettseg_SelectedIndexChanged(object sender, EventArgs e)
    {
        // csak email érkeztetésnél:
        if (Mode == ErkeztetesTipus.EmailErkeztetes || Mode == ErkeztetesTipus.HivataliKapusErkeztetes)
        {
            string selectedValue = isHivataliKapusErkeztetes ? eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue : Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue;
            if (selectedValue != KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando)
            {
                // Csak érkeztetés:
                EmailKezelesSelector.SetSelectedValue(ErkeztetesVagyIktatasRadioButtonList
                    , EmailKezelesSelector.EmailKezelesTipus.CsakErkeztetes);

                ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(sender, e);
            }
        }
    }

    /// <summary>
    /// Érkeztetés és iktatás közti váltás a felületen
    /// </summary>    
    private void ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        // csak email érkeztetésnél (egyelõre)
        if (Mode == ErkeztetesTipus.EmailErkeztetes
            || Mode == ErkeztetesTipus.HivataliKapusErkeztetes)
        {
            if (!IsCsakErkeztetes)
            {
                // Érkeztetés és iktatás (vagy Érkeztetés és munkapéldány létrehozás)
                if (SubMode == EmailBoritekTipus.Kuldemeny)
                {
                    EmailKuldemenyPanel.Visible = false;
                    eBeadvanyokPanel.Visible = false;
                    KuldemenyPanel.Visible = true;
                }
                else
                {
                    EmailKuldemenyPanel.Visible = !isHivataliKapusErkeztetes;
                    eBeadvanyokPanel.Visible = isHivataliKapusErkeztetes;
                    KuldemenyPanel.Visible = false;
                    // Az iktatási kötelezettséget vissza kell állítani Iktatandóra:
                    Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
                    eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
                }
                Ugyirat_Panel.Visible = true;
                UgyiratSzerelesiLista1.Visible = true;
                StandardTargyszavakPanel.Visible = true;
                StandardTargyszavakPanel_Iratok.Visible = true;
                IratPanel.Visible = true;
                TipusosTargyszavakPanel_Iratok.Visible = true;
                BejovoPeldanyListPanel1.Visible = PeldanyokSorszamaMegadhato;

                if (IsElozmenyUgyirat)
                {
                    RaiseErrorLezartIktatokonyvbeNemIktathat(Mode);
                }
                SetComponentsVisibility(Command);
            }
            else
            {
                // csak az email érkeztetés panel látszódik:
                SetAllPanelVisibility(false);
                EmailKuldemenyPanel.Visible = !isHivataliKapusErkeztetes;
                eBeadvanyokPanel.Visible = isHivataliKapusErkeztetes;
                KezelesiFeljegyzesPanelKuldemeny.ValidateIfFeladatIsNotEmpty = IsCsakErkeztetes;
            }

            // csak munkaanyag létrehozáskor látszik (fõszám igénylés)
            tr_JavasoltElozmeny.Visible = IsMunkaanyagLetrehozas;
        }
    }


    // form --> business object
    private EREC_IraIratok GetBusinessObjectFromComponents()
    {
        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);

        erec_IraIratok.Targy = IraIrat_Targy_RequiredTextBox.Text;
        erec_IraIratok.Updated.Targy = pageView.GetUpdatedByView(IraIrat_Targy_RequiredTextBox);

        //erec_IraIratok.Kategoria = IraIrat_Kategoria_KodtarakDropDownList.SelectedValue;
        //erec_IraIratok.Updated.Kategoria = pageView.GetUpdatedByView(IraIrat_Kategoria_KodtarakDropDownList);

        erec_IraIratok.Irattipus = IraIrat_Irattipus_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.Irattipus = pageView.GetUpdatedByView(IraIrat_Irattipus_KodtarakDropDownList);

        erec_IraIratok.KiadmanyozniKell = (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";
        erec_IraIratok.Updated.KiadmanyozniKell = pageView.GetUpdatedByView(IraIrat_KiadmanyozniKell_CheckBox);

        erec_IraIratok.Minosites = ktDropDownListIratMinosites.SelectedValue;
        erec_IraIratok.Updated.Minosites = pageView.GetUpdatedByView(ktDropDownListIratMinosites);

        erec_IraIratok.FelhasznaloCsoport_Id_Ugyintez = Irat_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IraIratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(Irat_Ugyintezo_FelhasznaloCsoportTextBox);

        // BLG_1020
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            LoadUgyintezesiNapokBoFromComponents(ref erec_IraIratok);

            erec_IraIratok.IntezesiIdoegyseg = IratIntezesiIdoegyseg_KodtarakDropDownList.SelectedValue;
            erec_IraIratok.Updated.IntezesiIdoegyseg = pageView.GetUpdatedByView(IratIntezesiIdoegyseg_KodtarakDropDownList);
        }
        erec_IraIratok.Hatarido = IraIrat_Hatarido_CalendarControl.Text;
        erec_IraIratok.Updated.Hatarido = pageView.GetUpdatedByView(IraIrat_Hatarido_CalendarControl);

        //erec_IraIratok.FelhasznaloCsoport_Id_Kiadmany = IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox.Id_HiddenField;
        //erec_IraIratok.Updated.FelhasznaloCsoport_Id_Kiadmany = pageView.GetUpdatedByView(IraIrat_Kiadmanyozo_FelhasznaloCsoportTextBox);        

        erec_IraIratok.AdathordozoTipusa = AdathordozoTipusa_DropDownList.SelectedValue;
        erec_IraIratok.Updated.AdathordozoTipusa = pageView.GetUpdatedByView(AdathordozoTipusa_DropDownList);

        //erec_IraIratok.Jelleg = KodTarak.IRAT_JELLEG.Webes;
        erec_IraIratok.Jelleg = IratJellegKodtarakDropDown.SelectedValue;
        erec_IraIratok.Updated.Jelleg = true;

        // nekrisz : munkaállomás mentése
        erec_IraIratok.Munkaallomas = Munkaallomas_TextBox.Text;
        erec_IraIratok.Updated.Munkaallomas = true;

        erec_IraIratok.Base.Ver = Record_Ver_HiddenField.Value;
        erec_IraIratok.Base.Updated.Ver = true;

        #region SAKKORA
        SetIratHatasaToBo(ref erec_IraIratok);
        SetLezarasOkaToBo(ref erec_IraIratok);
        SetEljarasiSzakaszFokToBo(ref erec_IraIratok);
        #endregion SAKKORA

        erec_IraIratok.Ugy_Fajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.Ugy_Fajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);

        FillIratFromEBeadvanyElozmenyAdatok(ref erec_IraIratok);

        return erec_IraIratok;
    }

    // Az ügyiratokra vonatkozó néhány komponensbõl egy EREC_UgyUgyiratok objektum felépítése
    private EREC_UgyUgyiratok GetBusinessObjectFromComponents_EREC_UgyUgyiratok()
    {
        EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_UgyUgyiratok.Updated.SetValueAll(false);
        erec_UgyUgyiratok.Base.Updated.SetValueAll(false);

        // csak New -nál van értelme, és ha nincs elõzmény (nincs betöltve ügyirat) vagy az elõzmény lezárt iktatókönyvben van
        if (Command == CommandName.New
            && (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value) || !String.IsNullOrEmpty(LezartIktatokonyv_HiddenField.Value)))
        {
            // BLG_44
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                erec_UgyUgyiratok.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(Ugykor_DropDownList);
            }
            else
            {
                erec_UgyUgyiratok.IraIrattariTetel_Id = IrattariTetelszam_DropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(IrattariTetelszam_DropDownList);

            }
            //erec_UgyUgyiratok.UgyTipus = UgyUgyirat_Ugytipus_KodtarakDropDownList.SelectedValue;
            //erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_KodtarakDropDownList);

            erec_UgyUgyiratok.UgyTipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.UgyTipus = pageView.GetUpdatedByView(UgyUgyirat_Ugytipus_DropDownList);

            if (Uj_Ugyirat_Hatarido_Kezeles)
            {
                erec_UgyUgyiratok.IntezesiIdo = inputUgyUgyintezesiNapok.Value;
                erec_UgyUgyiratok.Updated.IntezesiIdo = pageView.GetUpdatedByView(inputUgyUgyintezesiNapok);
                erec_UgyUgyiratok.IntezesiIdoegyseg = IntezesiIdoegyseg_KodtarakDropDownList.SelectedValue;
                erec_UgyUgyiratok.Updated.IntezesiIdoegyseg = pageView.GetUpdatedByView(IntezesiIdoegyseg_KodtarakDropDownList);
            }

            erec_UgyUgyiratok.Hatarido = UgyUgyirat_Hatarido_CalendarControl.Text;
            erec_UgyUgyiratok.Updated.Hatarido = pageView.GetUpdatedByView(UgyUgyirat_Hatarido_CalendarControl);

            erec_UgyUgyiratok.Targy = UgyUgyirat_Targy_RequiredTextBox.Text;
            erec_UgyUgyiratok.Updated.Targy = pageView.GetUpdatedByView(UgyUgyirat_Targy_RequiredTextBox);

            // kezelõ:
            erec_UgyUgyiratok.Csoport_Id_Felelos = UgyUgyiratok_CsoportId_Felelos.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(UgyUgyiratok_CsoportId_Felelos);

            //ügyintézõ
            erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.FelhasznaloCsoport_Id_Ugyintez = pageView.GetUpdatedByView(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox);

            // ügyfelelõs:
            erec_UgyUgyiratok.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Csoport_Id_Ugyfelelos = pageView.GetUpdatedByView(Ugyfelelos_CsoportTextBox);

            // ügyindító/ügyfél
            erec_UgyUgyiratok.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Partner_Id_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
            erec_UgyUgyiratok.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
            erec_UgyUgyiratok.Updated.NevSTR_Ugyindito = pageView.GetUpdatedByView(Ugy_PartnerId_Ugyindito_PartnerTextBox);
            erec_UgyUgyiratok.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
            erec_UgyUgyiratok.Updated.Cim_Id_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);
            erec_UgyUgyiratok.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;
            erec_UgyUgyiratok.Updated.CimSTR_Ugyindito = pageView.GetUpdatedByView(CimekTextBoxUgyindito);

            // régi rendszer iktatószáma:
            if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value) && !string.IsNullOrEmpty(regiAzonositoTextBox.Text))
            {
                erec_UgyUgyiratok.RegirendszerIktatoszam = regiAzonositoTextBox.Text;
                erec_UgyUgyiratok.Updated.RegirendszerIktatoszam = true;
                IktatasiParameterek.RegiAdatAzonosito = regiAzonositoTextBox.Text;
                IktatasiParameterek.RegiAdatId = MigraltUgyiratID_HiddenField.Value;
            }

            // BLG_44
            erec_UgyUgyiratok.Ugy_Fajtaja = UGY_FAJTAJA_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.Ugy_Fajtaja = pageView.GetUpdatedByView(UGY_FAJTAJA_KodtarakDropDownList);

            if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page)) )
            {
                erec_UgyUgyiratok.UgyintezesKezdete = Contentum.eUtility.Sakkora.GetUgyUgyintezesKezdeteDefaultGlobal(UI.SetExecParamDefault(Page));
            }
            else
            {
                erec_UgyUgyiratok.UgyintezesKezdete =
                  _iratForm.IsUgyiratUgyintezesKezdeteModosithato() && _iratForm.IsUgyintezesKezdeteVisible ? CalendarControl_UgyintezesKezdete.Text // BLG_8826
                  : GetUgyintezesKezdete().ToString();
            }
            erec_UgyUgyiratok.Updated.UgyintezesKezdete = true;

        }
        else if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            if (Command == CommandName.New)
            {
                // Alszámra iktatás és speciális jogok esetén, ha változott az ügyirat határideje, akkor azt is módosítani kell
                #region ügyirat határidõ módosítás, ha szükséges
                if (((!String.IsNullOrEmpty(LezartIktatokonyv_HiddenField.Value) || (IsUgyintezo() && IsOrzo())) && bUgyiratHataridoModositasiJog) && IsUgyiratHataridoChanged())
                {
                    IktatasiParameterek.UgyiratUjHatarido = UgyUgyirat_Hatarido_CalendarControl.Text;
                }
                #endregion ügyirat határidõ módosítás, ha szükséges
            }
        }

        if (Command == CommandName.New && IsMunkaanyagLetrehozas)
        {
            if (!String.IsNullOrEmpty(JavasoltElozmenyTextBox.Text))
            {
                //LZS - obsolate
                //erec_UgyUgyiratok.Base.Note = String.Format("<JavasoltElozmeny>{0}</JavasoltElozmeny>", JavasoltElozmenyTextBox.Text);

                #region LZS - BUG_11081
                //erec_UgyUgyiratok.Base.Note deserializálását követően beírjuk a JavasoltElozmenyTextBox.Text-et a 
                //note.JavasoltElozmenyek property-be, majd vissza-serializálva beírjuk a erec_UgyUgyiratok.Base.Note-ba.
                Note_JSON note;
                try
                {
                    note = JsonConvert.DeserializeObject<Note_JSON>(erec_UgyUgyiratok.Base.Note);
                }
                catch
                {
                    note = null;
                }

                if (note != null)
                {
                    note.JavasoltElozmenyek = JavasoltElozmenyTextBox.Text;

                    erec_UgyUgyiratok.Base.Note = JsonConvert.SerializeObject(note);
                    erec_UgyUgyiratok.Base.Updated.Note = pageView.GetUpdatedByView(JavasoltElozmenyTextBox);
                }
                #endregion
            }
        }
        #region SAKKORA ALLAPOTA
        if (!string.IsNullOrEmpty(IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue))
        {
            erec_UgyUgyiratok.SakkoraAllapot = IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue;
            erec_UgyUgyiratok.Updated.SakkoraAllapot = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);
        }
        else if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page, new ExecParam()))) //SAKKORAS UGYFEL
        {
            erec_UgyUgyiratok.SakkoraAllapot = KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE;
            erec_UgyUgyiratok.Updated.SakkoraAllapot = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);
        }
        #endregion


        if (isTUKRendszer)
        {
            erec_UgyUgyiratok.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_UgyUgyiratok.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
            //erec_UgyUgyiratok.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
            erec_UgyUgyiratok.IrattariHely = IrattariHelyLevelekDropDownTUK.SelectedText;
            erec_UgyUgyiratok.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
        }

        #region REFRESH UGYINTEZES KEZDETE
        Contentum.eUtility.Sakkora.SetUgyUgyintezesKezdeteGlobal(UI.SetExecParamDefault(Page), ref erec_UgyUgyiratok);
        #endregion

        return erec_UgyUgyiratok;
    }

    private bool IsUgyiratHataridoChanged()
    {
        DateTime elozmenyDate;
        DateTime ugyiratDate;
        bool isElozmenyDateParsed = DateTime.TryParse(ElozmenyUgyiratHatarido_HiddenField.Value, out elozmenyDate);
        bool isUgyiratDateParsed = DateTime.TryParse(UgyUgyirat_Hatarido_CalendarControl.Text, out ugyiratDate);
        if (isElozmenyDateParsed && isUgyiratDateParsed && elozmenyDate != ugyiratDate)
        {
            return true;
        }
        return false;
    }

    private bool IsUgyintezo()
    {
        return (UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField == FelhasznaloProfil.FelhasznaloId(Page));
    }

    private bool IsOrzo()
    {
        return (FelhasznaloCsoportIdOrzo_HiddenField.Value == FelhasznaloProfil.FelhasznaloId(Page));
    }

    private EREC_KuldKuldemenyek GetBusinessObjectFromComponents_EREC_KuldKuldemenyek()
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = new EREC_KuldKuldemenyek();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_KuldKuldemenyek.Updated.SetValueAll(false);
        erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

        if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
        {
            #region Küldemény

            erec_KuldKuldemenyek.IraIktatokonyv_Id = ErkeztetoKonyvekDropDrownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(ErkeztetoKonyvekDropDrownList);

            erec_KuldKuldemenyek.Partner_Id_Bekuldo = Bekuldo_PartnerTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = pageView.GetUpdatedByView(Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.NevSTR_Bekuldo = Bekuldo_PartnerTextBox.Text;
            erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = pageView.GetUpdatedByView(Bekuldo_PartnerTextBox);

            #region BUG 5197
            erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt = Bekuldo_PartnerTextBox.KapcsoltPartnerDropDownValue;
            erec_KuldKuldemenyek.Updated.Partner_Id_BekuldoKapcsolt = pageView.GetUpdatedByView(Bekuldo_PartnerTextBox);

            Bekuldo_PartnerTextBox.CheckPartnerCimek(Bekuldo_PartnerTextBox.KapcsoltPartnerDropDownValue, Kuld_CimId_CimekTextBox.Id_HiddenField, FormHeader1.Record_Ver, EErrorPanel1);
            #endregion

            erec_KuldKuldemenyek.Cim_Id = Kuld_CimId_CimekTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Cim_Id = pageView.GetUpdatedByView(Kuld_CimId_CimekTextBox);

            erec_KuldKuldemenyek.CimSTR_Bekuldo = Kuld_CimId_CimekTextBox.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = pageView.GetUpdatedByView(Kuld_CimId_CimekTextBox);

            erec_KuldKuldemenyek.KuldesMod = KuldesMod_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.KuldesMod = pageView.GetUpdatedByView(KuldesMod_DropDownList);

            // CR#2282: Itt nincs külön beviteli mezõ: Küldemény tárgyát átvesszük az iratból
            erec_KuldKuldemenyek.Targy = IraIrat_Targy_RequiredTextBox.Text;
            erec_KuldKuldemenyek.Updated.Targy = pageView.GetUpdatedByView(IraIrat_Targy_RequiredTextBox);

            if (!_Load)
            {
                if (string.IsNullOrEmpty(BeerkezesIdeje_CalendarControl.Text))
                    BeerkezesIdeje_CalendarControl.Text = Contentum.eUtility.Sakkora.GetUgyUgyintezesKezdeteDefaultGlobal(UI.SetExecParamDefault(Page));

                if (string.IsNullOrEmpty(Kuld_FelbontasDatuma_CalendarControl.Text))
                    Kuld_FelbontasDatuma_CalendarControl.Text = DateTime.Now.ToString();

                DateTime beerkezes;
                if (!DateTime.TryParse(BeerkezesIdeje_CalendarControl.Text, out beerkezes))
                {
                    throw new FormatException("Az érkeztetés idõpontjának megadott érték nem megfelelõ formátumú!");
                }
                if (beerkezes != DateTime.MinValue && beerkezes > DateTime.Now)
                {
                    throw new FormatException("Az érkeztetés idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
                }
                erec_KuldKuldemenyek.BeerkezesIdeje = BeerkezesIdeje_CalendarControl.Text;
                erec_KuldKuldemenyek.Updated.BeerkezesIdeje = pageView.GetUpdatedByView(BeerkezesIdeje_CalendarControl);

                // BUG_7678
                if (!string.IsNullOrEmpty(FeladasiIdo_CalendarControl.Text))
                {
                    DateTime feladasiIdo;
                    if (!DateTime.TryParse(FeladasiIdo_CalendarControl.Text, out feladasiIdo))
                    {
                        // BUG_8604
                        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "A feladási idõnek megadott érték nem megfelelõ formátumú!");
                        throw new FormatException("A feladási idõnek megadott érték nem megfelelõ formátumú!");
                    }
                    if (feladasiIdo > beerkezes)
                    {
                        // BUG_8604
                        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "A feladási idõ nem lehet későbbi mint a beérkezés ideje!");   
                        throw new FormatException("A feladási idõ nem lehet későbbi mint a beérkezés ideje!");
                    }
                    erec_KuldKuldemenyek.BelyegzoDatuma = FeladasiIdo_CalendarControl.Text;
                    erec_KuldKuldemenyek.Updated.BelyegzoDatuma = pageView.GetUpdatedByView(FeladasiIdo_CalendarControl);
                }
            }

            //bernat.laszlo added
            if (Command == CommandName.New)
            {
                erec_KuldKuldemenyek.Base.LetrehozasIdo = ErkeztetesIdeje_CalendarControl.Text;
                erec_KuldKuldemenyek.Base.Updated.LetrehozasIdo = true;
            }
            //bernat.laszlo eddig

            erec_KuldKuldemenyek.HivatkozasiSzam = HivatkozasiSzamUserControl_Kuldemeny_TextBox.Text;
            erec_KuldKuldemenyek.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(HivatkozasiSzamUserControl_Kuldemeny_TextBox.TextBox);

            erec_KuldKuldemenyek.AdathordozoTipusa = AdathordozoTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.AdathordozoTipusa = pageView.GetUpdatedByView(AdathordozoTipusa_DropDownList);

            // BUG_2051
            //erec_KuldKuldemenyek.UgyintezesModja = UgyintezesModja_DropDownList.SelectedValue;
            //erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(UgyintezesModja_DropDownList);
            erec_KuldKuldemenyek.UgyintezesModja = AdathordozoTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(AdathordozoTipusa_DropDownList);
            erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa = UgyintezesModja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.ElsodlegesAdathordozoTipusa = pageView.GetUpdatedByView(UgyintezesModja_DropDownList);

            erec_KuldKuldemenyek.RagSzam = RagszamRequiredTextBox.Text;
            erec_KuldKuldemenyek.Updated.RagSzam = pageView.GetUpdatedByView(RagszamRequiredTextBox);

            erec_KuldKuldemenyek.Csoport_Id_Cimzett = CsoportId_CimzettCsoportTextBox1.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = pageView.GetUpdatedByView(CsoportId_CimzettCsoportTextBox1);

            erec_KuldKuldemenyek.Surgosseg = Surgosseg_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.Surgosseg = pageView.GetUpdatedByView(Surgosseg_DropDownList);

            erec_KuldKuldemenyek.BarCode = VonalkodTextBoxVonalkod.Text;
            erec_KuldKuldemenyek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBoxVonalkod);

            erec_KuldKuldemenyek.BoritoTipus = KodtarakDropDownListBoritoTipus.SelectedValue;
            erec_KuldKuldemenyek.Updated.BoritoTipus = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            //bernat.laszlo added
            #region Iktatást nem igényel setup
            // CR 3113 : Iktatást nem igényel -> iktatást igényel
            // !! Fordított kezelés :(
            //Itt nincs értelme kiválasztani, ezért disabled
            //if (Ikt_n_igeny_Igen_RadioButton.Checked)
            //{
            erec_KuldKuldemenyek.IktatastNemIgenyel = "1";
            //erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = pageView.GetUpdatedByView(Ikt_n_igeny_Igen_RadioButton);
            erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = true;
            //}
            //else
            //{
            //    erec_KuldKuldemenyek.IktatastNemIgenyel = "0";
            //    erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = pageView.GetUpdatedByView(Ikt_n_igeny_Igen_RadioButton);
            //}

            #endregion

            erec_KuldKuldemenyek.KezbesitesModja = Kezbesites_modja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.KezbesitesModja = pageView.GetUpdatedByView(Kezbesites_modja_DropDownList);

            erec_KuldKuldemenyek.CimzesTipusa = CimzesTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.CimzesTipusa = pageView.GetUpdatedByView(CimzesTipusa_DropDownList);

            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            erec_KuldKuldemenyek.Munkaallomas = execParam_Munkaallomas.UserHostAddress;
            erec_KuldKuldemenyek.Updated.Munkaallomas = pageView.GetUpdatedByView(Munkaallomas_TextBox);

            #region Sérült küldemény setup
            if (serult_kuld_Igen_RadioButton.Checked)
            {
                erec_KuldKuldemenyek.SerultKuldemeny = "1";
                erec_KuldKuldemenyek.Updated.SerultKuldemeny = pageView.GetUpdatedByView(serult_kuld_Igen_RadioButton);
            }
            else
            {
                erec_KuldKuldemenyek.SerultKuldemeny = "0";
                erec_KuldKuldemenyek.Updated.SerultKuldemeny = pageView.GetUpdatedByView(serult_kuld_Igen_RadioButton);
            }
            #endregion

            #region Téves címzés setup
            if (teves_cim_Igen_RadioButton.Checked)
            {
                erec_KuldKuldemenyek.TevesCimzes = "1";
                erec_KuldKuldemenyek.Updated.TevesCimzes = pageView.GetUpdatedByView(teves_cim_Igen_RadioButton);
            }
            else
            {
                erec_KuldKuldemenyek.TevesCimzes = "0";
                erec_KuldKuldemenyek.Updated.TevesCimzes = pageView.GetUpdatedByView(teves_cim_Igen_RadioButton);
            }
            #endregion

            #region Téves érkeztetés setup
            if (teves_erk_Igen_RadioButton.Checked)
            {
                erec_KuldKuldemenyek.TevesErkeztetes = "1";
                erec_KuldKuldemenyek.Updated.TevesErkeztetes = pageView.GetUpdatedByView(teves_erk_Igen_RadioButton);
            }
            else
            {
                erec_KuldKuldemenyek.TevesErkeztetes = "0";
                erec_KuldKuldemenyek.Updated.TevesErkeztetes = pageView.GetUpdatedByView(teves_erk_Igen_RadioButton);
            }
            #endregion
            //bernat.laszlo eddig

            switch (KodtarakDropDownListBoritoTipus.SelectedValue)
            {
                case KodTarak.BoritoTipus.Boritek:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                    break;
                case KodTarak.BoritoTipus.Irat:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.Yes;
                    break;
                case KodTarak.BoritoTipus.EldobhatoBoritek:
                    erec_KuldKuldemenyek.MegorzesJelzo = Constants.Database.No;
                    break;
            }

            erec_KuldKuldemenyek.Updated.MegorzesJelzo = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            erec_KuldKuldemenyek.IktatniKell = KodTarak.IKTATASI_KOTELEZETTSEG.Iktatando;
            erec_KuldKuldemenyek.Updated.IktatniKell = true;

            GetFelbontoPanelDatas(erec_KuldKuldemenyek);

            #endregion
        }
        else if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            #region Email

            erec_KuldKuldemenyek.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_KuldKuldemenyek.Updated.AdathordozoTipusa = true;

            erec_KuldKuldemenyek.IraIktatokonyv_Id = Email_IraIktatoKonyvekDropDownList1.SelectedValue;
            erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(Email_IraIktatoKonyvekDropDownList1);

            erec_KuldKuldemenyek.Partner_Id_Bekuldo = Email_Bekuldo_PartnerTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = pageView.GetUpdatedByView(Email_Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.NevSTR_Bekuldo = Email_Bekuldo_PartnerTextBox.Text;
            erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = pageView.GetUpdatedByView(Email_Bekuldo_PartnerTextBox);

            erec_KuldKuldemenyek.Cim_Id = Email_KuldCim_CimekTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Cim_Id = pageView.GetUpdatedByView(Email_KuldCim_CimekTextBox);

            erec_KuldKuldemenyek.CimSTR_Bekuldo = Email_KuldCim_CimekTextBox.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = pageView.GetUpdatedByView(Email_KuldCim_CimekTextBox);

            erec_KuldKuldemenyek.KuldesMod = KodTarak.KULDEMENY_KULDES_MODJA.E_mail;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;

            // Tárgy:
            erec_KuldKuldemenyek.Targy = Email_Targy_TextBox.Text;
            erec_KuldKuldemenyek.Updated.Targy = true;

            //bernat.laszlo added
            erec_KuldKuldemenyek.KezbesitesModja = Kezbesites_modja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.KezbesitesModja = pageView.GetUpdatedByView(Kezbesites_modja_DropDownList);

            erec_KuldKuldemenyek.CimzesTipusa = CimzesTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.CimzesTipusa = pageView.GetUpdatedByView(CimzesTipusa_DropDownList);

            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            erec_KuldKuldemenyek.Munkaallomas = execParam_Munkaallomas.UserHostAddress;
            erec_KuldKuldemenyek.Updated.Munkaallomas = pageView.GetUpdatedByView(Munkaallomas_TextBox);
            //bernat.laszlo eddig

            if (!_Load)
            {
                if (String.IsNullOrEmpty(Email_BeerkezesIdeje_CalendarControl.Text))
                    Email_BeerkezesIdeje_CalendarControl.Text = DateTime.Now.ToString();

                DateTime beerkezes;
                if (!DateTime.TryParse(Email_BeerkezesIdeje_CalendarControl.Text, out beerkezes))
                {
                    throw new FormatException("Az érkeztetés idõpontjának megadott érték nem megfelelõ formátumú!");
                }
                if (beerkezes > DateTime.Now)
                {
                    throw new FormatException("Az érkeztetés idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
                }

                erec_KuldKuldemenyek.BeerkezesIdeje = Email_BeerkezesIdeje_CalendarControl.Text;
                erec_KuldKuldemenyek.Updated.BeerkezesIdeje = pageView.GetUpdatedByView(Email_BeerkezesIdeje_CalendarControl);

                // BUG_7678
                if (!String.IsNullOrEmpty(Email_FeladasiIdo_CalendarControl.Text))
                {
                    DateTime feladasiIdo;
                    if (!DateTime.TryParse(Email_FeladasiIdo_CalendarControl.Text, out feladasiIdo))
                    {
                        // BUG_8604
                        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "A feladási idõnek megadott érték nem megfelelõ formátumú!");                       
                        throw new FormatException("A feladási idõnek megadott érték nem megfelelõ formátumú!");
                    }
                    if (feladasiIdo > beerkezes)
                    {
                        // BUG_8604
                        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "A feladási idõ nem lehet későbbi mint a beérkezés ideje!");
                        throw new FormatException("A feladási idõ nem lehet későbbi mint a beérkezés ideje!");
                    }
                    erec_KuldKuldemenyek.BelyegzoDatuma = Email_FeladasiIdo_CalendarControl.Text;
                    erec_KuldKuldemenyek.Updated.BelyegzoDatuma = pageView.GetUpdatedByView(Email_FeladasiIdo_CalendarControl);
                }
            }

            erec_KuldKuldemenyek.HivatkozasiSzam = Email_HivatkozasiSzam_TextBox.Text;
            erec_KuldKuldemenyek.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(Email_HivatkozasiSzam_TextBox);

            //erec_KuldKuldemenyek.UgyintezesModja = UgyintezesModja_DropDownList.SelectedValue;
            //erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(UgyintezesModja_DropDownList);

            erec_KuldKuldemenyek.Csoport_Id_Cimzett = Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = pageView.GetUpdatedByView(Email_CsoportIdCimzett_CsoportTextBox);

            // Sürgõsség: kézzel beállítva Normál-ra
            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            //erec_KuldKuldemenyek.BarCode = VonalkodTextBoxVonalkod.Text;
            //erec_KuldKuldemenyek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBoxVonalkod);

            //erec_KuldKuldemenyek.BoritoTipus = KodtarakDropDownListBoritoTipus.SelectedValue;
            //erec_KuldKuldemenyek.Updated.BoritoTipus = pageView.GetUpdatedByView(KodtarakDropDownListBoritoTipus);

            //erec_KuldKuldemenyek.MegorzesJelzo = (cbMegorzesJelzo.Checked) ? Constants.Database.Yes : Constants.Database.No;
            //erec_KuldKuldemenyek.Updated.MegorzesJelzo = pageView.GetUpdatedByView(cbMegorzesJelzo);

            erec_KuldKuldemenyek.IktatniKell = Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.IktatniKell = pageView.GetUpdatedByView(Email_IktatasiKotelezettsegKodtarakDropDownList);

            // BUG_8722 (IktatastNemIgenyel: iktatást igényel!)
            erec_KuldKuldemenyek.IktatastNemIgenyel = erec_KuldKuldemenyek.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando ? "0" : "1";
            erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = true;

            // Kezelés módja: elektronikus
            erec_KuldKuldemenyek.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = true;

            //BUG_11613
            erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa = KodTarak.ELSODLEGES_ADATHORDOZO.Elektronikus_irat;
            erec_KuldKuldemenyek.Updated.ElsodlegesAdathordozoTipusa = true;

            #endregion
        }
        else if (isHivataliKapusErkeztetes)
        {
            #region Hivatali kapu

            erec_KuldKuldemenyek.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_KuldKuldemenyek.Updated.AdathordozoTipusa = true;

            erec_KuldKuldemenyek.IraIktatokonyv_Id = eBeadvanyokPanel_IraIktatoKonyvekDropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(eBeadvanyokPanel_IraIktatoKonyvekDropDownList);

            erec_KuldKuldemenyek.NevSTR_Bekuldo = eBeadvanyokPanel_Nev.Text;
            erec_KuldKuldemenyek.Updated.NevSTR_Bekuldo = pageView.GetUpdatedByView(eBeadvanyokPanel_Nev);

            erec_KuldKuldemenyek.CimSTR_Bekuldo = eBeadvanyokPanel_FeladoCim.Text;
            erec_KuldKuldemenyek.Updated.CimSTR_Bekuldo = pageView.GetUpdatedByView(eBeadvanyokPanel_FeladoCim);

            erec_KuldKuldemenyek.KuldesMod = eBeadvanyokPanel_BeerkezesMod.SelectedValue;
            erec_KuldKuldemenyek.Updated.KuldesMod = true;

            // Tárgy:
            erec_KuldKuldemenyek.Targy = eBeadvanyokPanel_FileNev.Text;
            erec_KuldKuldemenyek.Updated.Targy = true;

            //bernat.laszlo added
            erec_KuldKuldemenyek.KezbesitesModja = Kezbesites_modja_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.KezbesitesModja = pageView.GetUpdatedByView(Kezbesites_modja_DropDownList);

            erec_KuldKuldemenyek.CimzesTipusa = CimzesTipusa_DropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.CimzesTipusa = pageView.GetUpdatedByView(CimzesTipusa_DropDownList);

            ExecParam execParam_Munkaallomas = UI.SetExecParamDefault(Page, new ExecParam());
            erec_KuldKuldemenyek.Munkaallomas = execParam_Munkaallomas.UserHostAddress;
            erec_KuldKuldemenyek.Updated.Munkaallomas = pageView.GetUpdatedByView(Munkaallomas_TextBox);
            //bernat.laszlo eddig

            if (!_Load)
            {
                DateTime beerkezes;
                if (!DateTime.TryParse(eBeadvanyokPanel_BeerkezesIdeje.Text, out beerkezes))
                {
                    throw new FormatException("Az érkeztetés idõpontjának megadott érték nem megfelelõ formátumú!");
                }
                if (beerkezes > DateTime.Now)
                {
                    throw new FormatException("Az érkeztetés idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
                }

                erec_KuldKuldemenyek.BeerkezesIdeje = eBeadvanyokPanel_BeerkezesIdeje.Text;
                erec_KuldKuldemenyek.Updated.BeerkezesIdeje = pageView.GetUpdatedByView(eBeadvanyokPanel_BeerkezesIdeje);


                // BUG_7678
                if (!string.IsNullOrEmpty(eBeadvanyokPanel_FeladasiIdo.Text))
                {
                    DateTime feladasiIdo;
                    if (!DateTime.TryParse(eBeadvanyokPanel_FeladasiIdo.Text, out feladasiIdo))
                    {
                        // BUG_8604
                        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "A feladási idõnek megadott érték nem megfelelõ formátumú!");                    
                        throw new FormatException("A feladási idõnek megadott érték nem megfelelõ formátumú!");
                    }
                    if (feladasiIdo > beerkezes)
                    {
                        // BUG_8604
                        //ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", "A feladási idõ nem lehet későbbi mint a beérkezés ideje!");              
                        throw new FormatException("A feladási idõ nem lehet későbbi mint a beérkezés ideje!");
                    }

                    erec_KuldKuldemenyek.BelyegzoDatuma = eBeadvanyokPanel_FeladasiIdo.Text;
                    erec_KuldKuldemenyek.Updated.BelyegzoDatuma = pageView.GetUpdatedByView(eBeadvanyokPanel_FeladasiIdo);
                }
            }

            erec_KuldKuldemenyek.HivatkozasiSzam = eBeadvanyokPanel_ErkeztetesiSzam.Text;
            erec_KuldKuldemenyek.Updated.HivatkozasiSzam = pageView.GetUpdatedByView(eBeadvanyokPanel_ErkeztetesiSzam);

            //erec_KuldKuldemenyek.UgyintezesModja = UgyintezesModja_DropDownList.SelectedValue;
            //erec_KuldKuldemenyek.Updated.UgyintezesModja = pageView.GetUpdatedByView(UgyintezesModja_DropDownList);

            erec_KuldKuldemenyek.Csoport_Id_Cimzett = eBeadvanyokPanel_CsoportIdCimzett_CsoportTextBox.Id_HiddenField;
            erec_KuldKuldemenyek.Updated.Csoport_Id_Cimzett = pageView.GetUpdatedByView(eBeadvanyokPanel_CsoportIdCimzett_CsoportTextBox);

            // Sürgõsség: kézzel beállítva Normál-ra
            erec_KuldKuldemenyek.Surgosseg = KodTarak.SURGOSSEG.Normal;
            erec_KuldKuldemenyek.Updated.Surgosseg = true;

            erec_KuldKuldemenyek.IktatniKell = eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue;
            erec_KuldKuldemenyek.Updated.IktatniKell = pageView.GetUpdatedByView(eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList);

            // BUG_8722 (IktatastNemIgenyel: iktatást igényel!)
            erec_KuldKuldemenyek.IktatastNemIgenyel = erec_KuldKuldemenyek.IktatniKell == KodTarak.IKTATASI_KOTELEZETTSEG.Nem_Iktatando ? "0" : "1";
            erec_KuldKuldemenyek.Updated.IktatastNemIgenyel = true;

            // Kezelés módja: elektronikus
            erec_KuldKuldemenyek.UgyintezesModja = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;
            erec_KuldKuldemenyek.Updated.UgyintezesModja = true;

            if (!string.IsNullOrEmpty(HiddenField_PR_ErkeztetesiSzam.Value))
            {
                erec_KuldKuldemenyek.KulsoAzonosito = HiddenField_PR_ErkeztetesiSzam.Value;
                erec_KuldKuldemenyek.Updated.KulsoAzonosito = true;
            }

            if (!string.IsNullOrEmpty(HiddenField_Partner_Id.Value))
            {
                erec_KuldKuldemenyek.Partner_Id_Bekuldo = HiddenField_Partner_Id.Value;
                erec_KuldKuldemenyek.Updated.Partner_Id_Bekuldo = true;
            }
            if (!string.IsNullOrEmpty(HiddenField_Cim_Id.Value))
            {
                erec_KuldKuldemenyek.Cim_Id = HiddenField_Cim_Id.Value;
                erec_KuldKuldemenyek.Updated.Cim_Id = true;
            }

            #endregion
        }

        erec_KuldKuldemenyek.Csoport_Id_Felelos = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
        erec_KuldKuldemenyek.Updated.Csoport_Id_Felelos = true;

        // WS-en állítva:
        //erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Erkeztetve;
        //erec_KuldKuldemenyek.Updated.Allapot = true;

        // iratnál beállított minõsítést vesszük át
        erec_KuldKuldemenyek.Minosites = ktDropDownListIratMinosites.SelectedValue;
        erec_KuldKuldemenyek.Updated.Minosites = pageView.GetUpdatedByView(ktDropDownListIratMinosites);

        erec_KuldKuldemenyek.Base.Ver = Record_Ver_HiddenField.Value;
        erec_KuldKuldemenyek.Base.Updated.Ver = true;

        // Áttéve a webservice-be:
        //erec_KuldKuldemenyek.PostazasIranya = KodTarak.POSTAZAS_IRANYA.Bejovo;
        //erec_KuldKuldemenyek.Updated.PostazasIranya = true;

        erec_KuldKuldemenyek.Erkeztetes_Ev = DateTime.Now.Year.ToString();
        erec_KuldKuldemenyek.Updated.Erkeztetes_Ev = true;

        // TODO: nem tudjuk miert kellenek, de kotelezoek
        erec_KuldKuldemenyek.PeldanySzam = "1";
        erec_KuldKuldemenyek.Updated.PeldanySzam = true;

        #region BLG_452
        FillKuldemenyBOFromFutarJegyzekParameters(erec_KuldKuldemenyek);
        #endregion BLG_452

        if (isTUKRendszer)
        {
            erec_KuldKuldemenyek.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
            erec_KuldKuldemenyek.Updated.IrattarId = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
            //erec_KuldKuldemenyek.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
            erec_KuldKuldemenyek.IrattariHely = IrattariHelyLevelekDropDownTUK.SelectedText;
            erec_KuldKuldemenyek.Updated.IrattariHely = pageView.GetUpdatedByView(IrattariHelyLevelekDropDownTUK);
        }

        return erec_KuldKuldemenyek;
    }

    private void GetFelbontoPanelDatas(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (!_Load)
        {
            DateTime beerkezes;
            if (!DateTime.TryParse(BeerkezesIdeje_CalendarControl.Text, out beerkezes))
            {
                throw new FormatException("A beérkezés idõpontjának megadott érték nem megfelelõ formátumú!");
            }
            DateTime felbontas;
            if (!DateTime.TryParse(Kuld_FelbontasDatuma_CalendarControl.Text, out felbontas))
            {
                throw new FormatException("A bontás idõpontjának megadott érték nem megfelelõ formátumú!!");
            }
            if (beerkezes > DateTime.Now)
            {
                throw new FormatException("Az érkeztetés idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
            }
            if (felbontas > DateTime.Now)
            {
                throw new FormatException("A bontás idõpontja nem lehet a jelenlegi idõpontnál késõbbi!");
            }
            if (felbontas < beerkezes)
            {
                throw new FormatException("A bontás idõpontja nem lehet a beérkezés idõpontjánál korábbi!");
            }

            erec_KuldKuldemenyek.FelbontasDatuma = Kuld_FelbontasDatuma_CalendarControl.Text;
            erec_KuldKuldemenyek.Updated.FelbontasDatuma = pageView.GetUpdatedByView(Kuld_FelbontasDatuma_CalendarControl);
        }
        erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto = Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField;
        erec_KuldKuldemenyek.Updated.FelhasznaloCsoport_Id_Bonto = pageView.GetUpdatedByView(Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox);

        erec_KuldKuldemenyek.BontasiMegjegyzes = bontasMegjegyzesTextBox.Text;
        erec_KuldKuldemenyek.Updated.BontasiMegjegyzes = pageView.GetUpdatedByView(bontasMegjegyzesTextBox); ;
    }

    private void LoadKuldemenyComponents(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            // Küldemény lekérdezése:
            EREC_KuldKuldemenyekService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execparam_emailkuldemenyGet = UI.SetExecParamDefault(Page, new ExecParam());
            execparam_emailkuldemenyGet.Record_Id = id;

            Result result_emailkuldemenyGet = erec_eMailBoritekokService.Get(execparam_emailkuldemenyGet);
            if (result_emailkuldemenyGet.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailkuldemenyGet);
                ErrorUpdatePanel1.Update();
                return;
            }

            EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result_emailkuldemenyGet.Record;

            LoadKuldemenyComponents(erec_KuldKuldemenyek);

        }
    }

    private void BekuldoCimBeallitas(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (erec_KuldKuldemenyek == null) { return; }

        // BUG_11574, további beküldők keresése
        var bekuldoNeve = erec_KuldKuldemenyek.NevSTR_Bekuldo;
        var bekuldoCime = erec_KuldKuldemenyek.CimSTR_Bekuldo;
        var bekuldoId = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
        var bekuldoCimId = "";

        var bekuldokService = eRecordService.ServiceFactory.GetEREC_KuldBekuldokService();
        var bekuldokSearch = new EREC_KuldBekuldokSearch();
        bekuldokSearch.KuldKuldemeny_Id.Filter(erec_KuldKuldemenyek.Id);

        var execParam = UI.SetExecParamDefault(Page, new ExecParam());
        var bekuldokResult = bekuldokService.GetAll(execParam, bekuldokSearch);
        if (!bekuldokResult.IsError && bekuldokResult.GetCount == 1)
        {
            // csak egy beküldő van
            var bekuldoRow = bekuldokResult.Ds.Tables[0].Rows[0];
            bekuldoNeve = bekuldoRow["NevSTR"].ToString();
            bekuldoCime = bekuldoRow["CimSTR"].ToString();
            bekuldoId = bekuldoRow["Partner_Id_Bekuldo"].ToString();
            bekuldoCimId = bekuldoRow["PartnerCim_Id_Bekuldo"] == DBNull.Value ? "" : bekuldoRow["PartnerCim_Id_Bekuldo"].ToString();
        }

        Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(bekuldoId, bekuldoNeve, EErrorPanel1);

        if (String.IsNullOrEmpty(bekuldoCimId) && !String.IsNullOrEmpty(bekuldoId))
        {
            // ha nem volt beállítva a cím id, megkeressük
            bekuldoCimId = Contentum.eUtility.CimUtility.GetPartnerCimId(execParam.Clone(), bekuldoId);
        }
        CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(bekuldoCimId, bekuldoCime, EErrorPanel1);

        eBeadvanyokPanel_Nev.Text = bekuldoNeve;
        eBeadvanyokPanel_FeladoCim.Text = bekuldoCime;
    }

    private void LoadKuldemenyComponents(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (Mode == ErkeztetesTipus.KuldemenyErkeztetes)
        {
            #region Küldemény

            if (_Load)
            {
                ListItem item =
                ErkeztetoKonyvekDropDrownList.DropDownList.Items.FindByValue(erec_KuldKuldemenyek.IraIktatokonyv_Id);
                if (item != null)
                {
                    ErkeztetoKonyvekDropDrownList.SelectedValue = item.Value;
                }
            }

            Bekuldo_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_KuldKuldemenyek.Partner_Id_Bekuldo, erec_KuldKuldemenyek.NevSTR_Bekuldo, EErrorPanel1);

            #region BUG 5197
            Bekuldo_PartnerTextBox.KapcsoltPartnerDropDownValue = erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt;
            #endregion

            Kuld_CimId_CimekTextBox.SetCimekTextBoxByStringOrId(erec_KuldKuldemenyek.Cim_Id, erec_KuldKuldemenyek.CimSTR_Bekuldo, EErrorPanel1);

            KuldesMod_DropDownList.SelectedValue = erec_KuldKuldemenyek.KuldesMod;

            //BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

            HivatkozasiSzamUserControl_Kuldemeny_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

            UgyintezesModja_DropDownList.SelectedValue = erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa;

            AdathordozoTipusa_DropDownList.SelectedValue = erec_KuldKuldemenyek.AdathordozoTipusa;

            RagszamRequiredTextBox.Text = erec_KuldKuldemenyek.RagSzam;

            CsoportId_CimzettCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
            CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

            BejovoPeldanyListPanel1.Partner_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Cimzett;

            Surgosseg_DropDownList.SelectedValue = erec_KuldKuldemenyek.Surgosseg;

            //VonalkodTextBoxVonalkod.Text = erec_KuldKuldemenyek.BarCode;

            KodtarakDropDownListBoritoTipus.SelectedValue = erec_KuldKuldemenyek.BoritoTipus;

            bontasMegjegyzesTextBox.Text = erec_KuldKuldemenyek.BontasiMegjegyzes;
            LoadBontoPanel(erec_KuldKuldemenyek);

            //bernat.laszlo added
            #region Iktatást nem igényel setup
            // CR 3113 : Iktatást nem igényel -> iktatást igényel
            // Fordított kezelés
            // Elvileg itt is csak true lehet...
            if (string.IsNullOrEmpty(erec_KuldKuldemenyek.IktatastNemIgenyel.ToString()))
            {
                //Ikt_n_igeny_Nem_RadioButton.Checked = true;
                Ikt_n_igeny_Igen_RadioButton.Checked = true;

            }
            else
            {
                if (erec_KuldKuldemenyek.IktatastNemIgenyel == "1")
                {
                    //Ikt_n_igeny_Igen_RadioButton.Checked = true;
                    Ikt_n_igeny_Nem_RadioButton.Checked = true;

                }
                else
                {
                    //Ikt_n_igeny_Nem_RadioButton.Checked = true;
                    Ikt_n_igeny_Igen_RadioButton.Checked = true;

                }
            }
            #endregion

            Kezbesites_modja_DropDownList.SelectedValue = erec_KuldKuldemenyek.KezbesitesModja;

            CimzesTipusa_DropDownList.SelectedValue = erec_KuldKuldemenyek.CimzesTipusa;

            Munkaallomas_TextBox.Text = erec_KuldKuldemenyek.Munkaallomas;

            #region Sérült küldemény setup
            if (string.IsNullOrEmpty(erec_KuldKuldemenyek.SerultKuldemeny.ToString()))
            {
                serult_kuld_Nem_RadioButton.Checked = true;
            }
            else
            {
                if (erec_KuldKuldemenyek.SerultKuldemeny == "1")
                {
                    serult_kuld_Igen_RadioButton.Checked = true;
                }
                else
                {
                    serult_kuld_Nem_RadioButton.Checked = true;
                }
            }
            #endregion

            #region Téves címzés setup
            if (string.IsNullOrEmpty(erec_KuldKuldemenyek.TevesCimzes.ToString()))
            {
                teves_cim_Nem_RadioButton.Checked = true;
            }
            else
            {
                if (erec_KuldKuldemenyek.TevesCimzes == "1")
                {
                    teves_cim_Igen_RadioButton.Checked = true;
                }
                else
                {
                    teves_cim_Nem_RadioButton.Checked = true;
                }
            }
            #endregion

            #region Téves érkeztetés setup
            if (string.IsNullOrEmpty(erec_KuldKuldemenyek.TevesErkeztetes.ToString()))
            {
                teves_erk_Nem_RadioButton.Checked = true;
            }
            else
            {
                if (erec_KuldKuldemenyek.TevesErkeztetes == "1")
                {
                    teves_erk_Igen_RadioButton.Checked = true;
                }
                else
                {
                    teves_erk_Nem_RadioButton.Checked = true;
                }
            }
            #endregion
            //bernat.laszlo eddig

            #endregion
        }
        else if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            if (SubMode == EmailBoritekTipus.Kuldemeny)
            {
                LoadEmailKuldemenyComponents(erec_KuldKuldemenyek);
            }
            else
            {
                #region Email

                if (_Load)
                {
                    ListItem item =
                    Email_IraIktatoKonyvekDropDownList1.DropDownList.Items.FindByValue(erec_KuldKuldemenyek.IraIktatokonyv_Id);
                    if (item != null)
                    {
                        Email_IraIktatoKonyvekDropDownList1.SelectedValue = item.Value;
                    }
                }
                //bernat.laszlo added
                Kezbesites_modja_DropDownList.SelectedValue = erec_KuldKuldemenyek.KezbesitesModja;

                CimzesTipusa_DropDownList.SelectedValue = erec_KuldKuldemenyek.CimzesTipusa;

                KuldesMod_DropDownList.SetSelectedValue(KodTarak.KULDEMENY_KULDES_MODJA.E_mail);
                //bernat.laszlo eddig

                Email_Bekuldo_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_KuldKuldemenyek.Partner_Id_Bekuldo, erec_KuldKuldemenyek.NevSTR_Bekuldo, EErrorPanel1);

                Email_KuldCim_CimekTextBox.SetCimekTextBoxByStringOrId(erec_KuldKuldemenyek.Cim_Id, erec_KuldKuldemenyek.CimSTR_Bekuldo, EErrorPanel1);

                // BUG_7678
                Email_FeladasiIdo_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

                Email_BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

                Email_HivatkozasiSzam_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

                Email_CsoportIdCimzett_CsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
                Email_CsoportIdCimzett_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                BejovoPeldanyListPanel1.Partner_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Cimzett;

                Email_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue = erec_KuldKuldemenyek.IktatniKell;

                #endregion
            }
        }
        else if (isHivataliKapusErkeztetes)
        {
            if (SubMode == EmailBoritekTipus.Kuldemeny)
            {
                LoadDokumentumPanelComponents(erec_KuldKuldemenyek);
                BekuldoCimBeallitas(erec_KuldKuldemenyek);
            }
            else
            {
                #region Hivatali kapu

                if (_Load)
                {
                    ListItem item =
                    eBeadvanyokPanel_IraIktatoKonyvekDropDownList.DropDownList.Items.FindByValue(erec_KuldKuldemenyek.IraIktatokonyv_Id);
                    if (item != null)
                    {
                        eBeadvanyokPanel_IraIktatoKonyvekDropDownList.SelectedValue = item.Value;
                    }
                }
                Kezbesites_modja_DropDownList.SelectedValue = erec_KuldKuldemenyek.KezbesitesModja;

                CimzesTipusa_DropDownList.SelectedValue = erec_KuldKuldemenyek.CimzesTipusa;

                KuldesMod_DropDownList.SetSelectedValue(KodTarak.KULDEMENY_KULDES_MODJA.E_mail);

                eBeadvanyokPanel_Nev.Text = erec_KuldKuldemenyek.NevSTR_Bekuldo;
                Ugy_PartnerId_Ugyindito_PartnerTextBox.Text = erec_KuldKuldemenyek.NevSTR_Bekuldo;
                eBeadvanyokPanel_FeladoCim.Text = erec_KuldKuldemenyek.CimSTR_Bekuldo;
                CimekTextBoxUgyindito.Text = erec_KuldKuldemenyek.CimSTR_Bekuldo;

                // BUG_7678
                eBeadvanyokPanel_FeladasiIdo.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

                eBeadvanyokPanel_BeerkezesIdeje.Text = erec_KuldKuldemenyek.BeerkezesIdeje;

                eBeadvanyokPanel_ErkeztetesiSzam.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

                eBeadvanyokPanel_CsoportIdCimzett_CsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
                eBeadvanyokPanel_CsoportIdCimzett_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                BejovoPeldanyListPanel1.Partner_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Cimzett;

                eBeadvanyokPanel_IktatasiKotelezettsegKodtarakDropDownList.SelectedValue = erec_KuldKuldemenyek.IktatniKell;
                #endregion
            }
        }

        LoadFutarJegyzekParameters(erec_KuldKuldemenyek);
        SetElektronikusIratVonalkodControl();
        #region MINOSITO
        if (!string.IsNullOrEmpty(erec_KuldKuldemenyek.Minosito))
        {
            MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoPartnerId = erec_KuldKuldemenyek.Minosito;
            MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoSzuloPartnerId = erec_KuldKuldemenyek.MinositoSzervezet;
            // Névfeloldás Id alapján:
            MinositoPartnerControlEgyszerusitettIktatas.SetPartnerTextBoxById(EErrorPanel1);
        }
        #endregion

        if (isTUKRendszer && !string.IsNullOrEmpty(erec_KuldKuldemenyek.IrattarId))
        {
            IrattariHelyLevelekDropDownTUK.SelectedValue = erec_KuldKuldemenyek.IrattarId;
        }

        SetElektronikusIratVonalkodControl();
    }

    private void LoadDokumentumPanelComponents(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        ErkeztetoSzam_TextBox.Text = erec_KuldKuldemenyek.Azonosito;

        KuldesMod_DropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
        erec_KuldKuldemenyek.KuldesMod, EErrorPanel1);

        // BLG_361
        //UgyintezesModja_DropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
        //erec_KuldKuldemenyek.UgyintezesModja, EErrorPanel1);
        UgyintezesModja_DropDownList.FillAndSetSelectedValue(kcs_ELSODLEGES_ADATHORDOZO, erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);

        Surgosseg_DropDownList.FillAndSetSelectedValue(kcs_SURGOSSEG, erec_KuldKuldemenyek.Surgosseg, EErrorPanel1);


        Bekuldo_PartnerTextBox.Id_HiddenField = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
        Bekuldo_PartnerTextBox.Text = erec_KuldKuldemenyek.NevSTR_Bekuldo;
        //BUG 5197
        Bekuldo_PartnerTextBox.KapcsoltPartnerDropDownValue = erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt;

        Kuld_FelbontasDatuma_CalendarControl.Text = erec_KuldKuldemenyek.FelbontasDatuma;
        if (string.IsNullOrEmpty(Kuld_FelbontasDatuma_CalendarControl.Text))
            Kuld_FelbontasDatuma_CalendarControl.SetToday();

        Kuld_CimId_CimekTextBox.Id_HiddenField = erec_KuldKuldemenyek.Cim_Id;
        Kuld_CimId_CimekTextBox.Text = erec_KuldKuldemenyek.CimSTR_Bekuldo;

        // BUG_7678
        FeladasiIdo_CalendarControl.Text = erec_KuldKuldemenyek.BelyegzoDatuma;

        BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;
        if (string.IsNullOrEmpty(BeerkezesIdeje_CalendarControl.Text))
            BeerkezesIdeje_CalendarControl.SetToday();

        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        HivatkozasiSzamUserControl_Kuldemeny_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

        CsoportId_CimzettCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
        CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

        BejovoPeldanyListPanel1.Partner_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Cimzett;

        if (Command == CommandName.New)
        {
            // küldemény minõsítés átvétele az iratba
            ktDropDownListIratMinosites.FillAndSetSelectedValue(kcs_IRATMINOSITES, erec_KuldKuldemenyek.Minosites, true, EErrorPanel1);
        }

        if (Command == CommandName.New && Mode == ErkeztetesTipus.HivataliKapusErkeztetes && SubMode == EmailBoritekTipus.Kuldemeny)
        {
            VonalkodTextBoxVonalkod.ReadOnly = true;
            VonalkodTextBoxVonalkod.Text = erec_KuldKuldemenyek.BarCode;
        }
    }

    private void LoadEmailKuldemenyComponents(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        ErkeztetoSzam_TextBox.Text = erec_KuldKuldemenyek.Azonosito;

        // BUG_4809
        //Kezbesites_modja_DropDownList.FillDropDownList(kcs_KULD_KEZB_MODJA, EErrorPanel1);
        Kezbesites_modja_DropDownList.FillAndSetSelectedValue(kcs_KULD_KEZB_MODJA, erec_KuldKuldemenyek.KezbesitesModja, EErrorPanel1);
        //CimzesTipusa_DropDownList.FillDropDownList(kcs_KULD_CIMZES_TIPUS, EErrorPanel1);
        if (String.IsNullOrEmpty(erec_KuldKuldemenyek.CimzesTipusa))
        {
            CimzesTipusa_DropDownList.FillAndSetSelectedValue(kcs_KULD_CIMZES_TIPUS, KodTarak.KULD_CIMZES_TIPUS.nevre_szolo, EErrorPanel1);

        }
        else
            CimzesTipusa_DropDownList.FillAndSetSelectedValue(kcs_KULD_CIMZES_TIPUS, erec_KuldKuldemenyek.CimzesTipusa, EErrorPanel1);
        tr_Kiadmanyozas.Visible = false;

        KuldesMod_DropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
        erec_KuldKuldemenyek.KuldesMod, EErrorPanel1);

        UgyintezesModja_DropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
        erec_KuldKuldemenyek.ElsodlegesAdathordozoTipusa, EErrorPanel1);

        Surgosseg_DropDownList.FillAndSetSelectedValue(kcs_SURGOSSEG, erec_KuldKuldemenyek.Surgosseg, EErrorPanel1);


        Bekuldo_PartnerTextBox.Id_HiddenField = erec_KuldKuldemenyek.Partner_Id_Bekuldo;
        Bekuldo_PartnerTextBox.Text = erec_KuldKuldemenyek.NevSTR_Bekuldo;
        //BUG 5197
        Bekuldo_PartnerTextBox.KapcsoltPartnerDropDownValue = erec_KuldKuldemenyek.Partner_Id_BekuldoKapcsolt;

        Kuld_FelbontasDatuma_CalendarControl.Text = erec_KuldKuldemenyek.FelbontasDatuma;
        if (string.IsNullOrEmpty(Kuld_FelbontasDatuma_CalendarControl.Text))
            Kuld_FelbontasDatuma_CalendarControl.SetToday();

        Kuld_CimId_CimekTextBox.Id_HiddenField = erec_KuldKuldemenyek.Cim_Id;
        Kuld_CimId_CimekTextBox.Text = erec_KuldKuldemenyek.CimSTR_Bekuldo;

        // BUG_7678
        FeladasiIdo_CalendarControl.Text = erec_KuldKuldemenyek.BelyegzoDatuma;

        BeerkezesIdeje_CalendarControl.Text = erec_KuldKuldemenyek.BeerkezesIdeje;
        if (string.IsNullOrEmpty(BeerkezesIdeje_CalendarControl.Text))
            BeerkezesIdeje_CalendarControl.SetToday();

        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        HivatkozasiSzamUserControl_Kuldemeny_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

        CsoportId_CimzettCsoportTextBox1.Id_HiddenField = erec_KuldKuldemenyek.Csoport_Id_Cimzett;
        CsoportId_CimzettCsoportTextBox1.SetCsoportTextBoxById(EErrorPanel1);

        BejovoPeldanyListPanel1.Partner_Id_Cimzett = erec_KuldKuldemenyek.Csoport_Id_Cimzett;

        if (Command == CommandName.New)
        {
            // küldemény minõsítés átvétele az iratba
            ktDropDownListIratMinosites.FillAndSetSelectedValue(kcs_IRATMINOSITES, erec_KuldKuldemenyek.Minosites, true, EErrorPanel1);
        }

        if (Command == CommandName.New && Mode == ErkeztetesTipus.EmailErkeztetes && SubMode == EmailBoritekTipus.Kuldemeny)
        {
            VonalkodTextBoxVonalkod.ReadOnly = true;
            VonalkodTextBoxVonalkod.Text = erec_KuldKuldemenyek.BarCode;
        }
    }

    private void LoadBontoPanel(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        //Kuld_FelbontasDatuma_CalendarControl.Text = erec_KuldKuldemenyek.FelbontasDatuma;
        Kuld_FelhCsopId_Bonto_FelhasznaloCsoportTextBox.Id_HiddenField = erec_KuldKuldemenyek.FelhasznaloCsoport_Id_Bonto;
    }

    #region Utility
    private bool IsIktatokonyvLezart
    {
        get
        {
            return !String.IsNullOrEmpty(LezartIktatokonyv_HiddenField.Value) || !String.IsNullOrEmpty(LezartFolyosorszamosIktatokonyv_HiddenField.Value);
        }
    }

    private bool IsIktatokonyvLezartFolyosorszamos
    {
        get
        {
            return !String.IsNullOrEmpty(LezartFolyosorszamosIktatokonyv_HiddenField.Value);
        }
    }

    private bool IsElozmenyUgyirat
    {
        get
        {
            return !String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value);
        }
    }

    private bool IsAlszamraIktatas
    {
        get
        {
            // Ha van megadva ügyirat id, akkor alszámra iktatás van
            // de ha az iktatókönyv lezárt és van joga fõszámra iktatni, akkor mégis fõszámra iktatás + szerelés/szerelés elõkészítés
            return (IsElozmenyUgyirat && (!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos));
        }
    }

    private bool IsCsakErkeztetes
    {
        get
        {
            return EmailKezelesSelector.IsSelectedValue(ErkeztetesVagyIktatasRadioButtonList, EmailKezelesSelector.EmailKezelesTipus.CsakErkeztetes);
        }
    }

    private bool IsMunkaanyagLetrehozas
    {
        get
        {
            return EmailKezelesSelector.IsSelectedValue(ErkeztetesVagyIktatasRadioButtonList, EmailKezelesSelector.EmailKezelesTipus.Munkapeldany);
        }
    }

    private bool IsErkeztetesIktatas
    {
        get
        {
            return EmailKezelesSelector.IsSelectedValue(ErkeztetesVagyIktatasRadioButtonList, EmailKezelesSelector.EmailKezelesTipus.ErkeztetesIktatas);
        }
    }

    private bool IktathatFoszamra(ErkeztetesTipus Mode)
    {
        switch (Mode)
        {
            // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?
            // (munkaanyag létrehozásnál nem nézzük)
            case ErkeztetesTipus.EmailErkeztetes:
            case ErkeztetesTipus.HivataliKapusErkeztetes:
                return (!FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra) || IsMunkaanyagLetrehozas);
            default:
                return !FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra);
        }
    }

    private bool RaiseErrorNemIktathatFoszamra(ErkeztetesTipus Mode)
    {
        if (!IktathatFoszamra(Mode))
        {
            // Hiba: Csak alszámra iktathat!
            // "A jogosultsága alapján Ön csak alszámra iktathat! Válasszon elõzmény ügyiratot!"
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UICsakAlszamraIktathat);
            ErrorUpdatePanel1.Update();
        }

        return !IktathatFoszamra(Mode);
    }


    //Lezárt iktatókönyvû elõzménybe nem iktathat, ha csak alszámra iktathat joga van
    private bool RaiseErrorLezartIktatokonyvbeNemIktathat(ErkeztetesTipus Mode)
    {
        bool ret = IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos && !IktathatFoszamra(Mode);

        if (ret)
        {
            ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.UILezartIktatokonyCsakAlszamraIktathat);
            ErrorUpdatePanel1.Update();
        }

        return ret;
    }
    #endregion

    /// <summary>
    /// A megõrzõs checkboxok alapján adat Session-be mentése vagy kidobása onnan
    /// </summary>
    public void SetSessionData()
    {
        #region Megõrzést jelzõ checkboxok állítása
        if (cbMegorzesJelzo.Checked)
        {
            Session["BoritoTipusValue"] = KodtarakDropDownListBoritoTipus.SelectedValue;
            Session["BoritoTipusChecked"] = "true";
        }
        else
        {
            Session.Remove("BoritoTipusValue");
            Session.Remove("BoritoTipusChecked");
        }

        if (CheckBox_CimzettMegorzes.Checked)
        {
            Session[session_CimzettId] = CsoportId_CimzettCsoportTextBox1.Id_HiddenField;
            Session[session_CimzettChecked] = "true";
        }
        else
        {
            Session.Remove(session_CimzettId);
            Session.Remove(session_CimzettChecked);
        }

        UI.StoreValueInSession(Page, BeerkezesIdeje_CalendarControl.ID, BeerkezesIdeje_CalendarControl.Text, cbBeerkezesIdopontjaMegorzes);
        UI.StoreValueInSession(Page, FeladasiIdo_CalendarControl.ID, FeladasiIdo_CalendarControl.Text, cbFeladasIdoMegorzes);
        UI.StoreValueInSession(Page, RagszamRequiredTextBox.ID, RagszamRequiredTextBox.Text, cbRagszamMegorzes);
        UI.StoreValueInSession(Page, bontasMegjegyzesTextBox.ID, bontasMegjegyzesTextBox.Text, cbBontasiMegjegyzesMegorzes);

        #endregion
    }

    private void TabFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //Megõrzést jelzõ checkboxok állítása
        SetSessionData();

        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndClose)
        {
            if ((Command == CommandName.New && FunctionRights.GetFunkcioJog(Page, funkcio_EgyszerusitettIktatas) && Mode == ErkeztetesTipus.KuldemenyErkeztetes)
                || (Command == CommandName.New && FunctionRights.GetFunkcioJog(Page, funkcio_EmailErkeztetes) && Mode == ErkeztetesTipus.EmailErkeztetes)
                || (Command == CommandName.New && FunctionRights.GetFunkcioJog(Page, funkcio_eBeadvanyokErkeztetes) && Mode == ErkeztetesTipus.HivataliKapusErkeztetes))
            {
                bool voltHiba = false;
                bool ujUgyirat = false; // annak nyilvántartásához, hogy kell-e beszúrni standard tárgyszó rekordokat (csak újhoz kell)

                try
                {
                    switch (Command)
                    {
                        case CommandName.New:
                            {
                                bool isHatosagi = CheckUgyFajtaIsHatosagi(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue);

                                #region UGY FAJTA: HATOSAGI -ELJARASI SZAKASZ
                                if (!CheckUgyFajtaEsEljarasiSzakasz(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue, KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue))
                                    return;
                                #endregion UGY FAJTA: HATOSAGI -ELJARASI SZAKASZ

                                #region SAKKORA KOTELEZO
                                if (!CheckUgyFajtaSakkoraKotelezoseg(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue))
                                    return;
                                #endregion

                                Result result_iktatas = null;
                                bool csakKuldemenyElokeszitesVolt = false;
                                bool csakerkeztetes = false;
                                string kuldemenyId = "";

                                #region Iktatáshoz szükséges adatok:

                                // (Alszámra iktatásnál) ha lezárt az ügyirat, felhasználótól megkérdezni valahogy, hogy most akkor 
                                // újra kéne nyitni az ügyiratot, vagy beleiktassunk a lezárt ügyiratba
                                // defaultból most az utóbbi eset lesz
                                IktatasiParameterek.UgyiratUjranyitasaHaLezart = RadioButtonList_Lezartbaiktat_vagy_Ujranyit.SelectedValue == "1";

                                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                // objektumok feltöltése a form komponenseibõl:
                                String UgyUgyirat_Id = ElozmenyUgyiratID_HiddenField.Value;

                                // A cascading dropdown cuccból vesszük:
                                String Erec_IraIktatokonyvek_Id = GetSelectedIktatokonyvId();

                                EREC_UgyUgyiratok erec_UgyUgyirat = GetBusinessObjectFromComponents_EREC_UgyUgyiratok();
                                EREC_IraIratok erec_IraIrat = GetBusinessObjectFromComponents();
                                EREC_HataridosFeladatok erec_HataridosFeladatok = FeljegyzesPanel.GetBusinessObject();

                                EREC_KuldKuldemenyek kuldemenyAdatok = null;
                                if (Mode == ErkeztetesTipus.EmailErkeztetes || isHivataliKapusErkeztetes)
                                {
                                    if (SubMode == EmailBoritekTipus.Kuldemeny)
                                    {
                                        // be kell betölteni a küldemény adatokat, különben NullReferenceException lesz! (pl. itt: KettosIktatasControlling(kuldemenyAdatok.HivatkozasiSzam, service)
                                        kuldemenyAdatok = GetKuldemenyById(EmailBoritekokKuldemenyId);
                                    }
                                    else
                                    {
                                        kuldemenyAdatok = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
                                    }
                                }
                                else
                                {
                                    kuldemenyAdatok = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
                                }
                                IktatasiParameterek.Adoszam = textAdoszam.Text;

                                if (IsAlszamraIktatas)
                                {
                                    if (!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos)
                                    {
                                        // !!! Alszámra iktatásnál nem az ajax-os dropdownból vesszük az iktatókönyvet!!! (IDEIGLENES MEGOLDÁS)
                                        Erec_IraIktatokonyvek_Id = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.SelectedValue;
                                    }
                                    else
                                    {
                                        // Ha lezárt iktatókönyvben lévõ ügyiratba iktatunk, akkor a Cascadingdrop downból vesszük az adatokat
                                        Erec_IraIktatokonyvek_Id = GetSelectedIktatokonyvId();
                                        IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
                                        IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
                                    }
                                }
                                else
                                {
                                    // ha elõzményt választott, de az elõzmény lezárt iktatókönyvben volt,
                                    // és van joga fõszámra iktatni, akkor itt adjuk át a szerelendõ eredeti ügyirat azonosítóját
                                    if (IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos)
                                    {
                                        IktatasiParameterek.SzerelendoUgyiratId = UgyUgyirat_Id;
                                    }

                                    IktatasiParameterek.UgykorId = Ugykor_DropDownList.SelectedValue;
                                    IktatasiParameterek.Ugytipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
                                }

                                if (PeldanyokSorszamaMegadhato)
                                {
                                    string peldanyokError;

                                    if (!BejovoPeldanyListPanel1.IsValid(out peldanyokError))
                                    {
                                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, peldanyokError);
                                        ErrorUpdatePanel1.Update();
                                        return;
                                    }

                                    IktatasiParameterek.Peldanyok = BejovoPeldanyListPanel1.GetBusinessObjectsFromComponents();

                                    //if (isTUKRendszer && IktatasiParameterek.Peldanyok!=null && IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null)
                                    //{
                                    //    foreach (EREC_PldIratPeldanyok pld in IktatasiParameterek.Peldanyok)
                                    //    {
                                    //        pld.IrattarId = IrattariHelyLevelekDropDownTUK.SelectedValue;
                                    //        pld.Updated.IrattarId = true;
                                    //        pld.IrattariHely = (IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem != null) ? IrattariHelyLevelekDropDownTUK.DropDownList.SelectedItem.Text : string.Empty;
                                    //        pld.Updated.IrattariHely = true;
                                    //    }
                                    //}
                                }

                                #endregion

                                bool hivszam_ellenorzes = Rendszerparameterek.Get(execParam, Rendszerparameterek.HIVSZAM_ELLENORZES) == "1" ? true : false;

                                if (isHivataliKapusErkeztetes)
                                {
                                    #region Email érkeztetés/iktatás

                                    EREC_KuldKuldemenyekService service_kuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

                                    erec_IraIrat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

                                    //bool munkaanyagLetrehozas = (ErkeztetesVagyIktatasRadioButtonList.SelectedValue == "2");

                                    ErkeztetesParameterek ep = new ErkeztetesParameterek();
                                    //ep.Mellekletek = mellekletekPanelEmail.GetMellekletek();

                                    // Csak érkeztetés, vagy iktatás is?
                                    if (!IsCsakErkeztetes)
                                    {
                                        #region Érkeztetés/Iktatás illetve Érkeztetés/Munkapéldány létrehozás

                                        // Funkciójog ellenõrzés iktatásra vagy munkaanyag létrehozásra (érkeztetésre már ellenõriztük)                                        
                                        if ((IsMunkaanyagLetrehozas && !FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites))
                                            || (!IsMunkaanyagLetrehozas && !FunctionRights.GetFunkcioJog(Page, funkcio_eBeadvanyokIktatas)))
                                        {
                                            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                                            return;
                                        }

                                        // Ha munkaanyag létrehozás kell, nem iktatás:
                                        if (IsMunkaanyagLetrehozas)
                                        {
                                            iktatasiParameterek.MunkaPeldany = true;
                                        }

                                        #region BLG_232
                                        if (KettosIktatasControlling(kuldemenyAdatok.HivatkozasiSzam, service))
                                        {
                                            return;
                                        }
                                        #endregion

                                        if (IsAlszamraIktatas)
                                        {
                                            if (SubMode == EmailBoritekTipus.Kuldemeny)
                                            {
                                                iktatasiParameterek.KuldemenyId = EmailBoritekokKuldemenyId;
                                                iktatasiParameterek.Iratpeldany_Vonalkod = VonalkodTextBoxVonalkod.Text;
                                                result_iktatas = service.BejovoIratIktatasa_Alszamra(execParam, Erec_IraIktatokonyvek_Id,
                                                        UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, iktatasiParameterek);
                                            }
                                            else
                                            {
                                                result_iktatas = service.HivataliKapusErkeztetesIktatas_Alszamra(execParam, kuldemenyAdatok, eBeadvanyokId, Erec_IraIktatokonyvek_Id,
                                                    UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, IktatasiParameterek, ep);
                                            }

                                        }
                                        else
                                        {
                                            // Normál iktatás
                                            if (RaiseErrorNemIktathatFoszamra(Mode))
                                            {
                                                voltHiba = true;
                                            }
                                            else
                                            {
                                                if (SubMode == EmailBoritekTipus.Kuldemeny)
                                                {
                                                    iktatasiParameterek.KuldemenyId = EmailBoritekokKuldemenyId;
                                                    iktatasiParameterek.Iratpeldany_Vonalkod = VonalkodTextBoxVonalkod.Text;
                                                    result_iktatas = service.BejovoIratIktatasa(execParam, Erec_IraIktatokonyvek_Id,
                                                            erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, iktatasiParameterek);
                                                }
                                                else
                                                {
                                                    #region BLG 232
                                                    //LZS
                                                    //Hivatali kapus(elektronikus üzenetek) érkeztetésekor / iktatásakor a kiválasztott eBeadvany - t kell lekérni, 
                                                    //majd a benne található PR_Erkeztetoszam/ KR_Erkeztetoszam mezőt kell beírni 
                                                    //az EREC_IraIratok és a KuldKuldemenyek objektumba a HivataliKapusErkeztetesIktatas() meghívása előtt.

                                                    if (hivszam_ellenorzes)
                                                    {
                                                        #region BLG_232
                                                        if (KettosIktatasControlling(kuldemenyAdatok.HivatkozasiSzam, service))
                                                        {
                                                            return;
                                                        }
                                                        #endregion

                                                        #region Küldemény hivatkozási száma
                                                        //Küldemény hivatkozási számába beírjuk az eBeadvanyokPanel_ErkeztetesiSzam.Text mezőt.
                                                        kuldemenyAdatok.HivatkozasiSzam = eBeadvanyokPanel_ErkeztetesiSzam.Text;

                                                        #endregion

                                                        #region Irat hivatkozási száma
                                                        //Irat hivatkozási számába beírjuk az eBeadvanyokPanel_ErkeztetesiSzam.Text mezőt.
                                                        erec_IraIrat.HivatkozasiSzam = eBeadvanyokPanel_ErkeztetesiSzam.Text;

                                                        #endregion
                                                    }
                                                    #endregion

                                                    result_iktatas = service.HivataliKapusErkeztetesIktatas(execParam, kuldemenyAdatok, eBeadvanyokId,
                                                        Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(),
                                                        erec_IraIrat, erec_HataridosFeladatok, IktatasiParameterek, ep);


                                                }

                                            }
                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        #region Csak érkeztetés

                                        EREC_HataridosFeladatok erec_HataridosFeladatokKuldemeny = KezelesiFeljegyzesPanelKuldemeny.GetBusinessObject();
                                        result_iktatas = service_kuldemenyek.ErkeztetesHivataliKapubol(execParam, kuldemenyAdatok, eBeadvanyokId, erec_HataridosFeladatokKuldemeny, ep);

                                        #endregion
                                    }

                                    #endregion
                                }
                                else if (Mode == ErkeztetesTipus.EmailErkeztetes)
                                {
                                    #region Email érkeztetés/iktatás

                                    EREC_KuldKuldemenyekService service_kuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

                                    // Id-t és a verziót ebben adjuk át:
                                    EREC_eMailBoritekok emailBoritek = new EREC_eMailBoritekok();
                                    emailBoritek.Updated.SetValueAll(false);
                                    emailBoritek.Base.Updated.SetValueAll(false);
                                    emailBoritek.Id = EmailBoritekokId;
                                    emailBoritek.Base.Ver = eMailBoritekok_Ver_HiddenField.Value;
                                    emailBoritek.Base.Updated.Ver = true;

                                    erec_IraIrat.AdathordozoTipusa = KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir;

                                    //bool munkaanyagLetrehozas = (ErkeztetesVagyIktatasRadioButtonList.SelectedValue == "2");

                                    ErkeztetesParameterek ep = new ErkeztetesParameterek();
                                    ep.Mellekletek = mellekletekPanelEmail.GetMellekletek();

                                    // Csak érkeztetés, vagy iktatás is?
                                    if (!IsCsakErkeztetes)
                                    {
                                        #region Érkeztetés/Iktatás illetve Érkeztetés/Munkapéldány létrehozás

                                        // Funkciójog ellenõrzés iktatásra vagy munkaanyag létrehozásra (érkeztetésre már ellenõriztük)                                        
                                        if ((IsMunkaanyagLetrehozas && !FunctionRights.GetFunkcioJog(Page, funkcio_IktatasElokeszites))
                                            || (!IsMunkaanyagLetrehozas && !FunctionRights.GetFunkcioJog(Page, funkcio_EmailIktatas)))
                                        {
                                            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
                                            return;
                                        }

                                        // Ha munkaanyag létrehozás kell, nem iktatás:
                                        if (IsMunkaanyagLetrehozas)
                                        {
                                            iktatasiParameterek.MunkaPeldany = true;
                                        }

                                        if (IsAlszamraIktatas)
                                        {
                                            if (SubMode == EmailBoritekTipus.Kuldemeny)
                                            {
                                                iktatasiParameterek.KuldemenyId = EmailBoritekokKuldemenyId;
                                                // nekrisz CR 2993: Vonalkód nem jelenik meg az Email-nyomtatásnál (nincs az iratpéldányon)
                                                iktatasiParameterek.Iratpeldany_Vonalkod = VonalkodTextBoxVonalkod.Text;  // küldeményadatok nem kerülnek betöltésre ezért a textboxból szedjük a vonalkódot
                                                result_iktatas = service.BejovoIratIktatasa_Alszamra(execParam, Erec_IraIktatokonyvek_Id,
                                                        UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, iktatasiParameterek);
                                            }
                                            else
                                            {
                                                result_iktatas = service.EmailErkeztetesIktatas_Alszamra(execParam, kuldemenyAdatok, emailBoritek, Erec_IraIktatokonyvek_Id,
                                                    UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, IktatasiParameterek, ep);
                                            }

                                        }
                                        else
                                        {
                                            // Normál iktatás
                                            if (RaiseErrorNemIktathatFoszamra(Mode))
                                            {
                                                voltHiba = true;
                                            }
                                            else
                                            {
                                                if (SubMode == EmailBoritekTipus.Kuldemeny)
                                                {
                                                    iktatasiParameterek.KuldemenyId = EmailBoritekokKuldemenyId;
                                                    // nekrisz CR 2993: Vonalkód nem jelenik meg az Email-nyomtatásnál (nincs az iratpéldányon)
                                                    iktatasiParameterek.Iratpeldany_Vonalkod = VonalkodTextBoxVonalkod.Text; // küldeményadatok nem kerülnek betöltésre ezért a textboxból szedjük a vonalkódot
                                                    result_iktatas = service.BejovoIratIktatasa(execParam, Erec_IraIktatokonyvek_Id,
                                                            erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, iktatasiParameterek);

                                                    AutomatikusSzignalas(service, result_iktatas.Uid);
                                                }
                                                else
                                                {
                                                    result_iktatas = service.EmailErkeztetesIktatas(execParam, kuldemenyAdatok, emailBoritek,
                                                        Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(),
                                                        erec_IraIrat, erec_HataridosFeladatok, IktatasiParameterek, ep);

                                                    AutomatikusSzignalas(service, result_iktatas.Uid);
                                                }
                                            }

                                        }

                                        #endregion
                                    }
                                    else
                                    {
                                        #region Csak érkeztetés

                                        EREC_HataridosFeladatok erec_HataridosFeladatokKuldemeny = KezelesiFeljegyzesPanelKuldemeny.GetBusinessObject();
                                        result_iktatas = service_kuldemenyek.ErkeztetesEmailbol(execParam, kuldemenyAdatok, emailBoritek, erec_HataridosFeladatokKuldemeny, ep);

                                        #endregion
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Küldemény érkeztetés/iktatás
                                    ErkeztetesParameterek ep = new ErkeztetesParameterek();
                                    ep.Mellekletek = mellekletekPanel.GetMellekletek();

                                    //if (!String.IsNullOrEmpty(UgyUgyirat_Id))
                                    if (IsAlszamraIktatas)
                                    {
                                        #region BLG_232
                                        if (KettosIktatasControlling(kuldemenyAdatok.HivatkozasiSzam, service))
                                        {
                                            return;
                                        }
                                        #endregion

                                        result_iktatas = service.EgyszerusitettIktatasa_Alszamra(execParam, Erec_IraIktatokonyvek_Id,
                                            UgyUgyirat_Id, new EREC_UgyUgyiratdarabok(), erec_IraIrat, erec_HataridosFeladatok, kuldemenyAdatok, IktatasiParameterek, ep);
                                    }
                                    else
                                    {
                                        // Normál iktatás

                                        // Funkciójog ellenõrzés: van-e korlátozás arra, hogy csak alszámra iktathat?                                        
                                        if (RaiseErrorNemIktathatFoszamra(Mode))
                                        {
                                            voltHiba = true;
                                        }
                                        else
                                        {

                                            #region BLG_232
                                            if (KettosIktatasControlling(kuldemenyAdatok.HivatkozasiSzam, service))
                                            {
                                                return;
                                            }
                                            #endregion

                                            result_iktatas = service.EgyszerusitettIktatasa(execParam,
                                                Erec_IraIktatokonyvek_Id, erec_UgyUgyirat, new EREC_UgyUgyiratdarabok(),
                                                erec_IraIrat, erec_HataridosFeladatok, kuldemenyAdatok, IktatasiParameterek, ep);

                                            #region BLG_2950 - Automaitikus szignálás
                                            if (!result_iktatas.IsError)
                                            {
                                                //LZS - Automatikus szignálás javasolt ügyintézőre iktatás után
                                                AutomatikusSzignalas(service, result_iktatas.Uid);
                                            }
                                            #endregion
                                        }
                                    }


                                    #endregion
                                }

                                #region IRAT HATASA
                                if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
                                {
                                    if (result_iktatas != null && result_iktatas.Record != null && result_iktatas.Record is ErkeztetesIktatasResult)
                                    {
                                        ErkeztetesIktatasResult erkeztetesIktatasResult = (ErkeztetesIktatasResult)result_iktatas.Record;
                                        string iratid = erkeztetesIktatasResult.IratId;
                                        // BUG_2527
                                        if (!String.IsNullOrEmpty(iratid))
                                        {
                                            StartIrataHatasaProcedure(iratid);
                                        }
                                    }
                                }
                                #endregion

                                #region hiba kezelés
                                if (result_iktatas != null)
                                {
                                    if (result_iktatas.IsError)
                                    {
                                        // hiba
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_iktatas);
                                        ErrorUpdatePanel1.Update();
                                        voltHiba = true;
                                    }
                                    else if (IsCsakErkeztetes)
                                    {
                                        kuldemenyId = result_iktatas.Uid;

                                        csakerkeztetes = true;
                                    }
                                    else if (result_iktatas.ErrorType == "NincsIktatva")
                                    {
                                        // Küldemény nem lett iktatva, csak rögzítve lettek bizonyos iktatási adatok

                                        csakKuldemenyElokeszitesVolt = true;
                                    }
                                    else if (!IsAlszamraIktatas)
                                    {
                                        ujUgyirat = true;
                                    }
                                }
                                #endregion

                                if (!voltHiba && result_iktatas != null)
                                {
                                    if (csakKuldemenyElokeszitesVolt)
                                    {
                                        // Nem volt hiba, de a küldemény nem lett iktatva:

                                        // Érkeztetés ResultPanelt is beállítjuk:
                                        SetErkeztetesResultPanel((ErkeztetesIktatasResult)result_iktatas.Record);

                                        #region ResultPanel_KuldemenyElokeszites beállítása

                                        //Panel-ek beállítása
                                        KuldemenyPanel.Visible = false;
                                        Ugyirat_Panel.Visible = false;
                                        IratPanel.Visible = false;
                                        //IratPeldanyPanel.Visible = false;

                                        ResultPanel.Visible = false;
                                        StandardTargyszavakPanel.Visible = false;
                                        StandardTargyszavakPanel_Iratok.Visible = false;
                                        TipusosTargyszavakPanel_Iratok.Visible = false;
                                        BejovoPeldanyListPanel1.Visible = false;
                                        ResultPanel_KuldemenyElokeszites.Visible = true;

                                        //TabFooter1.HideButton();
                                        //TabFooter1.ImageButton_Close.Visible = true;

                                        #endregion
                                    }
                                    else
                                    {
                                        // Iktatás sikeres volt:
                                        String UjUgyirat_Id = "";
                                        String UjIrat_Id = "";
                                        ErkeztetesIktatasResult erkeztetesIktatasResult = null;
                                        try
                                        {
                                            //// TODO: Nem Ok egyelõre, az osztály serializációját még meg kell csinálni
                                            //Contentum.eRecord.BaseUtility.Iratok.IktatasResult iktatasResult =
                                            //    (Contentum.eRecord.BaseUtility.Iratok.IktatasResult)result_iktatas.Record;
                                            //if (iktatasResult != null)
                                            //{
                                            //    UjUgyirat_Id = iktatasResult.UjUgyirat_Id;
                                            //    UjUgyiratDarab_Id = iktatasResult.UjUgyiratDarab_Id;
                                            //    UjIrat_Id = iktatasResult.UjIrat_Id;
                                            //    UjIratPeldany_Id = iktatasResult.UjIratPeldany_Id;
                                            //}

                                            // egyelõre csak az ügyirat id-ját küldjük vissza
                                            UjIrat_Id = result_iktatas.Uid;

                                            if (result_iktatas.Record != null && result_iktatas.Record is ErkeztetesIktatasResult)
                                            {
                                                erkeztetesIktatasResult = (ErkeztetesIktatasResult)result_iktatas.Record;
                                                UjUgyirat_Id = erkeztetesIktatasResult.UgyiratId;
                                            }
                                            else
                                            {
                                                UjUgyirat_Id = (String)result_iktatas.Record;
                                            }

                                            if (!csakerkeztetes)
                                            {
                                                #region standard objektumfüggõ tárgyszavak mentése ügyirathoz
                                                // csak új ügyiratra
                                                if (ujUgyirat)
                                                {
                                                    // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                                                    // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                                                    // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                                                    EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList = otpStandardTargyszavak.GetEREC_ObjektumTargyszavaiList(!IsIktatokonyvLezart || IsIktatokonyvLezartFolyosorszamos, IsIktatokonyvLezart && !IsIktatokonyvLezartFolyosorszamos);

                                                    if (EREC_ObjektumTargyszavaiList != null && EREC_ObjektumTargyszavaiList.Length > 0)
                                                    {
                                                        ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                                        EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                                        Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                                                    , EREC_ObjektumTargyszavaiList
                                                                    , UjUgyirat_Id
                                                                    , null
                                                                    , Constants.TableNames.EREC_UgyUgyiratok
                                                                    , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                                                                    , false
                                                                    );

                                                        if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                                        {
                                                            // hiba
                                                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                                            ErrorUpdatePanel1.Update();
                                                            //voltHiba = true; // TODO: ??? true or false ???
                                                        }
                                                    }
                                                }
                                                #endregion standard objektumfüggõ tárgyszavak mentése ügyirathoz

                                                #region standard objektumfüggõ tárgyszavak mentése irathoz
                                                // ha nem lezárt az iktatókönyv, csak a változásokat vesszük figyelembe, és átadjuk a rekord id-t is
                                                // ha lezárt az iktatókönyv, be kell majd szúrni minden kitöltött elemet, és nem szabad átadni a rekord id-ket
                                                // elvileg itt nem lehet folyósorszámos lezárt, mert akkor nem jön létre új ügyirat, de azért vizsgáljuk
                                                EREC_ObjektumTargyszavai[] EREC_ObjektumTargyszavaiList_Iratok = otpStandardTargyszavak_Iratok.GetEREC_ObjektumTargyszavaiList(true);

                                                if (EREC_ObjektumTargyszavaiList_Iratok != null && EREC_ObjektumTargyszavaiList_Iratok.Length > 0)
                                                {
                                                    ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
                                                    EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                                                    Result result_ot = service_ot.SetValuesFromListAllMetaByDefinicioTipus(execparam_ot
                                                                , EREC_ObjektumTargyszavaiList_Iratok
                                                                , UjIrat_Id
                                                                , null
                                                                , Constants.TableNames.EREC_IraIratok
                                                                , KodTarak.OBJMETADEFINICIO_TIPUS.B1
                                                                , false
                                                                );

                                                    if (!String.IsNullOrEmpty(result_ot.ErrorCode))
                                                    {
                                                        // hiba
                                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot);
                                                        ErrorUpdatePanel1.Update();
                                                        //voltHiba = true; // TODO: ??? true or false ???
                                                    }
                                                }
                                                #endregion standard objektumfüggõ tárgyszavak mentése irathoz

                                                #region típusos objektumfüggõ tárgyszavak mentése irathoz

                                                Result result_ot_tipusos = Save_TipusosObjektumTargyszavai_Iratok(UjIrat_Id);

                                                if (!String.IsNullOrEmpty(result_ot_tipusos.ErrorCode))
                                                {
                                                    // hiba
                                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ot_tipusos);
                                                    ErrorUpdatePanel1.Update();
                                                    //voltHiba = true; // TODO: ??? true or false ???
                                                }

                                                #endregion típusos objektumfüggõ tárgyszavak mentése irathoz
                                            }
                                        }
                                        catch
                                        {
                                            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                            return;
                                        }

                                        /// TODO majd a hívó listától függõen beállítani az új rekord id-t,
                                        /// egyelõre az új ügyirat id-ja lesz megadva
                                        String returnId = UjUgyirat_Id;

                                        if (e.CommandName == CommandName.Save)
                                        {
                                            if (Command == CommandName.New)
                                            {
                                                JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);

                                                if (csakerkeztetes)
                                                {
                                                    // Érkeztetés eredménye:
                                                    SetErkeztetesResultPanel(erkeztetesIktatasResult);
                                                }
                                                else
                                                {
                                                    // Iktatás eredménye: (+ Érkeztetés eredménye:)
                                                    this.SetResultPanel(erkeztetesIktatasResult);
                                                }
                                            }
                                        }
                                        else if (e.CommandName == CommandName.SaveAndClose)
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);
                                            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                                        }
                                    }

                                }
                                else
                                {
                                    // ha be volt töltve küldemény iktatási adat,
                                    // egy hibajelenség leküzdésére:
                                    //LoadKuldemenyComponents();
                                }

                                break;
                            }
                    }
                }
                catch (FormatException fe)
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Formátum hiba!", fe.Message);
                    ErrorUpdatePanel1.Update();
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }

    #region BLG_232
    //LZS
    //Lekérjük a HIVSZAM_ELLENORZES rendszerparaméter értékét, és ha az „1” és a TabFooter.ConfirmationResult.Value nem „1”,
    //akkor lekérjük, hogy van - e már irat a felületen kiválasztott küldemény „Hivatkozási számával” a rendszerben.
    //Amennyiben nincs, akkor megyünk tovább az ágban, de ha igen, akkor feldobunk a felhasználónak egy figyelmeztető ablakot,
    //amelyen kiíratjuk a meglévő rekord „Azonosítóját” és „Tárgyát”. 
    //A felhasználó az ablakon „Ok”-t választva a végrehajtás egy speciális „ForceSave” postbackre fut, amit a Page_Load()-ban kezelünk le.
    private bool KettosIktatasControlling(string hivatkozasiSzam, EREC_IraIratokService service)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        bool hivszam_ellenorzes = Rendszerparameterek.Get(execParam, Rendszerparameterek.HIVSZAM_ELLENORZES) == "1" ? true : false;

        if (hivszam_ellenorzes && TabFooter1.ConfirmationResult.Value != "1")
        {
            bool isExist = false;
            EREC_IraIratokSearch hivatkozasiSzamSearch = new EREC_IraIratokSearch();
            hivatkozasiSzamSearch.HivatkozasiSzam.Filter(hivatkozasiSzam);

            Result resIratokHivatkozasiSzam = service.GetAll(new ExecParam(), hivatkozasiSzamSearch);

            if (!resIratokHivatkozasiSzam.IsError)
            {
                isExist = resIratokHivatkozasiSzam.GetCount > 0 ? true : false;
            }

            if (isExist)
            {
                string azonosito = "";
                string targy = "";
                string summary = "";

                foreach (DataRow irat in resIratokHivatkozasiSzam.Ds.Tables[0].Rows)
                {
                    azonosito = irat["Azonosito"].ToString();
                    targy = irat["Targy"].ToString();
                    summary += String.Format("\\n[{0}], [{1}]", azonosito, targy);
                }

                string msg = String.Format("A hivatkozásszám mező már szerepel az iratok táblában, a következő azonosítóval és tárggyal. {0} \\n\\nEnnek ellenére végre kívánja hajtani az iktatási műveletet?", summary);

                string js = String.Format("if (confirm('{0}')) {{ __doPostBack('','ForceSave'); }}", msg);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
                return true;
            }

            return false;
        }

        return false;
    }
    #endregion

    private EREC_IraIktatoKonyvek LoadUgyiratComponents(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        ElozmenyUgyiratID_HiddenField.Value = erec_UgyUgyiratok.Id;

        EREC_IraIktatoKonyvek iktatoKonyv = null;

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            iktatoKonyv = ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillWithOneValue(
               Constants.IktatoErkezteto.Iktato, erec_UgyUgyiratok.IraIktatokonyv_Id, EErrorPanel1);
        }
        else
        {
            // iktatásnál
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillAndSetSelectedValue(true, true, Constants.IktatoErkezteto.Iktato
                , erec_UgyUgyiratok.IraIktatokonyv_Id, false, EErrorPanel1);
        }

        //UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IKTATOSZAM_KIEG
        //    , erec_UgyUgyiratok.IktatoszamKieg, true, EErrorPanel1);

        //UgyUgyirat_UgyiratTextBox.SetUgyiratTextBoxById(EErrorPanel1);

        trFoszam.Visible = true;
        //UgyiratFoszam_Label.Enabled = true;

        string fullFoszam = "";

        #region Iktatókönyv lekérése:
        if (iktatoKonyv == null)
        {
            EREC_IraIktatoKonyvekService IktatoKonyvekservice = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;

            Result IktatoKonyvekresult = IktatoKonyvekservice.Get(execParam);
            if (!string.IsNullOrEmpty(IktatoKonyvekresult.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, IktatoKonyvekresult);
                if (ErrorUpdatePanel1 != null)
                {
                    ErrorUpdatePanel1.Update();
                }
                return null;
            }
            else
            {
                iktatoKonyv = (EREC_IraIktatoKonyvek)IktatoKonyvekresult.Record;
            }
        }

        #endregion

        // BLG_292
        ExecParam execParamFoszam = UI.SetExecParamDefault(Page, new ExecParam());
        fullFoszam = Ugyiratok.GetFullFoszam(execParamFoszam, erec_UgyUgyiratok, iktatoKonyv);

        UgyiratFoszam_Label.Text = fullFoszam;

        #region Elõzmény törlés gomb megjelenítése

        if (Command == CommandName.New && !FunctionRights.GetFunkcioJog(Page, funkcio_BejovoIratIktatasCsakAlszamra))
        {
            FoszamraIktat_ImageButton.Visible = true;
        }
        else
        {
            FoszamraIktat_ImageButton.Visible = false;
        }

        #endregion

        if (!_Load)
        {
            UgyUgyirat_Hatarido_CalendarControl.Text = erec_UgyUgyiratok.Hatarido;
            inputUgyUgyintezesiNapok.Value = erec_UgyUgyiratok.IntezesiIdo;
            // BLG_1020
            if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IntezesiIdoegyseg))
            {
                IntezesiIdoegyseg_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdoegyseg;
            }
            //IratIntezesiIdo_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdo;
            //if (!String.IsNullOrEmpty(erec_UgyUgyiratok.IntezesiIdoegyseg))
            //{
            //    IratIntezesiIdoegyseg_KodtarakDropDownList.SelectedValue = erec_UgyUgyiratok.IntezesiIdoegyseg;
            //}
            IktatasDatuma_Irat_HiddenField.Value = erec_UgyUgyiratok.Base.LetrehozasIdo;

            ElozmenyUgyiratHatarido_HiddenField.Value = erec_UgyUgyiratok.Hatarido;
            IktatasDatuma_Ugyirat_HiddenField.Value = erec_UgyUgyiratok.Base.LetrehozasIdo;
            FelhasznaloCsoportIdOrzo_HiddenField.Value = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;

            EREC_IratMetaDefinicio erec_IratMetaDefinicio = IratMetaDefinicio.GetBusinessObjectById(Page, erec_UgyUgyiratok.IratMetadefinicio_Id);
            IratMetaDefinicio.SetHataridoToolTipByIratMetaDefinicio(Page, erec_IratMetaDefinicio, UgyUgyirat_Hatarido_CalendarControl.TextBox);
            if (erec_IratMetaDefinicio != null)
            {
                UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = (String.IsNullOrEmpty(erec_IratMetaDefinicio.UgyiratHataridoKitolas) ? "1" : erec_IratMetaDefinicio.UgyiratHataridoKitolas);
                if (erec_IratMetaDefinicio.UgyiratHataridoKitolas != "0"
                    && bUgyiratHataridoModositasiJog
                    && erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez == FelhasznaloProfil.FelhasznaloId(Page))
                {
                    UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
                    if (Uj_Ugyirat_Hatarido_Kezeles)
                    {
                        trUgyiratIntezesiIdoVisible = true;
                        // BLG_1020
                        trIratIntezesiIdo.Visible = true;
                    }
                }
            }
            else
            {
                UgyiratHataridoKitolas_Ugyirat_HiddenField.Value = "1";
                if (bUgyiratHataridoModositasiJog
                    && erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez == FelhasznaloProfil.FelhasznaloId(Page))
                {
                    UgyUgyirat_Hatarido_CalendarControl.ReadOnly = false;
                    if (Uj_Ugyirat_Hatarido_Kezeles)
                    {
                        trUgyiratIntezesiIdoVisible = true;
                        // BLG_1020
                        trIratIntezesiIdo.Visible = true;
                    }
                }
            }

            // BLG_8826
            _iratForm.SetUgyintezesKezdete(erec_UgyUgyiratok, GetUgyintezesKezdete().ToString());
        }

        //UgyUgyirat_Ugytipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYTIPUS
        //    , erec_UgyUgyiratok.UgyTipus, true, EErrorPanel1);

        UgyUgyirat_SkontroVege_CalendarControl.Text = erec_UgyUgyiratok.SkontroVege;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = erec_UgyUgyiratok.IrattarbaKuldDatuma;

        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField =
            erec_UgyUgyiratok.IraIrattariTetel_Id;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxById(EErrorPanel1);

        UgyUgyirat_Targy_RequiredTextBox.Text = erec_UgyUgyiratok.Targy;

        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Felelos;
        UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField =
            erec_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez;
        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        Ugyfelelos_CsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Ugyfelelos;
        Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(erec_UgyUgyiratok.Partner_Id_Ugyindito, erec_UgyUgyiratok.NevSTR_Ugyindito, EErrorPanel1);
        CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(erec_UgyUgyiratok.Cim_Id_Ugyindito, erec_UgyUgyiratok.CimSTR_Ugyindito, EErrorPanel1);

        UgyiratOrzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratok.FelhasznaloCsoport_Id_Orzo;
        UgyiratOrzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

        regiAzonositoTextBox.Text = erec_UgyUgyiratok.RegirendszerIktatoszam;

        if (isTUKRendszer && !string.IsNullOrEmpty(erec_UgyUgyiratok.IrattarId))
        {
            IrattariHelyLevelekDropDownTUK.SelectedValue = erec_UgyUgyiratok.IrattarId;
        }

        // BLG_44
        IrattariTetelszam_DropDownList.SetSelectedValue(erec_UgyUgyiratok.IraIrattariTetel_Id);
        // BUG_1499
        //UGY_FAJTAJA_KodtarakDropDownList.SetSelectedValue(erec_UgyUgyiratok.Ugy_Fajtaja);
        SetMergeItszFromDDL();
        UGY_FAJTAJA_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGY_FAJTAJA, erec_UgyUgyiratok.Ugy_Fajtaja, EErrorPanel1);
        InitializeIratHatasaDDL(null);
        // BUG_1499
        //erec_UgyUgyiratok.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;
        //erec_UgyUgyiratok.Updated.IraIrattariTetel_Id = pageView.GetUpdatedByView(Ugykor_DropDownList);

        return iktatoKonyv;
    }

    private void ClearUgyiratComponents()
    {
        ElozmenyUgyiratID_HiddenField.Value = String.Empty;

        //// iktatásnál
        //ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.FillDropDownList(true, Constants.IktatoErkezteto.Iktato
        //    , false, EErrorPanel1);

        trFoszam.Visible = false;

        UgyiratFoszam_Label.Text = String.Empty;

        SetDefaultHataridok();
        ElozmenyUgyiratHatarido_HiddenField.Value = String.Empty;

        UgyUgyirat_SkontroVege_CalendarControl.Text = String.Empty;
        UgyUgyirat_IrattarbaHelyezes_CalendarControl.Text = String.Empty;

        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField = String.Empty;
        UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text = String.Empty;

        UgyUgyirat_Targy_RequiredTextBox.Text = String.Empty;

        //Kezelõ kitöltve alapból a felhasználóra:
        UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
        UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = String.Empty;
        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text = String.Empty;

        Ugyfelelos_CsoportTextBox.Id_HiddenField = String.Empty;
        Ugyfelelos_CsoportTextBox.Text = String.Empty;

        // ügyindító, ügyfél
        Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField = String.Empty;
        Ugy_PartnerId_Ugyindito_PartnerTextBox.Text = String.Empty;
        CimekTextBoxUgyindito.Id_HiddenField = String.Empty;
        CimekTextBoxUgyindito.Text = String.Empty;

        regiAzonositoTextBox.Text = String.Empty;

        Merge_IrattariTetelszamTextBox.Text = String.Empty;

        if (isTUKRendszer)
        {
            ReloadIrattariHelyLevelekDropDownTUK(UgyUgyiratok_CsoportId_Felelos.HiddenField.Value, null);
        }

        _iratForm.SetUgyintezesKezdete(null, "");
    }

    protected bool KuldemenyAtadasErtesitesVisible
    {
        get
        {
            object o = ViewState["KuldemenyAtadasErtesitesVisible"];

            if (o != null)
                return (bool)o;

            return false;
        }
        set
        {
            ViewState["KuldemenyAtadasErtesitesVisible"] = value;
        }
    }

    private void SetErkeztetesResultPanel(ErkeztetesIktatasResult erkeztetesResult)
    {
        SetErkeztetesResultPanel(erkeztetesResult, false);
    }

    // ha volt iktatás, a küldemény nem módosítható, eltüntetjük a gombot
    private void SetErkeztetesResultPanel(ErkeztetesIktatasResult erkeztetesResult, bool voltIktatas)
    {
        //Panel-ek beállítása
        KuldemenyPanel.Visible = false;
        EmailKuldemenyPanel.Visible = false;
        Ugyirat_Panel.Visible = false;
        IratPanel.Visible = false;
        StandardTargyszavakPanel.Visible = false;
        StandardTargyszavakPanel_Iratok.Visible = false;
        TipusosTargyszavakPanel_Iratok.Visible = false;
        ErkeztetesVagyIktatasRadioButtonList.Visible = false;
        eBeadvanyokPanel.Visible = false;
        BejovoPeldanyListPanel1.Visible = false;

        ErkeztetesResultPanel.Visible = true;

        //TabFooter beállítása
        TabFooter1.ImageButton_Close.Visible = true;
        TabFooter1.ImageButton_Close.OnClientClick = JavaScripts.GetOnCloseClientClickScript();

        // Email érkeztetéskor nem kell a Rendben és új gomb:
        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            TabFooter1.Link_SaveAndNew.Visible = false;
        }
        else
        {
            TabFooter1.Link_SaveAndNew.Visible = true;
        }

        TabFooter1.ImageButton_Save.Visible = false;
        TabFooter1.ImageButton_Cancel.Visible = false;

        //string saveAndNewUrl = Request.Url.OriginalString;
        string saveAndNewUrl = "~/EgyszerusitettIktatasForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New;

        // ha volt betöltött template:
        if (!String.IsNullOrEmpty(FormHeader1.CurrentTemplateId))
        {
            saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader1.CurrentTemplateId;
        }
        TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

        if (erkeztetesResult == null)
        {
            Logger.Error("iktatasResult == null");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ErkeztetesResultPanel.Visible = false;
            return;
        }
        if (String.IsNullOrEmpty(erkeztetesResult.KuldemenyId))
        {
            Logger.Error("String.IsNullOrEmpty(erkeztetesResult.KuldemenyId)");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ErkeztetesResultPanel.Visible = false;
            return;
        }

        string kuldemenyID = erkeztetesResult.KuldemenyId;

        try
        {
            //Étkeztetõszám beállítása
            labelKuldemenyErkSzam.Text = erkeztetesResult.KuldemenyAzonosito ?? Resources.Error.UIAzonositoUndefined;

            //Módosítás,megtekintés gombok beállítása
            imgKuldemenyMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            if (!voltIktatas)
            {
                imgKuldemenyModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                                "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                KuldemenyAtadasErtesitesVisible = true;
                imgAtadas.Enabled = FunctionRights.GetFunkcioJog(Page, "KuldemenyAtadas");

                // CR3331 Email iktatáskor automatikus átadás (nem csak FPHban)
                // Áthelyezve késõbbre (csak hogy egy helyen legyen)
                //imgAtadas.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("AtadasForm.aspx", 
                //    QueryStringVars.Command + "=" + CommandName.Atadas
                //    + "&" + QueryStringVars.KuldemenyId + "=" + kuldemenyID,
                //    Defaults.PopupWidth_Max, Defaults.PopupHeight);
                imgEmailErtesites.CommandArgument = kuldemenyID;

                // modified by nekrisz : EMAIL_ERKEZTETES_UZENET

                //imgEmailErtesites.OnClientClick = JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
                //    QueryStringVars.HiddenFieldId + "=" + hfEmailAddresses.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + hfEmailMessage.ClientID
                //    + "&Message=" + HttpUtility.UrlEncode("Szervezeti egysége részére e-mail érkezett. Az e-mail érkeztetésre került, gondoskodjon a további ügyintézésrõl, a küldemény esetleges iktatásáról.")
                //    , Defaults.PopupWidth, Defaults.PopupHeight, imgEmailErtesites.ClientID, kuldemenyID, true);
                ExecParam xcParam = UI.SetExecParamDefault(Page, new ExecParam());
                imgEmailErtesites.OnClientClick = JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
                   QueryStringVars.HiddenFieldId + "=" + hfEmailAddresses.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + hfEmailMessage.ClientID
                   + "&Message=" + HttpUtility.UrlEncode(Rendszerparameterek.Get(xcParam, Rendszerparameterek.EMAIL_ERKEZTETES_UZENET))
                   , Defaults.PopupWidth, Defaults.PopupHeight, imgEmailErtesites.ClientID, kuldemenyID, true);

                imgEmailErtesites.Command += new CommandEventHandler(imgEmailErtesites_Command);
            }
            else
            {
                imgKuldemenyModosit.Visible = false;
                KuldemenyAtadasErtesitesVisible = false;
            }

            //CR3058
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
            {

                ImageButtonVonalkodNyomtatasKuldemeny.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Kuldemeny&" + QueryStringVars.Command + "=Modify" + "')";
            }
            else
            {
                ImageButtonVonalkodNyomtatasKuldemeny.Visible = false;
                ImageButtonVonalkodNyomtatasKuldemeny.Enabled = false;
            }

            #region CR3140 - Iktatószám visszajelzõ panelre is kerüljön ki az Átadás gomb, van amikor kinn láttam már, de általánosságban legyen kinn, mert hasznos. CR3183 - org független
            //if (FelhasznaloProfil.OrgIsBOPMH(Page))
            //{
            Atadasra_kijelolesUgyirat.Visible = true;
            Atadasra_kijelolesUgyirat.Enabled = true;
            imgAtadas.Visible = true;
            imgAtadas.Enabled = true;
            // Átadásra kijelölés
            if (Atadasra_kijelolesUgyirat.Enabled)
            {
                if (!string.IsNullOrEmpty(erkeztetesResult.UgyiratId))
                {
                    var statusz = Ugyiratok.GetAllapotById(erkeztetesResult.UgyiratId, Page, EErrorPanel1);
                    Ugyiratok.SetAtadasraKijelolesMulti_Funkciogomb(erkeztetesResult.UgyiratId, statusz, Atadasra_kijelolesUgyirat, Page);
                }
            }
            // CR3324 - FPH-ban továbbra is egylépcsõs átadás kell az email érkeztetéskor
            // Ha mindenhol fut ez a rész, felüldefiniálja az FPH-s mûködést
            //if (FelhasznaloProfil.OrgIsBOPMH(Page))
            //{
            if (imgAtadas.Enabled)
            {
                if (!string.IsNullOrEmpty(erkeztetesResult.KuldemenyId))
                {
                    // CR3331 Email iktatáskor automatikus átadás (nem csak FPHban)
                    var statusz = Kuldemenyek.GetAllapotById(erkeztetesResult.KuldemenyId, Page, EErrorPanel1);
                    if (Mode == ErkeztetesTipus.EmailErkeztetes)
                    {
                        imgAtadas.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("AtadasForm.aspx",
                            QueryStringVars.Command + "=" + CommandName.Atadas
                            + "&" + QueryStringVars.KuldemenyId + "=" + kuldemenyID,
                            Defaults.PopupWidth_Max, Defaults.PopupHeight);

                    }
                    else
                    {
                        // var statusz = Kuldemenyek.GetAllapotById(erkeztetesResult.KuldemenyId, Page, EErrorPanel1);
                        Kuldemenyek.SetAtadasraKijeloles(erkeztetesResult.KuldemenyId, statusz, imgAtadas, UpdatePanel1);
                    }

                    //  }
                }
            }
            //}
            #endregion
            #region CR3153 - BOPMH-ban az iktatás visszajelzõ képernyõre kellene irat átadas gomb is. CR3183 - org független
            //if (FelhasznaloProfil.OrgIsBOPMH(Page))
            //{
            Atadasra_kijelolesIrat.Visible = true;
            Atadasra_kijelolesIrat.Enabled = true;
            imgAtadas.Visible = true;
            imgAtadas.Enabled = true;
            // Átadásra kijelölés
            if (Atadasra_kijelolesIrat.Enabled)
            {
                if (!string.IsNullOrEmpty(erkeztetesResult.IratId))
                {
                    //GetElsoIratPeldanyByIraIrat
                    var elsoIratpeldany = new EREC_PldIratPeldanyok();
                    using (var service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
                    {
                        var res = service.GetElsoIratPeldanyByIraIrat(UI.SetExecParamDefault(Page), erkeztetesResult.IratId);
                        if (res.IsError)
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
                            ErrorUpdatePanel1.Update();
                            ErkeztetesResultPanel.Visible = false;
                            return;
                        }
                        elsoIratpeldany = (EREC_PldIratPeldanyok)res.Record;

                    }

                    if (elsoIratpeldany != null)
                    {
                        var ugyiratStatusz = Ugyiratok.GetAllapotById(erkeztetesResult.UgyiratId, UI.SetExecParamDefault(Page), EErrorPanel1);
                        var iratStatusz = Iratok.GetAllapotById(erkeztetesResult.IratId, UI.SetExecParamDefault(Page), EErrorPanel1);
                        var iratPeldanyStatusz = IratPeldanyok.GetAllapotById(elsoIratpeldany.Id, UI.SetExecParamDefault(Page), EErrorPanel1);
                        IratPeldanyok.SetAtadasraKijelolesMulti_Funkciogomb(elsoIratpeldany.Id, iratPeldanyStatusz, iratStatusz, ugyiratStatusz, Atadasra_kijelolesIrat, Page);
                    }
                    else
                    {
                        Atadasra_kijelolesIrat.Enabled = false;
                    }
                }
            }
            //}
            #endregion
        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }

        if (!voltIktatas)
        {
            FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);

            if (fm != null)
            {
                fm.ObjektumId = kuldemenyID;
                fm.ObjektumType = Constants.TableNames.EREC_KuldKuldemenyek;
            }
        }

        if (Mode == ErkeztetesTipus.EmailErkeztetes)
        {
            imgSendEmailErkeztetve.Visible = true;
            imgSendEmailErkeztetve.CommandArgument = erkeztetesResult.KuldemenyAzonosito;
            imgSendEmailErkeztetve.Enabled = FunctionRights.HasFunctionRight(UI.SetExecParamDefault(Page), "UgyfelkapuValaszGeneralas");
        }
        else
        {
            imgSendEmailErkeztetve.Visible = false;
        }
    }

    protected void imgSendEmailErkeztetve_Command(object sender, CommandEventArgs e)
    {
        if (!String.IsNullOrEmpty(EmailBoritekokId))
        {
            // Emailboríték lekérdezése:
            EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
            execparam_emailGet.Record_Id = EmailBoritekokId;

            Result result_emailGet = erec_eMailBoritekokService.Get(execparam_emailGet);
            if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                ErrorUpdatePanel1.Update();
                return;
            }

            EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)result_emailGet.Record;

            string erkeztetoszam = e.CommandArgument as string;

            Contentum.eAdmin.Service.EmailService emailService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetEmailService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            bool ret = emailService.SendEmailErkeztetve(execParam, erec_eMailBoritekok, erkeztetoszam);

            if (ret)
            {
                string js = "alert('Az e-mail küldése sikeres!')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
            }
            else
            {
                string js = "alert('Az e-mail küldése sikertelen!')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
            }
        }
    }

    protected void imgEmailErtesites_Command(object sender, CommandEventArgs e)
    {
        if (!String.IsNullOrEmpty(hfEmailAddresses.Value))
        {
            string kuldemenyID = e.CommandArgument.ToString();
            Notify.SendSelectedItemsByEmail(Page, new List<string> { kuldemenyID }, UI.StringToListBySeparator(hfEmailAddresses.Value, ';'), hfEmailMessage.Value, "EREC_KuldKuldemenyek");
        }
    }

    // a küldemény Id meghatározása után után meghívja a SetErkeztetesResultPanelt is
    private void SetResultPanel(ErkeztetesIktatasResult erkeztesIktatasResult)
    {
        //Panel-ek beállítása
        KuldemenyPanel.Visible = false;
        EmailKuldemenyPanel.Visible = false;
        Ugyirat_Panel.Visible = false;
        //UgyiratDarab_Panel.Visible = false;
        IratPanel.Visible = false;
        ResultPanel.Visible = true;
        StandardTargyszavakPanel.Visible = false;
        StandardTargyszavakPanel_Iratok.Visible = false;
        ErkeztetesVagyIktatasRadioButtonList.Visible = false;
        UgyiratSzerelesiLista1.Reset();
        eBeadvanyokPanel.Visible = false;
        BejovoPeldanyListPanel1.Visible = false;

        //TabFooter beállítása
        TabFooter1.ImageButton_Close.Visible = true;
        TabFooter1.ImageButton_Close.OnClientClick = JavaScripts.GetOnCloseClientClickScript();

        // Rendben és új gomb nem kell az email iktatásnál
        // (Nem kell a rendben és köv. alszám sem):
        if (Mode == ErkeztetesTipus.EmailErkeztetes || isHivataliKapusErkeztetes)
        {
            TabFooter1.Link_SaveAndNew.Visible = false;
            TabFooter1.Link_SaveAndAlszamBejovo.Visible = false;
            TabFooter1.Link_SaveAndAlszamBelso.Visible = false;
        }
        else
        {
            TabFooter1.Link_SaveAndNew.Visible = true;
            TabFooter1.Link_SaveAndAlszamBejovo.Visible = true;
            TabFooter1.Link_SaveAndAlszamBelso.Visible = true;
        }
        TabFooter1.ImageButton_Save.Visible = false;
        TabFooter1.ImageButton_Cancel.Visible = false;

        string saveAndNewUrl = String.Empty;

        saveAndNewUrl = "~/EgyszerusitettIktatasForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New;

        // ha volt betöltve template:
        if (!String.IsNullOrEmpty(FormHeader1.CurrentTemplateId))
        {
            saveAndNewUrl += "&" + QueryStringVars.TemplateId + "=" + FormHeader1.CurrentTemplateId;
        }

        TabFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

        if (erkeztesIktatasResult == null)
        {
            Logger.Error("iktatasResult == null");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ResultPanel.Visible = false;
            return;
        }

        if (String.IsNullOrEmpty(erkeztesIktatasResult.UgyiratId) || String.IsNullOrEmpty(erkeztesIktatasResult.IratId))
        {
            Logger.Error("String.IsNullOrEmpty(iktatasResult.UgyiratId) || String.IsNullOrEmpty(iktatasResult.IratId)");
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.Error.UIErkeztetesIktatasResultError);
            ErrorUpdatePanel1.Update();
            ResultPanel.Visible = false;
            return;
        }

        string ugyiratId = erkeztesIktatasResult.UgyiratId;
        string iratId = erkeztesIktatasResult.IratId;

        // Felirat az IktatasResult panelen:
        if (erkeztesIktatasResult.MunkaPeldany)
        {
            // A munkapéldány sikeresen létrejött.
            Label_ResultPanelText.Text = Resources.Form.MunkaanyagLetrehozasResultText;
        }
        else
        {
            // Az iktatás sikeresen végrehajtódott.
            Label_ResultPanelText.Text = Resources.Form.IktatasResultText;
        }

        try
        {
            //Iktatószámok, érkeztetõszám beállítása

            //Ugyirat           
            labelUgyiratFoszam.Text = erkeztesIktatasResult.UgyiratAzonosito ?? Resources.Error.UIAzonositoUndefined;

            //Irat            
            labelIratFoszam.Text = erkeztesIktatasResult.IratAzonosito ?? Resources.Error.UIAzonositoUndefined;

            // küldemény
            if ((Mode == ErkeztetesTipus.EmailErkeztetes || isHivataliKapusErkeztetes) && SubMode == EmailBoritekTipus.Kuldemeny)
            {
                // nem kellett érkeztetni
            }
            else
            {
                SetErkeztetesResultPanel(erkeztesIktatasResult, true);
            }

            string saveAndAlszamBejovoUrl = "~/EgyszerusitettIktatasForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New
                + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;
            TabFooter1.Link_SaveAndAlszamBejovo.NavigateUrl = saveAndAlszamBejovoUrl;

            string saveAndAlszamBelsoUrl = String.Empty;
            saveAndAlszamBelsoUrl = "~/IraIratokForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New
                + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId
                + "&" + QueryStringVars.Startup + "=" + Constants.Startup.FromEgyszerusitettIktatas;

            TabFooter1.Link_SaveAndAlszamBelso.NavigateUrl = saveAndAlszamBelsoUrl;

            // ha módosult az ügyirat határideje alszámos iktatásnál, megjelenítjük:
            if (!String.IsNullOrEmpty(erkeztesIktatasResult.UgyiratUjHatarido))
            {
                tr_resultPanel_UgyiratUjHatarido.Visible = true;
                labelResultUgyiratUjHatarido.Text = erkeztesIktatasResult.UgyiratUjHatarido;
            }

            //Módosítás,megtekintés gombok beállítása
            //ügyirat
            imgUgyiratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgUgyiratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            //imgEloadoiIv.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("EloadoiIv_PrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId);
            imgEloadoiIv.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokPrintFormSSRSEloadoiIv.aspx", QueryStringVars.UgyiratId + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            //irat
            imgIratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgIratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgIratModositHatosagi.OnClientClick = UI.OpenHatosagiAdatokWithNoPostback(iratId, true);
            imgeMailNyomtatas.OnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("eMail_PrintForm.aspx?" + QueryStringVars.IratId + "=" + iratId + "&" + QueryStringVars.EmailBoritekokId + "=" + EmailBoritekokId);

            //HA NEM TÜK-ös
            if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.TUK).ToUpper().Trim().Equals("0"))
            {
                //CR3058
                if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
                {
                    ImageButtonVonalkodNyomtatasUgyirat.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + ugyiratId + "&tipus=Ugyirat&" + QueryStringVars.Command + "=Modify" + "')";
                    //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny"); 
                    ImageButtonVonalkodNyomtatasIrat.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.IratId + "=" + iratId + "&tipus=Irat&" + QueryStringVars.Command + "=Modify" + "')";
                    //JavaScripts.SetOnClientClickIFramePrintForm("VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + ParentId + "&tipus=Kuldemeny"); 
                    ////ImageButtonVonalkodNyomtatasKuldemeny.OnClientClick = "javascript:window.open('VonalkodTabPrintForm.aspx?" + QueryStringVars.KuldemenyId + "=" + kuldemenyID + "&tipus=Kuldemeny&" + QueryStringVars.Command + "=Modify" + "')";
                }
                else
                {
                    ImageButtonVonalkodNyomtatasUgyirat.Visible = false;
                    ImageButtonVonalkodNyomtatasUgyirat.Enabled = false;
                    //JavaScripts.SetOnClientClickIFrame
                    ImageButtonVonalkodNyomtatasIrat.Visible = false;
                    ImageButtonVonalkodNyomtatasIrat.Enabled = false;

                    //ImageButtonVonalkodNyomtatasKuldemeny.Visible = false;
                    //ImageButtonVonalkodNyomtatasKuldemeny.Enabled = false;
                }
            }
            else//HA TÜK-ös
            {
                bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
                ImageButtonVonalkodNyomtatasUgyirat.Visible = _visible;
                ImageButtonVonalkodNyomtatasUgyirat.Enabled = _visible;
                ImageButtonVonalkodNyomtatasIrat.Visible = _visible;
                ImageButtonVonalkodNyomtatasIrat.Enabled = _visible;

                // BLG_1052 inserted by nekrisz
                ImageButtonVonalkodNyomtatasKuldemeny.Visible = _visible;
                ImageButtonVonalkodNyomtatasKuldemeny.Enabled = _visible;
            }

            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = iratId;

            Result result = service.Get(execParam);

            if (string.IsNullOrEmpty(result.ErrorMessage))
            {
                EREC_IraIratok erec_IraIratok = (EREC_IraIratok)result.Record;

                if (erec_IraIratok.AdathordozoTipusa.Equals("1"))
                {
                    imgeMailNyomtatas.Visible = true;
                }
                else
                {
                    imgeMailNyomtatas.Visible = false;
                }

                // Bejövõ irat esetén a ahtósági adatok gomb eltüntetése
                // BLG_354
                // BLG_2020 (execParam)
                if (!Iratok.IsHatosagiUgy(execParam, iratId))
                {
                    imgIratModositHatosagi.Visible = false;
                }
                else
                    if ((erec_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo) && (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.HATOSAGI_ADATOK_BEJOVOHOZ).ToUpper().Trim().Equals("NEM")))
                {
                    imgIratModositHatosagi.Visible = false;
                }
                else
                    imgIratModositHatosagi.Visible = true;

                //if (erec_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                //    imgIratModositHatosagi.Visible = false;
                //else
                //    imgIratModositHatosagi.Visible = true;
            }
            else
            {
                imgeMailNyomtatas.Visible = false;
            }
        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }

        FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);

        if (fm != null)
        {
            fm.ObjektumId = iratId;
            fm.ObjektumType = Constants.TableNames.EREC_IraIratok;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Mode == ErkeztetesTipus.EmailErkeztetes || isHivataliKapusErkeztetes)
        {
            labelVonalkodPirosCsillag.Visible = false;
            VonalkodTextBoxVonalkod.RequiredValidate = false;
        }
        else if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT) == "1"
               && AdathordozoTipusa_DropDownList.SelectedValue == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
        {
            labelVonalkodPirosCsillag.Visible = false;
            VonalkodTextBoxVonalkod.RequiredValidate = false;
        }
        else
        {
            VonalkodTextBoxVonalkod.Validate = true;
            labelVonalkodPirosCsillag.Visible = true;
        }
        // CR 3058
        if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH)
        {
            labelVonalkodPirosCsillag.Visible = false;
        }

        if (ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible)
        {
            //Szûrés beállítása a csoportTextBox-okra
            Ugyfelelos_CsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            UgyUgyiratok_CsoportId_Felelos.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterByIktatokonyv(ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.DropDownList);
        }
        else
        {
            //Szûrés beállítása a csoportTextBox-okra
            Ugyfelelos_CsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            UgyUgyiratok_CsoportId_Felelos.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
            Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterByIktatokonyv(Iktatokonyvek_DropDownLis_Ajax);
        }

        UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetFilterBySzervezet(Ugyfelelos_CsoportTextBox);
        Irat_Ugyintezo_FelhasznaloCsoportTextBox.SetFilterBySzervezet(Ugyfelelos_CsoportTextBox);

        // CR3113 Ügyindító nevét, címét automatikusan töltse a küldõ/feladó alapján

        //LZS - BUG_6999
        if (!Ugy_PartnerId_Ugyindito_PartnerTextBox.ReadOnly)
        {
            Bekuldo_PartnerTextBox.ClonePartnerTextBoxClientId = Ugy_PartnerId_Ugyindito_PartnerTextBox.TextBox.ClientID;
            Bekuldo_PartnerTextBox.ClonePartnerHiddenFieldClientId = Ugy_PartnerId_Ugyindito_PartnerTextBox.Control_HiddenField.ClientID;
            Bekuldo_PartnerTextBox.CloneCimTextBox = CimekTextBoxUgyindito.TextBox;
            Bekuldo_PartnerTextBox.CloneCimHiddenField = CimekTextBoxUgyindito.HiddenField;
        }

        // Email érkeztetésnél Partner
        Email_Bekuldo_PartnerTextBox.ClonePartnerTextBox = Ugy_PartnerId_Ugyindito_PartnerTextBox;
        Email_Bekuldo_PartnerTextBox.CloneCimTextBox = CimekTextBoxUgyindito.TextBox;
        Email_Bekuldo_PartnerTextBox.CloneCimHiddenField = CimekTextBoxUgyindito.HiddenField;


        if (!FelhasznaloProfil.OrgIsNMHH(Page))
        {
            LabelIratHatasaUgyintezesre.Visible = false;
            IratHatasaUgyintezesre_KodtarakDropDownList.Visible = false;
        }

        if (SubMode != EmailBoritekTipus.Kuldemeny)
        {
            // BUG_8604
            _Load = true;
            EREC_KuldKuldemenyek kuldemeny = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
            // BUG_8604
            _Load = false;
            BejovoPeldanyListPanel1.Partner_Id_Cimzett = kuldemeny.Csoport_Id_Cimzett;
        }

        BejovoPeldanyListPanel1.ParentClonetTextboxClientId = CsoportId_CimzettCsoportTextBox1.TextBox.ClientID; ;
        BejovoPeldanyListPanel1.ParentCloneHiddenFieldClientId = CsoportId_CimzettCsoportTextBox1.HiddenField.ClientID;

        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRATTAROZAS_JOVAHAGYO_UGYFELELOS, false))
        {
            Ugyfelelos_CsoportTextBox.Validate = !Ugyfelelos_CsoportTextBox.ReadOnly && Ugyfelelos_CsoportTextBox.Visible;
        }

        //LZS - BUG_7248
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            lblUgyindito.Text = Resources.Form.UI_Ugyfel_Ugyindito_Szervezet;
        }

        // BLG_8826
        if (!Page.IsPostBack)
        {
            _iratForm.SetDefaultUgyintezesKezdete();
        }
    }

    protected override void Render(HtmlTextWriter writer)
    {
        KezelesiFeljegyzesPanelKuldemeny.Visible = (((EmailKuldemenyPanel.Visible && Mode == ErkeztetesTipus.EmailErkeztetes) || (eBeadvanyokPanel.Visible && isHivataliKapusErkeztetes)) && IsCsakErkeztetes);//ErkeztetesVagyIktatasRadioButtonList.SelectedValue == "1");
        base.Render(writer);
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            _Load = true;

            LoadComponentsFromTemplate((IktatasFormTemplateObject)FormHeader1.TemplateObject);

            _Load = false;
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            try
            {
                FormHeader1.NewTemplate(GetTemplateObjectFromComponents());
            }
            catch (FormatException fe)
            {

                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Formátum hiba!", fe.Message);
                ErrorUpdatePanel1.Update();
            }
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader1.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    private void LoadComponentsFromTemplate(IktatasFormTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            // csak ha nincs megadva elõzmény:
            if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                #region Ügyirat komponensek

                CascadingDropDown_AgazatiJel.SelectedValue = templateObject.AgazatiJel_Id;
                CascadingDropDown_Ugykor.SelectedValue = templateObject.UgyiratComponents.IraIrattariTetel_Id;
                CascadingDropDown_IktatoKonyv.SelectedValue = templateObject.UgyiratComponents.IraIktatokonyv_Id;
                CascadingDropDown_Ugytipus.SelectedValue = templateObject.UgyiratComponents.UgyTipus;

                UgyUgyirat_Targy_RequiredTextBox.Text = templateObject.UgyiratComponents.Targy;

                Ugyfelelos_CsoportTextBox.Id_HiddenField = templateObject.UgyiratComponents.Csoport_Id_Ugyfelelos;
                Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                UgyUgyiratok_CsoportId_Felelos.Id_HiddenField = templateObject.UgyiratComponents.Csoport_Id_Felelos;
                UgyUgyiratok_CsoportId_Felelos.SetCsoportTextBoxById(EErrorPanel1);

                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField =
                    templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez;
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);

                // ügyindító/ügyfél
                Ugy_PartnerId_Ugyindito_PartnerTextBox.SetPartnerTextBoxByStringOrId(templateObject.UgyiratComponents.Partner_Id_Ugyindito, templateObject.UgyiratComponents.NevSTR_Ugyindito, EErrorPanel1);
                CimekTextBoxUgyindito.SetCimekTextBoxByStringOrId(templateObject.UgyiratComponents.Cim_Id_Ugyindito, templateObject.UgyiratComponents.CimSTR_Ugyindito, EErrorPanel1);

                #endregion
            }

            #region Irat komponensek

            //IraIrat_Kategoria_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATKATEGORIA, templateObject.IratComponents.Kategoria
            //    , true, EErrorPanel1);

            IraIrat_Irattipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, templateObject.IratComponents.Irattipus
                , true, EErrorPanel1);

            IraIrat_KiadmanyozniKell_CheckBox.Checked = (templateObject.IratComponents.KiadmanyozniKell == "1") ? true : false;

            IraIrat_Targy_RequiredTextBox.Text = templateObject.IratComponents.Targy;

            //IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KEZELESI_FELJEGYZESEK_TIPUSA,
            //  templateObject.IraKezFeljegyz_KezelesTipus, true, EErrorPanel1);

            //IraKezFeljegyz_Leiras_TextBox.Text = templateObject.IraKezFeljegyz_Leiras;

            #endregion

            #region Kuldemeny komponensek

            if (templateObject.KuldemenyComponents != null)
            {
                LoadKuldemenyComponents(templateObject.KuldemenyComponents);
            }

            #endregion

            #region elozmeny search

            if (templateObject.ElozmenySearch != null)
            {
                evIktatokonyvSearch.EvTol = templateObject.ElozmenySearch.Ev;
                bool isTUK = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK, false);
                if (isTUK)
                {
                    //TÜK esetén az id lesz az érték mezőnek feltöltve, mivel itt Id szerinti keresés lesz
                    IktatoKonyvekDropDownList_Search.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato, templateObject.ElozmenySearch.Ev, templateObject.ElozmenySearch.Ev, true,
                     false, templateObject.ElozmenySearch.Iktatokonyv, EErrorPanel1);
                }
                else
                {
                    IktatoKonyvekDropDownList_Search.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, templateObject.ElozmenySearch.Ev, templateObject.ElozmenySearch.Ev, true,
                     false, templateObject.ElozmenySearch.Iktatokonyv, EErrorPanel1);
                }
                numberFoszamSearch.Text = templateObject.ElozmenySearch.Foszam;
            }

            #endregion

            #region hataridos feladat

            if (templateObject.HataridosFeladat != null)
            {
                FeljegyzesPanel.SetFeladatByBusinessObject(templateObject.HataridosFeladat);
            }

            #endregion

            #region #Email

            if (ErkeztetesVagyIktatasRadioButtonList.Visible && !String.IsNullOrEmpty(templateObject.ErtkeztetesVagyIktatas))
            {
                //ErkeztetesVagyIktatasRadioButtonList.SelectedValue = templateObject.ErtkeztetesVagyIktatas;
                EmailKezelesSelector.SetSelectedValue(ErkeztetesVagyIktatasRadioButtonList, templateObject.ErtkeztetesVagyIktatas);
                ErkeztetesVagyIktatasRadioButtonList_SelectedIndexChanged(ErkeztetesVagyIktatasRadioButtonList, EventArgs.Empty);
            }

            #endregion

            if (IsRequireOverwriteTemplateDatas())
            {
                LoadSessionData();
            }

        }
    }

    private IktatasFormTemplateObject GetTemplateObjectFromComponents()
    {
        //EREC_UgyUgyiratok erec_UgyUgyiratok = new EREC_UgyUgyiratok();      
        IktatasFormTemplateObject templateObject = new IktatasFormTemplateObject();

        // csak ha nincs megadva elõzmény:
        if (String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
        {
            #region Ügyirat komponensek

            templateObject.AgazatiJel_Id = AgazatiJelek_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.IraIrattariTetel_Id = Ugykor_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.IraIktatokonyv_Id = Iktatokonyvek_DropDownLis_Ajax.SelectedValue; ;

            templateObject.UgyiratComponents.UgyTipus = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;

            templateObject.UgyiratComponents.Targy = UgyUgyirat_Targy_RequiredTextBox.Text;

            // intézési határidõ nem kell

            //templateObject.UgyiratComponents.IktatoszamKieg = UgyUgyirat_IktatoszamKieg_KodtarakDropDownList.SelectedValue;

            templateObject.UgyiratComponents.Csoport_Id_Ugyfelelos = Ugyfelelos_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.FelhasznaloCsoport_Id_Ugyintez = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;

            templateObject.UgyiratComponents.Csoport_Id_Felelos = UgyUgyiratok_CsoportId_Felelos.Id_HiddenField;

            // ügyindító/ügyfél
            templateObject.UgyiratComponents.Partner_Id_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Id_HiddenField;
            templateObject.UgyiratComponents.NevSTR_Ugyindito = Ugy_PartnerId_Ugyindito_PartnerTextBox.Text;
            templateObject.UgyiratComponents.Cim_Id_Ugyindito = CimekTextBoxUgyindito.Id_HiddenField;
            templateObject.UgyiratComponents.CimSTR_Ugyindito = CimekTextBoxUgyindito.Text;

            #endregion
        }

        #region Irat komponensek

        templateObject.IratComponents.Targy = IraIrat_Targy_RequiredTextBox.Text;

        //templateObject.IratComponents.Kategoria = IraIrat_Kategoria_KodtarakDropDownList.SelectedValue;

        templateObject.IratComponents.Irattipus = IraIrat_Irattipus_KodtarakDropDownList.SelectedValue;

        //templateObject.IraKezFeljegyz_KezelesTipus = IraKezFeljegyz_KezelesTipus_KodtarakDropDownList.SelectedValue;

        //templateObject.IraKezFeljegyz_Leiras = IraKezFeljegyz_Leiras_TextBox.Text;

        templateObject.IratComponents.KiadmanyozniKell = (IraIrat_KiadmanyozniKell_CheckBox.Checked) ? "1" : "0";

        #endregion

        #region Kuldemeny komponensek

        _Load = true;
        templateObject.KuldemenyComponents = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
        _Load = false;

        #endregion

        #region elozmeny search

        templateObject.ElozmenySearch = new IktatasFormTemplateObject.ElozmenySearchcomponent();
        templateObject.ElozmenySearch.Ev = evIktatokonyvSearch.EvTol;
        templateObject.ElozmenySearch.Iktatokonyv = IktatoKonyvekDropDownList_Search.SelectedValue;
        templateObject.ElozmenySearch.Foszam = numberFoszamSearch.Text;

        #endregion

        #region hataridos feladat

        templateObject.HataridosFeladat = FeljegyzesPanel.GetBusinessObject();

        #endregion

        #region #Email

        if (ErkeztetesVagyIktatasRadioButtonList.Visible)
        {
            templateObject.ErtkeztetesVagyIktatas = EmailKezelesSelector.GetSelectedValueString(ErkeztetesVagyIktatasRadioButtonList);//ErkeztetesVagyIktatasRadioButtonList.SelectedValue;
        }

        #endregion

        return templateObject;
    }

    #endregion

    #region Kuldemeny, Elozmeny gyors kereses

    protected void ImageButton_Elozmeny_Click(object sender, ImageClickEventArgs e)
    {
        UgyiratSzerelesiLista1.Reset();

        #region Keresesi mezok ellenorzese
        string errorHeader = "Elõzmény keresés hiba!";
        if (String.IsNullOrEmpty(evIktatokonyvSearch.EvTol))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs megadva év!");
            ErrorUpdatePanel1.Update();
            return;
        }

        if (IktatoKonyvekDropDownList_Search.DropDownList.SelectedIndex == -1 || String.IsNullOrEmpty(IktatoKonyvekDropDownList_Search.SelectedValue))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs kiválasztva iktatókönyv!");
            ErrorUpdatePanel1.Update();
            return;
        }

        if (String.IsNullOrEmpty(numberFoszamSearch.Text))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, "Nincs megadva fõszám!");
            ErrorUpdatePanel1.Update();
            return;
        }
        #endregion

        try
        {
            if (IsRegiAdatSearch())
            {
                SearchRegiElozmenyUgyirat();
            }
            else
            {
                SearchElozmenyUgyirat();
            }
        }
        catch (FormatException fe)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, errorHeader, fe.Message);
            ErrorUpdatePanel1.Update();
        }
    }

    private void SearchRegiElozmenyUgyirat()
    {
        Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch search = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();
        evIktatokonyvSearch.SetSearchObjectFields(search.UI_YEAR);
        search.UI_SAV.Value = IktatoKonyvekDropDownList_Search.GetSelectedSav();
        search.UI_SAV.Operator = Query.Operators.equals;
        search.UI_NUM.Value = numberFoszamSearch.Text;
        search.UI_NUM.Operator = Query.Operators.equals;
        Contentum.eMigration.Service.MIG_FoszamService service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = service.GetFoszamWithSzulokByAzonosito(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            DisplayElozmenySearchError(res, false);
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }

        Contentum.eMigration.BusinessDocuments.MIG_Foszam foszam = (Contentum.eMigration.BusinessDocuments.MIG_Foszam)res.Record;

        if (foszam != null && !String.IsNullOrEmpty(foszam.Id))
        {
            //Edok-os ügyiratba van szerelve
            string utoiratId = foszam.Edok_Utoirat_Id;
            if (!String.IsNullOrEmpty(utoiratId))
            {
                SetSzerelesiLista(res, true, true);
                SearchElozmenyUgyirat(utoiratId);
                return;
            }

            if (!String.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            {
                ClearElozmenyUgyirat();
            }

            DataRow foszamRow = res.Ds.Tables[0].Rows[res.Ds.Tables[0].Rows.Count - 1];
            String UgyHol = foszamRow["UGYHOL"].ToString();

            // selejtezett migrált ügyirat: nem lehet bele iktatni!
            if (UgyHol == Constants.MIG_IratHelye.Selejtezett)
            {
                MigraltUgyiratID_HiddenField.Value = "";
                ClearElozmenyUgyirat();

                // Az ügyiratba nem lehet iktatni! Az ügyirat selejtezett.
                ResultError.DisplayWarningOnErrorPanel(EErrorPanel1, Resources.Error.WarningLabel, Resources.Error.ErrorCode_52139);
                ErrorUpdatePanel1.Update();
                return;
            }

            MigraltUgyiratID_HiddenField.Value = foszam.Id;
            // BUG_13049
            //UgyUgyirat_Targy_RequiredTextBox.Text = foszam.Conc;
            UgyUgyirat_Targy_RequiredTextBox.Text = foszam.MEMO;

            // BUG_13050
            if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
            {
                if (String.IsNullOrEmpty(IraIrat_Targy_RequiredTextBox.Text))
                {
                    IraIrat_Targy_RequiredTextBox.Text = UgyUgyirat_Targy_RequiredTextBox.Text;
                }
            }
            regiAzonositoTextBox.Text = foszamRow["Foszam_Merge"].ToString();
            IrattariTetelTextBox.Text = foszamRow["IrattariTetelszam"].ToString();
            MegkulJelzesTextBox.Text = foszamRow["MegkulJelzes"].ToString();
            IRJ2000TextBox.Text = foszamRow["IRJ2000"].ToString();

            // a JavaScript valamiért nem mûködik, ha közvetlenül a CalendarControl
            // DateTextBoxba írunk (Visible = "true" Style="display: none;" mellett)
            // ezért másik textboxba írással kerüljük meg a problémát -> itt csak azért,
            // mert úgyis ezekbõl vesszük át az értéket a CalendarControlokba a
            // SetElozmenyRegiUgyirat()-ban
            txtIrattarba.Text = "";
            txtSkontroVege.Text = "";
            if (UgyHol == Constants.MIG_IratHelye.Irattarban)
            {
                txtIrattarba.Text = foszamRow["IRATTARBA"].ToString();
                // ne legyen üres érték, mert az irat helyét nem adjuk át
                if (String.IsNullOrEmpty(txtIrattarba.Text))
                {
                    txtIrattarba.Text = " ";
                }
            }
            else if (UgyHol == Constants.MIG_IratHelye.Skontro)
            {
                txtSkontroVege.Text = foszamRow["SCONTRO"].ToString();
                // ne legyen üres érték, mert az irat helyét nem adjuk át
                if (String.IsNullOrEmpty(txtSkontroVege.Text))
                {
                    txtSkontroVege.Text = " ";
                }
            }

            #region standard objektumfüggõ tárgyszavak
            string HelyrajziSzam = foszamRow["UI_HRSZ"].ToString();
            if (!String.IsNullOrEmpty(HelyrajziSzam))
            {
                otpStandardTargyszavak.TryFillFieldWithValue("Helyrajzi szám", HelyrajziSzam);
            }
            #endregion standard objektumfüggõ tárgyszavak

            SetElozmenyRegiUgyirat();
            SetSzerelesiLista(res, true);
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }
    }

    private void SearchElozmenyUgyirat(string ugyiratId)
    {
        bool searchUtoirat = !String.IsNullOrEmpty(ugyiratId);
        EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch(true);

        if (!searchUtoirat)
        {
            evIktatokonyvSearch.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);
            IktatoKonyvekDropDownList_Search.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
            search.Foszam.Value = numberFoszamSearch.Text;
            search.Foszam.Operator = Query.Operators.equals;
        }
        else
        {
            search.Id.Value = ugyiratId;
            search.Id.Operator = Query.Operators.equals;
        }

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
        Result res = service.GetUgyiratWithSzulokByAzonosito(xpm, search);

        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            if (searchUtoirat)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A keresett régi ügyirat szerelve van. A szülõ ügyirat lekérése sikertelen: " + ugyiratId);
            }
            else
            {
                DisplayElozmenySearchError(res, true);
                ClearElozmenyUgyirat();
            }
            ErrorUpdatePanel1.Update();
            return;
        }



        EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;

        if (ugyirat != null && !String.IsNullOrEmpty(ugyirat.Id))
        {
            if (!String.IsNullOrEmpty(MigraltUgyiratID_HiddenField.Value))
            {
                ClearElozmenyUgyirat();
            }
            ElozmenyUgyiratID_HiddenField.Value = ugyirat.Id;
            SetElozmenyUgyirat(ugyirat);
            SetSzerelesiLista(res, false, searchUtoirat);
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", "A rekord lekérése sikertelen!");
            ErrorUpdatePanel1.Update();
            ClearElozmenyUgyirat();
            return;
        }

    }

    private void DisplayElozmenySearchError(Result res, bool IsElozmenyHiba)
    {
        if (res.ErrorCode == "57000")
        {
            string onclick;
            if (IsElozmenyHiba)
                onclick = GetElozmenyOnClientClick();
            else
                onclick = GetMigraltOnClientClick();
            string hiba = "A keresett ügyirat nem található. <span onclick=\"" + onclick + "\" class=\"linkStyle\">Keresõ képernyõ megnyitása</span>";
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", hiba);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
        }
    }

    private void SearchElozmenyUgyirat()
    {
        SearchElozmenyUgyirat(String.Empty);
    }

    private void SetSzerelesiLista(Result result)
    {
        SetSzerelesiLista(result, false, false);
    }

    private void SetSzerelesiLista(Result result, bool RegiAdat)
    {
        SetSzerelesiLista(result, RegiAdat, false);
    }

    private void SetSzerelesiLista(Result result, bool RegiAdat, bool IsUtoirat)
    {
        string SelectedId = String.Empty;

        if (!RegiAdat)
            SelectedId = ElozmenyUgyiratID_HiddenField.Value;
        else
            SelectedId = MigraltUgyiratID_HiddenField.Value;

        DataTable ugyiratokTable = result.Ds.Tables[0];

        if (ugyiratokTable != null)
        {
            if (ugyiratokTable.Rows.Count > 1 || IsUtoirat)
            {
                UgyiratSzerelesiLista1.SelectedId = SelectedId;
                if (RegiAdat)
                {
                    UgyiratSzerelesiLista1.DataSourceRegiAdat = ugyiratokTable;
                    UgyiratSzerelesiLista1.DataBindRegiAdat();
                }
                else
                {
                    UgyiratSzerelesiLista1.DataSource = ugyiratokTable;
                    UgyiratSzerelesiLista1.DataBind();
                }
            }
        }
    }

    private int MigralasEve
    {
        get
        {
            int MigralasEve = Rendszerparameterek.GetInt(Page, Rendszerparameterek.MIGRALAS_EVE);
            if (MigralasEve == 0) MigralasEve = 2008;
            return MigralasEve;
        }
    }

    private bool IsRegiAdatSearch()
    {
        int Ev = evIktatokonyvSearch.EvTol_Integer;
        Logger.Debug("Migrálás Éve: " + MigralasEve);
        if (Ev < MigralasEve)
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    public string GetElozmenyKeresesOnClientClick()
    {
        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            return "return false;";
        }

        string jsQuery = "var query = ''; if(ev) query = '&" + QueryStringVars.ElozmenySearchParameters.Ev + "=' + ev.value;";
        jsQuery += "if(selectedValue.trim() != '') query += '&" + QueryStringVars.ElozmenySearchParameters.IktatokonyvValue + "=' + selectedValue;";
        jsQuery += "if(foszam && foszam.value != '') query += '&" + QueryStringVars.ElozmenySearchParameters.Foszam + "=' + foszam.value;";

        string js = "var ev = $get('" + evIktatokonyvSearch.EvTol_TextBox.ClientID + "');var iktatokonyv = $get('" + IktatoKonyvekDropDownList_Search.DropDownList.ClientID + "');var index = -1; if(iktatokonyv) index = iktatokonyv.selectedIndex;";
        js += "var selectedValue = ''; if(index > -1) selectedValue = iktatokonyv.options[index].value;var foszam = $get('" + numberFoszamSearch.TextBox.ClientID + "');if(ev && foszam && ev.value.trim() != '' && foszam.value.trim() != '' && selectedValue.trim() != '')";
        js += "{" + Page.ClientScript.GetPostBackEventReference(ImageButton_Elozmeny, "") + ";this.disabled = true;ev.disabled = true;foszam.disabled = true;iktatokonyv.disabled = true;iktatokonyv.options[iktatokonyv.selectedIndex].text = 'Keresés folyamatban...';return false;}";

        string OnClientClick = js + jsQuery + "if(ev && (ev.value== '' && this.id == '" + ImageButton_MigraltKereses.ClientID + "' || !isNaN(parseInt(ev.value)) && parseInt(ev.value) < " + MigralasEve + ")){";
        OnClientClick += GetMigraltOnClientClick("' + query + '");
        OnClientClick += "} else {";
        OnClientClick += GetElozmenyOnClientClick("' + query + '");
        OnClientClick += "}";

        return OnClientClick;
    }

    public string GetMigraltOnClientClick()
    {
        return GetMigraltOnClientClick(String.Empty);
    }

    public string GetMigraltOnClientClick(string dynamicQuery)
    {
        // BUG_13049
        string targymasolas = "&UgyiratTargy=" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID;
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.TARGY_MASOLAS_UGYROL) == "1")
        {
            targymasolas += "&IratTargy=" + IraIrat_Targy_RequiredTextBox.TextBox.ClientID;
        }
            string OnClientClick = JavaScripts.SetOnClientClick(eMigration.migralasLovListUrl,
            QueryStringVars.HiddenFieldId + "=" + MigraltUgyiratID_HiddenField.ClientID
            + "&" + QueryStringVars.AzonositoTextBox + "=" + regiAzonositoTextBox.ClientID
            + "&" + QueryStringVars.IrattariTetelTextBox + "=" + IrattariTetelTextBox.ClientID
            + "&" + QueryStringVars.MegkulJelzesTextBox + "=" + MegkulJelzesTextBox.ClientID
            + "&Irj=" + IRJ2000TextBox.ClientID
            // BUG_13049
            +  targymasolas
            + "&" + QueryStringVars.TextBoxId + "=" + UgyUgyirat_Targy_RequiredTextBox.TextBox.ClientID
            // valamiért nem mûködik, ha közvetlenül a DateTextBoxba írunk
            // (Visible = "true" Style="display: none;" mellett)
            // ezért másik textboxba írással kerüljük meg a problémát
            //+ "&" + QueryStringVars.IrattarbaTextBox + "=" + UgyUgyirat_IrattarbaHelyezes_CalendarControl.TextBox.ClientID
            //+ "&" + QueryStringVars.SkontroVegeTextBox + "=" + UgyUgyirat_SkontroVege_CalendarControl.TextBox.ClientID
            + "&" + QueryStringVars.IrattarbaTextBox + "=" + txtIrattarba.ClientID
            + "&" + QueryStringVars.SkontroVegeTextBox + "=" + txtSkontroVege.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=1"
            + "&" + QueryStringVars.Filter + "=" + Constants.FilterType.Ugyiratok.Szereles
            + dynamicQuery,
            Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, Ugyirat_Panel.ClientID, EventArgumentConst.refreshUgyiratokPanel);

        return OnClientClick;
    }

    public string GetElozmenyOnClientClick()
    {
        return GetElozmenyOnClientClick(String.Empty);
    }

    public string GetElozmenyOnClientClick(string dynamicQuery)
    {
        string OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokLovList.aspx"
            , QueryStringVars.HiddenFieldId + "=" + ElozmenyUgyiratID_HiddenField.ClientID
            + "&" + QueryStringVars.RefreshCallingWindow + "=1"
            + "&" + QueryStringVars.Filter + "=" + Constants.AlszamraIktathatoak + dynamicQuery
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, ImageButton_Elozmeny.ClientID, EventArgumentConst.refreshUgyiratokPanel);

        return OnClientClick;
    }

    #endregion


    protected void FoszamraIktat_ImageButton_Click(object sender, EventArgs e)
    {
        if (Command == CommandName.New)
        {
            // Ügyirat panel beállítása fõszámra iktatáshoz:
            ClearElozmenyUgyirat();

            DisplayElozmenySearchRow();

            ClearElozmenySearch();
        }
    }

    private void DisplayElozmenySearchRow()
    {
        trElozmenySearch.Visible = true;

        ImageButton_Elozmeny.Enabled = true;
        UI.SetImageButtonStyleToEnabled(ImageButton_Elozmeny);
    }

    /// <summary>
    /// Elõzmény keresõ sor elemeit kiüríti
    /// </summary>
    private void ClearElozmenySearch()
    {
        evIktatokonyvSearch.EvTol = String.Empty;
        evIktatokonyvSearch.EvIg = String.Empty;

        IktatoKonyvekDropDownList_Search.SelectedValue = String.Empty;

        numberFoszamSearch.Text = String.Empty;
    }

    public override void Load_ComponentSelectModul()
    {
        base.Load_ComponentSelectModul();

        if (pageView.CompSelector == null) { return; }
        else
        {
            pageView.CompSelector.Enabled = true;
            List<Control> componentList = new List<Control>();
            componentList.Add(ErkeztetesVagyIktatasRadioButtonList);
            ////Küldemény
            componentList.Add(KuldemenyPanel);
            componentList.AddRange(PageView.GetSelectableChildComponents(KuldemenyPanel.Controls));
            ////E-mail
            componentList.Add(EmailKuldemenyPanel);
            componentList.AddRange(PageView.GetSelectableChildComponents(EmailKuldemenyPanel.Controls));
            componentList.Add(KezelesiFeljegyzesPanelKuldemeny);
            componentList.Add(IrattariTetelTextBox);
            componentList.Add(MegkulJelzesTextBox);
            ////Ügyirat
            componentList.Add(Ugyirat_Panel);
            componentList.AddRange(PageView.GetSelectableChildComponents(Ugyirat_Panel.Controls));
            ////Irat
            componentList.Add(IratPanel);
            componentList.AddRange(PageView.GetSelectableChildComponents(IratPanel.Controls));
            pageView.CompSelector.Add_ComponentOnClick(componentList);
            TabFooter1.SaveEnabled = false;
        }
    }

    private string GetDefaultIktatokonyvId()
    {
        List<IktatoKonyvek.IktatokonyvItem> iktatokonyvek;
        IktatoKonyvek.IktatokonyvCache.GetIktatokonyvekList(this.Session, Constants.IktatoErkezteto.Iktato, true, true, out iktatokonyvek);

        //csak egy iktatókönyv van, akkor nem kell kiválasztani felületen
        if (iktatokonyvek.Count == 1)
        {
            return iktatokonyvek[0].Id;
        }

        return String.Empty;
    }

    private string GetSelectedIktatokonyvId()
    {
        if (!Iktatokonyvek_DropDownLis_Ajax.Visible)
            return GetDefaultIktatokonyvId();

        return Iktatokonyvek_DropDownLis_Ajax.SelectedValue;
    }


    #region tipusos tárgyszavak

    private void IraIrat_Irattipus_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTipusosObjektumTargyszavaiPanel_Iratok(null);
    }
    protected void FillTipusosObjektumTargyszavaiPanel_Iratok(String id)
    {
        TipusosTargyszavakPanel_Iratok.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_Iratok.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_IraIratok, "Irattipus", new string[] { IraIrat_Irattipus_KodtarakDropDownList.SelectedValue }, false
            , null, false, true, EErrorPanel1);

        TipusosTargyszavakPanel_Iratok.Visible = (otpTipusosTargyszavak_Iratok.Count > 0 || otpTipusosTargyszavak_IratokPostazasIranya.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_Iratok.Count > 0)
            {
                otpTipusosTargyszavak_Iratok.SetDefaultValues();
                TabFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpTipusosTargyszavak_Iratok.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_Iratok.ReadOnly = true;
            otpTipusosTargyszavak_Iratok.ViewMode = true;
        }
    }

    Result Save_TipusosObjektumTargyszavai_Iratok(string iratid)
    {
        List<EREC_ObjektumTargyszavai> EREC_ObjektumTargyszavaiList = new List<EREC_ObjektumTargyszavai>();

        EREC_ObjektumTargyszavai[] tipusosTargyszavak = otpTipusosTargyszavak_Iratok.GetEREC_ObjektumTargyszavaiList(true);
        EREC_ObjektumTargyszavai[] tipusosTargyszavaPostazasIranya = otpTipusosTargyszavak_IratokPostazasIranya.GetEREC_ObjektumTargyszavaiList(true);

        if (tipusosTargyszavak != null)
        {
            EREC_ObjektumTargyszavaiList.AddRange(tipusosTargyszavak);
        }
        if (tipusosTargyszavaPostazasIranya != null)
        {
            EREC_ObjektumTargyszavaiList.AddRange(tipusosTargyszavaPostazasIranya);
        }
        Result result_ot = new Result();

        if (EREC_ObjektumTargyszavaiList.Count > 0)
        {
            ExecParam execparam_ot = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_ObjektumTargyszavaiService service_ot = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
            result_ot = service_ot.InsertOrUpdateValuesByObjMetaDefinicio(execparam_ot
                        , EREC_ObjektumTargyszavaiList.ToArray()
                        , iratid
                        , null
                        , Constants.TableNames.EREC_IraIratok
                        , "*"
                        , null
                        , false
                        , null
                        , false
                        , false
                        );
        }

        return result_ot;
    }

    protected void FillTipusosObjektumTargyszavaiPanel_IratokPostazasIranya(String id, string postazasIranya)
    {
        TipusosTargyszavakPanel_Iratok.Visible = true;

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpTipusosTargyszavak_IratokPostazasIranya.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_IraIratok, "PostazasIranya", new string[] { postazasIranya }, false
            , null, false, true, EErrorPanel1);

        TipusosTargyszavakPanel_Iratok.Visible = (otpTipusosTargyszavak_Iratok.Count > 0 || otpTipusosTargyszavak_IratokPostazasIranya.Count > 0);

        if (String.IsNullOrEmpty(id))
        {
            if (otpTipusosTargyszavak_IratokPostazasIranya.Count > 0)
            {
                otpTipusosTargyszavak_IratokPostazasIranya.SetDefaultValues();
                TabFooter1.ImageButton_Save.OnClientClick = otpTipusosTargyszavak_IratokPostazasIranya.GetCheckErtekJavaScript() + TabFooter1.ImageButton_Save.OnClientClick;
                TabFooter1.ImageButton_SaveAndClose.OnClientClick = otpTipusosTargyszavak_IratokPostazasIranya.GetCheckErtekJavaScript() + TabFooter1.ImageButton_SaveAndClose.OnClientClick;
            }
        }

        if (Command == CommandName.View)
        {
            otpTipusosTargyszavak_IratokPostazasIranya.ReadOnly = true;
            otpTipusosTargyszavak_IratokPostazasIranya.ViewMode = true;
        }
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            otpTipusosTargyszavak_IratokPostazasIranya.Visible = false;
            otpTipusosTargyszavak_IratokPostazasIranya.Enabled = false;
        }
    }

    #endregion

    DateTime GetUgyintezesKezdete()
    {
        DateTime dtUgyintezesKezdete = DateTime.Now;

        //EREC_KuldKuldemenyek kuldemeny = GetBusinessObjectFromComponents_EREC_KuldKuldemenyek();
        // BLG_1348
        // A vizsgálat nem csak a papíralapúakra vonatkozik
        //if (Uj_Ugyirat_Hatarido_Kezeles && Kuldemenyek.IsPapirAlapu(kuldemeny))
        if (Uj_Ugyirat_Hatarido_Kezeles)
        {
            if (!String.IsNullOrEmpty(BeerkezesIdeje_CalendarControl.Text))
            {
                DateTime.TryParse(BeerkezesIdeje_CalendarControl.Text, out dtUgyintezesKezdete);
            }
        }

        return dtUgyintezesKezdete;
    }

    #region BLG_452_FUTARJEGYZEK

    private void KuldesModja_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetFutarJegyzekHaFutarSzolgalat();
        // BUG_10649
        if (Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.POSTAI_RAGSZAM_ERKEZTETESKOR, false))
        {
            RagszamRequiredTextBox.RequiredValidate = LabelReqRagszam.Visible = RagszamRequiredTextBox.CheckRagszamRequired(KuldesMod_DropDownList.SelectedValue);
        }
    }

    /// <summary>
    /// Beallitja a futarjegyzek parametereinek lathatosagat
    /// </summary>
    private void SetFutarJegyzekVisibility()
    {
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

        TextBoxFutarjegyzekListaSzama.Visible = isTUK;
        trFutarJegyzekParametersB.Visible = isTUK;
        trTerjedelemMainPanel.Visible = isTUK;
        TerjedelemPanelA.Visible = isTUK;
        ///BLG_452
        SetFutarJegyzekHaFutarSzolgalat();

        MinositoPartnerControlEgyszerusitettIktatas.Validate = false;
        MinositoPartnerControlEgyszerusitettIktatas.ValidationGroup = null;
    }
    /// <summary>
    /// ha futárszolgálat a küldés módja, beállítja  akontrolokat hozzá
    /// </summary>
    /// <param name="isTUK"></param>
    private void SetFutarJegyzekHaFutarSzolgalat()
    {
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            MinositoPartnerControlEgyszerusitettIktatas.Visible = true;
            CalendarControlMinositesErvenyessegIdeje.Visible = true;

            if (KuldesMod_DropDownList.SelectedValue == KodTarak.KULDEMENY_KULDES_MODJA.TUK_Futarszolgalat)
            {
                cbRagszamMegorzes.Visible = false;
                labelRagszam.Visible = false;
                RagszamRequiredTextBox.Visible = false;

                LabelFutarjegyzekListaSzama.Visible = true;
                TextBoxFutarjegyzekListaSzama.Visible = true;
            }
            else
            {
                cbRagszamMegorzes.Visible = true;
                labelRagszam.Visible = true;
                RagszamRequiredTextBox.Visible = true;

                LabelFutarjegyzekListaSzama.Visible = false;
                TextBoxFutarjegyzekListaSzama.Visible = false;
            }
        }
        else
        {
            LabelFutarjegyzekListaSzama.Visible = false;
            TextBoxFutarjegyzekListaSzama.Visible = false;
            MinositoPartnerControlEgyszerusitettIktatas.Visible = false;
            CalendarControlMinositesErvenyessegIdeje.Visible = false;

            cbRagszamMegorzes.Visible = true;
            labelRagszam.Visible = true;
            RagszamRequiredTextBox.Visible = true;
        }
    }

    /// <summary>
    /// Beallitja a futarjegyzek parametereinek modosithatosagat
    /// </summary>
    /// <param name="isReadOnly"></param>
    private void SetFutarJegyzekReadOnly(bool isReadOnly)
    {
        TextBoxFutarjegyzekListaSzama.ReadOnly = isReadOnly;
        CalendarControlMinositesErvenyessegIdeje.ReadOnly = isReadOnly;
        ///BLG_452
        MinositoPartnerControlEgyszerusitettIktatas.SetReadOnly(isReadOnly);
        TerjedelemPanelA.SetReadOnly(isReadOnly);
    }

    private void LoadFutarJegyzekParameters(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        if (Command == CommandName.View)
        {
            SetFutarJegyzekReadOnly(true);
            TextBoxFutarjegyzekListaSzama.Text = erec_KuldKuldemenyek.FutarJegyzekListaSzama;
            CalendarControlMinositesErvenyessegIdeje.Text = erec_KuldKuldemenyek.MinositesErvenyessegiIdeje;
            ///BLG_452
            MinositoPartnerControlEgyszerusitettIktatas.SetMinositoPartner(erec_KuldKuldemenyek.Minosito, null);

            TerjedelemPanelA.SetTerjedelem(erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMegjegyzes);
        }
        else if (Command == CommandName.Modify)
        {
            TextBoxFutarjegyzekListaSzama.Text = erec_KuldKuldemenyek.FutarJegyzekListaSzama;
            CalendarControlMinositesErvenyessegIdeje.Text = erec_KuldKuldemenyek.MinositesErvenyessegiIdeje;
            ///BLG_452
            MinositoPartnerControlEgyszerusitettIktatas.SetMinositoPartner(erec_KuldKuldemenyek.Minosito, null);

            TerjedelemPanelA.SetTerjedelem(erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMennyiseg, erec_KuldKuldemenyek.TerjedelemMegjegyzes);

            SetFutarJegyzekReadOnly(false);
        }
        else if (Command == CommandName.New)
        {
            SetFutarJegyzekReadOnly(false);
        }
    }
    /// <summary>
    /// Fill kuldemeny bo from futarjegyzek params
    /// </summary>
    /// <param name="erec_KuldKuldemenyek"></param>
    private void FillKuldemenyBOFromFutarJegyzekParameters(EREC_KuldKuldemenyek erec_KuldKuldemenyek)
    {
        #region BLG_452
        if (!string.IsNullOrEmpty(TextBoxFutarjegyzekListaSzama.Text))
        {
            erec_KuldKuldemenyek.FutarJegyzekListaSzama = TextBoxFutarjegyzekListaSzama.Text;
            erec_KuldKuldemenyek.Updated.FutarJegyzekListaSzama = true;
        }
        if (!string.IsNullOrEmpty(CalendarControlMinositesErvenyessegIdeje.SelectedDateText))
        {
            erec_KuldKuldemenyek.MinositesErvenyessegiIdeje = CalendarControlMinositesErvenyessegIdeje.SelectedDateText;
            erec_KuldKuldemenyek.Updated.MinositesErvenyessegiIdeje = true;
        }
        if (!string.IsNullOrEmpty(MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoSzuloPartnerId))
        {
            erec_KuldKuldemenyek.MinositoSzervezet = MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoSzuloPartnerId;
            erec_KuldKuldemenyek.Updated.MinositoSzervezet = true;
        }
        if (!string.IsNullOrEmpty(MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoPartnerId))
        {
            erec_KuldKuldemenyek.Minosito = MinositoPartnerControlEgyszerusitettIktatas.HiddenFieldMinositoPartnerId;
            erec_KuldKuldemenyek.Updated.Minosito = true;
        }
        string megj, menny, mennyEgys;
        TerjedelemPanelA.GetTerjedelem(out menny, out mennyEgys, out megj);

        if (!string.IsNullOrEmpty(megj))
        {
            erec_KuldKuldemenyek.TerjedelemMegjegyzes = megj;
            erec_KuldKuldemenyek.Updated.TerjedelemMegjegyzes = true;
        }
        if (!string.IsNullOrEmpty(menny))
        {
            erec_KuldKuldemenyek.TerjedelemMennyiseg = menny;
            erec_KuldKuldemenyek.Updated.TerjedelemMennyiseg = true;
        }
        if (!string.IsNullOrEmpty(mennyEgys))
        {
            erec_KuldKuldemenyek.TerjedelemMennyisegiEgyseg = mennyEgys;
            erec_KuldKuldemenyek.Updated.TerjedelemMennyisegiEgyseg = true;
        }
        #endregion BLG_452
    }
    private void Bekuldo_PartnerTextBox_TextChanged(object sender, EventArgs e)
    {
        SetFutarJegyzekHaFutarSzolgalat();
    }
    #endregion BLG_452_FUTARJEGYZEK

    #region EMAIL ADDRESS FROM EMAIL BODY
    private string SessionNameEmailAddressFromEmailBody = "SessionValue_EmailAddressFromEmailBody";
    /// <summary>
    /// Beállítja a modal ablakban kiválasztott értéket a beviteli mezõben.
    /// </summary>
    private void SetKuldoCimEmailAddressFromSession()
    {
        if (Session[SessionNameEmailAddressFromEmailBody] != null && !string.IsNullOrEmpty(Session[SessionNameEmailAddressFromEmailBody].ToString()))
        {
            var text = Session[SessionNameEmailAddressFromEmailBody].ToString();
            //Email_KuldCim_CimekTextBox.ID =;
            Email_KuldCim_CimekTextBox.Text = text;
            Session.Remove(SessionNameEmailAddressFromEmailBody);

        }
    }

    #endregion  

    #region bejövõ iratpéldányok

    bool PeldanyokSorszamaMegadhato
    {
        get
        {
            return Rendszerparameterek.IsTUK(Page);
        }
    }
    #endregion

    #region SAKKORA
    private const string FelfuggesztesOkaSpecItemContains = "EGYEB_OK";
    private const string FelfuggesztesOkaCustomtextValue = "Egyéb ok";

    private bool StartIrataHatasaProcedure(string iratid)
    {
        EREC_IraIratok obj_Irat = Contentum.eUtility.Sakkora.GetIrat(UI.SetExecParamDefault(Page, new ExecParam()), iratid);
        if (obj_Irat == null)
            return false;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        // BUG_3435
        bool kellElteltIdoUjraSzamolas;
        Contentum.eUtility.Sakkora.SakkoraKezeles(execParam, obj_Irat, null, out kellElteltIdoUjraSzamolas);
        //if (kellElteltIdoUjraSzamolas && !string.IsNullOrEmpty(obj_Irat.Ugyirat_Id))
        //{
        //    SakkoraService sakkSvc = eRecordService.ServiceFactory.Get_SakkoraService();
        //    sakkSvc.SakkoraUgyintezesiIdoUjraSzamolas(execParam.Clone(), obj_Irat.Ugyirat_Id);
        //}
        return true;
    }

    #region BO
    /// <summary>
    /// LoadIratHatasaComponentsFromBo
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadIratHatasaComponentsFromBo(EREC_IraIratok erec_IraIratok)
    {
        try
        {
            #region BLG_1051
            if (!string.IsNullOrEmpty(erec_IraIratok.IratHatasaUgyintezesre))
                IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue = erec_IraIratok.IratHatasaUgyintezesre;
            if (!string.IsNullOrEmpty(erec_IraIratok.FelfuggesztesOka))
            {
                string[] splitted = erec_IraIratok.FelfuggesztesOka.Split(new string[] { "|" }, StringSplitOptions.None);
                if (splitted != null && splitted.Length == 3)
                {
                    if (!string.IsNullOrEmpty(splitted[0]))
                        KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue = splitted[0];
                    if (!string.IsNullOrEmpty(splitted[2]))
                        TextBoxFelfuggesztesOka.Text = splitted[2];
                }
            }
            #endregion BLG_1051
        }
        catch (Exception exc)
        {
            Logger.Error("LoadIratHatasaComponentsFromBo error:", exc);
        }
    }

    /// <summary>
    /// LoadIrathatasaBoFromComponents
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void SetIratHatasaToBo(ref EREC_IraIratok erec_IraIratok)
    {
        #region BLG_1051
        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
        {
            erec_IraIratok.IratHatasaUgyintezesre = IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue;
            erec_IraIratok.Updated.IratHatasaUgyintezesre = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);

            erec_IraIratok.FelfuggesztesOka = string.Format("{0}|{1}|{2}", KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue, KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedItem.Text, TextBoxFelfuggesztesOka.Text);
            erec_IraIratok.Updated.FelfuggesztesOka = pageView.GetUpdatedByView(KodtarakDropDownListUgyiratFelfuggesztesOka);
        }
        #endregion BLG_1051
    }

    /// <summary>
    /// LoadLezarasOkaComponentsFromBo
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadLezarasOkaComponentsFromBo(EREC_IraIratok erec_IraIratok)
    {
        if (!string.IsNullOrEmpty(erec_IraIratok.LezarasOka))
            KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue = erec_IraIratok.LezarasOka;
    }
    /// <summary>
    /// LoadLezarasOkaBoFromComponents
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void SetLezarasOkaToBo(ref EREC_IraIratok erec_IraIratok)
    {
        if (Rendszerparameterek.GetUgyintezesiIdoFelfuggesztes(Page) == Contentum.eUtility.Constants.EnumUgyintezesiIdoFelfuggesztes.FANCY)
        {
            erec_IraIratok.LezarasOka = KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue;
            erec_IraIratok.Updated.LezarasOka = pageView.GetUpdatedByView(KodtarakDropDownListUgyiratLezarasOka);
        }
    }
    #endregion

    #region CTRL
    /// <summary>
    /// Beállítja az irat Hatása lenyíló listát
    /// </summary>
    private void InitializeIratHatasaDDL(string addIratHatasaElemMindenkepp)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        IratHatasaUgyintezesre_KodtarakDropDownList.Enabled = true;
        IratHatasaUgyintezesre_KodtarakDropDownList.ReadOnly = false;
        Contentum.eUtility.Sakkora.EnumIratHatasaUgyintezesre actualType = Contentum.eUtility.Sakkora.EnumIratHatasaUgyintezesre.NOTHING;

        List<string> toStates = new List<string>();
        if (string.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            toStates = Contentum.eUtility.Sakkora.SakkoraAllapotok(execParam, null, KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue, false, out actualType);
        else
        {
            EREC_UgyUgyiratok obj_Ugyirat = Contentum.eUtility.Sakkora.GetUgyirat(execParam, (ElozmenyUgyiratID_HiddenField.Value));
            if (obj_Ugyirat != null)
                toStates = Contentum.eUtility.Sakkora.SakkoraAllapotok(execParam, obj_Ugyirat, KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue, false, out actualType);
        }

        if (toStates != null && toStates.Count > 0)
            IratHatasaUgyintezesre_KodtarakDropDownList.FillDropDownList(kcs_IRAT_HATASA_UGYINTEZESRE, toStates, false, EErrorPanel1);

    }
    #endregion

    #region HELPER
    private void ShowErrorOnPanel(string message)
    {
        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, message);
        ErrorUpdatePanel1.Update();
    }

    private void ShowErrorOnPanel(Result result)
    {
        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        ErrorUpdatePanel1.Update();
    }

    #region FELFUGGESZTES OKA
    public void ShowFelfuggesztesOka()
    {
        if (IsIratHatasaPause())
            SetVisibilityFelfuggesztesOka(true);
        else
            SetVisibilityFelfuggesztesOka(false);
    }
    public void HideFelfuggesztesOka()
    {
        SetVisibilityFelfuggesztesOka(false);
    }
    /// <summary>
    /// Visszaadja a felfüggesztés okát.
    /// </summary>
    /// <returns></returns>
    private string GetFelfuggesztesOka()
    {
        if (KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue != null &&
                   KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue.EndsWith(FelfuggesztesOkaSpecItemContains))
            return string.IsNullOrEmpty(TextBoxFelfuggesztesOka.Text) ? FelfuggesztesOkaCustomtextValue : TextBoxFelfuggesztesOka.Text;
        else
            return KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedItem.Text;
    }
    /// <summary>
    /// Felfüggesztés oka kontrolok láthatósága
    /// </summary>
    /// <param name="visible"></param>
    private void SetVisibilityFelfuggesztesOka(bool visible)
    {
        LabelEljarasFefuggesztesenekOka.Visible = visible;
        KodtarakDropDownListUgyiratFelfuggesztesOka.Visible = visible;
        KodtarakDropDownListUgyiratFelfuggesztesOka.Enabled = visible;

        if (visible)
        {
            if (KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue != null &&
                    KodtarakDropDownListUgyiratFelfuggesztesOka.DropDownList.SelectedValue.EndsWith(FelfuggesztesOkaSpecItemContains))
            {
                TextBoxFelfuggesztesOka.Visible = true;
            }
            else
            {
                TextBoxFelfuggesztesOka.Visible = false;
            }
        }
        else
        {
            TextBoxFelfuggesztesOka.Visible = false;
        }
    }
    #endregion

    #region LEZARAS OKA
    public void ShowLezarasOka()
    {
        if (IsIratHatasaStop())
            SetVisibilityLezarasOka(true);
        else
            SetVisibilityLezarasOka(false);
    }
    public void HideLezarasOka()
    {
        SetVisibilityLezarasOka(false);
    }

    /// <summary>
    /// GetLezarasOka
    /// </summary>
    /// <returns></returns>
    private string GetLezarasOka()
    {
        if (KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue != null &&
                   KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue.EndsWith(FelfuggesztesOkaSpecItemContains))
            return FelfuggesztesOkaCustomtextValue;
        else
            return KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedItem.Text;
    }
    /// <summary>
    /// Lezárás oka kontrolok láthatósága
    /// </summary>
    /// <param name="visible"></param>
    private void SetVisibilityLezarasOka(bool visible)
    {
        LabelEljarasLezarasOka.Visible = visible;
        KodtarakDropDownListUgyiratLezarasOka.Visible = visible;
    }
    /// <summary>
    /// FillLezarasOkaDDL
    /// </summary>
    private void InitializeLezarasOkaDDL(string inIratHatasa, string lezarasOka)
    {
        string iratHatasValue;
        if (string.IsNullOrEmpty(inIratHatasa))
            iratHatasValue = IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue;
        else
            iratHatasValue = inIratHatasa;

        if (string.IsNullOrEmpty(iratHatasValue) || Command == CommandName.View)
            KodtarakDropDownListUgyiratLezarasOka.FillDropDownList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, true, EErrorPanel1);
        else
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            string[] lezarasOkai = Contentum.eUtility.Sakkora.SakkoraLezarasOkai(execParam, iratHatasValue);
            if (lezarasOkai == null || lezarasOkai.Length < 1)
                KodtarakDropDownListUgyiratLezarasOka.FillDropDownList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, true, EErrorPanel1);
            else
            {
                List<string> filterList = new List<string>();
                filterList.AddRange(lezarasOkai);
                KodtarakDropDownListUgyiratLezarasOka.FillDropDownList(KodTarak.UGYIRAT_LEZARAS_OKA.KCS, filterList, false, EErrorPanel1);
            }
        }

        if (!string.IsNullOrEmpty(lezarasOka))
            KodtarakDropDownListUgyiratLezarasOka.DropDownList.SelectedValue = lezarasOka;
    }
    #endregion

    private void SetVisibilityIratHatasa(bool visible)
    {
        LabelIratHatasaUgyintezesre.Visible = visible;
        IratHatasaUgyintezesre_KodtarakDropDownList.Visible = visible;
    }
    #endregion HELPER

    #region CHECKS

    #region IRAT HATAS
    /// <summary>
    /// Irat hatása felfüggesztés?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaPause()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaPause(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    /// <summary>
    /// Irat hatása indítás?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaStart()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaStart(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    /// <summary>
    /// Irat hatása lezárt?
    /// </summary>
    /// <returns></returns>
    private bool IsIratHatasaStop()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return Contentum.eUtility.Sakkora.IsIratHatasaStop(execParam, IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue);
    }
    #endregion IRAT HATAS

    #endregion CHECKS



    private void LoadUgyintezesiNapokFromBo(EREC_IraIratok erec_IraIratok)
    {
        inputUgyintezesiNapok.Value = erec_IraIratok.IntezesiIdo;
    }
    private void LoadUgyintezesiNapokBoFromComponents(ref EREC_IraIratok erec_IraIratok)
    {
        CheckUgyintezesiNapok();
        erec_IraIratok.IntezesiIdo = inputUgyintezesiNapok.Value;
        erec_IraIratok.Updated.IntezesiIdo = true;
    }
    private bool CheckUgyintezesiNapok()
    {
        if (string.IsNullOrEmpty(inputUgyintezesiNapok.Value))
            return true;

        int value;
        if (int.TryParse(inputUgyintezesiNapok.Value, out value))
        {
            if (value < 0)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageUgyintzesiIdoNegativ);
                ErrorUpdatePanel1.Update();
                throw new Exception(Resources.Error.MessageUgyintzesiIdoNegativ);
            }
        }
        return true;
    }
    /// <summary>
    /// InitUgyintezesiNapok
    /// </summary>
    /// <param name="execParam"></param>
    public void InitUgyintezesiNapok(ExecParam execParam)
    {
        string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
              Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYIRAT_INTEZESI_IDO, Page, null);

        var builder = new System.Text.StringBuilder();
        string kodtarKod;
        string kodtarNev;
        foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
        {
            kodtarKod = kte.Kod; // Key a kódtár kód
            kodtarNev = kte.Nev; // Value a kódtárérték neve
            builder.Append(String.Format("<option value='{0}'>", kodtarKod));
        }
        datalistUgyintezesiNapok.InnerHtml = builder.ToString();
        inputUgyintezesiNapok.Attributes["list"] = datalistUgyintezesiNapok.ClientID;
    }

    /// <summary>
    /// InitUgyintezesiNapok
    /// </summary>
    /// <param name="execParam"></param>
    public void InitUgyUgyintezesiNapok(ExecParam execParam)
    {
        string deletedValue = "[" + Resources.Form.UI_ToroltKodErtek + "]";

        List<Contentum.eUtility.KodTar_Cache.KodTarElem> kodtarakList =
              Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoportList(kcs_UGYIRAT_INTEZESI_IDO, Page, null);

        var builder = new System.Text.StringBuilder();
        string kodtarKod;
        string kodtarNev;
        foreach (Contentum.eUtility.KodTar_Cache.KodTarElem kte in kodtarakList)
        {
            kodtarKod = kte.Kod; // Key a kódtár kód
            kodtarNev = kte.Nev; // Value a kódtárérték neve
            builder.Append(String.Format("<option value='{0}'>", kodtarKod));
        }
        datalistUgyUgyintezesiNapok.InnerHtml = builder.ToString();
        inputUgyUgyintezesiNapok.Attributes["list"] = datalistUgyUgyintezesiNapok.ClientID;
    }

    /// <summary>
    /// LoadEljarasiSzakaszFokComponentsFromBo
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void LoadEljarasiSzakaszFokComponentsFromBo(EREC_IraIratok erec_IraIratok)
    {
        if (!string.IsNullOrEmpty(erec_IraIratok.EljarasiSzakasz))
            KodtarakDropDownList_EljarasiSzakaszFok.DropDownList.SelectedValue = erec_IraIratok.EljarasiSzakasz;
    }

    /// <summary>
    /// LoadEljarasiSzakaszFokBoFromComponents
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void SetEljarasiSzakaszFokToBo(ref EREC_IraIratok erec_IraIratok)
    {
        erec_IraIratok.EljarasiSzakasz = KodtarakDropDownList_EljarasiSzakaszFok.DropDownList.SelectedValue;
        erec_IraIratok.Updated.EljarasiSzakasz = pageView.GetUpdatedByView(KodtarakDropDownList_EljarasiSzakaszFok);

        //if (KodtarakDropDownList_EljarasiSzakaszFok.Enabled && string.IsNullOrEmpty(erec_IraIratok.EljarasiSzakasz))
        //{
        //    Contentum.eUtility.Sakkora.SetIratEljarasiSzakaszDefault(Contentum.eUtility.UI.SetExecParamDefault(Page), ref erec_IraIratok, UGY_FAJTAJA_KodtarakDropDownList.SelectedValue);
        //}
    }

    /// <summary>
    /// Check UgyFajta: hatósági -> Eljarasi szakasz
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyFajtaKodja"></param>
    /// <param name="eljarasiSzakaszFokKodja"></param>
    /// <returns></returns>
    private bool CheckUgyFajtaEsEljarasiSzakasz(ExecParam execParam, string ugyFajtaKodja, string eljarasiSzakaszFokKodja)
    {
        bool isHatosagi = CheckUgyFajtaIsHatosagi(execParam, ugyFajtaKodja);

        if (isHatosagi && string.IsNullOrEmpty(eljarasiSzakaszFokKodja))
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageEljarasiSzakaszFokRequired);
            ErrorUpdatePanel1.Update();
            return false;
            // throw new Exception(Resources.Error.MessageEljarasiSzakaszFokRequired);
        }
        return true;
    }
    /// <summary>
    /// CheckUgyFajtaSakkoraKotelezo
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyFajtaKodja"></param>
    /// <returns></returns>
    private bool CheckUgyFajtaSakkoraKotelezoseg(ExecParam execParam, string ugyFajtaKodja)
    {
        if (!Rendszerparameterek.UseSakkora(execParam))
        { return true; }

        bool isHatosagi = CheckUgyFajtaIsHatosagi(execParam, ugyFajtaKodja);
        if (isHatosagi)
        {
            if (string.IsNullOrEmpty(IratHatasaUgyintezesre_KodtarakDropDownList.DropDownList.SelectedValue))
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.MessageError, Resources.Error.MessageHatosagiUgySakkoraKotelezo);
                ErrorUpdatePanel1.Update();
                return false;
            }
        }
        return true;
    }
    /// <summary>
    /// CheckUgyFajtaIsHatosagi
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="ugyFajtaKodja"></param>
    /// <returns></returns>
    private bool CheckUgyFajtaIsHatosagi(ExecParam execParam, string ugyFajtaKodja)
    {
        return Contentum.eUtility.Sakkora.IsUgyFajtaHatosagi(execParam.Clone(), ugyFajtaKodja);
    }
    /// <summary>
    /// GetKuldemenyById
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private EREC_KuldKuldemenyek GetKuldemenyById(string id)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = new Result();
        execParam.Record_Id = id;
        result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
            return null;
        return (EREC_KuldKuldemenyek)result.Record;
    }
    /// <summary>
    /// SetBejovoIktatasUgyintezesKezdoDatuma
    /// </summary>
    /// <param name="erec_IraIrat"></param>
    /// <param name="kuldemeny"></param>
    private void SetBejovoIktatasUgyintezesKezdoDatuma(EREC_IraIratok erec_IraIrat, EREC_KuldKuldemenyek kuldemeny)
    {
        DateTime newUgyintezesKezdoDatuma;
        DateTime.TryParse(kuldemeny.BeerkezesIdeje, out newUgyintezesKezdoDatuma);
        if (newUgyintezesKezdoDatuma == null)
            newUgyintezesKezdoDatuma = DateTime.Now;
        newUgyintezesKezdoDatuma = newUgyintezesKezdoDatuma.AddHours(1);
        newUgyintezesKezdoDatuma = new DateTime(newUgyintezesKezdoDatuma.Year, newUgyintezesKezdoDatuma.Month, newUgyintezesKezdoDatuma.Day, newUgyintezesKezdoDatuma.Hour, 0, 0);

        erec_IraIrat.UgyintezesKezdoDatuma = newUgyintezesKezdoDatuma.ToString();
        erec_IraIrat.Updated.UgyintezesKezdoDatuma = true;
    }
    /// <summary>
    /// SetBelsoIktatasUgyintezesKezdoDatuma
    /// </summary>
    /// <param name="erec_IraIrat"></param>
    private void SetBelsoIktatasUgyintezesKezdoDatuma(ref EREC_IraIratok erec_IraIrat)
    {
        if (erec_IraIrat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso)
        {
            erec_IraIrat.UgyintezesKezdoDatuma = DateTime.Now.ToString();
            erec_IraIrat.Updated.UgyintezesKezdoDatuma = true;
        }
        else if (erec_IraIrat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Kimeno)
        {
            ///POSTAZAS LESZ MAJD
        }
    }
    #endregion

    protected void KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitializeIratHatasaDDL(null);
    }

    protected void AdathordozoTipusa_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetElektronikusIratVonalkodControl();
    }

    /// <summary>
    /// SetElektronikusIratVonalkodControl
    /// </summary>
    private void SetElektronikusIratVonalkodControl()
    {
        if (Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.VONALKOD_BEJOVO_ELEKTRONIKUS_GENERALT) == "1"
                && AdathordozoTipusa_DropDownList.SelectedValue == KodTarak.UGYINTEZES_ALAPJA.Elektronikus_nincs_papir)
        {
            labelVonalkodPirosCsillag.Visible = false;
            VonalkodTextBoxVonalkod.RequiredValidate = false;
        }
    }

    private void LoadKRXMetaAdatok()
    {
        if (!String.IsNullOrEmpty(eBeadvanyokId))
        {
            try
            {
                string metaDokId = GetKRXMetaDokId();
                if (!String.IsNullOrEmpty(metaDokId))
                {
                    byte[] metaContent = GetDokContent(metaDokId);
                    Contentum.eUtility.KRX.KULDEMENY_META.KULDEMENY kuldemeny = Contentum.eUtility.KRX.KRXFileManager.GetMetaAdatok(metaContent);

                    if (kuldemeny != null)
                    {
                        if (kuldemeny.FEJRESZ != null)
                        {
                            eBeadvanyokPanel_ErkeztetesiSzam.Text = kuldemeny.FEJRESZ.KULDEMENY_AZONOSITO;
                        }
                        if (kuldemeny.EXPEDIALASOK != null && kuldemeny.EXPEDIALASOK.EXPEDIALAS != null)
                        {
                            eBeadvanyokPanel_Nev.Text = kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_NEV;
                            eBeadvanyokPanel_FeladoCim.Text = kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_CIM;

                            Ugy_PartnerId_Ugyindito_PartnerTextBox.Text = kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_NEV;
                            CimekTextBoxUgyindito.Text = kuldemeny.EXPEDIALASOK.EXPEDIALAS.KULDO_CIM;

                            Contentum.eUtility.KRX.KuldesModjaConverter converter = new Contentum.eUtility.KRX.KuldesModjaConverter(Page);
                            string kod = converter.GetKod(kuldemeny.EXPEDIALASOK.EXPEDIALAS.KEZBESITES_MODJA);

                            if (!String.IsNullOrEmpty(kod))
                            {
                                eBeadvanyokPanel_BeerkezesMod.FillWithOneValue(kcs_KULDEMENY_KULDES_MODJA, kod, EErrorPanel1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", ex.Message);
            }
        }
    }

    private string GetKRXMetaDokId()
    {
        ExecParam xpm = UI.SetExecParamDefault(Page);
        EREC_eBeadvanyCsatolmanyokService eBeadvanyCsatService = eRecordService.ServiceFactory.GetEREC_eBeadvanyCsatolmanyokService();
        EREC_eBeadvanyCsatolmanyokSearch eBeadvanyCsatSearch = new EREC_eBeadvanyCsatolmanyokSearch();
        eBeadvanyCsatSearch.eBeadvany_Id.Value = eBeadvanyokId;
        eBeadvanyCsatSearch.eBeadvany_Id.Operator = Query.Operators.equals;

        Result eBeadvanyCsatResult = eBeadvanyCsatService.GetAll(xpm, eBeadvanyCsatSearch);

        if (eBeadvanyCsatResult.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, eBeadvanyCsatResult);
            return String.Empty;
        }

        bool krxFile = false;

        foreach (DataRow row in eBeadvanyCsatResult.Ds.Tables[0].Rows)
        {
            string nev = row["Nev"].ToString();
            string ext = System.IO.Path.GetExtension(nev);

            if (".krx".Equals(ext, StringComparison.CurrentCultureIgnoreCase))
            {
                krxFile = true;
                break;
            }
        }

        if (!krxFile)
        {
            return String.Empty;
        }

        string kuldemenyMetaDokId = String.Empty;

        foreach (DataRow row in eBeadvanyCsatResult.Ds.Tables[0].Rows)
        {
            string nev = row["Nev"].ToString();

            if ("KULDEMENY_META.xml".Equals(nev, StringComparison.CurrentCultureIgnoreCase))
            {
                kuldemenyMetaDokId = row["Dokumentum_Id"].ToString();
                break;
            }
        }

        return kuldemenyMetaDokId;
    }

    byte[] GetDokContent(string dokumentumId)
    {
        ExecParam xpm = UI.SetExecParamDefault(Page);
        KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
        xpm.Record_Id = dokumentumId;
        Result dokGetResult = dokService.Get(xpm);

        if (dokGetResult.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, dokGetResult);
            return null;
        }

        string externalLink = (dokGetResult.Record as KRT_Dokumentumok).External_Link;

        using (System.Net.WebClient wc = new System.Net.WebClient())
        {
            wc.Credentials = new System.Net.NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
            return wc.DownloadData(externalLink);
        }
    }

    #region BLG_2950
    private void AutomatikusSzignalas(EREC_IraIratokService service, string erec_IratId)
    {
        //LZS - Amennyiben a szignálás típusa automatikus (5) és 
        //a felelős ügyintéző nem lett megváltoztattva az ügytípus betöltése utáni javasolt felelős ügyintézőről
        //akkor lekérjük az ügytípushoz kapcsolódó határidős feladatot, és ID nélkül átadjuk az EREC_IraIratokService Szignalas() metódusának.
        //Szükséges paraméterek még az irat id-ja és a javasoltügyintéző id-ja is.
        if (SzignalasTipusa.Value == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore &&
            JavasoltUgyintezoId.Value.Equals(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField))
        {
            EREC_HataridosFeladatokService hataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = HataridosFeladatId.Value;

            EREC_HataridosFeladatok hataridosFeladat = null;

            if (!string.IsNullOrEmpty(HataridosFeladatId.Value))
            {
                Result resGet = hataridosFeladatokService.Get(execParam);

                if (resGet.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resGet);
                }
                else
                {
                    hataridosFeladat = (EREC_HataridosFeladatok)resGet.Record;
                    //kiszedjük, mert csak business object feltöltésre használjuk, ellenkező esetben access violation
                    hataridosFeladat.Id = "";
                }
            }

            #region Automatikis szignálás
            ExecParam execParamSzignalas = UI.SetExecParamDefault(Page, new ExecParam());

            Result resultSzignalas = service.Szignalas(execParamSzignalas, erec_IratId, JavasoltUgyintezoId.Value, hataridosFeladat);

            if (resultSzignalas.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultSzignalas);
            }

            #endregion
        }
    }

    //LZS
    //Az ügytípus kiválasztása során betöltjük az javasolt ügyintézőt a felületre és a határidős feladat id-ját.
    protected void UgyUgyirat_Ugytipus_DropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Hidden field - ekben tároljuk a felületen, hogy postback után is elérhetőek legyenek. Itt clear-eljük őket.
        #region Hidden fields
        JavasoltUgyintezoId.Value = "";
        HataridosFeladatId.Value = "";
        SzignalasTipusa.Value = "";
        #endregion

        //Lekérjük az EREC_IratMetaDefinicio táblából a kiválasztott ügytípushoz tartozó metaDef_Id-t.
        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        EREC_IratMetaDefinicioSearch metaDefSearch = new EREC_IratMetaDefinicioSearch();
        metaDefSearch.Ugytipus.Value = UgyUgyirat_Ugytipus_DropDownList.SelectedValue;
        metaDefSearch.Ugytipus.Operator = Query.Operators.equals;
        metaDefSearch.UgytipusNev.Value = UgyUgyirat_Ugytipus_DropDownList.SelectedItem.Text;
        metaDefSearch.UgytipusNev.Operator = Query.Operators.equals;
        metaDefSearch.Ugykor_Id.Value = Ugykor_DropDownList.SelectedValue;
        metaDefSearch.Ugykor_Id.Operator = Query.Operators.equals;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string metaDef_Id = "";

        Result result = service.GetAll(execParam, metaDefSearch);

        if (!result.IsError)
        {
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                metaDef_Id = row["Id"].ToString();
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }

        //A metaDef_Id segítségével lekérjük a megfelelő szignálási jegyzéket EREC_SzignalasiJegyzekek táblából, amely tartalmazza a 
        //szignálás típusát, a határidős feladat id-ját és a javasolt felelős ügyintéző id-ját.
        if (!string.IsNullOrEmpty(metaDef_Id))
        {
            System.Data.DataRow dataRow_EREC_SzignalasiJegyzekek = null;


            EREC_SzignalasiJegyzekekService SzJservice = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
            EREC_SzignalasiJegyzekekSearch SzJSearch = new EREC_SzignalasiJegyzekekSearch();

            ExecParam SzignalasiJegyzek = UI.SetExecParamDefault(Page, new ExecParam());

            SzJSearch.UgykorTargykor_Id.Value = metaDef_Id;
            SzJSearch.UgykorTargykor_Id.Operator = Contentum.eQuery.Query.Operators.equals;

            Result resSzJ = SzJservice.GetAll(SzignalasiJegyzek, SzJSearch);

            if (!resSzJ.IsError)
            {
                if (resSzJ.Ds.Tables[0].Rows.Count == 1)
                {
                    dataRow_EREC_SzignalasiJegyzekek = resSzJ.Ds.Tables[0].Rows[0];

                    if (dataRow_EREC_SzignalasiJegyzekek["Csoport_Id_Felelos"] != null)
                    {
                        JavasoltUgyintezoId.Value = dataRow_EREC_SzignalasiJegyzekek["Csoport_Id_Felelos"].ToString();
                    }
                    else
                    {
                        JavasoltUgyintezoId.Value = "";
                    }

                    if (dataRow_EREC_SzignalasiJegyzekek["HataridosFeladatId"] != null)
                    {
                        HataridosFeladatId.Value = dataRow_EREC_SzignalasiJegyzekek["HataridosFeladatId"].ToString();
                    }
                    else
                    {
                        HataridosFeladatId.Value = "";
                    }

                    if (dataRow_EREC_SzignalasiJegyzekek["SzignalasTipusa"] != null)
                    {
                        SzignalasTipusa.Value = dataRow_EREC_SzignalasiJegyzekek["SzignalasTipusa"].ToString();
                    }
                    else
                    {
                        SzignalasTipusa.Value = "";
                    }

                }
                else if (resSzJ.Ds.Tables[0].Rows.Count > 1)
                {
                    Logger.Warn("A szignálási jegyzék nem egyértelmû! Szignálási jegyzékek -bõl visszakapott sorok száma = " + resSzJ.Ds.Tables[0].Rows.Count);
                    ResultError.SetResultByErrorCode(resSzJ, 57001); // a keresés több találatot eredményezett! 
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resSzJ);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resSzJ);
            }

            //Itt történik a javasolt ügyintéző felületen történő beállítása.
            if (!string.IsNullOrEmpty(JavasoltUgyintezoId.Value) && SzignalasTipusa.Value == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore)
            {
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField = JavasoltUgyintezoId.Value;
                UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }
        }

        RegisterStartupScript("UgyUgyirat_Ugytipus_DropDownList_SelectedIndexChanged", "ugytipus_lastValue = '';UgyTipusChangeHandler();");


    }
    #endregion

    protected void Bekuldo_PartnerTextBox_TextChanged1(object sender, EventArgs e)
    {
    }

    #region EBEADVANY ELOZMENY
    /// <summary>
    /// LoadEBeadvanyElozmeny
    /// </summary>
    /// <param name="eBeadvany"></param>
    private void LoadEBeadvanyElozmeny(EREC_eBeadvanyok eBeadvany)
    {
        if (eBeadvany == null || !string.IsNullOrEmpty(ElozmenyUgyiratID_HiddenField.Value))
            return;

        string ugyiratId = null;
        string iratId = null;

        string elozmenyId = eBeadvany.Contentum_HivatkozasiSzam;
        if (string.IsNullOrEmpty(elozmenyId))
            return;

        ExecParam xparamIra = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraIratokService iraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        EREC_IraIratokSearch srcIra = new EREC_IraIratokSearch();
        srcIra.Id.Value = elozmenyId;
        srcIra.Id.Operator = Query.Operators.equals;
        srcIra.TopRow = 1;
        Result resultIra = iraIratokService.GetAll(xparamIra, srcIra);
        if (string.IsNullOrEmpty(resultIra.ErrorCode) && resultIra.Ds.Tables[0].Rows.Count > 0)
        {
            iratId = resultIra.Ds.Tables[0].Rows[0]["Id"].ToString();
            ugyiratId = resultIra.Ds.Tables[0].Rows[0]["Ugyirat_Id"].ToString();

            if (!string.IsNullOrEmpty(ugyiratId))
                SearchElozmenyUgyirat(ugyiratId);

            SeteBeadvanyElozmenydatok(resultIra.Ds.Tables[0].Rows[0]);
        }
    }
    /// <summary>
    /// SeteBeadvanyElozmenydatok
    /// </summary>
    /// <param name="iratRow"></param>
    private void SeteBeadvanyElozmenydatok(DataRow iratRow)
    {
        SetIratCsoportIdUgyFelelos_Visibility(false);

        string csoportIdFUgyFelelos = iratRow["Csoport_Id_Ugyfelelos"].ToString();
        if (string.IsNullOrEmpty(csoportIdFUgyFelelos))
        {
            string ugyiratId = iratRow["Ugyirat_Id"].ToString();
            ExecParam xparamUgy = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_UgyUgyiratok obj_Ugy = Contentum.eUtility.Sakkora.GetUgyirat(xparamUgy, ugyiratId);
            if (obj_Ugy == null)
                return;

            csoportIdFUgyFelelos = obj_Ugy.Csoport_Id_Ugyfelelos;
        }

        if (!string.IsNullOrEmpty(csoportIdFUgyFelelos))
        {
            SetIratCsoportIdUgyFelelos_Visibility(true);

            Irat_Ugyfelelos_CsoportTextBox.Id_HiddenField = csoportIdFUgyFelelos;
            Irat_Ugyfelelos_CsoportTextBox.SetCsoportTextBoxById(EErrorPanel1);
        }
    }
    /// <summary>
    /// SetIratCsoportIdUgyFelelos_Visibility
    /// </summary>
    /// <param name="visible"></param>
    private void SetIratCsoportIdUgyFelelos_Visibility(bool visible)
    {
        Label_Irat_Ugyfelelos_CsoportTextBox.Visible = visible;
        Irat_Ugyfelelos_CsoportTextBox.Visible = visible;
    }

    /// <summary>
    /// SeteBeadvanyElozmenyAdatok
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void FillIratFromEBeadvanyElozmenyAdatok(ref EREC_IraIratok erec_IraIratok)
    {
        if (isHivataliKapusErkeztetes && Irat_Ugyfelelos_CsoportTextBox.Visible && !string.IsNullOrEmpty(Irat_Ugyfelelos_CsoportTextBox.Id_HiddenField))
        {
            erec_IraIratok.Csoport_Id_Ugyfelelos = Irat_Ugyfelelos_CsoportTextBox.Id_HiddenField;
            erec_IraIratok.Updated.Csoport_Id_Ugyfelelos = true;
        }
    }
    #endregion

    /// <summary>
    /// KuldemenyBeerkezesIdoMeghatarozas
    /// </summary>
    /// <param name="execParam"></param>
    /// <param name="eBeadvany"></param>
    /// <param name="kuldemeny"></param>
    public string EBeadvanyKuldemenyBeerkezesIdoMeghatarozas(EREC_eBeadvanyok eBeadvany)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);
        Contentum.eRecord.Service.EREC_eBeadvanyokService svcEBead = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_eBeadvanyokService();
        Result result = svcEBead.IsEUzenetForras_Elhisz(execParam);

        if (!result.IsError)
        {
            bool isElhisz = (bool)result.Record;
            if (isElhisz)
            {
                string valueErkDatum = eBeadvany.KR_ErkeztetesiDatum;
                if (!string.IsNullOrEmpty(valueErkDatum))
                {
                    return valueErkDatum;
                }
            }
        }
        return eBeadvany.Base.LetrehozasIdo;
    }

    /// <summary>
    /// InitSakkoraDateControls
    /// </summary>
    private void InitSakkoraDateControls()
    {
        if (Rendszerparameterek.UseSakkora(UI.SetExecParamDefault(Page)))
        {
            CalendarControl_UgyintezesKezdete.Text = Contentum.eUtility.Sakkora.GetUgyUgyintezesKezdeteDefaultGlobal(UI.SetExecParamDefault(Page));
            //if (string.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.Text))
            //    UgyUgyirat_Hatarido_CalendarControl.GetHourTextBox.Text = "23";
            //if (string.IsNullOrEmpty(UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.Text))
            //    UgyUgyirat_Hatarido_CalendarControl.GetMinuteTextBox.Text = "59";

            //if (string.IsNullOrEmpty(IraIrat_Hatarido_CalendarControl.GetHourTextBox.Text))
            //    IraIrat_Hatarido_CalendarControl.GetHourTextBox.Text = "23";
            //if (string.IsNullOrEmpty(IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.Text))
            //    IraIrat_Hatarido_CalendarControl.GetMinuteTextBox.Text = "59";
        }
    }

    protected void UGY_FAJTAJA_KodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool isHatosagi = CheckUgyFajtaIsHatosagi(UI.SetExecParamDefault(Page), UGY_FAJTAJA_KodtarakDropDownList.SelectedValue);
        if (isHatosagi && String.IsNullOrEmpty(KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue))
        {
            KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue = KodTarak.ELJARASI_SZAKASZ_FOK.I;
            KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged(KodtarakDropDownList_EljarasiSzakaszFok, EventArgs.Empty);
        }
        else if (!isHatosagi && !String.IsNullOrEmpty(KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue))
        {
            KodtarakDropDownList_EljarasiSzakaszFok.SelectedValue = String.Empty;
            KodtarakDropDownList_EljarasiSzakaszFok_SelectedIndexChanged(KodtarakDropDownList_EljarasiSzakaszFok, EventArgs.Empty);
        }
    }
}
