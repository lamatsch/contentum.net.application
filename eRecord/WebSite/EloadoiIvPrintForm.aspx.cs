﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.Collections.Generic;

public partial class EloadoiIvPrintForm : Contentum.eUtility.UI.PageBase
{
    private int Ev = -1;

    private bool IsAfter2011
    {
        get
        {
            return (Ev == -1 || Ev > 2011);
        }
    }

    private string EloadoIvSablonNev
    {
        get
        {
            if (IsAfter2011)
                return "Eloadoi ivek_2012.xml";

            return "Eloadoi ivek.xml";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        IraIktatoKonyvekDropDownList1.FillAndSetEmptyValue(true, false, Constants.IktatoErkezteto.Iktato, EErrorPanel1);
        DatumCalendarControl1.Text = "";
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_UgyUgyiratokService erec_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch Search = new EREC_UgyUgyiratokSearch();

            string where = "";

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                where = " EREC_UgyUgyiratok.IraIktatokonyv_Id='" + IraIktatoKonyvekDropDownList1.SelectedValue + "' ";
                Ev = DateTime.Now.Year;
            }

            if (!string.IsNullOrEmpty(DatumCalendarControl1.Text))
            {
                string _date = DatumCalendarControl1.Text.Replace(" ","").Remove(10).Replace(".", "-");
                if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
                {
                    where = where + " and EREC_UgyUgyiratok.Letrehozasido BETWEEN '" + _date + " 00:00:00' and ' " + _date + " 23:59:59' ";
                }
                else
                {
                    where = " EREC_UgyUgyiratok.Letrehozasido BETWEEN '" + _date + " 00:00:00' and ' " + _date + " 23:59:59' ";
                }

                string letrehozasIdo = DatumCalendarControl1.Text;
                DateTime dtLetrehozasIdo;

                if (DateTime.TryParse(letrehozasIdo, out dtLetrehozasIdo))
                {
                    Ev = dtLetrehozasIdo.Year;
                }
                
            }

            Search.WhereByManual = where;
            Search.TopRow = 400;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result ugy_result = erec_UgyUgyiratokService.GetAllWithExtension(execParam, Search);

            /*if (string.IsNullOrEmpty(ugy_result.ErrorMessage))
            {
                Logger.Info("##### erec_UgyUgyiratokService.GetAllWithExtension rendben.");
            }
            else
            {
                Logger.Error("##### erec_UgyUgyiratokService.GetAllWithExtension hiba: " + ugy_result.ErrorMessage);
            }*/

            string ugyirat_ids = "";
            
            foreach (DataRow row in ugy_result.Ds.Tables[0].Rows)
            {
                ugyirat_ids = ugyirat_ids + "$" + row["Id"].ToString() + "$,";
            }

            //Logger.Info("##### ugyirat_ids:" + ugyirat_ids);

            if (ugyirat_ids.Length > 1)
            {
                ugyirat_ids = ugyirat_ids.Remove(ugyirat_ids.Length - 1);

                bool isBefore2012 = false;
                bool isAfter2012 = false;
                int Ev = -1;
                string js = string.Empty;

                var selectedItemsLetrehozasIdo = ugy_result.Ds.Tables[0].AsEnumerable().Select(x => x["LetrehozasIdo"].ToString());

                foreach (string letrehozasIdo in selectedItemsLetrehozasIdo)
                {
                    if (!String.IsNullOrEmpty(letrehozasIdo))
                    {
                        DateTime dt;
                        if (DateTime.TryParse(letrehozasIdo, out dt))
                        {
                            if (dt.Year > 2011)
                                isAfter2012 = true;
                            else
                                isBefore2012 = true;

                            if (dt.Year > Ev) Ev = dt.Year;
                        }
                    }
                }

                if (isAfter2012 && isBefore2012)
                {
                    js = "alert('A 2012 elõtti és utáni ügyiratokhoz nem nyomtatható egyszerre elõadóív!');";
                }
                else
                { 
                    Session["SelectedUgyiratok"] = ugyirat_ids;
                    string url = "UgyUgyiratokPrintFormSSRSEloadoiIvTomeges.aspx";
                    string qs = "";
                    if (Ev > -1)
                    {
                        qs += QueryStringVars.Ev + "=" + Ev.ToString();
                    }

                    // Bug 6295
                    if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.SSRS_AUTO_PDF_RENDER))
                    {
                        js = JavaScripts.SetSSRSPrintOnClientClick(url, qs);
                    }
                    else
                    {
                        js = JavaScripts.SetOnCLientClick_NoPostBack(url, qs, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                    }
                    js = js.TrimEnd("return false;".ToCharArray());
                }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "valami", js, true);
            }
            else
            {
                string js = "alert('Az adott napon nem iktattak főszámra az adott iktatókönyvbe!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "noitem", js, true);
            }
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
