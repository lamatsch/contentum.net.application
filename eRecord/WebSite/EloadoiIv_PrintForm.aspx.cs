﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Web.UI;

public partial class EloadoiIv_PrintForm : Contentum.eUtility.UI.PageBase
{
    private string ugyiratId = String.Empty;

    private int Ev = -1;

    private bool IsAfter2011
    {
        get
        {
            return (Ev == -1 || Ev > 2011);
        }
    }

    private string EloadoIvSablonNev
    {
        get
        {
            if (IsAfter2011)
                return "Eloadoi iv_2012.xml";

            return "Eloadoi iv.xml";
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ugyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (String.IsNullOrEmpty(ugyiratId))
        {
            // nincs Id megadva:
        }
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_PldIratPeldanyokSearch GetSearchObjectFromComponents_ip()
    {
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Value = ugyiratId;
        erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Id.Operator = Query.Operators.equals;

        return erec_PldIratPeldanyokSearch;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = GetSearchObjectFromComponents();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

        //ügy fajtájának feloldása
        //létrehozási idő megállapítása
        if (!result.IsError)
        {
            Dictionary<string, string> ugyFajtajaDict = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(KodTarak.UGY_FAJTAJA.KodcsoportKod, Page, null);
            foreach (DataRow _row in result.Ds.Tables[0].Rows)
            {
                string ugyFajtaja = _row["Ugy_Fajtaja"].ToString();
                if (!String.IsNullOrEmpty(ugyFajtaja))
                {
                    if (ugyFajtajaDict.ContainsKey(ugyFajtaja))
                        _row["UgyFajtaja"] = ugyFajtajaDict[ugyFajtaja];
                }

                DateTime? letrehozasIdo = _row["LetrehozasIdo"] as DateTime?;
                if (letrehozasIdo != null)
                {
                    this.Ev = letrehozasIdo.Value.Year;
                }
            }
        }

        Result eloirat_result = service.GetSzereltekFoszam(execParam, erec_UgyUgyiratokSearch);

        string eloirat = "";

        foreach (DataRow _row in eloirat_result.Ds.Tables[0].Rows)
        {
            eloirat = eloirat + _row["Foszam"].ToString() + ", ";
        }

        if (eloirat.Length > 2)
        {
            eloirat = eloirat.Remove(eloirat.Length - 2);
        }

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Date";
        column.AutoIncrement = false;
        column.Caption = "Date";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FelhNev";
        column.AutoIncrement = false;
        column.Caption = "FelhNev";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "EloIrat";
        column.AutoIncrement = false;
        column.Caption = "EloIrat";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Date"] = System.DateTime.Now.ToString();
        row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
        row["EloIrat"] = eloirat;
        table.Rows.Add(row);

        EREC_PldIratPeldanyokService ip_service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = GetSearchObjectFromComponents_ip();

        erec_PldIratPeldanyokSearch.WhereByManual = " and EREC_PldIratPeldanyok.Sorszam = '1' ";
        erec_PldIratPeldanyokSearch.OrderBy = "  EREC_IraIratok.Alszam";

        Result ip_result = ip_service.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

        table = new DataTable("ip_Table");
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "IktatasDatuma";
        column.AutoIncrement = false;
        column.Caption = "IktatasDatuma";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        //int x = 60;
        for (int i = 0; i < 1; i++)
        {
            row["IktatasDatuma"] = ip_result.Ds.Tables[0].Rows[i]["IktatasDatuma"].ToString();
        }
        table.Rows.Add(row);

        EREC_UgyiratObjKapcsolatokService kap_service = eRecordService.ServiceFactory.GetEREC_UgyiratObjKapcsolatokService();
        EREC_UgyiratObjKapcsolatokSearch erec_UgyiratObjKapcsolatokSearch = new EREC_UgyiratObjKapcsolatokSearch();

        erec_UgyiratObjKapcsolatokSearch.Obj_Id_Elozmeny.Filter(ugyiratId);
        erec_UgyiratObjKapcsolatokSearch.Obj_Type_Kapcsolt.Filter("EREC_UgyUgyiratok");
        erec_UgyiratObjKapcsolatokSearch.KapcsolatTipus.Filter("01");
        erec_UgyiratObjKapcsolatokSearch.WhereByManual = " AND getdate() BETWEEN EREC_UgyiratObjKapcsolatok.ErvKezd AND EREC_UgyiratObjKapcsolatok.ErvVege ";

        Result kap_result = kap_service.GetAllWithExtension(execParam, erec_UgyiratObjKapcsolatokSearch);

        DataTable tempTable = new DataTable();
        tempTable = kap_result.Ds.Tables[0].Copy();
        tempTable.TableName = "kap_Table";
        result.Ds.Tables.Add(tempTable);

        table = new DataTable("IartariTetelszamTable");
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Itsz1";
        column.AutoIncrement = false;
        column.Caption = "Itsz1";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Itsz2";
        column.AutoIncrement = false;
        column.Caption = "Itsz2";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Itsz3";
        column.AutoIncrement = false;
        column.Caption = "Itsz3";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Itsz4";
        column.AutoIncrement = false;
        column.Caption = "Itsz4";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "selejtkod";
        column.AutoIncrement = false;
        column.Caption = "selejtkod";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "megorzesi_ido";
        column.AutoIncrement = false;
        column.Caption = "megorzesi_ido";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "szervezeti_egyseg";
        column.AutoIncrement = false;
        column.Caption = "szervezeti_egyseg";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        string itsz = "";
        string szerv = "";
        string irattari_jel = "";
        string megorzesi_ido = "";
        string megorzesi_ido_regi = "";
        string letrehozas_ido = "";
        string agjel = "";

        foreach (DataRow _row in result.Ds.Tables[0].Rows)
        {
            itsz = _row["IrattariTetelszam"].ToString();
            szerv = _row["Iktatokonyv_Nev"].ToString();
            irattari_jel = _row["IrattariJel"].ToString();
            megorzesi_ido = _row["MegorzesiIdo1"].ToString();
            megorzesi_ido_regi = _row["MegorzesiIdo"].ToString();
            letrehozas_ido = _row["LetrehozasIdo_Rovid"].ToString();
            agjel = _row["AgazatiJel"].ToString();
        }

        if (szerv.IndexOf("iktatókönyve") >= 0)
        {
            szerv = szerv.Remove(szerv.IndexOf("iktatókönyve"));
        }

        row = table.NewRow();
        row["id"] = 0;

        if (Convert.ToInt32(letrehozas_ido.Substring(0, 4)) < 2010)
        {
            if (itsz.Length >= 3)
            {
                row["Itsz2"] = itsz.Substring(0, 1);
                row["Itsz3"] = itsz.Substring(1, 1);
                row["Itsz4"] = itsz.Substring(2, 1);
            }
            if (agjel.Length >= 1)
            {
                row["selejtkod"] = agjel.Substring(0, 1);
            }
            row["megorzesi_ido"] = megorzesi_ido_regi;
        }
        else
        {
            if (itsz.Length >= 3)
            {
                row["Itsz1"] = "";
                row["Itsz2"] = itsz.Substring(0, 1);
                row["Itsz3"] = itsz.Substring(1, 1);
                row["Itsz4"] = itsz.Substring(2, 1);
            }
            if (itsz.Length >= 4)
            {
                row["Itsz1"] = itsz.Substring(0, 1);
                row["Itsz2"] = itsz.Substring(1, 1);
                row["Itsz3"] = itsz.Substring(2, 1);
                row["Itsz4"] = itsz.Substring(3, 1);
            }
            if (irattari_jel.Equals("L") || irattari_jel.Equals("N"))
            {
                row["selejtkod"] = "NS";

                if (irattari_jel.Equals("N"))
                {
                    row["megorzesi_ido"] = "-";
                }
                else
                {
                    row["megorzesi_ido"] = megorzesi_ido;
                }
            }
            else
            {
                row["megorzesi_ido"] = "";

                if (irattari_jel.Equals("N"))
                {
                    row["selejtkod"] = "-";
                }
                else
                {
                    row["selejtkod"] = megorzesi_ido;
                }
            }
        }
        row["szervezeti_egyseg"] = szerv;
        table.Rows.Add(row);

        //string xsd = result.Ds.GetXmlSchema();

        string templateText = "";
        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
        string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
        WebRequest wr = WebRequest.Create(SP_TM_site_url + EloadoIvSablonNev);
        wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
        StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
        templateText = template.ReadToEnd();
        template.Close();
 
        bool pdf = false;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            pdf = true;
        }

        result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

        string filename = "";

        if (pdf)
        {
            filename = "Eloadoi_iv_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
        }
        else
        {
            filename = "Eloadoi_iv_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
        }

        int priority = 1;
        bool prior = false;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (string.IsNullOrEmpty(csop_result.ErrorCode))
        {
            foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
            {
                if (_row["Tipus"].ToString().Equals("3"))
                {
                    prior = true;
                }
            }
        }

        if (prior)
        {
            priority++;
        }

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

        byte[] res = (byte[])result.Record;

        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            if (pdf)
            {
                Response.ContentType = "application/pdf";
            }
            else
            {
                Response.ContentType = "application/msword";
            }
            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
        }
        else
        {
            if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
            {
                string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
            }
        }
    }
}
