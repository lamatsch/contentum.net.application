﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="EloadoiIvekPrintFormSSRS.aspx.cs" Inherits="EloadoiIvekPrintFormSSRS" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table cellpadding="0" cellspacing="0">
    <tr class="urlapSor_kicsi">
            <td class="mrUrlapCaption_short">
                <asp:Label Text="Sorok száma:" ID="labelTopRow" runat="server" />
            </td>
            <td class="mrUrlapCaption_short">
                <asp:TextBox runat="server" ID="tbTopRow" Text="10"/>
            </td>
        </tr>
    <tr class="urlapSor_kicsi">
            <td class="mrUrlapCaption_short" colspan="2">
                <asp:Button Text="PDF" runat="server" ID="btnPDF" OnClick="btnPDF_Click"/>
            </td>
        </tr>
    </table>
    <hr />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr class="urlapSor_kicsi">
            <td class="mrUrlapCaption_short">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="1200px"
                    Height="500px" EnableTelemetry="false">
                    <ServerReport ReportPath="/Test/EloadoiIvek" />
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>

