﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using System.Configuration;

public partial class EloadoiIvekPrintFormSSRS : Contentum.eUtility.UI.ListSSRSPageBase
{
    protected override ReportViewer ReportViewer { get { return ReportViewer1; } }
    protected override String DefaultOrderBy { get { return " order by EREC_UgyUgyiratok.LetrehozasIdo DESC"; } }
    protected override Contentum.eUIControls.eErrorPanel ErrorPanel { get { return EErrorPanel1; } }

    protected override Result GetData()
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        execParam.Fake = true;

        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)this.SearchObject;

        Result result = service.GetAllWithExtensionAndJogosultak(execParam, search, true);

        return result;
    }

    protected override BaseSearchObject GetSearchObject()
    {
        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject(Page, new EREC_UgyUgyiratokSearch(true));

        search.TopRow = 10;

        //string topRowParam = Request.QueryString.Get("TopRow");
        string topRowParam = tbTopRow.Text;
        int i;

        if (!String.IsNullOrEmpty(topRowParam) && Int32.TryParse(topRowParam, out i))
        {
            search.TopRow = i;
        }

        return search;
    }

    protected override void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        //this.ReportViewer.Visible = false;

        if (!IsPostBack)
        { 
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;
        }
		
        this.ReportViewer.ReportRefresh += new System.ComponentModel.CancelEventHandler(ReportViewer_ReportRefresh);
    }

    void ReportViewer_ReportRefresh(object sender, System.ComponentModel.CancelEventArgs e)
    {
        SetReportViewer();
    }

    protected override Dictionary<string, int> CreateVisibilityColumnsInfoDictionary()
    {
        return null;
    }

    protected override void SetSpecificReportParameter(ReportParameter rp)
    {
        if (rp != null && !String.IsNullOrEmpty(rp.Name) && ResultOfDataQuery != null)
        {
            switch (rp.Name)
            {
                case "Where_KuldKuldemenyek":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_KuldKuldemenyek"));
                    break;
                case "Where_UgyUgyiratdarabok":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_UgyUgyiratdarabok"));
                    break;
                case "Where_IraIratok":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_IraIratok"));
                    break;
                case "Where_IraIktatokonyvek":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_IraIktatokonyvek"));
                    break;
                case "Where_Dosszie":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_Dosszie"));
                    break;
                case "ObjektumTargyszavai_ObjIdFilter":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                    break;
                case "Altalanos_FTS":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Altalanos_FTS"));
                    break;
                case "ForMunkanaplo":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@ForMunkanaplo"));
                    break;
                case "Ugyintezo_Id_ForMunkanaplo":
                    rp.Values.Add(null);
                    break;
            }
        }
    }

    //protected override Contentum.eUtility.ReportViewerCredentials GetReportViewCredentials()
    //{
    //    //return base.GetReportViewCredentials();

    //    Contentum.eUtility.ReportViewerCredentials rvc = new Contentum.eUtility.ReportViewerCredentials("contentumspuser", "123456", "AXIS");

    //    return rvc;
    //}

    protected override void SetReportViewer()
    {
        Contentum.eUtility.ReportViewerCredentials rvc = GetReportViewCredentials();

        if (ReportViewer != null)
        {
            ReportViewer.ServerReport.ReportServerCredentials = rvc;
            //ReportViewer.ShowRefreshButton = false;
            ReportViewer.ShowParameterPrompts = false;

            Microsoft.Reporting.WebForms.ReportParameterInfoCollection rpis = ReportViewer.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                Microsoft.Reporting.WebForms.ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer.ServerReport.SetParameters(ReportParameters);
            }

            //ReportViewer.ServerReport.Refresh();
        }
    }

    protected void btnPDF_Click(object sender, EventArgs e)
    {
        SetReportViewer();
        RenderPDF();
    }

    private void RenderPDF()
    {
        string mimeType, encoding, extension, deviceInfo;

        string[] streamids;

        Microsoft.Reporting.WebForms.Warning[] warnings;

        string format = "PDF"; //Desired format goes here (PDF, Excel, or Image)

        deviceInfo =

"<DeviceInfo>" +

"<SimplePageHeaders>True</SimplePageHeaders>" +

"</DeviceInfo>";

        byte[] bytes = this.ReportViewer.ServerReport.Render(format, deviceInfo, out mimeType, out encoding, out extension, out streamids, out warnings);

        string fileName = "EloadoiIvek.pdf";
        Response.Clear();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-disposition", "attachment;filename=" + fileName);
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.Close();


    }
}