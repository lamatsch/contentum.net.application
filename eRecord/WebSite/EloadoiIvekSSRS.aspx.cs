﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;

public partial class EloadoiIvekSSRS : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                string ugyiratId = string.Empty;
                Result result = null;

                if (!IsPostBack && Session["SelectedUgyiratok"] == null)
                {
                    //a nyitó oldalról érkező paramétereket kiolvasó script regisztrálása
                    string hiddenFieldID = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
                    if (!String.IsNullOrEmpty(hiddenFieldID))
                    {
                        Dictionary<string, string> ParentChildControl = new Dictionary<string, string>(1);
                        ParentChildControl.Add(hiddenFieldID, hfSelectedUgyiratok.ClientID);
                        JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl, "", EventArgumentConst.refreshValuesFromParent);

                    }
                }
                else
                {
                    if (Session["SelectedUgyiratok"] != null)
                    {
                        ugyiratId = Session["SelectedUgyiratok"].ToString();
                    }
                    else
                    {
                        string eventArgument = Request.Params.Get("__EVENTARGUMENT");
                        if (!String.IsNullOrEmpty(eventArgument))
                        {
                            if (eventArgument == EventArgumentConst.refreshValuesFromParent)
                            {
                                if (hfSelectedUgyiratok.Value.Trim() != String.Empty)
                                {
                                    ugyiratId = hfSelectedUgyiratok.Value;
                                }
                            }
                        }
                    }

                    ugyiratId = ugyiratId.Replace("$", "'");
                    EREC_UgyUgyiratokService erec_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    execParam.Fake = true;

                    erec_UgyUgyiratokSearch.Id.Value = ugyiratId;
                    erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

                    erec_UgyUgyiratokSearch.OrderBy = " EREC_UgyUgyiratok.Foszam ";

                    result = erec_UgyUgyiratokService.GetAllWithExtensionForEloadoiIvek(execParam, erec_UgyUgyiratokSearch);
                }
		
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
			    if (result != null)
				{
                          	  if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                          	  {
                          	      ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC");
                         	  }
                         	  else
                         	  {
                           	  	ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                        	  }
				}
                            break;
                        case "Where":
			    if (result != null)
                            	ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            if (result != null)
			    	ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId": 
                            if (result != null)
				ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
			    break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
