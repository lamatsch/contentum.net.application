<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ElosztoivekList.aspx.cs" Inherits="ElosztoivekList" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <contenttemplate>
<%--Hiba megjelenites--%>
<eUI:eErrorPanel id="EErrorPanel1" runat="server">
    </eUI:eErrorPanel>
</contenttemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

<%--/Hiba megjelenites--%>

<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
   
    
<%--Tablazat / Grid--%> 
 <table width="100%" cellpadding="0" cellspacing="0">   
    <tr>
    <td style="text-align: left; vertical-align: top; width: 0px;">
        <asp:ImageButton runat="server" ID="ElosztoivekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
    <td style="text-align: left; vertical-align: top; width: 100%;">                           
  
        <asp:UpdatePanel ID="ElosztoivekUpdatePanel" runat="server" OnLoad="ElosztoivekUpdatePanel_Load" >
            <ContentTemplate>            
            <ajaxToolkit:CollapsiblePanelExtender ID="ElosztoivekCPE" runat="server" TargetControlID="Panel1"
            CollapsedSize="20" Collapsed="False" ExpandControlID="ElosztoivekCPEButton" CollapseControlID="ElosztoivekCPEButton" ExpandDirection="Vertical"
            AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="ElosztoivekCPEButton"             
             ExpandedSize="0" ExpandedText="Eloszt��vek list�ja" CollapsedText="Eloszt��vek list�ja" 
              >            
            </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="Panel1" runat="server">
            <table style="width: 98%;" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">            
                 <asp:GridView ID="ElosztoivekGridView" runat="server" OnRowCommand="ElosztoivekGridView_RowCommand"
                                    CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" AllowPaging="True"
                                    PagerSettings-Visible="false" AllowSorting="True" OnPreRender="ElosztoivekGridView_PreRender"
                                    AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="ElosztoivekGridView_Sorting"
                                    OnRowDataBound="ElosztoivekGridView_RowDataBound">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>                                        
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                    runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:CommandField>
                                        <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="Nev" HeaderStyle-Width="200px" HeaderStyle-CssClass="GridViewBorderHeader"/>
                                        <asp:BoundField DataField="Fajta_Nev" HeaderText="Fajta" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                            SortExpression="Fajta_Nev" HeaderStyle-Width="150px" />

                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>               
        </td></tr></table>        
                 </asp:Panel>    
        </ContentTemplate>    
        </asp:UpdatePanel>        
    </td>                            
    </tr>

    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
    <tr>    
    <td style="text-align: left; vertical-align: top; width: 0px;">
    <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
    </td>
    <td style="text-align: left; vertical-align: top; width: 100%;">                           
    <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">                               

            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" TargetControlID="Panel8"
            CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton" CollapseControlID="DetailCPEButton" ExpandDirection="Vertical"
            AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton"
            >
            
            </ajaxToolkit:CollapsiblePanelExtender>
            
                <asp:Panel ID="Panel8" runat="server">

                
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" 
                        OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        
                        <ajaxToolkit:TabPanel ID="ElosztoivTagokTabPanel" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelIdeIktatok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="Header1" runat="server" Text="Eloszt��v tagok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>                                
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="ElosztoivTagokUpdatePanel" runat="server" OnLoad="ElosztoivTagokUpdatePanel_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="ElosztoivTagokPanel" runat="server" Visible="false" Width="100%">

                                            <uc1:SubListHeader ID="ElosztoivTagokSubListHeader" runat="server" />

                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                            <tr>
                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            
                                            <ajaxToolkit:CollapsiblePanelExtender ID="ElosztoivTagokCPE" runat="server" TargetControlID="Panel2"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0"
                                              >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                <asp:Panel ID="Panel2" runat="server">

                                        <asp:GridView ID="ElosztoivTagokGridView" runat="server"
                                             CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                             AutoGenerateColumns="false" OnSorting="ElosztoivTagokGridView_Sorting" OnPreRender="ElosztoivTagokGridView_PreRender" OnRowCommand="ElosztoivTagokGridView_RowCommand" DataKeyNames="Id" >        
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"  />
                                                <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>        
                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>  
                                                <asp:BoundField DataField="Partner_Nev" HeaderText="Partner neve" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Partner_Nev" HeaderStyle-Width="200px"> 
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CimSTR" HeaderText="Partner c�me" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="CimSTR" HeaderStyle-Width="200px"> 
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Kuldesmod_Nev" HeaderText="K�ld�sm�d" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="Kuldesmod_Nev" HeaderStyle-Width="200px"> 
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AlairoSzerep_Nev" HeaderText="Al��r�szerep" 
                                                                ItemStyle-CssClass="GridViewBoundFieldItemStyle"  
                                                                SortExpression="AlairoSzerep_Nev" HeaderStyle-Width="200px"> 
                                                </asp:BoundField>

                                                </Columns>                
                                            </asp:GridView> 
                                            </asp:Panel>

                                                                                       
                                            </td></tr></table>
                                            
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        
                        </ajaxToolkit:TabContainer>                         
                
                
            </asp:Panel>     
                
                </td>
            </tr>
        </table>
        
        
                </td>
            </tr>
        </table>
        <br />    

</asp:Content>

