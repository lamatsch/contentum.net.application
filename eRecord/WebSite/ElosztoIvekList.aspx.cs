using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class ElosztoivekList : Contentum.eUtility.UI.PageBase
{
    
    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ElosztoivekList");   
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ElosztoivTagokSubListHeader.RowCount_Changed += new EventHandler(ElosztoivTagokSubListHeader_RowCount_Changed);

        ElosztoivTagokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(ElosztoivTagokSubListHeader_ErvenyessegFilter_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.ElosztoivekListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_IraElosztoivekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("ElosztoivSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("ElosztoivekForm.aspx", QueryStringVars.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivekUpdatePanel.ClientID,EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ElosztoivekGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(ElosztoivekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(ElosztoivekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(ElosztoivekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(ElosztoivekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivekUpdatePanel.ClientID, "", true);


        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = ElosztoivekGridView;

        ElosztoivTagokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ElosztoivTagokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ElosztoivTagokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ElosztoivTagokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(ElosztoivTagokGridView.ClientID);
        ElosztoivTagokSubListHeader.ButtonsClick += new CommandEventHandler(ElosztoivTagokSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        ElosztoivTagokSubListHeader.AttachedGridView = ElosztoivTagokGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(ElosztoivekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(ElosztoivTagokGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraElosztoivekSearch());

        if (!IsPostBack) ElosztoivekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + CommandName.Lock);

        ElosztoivTagokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "ElosztoivTetelekList");

        ElosztoivTagokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ElosztoivTetel" + CommandName.New);
        ElosztoivTagokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ElosztoivTetel" + CommandName.View);
        ElosztoivTagokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ElosztoivTetel" + CommandName.Modify);
        ElosztoivTagokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ElosztoivTetel" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(ElosztoivekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }

    #endregion

    #region Master List

    protected void ElosztoivekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ElosztoivekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ElosztoivekGridView", ViewState);

        ElosztoivekGridViewBind(sortExpression, sortDirection);
    }

    protected void ElosztoivekGridViewBind(String SortExpression, SortDirection SortDirection)    
    {
        EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraElosztoivekSearch search = (EREC_IraElosztoivekSearch)Search.GetSearchObject(Page, new EREC_IraElosztoivekSearch());
        search.OrderBy = Search.GetOrderBy("ElosztoivekGridView", ViewState, SortExpression, SortDirection);

        Result res = service.GetAllWithExtension(execParam, search);

        UI.GridViewFill(ElosztoivekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
        //KRT_CsoportokService service = eAdminService.ServiceFactory.GetKRT_CsoportokService();

        //ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //KRT_CsoportokSearch search = (KRT_CsoportokSearch)Search.GetSearchObject(Page, new KRT_CsoportokSearch());
        //search.OrderBy = Search.GetOrderBy("ElosztoivekGridView", ViewState, SortExpression, SortDirection);

        //Result res = service.GetAllWithFK(ExecParam, search);

        //UI.GridViewFill(ElosztoivekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void ElosztoivekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbSystem", "System");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    
    protected void ElosztoivekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ElosztoivekGridView.PageIndex;

        ElosztoivekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = ElosztoivekGridView.PageCount;

        if (prev_PageIndex != ElosztoivekGridView.PageIndex )
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ElosztoivekCPE);
            ElosztoivekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, ElosztoivekCPE);
        }


        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(ElosztoivekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(ElosztoivekGridView);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        ElosztoivekGridViewBind();
    }
   

    protected void ElosztoivekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ElosztoivekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ElosztoivekGridView_SelectRowCommand(id);
        }
    }

    private void ElosztoivekGridView_SelectRowCommand(string csoportId)
    {
        string id = csoportId;
        if (!String.IsNullOrEmpty(id))
        {
            ActiveTabRefresh(TabContainer1, id);

            //RefreshOnClientClicksByMasterListSelectedRow(id);
                       
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("ElosztoivekForm.aspx"
            , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
             , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("ElosztoivekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivekUpdatePanel.ClientID);

            string tableName = "EREC_IraElosztoivek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, ElosztoivekUpdatePanel.ClientID);

            ElosztoivTagokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("ElosztoivTagokForm.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.ElosztoivId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivTagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void ElosztoivekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ElosztoivekGridViewBind();
                    //ElosztoivekGridView_SelectRowCommand(UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
                    break;
            }
        }        
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedElosztoivek();
            ElosztoivekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedElosztoivRecords();
                ElosztoivekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedElosztoivRecords();
                ElosztoivekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedElosztoivek();
                break;
        }
    }

    private void LockSelectedElosztoivRecords()
    {
        LockManager.LockSelectedGridViewRecords(ElosztoivekGridView, "EREC_IraElosztoivek"
            , "ElosztoivLock", "ElosztoivForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedElosztoivRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(ElosztoivekGridView, "EREC_IraElosztoivek"
            , "ElosztoivLock", "ElosztoivForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// T�rli a ElosztoivekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedElosztoivek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ElosztoivInvalidate"))
        {
            List<string[]> deletableItemsList = ui.GetGridViewSelectedRowsWithIndex(ElosztoivekGridView, EErrorPanel1, ErrorUpdatePanel);

            EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
            List<ExecParam> execParams = new List<ExecParam>();
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                ExecParam execParam;
                execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = deletableItemsList[i][0];
                execParams.Add(execParam);
            }
            Result result = service.MultiInvalidate(execParams.ToArray());
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a ElosztoivekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedElosztoivek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(ElosztoivekGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraElosztoivek");
        }
    }

    protected void ElosztoivekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ElosztoivekGridViewBind(e.SortExpression, UI.GetSortToGridView("ElosztoivekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }


    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (ElosztoivekGridView.SelectedIndex == -1)
        {
            return;
        }
        string csoportId = UI.GetGridViewSelectedRecordId(ElosztoivekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, csoportId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string csoportId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                ElosztoivTagokGridViewBind(csoportId);
                ElosztoivTagokPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(ElosztoivTagokGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        ElosztoivTagokSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(ElosztoivTagokTabPanel))
        {
            ElosztoivTagokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(ElosztoivTagokGridView));
        }
    }

    #endregion

    #region ElosztoivTagok Detail

    private void ElosztoivTagokSubListHeader_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedElosztoivTagok();
            ElosztoivTagokGridViewBind(UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
        }
    }

    private void ElosztoivTagokGridViewBind(string elosztoivId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("ElosztoivTagokGridView", ViewState, "Partner_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("ElosztoivTagokGridView", ViewState);

        ElosztoivTagokGridViewBind(elosztoivId, sortExpression, sortDirection);
    }

    private void ElosztoivTagokGridViewBind(string elosztoivId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(elosztoivId))
        {
            EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IraElosztoivek szuloElosztoIv = new EREC_IraElosztoivek();
            szuloElosztoIv.Id = elosztoivId;

            EREC_IraElosztoivTetelekSearch search = new EREC_IraElosztoivTetelekSearch();
            search.OrderBy = Search.GetOrderBy("ElosztoivTagokGridView", ViewState, SortExpression, SortDirection);
            
            search.ElosztoIv_Id.Filter(elosztoivId);

            ElosztoivTagokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);

            UI.GridViewFill(ElosztoivTagokGridView, res, ElosztoivTagokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(ElosztoivTagokGridView);
        }
        else
        {
            ui.GridViewClear(ElosztoivTagokGridView);
            Result res = new Result();
            res.Ds = new System.Data.DataSet();
            res.Ds.Tables.Add();
            UI.SetTabHeaderRowCountText(res, Header1);
        }
    }

    private void ElosztoivTagokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        ElosztoivTagokGridViewBind(UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
    }

    protected void ElosztoivTagokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(ElosztoivTagokTabPanel))
                    //{
                    //    ElosztoivTagokGridViewBind(UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
                    //}
                    ActiveTabRefreshDetailList(ElosztoivTagokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a ElosztoivTagokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedElosztoivTagok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ElosztoivTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(ElosztoivTagokGridView, EErrorPanel1, ErrorUpdatePanel);

            EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void ElosztoivTagokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(ElosztoivTagokGridView, selectedRowNumber, "check");
        }
    }

    private void ElosztoivTagokGridView_RefreshOnClientClicks(String id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ElosztoivTagokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("ElosztoivTagokForm.aspx"
               , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.ElosztoivId + "=" + UI.GetGridViewSelectedRecordId(ElosztoivekGridView) +"&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivTagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            ElosztoivTagokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("ElosztoivTagokForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.ElosztoivId + "=" + UI.GetGridViewSelectedRecordId(ElosztoivekGridView) +"&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, ElosztoivTagokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void ElosztoivTagokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = ElosztoivTagokGridView.PageIndex;

        ElosztoivTagokGridView.PageIndex = ElosztoivTagokSubListHeader.PageIndex;

        if (prev_PageIndex != ElosztoivTagokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, ElosztoivTagokCPE);
            ElosztoivTagokGridViewBind(UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(ElosztoivTagokSubListHeader.Scrollable, ElosztoivTagokCPE);
        }

        ElosztoivTagokSubListHeader.PageCount = ElosztoivTagokGridView.PageCount;
        ElosztoivTagokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(ElosztoivTagokGridView);
    }

    void ElosztoivTagokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(ElosztoivTagokSubListHeader.RowCount);
        ElosztoivTagokGridViewBind(UI.GetGridViewSelectedRecordId(ElosztoivekGridView));
    }

    protected void ElosztoivTagokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        ElosztoivTagokGridViewBind(UI.GetGridViewSelectedRecordId(ElosztoivekGridView)
            , e.SortExpression, UI.GetSortToGridView("ElosztoivTagokGridView", ViewState, e.SortExpression));
    }

    #endregion


}
