<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="ElosztoivLovList.aspx.cs" Inherits="ElosztoivLovList" Title="Untitled Page" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,ElosztoIvekLovListHeaderTitle%>" />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <table id="elosztoivekTable" style="width: 90%" cellspacing="0" cellpadding="0" border="0"
                                runat="server">
                                <tbody>
                                    <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Eloszt��v neve:"></asp:Label><br />
                                            <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%"></asp:TextBox>
                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server"
                                                AlternateText="Keres�s" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg">
                                            </asp:ImageButton>
                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" AlternateText="R�szletes keres�s"
                                                ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"></asp:ImageButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                                TargetControlID="Panel3">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel3" runat="server" Width="95%">
                                                <table style="width: 102%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" id="TD1" class="GridViewHeaderBackGroundStyle"
                                                                runat="server">
                                                                <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                                <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    PageSize="5000" DataKeyNames="Id" AutoGenerateColumns="False" AllowSorting="False"
                                                                    PagerSettings-Visible="false" AllowPaging="True" GridLines="Both" BorderWidth="1"
                                                                    CellSpacing="0" CellPadding="0"
                                                                    EmptyDataText="<%$Resources:Search,NoSearchResult%>" EmptyDataRowStyle-CssClass="GridViewRowStyle">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                    <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" Wrap="True" />
                                                                    <SelectedRowStyle CssClass="GridViewLovListSelectedRowStyle" />
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="NEV" HeaderText="Eloszt��v neve" SortExpression="NEV">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="200px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Fajta_Nev" HeaderText="Eloszt��v t�pusa" SortExpression="Fajta_Nev">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                            <HeaderStyle Width="200px" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <PagerSettings Visible="False" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <br />
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"
                                                ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png" onmouseover="swapByName(this.id,'reszletesadatok2.png')"
                                                onmouseout="swapByName(this.id,'reszletesadatok.png')"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc2:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
