using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eUIControls;
using System.Data;
using System.Text;

public partial class ElosztoivLovList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private bool disable_refreshLovList = false;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ElosztoivekList");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("ElosztoivekForm.aspx", "", GridViewSelectedIndex.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("ElosztoivSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooterElosztoiv_ButtonsClick);

        if (!IsPostBack)
        {
            FillGridViewSearchResult(TextBoxSearch.Text, false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        JavaScripts.ResetScroll(Page, CollapsiblePanelExtender2);
                        FillGridViewSearchResult("", true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, CollapsiblePanelExtender2, 12);
        if (!CollapsiblePanelExtender2.ScrollContents)
        {
            Panel3.Width = Unit.Percentage(96.6);
        }
        else
        {
            Panel3.Width = Unit.Percentage(95);
        }

        string js = @"var key= null; if(window.event) key = window.event.keyCode; else key = event.keyCode;this.focus();
                    if(key && key == Sys.UI.Key.enter){" + Page.ClientScript.GetPostBackEventReference(ButtonSearch, "") + ";}";

        TextBoxSearch.Attributes.Add("onkeypress", js);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedId, GridViewSelectedIndex);
            }

        }

        base.Render(writer);
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "CloseManual", "function Close(){window.close();}", true);
    }


    #region Eloszt��vek

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, CollapsiblePanelExtender2);
        FillGridViewSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillGridViewSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        GridViewSelectedIndex.Value = String.Empty;

        Contentum.eAdmin.Service.EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
        EREC_IraElosztoivekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_IraElosztoivekSearch)Search.GetSearchObject(Page, new EREC_IraElosztoivekSearch());
            SetDefaultElosztoivState();
        }
        else
        {
            search = new EREC_IraElosztoivekSearch();
            search.NEV.Value = SearchKey;
            search.NEV.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }

        search.OrderBy = "NEV";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(execParam, search);

        //GridViewElosztoivekFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    //r�szletes keres�s ut�n az lovlist keres�si mez�inek alapl�llapotba �ll�t�sa
    private void SetDefaultElosztoivState()
    {
        TextBoxSearch.Text = "";
    }

    protected void LovListFooterElosztoiv_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedId.Value) && !String.IsNullOrEmpty(GridViewSelectedIndex.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedId.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true);

                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, false);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedIndex.Value = String.Empty;
                GridViewSelectedId.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    #endregion
}
