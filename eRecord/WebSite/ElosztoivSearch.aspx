<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ElosztoivSearch.aspx.cs" Inherits="ElosztoivSearch" Title="Untitled Page" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc7" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>

<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,CsoportokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Megnevezes_Elosztoiv_Label" runat="server" Text="Eloszt��v neve:"></asp:Label>&nbsp;</td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Tipus_Csoport_Label" runat="server" Text="T�pus:"></asp:Label>&nbsp;</td>
                                    <td class="mrUrlapMezo">
                                        <uc7:KodtarakDropDownList ID="Tipus_KodtarakDropDownList" runat="server" __designer:errorcontrol="This control cannot be displayed because its TagPrefix is not registered in this Web Form.">
                                        </uc7:KodtarakDropDownList>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server">
                                        </uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2">
                                        <uc3:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server"></uc3:TalalatokSzama_SearchFormComponent>
                                        &nbsp; &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>


