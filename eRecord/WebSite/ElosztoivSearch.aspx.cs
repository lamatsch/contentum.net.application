using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class ElosztoivSearch : Contentum.eUtility.UI.PageBase
{

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = typeof(EREC_IraElosztoivekSearch);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.ElosztoivekSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IraElosztoivekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, typeof(EREC_IraElosztoivekSearch)))
            {
                searchObject = (EREC_IraElosztoivekSearch)Search.GetSearchObject(Page, new EREC_IraElosztoivekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraElosztoivekSearch erec_iraElosztoivekSearch = null;
        if (searchObject != null) erec_iraElosztoivekSearch = (EREC_IraElosztoivekSearch)searchObject;

        if (erec_iraElosztoivekSearch != null)
        {
            Nev_TextBox.Text = erec_iraElosztoivekSearch.NEV.Value;
            Tipus_KodtarakDropDownList.FillAndSetSelectedValue("ELOSZTOIV_FAJTA", erec_iraElosztoivekSearch.Fajta.Value, true, SearchHeader1.ErrorPanel);
            
            Ervenyesseg_SearchFormComponent1.SetDefault(
                erec_iraElosztoivekSearch.ErvKezd, erec_iraElosztoivekSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IraElosztoivekSearch SetSearchObjectFromComponents()
    {
        EREC_IraElosztoivekSearch erec_iraElosztoivekSearch = (EREC_IraElosztoivekSearch)SearchHeader1.TemplateObject;
        if (erec_iraElosztoivekSearch == null)
        {
            erec_iraElosztoivekSearch = new EREC_IraElosztoivekSearch();
        }
               
        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            erec_iraElosztoivekSearch.NEV.Value = Nev_TextBox.Text;
            erec_iraElosztoivekSearch.NEV.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Tipus_KodtarakDropDownList.SelectedValue))
        {
            erec_iraElosztoivekSearch.Fajta.Value = Tipus_KodtarakDropDownList.SelectedValue;
            erec_iraElosztoivekSearch.Fajta.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            erec_iraElosztoivekSearch.ErvKezd, erec_iraElosztoivekSearch.ErvVege);

        return erec_iraElosztoivekSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraElosztoivekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, typeof(EREC_IraElosztoivekSearch));
            }
            else
            {
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IraElosztoivekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_IraElosztoivekSearch();
    }

}

