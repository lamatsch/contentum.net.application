<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="ElosztoivTagokForm.aspx.cs" Inherits="ElosztoivTagokForm" Title="Untitled Page" %>


    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
    
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc9" %>

<%@ Register Src="Component/FullCimField.ascx" TagName="FullCimField" TagPrefix="uc10" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                    
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text="Partner:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:PartnerTextBox Id="PartnerTextBox" runat="server"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
						    <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label32" runat="server" Text="Partner c�me:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
						        <table cellspacing="0" cellpadding="0" width="100%">
						            <tr class="urlapSor" runat="server" id="tr_FullCimField">
						                <td class="mrUrlapMezo">
                                            <uc10:FullCimField ID="FullCimField1" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
						                <td class="mrUrlapMezo">
                                            <uc9:CimekTextBox ID="CimekTextBox1" runat="server" Validate="false" />
						                </td>
					                </tr>
                                </table>
						    </td>
                        </tr>
   
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label9" runat="server" Text="K�ld�s m�d:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="dropDownList_KuldesMod" runat="server"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label6" runat="server" Text="Al��r� szerep:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="dropDownList_AlairoSzerep" runat="server"/>
                            </td>
                        </tr>

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="�rv�nyess�g:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

