using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Utility;
using System;

public partial class ElosztoivTagokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = ""; 
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private string Id = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.ElosztoivTetelekFormHeaderTitle;

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        Id = Request.QueryString.Get(QueryStringVars.ElosztoivId);
        if (string.IsNullOrEmpty(Id))
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ElosztoivTetel" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraElosztoivTetelek EREC_IraElosztoivTetelek = (EREC_IraElosztoivTetelek)result.Record;
                    LoadComponentsFromBusinessObject(EREC_IraElosztoivTetelek);

                    //if (Command == CommandName.View)
                    //{
                    //    KodtarakDropDownList.FillWithOneValue("CSOPORTTAGSAG_TIPUS", EREC_IraElosztoivTetelek.Tipus, FormHeader1.ErrorPanel);
                    //    KodtarakDropDownList.Enabled = false;
                    //}
                    //else
                    //{
                    //    KodtarakDropDownList.FillDropDownList("CSOPORTTAGSAG_TIPUS", false, FormHeader1.ErrorPanel);
                    //    KodtarakDropDownList.Enabled = false;
                    //    KodtarakDropDownList.DropDownList.Items.FindByValue(EREC_IraElosztoivTetelek.Tipus).Selected = true;
                    //}


                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        PartnerTextBox.CimTextBox = CimekTextBox1.TextBox;
        PartnerTextBox.CimHiddenField = CimekTextBox1.HiddenField;

        FullCimField1.CimTextBox = CimekTextBox1.TextBox;
        FullCimField1.CimHiddenField = CimekTextBox1.HiddenField;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        PartnerTextBox.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(78);
        CimekTextBox1.TextBox.Width = System.Web.UI.WebControls.Unit.Percentage(78);

        if (!IsPostBack && Command == CommandName.New)
        {
            dropDownList_KuldesMod.FillAndSetEmptyValue("KULDEMENY_KULDES_MODJA", FormHeader1.ErrorPanel);
            dropDownList_AlairoSzerep.FillAndSetEmptyValue("ALAIRO_SZEREP", FormHeader1.ErrorPanel);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraElosztoivTetelek EREC_IraElosztoivTetelek)
    {
        PartnerTextBox.Id_HiddenField = EREC_IraElosztoivTetelek.Partner_Id;
        PartnerTextBox.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        PartnerTextBox.Enabled = false;

        CimekTextBox1.Id_HiddenField = EREC_IraElosztoivTetelek.Cim_Id;
        CimekTextBox1.Text = EREC_IraElosztoivTetelek.CimSTR;
        if (Command == CommandName.View && String.IsNullOrEmpty(EREC_IraElosztoivTetelek.Cim_Id))
        {
            CimekTextBox1.ImageButton_View.Enabled = false;
            CimekTextBox1.ImageButton_View.CssClass = "disabledLovListItem";
        }

        dropDownList_KuldesMod.FillAndSetSelectedValue("KULDEMENY_KULDES_MODJA", EREC_IraElosztoivTetelek.Kuldesmod, FormHeader1.ErrorPanel);

        dropDownList_AlairoSzerep.FillAndSetSelectedValue("ALAIRO_SZEREP", EREC_IraElosztoivTetelek.AlairoSzerep, FormHeader1.ErrorPanel);

        ErvenyessegCalendarControl1.ErvKezd = EREC_IraElosztoivTetelek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = EREC_IraElosztoivTetelek.ErvVege;

        FormHeader1.Record_Ver = EREC_IraElosztoivTetelek.Base.Ver;

        //FormPart_CreatedModified1.SetComponentValues(EREC_IraElosztoivTetelek.Base);

        if (Command == CommandName.View)
        {
            FullCimField1.Visible = false;
            CimekTextBox1.ReadOnly = true;
            dropDownList_KuldesMod.ReadOnly = true;
            dropDownList_AlairoSzerep.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
        }
    }

    // form --> business object
    private EREC_IraElosztoivTetelek GetBusinessObjectFromComponents()
    {
        EREC_IraElosztoivTetelek elosztoivTetelek = new EREC_IraElosztoivTetelek();
        elosztoivTetelek.Updated.SetValueAll(false);
        elosztoivTetelek.Base.Updated.SetValueAll(false);

        elosztoivTetelek.ElosztoIv_Id = Id;
        elosztoivTetelek.Updated.ElosztoIv_Id = true;

        elosztoivTetelek.Partner_Id = PartnerTextBox.Id_HiddenField;
        elosztoivTetelek.Updated.Partner_Id = pageView.GetUpdatedByView(PartnerTextBox);

        elosztoivTetelek.NevSTR = PartnerTextBox.Text;
        elosztoivTetelek.Updated.NevSTR = pageView.GetUpdatedByView(PartnerTextBox);

        elosztoivTetelek.Cim_Id = CimekTextBox1.Id_HiddenField;
        elosztoivTetelek.Updated.Cim_Id = pageView.GetUpdatedByView(CimekTextBox1);

        elosztoivTetelek.CimSTR = CimekTextBox1.Text;
        elosztoivTetelek.Updated.CimSTR = pageView.GetUpdatedByView(CimekTextBox1);

        if (!string.IsNullOrEmpty(dropDownList_KuldesMod.SelectedValue))
        {
            elosztoivTetelek.Kuldesmod = dropDownList_KuldesMod.SelectedValue;
            elosztoivTetelek.Updated.Kuldesmod = pageView.GetUpdatedByView(dropDownList_KuldesMod);
        }

        if (!string.IsNullOrEmpty(dropDownList_AlairoSzerep.SelectedValue))
        {
            elosztoivTetelek.AlairoSzerep = dropDownList_AlairoSzerep.SelectedValue;
            elosztoivTetelek.Updated.AlairoSzerep = pageView.GetUpdatedByView(dropDownList_AlairoSzerep);
        }

        elosztoivTetelek.Sorszam = "0";
        elosztoivTetelek.Updated.Sorszam = true;

        elosztoivTetelek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        elosztoivTetelek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        elosztoivTetelek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        elosztoivTetelek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        elosztoivTetelek.Base.Ver = FormHeader1.Record_Ver;
        elosztoivTetelek.Base.Updated.Ver = true;

        return elosztoivTetelek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(PartnerTextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "ElosztoivTetel" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
                            EREC_IraElosztoivTetelek EREC_IraElosztoivTetelek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, EREC_IraElosztoivTetelek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraElosztoivTetelekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivTetelekService();
                                EREC_IraElosztoivTetelek EREC_IraElosztoivTetelek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, EREC_IraElosztoivTetelek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

    }

}
