using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Utility;
using System;

public partial class ElosztoivekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcsKod_ELOSZTOIV_FAJTA = "ELOSZTOIV_FAJTA";

    private void FillDropDrownList()
    {
        Tipus_KodtarakDropDownList.FillDropDownList(kcsKod_ELOSZTOIV_FAJTA, FormHeader1.ErrorPanel);
    }

    private void FillAndSetSelectedValue(string selectedValue)
    {
        Tipus_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_ELOSZTOIV_FAJTA
            , selectedValue, FormHeader1.ErrorPanel);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
       
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                //System_tr.Visible = false;
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Elosztoiv" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraElosztoivek EREC_IraElosztoivek = (EREC_IraElosztoivek)result.Record;
                    LoadComponentsFromBusinessObject(EREC_IraElosztoivek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (!IsPostBack)
            {
                FillDropDrownList();
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.ElosztoivekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraElosztoivek EREC_IraElosztoivek)
    {
        Nev.Text = EREC_IraElosztoivek.NEV;

        FillAndSetSelectedValue(EREC_IraElosztoivek.Fajta);

        ErvenyessegCalendarControl1.ErvKezd = EREC_IraElosztoivek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = EREC_IraElosztoivek.ErvVege;

        FormHeader1.Record_Ver = EREC_IraElosztoivek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(EREC_IraElosztoivek.Base);

        if (Command == CommandName.View)
        {
            Tipus_KodtarakDropDownList.ReadOnly = true;
            Nev.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
        }
    }

    // form --> business object
    private EREC_IraElosztoivek GetBusinessObjectFromComponents()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraElosztoivek EREC_IraElosztoivek = new EREC_IraElosztoivek();
        EREC_IraElosztoivek.Updated.SetValueAll(false);
        EREC_IraElosztoivek.Base.Updated.SetValueAll(false);

        EREC_IraElosztoivek.NEV = Nev.Text;
        EREC_IraElosztoivek.Updated.NEV = pageView.GetUpdatedByView(Nev);

        EREC_IraElosztoivek.Fajta = Tipus_KodtarakDropDownList.SelectedValue;
        EREC_IraElosztoivek.Updated.Fajta = pageView.GetUpdatedByView(Tipus_KodtarakDropDownList);

        EREC_IraElosztoivek.Kod = "0";
        EREC_IraElosztoivek.Updated.Kod = true;

        EREC_IraElosztoivek.Csoport_Id_Tulaj = execParam.Felhasznalo_Id;
        EREC_IraElosztoivek.Updated.Csoport_Id_Tulaj = true;

        EREC_IraElosztoivek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        EREC_IraElosztoivek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        EREC_IraElosztoivek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        EREC_IraElosztoivek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        EREC_IraElosztoivek.Base.Ver = FormHeader1.Record_Ver;
        EREC_IraElosztoivek.Base.Updated.Ver = true;

        return EREC_IraElosztoivek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(Tipus_KodtarakDropDownList);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }
    
    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Elosztoiv" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
                            EREC_IraElosztoivek EREC_IraElosztoivek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, EREC_IraElosztoivek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraElosztoivekService service = Contentum.eUtility.eAdminService.ServiceFactory.GetEREC_IraElosztoivekService();
                                EREC_IraElosztoivek EREC_IraElosztoivek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, EREC_IraElosztoivek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
