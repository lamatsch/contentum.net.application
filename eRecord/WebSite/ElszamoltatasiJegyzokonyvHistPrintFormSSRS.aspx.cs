﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public class ReportViewerCredentials : IReportServerCredentials
{
    [DllImport("advapi32.dll", SetLastError = true)]
    public extern static bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public extern static bool CloseHandle(IntPtr handle);

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
        int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);

    public ReportViewerCredentials()
    {
    }

    public ReportViewerCredentials(string username)
    {
        this.Username = username;
    }


    public ReportViewerCredentials(string username, string password)
    {
        this.Username = username;
        this.Password = password;
    }


    public ReportViewerCredentials(string username, string password, string domain)
    {
        this.Username = username;
        this.Password = password;
        this.Domain = domain;
    }


    public string Username
    {
        get
        {
            return this.username;
        }
        set
        {
            string username = value;
            if (username.Contains("\\"))
            {
                this.domain = username.Substring(0, username.IndexOf("\\"));
                this.username = username.Substring(username.IndexOf("\\") + 1);
            }
            else
            {
                this.username = username;
            }
        }
    }
    private string username;



    public string Password
    {
        get
        {
            return this.password;
        }
        set
        {
            this.password = value;
        }
    }
    private string password;


    public string Domain
    {
        get
        {
            return this.domain;
        }
        set
        {
            this.domain = value;
        }
    }
    private string domain;




    #region IReportServerCredentials Members

    public bool GetBasicCredentials(out string basicUser, out string basicPassword, out string basicDomain)
    {
        basicUser = username;
        basicPassword = password;
        basicDomain = domain;
        return username != null && password != null && domain != null;
    }

    public bool GetFormsCredentials(out string formsUser, out string formsPassword, out string formsAuthority)
    {
        formsUser = username;
        formsPassword = password;
        formsAuthority = domain;
        return username != null && password != null && domain != null;

    }

    public bool GetFormsCredentials(out Cookie authCookie,
  out string user, out string password, out string authority)
    {
        authCookie = null;
        user = password = authority = null;
        return false;  // Not implemented
    }


    public WindowsIdentity ImpersonationUser
    {
        get
        {

            string[] args = new string[3] { this.Domain.ToString(), this.Username.ToString(), this.Password.ToString() };
            IntPtr tokenHandle = new IntPtr(0);
            IntPtr dupeTokenHandle = new IntPtr(0);

            const int LOGON32_PROVIDER_DEFAULT = 0;
            //This parameter causes LogonUser to create a primary token.
            const int LOGON32_LOGON_INTERACTIVE = 2;
            const int SecurityImpersonation = 2;

            tokenHandle = IntPtr.Zero;
            dupeTokenHandle = IntPtr.Zero;
            try
            {
                // Call LogonUser to obtain an handle to an access token.
                bool returnValue = LogonUser(args[1], args[0], args[2],
                    LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                    ref tokenHandle);

                if (false == returnValue)
                {
                    Console.WriteLine("LogonUser failed with error code : {0}",
                        Marshal.GetLastWin32Error());
                    return null;
                }

                // Check the identity.
                System.Diagnostics.Trace.WriteLine("Before impersonation: "
                    + WindowsIdentity.GetCurrent().Name);


                bool retVal = DuplicateToken(tokenHandle, SecurityImpersonation, ref dupeTokenHandle);
                if (false == retVal)
                {
                    CloseHandle(tokenHandle);
                    Console.WriteLine("Exception in token duplication.");
                    return null;
                }


                // The token that is passed to the following constructor must
                // be a primary token to impersonate.
                WindowsIdentity newId = new WindowsIdentity(dupeTokenHandle);
                WindowsImpersonationContext impersonatedUser = newId.Impersonate();


                // Free the tokens.
                if (tokenHandle != IntPtr.Zero)
                    CloseHandle(tokenHandle);
                if (dupeTokenHandle != IntPtr.Zero)
                    CloseHandle(dupeTokenHandle);

                // Check the identity.
                System.Diagnostics.Trace.WriteLine("After impersonation: "
                    + WindowsIdentity.GetCurrent().Name);

                return newId;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred. " + ex.Message);
            }

            return null;
        }
    }

    public ICredentials NetworkCredentials
    {
        get
        {
            return null;  // Not using NetworkCredentials to authenticate.
        }
    }


    #endregion
}

public partial class ElszamoltatasiJegyzokonyvHistPrintFormSSRS : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string app_server_name = System.Environment.MachineName;
        ReportViewerCredentials rvc = new ReportViewerCredentials("AXIS\\" + app_server_name + "$", "");
        ReportViewer1.ServerReport.ReportServerCredentials = rvc;
        ReportViewer1.ShowRefreshButton = false;
        ReportViewer1.ShowParameterPrompts = false;
        ReportViewer1.ShowReportBody = false;

        //UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Reset

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);

        DatumIntervallum_SearchCalendarControl1.AutoPostback = true;
        DatumIntervallum_SearchCalendarControl1.DatumChanged += new EventHandler(DatumIntervallum_SearchCalendarControl1_DatumChanged);

        if (!IsPostBack)
        {
            DatumIntervallum_SearchCalendarControl1.DatumKezd = System.DateTime.Now.Year.ToString() + ".01.01.";
            DatumIntervallum_SearchCalendarControl1.DatumVege = System.DateTime.Now.ToString();

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }
        }
    }

    void DatumIntervallum_SearchCalendarControl1_DatumChanged(object sender, EventArgs e)
    {
        IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, DatumIntervallum_SearchCalendarControl1.DatumKezd.Remove(4), DatumIntervallum_SearchCalendarControl1.DatumVege.Remove(4)
                , true, false, EErrorPanel1);

        ReportViewer1.ShowReportBody = false;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;

        IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, DatumIntervallum_SearchCalendarControl1.DatumKezd.Remove(4), DatumIntervallum_SearchCalendarControl1.DatumVege.Remove(4)
                , true, false, EErrorPanel1);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ImageButton_Refresh.Click += new ImageClickEventHandler(ImageButton_Refresh_Click);
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    protected void ImageButton_Refresh_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewer1.ServerReport.Refresh();

        ReportViewer1.ShowReportBody = true;
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        string where = "";
        string MIG_where = "";
        string pld_where = "";
        string iktatokonyv = "";
        string ugyintezo = "";

        string date = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10) + "-tól " + DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10) + "-ig";
        string _datekezd = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10).Replace(".", "-");
        string _datevege = DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10).Replace(".", "-");

        if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
        {
            //where = "and EREC_UgyUgyiratok.IraIktatokonyv_Id='" + IraIktatoKonyvekDropDownList1.SelectedValue + "' ";

            EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam erec_IraIktatoKonyvek_execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

            erec_IraIktatoKonyvekSearch.Iktatohely.Value = IraIktatoKonyvekDropDownList1.SelectedValue.Remove(IraIktatoKonyvekDropDownList1.SelectedValue.Length - 1);
            erec_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
            erec_IraIktatoKonyvekSearch.IktatoErkezteto.Value = "I";
            erec_IraIktatoKonyvekSearch.IktatoErkezteto.Operator = Query.Operators.equals;
            erec_IraIktatoKonyvekSearch.Ev.Value = DatumIntervallum_SearchCalendarControl1.DatumKezd.Remove(4);
            erec_IraIktatoKonyvekSearch.Ev.ValueTo = DatumIntervallum_SearchCalendarControl1.DatumVege.Remove(4);
            erec_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.between;

            Result erec_IraIktatoKonyvek_result = erec_IraIktatoKonyvekService.GetAllWithExtension(erec_IraIktatoKonyvek_execParam, erec_IraIktatoKonyvekSearch);

            if (string.IsNullOrEmpty(erec_IraIktatoKonyvek_result.ErrorCode))
            {
                string ids = "";

                foreach (DataRow _row in erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows)
                {
                    ids = ids + "'" + _row["Id"] + "',";
                }

                if (ids.Length > 2)
                {
                    ids = ids.Remove(ids.Length - 1);

                    where = "and EREC_UgyUgyiratokHistory.IraIktatokonyv_Id in (" + ids + ") ";

                    pld_where = " EREC_IraIktatokonyvek.Id in (" + ids + ") and ";
                }

                erec_IraIktatoKonyvek_execParam.Record_Id = erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows[0]["Id"].ToString();
                Result erec_IraIktatoKonyvek_mig_result = erec_IraIktatoKonyvekService.Get(erec_IraIktatoKonyvek_execParam);
                EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)erec_IraIktatoKonyvek_mig_result.Record;
                string sav = "";
                if (!string.IsNullOrEmpty(erec_IraIktatoKonyvek.Iktatohely))
                {
                    if (erec_IraIktatoKonyvek.Iktatohely.Length < 3)
                    {
                        sav = erec_IraIktatoKonyvek.Iktatohely;
                    }
                    else
                    {
                        if (erec_IraIktatoKonyvek.Iktatohely.Substring(3, 1).Equals("0"))
                        {
                            sav = erec_IraIktatoKonyvek.Iktatohely.Substring(4);
                        }
                        else
                        {
                            sav = erec_IraIktatoKonyvek.Iktatohely.Substring(3);
                        }
                    }
                }
                MIG_where = " and PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 ";
            }

            iktatokonyv = IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text;
        }

        if (!string.IsNullOrEmpty(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField))
        {
            where = where + " and EREC_UgyUgyiratokHistory.FelhasznaloCsoport_Id_Ugyintez='" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField + "' ";

            string nev = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text.Remove(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text.IndexOf("(") - 1);
            MIG_where = MIG_where + " and MIG_Eloado.NAME='" + nev + "' ";

            pld_where = pld_where + " EREC_PldIratPeldanyokHistory.FelhasznaloCsoport_Id_Orzo='" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField + "' and EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez<>'" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField + "' and ";

            ugyintezo = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text;
        }

        where = where + " and EREC_UgyUgyiratokHistory.LetrehozasIdo BETWEEN '" + _datekezd + " 00:00:00' and '" + _datevege + " 23:59:59' ";
        MIG_where = MIG_where + " and MIG_Foszam.UI_YEAR BETWEEN '" + _datekezd.Remove(4) + "' and '" + _datevege.Remove(4) + "' and MIG_Sav.EV BETWEEN '" + _datekezd.Remove(4) + "' and '" + _datevege.Remove(4) + "' ";

        where = " (EREC_UgyUgyiratokHistory.Allapot in ('06','07','13','55','56','09') or (EREC_UgyugyiratokHistory.Allapot='50' and EREC_UgyugyiratokHistory.TovabbitasAlattAllapot in ('06','07','13','55','56','09')))" + where;
        MIG_where = " MIG_Foszam.UGYHOL != '2' and MIG_Foszam.UGYHOL != 'S' and MIG_Foszam.Edok_Utoirat_Id IS NULL and MIG_Foszam.Csatolva_Id IS NULL " + MIG_where;

        pld_where = pld_where + " EREC_UgyUgyiratok.Allapot in ('06','07','13','55','56','50','09') and EREC_PldIratPeldanyokHistory.LetrehozasIdo BETWEEN '" + _datekezd + " 00:00:00' and '" + _datevege + " 23:59:59'";

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "Where_Ugyirat":
                            ReportParameters[i].Values.Add(where);
                            break;
                        case "Where_Foszam":
                            ReportParameters[i].Values.Add(MIG_where);
                            break;
                        case "Where_PldIratPeldany":
                            ReportParameters[i].Values.Add(pld_where);
                            break;
                        case "OrderBy_Ugyirat":
                            ReportParameters[i].Values.Add(" order by EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratokHistory.Foszam,EREC_IraIratokHistory.Alszam");
                            break;
                        case "OrderBy_Foszam":
                            ReportParameters[i].Values.Add(" order by MIG_Foszam.UI_SAV,MIG_Foszam.UI_YEAR,MIG_Foszam.UI_NUM,MIG_Alszam.ALNO");
                            break;
                        case "OrderBy_PldIratPeldany":
                            ReportParameters[i].Values.Add(" order by EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_IraIratok.Alszam,EREC_PldIratPeldanyokHistory.Sorszam");
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField);
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add("100000");
                            break;
                        case "Ugyintezo":
                            ReportParameters[i].Values.Add(ugyintezo);
                            break;
                        case "Iktatokonyv":
                            ReportParameters[i].Values.Add(iktatokonyv);
                            break;
                        case "Date":
                            ReportParameters[i].Values.Add(date);
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}

