<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="ElszamoltatasiJegyzokonyvPrintForm.aspx.cs" Inherits="ElszamoltatasiJegyzokonyvPrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc15" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,ElszamoltatasiJegyzokonyvFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor" style="TEXT-ALIGN: left">
                            <td>
                            </td>
                            <td>
                            </td>
                            <td class="mrUrlapCaption" >
                                <asp:RadioButton id="SzervezetiEgyseg" Text="Szervezeti egys�g" Checked="True" GroupName="szint" runat="server" AutoPostBack="True" OnCheckedChanged="SzervezetiEgyseg_CheckedChanged"/>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="urlapSor" style="TEXT-ALIGN: left">
                        <td>
                        </td>
                        <td class="mrUrlapCaption_shorter">
                        <asp:Label ID="Label4" runat="server" Text="Fajta:"></asp:Label></td>
                        <td class="mrUrlapCaption" >
                        <asp:RadioButton id="Felhasznalo" Text="Felhaszn�l�" GroupName="szint" runat="server" AutoPostBack="True" OnCheckedChanged="Felhasznalo_CheckedChanged"/>
                        </td>
                        <td class="mrUrlapCaption" >
                        <uc15:FelhasznaloCsoportTextBox ID="FelhCsoportId_CsoportTextBox" runat="server" Validate="false" Enabled="false"/>
                        </td>
                        </tr>
                        <tr class="urlapSor" style="TEXT-ALIGN: left">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td class="mrUrlapCaption" >
                        <asp:RadioButton id="FelhasznaloAdottSzerepkoreben" Text="Felhaszn�l� adott szerepk�r�ben" GroupName="szint" runat="server" AutoPostBack="True" OnCheckedChanged="FelhasznaloAdottSzerepkoreben_CheckedChanged"/>
                        </td>
                        <td class="mrUrlapCaption" >
                        <uc15:FelhasznaloCsoportTextBox ID="FelhAdottSzerpkorbenCsoportId_CsoportTextBox" runat="server" Validate="false" Enabled="false" TryFireChangeEvent="true"/>
                        <asp:UpdatePanel runat ="server" ID="upSzerepkorList">
                        <ContentTemplate>
                            <asp:DropDownList id="Szerepkorok_DropDownList" runat="server" CssClass="formLoaderComboBox" Width="155px" Visible="True" Enabled="false"></asp:DropDownList>
                        </ContentTemplate>
                        </asp:UpdatePanel> 
                        </td>
                        </tr>
                        <tr><td><br/></td></tr>
                        <tr class="urlapSor" style="TEXT-ALIGN: left">
                            <td>
                            </td>
                            <td class="mrUrlapCaption_shorter">
                                <asp:Label ID="Label1" runat="server" Text="F�jl t�pusa:" Visible="false"></asp:Label></td>
                            <td class="mrUrlapCaption">
                                <asp:RadioButton id="PDF" Text="PDF" Checked="True" GroupName="type" runat="server" Visible="false"/>
                                <br />
                                <asp:RadioButton id="Excel" Text="Excel" GroupName="type" runat="server" Visible="false"/>
                            </td>
                        </tr>
                        <tr><td><br/></td></tr>
                        <tr class="urlapSor" style="TEXT-ALIGN: left">
                            <td>
                            </td>
                            <td class="mrUrlapCaption_shorter">
                                <asp:Label ID="Label3" runat="server" Text="M�velet:"></asp:Label></td>
                            <td class="mrUrlapCaption">
                                <asp:RadioButton id="mentes" Text="Ment�s" Checked="True" GroupName="muvelet" runat="server"/>
                                <br />
                                <asp:RadioButton id="nyomtatas" Text="Nyomtat�s" GroupName="muvelet" runat="server"/>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
