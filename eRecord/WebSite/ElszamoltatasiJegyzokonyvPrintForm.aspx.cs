using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

using Contentum.eDocument.Service;
using System.Net;

public partial class ElszamoltatasiJegyzokonyvPrintForm : Contentum.eUtility.UI.PageBase
{

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    private EREC_UgyUgyiratokSearch GetSearchObjectFromComponents_Felhasznalo()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        if (Felhasznalo.Checked)
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = FelhCsoportId_CsoportTextBox.Id_HiddenField;
        }
        if (FelhasznaloAdottSzerepkoreben.Checked)
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Id_HiddenField;
        }
        erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

        return erec_UgyUgyiratokSearch;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents_Iratok()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch();

        erec_IraIratokSearch.FelhasznaloCsoport_Id_Orzo.Value = FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Id_HiddenField;
        erec_IraIratokSearch.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;

        return erec_IraIratokSearch;
    }

    private EREC_KuldKuldemenyekSearch GetSearchObjectFromComponents_Kuldemenyek()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Value = FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Id_HiddenField;
        erec_KuldKuldemenyekSearch.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;

        return erec_KuldKuldemenyekSearch;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager1.RegisterAsyncPostBackControl(FelhAdottSzerpkorbenCsoportId_CsoportTextBox.TextBox);
        FelhAdottSzerpkorbenCsoportId_CsoportTextBox.TextBox.AutoPostBack = true;
        if (Request.Params["__EVENTTARGET"] == FelhAdottSzerpkorbenCsoportId_CsoportTextBox.TextBox.UniqueID)
        {
            this.LoadFelhasznaloSzerepkorei();
        }

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void LoadFelhasznaloSzerepkorei()
    {
        if (!String.IsNullOrEmpty(FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Id_HiddenField))
        {
            Contentum.eAdmin.Service.KRT_SzerepkorokService service_sz = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_SzerepkorokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_Felhasznalok felhasznalo = new KRT_Felhasznalok();
            felhasznalo.Id = FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Id_HiddenField;
            KRT_Felhasznalo_SzerepkorSearch search = new KRT_Felhasznalo_SzerepkorSearch();

            Result result = service_sz.GetAllByFelhasznalo(execParam, felhasznalo, search);

            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                Szerepkorok_DropDownList.Items.Clear();

                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    String Szerepkor_Id = row["Szerepkor_Id"].ToString();
                    String Szerepkor_Nev = row["Szerepkor_Nev"].ToString();
                    Szerepkorok_DropDownList.Items.Add(new ListItem(Szerepkor_Nev, Szerepkor_Id));
                }

                Szerepkorok_DropDownList.Enabled = true;
            }
        }
        else
        {
            Szerepkorok_DropDownList.Items.Clear();
            Szerepkorok_DropDownList.Enabled = false;
        }
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            bool ugyirat = false;
            bool irat = false;
            bool kuldemeny = false;
            
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

            if (!SzervezetiEgyseg.Checked)
            {
                erec_UgyUgyiratokSearch = GetSearchObjectFromComponents_Felhasznalo();
            }
            
            if (FelhasznaloAdottSzerepkoreben.Checked)
            {
                Contentum.eAdmin.Service.KRT_FunkciokService service_f = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_FunkciokService();
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_Szerepkorok szerepkor = new KRT_Szerepkorok();
                szerepkor.Id = Szerepkorok_DropDownList.SelectedValue;
                KRT_Szerepkor_FunkcioSearch search = new KRT_Szerepkor_FunkcioSearch();
                search.OrderBy = "KRT_Funkciok.Nev";
                search.WhereByManual = " and KRT_Funkciok.Nev = 'K�ldem�ny list�z�s' or KRT_Funkciok.Nev = '�gyiratok list�z�sa' or KRT_Funkciok.Nev = 'Iratok list�z�sa'";

                Result res = service_f.GetAllBySzerepkor(ExecParam, szerepkor, search);

                foreach (DataRow row1 in res.Ds.Tables[0].Rows)
                {
                    string Funkcio_Nev = row1["Funkcio_Nev"].ToString();
                    if (Funkcio_Nev.Equals("K�ldem�ny list�z�s"))
                    {
                        kuldemeny = true;
                    }
                    if (Funkcio_Nev.Equals("�gyiratok list�z�sa"))
                    {
                        ugyirat = true;
                    }
                    if (Funkcio_Nev.Equals("Iratok list�z�sa"))
                    {
                        irat = true;
                    }
                }
            }

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = new Result();

            if (Felhasznalo.Checked)
            {
                result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);
            }

            if (SzervezetiEgyseg.Checked)
            {
                result = service.GetAllWithExtensionAndJogosultak(execParam, erec_UgyUgyiratokSearch, true);
            }

            if (FelhasznaloAdottSzerepkoreben.Checked)
            {
                if (ugyirat)
                {
                    result = service.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);
                }
                else
                {
                    result.Ds = new DataSet();
                }
                if (kuldemeny)
                {
                    EREC_KuldKuldemenyekService service_kuldemenyek = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                    EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = GetSearchObjectFromComponents_Kuldemenyek();

                    Result result_kuldemenyek = service_kuldemenyek.GetAllWithExtension(execParam, erec_KuldKuldemenyekSearch);

                    DataTable tempTable = new DataTable();
                    tempTable = result_kuldemenyek.Ds.Tables[0].Copy();
                    tempTable.TableName = "Table_kuldemenyek";
                    result.Ds.Tables.Add(tempTable);
                }
                if (irat)
                {
                    EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    EREC_IraIratokSearch erec_IraIratokSearch = GetSearchObjectFromComponents_Iratok();

                    Result result_iratok = service_iratok.GetAllWithExtension(execParam, erec_IraIratokSearch);

                    DataTable tempTable = new DataTable();
                    tempTable = result_iratok.Ds.Tables[0].Copy();
                    tempTable.TableName = "Table_iratok";
                    result.Ds.Tables.Add(tempTable);
                }
            }

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date";
            column.AutoIncrement = false;
            column.Caption = "Date";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Date"] = System.DateTime.Now.ToString();
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            table.Rows.Add(row);

            string xml = "";
            string templateText = "";

            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }
            
            if (Felhasznalo.Checked)
            {
                //xml = result.Ds.GetXml();
                //string xsd = result.Ds.GetXmlSchema();
                if (PDF.Checked)
                {
                    string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                    string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                    string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
                    string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
                    WebRequest wr = WebRequest.Create(SP_TM_site_url + "Elszamoltatasi jegyzokonyv - felhasznalo.xml");
                    wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                    StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                    templateText = template.ReadToEnd();
                    template.Close();
                    //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"elsz-ugy\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>kiss.gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>5</o:Revision><o:TotalTime>2</o:TotalTime><o:Created>2008-02-25T10:47:00Z</o:Created><o:LastSaved>2008-04-07T14:08:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>29</o:Words><o:Characters>201</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>229</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"766C6E3A\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"00FABF00\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"360\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1080\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"1800\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2520\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3240\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"3960\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"4680\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5400\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6120\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Norm�l\"/><w:rsid w:val=\"004C6AD9\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezd�s alapbet�t�pusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Norm�l t�bl�zat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"R�csos t�bl�zat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00444BDF\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"10242\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00444BDF\"/><wsp:rsid wsp:val=\"00000191\"/><wsp:rsid wsp:val=\"00027026\"/><wsp:rsid wsp:val=\"000A7B4B\"/><wsp:rsid wsp:val=\"001273E3\"/><wsp:rsid wsp:val=\"001B3B4D\"/><wsp:rsid wsp:val=\"002A0192\"/><wsp:rsid wsp:val=\"003C4B4E\"/><wsp:rsid wsp:val=\"00444BDF\"/><wsp:rsid wsp:val=\"00473A63\"/><wsp:rsid wsp:val=\"00483D12\"/><wsp:rsid wsp:val=\"004C6AD9\"/><wsp:rsid wsp:val=\"00504350\"/><wsp:rsid wsp:val=\"005211FE\"/><wsp:rsid wsp:val=\"00573778\"/><wsp:rsid wsp:val=\"00574496\"/><wsp:rsid wsp:val=\"005A1EB5\"/><wsp:rsid wsp:val=\"005D7A3D\"/><wsp:rsid wsp:val=\"007B7495\"/><wsp:rsid wsp:val=\"007F4439\"/><wsp:rsid wsp:val=\"008120BE\"/><wsp:rsid wsp:val=\"00892303\"/><wsp:rsid wsp:val=\"00946B74\"/><wsp:rsid wsp:val=\"00AE66DF\"/><wsp:rsid wsp:val=\"00BB26A8\"/><wsp:rsid wsp:val=\"00C10031\"/><wsp:rsid wsp:val=\"00C363C4\"/><wsp:rsid wsp:val=\"00C51C20\"/><wsp:rsid wsp:val=\"00C84E73\"/><wsp:rsid wsp:val=\"00CD3AAF\"/><wsp:rsid wsp:val=\"00D52168\"/><wsp:rsid wsp:val=\"00D77A29\"/><wsp:rsid wsp:val=\"00E84903\"/><wsp:rsid wsp:val=\"00EA511B\"/><wsp:rsid wsp:val=\"00EE3A55\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"004C6AD9\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Elsz�moltat�si jegyz�k�nyv</w:t></w:r></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRPr=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(</w:t></w:r><w:r wsp:rsidR=\"00AE66DF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>felhaszn�l� </w:t></w:r><w:r wsp:rsidRPr=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>alapj�n)</w:t></w:r></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00573778\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�gyint�z�</w:t></w:r><w:r wsp:rsidR=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>: </w:t></w:r><Table><KRT_Csoportok_Orzo_Nev/></Table></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14653\" w:type=\"dxa\"/><w:tblInd w:w=\"-176\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"392\"/><w:gridCol w:w=\"1559\"/><w:gridCol w:w=\"1219\"/><w:gridCol w:w=\"1219\"/><w:gridCol w:w=\"1531\"/><w:gridCol w:w=\"850\"/><w:gridCol w:w=\"1049\"/><w:gridCol w:w=\"1361\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1417\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1220\"/></w:tblGrid><w:tr wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidTr=\"00574496\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>F�sz�m</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>T�rgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"00573778\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Kezel�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1531\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"00E84903\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Vonal</w:t></w:r><w:r wsp:rsidR=\"00573778\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>k�d</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"850\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Itsz</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1049\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�llapot</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1361\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Ikt</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>. d�tum</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Hat�rid�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1417\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Lez�r�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�gyirat elint�z�si id�pontja</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1220\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"00573778\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Irat helye</w:t></w:r></w:p></w:tc></w:tr><Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidTr=\"00574496\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"008120BE\"><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"1\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:tabs><w:tab w:val=\"left\" w:pos=\"180\"/><w:tab w:val=\"left\" w:pos=\"420\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><Foszam_Merge><w:tc><w:p /></w:tc></Foszam_Merge><Targy><w:tc><w:p /></w:tc></Targy><Felelos_Nev><w:tc><w:p /></w:tc></Felelos_Nev><BARCODE><w:tc><w:p /></w:tc></BARCODE><Merge_IrattariTetelszam><w:tc><w:p /></w:tc></Merge_IrattariTetelszam><Allapot_Nev><w:tc><w:p /></w:tc></Allapot_Nev><LetrehozasIdo_Rovid><w:tc><w:p /></w:tc></LetrehozasIdo_Rovid><Hatarido><w:tc><w:p /></w:tc></Hatarido><LezarasDat><w:tc><w:p /></w:tc></LezarasDat><ElintezesDat><w:tc><w:p /></w:tc></ElintezesDat><KRT_Csoportok_Orzo_Nev><w:tc><w:p /></w:tc></KRT_Csoportok_Orzo_Nev></w:tr></Table></w:tbl><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><ParentTable><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�sz�tette: </w:t></w:r><FelhNev/></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRPr=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�sz�lt: </w:t></w:r><Date/></w:p></ParentTable></ns0:NewDataSet><w:sectPr wsp:rsidR=\"00444BDF\" wsp:rsidRPr=\"00444BDF\" wsp:rsidSect=\"00444BDF\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

                    result = tms.GetWordDocument_DataSet(templateText, result, pdf);

                    byte[] res = (byte[])result.Record;

                    if (mentes.Checked)
                    {
                        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , "ElszamoltatasiJegyzokonyvek"
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                        
                        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                        result = ds.UploadFromeRecordWithCTTCheckin(execParam
                            , Constants.DocumentStoreType.SharePoint
                            , "Elszamoltatasi jegyzokonyv - felhasznalo alapjan - " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf"
                            , res
                            , new KRT_Dokumentumok()
                            , uploadXmlStrParams);
                    }

                    if (nyomtatas.Checked)
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        if (pdf)
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else
                        {
                            Response.ContentType = "application/msword";
                        }
                        Response.OutputStream.Write(res, 0, res.Length);
                        Response.OutputStream.Flush();
                        Response.End();
                    }
                }
                else
                {
                    FileStream fs = new FileStream("c:\\Contentum.Net\\eTemplateManager\\sablonok\\Elszamoltatasi jegyzokonyv - felhasznalo.xls", FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    byte[] fileRD = br.ReadBytes((int)fs.Length);

                    br.Close();
                    fs.Close();

                    result = tms.GetExcelDocument(fileRD, xml, false);

                    byte[] res = (byte[])result.Record;

                    if (mentes.Checked)
                    {
                        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , "ElszamoltatasiJegyzokonyvek"
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                        
                        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                        result = ds.UploadFromeRecordWithCTTCheckin(execParam
                            , Constants.DocumentStoreType.SharePoint
                            , "Elszamoltatasi jegyzokonyv - felhasznalo alapjan - " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xls"
                            , res
                            , new KRT_Dokumentumok()
                            , uploadXmlStrParams);
                    }

                    if (nyomtatas.Checked)
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/msexcel";
                        Response.AddHeader("content-disposition", "inline;filename=ExcelLogDetails.xls;");
                        Response.AddHeader("Cache-Control", " ");
                        Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
                        Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
                        Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");

                        Response.OutputStream.Write(res, 0, res.Length);
                        Response.OutputStream.Flush();
                        Response.End();
                    }
                }
            }

            if (SzervezetiEgyseg.Checked)
            {
                //xml = result.Ds.GetXml();
                //string xsd = result.Ds.GetXmlSchema();
                if (PDF.Checked)
                {
                    string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                    string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                    string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
                    string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
                    WebRequest wr = WebRequest.Create(SP_TM_site_url + "Elszamoltatasi jegyzokonyv - szervezeti egyseg.xml");
                    wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                    StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                    templateText = template.ReadToEnd();
                    template.Close();
                    //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"elsz-ugy\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>kiss.gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>8</o:Revision><o:TotalTime>33</o:TotalTime><o:Created>2007-11-26T08:48:00Z</o:Created><o:LastSaved>2008-04-07T14:25:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>29</o:Words><o:Characters>207</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>235</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"766C6E3A\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"00FABF00\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"360\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1080\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"1800\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2520\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3240\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"3960\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"4680\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5400\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6120\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Norm�l\"/><w:rsid w:val=\"004C6AD9\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezd�s alapbet�t�pusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Norm�l t�bl�zat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"R�csos t�bl�zat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00444BDF\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"9218\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00444BDF\"/><wsp:rsid wsp:val=\"00000191\"/><wsp:rsid wsp:val=\"000A7B4B\"/><wsp:rsid wsp:val=\"00115544\"/><wsp:rsid wsp:val=\"001273E3\"/><wsp:rsid wsp:val=\"001B3B4D\"/><wsp:rsid wsp:val=\"002A0192\"/><wsp:rsid wsp:val=\"002C4ABA\"/><wsp:rsid wsp:val=\"00304C9F\"/><wsp:rsid wsp:val=\"003C4B4E\"/><wsp:rsid wsp:val=\"00444BDF\"/><wsp:rsid wsp:val=\"00483D12\"/><wsp:rsid wsp:val=\"004C6AD9\"/><wsp:rsid wsp:val=\"00504350\"/><wsp:rsid wsp:val=\"005211FE\"/><wsp:rsid wsp:val=\"00573778\"/><wsp:rsid wsp:val=\"00574496\"/><wsp:rsid wsp:val=\"005D7A3D\"/><wsp:rsid wsp:val=\"007B7495\"/><wsp:rsid wsp:val=\"007F4439\"/><wsp:rsid wsp:val=\"008120BE\"/><wsp:rsid wsp:val=\"00892303\"/><wsp:rsid wsp:val=\"00946B74\"/><wsp:rsid wsp:val=\"00990068\"/><wsp:rsid wsp:val=\"009B5077\"/><wsp:rsid wsp:val=\"00BB26A8\"/><wsp:rsid wsp:val=\"00C10031\"/><wsp:rsid wsp:val=\"00C51C20\"/><wsp:rsid wsp:val=\"00CD3AAF\"/><wsp:rsid wsp:val=\"00D52168\"/><wsp:rsid wsp:val=\"00EA511B\"/><wsp:rsid wsp:val=\"00EE3A55\"/><wsp:rsid wsp:val=\"00FD4B89\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"004C6AD9\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Elsz�moltat�si jegyz�k�nyv</w:t></w:r></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRPr=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(</w:t></w:r><w:r wsp:rsidR=\"00CD3AAF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>szervezeti egys�g</w:t></w:r><w:r wsp:rsidRPr=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t> alapj�n)</w:t></w:r></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00573778\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�gyint�z�</w:t></w:r><w:r wsp:rsidR=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>: </w:t></w:r><Table><KRT_Csoportok_Orzo_Nev/></Table></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14653\" w:type=\"dxa\"/><w:tblInd w:w=\"-176\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"392\"/><w:gridCol w:w=\"1559\"/><w:gridCol w:w=\"1219\"/><w:gridCol w:w=\"1219\"/><w:gridCol w:w=\"1531\"/><w:gridCol w:w=\"850\"/><w:gridCol w:w=\"1049\"/><w:gridCol w:w=\"1361\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1417\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1220\"/></w:tblGrid><w:tr wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidTr=\"00574496\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>F�sz�m</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>T�rgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"00573778\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Kezel�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1531\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"00990068\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Vonal</w:t></w:r><w:r wsp:rsidR=\"00573778\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>k�d</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"850\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Itsz</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1049\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�llapot</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1361\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Ikt</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>. d�tum</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Hat�rid�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1417\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Lez�r�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Elint�z�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1220\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"00573778\" wsp:rsidP=\"00504350\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Irat helye</w:t></w:r></w:p></w:tc></w:tr><Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidTr=\"00574496\"><w:tc><w:tcPr><w:tcW w:w=\"392\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"008120BE\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"008120BE\" wsp:rsidP=\"008120BE\"><w:pPr><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"1\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:tabs><w:tab w:val=\"left\" w:pos=\"180\"/><w:tab w:val=\"left\" w:pos=\"420\"/></w:tabs><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><Foszam_Merge><w:tc><w:p /></w:tc></Foszam_Merge><Targy><w:tc><w:p /></w:tc></Targy><Felelos_Nev><w:tc><w:p /></w:tc></Felelos_Nev><BARCODE><w:tc><w:p /></w:tc></BARCODE><Merge_IrattariTetelszam><w:tc><w:p /></w:tc></Merge_IrattariTetelszam><Allapot_Nev><w:tc><w:p /></w:tc></Allapot_Nev><LetrehozasIdo_Rovid><w:tc><w:p /></w:tc></LetrehozasIdo_Rovid><Hatarido><w:tc><w:p /></w:tc></Hatarido><LezarasDat><w:tc><w:p /></w:tc></LezarasDat><ElintezesDat><w:tc><w:p /></w:tc></ElintezesDat><KRT_Csoportok_Orzo_Nev><w:tc><w:p /></w:tc></KRT_Csoportok_Orzo_Nev></w:tr></Table></w:tbl><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><ParentTable><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�sz�tette: </w:t></w:r><FelhNev/></w:p><w:p wsp:rsidR=\"00444BDF\" wsp:rsidRPr=\"00444BDF\" wsp:rsidRDefault=\"00444BDF\" wsp:rsidP=\"00444BDF\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�sz�lt: </w:t></w:r><Date/></w:p></ParentTable></ns0:NewDataSet><w:sectPr wsp:rsidR=\"00444BDF\" wsp:rsidRPr=\"00444BDF\" wsp:rsidSect=\"00444BDF\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

                    result = tms.GetWordDocument_DataSet(templateText, result, pdf);

                    byte[] res = (byte[])result.Record;

                    if (mentes.Checked)
                    {
                        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , "ElszamoltatasiJegyzokonyvek"
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                        
                        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                        result = ds.UploadFromeRecordWithCTTCheckin(execParam
                            , Constants.DocumentStoreType.SharePoint
                            , "Elszamoltatasi jegyzokonyv - szervezeti egyseg alapjan - " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf"
                            , res
                            , new KRT_Dokumentumok()
                            , uploadXmlStrParams);
                    }

                    if (nyomtatas.Checked)
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        if (pdf)
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else
                        {
                            Response.ContentType = "application/msword";
                        }
                        Response.OutputStream.Write(res, 0, res.Length);
                        Response.OutputStream.Flush();
                        Response.End();
                    }
                }
                else
                {
                    FileStream fs = new FileStream("c:\\Contentum.Net\\eTemplateManager\\sablonok\\Elszamoltatasi jegyzokonyv - szervezeti egyseg.xls", FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    byte[] fileRD = br.ReadBytes((int)fs.Length);

                    br.Close();
                    fs.Close();

                    result = tms.GetExcelDocument(fileRD, xml, false);

                    byte[] res = (byte[])result.Record;

                    if (mentes.Checked)
                    {
                        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , "ElszamoltatasiJegyzokonyvek"
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                        
                        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                        result = ds.UploadFromeRecordWithCTTCheckin(execParam
                            , Constants.DocumentStoreType.SharePoint
                            , "Elszamoltatasi jegyzokonyv - szervezeti egyseg alapjan - " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xls"
                            , res
                            , new KRT_Dokumentumok()
                            , uploadXmlStrParams);
                    }

                    if (nyomtatas.Checked)
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/msexcel";
                        Response.AddHeader("content-disposition", "inline;filename=ExcelLogDetails.xls;");
                        Response.AddHeader("Cache-Control", " ");
                        Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
                        Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
                        Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");

                        Response.OutputStream.Write(res, 0, res.Length);
                        Response.OutputStream.Flush();
                        Response.End();
                    }
                }
            }

            if (FelhasznaloAdottSzerepkoreben.Checked)
            {
                //xml = result.Ds.GetXml();
                //string xsd = result.Ds.GetXmlSchema();
                if (PDF.Checked)
                {
                    string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                    string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                    string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
                    string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
                    WebRequest wr = WebRequest.Create(SP_TM_site_url + "Elszamoltatasi jegyzokonyv - felhasznalo adott szerepkoreben.xml");
                    wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
                    StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                    templateText = template.ReadToEnd();
                    template.Close();
                    //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns4=\"elsz-\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>Kiss Gergely</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>1</o:Revision><o:TotalTime>18</o:TotalTime><o:Created>2008-02-25T10:57:00Z</o:Created><o:LastSaved>2008-02-25T11:18:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>65</o:Words><o:Characters>449</o:Characters><o:Lines>3</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>513</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"420020EB\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"2CE74C86\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"EA3ECF40\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"1\"><w:lsid w:val=\"47DA2DBB\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"F830F292\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"360\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1080\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"1800\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2520\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3240\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"3960\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"4680\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5400\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6120\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"2\"><w:lsid w:val=\"57371DCD\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"F830F292\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"360\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1080\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"1800\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2520\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3240\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"3960\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"4680\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5400\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6120\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"3\"><w:lsid w:val=\"766C6E3A\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"C3A06302\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"360\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1080\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"1800\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2520\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3240\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"3960\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"4680\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5400\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6120\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"3\"/></w:list><w:list w:ilfo=\"2\"><w:ilst w:val=\"2\"/></w:list><w:list w:ilfo=\"3\"><w:ilst w:val=\"1\"/></w:list><w:list w:ilfo=\"4\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"toc 1\"/><w:lsdException w:name=\"toc 2\"/><w:lsdException w:name=\"toc 3\"/><w:lsdException w:name=\"toc 4\"/><w:lsdException w:name=\"toc 5\"/><w:lsdException w:name=\"toc 6\"/><w:lsdException w:name=\"toc 7\"/><w:lsdException w:name=\"toc 8\"/><w:lsdException w:name=\"toc 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Default Paragraph Font\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"Table Grid\"/><w:lsdException w:name=\"Placeholder Text\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"Light Shading\"/><w:lsdException w:name=\"Light List\"/><w:lsdException w:name=\"Light Grid\"/><w:lsdException w:name=\"Medium Shading 1\"/><w:lsdException w:name=\"Medium Shading 2\"/><w:lsdException w:name=\"Medium List 1\"/><w:lsdException w:name=\"Medium List 2\"/><w:lsdException w:name=\"Medium Grid 1\"/><w:lsdException w:name=\"Medium Grid 2\"/><w:lsdException w:name=\"Medium Grid 3\"/><w:lsdException w:name=\"Dark List\"/><w:lsdException w:name=\"Colorful Shading\"/><w:lsdException w:name=\"Colorful List\"/><w:lsdException w:name=\"Colorful Grid\"/><w:lsdException w:name=\"Light Shading Accent 1\"/><w:lsdException w:name=\"Light List Accent 1\"/><w:lsdException w:name=\"Light Grid Accent 1\"/><w:lsdException w:name=\"Medium Shading 1 Accent 1\"/><w:lsdException w:name=\"Medium Shading 2 Accent 1\"/><w:lsdException w:name=\"Medium List 1 Accent 1\"/><w:lsdException w:name=\"Revision\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Medium List 2 Accent 1\"/><w:lsdException w:name=\"Medium Grid 1 Accent 1\"/><w:lsdException w:name=\"Medium Grid 2 Accent 1\"/><w:lsdException w:name=\"Medium Grid 3 Accent 1\"/><w:lsdException w:name=\"Dark List Accent 1\"/><w:lsdException w:name=\"Colorful Shading Accent 1\"/><w:lsdException w:name=\"Colorful List Accent 1\"/><w:lsdException w:name=\"Colorful Grid Accent 1\"/><w:lsdException w:name=\"Light Shading Accent 2\"/><w:lsdException w:name=\"Light List Accent 2\"/><w:lsdException w:name=\"Light Grid Accent 2\"/><w:lsdException w:name=\"Medium Shading 1 Accent 2\"/><w:lsdException w:name=\"Medium Shading 2 Accent 2\"/><w:lsdException w:name=\"Medium List 1 Accent 2\"/><w:lsdException w:name=\"Medium List 2 Accent 2\"/><w:lsdException w:name=\"Medium Grid 1 Accent 2\"/><w:lsdException w:name=\"Medium Grid 2 Accent 2\"/><w:lsdException w:name=\"Medium Grid 3 Accent 2\"/><w:lsdException w:name=\"Dark List Accent 2\"/><w:lsdException w:name=\"Colorful Shading Accent 2\"/><w:lsdException w:name=\"Colorful List Accent 2\"/><w:lsdException w:name=\"Colorful Grid Accent 2\"/><w:lsdException w:name=\"Light Shading Accent 3\"/><w:lsdException w:name=\"Light List Accent 3\"/><w:lsdException w:name=\"Light Grid Accent 3\"/><w:lsdException w:name=\"Medium Shading 1 Accent 3\"/><w:lsdException w:name=\"Medium Shading 2 Accent 3\"/><w:lsdException w:name=\"Medium List 1 Accent 3\"/><w:lsdException w:name=\"Medium List 2 Accent 3\"/><w:lsdException w:name=\"Medium Grid 1 Accent 3\"/><w:lsdException w:name=\"Medium Grid 2 Accent 3\"/><w:lsdException w:name=\"Medium Grid 3 Accent 3\"/><w:lsdException w:name=\"Dark List Accent 3\"/><w:lsdException w:name=\"Colorful Shading Accent 3\"/><w:lsdException w:name=\"Colorful List Accent 3\"/><w:lsdException w:name=\"Colorful Grid Accent 3\"/><w:lsdException w:name=\"Light Shading Accent 4\"/><w:lsdException w:name=\"Light List Accent 4\"/><w:lsdException w:name=\"Light Grid Accent 4\"/><w:lsdException w:name=\"Medium Shading 1 Accent 4\"/><w:lsdException w:name=\"Medium Shading 2 Accent 4\"/><w:lsdException w:name=\"Medium List 1 Accent 4\"/><w:lsdException w:name=\"Medium List 2 Accent 4\"/><w:lsdException w:name=\"Medium Grid 1 Accent 4\"/><w:lsdException w:name=\"Medium Grid 2 Accent 4\"/><w:lsdException w:name=\"Medium Grid 3 Accent 4\"/><w:lsdException w:name=\"Dark List Accent 4\"/><w:lsdException w:name=\"Colorful Shading Accent 4\"/><w:lsdException w:name=\"Colorful List Accent 4\"/><w:lsdException w:name=\"Colorful Grid Accent 4\"/><w:lsdException w:name=\"Light Shading Accent 5\"/><w:lsdException w:name=\"Light List Accent 5\"/><w:lsdException w:name=\"Light Grid Accent 5\"/><w:lsdException w:name=\"Medium Shading 1 Accent 5\"/><w:lsdException w:name=\"Medium Shading 2 Accent 5\"/><w:lsdException w:name=\"Medium List 1 Accent 5\"/><w:lsdException w:name=\"Medium List 2 Accent 5\"/><w:lsdException w:name=\"Medium Grid 1 Accent 5\"/><w:lsdException w:name=\"Medium Grid 2 Accent 5\"/><w:lsdException w:name=\"Medium Grid 3 Accent 5\"/><w:lsdException w:name=\"Dark List Accent 5\"/><w:lsdException w:name=\"Colorful Shading Accent 5\"/><w:lsdException w:name=\"Colorful List Accent 5\"/><w:lsdException w:name=\"Colorful Grid Accent 5\"/><w:lsdException w:name=\"Light Shading Accent 6\"/><w:lsdException w:name=\"Light List Accent 6\"/><w:lsdException w:name=\"Light Grid Accent 6\"/><w:lsdException w:name=\"Medium Shading 1 Accent 6\"/><w:lsdException w:name=\"Medium Shading 2 Accent 6\"/><w:lsdException w:name=\"Medium List 1 Accent 6\"/><w:lsdException w:name=\"Medium List 2 Accent 6\"/><w:lsdException w:name=\"Medium Grid 1 Accent 6\"/><w:lsdException w:name=\"Medium Grid 2 Accent 6\"/><w:lsdException w:name=\"Medium Grid 3 Accent 6\"/><w:lsdException w:name=\"Dark List Accent 6\"/><w:lsdException w:name=\"Colorful Shading Accent 6\"/><w:lsdException w:name=\"Colorful List Accent 6\"/><w:lsdException w:name=\"Colorful Grid Accent 6\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"Bibliography\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Norm�l\"/><w:rsid w:val=\"005977C6\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezd�s alapbet�t�pusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Norm�l t�bl�zat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszer� bekezd�s\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"005977C6\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"2050\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"005977C6\"/><wsp:rsid wsp:val=\"005977C6\"/><wsp:rsid wsp:val=\"006842CD\"/><wsp:rsid wsp:val=\"00AE2688\"/></wsp:rsids></w:docPr><w:body><ns4:NewDataSet><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Elsz�moltat�si jegyz�k�nyv</w:t></w:r></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00444BDF\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>(</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>felhaszn�l� adott szerepk�r�ben</w:t></w:r><w:r wsp:rsidRPr=\"00444BDF\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>)</w:t></w:r></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�gyint�z�: </w:t></w:r><Table><KRT_Csoportok_Orzo_Nev/></Table></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"005977C6\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr><w:t>�gyirat:</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14687\" w:type=\"dxa\"/><w:tblInd w:w=\"-176\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"426\"/><w:gridCol w:w=\"1559\"/><w:gridCol w:w=\"1219\"/><w:gridCol w:w=\"1219\"/><w:gridCol w:w=\"1531\"/><w:gridCol w:w=\"850\"/><w:gridCol w:w=\"1049\"/><w:gridCol w:w=\"1361\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1417\"/><w:gridCol w:w=\"1418\"/><w:gridCol w:w=\"1220\"/></w:tblGrid><w:tr wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidTr=\"005977C6\"><w:tc><w:tcPr><w:tcW w:w=\"426\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1559\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>F�sz�m</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>T�rgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1219\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Kezel�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1531\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Vonalk�d</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"850\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Itsz</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1049\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�llapot</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1361\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Ikt</w:t></w:r><w:proofErr w:type=\"spellEnd\"/><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>. d�tum</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Hat�rid�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1417\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Lez�r�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1418\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00504350\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Elint�z�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1220\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Irat helye</w:t></w:r></w:p></w:tc></w:tr><Table ns4:repeater=\"true\"><w:tr wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"00504350\" wsp:rsidTr=\"005977C6\"><w:tc><w:tcPr><w:tcW w:w=\"426\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:pStyle w:val=\"Listaszerbekezds\"/><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"4\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:ind w:left=\"34\" w:right=\"884\" w:first-line=\"0\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><Foszam_Merge><w:tc><w:p /></w:tc></Foszam_Merge><Targy><w:tc><w:p /></w:tc></Targy><Felelos_Nev><w:tc><w:p /></w:tc></Felelos_Nev><BARCODE><w:tc><w:p /></w:tc></BARCODE><Merge_IrattariTetelszam><w:tc><w:p /></w:tc></Merge_IrattariTetelszam><Allapot_Nev><w:tc><w:p /></w:tc></Allapot_Nev><LetrehozasIdo_Rovid><w:tc><w:p /></w:tc></LetrehozasIdo_Rovid><Hatarido><w:tc><w:p /></w:tc></Hatarido><LezarasDat><w:tc><w:p /></w:tc></LezarasDat><ElintezesDat><w:tc><w:p /></w:tc></ElintezesDat><KRT_Csoportok_Orzo_Nev><w:tc><w:p /></w:tc></KRT_Csoportok_Orzo_Nev></w:tr></Table></w:tbl><w:p wsp:rsidR=\"00AE2688\" wsp:rsidRDefault=\"00AE2688\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\"/></w:pPr></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"002C469C\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr><w:t>Iratok</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14570\" w:type=\"dxa\"/><w:tblInd w:w=\"-176\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"426\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/><w:gridCol w:w=\"1768\"/></w:tblGrid><w:tr wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidTr=\"005977C6\"><w:tc><w:tcPr><w:tcW w:w=\"426\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Iktat�sz�m</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Irat t�rgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�ldem�ny t�pusa</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Iktat�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�gyint�z�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Iktatva</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Sztorn�rozva</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1768\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�llapot</w:t></w:r></w:p></w:tc></w:tr><Table_iratok ns4:repeater=\"true\"><w:tr wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidTr=\"005977C6\"><w:tc><w:tcPr><w:tcW w:w=\"426\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:pStyle w:val=\"Listaszerbekezds\"/><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"2\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><IktatoSzam_Merge><w:tc><w:p /></w:tc></IktatoSzam_Merge><Targy1><w:tc><w:p /></w:tc></Targy1><Irattipus><w:tc><w:p /></w:tc></Irattipus><FelhasznaloCsoport_Id_Iktato_Nev><w:tc><w:p /></w:tc></FelhasznaloCsoport_Id_Iktato_Nev><FelhasznaloCsoport_Id_Ugyintezo_Nev><w:tc><w:p /></w:tc></FelhasznaloCsoport_Id_Ugyintezo_Nev><LetrehozasIdo><w:tc><w:p /></w:tc></LetrehozasIdo><SztornirozasDat><w:tc><w:p /></w:tc></SztornirozasDat><Allapot_Nev><w:tc><w:p /></w:tc></Allapot_Nev></w:tr></Table_iratok></w:tbl><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"002C469C\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"002C469C\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/><w:u w:val=\"single\"/></w:rPr><w:t>K�ldem�nyek</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14568\" w:type=\"dxa\"/><w:tblInd w:w=\"-176\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"426\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1415\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1414\"/><w:gridCol w:w=\"1415\"/></w:tblGrid><w:tr wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidTr=\"005977C6\"><w:tc><w:tcPr><w:tcW w:w=\"426\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�rk.sz�m</w:t></w:r><w:proofErr w:type=\"spellEnd\"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�ld�/felad� neve </w:t></w:r><w:proofErr w:type=\"spellStart\"/><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>ikt.sz�ma</w:t></w:r><w:proofErr w:type=\"spellEnd\"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�ld�/felad� neve</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>C�mzett</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1415\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Szervezeti egys�g</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>T�rgy</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�rk. d�tuma</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�llapot</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1414\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>Iktat�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1415\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"009B2C16\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0001386F\"><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>�rz�</w:t></w:r></w:p></w:tc></w:tr><Table_kuldemenyek ns4:repeater=\"true\"><w:tr wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"0001386F\" wsp:rsidTr=\"005977C6\"><w:tc><w:tcPr><w:tcW w:w=\"426\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:pStyle w:val=\"Listaszerbekezds\"/><w:listPr><w:ilvl w:val=\"0\"/><w:ilfo w:val=\"3\"/><wx:t wx:val=\"1.\"/><wx:font wx:val=\"Times New Roman\"/></w:listPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p></w:tc><FullErkeztetoSzam><w:tc><w:p /></w:tc></FullErkeztetoSzam><HivatkozasiSzam><w:tc><w:p /></w:tc></HivatkozasiSzam><NevSTR_Bekuldo><w:tc><w:p /></w:tc></NevSTR_Bekuldo><Csoport_Id_CimzettNev><w:tc><w:p /></w:tc></Csoport_Id_CimzettNev><Csoport_Id_FelelosNev><w:tc><w:p /></w:tc></Csoport_Id_FelelosNev><Targy><w:tc><w:p /></w:tc></Targy><LetrehozasIdo><w:tc><w:p /></w:tc></LetrehozasIdo><AllapotNev><w:tc><w:p /></w:tc></AllapotNev><IktatniKellNev><w:tc><w:p /></w:tc></IktatniKellNev><FelhasznaloCsoport_Id_OrzoNev><w:tc><w:p /></w:tc></FelhasznaloCsoport_Id_OrzoNev></w:tr></Table_kuldemenyek></w:tbl><w:p wsp:rsidR=\"005977C6\" wsp:rsidRPr=\"002C469C\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr></w:p><ParentTable><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�sz�tette: </w:t></w:r><FelhNev/></w:p><w:p wsp:rsidR=\"005977C6\" wsp:rsidRDefault=\"005977C6\" wsp:rsidP=\"005977C6\"><w:pPr><w:spacing w:after=\"0\"/></w:pPr><w:r><w:rPr><w:rFonts w:ascii=\"Times New Roman\" w:h-ansi=\"Times New Roman\"/><wx:font wx:val=\"Times New Roman\"/><w:sz w:val=\"24\"/><w:sz-cs w:val=\"24\"/></w:rPr><w:t>K�sz�lt: </w:t></w:r><Date/></w:p></ParentTable></ns4:NewDataSet><w:sectPr wsp:rsidR=\"005977C6\" wsp:rsidSect=\"005977C6\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

                    result = tms.GetWordDocument_DataSet(templateText, result, pdf);

                    byte[] res = (byte[])result.Record;

                    if (mentes.Checked)
                    {
                        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , "ElszamoltatasiJegyzokonyvek"
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                        
                        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                        result = ds.UploadFromeRecordWithCTTCheckin(execParam
                            , Constants.DocumentStoreType.SharePoint
                            , "Elszamoltatasi jegyzokonyv - felhasznalo adott szerepkoreben - " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf"
                            , res
                            , new KRT_Dokumentumok()
                            , uploadXmlStrParams);
                    }

                    if (nyomtatas.Checked)
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        if (pdf)
                        {
                            Response.ContentType = "application/pdf";
                        }
                        else
                        {
                            Response.ContentType = "application/msword";
                        }
                        Response.OutputStream.Write(res, 0, res.Length);
                        Response.OutputStream.Flush();
                        Response.End();
                    }
                }
                if (Excel.Checked)
                {
                    FileStream fs = new FileStream("c:\\Contentum.Net\\eTemplateManager\\sablonok\\Elszamoltatasi jegyzokonyv - felhasznalo adott szerepkoreben.xls", FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);

                    byte[] fileRD = br.ReadBytes((int)fs.Length);

                    br.Close();
                    fs.Close();

                    result = tms.GetExcelDocument(fileRD, xml, false);

                    byte[] res = (byte[])result.Record;

                    if (mentes.Checked)
                    {
                        String uploadXmlStrParams = String.Format("<uploadparameterek>" +
                                                          "<iktatokonyv></iktatokonyv>" +
                                                          "<sourceSharePath></sourceSharePath>" +
                                                          "<foszam>{0}</foszam>" +
                                                          "<kivalasztottIratId>{1}</kivalasztottIratId>" +
                                                          "<docmetaDokumentumId></docmetaDokumentumId>" +
                                                          "<megjegyzes></megjegyzes>" +
                                                          "<munkaanyag>{2}</munkaanyag>" +
                                                          "<ujirat>{3}</ujirat>" +
                                                          "<vonalkod></vonalkod>" +
                                                          "<docmetaIratId></docmetaIratId>" +
                                                          "<ujKrtDokBejegyzesKell>{4}</ujKrtDokBejegyzesKell>" +
                                                          "</uploadparameterek>"
                                                          , "ElszamoltatasiJegyzokonyvek"
                                                          , String.Empty
                                                          , "NEM"
                                                          , "IGEN"
                                                          , "IGEN"
                                                          );
                        
                        Contentum.eDocument.Service.DocumentService ds = eDocumentService.ServiceFactory.GetDocumentService();
                        result = ds.UploadFromeRecordWithCTTCheckin(execParam
                            , Constants.DocumentStoreType.SharePoint
                            , "Elszamoltatasi jegyzokonyv - felhasznalo adott szerepkoreben - " + System.DateTime.Today.ToString().Substring(0, 10).Replace('.', ' ') + " " + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".xls"
                            , res
                            , new KRT_Dokumentumok()
                            , uploadXmlStrParams);
                    }

                    if (nyomtatas.Checked)
                    {
                        Response.Clear();
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.ContentType = "application/msexcel";
                        Response.AddHeader("content-disposition", "inline;filename=ExcelLogDetails.xls;");
                        Response.AddHeader("Cache-Control", " ");
                        Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
                        Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
                        Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");

                        Response.OutputStream.Write(res, 0, res.Length);
                        Response.OutputStream.Flush();
                        Response.End();
                    }
                }
            }

            if (String.IsNullOrEmpty(result.ErrorCode))
            {

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    protected void FelhasznaloAdottSzerepkoreben_CheckedChanged(object sender, EventArgs e)
    {
        this.JegyzokonyvFajtaChanged();
    }

    protected void Felhasznalo_CheckedChanged(object sender, EventArgs e)
    {
        this.JegyzokonyvFajtaChanged();
    }

    protected void SzervezetiEgyseg_CheckedChanged(object sender, EventArgs e)
    {
        this.JegyzokonyvFajtaChanged();
    }

    protected void JegyzokonyvFajtaChanged()
    {
        if (FelhasznaloAdottSzerepkoreben.Checked)
        {
            FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Enabled = true;
            if (!String.IsNullOrEmpty(FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Id_HiddenField))
            {
                Szerepkorok_DropDownList.Enabled = true;
            }
        }
        else
        {
            FelhAdottSzerpkorbenCsoportId_CsoportTextBox.Enabled = false;
            Szerepkorok_DropDownList.Enabled = false;
        }

        if (Felhasznalo.Checked)
        {
            FelhCsoportId_CsoportTextBox.Enabled = true;
        }
        else
        {
            FelhCsoportId_CsoportTextBox.Enabled = false;
        }
    }
}
