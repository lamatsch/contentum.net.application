﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
    CodeFile="ElszamoltatasiJegyzokonyvPrintForm2.aspx.cs" Inherits="ElszamoltatasiJegyzokonyvPrintForm2" 
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc15" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc3" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
<%--    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>--%>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,ElszamoltatasiJegyzokonyvFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label5" runat="server" Text="Időszak:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="false" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <asp:Label ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:UpdatePanel ID="IktatoKonyvUpdatePanel" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="false" OnLoad="IktatoKonyvUpdatePanel_Load">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DatumIntervallum_SearchCalendarControl1" EventName="EvChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                       <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server" />
                                    </ContentTemplate>
                               </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <asp:Label ID="Label15" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                               <asp:Label ID="Label34" runat="server" Text="Ügyintéző:"></asp:Label></td>
                            <td class="mrUrlapMezo" width="0%" colspan="1">
                               <uc15:FelhasznaloCsoportTextBox ID="UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox" runat="server" Validate="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label1" runat="server" Text="Alszám:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <asp:CheckBox ID="cbAlszammal" runat="server" AutoPostBack="false" CssClass="HideCheckBoxText" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

