﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ElszamoltatasiJegyzokonyvPrintForm2 : Contentum.eUtility.UI.PageBase
{

    #region Utility

    #region TemplateManager params
    private int GetPriority()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        int priority = 1;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = FelhasznaloProfil.FelhasznaloId(Page);
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        krt_CsoportTagokSearch.Tipus.Value = KodTarak.CsoprttagsagTipus.vezeto;
        krt_CsoportTagokSearch.Tipus.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (!csop_result.IsError)
        {
            if (csop_result.Ds.Tables[0].Rows.Count > 0)
            {
                priority++;
            }
        }

        return priority;
    }

    private bool IsPdf()
    {
        return UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true");
    }

    private string GetFilename(bool isPdf)
    {
        return String.Format("{0}_{1}.{2}",
            "Elszamoltatasi_jegyzokonyv"
            , DateTime.Now.ToString("yyyyMMdd_HHmmss")
            , isPdf ? "pdf" : "doc"
            );
    }

    private string GetTemplateName(bool isThereAnyData, bool isAlszammal)
    {
        var kornyezet = FelhasznaloProfil.OrgKod(Page);
        var ext = ".xml";
        bool isNMHH = Constants.OrgKod.NMHH == kornyezet || Constants.OrgKod.NMHH_TUK == kornyezet;
        var s = !isNMHH ? ext : " " + kornyezet + ext;

        if (isThereAnyData)
        {
            if (isAlszammal)
            {
                return "Elszamoltatasi jegyzokonyv 2 - alszamos" + s;

            }
            else
            {
                return "Elszamoltatasi jegyzokonyv 2 - alszam nelkul" + s;
            }

        }
        else
        {
            return "Elszamoltatasi jegyzokonyv 2 - ures" + s;
        }
    }

    #endregion TemplateManager params

    #endregion Utility

    #region Page
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
        DatumIntervallum_SearchCalendarControl1.AutoPostback = true;

        if (!IsPostBack)
        {
            DatumIntervallum_SearchCalendarControl1.DatumKezd = new DateTime(System.DateTime.Today.Year, 1, 1).ToShortDateString();
            DatumIntervallum_SearchCalendarControl1.DatumVege = System.DateTime.Today.ToShortDateString();
        }

        if (IsPostBack)
        {
            if (FormHeader1.ErrorPanel.Visible == true)
            {
                FormHeader1.ErrorPanel.Visible = false;
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";
    }
    #endregion Page

    private void FillIktatoKonyvekList()
    {
        String selectedValue = IraIktatoKonyvekDropDownList1.SelectedValue;

        IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        if (DatumIntervallum_SearchCalendarControl1.IsValid_EvKezd && DatumIntervallum_SearchCalendarControl1.IsValid_EvVege)
        {
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                    , false, DatumIntervallum_SearchCalendarControl1.EvKezd.ToString(), DatumIntervallum_SearchCalendarControl1.EvVege.ToString()
                    , true, true, FormHeader1.ErrorPanel);
        }

        if (!String.IsNullOrEmpty(selectedValue))
        {
            ListItem liSelected = IraIktatoKonyvekDropDownList1.DropDownList.Items.FindByValue(selectedValue);
            if (liSelected != null)
            {
                liSelected.Selected = true;
            }
        }
    }

    protected void IktatoKonyvUpdatePanel_Load(object sender, EventArgs e)
    {
        FillIktatoKonyvekList();
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            DateTime dtKezd;
            DateTime dtVege;
            if (!DateTime.TryParse(DatumIntervallum_SearchCalendarControl1.DatumKezd, out dtKezd)
                || !DateTime.TryParse(DatumIntervallum_SearchCalendarControl1.DatumVege, out dtVege))
            {
                // hiba:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIFormatErrorHeader, Resources.Error.UIDateTimeFormatError);
                return;
            }

            string startYear = dtKezd.Year.ToString();
            string endYear = dtVege.Year.ToString();
            int migralasEve = Rendszerparameterek.GetInt(Page, Rendszerparameterek.MIGRALAS_EVE);

            bool pdf = this.IsPdf();
            int priority = this.GetPriority();

            string filename = GetFilename(pdf);

            // ha van megadva migrálás éve, akkor a migrált adatokat is lekérjük
            bool bGetMigralt = (migralasEve > 0);

            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

            EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch(true);

            string ParentTable = "<ParentTable>";

            string date = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10) + "-tól " + DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10) + "-ig";

            string Iktatohely = String.Empty;

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                ////where = "and EREC_UgyUgyiratok.IraIktatokonyv_Id='" + IraIktatoKonyvekDropDownList1.SelectedValue + "' ";

                Iktatohely = IraIktatoKonyvekDropDownList1.SelectedValue.Split(new char[] { '|' })[0];   // itt biztosan van legalább 0. elem

                EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam erec_IraIktatoKonyvek_execParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

                erec_IraIktatoKonyvekSearch.Iktatohely.Value = Iktatohely; //IraIktatoKonyvekDropDownList1.SelectedValue.Remove(IraIktatoKonyvekDropDownList1.SelectedValue.Length - 1);
                erec_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
                erec_IraIktatoKonyvekSearch.IktatoErkezteto.Value = Constants.IktatoErkezteto.Iktato; // "I"
                erec_IraIktatoKonyvekSearch.IktatoErkezteto.Operator = Query.Operators.equals;
                erec_IraIktatoKonyvekSearch.Ev.Value = startYear; //DatumIntervallum_SearchCalendarControl1.DatumKezd.Remove(4);
                erec_IraIktatoKonyvekSearch.Ev.ValueTo = endYear; //DatumIntervallum_SearchCalendarControl1.DatumVege.Remove(4);
                erec_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.between;

                Result erec_IraIktatoKonyvek_result = erec_IraIktatoKonyvekService.GetAllWithExtension(erec_IraIktatoKonyvek_execParam, erec_IraIktatoKonyvekSearch);

                if (erec_IraIktatoKonyvek_result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, erec_IraIktatoKonyvek_result);
                    return;
                }

                if (erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count > 0)
                {
                    string[] arrayIds = new string[erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count];
                    for (int i = 0; i < erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count; i++)
                    {
                        arrayIds[i] = erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows[i]["Id"].ToString();
                    }

                    var ids = Search.GetSqlInnerString(arrayIds);

                    erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Value = ids;
                    erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator = Query.Operators.inner;

                    erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id.Value = ids; //IraIktatoKonyvekDropDownList1.SelectedValue;
                    erec_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id.Operator = Query.Operators.inner; //Query.Operators.equals;

                }

                ParentTable = ParentTable + "<Iktatokonyv>" + IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text + "</Iktatokonyv>";
            }

            var ugyintezoId = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
            var ugyintezoNev = "";
            if (!string.IsNullOrEmpty(ugyintezoId))
            {
                ugyintezoNev = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text;

                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Group = "314";
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.GroupOperator = Query.Operators.or;
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = ugyintezoId;
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;

                erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Group = "314";
                erec_UgyUgyiratokSearch.Csoport_Id_Felelos.GroupOperator = Query.Operators.or;
                erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value = ugyintezoId;
                erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Operator = Query.Operators.equals;

                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Group = "314";
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.GroupOperator = Query.Operators.or;
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value = ugyintezoId;
                erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;

                erec_PldIratPeldanyokSearch.FelhasznaloCsoport_Id_Orzo.Value = ugyintezoId;
                erec_PldIratPeldanyokSearch.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.equals;
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = ugyintezoId;
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.notequals;

                ParentTable = ParentTable + "<Ugyintezo>" + ugyintezoNev + "</Ugyintezo>";
            }

            //where = where + " and EREC_UgyUgyiratok.LetrehozasIdo BETWEEN '" + _datekezd + " 00:00:00' and '" + _datevege + " 23:59:59' ";
            // EB 2010.02.12: mivel nincs az üzleti objektumban LetrehozasIdo, így létrehozás ideje helyett az érvényességre kérdezünk le
            // így ugyan lehetnek tizedmásodperc nagyságrendű eltérések, de ez az eredmény szempontjából gyakorlatilag nem jelent adatvesztési kockázatot
            erec_UgyUgyiratokSearch.ErvKezd.Value = dtKezd.ToShortDateString();
            erec_UgyUgyiratokSearch.ErvKezd.ValueTo = dtVege.Date.AddDays(1).AddMilliseconds(1).ToString();
            erec_UgyUgyiratokSearch.ErvKezd.Operator = Query.Operators.between;

            ParentTable = ParentTable + "<Datum>" + date + "</Datum>";

            bool isFPH = FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.FPH;

            //LZS - BUG_12027
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            string[] allapotok = Rendszerparameterek.Get(execParam,Rendszerparameterek.ELSZAMOLTATAS_RELEVANS_UGYIRAT_ALLAPOT).Split(',');

            var tovabbitasAlattAllapotInner = Search.GetSqlInnerString(allapotok);

            var allapotLista = new List<string>(allapotok);
            allapotLista.Add(KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt);
            var allapotListaInner = Search.GetSqlInnerString(allapotLista.ToArray());

            //
            erec_UgyUgyiratokSearch.Allapot.Value = allapotListaInner;
            erec_UgyUgyiratokSearch.Allapot.Operator = Query.Operators.inner;

            erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Value = tovabbitasAlattAllapotInner;
            erec_UgyUgyiratokSearch.TovabbitasAlattAllapot.Operator = Query.Operators.isnullorinner;

            erec_UgyUgyiratokSearch.OrderBy = "EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam,EREC_IraIratok.Alszam";

           
            erec_UgyUgyiratokSearch.TopRow = 5000;

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();



            //LZS - BUG_4747
            //NMHH esetén a régi template manageres megoldás helyett SSRS riportot használunk.
            //Ehhez a ForElszamoltatasiJkSSRS webmethod-ot kell meghívnunk.
            Result result = null;
            bool isNMHH = FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.NMHH;
            if (isNMHH)
            {
                //LZS - BUG_4747
                //Attól függően, hogy szükséges-e az alszámok lekérdezése, változik a rendezés is, alszám lekérdezéskor az EREC_IraIratok.Alszam -ra is rendezünk.
                if (cbAlszammal.Checked)
                {
                    erec_UgyUgyiratokSearch.OrderBy = "EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam,EREC_IraIratok.Alszam";
                }
                else
                {
                    erec_UgyUgyiratokSearch.OrderBy = "EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam";
                }

                //LZS - BUG_4747
                //Meghívjuk a webmethod-ot a megfelelő search objektummal és az alszám paraméterrel.
                result = service.ForElszamoltatasiJkSSRS(execParam, erec_UgyUgyiratokSearch, cbAlszammal.Checked);
            }
            else
            {
                result = service.ForElszamoltatasiJk(execParam, erec_UgyUgyiratokSearch);
            }

            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            // iratpéldányok (csak FPH)
            Result pld_result = null;
            if (isFPH)
            {
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Allapot.Value = allapotListaInner; //"'04','06','07','13','55','56','50','09'";
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.Allapot.Operator = Query.Operators.inner;

                // EB 2010.02.12: eddig nem volt
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.Value = tovabbitasAlattAllapotInner; //"'04','06','07','13','55','56','09'";
                erec_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch.TovabbitasAlattAllapot.Operator = Query.Operators.isnullorinner;

                // CR#2405: iratpéldány állapotok szűrése

                string[] PldNotInAllapotok = new string[] {
                    KodTarak.IRATPELDANY_ALLAPOT.Cimzett_atvette
                    , KodTarak.IRATPELDANY_ALLAPOT.Lezart_jegyzeken
                    , KodTarak.IRATPELDANY_ALLAPOT.Postazott
                    , KodTarak.IRATPELDANY_ALLAPOT.Sztornozott
                    , KodTarak.IRATPELDANY_ALLAPOT.Cimzett_nem_vette_at
                };

                erec_PldIratPeldanyokSearch.Allapot.Value = Search.GetSqlInnerString(PldNotInAllapotok);
                erec_PldIratPeldanyokSearch.Allapot.Operator = Query.Operators.notinner;

                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Value = Search.GetSqlInnerString(PldNotInAllapotok);
                erec_PldIratPeldanyokSearch.TovabbitasAlattAllapot.Operator = Query.Operators.isnullornotinner;

                //erec_PldIratPeldanyokSearch.WhereByManual = " and EREC_PldIratPeldanyok.LetrehozasIdo BETWEEN '" + _datekezd + " 00:00:00' and '" + _datevege + " 23:59:59'";
                // EB 2010.02.12: mivel nincs az üzleti objektumban LetrehozasIdo, így létrehozás ideje helyett az érvényességre kérdezünk le
                // így ugyan lehetnek tizedmásodperc nagyságrendű eltérések, de ez az eredmény szempontjából gyakorlatilag nem jelent adatvesztési kockázatot
                erec_PldIratPeldanyokSearch.ErvKezd.Value = dtKezd.ToShortDateString();
                erec_PldIratPeldanyokSearch.ErvKezd.ValueTo = dtVege.Date.AddDays(1).AddMilliseconds(1).ToString();
                erec_PldIratPeldanyokSearch.ErvKezd.Operator = Query.Operators.between;

                erec_PldIratPeldanyokSearch.OrderBy = " EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_IraIratok.Alszam,EREC_PldIratPeldanyok.Sorszam";

                EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                pld_result = erec_PldIratPeldanyokService.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

                if (pld_result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, pld_result);
                    return;
                }
            }

            //LZS - BUG_4747
            Result mig_result = null;
            Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch mig_FoszamSearch = null;

            //LZS - MIG result dataset lekérés áthelyezése ide
            if (bGetMigralt)
            {
                if (dtKezd.Year <= migralasEve)
                {
                    Contentum.eMigration.Service.MIG_FoszamService MIG_service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
                    mig_FoszamSearch = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();

                    if (!String.IsNullOrEmpty(Iktatohely))
                    {
                        string sav = IktatoKonyvek.GetSavFromIktatohely(Iktatohely);
                        //if (!string.IsNullOrEmpty(sav))
                        //{
                        //mig_FoszamSearch.WhereByManual = " and PATINDEX('(" + sav + ")%',MIG_Sav.NAME) != 0 ";
                        mig_FoszamSearch.EdokSav.Value = sav;
                        mig_FoszamSearch.EdokSav.Operator = Query.Operators.equals;
                        //}
                    }
                    else
                    {
                        mig_FoszamSearch.EdokSav.Value = "";
                        mig_FoszamSearch.EdokSav.Operator = Query.Operators.notnull;
                    }

                    string nev = null;
                    if (!string.IsNullOrEmpty(ugyintezoId))
                    {
                        nev = UI.RemoveEmailAddressFromCsoportNev(ugyintezoNev);
                        //MIG_where = MIG_where + " and MIG_Eloado.NAME='" + nev + "' ";

                        if (!String.IsNullOrEmpty(nev))
                        {
                            //// Foszam eloado: ket helyen is keresni kel
                            //// TODO: - index hiányában és főleg az OR feltétel miatt nagyon lassú, megoldási lehetőségek:
                            //// 1. index (mindkét további megoldás mellé ajánlott)
                            //// 2. két külön hívás és összefésülés vagy
                            //// 3. Név átadás külön paraméterben a tárolt eljárás felé és UNION (ajánlott)
                            //mig_FoszamSearch.Edok_Ugyintezo_Csoport_Id.Group = "444";
                            //mig_FoszamSearch.Edok_Ugyintezo_Csoport_Id.GroupOperator = Query.Operators.or;
                            //mig_FoszamSearch.Edok_Ugyintezo_Csoport_Id.Value = UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField;
                            //mig_FoszamSearch.Edok_Ugyintezo_Csoport_Id.Operator = Query.Operators.equals;

                            //mig_FoszamSearch.Manual_MIG_Eloado_NAME.Group = "444";
                            //mig_FoszamSearch.Manual_MIG_Eloado_NAME.GroupOperator = Query.Operators.or;
                            //mig_FoszamSearch.Manual_MIG_Eloado_NAME.Value = nev;
                            //mig_FoszamSearch.Manual_MIG_Eloado_NAME.Operator = Query.Operators.equals;
                        }
                        else
                        {
                            // TODO: hiba
                        }

                        mig_FoszamSearch.ExtendedMIG_AlszamSearch.OrderBy = " order by MIG_Alszam.ALNO";
                    }
                    mig_FoszamSearch.UI_YEAR.Value = startYear;
                    mig_FoszamSearch.UI_YEAR.ValueTo = endYear;
                    mig_FoszamSearch.UI_YEAR.Operator = Query.Operators.between;

                    //mig_FoszamSearch.WhereByManual = " MIG_Foszam.UGYHOL != '2' and MIG_Foszam.UGYHOL != 'S' and MIG_Foszam.Edok_Utoirat_Id IS NULL and MIG_Foszam.Csatolva_Id IS NULL " + MIG_where;
                    string[] UgyholNotIn = new string[] { Constants.MIG_IratHelye.Irattarban, Constants.MIG_IratHelye.Selejtezett, Constants.MIG_IratHelye.Lomtar,
                    Constants.MIG_IratHelye.Leveltar, Constants.MIG_IratHelye.Jegyzek, Constants.MIG_IratHelye.LezartJegyzek, Constants.MIG_IratHelye.EgyebSzervezetnekAtadott};

                    mig_FoszamSearch.UGYHOL.Value = Search.GetSqlInnerString(UgyholNotIn);
                    mig_FoszamSearch.UGYHOL.Operator = Query.Operators.notinner;

                    mig_FoszamSearch.OrderBy = " MIG_Foszam.EdokSav,MIG_Foszam.UI_YEAR,MIG_Foszam.UI_NUM,MIG_Alszam.ALNO";
                    mig_FoszamSearch.TopRow = 5000;

                    mig_result = MIG_service.ForElszamoltatasiJk(execParam, mig_FoszamSearch, ugyintezoId, nev);

                    if (mig_result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, mig_result);
                        return;
                    }
                }
            }



            //LZS - NMHH-nak SSRS riportos megoldás
            if (isNMHH)
            {


                Query query_EREC = new Query();
                Query query_MIG = new Query();

                if (erec_UgyUgyiratokSearch != null)
                {
                    query_EREC.BuildFromBusinessDocument(erec_UgyUgyiratokSearch);
                }

                if (mig_FoszamSearch != null)
                {
                    query_MIG.BuildFromBusinessDocument(mig_FoszamSearch);
                }

                string where_EREC = query_EREC.Where != null ? query_EREC.Where : "";
                string where_MIG = query_MIG.Where != null ? query_MIG.Where : "";
                string orderby = erec_UgyUgyiratokSearch.OrderBy;
                string executor = execParam.Felhasznalo_Id;


                Session["Where_EREC"] = where_EREC;
                Session["Where_MIG"] = where_MIG;

                string queryString = "Ugyintezo=" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text + '&' + "Iktatokonyv=" + IraIktatoKonyvekDropDownList1.SelectedValue + '&' + "Alszam=" + cbAlszammal.Checked + '&' + "DateFrom=" + DatumIntervallum_SearchCalendarControl1.DatumKezd + '&' + "DateTo=" + DatumIntervallum_SearchCalendarControl1.DatumVege + '&' + "OrderBy=" + orderby + '&' + "Executor=" + executor;

                //Meghívjuk az ElszamoltatasiJegyzokonyvAutoPrintSSRS.aspx nyomtatóoldalt, .
                string js = "javascript:window.open('ElszamoltatasiJegyzokonyvPrintFormSSRS.aspx?" + queryString.Trim() + "')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ElszamoltatasiJegyzokonyvPrintFormSSRS", js, true);
            }
            else //LZS - FPH-nak marad a template manageres megoldás
            {
                // template
                string xml = "";
                string mig_xml = "";
                string pld_xml = "";

                if (result != null && result.Ds.Tables.Count > 0 && result.Ds.Tables[0].Rows.Count > 0)
                    xml = result.Ds.Tables[0].Rows[0]["string_xml"].ToString();

                if (mig_result != null && mig_result.Ds.Tables.Count > 0 && mig_result.Ds.Tables[0].Rows.Count > 0)
                    mig_xml = mig_result.Ds.Tables[0].Rows[0]["string_xml"].ToString();

                //LZS - MIG_Xml áthelyezése ide
                if (!string.IsNullOrEmpty(mig_xml))
                {
                    mig_xml = mig_xml.Replace("MIG_Foszam", "EREC_UgyUgyiratok");
                    mig_xml = mig_xml.Replace("MIG_Alszam", "EREC_IraIratok");

                    if (!string.IsNullOrEmpty(xml))
                    {
                        xml = xml.Remove(0, 12);
                        xml = xml.Remove(xml.IndexOf("</NewDataSet>"));
                    }
                }

                string alszam = "";
                if (cbAlszammal.Checked)
                {
                    alszam = "X";
                }

                pld_xml = pld_result == null ? "" : pld_result.Ds.GetXml();

                if (!string.IsNullOrEmpty(pld_xml))
                {
                    pld_xml = pld_xml.Replace("Table", "pld_result_Table");
                    pld_xml = pld_xml.Remove(0, 12);
                    pld_xml = pld_xml.Remove(pld_xml.IndexOf("</NewDataSet>"));
                }

                string templateName = null;
                if (!string.IsNullOrEmpty(xml) || (bGetMigralt && !string.IsNullOrEmpty(mig_xml)) || !string.IsNullOrEmpty(pld_xml))
                {

                    if (bGetMigralt && !string.IsNullOrEmpty(mig_xml))
                    {
                        mig_xml = mig_xml.Insert(mig_xml.IndexOf("</NewDataSet>"), xml + pld_xml + ParentTable + "<Date>" + System.DateTime.Now.ToString() + "</Date><Alszam>" + alszam + "</Alszam></ParentTable>");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(xml))
                        {
                            mig_xml = xml;
                            mig_xml = mig_xml.Insert(mig_xml.IndexOf("</NewDataSet>"), pld_xml + ParentTable + "<Date>" + System.DateTime.Now.ToString() + "</Date><Alszam>" + alszam + "</Alszam></ParentTable>");
                        }
                        else
                        {
                            mig_xml = "<NewDataSet>" + pld_xml + ParentTable + "<Date>" + System.DateTime.Now.ToString() + "</Date><Alszam>" + alszam + "</Alszam></ParentTable></NewDataSet>";
                        }
                    }

                    //if (!cbAlszammal.Checked)
                    //{
                    //    requestUriStringName = "Elszamoltatasi jegyzokonyv 2 - alszam nelkul.xml";
                    //}
                    //else
                    //{
                    //    requestUriStringName = "Elszamoltatasi jegyzokonyv 2 - alszamos.xml";
                    //}

                    templateName = GetTemplateName(true, cbAlszammal.Checked);

                }
                else
                {
                    //requestUriStringName = "Elszamoltatasi jegyzokonyv 2 - ures.xml";
                    templateName = GetTemplateName(false, cbAlszammal.Checked);

                    mig_xml = "<NewDataSet>" + ParentTable + "<Date>" + System.DateTime.Now.ToString() + "</Date><Alszam>" + alszam + "</Alszam></ParentTable></NewDataSet>";
                }

                #region Prepare TemplateManager
                string templateText = null;
                string __usernev = null;
                string __password = null;
                string __domain = null;
                string SP_TM_site_url = null;

                try
                {
                    __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
                    __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
                    __domain = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
                    SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");

                    WebRequest wr = WebRequest.Create(String.Format("{0}{1}", SP_TM_site_url, templateName));
                    wr.Credentials = new NetworkCredential(__usernev, __password, __domain);
                    StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
                    templateText = template.ReadToEnd();
                    template.Close();
                }
                catch (ConfigurationErrorsException)
                {
                    // TODO: kiemelni Resources.Errorba (elvileg ilyen hiba nem léphet fel...)
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, "Hiba a konfigurációs fájl (webconfig) olvasásakor!");
                    return;
                }
                catch (Exception exeption)
                {
                    // TODO: kiemelni Resources.Errorba

                    string errormessage = exeption.Message;
                    Exception ex = exeption.InnerException;
                    while (ex != null)
                    {
                        errormessage = String.Format("{0} {1}", errormessage, ex.Message);
                        ex = ex.InnerException;
                    }

                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, errormessage);
                    return;
                }
                #endregion Prepare TemplateManager

                result.Ds.Tables[0].Rows[0]["string_xml"] = mig_xml;

                result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

                Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
                result = tms.GetWordDocument_ForXml_Thread(templateText, result, pdf, priority, filename, 25);

                byte[] res = (byte[])result.Record;

                if (!result.IsError)
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    if (pdf)
                    {
                        Response.ContentType = "application/pdf";
                    }
                    else
                    {
                        Response.ContentType = "application/msword";
                    }
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filename + "\";");
                    Response.OutputStream.Write(res, 0, res.Length);
                    Response.OutputStream.Flush();
                    Response.End();
                }
                else
                {
                    if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
                    {
                        string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        return;
                    }

                }
            }
        }
        else if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}