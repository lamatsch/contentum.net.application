<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="EmailBoritekokForm.aspx.cs" Inherits="EmailBoritekokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,EmailBoritekokFormHeaderTitle %>" />
    <div class="popupBody">
    <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption_shorter">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                <tr class="urlapSor">
                    <td class="mrUrlapCaption_shorter">
                    </td>
                    <td class="mrUrlapMezo" style="vertical-align: top; text-align: right">
                        <asp:ImageButton ID="ImageButton_HtmlPrint" runat="server" ImageUrl="~/images/hu/trapezgomb/nyomtatas_trap.jpg"
                            onmouseover="swapByName(this.id,'nyomtatas_trap2.jpg')" onmouseout="swapByName(this.id,'nyomtatas_trap.jpg')" />
                    </td>
                </tr>                  
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelFelado" runat="server" Text="Felad�:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                        <asp:TextBox ID="textFelado"  CssClass="mrUrlapInputFull" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelCimzett" runat="server" Text="C�mzett:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textCimzett"  CssClass="mrUrlapInputFull" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelCC" runat="server" Text="M�solatot kap:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textCC"  CssClass="mrUrlapInputFull" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelTargy" runat="server" Text="T�rgy:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textTargy"  CssClass="mrUrlapInputFull" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                   <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelFeladasDatuma" runat="server" Text="Felad�s d�tuma:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                        <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align:left">
                                <asp:TextBox ID="textFeladasDatuma"  CssClass="mrUrlapInputSearch" runat="server"></asp:TextBox>
                            </td>
                            <td class="mrUrlapCaption_short" style="padding-left:5px">
                                <asp:Label ID="labelErkezesDatuma" runat="server" Text="�rkez�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textErkezesDatuma"  CssClass="mrUrlapInputSearch" runat="server"></asp:TextBox>
                            </td>
                        </tr> 
                        </table>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelFeldolgozasiIdo" runat="server" Text="Feldolgoz�si id�:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                        <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textFeldolgozasiIdo"  CssClass="mrUrlapInputSearch" runat="server"></asp:TextBox>
                            </td>
                            <td class="mrUrlapCaption_short" style="padding-left:5px">
                                <asp:Label ID="labelFontossag" runat="server" Text="Fontoss�g:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:KodtarakDropDownList ID="KodtarakDropDownListFontossag" runat="server" CssClass="mrUrlapInputSearchComboBox"/>
                            </td>
                        </tr> 
                        </table>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter">
                            <asp:Label ID="labelUzenet" runat="server" Text="�zenet:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                        <asp:Panel runat="server" ID="paneluzenet" CssClass="mrUrlapInputFullComboBox">
                        <div runat="server" id="divUzenet" style="overflow:auto;height:200px;color:Black;border: solid 1px #9d9da1;width:100%">
                            <asp:Label ID="labelUzenetContent" runat="server"></asp:Label>
                        </div>
                        </asp:Panel>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption_shorter" >
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc5:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>        
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
     </div>
</asp:Content>

