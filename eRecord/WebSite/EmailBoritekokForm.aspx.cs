using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class EmailBoritekokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kodcsoportEmailBoritekFontossag = "EMAILBORITEK_FONTOSSAG";

    #region JavaScripts
    private void RegisterPrintControlJavaScript(string cssPath)
    {
        if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "PrintHtmlScript"))
        {
            string js = "";
            js = @"function PrintContent(ctrl_Id, title, footer)
{
    var DocumentContainer = $get(ctrl_Id);
    if (DocumentContainer)
    {
        var WindowObject = window.open('', 'Nyomtat�s','width=800,height=600,top=100,left=150,location=no,menubar=no,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<html><head>');";
            if (!String.IsNullOrEmpty(cssPath))
            {
                js += @"
        WindowObject.document.writeln('<link href=""" + cssPath + @""" type=""text/css"" rel=""stylesheet"" />');";
            }
            js += @"
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln('<h1 class=""PrintHeader"">' + title + '</h1>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('<p class=""PrintFooter"">' + footer + '</p>');
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
}
";

            js += @"function PrintContent(ctrl_Id, insertBefore, insertAfter, title, footer)
{
    var DocumentContainer = $get(ctrl_Id);
    if (DocumentContainer)
    {
        var WindowObject = window.open('', 'Nyomtat�s','width=800,height=600,top=100,left=150,location=no,menubar=no,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<html><head>');";
            if (!String.IsNullOrEmpty(cssPath))
            {
                js += @"
        WindowObject.document.writeln('<link href=""" + cssPath + @""" type=""text/css"" rel=""stylesheet"" />');";
            }
            js += @"
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln('<h1 class=""PrintHeader"">' + title + '</h1>');
        WindowObject.document.writeln(insertBefore);
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln(insertAfter);
        WindowObject.document.writeln('<p class=""PrintFooter"">' + footer + '</p>');
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
}
";

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "PrintHtmlScript", js, true);
        }
    }

    private void RegisterGetPrintHeaderFromTextPairsJavaScript(List<Pair> LabelTextPairs)
    {
        if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "GetPrintHeaderScript"))
        {
            string js = "";
            js = @"function GetPrintHeader()
{
    var header = '';
";
            if (LabelTextPairs != null)
            {
                int i = 0;
                for (i = 0; i < LabelTextPairs.Count; i++)
                {
                    string LabelText = LabelTextPairs[i].First.ToString();
                    string TextBoxText = LabelTextPairs[i].Second.ToString();
                    if (!String.IsNullOrEmpty(LabelText) && !String.IsNullOrEmpty(TextBoxText))

                        js += @"
        header +=  '<b>' + '" + LabelText + @"' + '<\/b> ' + '" + TextBoxText + @"' + '<br \/>';
";
                }
            }

            js += @"
    header += '<br \/><br \\/>---------------------------<br \\/>';
    return header;
}
";

            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "GetPrintHeaderScript", js, true);
        }
    }
    #endregion

    private List<Pair> GetLabelTextPairsForPrintHeader()
    {
        List<Pair> LabelTextPairs = new List<Pair>();

        LabelTextPairs.Add(new Pair(labelFelado.Text, textFelado.Text));
        LabelTextPairs.Add(new Pair(labelCimzett.Text, textCimzett.Text));
        LabelTextPairs.Add(new Pair(labelCC.Text, textCC.Text));
        LabelTextPairs.Add(new Pair(labelFeladasDatuma.Text, textFeladasDatuma.Text));
        LabelTextPairs.Add(new Pair(labelErkezesDatuma.Text, textErkezesDatuma.Text));
        LabelTextPairs.Add(new Pair(labelFeldolgozasiIdo.Text, textFeldolgozasiIdo.Text));
        LabelTextPairs.Add(new Pair(labelTargy.Text, textTargy.Text));
        LabelTextPairs.Add(new Pair(labelFontossag.Text, KodtarakDropDownListFontossag.Text));

        return LabelTextPairs;
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        textFelado.ReadOnly = true;
        textCimzett.ReadOnly = true;
        textCC.ReadOnly = true;
        textTargy.ReadOnly = true;
        textErkezesDatuma.ReadOnly = true;
        textFeladasDatuma.ReadOnly = true;
        textFeldolgozasiIdo.ReadOnly = true;
        KodtarakDropDownListFontossag.ReadOnly = true;
        //textUzenet.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelFelado.CssClass = "mrUrlapInputWaterMarked";
        labelCimzett.CssClass = "mrUrlapInputWaterMarked";
        labelCC.CssClass = "mrUrlapInputWaterMarked";
        labelTargy.CssClass = "mrUrlapInputWaterMarked";
        labelErkezesDatuma.CssClass = "mrUrlapInputWaterMarked";
        labelFeladasDatuma.CssClass = "mrUrlapInputWaterMarked";
        labelFeldolgozasiIdo.CssClass = "mrUrlapInputWaterMarked";
        labelFontossag.CssClass = "mrUrlapInputWaterMarked";
        labelUzenet.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "EmailBoritek" + Command);
                break;
        }

        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_eMailBoritekokService service = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_eMailBoritekok EREC_eMailBoritekok = (EREC_eMailBoritekok)result.Record;
                    LoadComponentsFromBusinessObject(EREC_eMailBoritekok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }

        string cssPath = Request.ApplicationPath + "/App_Themes/" + Page.Theme + "/StyleSheet.css";
        RegisterPrintControlJavaScript(cssPath);
        RegisterGetPrintHeaderFromTextPairsJavaScript(GetLabelTextPairsForPrintHeader());
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        string PrintHeader = FormHeader1.HeaderTitle + " - " + Resources.Form.FormHeaderPrint;
        string InsertBefore = "GetPrintHeader()";
        string InsertAfter = "'<br \\/>---------------------------<br \\/>'";
        string felhasznaloNev = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page), Page);
        string PrintFooter = String.Format(Resources.Form.UI_TreeView_PrintFooter, felhasznaloNev, System.DateTime.Now.ToShortDateString());
        ImageButton_HtmlPrint.OnClientClick = "PrintContent('" + divUzenet.ClientID + @"',"
            + InsertBefore + "," + InsertAfter + ",'" + PrintHeader + "','" + PrintFooter + @"');return false;";
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="EREC_eMailBoritekok"></param>
    private void LoadComponentsFromBusinessObject(EREC_eMailBoritekok EREC_eMailBoritekok)
    {
        textFelado.Text = EREC_eMailBoritekok.Felado;
        textCimzett.Text = EREC_eMailBoritekok.Cimzett;
        textCC.Text = EREC_eMailBoritekok.CC;
        textTargy.Text = EREC_eMailBoritekok.Targy;
        textErkezesDatuma.Text = EREC_eMailBoritekok.ErkezesDatuma;
        textFeladasDatuma.Text = EREC_eMailBoritekok.FeladasDatuma;
        textFeldolgozasiIdo.Text = EREC_eMailBoritekok.FeldolgozasIdo;
        KodtarakDropDownListFontossag.FillAndSetSelectedValue(kodcsoportEmailBoritekFontossag,EREC_eMailBoritekok.Fontossag,FormHeader1.ErrorPanel);
        labelUzenetContent.Text = EREC_eMailBoritekok.Uzenet;
        ErvenyessegCalendarControl1.ErvKezd = EREC_eMailBoritekok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = EREC_eMailBoritekok.ErvVege;

        //aktu�lis verzi� ekt�rol�sa
        FormHeader1.Record_Ver = EREC_eMailBoritekok.Base.Ver;
        //A mod�sit�s,l�trehoz�s form be�ll�t�sa
        //FormPart_CreatedModified1.SetComponentValues(EREC_eMailBoritekok.Base);
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;
            compSelector.Add_ComponentOnClick(textFelado);
            compSelector.Add_ComponentOnClick(textCimzett);
            compSelector.Add_ComponentOnClick(textCC);
            compSelector.Add_ComponentOnClick(textTargy);
            compSelector.Add_ComponentOnClick(textErkezesDatuma);
            compSelector.Add_ComponentOnClick(textFeladasDatuma);
            compSelector.Add_ComponentOnClick(textFeldolgozasiIdo);
            compSelector.Add_ComponentOnClick(KodtarakDropDownListFontossag);
            compSelector.Add_ComponentOnClick(paneluzenet);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
    }
}
