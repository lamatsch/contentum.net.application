<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmailBoritekokList.aspx.cs" Inherits="EmailBoritekokList" Title="Untitled Page" %>

<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<%--SharePoint js--%>
<script type="text/javascript" language="javascript" src="~/JavaScripts/SharePointInit.js"></script>
<script type="text/javascript" language="javascript" src="~/JavaScripts/SharePointCore.js"></script>


    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server"/>
    <!--Hiba megjelen�t�se-->
    <asp:UpdatePanel id="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" >
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 

     <!--F� lista-->
     <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td style="text-align: left; vertical-align: top; width: 0px;">
           <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeEmailBoritekok" OnClientClick="return false;" />
           </td>
             <td style="text-align: left; vertical-align: top; width: 100%;">           
                <asp:UpdatePanel ID="updatePanelEmailBoritekok" runat="server" OnLoad="updatePanelEmailBoritekok_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="cpeEmailBoritekok" runat="server" TargetControlID="panelEmailBoritekok"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeEmailBoritekok" CollapseControlID="btnCpeEmailBoritekok"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeEmailBoritekok"
                            ExpandedSize="0" ExpandedText="E-mail bor�t�kok list�ja" CollapsedText="E-mail bor�t�kok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="panelEmailBoritekok" runat="server" Width="100%"> 
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                     <asp:GridView id="gridViewEmailBoritekok" runat="server" GridLines="None" BorderWidth="1px" BorderStyle="Solid" BorderColor="#e6e6e6" OnRowCommand="gridViewEmailBoritekok_RowCommand" 
                                     OnPreRender="gridViewEmailBoritekok_PreRender" AllowSorting="True" PagerSettings-Visible="false" CellPadding="0" 
                                     DataKeyNames="Id" AutoGenerateColumns="False" OnRowDataBound="gridViewEmailBoritekok_RowDataBound" OnSorting="gridViewEmailBoritekok_Sorting" AllowPaging="true">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                        <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                <HeaderTemplate>
                                                <div style="white-space:nowrap">                                
                                                    <asp:ImageButton ID="SelectingRowsImageButton"  runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                     &nbsp;&nbsp;
                                                     <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                 </div>
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <asp:CheckBox id="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                 </ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:CommandField>  
                                            <asp:BoundField DataField="Felado" HeaderText="Felad�" SortExpression="EREC_eMailBoritekok.Felado">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Cimzett" HeaderText="C�mzett" SortExpression="EREC_eMailBoritekok.Cimzett">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CC" HeaderText="M�solatot kap" SortExpression="EREC_eMailBoritekok.CC">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_eMailBoritekok.Targy">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FontossagNev" HeaderText="Fontoss�g" SortExpression="FontossagKodTarak.Nev">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="80px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ErkezesDatuma" HeaderText="�rkez�s d�tuma" SortExpression="EREC_eMailBoritekok.ErkezesDatuma">
                                                <HeaderStyle  CssClass="GridViewBorderHeader" Width="140px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" Width="140px" />
                                            </asp:BoundField>       
                                            <asp:TemplateField HeaderText="�rkeztetve">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" Width="70px" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imageButtonErkeztetve" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                        AlternateText="K�ldem�ny megtekint�se" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Iktatva">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" Width="50px" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButtonIktatva" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                        AlternateText="Irat megtekint�se" OnClientClick="return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label_KuldemenyId" runat="server" Text='<%#Eval("KuldKuldemeny_Id") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label_IratId" runat="server" Text='<%#Eval("IraIrat_Id") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="GridViewInvisibleColumnStyle" ItemStyle-CssClass="GridViewInvisibleColumnStyle">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbIsIktathatoKuldemeny" runat="server" Text='<%#Eval("KuldKuldemeny_Id") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                <HeaderTemplate>
                                                    <asp:Image ID="imgLockedHeader" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                         <PagerSettings Visible="False" />
                                     </asp:GridView>
                                </td>
                                 </tr>
                             </table>
                          </asp:Panel>   
                      </ContentTemplate>
                </asp:UpdatePanel>
           </td>                            
          </tr>
          <tr>
            <td style="text-align: left;" colspan="2">
            <div style="height:8px"></div>
            </td>
          </tr>
          <!--Allist�k-->
          <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
            <asp:ImageButton ImageUrl="images/hu/Grid/minus.gif" runat="server" ID="btnCpeDetail" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;"> 
            <table width="100%" cellpadding="0" cellspacing="0">   
            <tr>
                <td style="text-align: left" colspan="2">
                <ajaxToolkit:CollapsiblePanelExtender ID="cpeDetail" runat="server" TargetControlID="panelDetail"
                 CollapsedSize="20" Collapsed="False" ExpandControlID="btnCpeDetail" CollapseControlID="btnCpeDetail" ExpandDirection="Vertical"
                 AutoCollapse="false" AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="btnCpeDetail">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="panelDetail" runat="server">
                    
                    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                        OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        <!--Csatolm�nyok lista-->
                        <ajaxToolkit:TabPanel ID="tabPanelEmailCsatolmanyok" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:Label ID="headerEmailCsatolmanyok" runat="server" Text="Csatolm�nyok"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelEmailCsatolmanyok" runat="server" OnLoad="updatePanelEmailCsatolmanyok_Load">
                                    <ContentTemplate>
                                        <asp:Panel ID="panelEmailCsatolmanyok" runat="server" Visible="false" Width="100%">
                                        <uc1:SubListHeader ID="EmailCsatolmanyokSubListHeader" runat="server" />
                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="cpeEmailCsatolmanyok" runat="server" TargetControlID="panelEmailCsatolmanyokList"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical"
                                            AutoCollapse="false" AutoExpand="false" ExpandedSize="0" >            
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="panelEmailCsatolmanyokList" runat="server">
                                            
                                                <asp:GridView ID="gridViewEmailCsatolmanyok" runat="server" AutoGenerateColumns="False" 
                                                     CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"     
                                                     OnPreRender="gridViewEmailCsatolmanyok_PreRender" OnRowCommand="gridViewEmailCsatolmanyok_RowCommand" DataKeyNames="Id" OnRowDataBound="gridViewEmailCsatolmanyok_RowDataBound"
                                                     OnSorting="gridViewEmailCsatolmanyok_Sorting">   
                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" HorizontalAlign="Left" />
                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"/>
                                                    <HeaderStyle CssClass="GridViewHeaderStyle"/>
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"  />
                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                            <HeaderTemplate>                                
                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>"  />
                                                                 &nbsp;&nbsp;
                                                                 <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />                            
                                                             </HeaderTemplate>
                                                             <ItemTemplate>
                                                                 <asp:CheckBox id="check" runat="server" Text='<%# Eval("FajlNev") %>' CssClass="HideCheckBoxText" />
                                                             </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle Width="25px" />
                                                             <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                             <ItemTemplate>
                                                                 <a href='javascript:void(0);' onclick="window.open('<%# Eval("External_Link") %>'); return false;"><img src="~/images/hu/Grid/3Drafts.gif" alt="" runat="server" id="megtekint" /></a>
                                                             </ItemTemplate>
                                                        </asp:TemplateField>                                                         
                                                         
<%--                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                            ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                            <HeaderStyle Width="25px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:CommandField> 
--%>                                                        <asp:TemplateField>
                                                             <HeaderTemplate>Megnevez�s</HeaderTemplate>
                                                             <ItemTemplate>
                                                                 <a href='javascript:void(0);' onclick="window.open('<%# Eval("External_Link") %>'); return false;"><%# Eval("FajlNev") %></a>
                                                             </ItemTemplate>
                                                        </asp:TemplateField>                                                         
<%--                                                        <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="200px"/>                                                                                                                        
                                                        </asp:BoundField>
--%>                                                     <%--   <asp:BoundField DataField="TipusNev" HeaderText="T�pus" SortExpression="TipusNev">
                                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px"/>
                                                        </asp:BoundField>--%>
                                                    </Columns>
                                                    <PagerSettings Visible="False" />   
                                                </asp:GridView>
                                              </asp:Panel>   
                                            </td>
                                          </tr>
                                        </table>                                          
                                     </asp:Panel>
                                  </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                  </asp:Panel> 
                </td>
            </tr>
        </table>
       </td>
      </tr>
   </table>
</asp:Content>

