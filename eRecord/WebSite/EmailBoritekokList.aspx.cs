using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using System.Collections.Generic;

public partial class EmailBoritekokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    public String eMailBoritekId = "";
    //public String sharepointurl = "http://srvcontentumweb/eRecord/EmailCsatolmanyok/";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "EmailBoritekokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        EmailCsatolmanyokSubListHeader.RowCount_Changed += new EventHandler(EmailCsatolmanyokSubListHeader_RowCount_Changed);

        EmailCsatolmanyokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(EmailCsatolmanyokSubListHeader_ErvenyessegFilter_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Keres�si objektum t�pus�nak megad�sa
        ListHeader1.SearchObjectType = typeof(EREC_eMailBoritekokSearch);

        //F� lista megjelen�sek testreszab�sa
        ListHeader1.HeaderLabel = Resources.List.EmailBoritekokListHeaderTitle;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.BejovoIratIktatasVisible = true;
        //ListHeader1.ErkeztetesVisible = true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        //F� lista gombokhoz kliens oldali szkriptek regisztr�l�sa
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("eMailBoritekokSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEmailBoritekok.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.NewOnClientClick = "";
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = "";
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(gridViewEmailBoritekok.ClientID,"", "check");

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(gridViewEmailBoritekok.ClientID);
        ListHeader1.HistoryOnClientClick = "";

        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.BejovoIratIktatas.ToolTip = Resources.Buttons.EmailErkeztetesIktatas;
        //ListHeader1.ErkeztetesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        
        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEmailBoritekok.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(gridViewEmailBoritekok.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(gridViewEmailBoritekok.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEmailBoritekok.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = gridViewEmailBoritekok;

        //F� lista �sszes kiv�laszt�sa �s az �sszes kiv�laszt�s megsz�ntet�se checkbox-aihoz szkriptek be�ll�t�sa
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEmailBoritekok);

        //F� lista esem�nykezel� f�ggv�nyei
        ListHeader1.LeftFunkcionButtonsClick += new
             System.Web.UI.WebControls.CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        //Allist�k gombjaihoz kliens oldali szkriptek regisztr�l�sa
        EmailCsatolmanyokSubListHeader.NewOnClientClick = "";
        EmailCsatolmanyokSubListHeader.ViewOnClientClick = "";
        EmailCsatolmanyokSubListHeader.ModifyOnClientClick = "";
        EmailCsatolmanyokSubListHeader.InvalidateOnClientClick = "";

        //selectedRecordId kezel�se
        EmailCsatolmanyokSubListHeader.AttachedGridView = gridViewEmailCsatolmanyok;

        //szkriptek regisztr�l�sa
        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);

        //TabContainer beregisztr�l�sa a ScriptManager-ben
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //aszinkron postback control beregisztr�l�sa
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        //Hiba�zenet panel el�ntet�se
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        /* Breaked Records, e-mail megtekint�s */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_eMailBoritekokSearch());

        //A f� lista adatk�t�s, ha nem postback a k�r�s
        if (!IsPostBack)
            EmailBoritekokGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //F� lista jogosults�gainak be�ll�t�sa
        ListHeader1.NewEnabled = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "EmailBoritek" + CommandName.View);
        ListHeader1.ModifyEnabled = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "EmailBoritek" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "EmailBoritek" + CommandName.ViewHistory);

        // erkeztetes
        //ListHeader1.ErkeztetesEnabled = FunctionRights.GetFunkcioJog(Page, "Kuldemeny" + CommandName.New);
        // Email �rkeztet�s/iktat�s:
        ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "EmailErkeztetes")
                                    || FunctionRights.GetFunkcioJog(Page, "EmailIktatas");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "EmailBoritek" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "EmailBoritek" + CommandName.Lock);

        //Allist�k l�that�s�ga jogosults�g alapj�n
        panelEmailCsatolmanyok.Visible = FunctionRights.GetFunkcioJog(Page, "EmailCsatolmanyokList");

        //Allist�k funkci�inak enged�lyez�se jogosults�gok alapj�n
        EmailCsatolmanyokSubListHeader.NewEnabled = false;
        EmailCsatolmanyokSubListHeader.NewVisible = false;
        EmailCsatolmanyokSubListHeader.ViewEnabled = false;
        EmailCsatolmanyokSubListHeader.ViewVisible = false;
        EmailCsatolmanyokSubListHeader.ModifyEnabled = false;
        EmailCsatolmanyokSubListHeader.ModifyVisible = false;
        EmailCsatolmanyokSubListHeader.InvalidateEnabled = false;
        EmailCsatolmanyokSubListHeader.InvalidateVisible = false;
        EmailCsatolmanyokSubListHeader.ValidFilterVisible = false;

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }

    #region Master List

    //adatk�t�s megh�v�sa default �rt�kekkel
    protected void EmailBoritekokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEmailBoritekok", ViewState, "EREC_eMailBoritekok.ErkezesDatuma");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEmailBoritekok", ViewState, SortDirection.Descending);

        EmailBoritekokGridViewBind(sortExpression, sortDirection);
    }

    //adatk�t�s webszolg�ltat�st�l kapott adatokkal
    protected void EmailBoritekokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_eMailBoritekokService service = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_eMailBoritekokSearch search = (EREC_eMailBoritekokSearch)Search.GetSearchObject(Page, new EREC_eMailBoritekokSearch());
        search.OrderBy = Search.GetOrderBy("gridViewEmailBoritekok", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);
        //Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(gridViewEmailBoritekok, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewEmailBoritekok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //�rkeztetve checkbox be�ll�t�sa a kuldem�ny id alapj�n
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton imageButtonErkeztetve = (ImageButton)e.Row.FindControl("imageButtonErkeztetve");
            ImageButton imageButtonIktatva = (ImageButton)e.Row.FindControl("ImageButtonIktatva");
            DataRowView drw = (DataRowView)e.Row.DataItem;
            
            #region �rkeztetve ikon be�ll�t�sa
            if (drw != null && drw["KuldKuldemeny_Id"] != null)
            {
                string kuldemenyId = drw["KuldKuldemeny_Id"].ToString();
                
                if (String.IsNullOrEmpty(kuldemenyId))
                {
                    imageButtonErkeztetve.Visible = false;
                    imageButtonErkeztetve.OnClientClick = String.Empty;
                }
                else
                {
                    string erkeztetoSzam = String.Empty;
                    if (drw["ErkeztetoSzam"] != null)
                    {
                        erkeztetoSzam = drw["ErkeztetoSzam"].ToString();
                    }

                    imageButtonErkeztetve.Visible = true;
                    imageButtonErkeztetve.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + kuldemenyId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    if (!String.IsNullOrEmpty(erkeztetoSzam))
                    {
                        // �rkeztet�sz�m megjelen�t�se a Tooltip-ben
                        imageButtonErkeztetve.ToolTip = erkeztetoSzam + " k�ldem�ny megtekint�se";
                        imageButtonErkeztetve.AlternateText = imageButtonErkeztetve.ToolTip;
                    }
                }
            }
            else
            {
                imageButtonErkeztetve.Visible = false;
                imageButtonErkeztetve.OnClientClick = String.Empty;
            }
            #endregion

            #region Iktatva ikon be�ll�t�sa

            if (drw != null && drw["IratIktatoszamEsId"] != null)
            {
                string IratIktatoszamEsId = drw["IratIktatoszamEsId"].ToString();

                if (String.IsNullOrEmpty(IratIktatoszamEsId))
                {
                    imageButtonIktatva.Visible = false;
                    imageButtonIktatva.OnClientClick = String.Empty;
                }
                else
                {
                    string iktatoszam = this.GetIktatoszamFrom_IratIktatoszamEsId(IratIktatoszamEsId);
                    string iratId = this.GetIdFrom_IratIktatoszamEsId(IratIktatoszamEsId);

                    imageButtonIktatva.Visible = true;
                    imageButtonIktatva.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx"
                    , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + iratId
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    if (!String.IsNullOrEmpty(iktatoszam))
                    {
                        // �rkeztet�sz�m megjelen�t�se a Tooltip-ben
                        imageButtonIktatva.ToolTip = iktatoszam + " irat megtekint�se";
                        imageButtonIktatva.AlternateText = imageButtonIktatva.ToolTip;
                    }
                }
            }
            else
            {
                imageButtonErkeztetve.Visible = false;
                imageButtonErkeztetve.OnClientClick = String.Empty;
            }

            #endregion

            #region cbIsIktathatoKuldemeny be�ll�t�sa (�rkeztetett, nem iktatott)
            CheckBox cbIsIktathatoKuldemeny = (CheckBox)e.Row.FindControl("cbIsIktathatoKuldemeny");
            if (cbIsIktathatoKuldemeny != null && imageButtonErkeztetve != null && imageButtonIktatva != null)
            {
                // �rkeztetett, nem iktatott:
                cbIsIktathatoKuldemeny.Checked = imageButtonErkeztetve.Visible && !imageButtonIktatva.Visible;
            }
            #endregion cbIsIktathatoKuldemeny be�ll�t�sa (�rkeztetett, nem iktatott)
        }
        //Lockol�s jelz�se
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    //GridView PreRender esem�nykezel�je
    protected void gridViewEmailBoritekok_PreRender(object sender, EventArgs e)
    {
        //select,deselect szkriptek
        ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEmailBoritekok);

        UI.GridViewSetScrollable(ListHeader1.Scrollable, cpeEmailBoritekok);
        ListHeader1.RefreshPagerLabel();

    }


    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        EmailBoritekokGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, cpeEmailBoritekok);
        EmailBoritekokGridViewBind();
    }


    //GridView RowCommand esem�nykezel�je, amit valamelyik sorban fell�pett parancs v�lt ki
    protected void gridViewEmailBoritekok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //sor kiv�laszt�sa (nem checkbox-al)
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEmailBoritekok, selectedRowNumber, "check");

            string id = gridViewEmailBoritekok.DataKeys[selectedRowNumber].Value.ToString();

            //Allist�k friss�t�se a kiv�lasztott sor f�ggv�ny�ben
            ActiveTabRefresh(TabContainer1, id);
        }

    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //F� lista kiv�lasztott elem eset�n �rv�nyes funkci�k regisztr�l�sa
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("EmailBoritekokForm.aspx"
                 , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEmailBoritekok.ClientID);
            ListHeader1.ModifyOnClientClick = "";
            string tableName = "EREC_eMailBoritekok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanelEmailBoritekok.ClientID);


            EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            ExecParam execparam_emailGet = UI.SetExecParamDefault(Page, new ExecParam());
            execparam_emailGet.Record_Id = id;

            Result result_emailGet = erec_eMailBoritekokService.Get(execparam_emailGet);
            if (!String.IsNullOrEmpty(result_emailGet.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_emailGet);
                ErrorUpdatePanel.Update();
                return;
            }

            EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)result_emailGet.Record;

            // ha lehet �rkeztetni:
            if (String.IsNullOrEmpty(erec_eMailBoritekok.KuldKuldemeny_Id) && String.IsNullOrEmpty(erec_eMailBoritekok.IraIrat_Id))
            {
                //ListHeader1.ErkeztetesOnClientClick = JavaScripts.SetOnClientClick("KuldKuldemenyekForm.aspx"
                //    , CommandName.Command + "=" + CommandName.New + "&" + QueryStringVars.EmailBoritekokId + "=" + id
                //    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, updatePanelEmailBoritekok.ClientID, EventArgumentConst.refreshMasterList);

                ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.EmailBoritekokId + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEmailBoritekok.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // alert('Az email �rkeztetett');
                //ListHeader1.ErkeztetesOnClientClick = "alert('" + Resources.Error.ErrorCode_52651 + "'); return false;";
                if (!String.IsNullOrEmpty(erec_eMailBoritekok.IraIrat_Id))
                {
                    ListHeader1.BejovoIratIktatasOnClientClick = "alert('Az email m�r iktatva van!'); return false;";
                }
                else
                {
                    if (gridViewEmailBoritekok.SelectedIndex > -1 && gridViewEmailBoritekok.SelectedIndex < gridViewEmailBoritekok.Rows.Count)
                    {
                        GridViewRow gvrow = gridViewEmailBoritekok.Rows[gridViewEmailBoritekok.SelectedIndex];

                        if (gvrow != null)
                        {
                            CheckBox cbIsIktathatoKuldemeny = (CheckBox)gvrow.FindControl("cbIsIktathatoKuldemeny");

                            if (cbIsIktathatoKuldemeny.Checked)
                            {

                                // Allapot szerint beallitja a headerben a gombok viselkedeset:
                                Kuldemenyek.Statusz statusz = Kuldemenyek.GetAllapotById(cbIsIktathatoKuldemeny.Text, Page, EErrorPanel1);

                                if (statusz == null)
                                {
                                    ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
                                }
                                else
                                {
                                    ExecParam execparam = UI.SetExecParamDefault(Page);
                                    ErrorDetails errorDetail = null;
                                    if (Kuldemenyek.Iktathato(execparam, statusz, out errorDetail))
                                    {
                                        ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("EgyszerusitettIktatasForm.aspx"
                                            , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.EmailBoritekokId + "=" + id
                                            + "&" + QueryStringVars.KuldemenyId + "=" + cbIsIktathatoKuldemeny.Text
                                            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, updatePanelEmailBoritekok.ClientID, EventArgumentConst.refreshMasterList);
                                    }
                                    else
                                    {
                                        ListHeader1.BejovoIratIktatasOnClientClick = "alert('" + Resources.Error.UINemIktathatoKuldemeny
                                            + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                                            + "'); return false;";
                                    }
                                }

                            }
                            else
                            {
                                ListHeader1.BejovoIratIktatasOnClientClick = "alert('Az email m�r iktatva van!'); return false;";
                            }

                        }
                    }
                    else
                    {
                        ListHeader1.BejovoIratIktatasOnClientClick = "alert('" + Resources.Error.ErrorCode_52651 + "'); return false;";
                    }
                }
            }

            //Allist�k �rv�nyes funkci�inak regisztr�l�sa
            EmailCsatolmanyokSubListHeader.NewOnClientClick = "";

            eMailBoritekId = id;

        }
    }

    //UpdatePanel Load esem�nykezel�je
    protected void updatePanelEmailBoritekok_Load(object sender, EventArgs e)
    {
        //F� lista friss�t�se
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    EmailBoritekokGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok));
                    break;
            }
        }
    }

    //F� lista bal oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //t�rl�s parancs
        if (e.CommandName == CommandName.Invalidate)
        {
            bool success = deleteSelectedEmailBoritek();
            if (success)
            {
                EmailBoritekokGridViewBind();
            }
        }
    }

    //kiv�lasztott elemek t�rl�se
    private bool deleteSelectedEmailBoritek()
    {        
        if (FunctionRights.GetFunkcioJog(Page, "EmailBoritekInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(gridViewEmailBoritekok, EErrorPanel1, ErrorUpdatePanel);

            int markedRows = MarkSelectedRowsWhichCannotInvalidate();

            if (markedRows > 0)
            {
                string errorMessage = (deletableItemsList.Count == 1) ? Resources.Error.UIEmailBoritekNemTorolheto
                    : Resources.Error.UIEmailBoritekokNemTorolhetok;
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.ErrorLabel, errorMessage);
                ErrorUpdatePanel.Update();
                return false;
            }

            EREC_eMailBoritekokService service = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return false;
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        return true;
    }

    private int MarkSelectedRowsWhichCannotInvalidate()
    {
        int markedRows = 0;
 
        foreach (GridViewRow row in gridViewEmailBoritekok.Rows)
        {
            CheckBox checkB = (CheckBox)row.FindControl("check");
            // ha ki van jel�lve a sor:
            if (checkB.Checked)
            {
                // �rkeztetve vagy iktatva van-e m�r?

                Label label_KuldemenyId = (Label)row.FindControl("Label_KuldemenyId");
                Label label_IratId = (Label)row.FindControl("Label_IratId");

                // ha m�r �rkeztetve vagy iktatva van:
                if ((label_KuldemenyId != null && !String.IsNullOrEmpty(label_KuldemenyId.Text))
                    || (label_IratId != null && !String.IsNullOrEmpty(label_IratId.Text)))
                {
                    markedRows++;

                    //row.Attributes["style"] += " background-color: Coral; ";
                    row.BackColor = System.Drawing.Color.Coral;
                }
                else
                {
                    row.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {
                // Csak hogy elt�nj�n a sz�nez�s a k�vetkez� pr�b�lkoz�skor:
                row.BackColor = System.Drawing.Color.White;
            }
        }

        return markedRows;
    }



    //F� lista jobb oldali funkci�gombjainak esem�nykezel�je
    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //lock,unlock,e-mail k�ld�s parancsok
        switch (e.CommandName)
        {
            case CommandName.Lock:
                lockSelectedEmailBoritekRecords();
                EmailBoritekokGridViewBind();
                break;

            case CommandName.Unlock:
                unlockSelectedEmailBoritekRecords();
                EmailBoritekokGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedEmailBoritekok();
                EmailBoritekokGridViewBind();
                break;
        }
    }

    //kiv�lasztott elemek z�rol�sa
    private void lockSelectedEmailBoritekRecords()
    {
        LockManager.LockSelectedGridViewRecords(gridViewEmailBoritekok, "EREC_eMailBoritekok"
            , "EmailBoritekLock", "EmailBoritekForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// kiv�lasztott emlemek elenged�se
    /// </summary>
    private void unlockSelectedEmailBoritekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(gridViewEmailBoritekok, "EREC_eMailBoritekok"
            , "EmailBoritekLock", "EmailBoritekForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Elkuldi emailben a gridViewEmailBoritekok -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedEmailBoritekok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(gridViewEmailBoritekok, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_EmailBoritekok");
        }
    }

    //GridView Sorting esm�nykezel�je
    protected void gridViewEmailBoritekok_Sorting(object sender, GridViewSortEventArgs e)
    {
        EmailBoritekokGridViewBind(e.SortExpression, UI.GetSortToGridView("gridViewEmailBoritekok", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion

    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok));
            ActiveTabRefreshOnClientClicks();
        }
    }

    /// <summary>
    /// TabContainer ActiveTabChanged esem�nykezel�je
    /// A kiv�lasztott record adataival friss�ti az �j panel-t
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (gridViewEmailBoritekok.SelectedIndex == -1)
        {
            return;
        }
        string masterId = UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, masterId);
    }

    /// <summary>
    /// Az akt�v panel friss�t�se a f� list�ban kiv�lasztott record adataival
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="masterId"></param>
    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string masterId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                JavaScripts.ResetScroll(Page, cpeEmailCsatolmanyok);
                EmailCsatolmanyokGridViewBind(masterId);
                panelEmailCsatolmanyok.Visible = true;
                break;
        }
    }

    /// <summary>
    /// Az akt�v panel tartalm�nak t�rl�se
    /// </summary>
    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(gridViewEmailCsatolmanyok);
                break;
        }
    }

    /// <summary>
    /// Az allist�k egy oldalon megjelen�tett sorok sz�m�nak be�ll�t�sa
    /// </summary>
    /// <param name="RowCount"></param>
    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        EmailCsatolmanyokSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(tabPanelEmailCsatolmanyok))
        {
            gridViewEmailCsatolmanyok_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(gridViewEmailCsatolmanyok));
        }
    }

    #endregion

    #region EmailCsatolmanyok Detail

    /// <summary>
    /// A EmailCsatolmanyok GridView adatk�t�se a EmailBoritekID f�ggv�ny�ben alap�rtelmezett
    /// rendez�si param�terekkel
    /// </summary>
    /// <param name="EmailBoritekId"></param>
    protected void EmailCsatolmanyokGridViewBind(String EmailBoritekId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEmailCsatolmanyok", ViewState, "FajlNev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEmailCsatolmanyok", ViewState);

        EmailCsatolmanyokGridViewBind(EmailBoritekId, sortExpression, sortDirection);
    }

    /// <summary>
    /// A EmailCsatolmanyok GridView adatk�t�se a EmailBoritekID �s rendez�si param�terek f�ggv�ny�ben
    /// </summary>
    /// <param name="EmailBoritekId"></param>
    /// <param name="SortExpression"></param>
    /// <param name="SortDirection"></param>
    protected void EmailCsatolmanyokGridViewBind(String EmailBoritekId, String SortExpression, SortDirection SortDirection)
    {

        if (!String.IsNullOrEmpty(EmailBoritekId))
        {
            EREC_eMailBoritekCsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_eMailBoritekCsatolmanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_eMailBoritekCsatolmanyokSearch _EREC_EmailCsatolmanyokSearch = (EREC_eMailBoritekCsatolmanyokSearch)Search.GetSearchObject(Page, new EREC_eMailBoritekCsatolmanyokSearch());
            _EREC_EmailCsatolmanyokSearch.OrderBy = Search.GetOrderBy("gridViewEmailCsatolmanyok", ViewState, SortExpression, SortDirection);
            _EREC_EmailCsatolmanyokSearch.eMailBoritek_Id.Value = EmailBoritekId;
            _EREC_EmailCsatolmanyokSearch.eMailBoritek_Id.Operator = Contentum.eQuery.Query.Operators.equals;
            
            //EmailCsatolmanyokSubListHeader.SetErvenyessegFields(_EREC_EmailCsatolmanyokSearch.ErvKezd, _EREC_EmailCsatolmanyokSearch.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, _EREC_EmailCsatolmanyokSearch);
            UI.GridViewFill(gridViewEmailCsatolmanyok, res, EmailCsatolmanyokSubListHeader, EErrorPanel1, ErrorUpdatePanel);

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridViewEmailCsatolmanyok);
        }
        else
        {
            ui.GridViewClear(gridViewEmailCsatolmanyok);
        }
    }


    void EmailCsatolmanyokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        EmailCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok));
    }

    /// <summary>
    /// EmailCsatolmanyok updatepanel Load esem�nykezel�je
    /// EmailCsatolmanyok panel friss�t�se, amennyiben akt�v
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void updatePanelEmailCsatolmanyok_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(tabPanelEmailCsatolmanyok))
                    //{
                    //    EmailCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok));
                    //}
                    ActiveTabRefreshDetailList(tabPanelEmailCsatolmanyok);
                    break;
            }
        }
    }

    /// <summary>
    /// EmailCsatolmanyok GridView RowCommand esem�nylezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewEmailCsatolmanyok_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEmailCsatolmanyok, selectedRowNumber, "check");
        }
    }

    //A GridView RowDataBound esem�ny kezel�je, ami egy sor adatk�t�se ut�n h�v�dik
    protected void gridViewEmailCsatolmanyok_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    /// <summary>
    /// A EmailCsatolmanyok lista kiv�lasztott sor�ra �rv�nye funkci�gombok be�ll�t�sa
    /// </summary>
    /// <param name="selectedId"></param>
    private void gridViewEmailCsatolmanyok_RefreshOnClientClicks(string selectedId)
    {
        string id = selectedId;
        if (!String.IsNullOrEmpty(id))
        {
            
            /*String sharepointdirectoy = id;
            EmailCsatolmanyokSubListHeader.ViewPostBackUrl = sharepointurl + sharepointdirectoy + "/" + gridViewEmailCsatolmanyok.SelectedRow.Cells[2].Text;
            EmailCsatolmanyokSubListHeader.ViewOnClientClick = "return DispEx(this,event,'TRUE','FALSE','FALSE','SharePoint.OpenDocuments','0','SharePoint.OpenDocuments','','','14','14','0','0','0x7fffffffffffffff')";
                
                //JavaScripts.SetOnClientClick(sharepointurl + sharepointdirectoy + "/" + gridViewEmailCsatolmanyok.SelectedRow.Cells[2].Text
                //, CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                //, Defaults.PopupWidth, Defaults.PopupHeight, updatePanelEmailCsatolmanyok.ClientID, EventArgumentConst.refreshDetailList);
            EmailCsatolmanyokSubListHeader.ModifyOnClientClick = "";
             */
        }
    }

    /// <summary>
    /// EmailCsatolmanyok GridView PreRender esem�nykezel�je
    /// Lapoz�s kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewEmailCsatolmanyok_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = gridViewEmailCsatolmanyok.PageIndex;
        gridViewEmailCsatolmanyok.PageIndex = EmailCsatolmanyokSubListHeader.PageIndex;

        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != gridViewEmailCsatolmanyok.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, cpeEmailCsatolmanyok);
            EmailCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok));
        }
        else
        {
            UI.GridViewSetScrollable(EmailCsatolmanyokSubListHeader.Scrollable, cpeEmailCsatolmanyok);
        }

        EmailCsatolmanyokSubListHeader.PageCount = gridViewEmailCsatolmanyok.PageCount;
        EmailCsatolmanyokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(gridViewEmailCsatolmanyok);
    }

    void EmailCsatolmanyokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(EmailCsatolmanyokSubListHeader.RowCount);
        EmailCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok));
    }

    /// <summary>
    /// EmailCsatolmanyok GridView Sorting esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridViewEmailCsatolmanyok_Sorting(object sender, GridViewSortEventArgs e)
    {
        EmailCsatolmanyokGridViewBind(UI.GetGridViewSelectedRecordId(gridViewEmailBoritekok)
            , e.SortExpression, UI.GetSortToGridView("gridViewEmailCsatolmanyok", ViewState, e.SortExpression));
    }

    #endregion


    #region segedFv

    // Segedfv.

    private const string IratIktatoszamEsId_Delimitter = "***";

    protected string GetIktatoszamFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            return strParts[0];
        }
        else
        {
            return String.Empty;
        }
    }

    protected string GetIdFrom_IratIktatoszamEsId(string IratIktatoszamEsId)
    {
        if (!string.IsNullOrEmpty(IratIktatoszamEsId))
        {
            string[] strParts = IratIktatoszamEsId.Split(new string[1] { IratIktatoszamEsId_Delimitter }, StringSplitOptions.RemoveEmptyEntries);
            if (strParts.Length > 1)
            {
                return strParts[1];
            }
            else
            {
                return String.Empty;
            }
        }
        else
        {
            return String.Empty;
        }
    }

    #endregion

}
