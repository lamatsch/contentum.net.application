<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="EmailBoritekokSearch.aspx.cs" Inherits="EmailBoritekokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc3" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc6" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallumSearch" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,EmailBoritekokSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapSpacer">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFelado" runat="server" Text="Felad�:"></asp:Label></td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textFelado"  CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                 <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelCimzett" runat="server" Text="C�mzett:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textCimzett"  CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelCC" runat="server" Text="M�solatot kap:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textCC"  CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelTargy" runat="server" Text="T�rgy:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textTargy"  CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFeladasDatuma" runat="server" Text="Felad�s d�tuma:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:DatumIntervallumSearch ID="DatumIntervallumFeladas" runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallumSearch>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                         <asp:Label ID="labelErkezesDatuma" runat="server" Text="�rkez�s d�tuma:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:DatumIntervallumSearch ID="DatumIntervallumErkezes" runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallumSearch>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFeldolgozasiIdo" runat="server" Text="Feldolgoz�si id�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc7:DatumIntervallumSearch ID="DatumIntervallumFeldolgozas" runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallumSearch>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFontossag" runat="server" Text="Fontoss�g:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:KodtarakDropDownList ID="KodtarakDropDownListFontossag" CssClass="mrUrlapInputComboBox"  runat="server" />
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelUzenet" runat="server" Text="�zenet:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="textUzenet"  CssClass="mrUrlapInput" runat="server" ></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                            <asp:Label ID="labelErkeztetve" runat="server" Text="�rkeztetve:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:RadioButtonList ID="rbListErkeztetve" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                        </asp:RadioButtonList>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIktatva" runat="server" Text="Iktatva:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:RadioButtonList ID="rbListIktatva" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                        </asp:RadioButtonList>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                     <uc5:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                    </uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="5">
                                    </td>
                                </tr>   
                                <tr class="urlapSor">
                                     <td colspan="5">
                                     <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                                        runat="server">
                                      </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

