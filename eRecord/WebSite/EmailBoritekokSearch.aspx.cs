using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;

public partial class EmailBoritekokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_eMailBoritekokSearch);

    private const string kodcsoportFontossag = "EMAILBORITEK_FONTOSSAG";

    private void setEqualsOperator(string value, Field field)
    {
        if (!String.IsNullOrEmpty(value))
        {
            field.Value = value;
            field.Operator = Query.Operators.equals;
        }
        else
        {
            field.Value = "";
            field.Operator = "";
        }
    }

    public static class BoolString
    {
        public static readonly ListItem Yes = new ListItem("Igen", "I");
        public static readonly ListItem No = new ListItem("Nem", "N");
        public static readonly ListItem NotSet = new ListItem("�sszes", "X");
        public static void FillRadioList(RadioButtonList list)
        {
            list.Items.Clear();
            //list.Items.Add(Yes);
            //list.Items.Add(No);
            //list.Items.Add(NotSet);

            //// egy�bk�nt az �sszes radiolistben azonosan jelenik meg a kijel�lt �rt�k
            list.Items.Add(new ListItem(Yes.Text, Yes.Value));
            list.Items.Add(new ListItem(No.Text, No.Value));
            list.Items.Add(new ListItem(NotSet.Text, NotSet.Value));
            list.SelectedValue = NotSet.Value;
        }

        public static string GetFromSearchField(Field field)
        {
            if (String.IsNullOrEmpty(field.Operator))
            {
                return NotSet.Value;
            }
            else
            {
                if (field.Operator == Query.Operators.isnull)
                {
                    return No.Value;
                }
                if (field.Operator == Query.Operators.notnull)
                {
                    return Yes.Value;
                }
                return NotSet.Value;
            }
        }
        public static void SetSearchField(RadioButtonList list, Field field)
        {
            if (list.SelectedValue == Yes.Value)
            {
                field.Operator = Query.Operators.notnull;
            }
            else
            {
                if (list.SelectedValue == No.Value)
                {
                    field.Operator = Query.Operators.isnull;
                }
                else
                {
                    field.Operator = String.Empty;
                }
            }
        }
    }


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

        SearchHeader1.TemplateObjectType = _type;

    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            EREC_eMailBoritekokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_eMailBoritekokSearch)Search.GetSearchObject(Page, new EREC_eMailBoritekokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            BoolString.FillRadioList(rbListErkeztetve);
            BoolString.FillRadioList(rbListIktatva);
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch = (EREC_eMailBoritekokSearch)searchObject;

        if (_EREC_eMailBoritekokSearch != null)
        {
            textFelado.Text = _EREC_eMailBoritekokSearch.Felado.Value;
            textCimzett.Text = _EREC_eMailBoritekokSearch.Cimzett.Value;
            textCC.Text = _EREC_eMailBoritekokSearch.CC.Value;
            textTargy.Text = _EREC_eMailBoritekokSearch.Targy.Value;
            DatumIntervallumFeladas.SetComponentFromSearchObjectFields(_EREC_eMailBoritekokSearch.FeladasDatuma);
            DatumIntervallumErkezes.SetComponentFromSearchObjectFields(_EREC_eMailBoritekokSearch.ErkezesDatuma);
            DatumIntervallumFeldolgozas.SetComponentFromSearchObjectFields(_EREC_eMailBoritekokSearch.FeldolgozasIdo);
            KodtarakDropDownListFontossag.FillAndSetSelectedValue(kodcsoportFontossag, _EREC_eMailBoritekokSearch.Fontossag.Value, true, SearchHeader1.ErrorPanel);
            textUzenet.Text = _EREC_eMailBoritekokSearch.Uzenet.Value;
            rbListErkeztetve.SelectedValue= BoolString.GetFromSearchField(_EREC_eMailBoritekokSearch.KuldKuldemeny_Id);
            rbListIktatva.SelectedValue = BoolString.GetFromSearchField(_EREC_eMailBoritekokSearch.Manual_IraIrat_Id);

            Ervenyesseg_SearchFormComponent1.SetDefault(
               _EREC_eMailBoritekokSearch.ErvKezd, _EREC_eMailBoritekokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private EREC_eMailBoritekokSearch SetSearchObjectFromComponents()
    {
        EREC_eMailBoritekokSearch _EREC_eMailBoritekokSearch = (EREC_eMailBoritekokSearch)SearchHeader1.TemplateObject;

        if (_EREC_eMailBoritekokSearch == null)
        {
            _EREC_eMailBoritekokSearch = new EREC_eMailBoritekokSearch();
        }

        _EREC_eMailBoritekokSearch.Felado.Value = textFelado.Text;
        _EREC_eMailBoritekokSearch.Felado.Operator = Search.GetOperatorByLikeCharater(textFelado.Text);
        _EREC_eMailBoritekokSearch.Cimzett.Value = textCimzett.Text;
        _EREC_eMailBoritekokSearch.Cimzett.Operator = Search.GetOperatorByLikeCharater(textCimzett.Text);
        _EREC_eMailBoritekokSearch.CC.Value = textCC.Text;
        _EREC_eMailBoritekokSearch.CC.Operator = Search.GetOperatorByLikeCharater(textCC.Text);
        _EREC_eMailBoritekokSearch.Targy.Value = textTargy.Text;
        _EREC_eMailBoritekokSearch.Targy.Operator = Search.GetOperatorByLikeCharater(textTargy.Text);
        setEqualsOperator(KodtarakDropDownListFontossag.SelectedValue, _EREC_eMailBoritekokSearch.Fontossag);
        DatumIntervallumFeladas.SetSearchObjectFields(_EREC_eMailBoritekokSearch.FeladasDatuma);
        DatumIntervallumErkezes.SetSearchObjectFields(_EREC_eMailBoritekokSearch.ErkezesDatuma);
        DatumIntervallumFeldolgozas.SetSearchObjectFields(_EREC_eMailBoritekokSearch.FeldolgozasIdo);
        _EREC_eMailBoritekokSearch.Uzenet.Value = textUzenet.Text;
        _EREC_eMailBoritekokSearch.Uzenet.Operator = Search.GetOperatorByLikeCharater(textUzenet.Text);
        BoolString.SetSearchField(rbListErkeztetve, _EREC_eMailBoritekokSearch.KuldKuldemeny_Id);
        BoolString.SetSearchField(rbListIktatva, _EREC_eMailBoritekokSearch.Manual_IraIrat_Id);

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _EREC_eMailBoritekokSearch.ErvKezd, _EREC_eMailBoritekokSearch.ErvVege);

        return _EREC_eMailBoritekokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_eMailBoritekokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private EREC_eMailBoritekokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_eMailBoritekokSearch();
    }
}
