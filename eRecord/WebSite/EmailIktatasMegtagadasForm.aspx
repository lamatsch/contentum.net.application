<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="EmailIktatasMegtagadasForm.aspx.cs" Inherits="EmailIktatasMegtagadasForm"
    Title="Email iktat�s�nak megtagad�sa" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc7" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc2" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc4" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 50px;
            min-width: 50px;
        }

        .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
            width: 230px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <%--Hiba megjelenites--%>
    <%-- FormHeader panelj�t haszn�ljuk
        <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
        </asp:UpdatePanel>
    --%>
    <%--/Hiba megjelenites--%>
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="KuldemenyPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label12" runat="server" Text="�rkeztet�si azonos�t�:"></asp:Label></td>
                            <td class="mrUrlapMezo" width="0%" colspan="3" style="color: red;">
                                <asp:TextBox ID="ErkeztetoSzam_TextBox" runat="server" CssClass="mrUrlapInputRed"
                                    ReadOnly="True"></asp:TextBox></td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label102" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="HivatkozasiSzam_Kuldemeny_TextBox" runat="server" CssClass="mrUrlapInput"
                                    ReadOnly="True"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label22" runat="server" Text="K�ld�/felad� neve:"></asp:Label></td>
                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                <uc2:PartnerTextBox ID="Bekuldo_PartnerTextBox" runat="server" ViewMode="true" ReadOnly="true" Validate="false"></uc2:PartnerTextBox>
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label3" runat="server" Text="K�ld�/felad� c�me:"></asp:Label></td>
                            <td class="mrUrlapMezo" width="0%">
                                <uc3:CimekTextBox ID="Kuld_CimId_CimekTextBox" runat="server" ReadOnly="true" ViewMode="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label122" runat="server" Text="C�mzett:"></asp:Label></td>
                            <td class="mrUrlapMezo" width="0%" colspan="3">
                                <uc5:CsoportTextBox ID="CsoportId_CimzettCsoportTextBox" runat="server" ViewMode="true"
                                    ReadOnly="true" Validate="false"></uc5:CsoportTextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label82" runat="server" Text="Felad�s id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezo" colspan="3" width="0%">
                                <cc:CalendarControl ID="FeladasDatuma_CalendarControl" runat="server"
                                    TimeVisible="true" ReadOnly="true" Validate="false"></cc:CalendarControl>
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label1" runat="server" Text="�rkeztet�s id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezo" width="0%">
                                <cc:CalendarControl ID="ErkezesDatuma_CalendarControl" runat="server"
                                    TimeVisible="true" ReadOnly="true" Validate="false"></cc:CalendarControl>
                            </td>

                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelCaptionTargy" runat="server" Text="T�rgy:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="6">
                                <asp:TextBox ID="TextBoxTargy" runat="server" CssClass="mrUrlapInputFull" ReadOnly="true" />
                            </td>
                        </tr>

                    </table>
                </eUI:eFormPanel>

                <eUI:eFormPanel ID="MegtagadasPanel" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short" style="vertical-align: top;">
                                <asp:Label ID="Label55" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label30" runat="server" Text="Megtagad�s indoka:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:RequiredTextBox ID="requiredTextBoxMegtagadasIndoka" Width="420px" runat="server"
                                    Rows="10" TextBoxMode="MultiLine" MaxLength="4000" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label19" runat="server" Text="Megtagad�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:FelhasznaloTextBox ID="MegtagadoId_FelhasznaloTextBox" runat="server" ViewEnabled="true"
                                    ReadOnly="true" Validate="false"></uc4:FelhasznaloTextBox>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>

                <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false">
                    <div class="mrResultPanelText">A k�ldem�ny iktat�s�nak megtagad�sa sikeres volt.</div>

                    <table cellspacing="0" cellpadding="0" width="700px" class="mrResultTableErkeztetes">
                        <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                            <td class="mrUrlapCaptionBigSize">
                                <asp:Label ID="labelKuldemeny" runat="server" Text="�rkeztet�si azonos�t�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoBigSize">
                                <asp:Label ID="labelKuldemenyErkSzam" runat="server" Text=""></asp:Label>
                            </td>
                            <td class="mrUrlapCaption" style="padding-top: 5px">
                                <asp:ImageButton ID="imgKuldemenyMegtekint" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                    onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');" onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" />
                            </td>
                            <%-- 
                         <td class="mrUrlapMezo" style="padding-top:5px">
                            <span style="padding-right:80px">
                                <asp:ImageButton ID="imgKuldemenyModosit" runat="server" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg" 
                                 onmouseover="swapByName(this.id,'modositas_trap2.jpg');" onmouseout="swapByName(this.id,'modositas_trap.jpg');"/>
                            </span>
                         </td>
                            --%>
                        </tr>
                        <tr class="urlapSorBigSize" style="padding-bottom: 20px;">
                            <td class="mrUrlapCaptionBigSize">
                                <asp:Label ID="labelErtesitesSikeresCaption" runat="server" Text="K�ld� �rtes�t�se:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoBigSize">
                                <asp:Label ID="labelErtesitesSikeres" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>


                        <tr id="tr_AnswerEmail" runat="server" class="urlapSorBigSize" visible="false" style="padding-bottom: 20px;">
                            <td colspan="4">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td class="listaFulFelsoCsikKicsi" colspan="2"></td>
                                        </tr>
                                        <tr class="urlapElvalasztoSor">
                                            <td class="mrUrlapCaption_short"></td>
                                            <td class="mrUrlapMezo"></td>
                                        </tr>
                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelFelado" runat="server" Text="Felad�:"></asp:Label></td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="textFelado" CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelCimzett" runat="server" Text="C�mzett:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="textCimzett" CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelTargy" runat="server" Text="T�rgy:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:TextBox ID="textTargy" CssClass="mrUrlapInputFull" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr class="urlapSor_kicsi">
                                            <td class="mrUrlapCaption_short">
                                                <asp:Label ID="labelUzenet" runat="server" Text="�zenet:"></asp:Label>
                                            </td>
                                            <td class="mrUrlapMezo">
                                                <asp:Panel runat="server" ID="paneluzenet" CssClass="mrUrlapInputFullComboBox">
                                                    <div runat="server" id="divUzenet" style="overflow: auto; height: 200px; color: Black; border: solid 1px #9d9da1; width: 100%">
                                                        <asp:Label ID="labelUzenetContent" runat="server"></asp:Label>
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
