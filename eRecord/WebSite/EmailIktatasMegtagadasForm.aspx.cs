using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class EmailIktatasMegtagadasForm : System.Web.UI.Page
{
    private string msgMessageSent_Success = "elk�ldve";
    private string msgMessageSent_Failed = "sikertelen";
    private string Command = "";
    private PageView pageView = null;
    private String KuldemenyId = "";

    private Type _type = typeof(EREC_KuldKuldemenyek);  // template-hez

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BejovoIratIktatas");
        KuldemenyId = Request.QueryString.Get(QueryStringVars.KuldemenyId);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooterButtonsClick);
        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshKuldemenyekPanel + "'; window.close(); return false;";

        FormHeader1.TemplateObjectType = _type;

        if (!IsPostBack)
        {
            LoadFormKuldemenyComponents();
            LoadFormEmailBoritekComponents();
        }

    }
    
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(KuldemenyId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else
        {
            if (!IsPostBack)
            {
                FormFooter1.ImageButton_Save.Visible = true;
            }
        }
        FormFooter1.ImageButton_Close.Visible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.FullManualHeaderTitle = Resources.Form.EmailRegistrationDeclinedFormFullManualHeaderTitle;
        JavaScripts.RegisterPopupWindowClientScript(Page);
    }

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls_AnswerEmail()
    {
        textFelado.ReadOnly = true;
        textCimzett.ReadOnly = true;
        textTargy.ReadOnly = true;

        //labelFelado.CssClass = "mrUrlapInputWaterMarked";
        //labelCimzett.CssClass = "mrUrlapInputWaterMarked";
        //labelTargy.CssClass = "mrUrlapInputWaterMarked";
        //labelUzenet.CssClass = "mrUrlapInputWaterMarked";

    }

    private EREC_eMailBoritekok GetEmailBoritekIdByKuldemenyId(string kuldemenyId)
    {
        if (!String.IsNullOrEmpty(kuldemenyId))
        {
            EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            // eMail bor�t�k megkeres�se
            EREC_eMailBoritekokSearch search = new EREC_eMailBoritekokSearch();
            search.KuldKuldemeny_Id.Value = kuldemenyId;
            search.KuldKuldemeny_Id.Operator = Query.Operators.equals;

            Result result_eMailBoritekok = erec_eMailBoritekokService.GetAll(execparam, search);
            if (!String.IsNullOrEmpty(result_eMailBoritekok.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_eMailBoritekok);
                return null;
            }

            if (result_eMailBoritekok.Ds.Tables[0].Rows.Count > 0)
            {
                // elvileg pontosan 1 tal�lat van
                execparam.Record_Id = result_eMailBoritekok.Ds.Tables[0].Rows[0]["Id"].ToString();
                Result _ret = erec_eMailBoritekokService.Get(execparam);

                if (!String.IsNullOrEmpty(_ret.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, _ret);
                    return null;
                }
                EREC_eMailBoritekok erec_eMailBoritekok = (EREC_eMailBoritekok)_ret.Record;
                return erec_eMailBoritekok;
            }
        }
        return null;
    }

    //business object --> form
    private void LoadFormKuldemenyComponents()
    {
        if (!String.IsNullOrEmpty(KuldemenyId))
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = KuldemenyId;
            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_KuldKuldemenyek erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result.Record;

                if (erec_KuldKuldemenyek != null)
                {
                    Kuldemenyek.Statusz kuldemenyStatusz =
                            Kuldemenyek.GetAllapotByBusinessDocument(erec_KuldKuldemenyek);


                    // nem megtagadhat� k�ldem�ny
                    if (Kuldemenyek.IktatasMegtagadhato(execParam, kuldemenyStatusz) == false)
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UIIktatasNemTagadhatoMegKuldemeny);
                        FormFooter1.ImageButton_Save.Visible = false;
                        return;
                    }
                    else
                    {
                        // nem volt hiba
                        FormFooter1.ImageButton_Save.Visible = true;
                    }

                    ErkeztetoSzam_TextBox.Text = erec_KuldKuldemenyek.Azonosito;

                    HivatkozasiSzam_Kuldemeny_TextBox.Text = erec_KuldKuldemenyek.HivatkozasiSzam;

                    MegtagadoId_FelhasznaloTextBox.Id_HiddenField = execParam.Felhasznalo_Id;
                    MegtagadoId_FelhasznaloTextBox.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);
                    MegtagadoId_FelhasznaloTextBox.NewImageButtonVisible = false;
                    MegtagadoId_FelhasznaloTextBox.LovImageButtonVisible = false;
                    MegtagadoId_FelhasznaloTextBox.ResetImageButtonVisible = false;

                    FormHeader1.Record_Ver = erec_KuldKuldemenyek.Base.Ver;
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                FormFooter1.ImageButton_Save.Visible = false;
            }
        }
    }

    private void LoadFormEmailBoritekComponents()
    {
        if (!String.IsNullOrEmpty(KuldemenyId))
        {
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            // eMail bor�t�k megkeres�se
            EREC_eMailBoritekok erec_eMailBoritekok = GetEmailBoritekIdByKuldemenyId(KuldemenyId);
            if (erec_eMailBoritekok != null)
            {
                Contentum.eAdmin.Service.KRT_FelhasznalokService fhservice = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();

                KRT_Felhasznalok krt_Felhasznalok = new KRT_Felhasznalok();

                Result fhresult = fhservice.GetByEmail(execparam, erec_eMailBoritekok.Felado);
                if (String.IsNullOrEmpty(fhresult.ErrorCode))
                {
                    krt_Felhasznalok = (KRT_Felhasznalok)fhresult.Record;
                }
                else
                {
                    if(fhresult.ErrorCode != "55885") //Dont show the error if sender mail address is not found in KRT_Felhasznalok
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, fhresult);
                    }
                }

                Bekuldo_PartnerTextBox.Text = krt_Felhasznalok.Nev;//eMailBoritekok.Felado;
                Bekuldo_PartnerTextBox.Id_HiddenField = krt_Felhasznalok.Id;

                Kuld_CimId_CimekTextBox.Text = erec_eMailBoritekok.Felado;
                // cimzett
                CsoportId_CimzettCsoportTextBox.Text = erec_eMailBoritekok.Cimzett;

                TextBoxTargy.Text = erec_eMailBoritekok.Targy;
                FeladasDatuma_CalendarControl.Text = erec_eMailBoritekok.FeladasDatuma;
                ErkezesDatuma_CalendarControl.Text = erec_eMailBoritekok.ErkezesDatuma;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.UINemEmailKuldemeny);
            }
        }
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="EREC_eMailBoritekok"></param>
    private void LoadComponentsFromBusinessObject_AnswerMail(EREC_eMailBoritekok erec_eMailBoritekok)
    {
        if (erec_eMailBoritekok != null)
        {
            textFelado.Text = erec_eMailBoritekok.Felado;
            textCimzett.Text = erec_eMailBoritekok.Cimzett;
            textTargy.Text = erec_eMailBoritekok.Targy;
            labelUzenetContent.Text = erec_eMailBoritekok.Uzenet;
        }
    }

    // form --> business object
    private EREC_KuldKuldemenyek GetFormKuldemenyComponents()
    {
        EREC_KuldKuldemenyek erec_KuldKuldemenyek = null;

        if (!String.IsNullOrEmpty(KuldemenyId))
        {
            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = KuldemenyId;
            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                erec_KuldKuldemenyek = (EREC_KuldKuldemenyek)result.Record;

                if (erec_KuldKuldemenyek != null)
                {
                    erec_KuldKuldemenyek.Updated.SetValueAll(false);
                    erec_KuldKuldemenyek.Base.Updated.SetValueAll(false);

                    erec_KuldKuldemenyek.Id = KuldemenyId;
                    erec_KuldKuldemenyek.Updated.Id = true;

                    // az elutas�t�s miatt az �llapot megv�ltozik
                    erec_KuldKuldemenyek.Allapot = KodTarak.KULDEMENY_ALLAPOT.Iktatas_Megtagadva; // Iktat�s megtagadva
                    erec_KuldKuldemenyek.Updated.Allapot = true;
                    // r�gz�tj�k az iktat�s elutas�t�s�nak ok�t, idej�t, az elutas�t�t
                    erec_KuldKuldemenyek.MegtagadasIndoka = requiredTextBoxMegtagadasIndoka.Text.Replace("\r\n","<br/>");
                    erec_KuldKuldemenyek.Updated.MegtagadasIndoka = pageView.GetUpdatedByView(requiredTextBoxMegtagadasIndoka);
                    erec_KuldKuldemenyek.Megtagado_Id = Contentum.eUtility.FelhasznaloProfil.FelhasznaloId(Page);
                    erec_KuldKuldemenyek.Updated.Megtagado_Id = true;
                    erec_KuldKuldemenyek.MegtagadasDat = System.DateTime.Now.ToString();
                    erec_KuldKuldemenyek.Updated.MegtagadasDat = true;

                    erec_KuldKuldemenyek.Base.Ver = FormHeader1.Record_Ver;
                    erec_KuldKuldemenyek.Base.Updated.Ver = true;

                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                FormFooter1.ImageButton_Save.Visible = false;
            }
        }

        return erec_KuldKuldemenyek;

    }

    protected void FormFooterButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            // rekord bet�lt�se

            EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();

            EREC_KuldKuldemenyek erec_KuldKuldemenyek = GetFormKuldemenyComponents();
            if (erec_KuldKuldemenyek != null)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = KuldemenyId;

                Result result = service.Update(execParam, erec_KuldKuldemenyek);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_eMailBoritekok erec_eMailBoritekok = GetEmailBoritekIdByKuldemenyId(KuldemenyId);
                    if (erec_eMailBoritekok != null)
                    {
                        Result result_Notify = Notify.SendAnswerDeclinedEmail(execParam, erec_eMailBoritekok, ErkeztetoSzam_TextBox.Text, erec_KuldKuldemenyek.MegtagadasIndoka);
                        if (!String.IsNullOrEmpty(result_Notify.ErrorCode))
                        {
                            // sikertelen �zenetk�ld�s
                            SetResultPanel(KuldemenyId, false);
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_Notify);
                        }
                        else
                        {
                            EREC_eMailBoritekok erec_EmailBoritekokAnswer = (EREC_eMailBoritekok)result_Notify.Record;
                            SetResultPanel(KuldemenyId, true);
                            SetAnwerEmailData(erec_EmailBoritekokAnswer);
                        }
                        FormFooter1.ImageButton_Save.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshKuldemenyekPanel + "'; window.close(); return true;";
                    }
                    else
                    {
                        // sikertelen �zenetk�ld�s
                        SetResultPanel(KuldemenyId, false);
                    }
                    
                }
                else
                {
                    // sikertelen k�ldem�ny Update
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
            else
            {
                //// k�ldem�ny nem tal�lhat�
                //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
        else if (e.CommandName == CommandName.Cancel)
        {
        
        }
        else if (e.CommandName == CommandName.Back)
        {
        
        }
    }

    private void SetResultPanel(string kuldemenyID, bool isErtesitesSikeres)
    {
        if (String.IsNullOrEmpty(kuldemenyID)) return;

        //Panel-ek be�ll�t�sa
        KuldemenyPanel.Visible = false;
        MegtagadasPanel.Visible = false;
        ResultPanel.Visible = true;

        FormHeader1.ErrorPanel.Visible = false;

        labelKuldemenyErkSzam.Text = ErkeztetoSzam_TextBox.Text;
        FormFooter1.ImageButton_Save.Visible = false;

        //M�dos�t�s,megtekint�s gombok be�ll�t�sa
        imgKuldemenyMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                        "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        //imgKuldemenyModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("KuldKuldemenyekForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
        //                                "&" + QueryStringVars.Id + "=" + kuldemenyID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        if (isErtesitesSikeres == true)   // TODO: email megtagad�s
        {
            labelErtesitesSikeres.Text = msgMessageSent_Success;

        }
        else
        {
            labelErtesitesSikeres.Text = msgMessageSent_Failed;
        }
    }

    private void SetAnwerEmailData(EREC_eMailBoritekok erec_eMailBoritekok)
    {
        if (erec_eMailBoritekok == null) return;

        //L�that�s�g be�ll�t�sa
        SetViewControls_AnswerEmail();       
        tr_AnswerEmail.Visible = true;

        LoadComponentsFromBusinessObject_AnswerMail(erec_eMailBoritekok);


    }
    
}
