﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

public partial class Email_PrintForm : Contentum.eUtility.UI.PageBase
{
    private string IratId = String.Empty;
    //private string EmailBoritekokId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        IratId = Request.QueryString.Get(QueryStringVars.IratId);
        //EmailBoritekokId = Request.QueryString.Get(QueryStringVars.EmailBoritekokId);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        erec_IraIratokSearch.Id.Value = IratId;
        erec_IraIratokSearch.Id.Operator = Query.Operators.equals;

        Result erec_IraIratok_result = erec_IraIratokService.GetAllWithExtension(execParam, erec_IraIratokSearch);

        if (erec_IraIratok_result.IsError)
        {
            ShowResultError(erec_IraIratok_result);
            return;
        }
        else if (erec_IraIratok_result.Ds.Tables[0].Rows.Count == 0)
        {
            Result result_nohit = new Result();
            ResultError.CreateNoRowResultError(result_nohit);
            ShowResultError(result_nohit);
            return;
        }

        //if (!erec_IraIratok_result.IsError)
        //{

        // ellenőrzés, hogy láthatja-e a csatolmányt
        int csatolmanyCount = 0;
        // elvileg pontosan 1 iratsor
        Int32.TryParse(erec_IraIratok_result.Ds.Tables[0].Rows[0]["CsatolmanyCount"].ToString(), out csatolmanyCount);

        if (csatolmanyCount <= 0)
        {
            ShowError(Resources.Error.ErrorCode_52792);
            return;
        }

        EREC_PldIratPeldanyokService erec_PldIratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        EREC_PldIratPeldanyokSearch erec_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

        erec_PldIratPeldanyokSearch.IraIrat_Id.Value = IratId;
        erec_PldIratPeldanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;
        erec_PldIratPeldanyokSearch.Sorszam.Value = "1";
        erec_PldIratPeldanyokSearch.Sorszam.Operator = Query.Operators.equals;

        Result erec_PldIratPeldanyok_result = erec_PldIratPeldanyokService.GetAllWithExtension(execParam, erec_PldIratPeldanyokSearch);

        if (erec_PldIratPeldanyok_result.IsError)
        {
            ShowResultError(erec_PldIratPeldanyok_result);
            return;
        }

        string barcode = "";

        if (erec_PldIratPeldanyok_result.Ds.Tables[0].Rows.Count > 0)
        {
            barcode = erec_PldIratPeldanyok_result.Ds.Tables[0].Rows[0]["BarCode"].ToString();
        }


        EREC_eMailBoritekokService erec_eMailBoritekokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekokService();
        EREC_eMailBoritekokSearch erec_eMailBoritekokSearch = new EREC_eMailBoritekokSearch();

        if (erec_IraIratok_result.Ds.Tables[0].Rows[0]["PostazasIranya"].ToString().Equals(KodTarak.POSTAZAS_IRANYA.Belso)) // "0"
        {
            erec_eMailBoritekokSearch.IraIrat_Id.Value = IratId;
            erec_eMailBoritekokSearch.IraIrat_Id.Operator = Query.Operators.equals;
        }
        else
        {
            erec_eMailBoritekokSearch.KuldKuldemeny_Id.Value = erec_IraIratok_result.Ds.Tables[0].Rows[0]["KuldKuldemenyek_Id"].ToString();
            erec_eMailBoritekokSearch.KuldKuldemeny_Id.Operator = Query.Operators.equals;
        }

        /*erec_eMailBoritekokSearch.Id.Value = EmailBoritekokId;
        erec_eMailBoritekokSearch.Id.Operator = Query.Operators.equals;*/

        Result erec_eMailBoritekok_result = erec_eMailBoritekokService.GetAllWithExtension(execParam, erec_eMailBoritekokSearch);

        string strResult = "";
        string csatolmanyok = "";
        string[] str = { "", "" };
        string SenderName = "";
        string To = "";
        string CC = "";
        string Erkezett = "";

        if (!erec_eMailBoritekok_result.IsError)
        {
            string strInput = erec_eMailBoritekok_result.Ds.Tables[0].Rows[0]["Uzenet"].ToString();
            SenderName = erec_eMailBoritekok_result.Ds.Tables[0].Rows[0]["Felado"].ToString();
            SenderName = getRealName(SenderName);

            To = erec_eMailBoritekok_result.Ds.Tables[0].Rows[0]["Cimzett"].ToString();
            if (!To.Length.Equals(0))
            {
                if (To.Contains(","))
                {
                    string[] _To = To.Split(new Char [] {' ', ','});
                    To = "";
                    foreach (string s in _To)
                    {
                        if (!string.IsNullOrEmpty(s))
                        {
                            To = To + getRealName(s) + ", ";
                        }
                    }
                    To = To.Remove(To.LastIndexOf(','));
                }
                else
                {
                    To = getRealName(To);
                }
            }

            CC = erec_eMailBoritekok_result.Ds.Tables[0].Rows[0]["CC"].ToString();
            if (!CC.Length.Equals(0))
            {
                if (CC.Contains(","))
                {
                    string[] _CC = CC.Split(new Char [] {' ', ','});
                    CC = "";
                    foreach (string s in _CC)
                    {
                        if (!string.IsNullOrEmpty(s))
                        {
                            CC = CC + getRealName(s) + ", ";
                        }
                    }
                    CC = CC.Remove(CC.LastIndexOf(','));
                }
                else
                {
                    CC = getRealName(CC);
                }
            }

            Erkezett = erec_eMailBoritekok_result.Ds.Tables[0].Rows[0]["ErkezesDatuma"].ToString();


       

            strResult = strInput.Replace("\r", " ");


            strResult = strResult.Replace("\n", " ");

            strResult = strResult.Replace("\t", string.Empty);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult, @"( )+", " ");

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*head([^>])*>", "<head>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"(<( )*(/)( )*head( )*>)", "</head>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            /*strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(<head>).*(</head>)", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);*/

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*script([^>])*>", "<script>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"(<( )*(/)( )*script( )*>)", "</script>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"(<script>).*(</script>)", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*style([^>])*>", "<style>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"(<( )*(/)( )*style( )*>)", "</style>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(<style>).*(</style>)", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*td([^>])*>", "\t",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*br( )*>", "\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*li( )*>", "\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*div([^>])*>", "\r\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*tr([^>])*>", "\r\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<( )*p([^>])*>", "\r\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"<[^>]*>", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @" ", " ",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&bull;", " * ",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&lsaquo;", "<",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&rsaquo;", ">",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&trade;", "(tm)",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&frasl;", "/",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&lt;", "<",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&gt;", ">",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&copy;", "(c)",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&reg;", "(r)",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            // nekrisz CR 2967 : html accents code conversion
         
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&aacute;", "á",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&eacute;", "é",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&iacute;", "í",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&oacute;", "ó",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&uacute;", "ú",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&ouml;", "ö",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&ocirc;", "ő",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&uuml;", "ü",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&ucirc;", "ű",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Aacute;", "Á",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Eacute;", "É",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Iacute;", "Í",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Oacute;", "Ó",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Uacute;", "Ú",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Ouml;", "Ö",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Ocirc;", "Ő",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Uuml;", "Ü",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                    @"&Ucirc;", "Ű",
                    System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     @"&(.{2,6});", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = strResult.Replace("\n", "\r");

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(\r)( )+(\r)", "\r\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(\t)( )+(\t)", "\t\t",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(\t)( )+(\r)", "\t\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(\r)( )+(\t)", "\r\t",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(\r)(\t)+(\r)", "\r\r",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            strResult = System.Text.RegularExpressions.Regex.Replace(strResult,
                     "(\r)(\t)+", "\r\t",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            string breaks = "\r\r\r";

            string tabs = "\t\t\t\t\t";
            for (int index = 0; index < strResult.Length; index++)
            {
                strResult = strResult.Replace(breaks, "\r\r");
                strResult = strResult.Replace(tabs, "\t\t\t\t");
                breaks = breaks + "\r";
                tabs = tabs + "\t";
            }

            str = strResult.Split("\r\r".ToCharArray());

            EREC_eMailBoritekCsatolmanyokService erec_eMailBoritekCsatolmanyokService = eRecordService.ServiceFactory.GetEREC_eMailBoritekCsatolmanyokService();
            EREC_eMailBoritekCsatolmanyokSearch erec_eMailBoritekCsatolmanyokSearch = new EREC_eMailBoritekCsatolmanyokSearch();

            erec_eMailBoritekCsatolmanyokSearch.eMailBoritek_Id.Value = erec_eMailBoritekok_result.Ds.Tables[0].Rows[0]["Id"].ToString();
            erec_eMailBoritekCsatolmanyokSearch.eMailBoritek_Id.Operator = Query.Operators.equals;

            Result emailcsat_result = erec_eMailBoritekCsatolmanyokService.GetAllWithExtension(execParam, erec_eMailBoritekCsatolmanyokSearch);

            if (!emailcsat_result.IsError)
            {
                System.Collections.Generic.List<string> lstCsatolmanyNevek = new System.Collections.Generic.List<string>(emailcsat_result.Ds.Tables[0].Rows.Count);
                foreach (DataRow _row in emailcsat_result.Ds.Tables[0].Rows)
                {
                    //if (!_row["Nev"].ToString().Equals("EredetiOulookUzenet.msg"))
                    //    csatolmanyok = csatolmanyok + _row["Nev"].ToString() + ", ";

                    string csatolmanyNev = _row["Nev"].ToString();

                    // régi elírás miatt
                    if (csatolmanyNev != "EredetiOulookUzenet.msg"
                        && csatolmanyNev != Contentum.eUtility.Constants.EMail.EredetiUzenetFileName) //"EredetiOulookUzenet.msg"
                    {
                        lstCsatolmanyNevek.Add(csatolmanyNev);
                    }
                }
                csatolmanyok = String.Join(", ", lstCsatolmanyNevek.ToArray());
            }
        }

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        for (int i = 0; i < str.Length; i++)
        {
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Body" + i;
            column.AutoIncrement = false;
            column.Caption = "Body" + i;
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
        }
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Csatolmanyok";
        column.AutoIncrement = false;
        column.Caption = "Csatolmanyok";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Keszult";
        column.AutoIncrement = false;
        column.Caption = "Keszult";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "BarCode";
        column.AutoIncrement = false;
        column.Caption = "BarCode";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "SenderName";
        column.AutoIncrement = false;
        column.Caption = "SenderName";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "To";
        column.AutoIncrement = false;
        column.Caption = "To";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "CC";
        column.AutoIncrement = false;
        column.Caption = "CC";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Erkezett";
        column.AutoIncrement = false;
        column.Caption = "Erkezett";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        erec_IraIratok_result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        for (int i = 0; i < str.Length; i++)
        {
            row["Body" + i] = str[i];
        }
        row["Csatolmanyok"] = csatolmanyok;
        row["Keszult"] = System.DateTime.Now.ToString();
        row["BarCode"] = barcode;
        row["SenderName"] = SenderName;
        row["To"] = To;
        row["CC"] = CC;
        row["Erkezett"] = Erkezett;
        table.Rows.Add(row);

        //string xsd = result.Ds.GetXmlSchema();
        string templateText = "";
        string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
        string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
        string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
        string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
        string templ = "";
        if (erec_IraIratok_result.Ds.Tables[0].Rows[0]["PostazasIranya"].ToString().Equals(KodTarak.POSTAZAS_IRANYA.Belso)) // "0"
        {
            templ = "E-mail nyomtatas - kimeno.xml";
        }
        else
        {
            templ = "E-mail nyomtatas - bejovo.xml";
        }
        WebRequest wr = WebRequest.Create(SP_TM_site_url + templ);
        wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
        StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
        templateText = template.ReadToEnd();
        template.Close();
        //templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"e-mail\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>bognar.laura</o:Author><o:LastAuthor>Kiss Gergely</o:LastAuthor><o:Revision>7</o:Revision><o:TotalTime>14</o:TotalTime><o:Created>2009-02-04T16:26:00Z</o:Created><o:LastSaved>2009-07-13T12:33:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>24</o:Words><o:Characters>171</o:Characters><o:Company>Axis Consulting 2000 Kft.</o:Company><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>194</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"420020EB\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Free 3 of 9 Extended\"><w:panose-1 w:val=\"00000000000000000000\"/><w:charset w:val=\"00\"/><w:family w:val=\"auto\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000003\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000001\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"FFFFFF7C\"/><w:plt w:val=\"SingleLevel\"/><w:tmpl w:val=\"AB8C9B78\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:tabs><w:tab w:val=\"list\" w:pos=\"1492\"/></w:tabs><w:ind w:left=\"1492\" w:hanging=\"360\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"1\"><w:lsid w:val=\"FFFFFF7D\"/><w:plt w:val=\"SingleLevel\"/><w:tmpl w:val=\"1E68E2DC\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:tabs><w:tab w:val=\"list\" w:pos=\"1209\"/></w:tabs><w:ind w:left=\"1209\" w:hanging=\"360\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"2\"><w:lsid w:val=\"FFFFFF7E\"/><w:plt w:val=\"SingleLevel\"/><w:tmpl w:val=\"B96A97E2\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:tabs><w:tab w:val=\"list\" w:pos=\"926\"/></w:tabs><w:ind w:left=\"926\" w:hanging=\"360\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"3\"><w:lsid w:val=\"FFFFFF7F\"/><w:plt w:val=\"SingleLevel\"/><w:tmpl w:val=\"C6B473CC\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:tabs><w:tab w:val=\"list\" w:pos=\"643\"/></w:tabs><w:ind w:left=\"643\" w:hanging=\"360\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"4\"><w:lsid w:val=\"FFFFFF88\"/><w:plt w:val=\"SingleLevel\"/><w:tmpl w:val=\"677C7854\"/><w:lvl w:ilvl=\"0\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:tabs><w:tab w:val=\"list\" w:pos=\"360\"/></w:tabs><w:ind w:left=\"360\" w:hanging=\"360\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"5\"><w:lsid w:val=\"2F2C59B4\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"535433D4\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"5\"/></w:list><w:list w:ilfo=\"2\"><w:ilst w:val=\"4\"/></w:list><w:list w:ilfo=\"3\"><w:ilst w:val=\"3\"/></w:list><w:list w:ilfo=\"4\"><w:ilst w:val=\"2\"/></w:list><w:list w:ilfo=\"5\"><w:ilst w:val=\"1\"/></w:list><w:list w:ilfo=\"6\"><w:ilst w:val=\"0\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Normál\"/><w:rsid w:val=\"00AE120F\"/><w:pPr><w:spacing w:before=\"120\" w:after=\"120\"/><w:ind w:left=\"482\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezdés alapbetűtípusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Normál táblázat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"character\" w:styleId=\"Hiperhivatkozs\"><w:name w:val=\"Hyperlink\"/><wx:uiName wx:val=\"Hiperhivatkozás\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:rsid w:val=\"00FE6B05\"/><w:rPr><w:color w:val=\"0000FF\"/><w:u w:val=\"single\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszerű bekezdés\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"00A02B86\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"Rácsos táblázat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"006B725C\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"bar\"><w:name w:val=\"bar\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"009E3386\"/><w:pPr><w:autoSpaceDE w:val=\"off\"/><w:autoSpaceDN w:val=\"off\"/><w:adjustRightInd w:val=\"off\"/><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/><w:jc w:val=\"right\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Free 3 of 9 Extended\" w:fareast=\"Times New Roman\" w:h-ansi=\"Free 3 of 9 Extended\"/><wx:font wx:val=\"Free 3 of 9 Extended\"/><w:sz w:val=\"52\"/><w:sz-cs w:val=\"24\"/><w:lang w:fareast=\"HU\"/></w:rPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Stlus1\"><w:name w:val=\"Stílus1\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"008018A1\"/><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"7371\"/></w:tabs><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr><w:rPr><w:rFonts w:ascii=\"Free 3 of 9 Extended\" w:h-ansi=\"Free 3 of 9 Extended\"/><wx:font wx:val=\"Free 3 of 9 Extended\"/><w:sz w:val=\"52\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"13314\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00FE6B05\"/><wsp:rsid wsp:val=\"00001FAC\"/><wsp:rsid wsp:val=\"000C6D9B\"/><wsp:rsid wsp:val=\"000E4B16\"/><wsp:rsid wsp:val=\"000F4899\"/><wsp:rsid wsp:val=\"0017139B\"/><wsp:rsid wsp:val=\"0017467C\"/><wsp:rsid wsp:val=\"002B194D\"/><wsp:rsid wsp:val=\"0036238A\"/><wsp:rsid wsp:val=\"003735CB\"/><wsp:rsid wsp:val=\"00391E20\"/><wsp:rsid wsp:val=\"0042651B\"/><wsp:rsid wsp:val=\"004607FD\"/><wsp:rsid wsp:val=\"00465882\"/><wsp:rsid wsp:val=\"004A6195\"/><wsp:rsid wsp:val=\"004C2BF6\"/><wsp:rsid wsp:val=\"0052109F\"/><wsp:rsid wsp:val=\"005E0268\"/><wsp:rsid wsp:val=\"005F235B\"/><wsp:rsid wsp:val=\"00600028\"/><wsp:rsid wsp:val=\"00632520\"/><wsp:rsid wsp:val=\"006A1429\"/><wsp:rsid wsp:val=\"006B725C\"/><wsp:rsid wsp:val=\"00706A6F\"/><wsp:rsid wsp:val=\"0072455E\"/><wsp:rsid wsp:val=\"00737828\"/><wsp:rsid wsp:val=\"0075082C\"/><wsp:rsid wsp:val=\"008018A1\"/><wsp:rsid wsp:val=\"008D7064\"/><wsp:rsid wsp:val=\"009E3386\"/><wsp:rsid wsp:val=\"00A02B86\"/><wsp:rsid wsp:val=\"00A32C00\"/><wsp:rsid wsp:val=\"00AE120F\"/><wsp:rsid wsp:val=\"00B2421D\"/><wsp:rsid wsp:val=\"00BC0ED1\"/><wsp:rsid wsp:val=\"00C91AFF\"/><wsp:rsid wsp:val=\"00CA6941\"/><wsp:rsid wsp:val=\"00CF24E3\"/><wsp:rsid wsp:val=\"00DA7E81\"/><wsp:rsid wsp:val=\"00E030F0\"/><wsp:rsid wsp:val=\"00E50CAE\"/><wsp:rsid wsp:val=\"00FE6B05\"/><wsp:rsid wsp:val=\"00FF51E7\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"00FE6B05\" wsp:rsidRPr=\"00FE6B05\" wsp:rsidRDefault=\"00FE6B05\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:jc w:val=\"center\"/><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"00FE6B05\"><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Budapest Főváros Önkormányzata Főpolgármesteri Hivatala</w:t></w:r></w:p><w:p wsp:rsidR=\"00FE6B05\" wsp:rsidRPr=\"00FE6B05\" wsp:rsidRDefault=\"00FE6B05\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr></w:p><w:p wsp:rsidR=\"00FE6B05\" wsp:rsidRDefault=\"006B725C\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:jc w:val=\"center\"/><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>E-mailből iktatott irat</w:t></w:r></w:p><w:p wsp:rsidR=\"009E3386\" wsp:rsidRPr=\"00FE6B05\" wsp:rsidRDefault=\"009E3386\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:jc w:val=\"center\"/><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr></w:pPr></w:p><w:p wsp:rsidR=\"008018A1\" wsp:rsidRDefault=\"008018A1\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/></w:pPr></w:p><w:p wsp:rsidR=\"008018A1\" wsp:rsidRPr=\"008018A1\" wsp:rsidRDefault=\"008018A1\" wsp:rsidP=\"008018A1\"><w:pPr><w:pStyle w:val=\"Stlus1\"/><w:jc w:val=\"right\"/><w:rPr><w:rFonts w:ascii=\"Calibri\" w:h-ansi=\"Calibri\"/><wx:font wx:val=\"Calibri\"/></w:rPr></w:pPr><w:r><w:t>*</w:t></w:r><ParentTable><BarCode/></ParentTable><w:r><w:t>*</w:t></w:r></w:p><w:p wsp:rsidR=\"006B725C\" wsp:rsidRDefault=\"006B725C\" wsp:rsidP=\"00DA7E81\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"7371\"/></w:tabs><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr><w:r><w:t>Iktatószám</w:t></w:r><w:r wsp:rsidR=\"00FE6B05\" wsp:rsidRPr=\"00FE6B05\"><w:t>: </w:t></w:r><Table><IktatoSzam_Merge/></Table><w:r wsp:rsidR=\"008018A1\"><w:tab/></w:r><ParentTable><BarCode/></ParentTable></w:p><w:p wsp:rsidR=\"00CA6941\" wsp:rsidRDefault=\"00CA6941\" wsp:rsidP=\"00DA7E81\"><w:pPr><w:tabs><w:tab w:val=\"center\" w:pos=\"7371\"/></w:tabs><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr><w:r><w:t>Tárgy: </w:t></w:r><Table><Targy1/></Table></w:p><w:p wsp:rsidR=\"00CA6941\" wsp:rsidRDefault=\"00CA6941\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr><w:r><w:t>Ügyintéző: </w:t></w:r><Table><FelhasznaloCsoport_Id_Ugyintezo_Nev_Print/></Table></w:p><w:p wsp:rsidR=\"00FE6B05\" wsp:rsidRDefault=\"00FE6B05\" wsp:rsidP=\"000E4B16\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr><w:r wsp:rsidRPr=\"00FE6B05\"><w:t>Készült: </w:t></w:r><ParentTable><Keszult/></ParentTable></w:p><w:p wsp:rsidR=\"00FE6B05\" wsp:rsidRDefault=\"00FE6B05\" wsp:rsidP=\"006B725C\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr></w:p><w:p wsp:rsidR=\"003735CB\" wsp:rsidRPr=\"00FE6B05\" wsp:rsidRDefault=\"003735CB\" wsp:rsidP=\"006B725C\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr><w:r><w:t>E-mail csatolmányok: </w:t></w:r><ParentTable><Csatolmanyok/></ParentTable></w:p><w:tbl><w:tblPr><w:tblW w:w=\"0\" w:type=\"auto\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"9212\"/></w:tblGrid><w:tr wsp:rsidR=\"006B725C\" wsp:rsidTr=\"006B725C\"><w:tc><w:tcPr><w:tcW w:w=\"9212\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"00CA6941\" wsp:rsidRDefault=\"00CA6941\" wsp:rsidP=\"006B725C\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr></w:p><ParentTable>";
        string rs = "";
        for (int i = 0; i < str.Length; i++)
        {
            rs = rs + "<Body" + i + "><w:p wsp:rsidR=\"006B725C\" wsp:rsidRDefault=\"00FF51E7\" wsp:rsidP=\"006B725C\"><w:pPr><w:spacing w:before=\"0\" w:after=\"0\"/><w:ind w:left=\"0\"/></w:pPr></w:p></Body" + i + ">";
        }
        string os = templateText.Remove(templateText.LastIndexOf("</ParentTable>"));
        os = os.Substring(os.IndexOf("<Body>"));
        templateText = templateText.Replace(os,rs);

        bool pdf = false;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            pdf = true;
        }

        erec_IraIratok_result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam);

        string filename = "";

        if (pdf)
        {
            filename = "Iktatott_Email_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
        }
        else
        {
            filename = "Iktatott_Email_" +
                        System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                        "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
        }

        int priority = 1;
        bool prior = false;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = erec_IraIratok_result.Uid;
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (!csop_result.IsError)
        {
            foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
            {
                if (_row["Tipus"].ToString().Equals(KodTarak.CsoprttagsagTipus.vezeto)) // "3"
                {
                    prior = true;
                }
            }
        }

        if (prior)
        {
            priority++;
        }

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        Result result = tms.GetWordDocument_DataSet_Thread(templateText, erec_IraIratok_result, pdf, priority, filename, 25);

        byte[] res = (byte[])result.Record;

        if (!result.IsError)
        {
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            if (pdf)
            {
                Response.ContentType = "application/pdf";
            }
            else
            {
                Response.ContentType = "application/msword";
            }
            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();
        }
        else
        {
            if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
            {
                string js = "alert('A dokumentum elkészüléséről e-mail értesítést fog kapni!');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                return;
            }
            else
            {
                ShowResultError(result);
                return;
            }
        }
        //}
    }

    private void ShowResultError(Result result)
    {
        if (result.IsError)
        {
            string error = String.Empty;
            try
            {
                error = ResultError.GetErrorMessageFromResultObject(result);
            }
            catch { }

            if (String.IsNullOrEmpty(error))
            {
                error = Resources.Error.EmptyErrorMessage;
            }
            ShowError(error);
        }
    }

    private void ShowError(string errormsg)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "error", String.Format("alert('{0}\\n\\n{1}');", Resources.Error.DefaultErrorHeader ,errormsg ?? Resources.Error.DefaultErrorHeader), true);
    }

    private string getRealName(string email)
    {
        Contentum.eAdmin.Service.KRT_CsoportokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        KRT_CsoportokSearch krt_CsoportokSearch = new KRT_CsoportokSearch();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        krt_CsoportokSearch.ErtesitesEmail.Value = email;
        krt_CsoportokSearch.ErtesitesEmail.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportokSearch);

        if (!csop_result.IsError)
        {
            if (csop_result.Ds.Tables[0].Rows.Count.Equals(0))
            {
                return email;
            }
            else
            {
                string name = csop_result.Ds.Tables[0].Rows[0]["Nev"].ToString();
                return name;
            }
        }
        else
        {
            return email;
        }
    }
}
