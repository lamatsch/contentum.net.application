<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" Title="Untitled Page" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <meta http-equiv="cache-control" content="no-store" />
    <title>Hiba</title>       
</head>
<body style="background-color: White;">
<script language="JavaScript" type="text/javascript">

function swapByName(obj, newImageName) 
{
    var myobj = document.getElementById(obj); 
    if (myobj) 
    {
        var fileNameStartIndex = myobj.src.lastIndexOf("/");
        var newUrl = myobj.src.substring(0,fileNameStartIndex+1)+newImageName;
        
        myobj.src = newUrl;            
    }
}

</script>
    <form id="form1" runat="server">
    <div style="vertical-align:middle; text-align: center; height: 300px;">
        <table cellpadding="0" cellspacing="0" style="width: 70%; height: 100%;">
                <tr>
                    <td style="text-align: center;">
                        <br/>
                        <br/>
                                    
                        <eUI:eErrorPanel ID="ErrorPanel1" runat="server">
                            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                        </eUI:eErrorPanel>
                        
                        <eUI:eFormPanel ID="GenericErrorPanel" runat="server" Visible="false">
                            <table cellspacing="0" cellpadding="0" width="100%" class="mrHiba">
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td align="center" style="width: 80px; vertical-align: middle;">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="images/hu/design/hiba.jpg" />
                                                </td>
                                                <td class="hibaHeader" style="width: auto; vertical-align: middle;">
                                                    &nbsp;
                                                    <asp:Label ID="Label_DefaultErrorHeader" runat="server" Text="Hiba a k�r�s feldolgoz�sa sor�n!"></asp:Label>
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td align="center" style="width: 80px; vertical-align: middle;">
                                                </td>
                                                <td class="" style="width: auto; vertical-align: middle; text-align: left;">
                                                    <asp:Label ID="Label2" runat="server" 
                                                        Text="A szerveren hiba l�pett fel az oldal feldolgoz�sa k�zben. A kellemetlens�g�rt eln�z�s�t k�rj�k!">
                                                    </asp:Label>                                                    
                                                </td>                                                 
                                            </tr>
                                            
                                            <tr>
                                                <td align="center" style="width: 80px; vertical-align: middle;">
                                                </td>
                                                <td class="" style="width: auto; vertical-align: middle; text-align: left;">
                                                    <br />
                                                    <asp:Label ID="Label4" runat="server" Text="A hib�t jelezheti e-mailben, az " /> 
                                                        <a runat="server" id="mailtoLink" href="mailto:edok@budapest.hu" style="text-decoration: underline;">edok@budapest.hu</a>
                                                        <asp:Label ID="Label6" runat="server" Text=" c�men."></asp:Label>
                                                    
                                                    <br />                              
                                                    <asp:Label ID="Label5" runat="server" Text=" A hiba ok�nak felder�t�se �rdek�ben k�rj�k, hogy �zenet�ben t�ntesse fel a hiba bek�vetkezt�nek id�pontj�t!">
                                                    </asp:Label>              
                                                </td>                                                                                                 
                                            </tr>
                                            
                                            <tr>
                                                <td align="center" style="width: 80px; vertical-align: middle;">
                                                </td>
                                                <td style="width: auto; vertical-align: middle; text-align: left;">
                                                    <br />
                                                    <asp:Label ID="Label3" runat="server" 
                                                        Text="A hiba bek�vetkezt�nek id�pontja: ">
                                                    </asp:Label>               
                                                    <asp:Label ID="Label_CurrentTime" runat="server" CssClass="FuttatasIdejeRowStyle" Text=""></asp:Label>                                     
                                                </td>                                                 
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                        <br />
                        <asp:ImageButton ID="ImageVissza" runat="server" ImageUrl="~/images/hu/ovalgomb/vissza.jpg"
                            OnClientClick=""
                            OnClick="ImageVissza_Click" />
                        <asp:ImageButton ID="ImageClose" runat="server"
                            ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                            onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                            OnClientClick="window.close(); return false;" />
                        <br />
                        <br/>
                   </td>
                </tr>
            </table> 
     </div>               
     </form>
</body>
</html>
