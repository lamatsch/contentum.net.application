using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;

// A teljes k�d kidolgozand�! 

public partial class EsemenyekSearch : Contentum.eUtility.UI.PageBase
{
    private const string kcs_HELYETTESITES_MOD = "HELYETTESITES_MOD";

    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
        static public bool Contain(string value)
        {
            if ((value == Yes) || (value == No) || (value == NotSet))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private Type _type = typeof(KRT_EsemenyekSearch);


    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {

        SearchHeader1.TemplateObjectType = _type;
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            KodTarakDropDownList_HelyettesitesMod.FillDropDownList(kcs_HELYETTESITES_MOD, true, null);

            KRT_EsemenyekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_EsemenyekSearch)Search.GetSearchObject(Page, new KRT_EsemenyekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
        else
        {
            
        }

    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_EsemenyekSearch _KRT_EsemenyekSearch = (KRT_EsemenyekSearch)searchObject;

        if (_KRT_EsemenyekSearch != null)
        {
            switch (_KRT_EsemenyekSearch.LetrehozasIdo.Operator)
            {
                case Query.Operators.between:
                    DatumIntervallum_SearchCalendarControl1.DatumKezdValue = _KRT_EsemenyekSearch.LetrehozasIdo.Value;
                    DatumIntervallum_SearchCalendarControl1.DatumVegeValue = _KRT_EsemenyekSearch.LetrehozasIdo.ValueTo;
                    break;
                case Query.Operators.greaterorequal:
                    DatumIntervallum_SearchCalendarControl1.DatumKezdValue = _KRT_EsemenyekSearch.LetrehozasIdo.Value;
                    break;
                case Query.Operators.lessorequal:
                    DatumIntervallum_SearchCalendarControl1.DatumVegeValue = _KRT_EsemenyekSearch.LetrehozasIdo.Value;
                    break;
                default:
                    DatumIntervallum_SearchCalendarControl1.DatumKezdValue = string.Empty;
                    DatumIntervallum_SearchCalendarControl1.DatumVegeValue = string.Empty;
                    break;
            }

            FunckioTextBox.Text = _KRT_EsemenyekSearch.Extended_KRT_FunkciokSearch.Nev.Value;
            ObjektumTipusTextBox.Text = _KRT_EsemenyekSearch.Extended_KRT_ObjTipusokSearch.Nev.Value;
            TextBox1.Text = _KRT_EsemenyekSearch.Azonositoja.Value;
            FelhasznaloCsoportTextBox1.Id_HiddenField = _KRT_EsemenyekSearch.Felhasznalo_Id_User.Value;
            FelhasznaloCsoportTextBox1.SetCsoportTextBoxById(null);

            FelhasznaloCsoportTextBox2.Id_HiddenField = _KRT_EsemenyekSearch.Felhasznalo_Id_Login.Value;
            FelhasznaloCsoportTextBox2.SetCsoportTextBoxById(null);

            CsoportTextBox1.Id_HiddenField = _KRT_EsemenyekSearch.Csoport_Id_FelelosUserSzerveze.Value;
            CsoportTextBox1.SetCsoportTextBoxById(null);

            KodTarakDropDownList_HelyettesitesMod.SetSelectedValue(_KRT_EsemenyekSearch.Extended_KRT_HelyettesitesekSearch.HelyettesitesMod.Value);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private KRT_EsemenyekSearch SetSearchObjectFromComponents()
    {
        KRT_EsemenyekSearch _KRT_EsemenyekSearch = (KRT_EsemenyekSearch)SearchHeader1.TemplateObject;

        if (_KRT_EsemenyekSearch == null)
        {
            _KRT_EsemenyekSearch = new KRT_EsemenyekSearch(true);
        }

        if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumKezdValue))
        {
            _KRT_EsemenyekSearch.LetrehozasIdo.Value = DatumIntervallum_SearchCalendarControl1.DatumKezdValue;
            if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumVegeValue))
            {
                _KRT_EsemenyekSearch.LetrehozasIdo.ValueTo = DatumIntervallum_SearchCalendarControl1.DatumVegeValue;
                _KRT_EsemenyekSearch.LetrehozasIdo.Operator = Query.Operators.between;
            }
            else
            {
                _KRT_EsemenyekSearch.LetrehozasIdo.Operator = Query.Operators.greaterorequal;
            }
        }
        else
        {
            if (!string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumVegeValue))
            {
                _KRT_EsemenyekSearch.LetrehozasIdo.Value = DatumIntervallum_SearchCalendarControl1.DatumVegeValue;
                _KRT_EsemenyekSearch.LetrehozasIdo.Operator = Query.Operators.lessorequal;
            }
        }


        if (!string.IsNullOrEmpty(FunckioTextBox.Text))
        {
            _KRT_EsemenyekSearch.Extended_KRT_FunkciokSearch.Nev.Value = FunckioTextBox.Text;
            _KRT_EsemenyekSearch.Extended_KRT_FunkciokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(FunckioTextBox.Text);
        }

        if (!string.IsNullOrEmpty(ObjektumTipusTextBox.Text))
        {
            _KRT_EsemenyekSearch.Extended_KRT_ObjTipusokSearch.Nev.Value = ObjektumTipusTextBox.Text;
            _KRT_EsemenyekSearch.Extended_KRT_ObjTipusokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(ObjektumTipusTextBox.Text);
        }

        _KRT_EsemenyekSearch.Azonositoja.Value = TextBox1.Text;
        _KRT_EsemenyekSearch.Azonositoja.Operator = Search.GetOperatorByLikeCharater(TextBox1.Text);

        if (!string.IsNullOrEmpty(FelhasznaloCsoportTextBox1.Id_HiddenField))
        {
            _KRT_EsemenyekSearch.Felhasznalo_Id_User.Value = FelhasznaloCsoportTextBox1.Id_HiddenField;
            _KRT_EsemenyekSearch.Felhasznalo_Id_User.Operator = Query.Operators.equals;
        }

        if (!string.IsNullOrEmpty(FelhasznaloCsoportTextBox2.Id_HiddenField))
        {
            _KRT_EsemenyekSearch.Felhasznalo_Id_Login.Value = FelhasznaloCsoportTextBox2.Id_HiddenField;
            _KRT_EsemenyekSearch.Felhasznalo_Id_Login.Operator = Query.Operators.equals;
        }

        if (!string.IsNullOrEmpty(CsoportTextBox1.Id_HiddenField))
        {
            _KRT_EsemenyekSearch.Csoport_Id_FelelosUserSzerveze.Value = CsoportTextBox1.Id_HiddenField;
            _KRT_EsemenyekSearch.Csoport_Id_FelelosUserSzerveze.Operator = Query.Operators.equals;
        }

        if (!string.IsNullOrEmpty(KodTarakDropDownList_HelyettesitesMod.SelectedValue))
        {
            _KRT_EsemenyekSearch.Extended_KRT_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarakDropDownList_HelyettesitesMod.SelectedValue;
            _KRT_EsemenyekSearch.Extended_KRT_HelyettesitesekSearch.HelyettesitesMod.Operator = Query.Operators.equals;
        }

        return _KRT_EsemenyekSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_EsemenyekSearch searchObject = SetSearchObjectFromComponents();
            KRT_EsemenyekSearch defaultSearchObject = GetDefaultSearchObject();

            if (Search.IsIdentical(searchObject, defaultSearchObject)
                && Search.IsIdentical(searchObject.Extended_KRT_ObjTipusokSearch,defaultSearchObject.Extended_KRT_ObjTipusokSearch)
                && Search.IsIdentical(searchObject.Extended_KRT_FunkciokSearch,defaultSearchObject.Extended_KRT_FunkciokSearch)
                && Search.IsIdentical(searchObject.Extended_KRT_HelyettesitesekSearch, defaultSearchObject.Extended_KRT_HelyettesitesekSearch)
                )
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private KRT_EsemenyekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_EsemenyekSearch(true);
    }
}
