﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="EvesIratForgalmiStatisztikaPrintForm.aspx.cs" Inherits="EvesIratForgalmiStatisztikaPrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,EvesIratForgalmiStatisztikaPrintFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                     
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label33" runat="server" CssClass="mrUrlapCaption" Text="Év:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="Iktatokonyv_Ev_EvIntervallum_SearchFormControl" 
                                    runat="server" IsIntervallumMode="False" />
                            </td>
                        </tr>
                    
                         <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTipus" runat="server" Text="Statisztika típusa:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                       
                                <asp:RadioButtonList ID="rbTipus" runat="server" TextAlign="Right" AutoPostBack="True">
                                    <asp:ListItem Selected="True" Text="Szervezet szintű" Value="SzervezetSzintu" />
                                    <asp:ListItem Selected="False" Text="Iktatohely szintű" Value="IktatohelySzintu" />
                                </asp:RadioButtonList>
                            
                                
                            </td>
                        </tr>                          
                         <tr class="urlapElvalasztoSor">
                            <td />
                        </tr>                      
                        
                        <tr class="urlapSor_kicsi" id="tr_Iktatohely"  runat="server">
                            <td class="mrUrlapCaption_short">
                               <asp:Label ID="Label49" runat="server" Text="Iktatóhely:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                        
                               <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server" 
                                Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                Filter_IdeIktathat="false" />
                            
                            </td>
                        </tr>
                     
                    </table>
                           
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
