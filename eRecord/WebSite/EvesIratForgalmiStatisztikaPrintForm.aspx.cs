﻿using System;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;

public partial class EvesIratForgalmiStatisztikaPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol = System.DateTime.Today.Year.ToString();
            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg = System.DateTime.Today.Year.ToString();
        }
        tr_Iktatohely.Visible = (rbTipus.SelectedValue == "IktatohelySzintu");
       
  
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
     //   FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato, false,
               Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg, false, false, "", EErrorPanel1);


    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            DateTime datumTol = new DateTime(Convert.ToInt32(Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol), 1, 1);
            DateTime datumIg = new DateTime(Convert.ToInt32(Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg), 12, 31,23,59,59);
        String iktatokonyvId;
        if (rbTipus.SelectedValue == "IktatohelySzintu")
        {
            iktatokonyvId = IraIktatoKonyvekDropDownList1.SelectedValue;
        }
        else
            iktatokonyvId = "";

            Response.Redirect("EvesIratForgalmiStatisztikaPrintFormSSRS.aspx?IktatokonyvId=" + iktatokonyvId + "&DatumTol=" + datumTol.ToString() + "&DatumIg=" + datumIg.ToString());


        } else if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
