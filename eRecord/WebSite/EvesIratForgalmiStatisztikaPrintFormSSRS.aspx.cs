using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Net;
using Microsoft.Reporting.WebForms;

public partial class EvesIratForgalmiStatisztikaPrintFormSSRS : Contentum.eUtility.UI.PageBase
{

    private string executor = String.Empty;
    private string szervezet = String.Empty;
    private string datumtol;
    private string datumig;
    private string iktatokonyvid;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatDefiniciokList");
        if (Request.QueryString["DatumTol"] != null)
        {
            datumtol = Request.QueryString["DatumTol"].ToString();

        }
        if (Request.QueryString["DatumIg"] != null)
        {
            datumig = Request.QueryString["DatumIg"].ToString();

        }

        if (Request.QueryString["Iktatokonyvid"] != null)
        {
            iktatokonyvid = Request.QueryString["Iktatokonyvid"].ToString();

        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                 ReportParameter[] ReportParameters = GetReportParameters(rpis);
                 ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                executor = ExecParam.LoginUser_Id;
                szervezet = ExecParam.Org_Id;


                

                ReportParameters = new ReportParameter[rpis.Count];



                for (int i = 0; i < rpis.Count; i++)
                {


                    ReportParameters[i] = new ReportParameter(rpis[i].Name);



                    switch (rpis[i].Name)
                    {

                        case "Where":
                            if (!string.IsNullOrEmpty(iktatokonyvid))
                            {
                                ReportParameters[i].Values.Add(" (EREC_IraIktatokonyvek.id = '"+iktatokonyvid+"' ) ");
                            }
                            break;



                        case "DatumTol":
                            if (!string.IsNullOrEmpty(datumtol))
                            {
                                ReportParameters[i].Values.Add(datumtol);
                            }
                            break;




                        case "DatumIg":
                            if (!string.IsNullOrEmpty(datumig))
                            {
                                ReportParameters[i].Values.Add(datumig);
                            }
                            
                            break;


                        case "ExecutorUserId":
                            if (!string.IsNullOrEmpty(executor))
                            {
                                ReportParameters[i].Values.Add(executor);
                            }
                            break;


                        case "FelhasznaloSzervezet_Id":
                            if (!string.IsNullOrEmpty(szervezet))
                            {
                                ReportParameters[i].Values.Add(szervezet);
                            }
                            break;

                        case "IktatokonyvId":
                            if (!string.IsNullOrEmpty(iktatokonyvid))
                            {
                                ReportParameters[i].Values.Add(iktatokonyvid);
                            }
                            break;

                    }
                }
            }
        }
        return ReportParameters;
    }
}
