using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class ExcelDetailsForLeaders : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "VezetoiPanelView");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        // Adatok betöltése
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string kezdDatum = System.DateTime.Now.Subtract(new TimeSpan(90, 0, 0, 0, 0)).ToString();

        string vegeDatum = System.DateTime.Now.ToString();

        Result result = service.GetAllExcelDetails(execParam, kezdDatum, vegeDatum);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
            return;
        }

        string xml;
        try
        {
            // rename dataset tables for better understanding
            result.Ds.Tables[0].TableName = "Department";
            result.Ds.Tables[1].TableName = "PostAndFiles";
            result.Ds.Tables[2].TableName = "StatisticsOverview";
            result.Ds.Tables[3].TableName = "StatisticsDetails";

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Datumtol";
            column.AutoIncrement = false;
            column.Caption = "Datumtol";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Datumig";
            column.AutoIncrement = false;
            column.Caption = "Datumig";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DatumRef";
            column.AutoIncrement = false;
            column.Caption = "DatumRef";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Datumtol"] = kezdDatum.Substring(0, 10).Replace('.', '-');
            row["Datumig"] = vegeDatum.Substring(0, 10).Replace('.', '-');
            row["DatumRef"] = vegeDatum.Substring(0, 10).Replace('.', '-');
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            table.Rows.Add(row);

            xml = result.Ds.GetXml();
            //string xsd = result.Ds.GetXmlSchema();
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorText_Query + "<br />" + ex.GetType().ToString() + ": " + ex.Message);
            ErrorUpdatePanel.Update();
            return;
        }

        FileStream fs;
        BinaryReader br;
        byte[] fileRD;
        string pathToExcelTemplate;
        // key["RelativePathToExcelTemplate"] = ".\\ExcelTemplates\\VezetoiStatisztika.xls"
        //string relativePathToExcelTemplate = Server.MapPath(Page.ResolveUrl(ConfigurationSettings.AppSettings["RelativePathToExcelTemplate"]));
        try
        {
            pathToExcelTemplate = Server.MapPath(".\\ExcelTemplates\\VezetoiStatisztika.xls");
            fs = new FileStream(pathToExcelTemplate, FileMode.Open, FileAccess.Read);
            br = new BinaryReader(fs);

            fileRD = br.ReadBytes((int)fs.Length);

            br.Close();
            fs.Close();
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, ex.GetType().ToString() + ": " + ex.Message);
            ErrorUpdatePanel.Update();
            return;
        }
        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        try
        {
            result = tms.GetExcelDocument(fileRD, xml, false);


            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                byte[] res = (byte[])result.Record;

                /*string outputFile = "";

                outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test6.xls";

                FileStream fst = new FileStream(outputFile, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fst);
                bw.Write(res);
                bw.Close();*/

                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.ContentType = "application/msexcel";
                Response.AddHeader("content-disposition", "attachment;filename=VezetoiStatisztika.xls;");
                //Response.AddHeader("content-disposition", "inline;filename=vezetoi_stat.xls;");
                Response.AddHeader("Cache-Control", " ");
                Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
                Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
                Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");
               
                Response.OutputStream.Write(res, 0, res.Length);
                Response.OutputStream.Flush();
                Response.End();
            }
            else  // hiba a template manager service alkalmazása során
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
                return;
            }
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, ex.GetType().ToString() + ": " + ex.Message);
            ErrorUpdatePanel.Update();
            return;
        }


    }
}
