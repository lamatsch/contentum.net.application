using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Threading;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class ExcelLogDetails : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            ThreadStart ExcelLogDetailsThread = new ThreadStart(ExcelLogDetailsTh);
            Thread thread = new Thread(ExcelLogDetailsThread);
            thread.Start();
            
            /*EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            string kezdDatum = DatumIntervallum_SearchCalendarControl1.DatumKezd.ToString();

            if (String.IsNullOrEmpty(kezdDatum))
            {
                kezdDatum = System.DateTime.Today.Subtract(new TimeSpan(90, 0, 0, 0, 0)).ToString().Substring(0, 10).Replace('.', '-') + " " + System.DateTime.Now.ToLongTimeString().ToString();
            }

            string vegeDatum = DatumIntervallum_SearchCalendarControl1.DatumVege.ToString();

            if (String.IsNullOrEmpty(vegeDatum))
            {
                vegeDatum = System.DateTime.Today.ToString().Substring(0, 10).Replace('.', '-') + " " + System.DateTime.Now.ToLongTimeString().ToString();
            }

            Result result = service.GetAllExcelLogDetails(execParam, kezdDatum, vegeDatum);

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Datumtol";
            column.AutoIncrement = false;
            column.Caption = "Datumtol";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Datumig";
            column.AutoIncrement = false;
            column.Caption = "Datumig";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DatumRef";
            column.AutoIncrement = false;
            column.Caption = "DatumRef";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Datumtol"] = kezdDatum.Substring(0, 10).Replace('.', '-');
            row["Datumig"] = vegeDatum.Substring(0, 10).Replace('.', '-');
            row["DatumRef"] = vegeDatum.Substring(0, 10).Replace('.', '-');
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            table.Rows.Add(row);

            string xml = result.Ds.GetXml();
            string xsd = result.Ds.GetXmlSchema();

            FileStream fs = new FileStream("c:\\Contentum.Net\\eTemplateManager\\sablonok\\ExcelLogDetails.xls", FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);

            byte[] fileRD = br.ReadBytes((int)fs.Length);

            br.Close();
            fs.Close();

            Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
            result = tms.GetExcelDocument(fileRD, xml, false);

            byte[] res = (byte[])result.Record;

            string outputFile = "";

            outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test6.xls";

            FileStream fst = new FileStream(outputFile, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fst);
            bw.Write(res);
            bw.Close();*/

            /*Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/msexcel";
            Response.AddHeader("content-disposition", "inline;filename=ExcelLogDetails.xls;");
            Response.AddHeader("Cache-Control", " ");
            Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
            Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
            Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");

            Response.OutputStream.Write(res, 0, res.Length);
            Response.OutputStream.Flush();
            Response.End();*/

            /*if (String.IsNullOrEmpty(result.ErrorCode))
            {
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }*/
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    private void ExcelLogDetailsTh()
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        string kezdDatum = DatumIntervallum_SearchCalendarControl1.DatumKezd.ToString();

        if (String.IsNullOrEmpty(kezdDatum))
        {
            kezdDatum = System.DateTime.Today.Subtract(new TimeSpan(90, 0, 0, 0, 0)).ToString().Substring(0, 10).Replace('.', '-') + " " + System.DateTime.Now.ToLongTimeString().ToString();
        }

        string vegeDatum = DatumIntervallum_SearchCalendarControl1.DatumVege.ToString();

        if (String.IsNullOrEmpty(vegeDatum))
        {
            vegeDatum = System.DateTime.Today.ToString().Substring(0, 10).Replace('.', '-') + " " + System.DateTime.Now.ToLongTimeString().ToString();
        }

        Result result = service.GetAllExcelLogDetails(execParam, kezdDatum, vegeDatum);

        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        DataRow row;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Datumtol";
        column.AutoIncrement = false;
        column.Caption = "Datumtol";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Datumig";
        column.AutoIncrement = false;
        column.Caption = "Datumig";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "DatumRef";
        column.AutoIncrement = false;
        column.Caption = "DatumRef";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FelhNev";
        column.AutoIncrement = false;
        column.Caption = "FelhNev";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;
        result.Ds.Tables.Add(table);

        row = table.NewRow();
        row["id"] = 0;
        row["Datumtol"] = kezdDatum.Substring(0, 10).Replace('.', '-');
        row["Datumig"] = vegeDatum.Substring(0, 10).Replace('.', '-');
        row["DatumRef"] = vegeDatum.Substring(0, 10).Replace('.', '-');
        row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
        table.Rows.Add(row);

        string xml = result.Ds.GetXml();
        string xsd = result.Ds.GetXmlSchema();

        FileStream fs = new FileStream("c:\\Contentum.Net\\eTemplateManager\\sablonok\\ExcelLogDetails.xls", FileMode.Open, FileAccess.Read);
        BinaryReader br = new BinaryReader(fs);

        byte[] fileRD = br.ReadBytes((int)fs.Length);

        br.Close();
        fs.Close();

        Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
        result = tms.GetExcelDocument(fileRD, xml, false);

        byte[] res = (byte[])result.Record;

        string outputFile = "";

        outputFile = "c:\\Documents and Settings\\kiss.gergely\\Asztal\\test6.xls";

        FileStream fst = new FileStream(outputFile, FileMode.Create);
        BinaryWriter bw = new BinaryWriter(fst);
        bw.Write(res);
        bw.Close();

        /*if (String.IsNullOrEmpty(result.ErrorCode))
        {
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }*/
    }
}
