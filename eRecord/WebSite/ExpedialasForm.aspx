<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="ExpedialasForm.aspx.cs" Inherits="ExpedialasForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="eRecordComponent/PldIratPeldanyokTextBox.ascx" TagName="PldIratPeldanyokTextBox"
    TagPrefix="uc3" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc10" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc4" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc6" %>

<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox"
    TagPrefix="uc2" %>

<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox"
    TagPrefix="uc3" %>

<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox"
    TagPrefix="uc" %>

<%@ Register Src="~/Component/TelepulesTextBox.ascx" TagName="TelepulesTextBox"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl"
    TagPrefix="uc" %>

<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc13" %>
<%@ Register Src="~/eRecordComponent/IratElosztoIvListaUserControl.ascx" TagPrefix="uc" TagName="IratElosztoIvListaUserControl" %>


<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<%@ Register src="~/Component/EditablePartnerTextBoxWithTypeFilter.ascx" tagname="EditablePartnerTextBoxWithTypeFilter" tagprefix="EPARTTBWF" %>

<%@ Register Src="~/eRecordComponent/PostaHibridPanel.ascx" TagName="PostaHibridPanel" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript" src="JavaScripts/jquery-3.2.1.min.js"></script>
    <style type="text/css">
        .panelHivataliKapu .mrUrlapCaption {
            width: 170px;
        }

        .panelHivataliKapu {
            margin-top: 5px;
        }

        .panelIratPeldany {
            margin-top: 5px;
        }
    </style>

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" AsyncPostBackTimeOut="1800">
    </asp:ScriptManager>
    <uc10:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <script type="text/javascript">
                function HivatalKapuBehavior() {
                    this.ddownKuldesMod = $get('<%=Pld_KuldesMod_KodtarakDropDownList.DropDownList.ClientID %>');
                    this.panelIratPeldany = $get('<%=panelIratPeldany.ClientID %>');
                    this.cimzettValidator = $get('<%=Pld_PartnerId_Cimzett_PartnerTextBox.Validator.ClientID %>');;
                    this.panelHivataliKapu = $get('<%=panelHivataliKapu.ClientID %>');

                    if (this.ddownKuldesMod) {
                        $addHandler(this.ddownKuldesMod, "change", Function.createDelegate(this, this.OnKuldesModChanged));
                    }

                    this.rbHivatal = $get('<%=rbCimzettTipusHivatal.ClientID %>');
                    this.rbSzemely = $get('<%=rbCimzettTipusSzemely.ClientID %>');
                    this.panelHivatal = $get('<%=panelHivatalCimzett.ClientID %>');
                    this.panelSzemely = $get('<%=panelSzemelyCimzett.ClientID %>');

                    if (this.rbHivatal) {
                        $addHandler(this.rbHivatal, "click", Function.createDelegate(this, this.OnCimzettTipusChanged));
                    }

                    if (this.rbSzemely) {
                        $addHandler(this.rbSzemely, "click", Function.createDelegate(this, this.OnCimzettTipusChanged));
                    }

                    this.hivatalKRID = $get('<%=HivatalKRID.ClientID %>');
                    this.hivatalNeve = $get('<%=HivatalNeve.ClientID %>');
                    this.hivatalRovidNeve = $get('<%=HivatalRovidNeve.ClientID %>');
                    this.hivatalMAKKod = $get('<%=HivatalMAKKod.ClientID %>');

                    if (this.hivatalKRID) {
                        $addHandler(this.hivatalKRID, "change", Function.createDelegate(this, this.OnKRIDChanged));
                    }

                    this.kapcsolatiKod = $get('<%=KapcsolatiKod.ClientID %>');
                    this.szemelyNeve = $get('<%=SzemelyNeve.ClientID %>');
                    this.szemelyEmail = $get('<%=SzemelyEmail.ClientID %>');

                    if (this.kapcsolatiKod) {
                        $addHandler(this.kapcsolatiKod, "change", Function.createDelegate(this, this.OnKapcsolatiKodChanged));
                    }

                    this.init();
                }

                HivatalKapuBehavior.prototype.init = function () {
                    if (this.ddownKuldesMod)
                        this.OnKuldesModChanged();
                    if (this.rbHivatal && this.rbSzemely)
                        this.OnCimzettTipusChanged();
                }

                HivatalKapuBehavior.prototype.OnKuldesModChanged = function (ev) {

                    var index = this.ddownKuldesMod.selectedIndex;

                    if (index > -1) {
                        var opt = this.ddownKuldesMod.options[index];

                        //Hivatali kapu
                        if (opt.value == '<%=Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu %>') {
                            this.panelHivataliKapu.style.display = '';
                            if (this.panelIratPeldany != null)
                                this.panelIratPeldany.style.display = 'none';
                            //ValidatorEnable(this.cimzettValidator, false);
                            if (this.cimzettValidator != null)
                                this.cimzettValidator.enabled = false;
                        }
                        else {
                            this.panelHivataliKapu.style.display = 'none';
                            if (this.panelIratPeldany != null)
                                this.panelIratPeldany.style.display = '';
                            //ValidatorEnable(this.cimzettValidator, true);
                            if (this.cimzettValidator != null)
                                this.cimzettValidator.enabled = true;
                        }
                    }
                }

                HivatalKapuBehavior.prototype.OnCimzettTipusChanged = function (ev) {
                    var selectedIndex = this.GetSelectedCimzettTipus();

                    if (selectedIndex == 1) {
                        this.panelHivatal.style.display = "";
                        this.panelSzemely.style.display = "none";
                    }

                    if (selectedIndex == 2) {
                        this.panelSzemely.style.display = "";
                        this.panelHivatal.style.display = "none";
                    }
                }

                HivatalKapuBehavior.prototype.GetSelectedCimzettTipus = function () {
                    if (this.rbHivatal.checked) {
                        return 1;
                    }

                    if (this.rbSzemely.checked) {
                        return 2;
                    }

                    return 0;
                }

                HivatalKapuBehavior.prototype.OnKRIDChanged = function (ev) {
                    if (this.hivatalNeve)
                        this.hivatalNeve.value = '';

                    if (this.hivatalRovidNeve)
                        this.hivatalRovidNeve.value = '';

                    if (this.hivatalMAKKod)
                        this.hivatalMAKKod.value = '';
                }

                HivatalKapuBehavior.prototype.OnKapcsolatiKodChanged = function (ev) {
                    if (this.szemelyNeve)
                        this.szemelyNeve.value = '';

                    if (this.szemelyEmail)
                        this.szemelyEmail.value = '';
                }

                HivatalKapuBehavior.getSelectedHivataliKapuFiok = function () {
                    var ddownFiok = $get('<%=KR_Fiok.ClientID %>');

                    if (ddownFiok) {
                        var index = ddownFiok.selectedIndex;

                        if (index > -1) {
                            var opt = ddownFiok.options[index];
                            return opt.value;
                        }
                    }

                    return '';
                }

                function pageLoad() {
                    var hkp = new HivatalKapuBehavior();
                }

                //--------------------------------------------------------
                //RAGSZAM IGENYLES
                //--------------------------------------------------------
                function CheckRagszamIgenyles() {

                    try {
                        var mode = $("#ctl00_ContentPlaceHolder1_HiddenFieldRagszamIgenylesModja").val();
                        if (mode == "A") {
                            CheckRagszamIgenylesAuto();
                        }
                        else if (mode == "M") {
                            CheckRagszamIgenylesManual()
                        }
                    } catch (e) {
                        // console.log(e);
                    }
                }

                function CheckRagszamIgenylesManual() {

                    try {
                        var tbRagszam = document.getElementById('ctl00_ContentPlaceHolder1_TextBoxPostaiRagszam');

                        var tbRagszamValue = tbRagszam.value;
                        if (tbRagszamValue == "")
                            return;

                        dataValue = JSON.stringify('M|' + tbRagszamValue);
                        $.ajax({
                            type: "POST",
                            url: "ExpedialasForm.aspx/CheckRagszamIgenyles",
                            data: '{allParameters: ' + dataValue + ' }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: OnSuccess,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    } catch (e) {
                        // console.log(e);
                    }
                }
                function CheckRagszamIgenylesAuto() {
                    try {
                        var ddPostakonyv = document.getElementById('ctl00_ContentPlaceHolder1_DropDownListPostaiRagszamPostakonyv');
                        var ddKuldfajta = document.getElementById('ctl00_ContentPlaceHolder1_DropDownListPostaiRagszamKuldemenyFajtaja');

                        var ddPostakonyvValue = ddPostakonyv.options[ddPostakonyv.selectedIndex].value;
                        var ddKuldfajtaValue = ddKuldfajta.options[ddKuldfajta.selectedIndex].value;

                        dataValue = JSON.stringify('A|' + ddPostakonyvValue + '|' + ddKuldfajtaValue);
                        $.ajax({
                            type: "POST",
                            url: "ExpedialasForm.aspx/CheckRagszamIgenyles",
                            data: '{allParameters: ' + dataValue + ' }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: OnSuccess,
                            failure: function (response) {
                                alert(response.d);
                            }
                        });
                    } catch (e) {
                        // console.log(e);
                    }
                }
                function OnSuccess(response) {
                    if (response["d"] != "") {
                        $("#ctl00_ContentPlaceHolder1_LabelErrorMessageRagszamCheck").html(response["d"]);
                        $("#ctl00_ContentPlaceHolder1_LabelOkMessageRagszamCheck").html("");
                    }
                    else {
                        $("#ctl00_ContentPlaceHolder1_LabelOkMessageRagszamCheck").html("OK");
                        $("#ctl00_ContentPlaceHolder1_LabelErrorMessageRagszamCheck").html("");
                    }
                    //alert(response.d);
                }
                //--------------------------------------------------------

            </script>

            <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,ExpedialasFormHeaderTitle %>" />

            <div class="popupBody">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <%--<tr class="urlapElvalasztoSor">
                </tr>--%>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption_shorter">
                                    <asp:Panel ID="PanelGombok" runat="server" Visible="true">
                                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:ImageButton ID="Megtekintes" runat="server" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg" /></td>
                                                <td style="text-align: center; padding-left: 5px;">
                                                    <asp:ImageButton ID="Torles" runat="server" ImageUrl="~/images/hu/trapezgomb/torles_trap.jpg" OnClick="Torles_Click" /></td>
                                                <td style="width: 100%"></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td>
                                    <eUI:eFormPanel ID="TreeView_EFormPanel" runat="server">

                                        <asp:TreeView ID="TreeView1" runat="server" EnableClientScript="true"
                                            BackColor="White" ShowLines="True" PopulateNodesFromClient="True" CollapseImageToolTip="" ExpandImageToolTip="" SkipLinkText="" NodeIndent="40" NodeWrap="True">
                                            <SelectedNodeStyle Font-Bold="False" CssClass="tvSelectedRowStyle" />
                                        </asp:TreeView>
                                        <asp:HiddenField ID="hfUgyintezesModja" runat="server" />
                                    </eUI:eFormPanel>
                                </td>
                                <td style="width: 50%;"></td>
                            </tr>
                        </tbody>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="100%" style="text-align: left;">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short" style="padding-top: 10px;">
                                <asp:Label ID="Label1" runat="server" Text="Iratp�ld�ny kiv�laszt�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:PldIratPeldanyokTextBox ID="PldIratPeldanyokTextBox1" runat="server" Validate="false" FilterForExpedialas="true" />
                            </td>
                            <td class="mrUrlapMezo" style="padding-left: 10px; padding-top: 5px;">
                                <asp:ImageButton ID="Hozzaadas" runat="server" ImageUrl="~/images/hu/trapezgomb/hozzaad_trap.jpg" OnClick="Hozzaadas_Click" />
                            </td>
                            <td class="mrUrlapMezo" style="width: 300px;"></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelVonalkod" runat="server" Text="Vonalk�d:"></asp:Label></td>
                            <td class="mrUrlapMezo" colspan="3">
                                <uc7:VonalKodTextBox ID="VonalKodTextBox1" runat="server" RequiredValidate="false" />
                                <asp:CheckBox ID="cbAblakosBoritek" runat="server" Text="Ablakos bor�t�k" Checked="false" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelKuldesModja" runat="server" Text="Expedi�l�s m�dja:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:KodtarakDropDownList ID="Pld_KuldesMod_KodtarakDropDownList" runat="server" AutoPostBack="true" />
                            </td>
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelKR_Fiok" runat="server" Text="Hivatali kapu fi�k:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="KR_Fiok" runat="server" CssClass="mrUrlapInputComboBox" />
                            </td>
                        </tr>

                        <asp:HiddenField runat="server" ID="HiddenFieldRagszamIgenylesModja" />
                        <tr class="urlapSor" runat="server" id="TRPostaiRagszamBeviteliMezo">
                            <td class="mrUrlapCaption_short" runat="server">
                                <asp:Label runat="server" Text="Postai ragsz�m:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="TextBoxPostaiRagszam" runat="server" CausesValidation="true" onblur="CheckRagszamIgenyles();" Width="300px" />
                                <%--<asp:TextBox ID="TextBoxPostaiRagszam" runat="server" CssClass="mrUrlapInput" />--%>
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>

                        <tr class="urlapSor" runat="server" id="TRPostaiRagszamPostakonyv">
                            <td class="mrUrlapCaption_short">
                                <asp:Label runat="server" Text="PostaK�nyv:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList runat="server" ID="DropDownListPostaiRagszamPostakonyv" Width="300px"
                                    AutoPostBack="true" />
                                <input type="button" onclick="CheckRagszamIgenyles();" value="Ragsz�ms�v ellen�rz�s" />
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>

                        <tr class="urlapSor" runat="server" id="TRPostaiRagszamKuldemenyFajtaja">
                            <td class="mrUrlapCaption_short">
                                <asp:Label runat="server" Text="K�ldem�ny fajt�ja:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList runat="server" ID="DropDownListPostaiRagszamKuldemenyFajtaja" Width="300px"
                                    AutoPostBack="true" />
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                    </table>
                    <asp:Label runat="server" ID="LabelErrorMessageRagszamCheck" ForeColor="Red" Font-Size="Smaller" Text="" />
                    <asp:Label runat="server" ID="LabelOkMessageRagszamCheck" ForeColor="Green" Font-Size="Smaller" Text="" />
                </eUI:eFormPanel>

                <eUI:eFormPanel runat="server" ID="panelIratPeldany" CssClass="panelIratPeldany">
                    <table cellspacing="0" cellpadding="0" width="100%" style="text-align: left;">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="LabelCimzettReq" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="LabelCimzett" runat="server" Text="C�mzett neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <EPARTTBWF:EditablePartnerTextBoxWithTypeFilter ID="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" />
                                <%--<uc2:PartnerTextBox ID="Pld_PartnerId_Cimzett_PartnerTextBox" runat="server" />--%>
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="labelCimzettCime" runat="server" Text="C�mzett c�me:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CimekTextBox ID="Pld_CimId_Cimzett_CimekTextBox" runat="server" Validate="false" />
                            </td>
                            <td class="mrUrlapCaption_short"></td>
                            <td class="mrUrlapMezo"></td>
                        </tr>
                    </table>
                </eUI:eFormPanel>

                <eUI:eFormPanel runat="server" ID="EFormPanelCimzettLista" CssClass="panelIratPeldany" Visible="false">
                    <uc:IratElosztoIvListaUserControl runat="server" ID="IratElosztoIvListaUserControl" />
                </eUI:eFormPanel>

                <eUI:eFormPanel ID="panelHivataliKapu" runat="server" CssClass="panelHivataliKapu">
                    <div style="height: 10px">
                    </div>
                    <div style="text-align: center">
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbCimzettTipusHivatal" runat="server" Text="Hivatal/C�gkapu" Checked="true" GroupName="CimzettTipus" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="rbCimzettTipusSzemely" runat="server" Text="Term�szetes szem�ly" GroupName="CimzettTipus" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 10px">
                    </div>
                    <asp:Panel ID="panelHivatalCimzett" runat="server" Style="text-align: left;">
                        <table cellspacing="0" cellpadding="0">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHivatalNeve" runat="server" Text="Hivatal neve:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="HivatalNeve" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="">
                                    <asp:ImageButton ID="searchHivatal" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap.jpg"
                                        onmouseover="swapByName(this.id,'kereses_trap2.jpg')" onmouseout="swapByName(this.id,'kereses_trap1.jpg')"
                                        Style="vertical-align: middle; margin-left: 5px; margin-top: 1px" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHivatalKRIDReq" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelHivatalKRID" runat="server" Text="Hivatal KRID-ja:"/>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="HivatalKRID" runat="server" CssClass="mrUrlapInput" />
                                    <asp:HiddenField ID="HivatalRovidNeve" runat="server" />
                                    <asp:HiddenField ID="HivatalMAKKod" runat="server" />
                                </td>
                                <td class=""></td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="panelSzemelyCimzett" runat="server" Style="text-align: left;">
                        <table cellspacing="0" cellpadding="0">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label2" runat="server" Text="Sz�let�si n�v vezet�kn�v:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="SzuletesiNevVezetekNev" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label3" runat="server" Text="Sz�let�si n�v ut�n�v1:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="SzuletesiNevUtonev1" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label4" runat="server" Text="Sz�let�si n�v ut�n�v2:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="SzuletesiNevUtonev2" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption"></td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label5" runat="server" Text="Viselt n�v vezet�kn�v:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="ViseltNevVezetekNev" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label6" runat="server" Text="Viselt n�v ut�n�v1:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="ViseltNevUtonev1" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label7" runat="server" Text="Viselt n�v ut�n�v2:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="ViseltNevUtonev2" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption"></td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label8" runat="server" Text="Anyja neve vezet�kn�v:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="AnyjaNeveVezetekNev" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label9" runat="server" Text="Anyja neve ut�n�v1:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="AnyjaNeveUtonev1" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label10" runat="server" Text="Anyja neve ut�n�v2:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="AnyjaNeveUtonev2" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption"></td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label11" runat="server" Text="Sz�let�si hely:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:TelepulesTextBox ID="SzuletesiHely" runat="server" CssClass="mrUrlapInput" Validate="false" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label12" runat="server" Text="Sz�let�si d�tum:" />
                                </td>
                                <td class="mrUrlapMezo">

                                    <table cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <uc:CalendarControl ID="SzuletesDatuma" runat="server" CssClass="mrUrlapInput" Validate="false" />
                                            </td>
                                            <td style="text-align: right">
                                                <asp:ImageButton ID="searhSzemely" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap.jpg"
                                                    onmouseover="swapByName(this.id,'kereses_trap2.jpg')" onmouseout="swapByName(this.id,'kereses_trap2.jpg')"
                                                    Style="vertical-align: middle; margin-top: 1px" OnClick="searhSzemely_Click" />
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKapcsolatiKodReq" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelKapcsolatiKod" runat="server" Text="Kapcsolati k�d:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="KapcsolatiKod" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption"></td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label13" runat="server" Text="N�v:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="SzemelyNeve" runat="server" CssClass="mrUrlapInput ReadOnlyWebControl" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="label14" runat="server" Text="E-mail:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="SzemelyEmail" runat="server" CssClass="mrUrlapInput" ReadOnly="true" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <div style="height: 10px">
                    </div>
                    <div style="text-align: left;">
                        <table cellspacing="0" cellpadding="0">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKR_DokTipusHivatal" runat="server" Text="Hivatal:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="KR_DokTipusHivatal" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKR_DokTipusAzonosito" runat="server" Text="T�pus azonos�t�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="KR_DokTipusAzonosito" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKR_DokTipusLeiras" runat="server" Text="T�pus le�r�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="KR_DokTipusLeiras" runat="server" CssClass="mrUrlapInput" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelKR_Megjegyzes" runat="server" Text="Megjegyz�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="KR_Megjegyzes" runat="server" CssClass="mrUrlapInput" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </eUI:eFormPanel>

                <uc:PostaHibridPanel ID="panelPostaHibrid" runat="server" style="margin-top: 5px;" Visible="false"/>

                <%-- CR3152 - Expedi�l�s folyamat t�meges�t�se --%>
                <asp:Panel ID="TomegesPanel" runat="server" Visible="false">
                    <div>
                        <uc13:InfoModalPopup ID="InfoModalPopup1" runat="server" />
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                    <br />
                                    <asp:Panel ID="IratPeldanyokListPanel" runat="server">
                                        <asp:Label ID="Label_IratPeldanyok" runat="server" Text="Iratp�ld�nyok:" CssClass="GridViewTitle"></asp:Label>
                                        <br />
                                        <asp:GridView ID="PldIratPeldanyokGridView" runat="server"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id"
                                            OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText"  AutoPostBack='<%#  Contentum.eUtility.KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu.Equals(Eval("KuldesMod")) %>'/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                            Visible="false" OnClientClick="return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktat�sz�m"
                                                    SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="T�rgy" SortExpression="EREC_IraIratok_Targy">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%--<asp:BoundField DataField="EREC_KuldKuldemenyek_PostazasIranya" HeaderText="Ir�ny" SortExpression="EREC_KuldKuldemenyek_PostazasIranya">
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <HeaderStyle CssClass="GridViewBorderHeader" />
                                    </asp:BoundField> --%>
                                                <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Hat�rid�" SortExpression="VisszaerkezesiHatarido">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="C�mzett" SortExpression="NevSTR_Cimzett">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CimSTR_Cimzett" HeaderText="C�mzett c�me" SortExpression="CimSTR_Cimzett">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Expedi�l�s m�dja" SortExpression="KuldesMod_Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelKuldesModNev" runat="server" Text='<%# Eval("KuldesMod_Nev") %>' />
                                                        <asp:Label ID="labelKuldesMod" runat="server" Text='<%# Eval("KuldesMod") %>' style="display:none;"/>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>

                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">
                                        <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <eUI:eFormPanel ID="PanelTomegesKR_Fiok" runat="server">
                            <div style="text-align:left;padding-left: 5px;">
                                <table cellspacing="0" cellpadding="0">
                                    <td class="mrUrlapCaption_nowidth">
                                        <asp:Label ID="labelTomegesKR_Fiok" runat="server" Text="Hivatali kapu fi�k:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="TomegesKR_Fiok" runat="server" CssClass="mrUrlapInputComboBox" />
                                    </td>
                                </table>
                            </div>
                         </eUI:eFormPanel>
                    </div>
                </asp:Panel>
                <%--CR3152 - Expedi�l�s folyamat t�meges�t�se --%>

                <eUI:eFormPanel ID="panelCsatolmanyok" runat="server" Style="margin-top: 5px;">
                    <uc:DokumentumVizualizerComponent ID="DokumentumVizualizerComponent" runat="server" />
                    <div style="text-align: left; width: 98%; padding-bottom: 3px;" id="PackPanel" runat="server">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="cbPackDokumentumok" runat="server"
                                    Text="Csatolm�nyok �sszecsomagol�sa"
                                    Checked="True" />
                                </td>
                                <td class="mrUrlapCaption">
                                    Csomagolt f�jl neve:
                                </td>
                                <td>
                                     <asp:TextBox ID="PackedFileName" runat="server" CssClass="mrUrlapInput"/>
                                </td>
                                <td class="mrUrlapCaption_nowidth">
                                    <asp:DropDownList ID="PackedFileFormat" runat="server">
                                        <asp:ListItem Text=".zip" Value="0" />
                                        <asp:ListItem Text=".krx" Value="1" />
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td colspan="4" style="text-align:right">
                                    <asp:CheckBox ID="cbEncrypt" runat="server" Text="Csatolm�nyok titkos�t�sa" Checked="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div>
                        <asp:HiddenField ID="hfIsAnyKrFileSelected" runat="server" />
                        <asp:GridView ID="CsatolmanyokGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                            AllowSorting="True" DataKeyNames="Id" AutoGenerateColumns="False" Width="98%"
                            OnRowCommand="CsatolmanyokGridView_RowCommand" OnRowDataBound="CsatolmanyokGridView_RowDataBound"
                            OnSorting="CsatolmanyokGridView_Sorting">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                    <HeaderTemplate>
                                        <span style="white-space: nowrap">
                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kispipa.jpg"
                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                            &nbsp;&nbsp;
                                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="~/images/hu/egyeb/kisiksz.jpg"
                                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                        </span>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="check" runat="server" AutoPostBack='<%# IsKrFile(Eval("FajlNev") as string) %>' Text='<%# Eval("Id") %>'
                                            CssClass="HideCheckBoxText" CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                    <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="GridViewSelectRowImage" />
                                </asp:CommandField>
                                <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktat�sz�m" SortExpression="IktatoSzam_Merge">
                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="�llom�ny" SortExpression="FajlNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <ItemTemplate>
                                        <asp:Label ID="labelFajlNev" runat="server" Text='<%# Eval("FajlNev") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" SortExpression="KRT_Dokumentumok.Formatum">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="0px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="fileIcon_ImageButton" runat="server" Enabled="false" OnClientClick="return false;"
                                            ImageUrl="images/hu/fileicons/default.gif" ToolTip="Megnyit�s" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Leiras" HeaderText="Megjegyz�s" SortExpression="Leiras">
                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Verzi�" SortExpression="KRT_Dokumentumok.VerzioJel">
                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="linkVerzio" runat="server" NavigateUrl='<%# Contentum.eRecord.Utility.Dokumentumok.GetVerziokLink(Eval("krtDok_Id"),Eval("VerzioJel")) %>'
                                            Text='<%# Eval("VerzioJel") %>' ToolTip="Kor�bbi verzi�k megtekint�se" Visible='<%# Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                        <asp:Label ID="labelVerzio" runat="server" Text='<%# Eval("VerzioJel") %>' Visible='<%# !Contentum.eRecord.Utility.Dokumentumok.IsMoreVerzio(Eval("VerzioJel")) %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Formatum_Nev" HeaderText="Form�tum" SortExpression="Formatum_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLovListInvisibleCoulumnStyle" HeaderStyle-CssClass="GridViewLovListInvisibleCoulumnStyle">
                                    <ItemTemplate>
                                        <asp:Label ID="Label_krtDok_Id" runat="server" Text='<%# Eval("krtDok_Id") %>' />
                                        <asp:Label ID="labelExternal_Link" runat="server" Text='<%# Eval("External_Link") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </div>
                </eUI:eFormPanel>

                <asp:Panel ID="panelFooter" runat="server">
                    <table style="width: 50%; text-align: center;">
                        <tr>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="ImageSave" runat="server"
                                    ImageUrl="~/images/hu/ovalgomb/mentes.gif"
                                    onmouseover="swapByName(this.id,'mentes2.gif')" onmouseout="swapByName(this.id,'mentes.gif')"
                                    CommandName="Save" OnClick="ImageSave_Click" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="ImageExpedialas" runat="server"
                                    ImageUrl="~/images/hu/ovalgomb/expedialas.gif"
                                    onmouseover="swapByName(this.id,'expedialas2.gif')" onmouseout="swapByName(this.id,'expedialas.gif')"
                                    CommandName="Expedialas" OnClick="ImageExpedialas_Click" />
                            </td>
                            <td style="text-align: center;">
                                <asp:ImageButton ID="ImageCancel" runat="server"
                                    ImageUrl="~/images/hu/ovalgomb/megsem.jpg"
                                    onmouseover="swapByName(this.id,'megsem2.jpg')" onmouseout="swapByName(this.id,'megsem.jpg')"
                                    OnClientClick="window.close(); return false;" CommandName="Cancel" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>

