using Contentum.eAdmin.BaseUtility.KodtarFuggoseg;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eBusinessDocuments.PH;
using Contentum.eIntegrator.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

public partial class ExpedialasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    //private ComponentSelectControl compSelector = null;

    #region CR3152 - Expedi�l�s folyamat t�meges�t�se
    private String[] IratPeldanyokArray;
    private UI ui = new UI();
    public string maxTetelszam = "0";
    #endregion
    private const string kodcsoportExpedialasFontossag = "Expedialas_FONTOSSAG";
    private const string funkciokod_Expedialas = "Expedialas";

    private string IratPeldany_Id = "";

    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";

    private string IratPeldany_Vonalkod
    {
        set { ViewState["PldVonalkod"] = value; }
        get
        {
            if (ViewState["PldVonalkod"] == null)
            {
                return String.Empty;
            }
            else
            {
                return ViewState["PldVonalkod"].ToString();
            }
        }
    }

    private bool IsKuldesModHivataliKapu
    {
        get
        {
            return KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu.Equals(Pld_KuldesMod_KodtarakDropDownList.SelectedValue);
        }
    }

    private bool IsKuldesModElhisz
    {
        get
        {
            return KodTarak.KULDEMENY_KULDES_MODJA.Elhisz.Equals(Pld_KuldesMod_KodtarakDropDownList.SelectedValue);
        }
    }

    private bool IsKuldesModPostaHibrid
    {
        get
        {
            return KodTarak.KULDEMENY_KULDES_MODJA.PostaHibrid.Equals(Pld_KuldesMod_KodtarakDropDownList.SelectedValue);
        }
    }


    private bool IsHivatalCimzett
    {
        get
        {
            return rbCimzettTipusHivatal.Checked;
        }
    }

    private bool IsSzemelyCimzett
    {
        get
        {
            return rbCimzettTipusSzemely.Checked;
        }
    }

    //private bool IsCimzettListasExpedialas = false;


    bool IsCsatolmanySelectionEnabled
    {
        get
        {
            return IsKuldesModHivataliKapu || IsKuldesModElhisz || IsKuldesModPostaHibrid
                || (IsTomegesCsatolmanySelectionEnabled && !String.IsNullOrEmpty(IratId));
        }
    }

    bool IsPackCsatolmanyokEnabled
    {
        get
        {
            return IsCsatolmanySelectionEnabled && !IsKuldesModElhisz && !IsKuldesModPostaHibrid;
        }
    }

    bool PackDokumentumok
    {
        get
        {
            return cbPackDokumentumok.Checked;
        }
    }

    bool IsTomegesExpedialas { get; set; }

    bool IsTomegesCsatolmanySelectionEnabled
    {
        get
        {
            object o = ViewState["IsTomegesCsatolmanySelectionEnabled"];

            if (o != null)
                return (bool)o;

            return false;
        }
        set
        {
            ViewState["IsTomegesCsatolmanySelectionEnabled"] = value;
        }
    }

    string IratId
    {
        get
        {
            object o = ViewState["IratId"];

            if (o != null)
                return (string)o;

            return String.Empty;
        }
        set
        {
            ViewState["IratId"] = value;
        }
    }

    #region CR3152 - Expedi�l�s folyamat t�meges�t�se
    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }
    #endregion CR3152 - Expedi�l�s folyamat t�meges�t�se

    protected override void OnInit(EventArgs e)
    {
        Contentum.eUIControls.FuggoKodtarExtender.Add_UGYINTEZES_ALAPJA_KULDEMENY_KULDES_MODJA(Page, Pld_KuldesMod_KodtarakDropDownList.DropDownList.UniqueID, hfUgyintezesModja.ClientID);
        base.OnInit(e);

    }

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        IratPeldany_Id = Request.QueryString.Get(QueryStringVars.IratPeldanyId);

        if (String.IsNullOrEmpty(IratPeldany_Id))
        {
            string iratId = Request.QueryString.Get(QueryStringVars.IratId);

            if (!String.IsNullOrEmpty(iratId))
            {
                List<string> peldanyok = GetIratPeldanyai(iratId);
                IratPeldany_Id = String.Join(",", peldanyok.ToArray());

                IsTomegesExpedialas = peldanyok.Count > 1;
            }
            else
            {
                if ("1".Equals(Session["Tomeges"]))
                {
                    if (Session["SelectedIratPeldanyIds"] != null)
                    {
                        IratPeldany_Id = Session["SelectedIratPeldanyIds"].ToString();
                        IsTomegesExpedialas = IratPeldany_Id.Contains(",");
                    }
                }
            }

        }

        #region CR3152 - Expedi�l�s folyamat t�meges�t�se
        if (IsTomegesExpedialas)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkciokod_Expedialas);
            FormHeader1.DisableModeLabel = true;
            FormHeader1.HeaderTitle = Resources.Form.ExpedialasTomegesFormHeaderTitle;
        }

        #endregion CR3152 - Expedi�l�s folyamat t�meges�t�se

        Command = Request.QueryString.Get(CommandName.Command);

        FormHeader1.DisableModeLabel = true;

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                //compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                //FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkciokod_Expedialas);
                break;
        }



        //if (String.IsNullOrEmpty(IratPeldany_Id))
        //{
        //    // nincs Id megadva:
        //    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //}
        //else
        //{
        //    if (!IsPostBack)
        //    {
        //        InitTreeViewByIratPeldany(IratPeldany_Id);
        //    }
        //}

        panelPostaHibrid.ErrorPanel = FormHeader1.ErrorPanel;
        Pld_CimId_Cimzett_CimekTextBox.SetCimTipusFilter(Pld_KuldesMod_KodtarakDropDownList.DropDownList, Pld_PartnerId_Cimzett_PartnerTextBox.BehaviorID);
    }

    /// <summary>
    /// Oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //FormFooter1.ButtonsClick += new
        //   System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (IsPostBack)
        {
            if (FormHeader1.ErrorPanel.Visible == true)
            {
                FormHeader1.ErrorPanel.Visible = false;
                //ErrorUpdatePanel1.Update();
            }
        }


        if (IsTomegesExpedialas)
        {
            #region CR3152 - Expedi�l�s folyamat t�meges�t�se

            EFormPanel1.Visible = false;
            panelIratPeldany.Visible = false;
            panelHivataliKapu.Visible = false;
            panelPostaHibrid.Visible = false;
            ImageSave.Visible = false;
            TomegesPanel.Visible = true;
            if (!String.IsNullOrEmpty(IratPeldany_Id))
            {
                IratPeldanyokArray = IratPeldany_Id.Split(',');
            }
            if (!IsPostBack)
            {
                InitTomegesExpedialas();
            }
            int cntKijeloltIratpeldanyok = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count;
            labelTetelekSzamaDb.Text = cntKijeloltIratpeldanyok.ToString();
            #endregion CR3152 - Expedi�l�s folyamat t�meges�t�se

            registerJavascripts();
        }
        else
        {
            string js_buttonDisabled = JavaScripts.SetDisableButtonOnClientClick(Page, Hozzaadas);

            Hozzaadas.OnClientClick = "if ($get('" + PldIratPeldanyokTextBox1.HiddenField.ClientID + "').value == '') "
                    + " { alert('" + Resources.Error.UINoSelectedItem + "'); return false; } "
                    + js_buttonDisabled;

            //ImageExpedialas.OnClientClick = "if ($get('" + VonalKodTextBox1.TextBox.ClientID + "').value == '') "
            //        + " { alert('" + Resources.Error.UINoBarCode + "'); return false; } "
            //        + JavaScripts.SetDisableButtonOnClientClick(Page, ImageExpedialas);

            string js_pldselected = @"var hf=$get('" + PldIratPeldanyokTextBox1.HiddenField.ClientID + "'); if (hf && hf.value && hf.value != '') { return confirm('Kiv�laszott egy iratp�ld�nyt, de nem adta hozz� a f�hoz. Ha folytatja a m�veletet, a kiv�lasztott iratp�ld�ny nem ker�l bele a kimen� k�ldem�nybe. K�v�nja folytatni a m�veletet?'); }";
            ImageSave.OnClientClick = js_pldselected;

            ImageExpedialas.OnClientClick = js_pldselected + JavaScripts.SetDisableButtonOnClientClick(Page, ImageExpedialas);

            registerJavascripts();

            if (Command == CommandName.DesignView)
            {
                Load_ComponentSelectModul();
            }
            else
            {
                pageView.SetViewOnPage(Command);
            }


            Pld_PartnerId_Cimzett_PartnerTextBox.CimHiddenField = Pld_CimId_Cimzett_CimekTextBox.HiddenField;
            Pld_PartnerId_Cimzett_PartnerTextBox.CimTextBox = Pld_CimId_Cimzett_CimekTextBox.TextBox;

            if (String.IsNullOrEmpty(IratPeldany_Id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                if (!IsPostBack)
                {
                    InitTreeViewByIratPeldany(IratPeldany_Id);
                }
                //cbAblakosBoritek.Enabled = (TreeView1.Nodes.Count == 1 && TreeView1.Nodes[0].ChildNodes.Count == 1);
                //if (!cbAblakosBoritek.Enabled || !IsPostBack)
                //{
                //    cbAblakosBoritek.Checked = false;
                //}
                cbAblakosBoritek.CheckedChanged += new EventHandler(cbAblakosBoritek_CheckedChanged);
            }

            string query = "&" + QueryStringVars.HiddenFieldId + "=" + HivatalKRID.ClientID
                           + "&" + QueryStringVars.TextBoxId + "=" + HivatalNeve.ClientID
                           + "&" + "RovidNevHiddenFieldId" + "=" + HivatalRovidNeve.ClientID
                           + "&" + "MAKKodHiddenFieldId" + "=" + HivatalMAKKod.ClientID;

            query += "&InitText=' + encodeURIComponent($get('" + HivatalNeve.ClientID + "').value) + '";
            query += "&Fiok=' + HivatalKapuBehavior.getSelectedHivataliKapuFiok() + '";
            searchHivatal.OnClientClick = JavaScripts.SetOnClientClick("HKP_HivatalLovList.aspx", query, Defaults.PopupWidth, Defaults.PopupHeight, "", "", false);

            HivatalNeve.Attributes["onchange"] = "$get('" + HivatalKRID.ClientID + "').value = '';";
        }

        SetControlsByRendszerParameterRagszamKiosztasModja();

        if (!Page.IsPostBack)
        {
            InitializeCimzettLista();

            //LZS BUG_5221
            if (IsTomegesExpedialas)
            {
                panelIratPeldany.Visible = false;
            }
        }

        if (!IsPostBack)
        {
            PackedFileName.Text = String.Format("{0}_{1:yyyyMMddHHmmss}", GetDokTipusHivatal(), DateTime.Now);
            FillFiokokDropDownList();
        }
    }

    void cbAblakosBoritek_CheckedChanged(object sender, EventArgs e)
    {
        SetAblakosBoritekComponents();
    }

    private void LoadComponentsFromBusinessObject(EREC_PldIratPeldanyok erec_PldIratPeldanyok)
    {
        if (erec_PldIratPeldanyok != null)
        {
            IratPeldany_Vonalkod = erec_PldIratPeldanyok.BarCode;

            Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField = erec_PldIratPeldanyok.Partner_Id_Cimzett;
            Pld_PartnerId_Cimzett_PartnerTextBox.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;
            // BUG_13213
            //Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = String.Empty;
            Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue = erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt;

            Pld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
                erec_PldIratPeldanyok.KuldesMod, true, FormHeader1.ErrorPanel);

            Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField = erec_PldIratPeldanyok.Cim_id_Cimzett;
            string cimToSearch = erec_PldIratPeldanyok.CimSTR_Cimzett;
            if (string.IsNullOrEmpty(cimToSearch))
            {
                cimToSearch = Contentum.eUtility.CimUtility.GetCimSTRById(UI.SetExecParamDefault(Page, new ExecParam()), erec_PldIratPeldanyok.Cim_id_Cimzett);
            }
            Pld_CimId_Cimzett_CimekTextBox.Text = cimToSearch;
            FormHeader1.Record_Ver = erec_PldIratPeldanyok.Base.Ver;

            //hivatali kapu
            if (!String.IsNullOrEmpty(erec_PldIratPeldanyok.CimSTR_Cimzett))
            {
                //term�szetes szem�ly
                if (erec_PldIratPeldanyok.CimSTR_Cimzett.Length == 32)
                {
                    rbCimzettTipusSzemely.Checked = true;
                    SzemelyNeve.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;
                    KapcsolatiKod.Text = erec_PldIratPeldanyok.CimSTR_Cimzett;
                }
                else
                {
                    HivatalNeve.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;
                    HivatalKRID.Text = erec_PldIratPeldanyok.CimSTR_Cimzett;

                    SzemelyNeve.Text = erec_PldIratPeldanyok.NevSTR_Cimzett;
                    KapcsolatiKod.Text = erec_PldIratPeldanyok.CimSTR_Cimzett;
                }
            }

            if (!string.IsNullOrEmpty(erec_PldIratPeldanyok.Partner_Id_Cimzett))
            {
                GetAndSetSzemelyAdatok(erec_PldIratPeldanyok.Partner_Id_Cimzett);
            }

            KR_DokTipusHivatal.Text = GetDokTipusHivatal();
        }
    }

    private void SetVonalkodComponentsKimenoKuldemeny(string kimenoKuldemenyVonalkod, string iratpldVonalkod)
    {
        if (kimenoKuldemenyVonalkod != null)
        {
            VonalKodTextBox1.Text = kimenoKuldemenyVonalkod;

            if (kimenoKuldemenyVonalkod == iratpldVonalkod)
            {
                cbAblakosBoritek.Checked = true;
            }
        }

        var a = Pld_PartnerId_Cimzett_PartnerTextBox.Validator;
    }

    private EREC_PldIratPeldanyok GetBusinessObjectFromComponents()
    {
        EREC_PldIratPeldanyok erec_PldIratPeldanyok = new EREC_PldIratPeldanyok();
        erec_PldIratPeldanyok.Updated.SetValueAll(false);
        erec_PldIratPeldanyok.Base.Updated.SetValueAll(false);

        erec_PldIratPeldanyok.KuldesMod = Pld_KuldesMod_KodtarakDropDownList.SelectedValue;
        erec_PldIratPeldanyok.Updated.KuldesMod = pageView.GetUpdatedByView(Pld_KuldesMod_KodtarakDropDownList);

        if (IsKuldesModHivataliKapu)
        {
            erec_PldIratPeldanyok.NevSTR_Cimzett = GetHivataliKapuCimzett();
            erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = true;

            if (IsHivatalCimzett)
                erec_PldIratPeldanyok.CimSTR_Cimzett = HivatalKRID.Text;
            else
                erec_PldIratPeldanyok.CimSTR_Cimzett = KapcsolatiKod.Text;

            erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = true;
        }
        else
        {
            erec_PldIratPeldanyok.Partner_Id_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Id_HiddenField;
            erec_PldIratPeldanyok.Updated.Partner_Id_Cimzett = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);
			
			// BUG:13213
            erec_PldIratPeldanyok.Partner_Id_CimzettKapcsolt = Pld_PartnerId_Cimzett_PartnerTextBox.KapcsoltPartnerDropDownValue;
            erec_PldIratPeldanyok.Updated.Partner_Id_CimzettKapcsolt = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

            erec_PldIratPeldanyok.NevSTR_Cimzett = Pld_PartnerId_Cimzett_PartnerTextBox.Text;
            erec_PldIratPeldanyok.Updated.NevSTR_Cimzett = pageView.GetUpdatedByView(Pld_PartnerId_Cimzett_PartnerTextBox);

            erec_PldIratPeldanyok.Cim_id_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Id_HiddenField;
            erec_PldIratPeldanyok.Updated.Cim_id_Cimzett = pageView.GetUpdatedByView(Pld_CimId_Cimzett_CimekTextBox);

            erec_PldIratPeldanyok.CimSTR_Cimzett = Pld_CimId_Cimzett_CimekTextBox.Text;
            erec_PldIratPeldanyok.Updated.CimSTR_Cimzett = pageView.GetUpdatedByView(Pld_CimId_Cimzett_CimekTextBox);
        }

        erec_PldIratPeldanyok.Base.Ver = FormHeader1.Record_Ver;
        erec_PldIratPeldanyok.Base.Updated.Ver = true;

        return erec_PldIratPeldanyok;
    }

    HivataliKapuFileSettings GetFileSettingsFromComponents()
    {
        HivataliKapuFileSettings fileSettings = new HivataliKapuFileSettings();
        fileSettings.PackFiles = PackDokumentumok;
        fileSettings.PackedFileName = PackedFileName.Text;
        fileSettings.FileFormat = (HivataliKapuFileSettings.HivataliKapuFileFormat)Enum.Parse(typeof(HivataliKapuFileSettings.HivataliKapuFileFormat), PackedFileFormat.SelectedValue);
        fileSettings.EncryptFiles = cbEncrypt.Checked;
        return fileSettings;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // ha van kiv�lasztva valami a treeView-ban, a megtekint�s gombot be kell r� �ll�tani
        if (TreeView1.SelectedNode != null)
        {
            int depth = TreeView1.SelectedNode.Depth;
            //string id = TreeView1.SelectedNode.Value;

            if (depth == 0)
            {
                string id = TreeView1.SelectedNode.Value;

                // irat
                Megtekintes.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            }
            else if (depth == 1)
            {
                // iratP�ld�ny:
                string id = GetTreeNodePldId(TreeView1.SelectedNode);

                Megtekintes.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("PldIratPeldanyokForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            }
            else
            {
                Megtekintes.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            }
        }
        else
        {
            Megtekintes.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        }
        //if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).ToUpper().Trim().Equals("AZONOSITO"))
        //{
        //    ImageSave.Visible = false;
        //    ImageSave.Enabled = false;
        //}
        SetAblakosBoritekComponents();

        SzemelyNeve.Attributes.Add("readonly", "readonly");

        IsTomegesCsatolmanySelectionEnabled = ExistSelectedHivataliKapuPeldany();

        panelCsatolmanyok.Visible = IsCsatolmanySelectionEnabled;

        PanelTomegesKR_Fiok.Visible = ExistSelectedHivataliKapuPeldany();

        ui.SetClientScriptToGridViewSelectDeSelectButton(CsatolmanyokGridView);

        if (IsCsatolmanySelectionEnabled)
        {
            if (!IsKuldesModPostaHibrid)
            {
                string js = String.Format("var count = getSelectedCheckBoxesCount('{0}','check'); if(count == 0) {{alert('Nincs kiv�lasztva csatolm�ny!'); return false; }}; var hf = $get('{2}'); if(count > 1 && hf && hf.value == '1') {{alert('KR �llom�ny eset�n csak egy f�jlt lehet kiv�lasztani!'); return false;}} var cb = $get('{1}'); if ((count > 1) && (!cb || !cb.checked) && !confirm('Figyelem, csatolm�nyonk�nt k�l�n �zenet ker�l elk�ld�sre! Folytatja?')) return false;", CsatolmanyokGridView.ClientID, cbPackDokumentumok.ClientID, hfIsAnyKrFileSelected.ClientID);
                ImageExpedialas.OnClientClick = js;
            }
            else
            {
                string js = String.Format("var count = getSelectedCheckBoxesCount('{0}','check'); if(count == 0) {{alert('Nincs kiv�lasztva csatolm�ny!'); return false; }};", CsatolmanyokGridView.ClientID);
                ImageExpedialas.OnClientClick = js;
            }
        }
        else
        {
            if (IsTomegesExpedialas)
            {
                ImageExpedialas.OnClientClick = String.Empty;
            }
        }

        string krFileName = GetKRFileName();
        if (krFileName != LoadedKRFileName)
        {
            LoadedKRFileName = krFileName;
            if (!String.IsNullOrEmpty(krFileName))
            {
                LoadKRFileMetaAdatok();
            }
            else
            {
                LoadDefaultMetaAdatok();
            }
        }
        hfIsAnyKrFileSelected.Value = IsAnyKrFileSelected() ? "1" : "0";
        labelKR_Fiok.Visible = IsKuldesModHivataliKapu || IsKuldesModPostaHibrid;
        KR_Fiok.Visible = IsKuldesModHivataliKapu || IsKuldesModPostaHibrid;
        PackPanel.Visible = IsPackCsatolmanyokEnabled;

        //if (IsKuldesModPostaHibrid)
        //{
        //    cbPackDokumentumok.Visible = false;
        //    PackedFileName.ReadOnly = true;
        //    PackedFileFormat.SelectedValue = ((int)HivataliKapuFileSettings.HivataliKapuFileFormat.Krx).ToString();
        //    PackedFileFormat.ReadOnly = true;
        //    cbEncrypt.Visible = false;
        //}
        //else
        //{
        //    cbPackDokumentumok.Visible = true;
        //    PackedFileName.ReadOnly = false;
        //    PackedFileFormat.ReadOnly = false;
        //    cbEncrypt.Visible = true;
        //}

        if (cbEncrypt.Visible)
        {
            string msg;
            if (!ExistsSajatPublikusKulcs(out msg))
            {
                msg = String.Format("{0} A kik�ld�tt csatolm�nyt csak a c�mzett fogja tudni megnyitni, a felad� nem!", msg);
                cbEncrypt.Attributes["onclick"] = String.Format("if (this.checked) alert('{0}');", msg);
            }
        }

        SetPostaHibridComponents();
    }

    /// <summary>
    /// Komponensek bet�lt�se �zleti objektumb�l
    /// </summary>
    /// <param name="EREC_Expedialas"></param>
    //private void LoadComponentsFromBusinessObject(EREC_Expedialas EREC_Expedialas)
    //{
    //    textFelado.Text = EREC_Expedialas.Felado;
    //    textCimzett.Text = EREC_Expedialas.Cimzett;
    //    textTargy.Text = EREC_Expedialas.Targy;
    //    textErkezesDatuma.Text = EREC_Expedialas.ErkezesDatuma;
    //    textFeladasDatuma.Text = EREC_Expedialas.FeladasDatuma;
    //    textFeldolgozasiIdo.Text = EREC_Expedialas.FeldolgozasIdo;
    //    KodtarakDropDownListFontossag.FillAndSetSelectedValue(kodcsoportExpedialasFontossag, EREC_Expedialas.Fontossag, FormHeader1.ErrorPanel);
    //    labelUzenetContent.Text = EREC_Expedialas.Uzenet;
    //    ErvenyessegCalendarControl1.ErvKezd = EREC_Expedialas.ErvKezd;
    //    ErvenyessegCalendarControl1.ErvVege = EREC_Expedialas.ErvVege;

    //    //aktu�lis verzi� ekt�rol�sa
    //    FormHeader1.Record_Ver = EREC_Expedialas.Base.Ver;
    //    //A mod�sit�s,l�trehoz�s form be�ll�t�sa
    //    FormPart_CreatedModified1.SetComponentValues(EREC_Expedialas.Base);
    //}

    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    FormFooter1.SaveEnabled = false;
        //}
    }

    private void SetAblakosBoritekComponents()
    {
        cbAblakosBoritek.Enabled = (TreeView1.Nodes.Count == 1 && TreeView1.Nodes[0].ChildNodes.Count == 1);
        if (!cbAblakosBoritek.Enabled)
        {
            // ha t�bb p�ld�ny van, nem lehet ablakos a bor�t�k
            cbAblakosBoritek.Checked = false;
        }
        if (cbAblakosBoritek.Checked)
        {
            // ablakos bor�t�kban csak egy p�ld�ny lehet, ekkor �tvessz�k a vonalk�dj�t
            VonalKodTextBox1.Text = GetTreeNodePldVonalkod(TreeView1.Nodes[0].ChildNodes[0]);
        }
        else
        {
            if (VonalKodTextBox1.Text == IratPeldany_Vonalkod)
            {
                // nem lehet azonos a p�ld�ny vonalk�dj�val
                VonalKodTextBox1.Text = String.Empty;
            }
        }

        if (cbAblakosBoritek.Checked && !String.IsNullOrEmpty(VonalKodTextBox1.Text))
        {
            VonalKodTextBox1.Enabled = false;
        }
        else
        {
            VonalKodTextBox1.Enabled = true;
        }
    }


    private void InitTreeViewByIratPeldany(string iratPeldanyId)
    {
        if (String.IsNullOrEmpty(iratPeldanyId)) { return; }

        // Iratp�ld�ny, irat �llapot�nak ellen�rz�se:
        EREC_PldIratPeldanyok obj_iratPeldany;
        EREC_IraIratok obj_irat;
        EREC_UgyUgyiratdarabok obj_ugyiratDarab;
        EREC_UgyUgyiratok obj_ugyirat;

        bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(iratPeldanyId, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
            , Page, FormHeader1.ErrorPanel);
        if (success_objectsGet == false)
        {
            HidePanels();
            return;
        }

        IratPeldanyok.Statusz statusz_iratpeldany = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);
        Iratok.Statusz statusz_irat = Iratok.GetAllapotByBusinessDocument(obj_irat);
        UgyiratDarabok.Statusz statusz_ugyiratdarab = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
        Ugyiratok.Statusz statusz_ugyirat = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
        bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        // Iratp�ld�ny expedi�lhat�-e?       
        ErrorDetails errorDetail = null;
        if (IratPeldanyok.Expedialhato(statusz_iratpeldany, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail) == false)
        {
            // nem expedi�lhat�
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                , Resources.Error.ErrorCode_52501 + " <br/> " + ResultError.GetErrorDetailInfoMsg(errorDetail, Page));
            HidePanels();
            return;
        }


        // benne van-e m�r valamelyik kimen� k�ldem�nyben?
        EREC_Kuldemeny_IratPeldanyaiService service_kuldIratPeldanyai = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();


        EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();

        search.Peldany_Id.Filter(iratPeldanyId);

        Result result_kuldIratPeldanyaiGetAll = service_kuldIratPeldanyai.GetAll(execParam, search);

        if (!string.IsNullOrEmpty(result_kuldIratPeldanyaiGetAll.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_kuldIratPeldanyaiGetAll);
            HidePanels();
        }
        else
        {
            // az iratp�ld�nyhoz tartoz� kimen� k�ldem�nyek sz�ma (ahol az �llapot Kimen�, �ssze�ll�t�s alatt)

            int kuldCount = 0;
            string kuldId = "";
            string kimenoKuldemenyVonalkod = null;

            // az iratp�ld�nyokhoz tartoz� kimen� k�ldem�nyek sz�ma (ahol az �llapot Kimen�, �ssze�ll�t�s alatt)
            if (result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                List<string> kimenoKuldemenyek = new List<string>();

                foreach (DataRow row in result_kuldIratPeldanyaiGetAll.Ds.Tables[0].Rows)
                {
                    kuldId = row["KuldKuldemeny_Id"].ToString();

                    if (!kimenoKuldemenyek.Contains(kuldId))
                    {
                        kimenoKuldemenyek.Add(kuldId);
                    }
                }
                // k�ldem�ny �llapot�nak lek�r�se:
                EREC_KuldKuldemenyekService service_kuld = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                ExecParam execParam_kuldGet = execParam.Clone();
                //execParam_kuldGet.Record_Id = kuldId;

                //Result result_kuldGet = service_kuld.Get(execParam_kuldGet);
                //if (!String.IsNullOrEmpty(result_kuldGet.ErrorCode))
                //{
                //    // hiba:
                //    throw new ResultException(result_kuldGet);
                //}

                EREC_KuldKuldemenyekSearch search_kuld = new EREC_KuldKuldemenyekSearch();
                search_kuld.Id.In(kimenoKuldemenyek);

                Result result_kuldGetAll = service_kuld.GetAll(execParam_kuldGet, search_kuld);
                if (!String.IsNullOrEmpty(result_kuldGetAll.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_kuldGetAll);
                    HidePanels();
                    return;
                }

                // TODO: kell �llapotellen�rz�s? vagy b�rmilyen �llapot� egy�b kimen� k�ldem�ny eset�n hiba
                DataRow[] rows_kimenoben = result_kuldGetAll.Ds.Tables[0].Select(String.Format("Allapot='{0}'", KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt));

                //EREC_KuldKuldemenyek kimenoKuld = (EREC_KuldKuldemenyek)result_kuldGet.Record;

                //if (kimenoKuld.Allapot == KodTarak.KULDEMENY_ALLAPOT.Kimeno_osszeallitas_alatt)
                //{
                //    kuldCount++;
                //}

                kuldCount = rows_kimenoben.Length;
                //}

                if (kuldCount == 1)
                {
                    kimenoKuldemenyVonalkod = rows_kimenoben[0]["BarCode"].ToString();
                }
            }

            // ha t�bb kimen� k�ldem�ny is van, hiba:
            if (kuldCount > 1)
            {
                // hiba:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_52511);
                HidePanels();
                return;
            }
            else if (kuldCount == 1)
            {
                CreateTreeViewByKuldemeny(kuldId);
                SetVonalkodComponentsKimenoKuldemeny(kimenoKuldemenyVonalkod, obj_iratPeldany.BarCode);
            }
            else if (kuldCount == 0)
            {
                // nincs m�g hozz� kimen� k�ldem�ny:

                // iktat�sz�mok a megjelen�t�shez:
                string iktatoszam_pld = obj_iratPeldany.Azonosito;
                string iktatoszam_irat = obj_irat.Azonosito;

                string vonalkod_pld = obj_iratPeldany.BarCode;

                CreateTreeViewWithOneIratPeldany(iratPeldanyId, obj_irat.Id, iktatoszam_pld, iktatoszam_irat, vonalkod_pld);
                hfUgyintezesModja.Value = obj_iratPeldany.UgyintezesModja;
            }

        }

        LoadComponentsFromBusinessObject(obj_iratPeldany);
    }

    //    private void CreateTreeViewWithOneIratPeldany(string iratPeldanyId,string iratId, string iktatoszam_pld,string iktatoszam_irat)
    private void CreateTreeViewWithOneIratPeldany(string iratPeldanyId, string iratId, string iktatoszam_pld, string iktatoszam_irat, string vonalkod_pld)
    {
        TreeView1.Nodes.Clear();
        TreeNode node_irat = new TreeNode(iktatoszam_irat, iratId);
        TreeView1.Nodes.Add(node_irat);
        TreeNode node_pld = new TreeNode(iktatoszam_pld, JoinTreeNodePldValue(iratPeldanyId, vonalkod_pld));
        node_irat.ChildNodes.Add(node_pld);

        LoadDoktipusAdatok();
        CsatolmanyokGridViewBind();

    }

    private void CreateTreeViewByKuldemeny(string kuldemenyId)
    {
        if (String.IsNullOrEmpty(kuldemenyId))
        {
            return;
        }

        TreeView1.Nodes.Clear();

        // k�ldem�ny iratp�ld�nyainak lek�r�se
        EREC_Kuldemeny_IratPeldanyaiService service = eRecordService.ServiceFactory.GetEREC_Kuldemeny_IratPeldanyaiService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_Kuldemeny_IratPeldanyaiSearch search = new EREC_Kuldemeny_IratPeldanyaiSearch();
        search.KuldKuldemeny_Id.Filter(kuldemenyId);

        Result result_pldGetAll = service.GetAll(execParam, search);
        if (!String.IsNullOrEmpty(result_pldGetAll.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_pldGetAll);
            return;
        }
        else
        {
            string pld_Ids_searchString = "";

            // TODO: ez �gy most nem valami hat�kony, csak h�t most sietni kell...

            foreach (DataRow row in result_pldGetAll.Ds.Tables[0].Rows)
            {
                string pld_Id = row["Peldany_Id"].ToString();


                // Iratp�ld�ny, irat �llapot�nak ellen�rz�se:
                EREC_PldIratPeldanyok obj_iratPeldany = null;
                EREC_IraIratok obj_irat = null;
                EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
                EREC_UgyUgyiratok obj_ugyirat = null;

                bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(pld_Id, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                    , Page, FormHeader1.ErrorPanel);
                if (success_objectsGet == false)
                {
                    HidePanels();
                    return;
                }

                IratPeldanyok.Statusz statusz_iratpeldany = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);
                Iratok.Statusz statusz_irat = Iratok.GetAllapotByBusinessDocument(obj_irat);
                UgyiratDarabok.Statusz statusz_ugyiratdarab = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
                Ugyiratok.Statusz statusz_ugyirat = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
                bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;


                // Iratp�ld�ny expedi�lhat�-e?       
                ErrorDetails errorDetail = null;
                if (IratPeldanyok.Expedialhato(statusz_iratpeldany, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail) == false)
                {
                    // nem expedi�lhat�
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                        , Resources.Error.ErrorCode_52501 + " <br /> " + ResultError.GetErrorDetailInfoMsg(errorDetail, Page));
                    HidePanels();
                    return;
                }
                else
                {
                    // iktat�sz�mok a megjelen�t�shez:
                    string iktatoszam_pld = obj_iratPeldany.Azonosito;
                    string iktatoszam_irat = obj_irat.Azonosito;

                    // vonalk�d ablakos bor�t�khoz
                    string vonalkod_pld = obj_iratPeldany.BarCode;

                    TreeNode node_irat = new TreeNode(iktatoszam_irat, obj_irat.Id);
                    TreeView1.Nodes.Add(node_irat);
                    TreeNode node_pld = new TreeNode(iktatoszam_pld, JoinTreeNodePldValue(obj_iratPeldany.Id, vonalkod_pld));
                    node_irat.ChildNodes.Add(node_pld);

                }


                //if (String.IsNullOrEmpty(pld_Ids_searchString))
                //{
                //    pld_Ids_searchString = "'" + pld_Id + "'";
                //}
                //else
                //{
                //    pld_Ids_searchString += ",'" + pld_Id + "'";
                //}
            }

            LoadDoktipusAdatok();
            CsatolmanyokGridViewBind();
            SetSelectedDokumentumokByKuldemeny(kuldemenyId);

            if (String.IsNullOrEmpty(pld_Ids_searchString))
            {
                return;
            }


        }
    }

    protected bool CheckVonalkod(string[] pld_Array, out string errormsg)
    {
        bool isValid = true;
        errormsg = null;
        String vonalkod = VonalKodTextBox1.Text;    // kimen� k�ldem�ny vonalk�dja

        if (cbAblakosBoritek.Checked)
        {
            // ablakos bor�t�kn�l k�telez� a vonalk�d
            //(megegyezik a p�ld�ny vonalk�dj�val, automatikus kit�lt�s, csak hiba eset�n maradhat �res)
            if (pld_Array.Length == 1)
            {
                // �tvessz�k a p�ld�ny vonalk�dj�t:
                vonalkod = IratPeldany_Vonalkod;

                if (String.IsNullOrEmpty(vonalkod))
                {
                    // nincs megadva vonalk�d:
                    //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoBarCode);
                    errormsg = Resources.Error.UINoBarCode;
                    isValid = false;
                }
            }
            else
            {
                // t�bb p�ld�ny eset�n nem lehet ablakos a bor�t�k (pontosabban mindek�pp k�l�n vonalk�d kell neki)
                //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "T�bb p�ld�ny eset�n az ablakos bor�t�k opci� nem haszn�lhat�! (�n�ll� vonalk�dot kell adni a k�ldem�nynek)");
                //"T�bb p�ld�ny eset�n az ablakos bor�t�k opci� nem haszn�lhat�! (�n�ll� vonalk�dot kell adni a k�ldem�nynek)";
                errormsg = Resources.Error.ErrorCode_52515;
                isValid = false;
            }
        }
        else
        {
            if (!String.IsNullOrEmpty(vonalkod))
            {
                string[] pldVonalkodArray = CreateIratPeldanyVonalkodArrayFromTreeView();
                if (pldVonalkodArray != null)
                {
                    if (Array.IndexOf(pldVonalkodArray, vonalkod) > -1)
                    {
                        // nem ablakos bor�t�k eset�n k�l�n vonalk�d kell a k�ldem�nynek, nem egyezhet meg a p�ld�ny�val
                        //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "A vonalk�d megegyezik valamely p�ld�ny vonalk�dj�val! (nem ablakos bor�t�k eset�n �n�ll� vonalk�dot kell adni a k�ldem�nynek)");
                        //"A vonalk�d megegyezik valamely p�ld�ny vonalk�dj�val! (nem ablakos bor�t�k eset�n �n�ll� vonalk�dot kell adni a k�ldem�nynek)";
                        errormsg = Resources.Error.ErrorCode_52516;
                        isValid = false;
                    }
                }
            }
        }

        return isValid;
    }


    /// <summary>
    /// Kimen� k�ldem�ny �ssze�ll�t�sa / elment�se
    /// </summary>    
    protected void ImageSave_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, funkciokod_Expedialas))
        {
            if (TreeView1.Nodes.Count == 0 || String.IsNullOrEmpty(IratPeldany_Id))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_52510);
                return;
            }

            if (!CheckParameters())
            {
                return;
            }

            String[] pld_Array = CreateIratPeldanyArrayFromTreeView();

            string errormsg;
            if (!CheckVonalkod(pld_Array, out errormsg))
            {
                // hiba
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, errormsg);
                return;
            }

            string[] dokumentumId_Array = null;
            if (IsCsatolmanySelectionEnabled)
            {
                List<string> dokumentumId_List = GetSelectedDokumentumok();
                dokumentumId_Array = dokumentumId_List.ToArray();
            }

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyok erec_PldIratPeldanyok = GetBusinessObjectFromComponents();

            Result result = service.KimenoKuldemeny_Save(execParam, IratPeldany_Id, pld_Array, erec_PldIratPeldanyok, VonalKodTextBox1.Text, dokumentumId_Array);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }
            else
            {
                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }

    // iratp�ld�ny t�mb �ssze�ll�t�sa a treeviewb�l
    private String[] CreateIratPeldanyArrayFromTreeView()
    {
        String[] pldArray = new String[TreeView1.Nodes.Count];

        for (int i = 0; i < TreeView1.Nodes.Count; i++)
        {
            pldArray[i] = GetTreeNodePldId(TreeView1.Nodes[i].ChildNodes[0]);
        }

        return pldArray;
    }


    // iratp�ld�ny vonalk�d t�mb �ssze�ll�t�sa a treeviewb�l - ellen�rz�shez
    private String[] CreateIratPeldanyVonalkodArrayFromTreeView()
    {
        String[] pldVonalkodArray = new String[TreeView1.Nodes.Count];

        for (int i = 0; i < TreeView1.Nodes.Count; i++)
        {
            pldVonalkodArray[i] = GetTreeNodePldVonalkod(TreeView1.Nodes[i].ChildNodes[0]);
        }

        return pldVonalkodArray;
    }

    protected void ImageExpedialas_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (IsTomegesExpedialas)
            {
                TomegesExpedialas();
            }
            else
            {
                Expedialas();
            }
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba!", ex.Message);
        }
    }

    void Expedialas()
    {
        if (FunctionRights.GetFunkcioJog(Page, funkciokod_Expedialas))
        {
            if (TreeView1.Nodes.Count == 0 || String.IsNullOrEmpty(IratPeldany_Id))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.ErrorCode_52520);
                return;
            }

            if (!CheckParameters())
            {
                return;
            }

            #region POSTAI RAGSZAM
            string rszMode = null;
            string rszPostakonyvId = null;
            string rszkuldemenyFajta = null;
            string rszCode = null;
            string rszRagszamSavId = null;
            string outError = null;
            if (CheckRendszerParameterKellPostaiRagszam())
            {
                var pk = DropDownListPostaiRagszamPostakonyv.SelectedValue;
                var kf = DropDownListPostaiRagszamKuldemenyFajtaja.SelectedValue;

                if (!CheckPostaiRagszamParameters(out rszMode, out rszPostakonyvId, out rszkuldemenyFajta, out rszCode, out rszRagszamSavId, out outError))
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, outError);
                    return;
                }

                #endregion
            }
            //// TODO: jelenleg nem k�telez� a kimen� k�ldem�ny vonalk�d
            //// - t�meges vonalk�dos post�z�sn�l viszont majd k�telez�v� kell tenni
            //bool isVonalkodKotelezo = (Pld_KuldesMod_KodtarakDropDownList.SelectedValue == KodTarak.KULDEMENY_KULDES_MODJA.Postai_tertivevenyes);

            //if (isVonalkodKotelezo && String.IsNullOrEmpty(VonalKodTextBox1.Text))
            //{
            //    // nincs megadva vonalk�d:
            //    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoBarCode);
            //    return;
            //}

            String[] pld_Array = CreateIratPeldanyArrayFromTreeView();

            string errormsg;
            if (!CheckVonalkod(pld_Array, out errormsg))
            {
                // hiba
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, errormsg);
                return;
            }



            string[] dokumentumId_Array = null;
            if (IsCsatolmanySelectionEnabled)
            {
                if (!IsSelectedDokumentum())
                {
                    return;
                }

                List<string> dokumentumId_List = GetSelectedDokumentumok();
                dokumentumId_Array = dokumentumId_List.ToArray();
            }

            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            service.Timeout = 1800000;
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyok erec_PldIratPeldanyok = GetBusinessObjectFromComponents();

            Result result = null;

            if (IsKuldesModHivataliKapu)
            {
                EREC_eBeadvanyok _EREC_eBeadvanyok = GetDokAdatokFromComponents();
                HivataliKapuFileSettings fileSettings = GetFileSettingsFromComponents();
                result = service.Expedialas_HivataliKapus(execParam, pld_Array, VonalKodTextBox1.Text, erec_PldIratPeldanyok, _EREC_eBeadvanyok, dokumentumId_Array, fileSettings);
            }
            else if (IsKuldesModPostaHibrid)
            {
                deliveryInstructions kezbesitesiUtasitas = panelPostaHibrid.GetKezbesitesiUtasitas(GetBusinessObjectFromComponents(), GetSelectedDokumentumok().ToArray());
                EREC_eBeadvanyok _EREC_eBeadvanyok = GetDokAdatokFromComponents();
                HivataliKapuFileSettings fileSettings = GetFileSettingsFromComponents();
                result = service.Expedialas_PostaHibrid(execParam, pld_Array, VonalKodTextBox1.Text, erec_PldIratPeldanyok, _EREC_eBeadvanyok, dokumentumId_Array, fileSettings, kezbesitesiUtasitas);
            }
            else
            {
                result = service.Expedialas(execParam, pld_Array, VonalKodTextBox1.Text, erec_PldIratPeldanyok, dokumentumId_Array);
            }
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                return;
            }

            if (CheckRendszerParameterKellPostaiRagszam())
            {
                #region POSTAI RAGSZAM
                if (CheckKuldesModjaPostai())
                {
                    var kuldId = result.Uid != null ? result.Uid : (result.GetCount > 0 ? result.Ds.Tables[0].Rows[0]["ID"].ToString() : "");
                    Result resultKuldKuldemeny = FindKimenoKuldemeny(kuldId);
                    if (!String.IsNullOrEmpty(resultKuldKuldemeny.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultKuldKuldemeny);
                        return;
                    }
                    EREC_KuldKuldemenyek kuld = (EREC_KuldKuldemenyek)resultKuldKuldemeny.Record;
                    Result resultPostaiRagszam = PostaiRagSzamKiosztasProcedure(kuld, rszMode, rszPostakonyvId, rszkuldemenyFajta, rszRagszamSavId, rszCode);
                    if (resultPostaiRagszam != null)
                        if (!String.IsNullOrEmpty(resultPostaiRagszam.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultPostaiRagszam);
                            return;
                        }
                }
                #endregion
            }
            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }
    private Result PostaiRagSzamKiosztasProcedure(EREC_KuldKuldemenyek kuldemeny, string mode, string postakonyvId, string kuldesModja, string ragszamSavId, string ragszam)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.PostaiRagszamKiosztas(execParam, kuldemeny, mode, postakonyvId, kuldesModja, ragszamSavId, ragszam);
        return result;
    }
    private bool CheckParameters()
    {
        if (IsKuldesModHivataliKapu)
        {
            if (IsHivatalCimzett)
            {
                if (String.IsNullOrEmpty(HivatalKRID.Text))
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "Figyelmeztet�s", "A hivatal megad�sa k�telez�!");
                    return false;
                }
            }
            else if (IsSzemelyCimzett)
            {
                if (String.IsNullOrEmpty(KapcsolatiKod.Text))
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "Figyelmeztet�s", "A szem�ly megad�sa k�telez�!");
                    return false;
                }
            }
        }

        return true;
    }

    // megadott 
    protected void Hozzaadas_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(PldIratPeldanyokTextBox1.Id_HiddenField))
        {
            if (TreeView1.FindNode(PldIratPeldanyokTextBox1.Id_HiddenField) == null)
            {
                AddIratPeldanyToTreeView(PldIratPeldanyokTextBox1.Id_HiddenField, PldIratPeldanyokTextBox1.Text);
                // p�ld�ny textbox t�rl�se
                PldIratPeldanyokTextBox1.Id_HiddenField = "";
                PldIratPeldanyokTextBox1.Text = "";
            }
            else
            {
                // jelz�s hogy m�r van ilyen
                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UIAlreadyExistsIratPeldanyInList);
            }

            SetAblakosBoritekComponents();
        }
    }

    private void AddIratPeldanyToTreeView(string ujPld_Id, string ujPld_iktatoszam)
    {
        if (String.IsNullOrEmpty(ujPld_Id) || String.IsNullOrEmpty(ujPld_iktatoszam))
        {
            return;
        }

        // Iratp�ld�ny, irat �llapot�nak ellen�rz�se:
        EREC_PldIratPeldanyok obj_iratPeldany = null;
        EREC_IraIratok obj_irat = null;
        EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
        EREC_UgyUgyiratok obj_ugyirat = null;

        bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(ujPld_Id, out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
            , Page, FormHeader1.ErrorPanel);
        if (success_objectsGet == false)
        {
            return;
        }

        // az irat benne van-e m�r a treeview-ban?
        if (TreeView1.FindNode(obj_irat.Id) != null)
        {
            // jelz�s hogy m�r van ilyen
            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UIAlreadyExistsIratInList);
            return;
        }

        IratPeldanyok.Statusz statusz_iratpeldany = IratPeldanyok.GetAllapotByBusinessDocument(obj_iratPeldany);
        Iratok.Statusz statusz_irat = Iratok.GetAllapotByBusinessDocument(obj_irat);
        UgyiratDarabok.Statusz statusz_ugyiratdarab = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
        Ugyiratok.Statusz statusz_ugyirat = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
        bool kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        // Iratp�ld�ny expedi�lhat�-e?       
        ErrorDetails errorDetail = null;
        if (IratPeldanyok.Expedialhato(statusz_iratpeldany, kiadmanyozando, statusz_irat, statusz_ugyirat, execParam, out errorDetail) == false)
        {
            // nem expedi�lhat�
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader
                , Resources.Error.ErrorCode_52501 + " <br /> " + ResultError.GetErrorDetailInfoMsg(errorDetail, Page));
            return;
        }

        // Irat, iratp�ld�ny Node-ok hozz�ad�sa:

        string irat_iktatoszam = obj_irat.Azonosito;

        string vonalkod_pld = obj_iratPeldany.BarCode;
        TreeNode newIratNode = new TreeNode(irat_iktatoszam, obj_irat.Id);

        TreeNode newPldNode = new TreeNode(ujPld_iktatoszam, JoinTreeNodePldValue(ujPld_Id, vonalkod_pld));

        TreeView1.Nodes.Add(newIratNode);
        newIratNode.ChildNodes.Add(newPldNode);
        newIratNode.ExpandAll();

        LoadDoktipusAdatok();
        CsatolmanyokGridViewBind();
    }

    protected void Torles_Click(object sender, ImageClickEventArgs e)
    {
        TreeNode selectedNode = TreeView1.SelectedNode;
        if (selectedNode == null)
        {
            // nincs kijel�lve sor:
            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINoSelectedRow);
            return;
        }
        else
        {
            // ellen�rz�s: az eredeti iratp�ld�ny nem t�r�lhet� (amivel megh�vtuk a formot)
            //if ((selectedNode.Depth == 0 && selectedNode.ChildNodes[0] != null && selectedNode.ChildNodes[0].Value == IratPeldany_Id)
            //    || (selectedNode.Depth == 1 && selectedNode.Value == IratPeldany_Id))
            if ((selectedNode.Depth == 0 && selectedNode.ChildNodes[0] != null && GetTreeNodePldId(selectedNode.ChildNodes[0]) == IratPeldany_Id)
                || (selectedNode.Depth == 1 && GetTreeNodePldId(selectedNode) == IratPeldany_Id))
            {
                // nem t�r�lhet� az iratp�ld�ny a list�b�l:
                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UICannotDeleteIratPeldanyFromList);
                return;
            }

            if (selectedNode.Depth == 0)
            {
                TreeView1.Nodes.Remove(selectedNode);
            }
            else if (selectedNode.Depth == 1)
            {
                TreeNode parentNode = selectedNode.Parent;
                selectedNode.Parent.ChildNodes.Remove(selectedNode);
                TreeView1.Nodes.Remove(parentNode);
            }

            SetAblakosBoritekComponents();
            LoadDoktipusAdatok();
            CsatolmanyokGridViewBind();
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        //cbAblakosBoritek.Attributes["onclick"] =
        //" enableOrDisableComponents('" + cbAblakosBoritek.ClientID
        //    + "',1,['"
        //    + VonalKodTextBox1.TextBox.ClientID
        //    + "','"
        //    + labelVonalkod.ClientID
        //    + "']);";

        //Page.ClientScript.RegisterStartupScript(Page.GetType(), "disableComponents", cbAblakosBoritek.Attributes["onclick"], true);
    }

    private void HidePanels()
    {
        EFormPanel1.Visible = false;
        panelIratPeldany.Visible = false;
        panelHivataliKapu.Visible = false;
        panelPostaHibrid.Visible = false;
    }

    protected void searhSzemely_Click(object sender, ImageClickEventArgs e)
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        AzonositasKerdes kerdes = new AzonositasKerdes();
        kerdes.TermeszetesSzemelyAzonosito = new hkpTermeszetesSzemelyAzonosito();

        #region sz�let�si n�v
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve = new NevAdat();
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.CsaladiNev = SzuletesiNevVezetekNev.Text;
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.UtoNev1 = SzuletesiNevUtonev1.Text;
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiNeve.UtoNev2 = SzuletesiNevUtonev2.Text;
        #endregion

        #region viselt n�v
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve = new NevAdat();
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.CsaladiNev = ViseltNevVezetekNev.Text;
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev1 = ViseltNevUtonev1.Text;
        kerdes.TermeszetesSzemelyAzonosito.ViseltNeve.UtoNev2 = ViseltNevUtonev2.Text;
        #endregion

        #region anyja neve
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve = new NevAdat();
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.CsaladiNev = AnyjaNeveVezetekNev.Text;
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev1 = AnyjaNeveUtonev1.Text;
        kerdes.TermeszetesSzemelyAzonosito.AnyjaNeve.UtoNev2 = AnyjaNeveUtonev2.Text;
        #endregion  

        kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely = new hkpSzuletesiHely();
        kerdes.TermeszetesSzemelyAzonosito.SzuletesiHely.Telepules = SzuletesiHely.Text;

        kerdes.TermeszetesSzemelyAzonosito.SzuletesiIdo = SzuletesDatuma.SelectedDate;

        Result result = svc.Azonositas(KR_Fiok.SelectedValue, kerdes);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }

        AzonositasValasz valasz = result.Record as AzonositasValasz;

        if (valasz != null)
        {
            if (valasz.HibaUzenet != null && !String.IsNullOrEmpty(valasz.HibaUzenet.Tartalom))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", valasz.HibaUzenet.Tartalom);
                return;
            }

            if (valasz.AzonositottList.Length == 0)
            {
                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "Figyelmeztet�s", "A keresett szem�ly nem tal�lhat�");
                return;
            }

            if (valasz.AzonositottList.Length > 1)
            {
                ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "Figyelmeztet�s", "A keres�s eredm�nye nem egy�rtelm�");
                return;
            }

            Azonositott azonositott = valasz.AzonositottList[0];
            KapcsolatiKod.Text = azonositott.KapcsolatiKod;
            SzemelyNeve.Text = azonositott.Nev;
            SzemelyEmail.Text = azonositott.Email;
        }


    }

    #region seg�d met�dusok
    private string JoinTreeNodePldValue(string Pld_Id, string Pld_Vonalkod)
    {
        return String.Join(";", new string[] { Pld_Id, Pld_Vonalkod });
    }

    private string[] SplitTreeNodePldValue(TreeNode tnPld)
    {
        return tnPld.Value.Split(new char[] { ';' });
    }

    private string GetTreeNodePldId(TreeNode tnPld)
    {
        string[] items = SplitTreeNodePldValue(tnPld);
        return items[0];
    }

    private string GetTreeNodePldVonalkod(TreeNode tnPld)
    {
        string[] items = SplitTreeNodePldValue(tnPld);
        return items[1];
    }
    #endregion seg�d met�dusok

    private string GetHivataliKapuCimzett()
    {
        if (IsSzemelyCimzett)
        {
            return SzemelyNeve.Text;
        }
        else
        {
            return HivatalNeve.Text;
        }
    }

    private EREC_eBeadvanyok GetDokAdatokFromComponents()
    {
        EREC_eBeadvanyok dok = new EREC_eBeadvanyok();
        dok.Updated.SetValueAll(false);
        dok.Base.Updated.SetValueAll(false);

        if (IsSzemelyCimzett)
        {
            dok.PartnerKapcsolatiKod = KapcsolatiKod.Text;
            dok.Updated.PartnerKapcsolatiKod = true;
            dok.PartnerNev = SzemelyNeve.Text;
            dok.Updated.PartnerNev = true;
            dok.PartnerEmail = SzemelyEmail.Text;
            dok.Updated.PartnerEmail = true;
        }
        else if (IsHivatalCimzett)
        {
            dok.PartnerKRID = HivatalKRID.Text;
            dok.Updated.PartnerKRID = true;
            dok.PartnerNev = HivatalNeve.Text;
            dok.Updated.PartnerNev = true;
            dok.PartnerRovidNev = HivatalRovidNeve.Value;
            dok.Updated.PartnerRovidNev = true;
            dok.PartnerMAKKod = HivatalMAKKod.Value;
            dok.Updated.PartnerMAKKod = true;
        }
        else
        {
            //Hiba
        }
        dok.KR_ETertiveveny = "1";
        dok.Updated.KR_ETertiveveny = true;

        dok.KR_DokTipusHivatal = KR_DokTipusHivatal.Text;
        dok.Updated.KR_DokTipusHivatal = true;
        dok.KR_DokTipusAzonosito = KR_DokTipusAzonosito.Text;
        dok.Updated.KR_DokTipusAzonosito = true;
        dok.KR_DokTipusLeiras = KR_DokTipusLeiras.Text;
        dok.Updated.KR_DokTipusLeiras = true;
        dok.KR_Megjegyzes = KR_Megjegyzes.Text;
        dok.Updated.KR_Megjegyzes = true;

        //Fi�k
        dok.KR_Fiok = KR_Fiok.SelectedValue;
        dok.Updated.KR_Fiok = true;

        return dok;
    }


    #region CR3152 - Expedi�l�s folyamat t�meges�t�se
    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            search.Id.In(IratPeldanyokArray);

            Result res = service.GetAllWithExtension(ExecParam, search);

            if (!res.IsError)
            {
                IsTomegesCsatolmanySelectionEnabled = ExistHivataliKapuPeldany(res.Ds);
                IratId = GetIratId(res.Ds);
            }

            ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            EREC_PldIratPeldanyok obj_iratPeldany = null;
            EREC_IraIratok obj_irat = null;
            EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
            EREC_UgyUgyiratok obj_ugyirat = null;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;
                bool success_objectsGet = IratPeldanyok.Set4ObjectsByIratPeldanyId(drv.Row["Id"].ToString(), out obj_iratPeldany, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat
                    , Page, FormHeader1.ErrorPanel);
            }
            bool kiadmanyozando = false;
            if (obj_irat != null)
                kiadmanyozando = (obj_irat.KiadmanyozniKell == "1") ? true : false;

            bool isOneKikuldhetoCsatolmany = false;

            if (obj_iratPeldany.KuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
            {
                isOneKikuldhetoCsatolmany = IsOneKikuldhetoCsatolmany(obj_iratPeldany.Id);
            }

            IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckExpedialasKijeloles(e, Page, kiadmanyozando, obj_iratPeldany, obj_irat, IratId, isOneKikuldhetoCsatolmany);

            CheckBox cb = (CheckBox)e.Row.FindControl("check");
            if (cb != null)
            {
                cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                    "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                    "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                    ";");
            }
        }
    }

    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_expedialhatok = UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check");

        int count_NEMexpedialhatok = IratPeldanyokArray.Length - count_expedialhatok;

        if (count_NEMexpedialhatok > 0)
        {
            Label_Warning_IratPeldany.Text = String.Format(Resources.List.UI_IratPeldany_ExpedialasWarning, count_NEMexpedialhatok);
            //Label_Warning_IratPeldany.Visible = true;
            //Panel_Warning_IratPeldany.Visible = true;
        }
    }
    private void InitTomegesExpedialas()
    {
        //Felel�s ellen�rz�shez kell
        try
        {
            if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
            {
                FillIratPeldanyGridView();
                if (IsTomegesCsatolmanySelectionEnabled && !String.IsNullOrEmpty(IratId))
                {
                    CsatolmanyokGridViewBind();
                }
            }
        }
        catch (Contentum.eUtility.ResultException e)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
        }
    }

    #endregion

    #region POSTAI RAGSZAM
    #region FILL
    private void FillPostakonyvek()
    {
        if (DropDownListPostaiRagszamPostakonyv.Items.Count > 0)
            return;

        DataRowCollection items = LoadPostakonyvek();
        if (items == null)
            return;

        DropDownListPostaiRagszamPostakonyv.Items.Clear();
        ListItem li = null;
        foreach (DataRow item in items)
        {
            li = new ListItem();
            li.Text = item["Nev"].ToString();
            li.Value = item["Id"].ToString();
            DropDownListPostaiRagszamPostakonyv.Items.Add(li);
        }
    }
    private void FillKuldemenyFajta()
    {
        if (DropDownListPostaiRagszamKuldemenyFajtaja.Items.Count > 0)
            return;

        Dictionary<string, string> items = LoadPostaiKimenoKuldemenyFajta();
        if (items == null)
            return;

        DropDownListPostaiRagszamKuldemenyFajtaja.Items.Clear();
        ListItem li = null;
        foreach (KeyValuePair<string, string> item in items)
        {
            li = new ListItem();
            li.Text = item.Value;
            li.Value = item.Key;
            DropDownListPostaiRagszamKuldemenyFajtaja.Items.Add(li);
        }
    }
    #endregion

    private DataRowCollection LoadPostakonyvek()
    {
        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
        Search.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Postakonyv);
        Result erec_IraIktatoKonyvek_result = service.GetAllWithExtensionAndJogosultsag(execParam, Search, true);
        if (erec_IraIktatoKonyvek_result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, erec_IraIktatoKonyvek_result);
            return null;
        }
        if (erec_IraIktatoKonyvek_result.Ds.Tables.Count < 1 || erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count < 1)
            return null;

        return erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows;
    }
    private Dictionary<string, string> LoadPostaiKimenoKuldemenyFajta()
    {
        Dictionary<string, string> allElements = Contentum.eUtility.KodTar_Cache.GetKodtarakByKodCsoport(Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.kcsNev, Page);
        if (allElements == null || allElements.Count < 1)
            return null;

        Dictionary<string, string> filteredElements = new Dictionary<string, string>();
        foreach (KeyValuePair<string, string> item in allElements)
        {
            if (item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Ajanlott_Kuldemeny
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Cimzett_Kezebe_Level
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Erteknyilvanitasos_Hivatalos_Irat
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Erteknyilvanitasos_Level_Kuldemeny
                ||
                item.Key == Contentum.eUtility.KodTarak.KIMENO_KULDEMENY_FAJTA.Postai_Hivatalos_Irat)
            {
                filteredElements.Add(item.Key, item.Value);
            }
        }
        return filteredElements;
    }

    #region PARAMETEREK
    private bool CheckKuldesModjaPostai()
    {
        if (string.IsNullOrEmpty(Pld_KuldesMod_KodtarakDropDownList.SelectedValue))
            return false;

        var mapping = GetDocumentTypeMapping("KULDEMENY_KULDES_MODJA", "POSTAI_AZONOSITO_KOTELEZO");
        if (mapping.Keys.Any(x => x.Kod == Pld_KuldesMod_KodtarakDropDownList.SelectedValue))
        {
            if (mapping.First(x => x.Key.Kod == Pld_KuldesMod_KodtarakDropDownList.SelectedValue).Value.Kod.ToLower() == "igen")
            {
                return true;
            }

        }
        return false;
    }

    private bool CheckRendszerParameterKellPostaiRagszam()
    {
        string val = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.POSTAI_RAGSZAM_EXPEDIALASKOR);
        if (val == null)
            return false;
        else if (val == "1")
            return true;
        else
            return false;
    }

    private void SetControlsByRendszerParameterRagszamKiosztasModja()
    {
        if (!CheckRendszerParameterKellPostaiRagszam())
        {
            HidePostaiRagszamFields();
            return;
        }

        if (CheckKuldesModjaPostai())
        {
            string val = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA);
            switch (val)
            {
                case "A": //AUTOMATIKUS KIOSZTAS
                    TRPostaiRagszamBeviteliMezo.Visible = false;
                    TRPostaiRagszamPostakonyv.Visible = true;
                    TRPostaiRagszamKuldemenyFajtaja.Visible = true;

                    TextBoxPostaiRagszam.Visible = false;
                    TextBoxPostaiRagszam.Enabled = false;
                    DropDownListPostaiRagszamPostakonyv.Visible = true;
                    DropDownListPostaiRagszamPostakonyv.Enabled = true;
                    DropDownListPostaiRagszamKuldemenyFajtaja.Visible = true;
                    DropDownListPostaiRagszamKuldemenyFajtaja.Enabled = true;

                    HiddenFieldRagszamIgenylesModja.Value = val;
                    break;
                case "M": //MANU�LIS KIOSZT�S
                    TRPostaiRagszamBeviteliMezo.Visible = true;
                    TRPostaiRagszamPostakonyv.Visible = true;
                    TRPostaiRagszamKuldemenyFajtaja.Visible = true;

                    TextBoxPostaiRagszam.Visible = true;
                    TextBoxPostaiRagszam.Enabled = true;

                    TRPostaiRagszamPostakonyv.Visible = false;
                    DropDownListPostaiRagszamPostakonyv.Visible = false;
                    DropDownListPostaiRagszamPostakonyv.Enabled = false;

                    TRPostaiRagszamKuldemenyFajtaja.Visible = false;
                    DropDownListPostaiRagszamKuldemenyFajtaja.Visible = false;
                    DropDownListPostaiRagszamKuldemenyFajtaja.Enabled = false;

                    HiddenFieldRagszamIgenylesModja.Value = val;
                    break;
                default:
                    TRPostaiRagszamBeviteliMezo.Visible = true;
                    TRPostaiRagszamPostakonyv.Visible = true;
                    TRPostaiRagszamKuldemenyFajtaja.Visible = true;

                    TextBoxPostaiRagszam.Visible = true;
                    TextBoxPostaiRagszam.Enabled = false;
                    DropDownListPostaiRagszamPostakonyv.Visible = true;
                    DropDownListPostaiRagszamPostakonyv.Enabled = false;
                    DropDownListPostaiRagszamKuldemenyFajtaja.Visible = true;
                    DropDownListPostaiRagszamKuldemenyFajtaja.Enabled = false;
                    break;
            }

            //TablePostaiRagszam.Visible = true;
            FillKuldemenyFajta();
            FillPostakonyvek();
        }
        else
        {
            TRPostaiRagszamBeviteliMezo.Visible = false;
            TRPostaiRagszamPostakonyv.Visible = false;
            TRPostaiRagszamKuldemenyFajtaja.Visible = false;

            TextBoxPostaiRagszam.Visible = true;
            TextBoxPostaiRagszam.Enabled = false;

            TRPostaiRagszamPostakonyv.Visible = false;
            DropDownListPostaiRagszamPostakonyv.Visible = false;
            DropDownListPostaiRagszamPostakonyv.Enabled = false;

            TRPostaiRagszamKuldemenyFajtaja.Visible = false;
            DropDownListPostaiRagszamKuldemenyFajtaja.Visible = false;
            DropDownListPostaiRagszamKuldemenyFajtaja.Enabled = false;
        }
    }
    /// <summary>
    /// Hide postai ragszam fields
    /// </summary>
    private void HidePostaiRagszamFields()
    {
        TRPostaiRagszamBeviteliMezo.Visible = false;
        TRPostaiRagszamPostakonyv.Visible = false;
        TRPostaiRagszamKuldemenyFajtaja.Visible = false;

        TextBoxPostaiRagszam.Visible = false;
        TextBoxPostaiRagszam.Enabled = false;
        DropDownListPostaiRagszamPostakonyv.Visible = false;
        DropDownListPostaiRagszamPostakonyv.Enabled = false;
        DropDownListPostaiRagszamKuldemenyFajtaja.Visible = false;
        DropDownListPostaiRagszamKuldemenyFajtaja.Enabled = false;
    }

    private static Dictionary<KRT_KodTarak, KRT_KodTarak> GetDocumentTypeMapping(string vezerloKodcsoportKod, string fuggoKodcsoportKod)
    {
        Dictionary<KRT_KodTarak, KRT_KodTarak> dic = new Dictionary<KRT_KodTarak, KRT_KodTarak>();
        Contentum.eAdmin.Service.KRT_KodtarFuggosegService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_KodtarFuggosegService();

        KRT_KodtarFuggosegSearch search = new KRT_KodtarFuggosegSearch();
        search.Vezerlo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", vezerloKodcsoportKod);
        search.Vezerlo_KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.inner;
        search.Fuggo_KodCsoport_Id.Value = String.Format("select id from krt_kodcsoportok where kod = '{0}' and getdate() between ErvKezd and ErvVege", fuggoKodcsoportKod);
        search.Fuggo_KodCsoport_Id.Operator = Contentum.eQuery.Query.Operators.inner;
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());
        Result res = service.GetAll(execParam, search);

        if (!res.IsError)
        {
            {
                if (res.Ds.Tables[0].Rows.Count > 0)
                {
                    string adat = res.Ds.Tables[0].Rows[0]["Adat"].ToString();
                    KodtarFuggosegDataClass fuggosegek = Contentum.eAdmin.BaseUtility.KodtarFuggoseg.JSONFunctions.DeSerialize(adat);

                    foreach (KodtarFuggosegDataItemClass item in fuggosegek.Items)
                    {
                        var kod1 = GetKodtarElem(item.VezerloKodTarId);
                        var kod2 = GetKodtarElem(item.FuggoKodtarId);
                        if (kod1 != null && kod2 != null)
                        {
                            dic.Add(kod1, kod2);
                        }
                    }
                    return dic;
                }
            }
        }
        return dic;
    }

    private static KRT_KodTarak GetKodtarElem(string kodtarId)
    {
        Contentum.eAdmin.Service.KRT_KodTarakService ktService = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_KodTarakService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());
        execParam.Record_Id = kodtarId;
        Result resultFind = ktService.Get(execParam);
        if (resultFind.IsError)
        {
            return null;
        }
        return (KRT_KodTarak)resultFind.Record;
    }

    private bool CheckPostaiRagszamParameters(out string mode, out string postakonyvId, out string kuldemenyFajta, out string ragszam, out string outRagszamSavId, out string outError)
    {
        mode = null;
        postakonyvId = null;
        kuldemenyFajta = null;
        ragszam = null;
        outRagszamSavId = null;
        outError = null;
        if (!CheckKuldesModjaPostai())
            return true;
        else
        {
            mode = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TERTIVEVENY_RAGSZAM_KIOSZTAS_MODJA);
            GetRagszamParameters(mode, ref postakonyvId, ref kuldemenyFajta, ref ragszam);
            var res = IsValidRagszamParameters(mode, postakonyvId, kuldemenyFajta, ragszam, out outError);
            if (!res)
                return false;

            if (mode == "M")
            {
                if (!CheckRagszamModeManual(ragszam, out postakonyvId, out kuldemenyFajta, out outRagszamSavId, out outError))
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageRagszamValidationError, outError);
                    return false;
                }
            }
            else if (mode == "A")
            {
                if (!CheckRagszamModeAuto(postakonyvId, kuldemenyFajta, out outRagszamSavId, out outError))
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.MessageRagszamValidationError, outError);
                    return false;
                }
            }
        }
        return true;
    }

    private void GetRagszamParameters(string mode, ref string postakonyv, ref string kuldemenyFajta, ref string ragszam)
    {
        switch (mode)
        {
            case "M": //MANU�LIS KIOSZT�S
                ragszam = TextBoxPostaiRagszam.Text;
                break;
        }
        postakonyv = DropDownListPostaiRagszamPostakonyv.SelectedValue;
        kuldemenyFajta = DropDownListPostaiRagszamKuldemenyFajtaja.SelectedValue;
    }
    private bool IsValidRagszamParameters(string mode, string postakonyv, string kuldemenyFajta, string ragszam, out string outError)
    {
        outError = null;
        if (mode == "M")
            if (string.IsNullOrEmpty(ragszam))
            {
                outError = Resources.Error.UI_ValidationMessage_EmptyRagszam;
                //   ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Valid�ci�s hiba", "�res ragsz�m nem megengedett !");
                return false;
            }

        if (string.IsNullOrEmpty(kuldemenyFajta))
        {
            outError = Resources.Error.UI_ValidationMessage_EmptyKudlemenyFajta;
            //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Valid�ci�s hiba", "�res k�ldem�ny fajta nem megengedett !");
            return false;
        }

        if (string.IsNullOrEmpty(postakonyv))
        {
            outError = Resources.Error.UI_ValidationMessage_EmptyPostakonyv;
            //ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Valid�ci�s hiba", "�res postakonyv nem megengedett !");
            return false;
        }

        return true;
    }
    #endregion
    #endregion

    #region WEBMETHODS
    [WebMethod]
    public static string CheckRagszamIgenyles(string allParameters)
    {
        if (string.IsNullOrEmpty(allParameters))
            return Resources.Error.MessageRagszamParametersIncorrect;

        string[] separated = allParameters.Split('|');

        if (separated == null || separated.Length < 2 || separated.Length > 3)
            return Resources.Error.MessageRagszamParametersIncorrect;

        string outError = null;
        string outRagszamSavid = null;
        if (separated.Length == 3 && separated[0] == "A") //auto
        {
            string postakonyv = separated[1];
            string kuldFajta = separated[2];
            if (!CheckRagszamModeAuto(postakonyv, kuldFajta, out outRagszamSavid, out outError))
                return outError;
        }
        else if (separated.Length == 2 && separated[0] == "M") //manual
        {
            string postakonyv = null;
            string kuldFajta = null;
            string ragszam = separated[1];
            if (!CheckRagszamModeManual(ragszam, out postakonyv, out kuldFajta, out outRagszamSavid, out outError))
                return outError;
        }

        return string.Empty;
    }

    #endregion

    private static bool CheckRagszamModeAuto(string postakonyv, string kuldFajta, out string outRagszamSavId, out string outError)
    {
        outError = null;
        string errorString = null;
        outRagszamSavId = null;
        DataRow ragszamSavData = null;
        bool ret = CheckRagszamSavInService(postakonyv, kuldFajta, out errorString, out outRagszamSavId, out ragszamSavData);

        if (!ret)
        {
            outError = errorString;
            return false;
        }

        return true;
    }
    private static bool CheckRagszamModeManual(string ragszamValue, out string postakonyv, out string kuldFajta, out string outRagszamSavId, out string outError)
    {
        outError = null;
        outRagszamSavId = null;
        postakonyv = null;
        kuldFajta = null;
        if (string.IsNullOrEmpty(ragszamValue))
        {
            outError = Resources.Error.MessageRagszamEmpty;
            return false;
        }

        float? ragszamNum = GetRagszamNumber(ragszamValue);
        if (ragszamNum == null)
        {
            outError = Resources.Error.MessageRagszamIncorrect;
            return false;
        }

        string errorStringRagszamExist = null;

        if (!CheckRagszamExistingInService(ragszamValue, out postakonyv, out outRagszamSavId, out errorStringRagszamExist))
        {
            outError = errorStringRagszamExist;
            return false;
        }

        return true;
    }
    private static bool CheckRagszamSavInService(string postakonyv, string kuldFajta, out string errorString, out string outRagSzamSavId, out DataRow outRagSzamSav)
    {
        outRagSzamSavId = null;
        errorString = null;
        outRagSzamSav = null;
        Result result = GetAllRagszamSavFilteredByPkKf(postakonyv, kuldFajta);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds == null || result.Ds.Tables.Count < 1 || result.Ds.Tables[0].Rows.Count < 1)
        {
            errorString = Resources.Error.MessageRagszamSavNotFound;
            return false;
        }

        //if (result.Ds.Tables[0].Rows.Count > 1)
        //{
        //    errorString = Resources.Error.MessageRagszamSavMoreThanOne;
        //    return false;
        //}

        outRagSzamSavId = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        outRagSzamSav = result.Ds.Tables[0].Rows[0];
        return true;
    }
    private static bool CheckRagszamExistingInService(string ragszam, string postakonyv, string ragSzamSavId, out string errorString)
    {
        float? ragszamNum = GetRagszamNumber(ragszam);
        errorString = null;
        Result result = FindRagszamByCode(ragszam, ragszamNum.ToString(), postakonyv, ragSzamSavId);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count >= 1)
        {
            errorString = Resources.Error.MessageRagszamExist;
            return false;
        }
        return true;
    }

    private static bool CheckRagszamExistingInService(string ragszam, out string postakonyv, out string ragSzamSavId, out string errorString)
    {
        float? ragszamNum = GetRagszamNumber(ragszam);
        errorString = null;
        ragSzamSavId = null;
        postakonyv = null;
        Result result = FindRagszamByCode(ragszam, ragszamNum.ToString());
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            errorString = result.ErrorMessage;
            return false;
        }
        if (result.Ds != null && result.Ds.Tables.Count >= 1 && result.Ds.Tables[0].Rows.Count >= 1)
        {
            postakonyv = result.Ds.Tables[0].Rows[0]["Postakonyv_Id"].ToString();
            string localPostaKonyv = postakonyv;

            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

            EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
            Search.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Postakonyv);
            Result erec_IraIktatoKonyvek_result = service.GetAllWithExtensionAndJogosultsag(execParam, Search, true);
            if (erec_IraIktatoKonyvek_result.IsError)
            {
                errorString = erec_IraIktatoKonyvek_result.ErrorMessage;
                return false;
            }
            if (erec_IraIktatoKonyvek_result.Ds.Tables.Count < 1 || erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count < 1)
            {
                errorString = "Nem tal�lhat� postak�nyv vagy nincs jogosults�ga.";
                return false;
            }

            var konyvek = erec_IraIktatoKonyvek_result.Ds.Tables[0].AsEnumerable();

            if (!konyvek.Any(x => x["Id"].ToString() == localPostaKonyv))
            {
                errorString = "Nem tal�lhat� a ragsz�m postak�nyve vagy nincs jogosults�ga.";
                return false;
            }

            ragSzamSavId = result.Ds.Tables[0].Rows[0]["RagszamSav_Id"] == DBNull.Value ? null : result.Ds.Tables[0].Rows[0]["RagszamSav_Id"].ToString();
            string objId = result.Ds.Tables[0].Rows[0]["Obj_Id"] == DBNull.Value ? null : result.Ds.Tables[0].Rows[0]["Obj_Id"].ToString();
            string allapot = result.Ds.Tables[0].Rows[0]["Allapot"].ToString();

            if (string.IsNullOrEmpty(ragSzamSavId))
            {
                errorString = "A megadott ragsz�m nem szerepel ragsz�ms�vban.";
                return false;
            }

            if (allapot != "H")
            {
                errorString = "A megadott ragsz�m �llapota nem haszn�lhat�.";
                return false;
            }

            if (!string.IsNullOrEmpty(objId))
            {
                errorString = Resources.Error.MessageRagszamExist;
                return false;
            }
        }
        else
        {
            errorString = "A megadott ragsz�m nem tal�lhat�.";
            return false;
        }

        return true;
    }

    #region SERVICE CALL
    /// <summary>
    /// Get all allokalt ragszamsav by postakonyv and kuldemeny fajta.
    /// </summary>
    /// <param name="postakonyv"></param>
    /// <param name="kuldFajta"></param>
    /// <returns></returns>
    private static Result GetAllRagszamSavFilteredByPkKf(string postakonyv, string kuldFajta)
    {
        KRT_RagszamSavokService service = eRecordService.ServiceFactory.GetKRT_RagszamSavokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagszamSavokSearch search = new KRT_RagszamSavokSearch();
        search.Postakonyv_Id.Filter(postakonyv);
        search.SavType.Filter(kuldFajta);
        search.SavAllapot.Filter("H");

        return service.GetAll(execParam, search);
    }

    private static Result FindRagszamByCode(string ragszam, string ragszamNum, string postakonyv, string ragszamSavId)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Filter(ragszam);
        search.KodNum.Filter(ragszamNum);
        search.Postakonyv_Id.Filter(postakonyv);
        search.KodType.Filter("H");
        search.RagszamSav_Id.Filter(ragszamSavId);

        return service.GetAll(execParam, search);
    }
    private static Result FindRagszamByCode(string ragszam, string ragszamNum)
    {
        KRT_RagSzamokService service = eRecordService.ServiceFactory.GetKRT_RagSzamokService();
        ExecParam execParam = UI.SetExecParamDefault(HttpContext.Current.Session, new ExecParam());

        KRT_RagSzamokSearch search = new KRT_RagSzamokSearch();
        search.Kod.Filter(ragszam);

        //search.KodNum.Value = ragszamNum;
        //search.KodNum.Operator = Query.Operators.equals;

        search.Allapot.Filter("H");

        return service.GetAll(execParam, search);
    }

    private Result FindKimenoKuldemeny(string id)
    {
        EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        return service.Get(execParam);
    }
    #endregion

    #region HELPER
    public static float? GetRagszamNumber(string value)
    {
        int index = int.MinValue;
        for (int i = 0; i < value.Length; i++)
        {
            byte val;
            if (byte.TryParse(value[i].ToString(), out val))
            {
                index = i;
                break;
            }
        }
        if (index != int.MinValue)
        {
            string numString = value.Substring(index);
            float ret = float.MinValue;
            if (float.TryParse(numString, out ret))
            {
                return ret;
            }
        }
        return null;
    }
    #endregion

    #region csatolm�nyok

    private String[] CreateIratArrayFromTreeView()
    {
        String[] iratArray = new String[TreeView1.Nodes.Count];

        for (int i = 0; i < TreeView1.Nodes.Count; i++)
        {
            iratArray[i] = TreeView1.Nodes[i].Value;
        }

        return iratArray;
    }

    protected void CsatolmanyokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsatolmanyokGridView", ViewState, "IktatoSzam_Merge, FajlNev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsatolmanyokGridView", ViewState);
        CsatolmanyokGridViewBind(sortExpression, sortDirection);
    }

    protected void CsatolmanyokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponent.ClearDokumentElements();

        UI.ClearGridViewRowSelection(CsatolmanyokGridView);
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        EREC_CsatolmanyokService service = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch search = new EREC_CsatolmanyokSearch();

        if (IsTomegesExpedialas)
        {
            search.IraIrat_Id.Filter(IratId);
        }
        else
        {
            String[] iratArray = CreateIratArrayFromTreeView();
            search.IraIrat_Id.In(iratArray);
        }

        search.OrderBy = Search.GetOrderBy("CsatolmanyokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        result = service.GetAllWithExtension(ExecParam, search);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            return;
        }

        List<string> selectedDokumentumokList = GetSelectedDokumentumok();

        ui.GridViewFill(CsatolmanyokGridView, result, FormHeader1.ErrorPanel, null);

        SetSelectedDokumentumok(selectedDokumentumokList);

        SetDefaultSelectedDokumentum();
    }

    protected void CsatolmanyokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, selectedRowNumber, "check");
        }
    }

    protected void CsatolmanyokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsatolmanyokGridViewBind(e.SortExpression, UI.GetSortToGridView("CsatolmanyokGridView", ViewState, e.SortExpression));
    }

    protected void CsatolmanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SetFileIcons_GridViewRowDataBound(e, Page, "fileIcon_ImageButton");
    }

    public void SetFileIcons_GridViewRowDataBound(GridViewRowEventArgs e, Page page, string imageButtonId)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            string formatum = String.Empty;
            string externalLink = String.Empty;

            if (drv != null && drv["Formatum"] != null)
            {
                formatum = drv["Formatum"].ToString();
            }

            if (drv != null && drv["External_Link"] != null)
            {
                //externalLink = drv["External_Link"].ToString();
                externalLink = "GetDocumentContent.aspx?id=" + drv["krtDok_Id"];
            }

            if (!string.IsNullOrEmpty(formatum))
            {
                ImageButton fileIcon = (ImageButton)e.Row.FindControl(imageButtonId);

                if (fileIcon != null)
                {
                    fileIcon.ImageUrl = "~/images/hu/fileicons/" + FileFunctions.GetFileIconNameByFormatum(formatum);

                    if (!String.IsNullOrEmpty(externalLink))
                    {
                        fileIcon.OnClientClick = "window.open('" + externalLink + "'); return false;";
                        fileIcon.Enabled = true;
                    }

                    DokumentumVizualizerComponent.AddDokumentElement(
                        fileIcon.ClientID, drv["krtDok_Id"].ToString(), drv["FajlNev"].ToString(), drv["VerzioJel"].ToString(), drv["External_Link"].ToString());
                }
            }
        }
    }

    private List<string> GetSelectedDokumentumok()
    {
        return UI.GetGridViewColumnValuesOfSelectedRows(CsatolmanyokGridView, "Label_krtDok_Id");
    }

    private void SetDefaultSelectedDokumentum()
    {
        if (CsatolmanyokGridView.Rows.Count == 1)
        {
            UI.SetGridViewCheckBoxesToSelectedRow(CsatolmanyokGridView, 0, "check");
        }
    }

    bool IsSelectedDokumentum()
    {
        List<string> selectedDokumentumok = GetSelectedDokumentumok();

        if (selectedDokumentumok.Count == 0)
        {
            ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "Figyelmeztet�s", "Nincs kiv�lasztva csatolm�ny!");
            return false;
        }
        else
        {
            return true;
        }
    }

    void SetSelectedDokumentumokByKuldemeny(string kuldemenyId)
    {
        EREC_CsatolmanyokService csatService = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch csatSearch = new EREC_CsatolmanyokSearch();
        csatSearch.KuldKuldemeny_Id.Filter(kuldemenyId);

        ExecParam execParam = UI.SetExecParamDefault(Page);

        Result res = csatService.GetAll(execParam, csatSearch);

        if (!res.IsError)
        {
            List<string> dokumentumIdList = new List<string>();
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string dokumentumId = row["Dokumentum_Id"].ToString();
                dokumentumIdList.Add(dokumentumId);
            }

            SetSelectedDokumentumok(dokumentumIdList);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
        }
    }

    void SetSelectedDokumentumok(List<string> dokumentumIdList)
    {
        foreach (GridViewRow gridRow in CsatolmanyokGridView.Rows)
        {
            Label label = (Label)gridRow.FindControl("Label_krtDok_Id");

            if (label != null)
            {
                string dokId = label.Text;

                CheckBox check = (CheckBox)gridRow.FindControl("check");

                if (check != null)
                {
                    if (dokumentumIdList.Contains(dokId))
                    {
                        check.Checked = true;
                    }
                    else
                    {
                        check.Checked = false;
                    }
                }
            }
        }
    }

    #endregion

    #region CIMZETTLISTA

    private void InitializeCimzettLista()
    {
        string pldIratId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);
        if (!string.IsNullOrEmpty(pldIratId))
        {
            EREC_PldIratPeldanyok pldIrat = GetPldIrat(pldIratId);

            IratElosztoIvListaUserControl.InitializeWithIrat(pldIrat.IraIrat_Id);
            if (!string.IsNullOrEmpty(IratElosztoIvListaUserControl.ElosztoIvId))
            {
                IratElosztoIvListaUserControl.LoadData();
                VisibleCimzettLista(true);
                //IsCimzettListasExpedialas = true;
            }
            else
            {
                VisibleCimzettLista(false);
                return;
            }
        }
        else
        {
            VisibleCimzettLista(false);
        }

    }
    private void VisibleCimzettLista(bool visible)
    {
        EFormPanelCimzettLista.Visible = visible;
        panelIratPeldany.Visible = !visible;
    }

    /// <summary>
    /// Irat p�ld�ny lek�r�se id-b�l
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    private EREC_PldIratPeldanyok GetPldIrat(string id)
    {
        Contentum.eRecord.Service.EREC_PldIratPeldanyokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;
        Result result = service.Get(execParam);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("eRecordComponent_IratElosztoIvListaUserControl.SaveElosztoIvIdToPldIrat ", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "EIV.CTRL.NoSelectedItem", "javascript:alert('" + result.ErrorCode + " " + result.ErrorMessage + "');", true);
            return null;
        }
        EREC_PldIratPeldanyok item = (EREC_PldIratPeldanyok)result.Record;
        return item;
    }
    #endregion

    #region Hivatali kapu

    private String[] CreateIratPeldanyAzonositoArrayFromTreeView()
    {
        String[] pldArray = new String[TreeView1.Nodes.Count];

        for (int i = 0; i < TreeView1.Nodes.Count; i++)
        {
            pldArray[i] = TreeView1.Nodes[i].ChildNodes[0].Text;
        }

        return pldArray;
    }

    void LoadDoktipusAdatok()
    {
        if (!IsKrFileSelected())
        {
            string[] peldanyAzonositoArray = CreateIratPeldanyAzonositoArrayFromTreeView();

            string doktipusAzonosito = String.Join(", ", peldanyAzonositoArray);

            string[] iratIds = CreateIratArrayFromTreeView();
            EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
            iratokSearch.Id.In(iratIds);
            ExecParam execParam = UI.SetExecParamDefault(Page);

            Result result = iratokService.GetAll(execParam, iratokSearch);

            string doktipusLeiras = String.Empty;
            if (!result.IsError)
            {
                List<string> iratTargyList = new List<string>();
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    iratTargyList.Add(row["Targy"].ToString());
                }
                doktipusLeiras = String.Join(", ", iratTargyList.ToArray());
            }


            KR_DokTipusAzonosito.Text = doktipusAzonosito;
            KR_DokTipusLeiras.Text = doktipusLeiras;
        }

    }

    string GetDokTipusHivatal()
    {
        return FelhasznaloProfil.OrgKod(Page);
    }

    void FillFiokokDropDownList()
    {
        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        string[] fiokok = svc.GetHivataliKapuFiokok();

        KR_Fiok.Items.Clear();
        TomegesKR_Fiok.Items.Clear();
        foreach (string fiok in fiokok)
        {
            KR_Fiok.Items.Add(new ListItem(fiok, fiok));
            TomegesKR_Fiok.Items.Add(new ListItem(fiok, fiok));
        }
    }

    #endregion

    #region KR File

    private string LoadedKRFileName
    {
        get
        {
            object o = ViewState["LoadedKRFileName"];

            if (o == null)
            {
                return String.Empty;
            }
            else
            {
                return o as string;
            }
        }
        set
        {
            ViewState["LoadedKRFileName"] = value;
        }
    }

    protected bool IsKrFile(string fajlNev)
    {
        return ".kr".Equals(Path.GetExtension(fajlNev), StringComparison.CurrentCultureIgnoreCase);
    }

    List<string> GetselectedKRFiles()
    {
        return UI.GetGridViewColumnValuesOfSelectedRows(CsatolmanyokGridView, "labelFajlNev").Where(f => IsKrFile(f)).ToList();
    }

    bool IsKrFileSelected()
    {
        List<string> selectedFajlNevek = GetselectedKRFiles();

        if (selectedFajlNevek.Count == 1)
        {
            string fajlNev = selectedFajlNevek[0];
            return IsKrFile(fajlNev);
        }

        return false;
    }

    string GetKRFileName()
    {
        List<string> selectedFajlNevek = GetselectedKRFiles();

        if (selectedFajlNevek.Count == 1)
        {
            string fajlNev = selectedFajlNevek[0];
            if (IsKrFile(fajlNev))
                return fajlNev;
        }

        return String.Empty;
    }

    bool IsAnyKrFileSelected()
    {
        List<string> selectedFajlNevek = GetselectedKRFiles();
        return selectedFajlNevek.Count > 0;
    }

    void LoadKRFileMetaAdatok()
    {
        try
        {
            string externalLink = UI.GetGridViewColumnValuesOfSelectedRows(CsatolmanyokGridView, "labelExternal_Link")[0];
            byte[] fileContent = GetDokTartalom(externalLink);
            EREC_eBeadvanyok eBeadvanyok = GetKRFileMataAdatok(fileContent);

            if (eBeadvanyok != null)
            {
                KR_DokTipusAzonosito.Text = eBeadvanyok.KR_DokTipusAzonosito;
                KR_DokTipusAzonosito.ReadOnly = true;
                KR_DokTipusLeiras.Text = eBeadvanyok.KR_DokTipusLeiras;
                KR_DokTipusLeiras.ReadOnly = true;
                KR_Megjegyzes.Text = eBeadvanyok.KR_Megjegyzes;
                KR_Megjegyzes.ReadOnly = true;

                if (!String.IsNullOrEmpty(eBeadvanyok.PartnerRovidNev))
                {
                    HivatalListaElem cimzett = GetCimzett(eBeadvanyok.PartnerRovidNev);
                    if (cimzett != null)
                    {
                        rbCimzettTipusHivatal.Checked = true;
                        rbCimzettTipusSzemely.Checked = false;
                        HivatalNeve.Text = cimzett.Nev;
                        HivatalKRID.Text = cimzett.KRID;
                        HivatalRovidNeve.Value = cimzett.RovidNev;
                        HivatalMAKKod.Value = cimzett.MAKKod;
                        SetCimzettekReadonly(true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "KR f�jl feldogloz�si hiba", ex.Message);
        }
    }

    EREC_eBeadvanyok GetKRFileMataAdatok(byte[] fileContent)
    {
        EREC_eBeadvanyok adatok = new EREC_eBeadvanyok();

        try
        {
            MemoryStream ms = new MemoryStream(fileContent);
            XmlReader reader = XmlReader.Create(ms);
            XDocument doc = XDocument.Load(reader);

            var fejlec = from element in doc.Root.Elements()
                         where element.Name.LocalName == "Fejlec"
                         select element;

            adatok.PartnerRovidNev = (from element in fejlec.Elements()
                                      where element.Name.LocalName == "Cimzett"
                                      select element.Value).FirstOrDefault();

            adatok.KR_DokTipusAzonosito = (from element in fejlec.Elements()
                                           where element.Name.LocalName == "DokTipusAzonosito"
                                           select element.Value).FirstOrDefault();

            adatok.KR_DokTipusLeiras = (from element in fejlec.Elements()
                                        where element.Name.LocalName == "DokTipusLeiras"
                                        select element.Value).FirstOrDefault();

            adatok.KR_FileNev = (from element in fejlec.Elements()
                                 where element.Name.LocalName == "FileNev"
                                 select element.Value).FirstOrDefault();

            adatok.KR_Megjegyzes = (from element in fejlec.Elements()
                                    where element.Name.LocalName == "Megjegyzes"
                                    select element.Value).FirstOrDefault();
        }
        catch (Exception ex)
        {
            Logger.Error("GetKRFileMataAdatok.Error", ex);
            throw;
        }

        return adatok;
    }

    private byte[] GetDokTartalom(string externalLink)
    {
        using (WebClient client = new WebClient())
        {
            client.Credentials = new NetworkCredential(UI.GetAppSetting("SharePointUserName"), UI.GetAppSetting("SharePointPassword"), UI.GetAppSetting("SharePointUserDomain"));
            return client.DownloadData(externalLink);
        }
    }

    public HivatalListaElem GetCimzett(string rovidNev)
    {
        HivatalListaElem cimzett = null;

        ExecParam execParam = UI.SetExecParamDefault(Page);

        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        HivatalSzuroParameterek szuroParameterk = new HivatalSzuroParameterek();
        szuroParameterk.RovidNev = rovidNev;
        szuroParameterk.Tipus = HivatalTipus.HivataliKapu;
        Result result = svc.HivatalokListajaFeldolgozas_Szurt(KR_Fiok.SelectedValue, szuroParameterk);

        if (!result.IsError)
        {
            HivatalokListajaValasz valasz = result.Record as HivatalokListajaValasz;
            foreach (HivatalListaElem hivatal in valasz.HivatalokLista)
            {
                if (hivatal.RovidNev.Equals(rovidNev, StringComparison.CurrentCultureIgnoreCase))
                {
                    cimzett = hivatal;
                    break;
                }
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }

        return cimzett;
    }

    private void SetCimzettekReadonly(bool readOnly)
    {
        rbCimzettTipusHivatal.Enabled = !readOnly;
        rbCimzettTipusSzemely.Enabled = !readOnly;
        searchHivatal.Enabled = !readOnly;
        HivatalNeve.ReadOnly = readOnly;
        HivatalKRID.ReadOnly = readOnly;
    }

    void LoadDefaultMetaAdatok()
    {
        KR_DokTipusAzonosito.ReadOnly = false;
        KR_DokTipusLeiras.ReadOnly = false;
        KR_Megjegyzes.ReadOnly = false;
        KR_DokTipusLeiras.Text = String.Empty;
        LoadDoktipusAdatok();

        EREC_PldIratPeldanyokService peldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        ExecParam execParam = UI.SetExecParamDefault(Page);
        execParam.Record_Id = IratPeldany_Id;
        Result res = peldanyokService.Get(execParam);

        if (!res.IsError)
        {
            HivatalNeve.Text = String.Empty;
            HivatalKRID.Text = String.Empty;
            HivatalRovidNeve.Value = String.Empty;
            HivatalMAKKod.Value = String.Empty;
            LoadComponentsFromBusinessObject(res.Record as EREC_PldIratPeldanyok);
        }


        SetCimzettekReadonly(false);
    }

    #endregion

    #region Tomeges expedi�l�s

    private void TomegesExpedialas()
    {
        if (FunctionRights.GetFunkcioJog(Page, funkciokod_Expedialas))
        {
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);

            if (selectedItemsList.Count == 0)
            {

                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                return;
            }



            string[] dokumentumId_Array = null;
            if (IsCsatolmanySelectionEnabled)
            {
                if (!IsSelectedDokumentum())
                {
                    return;
                }

                List<string> dokumentumId_List = GetSelectedDokumentumok();
                dokumentumId_Array = dokumentumId_List.ToArray();
            }

            HivataliKapuFileSettings fileSettings = GetFileSettingsFromComponents();
            fileSettings.PackFiles = false;

            EREC_eBeadvanyok _EREC_eBeadvanyok = new EREC_eBeadvanyok();
            _EREC_eBeadvanyok.KR_Fiok = TomegesKR_Fiok.SelectedValue;
            _EREC_eBeadvanyok.Updated.KR_Fiok = true;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());


            String[] IratPeldanyIds = selectedItemsList.ToArray();
            using (EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService())
            {
                service.Timeout = 1800000;
                Result result = service.TomegesExpedialas(execParam, IratPeldanyIds, dokumentumId_Array, fileSettings, _EREC_eBeadvanyok);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    JavaScripts.RegisterSelectedRecordIdToParent(Page, IratPeldany_Id);

                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UI.MarkFailedRecordsInGridViewAndSetInfo(PldIratPeldanyokGridView, result, Constants.TableNames.EREC_PldIratPeldanyok);
                    UI.MarkSuccededRecordsInGridView(PldIratPeldanyokGridView, result, new List<string>(IratPeldanyIds));
                }
            }
        }
    }

    List<string> GetIratPeldanyai(string iratId)
    {
        List<string> peldanyok = new List<string>();

        EREC_PldIratPeldanyokService peldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        ExecParam xpm = UI.SetExecParamDefault(Page);

        EREC_PldIratPeldanyokSearch peldanyokSearch = new EREC_PldIratPeldanyokSearch();
        peldanyokSearch.IraIrat_Id.Filter(iratId);

        Result res = peldanyokService.GetAll(xpm, peldanyokSearch);

        if (res.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
        }
        else
        {
            foreach (DataRow row in res.Ds.Tables[0].Rows)
            {
                string peldanyId = row["Id"].ToString();
                peldanyok.Add(peldanyId);
            }
        }

        return peldanyok;
    }

    bool ExistHivataliKapuPeldany(DataSet peldanyok)
    {
        if (peldanyok != null)
        {
            foreach (DataRow row in peldanyok.Tables[0].Rows)
            {
                string kuldesMod = row["KuldesMod"].ToString();

                if (kuldesMod == KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu)
                {
                    return true;
                }
            }
        }

        return false;
    }

    string GetIratId(DataSet peldanyok)
    {
        if (peldanyok != null)
        {
            List<string> iratIds = new List<string>();
            foreach (DataRow row in peldanyok.Tables[0].Rows)
            {
                string iratId = row["IraIrat_Id"].ToString();
                if (!iratIds.Contains(iratId))
                    iratIds.Add(iratId);
            }

            if (iratIds.Count == 1)
                return iratIds[0];
        }

        return String.Empty;
    }

    bool ExistSelectedHivataliKapuPeldany()
    {
        List<string> selectedPeldanyokKuldesMod = UI.GetGridViewColumnValuesOfSelectedRows(PldIratPeldanyokGridView, "labelKuldesMod");
        return selectedPeldanyokKuldesMod.Exists(s => s.Equals(KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu));
    }

    #endregion

    bool ExistsSajatPublikusKulcs(out string msg)
    {
        msg = null;

        string value = Rendszerparameterek.Get(Page, Contentum.eUtility.GPGEncryptFileManager.ENCRYPT_SAJAT_PUBLIKUS_KULCS_PATH);

        if (String.IsNullOrEmpty(value))
        {
            msg = "Nincs megadva saj�t publikus kulcs!";
            return false;
        }

        if (!File.Exists(value))
        {
            msg = "A rendszerparam�terben megadott publikus kulcsot tartalmaz� f�jl nem tal�lhat�!";
            return false;
        }

        return true;
    }

    #region PostaHibrid

    void SetPostaHibridComponents()
    {
        if (IsKuldesModPostaHibrid)
        {
            panelPostaHibrid.Visible = true;
        }
        else
        {
            panelPostaHibrid.Visible = false;
        }
    }

    #endregion

    /// <summary>
    /// Szemely objektum alapj�n be�ll�tja a kontolokat.
    /// </summary>
    /// <param name="partnerId"></param>
    private void GetAndSetSzemelyAdatok(string partnerId)
    {
        if (string.IsNullOrEmpty(partnerId))
            return;

        ExecParam execParam = UI.SetExecParamDefault(Page);
        KRT_SzemelyekService serviceSzemely = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_SzemelyekService();
        KRT_SzemelyekSearch src = new KRT_SzemelyekSearch();
        src.Partner_Id.Filter(partnerId);
        src.TopRow = 1;

        Result result = serviceSzemely.GetAll(execParam, src);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            Logger.Error("ExpedialasForm.SetSzemelyAdatok", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ExpedialasForm.LoadSzemelyAdatok", "javascript:alert('" + result.ErrorCode + " " + result.ErrorMessage + "');", true);
            return;
        }

        if (result.Ds.Tables[0].Rows.Count < 1)
            return;

        execParam.Record_Id = result.Ds.Tables[0].Rows[0]["Id"].ToString();
        if (string.IsNullOrEmpty(execParam.Record_Id))
            return;

        Result resultSzemely = serviceSzemely.Get(execParam);
        if (!string.IsNullOrEmpty(resultSzemely.ErrorCode))
        {
            Logger.Error("ExpedialasForm.SetSzemelyAdatok", execParam, result);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ExpedialasForm.LoadSzemelyAdatok", "javascript:alert('" + resultSzemely.ErrorCode + " " + resultSzemely.ErrorMessage + "');", true);
            return;
        }
        if (resultSzemely.Record == null)
            return;

        KRT_Szemelyek szemelyObj = (KRT_Szemelyek)resultSzemely.Record;
        SetSzemelyAdatok(szemelyObj);
    }
    /// <summary>
    /// Kontrolok be�ll�t�sa szem�ly alapj�n
    /// </summary>
    /// <param name="szemelyObj"></param>
    private void SetSzemelyAdatok(KRT_Szemelyek szemelyObj)
    {
        SzuletesDatuma.Text = szemelyObj.SzuletesiIdo;
        SzuletesiHely.Text = szemelyObj.SzuletesiHely;
        SzuletesiNevVezetekNev.Text = szemelyObj.UjCsaladiNev;
        SzuletesiNevUtonev1.Text = szemelyObj.UjUtonev;
        SzuletesiNevUtonev2.Text = szemelyObj.UjTovabbiUtonev;

        SzuletesiNevVezetekNev.Text = szemelyObj.SzuletesiCsaladiNev;
        SzuletesiNevUtonev1.Text = szemelyObj.SzuletesiElsoUtonev;
        SzuletesiNevUtonev2.Text = szemelyObj.SzuletesiTovabbiUtonev;

        ViseltNevVezetekNev.Text = szemelyObj.UjCsaladiNev;
        ViseltNevUtonev1.Text = szemelyObj.UjUtonev;
        ViseltNevUtonev2.Text = szemelyObj.UjTovabbiUtonev;

        AnyjaNeveUtonev2.Text = szemelyObj.AnyjaNeveTovabbiUtonev;
        AnyjaNeveUtonev1.Text = szemelyObj.AnyjaNeveElsoUtonev;
        AnyjaNeveVezetekNev.Text = szemelyObj.AnyjaNeveCsaladiNev;
    }

    private bool IsOneKikuldhetoCsatolmany(string peldanyId)
    {
        ExecParam execParam = UI.SetExecParamDefault(Page);
        EREC_CsatolmanyokService csatolmanyokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
        EREC_CsatolmanyokSearch csatolmanyokSearch = new EREC_CsatolmanyokSearch();
        csatolmanyokSearch.IraIrat_Id.Value = String.Format("select IraIrat_Id from EREC_PldIratPeldanyok where Id = '{0}'", peldanyId);
        csatolmanyokSearch.IraIrat_Id.Operator = Query.Operators.inner;

        Result csatolmanyokResult = csatolmanyokService.GetAllWithExtension(execParam, csatolmanyokSearch);

        if (!csatolmanyokResult.IsError)
        {

            int csatolmanyokSzama = csatolmanyokResult.GetCount;

            int hitelesCsatolmanyokSzama = 0;

            if (csatolmanyokSzama > 1)
            {
                foreach (DataRow row in csatolmanyokResult.Ds.Tables[0].Rows)
                {
                    string elektronikusAlairas = row["ElektronikusAlairas"].ToString();

                    if (!String.IsNullOrEmpty(elektronikusAlairas) && elektronikusAlairas != "0")
                    {
                        hitelesCsatolmanyokSzama++;
                    }
                }
            }

            return csatolmanyokSzama == 1 || hitelesCsatolmanyokSzama == 1;
        }

        return false;
    }
}
