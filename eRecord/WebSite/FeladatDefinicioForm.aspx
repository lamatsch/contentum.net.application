<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FeladatDefinicioForm.aspx.cs" Inherits="FeladatDefinicioForm" Title="Feladat defin�ci�k karbantart�sa" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList"
    TagPrefix="kddl" %>
<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="ftb" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Component/FuggoKodtarakDropDownList.ascx" TagPrefix="uc7" TagName="FuggoKodtarakDropDownList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 868px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFunkcioKivaltoStar" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelFunkcioKivalto" runat="server" Text="Kiv�lt� funkci�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <ftb:FunkcioTextBox ID="FunkcioTextBox_FunkcioKivalto" runat="server" Validate="false" AjaxEnabled="true" FeladatDefinicioMode="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelDefinicioTipus" runat="server" Text="Defin�ci� t�pus:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodTarakDropDownList_DefinicioTipus" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td colspan="2">
                                    <eUI:eFormPanel ID="AtfutasiIdoPanel" runat="server">
                                        <table width="100%">
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelIdobazis" runat="server" Text="Id�b�zis:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <kddl:KodTarakDropDownList ID="KodTarakDropDownList_Idobazis" runat="server" CssClass="mrUrlapInputKozepes" />
                                                            </td>
                                                            <td>
                                                                <uc9:ObjTipusokTextBox ID="ObjTipusokTextBox_ObjTipusokDateCol" CssClass="mrUrlapInput"
                                                                    Validate="false" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelAtfutasiIdo" runat="server" Text="Id�tartam:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <uc4:RequiredNumberBox ID="RequiredNumberBox_AtfutasiIdo" Validate="false" CssClass="mrUrlapInputNumber"
                                                                    runat="server" />
                                                            </td>
                                                            <td>
                                                                <%--BLG_2156--%>
                                                                <%--<kddl:KodTarakDropDownList ID="KodtarakDropDownList_Idoegyseg" CssClass="mrUrlapInputRovid"
                                                                    runat="server" />--%>
                                                               <uc7:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" KodcsoportKod="IDOEGYSEG_FELHASZNALAS"   CssClass="hiddenitem" />
                                                               <uc7:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_Idoegyseg" KodcsoportKod="IDOEGYSEG"  ParentControlId="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" CssClass="mrUrlapInputComboBoxKozepes" />

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <%--<asp:Label ID="labelJelzesElojel" runat="server" Text="Jelz�s:" />--%>
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:RadioButtonList ID="rblJelzesElojel" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="b�zisid� el�tt" Value="+" Selected="True" />
                                                        <asp:ListItem Text="b�zisid� ut�n" Value="-" />
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </eUI:eFormPanel>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFeladatLeirasStar" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelFeladatLeiras" runat="server" Text="Feladat le�r�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="RequiredTextBox_FeladatLeiras" Validate="false" runat="server"
                                        TextBoxMode="MultiLine" Rows="3" CssClass="mrUrlapInputSzeles" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelPrioritasStar" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelPrioritas" runat="server" Text="Priorit�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_Prioritas" CssClass="mrUrlapInput"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelLezarasPrioritas" runat="server" Text="Lez�r�s priorit�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_LezarasPrioritas" CssClass="mrUrlapInput"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelErvenyessegStar" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                    </uc6:ErvenyessegCalendarControl>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                &nbsp; &nbsp;&nbsp; &nbsp;
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
