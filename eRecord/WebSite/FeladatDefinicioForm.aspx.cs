using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class FeladatDefinicioForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const string kcs_FELADAT_DEFINICIO_TIPUS = "FELADAT_DEFINICIO_TIPUS";
    private const string kcs_FELADAT_IDOBAZIS = "FELADAT_IDOBAZIS";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";
    private const string kcs_FELADAT_PRIORITAS = "FELADAT_PRIORITAS";

    #region utility
    static class BoolString
    {
        public const string Yes = "1";
        public const string No = "0";
        public const string NotSet = "X";
    }
    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        FunkcioTextBox_FunkcioKivalto.ReadOnly = true;
        KodTarakDropDownList_DefinicioTipus.ReadOnly = true;
        KodTarakDropDownList_Idobazis.ReadOnly = true;
        ObjTipusokTextBox_ObjTipusokDateCol.ReadOnly = true;
        rblJelzesElojel.Enabled = false;
        RequiredNumberBox_AtfutasiIdo.ReadOnly = true;
        // BLG_2156
        //KodtarakDropDownList_Idoegyseg.ReadOnly = true;
        FuggoKodtarakDropDownList_Idoegyseg.ReadOnly = true;

        RequiredTextBox_FeladatLeiras.ReadOnly = true;
        KodtarakDropDownList_Prioritas.ReadOnly = true;
        KodtarakDropDownList_LezarasPrioritas.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;

        labelFunkcioKivalto.CssClass = "mrUrlapInputWaterMarked";
        labelDefinicioTipus.CssClass = "mrUrlapInputWaterMarked";
        labelIdobazis.CssClass = "mrUrlapInputWaterMarked";
        //labelObjTipusokDateCol.CssClass = "mrUrlapInputWaterMarked";
        //labelJelzesElojel.CssClass = "mrUrlapInputWaterMarked";
        labelAtfutasiIdo.CssClass = "mrUrlapInputWaterMarked";
        //labelIdoegyseg.CssClass = "mrUrlapInputWaterMarked";
        labelFeladatLeiras.CssClass = "mrUrlapInputWaterMarked";
        labelPrioritas.CssClass = "mrUrlapInputWaterMarked";
        labelLezarasPrioritas.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";

        ObjTipusokTextBox_ObjTipusokDateCol.ViewMode = true;
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        FunkcioTextBox_FunkcioKivalto.ReadOnly = true;
        KodTarakDropDownList_DefinicioTipus.ReadOnly = true;
        KodTarakDropDownList_Idobazis.ReadOnly = true;
        ObjTipusokTextBox_ObjTipusokDateCol.ReadOnly = true;

        RequiredTextBox_FeladatLeiras.Validate = true;
        KodtarakDropDownList_Prioritas.Validate = true;

        ObjTipusokTextBox_ObjTipusokDateCol.ViewMode = true;
    }

    /// <summary>
    /// New m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetNewControls()
    {
        // nem lehet �jat l�trehozni!
        EFormPanel1.Visible = false;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatDefinicio" + Command);
                break;
        }


        if (!IsPostBack)
        {
            KodTarakDropDownList_DefinicioTipus.FillDropDownList(kcs_FELADAT_DEFINICIO_TIPUS, true, FormHeader1.ErrorPanel);
            KodTarakDropDownList_Idobazis.FillDropDownList(kcs_FELADAT_IDOBAZIS, true, FormHeader1.ErrorPanel);
            // BLG_2156
            //KodtarakDropDownList_Idoegyseg.FillDropDownList(kcs_IDOEGYSEG, false, FormHeader1.ErrorPanel);
            FuggoKodtarakDropDownList_IdoegysegFelhasznalas.SelectedValue = KodTarak.IdoegysegFelhasznalas.Hatarido;

            KodtarakDropDownList_Prioritas.FillDropDownList(kcs_FELADAT_PRIORITAS, true, FormHeader1.ErrorPanel);
            KodtarakDropDownList_LezarasPrioritas.FillDropDownList(kcs_FELADAT_PRIORITAS, true, FormHeader1.ErrorPanel);
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_FeladatDefinicio EREC_FeladatDefinicio = (EREC_FeladatDefinicio)result.Record;
                    LoadComponentsFromBusinessObject(EREC_FeladatDefinicio);

                    //LZS � 7905 � Ha nincs hiba, akkor t�vol�tsuk el az error panelt.
                    FormHeader1.ErrorPanel.Enabled =
                        FormHeader1.ErrorPanel.Visible = false;
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.New)
        {
            SetNewControls();
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.FeladatDefinicioFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_FeladatDefinicio erec_FeladatDefinicio)
    {
        FunkcioTextBox_FunkcioKivalto.Id_HiddenField = erec_FeladatDefinicio.Funkcio_Id_Kivalto;
        FunkcioTextBox_FunkcioKivalto.SetFunkcioTextBoxById(FormHeader1.ErrorPanel);

        KodTarakDropDownList_DefinicioTipus.SetSelectedValue(erec_FeladatDefinicio.FeladatDefinicioTipus);

        KodTarakDropDownList_Idobazis.SetSelectedValue(erec_FeladatDefinicio.Idobazis);

        ObjTipusokTextBox_ObjTipusokDateCol.Id_HiddenField = erec_FeladatDefinicio.ObjTip_Id_DateCol;
        ObjTipusokTextBox_ObjTipusokDateCol.SetObjTipusokTextBoxById(FormHeader1.ErrorPanel);

        RequiredNumberBox_AtfutasiIdo.Text = "";
        int nAtfutasiIdo;
        if (Int32.TryParse(erec_FeladatDefinicio.AtfutasiIdo, out nAtfutasiIdo))
        {
            if (nAtfutasiIdo < 0)
            {
                RequiredNumberBox_AtfutasiIdo.Text = (-nAtfutasiIdo).ToString();
                rblJelzesElojel.SelectedValue = "-";
            }
            else
            {
                RequiredNumberBox_AtfutasiIdo.Text = erec_FeladatDefinicio.AtfutasiIdo;
                rblJelzesElojel.SelectedValue = "+";
            }
        }
        // BLG_2156
        //KodtarakDropDownList_Idoegyseg.SetSelectedValue(erec_FeladatDefinicio.Idoegyseg);
        FuggoKodtarakDropDownList_Idoegyseg.SelectedValue = erec_FeladatDefinicio.Idoegyseg;

        RequiredTextBox_FeladatLeiras.Text = erec_FeladatDefinicio.FeladatLeiras;

        KodtarakDropDownList_Prioritas.SetSelectedValue(erec_FeladatDefinicio.Prioritas);

        KodtarakDropDownList_LezarasPrioritas.SetSelectedValue(erec_FeladatDefinicio.LezarasPrioritas);

        ErvenyessegCalendarControl1.ErvKezd = erec_FeladatDefinicio.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_FeladatDefinicio.ErvVege;

        FormHeader1.Record_Ver = erec_FeladatDefinicio.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_FeladatDefinicio.Base);
    }

    // form --> business object
    private EREC_FeladatDefinicio GetBusinessObjectFromComponents()
    {
        EREC_FeladatDefinicio erec_FeladatDefinicio = new EREC_FeladatDefinicio();
        // �sszes mez� update-elhet�s�g�t kezdetben letiltani:
        erec_FeladatDefinicio.Updated.SetValueAll(false);
        erec_FeladatDefinicio.Base.Updated.SetValueAll(false);

        erec_FeladatDefinicio.Funkcio_Id_Kivalto = FunkcioTextBox_FunkcioKivalto.Id_HiddenField;
        erec_FeladatDefinicio.Updated.Funkcio_Id_Kivalto = pageView.GetUpdatedByView(FunkcioTextBox_FunkcioKivalto);

        erec_FeladatDefinicio.FeladatDefinicioTipus = KodTarakDropDownList_DefinicioTipus.SelectedValue;
        erec_FeladatDefinicio.Updated.FeladatDefinicioTipus = pageView.GetUpdatedByView(KodTarakDropDownList_DefinicioTipus);

        erec_FeladatDefinicio.Idobazis = KodTarakDropDownList_Idobazis.SelectedValue;
        erec_FeladatDefinicio.Updated.Idobazis = pageView.GetUpdatedByView(KodTarakDropDownList_Idobazis);

        erec_FeladatDefinicio.ObjTip_Id_DateCol = ObjTipusokTextBox_ObjTipusokDateCol.Id_HiddenField;
        erec_FeladatDefinicio.Updated.ObjTip_Id_DateCol = pageView.GetUpdatedByView(ObjTipusokTextBox_ObjTipusokDateCol);

        // az id� megad�s elemeinek csak egy�ttesen van �rtelme
        if (!String.IsNullOrEmpty(RequiredNumberBox_AtfutasiIdo.Text))
        {
            erec_FeladatDefinicio.AtfutasiIdo = (!String.IsNullOrEmpty(RequiredNumberBox_AtfutasiIdo.Text) && rblJelzesElojel.SelectedValue == "-" ? "-" : "") + RequiredNumberBox_AtfutasiIdo.Text;
            erec_FeladatDefinicio.Updated.AtfutasiIdo = pageView.GetUpdatedByView(RequiredNumberBox_AtfutasiIdo);

            // BLG_2156
            //erec_FeladatDefinicio.Idoegyseg = KodtarakDropDownList_Idoegyseg.SelectedValue;
            //erec_FeladatDefinicio.Updated.Idoegyseg = pageView.GetUpdatedByView(KodtarakDropDownList_Idoegyseg);
            erec_FeladatDefinicio.Idoegyseg = FuggoKodtarakDropDownList_Idoegyseg.SelectedValue;
            erec_FeladatDefinicio.Updated.Idoegyseg = pageView.GetUpdatedByView(FuggoKodtarakDropDownList_Idoegyseg);
        }
        else
        {
            erec_FeladatDefinicio.AtfutasiIdo = "";
            erec_FeladatDefinicio.Updated.AtfutasiIdo = pageView.GetUpdatedByView(RequiredNumberBox_AtfutasiIdo);

            erec_FeladatDefinicio.Idoegyseg = "";
            // BLG_2156
            // erec_FeladatDefinicio.Updated.Idoegyseg = pageView.GetUpdatedByView(KodtarakDropDownList_Idoegyseg);
            erec_FeladatDefinicio.Updated.Idoegyseg = pageView.GetUpdatedByView(FuggoKodtarakDropDownList_Idoegyseg);

        }
        erec_FeladatDefinicio.FeladatLeiras = RequiredTextBox_FeladatLeiras.Text;
        erec_FeladatDefinicio.Updated.FeladatLeiras = pageView.GetUpdatedByView(RequiredTextBox_FeladatLeiras);

        erec_FeladatDefinicio.Prioritas = KodtarakDropDownList_Prioritas.SelectedValue;
        erec_FeladatDefinicio.Updated.Prioritas = pageView.GetUpdatedByView(KodtarakDropDownList_Prioritas);

        erec_FeladatDefinicio.LezarasPrioritas = KodtarakDropDownList_LezarasPrioritas.SelectedValue;
        erec_FeladatDefinicio.Updated.LezarasPrioritas = pageView.GetUpdatedByView(KodtarakDropDownList_LezarasPrioritas);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_FeladatDefinicio, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        erec_FeladatDefinicio.Base.Ver = FormHeader1.Record_Ver;
        erec_FeladatDefinicio.Base.Updated.Ver = true;

        return erec_FeladatDefinicio;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            compSelector.Add_ComponentOnClick(FunkcioTextBox_FunkcioKivalto);
            compSelector.Add_ComponentOnClick(KodTarakDropDownList_DefinicioTipus);
            compSelector.Add_ComponentOnClick(KodTarakDropDownList_Idobazis);
            compSelector.Add_ComponentOnClick(ObjTipusokTextBox_ObjTipusokDateCol);
            compSelector.Add_ComponentOnClick(rblJelzesElojel);
            compSelector.Add_ComponentOnClick(RequiredNumberBox_AtfutasiIdo);
            // BLG_2156
            // compSelector.Add_ComponentOnClick(KodtarakDropDownList_Idoegyseg);
            compSelector.Add_ComponentOnClick(FuggoKodtarakDropDownList_Idoegyseg);

            compSelector.Add_ComponentOnClick(RequiredTextBox_FeladatLeiras);
            compSelector.Add_ComponentOnClick(KodtarakDropDownList_Prioritas);
            compSelector.Add_ComponentOnClick(KodtarakDropDownList_LezarasPrioritas);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
                            EREC_FeladatDefinicio erec_FeladatDefinicio = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_FeladatDefinicio);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, FunkcioTextBox_FunkcioKivalto.Text))
                                {
                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                    if (!String.IsNullOrEmpty(refreshCallingWindow)
                                        && refreshCallingWindow == "1"
                                        )
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
                                EREC_FeladatDefinicio erec_FeladatDefinicio = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_FeladatDefinicio);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
