<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FeladatDefinicioList.aspx.cs" Inherits="FeladatDefinicioList" Title="Feladat defin�ci�k" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="FeladatDefinicioCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="FeladatDefinicioUpdatePanel" runat="server" OnLoad="FeladatDefinicioUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="FeladatDefinicioCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="FeladatDefinicioCPEButton"
                            CollapseControlID="FeladatDefinicioCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="FeladatDefinicioCPEButton" ExpandedSize="0" ExpandedText="Feladat defin�ci�k list�ja"
                            CollapsedText="Feladat defin�ci�k list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="FeladatDefinicioGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="FeladatDefinicioGridView_RowCommand" OnPreRender="FeladatDefinicioGridView_PreRender"
                                            OnSorting="FeladatDefinicioGridView_Sorting" OnRowDataBound="FeladatDefinicioGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Funkcio_Nev_Kivalto" HeaderText="Kiv�lt�" SortExpression="Funkcio_Nev_Kivalto">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FeladatDefinicioTipus_Nev" HeaderText="Defin�ci� t�pus" SortExpression="FeladatDefinicioTipus_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Idobazis_Nev" HeaderText="Id�b�zis" SortExpression="Idobazis_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AtfutasiIdo" HeaderText="Id�tartam" SortExpression="AtfutasiIdo" DataFormatString="{0:#;#;0}" HtmlEncode="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Idoegyseg_Nev" HeaderText="Id�egys�g" SortExpression="Idoegyseg_Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AtfutasiIdo" HeaderText="Jelz�s" SortExpression="AtfutasiIdo" DataFormatString="{0:el�tte;ut�na}" HtmlEncode="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ObjTip_Nev_DateCol" HeaderText="D�tum mez�" SortExpression="ObjTip_Nev_DateCol">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                 <asp:BoundField DataField="FeladatLeiras" HeaderText="Feladat le�r�s" SortExpression="FeladatLeiras">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="400px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                 <asp:BoundField DataField="Prioritas_Nev" HeaderText="Priorit�s" SortExpression="Prioritas">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                 <asp:BoundField DataField="LezarasPrioritas_Nev" HeaderText="Lez�r�s priorit�s" SortExpression="LezarasPrioritas">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>                  
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
