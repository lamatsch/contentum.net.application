using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class FeladatDefinicioList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatDefiniciokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.FeladatDefinicioListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_FeladatDefinicioSearch);
        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = true;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FeladatDefinicioSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, FeladatDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("FeladatDefinicioForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, FeladatDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FeladatDefinicioGridView.ClientID);
        ListHeader1.PrintOnClientClick = "javascript:window.open('FeladatDefinicioListPrintFormSSRS.aspx')";

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(FeladatDefinicioGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(FeladatDefinicioGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(FeladatDefinicioGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(FeladatDefinicioGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, FeladatDefinicioUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = FeladatDefinicioGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(FeladatDefinicioGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_FeladatDefinicioSearch());

        if (!IsPostBack) FeladatDefinicioGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatDefinicio" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(FeladatDefinicioGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }


    #endregion

    #region Master List

    protected void FeladatDefinicioGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FeladatDefinicioGridView", ViewState, "EREC_FeladatDefinicio.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FeladatDefinicioGridView", ViewState);

        FeladatDefinicioGridViewBind(sortExpression, sortDirection);
    }

    protected void FeladatDefinicioGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(FeladatDefinicioGridView);

        EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_FeladatDefinicioSearch search = null;
        search = (EREC_FeladatDefinicioSearch)Search.GetSearchObject(Page, new EREC_FeladatDefinicioSearch());

        search.OrderBy = Search.GetOrderBy("FeladatDefinicioGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(FeladatDefinicioGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void FeladatDefinicioGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void FeladatDefinicioGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = FeladatDefinicioGridView.PageIndex;

        FeladatDefinicioGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = FeladatDefinicioGridView.PageCount;

        if (prev_PageIndex != FeladatDefinicioGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, FeladatDefinicioCPE);
            FeladatDefinicioGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, FeladatDefinicioCPE);
        }

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(FeladatDefinicioGridView);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        FeladatDefinicioGridViewBind();
    }

    protected void FeladatDefinicioGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FeladatDefinicioGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FeladatDefinicioForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, FeladatDefinicioUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("FeladatDefinicioForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FeladatDefinicioUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_FeladatDefinicio";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, FeladatDefinicioUpdatePanel.ClientID);

            //ListHeader1.PrintOnClientClick = "javascript:window.open('FeladatDefinicioPrintForm.aspx?" + QueryStringVars.Id + "=" + id + "')";

        }
    }

    protected void FeladatDefinicioUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    FeladatDefinicioGridViewBind();
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedFeladatDefinicio();
            FeladatDefinicioGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedFeladatDefinicioRecords();
                FeladatDefinicioGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedFeladatDefinicioRecords();
                FeladatDefinicioGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedFeladatDefinicio();
                break;
        }
    }

    private void LockSelectedFeladatDefinicioRecords()
    {
        LockManager.LockSelectedGridViewRecords(FeladatDefinicioGridView, "EREC_FeladatDefinicio"
            , "FeladatDefinicioLock", "FeladatDefinicioForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedFeladatDefinicioRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(FeladatDefinicioGridView, "EREC_FeladatDefinicio"
            , "FeladatDefinicioLock", "FeladatDefinicioForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a FeladatDefinicioGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedFeladatDefinicio()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FeladatDefinicioInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FeladatDefinicioGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a FeladatDefinicioGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedFeladatDefinicio()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(FeladatDefinicioGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_FeladatDefinicio");
        }
    }

    protected void FeladatDefinicioGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FeladatDefinicioGridViewBind(e.SortExpression, UI.GetSortToGridView("FeladatDefinicioGridView", ViewState, e.SortExpression));
    }

    #endregion

}

