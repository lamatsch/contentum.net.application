using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Net;

public partial class FeladatDefinicioListPrintForm : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatDefiniciokList");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        // rekord bet�lt�se
        EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_FeladatDefinicioSearch search = (EREC_FeladatDefinicioSearch)Search.GetSearchObject(Page, new EREC_FeladatDefinicioSearch());

        Result result = service.GetAllWithExtension(ExecParam, search);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else
        {
            // convert AtfutasiIdo to sign + absolute value format to make it in MSWord "readable"
            DataColumn column_Jelzes = new DataColumn("Jelzes", System.Type.GetType("System.String"));
            column_Jelzes.AutoIncrement = false;
            column_Jelzes.Caption = "Jelzes";
            column_Jelzes.ReadOnly = false;
            column_Jelzes.Unique = false;
            result.Ds.Tables[0].Columns.Add(column_Jelzes);

            DataColumn column_Idotartam = new DataColumn("Idotartam", System.Type.GetType("System.Int32"));
            column_Idotartam.AutoIncrement = false;
            column_Idotartam.Caption = "Idotartam";
            column_Idotartam.ReadOnly = false;
            column_Idotartam.Unique = false;
            result.Ds.Tables[0].Columns.Add(column_Idotartam);

            // tricky check of nullity
            result.Ds.Tables[0].Columns["Jelzes"].Expression = "IIF(IsNull(AtfutasiIdo, 1)=1 AND IsNull(AtfutasiIdo, -1)=-1 , '', IIF(AtfutasiIdo<0, 'b�zisid� ut�n', 'b�zisid� el�tt'))";
            result.Ds.Tables[0].Columns["Idotartam"].Expression = "IIF(IsNull(AtfutasiIdo, 1)=1 AND IsNull(AtfutasiIdo, -1)=-1, AtfutasiIdo, IIF(AtfutasiIdo<0, -1*AtfutasiIdo, AtfutasiIdo))";

            DataTable table = new DataTable("ParentTable");
            DataColumn column;
            DataRow row;
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "id";
            column.ReadOnly = true;
            column.Unique = true;
            table.Columns.Add(column);
            column = new DataColumn();

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Date";
            column.AutoIncrement = false;
            column.Caption = "Date";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FelhNev";
            column.AutoIncrement = false;
            column.Caption = "FelhNev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Csop_Nev";
            column.AutoIncrement = false;
            column.Caption = "Csop_Nev";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Where";
            column.AutoIncrement = false;
            column.Caption = "Where";
            column.ReadOnly = false;
            column.Unique = false;
            table.Columns.Add(column);

            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = table.Columns["id"];
            table.PrimaryKey = PrimaryKeyColumns;
            result.Ds.Tables.Add(table);

            row = table.NewRow();
            row["id"] = 0;
            row["Date"] = System.DateTime.Now.ToShortDateString();
            row["FelhNev"] = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
            row["Csop_Nev"] = Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(FelhasznaloProfil.FelhasznaloSzerverzetId(Page), Page);
            row["Where"] = search.ReadableWhere.TrimEnd(" |".ToCharArray());
            table.Rows.Add(row);

            //string xml = result.Ds.GetXml();
            //string xsd = result.Ds.GetXmlSchema();
            string templateText = "";
            string __usernev = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserName");
            string __password = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointPassword");
            string __doamin = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SharePointUserDomain");
            string SP_TM_site_url = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SP_TM_site_url");
            WebRequest wr = WebRequest.Create(SP_TM_site_url + "FeladatDefinicioList.xml");
            wr.Credentials = new NetworkCredential(__usernev, __password, __doamin);
            StreamReader template = new StreamReader(wr.GetResponse().GetResponseStream());
            templateText = template.ReadToEnd();
            template.Close();
            //string templateText = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><?mso-application progid=\"Word.Document\"?><w:wordDocument xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" xmlns:ve=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:wsp=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:ns0=\"FeladatDefinicioList\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"><w:ignoreSubtree w:val=\"http://schemas.microsoft.com/office/word/2003/wordml/sp2\"/><o:DocumentProperties><o:Author>Kiss Gergely</o:Author><o:LastAuthor>Boda Eszter</o:LastAuthor><o:Revision>9</o:Revision><o:TotalTime>21</o:TotalTime><o:Created>2009-03-25T10:27:00Z</o:Created><o:LastSaved>2009-03-25T11:15:00Z</o:LastSaved><o:Pages>1</o:Pages><o:Words>30</o:Words><o:Characters>214</o:Characters><o:Lines>1</o:Lines><o:Paragraphs>1</o:Paragraphs><o:CharactersWithSpaces>243</o:CharactersWithSpaces><o:Version>12</o:Version></o:DocumentProperties><w:fonts><w:defaultFonts w:ascii=\"Calibri\" w:fareast=\"Calibri\" w:h-ansi=\"Calibri\" w:cs=\"Times New Roman\"/><w:font w:name=\"Times New Roman\"><w:panose-1 w:val=\"02020603050405020304\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Roman\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Arial\"><w:panose-1 w:val=\"020B0604020202020204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"20002A87\" w:usb-1=\"80000000\" w:usb-2=\"00000008\" w:usb-3=\"00000000\" w:csb-0=\"000001FF\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Cambria Math\"><w:panose-1 w:val=\"02040503050406030204\"/><w:charset w:val=\"01\"/><w:family w:val=\"Roman\"/><w:notTrueType/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"00000000\" w:usb-1=\"00000000\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"00000000\" w:csb-1=\"00000000\"/></w:font><w:font w:name=\"Calibri\"><w:panose-1 w:val=\"020F0502020204030204\"/><w:charset w:val=\"EE\"/><w:family w:val=\"Swiss\"/><w:pitch w:val=\"variable\"/><w:sig w:usb-0=\"A00002EF\" w:usb-1=\"4000207B\" w:usb-2=\"00000000\" w:usb-3=\"00000000\" w:csb-0=\"0000009F\" w:csb-1=\"00000000\"/></w:font></w:fonts><w:lists><w:listDef w:listDefId=\"0\"><w:lsid w:val=\"3ECA7D50\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"698223CE\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"1\"><w:lsid w:val=\"43BD71EC\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"6B02C540\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:listDef w:listDefId=\"2\"><w:lsid w:val=\"5F923F7E\"/><w:plt w:val=\"HybridMultilevel\"/><w:tmpl w:val=\"2BFE1A5E\"/><w:lvl w:ilvl=\"0\" w:tplc=\"040E000F\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%1.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"720\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"1\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%2.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"1440\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"2\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%3.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"2160\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"3\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%4.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"2880\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"4\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%5.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"3600\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"5\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%6.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"4320\" w:hanging=\"180\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"6\" w:tplc=\"040E000F\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:lvlText w:val=\"%7.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5040\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"7\" w:tplc=\"040E0019\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"4\"/><w:lvlText w:val=\"%8.\"/><w:lvlJc w:val=\"left\"/><w:pPr><w:ind w:left=\"5760\" w:hanging=\"360\"/></w:pPr></w:lvl><w:lvl w:ilvl=\"8\" w:tplc=\"040E001B\" w:tentative=\"on\"><w:start w:val=\"1\"/><w:nfc w:val=\"2\"/><w:lvlText w:val=\"%9.\"/><w:lvlJc w:val=\"right\"/><w:pPr><w:ind w:left=\"6480\" w:hanging=\"180\"/></w:pPr></w:lvl></w:listDef><w:list w:ilfo=\"1\"><w:ilst w:val=\"0\"/></w:list><w:list w:ilfo=\"2\"><w:ilst w:val=\"1\"/></w:list><w:list w:ilfo=\"3\"><w:ilst w:val=\"2\"/></w:list></w:lists><w:styles><w:versionOfBuiltInStylenames w:val=\"7\"/><w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"267\"><w:lsdException w:name=\"Normal\"/><w:lsdException w:name=\"heading 1\"/><w:lsdException w:name=\"heading 2\"/><w:lsdException w:name=\"heading 3\"/><w:lsdException w:name=\"heading 4\"/><w:lsdException w:name=\"heading 5\"/><w:lsdException w:name=\"heading 6\"/><w:lsdException w:name=\"heading 7\"/><w:lsdException w:name=\"heading 8\"/><w:lsdException w:name=\"heading 9\"/><w:lsdException w:name=\"caption\"/><w:lsdException w:name=\"Title\"/><w:lsdException w:name=\"Subtitle\"/><w:lsdException w:name=\"Strong\"/><w:lsdException w:name=\"Emphasis\"/><w:lsdException w:name=\"No Spacing\"/><w:lsdException w:name=\"List Paragraph\"/><w:lsdException w:name=\"Quote\"/><w:lsdException w:name=\"Intense Quote\"/><w:lsdException w:name=\"Subtle Emphasis\"/><w:lsdException w:name=\"Intense Emphasis\"/><w:lsdException w:name=\"Subtle Reference\"/><w:lsdException w:name=\"Intense Reference\"/><w:lsdException w:name=\"Book Title\"/><w:lsdException w:name=\"TOC Heading\"/></w:latentStyles><w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Norml\"><w:name w:val=\"Normal\"/><wx:uiName wx:val=\"Norm�l\"/><w:rsid w:val=\"00B0580C\"/><w:pPr><w:spacing w:after=\"200\" w:line=\"276\" w:line-rule=\"auto\"/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/><w:sz w:val=\"22\"/><w:sz-cs w:val=\"22\"/><w:lang w:val=\"HU\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/></w:rPr></w:style><w:style w:type=\"character\" w:default=\"on\" w:styleId=\"Bekezdsalapbettpusa\"><w:name w:val=\"Default Paragraph Font\"/><wx:uiName wx:val=\"Bekezd�s alapbet�t�pusa\"/></w:style><w:style w:type=\"table\" w:default=\"on\" w:styleId=\"Normltblzat\"><w:name w:val=\"Normal Table\"/><wx:uiName wx:val=\"Norm�l t�bl�zat\"/><w:rPr><wx:font wx:val=\"Calibri\"/><w:lang w:val=\"HU\" w:fareast=\"HU\" w:bidi=\"AR-SA\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"list\" w:default=\"on\" w:styleId=\"Nemlista\"><w:name w:val=\"No List\"/><wx:uiName wx:val=\"Nem lista\"/></w:style><w:style w:type=\"table\" w:styleId=\"Rcsostblzat\"><w:name w:val=\"Table Grid\"/><wx:uiName wx:val=\"R�csos t�bl�zat\"/><w:basedOn w:val=\"Normltblzat\"/><w:rsid w:val=\"00D0693E\"/><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr><w:tblPr><w:tblInd w:w=\"0\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblCellMar><w:top w:w=\"0\" w:type=\"dxa\"/><w:left w:w=\"108\" w:type=\"dxa\"/><w:bottom w:w=\"0\" w:type=\"dxa\"/><w:right w:w=\"108\" w:type=\"dxa\"/></w:tblCellMar></w:tblPr></w:style><w:style w:type=\"paragraph\" w:styleId=\"Listaszerbekezds\"><w:name w:val=\"List Paragraph\"/><wx:uiName wx:val=\"Listaszer� bekezd�s\"/><w:basedOn w:val=\"Norml\"/><w:rsid w:val=\"00D0693E\"/><w:pPr><w:ind w:left=\"720\"/><w:contextualSpacing/></w:pPr><w:rPr><wx:font wx:val=\"Calibri\"/></w:rPr></w:style><w:style w:type=\"character\" w:styleId=\"Hiperhivatkozs\"><w:name w:val=\"Hyperlink\"/><wx:uiName wx:val=\"Hiperhivatkoz�s\"/><w:basedOn w:val=\"Bekezdsalapbettpusa\"/><w:rsid w:val=\"00D226C0\"/><w:rPr><w:rFonts w:ascii=\"Arial\" w:h-ansi=\"Arial\" w:cs=\"Arial\" w:hint=\"default\"/><w:strike w:val=\"off\"/><w:dstrike w:val=\"off\"/><w:color w:val=\"335674\"/><w:sz w:val=\"18\"/><w:sz-cs w:val=\"18\"/><w:u w:val=\"none\"/><w:effect w:val=\"none\"/><w:bdr w:val=\"none\" w:sz=\"0\" wx:bdrwidth=\"0\" w:space=\"0\" w:color=\"auto\" w:frame=\"on\"/></w:rPr></w:style></w:styles><w:shapeDefaults><o:shapedefaults v:ext=\"edit\" spidmax=\"13314\"/><o:shapelayout v:ext=\"edit\"><o:idmap v:ext=\"edit\" data=\"1\"/></o:shapelayout></w:shapeDefaults><w:docPr><w:view w:val=\"print\"/><w:zoom w:percent=\"100\"/><w:doNotEmbedSystemFonts/><w:proofState w:spelling=\"clean\" w:grammar=\"clean\"/><w:formsDesign/><w:defaultTabStop w:val=\"708\"/><w:hyphenationZone w:val=\"425\"/><w:drawingGridHorizontalSpacing w:val=\"110\"/><w:displayHorizontalDrawingGridEvery w:val=\"2\"/><w:punctuationKerning/><w:characterSpacingControl w:val=\"DontCompress\"/><w:optimizeForBrowser/><w:validateAgainstSchema/><w:saveInvalidXML/><w:ignoreMixedContent w:val=\"off\"/><w:alwaysShowPlaceholderText w:val=\"off\"/><w:compat><w:breakWrappedTables/><w:snapToGridInCell/><w:wrapTextWithPunct/><w:useAsianBreakRules/><w:dontGrowAutofit/></w:compat><wsp:rsids><wsp:rsidRoot wsp:val=\"00D0693E\"/><wsp:rsid wsp:val=\"000874E3\"/><wsp:rsid wsp:val=\"00103281\"/><wsp:rsid wsp:val=\"00144EFF\"/><wsp:rsid wsp:val=\"001748E3\"/><wsp:rsid wsp:val=\"00184821\"/><wsp:rsid wsp:val=\"00363A01\"/><wsp:rsid wsp:val=\"0040653A\"/><wsp:rsid wsp:val=\"00474299\"/><wsp:rsid wsp:val=\"004B2016\"/><wsp:rsid wsp:val=\"005A7171\"/><wsp:rsid wsp:val=\"0065707D\"/><wsp:rsid wsp:val=\"006D14FE\"/><wsp:rsid wsp:val=\"007620D9\"/><wsp:rsid wsp:val=\"00787483\"/><wsp:rsid wsp:val=\"007B6579\"/><wsp:rsid wsp:val=\"008140A2\"/><wsp:rsid wsp:val=\"008529C2\"/><wsp:rsid wsp:val=\"0091311B\"/><wsp:rsid wsp:val=\"0096345B\"/><wsp:rsid wsp:val=\"009C7C2C\"/><wsp:rsid wsp:val=\"009D4A43\"/><wsp:rsid wsp:val=\"00A27C9C\"/><wsp:rsid wsp:val=\"00B0580C\"/><wsp:rsid wsp:val=\"00B306A8\"/><wsp:rsid wsp:val=\"00C579E1\"/><wsp:rsid wsp:val=\"00D0693E\"/><wsp:rsid wsp:val=\"00D226C0\"/><wsp:rsid wsp:val=\"00E54A6F\"/><wsp:rsid wsp:val=\"00E853CD\"/><wsp:rsid wsp:val=\"00F2122C\"/><wsp:rsid wsp:val=\"00F35C21\"/><wsp:rsid wsp:val=\"00FF5879\"/></wsp:rsids></w:docPr><w:body><ns0:NewDataSet><w:p wsp:rsidR=\"00B0580C\" wsp:rsidRDefault=\"00D226C0\" wsp:rsidP=\"00D0693E\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>Feladat defin�ci�</w:t></w:r><w:r wsp:rsidR=\"00B306A8\"><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t>k</w:t></w:r><w:r wsp:rsidR=\"00D0693E\" wsp:rsidRPr=\"00D0693E\"><w:rPr><w:b/><w:sz w:val=\"28\"/><w:sz-cs w:val=\"28\"/></w:rPr><w:t> list�ja</w:t></w:r></w:p><w:p wsp:rsidR=\"00D0693E\" wsp:rsidRDefault=\"00787483\" wsp:rsidP=\"00D0693E\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr><w:r><w:t>(</w:t></w:r><ParentTable><Where/></ParentTable><w:r><w:t>)</w:t></w:r></w:p><w:p wsp:rsidR=\"00D226C0\" wsp:rsidRDefault=\"00D226C0\" wsp:rsidP=\"00D0693E\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/><w:jc w:val=\"center\"/></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w=\"14529\" w:type=\"dxa\"/><w:tblBorders><w:top w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:left w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:bottom w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:right w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideH w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/><w:insideV w:val=\"single\" w:sz=\"4\" wx:bdrwidth=\"10\" w:space=\"0\" w:color=\"000000\"/></w:tblBorders><w:tblLayout w:type=\"Fixed\"/><w:tblLook w:val=\"04A0\"/></w:tblPr><w:tblGrid><w:gridCol w:w=\"626\"/><w:gridCol w:w=\"2034\"/><w:gridCol w:w=\"992\"/><w:gridCol w:w=\"1134\"/><w:gridCol w:w=\"1134\"/><w:gridCol w:w=\"1160\"/><w:gridCol w:w=\"959\"/><w:gridCol w:w=\"1129\"/><w:gridCol w:w=\"2952\"/><w:gridCol w:w=\"1143\"/><w:gridCol w:w=\"1266\"/></w:tblGrid><w:tr wsp:rsidR=\"0065707D\" wsp:rsidTr=\"0065707D\"><w:tc><w:tcPr><w:tcW w:w=\"626\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2034\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Kiv�lt�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"992\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"0065707D\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Defin�ci� t�pus</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1134\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"0065707D\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Id�b�zis</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1134\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"003E74A8\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Id�tartam</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1160\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"003E74A8\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Id�egys�g</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"959\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Jelz�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1129\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>D�tum mez�</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"2952\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Feladat le�r�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1143\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Priorit�s</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w=\"1266\" w:type=\"dxa\"/><w:vAlign w:val=\"center\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRPr=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr></w:pPr><w:r wsp:rsidRPr=\"0065707D\"><w:rPr><w:rStyle w:val=\"Hiperhivatkozs\"/><wx:font wx:val=\"Arial\"/><w:b/><w:b-cs/><w:color w:val=\"auto\"/></w:rPr><w:t>Lez�r�s priorit�s</w:t></w:r></w:p></w:tc></w:tr><Table ns0:repeater=\"true\"><w:tr wsp:rsidR=\"0065707D\" wsp:rsidTr=\"0065707D\"><w:tc><w:tcPr><w:tcW w:w=\"626\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr><w:r><w:fldChar w:fldCharType=\"begin\"/></w:r><w:r><w:instrText> AUTONUM  \\* Arabic </w:instrText></w:r><w:r><w:fldChar w:fldCharType=\"end\"/><wx:t wx:val=\"1.\"/></w:r></w:p></w:tc><Funkcio_Nev_Kivalto><w:tc><w:tcPr><w:tcW w:w=\"2034\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></Funkcio_Nev_Kivalto><FeladatDefinicioTipus_Nev><w:tc><w:tcPr><w:tcW w:w=\"992\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></FeladatDefinicioTipus_Nev><Idobazis_Nev><w:tc><w:tcPr><w:tcW w:w=\"1134\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></Idobazis_Nev><Idotartam><w:tc><w:tcPr><w:tcW w:w=\"1134\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"003E74A8\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></Idotartam><Idoegyseg_Nev><w:tc><w:tcPr><w:tcW w:w=\"1160\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"003E74A8\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></Idoegyseg_Nev><Jelzes><w:tc><w:tcPr><w:tcW w:w=\"959\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"0065707D\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></Jelzes><ObjTip_Nev_DateCol><w:tc><w:tcPr><w:tcW w:w=\"1129\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></ObjTip_Nev_DateCol><FeladatLeiras><w:tc><w:tcPr><w:tcW w:w=\"2952\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></FeladatLeiras><Prioritas_Nev><w:tc><w:tcPr><w:tcW w:w=\"1143\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></Prioritas_Nev><LezarasPrioritas_Nev><w:tc><w:tcPr><w:tcW w:w=\"1266\" w:type=\"dxa\"/></w:tcPr><w:p wsp:rsidR=\"0065707D\" wsp:rsidRDefault=\"0065707D\" wsp:rsidP=\"007620D9\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p></w:tc></LezarasPrioritas_Nev></w:tr></Table></w:tbl><w:p wsp:rsidR=\"00D0693E\" wsp:rsidRDefault=\"00D0693E\" wsp:rsidP=\"00D0693E\"><w:pPr><w:jc w:val=\"center\"/></w:pPr></w:p><ParentTable><w:p wsp:rsidR=\"00D226C0\" wsp:rsidRDefault=\"00D226C0\" wsp:rsidP=\"00F2122C\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r wsp:rsidRPr=\"00C579E1\"><w:rPr><w:b/></w:rPr><w:t>K�sz�tette:</w:t></w:r><w:r><w:t> </w:t></w:r><FelhNev/><w:r><w:t> / </w:t></w:r><Csop_Nev/></w:p><w:p wsp:rsidR=\"00D226C0\" wsp:rsidRPr=\"001748E3\" wsp:rsidRDefault=\"00D226C0\" wsp:rsidP=\"00F2122C\"><w:pPr><w:spacing w:after=\"0\" w:line=\"240\" w:line-rule=\"auto\"/></w:pPr><w:r wsp:rsidRPr=\"00C579E1\"><w:rPr><w:b/></w:rPr><w:t>K�sz�lt: </w:t></w:r><Date/></w:p></ParentTable></ns0:NewDataSet><w:sectPr wsp:rsidR=\"00D226C0\" wsp:rsidRPr=\"001748E3\" wsp:rsidSect=\"00D0693E\"><w:pgSz w:w=\"16838\" w:h=\"11906\" w:orient=\"landscape\"/><w:pgMar w:top=\"1417\" w:right=\"1417\" w:bottom=\"1417\" w:left=\"1417\" w:header=\"708\" w:footer=\"708\" w:gutter=\"0\"/><w:cols w:space=\"708\"/><w:docGrid w:line-pitch=\"360\"/></w:sectPr></w:body></w:wordDocument>";

            bool pdf = false;
            if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
            {
                pdf = true;
            }
            result.Uid = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam);

            string filename = "";

            if (pdf)
            {
                filename = "FeladatDefinicio_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".pdf";
            }
            else
            {
                filename = "FeladatDefinicio_" +
                            System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ') +
                            "_" + System.DateTime.Now.ToLongTimeString().ToString().Replace(':', ' ') + ".doc";
            }

            int priority = 1;
            bool prior = false;

            Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = result.Uid;
            krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

            Result csop_result = csop_service.GetAll(ExecParam, krt_CsoportTagokSearch);

            if (string.IsNullOrEmpty(csop_result.ErrorCode))
            {
                foreach (DataRow _row in csop_result.Ds.Tables[0].Rows)
                {
                    if (_row["Tipus"].ToString().Equals("3"))
                    {
                        prior = true;
                    }
                }
            }

            if (prior)
            {
                priority++;
            }

            try
            {
                Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
                result = tms.GetWordDocument_DataSet_Thread(templateText, result, pdf, priority, filename, 25);

                byte[] res = (byte[])result.Record;

                if (string.IsNullOrEmpty(result.ErrorCode))
                {
                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                    if (pdf)
                    {
                        Response.ContentType = "application/pdf";
                    }
                    else
                    {
                        Response.ContentType = "application/msword";
                    }
                    Response.OutputStream.Write(res, 0, res.Length);
                    Response.OutputStream.Flush();
                    Response.End();

                    JavaScripts.RegisterCloseWindowClientScript(Page);
                }
                else
                {
                    if (result.ErrorCode == "99999" || result.ErrorCode == "99998")
                    {
                        string js = "alert('A dokumentum elk�sz�l�s�r�l e-mail �rtes�t�st fog kapni!');";
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "dokready", js, true);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel.Update();
                    }
                }

            }
            catch (Exception ex)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba a dokumentum el��ll�t�sa sor�n!", ex.Message);
                ErrorUpdatePanel.Update();
            }

        }

    }
}
