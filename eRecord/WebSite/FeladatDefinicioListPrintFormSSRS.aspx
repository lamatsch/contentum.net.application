<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FeladatDefinicioListPrintFormSSRS.aspx.cs" Inherits="FeladatDefinicioListPrintForm" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>		

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
<%--Hiba megjelenites--%>
    
        <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
        </asp:UpdatePanel>

<%--/Hiba megjelenites--%>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
	
	
<asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel2" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
        <table cellpadding="0" cellspacing="0" width="95%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" 
                                    Width="1200px" Height="1000px" EnableTelemetry="false">   
                                   <ServerReport  ReportPath = "/Sablonok/FeladatDefiniciok" />
                                </rsweb:ReportViewer>
                            </td>
                        </tr>
    </table>	
	
</asp:Content>
