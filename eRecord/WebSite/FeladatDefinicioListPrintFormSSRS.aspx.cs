using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eQuery;
using System.Net;
using Microsoft.Reporting.WebForms;

public partial class FeladatDefinicioListPrintForm : System.Web.UI.Page
{

    private string executor = String.Empty;
    private string szervezet = String.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatDefiniciokList");
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                 ReportParameter[] ReportParameters = GetReportParameters(rpis);
                 ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                executor = ExecParam.LoginUser_Id;
                szervezet = ExecParam.Org_Id;


                EREC_FeladatDefinicioSearch search = (EREC_FeladatDefinicioSearch)Search.GetSearchObject(Page, new EREC_FeladatDefinicioSearch());
                ExecParam.Fake = true;
                Result result = service.GetAllWithExtension(ExecParam, search);

                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel1.Update();
                }




                ReportParameters = new ReportParameter[rpis.Count];



                for (int i = 0; i < rpis.Count; i++)
                {


                    ReportParameters[i] = new ReportParameter(rpis[i].Name);



                    switch (rpis[i].Name)
                    {
                        //case "OrderBy":
                        //    if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                        //    {
                        //        ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));


                        //    }
                        //    break;
                        //case "Where":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                        //    break;

                        //case "Where_Dosszie":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                        //    break;

                        //case "Where_Szamlak":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Szamlak"));
                        //    break;




                        //case "Where_KuldKuldemenyek_Csatolt":
                        //    if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt")))
                        //    {
                        //        ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt"));
                        //    }


                        //    break;

                        //case "Where_EREC_IraOnkormAdatok":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                        //    break;

                        //case "TopRow":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                        //    break;

                        //case "Jogosultak":
                        //      ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                        //    break;




                        //case "pageNumber":
                        //    if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                        //    {
                        //        ReportParameters[i].Values.Add("0");
                        //    }
                        //    else
                        //    {
                        //        ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                        //    }
                        //    break;
                        //case "pageSize":
                        //    if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                        //    {
                        //        ReportParameters[i].Values.Add("10000");
                        //    }
                        //    else
                        //    {
                        //        ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                        //    }
                        //    break;
                        //case "SelectedRowId":
                        //    if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                        //    {
                        //        ReportParameters[i].Values.Add(" ");
                        //    }
                        //    else
                        //    {
                        //        ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                        //    }
                        //    break;


                        //case "AlSzervezetId":
                        //    ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                        //    break;

                        case "ExecutorUserId":
                            if (!string.IsNullOrEmpty(executor))
                            {
                                ReportParameters[i].Values.Add(executor);
                            }
                            break;


                        case "FelhasznaloSzervezet_Id":
                            if (!string.IsNullOrEmpty(szervezet))
                            {
                                ReportParameters[i].Values.Add(szervezet);
                            }
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
