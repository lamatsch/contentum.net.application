<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FeladatDefinicioLovList.aspx.cs" Inherits="FeladatDefinicioLovList" %>

<%@ Register Src="Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList"
    TagPrefix="kddl" %>
<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="ftb" %>
<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,FeladatDefinicioLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr>
                                    <td>
                                        <table>
                                            <tr id="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                                <td colspan="4">
                                                    <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelFeladatLeiras" runat="server" Text="Feladat le�r�s:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox ID="TextBox_FeladatLeiras" runat="server" CssClass="mrUrlapInput" />
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelIdobazis" runat="server" Text="Id�b�zis:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <kddl:KodTarakDropDownList ID="KodTarakDropDownList_Idobazis" runat="server"
                                                        Validate="false" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelFunkcioKivalto" runat="server" Text="Kiv�lt� funkci�:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <ftb:FunkcioTextBox ID="FunkcioTextBox_FunkcioKivalto" runat="server" SearchMode="true"
                                                        Validate="false" AjaxEnabled="true" FeladatDefinicioMode="true" />
                                                </td>
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelDefinicioTipus" runat="server" Text="Defin�ci� t�pus:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <kddl:KodTarakDropDownList ID="KodTarakDropDownList_DefinicioTipus" runat="server"
                                                        Validate="false" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption">
                                                    <asp:Label ID="labelPrioritas" runat="server" Text="Priorit�s:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_Prioritas" CssClass="mrUrlapInput"
                                                        runat="server" />
                                                </td>
                                                <td class="mrUrlapCaption"
                                                    <asp:Label ID="labelLezarasPrioritas" runat="server" Text="Lez�r�s priorit�s:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_LezarasPrioritas" CssClass="mrUrlapInput"
                                                        runat="server" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td colspan="4">
                                                    <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                        AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                                                    <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                        AlternateText="R�szletes keres�s" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">
                                        <div class="listaFulFelsoCsikKicsi">
                                            <img src="images/hu/design/spacertrans.gif" alt="" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                        <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                        <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                        <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                            DataKeyNames="Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" SortExpression="Id">
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <HeaderStyle CssClass = "GridViewLovListInvisibleCoulumnStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Funkcio_Nev_Kivalto" HeaderText="Kiv�lt�" SortExpression="Funkcio_Nev_Kivalto">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="250px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FeladatDefinicioTipus_Nev" HeaderText="Defin�ci� t�pus"
                                                                    SortExpression="FeladatDefinicioTipus_Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Idobazis_Nev" HeaderText="Id�b�zis" SortExpression="Idobazis_Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FeladatLeiras" HeaderText="Feladat le�r�s" SortExpression="FeladatLeiras">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="400px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Prioritas_Nev" HeaderText="Priorit�s" SortExpression="Prioritas">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LezarasPrioritas_Nev" HeaderText="Lez�r�s priorit�s" SortExpression="LezarasPrioritas">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <%--
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        &nbsp;
                                        <br />
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                            onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"
                                            AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
