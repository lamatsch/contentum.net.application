using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class FeladatDefinicioLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    private const string kcs_FELADAT_DEFINICIO_TIPUS = "FELADAT_DEFINICIO_TIPUS";
    private const string kcs_FELADAT_IDOBAZIS = "FELADAT_IDOBAZIS";
    private const string kcs_FELADAT_PRIORITAS = "FELADAT_PRIORITAS";

    protected void Page_PreInit(object sender, EventArgs e)
    {        
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatDefiniciokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.Enabled = true;
        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("FeladatDefinicioForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.Enabled = true;
        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("FeladatDefinicioSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);


        if (!IsPostBack)
        {           
            FillGridViewSearchResult(false);

            KodTarakDropDownList_DefinicioTipus.FillDropDownList(kcs_FELADAT_DEFINICIO_TIPUS, true, LovListHeader1.ErrorPanel);
            KodTarakDropDownList_Idobazis.FillDropDownList(kcs_FELADAT_IDOBAZIS, true, LovListHeader1.ErrorPanel);
            KodtarakDropDownList_Prioritas.FillDropDownList(kcs_FELADAT_PRIORITAS, true, LovListHeader1.ErrorPanel);
            KodtarakDropDownList_LezarasPrioritas.FillDropDownList(kcs_FELADAT_PRIORITAS, true, LovListHeader1.ErrorPanel);

        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);

        if (IsPostBack)
        {
            String selectedRowId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
            RefreshOnClientClicks(selectedRowId);
        }
    }

    private void RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {

            ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick(
                "FeladatDefinicioForm.aspx", QueryStringVars.Command + "=" + CommandName.View
                + "&" + QueryStringVars.Id + "=" + id
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, null);
        }
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
        disable_refreshLovList = true;
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
        EREC_FeladatDefinicioSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_FeladatDefinicioSearch)Search.GetSearchObject(Page, new EREC_FeladatDefinicioSearch());
        }
        else
        {
            search = new EREC_FeladatDefinicioSearch();
            
        if (!String.IsNullOrEmpty(FunkcioTextBox_FunkcioKivalto.Id_HiddenField))
        {
            search.Funkcio_Id_Kivalto.Value = FunkcioTextBox_FunkcioKivalto.Id_HiddenField;
            search.Funkcio_Id_Kivalto.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(KodTarakDropDownList_DefinicioTipus.SelectedValue))
        {
            search.FeladatDefinicioTipus.Value = KodTarakDropDownList_DefinicioTipus.SelectedValue;
            search.FeladatDefinicioTipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(TextBox_FeladatLeiras.Text))
        {
            search.FeladatLeiras.Value = TextBox_FeladatLeiras.Text;
            search.FeladatLeiras.Operator = Search.GetOperatorByLikeCharater(TextBox_FeladatLeiras.Text);
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_Prioritas.SelectedValue))
        {
            search.Prioritas.Value = KodtarakDropDownList_Prioritas.SelectedValue;
            search.Prioritas.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_LezarasPrioritas.SelectedValue))
        {
            search.LezarasPrioritas.Value = KodtarakDropDownList_LezarasPrioritas.SelectedValue;
            search.LezarasPrioritas.Operator = Query.Operators.equals;
        }
        }

        // Rendez�s:
        search.OrderBy = "KRT_Funkciok_Kivalto.Nev";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(execParam, search);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1)
                        + " (" + UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2) + ")";

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }



    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

}
