<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FeladatDefinicioSearch.aspx.cs" Inherits="FeladatDefinicioSearch" Title="<%$Resources:Search,FeladatDefinicioSearchHeaderTitle%>" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList"
    TagPrefix="kddl" %>
<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox"
    TagPrefix="ftb" %>
<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc9" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
    text-align: left;
    width: 100px;
    min-width: 100px;
}
    </style>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,FeladatDefinicioSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <br />
      <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFunkcioKivalto" runat="server" Text="Kiv�lt� funkci�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <ftb:FunkcioTextBox ID="FunkcioTextBox_FunkcioKivalto" runat="server" SearchMode="true"
                                        Validate="false" AjaxEnabled="true" FeladatDefinicioMode="true" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelDefinicioTipus" runat="server" Text="Defin�ci� t�pus:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodTarakDropDownList_DefinicioTipus" runat="server"
                                        Validate="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelIdobazis" runat="server" Text="Id�b�zis:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <table>
                                        <tr>
                                            <td>
                                                <kddl:KodTarakDropDownList ID="KodTarakDropDownList_Idobazis" runat="server" CssClass="mrUrlapInputKozepes" />
                                            </td>
                                            <td>
                                                <uc9:ObjTipusokTextBox ID="ObjTipusokTextBox_ObjTipusokDateCol" CssClass="mrUrlapInput" SearchMode="true"
                                                    Validate="false" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelJelzesElojel" runat="server" Text="Jelz�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButtonList ID="rblJelzesElojel" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="b�zisid� el�tt" Value="+"/>
                                    <asp:ListItem Text="b�zisid� ut�n" Value="-" />
                                    <asp:ListItem Text="�sszes" Value="*" Selected="True"  />
                                   </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelAtfutasiIdo" runat="server" Text="Id�tartam:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc4:RequiredNumberBox ID="RequiredNumberBox_AtfutasiIdo" Validate="false" CssClass="mrUrlapInputNumber" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelIdoegyseg" runat="server" Text="Id�egys�g:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_Idoegyseg" CssClass="mrUrlapInputRovid"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFeladatLeiras" runat="server" Text="Feladat le�r�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="RequiredTextBox_FeladatLeiras" Validate="false"  runat="server" TextBoxMode="MultiLine"
                                        Rows="3" CssClass="mrUrlapInputSzeles" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelPrioritas" runat="server" Text="Priorit�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_Prioritas" CssClass="mrUrlapInput"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelLezarasPrioritas" runat="server" Text="Lez�r�s priorit�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <kddl:KodTarakDropDownList ID="KodtarakDropDownList_LezarasPrioritas" CssClass="mrUrlapInput"
                                        runat="server" />
                                </td>
                            </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server">
                                        </uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                    </td>
                                    <td class="mrUrlapMezo">
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                     <td colspan="2">
                                     <uc4:TalalatokSzama_SearchFormComponent id="TalalatokSzama_SearchFormComponent1"
                                        runat="server">
                                      </uc4:TalalatokSzama_SearchFormComponent>
                                      </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

