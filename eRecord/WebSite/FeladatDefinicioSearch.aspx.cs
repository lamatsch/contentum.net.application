using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eAdmin.Utility;
using Contentum.eQuery;

public partial class FeladatDefinicioSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_FeladatDefinicioSearch);

    private const string kcs_FELADAT_DEFINICIO_TIPUS = "FELADAT_DEFINICIO_TIPUS";
    private const string kcs_FELADAT_IDOBAZIS = "FELADAT_IDOBAZIS";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";
    private const string kcs_FELADAT_PRIORITAS = "FELADAT_PRIORITAS";

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            EREC_FeladatDefinicioSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_FeladatDefinicioSearch)Search.GetSearchObject(Page, new EREC_FeladatDefinicioSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_FeladatDefinicioSearch _EREC_FeladatDefinicioSearch = (EREC_FeladatDefinicioSearch)searchObject;

        if (_EREC_FeladatDefinicioSearch != null)
        {
            FunkcioTextBox_FunkcioKivalto.Id_HiddenField = _EREC_FeladatDefinicioSearch.Funkcio_Id_Kivalto.Value;
            FunkcioTextBox_FunkcioKivalto.SetFunkcioTextBoxById(SearchHeader1.ErrorPanel);

            KodTarakDropDownList_DefinicioTipus.FillAndSetSelectedValue(kcs_FELADAT_DEFINICIO_TIPUS, _EREC_FeladatDefinicioSearch.FeladatDefinicioTipus.Value, true, SearchHeader1.ErrorPanel);

            KodTarakDropDownList_Idobazis.FillAndSetSelectedValue(kcs_FELADAT_IDOBAZIS, _EREC_FeladatDefinicioSearch.Idobazis.Value, true, SearchHeader1.ErrorPanel);

            ObjTipusokTextBox_ObjTipusokDateCol.Id_HiddenField = _EREC_FeladatDefinicioSearch.ObjTip_Id_DateCol.Value;
            ObjTipusokTextBox_ObjTipusokDateCol.SetObjTipusokTextBoxById(SearchHeader1.ErrorPanel);

            RequiredNumberBox_AtfutasiIdo.Text = "";
            rblJelzesElojel.SelectedValue = "*";
            int nAtfutasiIdo;
            if (!String.IsNullOrEmpty(_EREC_FeladatDefinicioSearch.AtfutasiIdo.Value) && _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length > 1)
            {
                RequiredNumberBox_AtfutasiIdo.Text = _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[1];
            }
            else if (Int32.TryParse(_EREC_FeladatDefinicioSearch.AtfutasiIdo.Value, out nAtfutasiIdo))
            {
                if (nAtfutasiIdo < 0)
                {
                    RequiredNumberBox_AtfutasiIdo.Text = (-nAtfutasiIdo).ToString();
                    rblJelzesElojel.SelectedValue = "-";
                }
                else if (nAtfutasiIdo > 0)
                {
                    RequiredNumberBox_AtfutasiIdo.Text = _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value;
                    rblJelzesElojel.SelectedValue = "+";
                }
                else if (_EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator == Query.Operators.equals)
                {
                    // ha t�nyleges vizsg�lat volt r�
                    RequiredNumberBox_AtfutasiIdo.Text = "0";
                    rblJelzesElojel.SelectedValue = "*";
                }
                else
                {
                    if (_EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator == Query.Operators.less)
                    {
                        RequiredNumberBox_AtfutasiIdo.Text = "";
                        rblJelzesElojel.SelectedValue = "-";
                    }
                    else if (_EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator == Query.Operators.greaterorequal)
                    {
                        RequiredNumberBox_AtfutasiIdo.Text = "";
                        rblJelzesElojel.SelectedValue = "+";
                    }
                }
            }

            KodtarakDropDownList_Idoegyseg.FillAndSetSelectedValue(kcs_IDOEGYSEG, _EREC_FeladatDefinicioSearch.Idoegyseg.Value, true, SearchHeader1.ErrorPanel);

            RequiredTextBox_FeladatLeiras.Text = _EREC_FeladatDefinicioSearch.FeladatLeiras.Value;

            KodtarakDropDownList_Prioritas.FillAndSetSelectedValue(kcs_FELADAT_PRIORITAS, _EREC_FeladatDefinicioSearch.Prioritas.Value, true, SearchHeader1.ErrorPanel);

            KodtarakDropDownList_LezarasPrioritas.FillAndSetSelectedValue(kcs_FELADAT_PRIORITAS, _EREC_FeladatDefinicioSearch.LezarasPrioritas.Value, true, SearchHeader1.ErrorPanel);

            Ervenyesseg_SearchFormComponent1.SetDefault(
                _EREC_FeladatDefinicioSearch.ErvKezd, _EREC_FeladatDefinicioSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private EREC_FeladatDefinicioSearch SetSearchObjectFromComponents()
    {
        EREC_FeladatDefinicioSearch _EREC_FeladatDefinicioSearch = (EREC_FeladatDefinicioSearch)SearchHeader1.TemplateObject;

        if (_EREC_FeladatDefinicioSearch == null)
        {
            _EREC_FeladatDefinicioSearch = new EREC_FeladatDefinicioSearch();
        }

        if (!String.IsNullOrEmpty(FunkcioTextBox_FunkcioKivalto.Id_HiddenField))
        {
            _EREC_FeladatDefinicioSearch.Funkcio_Id_Kivalto.Value = FunkcioTextBox_FunkcioKivalto.Id_HiddenField;
            _EREC_FeladatDefinicioSearch.Funkcio_Id_Kivalto.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(KodTarakDropDownList_DefinicioTipus.SelectedValue))
        {
            _EREC_FeladatDefinicioSearch.FeladatDefinicioTipus.Value = KodTarakDropDownList_DefinicioTipus.SelectedValue;
            _EREC_FeladatDefinicioSearch.FeladatDefinicioTipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(KodTarakDropDownList_Idobazis.SelectedValue))
        {
            _EREC_FeladatDefinicioSearch.Idobazis.Value = KodTarakDropDownList_Idobazis.SelectedValue;
            _EREC_FeladatDefinicioSearch.Idobazis.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(ObjTipusokTextBox_ObjTipusokDateCol.Id_HiddenField))
        {
            _EREC_FeladatDefinicioSearch.ObjTip_Id_DateCol.Value = ObjTipusokTextBox_ObjTipusokDateCol.Id_HiddenField;
            _EREC_FeladatDefinicioSearch.ObjTip_Id_DateCol.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(RequiredNumberBox_AtfutasiIdo.Text))
        {
            if (RequiredNumberBox_AtfutasiIdo.Text != "0")
            {
                if (rblJelzesElojel.SelectedValue == "-")
                {
                    _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value = "-" + RequiredNumberBox_AtfutasiIdo.Text;
                    _EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator = Query.Operators.equals;
                }
                else if (rblJelzesElojel.SelectedValue == "+")
                {
                    _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value = RequiredNumberBox_AtfutasiIdo.Text;
                    _EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator = Query.Operators.equals;
                }
                else
                {
                    _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value = "-" + RequiredNumberBox_AtfutasiIdo.Text + "," + RequiredNumberBox_AtfutasiIdo.Text;
                    _EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator = Query.Operators.inner;
                }
            }
            else
            {
                _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value = "0";
                _EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator = Query.Operators.equals;

                // ReadableWhere miatt �t�ll�tjuk
                rblJelzesElojel.SelectedValue = "*";
            }
        }
        else if (rblJelzesElojel.SelectedValue == "-")
        {
            _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value = "0";
            _EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator = Query.Operators.less;
        }
        else if (rblJelzesElojel.SelectedValue == "+")
        {
            _EREC_FeladatDefinicioSearch.AtfutasiIdo.Value = "0";
            _EREC_FeladatDefinicioSearch.AtfutasiIdo.Operator = Query.Operators.greaterorequal;
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_Idoegyseg.SelectedValue))
        {
            _EREC_FeladatDefinicioSearch.Idoegyseg.Value = KodtarakDropDownList_Idoegyseg.SelectedValue;
            _EREC_FeladatDefinicioSearch.Idoegyseg.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(RequiredTextBox_FeladatLeiras.Text))
        {
            _EREC_FeladatDefinicioSearch.FeladatLeiras.Value = RequiredTextBox_FeladatLeiras.Text;
            _EREC_FeladatDefinicioSearch.FeladatLeiras.Operator = Search.GetOperatorByLikeCharater(RequiredTextBox_FeladatLeiras.Text);
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_Prioritas.SelectedValue))
        {
            _EREC_FeladatDefinicioSearch.Prioritas.Value = KodtarakDropDownList_Prioritas.SelectedValue;
            _EREC_FeladatDefinicioSearch.Prioritas.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(KodtarakDropDownList_LezarasPrioritas.SelectedValue))
        {
            _EREC_FeladatDefinicioSearch.LezarasPrioritas.Value = KodtarakDropDownList_LezarasPrioritas.SelectedValue;
            _EREC_FeladatDefinicioSearch.LezarasPrioritas.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _EREC_FeladatDefinicioSearch.ErvKezd, _EREC_FeladatDefinicioSearch.ErvVege);

        return _EREC_FeladatDefinicioSearch;
    }
    
    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_FeladatDefinicioSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }        
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private EREC_FeladatDefinicioSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_FeladatDefinicioSearch();
    }
}
