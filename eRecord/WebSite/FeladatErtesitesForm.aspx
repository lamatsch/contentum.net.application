<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FeladatErtesitesForm.aspx.cs" Inherits="FeladatErtesitesForm" Title="Feladat �rtes�t�s karbantart�sa" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="ucFELH" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 868px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelFeladatDef" runat="server" Text="Feladat defin�ci�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:DropDownList ID="DropDownListFeladatDefinicio" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelfelhasznalo" runat="server" Text="Felhaszn�l�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <ucFELH:FelhasznaloTextBox ID="FelhasznaloTextBoxFelErtFelh" runat="server" Validate="false" CustomTextEnabled="false"
                                        WithEmail="true" />
                                </td>
                            </tr>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelErtesites" runat="server" Text="�rtes�t�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButtonList ID="radioButtonListEresites" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="<%$Resources:Question,Enabled%>" Value="1" />
                                        <asp:ListItem Text="<%$Resources:Question,Disabled%>" Value="0" />
                                        <asp:ListItem Text="<%$Resources:Question,NotSet%>" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelErvenyessegStar" runat="server" Text="*" CssClass="ReqStar" />
                                    <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server"></uc6:ErvenyessegCalendarControl>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                &nbsp; &nbsp;&nbsp; &nbsp;
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
