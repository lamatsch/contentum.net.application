using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class FeladatErtesitesForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    #region utility

    /// <summary>
    /// View m�dban a vez�rl�k enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetViewControls()
    {
        EFormPanel1.Visible = true;

        DropDownListFeladatDefinicio.ReadOnly = true;
        DropDownListFeladatDefinicio.Enabled = false;

        FelhasznaloTextBoxFelErtFelh.Enabled = false;
        FelhasznaloTextBoxFelErtFelh.ReadOnly = true;

        radioButtonListEresites.Enabled = false;

        ErvenyessegCalendarControl1.ReadOnly = true;

        labelFeladatDef.CssClass = "mrUrlapInputWaterMarked";
        labelfelhasznalo.CssClass = "mrUrlapInputWaterMarked";
        labelErtesites.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }
    /// <summary>
    /// Modify m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetModifyControls()
    {
        EFormPanel1.Visible = true;

        DropDownListFeladatDefinicio.ReadOnly = false;
        DropDownListFeladatDefinicio.Enabled = true;

        FelhasznaloTextBoxFelErtFelh.Enabled = true;
        FelhasznaloTextBoxFelErtFelh.ReadOnly = false;

        radioButtonListEresites.Enabled = true;

        ErvenyessegCalendarControl1.ReadOnly = false;
    }

    /// <summary>
    /// New m�dban a vez�rl� enged�lyez�se �s megjelen�se
    /// </summary>
    private void SetNewControls()
    {
        EFormPanel1.Visible = true;
        DropDownListFeladatDefinicio.ReadOnly = false;
        DropDownListFeladatDefinicio.Enabled = true;

        FelhasznaloTextBoxFelErtFelh.Enabled = true;
        FelhasznaloTextBoxFelErtFelh.ReadOnly = false;

        radioButtonListEresites.Enabled = true;

        ErvenyessegCalendarControl1.ReadOnly = false;
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            case null:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatErtesitesKezeles");
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatErtesites" + Command);
                break;
        }

        if (!IsPostBack)
        {
            FillControls();
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_FeladatErtesitesService service = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_FeladatErtesites EREC_FeladatErtesites = (EREC_FeladatErtesites)result.Record;
                    LoadComponentsFromBusinessObject(EREC_FeladatErtesites);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        if (Command == CommandName.New)
        {
            SetNewControls();
        }

        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.FeladatErtesitesFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }
    /// <summary>
    /// FillControls
    /// </summary>
    private void FillControls()
    {
        DataRowCollection definiciok = LoadFeladatDefiniciok();
        FillControlFeladatDefiniciok(definiciok);
    }

    /// <summary>
    /// LoadFeladatDefiniciok
    /// </summary>
    /// <returns></returns>
    private DataRowCollection LoadFeladatDefiniciok()
    {
        EREC_FeladatDefinicioService service = eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
        EREC_FeladatDefinicioSearch src = new EREC_FeladatDefinicioSearch();
        src.OrderBy = "Funkcio_Nev_Kivalto";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAllWithExtension(execParam, src);
        if (result.IsError || result.GetCount < 1)
            return null;

        return result.Ds.Tables[0].Rows;
    }

    /// <summary>
    /// FillControlFeladatDefiniciok
    /// </summary>
    /// <param name="items"></param>
    private void FillControlFeladatDefiniciok(DataRowCollection items)
    {
        if (items == null)
            return;
        foreach (DataRow item in items)
        {
            var name = string.Format("[{0}] - [{1}]", item["Funkcio_Nev_Kivalto"], item["FeladatDefinicioTipus_Nev"]);
            DropDownListFeladatDefinicio.Items.Add(new ListItem(name, item["Id"].ToString()));
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_FeladatErtesites erec_FeladatErtesites)
    {
        DropDownListFeladatDefinicio.SelectedValue = erec_FeladatErtesites.FeladatDefinicio_id;

        FelhasznaloTextBoxFelErtFelh.Id_HiddenField = erec_FeladatErtesites.Felhasznalo_Id;
        FelhasznaloTextBoxFelErtFelh.SetFelhasznaloTextBoxById(FormHeader1.ErrorPanel);

        radioButtonListEresites.SelectedValue = erec_FeladatErtesites.Ertesites;

        ErvenyessegCalendarControl1.ErvKezd = erec_FeladatErtesites.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_FeladatErtesites.ErvVege;

        FormHeader1.Record_Ver = erec_FeladatErtesites.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_FeladatErtesites.Base);
    }

    // form --> business object
    private EREC_FeladatErtesites GetBusinessObjectFromComponents()
    {
        EREC_FeladatErtesites erec_FeladatErtesites = new EREC_FeladatErtesites();
        // �sszes mez� update-elhet�s�g�t kezdetben letiltani:
        erec_FeladatErtesites.Updated.SetValueAll(false);
        erec_FeladatErtesites.Base.Updated.SetValueAll(false);

        erec_FeladatErtesites.FeladatDefinicio_id = DropDownListFeladatDefinicio.SelectedValue;
        erec_FeladatErtesites.Updated.FeladatDefinicio_id = pageView.GetUpdatedByView(DropDownListFeladatDefinicio);

        erec_FeladatErtesites.Felhasznalo_Id = FelhasznaloTextBoxFelErtFelh.Id_HiddenField;
        erec_FeladatErtesites.Updated.Felhasznalo_Id = pageView.GetUpdatedByView(FelhasznaloTextBoxFelErtFelh);

        erec_FeladatErtesites.Ertesites = radioButtonListEresites.SelectedValue;
        erec_FeladatErtesites.Updated.Ertesites = pageView.GetUpdatedByView(radioButtonListEresites);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_FeladatErtesites, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        erec_FeladatErtesites.Base.Ver = FormHeader1.Record_Ver;
        erec_FeladatErtesites.Base.Updated.Ver = true;

        return erec_FeladatErtesites;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            compSelector.Add_ComponentOnClick(DropDownListFeladatDefinicio);
            compSelector.Add_ComponentOnClick(FelhasznaloTextBoxFelErtFelh);
            compSelector.Add_ComponentOnClick(radioButtonListEresites);
            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        SaveProcedure(e);
    }

    private void SaveProcedure(CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_FeladatErtesitesService service = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();
                            EREC_FeladatErtesites erec_FeladatErtesites = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            erec_FeladatErtesites.Id = Guid.NewGuid().ToString();
                            erec_FeladatErtesites.Updated.Id = true;

                            if (IsExistErtesites(erec_FeladatErtesites, false))
                            {
                                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.MessageItemAlreadyExist);
                                return;
                            }
                            Result result = service.Insert(execParam, erec_FeladatErtesites);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_FeladatErtesitesService service = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();
                                EREC_FeladatErtesites erec_FeladatErtesites = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                if (IsExistErtesites(erec_FeladatErtesites, true))
                                {
                                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, Resources.Error.MessageItemAlreadyExist);
                                    return;
                                }

                                Result result = service.Update(execParam, erec_FeladatErtesites);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    /// <summary>
    /// IsExistErtesites
    /// </summary>
    /// <param name="item"></param>
    /// <param name="modeUpdate"></param>
    /// <returns></returns>
    private bool IsExistErtesites(EREC_FeladatErtesites item, bool modeUpdate)
    {
        EREC_FeladatErtesitesService service = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();
        EREC_FeladatErtesitesSearch src = new EREC_FeladatErtesitesSearch();

        src.FeladatDefinicio_id.Filter(item.FeladatDefinicio_id);
        src.Felhasznalo_Id.Filter(item.Felhasznalo_Id);

        if (modeUpdate)
        {
            src.Felhasznalo_Id.NotEquals(item.Id);
        }
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Result result = service.GetAll(execParam, src);
        if (result.IsError) { return false; }

        return result.GetCount > 0;
    }
}
