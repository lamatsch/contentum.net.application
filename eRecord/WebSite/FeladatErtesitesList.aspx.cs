using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class FeladatErtesitesList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FeladatErtesitesList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.FeladatErtesitesListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_FeladatErtesitesSearch);
        // A baloldali, alapb�l invisible ikonok megjelen�t�se:

        //LZS - BUG_10196
        ListHeader1.PrintVisible = false;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FeladatErtesitesSearch.aspx", ""
            , Defaults.PopupWidth_1280, Defaults.PopupHeight, FeladatErtesitesUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("FeladatErtesitesForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, FeladatErtesitesUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(FeladatErtesitesGridView.ClientID);
        ListHeader1.PrintOnClientClick = "javascript:window.open('FeladatErtesitesListPrintFormSSRS.aspx')";

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(FeladatErtesitesGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(FeladatErtesitesGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(FeladatErtesitesGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(FeladatErtesitesGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, FeladatErtesitesUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = FeladatErtesitesGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(FeladatErtesitesGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_FeladatErtesitesSearch());

        if (!IsPostBack) FeladatErtesitesGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "FeladatErtesites" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(FeladatErtesitesGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }


    #endregion

    #region Master List

    protected void FeladatErtesitesGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("FeladatErtesitesGridView", ViewState, "EREC_FeladatErtesites.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("FeladatErtesitesGridView", ViewState);

        FeladatErtesitesGridViewBind(sortExpression, sortDirection);
    }

    protected void FeladatErtesitesGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        UI.ClearGridViewRowSelection(FeladatErtesitesGridView);

        EREC_FeladatErtesitesService service = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_FeladatErtesitesSearch search = null;
        search = (EREC_FeladatErtesitesSearch)Search.GetSearchObject(Page, new EREC_FeladatErtesitesSearch());

        search.OrderBy = Search.GetOrderBy("FeladatErtesitesGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(FeladatErtesitesGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void FeladatErtesitesGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void FeladatErtesitesGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = FeladatErtesitesGridView.PageIndex;

        FeladatErtesitesGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = FeladatErtesitesGridView.PageCount;

        if (prev_PageIndex != FeladatErtesitesGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, FeladatErtesitesCPE);
            FeladatErtesitesGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, FeladatErtesitesCPE);
        }

        //ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(FeladatErtesitesGridView);
        ListHeader1.RefreshPagerLabel();

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        FeladatErtesitesGridViewBind();
    }

    protected void FeladatErtesitesGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(FeladatErtesitesGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("FeladatErtesitesForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, FeladatErtesitesUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("FeladatErtesitesForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, FeladatErtesitesUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_FeladatErtesites";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, FeladatErtesitesUpdatePanel.ClientID);

            //ListHeader1.PrintOnClientClick = "javascript:window.open('FeladatErtesitesPrintForm.aspx?" + QueryStringVars.Id + "=" + id + "')";

        }
    }

    protected void FeladatErtesitesUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    FeladatErtesitesGridViewBind();
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedFeladatErtesites();
            FeladatErtesitesGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedFeladatErtesitesRecords();
                FeladatErtesitesGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedFeladatErtesitesRecords();
                FeladatErtesitesGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedFeladatErtesites();
                break;
        }
    }

    private void LockSelectedFeladatErtesitesRecords()
    {
        LockManager.LockSelectedGridViewRecords(FeladatErtesitesGridView, "EREC_FeladatErtesites"
            , "FeladatErtesitesLock", "FeladatErtesitesForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedFeladatErtesitesRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(FeladatErtesitesGridView, "EREC_FeladatErtesites"
            , "FeladatErtesitesLock", "FeladatErtesitesForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a FeladatErtesitesGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedFeladatErtesites()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FeladatErtesitesInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(FeladatErtesitesGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_FeladatErtesitesService service = eRecordService.ServiceFactory.GetEREC_FeladatErtesitesService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a FeladatErtesitesGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedFeladatErtesites()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(FeladatErtesitesGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_FeladatErtesites");
        }
    }

    protected void FeladatErtesitesGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        FeladatErtesitesGridViewBind(e.SortExpression, UI.GetSortToGridView("FeladatErtesitesGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region UI FUNCTION
    /// <summary>
    /// GetErtesitesTextFromValue
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    protected string GetErtesitesTextFromValue(object value)
    {
        if (value == null || string.IsNullOrEmpty(value.ToString()))
            return Resources.Question.NotSet;

        switch (value.ToString())
        {
            case "1":
                return Resources.Question.Enabled;
            case "0":
                return Resources.Question.Disabled;
            default:
                return Resources.Question.NotSet;
        }
    }
    #endregion
}

