<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FeladatErtesitesSearch.aspx.cs" Inherits="FeladatErtesitesSearch" Title="<%$Resources:Search,FeladatErtesitesSearchHeaderTitle%>" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="ucFELH" %>
<%--<%@ Register Src="Component/KodTarakDropDownList.ascx" TagName="KodTarakDropDownList"
    TagPrefix="kddl" %>
<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox"
    TagPrefix="ftb" %>--%>
<%--<%@ Register Src="Component/ObjTipusokTextBox.ascx" TagName="ObjTipusokTextBox" TagPrefix="uc9" %>--%>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
    text-align: left;
    width: 100px;
    min-width: 100px;
}
    </style>
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,FeladatErtesitesSearchHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="90%">
        <tbody>
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tbody>
                                <tr class="urlapNyitoSor">
                                    <td class="mrUrlapCaption">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                    <td class="mrUrlapMezo">
                                        <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelFunkcioKivalto" runat="server" Text="Feladat defin�ci�:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="DropDownListFeladatDefinicio" runat="server"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelDefinicioTipus" runat="server" Text="Felhaszn�l�:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <ucFELH:FelhasznaloTextBox ID="FelhasznaloTextBoxFelErtFelh" runat="server" Validate="false" CustomTextEnabled="false"
                                            WithEmail="true" />
                                    </td>
                                </tr>

                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelErtesites" runat="server" Text="�rtes�t�s:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:RadioButtonList ID="radioButtonListEresites" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="<%$Resources:Question,Enabled%>" Value="1" />
                                            <asp:ListItem Text="<%$Resources:Question,Disabled%>" Value="0" />
                                            <asp:ListItem Text="<%$Resources:Question,NotSet%>" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2" rowspan="2">
                                        <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server"></uc5:Ervenyesseg_SearchFormComponent>
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo"></td>
                                </tr>
                                <tr class="urlapSor">
                                    <td colspan="2">
                                        <uc4:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                            runat="server"></uc4:TalalatokSzama_SearchFormComponent>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

