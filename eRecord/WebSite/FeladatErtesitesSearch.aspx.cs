using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class FeladatErtesitesSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_FeladatErtesitesSearch);

    /// <summary>
    /// Az oldal Init esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

        if (!IsPostBack)
        {
            FillControls();
        }
    }

    /// <summary>
    /// Az oldal Load esem�nykezel�je
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);


        if (!IsPostBack)
        {
            EREC_FeladatErtesitesSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_FeladatErtesitesSearch)Search.GetSearchObject(Page, new EREC_FeladatErtesitesSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Form komponensek felt�lt�se a keres�si objektumb�l
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch = (EREC_FeladatErtesitesSearch)searchObject;

        if (_EREC_FeladatErtesitesSearch != null)
        {
            FelhasznaloTextBoxFelErtFelh.Id_HiddenField = _EREC_FeladatErtesitesSearch.Felhasznalo_Id.Value;
            FelhasznaloTextBoxFelErtFelh.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            radioButtonListEresites.SelectedValue = _EREC_FeladatErtesitesSearch.Ertesites.Value;

            DropDownListFeladatDefinicio.SelectedValue = _EREC_FeladatErtesitesSearch.FeladatDefinicio_id.Value;


            Ervenyesseg_SearchFormComponent1.SetDefault(
                _EREC_FeladatErtesitesSearch.ErvKezd, _EREC_FeladatErtesitesSearch.ErvVege);
        }
    }

    /// <summary>
    /// Keres�si objektum l�trehoz�sa a form komponenseinek �rt�keib�l
    /// </summary>
    private EREC_FeladatErtesitesSearch SetSearchObjectFromComponents()
    {
        EREC_FeladatErtesitesSearch _EREC_FeladatErtesitesSearch = (EREC_FeladatErtesitesSearch)SearchHeader1.TemplateObject;

        if (_EREC_FeladatErtesitesSearch == null)
        {
            _EREC_FeladatErtesitesSearch = new EREC_FeladatErtesitesSearch();
        }

        //LZS - BUG_10196
        if (DropDownListFeladatDefinicio.SelectedIndex != 0)
        {
            _EREC_FeladatErtesitesSearch.FeladatDefinicio_id.Value = DropDownListFeladatDefinicio.SelectedValue;
            _EREC_FeladatErtesitesSearch.FeladatDefinicio_id.Operator = Query.Operators.equals;
        }

        //LZS - BUG_10196
        if (!string.IsNullOrEmpty(radioButtonListEresites.SelectedValue))
        {
            _EREC_FeladatErtesitesSearch.Ertesites.Value = radioButtonListEresites.SelectedValue;
            _EREC_FeladatErtesitesSearch.Ertesites.Operator = Query.Operators.equals;
        }
        //LZS - BUG_10196
        if (!string.IsNullOrEmpty(FelhasznaloTextBoxFelErtFelh.Id_HiddenField))
        {
            _EREC_FeladatErtesitesSearch.Felhasznalo_Id.Value = FelhasznaloTextBoxFelErtFelh.Id_HiddenField;
            _EREC_FeladatErtesitesSearch.Felhasznalo_Id.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
           _EREC_FeladatErtesitesSearch.ErvKezd, _EREC_FeladatErtesitesSearch.ErvVege);

        return _EREC_FeladatErtesitesSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    /// <summary>
    /// A keres�si form footer funkci�gomjainak esem�nyeinek kezel�se
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_FeladatErtesitesSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }

    /// <summary>
    /// �j keres�si objektum l�trehoz�sa az alapbe�ll�t�sokkal
    /// </summary>
    /// <returns></returns>
    private EREC_FeladatErtesitesSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new EREC_FeladatErtesitesSearch();
    }

    #region FILL CONTROLS
    private void FillControls()
    {
        DataRowCollection definiciok = LoadFeladatDefiniciok();
        FillControlFeladatDefiniciok(definiciok);
    }
    /// <summary>
    /// LoadFeladatDefiniciok
    /// </summary>
    /// <returns></returns>
    private DataRowCollection LoadFeladatDefiniciok()
    {
        EREC_FeladatDefinicioService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_FeladatDefinicioService();
        EREC_FeladatDefinicioSearch src = new EREC_FeladatDefinicioSearch();
        src.OrderBy = "Funkcio_Nev_Kivalto";

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service.GetAllWithExtension(execParam, src);
        if (!string.IsNullOrEmpty(result.ErrorCode) || result.Ds.Tables[0].Rows.Count < 1)
            return null;

        return result.Ds.Tables[0].Rows;

    }
    /// <summary>
    /// FillControlFeladatDefiniciok
    /// </summary>
    /// <param name="items"></param>
    private void FillControlFeladatDefiniciok(DataRowCollection items)
    {
        if (items == null)
            return;

        //LZS - BUG_10196
        //�res elem hozz�ada�sa
        DropDownListFeladatDefinicio.Items.Insert(0, new ListItem("[" + Resources.Form.UI_No_SelectedItem + "]", "[" + Resources.Form.UI_No_SelectedItem + "]"));
        DropDownListFeladatDefinicio.SelectedIndex = 0;


        string name = null;
        foreach (DataRow item in items)
        {
            //LZS - BUG_10196
            name = string.Format("[{0}] - [{1}] - [{2}]", item["Funkcio_Nev_Kivalto"], item["FeladatDefinicioTipus_Nev"], item["FeladatLeiras"]);
            DropDownListFeladatDefinicio.Items.Add(new ListItem(name, item["Id"].ToString()));
        }
    }
    #endregion
}
