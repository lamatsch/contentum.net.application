<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Feladatok.aspx.cs" Inherits="Feladatok" %>
<%@ MasterType TypeName="MasterPage" %>

<%@ Register Src="eRecordComponent/FeladataimPanel.ascx" TagName="FeladataimPanel"
    TagPrefix="uc1" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
    
<%@ Register Src="~/eRecordComponent/FeladatokTab.ascx" TagName="FeladatokTab" TagPrefix="uc1" %>    
<%@ Register Src="~/eRecordComponent/FeladatRiportPanel.ascx" TagName="FeladatRiportPanel" TagPrefix="uc1" %>     
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server"/>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="left">
                    <ajaxToolkit:TabContainer ID="TabContainerMaster" runat="server"
                        AutoPostBack="true" Width="100%">
                        <ajaxToolkit:TabPanel ID="TabPanelIratkezelesiFeldataim" runat="server" TabIndex="0">
                            <HeaderTemplate>
                            <asp:UpdatePanel runat="server" ID="updatePanelIratkezelesiFeladataimHeader">
                                <ContentTemplate>
                                    <asp:Label ID="labelIratkezelesiFeladataim" runat="server" Text="Iratkezelési feladataim"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                    <uc1:FeladataimPanel ID="FeladataimPanel1" runat="server">
                                    </uc1:FeladataimPanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanelFeldataim" runat="server" TabIndex="1">
                            <HeaderTemplate>
                            <asp:UpdatePanel runat="server" ID="updatePanelFeladataimHeader">
                                <ContentTemplate>
                                    <asp:Label ID="labelFeladataim" runat="server" Text="Feladataim"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <uc1:FeladatokTab ID="FeladatokTabFeladataim" runat="server">
                                </uc1:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanelKiadott" runat="server" TabIndex="2">
                            <HeaderTemplate>
                            <asp:UpdatePanel runat="server" ID="updatePanelKiadottHeader">
                                <ContentTemplate>
                                    <asp:Label ID="labelKiadott" runat="server" Text="Kiadott feladataim"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                    <uc1:FeladatokTab ID="FeladatokTabKiadott" runat="server">
                                    </uc1:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanelDolgozokFeladatai" runat="server" TabIndex="3">
                            <HeaderTemplate>
                            <asp:UpdatePanel runat="server" ID="updatePanelDolgozokFeladataiHeader">
                                <ContentTemplate>
                                    <asp:Label ID="labelDolgozokFeladatai" runat="server" Text="Dolgozók feladatai"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                    <uc1:FeladatokTab ID="FeladatokTabDolgozokFeladatai" runat="server">
                                    </uc1:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanelObjektumFeladatai" runat="server" TabIndex="4" Enabled="false">
                            <HeaderTemplate>
                            <asp:UpdatePanel runat="server" ID="updatePanelObjektumFeldataiHeader">
                                <ContentTemplate>
                                    <asp:Label ID="labelObjektumFeladatai" runat="server" Text="Feladatok/Kezelési feljegyzések"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                    <uc1:FeladatokTab ID="FeladatokTabObjektumFeladatai" runat="server" HeaderText="Feladatok/Kezelési feljegyzések">
                                    </uc1:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanelFeladatRiport" runat="server" TabIndex="5">
                            <HeaderTemplate>
                                <asp:Label ID="labelFeladatRiport" runat="server" Text="Feladat összesítő"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                    <uc1:FeladatRiportPanel ID="FeladatRiportPanel" runat="server">
                                    </uc1:FeladatRiportPanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td style="text-align:center">
                <asp:ImageButton ID="ImageClose" runat="server" 
                ImageUrl="~/images/hu/ovalgomb/bezar.jpg" 
                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')" 
                OnClientClick="window.close(); return false;" Visible="false"/>    
            </td>
        </tr>
    </table>
</asp:Content>
