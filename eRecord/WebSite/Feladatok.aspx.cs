﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Collections.Generic;

public partial class Feladatok : Contentum.eUtility.UI.PageBase
{
    protected void Page_Init(object sender, EventArgs e)
    {
        Response.CacheControl = "no-cache";

        FeladatokTabFeladataim.ParentForm = Constants.ParentForms.Feladataim;
        FeladatokTabKiadott.ParentForm = Constants.ParentForms.KiadottFeladataim;
        FeladatokTabDolgozokFeladatai.ParentForm = Constants.ParentForms.DolgozokFeladatai;
        FeladatokTabFeladataim.ListHeader = ListHeader1;
        FeladatokTabFeladataim.TabHeader = labelFeladataim;
        FeladatokTabKiadott.ListHeader = ListHeader1;
        FeladatokTabKiadott.TabHeader = labelKiadott;
        FeladatokTabDolgozokFeladatai.ListHeader = ListHeader1;
        FeladatokTabDolgozokFeladatai.TabHeader = labelDolgozokFeladatai;
        FeladatokTabObjektumFeladatai.ListHeader = ListHeader1;
        FeladatokTabObjektumFeladatai.TabHeader = labelObjektumFeladatai;

        FeladatRiportPanel.ListHeader = ListHeader1;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //FeladatokTabFeladataim.Height = 200;
        //FeladatokTabKiadott.Height = 200;
        ListHeader1.HeaderLabel = Resources.List.FeladataimHeaderTitle;
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PagerVisible = false;        
        #region Baloldali funkciógombok kiszedése
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.AddVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;

        ListHeader1.RefreshVisible = true;
        #endregion
        ListHeader1.CustomSearchObjectSessionName = "-----";

        // BUG#5711: NMHH-nál ne legyen jogosultságszűrés a kezelési feljegyzésekre
        if (FelhasznaloProfil.OrgIsNMHH(this.Page))
        {
            FeladatokTabObjektumFeladatai.Jogosultak = false;
        }

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainerMaster);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainerMaster);
        ScriptManager1.AsyncPostBackError += new EventHandler<AsyncPostBackErrorEventArgs>(ScriptManager1_AsyncPostBackError);


        ListHeader1.RefreshOnClientClick = "__doPostBack('" + FeladataimPanel1.MainUpdatePanel.ClientID + "','" + EventArgumentConst.refreshFeladatok + "');";
        //ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(FeladataimPanel1.MainUpdatePanel.ClientID);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
        if (!IsPostBack)
        {
            #region Dolgozók feladatai tabpanel csak a vezetõknél látszódik:

            string CsoportTagsag_Tipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
            if (CsoportTagsag_Tipus == KodTarak.CsoprttagsagTipus.vezeto)
            {
                TabPanelDolgozokFeladatai.Enabled = true;
                FeladatokTabDolgozokFeladatai.Visible = true;
            }
            else
            {
                TabPanelDolgozokFeladatai.Enabled = false;
                FeladatokTabDolgozokFeladatai.Visible = false;
            }

            #endregion

            #region Feladat összesítõ csak FeladatRiport jog esetén látszik
            if (FunctionRights.GetFunkcioJog(Page, "FeladatRiport"))
            {
                TabPanelFeladatRiport.Enabled = true;
                TabPanelFeladatRiport.Visible = true;
            }
            else
            {
                TabPanelFeladatRiport.Enabled = false;
                TabPanelFeladatRiport.Visible = false;
            }
            #endregion Feladat összesítõ csak FeladatRiport jog esetén látszik

            #region Default tab beallitasa
            string selectedTab = GetDefaultSelectedTabId();
            bool isSelectedTab = false;
            if (!String.IsNullOrEmpty(selectedTab))
            {
                foreach (AjaxControlToolkit.TabPanel tab in TabContainerMaster.Tabs)
                {
                    if (IsTabVisible(tab))
                    {
                        tab.Enabled = true;
                        if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                        {
                            isSelectedTab = true;
                            TabContainerMaster.ActiveTab = tab;
                        }
                    }
                    else
                    {
                        tab.Enabled = false;
                    }
                }
            }

            if (!isSelectedTab)
            {
                FeladataimPanel1.Active = true;
            }
            #endregion

            //ListHeader1.Collapsed = true;
 
            ActiveTabRefresh(TabContainerMaster);
        }

        if (IsPostBack)
        {
            HandlePostBackEvent();
        }
    }

    private string GetDefaultSelectedTabId()
    {
        string selectedTabId = TabPanelIratkezelesiFeldataim.ID;

        string qsSelectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);

        if (!String.IsNullOrEmpty(qsSelectedTab))
        {
            return qsSelectedTab;
        }

        string objektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);
        string objektumType = Request.QueryString.Get(QueryStringVars.ObjektumType);

        if (!String.IsNullOrEmpty(objektumId) && !String.IsNullOrEmpty(objektumType))
        {
            FeladatokTabObjektumFeladatai.ObjektumType = objektumType;
            FeladatokTabObjektumFeladatai.ObjektumId = objektumId;
            selectedTabId = TabPanelObjektumFeladatai.ID;
            AddVisibleTab(TabPanelObjektumFeladatai);
        }

        return selectedTabId;
    }

    private List<string> _visibleTabs = null;

    private List<string> GetVisibleTabs()
    {
        if (_visibleTabs == null)
        {
            if (!HataridosFeladatok.IsFeladatTabEnabled(Page))
            {
                AddVisibleTab(TabPanelIratkezelesiFeldataim);
            }
            else
            {
                foreach (AjaxControlToolkit.TabPanel tab in TabContainerMaster.Tabs)
                {
                    if (tab.Enabled)
                    {
                        AddVisibleTab(tab);
                    }
                }
            }
        }

        return _visibleTabs;
    }

    private void AddVisibleTab(AjaxControlToolkit.TabPanel tab)
    {
        if (_visibleTabs == null)
        {
            _visibleTabs = new List<string>();
        }
        _visibleTabs.Add(tab.ID);
    }

    private bool IsTabVisible(AjaxControlToolkit.TabPanel tab)
    {
        if (GetVisibleTabs().Contains(tab.ID))
        {
            return true;
        }

        return false;
        
    }

    void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
    }

    private void HandlePostBackEvent()
    {
        if (Request.Params["__EVENTARGUMENT"] != null && Request.Params["__EVENTTARGET"] != null)
        {
            string eventTarget = Request.Params["__EVENTTARGET"];
            if (eventTarget == TabContainerMaster.UniqueID)
            {
                string eventArgument = Request.Params["__EVENTARGUMENT"];
                string eventName = eventArgument.Split(':')[0];
                if (String.Compare(eventName, "activeTabChanged", true) == 0)
                {
                    TabContainerMaster_ActiveTabChanged(TabContainerMaster, EventArgs.Empty);
                }
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelObjektumFeladatai))
        {
            Master.SetPopupFromat();
            ListHeader1.DafaultPageLinkVisible = false;
            ImageClose.Visible = true;
        }
    }

    #region Master Tab

    protected void TabContainerMaster_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);

    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        FeladatokTabFeladataim.Active = false;
        FeladatokTabKiadott.Active = false;
        FeladataimPanel1.Active = false;
        FeladatokTabDolgozokFeladatai.Active = false;
        FeladatokTabObjektumFeladatai.Active = false;

        FeladatRiportPanel.Active = false;

        if (TabContainerMaster.ActiveTab.Equals(TabPanelFeldataim))
        {
            FeladatokTabFeladataim.Active = true;
            FeladatokTabFeladataim.ReLoadTab();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelKiadott))
        {
            FeladatokTabKiadott.Active = true;
            FeladatokTabKiadott.ReLoadTab();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelIratkezelesiFeldataim))
        {
            ListHeader1.SetRightFunctionButtonsVisible(false);
            FeladataimPanel1.Active = true;
            FeladataimPanel1.ReloadDatas();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelDolgozokFeladatai))
        {
            FeladatokTabDolgozokFeladatai.Active = true;
            FeladatokTabDolgozokFeladatai.ReLoadTab();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelObjektumFeladatai))
        {
            FeladatokTabObjektumFeladatai.Active = true;
            FeladatokTabObjektumFeladatai.ReLoadTab();
            //TabContainerMaster.CssClass = "InvisibleHeaderTabStyle";
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelFeladatRiport))
        {
            FeladatRiportPanel.Active = true;
            FeladatRiportPanel.ReLoadTab();
        }

    }

    #endregion
    
    


   



}
