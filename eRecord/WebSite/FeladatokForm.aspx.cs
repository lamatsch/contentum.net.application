﻿using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class FeladatokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = String.Empty;
    private string ObjektumId = String.Empty;
    private string ObjektumType = String.Empty;
    private string Startup = String.Empty;
    private string FeladatId = String.Empty;
    private string PrevId = String.Empty;
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    public string GetObjType()
    {
        if (!String.IsNullOrEmpty(ObjektumType))
            return ObjektumType;

        string objType = String.Empty;

        switch (Startup)
        {
            case Constants.Startup.FromUgyirat:
                objType = Constants.TableNames.EREC_UgyUgyiratok;
                break;
            case Constants.Startup.FromIrat:
                objType = Constants.TableNames.EREC_IraIratok;
                break;
            case Constants.Startup.FromKuldemeny:
                objType = Constants.TableNames.EREC_KuldKuldemenyek;
                break;
            case Constants.Startup.FromIratPeldany:
                objType = Constants.TableNames.EREC_PldIratPeldanyok;
                break;
            default:
                break;
        }

        return objType;
    }

    private void SetViewControls()
    {
    }

    private void SetModifyControls()
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        ObjektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);
        ObjektumType = Request.QueryString.Get(QueryStringVars.ObjektumType);
        Startup = Request.QueryString.Get(QueryStringVars.Startup);
        FeladatId = Request.QueryString.Get(QueryStringVars.Id);
        PrevId = Request.QueryString.Get("PrevId");

        ParentFeladatPanel.ErrorPanel = FormHeader1.ErrorPanel;
        FeladatPanel.ErrorPanel = FormHeader1.ErrorPanel;

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Feladat" + Command);
                break;
        }

        if (!IsPostBack)
        {
            InitForm();
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                FeladatPanel.FeladatId = id;
                FeladatPanel.SetFeladatById();
            }

        }
        else if (Command == CommandName.New)
        {
            FormFooter1.ImageButton_SaveAndNew.Visible = true;
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command == CommandName.Modify)
        {
            SetModifyControls();
        }
    }

    private void InitForm()
    {
        bool resetFeladatPanel = true;

        if (Command == CommandName.New)
        {
            if (!String.IsNullOrEmpty(FeladatId))
            {
                ParentFeladatPanel.FeladatId = FeladatId;
                ParentFeladatPanel.SetFeladatById();
                FeladatPanel.SetObjektum(ParentFeladatPanel.GetObjektum_Id(), ParentFeladatPanel.GetObjektum_Type(), ParentFeladatPanel.GetObjektum_Azonosito());
                resetFeladatPanel = false;
            }
            else if (!String.IsNullOrEmpty(ObjektumId))
            {
                FeladatPanel.SetObjektum(ObjektumId, GetObjType());
            }
        }

        if (resetFeladatPanel)
        {
            ParentFeladatPanel.Reset();
        }


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Startup == Constants.Startup.FromObjektumok)
        {
            FormHeader1.FullManualHeaderTitle = "Feladat - Objektum hozzárendelés";
        }
        else
        {
            if (String.IsNullOrEmpty(FeladatId) || Command != CommandName.New)
                FormHeader1.HeaderTitle = Resources.Form.FeladatokFormHeaderTitle;
            else
                FormHeader1.HeaderTitle = "Részfeladatok";
        }

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        FormFooter1.ValidationGroup = FeladatPanel.ValidationGroup;
        FormFooter1.ImageButton_SaveAndNew.ValidationGroup = FeladatPanel.ValidationGroup;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        SetDisplayFormat();

        if (!String.IsNullOrEmpty(PrevId))
        {
            FormFooter1.ImageButton_Cancel.OnClientClick = String.Empty;
        }
    }

    private void SetDisplayFormat()
    {
        string format = Request.QueryString.Get("Format");
        if (format == "Email")
        {
            SetEmailFormat();
        }
    }

    private void SetEmailFormat()
    {
        SetChildControlsEmailFormat(Page.Controls);
        Page.EnableViewState = false;
    }

    private void SetChildControlsEmailFormat(ControlCollection cc)
    {
        for (int i = cc.Count - 1; i >= 0; i--)
        {
            Control c = cc[i];
            SetControlEmailFormat(c);
            if (c.HasControls())
            {
                SetChildControlsEmailFormat(c.Controls);
            }
        }
    }

    private void SetControlEmailFormat(Control c)
    {
        if (!c.Visible) return;

        Label label = new Label();

        CopyStyleToStyle(c, label);
        CopyCssToStyle(c, label);

        label.Style.Add(HtmlTextWriterStyle.Color, "black");
        //label.Style.Add(HtmlTextWriterStyle.BorderColor, "silver");
        //label.Style.Add(HtmlTextWriterStyle.BorderStyle, "inset");
        //label.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");

        Control parent = c.Parent;

        if (c is ListControl)
        {
            ListControl listControl = c as ListControl;
            if (listControl.SelectedIndex > -1)
            {
                label.Text = listControl.SelectedItem.Text;
            }
            RepaceControl(c, label);
        }

        if (c is ExtenderControl)
        {
            try
            {
                ExtenderControl ex = c as ExtenderControl;
                if (parent != null)
                {
                    parent.Controls.Remove(ex);
                }
            }
            catch (Exception)
            {
            }
        }
        if (c is BaseValidator)
        {
            c.Visible = false;
        }

        if (c is TextBox)
        {
            TextBox tb = c as TextBox;
            label.Text = tb.Text;
            RepaceControl(c, label);
        }

        if (c is Image)
        {
            c.Visible = false;
        }

        if (c is CheckBox)
        {
            CheckBox cb = c as CheckBox;
            if (cb.Checked)
            {
                label.Text = cb.Text;
                RepaceControl(cb, label);
            }
            else
            {
                RepaceControl(cb, null);
            }
        }

        if (c is UpdateProgress)
        {
            c.Visible = false;
        }

        if (c is Label)
        {
            Label lbl = c as Label;
            if (lbl.Text == "*")
            {
                lbl.Visible = false;
            }
        }

        if (c is Contentum.eUIControls.eFormPanel)
        {
            Contentum.eUIControls.eFormPanel formPanel = c as Contentum.eUIControls.eFormPanel;
            formPanel.RenderMode = Contentum.eUIControls.FormPanelRenderMode.Panel;
            formPanel.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
            formPanel.Style.Add(HtmlTextWriterStyle.BorderColor, "silver");
            formPanel.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");

        }

        if (c.GetType().Name.ToLower().Contains("formheader"))
        {
            c.Visible = false;
        }

        if (c.GetType().Name.ToLower().Contains("errormodalpopup"))
        {
            c.Visible = false;
        }

        if (c is HiddenField)
        {
            c.Visible = false;
        }
    }

    private void CopyStyleToStyle(Control c, Label label)
    {
        if (c is WebControl)
        {
            WebControl wc = c as WebControl;
            label.Width = wc.Width;
            foreach (string key in wc.Style.Keys)
            {
                label.Style.Add(key, wc.Style[key]);
            }
        }

        if (c is HtmlControl)
        {
            HtmlControl hc = c as HtmlControl;
            foreach (string key in hc.Style.Keys)
            {
                label.Style.Add(key, hc.Style[key]);
            }
        }
    }

    private void CopyCssToStyle(Control c, Label label)
    {
        if (c is WebControl)
        {
            WebControl wc = c as WebControl;
            AddStyles(label, wc.CssClass);
            foreach (string key in label.Style.Keys)
            {
                wc.Style.Add(key, label.Style[key]);
            }
            wc.CssClass = String.Empty;
        }

        if (c is HtmlControl)
        {
            HtmlControl hc = c as HtmlControl;
            AddStyles(label, hc.Attributes["class"]);
            foreach (string key in label.Style.Keys)
            {
                hc.Style.Add(key, label.Style[key]);
            }
            hc.Attributes.Remove("class");

            //if (hc.TagName == "body")
            //{
            //    hc.Style.Add(HtmlTextWriterStyle.BackgroundColor,"rgb(243, 243, 243)");
            //    hc.Style.Add(HtmlTextWriterStyle.Margin,"0px 0px 0px 0px");
            //    //hc.Style.Add(HtmlTextWriterStyle.FontFamily,"arial, verdana, helvetica");
            //    //hc.Style.Add(HtmlTextWriterStyle.FontSize,"12px");
            //    hc.Style.Add(HtmlTextWriterStyle.Color, "#335674");
            //}
        }

        if (c is LiteralControl)
        {
            LiteralControl li = c as LiteralControl;

            int i = li.Text.IndexOf("class");
            while (i > -1)
            {
                int startIndex = i + "class=\"".Length;
                int endIndex = li.Text.IndexOf("\"", startIndex);
                if (endIndex > -1)
                {
                    string cssClass = li.Text.Substring(startIndex, endIndex - startIndex);
                    AddStyles(label, cssClass);

                    string ls = UI.RenderControl(label);

                    int sIndex = ls.IndexOf("style");

                    if (sIndex > -1)
                    {
                        int sStartIndex = sIndex + "style=\"".Length;
                        int sEndIndex = ls.IndexOf("\"", sStartIndex);
                        if (sEndIndex > -1)
                        {
                            string style = ls.Substring(sIndex, sEndIndex + 1 - sIndex);
                            li.Text = li.Text.Insert(endIndex + 1, " " + style);
                        }
                    }

                    li.Text = li.Text.Remove(i, endIndex + 1 - i);
                }
                i = li.Text.IndexOf("class", i);
            }
        }
    }

    private void AddStyles(Label styleContainer, string cssClass)
    {
        switch (cssClass)
        {
            case "mrUrlapCaption":
                styleContainer.Style.Add(HtmlTextWriterStyle.Width, "200px");
                goto case "mrUrlapCaptionCommon";
            case "mrUrlapCaption_shorter":
                styleContainer.Style.Add(HtmlTextWriterStyle.Width, "140px");
                goto case "mrUrlapCaptionCommon";
            case "mrUrlapCaption_short":
                styleContainer.Style.Add(HtmlTextWriterStyle.Width, "120px");
                goto case "mrUrlapCaptionCommon";
            case "mrUrlapCaptionCommon":
                styleContainer.Style.Add(HtmlTextWriterStyle.Color, "#335674");
                styleContainer.Style.Add(HtmlTextWriterStyle.FontWeight, "bold");
                styleContainer.Style.Add(HtmlTextWriterStyle.TextAlign, "right");
                styleContainer.Style.Add(HtmlTextWriterStyle.WhiteSpace, "nowrap");
                styleContainer.Style.Add(HtmlTextWriterStyle.VerticalAlign, "middle");
                styleContainer.Style.Add(HtmlTextWriterStyle.PaddingRight, "6px");
                break;
            case "urlapSor":
                styleContainer.Style.Add(HtmlTextWriterStyle.Height, "30px");
                break;
            case "mrUrlapMezo":
                styleContainer.Style.Add(HtmlTextWriterStyle.TextAlign, "left");
                break;
        }
    }

    private void RepaceControl(Control originalControl, Control controlToReplace)
    {
        Control parent = originalControl.Parent;
        if (parent != null)
        {
            int i = parent.Controls.IndexOf(originalControl);
            parent.Controls.Remove(originalControl);
            if (controlToReplace != null)
            {
                parent.Controls.AddAt(i, controlToReplace);
            }
        }
    }

    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (!Page.IsValid) return;
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndNew)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Feladat" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            string resultId;
                            bool isError = FeladatPanel.Create(FeladatId, false, out resultId);
                            if (e.CommandName == CommandName.SaveAndNew)
                            {
                                if (!isError)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, resultId);
                                    string url = Request.Url.ToString();
                                    int index = url.IndexOf("&PrevId");
                                    if (index > -1)
                                    {
                                        url = url.Substring(0, index);
                                    }
                                    url += "&PrevId=" + resultId;
                                    Response.Redirect(url, true);
                                }
                            }
                            else
                            {
                                if (!isError)
                                {
                                    CloseWindow(resultId);
                                }
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            if (String.IsNullOrEmpty(FeladatId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                bool isError = FeladatPanel.Save();
                                if (!isError)
                                {
                                    CloseWindow(FeladatId);
                                }
                            }
                            break;
                        }

                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }

        if (e.CommandName == CommandName.Cancel)
        {
            CloseWindow(PrevId);
        }
    }

    private void CloseWindow(string returnId)
    {
        ParentFeladatPanel.Reset();
        FeladatPanel.Reset();
        //if (!String.IsNullOrEmpty(PrevId))
        if (!String.IsNullOrEmpty(returnId))
        {
            JavaScripts.RegisterSelectedRecordIdToParent(Page, returnId);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
        else
        {
            JavaScripts.RegisterCloseWindowClientScript(Page, false);
        }
    }
}
