﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FeladatokSearch.aspx.cs" Inherits="FeladatokSearch" Title="Untitled Page" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc3" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc10" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc5" %>
<%@ Register Src="Component/FunkcioTextBox.ascx" TagName="FunkcioTextBox" TagPrefix="uc11" %>
<%@ Register Src="~/eRecordComponent/ObjektumValasztoPanel.ascx" TagName="ObjektumValasztoPanel" TagPrefix="uc12" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server"/>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <asp:UpdatePanel ID="updatePanelMain" runat="server">
                            <ContentTemplate>
                                <table id="tableMain" runat="server" cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAltipus" runat="server" Text="Kezelés:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="ktddAltipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                        </td>
                                    </tr>
                                    <tr id="trTipus" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labeTipus" runat="server" Text="Típus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="ktddTipus" runat="server" CssClass="mrUrlapInputComboBox" />
                                        </td>
                                    </tr>
                                    <tr id="trMemo" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="LabelMemo" runat="server" Text="Memo:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList runat="server" ID="rbListMemo" CssClass="mrUrlapInputRadioButtonList"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Mind" Value="-1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Igen" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Nem" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelLeiras" runat="server" Text="Leírás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox runat="server" ID="tbxLeiras" CssClass="mrUrlapInputFull" TextMode="MultiLine"
                                                Rows="2" />
                                        </td>
                                    </tr>
                                    <tr id="trObjektum" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelObjektum" runat="server" Text="Iratkezelési elem:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc12:ObjektumValasztoPanel ID="ObjektumValaszto" runat="server" SearchMode="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelFunkcio" runat="server" Text="Iratkezelési feladat:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:UpdatePanel ID="updatePanelFunkcio" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <uc11:FunkcioTextBox ID="funkcioTextBox" runat="server" AjaxEnabled="true" SearchMode="true" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>    
                                    </tr>
                                    <tr class="urlapSor" id="trAllapot" runat="server" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAllapot" runat="server" Text="Állapot:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="ktddAllapot" runat="server" CssClass="mrUrlapInputComboBox" />
                                        </td>
                                    </tr>
                                    <tr id="trAktiv" class="urlapSor" runat="server" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAktiv" runat="server" Text="Aktív/lezárt állapot:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButtonList runat="server" ID="rbListAktiv" CssClass="mrUrlapInputRadioButtonList"
                                                RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Mind" Value="-1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Aktív" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Lezárt" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelIntezkedesiHatarido" runat="server" Text="Intézkedési határidő:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:DatumIntervallum_SearchCalendarControl runat="server" ID="ccIntezkedes" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelPrioritas" runat="server" Text="Prioritás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc10:KodtarakDropDownList runat="server" ID="ktListPrioritas" CssClass="mrUrlapInputRadioButtonList" Validate="false"/>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelFelelos" runat="server" Text="Felelős:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc8:CsoportTextBox runat="server" ID="cstbxFelelos" SearchMode="true" />
                                        </td>
                                    </tr>
                                    
                                    <tr class="urlapSor" runat="server" id="trKiado" group="Feladat">
                                    <%--<tr class="urlapSor" runat="server" id="trKiado">--%>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelKiado" runat="server" Text="Kiadó:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:FelhasznaloCsoportTextBox runat="server" ID="ftbxKiado" SearchMode="true" />
                                        </td>
                                    </tr>
                                      <%--BUG_4651--%>
                                      <tr class="urlapSor" id="tr_letrehozo"  runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelLetrehozo" runat="server" Text="Létrehozó:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc7:FelhasznaloCsoportTextBox runat="server" ID="ftbxLetrehozo" SearchMode="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelMegoldas" runat="server" Text="Megoldás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox runat="server" ID="txtMegoldas" CssClass="mrUrlapInputFull" TextMode="MultiLine"
                                                Rows="2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labeNote" runat="server" Text="Megjegyzés:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox runat="server" ID="txtNote" CssClass="mrUrlapInputFull" TextMode="MultiLine"
                                                Rows="2" />
                                        </td>
                                    </tr>
                                   <%-- <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelKezdesiIdo" runat="server" Text="Kezdési határidő:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:DatumIntervallum_SearchCalendarControl runat="server" ID="ccKezdes" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>--%>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelLezarasDatuma" runat="server" Text="Lezárás dátuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:DatumIntervallum_SearchCalendarControl runat="server" ID="ccLezaras" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelKiadasDatuma" runat="server" Text="Kiadás dátuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:DatumIntervallum_SearchCalendarControl runat="server" ID="ccKiadas" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" group="Feladat">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelForras" runat="server" Text="Forrás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <asp:RadioButtonList runat="server" ID="rblistForras" RepeatDirection="Horizontal"
                                                CssClass="mrUrlapInputRadioButtonList">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                  
                                </table> 
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </eUI:eFormPanel>
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
