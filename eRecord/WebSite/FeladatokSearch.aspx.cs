﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;

public partial class FeladatokSearch : System.Web.UI.Page
{
    string Startup = String.Empty;
    private Type _type = typeof(EREC_HataridosFeladatokSearch);

    string ObjektumId = String.Empty;
    string ObjektumType = String.Empty;
    string FeladatTipus = String.Empty;

    private bool IsFeladat
    {
        get
        {
            if (ktddTipus.SelectedValue == KodTarak.FELADAT_TIPUS.Feladat)
                return true;
            return false;
        }
        set
        {
            ktddTipus.SetSelectedValue(KodTarak.FELADAT_TIPUS.Feladat);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);
        ObjektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);
        ObjektumType = Request.QueryString.Get(QueryStringVars.ObjektumType);
        FeladatTipus = Request.QueryString.Get(QueryStringVars.FeladatTipus);
        SearchHeader1.TemplateObjectType = _type;

        if (HataridosFeladatok.IsDisabled(Page))
        {
            trTipus.Visible = false;
            trMemo.Visible = false;
            trObjektum.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.FeladatokSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            InitForm();
            EREC_HataridosFeladatokSearch searchObject = null;
            switch (Startup)
            {
                case Constants.Startup.FromFeladataim:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.FeladataimSearch))
                    {
                        searchObject = (EREC_HataridosFeladatokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_HataridosFeladatokSearch(), Constants.CustomSearchObjectSessionNames.FeladataimSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromKiadottFeladatok:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KiadottFeladatokSearch))
                    {
                        searchObject = (EREC_HataridosFeladatokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_HataridosFeladatokSearch(), Constants.CustomSearchObjectSessionNames.KiadottFeladatokSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromDolgozokFeladatai:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.DolgozokFeladataiSearch))
                    {
                        searchObject = (EREC_HataridosFeladatokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_HataridosFeladatokSearch(), Constants.CustomSearchObjectSessionNames.DolgozokFeladataiSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                default:
                    if (Search.IsSearchObjectInSession(Page, _type))
                    {
                        searchObject = (EREC_HataridosFeladatokSearch)Search.GetSearchObject(Page, new EREC_HataridosFeladatokSearch());
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
            }

            LoadComponentsFromSearchObject(searchObject);

            //if (!String.IsNullOrEmpty(ObjektumType) && !String.IsNullOrEmpty(ObjektumId))
            //{
            //    ObjektumValaszto.SetObjektum(ObjektumId, ObjektumType);
            //    ObjektumValaszto.ReadOnly = true;
            //}
        }

        ObjektumValaszto.SelectedTipusChanged += new EventHandler(ObjektumValaszto_SelectedTipusChanged);
        ktddTipus.DropDownList.AutoPostBack = true;
    }

    private IEnumerable<Control> GetFeladatControls()
    {
        foreach (HtmlTableRow row in tableMain.Rows)
        {
            if (row.Attributes["group"] != null)
            {
                if (row.Attributes["group"] == "Feladat")
                    yield return row;
            }
        }
    }

    private void SetFeladatControlsVisiblity(bool visible)
    {
        foreach (Control c in GetFeladatControls())
        {
            c.Visible = visible;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        funkcioTextBox.ObjektumTipusFilter = ObjektumValaszto.GetSelected_Obj_Type();
        // BUG_4651
        SetFelhasznaloFilterField();
        SetFeladatControlsVisiblity(IsFeladat);
    }

    void ObjektumValaszto_SelectedTipusChanged(object sender, EventArgs e)
    {
        updatePanelFunkcio.Update();
    }

    private void InitForm()
    {
        SetFelhasznaloFilterField();

        ktddAllapot.FillAndSetEmptyValue(KodTarak.FELADAT_ALLAPOT.kcsNev, SearchHeader1.ErrorPanel);
        //ktddTipus.FillAndSetEmptyValue(KodTarak.FELADAT_TIPUS.kcsNev, SearchHeader1.ErrorPanel);

        ktListPrioritas.FillAndSetEmptyValue(KodTarak.FELADAT_Prioritas.kcsNev, SearchHeader1.ErrorPanel);
        HataridosFeladatok.Forras.FillRadioButtonList(rblistForras, true);
        ktddTipus.FillAndSetEmptyValue(KodTarak.FELADAT_TIPUS.kcsNev, SearchHeader1.ErrorPanel);
        ktddAltipus.FillAndSetEmptyValue(KodTarak.FELADAT_ALTIPUS.kcsNev, SearchHeader1.ErrorPanel);
    }

    private void SetFelhasznaloFilterField()
    {
        if (Startup == Constants.Startup.FromFeladataim)
        {
            // BUG_4651
            if (IsFeladat)
            {
                cstbxFelelos.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                cstbxFelelos.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                //cstbxFelelos.ReadOnly = true;
            }
            else
            {
                ftbxLetrehozo.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                ftbxLetrehozo.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            }

        }
        if (Startup == Constants.Startup.FromKiadottFeladatok)
        {
            // BUG_4651
            if (IsFeladat)
            {
                ftbxKiado.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                ftbxKiado.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                //ftbxKiado.ReadOnly = true;
            }
            else
            {
                ftbxLetrehozo.Id_HiddenField = FelhasznaloProfil.FelhasznaloId(Page);
                ftbxLetrehozo.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            }
           
        }
        tr_letrehozo.Visible = !IsFeladat;
    }

    public bool IsFuggetlen(EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch)
    {
        if (erec_HataridosFeladatokSearch.Obj_type.Operator == Query.Operators.isnull)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = null;
        if (searchObject != null) erec_HataridosFeladatokSearch = (EREC_HataridosFeladatokSearch)searchObject;

        if (erec_HataridosFeladatokSearch != null)
        {

            ftbxKiado.Id_HiddenField = erec_HataridosFeladatokSearch.Felhasznalo_Id_Kiado.Value;
            ftbxKiado.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            cstbxFelelos.Id_HiddenField = erec_HataridosFeladatokSearch.Felhasznalo_Id_Felelos.Value.Split(',')[0].Trim('\'');
            cstbxFelelos.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            ktddAllapot.SetSelectedValue(erec_HataridosFeladatokSearch.Allapot.Value);

            // BUG_4651
           ftbxLetrehozo.Id_HiddenField = erec_HataridosFeladatokSearch.Letrehozo_id.Value;
           ftbxLetrehozo.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
           
            if (!String.IsNullOrEmpty(erec_HataridosFeladatokSearch.Manual_Lezartak.Value)
                && !String.IsNullOrEmpty(erec_HataridosFeladatokSearch.Manual_Lezartak.Operator))
            {
                rbListAktiv.SelectedValue = (erec_HataridosFeladatokSearch.Manual_Lezartak.Operator == Query.Operators.inner ? "0" : "1");

            }
            else
            {
                rbListAktiv.SelectedValue = "-1";
            }

            ktddAltipus.SetSelectedValue(erec_HataridosFeladatokSearch.Altipus.Value);
            ktddTipus.SetSelectedValue(erec_HataridosFeladatokSearch.Tipus.Value);
            if (erec_HataridosFeladatokSearch.Memo.Value == Constants.Database.No ||
                erec_HataridosFeladatokSearch.Memo.Value == Constants.Database.Yes)
            {
                rbListMemo.SelectedValue = erec_HataridosFeladatokSearch.Memo.Value;
            }
            else
            {
                rbListMemo.SelectedValue = "-1";
            }
            //ktddTipus.SetSelectedValue(erec_HataridosFeladatokSearch.Tipus.Value);
            funkcioTextBox.Id_HiddenField = erec_HataridosFeladatokSearch.Funkcio_Id_Inditando.Value;
            funkcioTextBox.SetFunkcioTextBoxById(SearchHeader1.ErrorPanel);
            tbxLeiras.Text = erec_HataridosFeladatokSearch.Leiras.Value;
            txtMegoldas.Text = erec_HataridosFeladatokSearch.Megoldas.Value;
            txtNote.Text = erec_HataridosFeladatokSearch.Note.Value;
            if (!String.IsNullOrEmpty(erec_HataridosFeladatokSearch.Forras.Value))
            {
                rblistForras.SelectedValue = erec_HataridosFeladatokSearch.Forras.Value;
            }
            else
            {
                rblistForras.SelectedValue = HataridosFeladatok.Forras.Empty;
            }
            if (!String.IsNullOrEmpty(erec_HataridosFeladatokSearch.Prioritas.Value))
            {
                ktListPrioritas.SelectedValue = erec_HataridosFeladatokSearch.Prioritas.Value;
            }
            else
            {
                ktListPrioritas.SelectedValue = String.Empty;
            }

            //LZS - BUG_7054
            //ccKezdes.SetComponentFromSearchObjectFields(erec_HataridosFeladatokSearch.KezdesiIdo);
            ccIntezkedes.SetComponentFromSearchObjectFields(erec_HataridosFeladatokSearch.IntezkHatarido);
            ccLezaras.SetComponentFromSearchObjectFields(erec_HataridosFeladatokSearch.LezarasDatuma);
            ccKiadas.SetComponentFromSearchObjectFields(erec_HataridosFeladatokSearch.LetrehozasIdo);

            if (IsFuggetlen(erec_HataridosFeladatokSearch))
            {
                ObjektumValaszto.IsFuggetlen = true;
            }
            else
            {
                ObjektumValaszto.IsFuggetlen = false;
                ObjektumValaszto.SetObjektum(erec_HataridosFeladatokSearch.Obj_Id.Value, erec_HataridosFeladatokSearch.Obj_type.Value);
            }
        }
        else
        {

        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_HataridosFeladatokSearch SetSearchObjectFromComponents()
    {
        EREC_HataridosFeladatokSearch erec_HataridosFeladatokSearch = (EREC_HataridosFeladatokSearch)SearchHeader1.TemplateObject;
        if (erec_HataridosFeladatokSearch == null)
        {
            erec_HataridosFeladatokSearch = new EREC_HataridosFeladatokSearch();
        }

        if (ktddAltipus.Visible)
        {
            if (!String.IsNullOrEmpty(ktddAltipus.SelectedValue))
            {
                erec_HataridosFeladatokSearch.Altipus.Value = ktddAltipus.SelectedValue;
                erec_HataridosFeladatokSearch.Altipus.Operator = Query.Operators.equals;
            }
        }

        if (ftbxKiado.Visible)
        {
            if (!String.IsNullOrEmpty(ftbxKiado.Id_HiddenField))
            {
                erec_HataridosFeladatokSearch.Felhasznalo_Id_Kiado.Value = ftbxKiado.Id_HiddenField;
                erec_HataridosFeladatokSearch.Felhasznalo_Id_Kiado.Operator = Query.Operators.equals;
            }
        }

        if (cstbxFelelos.Visible)
        {
            if (!String.IsNullOrEmpty(cstbxFelelos.Id_HiddenField))
            {
                erec_HataridosFeladatokSearch.Felhasznalo_Id_Felelos.Value = cstbxFelelos.Id_HiddenField;
                erec_HataridosFeladatokSearch.Felhasznalo_Id_Felelos.Operator = Query.Operators.equals;
            }
        }

        if (rbListAktiv.Visible)
        {
            if (rbListAktiv.SelectedIndex > -1 && rbListAktiv.SelectedValue != "-1")
            {
                erec_HataridosFeladatokSearch.Manual_Lezartak.Value = Search.GetSqlInnerString(HataridosFeladatok.GetLezartAllapotok().ToArray());
                erec_HataridosFeladatokSearch.Manual_Lezartak.Operator = (rbListAktiv.SelectedValue == "0" ? Query.Operators.inner : Query.Operators.notinner);
            }
            
        }

        if (ktddAllapot.Visible)
        {
            if (!String.IsNullOrEmpty(ktddAllapot.SelectedValue))
            {
                erec_HataridosFeladatokSearch.Allapot.Value = ktddAllapot.SelectedValue;
                erec_HataridosFeladatokSearch.Allapot.Operator = Query.Operators.equals;
            }
        }


        if (ktddTipus.Visible)
        {
            if (!String.IsNullOrEmpty(ktddTipus.SelectedValue))
            {
                erec_HataridosFeladatokSearch.Tipus.Value = ktddTipus.SelectedValue;
                erec_HataridosFeladatokSearch.Tipus.Operator = Query.Operators.equals;
            }
        }

        if (rbListMemo.Visible)
        {
            if (rbListMemo.SelectedIndex > -1 && rbListMemo.SelectedValue != "-1")
            {
                erec_HataridosFeladatokSearch.Memo.Value = rbListMemo.SelectedValue;
                erec_HataridosFeladatokSearch.Memo.Operator = Query.Operators.equals;
            }
        }

        if (funkcioTextBox.Visible)
        {
            if (!String.IsNullOrEmpty(funkcioTextBox.Id_HiddenField))
            {
                erec_HataridosFeladatokSearch.Funkcio_Id_Inditando.Value = funkcioTextBox.Id_HiddenField;
                erec_HataridosFeladatokSearch.Funkcio_Id_Inditando.Operator = Query.Operators.equals;
            }
        }

        if (ktListPrioritas.Visible)
        {
            if (ktListPrioritas.SelectedValue != String.Empty)
            {
                erec_HataridosFeladatokSearch.Prioritas.Value = ktListPrioritas.SelectedValue;
                erec_HataridosFeladatokSearch.Prioritas.Operator = Query.Operators.equals;
            }
        }

        if (rblistForras.Visible)
        {
            if (rblistForras.SelectedValue != HataridosFeladatok.Forras.Empty)
            {
                erec_HataridosFeladatokSearch.Forras.Value = rblistForras.SelectedValue;
                erec_HataridosFeladatokSearch.Forras.Operator = Query.Operators.equals;
            }
        }

        if (tbxLeiras.Visible)
        {
            erec_HataridosFeladatokSearch.Leiras.Value = tbxLeiras.Text;
            erec_HataridosFeladatokSearch.Leiras.Operator = Search.GetOperatorByLikeCharater(tbxLeiras.Text);
        }

        if (txtMegoldas.Visible)
        {
            erec_HataridosFeladatokSearch.Megoldas.Value = txtMegoldas.Text;
            erec_HataridosFeladatokSearch.Megoldas.Operator = Search.GetOperatorByLikeCharater(txtMegoldas.Text);
        }

        if (txtNote.Visible)
        {
            erec_HataridosFeladatokSearch.Note.Value = txtNote.Text;
            erec_HataridosFeladatokSearch.Note.Operator = Search.GetOperatorByLikeCharater(txtNote.Text);
        }

        //LZS - BUG_7054
        //if (ccKezdes.Visible)
        //{
        //    ccKezdes.SetSearchObjectFields(erec_HataridosFeladatokSearch.KezdesiIdo);
        //}
        if (ccIntezkedes.Visible)
        {
            ccIntezkedes.SetSearchObjectFields(erec_HataridosFeladatokSearch.IntezkHatarido);
        }
        if (ccLezaras.Visible)
        {
            ccLezaras.SetSearchObjectFields(erec_HataridosFeladatokSearch.LezarasDatuma);
        }
        if (ccKiadas.Visible)
        {
            ccKiadas.SetSearchObjectFields(erec_HataridosFeladatokSearch.LetrehozasIdo);
        }

        if (ObjektumValaszto.Visible)
        {
            if (ObjektumValaszto.IsFuggetlen)
            {
                erec_HataridosFeladatokSearch.Obj_type.Operator = Query.Operators.isnull;
            }
            else
            {
                if (!String.IsNullOrEmpty(ObjektumValaszto.GetSelected_Obj_Type()))
                {
                    erec_HataridosFeladatokSearch.Obj_type.Value = ObjektumValaszto.GetSelected_Obj_Type();
                    erec_HataridosFeladatokSearch.Obj_type.Operator = Query.Operators.equals;

                    if (!String.IsNullOrEmpty(ObjektumValaszto.GetSelected_Obj_Id()))
                    {
                        erec_HataridosFeladatokSearch.Obj_Id.Value = ObjektumValaszto.GetSelected_Obj_Id();
                        erec_HataridosFeladatokSearch.Obj_Id.Operator = Query.Operators.equals;
                    }
                }
            }
        }

        // BUG_4651
        if (ftbxLetrehozo.Visible)
        {
            if (!String.IsNullOrEmpty(ftbxLetrehozo.Id_HiddenField))
            {
                erec_HataridosFeladatokSearch.Letrehozo_id.Value = ftbxLetrehozo.Id_HiddenField;
                erec_HataridosFeladatokSearch.Letrehozo_id.Operator = Query.Operators.equals;
            }
        }

        return erec_HataridosFeladatokSearch;
    }


    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_HataridosFeladatokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                switch (Startup)
                {
                    case Constants.Startup.FromFeladataim:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.FeladataimSearch);
                        break;
                    case Constants.Startup.FromKiadottFeladatok:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KiadottFeladatokSearch);
                        break;
                    case Constants.Startup.FromDolgozokFeladatai:
                        Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.DolgozokFeladataiSearch);
                        break;
                    default:
                        Search.RemoveSearchObjectFromSession(Page, _type);
                        break;
                }   
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhereOnPanel(tableMain);
                switch (Startup)
                {
                    case Constants.Startup.FromFeladataim:
                        Search.SetSearchObject_CustomSessionName(Page,searchObject, Constants.CustomSearchObjectSessionNames.FeladataimSearch);
                        break;
                    case Constants.Startup.FromKiadottFeladatok:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.KiadottFeladatokSearch);
                        break;
                    case Constants.Startup.FromDolgozokFeladatai:
                        Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.DolgozokFeladataiSearch);
                        break;
                    default:
                        Search.SetSearchObject(Page, searchObject);
                        break;
                }
                
            }
            //TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_HataridosFeladatokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        EREC_HataridosFeladatokSearch defaultSearchObject = new EREC_HataridosFeladatokSearch();
        return defaultSearchObject;
    }

}
