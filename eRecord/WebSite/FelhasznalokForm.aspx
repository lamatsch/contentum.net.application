<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="FelhasznalokForm.aspx.cs" Inherits="FelhasznalokForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register Src="~/Component/PasswordControl.ascx" TagName="PasswordControl" TagPrefix="uc" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    <Scripts>
        <asp:ScriptReference Path="~/JavaScripts/jquery.js" />
    </Scripts>
    </asp:ScriptManager>
    <asp:HiddenField ID="hfIsNewPassword" runat="server" Value="0" />
    <script type="text/javascript">
        var originalAuthType;
        var isModify = Boolean.parse('<%=IsModify %>');
        var isView = Boolean.parse('<%=IsView %>');
        var isNew = Boolean.parse('<%=IsNew %>');
        var modifyPassword = $('#<%=hfIsNewPassword.ClientID %>').val() == '1' ? true : false;
        function OnAuthenticationChanged() {
            var IsBasicAuthentication = $('#<%=ddownAuthType.ClientID %> > option:selected').text() == 'Basic';
 
            var isNewPassword = (isNew && IsBasicAuthentication) 
                            || (isModify && IsBasicAuthentication && originalAuthType != 'Basic')
                            || (isModify && IsBasicAuthentication && originalAuthType == 'Basic' && modifyPassword);
                        
            eval('<%=PasswordControl.ClientID %>' + '_Display(' + isNewPassword + ')');
            
            if(isModify && IsBasicAuthentication && originalAuthType=='Basic' && !modifyPassword)
            {
                $('#<%=linkJelszoModositas.ClientID %>').show();
            }
            else
            {
                $('#<%=linkJelszoModositas.ClientID %>').hide();
            }
            if (IsBasicAuthentication) {
                $('#<%=tr_Jelszo_lejar_ido.ClientID %>').show();
            }
            else {
                $('#<%=tr_Jelszo_lejar_ido.ClientID %>').hide();
            }
            $get('<%=calendarJelszoLejar.Validator.ClientID %>').enabled = IsBasicAuthentication;
            $('#<%=hfIsNewPassword.ClientID %>').val(isNewPassword ? '1' : '0');
        }
        function OnModifyPasswordChanged() {
            modifyPassword = true;
            OnAuthenticationChanged();
        }
        $(document).ready(
        function() {
            if (!isView) {
                originalAuthType = $('#<%=ddownAuthType.ClientID %> > option:selected').text();
                $('#<%=ddownAuthType.ClientID %>').change(OnAuthenticationChanged);
                OnAuthenticationChanged();
                $('#<%=linkJelszoModositas.ClientID %>').click(function() { OnModifyPasswordChanged(); return false; });
            }
        }
        );
    </script>
    
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelUserNev" runat="server" Text="Felhaszn�l�n�v:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="requiredTextBoxUserNev" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelNev" runat="server" Text="N�v:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="requiredTextBoxNev" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelPartner" runat="server" Text="Partner:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:PartnerTextBox id="PartnerTextBox1" runat="server" Type="All" Validate="true">
                                </uc7:PartnerTextBox></td>
                        </tr>
                        <tr class="urlapSor" id="tr_AutoGeneratePartner_CheckBox" runat="server">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:CheckBox ID="cbAutogeneratePartner" runat="server" Text="A felhaszn�l�hoz automatikus partner gener�l�s" /></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMunkahely" runat="server" Text="Munkahely (szervezet):"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:PartnerTextBox ID="PartnerTextBoxMunkahely" runat="server" Validate="false"/>
                            </td>
                        </tr>                                                
                        <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMaxMinosites" runat="server" Text="Max.min�s�t�s:" Enabled="False"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddownMaxMinosites" runat="server" CssClass="mrUrlapInputComboBox" Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>--%>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEmail" runat="server" Text="E-mail c�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textEmail" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTelefon" runat="server" Text="Telefonsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textTelefon" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelBeosztas" runat="server" Text="Beoszt�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textBeosztas" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor" id="tr_Tipus_dropdown" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelAuthType" runat="server" Text="Azonos�t�s tipusa:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddownAuthType" runat="server" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList>
                                <asp:LinkButton ID="linkJelszoModositas" runat="server" Text="Jelsz� m�dos�t�sa" style="position:relative;padding-left:5px;top:-2px;"/></td>
                        </tr>
                        <%--<tr class="urlapSor" id="tr_Jelszo" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label12" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelJelszo" runat="server" Text="Jelsz�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="requiredTextBoxJelszo" Text="" runat="server" />
                                </td>
                        </tr>--%>
                        <uc:PasswordControl ID="PasswordControl" runat="server" />
                        <tr class="urlapSor" id="tr_Jelszo_lejar_ido" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label17" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelJelszoLejar" runat="server" Text="Jelsz� lej�r:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="calendarJelszoLejar" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" bOpenDirectionTop="true" />
                            </td>
                        </tr>                        
                        <tr class="urlapSor" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSystem" runat="server" Text="Admin:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="cbSystem" runat="server" Text="" CssClass="urlapCheckbox" />   
                        </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbListEngedelyzett" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>   
                        </td>
                        </tr>                      
                    </table>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
