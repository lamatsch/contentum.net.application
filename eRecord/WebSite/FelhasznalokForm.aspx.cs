using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System;
using System.Web.UI.WebControls;

public partial class FelhasznalokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected readonly ListItem authTypeWindows = new ListItem("Windows", "Windows");
    protected readonly ListItem authTypeBasic = new ListItem("Basic", "Basic");

    #region radiobuttonlist
    private void FillRadioList()
    {
        rbListEngedelyzett.Items.Clear();
        rbListEngedelyzett.Items.Add(new ListItem("Enged�lyezve", Constants.Database.Yes));
        rbListEngedelyzett.Items.Add(new ListItem("Letiltva", Constants.Database.No));
    }

    private void SetRadioList(string value)
    {
        if (value == Constants.Database.Yes || value == Constants.Database.No)
        {
            rbListEngedelyzett.SelectedValue = value;
        }
    }
    #endregion

    private void SetViewControls()
    {
        requiredTextBoxUserNev.ReadOnly = true;
        labelUserNev.CssClass = "mrUrlapInputWaterMarked";
        requiredTextBoxNev.ReadOnly = true;
        labelNev.CssClass = "mrUrlapInputWaterMarked";
        PartnerTextBox1.ReadOnly = true;
        labelPartner.CssClass = "mrUrlapInputWaterMarked";
        PartnerTextBoxMunkahely.ReadOnly = true;
        labelMunkahely.CssClass = "mrUrlapInputWaterMarked";
        //ddownMaxMinosites.Enabled = false;
        //labelMaxMinosites.CssClass = "mrUrlapInputWaterMarked";
        textEmail.ReadOnly = true;
        labelEmail.CssClass = "mrUrlapInputWaterMarked";
        ErvenyessegCalendarControl1.ReadOnly = true;
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        //cbSystem.Enabled = false;
        //labelSystem.CssClass = "mrUrlapInputWaterMarked";
        //ddownAuthType.Enabled = false;
        ddownAuthType.ReadOnly = true;
        labelAuthType.CssClass = "mrUrlapInputWaterMarked";
        rbListEngedelyzett.Enabled = false;
        labelTelefon.CssClass = "mrUrlapInputWaterMarked";
        textTelefon.ReadOnly = true;
        labelBeosztas.CssClass = "mrUrlapInputWaterMarked";
        textBeosztas.ReadOnly = true;
        PasswordControl.Visible = false;
        calendarJelszoLejar.ReadOnly = true;
        labelJelszoLejar.CssClass = "mrUrlapInputWaterMarked";
        tr_Jelszo_lejar_ido.Visible = IsBasicAuthentication;
        linkJelszoModositas.Visible = false;
        //requiredTextBoxJelszo.ReadOnly = true;
        //labelJelszo.CssClass = "mrUrlapInputWaterMarked";
    }

    private void SetModifyControls()
    {
        requiredTextBoxUserNev.ReadOnly = true;
        labelUserNev.CssClass = "mrUrlapInputWaterMarked";
        rbListEngedelyzett.Enabled = false;
        //ddownAuthType.Enabled = false;
        //labelAuthType.CssClass = "mrUrlapInputWaterMarked";
    }

    private void SetNewControls()
    {
        rbListEngedelyzett.Enabled = false;
        rbListEngedelyzett.Visible = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felhasznalo" + Command);
                break;
        }

        if (!IsPostBack)
        {
            ddownAuthType.Items.Add(authTypeWindows);
            ddownAuthType.Items.Add(authTypeBasic);
            this.FillRadioList();
            if (IsNew)
            {
                calendarJelszoLejar.Text = DefaultDates.EndDate;
            }
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Felhasznalok krt_felhasznalok = (KRT_Felhasznalok)result.Record;
                    LoadComponentsFromBusinessObject(krt_felhasznalok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }

                if (Command == CommandName.View)
                {
                    Contentum.eUtility.EsemenyNaplo.Insert(execParam, "KRT_Felhasznalok", FunkcioKod, id , result);
                }
            }

            cbAutogeneratePartner.Checked = false;
            tr_AutoGeneratePartner_CheckBox.Visible = false;
            //tr_Jelszo_lejar_ido.Visible = false;
            //tr_Jelszo.Visible = false;
            //tr_Tipus_dropdown.Visible = false;
        }
        else if (Command == CommandName.New)
        {
            string partnerID = Request.QueryString.Get(QueryStringVars.PartnerId);
            if (!String.IsNullOrEmpty(partnerID))
            {
                PartnerTextBox1.Id_HiddenField = partnerID;
                PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
                PartnerTextBox1.Enabled = false;
                tr_AutoGeneratePartner_CheckBox.Visible = false;
            }

        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if(Command == CommandName.Modify)
        {
            SetModifyControls();
        }
        if (Command == CommandName.New)
        {
            SetNewControls();
        }

        PartnerTextBox1.Type = Constants.FilterType.Partnerek.BelsoSzemely;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        FormHeader1.HeaderTitle = Resources.Form.FelhasznalokFormHeaderTitle;

        // Munkahely kiv�laszt�sn�l sz�r�s a szervezetre:
        PartnerTextBoxMunkahely.SetFilterType_Szervezet();

        //calendarJelszoLejar.Text = DefaultDates.EndDate;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private bool IsBasicAuthentication
    {
        get
        {
            return ddownAuthType.SelectedValue == "Basic";
               
        }
    }

    protected bool IsModify
    {
        get
        {
            return Command == CommandName.Modify;
        }
    }

    protected bool IsNew
    {
        get
        {
            return Command == CommandName.New;
        }
    }

    protected bool IsView
    {
        get
        {
            return Command == CommandName.View;
        }
    }

    private bool IsNewPassword
    {
        get
        {
            return (IsNew && IsBasicAuthentication)
                || (IsModify && IsBasicAuthentication && hfIsNewPassword.Value == "1");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Felhasznalok krt_felhasznalok)
    {
        requiredTextBoxUserNev.Text = krt_felhasznalok.UserNev;
        requiredTextBoxNev.Text = krt_felhasznalok.Nev;

        PartnerTextBox1.Id_HiddenField = krt_felhasznalok.Partner_id;
        PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        PartnerTextBoxMunkahely.Id_HiddenField = krt_felhasznalok.Partner_Id_Munkahely;
        PartnerTextBoxMunkahely.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

        textEmail.Text = krt_felhasznalok.EMail;

        try
        {
            ddownAuthType.SelectedValue = krt_felhasznalok.Tipus;
        }
        catch (System.ArgumentOutOfRangeException)
        {
            ddownAuthType.ClearSelection();
        }

        textTelefon.Text = krt_felhasznalok.Telefonszam;
        textBeosztas.Text = krt_felhasznalok.Beosztas;

        calendarJelszoLejar.Text = krt_felhasznalok.JelszoLejaratIdo;
        //requiredTextBoxJelszo.Visible = false;
        if (krt_felhasznalok.Tipus == authTypeBasic.Value)
        {
            //requiredTextBoxJelszo.Text = krt_felhasznalok.Jelszo;
        }

        this.SetRadioList(krt_felhasznalok.Engedelyezett);

        //cbSystem.Checked = (krt_felhasznalok.System == "1") ? true : false;

        ErvenyessegCalendarControl1.ErvKezd = krt_felhasznalok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_felhasznalok.ErvVege;

        FormHeader1.Record_Ver = krt_felhasznalok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_felhasznalok.Base);

    }

    // form --> business object
    private KRT_Felhasznalok GetBusinessObjectFromComponents()
    {
        KRT_Felhasznalok krt_felhasznalok = new KRT_Felhasznalok();
        krt_felhasznalok.Updated.SetValueAll(false);
        krt_felhasznalok.Base.Updated.SetValueAll(false);

        krt_felhasznalok.UserNev = requiredTextBoxUserNev.Text;
        krt_felhasznalok.Updated.UserNev = pageView.GetUpdatedByView(requiredTextBoxUserNev);

        krt_felhasznalok.Nev = requiredTextBoxNev.Text;
        krt_felhasznalok.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);

        if (cbAutogeneratePartner.Checked == false)
        {
            krt_felhasznalok.Partner_id = PartnerTextBox1.Id_HiddenField;
            krt_felhasznalok.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBox1);
        }

        krt_felhasznalok.Partner_Id_Munkahely = PartnerTextBoxMunkahely.Id_HiddenField;
        krt_felhasznalok.Updated.Partner_Id_Munkahely = pageView.GetUpdatedByView(PartnerTextBoxMunkahely);

        krt_felhasznalok.EMail = textEmail.Text;
        krt_felhasznalok.Updated.EMail = pageView.GetUpdatedByView(textEmail);

        krt_felhasznalok.Telefonszam = textTelefon.Text;
        krt_felhasznalok.Updated.Telefonszam = pageView.GetUpdatedByView(textTelefon);

        krt_felhasznalok.Beosztas = textBeosztas.Text;
        krt_felhasznalok.Updated.Beosztas = pageView.GetUpdatedByView(textBeosztas);

        krt_felhasznalok.Tipus = ddownAuthType.SelectedValue;
        krt_felhasznalok.Updated.Tipus = pageView.GetUpdatedByView(ddownAuthType);

        if (!String.IsNullOrEmpty(calendarJelszoLejar.Text))
        {
            krt_felhasznalok.JelszoLejaratIdo = calendarJelszoLejar.Text;
            krt_felhasznalok.Updated.JelszoLejaratIdo = pageView.GetUpdatedByView(calendarJelszoLejar);
        }

        if (IsNewPassword)
        {
            krt_felhasznalok.Jelszo = PasswordControl.Password;
            krt_felhasznalok.Updated.Jelszo = true;
        }

        //if (rbListEngedelyzett.SelectedIndex > -1)
        //{
        //    krt_felhasznalok.Engedelyezett = rbListEngedelyzett.SelectedValue;
        //    krt_felhasznalok.Updated.Engedelyezett = pageView.GetUpdatedByView(rbListEngedelyzett);
        //}

        //krt_felhasznalok.System = cbSystem.Checked ? "1" : "0";
        //krt_felhasznalok.Updated.System = pageView.GetUpdatedByView(cbSystem);

        //krt_felhasznalok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_felhasznalok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_felhasznalok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_felhasznalok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_felhasznalok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_felhasznalok.Base.Ver = FormHeader1.Record_Ver;
        krt_felhasznalok.Base.Updated.Ver = true;

        return krt_felhasznalok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);
            compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            compSelector.Add_ComponentOnClick(PartnerTextBox1);
            compSelector.Add_ComponentOnClick(cbAutogeneratePartner);
            compSelector.Add_ComponentOnClick(PartnerTextBoxMunkahely);
            //compSelector.Add_ComponentOnClick(ddownMaxMinosites);
            compSelector.Add_ComponentOnClick(textEmail);
            compSelector.Add_ComponentOnClick(ddownAuthType);
            compSelector.Add_ComponentOnClick(calendarJelszoLejar);
            //compSelector.Add_ComponentOnClick(requiredTextBoxJelszo);
            compSelector.Add_ComponentOnClick(rbListEngedelyzett);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(textTelefon);
            compSelector.Add_ComponentOnClick(textBeosztas);
            //compSelector.Add_ComponentOnClick(cbSystem);

            FormFooter1.SaveEnabled = false;

            //requiredTextBoxJelszo.Enabled = true;
            //AutoGeneratePartner_CheckBox.Attributes["onclick"] = "";
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            if (PasswordControl.DisplayErrorPanel(FormHeader1.ErrorPanel,IsNewPassword))
                                return;

                            KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                            KRT_Felhasznalok krt_felhasznalok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = null;
                            if (cbAutogeneratePartner.Checked == false)
                            {
                                result = service.Insert(execParam, krt_felhasznalok);
                            }
                            else
                            {
                                // Insert, partner gener�l�ssal:
                                result = service.InsertAndCreateNewPartner(execParam, krt_felhasznalok);
                            }

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, requiredTextBoxNev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            if (PasswordControl.DisplayErrorPanel(FormHeader1.ErrorPanel,IsNewPassword))
                                return;

                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                                KRT_Felhasznalok krt_felhasznalok = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, krt_felhasznalok);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    private void registerJavascripts()
    {

        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");


        cbAutogeneratePartner.Attributes["onclick"] =
            "showOrHideComponents('" + cbAutogeneratePartner.ClientID
                + "',1,['" + PartnerTextBox1.ImageButton_Lov.ClientID + "','"
                    + PartnerTextBox1.ImageButton_New.ClientID + "','"
                    + PartnerTextBox1.ImageButton_View.ClientID + "']); "
            + " SetComponentValueByCheckBox(this.id,'" + PartnerTextBox1.TextBox.ClientID
                + "','<" + Resources.Form.UI_NewPartner + ">','');"
            + " SetComponentValueByCheckBox(this.id,'" + PartnerTextBox1.Control_HiddenField.ClientID
                + "','','');";
    }
}
