<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FelhasznalokList.aspx.cs" Inherits="FelhasznalokList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="FelhasznalokCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="FelhasznalokUpdatePanel" runat="server" OnLoad="FelhasznalokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="FelhasznalokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="FelhasznalokCPEButton"
                            CollapseControlID="FelhasznalokCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="FelhasznalokCPEButton" ExpandedSize="0" ExpandedText="Felhasználók listája"
                            CollapsedText="Felhasználók listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="FelhasznalokGridView" runat="server" OnRowCommand="FelhasznalokGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="FelhasznalokGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="FelhasznalokGridView_Sorting"
                                            OnRowDataBound="FelhasznalokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Nev" HeaderText="Név" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                    SortExpression="Nev" HeaderStyle-Width="200px" />
                                                <asp:BoundField DataField="UserNev" HeaderText="Felhasználónév" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                    SortExpression="UserNev" HeaderStyle-Width="150px" />
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="linkEngedelyezett" CommandArgument="Engedelyezett" CommandName="Sort"
                                                            runat="server">Engedélyezett</asp:LinkButton>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbEngedelyezett" runat="server" Enabled="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="CsoportokTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelCsoporttagsagok" runat="server">
                                                <ContentTemplate>                                           
                                                    <asp:Label ID="Header1" runat="server" Text="Csoporttagságok"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                       
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="CsoportokUpdatePanel" runat="server" OnLoad="CsoportokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="CsoportokPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="CsoportokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="CsoportokCPE" runat="server" TargetControlID="Panel2"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <asp:GridView ID="CsoportokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="CsoportokGridView_Sorting"
                                                                            OnPreRender="CsoportokGridView_PreRender" OnRowCommand="CsoportokGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Csoport_Nev" HeaderText="Csoport neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Csoport_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozzárendelés ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="SzerepkorokTabPanel" runat="server" TabIndex="1">
                                        <HeaderTemplate>
                                            <asp:UpdatePanel ID="LabelUpdatePanelHozzarendeltSzerepkorok" runat="server">
                                                <ContentTemplate>                                               
                                                    <asp:Label ID="Label1" runat="server" Text="Hozzárendelt szerepkörök"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>                                                     
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="SzerepkorokUpdatePanel" runat="server" OnLoad="SzerepkorokUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="SzerepkorokPanel" runat="server" Visible="false" Width="100%">
                                                        <uc1:SubListHeader ID="SzerepkorokSubListHeader" runat="server" />
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="SzerepkorokCPE" runat="server" TargetControlID="Panel3"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel3" runat="server">
                                                                        <asp:GridView ID="SzerepkorokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                            AllowSorting="True" AutoGenerateColumns="false" OnSorting="SzerepkorokGridView_Sorting"
                                                                            OnPreRender="SzerepkorokGridView_PreRender" OnRowCommand="SzerepkorokGridView_RowCommand"
                                                                            DataKeyNames="Id">
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField>
                                                                                <asp:BoundField DataField="Szerepkor_Nev" HeaderText="Szerepkör neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Szerepkor_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="Csoport_Nev" HeaderText="Csoport" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="Csoport_Nev" HeaderStyle-Width="200px" />
                                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozzárendelés ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                    SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                                <asp:TemplateField HeaderText="Megbízásban kapott" SortExpression="Helyettesites_Id">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="cbMegbizasbanKapott" runat="server" Checked='<%# string.IsNullOrEmpty(Eval("Helyettesites_Id").ToString()) ? false : true  %>'
                                                                                            Enabled="false" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
