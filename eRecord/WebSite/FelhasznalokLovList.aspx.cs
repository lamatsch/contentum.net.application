using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class FelhasznalokLovList : Contentum.eUtility.UI.PageBase
{
    
    private bool disable_refreshLovList = false;
    private string filterType = String.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        filterType = Request.QueryString.Get(QueryStringVars.Filter);
        if (filterType == null) filterType = String.Empty;

        if (filterType == Constants.FilterType.Felhasznalok.FelhasznalokWithoutPartner)
        {
            LovListHeader1.HeaderTitle = Resources.LovList.FelhasznalokLovListHeaderTitle + " (Sz�rt)";
        }

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("FelhasznalokSearch.aspx", ""
            , 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick =
             JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
            + JavaScripts.SetOnClientClick("FelhasznalokForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'"
            , Defaults.PopupWidth, Defaults.PopupHeight, null);
                  
        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text,false);
        }

    }
    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text,false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        KRT_FelhasznalokService service = eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
        KRT_FelhasznalokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (KRT_FelhasznalokSearch)Search.GetSearchObject(Page, new KRT_FelhasznalokSearch());
        }
        else
        {
            search = new KRT_FelhasznalokSearch();

            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
            search.Nev.Value = SearchKey;
        }

        search.OrderBy = "Nev";

        if (filterType == Constants.FilterType.Felhasznalok.FelhasznalokWithoutPartner)
        {
            search.Partner_id.Operator = Query.Operators.isnull;
        }

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        if (filterType == Constants.FilterType.Felhasznalok.WithEmail)
        {
            if (!result.IsError)
            {
                foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                {
                    row["Nev"] = Contentum.eUtility.Felhasznalok.GetFelhasznaloNevWithEmail(row);
                }
            }
        }
        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel, "dr.");

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {      
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }
    }

}
