using Contentum.eAdmin.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FelhasznalokMultiSelect : Contentum.eUtility.UI.PageBase
{
    private string InitMessage
    {
        get
        {
            return Request.QueryString.Get("Message");
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //ScriptManager1.Services.Add(new ServiceReference("WrappedWebService/" + "Ajax.asmx"));

        FormFooter1.ButtonsClick += new CommandEventHandler(LovListFooter_ButtonsClick);

        String felh_Id = FelhasznaloProfil.FelhasznaloId(Page);
        //AutoCompleteExtender1.ContextKey = felh_Id + ";";

        FormFooter1.Command = CommandName.New;
        
        // a megsem gombra torli a parent window hidden field erteket
        string hiddenFieldId = Page.Request.QueryString.Get(QueryStringVars.HiddenFieldId);
        if (!String.IsNullOrEmpty(hiddenFieldId))
        {
            String script = "";
            script += " if (window.dialogArguments) { ";
            script += " window.dialogArguments.parentWindow.document.getElementById('" + hiddenFieldId + "').value = ''; \n";
            script += " } else { "
                + " window.opener.document.getElementById('" + hiddenFieldId + "').value = ''; \n"
                + " }";
            FormFooter1.ImageButton_Cancel.OnClientClick = script + FormFooter1.ImageButton_Cancel.OnClientClick;
        }

        this.MessageTextBox.Text = InitMessage;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        // BUG#7556: 
        JavaScripts.SetFocus(this.fmspFelhasznalok.FelhasznaloTextBox);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Save)
        {
            if (fmspFelhasznalok.Count > 0)
            {                                
                string selectedIds = String.Join(";", fmspFelhasznalok.SelectedIds);
                string selectedTexts = String.Join(";", fmspFelhasznalok.SelectedTexts);

                //JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                // mask the common line break chars (depends on the client) for JavaScript
                string message = MessageTextBox.Text.Replace("\r", "\\r").Replace("\n", "\\n");
                JavaScripts.SendBackResultIdsListToCallingWindow(Page, selectedTexts, message);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
            }
            else
            {
                ResultError.DisplayWarningOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }
}
