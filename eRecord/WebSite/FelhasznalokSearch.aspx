<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FelhasznalokSearch.aspx.cs" Inherits="FelhasznalokSearch" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>


<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
    
    
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>


<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="Felhasználónév:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Usernev" runat="server" Validate="false" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                                <asp:Label ID="Label3" runat="server" Text="Név:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:RequiredTextBox ID="Nev_TextBox" runat="server" Validate="false" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label9" runat="server" Text="Partner:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:PartnerTextBox id="PartnerTextBox1" runat="server" Type="All" SearchMode="true">
                                </uc7:PartnerTextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label15" runat="server" Text="Munkahely:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:PartnerTextBox ID="Munkahely_PartnerTextBox" runat="server" Visible="true" SearchMode="true" />
                            </td>
                        </tr>                                                
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="E-mail cím:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="email_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTelefon" runat="server" Text="Telefonszám:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textTelefon" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelBeosztas" runat="server" Text="Beosztás:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textBeosztas" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAuthType" runat="server" Text="Azonosítás tipusa:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddownAuthType" runat="server" CssClass="mrUrlapInputComboBox">
                                </asp:DropDownList></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbListEngedelyzett" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>   
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" >
                                <uc9:Ervenyesseg_SearchFormComponent id="Ervenyesseg_SearchFormComponent1" runat="server">
                                </uc9:Ervenyesseg_SearchFormComponent>
                                &nbsp;</td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                &nbsp; &nbsp;
                    
                    &nbsp; &nbsp;
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>
