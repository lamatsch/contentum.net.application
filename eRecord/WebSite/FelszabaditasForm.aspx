﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FelszabaditasForm.aspx.cs" Inherits="FelszabaditasForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>

<%@ Register src="eRecordComponent/KuldemenyMasterAdatok.ascx" tagname="KuldemenyMasterAdatok" tagprefix="uc3" %>
<%@ Register src="eRecordComponent/IratMasterAdatok.ascx" tagname="IratMasterAdatok" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,FelszabaditasFormHeaderTitle %>" />
    <asp:Panel ID="MainPanel" runat="server">
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <uc3:KuldemenyMasterAdatok ID="KuldemenyMasterAdatok1" runat="server" />
                    
                    <br />
                    
                    <uc4:IratMasterAdatok ID="IratMasterAdatok1" runat="server" />
                   
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    
                    <eUI:eFormPanel ID="UgyiratSztornoQuestion_EFormPanel1" runat="server" Visible="false">
                    <table cellspacing="0" cellpadding="0" width="90%">
                        <tr class="urlapSor">
                            <td style="text-align: center; color: Red; font-weight: bold;">                                
                                <asp:Label ID="label7" runat="server" Text="<%$Resources:Question,UISztornoUresUgyirat %>"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="UgyiratSztorno_RadioButtonList" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Igen" Value="Igen"></asp:ListItem>
                                    <asp:ListItem Text="Nem" Value="Nem" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>                            
                            <td style="width: 40%"></td>
                        </tr>                        
                               
                    </table>
                    </eUI:eFormPanel>    
                                                      
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>            
        </table>
     </asp:Panel>
</asp:Content>
