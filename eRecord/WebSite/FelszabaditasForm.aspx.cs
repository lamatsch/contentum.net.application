﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class FelszabaditasForm : Contentum.eUtility.UI.PageBase
{
    private string IratId = String.Empty;

    private const string FunkcioKod_IratFelszabaditas = "IratFelszabaditas";

    protected void Page_Init(object sender, EventArgs e)
    {        
        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        IratId = Request.QueryString.Get(QueryStringVars.IratId);

        // Jogosultságellenőrzés:                       
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratFelszabaditas);
               
        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);
    }



    private void LoadFormComponents()
    {
        if (String.IsNullOrEmpty(IratId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
            return;
        }

        IratMasterAdatok1.IratId = IratId;
        EREC_IraIratok iratObj = IratMasterAdatok1.SetIratMasterAdatokById(FormHeader1.ErrorPanel);

        // Van küldemény az irathoz? (Bejövő irat?)
        if (String.IsNullOrEmpty(iratObj.KuldKuldemenyek_Id))
        {
            // Az irat nem szabadítható fel, nem bejövő az irat
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorCode_52661, Resources.Error.ErrorCode_52662);
            KuldemenyMasterAdatok1.Visible = false;
            FormFooter1.Visible = false;
            return;
        }

        // Küldemény Master adatok feltöltése:
        KuldemenyMasterAdatok1.SetKuldemenyMasterAdatok(iratObj.KuldKuldemenyek_Id, FormHeader1.ErrorPanel, null);

        // Felszabadítható-e?

        // Ügyirat lekérése:
        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam_ugyiratGet = UI.SetExecParamDefault(Page);
        execParam_ugyiratGet.Record_Id = iratObj.Ugyirat_Id;

        Result result_ugyiratGet = service_ugyiratok.Get(execParam_ugyiratGet);
        if (result_ugyiratGet.IsError)
        {
            // hiba:
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGet);
            MainPanel.Visible = false;
            return;
        }

        EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok) result_ugyiratGet.Record;

        Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(iratObj);
        Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(ugyiratObj);
        ErrorDetails errorDetail = null;
                
        if (!Iratok.Felszabadithato(iratStatusz, ugyiratStatusz, UI.SetExecParamDefault(Page), out errorDetail))
        {
            // Nem szabadítható fel az irat:
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel,
                ResultError.CreateNewResultWithErrorCode(52661, errorDetail));
            FormFooter1.Visible = false;
            return;
        }

        // Ha nincs több irat az ügyiratban, akkor kitesszük a kérdést, hogy sztornózzuk-e az ügyiratot
        if (ugyiratObj.IratSzam == "1" && !iratStatusz.Munkairat)
        {
            UgyiratSztornoQuestion_EFormPanel1.Visible = true;
        }

    }



    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (!FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratFelszabaditas))
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }
            else
            {
                if (String.IsNullOrEmpty(IratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    // Felszabadítás:

                    EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page);

                    bool ugyiratSztornoHaUres = (UgyiratSztorno_RadioButtonList.SelectedValue == "Igen") ? true : false;

                    Result result = service.Felszabaditas(execParam, IratId, ugyiratSztornoHaUres);
                    if (result.IsError)
                    {
                        // hiba:
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                    else
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, IratId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);
                    }

                }
            }
        }
    }



}
