<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="FelulvizsgalatForm.aspx.cs" Inherits="FelulvizsgalatForm" Title="Untitled Page" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc10" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/JegyzekTextBox.ascx" TagName="JegyzekTextBox" TagPrefix="uc12" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc"%>
<%-- bernat.laszlo added - �j meg�rz�si id�h�z a control--%>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc20" %>
<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc8" %>
<%@ Register Src="~/Component/FuggoKodtarakDropDownList.ascx" TagPrefix="uc13" TagName="FuggoKodtarakDropDownList" %>
<%@ Register Src="~/eRecordComponent/IraIrattariTetelTextBoxWithExtensions.ascx" TagName="IraIrattariTetelTextBoxWithExtensions" TagPrefix="uc21" %>
<%@ Register Src="~/Component/TUKFelhasznaloMultiSelect.ascx" TagName="TUKFelhasznaloMultiSelect" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc:CustomUpdateProgress ID="UpdateProgress" runat="server" />

    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upFelulvizsgalat" runat="server">
    <ContentTemplate>
                    <eUI:eFormPanel ID="EFormPanelCsatolasError" runat="server" Visible="false">
                    <asp:HiddenField ID="hfBreakAbleUgyiratok" runat="server" />
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label2" runat="server" Text="A k�vetkez� csatolt �gyiratokon nem hajthat� v�gre a m�velet!"></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="labelBadUgyiratok" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label4" runat="server" Text="K�v�nja felbontani ezeket a csatol�sokat?"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="EFormPanelSzerelesError" runat="server" Visible="false">
                    <asp:HiddenField ID="hfBreakAbleSzereltUgyiratok" runat="server" />
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label3" runat="server" Text="A k�vetkez� szerelt �gyiratokon nem hajthat� v�gre a m�velet!"></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="labelBadSzereltUgyiratok" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label6" runat="server" Text="K�v�nja felbontani ezeket a szerel�seket?"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    <eUI:eFormPanel ID="EFormPanelElokeszitettSzerelesError" runat="server" Visible="false">
                    <asp:HiddenField ID="hfBreakAbleElokeszitettSzereltUgyiratok" runat="server" />
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label5" runat="server" Text="A k�vetkez�, el�k�sz�tett szerel�sekben �rintett �gyiratok miatt nem hajthat� v�gre a m�velet!"></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="labelBadElokeszitettSzereltUgyiratok" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td style="font-weight:bold;text-align:center;color:firebrick;" colspan="2">
                                <asp:Label ID="label7" runat="server" Text="K�v�nja felbontani ezeket az el�k�sz�tett szerel�seket?"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    <asp:Panel ID="IratPeldanyokListPanel" runat="server" Visible="false">
                         <uc:InfoModalPopup ID="InfoModalPopup1" runat="server" />
    
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                         <br />
                                         <asp:Label ID="Label_IratPeldanyok" runat="server" Text="Iratp�ld�nyok:" Visible="false" CssClass="GridViewTitle"></asp:Label>          
                                         <br />  
                                         <asp:GridView ID="PldIratPeldanyokGridView" runat="server"
                                                                    CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                                                    PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" 
                                                                    OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                
                                                                       
                                                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                                                        <ItemTemplate>                                    
                                                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                                                Visible="false" OnClientClick="return false;" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>  
                                                       
                                                                <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktat�sz�m" 
                                                                    SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="160px"/>
                                                                </asp:BoundField>             
                                                                <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="T�rgy" SortExpression="EREC_IraIratok_Targy">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Hat�rid�" SortExpression="VisszaerkezesiHatarido">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="C�mzett" SortExpression="NevSTR_Cimzett">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="KuldesMod_Nev" HeaderText="K�ld�sm�d" SortExpression="KuldesMod_Nev">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                                                </asp:BoundField>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                                            <HeaderTemplate>
                                                                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <PagerSettings Visible="False" />
                                                                </asp:GridView>                   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                                </td>
                            </tr>
                            <tr>
                            <td>
                                <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">     
                                <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                                <br />
                                <br />
                                </asp:Panel>
                            </td></tr>
                            </table>
                     </asp:Panel>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <%--BUG_8507--%>
                                <asp:Label ID="labelUserNev" runat="server" Text="<%$Forditas:labelUserNev|Fel�lvizsg�l�:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc10:FelhasznaloCsoportTextBox ID="FelhasznaloCsoportTextBoxFelulvizsgalo" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor"  id="tr_UjVizsgalat" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label150" runat="server" Text="�j �rz�si id�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc20:RequiredNumberBox ID="RequierdNumberBox_UjorzesIdo" runat="server"/>
                                <%--BLG_2156--%>
                                  <uc13:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" KodcsoportKod="IDOEGYSEG_FELHASZNALAS"   CssClass="hiddenitem" />
                                  <uc13:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_Idoegyseg" KodcsoportKod="IDOEGYSEG"  ParentControlId="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" Validate="True" />
                           </td>
                            
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelNev" runat="server" Text="Fel�lvizsg�lat ideje:"></asp:Label>
                             </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControlFelulvizsgalatIdeje" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_FelulvizsgalatEredmenye" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelPartner" runat="server" Text="Fel�lvizsg�lat eredm�nye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddlistFelulvizsgalatEredmenye" CausesValidation="false" AutoPostBack="true" runat="server" CssClass="mrUrlapInputComboBox" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_MinositesFelulvizsgalatEredmenye" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label8" runat="server" Text="Min�s�t�s fel�lvizsg�lat eredm�nye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="ddlistMinositesFelulvizsgalatEredmenye" CausesValidation="false" AutoPostBack="true" runat="server" CssClass="mrUrlapInputComboBox" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_modositasErvenyessegKezdete" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label9" runat="server" Text="M�dos�t�s �rv�nyess�g�nek kezdete:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="ccModositasErvenyessegKezdete" runat="server" ReadOnly="false" TimeVisible="true" Validate="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_ujMinositesiSzint" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label10" runat="server" Text="�j min�s�t�si szint:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:KodtarakDropDownList ID="KodtarakDropDownListMinositesiSzint" runat="server" CssClass="mrUrlapInputComboBox" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_ujMinositesErvenyessegiIdo" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label11" runat="server" Text="�j min. �rv�nyess�gi id�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl Validate="false" ID="ccUjErvenyessegiIdo" runat="server" ReadOnly="false" TimeVisible="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_minositoNeve" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label12" runat="server" Text="Min�s�t� neve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:EditablePartnerTextBox ID="PartnerTextBox_Minosito" runat="server" Validate="false"
                                            LabelRequiredIndicatorID="labelPartnerStar" WithAllCimCheckBoxVisible="false"
                                            WithKuldemeny="false" CssClass="mrUrlapInputSzeles" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_megszuntetoHatarozat" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label13" runat="server" Text="Megsz�ntet� hat�rozat, javaslat:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="tbMegszuntetoHatarozat" runat="server" CssClass="mrUrlapInput" ReadOnly="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_MinositesFelulvizsgalatBizTag" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMinositesFelulvizsgalatBizTag" runat="server" Text="Bizotts�gi tagok:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:TUKFelhasznaloMultiSelect ID="MinositesFelulvizsgalatBizTag" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor"  id="tr_KovetkezoVizsgalat" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEmail" runat="server" Text="K�vetkez� vizsg�lat d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControlKovetkezoVizsgalat" runat="server" ReadOnly="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor"  id="tr_Jegyzek" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label1" runat="server" Text="Jegyz�k:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc12:JegyzekTextBox runat="server" ID="JegyzekekTextBox1" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_Megyjegyzes" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAuthType" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="TextBoxMegyjegyzes" runat="server" Height="60px" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_IrattariTetel" runat="server">
                            <td colspan="2">
                                <uc21:IraIrattariTetelTextBoxWithExtensions ID="IraIrattariTetelTextBox" runat="server" LongCaption="true" Validate="true" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

