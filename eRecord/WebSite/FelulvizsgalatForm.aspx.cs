using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using System.Data;
using Contentum.eAdmin.Service;

public partial class FelulvizsgalatForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private string StartUp = String.Empty;
    private bool isTuk = false;
    UI ui = new UI();
    //BLG 1722
    private const string kcs_IRAT_MINOSITES = "IRAT_MINOSITES";

    bool FromPeldanyok
    {
        get
        {
            return StartUp == Constants.Startup.FromIratPeldany;
        }
    }

    private void SetViewControls()
    {

    }

    private void SetModifyControls()
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        StartUp = Request.QueryString.Get(Constants.Startup.StartupName);
        JegyzekekTextBox1.StartUp = StartUp;

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Felulvizsgalat" + Command);
                break;
        }
        // BLG_2156
        FuggoKodtarakDropDownList_IdoegysegFelhasznalas.SelectedValue = KodTarak.IdoegysegFelhasznalas.Selejtezes;
        //BLG1722
        isTuk = Rendszerparameterek.IsTUK(Page);

        if (!IsPostBack)
        {
            if (!FromPeldanyok)
            {
                EREC_IraJegyzekTetelekService svc = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                if (StartUp == Constants.Startup.FromEgyebSzervezetnekAtadas)
                {
                    //el�k�sz�tett szerel�sek ellen�rz�se
                    Result res = svc.CheckElokeszitettSzerelesek(xpm, GetSelectedUgyiratok());
                    if (string.IsNullOrEmpty(res.ErrorCode))
                        if (res.Ds.Tables[0].Rows.Count > 0)
                        {
                            EFormPanelElokeszitettSzerelesError.Visible = true;
                            EFormPanel1.Visible = false;
                            int i = 0;
                            foreach (DataRow row in res.Ds.Tables[0].Rows)
                            {
                                if (i == 0)
                                {
                                    labelBadElokeszitettSzereltUgyiratok.Text = "<ul style=\"list-style-type:decimal;\">";
                                }
                                else
                                {

                                    hfBreakAbleElokeszitettSzereltUgyiratok.Value += ";";
                                }

                                string onclick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx"
                                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + row["Id"].ToString()
                                + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromFelulvizsgalat
                                + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                                labelBadElokeszitettSzereltUgyiratok.Text += "<li style=\"padding-top:5px;\"><a href='' class=\"badUgyiratok\" onclick=\"" + onclick + "\">" + row["Foszam_Merge"].ToString() + "</a></<li>";
                                hfBreakAbleElokeszitettSzereltUgyiratok.Value += row["Id"].ToString();
                                i++;
                            }
                            labelBadElokeszitettSzereltUgyiratok.Text += "</ul>";
                        }
                }
                else
                {
                    if (StartUp == Constants.Startup.FromFelulvizsgalando)
                        LoadIraIrattariTetel();

                    //csatol�sok ellen�rz�se
                    Result res = svc.CheckCsatolasok(xpm, GetSelectedUgyiratok());
                    if (string.IsNullOrEmpty(res.ErrorCode))
                        if (res.Ds.Tables[0].Rows.Count > 0)
                        {
                            EFormPanelCsatolasError.Visible = true;
                            EFormPanel1.Visible = false;
                            int i = 0;
                            string[] ugyiratKapcsolatok = new string[res.Ds.Tables[0].Rows.Count];
                            foreach (DataRow row in res.Ds.Tables[0].Rows)
                            {
                                if (i == 0)
                                {
                                    labelBadUgyiratok.Text = "<ul style=\"list-style-type:decimal;\">";
                                }
                                else // if (i != 0)
                                {

                                    hfBreakAbleUgyiratok.Value += ";";
                                }


                                string onclick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx"
                                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + row["Ugyirat_Ugyirat_Felepul"].ToString()
                                + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromFelulvizsgalat
                                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                                labelBadUgyiratok.Text += "<li style=\"padding-top:5px;\"><a href='' class=\"badUgyiratok\" onclick=\"" + onclick + "\">" + row["Foszam_Merge"].ToString() + "</a></<li>";
                                hfBreakAbleUgyiratok.Value += row["Id"].ToString();
                                i++;
                            }
                            labelBadUgyiratok.Text += "</ul>";


                        }

                    //szerel�sek ellen�rz�se
                    res = svc.CheckSzerelesek(xpm, GetSelectedUgyiratok());
                    if (string.IsNullOrEmpty(res.ErrorCode))
                        if (res.Ds.Tables[0].Rows.Count > 0)
                        {
                            EFormPanelSzerelesError.Visible = true;
                            EFormPanel1.Visible = false;
                            int i = 0;
                            string[] ugyiratok = new string[res.Ds.Tables[0].Rows.Count];
                            foreach (DataRow row in res.Ds.Tables[0].Rows)
                            {
                                if (i == 0)
                                {
                                    labelBadSzereltUgyiratok.Text = "<ul style=\"list-style-type:decimal;\">";
                                }
                                else // if (i != 0)
                                {

                                    hfBreakAbleSzereltUgyiratok.Value += ";";
                                }



                                string onclick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx"
                                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + row["Id"].ToString()
                                + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromFelulvizsgalat
                                + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
                                labelBadSzereltUgyiratok.Text += "<li style=\"padding-top:5px;\"><a href='' class=\"badUgyiratok\" onclick=\"" + onclick + "\">" + row["Foszam_Merge"].ToString() + "</a></<li>";
                                hfBreakAbleSzereltUgyiratok.Value += row["Id"].ToString();
                                i++;
                            }
                            labelBadSzereltUgyiratok.Text += "</ul>";


                        }
                }
            }

        }
        else
        {
            EFormPanelCsatolasError.Visible = false;
            EFormPanel1.Visible = true;
        }

        //if (Command == CommandName.View || Command == CommandName.Modify)
        //{
        //    String id = Request.QueryString.Get(QueryStringVars.Id);
        //    if (String.IsNullOrEmpty(id))
        //    {
        //        // nincs Id megadva:
        //        ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        //    }
        //    else
        //    {
        //        //KRT_FelulvizsgalatService service = eAdminService.ServiceFactory.GetKRT_FelulvizsgalatService();
        //        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //        //execParam.Record_Id = id;

        //        //Result result = service.Get(execParam);
        //        //if (String.IsNullOrEmpty(result.ErrorCode))
        //        //{
        //        //    KRT_Felulvizsgalat krt_Felulvizsgalat = (KRT_Felulvizsgalat)result.Record;
        //        //    LoadComponentsFromBusinessObject(krt_Felulvizsgalat);
        //        //}
        //        //else
        //        //{
        //        //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        //        //}
        //    }

        //    //cbAutogeneratePartner.Checked = false;
        //    //tr_AutoGeneratePartner_CheckBox.Visible = false;
        //    //tr_Jelszo_lejar_ido.Visible = false;
        //    //tr_Jelszo.Visible = false;
        //    //tr_Tipus_dropdown.Visible = false;
        //}
        //else if (Command == CommandName.New)
        //{
        //    string partnerID = Request.QueryString.Get(QueryStringVars.PartnerId);
        //    if (!String.IsNullOrEmpty(partnerID))
        //    {
        //        //PartnerTextBox1.Id_HiddenField = partnerID;
        //        //PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        //        //PartnerTextBox1.Enabled = false;
        //        //tr_AutoGeneratePartner_CheckBox.Visible = false;
        //    }

        //}
        //if (Command == CommandName.View)
        //{
        //    SetViewControls();
        //}
        //if (Command == CommandName.Modify)
        //{
        //    SetModifyControls();
        //}

        //PartnerTextBox1.Type = Constants.FilterType.Partnerek.BelsoSzemely;

        if (!IsPostBack)
        {
            FillDropDownListFelulvizsgalat();
            FelhasznaloCsoportTextBoxFelulvizsgalo.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
            FelhasznaloCsoportTextBoxFelulvizsgalo.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            CalendarControlFelulvizsgalatIdeje.Text = DateTime.Now.ToString();
        }
    }

    private void FillDropDownListFelulvizsgalat()
    {
        Felulvizsgalat.FillDropDownList(ddlistFelulvizsgalatEredmenye, StartUp, isTuk, false);
        if (FromPeldanyok && isTuk)
        {
            Felulvizsgalat.FillDropDownList(ddlistMinositesFelulvizsgalatEredmenye, StartUp, isTuk, true);
            KodtarakDropDownListMinositesiSzint.FillDropDownList(kcs_IRAT_MINOSITES, true, FormHeader1.ErrorPanel);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        //FormHeader1.HeaderTitle = Resources.Form.FelulvizsgalatFormHeaderTitle;

        // Munkahely kiv�laszt�sn�l sz�r�s a szervezetre:
        ////PartnerTextBoxMunkahely.SetFilterType_Szervezet();

        ////calendarJelszoLejar.Text = DefaultDates.EndDate;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        FormFooter1.Command = CommandName.New;

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel1.Update();
        }

        if (FromPeldanyok)
        {
            if (!IsPostBack)
            {
                FillIratPeldanyGridView();
            }

            FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                + "if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; }";
        }

    }

    // business object --> form
    //private void LoadComponentsFromBusinessObject(KRT_Felulvizsgalat krt_Felulvizsgalat)
    //{
    //    requiredTextBoxUserNev.Text = krt_Felulvizsgalat.UserNev;
    //    requiredTextBoxNev.Text = krt_Felulvizsgalat.Nev;

    //    PartnerTextBox1.Id_HiddenField = krt_Felulvizsgalat.Partner_id;
    //    PartnerTextBox1.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

    //    PartnerTextBoxMunkahely.Id_HiddenField = krt_Felulvizsgalat.Partner_Id_Munkahely;
    //    PartnerTextBoxMunkahely.SetPartnerTextBoxById(FormHeader1.ErrorPanel);

    //    textEmail.Text = krt_Felulvizsgalat.EMail;

    //    try
    //    {
    //        ddownAuthType.SelectedValue = krt_Felulvizsgalat.Tipus;
    //    }
    //    catch (System.ArgumentOutOfRangeException)
    //    {
    //        ddownAuthType.ClearSelection();
    //    }

    //    calendarJelszoLejar.Visible = false;
    //    requiredTextBoxJelszo.Visible = false;

    //    this.SetRadioList(krt_Felulvizsgalat.Engedelyezett);

    //    //cbSystem.Checked = (krt_Felulvizsgalat.System == "1") ? true : false;

    //    ErvenyessegCalendarControl1.ErvKezd = krt_Felulvizsgalat.ErvKezd;
    //    ErvenyessegCalendarControl1.ErvVege = krt_Felulvizsgalat.ErvVege;

    //    FormHeader1.Record_Ver = krt_Felulvizsgalat.Base.Ver;

    //    FormPart_CreatedModified1.SetComponentValues(krt_Felulvizsgalat.Base);

    //}

    // form --> business object
    //private KRT_Felulvizsgalat GetBusinessObjectFromComponents()
    //{
    //    KRT_Felulvizsgalat krt_Felulvizsgalat = new KRT_Felulvizsgalat();
    //    krt_Felulvizsgalat.Updated.SetValueAll(false);
    //    krt_Felulvizsgalat.Base.Updated.SetValueAll(false);

    //    krt_Felulvizsgalat.UserNev = requiredTextBoxUserNev.Text;
    //    krt_Felulvizsgalat.Updated.UserNev = pageView.GetUpdatedByView(requiredTextBoxUserNev);

    //    krt_Felulvizsgalat.Nev = requiredTextBoxNev.Text;
    //    krt_Felulvizsgalat.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxNev);

    //    if (cbAutogeneratePartner.Checked == false)
    //    {
    //        krt_Felulvizsgalat.Partner_id = PartnerTextBox1.Id_HiddenField;
    //        krt_Felulvizsgalat.Updated.Partner_id = pageView.GetUpdatedByView(PartnerTextBox1);
    //    }

    //    krt_Felulvizsgalat.Partner_Id_Munkahely = PartnerTextBoxMunkahely.Id_HiddenField;
    //    krt_Felulvizsgalat.Updated.Partner_Id_Munkahely = pageView.GetUpdatedByView(PartnerTextBoxMunkahely);

    //    krt_Felulvizsgalat.EMail = textEmail.Text;
    //    krt_Felulvizsgalat.Updated.EMail = pageView.GetUpdatedByView(textEmail);

    //    krt_Felulvizsgalat.Tipus = ddownAuthType.SelectedValue;
    //    krt_Felulvizsgalat.Updated.Tipus = pageView.GetUpdatedByView(ddownAuthType);

    //    if (!String.IsNullOrEmpty(calendarJelszoLejar.Text))
    //    {
    //        krt_Felulvizsgalat.JelszoLejaratIdo = calendarJelszoLejar.Text;
    //        krt_Felulvizsgalat.Updated.JelszoLejaratIdo = pageView.GetUpdatedByView(calendarJelszoLejar);
    //    }

    //    krt_Felulvizsgalat.Jelszo = requiredTextBoxJelszo.Text;
    //    krt_Felulvizsgalat.Updated.Jelszo = pageView.GetUpdatedByView(requiredTextBoxJelszo);

    //    if (rbListEngedelyzett.SelectedIndex > -1)
    //    {
    //        krt_Felulvizsgalat.Engedelyezett = rbListEngedelyzett.SelectedValue;
    //        krt_Felulvizsgalat.Updated.Engedelyezett = pageView.GetUpdatedByView(rbListEngedelyzett);
    //    }

    //    //krt_Felulvizsgalat.System = cbSystem.Checked ? "1" : "0";
    //    //krt_Felulvizsgalat.Updated.System = pageView.GetUpdatedByView(cbSystem);

    //    krt_Felulvizsgalat.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
    //    krt_Felulvizsgalat.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

    //    krt_Felulvizsgalat.ErvVege = ErvenyessegCalendarControl1.ErvVege;
    //    krt_Felulvizsgalat.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

    //    krt_Felulvizsgalat.Base.Ver = FormHeader1.Record_Ver;
    //    krt_Felulvizsgalat.Base.Updated.Ver = true;

    //    return krt_Felulvizsgalat;
    //}

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);
            //compSelector.Add_ComponentOnClick(requiredTextBoxNev);
            //compSelector.Add_ComponentOnClick(PartnerTextBox1);
            //compSelector.Add_ComponentOnClick(cbAutogeneratePartner);
            //compSelector.Add_ComponentOnClick(PartnerTextBoxMunkahely);
            //compSelector.Add_ComponentOnClick(ddownMaxMinosites);
            //compSelector.Add_ComponentOnClick(textEmail);
            //compSelector.Add_ComponentOnClick(ddownAuthType);
            //compSelector.Add_ComponentOnClick(calendarJelszoLejar);
            //compSelector.Add_ComponentOnClick(requiredTextBoxJelszo);
            //compSelector.Add_ComponentOnClick(rbListEngedelyzett);
            //compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            //compSelector.Add_ComponentOnClick(cbSystem);

            FormFooter1.SaveEnabled = false;

            //requiredTextBoxJelszo.Enabled = true;
            //AutoGeneratePartner_CheckBox.Attributes["onclick"] = "";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //C�m be�ll�t�sa
        FormHeader1.DisableModeLabel = true;
        FormHeader1.HeaderTitle = "Fel�lvizsg�lat";
        if (StartUp == Constants.Startup.FromSelejtezes)
        {
            FormHeader1.HeaderTitle = Resources.Form.SelejtezesFelulvizsgalatFormHeaderTitle;
        }
        else if (StartUp == Constants.Startup.FromLeveltarbaAdas)
        {
            FormHeader1.HeaderTitle = Resources.Form.LeveltarbaAdasFelulvizsgalatFormHeaderTitle;
        }
        else if (StartUp == Constants.Startup.FromEgyebSzervezetnekAtadas)
        {
            FormHeader1.HeaderTitle = Resources.Form.EgyebSzervezetnekAtadasFelulvizsgalatFormHeaderTitle;
        }

        tr_MinositesFelulvizsgalatEredmenye.Visible = false;
        tr_modositasErvenyessegKezdete.Visible = false;
        tr_ujMinositesiSzint.Visible = false;
        tr_ujMinositesErvenyessegiIdo.Visible = false;
        tr_minositoNeve.Visible = false;
        tr_megszuntetoHatarozat.Visible = false;
        tr_MinositesFelulvizsgalatBizTag.Visible = false;

        if (FromPeldanyok)
        {
            if (isTuk && ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.MinositesFelulvizsgalata.Value)
            {
                tr_MinositesFelulvizsgalatEredmenye.Visible = true;
                tr_modositasErvenyessegKezdete.Visible = true;
                tr_ujMinositesiSzint.Visible = true;
                tr_ujMinositesErvenyessegiIdo.Visible = true;
                tr_minositoNeve.Visible = true;
                tr_megszuntetoHatarozat.Visible = true;
                tr_MinositesFelulvizsgalatBizTag.Visible = true;
                tr_Jegyzek.Visible = false;
            }
            else
            {
                tr_Jegyzek.Visible = true;
            }
            tr_UjVizsgalat.Visible = false;
            tr_FelulvizsgalatEredmenye.Visible = true;
            tr_KovetkezoVizsgalat.Visible = false;
            IratPeldanyokListPanel.Visible = true;
            int cntKijeloltIratpeldanyok = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count;
            labelTetelekSzamaDb.Text = cntKijeloltIratpeldanyok.ToString();
        }
        else
        {
            if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.LeveltarbaAdas.Value || ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Selejtezes.Value
            || ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.EgyebSzervezetnekAtadas.Value || ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Felulvizsgalando.Value)
            {
                tr_KovetkezoVizsgalat.Visible = false;
                tr_UjVizsgalat.Visible = false;
                tr_Jegyzek.Visible = true;
            }
            else if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Meghosszabitas.Value)
            {
                tr_KovetkezoVizsgalat.Visible = false;
                tr_UjVizsgalat.Visible = true;
                tr_Jegyzek.Visible = false;
            }

            if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Felulvizsgalando.Value)
                tr_IrattariTetel.Visible = true;
            else
                tr_IrattariTetel.Visible = false;
        }

        if (isTuk && StartUp == Constants.Startup.FromIratPeldany && ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Selejtezes.Value)
        {
            tr_IrattariTetel.Visible = false;
            tr_Megyjegyzes.Visible = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Felulvizsgalat" + Command))
            {

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                Result result = null;
                #region BLG1722 - min�s�t�s fel�lvizsg�lata
                //Egyel�re iratp�ld�nyokhoz van bek�tve
                if (isTuk && FromPeldanyok && ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.MinositesFelulvizsgalata.Value)
                {
                    string[] peldanyIds = GetSelectedPeldanyok();
                    foreach (string iratPldId in peldanyIds)
                    {
                        EREC_PldIratPeldanyokService iratPldService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                        ExecParam execParamPld = execParam.Clone();
                        execParamPld.Record_Id = iratPldId;

                        Result pldResult = iratPldService.Get(execParamPld);
                        if (pldResult.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, pldResult);
                            ErrorUpdatePanel1.Update();
                            return;
                        }
                        EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)pldResult.Record;

                        EREC_IraIratokService iratService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                        ExecParam execParamIrat = execParam.Clone();
                        execParamIrat.Record_Id = iratPeldany.IraIrat_Id;

                        Result iratResult = iratService.Get(execParamIrat);
                        if (iratResult.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, iratResult);
                            ErrorUpdatePanel1.Update();
                            return;
                        }
                        EREC_IraIratok irat = (EREC_IraIratok)iratResult.Record;
                        string updatedIratVer = irat.Base.Ver;

                        //bej�v� eset�n a k�ldem�nyn�l ,kimen� eset�n az iraton t�lt�nk
                        if (irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
                        {
                            EREC_KuldKuldemenyekService kuldemenyService = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                            ExecParam execParamKuld = execParam.Clone();
                            execParamKuld.Record_Id = irat.KuldKuldemenyek_Id;

                            Result kuldemenyResult = kuldemenyService.Get(execParamKuld);
                            if (kuldemenyResult.IsError)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kuldemenyResult);
                                ErrorUpdatePanel1.Update();
                                return;
                            }

                            EREC_KuldKuldemenyek kuldemeny = (EREC_KuldKuldemenyek)kuldemenyResult.Record;
                            string updatedKuldemenyVer = kuldemeny.Base.Ver;

                            kuldemeny.Updated.SetValueAll(false);
                            kuldemeny.Base.Updated.SetValueAll(false);

                            kuldemeny.Base.Ver = updatedKuldemenyVer;
                            kuldemeny.Base.Updated.Ver = true;

                            kuldemeny.Minosites = KodtarakDropDownListMinositesiSzint.SelectedValue;
                            kuldemeny.Updated.Minosites = true;

                            if (!String.IsNullOrEmpty(ccUjErvenyessegiIdo.Text))
                            {
                                kuldemeny.MinositesErvenyessegiIdeje = ccUjErvenyessegiIdo.Text;
                                kuldemeny.Updated.MinositesErvenyessegiIdeje = true;
                            }

                            kuldemeny.Minosito = PartnerTextBox_Minosito.Id_HiddenField;
                            kuldemeny.Updated.Minosito = true;

                            //uj mez�k t�lt�se
                            if (!String.IsNullOrEmpty(ccModositasErvenyessegKezdete.Text))
                            {
                                kuldemeny.ModositasErvenyessegKezdete = ccModositasErvenyessegKezdete.Text;
                                kuldemeny.Updated.ModositasErvenyessegKezdete = true;
                            }

                            kuldemeny.MegszuntetoHatarozat = tbMegszuntetoHatarozat.Text;
                            kuldemeny.Updated.MegszuntetoHatarozat = true;

                            kuldemeny.FelulvizsgalatDatuma = DateTime.Now.ToString();
                            kuldemeny.Updated.FelulvizsgalatDatuma = true;

                            kuldemeny.Felulvizsgalo_id = execParam.Felhasznalo_Id;
                            kuldemeny.Updated.Felulvizsgalo_id = true;

                            kuldemeny.MinositesFelulvizsgalatEredm = ddlistMinositesFelulvizsgalatEredmenye.SelectedValue;
                            kuldemeny.Updated.MinositesFelulvizsgalatEredm = true;

                            kuldemeny.MinositesFelulvizsgalatBizTag = MinositesFelulvizsgalatBizTag.GetSelectedText();
                            kuldemeny.Updated.MinositesFelulvizsgalatBizTag = true;

                            Result kuldemenyUpdate = kuldemenyService.Update(execParamKuld, kuldemeny);

                            if (kuldemenyUpdate.IsError)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kuldemenyUpdate);
                                ErrorUpdatePanel1.Update();
                                return;
                            }

                        }

                        //az iraton mindenk�pp ment�nk!
                        //else
                        //{

                        irat.Updated.SetValueAll(false);
                        irat.Base.Updated.SetValueAll(false);

                        irat.Base.Ver = updatedIratVer;
                        irat.Base.Updated.Ver = true;

                        irat.Minosites = KodtarakDropDownListMinositesiSzint.SelectedValue;
                        irat.Updated.Minosites = true;

                        if (!String.IsNullOrEmpty(ccUjErvenyessegiIdo.Text))
                        {
                            irat.MinositesErvenyessegiIdeje = ccUjErvenyessegiIdo.Text;
                            irat.Updated.MinositesErvenyessegiIdeje = true;
                        }

                        irat.Minosito = PartnerTextBox_Minosito.Id_HiddenField;
                        irat.Updated.Minosito = true;

                        //uj mez�k t�lt�se
                        if (!String.IsNullOrEmpty(ccModositasErvenyessegKezdete.Text))
                        {
                            irat.ModositasErvenyessegKezdete = ccModositasErvenyessegKezdete.Text;
                            irat.Updated.ModositasErvenyessegKezdete = true;
                        }

                        irat.MegszuntetoHatarozat = tbMegszuntetoHatarozat.Text;
                        irat.Updated.MegszuntetoHatarozat = true;

                        irat.FelulvizsgalatDatuma = DateTime.Now.ToString();
                        irat.Updated.FelulvizsgalatDatuma = true;

                        irat.Felulvizsgalo_id = execParam.Felhasznalo_Id;
                        irat.Updated.Felulvizsgalo_id = true;

                        irat.MinositesFelulvizsgalatEredm = ddlistMinositesFelulvizsgalatEredmenye.SelectedValue;
                        irat.Updated.MinositesFelulvizsgalatEredm = true;

                        irat.MinositesFelulvizsgalatBizTag = MinositesFelulvizsgalatBizTag.GetSelectedText();
                        irat.Updated.MinositesFelulvizsgalatBizTag = true;

                        Result iratUpdate = iratService.Update(execParamIrat, irat);

                        if (iratUpdate.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, iratUpdate);
                            ErrorUpdatePanel1.Update();
                            return;
                        }
                        else
                        {
                            JavaScripts.RegisterSelectedRecordIdToParent(Page, pldResult.Uid);
                            JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                        }
                        //}
                    }
                    //iratokSearch.ira

                    //T�K fel�lvizsg�lat eset�n ne legyen st�tusz�ll�t�s, a kor�bbi �g menjen else -be
                }
                else
                {
                    #endregion
                    if (String.IsNullOrEmpty(hfBreakAbleUgyiratok.Value) && String.IsNullOrEmpty(hfBreakAbleSzereltUgyiratok.Value)
                        && String.IsNullOrEmpty(hfBreakAbleElokeszitettSzereltUgyiratok.Value))
                    {
                        try
                        {
                            ValidateFields();
                            if (FromPeldanyok)
                            {
                                string[] peldanyIds = GetSelectedPeldanyok();
                                EREC_IraJegyzekTetelekService svcJegyzekTetelek = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                                result = svcJegyzekTetelek.JegyzekreHelyezes_Peldanyok(execParam, peldanyIds, JegyzekekTextBox1.Id_HiddenField, TextBoxMegyjegyzes.Text);
                            }
                            else
                            {
                                string[] ugyiratIds = GetSelectedUgyiratok();
                                if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Meghosszabitas.Value)
                                {
                                    EREC_UgyUgyiratokService svcUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                                    // BLG_2156
                                    //result = svcUgyiratok.Meghosszabitas(execParam, ugyiratIds, RequierdNumberBox_UjorzesIdo.Text, TextBoxMegyjegyzes.Text);
                                    result = svcUgyiratok.Meghosszabitas(execParam, ugyiratIds, RequierdNumberBox_UjorzesIdo.Text, FuggoKodtarakDropDownList_Idoegyseg.SelectedValue, CalendarControlFelulvizsgalatIdeje.SelectedDate, TextBoxMegyjegyzes.Text);

                                }
                                else if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Felulvizsgalando.Value)
                                {
                                    EREC_UgyUgyiratokService svcUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                                    execParam.Record_Id = GetSelectedUgyiratok()[0];
                                    result = svcUgyiratok.UpdateIrattariTetel(execParam, IraIrattariTetelTextBox.Id_HiddenField);
                                }
                                else
                                {
                                    EREC_IraJegyzekTetelekService svcJegyzekTetelek = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();

                                    result = svcJegyzekTetelek.JegyzekreHelyezes(execParam, ugyiratIds, JegyzekekTextBox1.Id_HiddenField, TextBoxMegyjegyzes.Text);
                                }
                            }

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {

                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }
                        catch (FormatException fe)
                        {
                            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Form�tum hiba!", fe.Message);
                            ErrorUpdatePanel1.Update();
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(hfBreakAbleUgyiratok.Value))
                        {
                            string[] ugyiratKapcsolatok = hfBreakAbleUgyiratok.Value.Split(';');
                            EREC_IraJegyzekTetelekService svcJegyzekTetelek = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                            result = svcJegyzekTetelek.BreakUpCsatolasok(execParam, ugyiratKapcsolatok);
                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EFormPanelCsatolasError.Visible = false;
                                EFormPanel1.Visible = true;
                                hfBreakAbleUgyiratok.Value = String.Empty;
                            }
                        }

                        if (!String.IsNullOrEmpty(hfBreakAbleSzereltUgyiratok.Value))
                        {
                            string[] ugyiratok = hfBreakAbleSzereltUgyiratok.Value.Split(';');
                            EREC_IraJegyzekTetelekService svcJegyzekTetelek = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                            result = svcJegyzekTetelek.BreakUpSzerelesek(execParam, ugyiratok);
                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {

                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();

                            }
                            else
                            {
                                hfBreakAbleSzereltUgyiratok.Value = String.Empty;
                                EFormPanelSzerelesError.Visible = false;
                                EFormPanel1.Visible = true;
                            }
                        }

                        if (!String.IsNullOrEmpty(hfBreakAbleElokeszitettSzereltUgyiratok.Value))
                        {
                            string[] ugyiratok = hfBreakAbleElokeszitettSzereltUgyiratok.Value.Split(';');
                            EREC_IraJegyzekTetelekService svcJegyzekTetelek = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                            result = svcJegyzekTetelek.BreakUpElokeszitettSzerelesek(execParam, ugyiratok);
                            if (!String.IsNullOrEmpty(result.ErrorCode))
                            {

                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                hfBreakAbleElokeszitettSzereltUgyiratok.Value = String.Empty;
                                EFormPanelElokeszitettSzerelesError.Visible = false;
                                EFormPanel1.Visible = true;
                            }
                        }
                    }
                }
            }

            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    private bool CheckSelectedUgyirat()
    {
        string[] ugyiratIds = GetSelectedUgyiratok();
        if (ugyiratIds.Length == 1)
        {
            return true;
        }
        else
        {
            if (ugyiratIds.Length > 1)
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "T�bb kiv�lasztott �gyirat", "Egyszerre csak egy �gyirat iratt�ri t�telsz�ma m�dos�that�!");
            else
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Nincs kiv�lasztott �gyirat", "Az iratt�ri t�telsz�ma m�dos�t�s�hoz ki kell jel�lni egy �gyiratot!");
            ErrorUpdatePanel1.Update();
            FormFooter1.SaveEnabled = false;
            return false;
        }
    }

    private void LoadIraIrattariTetel()
    {
        if (!CheckSelectedUgyirat())
            return;

        string[] ugyiratIds = GetSelectedUgyiratok();
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = ugyiratIds[0];
        Result result = service.Get(execParam);
        if (string.IsNullOrEmpty(result.ErrorCode))
        {
            EREC_UgyUgyiratok erec_UgyUgyiratok = result.Record as EREC_UgyUgyiratok;

            IraIrattariTetelTextBox.ErvenyessegDatum = erec_UgyUgyiratok.Base.LetrehozasIdo;
            IraIrattariTetelTextBox.IraIktatokonyv_Id = erec_UgyUgyiratok.IraIktatokonyv_Id;
            IraIrattariTetelTextBox.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
            IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxWithExtensionsById(EErrorPanel1);
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }
    }

    private void ValidateFields()
    {
        //if (ddlistFelulvizsgalatEredmenye.SelectedValue == Felulvizsgalat.Meghosszabitas.Value)
        //{
        //    DateTime dtFelulvizsgalat;
        //    if (DateTime.TryParse(CalendarControlKovetkezoVizsgalat.Text, out dtFelulvizsgalat))
        //    {
        //        if (DateTime.Now > dtFelulvizsgalat)
        //        {
        //            throw new FormatException("A k�vetkez� fel�lvizsg�lat ideje nem lehet kor�bbi a jelenlegi d�tumn�l!");
        //        }
        //    }
        //    else
        //   {
        //        throw new FormatException("A k�vetkez� fel�lvizsg�lat idej�nek form�tuma nem megfelel�!");
        //    }
        //}
    }

    private string SelectedUgyiratokSessionKey
    {
        get
        {
            return Request.QueryString.Get(QueryStringVars.SelectedUgyiratokSessionKey);
        }
    }

    private string SelectedUgyiratokQueryString
    {
        get
        {
            return Request.QueryString.Get(QueryStringVars.UgyiratId);
        }
    }

    private string[] GetSelectedUgyiratok()
    {
        string ids = this.SelectedUgyiratokQueryString;

        if (String.IsNullOrEmpty(ids))
        {
            if (!String.IsNullOrEmpty(SelectedUgyiratokSessionKey) && Session[SelectedUgyiratokSessionKey] != null)
            {
                ids = Session[SelectedUgyiratokSessionKey].ToString();
            }
        }

        if (String.IsNullOrEmpty(ids) || String.IsNullOrEmpty(ids.Trim()))
        {
            return new string[] { };
        }

        string[] ugyiratIds = ids.Split(',');
        return ugyiratIds;
    }

    private string[] GetSelectedPeldanyok()
    {
        return ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).ToArray();
    }

    private void registerJavascripts()
    {

        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private string[] IratPeldanyokArray
    {
        get
        {
            string ids = Request.QueryString.Get(QueryStringVars.IratPeldanyId);
            return ids.Split(',');
        }
    }

    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adhat� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atadasrakijelolhetok = UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check");

        int count_atadasraNEMkijelolhetok = IratPeldanyokArray.Length - count_atadasrakijelolhetok;

        if (count_atadasraNEMkijelolhetok > 0)
        {
            Label_Warning_IratPeldany.Text = String.Format("A kiv�lasztott iratp�ld�nyok k�z�l {0} darab nem helyezhet� jegyz�kre.", count_atadasraNEMkijelolhetok);
            //Label_Warning_IratPeldany.Visible = true;
            Panel_Warning_IratPeldany.Visible = true;
        }
    }

    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            search.Id.In(IratPeldanyokArray);

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        IratPeldanyokGridView_RowDataBound_CheckJegyzekreHelyezheto(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
        }

    }
    public static void IratPeldanyokGridView_RowDataBound_CheckJegyzekreHelyezheto(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            // St�tuszok �ssze�ll�t�sa a DataRowView-b�l:
            IratPeldanyok.Statusz pld_statusz = IratPeldanyok.GetAllapotByDataRow(drv.Row);

            string Pld_Id = (pld_statusz != null) ? pld_statusz.Id : "";

            // �tad�sra kijel�lhet�s�g ellen�rz�se:     
            ErrorDetails errorDetail;
            bool jegyzekreHelyezheto = IratPeldanyok.JegyzekreHelyezheto(pld_statusz, UI.SetExecParamDefault(page), out errorDetail);
            UI.SetRowCheckboxAndInfo(jegyzekreHelyezheto, e, errorDetail, Constants.ErrorDetails.ObjectTypes.EREC_PldIratPeldanyok, Pld_Id, page);
        }
    }

}