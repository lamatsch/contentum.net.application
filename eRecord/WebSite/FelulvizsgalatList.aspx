<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FelulvizsgalatList.aspx.cs" Inherits="FelulvizsgalatList" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="FelulvizsgalhatoCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <ajaxToolkit:CollapsiblePanelExtender ID="FelulvizsgalhatoCPE" runat="server" TargetControlID="PanelFelulvizsgalgato"
                    CollapsedSize="20" Collapsed="False" ExpandControlID="FelulvizsgalhatoCPEButton"
                    CollapseControlID="FelulvizsgalhatoCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                    AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                    ImageControlID="FelulvizsgalhatoCPEButton" ExpandedSize="0" ExpandedText="Szerepk�r�k list�ja"
                    CollapsedText="Szerepk�r�k list�ja">
                </ajaxToolkit:CollapsiblePanelExtender>
                <asp:Panel ID="PanelFelulvizsgalgato" runat="server">
                    <ajaxToolkit:TabContainer ID="TabContainerMaster" runat="server" OnActiveTabChanged="TabContainerMaster_ActiveTabChanged"
                        AutoPostBack="false" OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                        <!--Selejtezheto-->
                        <ajaxToolkit:TabPanel ID="TabPanelSelejtezheto" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                <asp:Label ID="labelSelejtezheto" runat="server" Text="Selejtezhet� �gyiratok"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelSelejtezheto" runat="server" OnLoad="updatePanelSelejtezheto_Load">
                                    <ContentTemplate>
                                        <!-- scrollozhat�s�g miatt -->
                                        <ajaxToolkit:CollapsiblePanelExtender ID="SelejtezhetoCPE" runat="server" TargetControlID="panelSelejtezheto"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelSelejtezheto" runat="server">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <asp:GridView ID="gridViewSelejtezheto" runat="server" OnRowCommand="gridViewSelejtezheto_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewSelejtezheto_PreRender" AutoGenerateColumns="False" DataKeyNames="Id"
                                                            OnSorting="gridViewSelejtezheto_Sorting" OnRowDataBound="gridViewSelejtezheto_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="Itsz." SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="SzereltImage" AlternateText="Szerel�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/szerelt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatoltImage" AlternateText="Csatol�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Csoportok_FelelosNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="AllapotKodTarak.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="EREC_UgyUgyiratok.Lezarasdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="EREC_UgyUgyiratok.Elintezesdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <%--TODO: ide majd egy kis ikon kell, ami f�l� h�zva megjelenik az �rz� neve--%>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MegorzesiIdoVege" HeaderText="�rz�si id�" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort" CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelJelleg" runat="server"
                                                                            Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                                            ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- �tadhat� -->
                        <ajaxToolkit:TabPanel ID="TabPanelAtadhato" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:Label ID="labelAtadhato" runat="server" Text="Lev�lt�rba adhat� �gyiratok"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelAtadhato" runat="server" OnLoad="updatePanelAtadhato_Load">
                                    <ContentTemplate>
                                        <!-- scrollozhat�s�g miatt -->
                                        <ajaxToolkit:CollapsiblePanelExtender ID="AtadhatoCPE" runat="server" TargetControlID="panelAtadhato"
                                            CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelAtadhato" runat="server">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <asp:GridView ID="gridViewAtadhato" runat="server" OnRowCommand="gridViewAtadhato_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewAtadhato_PreRender" AutoGenerateColumns="False" DataKeyNames="Id"
                                                            OnSorting="gridViewAtadhato_Sorting" OnRowDataBound="gridViewAtadhato_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="Itsz." SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="SzereltImage" AlternateText="Szerel�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/szerelt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatoltImage" AlternateText="Csatol�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Csoportok_FelelosNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="AllapotKodTarak.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="EREC_UgyUgyiratok.Lezarasdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="EREC_UgyUgyiratok.Elintezesdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <%--TODO: ide majd egy kis ikon kell, ami f�l� h�zva megjelenik az �rz� neve--%>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MegorzesiIdoVege" HeaderText="�rz�si id�" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort" CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelJelleg" runat="server"
                                                                            Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                                            ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Egy�b szervezetnek �tad�s -->
                        <ajaxToolkit:TabPanel ID="TabPanelEgyebSzervezetnekAtadhato" runat="server" TabIndex="2">
                            <HeaderTemplate>
                                <asp:Label ID="labelEgyebSzervezetnekAtadhato" runat="server" Text="Egy�b szervezetnek �tadhat� �gyiratok"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelEgyebSzervezetnekAtadhato" runat="server" OnLoad="updatePanelEgyebSzervezetnekAtadhato_Load">
                                    <ContentTemplate>
                                        <!-- scrollozhat�s�g miatt -->
                                        <ajaxToolkit:CollapsiblePanelExtender ID="EgyebSzervezetnekAtadhatoCPE" runat="server"
                                            TargetControlID="panelEgyebSzervezetnekAtadhato" CollapsedSize="20" Collapsed="False"
                                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelEgyebSzervezetnekAtadhato" runat="server">
                                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                        <asp:GridView ID="gridViewEgyebSzervezetnekAtadhato" runat="server" OnRowCommand="gridViewEgyebSzervezetnekAtadhato_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewEgyebSzervezetnekAtadhato_PreRender" AutoGenerateColumns="False"
                                                            DataKeyNames="Id" OnSorting="gridViewEgyebSzervezetnekAtadhato_Sorting" OnRowDataBound="gridViewEgyebSzervezetnekAtadhato_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="Itsz." SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="SzereltImage" AlternateText="Szerel�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/szerelt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatoltImage" AlternateText="Csatol�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Csoportok_FelelosNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="AllapotKodTarak.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="EREC_UgyUgyiratok.Lezarasdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="EREC_UgyUgyiratok.Elintezesdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <%--TODO: ide majd egy kis ikon kell, ami f�l� h�zva megjelenik az �rz� neve--%>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MegorzesiIdoVege" HeaderText="�rz�si id�" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort" CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelJelleg" runat="server"
                                                                            Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                                            ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <!-- Lej�rati id� v�g�n fel�lvizsg�land� -->
                        <ajaxToolkit:TabPanel ID="TabPanelFelulvizsgalando" runat="server" TabIndex="3">
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Lej�rati id� v�g�n fel�lvizsg�land�"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:UpdatePanel ID="updatePanelFelulvizsgalando" runat="server" OnLoad="updatePanelFelulvizsgalando_Load">
                                    <ContentTemplate>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="FelulvizsgalandoCPE" runat="server"
                                            TargetControlID="panelFelulvizsgalando" CollapsedSize="20" Collapsed="False"
                                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" ExpandedSize="0">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="panelFelulvizsgalando" runat="server">
                                            <asp:GridView ID="gridViewFelulvizsgalando" runat="server" OnRowCommand="gridViewFelulvizsgalando_RowCommand"
                                                            CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                            BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                            OnPreRender="gridViewFelulvizsgalando_PreRender" AutoGenerateColumns="False"
                                                            DataKeyNames="Id" OnSorting="gridViewFelulvizsgalando_Sorting" OnRowDataBound="gridViewFelulvizsgalando_RowDataBound">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                            <Columns>
                                                                                                                                <asp:TemplateField>
                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <div class="DisableWrap">
                                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                            &nbsp;
                                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                </asp:CommandField>
                                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="Itsz." SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="SzereltImage" AlternateText="Szerel�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/szerelt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatoltImage" AlternateText="Csatol�s" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                                    HeaderStyle-Width="25px">
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Csoportok_FelelosNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="AllapotKodTarak.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="EREC_UgyUgyiratok.Lezarasdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="EREC_UgyUgyiratok.Elintezesdat">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <%--TODO: ide majd egy kis ikon kell, ami f�l� h�zva megjelenik az �rz� neve--%>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MegorzesiIdoVege" HeaderText="�rz�si id�" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                    <HeaderTemplate>
                                                                        <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort" CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="labelJelleg" runat="server"
                                                                            Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                                            ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                                        </asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <b style="padding-left: 10px">A lista tartalma �res!</b>
                                                            </EmptyDataTemplate>
                                                            <PagerSettings Visible="False" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
