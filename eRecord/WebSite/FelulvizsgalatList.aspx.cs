using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class FelulvizsgalatList : System.Web.UI.Page
{
    UI ui = new UI();

    public const int tabIndexSelejtezheto = 0;
    public const int tabIndexAtadhato = 1;
    public const int tabIndexEgyebSzervezetnekAtadhato = 2;
    public const int tabIndexFelulvizsgalando = 3;

    private const string defaultSortExpression = "EREC_IraIrattariTetelek.IrattariTetelszam ASC, EREC_IraIrattariTetelek.LetrehozasIdo";
    private const string customSessionName = "FelulvizsgalatSearch";

    private const string SelectedUgyiratokSessonKey = "FelulvizsgalatSelectedUgyiratok";   

    private string GetSelectedMasterRecordID()
    {
        if (TabContainerMaster.ActiveTabIndex == tabIndexSelejtezheto)
        {
            return UI.GetGridViewSelectedRecordId(gridViewSelejtezheto);
        }
        else if (TabContainerMaster.ActiveTabIndex == tabIndexAtadhato)
        {
            return UI.GetGridViewSelectedRecordId(gridViewAtadhato);
        }
        else if (TabContainerMaster.ActiveTabIndex == tabIndexEgyebSzervezetnekAtadhato)
        {
            return UI.GetGridViewSelectedRecordId(gridViewEgyebSzervezetnekAtadhato);
        }
        else if (TabContainerMaster.ActiveTabIndex == tabIndexFelulvizsgalando)
        {
            return UI.GetGridViewSelectedRecordId(gridViewFelulvizsgalando);
        }
        else
        {
            // ez volt kor�bban is, meghagyjuk alap�rtelmezettnek:
            return UI.GetGridViewSelectedRecordId(gridViewAtadhato);
        }
    }

    private class ActiveComponents
    {
        public string StartupName { get; set; }
        public GridView GridView { get; set; }
        public UpdatePanel UpdatePanel { get; set; }
    }

    private ActiveComponents GetActiveComponents()
    {
        ActiveComponents activeComponents = new ActiveComponents();

        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            activeComponents.StartupName = Constants.Startup.FromSelejtezes;
            activeComponents.GridView = gridViewSelejtezheto;
            activeComponents.UpdatePanel = updatePanelSelejtezheto;
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            activeComponents.StartupName = Constants.Startup.FromEgyebSzervezetnekAtadas;
            activeComponents.GridView = gridViewEgyebSzervezetnekAtadhato;
            activeComponents.UpdatePanel = updatePanelEgyebSzervezetnekAtadhato;
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelFelulvizsgalando))
        {
            activeComponents.StartupName = Constants.Startup.FromFelulvizsgalando;
            activeComponents.GridView = gridViewFelulvizsgalando;
            activeComponents.UpdatePanel = updatePanelFelulvizsgalando;
        }
        else // ezt meghagytuk default �gnak
        {
            activeComponents.StartupName = Constants.Startup.FromLeveltarbaAdas;
            activeComponents.GridView = gridViewAtadhato;
            activeComponents.UpdatePanel = updatePanelAtadhato;
        }

        return activeComponents;
    }



    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelulvizsgalatList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.FelulvizsgalatHeaderTitle;
        //Sorrend fontos!!! 1. Custom Session Name 2. SearchObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = customSessionName;
        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokSearch);
        ListHeader1.ModifyVisible = false;
        ListHeader1.UgyiratTerkepVisible = true;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.SendObjectsVisible = false;
        //ListHeader1.SearchVisible = false;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        //TabContainer
        JavaScripts.RegisterActiveTabChangedClientScript(Page,TabContainerMaster);
        //Master TabContainer
        ScriptManager1.RegisterAsyncPostBackControl(TabContainerMaster);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(TabContainerMaster.ClientID);

        ListHeader1.FelulvizsgalatVisible = true;
        ListHeader1.FelulvizsgalatOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ActiveComponents activeComponets = this.GetActiveComponents();

        ListHeader1.AttachedGridView = activeComponets.GridView;

        ListHeader1.FelulvizsgalatOnClientClick = JavaScripts.SetOnClientClickFitterOrAttachedConfirm(activeComponets.GridView.ClientID);
        
        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("FelulvizsgalatSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, activeComponets.UpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        /* Breaked Records, email eset�n */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratokSearch(),customSessionName);


        if (!IsPostBack)
        {
            ActiveTabRefreshMaster(TabContainerMaster);
        }

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        var hasFelulvizsgalatList = FunctionRights.GetFunkcioJog(Page, "FelulvizsgalatList");
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = hasFelulvizsgalatList;
        ListHeader1.ModifyEnabled = false;
        ListHeader1.UgyiratTerkepEnabled = hasFelulvizsgalatList;
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.ViewHistory);
        ListHeader1.FelulvizsgalatEnabled = FunctionRights.GetFunkcioJog(Page, "Felulvizsgalat");

        ListHeader1.LockEnabled = false;
        ListHeader1.UnlockEnabled = false;

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = "";
            if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewSelejtezheto);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewSelejtezheto(MasterListSelectedRowId);
            }
            else if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewAtadhato);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewAtadhato(MasterListSelectedRowId);
            }
            else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewEgyebSzervezetnekAtadhato);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewEgyebSzervezetnekAtadhato(MasterListSelectedRowId);
            }
            else if (TabContainerMaster.ActiveTab.Equals(TabPanelFelulvizsgalando))
            {
                MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(gridViewFelulvizsgalando);
                RefreshOnClientClicksByMasterListSelectedRow_gridViewFelulvizsgalando(MasterListSelectedRowId);
            }
        }
    }

    #endregion

    #region Master Tab

    protected void TabContainerMaster_ActiveTabChanged(object sender, EventArgs e)
    {
        ListHeader1.PageIndex = 0;
        ActiveTabRefreshMaster(sender as AjaxControlToolkit.TabContainer);

    }

    private void ActiveTabRefreshMaster(AjaxControlToolkit.TabContainer sender)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case tabIndexSelejtezheto:
                gridViewSelejtezhetoBind();
                break;
            case tabIndexAtadhato:
                gridViewAtadhatoBind();
                break;
            case tabIndexEgyebSzervezetnekAtadhato:
                gridViewEgyebSzervezetnekAtadhatoBind();
                break;
            case tabIndexFelulvizsgalando:
                gridViewFelulvizsgalandoBind();
                break;
        }
    }

    #endregion

    #region Master List

    private void SetGridView(AjaxControlToolkit.TabPanel tabPanel, GridView gridView)
    {
        if (TabContainerMaster.ActiveTab.Equals(tabPanel))
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, SelejtezhetoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, AtadhatoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, EgyebSzervezetnekAtadhatoCPE);
            UI.GridViewSetScrollable(ListHeader1.Scrollable, FelulvizsgalandoCPE);

            ListHeader1.RefreshPagerLabel();

            ui.SetClientScriptToGridViewSelectDeSelectButton(gridView);
        }
    }

    private void RowDataBound(GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetSzerelesInfo(e);
        UI.GridView_RowDataBound_SetCsatolasInfo(e);
        UI.GridView_RowDataBound_SetVegyesInfo(e);
        UI.GridView_RowDataBound_DisableSzereltCheckbox(e);
    }

    private EREC_UgyUgyiratokSearch GetSearch()
    {
        var search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), customSessionName);
        search.TopRow = UI.GetTopRow(Page);
        return search;
    }

    #region Selejtezheto

    protected void gridViewSelejtezhetoBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewSelejtezheto", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewSelejtezheto", ViewState);

        gridViewSelejtezhetoBind(sortExpression, sortDirection);
    }

    protected void gridViewSelejtezhetoBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokSearch search = GetSearch();
        search.OrderBy = Search.GetOrderBy("gridViewSelejtezheto", ViewState, SortExpression, SortDirection);
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllSelejtezheto(ExecParam, search);
        UI.GridViewFill(gridViewSelejtezheto, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void gridViewSelejtezheto_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RowDataBound(e);
    }

    protected void gridViewSelejtezheto_PreRender(object sender, EventArgs e)
    {
        SetGridView(TabPanelSelejtezheto, gridViewSelejtezheto);
    }

    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            gridViewSelejtezhetoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
        {
            gridViewAtadhatoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            gridViewEgyebSzervezetnekAtadhatoBind();
        }
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
        {
            JavaScripts.ResetScroll(Page, SelejtezhetoCPE);
            gridViewSelejtezhetoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
        {
            JavaScripts.ResetScroll(Page, AtadhatoCPE);
            gridViewAtadhatoBind();
        }
        else if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
        {
            JavaScripts.ResetScroll(Page, EgyebSzervezetnekAtadhatoCPE);
            gridViewEgyebSzervezetnekAtadhatoBind();
        }
    }

    protected void gridViewSelejtezheto_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewSelejtezheto, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewSelejtezheto_SelectRowCommand(id);
        }
    }

    private void gridViewSelejtezheto_SelectRowCommand(string cimId)
    {
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id, UpdatePanel updatePanel)
    {
        if (!String.IsNullOrEmpty(id) && updatePanel != null)
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromFelulvizsgalat
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, updatePanel.ClientID);

            // �gyiratt�rk�p View m�dban
            ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromFelulvizsgalat
                + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, updatePanel.ClientID);

            string tableName = "EREC_UgyUgyiratok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, updatePanel.ClientID);
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewSelejtezheto(string id)
    {
        RefreshOnClientClicksByMasterListSelectedRow(id, updatePanelSelejtezheto);
    }

    protected void updatePanelSelejtezheto_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelSelejtezheto))
                    {
                        gridViewSelejtezhetoBind();
                        gridViewSelejtezheto_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewSelejtezheto));
                    }
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                break;
            case CommandName.Unlock:
                break;
            case CommandName.SendObjects:
                break;
            case CommandName.Felulvizsgalat:

                ActiveComponents activeComponets = this.GetActiveComponents();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                
                foreach (string s in ui.GetGridViewSelectedRows(activeComponets.GridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                Session[SelectedUgyiratokSessonKey] = sb.ToString();

                string js = JavaScripts.SetOnClientClickWithTimeout("FelulvizsgalatForm.aspx",
                    QueryStringVars.SelectedUgyiratokSessionKey + "=" + SelectedUgyiratokSessonKey +
                    "&" + Constants.Startup.StartupName + "=" + activeComponets.StartupName
                    , Defaults.PopupWidth, Defaults.PopupHeight, activeComponets.UpdatePanel.ClientID, EventArgumentConst.refreshMasterList, 1).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SelectedUgyiratokFelulvizsgalat", js, true);

                break;
        }
    }

    protected void gridViewSelejtezheto_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewSelejtezhetoBind(e.SortExpression, UI.GetSortToGridView("gridViewSelejtezheto", ViewState, e.SortExpression));
    }

    #endregion

    #region Atadhato

    protected void gridViewAtadhatoBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewAtadhato", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewAtadhato", ViewState);

        gridViewAtadhatoBind(sortExpression, sortDirection);
    }

    protected void gridViewAtadhatoBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), customSessionName);
        
        search.OrderBy = Search.GetOrderBy("gridViewAtadhato", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllLeveltarbaAdhato(ExecParam, search);

        UI.GridViewFill(gridViewAtadhato, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void gridViewAtadhato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RowDataBound(e);
    }

    protected void gridViewAtadhato_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewAtadhato, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewAtadhato_SelectRowCommand(id);
        }
    }

    private void gridViewAtadhato_SelectRowCommand(string cimId)
    {
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewAtadhato(string id)
    {
        RefreshOnClientClicksByMasterListSelectedRow(id, updatePanelAtadhato);
    }

    protected void gridViewAtadhato_PreRender(object sender, EventArgs e)
    {
        SetGridView(TabPanelAtadhato, gridViewAtadhato);
    }

    protected void updatePanelAtadhato_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelAtadhato))
                    {
                        gridViewAtadhatoBind();
                        gridViewAtadhato_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewAtadhato));
                    }
                    break;
            }
        }
    }

    protected void gridViewAtadhato_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewAtadhatoBind(e.SortExpression, UI.GetSortToGridView("gridViewAtadhato", ViewState, e.SortExpression));
    }

    #endregion


    #region EgyebSzervezetnekAtadhato

    protected void gridViewEgyebSzervezetnekAtadhatoBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("gridViewEgyebSzervezetnekAtadhato", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewEgyebSzervezetnekAtadhato", ViewState);

        gridViewEgyebSzervezetnekAtadhatoBind(sortExpression, sortDirection);
    }

    protected void gridViewEgyebSzervezetnekAtadhatoBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();


        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), customSessionName);

        search.OrderBy = Search.GetOrderBy("gridViewEgyebSzervezetnekAtadhato", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllEgyebSzervezetnekAtadhato(ExecParam, search);

        UI.GridViewFill(gridViewEgyebSzervezetnekAtadhato, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void gridViewEgyebSzervezetnekAtadhato_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RowDataBound(e);
    }

    protected void gridViewEgyebSzervezetnekAtadhato_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewEgyebSzervezetnekAtadhato, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            gridViewEgyebSzervezetnekAtadhato_SelectRowCommand(id);
        }
    }

    private void gridViewEgyebSzervezetnekAtadhato_SelectRowCommand(string cimId)
    {
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewEgyebSzervezetnekAtadhato(string id)
    {
        RefreshOnClientClicksByMasterListSelectedRow(id, updatePanelEgyebSzervezetnekAtadhato);
    }

    protected void gridViewEgyebSzervezetnekAtadhato_PreRender(object sender, EventArgs e)
    {
        SetGridView(TabPanelEgyebSzervezetnekAtadhato, gridViewEgyebSzervezetnekAtadhato);
    }

    protected void updatePanelEgyebSzervezetnekAtadhato_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelEgyebSzervezetnekAtadhato))
                    {
                        gridViewEgyebSzervezetnekAtadhatoBind();
                        gridViewEgyebSzervezetnekAtadhato_SelectRowCommand(UI.GetGridViewSelectedRecordId(gridViewEgyebSzervezetnekAtadhato));
                    }
                    break;
            }
        }
    }

    protected void gridViewEgyebSzervezetnekAtadhato_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewEgyebSzervezetnekAtadhatoBind(e.SortExpression, UI.GetSortToGridView("gridViewEgyebSzervezetnekAtadhato", ViewState, e.SortExpression));
    }

    #endregion EgyebSzervezetnekAtadhato

    #region Felulvizsgalando

    protected void updatePanelFelulvizsgalando_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    if (TabContainerMaster.ActiveTab.Equals(TabPanelFelulvizsgalando))
                    {
                        gridViewFelulvizsgalandoBind();
                    }
                    break;
            }
        }
    }

    protected void gridViewFelulvizsgalando_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(gridViewFelulvizsgalando, selectedRowNumber, "check");
        }
    }

    protected void gridViewFelulvizsgalando_PreRender(object sender, EventArgs e)
    {
        SetGridView(TabPanelFelulvizsgalando, gridViewFelulvizsgalando);
    }

    protected void gridViewFelulvizsgalando_Sorting(object sender, GridViewSortEventArgs e)
    {
        gridViewFelulvizsgalandoBind(e.SortExpression, UI.GetSortToGridView("gridViewFelulvizsgalando", ViewState, e.SortExpression));
    }

    protected void gridViewFelulvizsgalando_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        RowDataBound(e);
    }

    private void gridViewFelulvizsgalandoBind()
    {
        string sortExpression = Search.GetSortExpressionFromViewState("gridViewFelulvizsgalando", ViewState, defaultSortExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("gridViewFelulvizsgalando", ViewState);

        gridViewFelulvizsgalandoBind(sortExpression, sortDirection);
    }

    private void gridViewFelulvizsgalandoBind(string sortExpression, SortDirection sortDirection)
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_UgyUgyiratokSearch search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true), customSessionName);

        search.OrderBy = Search.GetOrderBy("gridViewFelulvizsgalando", ViewState, sortExpression, sortDirection);
        search.TopRow = UI.GetTopRow(Page);

        UI.SetPaging(ExecParam, ListHeader1);

        Result res = service.GetAllFelulvizsgalando(ExecParam, search);

        UI.GridViewFill(gridViewFelulvizsgalando, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    private void RefreshOnClientClicksByMasterListSelectedRow_gridViewFelulvizsgalando(string id)
    {
        RefreshOnClientClicksByMasterListSelectedRow(id, updatePanelFelulvizsgalando);
    }

    #endregion Felulvizsgalando

    protected void GetJelleg(object oJelleg, out string JellegLabel, out string JellegTooltip)
    {
        string jelleg = (oJelleg ?? String.Empty) as string;

        switch (jelleg)
        {
            case KodTarak.UGYIRAT_JELLEG.Elektronikus:
                JellegLabel = "E";
                JellegTooltip = "Elektronikus";
                break;
            case KodTarak.UGYIRAT_JELLEG.Papir:
                JellegLabel = "P";
                JellegTooltip = "Pap�r";
                break;
            case KodTarak.UGYIRAT_JELLEG.Vegyes:
                JellegLabel = "V";
                JellegTooltip = "Vegyes";
                break;
            default:
                goto case KodTarak.UGYIRAT_JELLEG.Papir;
        }
    }

    protected string GetJellegLabel(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return label;
    }

    protected string GetJellegToolTip(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return tooltip;
    }

    #endregion
}
