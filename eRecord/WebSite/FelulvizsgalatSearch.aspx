<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FelulvizsgalatSearch.aspx.cs" Inherits="FelulvizsgalatSearch" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc15" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc13" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label33" runat="server" CssClass="mrUrlapCaption" Text="�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:EvIntervallum_SearchFormControl ID="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label17" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc15:IraIktatoKonyvekDropDownList ID="UgyDarab_IraIktatoKonyvek_DropDownList" runat="server"
                                    Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    Filter_IdeIktathat="false" IsMultiSearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label14" runat="server" Text="F�sz�m:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc14:szamintervallum_searchformcontrol id="UgyDarab_Foszam_SzamIntervallum_SearchFormControl"
                                    runat="server"></uc14:szamintervallum_searchformcontrol>
                                &nbsp;</td>                            
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc3:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" Validate="false" SearchMode="true"/>
                            </td>
                        </tr>
                        <!-- BUG 6520 -->
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:CheckBox ID="CheckBox_SzereltIratok" runat="server" Text="Szerelt iratok megjelen�t�se" />
                            </td>
                        </tr>

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label27" runat="server" Text="Meg�rz�si id� v�ge:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                 <uc13:DatumIntervallum_SearchCalendarControl ID="Ugy_MegorzesiIdoVege_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Iktat�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                 <uc13:DatumIntervallum_SearchCalendarControl ID="IktasDatuma_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Lez�r�s d�tuma:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                 <uc13:DatumIntervallum_SearchCalendarControl ID="LezarasDatuma_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>

                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc4:CsoportTextBox ID="Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                   <uc2:SearchFooter ID="SearchFooter1" runat="server" />                   
                </td>
            </tr>
        </table>
</asp:Content>

