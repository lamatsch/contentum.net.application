﻿using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;

public partial class FelulvizsgalatSearch : Contentum.eUtility.UI.PageBase
{
    Type _type = typeof(EREC_UgyUgyiratokSearch);
    private const string customSessionName = "FelulvizsgalatSearch";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.FelulvizsgalatSearchHeaderTitle;
        SearchHeader1.CustomTemplateTipusNev = customSessionName;
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {

            EREC_UgyUgyiratokSearch searchObject = null;
            if (Search.IsSearchObjectInSession_CustomSessionName(Page, customSessionName))
            {
                searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true),customSessionName);
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);

            Ugy_MegorzesiIdoVege_DatumIntervallum_SearchCalendarControl.OnlyPastEnabled = true;
        }
    }

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = null;
        if (searchObject != null) erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)searchObject;

        if (erec_UgyUgyiratokSearch != null)
        {
            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

            /// Iktatokonyvek megkülönböztetõ jelzés alapján való feltöltése:
            /// Nem szûrünk a szervezet által láthatókra, hozzuk az összeset (2008.11.10)
            UgyDarab_IraIktatoKonyvek_DropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);


            UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

            IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);

            // BUG 6520 - a szerelt iratok megjelenjenek-e
            CheckBox_SzereltIratok.Checked = !String.IsNullOrEmpty(erec_UgyUgyiratokSearch.Manual_Szereltek.Value);

            // õrzési idõ vége
            Ugy_MegorzesiIdoVege_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.MegorzesiIdoVege);

            // BLG_8686
            IktasDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.Manual_LetrehozasIdo);
            LezarasDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_UgyUgyiratokSearch.LezarasDat);
            Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value;
            Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
        }
    }

    private EREC_UgyUgyiratokSearch SetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)SearchHeader1.TemplateObject;
        if (erec_UgyUgyiratokSearch == null)
        {
            erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch(true);
        }

        if (!String.IsNullOrEmpty(UgyDarab_IraIktatoKonyvek_DropDownList.SelectedValue))
        {
            UgyDarab_IraIktatoKonyvek_DropDownList.SetSearchObject(erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
        }

        Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

        UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

        if (!String.IsNullOrEmpty(IraIrattariTetelTextBox1.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Filter(IraIrattariTetelTextBox1.Id_HiddenField);
        }

        // BUG 6520 - megőrzési idő vége
        Ugy_MegorzesiIdoVege_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_UgyUgyiratokSearch.MegorzesiIdoVege);

        // BUG 6520 - a szerelt iratok megjelenjenek-e
        if (CheckBox_SzereltIratok.Checked)
        {
            erec_UgyUgyiratokSearch.Manual_Szereltek.Filter(KodTarak.UGYIRAT_ALLAPOT.Szerelt);
        }

        // BLG_8686
        IktasDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_UgyUgyiratokSearch.Manual_LetrehozasIdo);
        LezarasDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_UgyUgyiratokSearch.LezarasDat);

        if (!String.IsNullOrEmpty(Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Filter(Ugy_FelhCsopId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField);
        }

        return erec_UgyUgyiratokSearch;
    }



    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_UgyUgyiratokSearch searchObject = SetSearchObjectFromComponents();

            EREC_UgyUgyiratokSearch defaultSearchObject = GetDefaultSearchObject();

            if (Search.IsIdentical(searchObject, defaultSearchObject)
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch)
                )
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession_CustomSessionName(Page, customSessionName);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject_CustomSessionName(Page, searchObject, customSessionName);
            }

            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }

    }

    private EREC_UgyUgyiratokSearch GetDefaultSearchObject()
    {
        return new EREC_UgyUgyiratokSearch(true);
    }
}
