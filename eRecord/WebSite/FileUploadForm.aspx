<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileUploadForm.aspx.cs" Inherits="FileUploadForm" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="cache-control" content="no-store" />
    <title></title>
    <base target="_self" />
</head>
<body>
<script language="JavaScript" type="text/javascript">
function swapByName(obj, newImageName) 
{
    var myobj = document.getElementById(obj); 
    if (myobj) 
    {
        var fileNameStartIndex = myobj.src.lastIndexOf("/");
        var newUrl = myobj.src.substring(0,fileNameStartIndex+1)+newImageName;
        
        myobj.src = newUrl;            
    }
}
</script>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div style="text-align:center">
            <uc1:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:Form,FileUploadFormHeaderTitle %>"/>
        </div>
        <div class="popupBody" style="margin-left:auto;margin-right:auto;margin-top:10px">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <div style="width:90%;margin-top:10px;margin-bottom:10px;margin-left:auto;margin-right:auto">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="width:100%">
                            <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" />
                        </td>
                        <td>
                            <asp:ImageButton ID="btnUpload" runat="server"
								ImageUrl="images/hu/trapezgomb/feltoltes_trap.jpg"
								onmouseover="swapByName(this.id,'feltoltes_trap2.jpg')"
								onmouseout="swapByName(this.id,'feltoltes_trap.jpg')"
								OnClick="btnUpload_Click" AlternateText="Felt�lt�s"
								Visible="True" CausesValidation="false" />
							
                        </td>
                        <td>
                            <asp:ImageButton ID="btnDelete" runat="server"
								ImageUrl="images/hu/trapezgomb/torles_trap.jpg"
								onmouseover="swapByName(this.id,'torles_trap2.jpg')"
								onmouseout="swapByName(this.id,'torles_trap.jpg')"
								OnClick="btnDelete_Click" AlternateText="T�rl�s"
								Visible="True" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
                <div style="width:100%">
                    <asp:UpdatePanel ID="updatePanelFileList" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hfParentParameters" runat="server" />
                            <asp:ListBox ID="listBoxFileNames" runat="server" Rows="15" Width="100%" SelectionMode="Multiple"></asp:ListBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
         </eUI:eFormPanel>   
            <div style="text-align:center;margin-top:10px;margin-left:auto;margin-right:auto">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: center;">
                            <asp:ImageButton ID="ImageSave" runat="server" 
                                ImageUrl="~/images/hu/ovalgomb/rendben.jpg"
                                onmouseover="swapByName(this.id,'rendben2.jpg')" onmouseout="swapByName(this.id,'rendben.jpg')" 
                                OnClick="ImageSave_Click" />         
                        </td> 
                        <td style="text-align: center;">
                            <asp:ImageButton ID="ImageCancel" runat="server" 
                                ImageUrl="~/images/hu/ovalgomb/megsem.jpg" 
                                onmouseover="swapByName(this.id,'megsem2.jpg')" 
                                onmouseout="swapByName(this.id,'megsem.jpg')" 
                                OnClick="ImageCancel_Click"/>         
                        </td>
                    </tr>
                </table> 
            </div>
        </div>
    </form>
</body>
</html>
