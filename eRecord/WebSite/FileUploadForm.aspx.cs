using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eRecord.Utility;
using System.Collections.Generic;

public partial class FileUploadForm : Contentum.eUtility.UI.PageBase
{
    private const string fileUploadErrorHeader = "F�jl felt�lt�si hiba!";
    private const string fileIsUploaded = "Ez a f�jl m�r fel lett t�ltve.";
    private const string noMoreUpload = "Nem lehet t�bb f�jlt felt�lteni.";

    private const string tempDirectory = "c:\\temp\\ContentumNet";
    private const string vsClientFileNames = "clientFilesName";
    private const string vsRandomDirectoryPath = "randomDirectoryPath";
    private const string vsUploadedFiles = "uploadedFiles";
    private const int Mega = 1048576;
    private const int Kilo = 1024;

    private bool SingleMode
    {
        get 
        {
            string mode = Request.Params.Get(QueryStringVars.fileUploadMode);
            if (String.IsNullOrEmpty(mode))
            {
                return false;
            }
            if (mode.Trim() == Constants.FileUploadMode.single.ToString())
            {
                return true;
            }
            return false;
        }
    }

    private string UploadedFiles
    {
        get
        {
            if (ViewState[vsUploadedFiles] == null)
            {
                return String.Empty;
            }
            else
            {
                return ViewState[vsUploadedFiles].ToString().Trim();
            }
        }
        set
        {
            ViewState[vsUploadedFiles] = value.Trim();
        }
    }

    private string convertFileSize(long fileSize)
    {
        if((fileSize/Mega) >0)
        {
            return "(" + (fileSize / Mega) + " Mbyte" + ")";
        }
        if ((fileSize / Kilo) > 0)
        {
            return "(" + (fileSize / Kilo) + " kbyte" + ")";
        }
        return "(" + fileSize + " byte" + ")";
    }

    private string convertFileSize(int fileSize)
    {
        return convertFileSize((long)fileSize);
    }

    private void AddFileNameToViewState(string fileName)
    {
        if (ViewState[vsClientFileNames] == null || ViewState[vsClientFileNames].ToString().Trim() == String.Empty)
        {
            ViewState[vsClientFileNames] = fileName;
        }
        else
        {
            ViewState[vsClientFileNames] += ";" + fileName;
        }
    }

    private void RemoveFileNameFromViewState(string fileName)
    {
        if (ViewState[vsClientFileNames] != null || ViewState[vsClientFileNames].ToString().Trim() != String.Empty)
        {
            string fileNames = ViewState[vsClientFileNames].ToString().Trim();
            int index = fileNames.IndexOf(fileName);
            if (index > -1)
            {
                //els� helyen szerepel:pontos vessz� ut�na t�rl�s
                if (index == 0)
                {
                    ViewState[vsClientFileNames] = fileNames.Remove(index,Math.Min(fileName.Length + 1,fileNames.Length));
                }
                //nem els� helyen: pontos vessz� el�tte t�rl�s
                else
                {
                    ViewState[vsClientFileNames] = fileNames.Remove(index - 1, fileName.Length + 1);
                }
               
            }
        }
    }
    private void DeleteMarkedFiles()
    {
        if (ViewState[vsRandomDirectoryPath] != null && ViewState[vsRandomDirectoryPath].ToString().Trim() != String.Empty)
        {
            string randomDirectory = ViewState[vsRandomDirectoryPath].ToString();
            DirectoryInfo di = new DirectoryInfo(randomDirectory);
            if (di.Exists)
            {
                FileInfo[] fis = di.GetFiles("*.deleted");
                foreach (FileInfo fi in fis)
                {
                    if (fi.Exists)
                        fi.Delete();
                }
            }

        }
    }

    private void BackUpMarkedFiles()
    {
        if (ViewState[vsRandomDirectoryPath] != null && ViewState[vsRandomDirectoryPath].ToString().Trim() != String.Empty)
        {
            string randomDirectory = ViewState[vsRandomDirectoryPath].ToString();
            DirectoryInfo di = new DirectoryInfo(randomDirectory);
            if (di.Exists)
            {
                FileInfo[] fis = di.GetFiles("*.deleted");
                foreach (FileInfo fi in fis)
                {
                    if (fi.Exists)
                    {
                        string oldName = GetFilePath(Path.GetFileNameWithoutExtension(fi.Name));
                        FileInfo oldFi = new FileInfo(oldName);
                        if (oldFi.Exists)
                        {
                            oldFi.Delete();
                        }
                        fi.MoveTo(oldName);
                    }
                }
            }

        }
    }

    private void AddFileNameToListBox(string fileName)
    {
        string filePath = GetFilePath(fileName);
        FileInfo fi = new FileInfo(filePath);
        if (fi.Exists)
        {
            string fileLength = convertFileSize(fi.Length);
            string fileNameAndLength = fileName + "  " + fileLength;
            listBoxFileNames.Items.Add(new ListItem(fileNameAndLength, fileName));
        }
        else
        {
            RemoveFileNameFromViewState(fileName);
        }
    }
    private string GetFilePath(string fileName)
    {
        fileName = Path.GetFileName(fileName);
        string randomDirectoryPath = String.Empty;
        if (ViewState[vsRandomDirectoryPath] == null || ViewState[vsRandomDirectoryPath].ToString().Trim() == String.Empty)
        {
            DirectoryInfo diRandomDirectory;
            do
            {
                string randomDirectoryName = Path.GetFileNameWithoutExtension(Path.GetRandomFileName());
                randomDirectoryPath = Path.Combine(tempDirectory, randomDirectoryName);
                diRandomDirectory = new DirectoryInfo(randomDirectoryPath);
            }
            while (diRandomDirectory.Exists);
            diRandomDirectory.Create();
            ViewState[vsRandomDirectoryPath] = randomDirectoryPath;
        }
        else
        {
            randomDirectoryPath = ViewState[vsRandomDirectoryPath].ToString();
            DirectoryInfo diRandomDirectory = new DirectoryInfo(randomDirectoryPath);
            if (!diRandomDirectory.Exists)
                diRandomDirectory.Create();
        }

        string filePath = Path.Combine(randomDirectoryPath, fileName);
        return filePath;
    }

    private void SaveFile(string filePath,ref byte[] buffer)
    {
        FileFunctions.Upload.SaveFile(filePath, ref buffer);
    }

    private void setSingleView()
    {
        Title = Resources.Form.FileUploadFormHeaderTitle_single;
        LovListHeader1.HeaderTitle = Resources.Form.FileUploadFormHeaderTitle_single;
        listBoxFileNames.Rows = 1;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = Resources.Form.FileUploadFormHeaderTitle;
        ScriptManager1.RegisterAsyncPostBackControl(ImageCancel);
        ScriptManager1.RegisterAsyncPostBackControl(btnDelete);
        if (LovListHeader1.ErrorPanel.Visible)
        {
            LovListHeader1.ErrorPanel.Visible = false;
            LovListHeader1.ErrorUpdatePanel.Update();
        }

        if (!IsPostBack)
        {
            if (SingleMode)
            {
                btnUpload.OnClientClick = "if($get('" + FileUpload1.ClientID + @"').value == '')
                                       {alert('" + Resources.Error.UINoSelectedItem + "');return false;} else{" +
                                       "if($get('" + listBoxFileNames.ClientID + "').options.length != 0){alert('" + noMoreUpload + "');return false;}}";
            }
            else
            {
                btnUpload.OnClientClick = "if($get('" + FileUpload1.ClientID + @"').value == '')
                                       {alert('" + Resources.Error.UINoSelectedItem + "');return false;}";
            }

            btnDelete.OnClientClick = "if($get('" + listBoxFileNames.ClientID + @"').selectedIndex < 0)
                                       {alert('" + Resources.Error.UINoSelectedItem + "');return false;}";

            //a nyit� oldalr�l �rkez� param�tereket kiolvas� script regisztr�l�sa
            string hiddenFieldID = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
            if (!String.IsNullOrEmpty(hiddenFieldID))
            {
                Dictionary<string, string> ParentChildControl = new Dictionary<string, string>(1);
                ParentChildControl.Add(hiddenFieldID,hfParentParameters.ClientID);
                JavaScripts.RegisterGetValuesFromParentWindowClientScript(Page, ParentChildControl,updatePanelFileList.ClientID,EventArgumentConst.refreshValuesFromParent);

            }
        }

        // a m�r felt�lt�tt f�jlok kilist�z�sa
        string eventArgument = Request.Params.Get("__EVENTARGUMENT");
        if (!String.IsNullOrEmpty(eventArgument))
        {
            if (eventArgument == EventArgumentConst.refreshValuesFromParent)
            {
                if (hfParentParameters.Value.Trim() != String.Empty)
                {
                    string uploadedFiles = hfParentParameters.Value;
                    hfParentParameters.Value = String.Empty;
                    string[] temp = uploadedFiles.Split(';');
                    if (temp.Length > 0)
                    {
                        ViewState[vsRandomDirectoryPath] = temp[0];
                        if (temp.Length > 1)
                        {
                            if (SingleMode)
                            {
                                UploadedFiles = temp[1];
                                ViewState[vsClientFileNames] = UploadedFiles;
                                AddFileNameToListBox(temp[1]);
                            }
                            else
                            {
                                UploadedFiles = uploadedFiles.Remove(0, temp[0].Length + 1);
                                ViewState[vsClientFileNames] = UploadedFiles;
                                for (int i = 1; i < temp.Length; i++)
                                {
                                    AddFileNameToListBox(temp[i]);
                                }
                            }
                        }
                    }

                }
            }
        }


        //t�rl�s gomb enged�lyez� script
        string js = "var btn = $get('" + btnDelete.ClientID + "');if($get('" + listBoxFileNames.ClientID + "').options.length == 0) {btn.disabled = true;}else{btn.disabled = false;}";
        ScriptManager.RegisterStartupScript(listBoxFileNames, listBoxFileNames.GetType(), "deleteDisabled", js, true);

        if (SingleMode)
        {
            setSingleView();
        }
        
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            if (!SingleMode || listBoxFileNames.Items.Count == 0)
            {
                HttpPostedFile file = FileUpload1.PostedFile;
                int fileLength = file.ContentLength;
                byte[] buffer = new byte[fileLength];
                file.InputStream.Read(buffer, 0, fileLength);
                string filePath = GetFilePath(file.FileName);
                FileInfo fi = new FileInfo(filePath);
                if (!fi.Exists)
                {
                    SaveFile(filePath, ref buffer);
                    AddFileNameToViewState(file.FileName);
                    string fileSize = convertFileSize(fileLength);
                    string fileNameAndLength = file.FileName + "  " + fileSize;
                    listBoxFileNames.Items.Add(new ListItem(fileNameAndLength, file.FileName));
                }
                else
                {
                    ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, fileUploadErrorHeader, fileIsUploaded);
                }
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel, fileUploadErrorHeader, noMoreUpload);
            }
        }
    }
    protected void ImageSave_Click(object sender, ImageClickEventArgs e)
    {
        //az �tnevez�ssel t�r�lt f�jlok v�gleges t�rl�se
        DeleteMarkedFiles();

        string hiddenFieldID = Request.QueryString.Get(QueryStringVars.HiddenFieldId);
        Dictionary<string,string> returnValues = new Dictionary<string,string>(1);
        string returnValue = String.Empty;
        if (ViewState[vsRandomDirectoryPath] != null && ViewState[vsRandomDirectoryPath].ToString().Trim() != String.Empty)
        {
            returnValue = ViewState[vsRandomDirectoryPath].ToString();
            if (ViewState[vsClientFileNames] != null && ViewState[vsClientFileNames].ToString().Trim() != String.Empty)
            {
                returnValue += ";" + ViewState[vsClientFileNames].ToString();
            }
            returnValue = returnValue.Replace("\\", "\\\\");
        }
        returnValues.Add(hiddenFieldID,returnValue);
        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, returnValues);
        JavaScripts.RegisterCloseWindowClientScript(Page, true);
    }
    protected void ImageCancel_Click(object sender, ImageClickEventArgs e)
    {
        if (ViewState[vsRandomDirectoryPath] != null && ViewState[vsRandomDirectoryPath].ToString().Trim() != String.Empty)
        {
            //ha nem voltak felt�ltve f�jlok, az eg�sz k�nyvt�r t�r�lve lesz
            if (String.IsNullOrEmpty(UploadedFiles))
            {
                DirectoryInfo diRandomDirectory = new DirectoryInfo(ViewState[vsRandomDirectoryPath].ToString());
                if (diRandomDirectory.Exists)
                {
                    diRandomDirectory.Delete(true);
                    listBoxFileNames.Items.Clear();
                }
            }
            //ha volt m�r felt�ltve f�jl, akkor azok nem lesznek t�r�lve
            else
            {
                //az �tnevez�ssel t�r�lt f�jlok vissza�ll�t�sa
                BackUpMarkedFiles();

                DirectoryInfo diRandomDirectory = new DirectoryInfo(ViewState[vsRandomDirectoryPath].ToString());
                if (diRandomDirectory.Exists)
                {
                    FileInfo[] fis = diRandomDirectory.GetFiles();
                    if (fis.Length > 0)
                    {
                        foreach (FileInfo fi in fis)
                        {
                            if (!UploadedFiles.Contains(fi.Name) && fi.Exists)
                            {
                                fi.Delete();
                            }
                        }
                    }
                }
            }
        }
        JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, false);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (listBoxFileNames.SelectedIndex > -1)
        {
            List<ListItem> deletableItemsList = new List<ListItem>(); 
            foreach (ListItem item in listBoxFileNames.Items)
            {
                if (item.Selected)
                {
                    deletableItemsList.Add(item);
                }
            }
            foreach (ListItem item in deletableItemsList)
            {
                string fileName = item.Value;
                string filePath = GetFilePath(fileName);
                FileInfo fi = new FileInfo(filePath);
                if (!UploadedFiles.Contains(fileName))
                {
                    if (fi.Exists)
                        fi.Delete();
                }
                else
                {
                    fi.MoveTo(filePath + ".deleted");
                }
                RemoveFileNameFromViewState(fileName);
                listBoxFileNames.Items.Remove(item);
            }
        }
    }
}