using Contentum.eAdmin.Service;
using Contentum.eAdmin.Utility;
using Contentum.eBusinessDocuments;
using System;
using System.Web.UI.WebControls;

public partial class FunkciokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Funkcio" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Funkciok krt_funkciok = (KRT_Funkciok)result.Record;
                    LoadComponentsFromBusinessObject(krt_funkciok);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Funkciok krt_funkciok)
    {
        Nev.Text = krt_funkciok.Nev;
        Kod.Text = krt_funkciok.Kod;
        if (Command == CommandName.Modify && krt_funkciok.Modosithato != "1")
        {
            Kod.Enabled = false;
        }
        Modosithato_CheckBox.Checked = (krt_funkciok.Modosithato == "1") ? true : false;
        Modosithato_CheckBox.Enabled = false;

        Leiras_TextBox.Text = krt_funkciok.Leiras;

        cbMunkanaplo.Checked = (krt_funkciok.MunkanaploJelzo == "1") ? true : false;
        cbFeladatJelzo.Checked = (krt_funkciok.FeladatJelzo == Constants.Database.Yes) ? true : false;
        cbKeziFeladatJelzo.Checked = (krt_funkciok.KeziFeladatJelzo == Constants.Database.Yes) ? true : false;

        ErvenyessegCalendarControl1.ErvKezd = krt_funkciok.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = krt_funkciok.ErvVege;
        if (Command == CommandName.Modify && krt_funkciok.Modosithato != "1")
        {
            ErvenyessegCalendarControl1.Enabled = false;
            cbMunkanaplo.Enabled = false;
        }

        FormHeader1.Record_Ver = krt_funkciok.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_funkciok.Base);

        if (Command == CommandName.View)
        {
            Nev.ReadOnly = true;
            Kod.ReadOnly = true;
            Leiras_TextBox.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
            cbMunkanaplo.Enabled = false;
            cbFeladatJelzo.Enabled = false;
        }
    }

    // form --> business object
    private KRT_Funkciok GetBusinessObjectFromComponents()
    {
        KRT_Funkciok krt_funkciok = new KRT_Funkciok();
        krt_funkciok.Updated.SetValueAll(false);
        krt_funkciok.Base.Updated.SetValueAll(false);

        krt_funkciok.Nev = Nev.Text;
        krt_funkciok.Updated.Nev = pageView.GetUpdatedByView(Nev);
                
        krt_funkciok.Kod = Kod.Text;
        krt_funkciok.Updated.Kod = pageView.GetUpdatedByView(Kod);

        if (Command == CommandName.New)
        {
            krt_funkciok.Modosithato = "1";
            krt_funkciok.Updated.Modosithato = true;
        }

        krt_funkciok.Leiras = Leiras_TextBox.Text;
        krt_funkciok.Updated.Leiras = pageView.GetUpdatedByView(Leiras_TextBox);

        krt_funkciok.MunkanaploJelzo = cbMunkanaplo.Checked ? Constants.Database.Yes : Constants.Database.No;
        krt_funkciok.Updated.MunkanaploJelzo = pageView.GetUpdatedByView(cbMunkanaplo);

        krt_funkciok.FeladatJelzo = cbFeladatJelzo.Checked ? Constants.Database.Yes : Constants.Database.No;
        krt_funkciok.Updated.FeladatJelzo = pageView.GetUpdatedByView(cbFeladatJelzo);

        krt_funkciok.KeziFeladatJelzo = cbKeziFeladatJelzo.Checked ? Constants.Database.Yes : Constants.Database.No;
        krt_funkciok.Updated.KeziFeladatJelzo = pageView.GetUpdatedByView(cbKeziFeladatJelzo);

        //krt_funkciok.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //krt_funkciok.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //krt_funkciok.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //krt_funkciok.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(krt_funkciok, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        krt_funkciok.Base.Ver = FormHeader1.Record_Ver;
        krt_funkciok.Base.Updated.Ver = true;
        return krt_funkciok;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev);
            compSelector.Add_ComponentOnClick(Kod);
            compSelector.Add_ComponentOnClick(Modosithato_CheckBox);
            compSelector.Add_ComponentOnClick(Leiras_TextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(cbMunkanaplo);
            compSelector.Add_ComponentOnClick(cbFeladatJelzo);
            compSelector.Add_ComponentOnClick(cbKeziFeladatJelzo);

            FormFooter1.SaveEnabled = false;
            Modosithato_CheckBox.Enabled = true;
        }
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Funkcio" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
                            KRT_Funkciok krt_funkciok = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, krt_funkciok);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }

                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                KRT_FunkciokService service = eAdminService.ServiceFactory.GetKRT_FunkciokService();
                                KRT_Funkciok krt_funkciok = GetBusinessObjectFromComponents();

                                // T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon
                                if (Modosithato_CheckBox.Checked == true)
                                {

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.Update(execParam, krt_funkciok);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                                else
                                {
                                    Result result = ResultError.CreateNewResultWithErrorCode(50404);
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
            }
        }
    }
}
