<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="FunkciokSearch.aspx.cs" Inherits="FunkciokSearch" %>

<%@ Register Src="Component/MuveletTextBox.ascx" TagName="MuveletTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc4" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc7" %>


<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:SearchHeader ID="SearchHeader1" runat="server" HeaderTitle="<%$Resources:Search,FunkciokSearchHeaderTitle%>" />
       
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" style="height: 30px">
                                &nbsp;<asp:Label ID="Nev_Label" runat="server" Text="Funkci� n�v:"></asp:Label></td>
                            <td class="mrUrlapMezo" style="height: 30px">
                                <asp:TextBox ID="Nev" runat="server" CssClass="mrUrlapInput">*</asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="K�d:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Kod" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="M�velett�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:MuveletTextBox id="MuveletTextBox1" runat="server" Validate="false">
                                </uc8:MuveletTextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="LabelModosithato" runat="server" Text="M�dos�that�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbModosithato" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>
                            </td>    
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Le�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Leiras" runat="server" CssClass="mrUrlapInput" Rows="3" TextMode="MultiLine"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMunkanaplo" runat="server" Text="Munkanapl�ban megjelen�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbMunkanaplo" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>
                            </td>    
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFeladatJelzo" runat="server" Text="Automatikus feladatjelz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbFeladatJelzo" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>
                            </td>    
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="label3" runat="server" Text="K�zi feladatjelz�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rbKeziFeladatJelzo" runat="server" RepeatDirection="Horizontal" CssClass="mrUrlapInputRadioButtonList">
                                </asp:RadioButtonList>
                            </td>    
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" rowspan="2" >
                                <uc4:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server" />
                                </td>
                        </tr>
                        <tr class="urlapSor">
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc7:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                
                    
                    <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

