using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class FunkciokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_FunkciokSearch);

    public static class Modosithato
    {
        public static readonly ListItem Yes = new ListItem("Igen", "1");
        public static readonly ListItem No = new ListItem("Nem", "0");
        public static readonly ListItem NotSet = new ListItem("Összes", "X");
        
        public static void FillRadioList(RadioButtonList list, string selectedValue)
        {
            list.Items.Clear();
            list.Items.Add(new ListItem(Yes.Text, Yes.Value));
            list.Items.Add(new ListItem(No.Text, No.Value));
            list.Items.Add(new ListItem(NotSet.Text, NotSet.Value));
            SetSelectedValue(list, selectedValue);
        }
        
        public static void FillRadioList(RadioButtonList list)
        {
            FillRadioList(list, NotSet.Value);
        }

        public static void SetSelectedValue(RadioButtonList list, string selectedValue)
        {
            if (selectedValue == Yes.Value || selectedValue == No.Value)
            {
                list.SelectedValue = selectedValue;
            }
            else
            {
                list.SelectedValue = NotSet.Value;
            }
        }
        public static string GetSelectedValue(RadioButtonList list)
        {
            if (list.SelectedValue == Yes.Value || list.SelectedValue == No.Value)
            {
                return list.SelectedValue;
            }
            else
            {
                return NotSet.Value;
            }
        }

        public static void SetSearchField(Field field, string value)
        {
            if (value == Yes.Value || value == No.Value)
            {
                field.Value = value;
                field.Operator = Query.Operators.equals;
            }
            else
            {
                field.Value = "";
                field.Operator = "";
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            Modosithato.FillRadioList(rbModosithato);
            Modosithato.FillRadioList(rbMunkanaplo);
            Modosithato.FillRadioList(rbFeladatJelzo);
            Modosithato.FillRadioList(rbKeziFeladatJelzo);

            KRT_FunkciokSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_FunkciokSearch)Search.GetSearchObject(Page, new KRT_FunkciokSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_FunkciokSearch krt_funkciokSearch = null;
        if (searchObject != null) krt_funkciokSearch = (KRT_FunkciokSearch)searchObject;

        if (krt_funkciokSearch != null)
        {
            Nev.Text = krt_funkciokSearch.Nev.Value;
            Kod.Text = krt_funkciokSearch.Kod.Value;
            Leiras.Text = krt_funkciokSearch.Leiras.Value;
            Modosithato.SetSelectedValue(rbModosithato, krt_funkciokSearch.Modosithato.Value);
            Modosithato.SetSelectedValue(rbMunkanaplo, krt_funkciokSearch.MunkanaploJelzo.Value);
            Modosithato.SetSelectedValue(rbFeladatJelzo, krt_funkciokSearch.FeladatJelzo.Value);
            Modosithato.SetSelectedValue(rbKeziFeladatJelzo, krt_funkciokSearch.KeziFeladatJelzo.Value);

            MuveletTextBox1.Id_HiddenField = krt_funkciokSearch.Muvelet_Id.Value;
            MuveletTextBox1.SetMuveletTextBoxById(SearchHeader1.ErrorPanel);

            Ervenyesseg_SearchFormComponent1.SetDefault(
                krt_funkciokSearch.ErvKezd, krt_funkciokSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_FunkciokSearch SetSearchObjectFromComponents()
    {
        KRT_FunkciokSearch krt_funkciokSearch = (KRT_FunkciokSearch)SearchHeader1.TemplateObject;
        if (krt_funkciokSearch == null)
        {
            krt_funkciokSearch = new KRT_FunkciokSearch();
        }

        if (!String.IsNullOrEmpty(Nev.Text))
        {
            krt_funkciokSearch.Nev.Value = Nev.Text;
            krt_funkciokSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev.Text);
        }
        if (!String.IsNullOrEmpty(Kod.Text))
        {
            krt_funkciokSearch.Kod.Value = Kod.Text;
            krt_funkciokSearch.Kod.Operator = Search.GetOperatorByLikeCharater(Kod.Text);
        }

        Modosithato.SetSearchField(krt_funkciokSearch.Modosithato, rbModosithato.SelectedValue);
        Modosithato.SetSearchField(krt_funkciokSearch.MunkanaploJelzo, rbMunkanaplo.SelectedValue);
        Modosithato.SetSearchField(krt_funkciokSearch.FeladatJelzo, rbFeladatJelzo.SelectedValue);
        Modosithato.SetSearchField(krt_funkciokSearch.KeziFeladatJelzo, rbKeziFeladatJelzo.SelectedValue);

        if (!String.IsNullOrEmpty(Leiras.Text))
        {
            krt_funkciokSearch.Leiras.Value = Leiras.Text;
            krt_funkciokSearch.Leiras.Operator = Search.GetOperatorByLikeCharater(Leiras.Text);
        }
        if (!String.IsNullOrEmpty(MuveletTextBox1.Id_HiddenField))
        {
            krt_funkciokSearch.Muvelet_Id.Value = MuveletTextBox1.Id_HiddenField;
            krt_funkciokSearch.Muvelet_Id.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            krt_funkciokSearch.ErvKezd, krt_funkciokSearch.ErvVege);

        return krt_funkciokSearch;
    }

    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject()); 
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_FunkciokSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiválasztott rekord törlése
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private KRT_FunkciokSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return new KRT_FunkciokSearch();
    }



   

}
