﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="HKP_HivatalLovList.aspx.cs" Inherits="HKP_HivatalLovList" EnableEventValidation="false"%>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <uc:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="Hivatal kiválasztás" /> 

    <table border="0" cellpadding="0" cellspacing="0" style="width: 95%">
        <tr>
            <td>
            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0" style="width: 99%">
                                    <tr>
                                        <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption_nowidth">
                                                    <asp:Label ID="labelNev" runat="server" Text="Név:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox id="tbNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                                <td style="width: 100px">
                                                    <asp:DropDownList ID="ddNevOperator" runat="server">
                                                        <asp:ListItem Text="És" Value="0" Selected="True" />
                                                        <asp:ListItem Text="Vagy" Value="1" />
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="mrUrlapCaption_nowidth">
                                                    <asp:Label ID="labelRovidNev" runat="server" Text="Rövid név:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:TextBox id="tbRovidNev" runat="server" CssClass="mrUrlapInputSearch"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                <td class="mrUrlapCaption_nowidth">
                                                    <asp:Label ID="lebelCimTipus" runat="server" Text="Cím típus:" />
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:DropDownList id="ddCimTipus" runat="server" CssClass="mrUrlapInputSearchComboBox">
                                                        <asp:ListItem Text="Mind" Value="0" />
                                                        <asp:ListItem Text="Hivatali kapu" Value="1"/>
                                                        <asp:ListItem Text="Cégkapu" Value="2" />
                                                        <asp:ListItem Text="Perkapu" Value="7" />
                                                    </asp:DropDownList>
                                                </td>
                                                <td>

                                                </td>
                                                <td class="mrUrlapCaption_nowidth">
                                                </td>
                                                <td class="mrUrlapMezo">
                                                    <asp:CheckBox ID="cbTamogatottSzolgaltatasok" runat="server" Checked="true" CssClass="urlapCheckbox mrUrlapCaption" Text="Támogatott szolgáltatások" />
                                                </td>
                                            </tr>
                                            <tr class="urlapSor">
                                                    <td colspan="5">
                                                         <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" AlternateText="Keresés" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" style="vertical-align:top"></asp:ImageButton>
                                                    </td>
                                             </tr>
                                        </table>
                                        </td>
                                    </tr>
                                    <tr>
                                    <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                        <div class="listaFulFelsoCsikKicsi">
                                            <img alt="" src="images/hu/design/spacertrans.gif" />
                                        </div>
                                    </td>
                                    </tr>
                                     <tr>
                                        <td style="text-align: left; vertical-align: top;">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                            <!-- Kliens oldali kiválasztás kezelése -->
                                                            <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                            <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                            <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                                                PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                                DataKeyNames="KRID">
                                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="KRID" HeaderText="KRID" SortExpression="KRID">
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="60px" />
                                                                    </asp:BoundField>                                                
                                                                    <asp:BoundField DataField="Nev" HeaderText="Hivatal neve" SortExpression="Nev" >
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="200px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RovidNev" HeaderText="Hivatal rövid neve" SortExpression="RovidNev" >
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="60px"/>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MAKKod" SortExpression="MAKKod">
                                                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="HivatalTipusNev" HeaderText="Típus" SortExpression="HivatalTipusNev" >
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="50px"/>
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="TamogatottSzolgaltatasok" HeaderText="Szolgáltatások" SortExpression="TamogatottSzolgaltatasok" >
                                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                        <HeaderStyle Width="50px"/>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <PagerSettings Visible="False" />
                                                                <EmptyDataTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:Search,NoSearchResult%>" ></asp:Label>
                                                                </EmptyDataTemplate>
                                                                <AlternatingRowStyle CssClass="GridViewAlternateRowStyle" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>

