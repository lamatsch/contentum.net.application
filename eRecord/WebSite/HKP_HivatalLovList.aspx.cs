﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using System.Data;
using Contentum.eUIControls;
using Contentum.eIntegrator.Service;

public partial class HKP_HivatalLovList : Contentum.eUtility.UI.PageBase
{
    private string InitText
    {
        get
        {
            return Request.QueryString.Get("InitText");
        }
    }

    private string Fiok
    {
        get
        {
            return Request.QueryString.Get("Fiok");
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PartnerekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(this.InitText))
        {
            this.tbNev.Text = this.InitText;
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();


        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        LovListFooter1.ButtonsClick += new System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);


        LovListHeader1.ErrorPanel.Visible = false;
        LovListHeader1.ErrorUpdatePanel.Update();

        ScriptManager1.SetFocus(ButtonSearch);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 0);
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    string rovidNevHiddenFieldId = Request.QueryString.Get("RovidNevHiddenFieldId");
                    string MAKKodHiddenFieldId = Request.QueryString.Get("MAKKodHiddenFieldId");

                    string rovidNev = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);
                    string MAKKod = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);

                    bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;

                    JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, true, tryFireChangeEvent);

                    if (!String.IsNullOrEmpty(rovidNevHiddenFieldId) && !String.IsNullOrEmpty(MAKKodHiddenFieldId))
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(rovidNevHiddenFieldId, rovidNev);
                        parameters.Add(MAKKodHiddenFieldId, MAKKod);
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, parameters, "HivatalReturnValues", true, tryFireChangeEvent, true);
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult();
    }

    protected void FillGridViewSearchResult()
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;


        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
        HivatalSzuroParameterek szuroParameterk = new HivatalSzuroParameterek();
        szuroParameterk.Nev = tbNev.Text;
        szuroParameterk.RovidNev = tbRovidNev.Text;
        szuroParameterk.NevOperator = ParseEnum<OperatorTipus>(ddNevOperator.SelectedValue, OperatorTipus.And); 
        szuroParameterk.Tipus = ParseEnum<HivatalTipus>(ddCimTipus.SelectedValue, HivatalTipus.Mind);
        szuroParameterk.TamogatottSzolgaltatasok = cbTamogatottSzolgaltatasok.Checked;
        Result result = svc.HivatalokListajaFeldolgozas_Szurt(Fiok, szuroParameterk);

        GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

    }

    private void GridViewFill(GridView gridView, Result result, eErrorPanel errorPanel, UpdatePanel parentErrorUpdatePanel)
    {
        Contentum.eUtility.ResultError resultError = new Contentum.eUtility.ResultError(result);

        if (resultError.IsError)
        {
            // Hiba kezeles
            ResultError.DisplayResultErrorOnErrorPanel(errorPanel, result, Contentum.eUtility.Resources.Error.GetString("ErrorHeader_FillListBox"));
            if (parentErrorUpdatePanel != null)
            {
                parentErrorUpdatePanel.Update();
            }
            return;
        }

        HivatalokListajaValasz valasz = result.Record as HivatalokListajaValasz;

        if (valasz != null)
        {
            gridView.DataSource = valasz.HivatalokLista;
            gridView.DataBind();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(this.InitText))
            {
                FillGridViewSearchResult();
            }
        }

        // scrollozhatóság állítása
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE, 12);
        if (!GridViewCPE.ScrollContents)
        {
            Panel1.Width = Unit.Percentage(96.6);
        }
        else
        {
            Panel1.Width = Unit.Percentage(95);
        }

    }

    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {

                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

    T ParseEnum<T>(string value, T defaultValue)
    {
       return (T)Enum.Parse(typeof(T), value);
    }
}