﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Microsoft.Reporting.WebForms;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using System.Collections;
using System.Reflection;
using System.Configuration;

public partial class HatosagiAdatokHibaList : Contentum.eUtility.ReportPageBase
{
    protected String DefaultOrderBy
    {
        get
        {
            return " order by  EREC_IraIktatokonyvek.Iktatohely ASC, EREC_UgyUgyiratok.Foszam ASC, EREC_IraIratok.Alszam ASC, EREC_IraIratok.Sorszam";
        }
    }

    protected String DefaultPageSize
    {
        get { return "100000"; }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "HatosagiAdatokHibaList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Page.Server.ScriptTimeout = 600;
        DatumIntervallumSearch.AktualisEv_CheckBox.Checked = true;
        InitReportViewer();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ImageButton_Refresh.Click += new ImageClickEventHandler(ImageButton_Refresh_Click);

        if (!IsPostBack)
        {
            InitReport(ReportViewer1, false, "", "");
            Refresh();
        }
    }

    public void InitReportViewer()
    {
        ReportViewer1.ShowParameterPrompts = false;
        ReportViewer1.ShowRefreshButton = false;
        //EnableRenderExtensions("EXCEL", "PDF");
    }


    void ImageButton_Refresh_Click(object sender, ImageClickEventArgs e)
    {
        Refresh();
    }

    private void Refresh()
    {
        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewer1.ServerReport.Refresh();

        ReportViewer1.ShowReportBody = true;
    }

    protected override ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        string where = String.Empty;
        string where_EREC_IraIratok = String.Empty;
      
        where = GetSearch();

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "Where":
                            ReportParameters[i].Values.Add(where);
                            break;
                        case "Where_EREC_IraIratok":
                            ReportParameters[i].Values.Add(where_EREC_IraIratok);
                            break;
                        case "OrderBy":
                            ReportParameters[i].Values.Add(DefaultOrderBy);
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(DefaultPageSize);
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloId(Page));
                            break;
                        case "ExecutorUserName":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.GetCurrent(Page).Felhasznalo.Nev);
                            break;
                        case "IntervallumFilter":
                            ReportParameters[i].Values.Add(GetIntervallumFilter());
                            break;
                        case "eRecordWebSiteUrl":
                            ReportParameters[i].Values.Add(GeteRecordWebSiteUrl());
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }

    private string GetSearch()
    {
        if (DatumIntervallumSearch.IsEmpty)
        {
            DatumIntervallumSearch.AktualisEv_CheckBox.Checked = true;
        }

        var search = new EREC_IraOnkormAdatokSearch();
        search.ErvKezd.Clear();
        search.ErvVege.Clear();
        DatumIntervallumSearch.SetSearchObjectFields(search.ErvKezd);

        Query query = new Query();
        query.BuildFromBusinessDocument(search);
        return query.Where;
    }

    private string GetIntervallumFilter()
    {
        string filter = String.Empty;

        if (DatumIntervallumSearch.AktualisEv_CheckBox.Checked)
        {
            filter = String.Format("Aktuális év ({0})", DateTime.Now.Year);
        }
        else if (!DatumIntervallumSearch.IsEmpty)
        {
            filter = String.Format("{0} - {1}", DatumIntervallumSearch.DatumKezd_TextBox.Text, DatumIntervallumSearch.DatumVege_TextBox.Text);
        }

        return filter;
    }

    private string GeteRecordWebSiteUrl()
    {
        string port = Request.Url.Port == 80 ? String.Empty : ":" + Request.Url.Port.ToString();
        string url = String.Format("{0}://{1}{2}{3}/", Request.Url.Scheme, Request.Url.Host, port, Request.ApplicationPath);
        return url;
    }

    private ReportViewer _ReportViewer
    {
        get
        {
            return ReportViewer1;
        }
    }

    private void EnableRenderExtensions(params string[] extensionName)
    {
        foreach (RenderingExtension extension in _ReportViewer.ServerReport.ListRenderingExtensions())
        {
            bool isEnabled = extensionName.Contains(extension.Name);

            // extension.m_isVisible = value;
            extension
                .GetType()
                .GetField("m_isVisible",
            BindingFlags.NonPublic | BindingFlags.Instance)
                .SetValue(extension, isEnabled);
        }
    }
}