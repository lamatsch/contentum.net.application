<%@ Page Title="Hatósági adatok tesztelési segéd lista" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="HatosagiAdatokTesztSegedLista.aspx.cs" Inherits="HatosagiAdatokTesztSegedLista" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc" %>
<%@ Register Src="~/Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
        .ajax__calendar .ajax__calendar_container {
            z-index:1001;
        }
    </style>
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <div style="text-align: left; padding-bottom: 10px">
        <asp:UpdatePanel ID="FeladatRiportUpdatePanel" runat="server">
            <ContentTemplate>
                <eUI:eFormPanel ID="EFormPanel1" runat="server" Visible="true" Width="1100px">
                    <div style="text-align: left;">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr class="urlapSor_kicsi">
                                    <td class="mrUrlapCaption_short">
                                        <asp:Label ID="Label5" runat="server" Text="Intervallum:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc:DatumIntervallum_SearchCalendarControl ID="DatumIntervallumSearch" runat="server"
                                            AktualisEv_Visible="true" Validate="false" />
                                    </td>
                                    <td style="text-align: left; padding-left: 50px;">
                                        <asp:ImageButton ID="ImageButton_Refresh" runat="server" ImageUrl="~/images/hu/trapezgomb/frissites_trap.jpg"
                                        onmouseover="swapByName(this.id,'frissites_trap2.jpg');" onmouseout="swapByName(this.id,'frissites_trap.jpg');"
                                        AlternateText="Riport frissítése" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </eUI:eFormPanel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="text-align:left">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="1100px"
            Height="500px" CssClass="ReportViewer" HyperlinkTarget="_blank" EnableTelemetry="false">
            <ServerReport ReportPath="/Adminisztracio/HatosagiAdatokTesztSegedLista" />
        </rsweb:ReportViewer>
    </div>
</asp:Content>

