﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Web.UI;
using System.Collections.Generic;

public partial class HatosagiAdatokTomegesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private string Command = "";
    private String IratId = "";
    private String[] IratokArray;

    private string funkcioKod = "IraIratOnkormanyzatiAdatList";

    public string maxTetelszam = "0";
    
    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Hatósági adatok tömeges módosítása";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratId)))
        {
            if (Session[Constants.SelectedIratIds] != null)
                IratId = Session[Constants.SelectedIratIds].ToString();
        }
        else IratId = Request.QueryString.Get(QueryStringVars.IratId);

        if (!String.IsNullOrEmpty(IratId))
        {
            IratokArray = IratId.Split(',');
        }

        // Funkciójog-ellenõrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod);

        if (!IsPostBack)
        {
            LoadFormComponents();

            if (IratokArray.Length > 0)
            {
                HatosagiStatisztikaTabA.ParentForm = Constants.ParentForms.IraIrat;
                HatosagiStatisztikaTabA.SetBatchModify(IratokArray, _ugyFajtaja);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);
        ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IratokArray.Length > 0 && !Page.IsPostBack)
        {
            HatosagiStatisztikaTabA.ClearTextBoxValues();
        }
    }

    private void LoadFormComponents()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            IratokListPanel.Visible = true;
            FillIratokGridView();
        }
    }

    private void FillIratokGridView()
    {
        var res = IratokGridViewBind();

        // ellenõrzés:
        if (res != null && res.GetCount != IratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        int count_ok = UI.GetGridViewSelectedCheckBoxesCount(IraIratokGridView, "check");

        int count_NEMok = IratokArray.Length - count_ok;

        if (count_NEMok > 0)
        {
            Label_Warning_Irat.Text = Resources.List.UI_NotModifiableItemsCount + count_NEMok.ToString();
            Panel_Warning_Irat.Visible = true;
        }

        Panel_Stat.Visible = count_ok > 0;
    }
  
    protected Result IratokGridViewBind()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            var search = new EREC_IraIratokSearch();
            
            search.Id.In(IratokArray);
            
            Result res = service.GetAllWithExtension(ExecParam, search);

            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                Panel_Stat.Visible = false;
            }
            else
            {
                if (CheckUgyfajtaja(res))
                {
                    ui.GridViewFill(IraIratokGridView, res, "", FormHeader1.ErrorPanel, null);
                    Panel_Stat.Visible = true;

                    if (!CheckItsz())
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel,
                            "Figyelem! A kijelölt tételek irattári tételszáma nem azonos!");
                    }
                }
                else
                {
                    res.ErrorCode = "IRAT_UGYFAJTAJA_KULONBOZIK";
                    res.ErrorMessage = "A kijelölt tételek esetén az ügy fajtája nem azonos, a művelet nem végrehajtható.";
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                    Panel_Stat.Visible = false;
                }
            }
            return res;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    private string GetUgyFajtaja(DataRow row)
    {
        var uf = "";
        var uft = "";
        if (row != null)
        {
            // BUG_11670
            //UI.GetUgyFajtaja(Page, row["UgyFajtaja"].ToString(), out uf, out uft);
            UI.GetUgyFajtaja(Page, row["Ugy_Fajtaja"].ToString(), out uf, out uft);
        }
        return uf;
    }

    private string _ugyFajtaja;

    private bool CheckUgyfajtaja(Result res)
    {
        var uf = "";
        _ugyFajtaja = "";
        var set = false;
        foreach (DataRow row in res.Ds.Tables[0].Rows)
        {
            if (!set)
            {
                uf = GetUgyFajtaja(row);
                //_ugyFajtaja = row["UgyFajtaja"].ToString();
                _ugyFajtaja = row["Ugy_Fajtaja"].ToString();
                set = true;
            }
            else if (uf != GetUgyFajtaja(row))
            {
                return false;
            }
        }
        return true;
    }

    private bool CheckItsz()
    {
        var itsz = "";
        var set = false;
        for (int i = 0; i < IraIratokGridView.Rows.Count; i++)
        {
            var row = IraIratokGridView.Rows[i];
            var checkB = (CheckBox)row.FindControl("check");
            if (checkB != null && checkB.Checked)
            {
                var cell = row.Cells.Cast<DataControlFieldCell>().FirstOrDefault(c => c != null && c.ContainingField is BoundField 
                && ((BoundField)c.ContainingField).DataField.Equals("ITSZ"));
                if (cell != null)
                {
                    if (!set)
                    {
                        itsz = cell.Text;
                        set = true;
                    }
                    else if (itsz != cell.Text)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void IratokGridView_RowDataBound_CheckHatosagiTomeges(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRow row = ((DataRowView)e.Row.DataItem).Row;

            Iratok.Statusz statusz = Iratok.GetAllapotFromDataRow(row);

            ErrorDetails errorDetails = null;
            bool ok = !String.IsNullOrEmpty(GetUgyFajtaja(row));
            if (ok)
            {
                ok = Iratok.Modosithato(statusz, UI.SetExecParamDefault(Page), out errorDetails);
            }
            else
            {
                errorDetails = new ErrorDetails { Message = "Az ügy fajtája nem megfelelő a hatósági statisztikához" };
            }
            UI.SetRowCheckboxAndInfo(ok, e, errorDetails, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, statusz.Id, page);
        }
    }

    protected void IraIratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        IratokGridView_RowDataBound_CheckHatosagiTomeges(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
        }
    }

    private List<string> GetSelectedIds()
    {
        return ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null);
    }
    
    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, funkcioKod))
            {
                if (String.IsNullOrEmpty(IratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    var selectedItemsList = GetSelectedIds();

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    var iratIds = selectedItemsList.ToArray();
                    var result = HatosagiStatisztikaTabA.BatchSave(iratIds);

                    if (result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(IraIratokGridView, result);
                    }
                    else
                    {
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                        ResultError.ResetErrorPanel(FormHeader1.ErrorPanel);
                    }
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }
        }
    }
}

