<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="HatosagiStatisztika.aspx.cs" Inherits="HatosagiStatisztika"
    Title="<%$Resources:Form,HatosagiStatisztikaFormHeaderTitle %>" %>


<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
    <%@ Register Src="~/Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />
    
<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField ID="MessageHiddenField" runat="server" />

    <asp:Panel ID="MainPanel" runat="server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,HatosagiStatisztikaFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel ID="HatosagiStatiszrikaUpdatePanel" runat="server" >
                    <ContentTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelTipus" runat="server" Text="Statisztika t�pusa:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <asp:RadioButtonList ID="rbTipus" runat="server" AutoPostBack="false" TextAlign="Right">
                                    <asp:ListItem Selected="True" Text="�gyiratforgalom" Value="Ugyiratforgalom" />
                                    <asp:ListItem Selected="False" Text="�nkorm�nyzati hat�s�gi" Value="Onkormanyzat" />
                                    <asp:ListItem Selected="False" Text="�llamigazgat�si hat�s�gi" Value="Allamigazgatas" />
                                </asp:RadioButtonList>
                                
                            </td>
                        </tr>                          
                         <tr class="urlapElvalasztoSor">
                            <td />
                        </tr>                      
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSzekhely" runat="server" Text="�nkorm�nyzat sz�khelye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <asp:DropDownList ID="ddownSzekhely"
                                    runat="server"></asp:DropDownList>
                                
                            </td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEv" runat="server" Text="Lek�rdez�s �ve:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                            <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddownEv" runat="server" />
                                </td>
                                <td style="padding-right:5px;padding-left:5px;">
                                    <asp:DropDownList ID="ddownFelEv" runat="server" AutoPostBack="true">
                                        <asp:ListItem Value="1" Text="1. f�l�v" />
                                        <asp:ListItem Value="2" Text="2. f�l�v" />
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <uc:DatumIntervallum_SearchCalendarControl ID="DatumIntervallumSearch" runat="server" Validate="false" />
                                </td>
                            </tr>
                            </table>
                                
                                
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelRogzitette" runat="server" Text="R�gz�tette:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" style="padding-right:10px">
                                <asp:TextBox ID="TextBoxRogzitette"
                                    runat="server" Text=""></asp:TextBox>
                            </td>
                        </tr>                        
                    </table>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
 
    </asp:Panel>
    
</asp:Content>

