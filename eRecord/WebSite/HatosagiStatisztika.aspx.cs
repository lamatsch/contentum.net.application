using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

using System.Drawing;

using System.IO;


public partial class HatosagiStatisztika : Contentum.eUtility.UI.PageBase
{
    private const string targetCellsColumnName = "Cells";
    private const string targetSeparators = ";";
    private const string targetCellSeparators = ",";
    private string templatePath = ".\\ExcelTemplates\\";
    private string templateFileName = "";
    private string worksheetName = "";
    private string parentTableCells = "";
    private string targetCell_Szekhely = "";
    private string targetCell_Kelt = "";
    private string targetCell_Rogzitette = "";

    private Dictionary<int, ListItemCollection> szekhelyekDictionary;

    //private const string szekhelyek = "Aba;Ab�dszal�k;Abaliget;Abas�r;Aba�jalp�r;Aba�jk�r;Aba�jlak;Aba�jsz�nt�;Aba�jszolnok;Aba�jv�r;Abda;Abod;Abony;�brah�mhegy;�cs;Acsa;Acs�d;Acsalag;�cstesz�r;Ad�cs;�d�nd;Ad�sztevel;Adony;Adorj�nh�za;Adorj�s;�g;�gasegyh�za;�gfalva;Aggtelek;Agyagosszerg�ny;Ajak;Ajka;Aka;Akaszt�;Alacska;Alap;Alatty�n;Albertirsa;Alcs�tdoboz;Aldebr�;Algy�;Alib�nfa;Almamell�k;Alm�sf�zit�;Alm�sh�za;Alm�skamar�s;Alm�skereszt�r;�lmosd;Als�berecki;Als�bog�t;Als�dobsza;Als�gagy;Als�mocsol�d;Als�n�na;Als�n�medi;Als�nemesap�ti;Als�ny�k;Als��rs;Als�p�hok;Als�pet�ny;Als�rajk;Als�regmec;Als�szenterzs�bet;Als�szentiv�n;Als�szentm�rton;Als�sz�ln�k;Als�szuha;Als�telekes;Als�told;Als��jlak;Als�vad�sz;Als�zsolca;Ambr�zfalva;Anarcs;Andocs;Andornakt�lya;Andr�sfa;Annav�lgy;Ap�catorna;Apagy;Apaj;Aparhant;Ap�tfalva;Ap�tistv�nfalva;Ap�tvarasd;Apc;�porka;Apostag;Aranyosap�ti;Aranyosgad�ny;Arka;Arl�;Arn�t;�rokt�;�rp�dhalom;�rp�s;�rt�nd;�sotthalom;�sv�nyr�r�;Aszal�;�sz�r;Asz�d;Asz�f�;�ta;�t�ny;Atk�r;Attala;Babarc;Babarcsz�l�s;Bab�csa;B�bolna;B�bonymegyer;Babosd�br�te;Bab�t;B�csalm�s;B�csbokod;B�csbors�d;B�csszentgy�rgy;B�cssz�l�s;Badacsonytomaj;Badacsonyt�rdemic;Bag;Bagam�r;Baglad;Bagod;B�gyogszov�t;Baj;Baja;Baj�nsenye;Bajna;Baj�t;Bak;Bakh�za;Bak�ca;Bakonszeg;Bakonya;Bakonyb�nk;Bakonyb�l;Bakonycsernye;Bakonygyir�t;Bakonyj�k�;Bakonykopp�ny;Bakonyk�ti;Bakonyn�na;Bakonyoszlop;Bakonyp�terd;Bakonyp�l�ske;Bakonys�g;Bakonys�rk�ny;Bakonyszentiv�n;Bakonyszentkir�ly;Bakonyszentl�szl�;Bakonyszombathely;Bakonysz�cs;Bakonytam�si;Baks;Baksa;Baktak�k;Baktal�r�nth�za;Bakt�tt�s;Balajt;Balassagyarmat;Bal�stya;Balaton;Balatonakali;Balatonalm�di;Balatonber�ny;Balatonbogl�r;Balatoncsics�;Balatonederics;Balatonendr�d;Balatonfenyves;Balatonf�kaj�r;Balatonf�ldv�r;Balatonf�red;Balatonf�zf�;Balatongy�r�k;Balatonhenye;Balatonkenese;Balatonkereszt�r;Balatonlelle;Balatonmagyar�d;Balatonm�riaf�rd�;Balaton�sz�d;Balatonrendes;Balatonszabadi;Balatonsz�rsz�;Balatonszemes;Balatonszentgy�rgy;Balatonszepezd;Balatonsz�l�s;Balatonudvari;Balaton�jlak;Balatonvil�gos;Balinka;Balk�ny;Ball�sz�g;Balmaz�jv�ros;Balogunyom;Balotasz�ll�s;Balsa;B�lv�nyos;Bana;B�nd;B�nfa;B�nhorv�ti;B�nk;B�nokszentgy�rgy;B�nr�ve;B�r;Barab�s;Baracs;Baracska;B�r�nd;Baranyah�dv�g;Baranyajen�;Baranyaszentgy�rgy;Barbacs;Barcs;B�rdudvarnok;Barlahida;B�rna;Barnag;B�rsonyos;Basal;Bask�;B�ta;B�taap�ti;B�tasz�k;Bat�;B�tmonostor;B�tonyterenye;B�tor;B�torliget;Battonya;B�tya;Batyk;B�zakerettye;Bazsi;B�b;Becsehely;Becske;Becskeh�za;Becsv�lgye;Bedegk�r;Bed�;Bejcgyerty�nos;B�k�s;Bekecs;B�k�s;B�k�scsaba;B�k�ss�mson;B�k�sszentandr�s;Bek�lce;B�lap�tfalva;B�lav�r;Belecska;Beled;Beleg;Belezna;B�lmegyer;Beloiannisz;Bels�s�rd;Belv�rdgyula;Benk;B�nye;B�r;B�rbaltav�r;Bercel;Beregdar�c;Beregsur�ny;Berekb�sz�rm�ny;Berekf�rd�;Beremend;Berente;Beret;Beretty��jfalu;Berhida;Berkenye;Berkesd;Berkesz;Bernecebar�ti;Berz�k;Berzence;Besence;Beseny�d;Beseny�telek;Besenysz�g;Besny�;Beszterec;Bezedek;Bezenye;Bezer�d;Bezi;Biatorb�gy;Bics�rd;Bicske;Bihardancsh�za;Biharkeresztes;Biharnagybajom;Bihartorda;Biharugra;Bik�cs;Bikal;Biri;Birj�n;Bisse;Boba;Bocf�lde;Bocon�d;B�csa;Bocska;Bocskaikert;Boda;Bodajk;Bodm�r;Bodolyab�r;Bodonhely;Bodony;Bodorfa;Bodrog;Bodroghalom;Bodrogkereszt�r;Bodrogkisfalud;Bodrogolaszi;B�dvalenke;B�dvar�k�;B�dvaszilas;Bog�cs;Bog�d;Bog�dmindszent;Bogd�sa;Bogyiszl�;Bogyoszl�;Bojt;B�kah�za;Bokod;Bokor;Boldog;Boldogasszonyfa;Boldogk��jfalu;Boldogk�v�ralja;Boldva;Bolh�s;Bolh�;B�ly;Boncodf�lde;Bonnya;Bonyh�d;Bonyh�dvarasd;Bord�ny;Borg�ta;Borj�d;Borota;Borsfa;Borsodb�ta;Borsodgeszt;Borsodiv�nka;Borsodn�dasd;Borsodszentgy�rgy;Borsodszir�k;Borsosber�ny;Borsz�rcs�k;Borzav�r;Bosta;Botpal�d;Botykapeterd;Bozsok;Bozzai;B�zsva;B�;B�cs;B�de;B�deh�za;B�g�t;B�g�te;B�h�nye;B�k�ny;B�lcske;B�ny;B�rcs;B�rz�nce;B�s�rk�ny;B�sz�nfa;Bucsa;Bucsu;B�cs�szentl�szl�;Bucsuta;Budajen�;Budakal�sz;Budakeszi;Buda�rs;Budapest I.ker�let;Budapest II.ker�let;Budapest III.ker�let;Budapest IV.ker�let;Budapest IX.ker�let;Budapest V.ker�let;Budapest VI.ker�let;Budapest VII.ker�let;Budapest VIII.ker�let;Budapest X.ker�let;Budapest XI.ker�let;Budapest XII.ker�let;Budapest XIII.ker�let;Budapest XIV.ker�let;Budapest XIX.ker�let;Budapest XV.ker�let;Budapest XVI.ker�let;Budapest XVII.ker�let;Budapest XVIII.ker�let;Budapest XX.ker�let;Budapest XXI.ker�let;Budapest XXII.ker�let;Budapest XXIII.ker�let;Bugac;Bugacpusztah�za;Bugyi;Buj;Buj�k;Buzs�k;B�k;B�kk�br�ny;B�kkaranyos;B�kkmogyor�sd;B�kk�sd;B�kksz�k;B�kkszenterzs�bet;B�kkszentkereszt;B�kkszentm�rton;B�kkzs�rc;B�r�s;B�ss�;B�tt�s;C�k;Cak�h�za;Cece;C�g�nyd�ny�d;Cegl�d;Cegl�dbercel;Celld�m�lk;Cered;Chernelh�zadamonya;Cibakh�za;Cig�nd;Cik�;Cir�k;Csabacs�d;Csabaszabadi;Csabdi;Csabrendek;Cs�fordj�nosfa;Csaholc;Csaj�g;Cs�k�ny;Cs�k�nydoroszl�;Cs�kber�ny;Cs�kv�r;Csan�dalberti;Csan�dap�ca;Csan�dpalota;Cs�nig;Cs�ny;Cs�nyoszr�;Csanytelek;Csapi;Csapod;Cs�rdasz�ll�s;Csarn�ta;Csaroda;Cs�sz�r;Cs�sz�rt�lt�s;Cs�szl�;Cs�talja;Csat�r;Csatasz�g;Csatka;Cs�voly;Cseb�ny;Cs�cse;Cseg�ld;Csehb�nya;Csehi;Csehimindszent;Cs�m;Csem�;Csempeszkop�cs;Csengele;Csenger;Csengersima;Csenger�jfalu;Cseng�d;Cs�nye;Cseny�te;Cs�p;Cs�pa;Csepreg;Cs�r;Cserdi;Cser�nfa;Cser�pfalu;Cser�pv�ralja;Cserh�thal�p;Cserh�tsur�ny;Cserh�tszentiv�n;Cserkesz�l�;Cserk�t;Csernely;Cserszegtomaj;Csertalakos;Csert�;Csesznek;Csesztreg;Csesztve;Cset�ny;Cs�vharaszt;Csibr�k;Csik�ria;Csik�st�tt�s;Csikv�nd;Csincse;Csipkerek;Csit�r;Csob�d;Csobaj;Csob�nka;Cs�kak�;Csokonyavisonta;Csokvaom�ny;Csolnok;Cs�lyosp�los;Csoma;Csom�d;Csomb�rd;Csongr�d;Csonkahegyh�t;Csonkamindszent;Csopak;Cs�r;Csorna;Csorv�s;Cs�t;Cs�de;Cs�gle;Cs�km�;Cs�k�ly;Cs�mend;Cs�m�d�r;Cs�m�r;Cs�nge;Cs�rnyef�ld;Cs�r�g;Cs�r�tnek;Cs�sz;Cs�v�r;Csurg�;Csurg�nagymarton;C�n;Dabas;Dabronc;Dabrony;Dad;D�g;D�ka;Dalmand;Damak;D�m�c;D�nszentmikl�s;D�ny;Daraboshegy;Dar�ny;Darn�;Darn�zseli;Daruszentmikl�s;Darvas;D�vod;Debercs�ny;Debrecen;Debr�te;Decs;D�destapolcs�ny;D�g;Dejt�r;D�legyh�za;Demecser;Demj�n;Dencsh�za;D�nesfa;Derecske;Derekegyh�z;Deszk;Detek;Detk;D�vav�nya;Devecser;Dinnyeberki;Di�sber�ny;Di�sd;Di�sjen�;Di�sk�l;Di�sviszl�;Doba;Doboz;Dobri;Dobronhegy;D�c;Domah�za;Domasz�k;Dombegyh�z;Dombiratos;Domb�v�r;Dombr�d;Domony;Domoszl�;Dorm�nd;Dorog;Dorogh�za;Dozmat;D�b�rhegy;D�br�ce;D�br�k�z;D�br�nte;D�ge;D�m�s;D�ms�d;D�r;D�rgicse;D�r�ske;D�tk;D�v�ny;Dr�gsz�l;Dr�vacsehi;Dr�vacsepely;Dr�vafok;Dr�vag�rdony;Dr�vaiv�nyi;Dr�vakereszt�r;Dr�vapalkonya;Dr�vapiski;Dr�vaszabolcs;Dr�vaszerdahely;Dr�vaszt�ra;Dr�vatam�si;Dr�gelypal�nk;Dubics�ny;Dudar;Duka;Dunaalm�s;Dunabogd�ny;Dunaegyh�za;Dunafalva;Dunaf�ldv�r;Dunaharaszti;Dunakeszi;Dunakiliti;Dunapataj;Dunaremete;Dunaszeg;Dunaszekcs�;Dunaszentbenedek;Dunaszentgy�rgy;Dunaszentmikl�s;Dunaszentp�l;Dunasziget;Dunatet�tlen;Duna�jv�ros;Dunavars�ny;Dunavecse;Dusnok;D�zs;Eberg�c;Ebes;�cs;Ecs�d;Ecseg;Ecsegfalva;Ecseny;Ecser;Edde;Edel�ny;Edve;Eger;Eger�g;Egeralja;Egeraracsa;Egerbakta;Egerbocs;Egercsehi;Egerfarmos;Egerl�v�;Egerszal�k;Egersz�l�t;�gersz�g;Egerv�r;Egerv�lgy;Egyed;Egyek;Egyh�zasdengeleg;Egyh�zasfalu;Egyh�zasgerge;Egyh�zasharaszti;Egyh�zashetye;Egyh�zasholl�s;Egyh�zaskesz�;Egyh�zaskoz�r;Egyh�zasr�d�c;Elek;Ellend;El�sz�ll�s;Em�d;Encs;Encsencs;Endrefalva;Endr�c;Enese;Enying;Eperjes;Eperjeske;Epl�ny;Ep�l;Ercsi;�rd;Erd�b�nye;Erd�horv�ti;Erd�kertes;Erd�k�vesd;Erd�k�rt;Erd�sm�rok;Erd�smecske;Erd�tarcsa;Erd�telek;Erk;�rpatak;�rsekcsan�d;�rsekhalma;�rsekvadkert;�rt�ny;Erzs�bet;Eszt�r;Eszteregnye;Eszterg�lyhorv�ti;Esztergom;Ete;Etes;Etyek;F�bi�nh�za;F�bi�nsebesty�n;F�c�nkert;Fadd;F�j;Fajsz;Fancsal;Far�d;Farkasgyep�;Farkaslyuk;Farmos;Fazekasboda;Fed�mes;Fegyvernek;Feh�rgyarmat;Feh�rt�;Feh�rv�rcsurg�;Feked;Feketeerd�;Felcs�t;Feldebr�;Felgy�;Felp�c;Fels�berecki;Fels�csat�r;Fels�dobsza;Fels�egerszeg;Fels�gagy;Fels�j�nosfa;Fels�kelecs�ny;Fels�lajos;Fels�mar�c;Fels�mocsol�d;Fels�n�na;Fels�ny�r�d;Fels�ny�k;Fels��rs;Fels�p�hok;Fels�pakony;Fels�pet�ny;Fels�rajk;Fels�regmec;Fels�szenterzs�bet;Fels�szentiv�n;Fels�szentm�rton;Fels�sz�ln�k;Fels�t�rk�ny;Fels�telekes;Fels�told;Fels�vad�sz;Fels�zsolca;F�nyeslitke;Feny�f�;Ferencsz�ll�s;Fert�boz;Fert�d;Fert�endr�d;Fert�homok;Fert�r�kos;Fert�szentmikl�s;Fert�sz�plak;Fiad;Filkeh�za;Fityeh�z;Fokt�;Foly�s;Fon�;Fony;Fony�d;Forr�sk�t;Forr�;F�t;F�lde�k;F�ldes;F�nyed;Ful�k�rcs;Furta;F�le;F�lesd;F�l�p;F�l�ph�za;F�l�pjakab;F�l�psz�ll�s;F�lp�sdar�c;F�rged;F�z�r;F�z�rkajata;F�z�rkoml�s;F�z�rradv�ny;F�zesabony;F�zesgyarmat;F�zv�lgy;G�borj�n;G�borj�nh�za;Gacs�ly;Gad�cs;Gad�ny;Gadna;G�doros;Gagyap�ti;Gagyb�tor;Gagyvend�gi;Galambok;Galgaguta;Galgagy�rk;Galgah�v�z;Galgam�csa;G�losfa;Galv�cs;Gam�s;Ganna;G�nt;Gara;Gar�b;Garabonc;Garadna;Garbolc;G�rdony;Gar�;Gasztony;G�t�r;G�vavencsell�;G�berj�n;Gecse;G�derlak;G�g�ny;Gelej;Gel�nes;Gell�nh�za;Gelse;Gelsesziget;Gemzse;Gencsap�ti;G�rce;Gerde;Gerend�s;Ger�nyes;Geresdlak;Gerjen;Gersekar�t;Geszt;Gesztely;Geszter�d;G�tye;Gic;Gige;Gilv�nfa;Girincs;G�g�nfa;Golop;Gomba;Gombosszeg;G�r;Gordisa;Gosztola;G�d;G�d�ll�;G�dre;G�lle;G�m�rsz�l�s;G�nc;G�ncruszka;G�ny�;G�rbeh�za;G�rcs�ny;G�rcs�nydoboka;G�rgeteg;G�sfa;Gr�b�c;Gul�cs;Gutorf�lde;Gy�l;Gyal�ka;Gyan�geregye;Gyarmat;Gy�k�nyes;Gyenesdi�s;Gyep�kaj�n;Gyermely;Gy�d;Gyomaendr�d;Gy�r�;Gy�m�re;Gy�mr�;Gy�ngyfa;Gy�ngy�s;Gy�ngy�sfalu;Gy�ngy�shal�sz;Gy�ngy�smell�k;Gy�ngy�soroszi;Gy�ngy�spata;Gy�ngy�ssolymos;Gy�ngy�starj�n;Gy�nk;Gy�r;Gy�rasszonyfa;Gy�re;Gy�rgytarl�;Gy�rk�ny;Gy�rladam�r;Gy�r�cske;Gy�rs�g;Gy�rs�v�nyh�z;Gy�rszemere;Gy�rtelek;Gy�r�jbar�t;Gy�r�jfalu;Gy�rv�r;Gy�rz�moly;Gyugy;Gyula;Gyulah�za;Gyulaj;Gyulakeszi;Gy�r�;Gy�gye;Gy�re;Gy�r�s;H�cs;Hagy�rosb�r�nd;Hah�t;Hajd�bagos;Hajd�b�sz�rm�ny;Hajd�dorog;Hajd�hadh�z;Hajd�n�n�s;Hajd�s�mson;Hajd�szoboszl�;Hajd�szov�t;Hajm�s;Hajm�sk�r;Haj�s;Halast�;Hal�szi;Hal�sztelek;Halimba;Halmaj;Halmajugra;Halogy;Hang�cs;Hangony;Hantos;Harasztifalu;Harc;Harka;Harkak�t�ny;Hark�ny;H�romfa;H�romhuta;Hars�ny;H�rsk�t;Harta;H�ss�gy;Hatvan;H�derv�r;Hedrehely;Hegyesd;Hegyeshalom;Hegyfalu;Hegyh�thod�sz;Hegyh�tmar�c;Hegyh�ts�l;Hegyh�tszentjakab;Hegyh�tszentm�rton;Hegyh�tszentp�ter;Hegyk�;Hegymagas;Hegymeg;Hegyszentm�rton;H�halom;Hejce;Hej�b�ba;Hej�kereszt�r;Hej�k�rt;Hej�papi;Hej�szalonta;Helesfa;Helv�cia;Hencida;Hencse;Herceghalom;Hercegk�t;Hercegsz�nt�;Her�d;H�reg;Herencs�ny;Herend;Heresznye;Herm�nszeg;Hern�d;Hern�db�d;Hern�dc�ce;Hern�dkak;Hern�dk�rcs;Hern�dn�meti;Hern�dpetri;Hern�dszentandr�s;Hern�dszurdok;Hern�dv�cse;Herny�k;H�t;Hetefej�rcse;Hetes;Hetvehely;Hetyef�;Heves;Hevesaranyos;Hevesvezek�ny;H�v�z;H�v�zgy�rk;Hidas;Hidasn�meti;Hidegk�t;Hidegs�g;Hidv�gard�;Himesh�za;Himod;Hirics;Hobol;Hod�sz;H�dmez�v�s�rhely;Holl�d;Holl�h�za;Holl�k�;Homokb�d�ge;Homokkom�rom;Homokm�gy;Homokszentgy�rgy;Homor�d;Homrogd;Hont;Horp�cs;Hort;Hortob�gy;Horv�thertelend;Horv�tl�v�;Horv�tzsid�ny;Hossz�het�ny;Hossz�p�lyi;Hossz�pereszteg;Hossz�v�z;Hossz�v�lgy;Hoszt�t;Hott�;H�gy�sz;H�vej;Hugyag;Hunya;Hunyadfalva;Huszt�t;Ibafa;Iborfia;Ibr�ny;Igal;Igar;Igrici;Iharos;Iharosber�ny;Ikerv�r;Iklad;Iklanber�ny;Ikl�db�rd�ce;Ikr�ny;Iliny;Ilk;Illocska;Imola;Imrehegy;In�ncs;In�rcs;Inke;Ipacsfa;Ipolydam�sd;Ipolytarn�c;Ipolyt�lgyes;Ipolyvece;Iregszemcse;Irota;Isaszeg;Isp�nk;Istenmezeje;Istv�ndi;Iszkaszentgy�rgy;Iszk�z;Isztim�r;Iv�d;Iv�n;Iv�nbatty�n;Iv�nc;Iv�ncsa;Iv�nd�rda;Izm�ny;Izs�k;Izs�falva;J�g�nak;J�k;Jakabsz�ll�s;J�kfa;J�kfalva;J�k�;J�nd;J�nkmajtis;J�noshalma;J�nosh�za;J�noshida;J�nossomorja;J�rd�nh�za;J�rmi;J�sd;J�sz�g�;J�szals�szentgy�rgy;J�szap�ti;J�sz�roksz�ll�s;J�szber�ny;J�szboldogh�za;J�szd�zsa;J�szfels�szentgy�rgy;J�szf�nyszaru;J�sziv�ny;J�szj�k�halma;J�szkarajen�;J�szkis�r;J�szlad�ny;J�szszentandr�s;J�szszentl�szl�;J�sztelek;J�ke;Jen�;Jobah�za;Jobb�gyi;J�svaf�;Juta;Kaba;Kacorlak;K�cs;Kacs�ta;Kadark�t;Kaj�rp�c;Kaj�sz�;Kajdacs;Kakasd;K�kics;Kakucs;K�l;Kalazn�;K�ld;K�ll�;Kall�sd;K�ll�semj�n;K�lm�ncsa;K�lm�nh�za;K�l�cfa;Kalocsa;K�loz;K�m;Kamond;Kamut;K�n�;K�ntorj�nosi;K�ny;K�nya;K�nyav�r;Kapolcs;K�polna;K�poln�sny�k;Kapoly;Kaposf�;Kaposgyarmat;Kaposhomok;Kaposkereszt�r;Kaposm�r�;Kapospula;Kaposszekcs�;Kaposszerdahely;Kapos�jlak;Kaposv�r;K�ptalanfa;K�ptalant�ti;Kapuv�r;K�ra;Kar�csond;Kar�d;Karak�;Karak�sz�rcs�k;Karancsalja;Karancsber�ny;Karancskeszi;Karancslapujt�;Karancss�g;K�r�sz;Karcag;Karcsa;Kardos;Kardosk�t;Karmacs;K�rolyh�za;Karos;Kartal;K�s�d;Kaskanty�;Kast�lyosdomb�;Kaszaper;Kasz�;Kat�dfa;Katafa;K�toly;Katym�r;K�va;K�v�s;Kaz�r;Kazincbarcika;K�zsm�rk;Kazsok;Kecel;Kecsk�d;Kecskem�t;Kehidakust�ny;K�k;K�kcse;K�ked;K�kesd;K�kk�t;Kelebia;Kel�d;Kelem�r;K�leshalom;Kelev�z;Kemecse;Kemence;Kemendoll�r;Kemenesh�gy�sz;Kemenesk�polna;Kemenesmagasi;Kemenesmih�lyfa;Kemenesp�lfa;Kemeness�mj�n;Kemenesszentm�rton;Kemenesszentp�ter;Kem�nfa;K�mes;Kemestar�dfa;Kemse;Kenderes;Ken�z;Ken�zl�;Kengyel;Kenyeri;Kercaszomor;Kercseliget;Kerecsend;Kerecseny;Kerekegyh�za;Kereki;Ker�kteleki;Kerepes;Kereszt�te;Kerkabarab�s;Kerkafalva;Kerkakutas;Kerk�sk�polna;Kerkaszentkir�ly;Kerkatesk�nd;K�rsemj�n;Kerta;Kert�szsziget;Keszeg;Keszny�ten;Kesz�hidegk�t;Keszthely;Keszt�lc;Kesz�;K�tbodony;K�tegyh�za;K�thely;K�tp�;K�tsoprony;K�t�jfalu;K�tv�lgy;K�ty;Kevermes;Kilim�n;Kimle;Kincsesb�nya;Kir�ld;Kir�lyegyh�za;Kir�lyhegyes;Kir�lyszentistv�n;Kisap�ti;Kisapostag;Kisar;Kisasszond;Kisasszonyfa;Kisbabot;Kisb�gyon;Kisbajcs;Kisbajom;Kisb�rap�ti;Kisb�rk�ny;Kisb�r;Kisber�ny;Kisberzseny;Kisbeszterce;Kisbodak;Kisbucsa;Kisbudm�r;Kiscs�cs;Kiscsehi;Kiscs�sz;Kisd�r;Kisdobsza;Kisdombegyh�z;Kisdorog;Kisecset;Kisfalud;Kisf�zes;Kisg�rb�;Kisgyal�n;Kisgy�r;Kishajm�s;Kishars�ny;Kisharty�n;Kisherend;Kish�dos;Kishuta;Kisigm�nd;Kisjakabfalva;Kiskassa;Kiskinizs;Kiskorp�d;Kisk�re;Kisk�r�s;Kiskunf�legyh�za;Kiskunhalas;Kiskunlach�za;Kiskunmajsa;Kiskutas;Kisl�ng;Kisl�ta;Kislipp�;Kisl�d;Kism�nyok;Kismarja;Kismaros;Kisnam�ny;Kisn�na;Kisn�medi;Kisny�r�d;Kisoroszi;Kispal�d;Kisp�li;Kispirit;Kisr�kos;Kisr�cse;Kisrozv�gy;Kissik�tor;Kissomly�;Kissz�ll�s;Kissz�kely;Kisszekeres;Kisszentm�rton;Kissziget;Kissz�l�s;Kistam�si;Kistapolca;Kistarcsa;Kistelek;Kistokaj;Kistolm�cs;Kistorm�s;Kist�tfalu;Kis�jsz�ll�s;Kisunyom;Kisv�rda;Kisvars�ny;Kisv�s�rhely;Kisvaszar;Kisvejke;Kiszombor;Kiszsid�ny;Kl�rafalva;Kocs;Kocs�r;Kocsola;Kocsord;K�ka;Kokad;Kolont�r;Kom�di;Kom�rom;Komj�ti;Koml�;Koml�dt�tfalu;Koml�sd;Koml�ska;Komor�;Kompolt;Kond�;Kondorfa;Kondoros;K�ny;Kony�r;K�ph�za;Kopp�nysz�nt�;Korl�t;Koronc�;K�r�s;Kosd;K�spallag;K�taj;Kov�cshida;Kov�cssz�n�ja;Kov�csv�g�s;Koz�rd;Koz�rmisleny;Kozmadombja;K�bl�ny;K�csk;K�k�ny;K�k�t;K�lcse;K�lesd;K�lked;K�ml�;K�ml�d;K�m�r�;K�mp�c;K�rmend;K�rnye;K�r�m;K�r�shegy;K�r�slad�ny;K�r�snagyhars�ny;K�r�starcsa;K�r�stet�tlen;K�r�s�jfalu;K�r�sszak�l;K�r�sszegap�ti;K�sz�rhegy;K�szeg;K�szegdoroszl�;K�szegpaty;K�szegszerdahely;K�tcse;K�tegy�n;K�telek;K�v�g��rs;K�v�g�sz�l�s;K�v�g�t�tt�s;K�vegy;K�vesk�l;Krasznokvajda;Kulcs;Kunadacs;Kun�gota;Kunbaja;Kunbaracs;Kuncsorba;Kunfeh�rt�;Kunhegyes;Kunmadaras;Kunpesz�r;Kunsz�ll�s;Kunszentm�rton;Kunszentmikl�s;Kunsziget;Kup;Kupa;Kurd;Kurity�n;Kust�nszeg;Kutas;Kutas�;K�bekh�za;K�ls�s�rd;K�ls�vat;K�ng�s;L�batlan;L�bod;L�cacs�ke;Lad;Lad�nybene;L�dbeseny�;Lajoskom�rom;Lajosmizse;Lak;Lakhegy;Lakitelek;Lak�csa;L�nycs�k;L�paf�;Lap�ncsa;Laskod;Lasztonya;L�tr�ny;L�zi;Le�nyfalu;Le�nyv�r;L�b�ny;Leg�nd;Legyesb�nye;L�h;L�n�rddar�c;Lendvadedes;Lendvajakabfa;Lengyel;Lengyelt�ti;Lenti;Leps�ny;Lesencefalu;Lesenceistv�nd;Lesencetomaj;L�tav�rtes;Letenye;Letk�s;Lev�l;Levelek;Libickozma;Lick�vadamos;Liget;Ligetfalva;Lip�t;Lipp�;Lipt�d;Lispeszentadorj�n;Lisz�;Lit�r;Litka;Litke;L�cs;L�k�t;L�nya;L�r�v;Loth�rd;Lovas;Lovasber�ny;Lov�szhet�ny;Lov�szi;Lov�szpatona;L�k�sh�za;L�rinci;L�v�;L�v�petri;Lucfalva;Lud�nyhal�szi;Ludas;Luk�csh�za;Lulla;L�zsok;M�d;Madaras;Madocsa;Magl�ca;Magl�d;M�gocs;Magosliget;Magy;Magyaralm�s;Magyarat�d;Magyarb�nhegyes;Magyarb�ly;Magyarcsan�d;Magyardombegyh�z;Magyaregregy;Magyaregres;Magyarf�ld;Magyarg�c;Magyargencs;Magyarhertelend;Magyarhomorog;Magyarkereszt�r;Magyarkeszi;Magyarlak;Magyarlukafa;Magyarmecske;Magyarn�dalja;Magyarn�ndor;Magyarpol�ny;Magyarsarl�s;Magyarszecs�d;Magyarsz�k;Magyarszentmikl�s;Magyarszerdahely;Magyarszombatfa;Magyartelek;Majosh�za;Majs;Mak�d;Makkoshotyka;Makl�r;Mak�;Malomsok;M�lyi;M�lyinka;M�nd;M�ndok;M�nfa;M�ny;Mar�za;Marcalgergelyi;Marcali;Marcalt�;M�rfa;M�riahalom;M�riak�lnok;M�riak�m�nd;M�rianosztra;M�riap�cs;Markaz;M�rkh�za;M�rk�;Mark�c;Markotab�d�ge;Mar�c;Mar�csa;M�rok;M�rokf�ld;M�rokpapi;Maroslele;M�rt�ly;Martf�;Martonfa;Martonv�s�r;Martonyi;M�t�szalka;M�t�telke;M�traballa;M�traderecske;M�tramindszent;M�tranov�k;M�traszele;M�traszentimre;M�trasz�l�s;M�traterenye;M�travereb�ly;M�ty�sdomb;Matty;M�tyus;M�za;Mecsekn�dasd;Mecsekp�l�ske;Mecs�r;Medgyesbodz�s;Medgyesegyh�za;Medina;Meggyeskov�csi;Megyasz�;Megyeh�d;Megyer;M�hker�k;M�htelek;Mek�nyes;M�lyk�t;Mencshely;Mende;M�ra;Merenye;M�rges;M�rk;Mernye;Mersev�t;Mesterh�za;Mesteri;Mestersz�ll�s;Meszes;Meszlen;Mesztegny�;Mez�ber�ny;Mez�cs�t;Mez�csokonya;Mez�d;Mez�falva;Mez�gy�n;Mez�hegyes;Mez�h�k;Mez�keresztes;Mez�kom�rom;Mez�kov�csh�za;Mez�k�vesd;Mez�lad�ny;Mez�lak;Mez�nagymih�ly;Mez�ny�r�d;Mez��rs;Mez�peterd;Mez�sas;Mez�szemere;Mez�szentgy�rgy;Mez�szilas;Mez�t�rk�ny;Mez�t�r;Mez�zombor;Mih�ld;Mih�lyfa;Mih�lygerge;Mih�lyh�za;Mih�lyi;Mike;Mikebuda;Mikekar�csonyfa;Mikep�rcs;Mikl�si;Mik�falva;Mik�h�za;Mikossz�plak;Milejszeg;Milota;Mindszent;Mindszentgodisa;Mindszentk�lla;Misefa;Miske;Miskolc;Miszla;Mocsa;Mogyor�d;Mogyor�sb�nya;Mogyor�ska;Moha;Moh�cs;Mohora;Moln�ri;Molnaszecs�d;Molv�ny;Monaj;Monok;Monor;M�nosb�l;Monostorap�ti;Monostorp�lyi;Monoszl�;Monyor�d;M�r;M�r�gy;M�rahalom;M�ricg�t;M�richida;Mosd�s;Mosonmagyar�v�r;Mosonszentmikl�s;Mosonszolnok;Mozsg�;M�cs�ny;Mucsfa;Mucsi;M�csony;Muhi;Murakereszt�r;Murar�tka;Muraszemenye;Murga;Murony;N�br�d;Nadap;N�dasd;N�dasdlad�ny;N�dudvar;N�gocs;Nagyacs�d;Nagyal�sony;Nagyar;Nagyat�d;Nagybajcs;Nagybajom;Nagybak�nak;Nagyb�nhegyes;Nagybaracska;Nagybarca;Nagyb�rk�ny;Nagyber�ny;Nagyberki;Nagyb�rzs�ny;Nagybudm�r;Nagycenk;Nagycs�ny;Nagycs�cs;Nagycsepely;Nagycserkesz;Nagyd�m;Nagydobos;Nagydobsza;Nagydorog;Nagyecsed;Nagy�r;Nagyeszterg�r;Nagyf�ged;Nagygeresd;Nagyg�rb�;Nagygyim�t;Nagyhajm�s;Nagyhal�sz;Nagyhars�ny;Nagyhegyes;Nagyh�dos;Nagyhuta;Nagyigm�nd;Nagyiv�n;Nagyk�ll�;Nagykamar�s;Nagykanizsa;Nagykapornak;Nagykar�csony;Nagyk�ta;Nagykereki;Nagykereszt�r;Nagykinizs;Nagyk�nyi;Nagykorp�d;Nagykov�csi;Nagykoz�r;Nagyk�k�nyes;Nagyk�lked;Nagyk�r�s;Nagyk�r�;Nagykutas;Nagylak;Nagylengyel;Nagyl�c;Nagyl�k;Nagyl�zs;Nagym�gocs;Nagym�nyok;Nagymaros;Nagymizd�;Nagyny�r�d;Nagyoroszi;Nagyp�li;Nagypall;Nagypeterd;Nagypirit;Nagyr�b�;Nagyrada;Nagyr�kos;Nagyr�cse;Nagyr�de;Nagyr�v;Nagyrozv�gy;Nagys�p;Nagysimonyi;Nagyszak�csi;Nagysz�kely;Nagyszekeres;Nagysz�n�s;Nagyszentj�nos;Nagyszokoly;Nagyt�lya;Nagytarcsa;Nagytevel;Nagytilaj;Nagyt�tfalu;Nagyt�ke;Nagy�t;Nagyvars�ny;Nagyv�ty;Nagyv�zsony;Nagyvejke;Nagyveleg;Nagyvenyim;Nagyvisny�;Nak;Napkor;N�rai;Narda;Nasz�ly;N�gyes;Nek�zseny;Nemesap�ti;Nemesbikk;Nemesborzova;Nemesb�d;Nemesb�k;Nemescs�;Nemesd�d;Nemesg�rzs�ny;Nemesgul�cs;Nemeshany;Nemeshet�s;Nemeske;Nemesk�r;Nemeskereszt�r;Nemeskisfalud;Nemeskocs;Nemeskolta;Nemesl�dony;Nemesmedves;Nemesn�dudvar;Nemesn�p;Nemesp�tr�;Nemesr�d�;Nemesrempeholl�s;Nemess�ndorh�za;Nemesszal�k;Nemesszentandr�s;Nemesv�mos;Nemesvid;Nemesvita;N�metb�nya;N�metfalu;N�metk�r;Nemti;Neszm�ly;N�zsa;Nick;Nikla;N�gr�d;N�gr�dk�vesd;N�gr�dmarcal;N�gr�dmegyer;N�gr�ds�p;N�gr�dsipek;N�gr�dszak�l;N�r�p;Noszlop;Noszvaj;Nova;Novaj;Novajidr�ny;N�tincs;Nyalka;Ny�r�d;Ny�regyh�za;Ny�rl�rinc;Ny�rsap�t;Ny�kl�dh�za;Nyerges�jfalu;Ny�sta;Nyim;Ny�r�br�ny;Ny�racs�d;Nyir�d;Ny�radony;Ny�rb�tor;Ny�rb�ltek;Ny�rbog�t;Ny�rbogd�ny;Ny�rcsaholy;Ny�rcs�sz�ri;Ny�rderzs;Ny�regyh�za;Ny�rgelse;Ny�rgyulaj;Ny�ri;Ny�ribrony;Ny�rj�k�;Ny�rkar�sz;Ny�rk�ta;Ny�rk�rcs;Ny�rl�v�;Ny�rlugos;Ny�rmada;Ny�rm�rtonfalva;Ny�rmeggyes;Ny�rmih�lydi;Ny�rparasznya;Ny�rpazony;Ny�rpilis;Ny�rtass;Ny�rtelek;Ny�rt�t;Ny�rtura;Ny�rvasv�ri;Nyom�r;Ny�g�r;Nyugotszenterzs�bet;Ny�l;�b�nya;�barok;�budav�r;�csa;�cs�rd;�falu;�feh�rt�;�f�lde�k;�h�d;Ok�ny;Okor�g;Okorv�lgy;Olasz;Olaszfa;Olaszfalu;Olaszliszka;Olcsva;Olcsvaap�ti;Old;�lmod;Olt�rc;Onga;�nod;�p�lyi;�pusztaszer;Orb�nyosfa;Orci;Ordacsehi;Ordas;Orfalu;Orf�;Orgov�ny;Orm�ndlak;Ormosb�nya;Orosh�za;Oroszi;Oroszl�ny;Oroszl�;Orosztony;Ortah�za;Osli;Ostffyasszonyfa;Ostoros;Oszk�;Oszl�r;Osztop�n;�zd;�zdfalu;Ozm�nb�k;Ozora;�cs;�cs�ny;�cs�d;�k�rit�f�lp�s;�lb�;�mb�ly;�r;�rbotty�n;�regcsert�;�reglak;�rhalom;�rimagyar�sd;�riszentp�ter;�rk�ny;�rm�nyes;�rm�nyk�t;�rtilos;�rv�nyes;�sag�rd;�si;�sk�;�ttev�ny;�tt�m�s;�tv�sk�nyi;P�cin;Pacsa;P�csony;Pad�r;P�hi;P�ka;Pakod;P�kozd;Paks;Pal�;P�lfa;P�lfiszeg;P�lh�za;P�li;Palkonya;P�lmajor;P�lmonostora;Palotabozsok;Palot�s;Paloznak;Paml�ny;Pamuk;P�nd;Pankasz;Pannonhalma;P�nyok;Panyola;Pap;P�pa;P�padereske;P�pakov�csi;P�pasalamon;P�patesz�r;Papkeszi;P�poc;Papos;P�pr�d;Par�d;Par�dsasv�r;Parasznya;Paszab;P�szt�;P�sztori;Pat;Patak;Patalom;Patapoklosi;Patca;P�tka;Patosfa;P�troha;Patvarc;P�ty;P�tyod;P�zm�nd;P�zm�ndfalu;P�cel;Pec�l;P�cs;P�csbagota;P�csdevecser;P�csely;P�csudvard;P�csv�rad;Pell�rd;P�ly;Penc;Pen�szlek;P�nzesgy�r;Penyige;P�r;Perb�l;Pere;Perecse;Pereked;Perenye;Peresznye;Pereszteg;Perk�ta;Perkupa;Per�cs�ny;Peterd;P�terhida;P�teri;P�terv�s�ra;P�tf�rd�;Peth�henye;Petneh�za;Pet�fib�nya;Pet�fisz�ll�s;Pet�h�za;Pet�mih�lyfa;Petrikereszt�r;Petrivente;Pettend;Piliny;Pilis;Pilisborosjen�;Piliscsaba;Piliscs�v;Pilisj�szfalu;Pilismar�t;Pilissz�nt�;Pilisszentiv�n;Pilisszentkereszt;Pilisszentl�szl�;Pilisv�r�sv�r;Pincehely;Pinkamindszent;Pinnye;Piricse;Pirt�;Pisk�;Pitvaros;P�csa;Pocsaj;P�csmegyer;P�cspetri;Pog�ny;Pog�nyszentp�ter;P�kaszepetk;Pol�ny;Polg�r;Polg�rdi;Pom�z;Porcsalma;Porn�ap�ti;Poroszl�;Porp�c;Porrog;Porrogszentkir�ly;Porrogszentp�l;P�rszombat;Porva;P�sfa;Potony;Potyond;P�l�ske;P�l�skef�;P�rb�ly;P�rdef�lde;P�tr�te;Pr�gy;Pula;Pusztaap�ti;Pusztaberki;Pusztacsal�d;Pusztacs�;Pusztadobos;Pusztaederics;Pusztafalu;Pusztaf�ldv�r;Pusztahencse;Pusztakov�csi;Pusztamagyar�d;Pusztam�rges;Pusztamiske;Pusztamonostor;Pusztaottlaka;Pusztaradv�ny;Pusztaszabolcs;Pusztaszemes;Pusztaszentl�szl�;Pusztaszer;Pusztavacs;Pusztav�m;Pusztaz�mor;Putnok;P�ski;P�sp�khatvan;P�sp�klad�ny;P�sp�kmoln�ri;P�sp�kszil�gy;R�bacsanak;R�bacs�cs�ny;R�bagyarmat;R�bah�dv�g;R�bakec�l;R�bapatona;R�bapaty;R�bapord�ny;R�basebes;R�baszentandr�s;R�baszentmih�ly;R�baszentmikl�s;R�batam�si;R�bat�tt�s;R�bcakapi;R�calm�s;R�ckereszt�r;R�ckeve;R�d;R�dfalva;R�d�ck�lked;Radosty�n;Rag�ly;Rajka;Rakaca;Rakacaszend;Rakamaz;R�k�czib�nya;R�k�czifalva;R�k�czi�jfalu;R�ksi;Ramocsa;Ramocsah�za;R�polt;Raposka;R�sonys�pberencs;R�tka;R�t�t;Ravazd;Recsk;R�de;R�dics;Reg�c;Regenye;Reg�ly;R�m;Remetesz�l�s;R�p�shuta;R�pcelak;R�pceszemere;R�pceszentgy�rgy;R�pcevis;Resznek;R�talap;R�tk�zberencs;R�ts�g;R�vf�l�p;R�vle�nyv�r;Rezi;Ricse;Rig�cs;Rigy�c;Rim�c;Rinyabeseny�;Rinyakov�csi;Rinyaszentkir�ly;Rinya�jlak;Rinya�jn�p;Rohod;Rom�nd;Romh�ny;Romonya;R�zsafa;Rozs�ly;R�zsaszentm�rton;R�jt�kmuzsaj;R�n�k;R�szke;Rudab�nya;Rudolftelep;Rum;Ruzsa;S�g�jfalu;S�gv�r;Saj�b�bony;Saj�ecseg;Saj�galg�c;Saj�h�dv�g;Saj�iv�nka;Saj�k�polna;Saj�kaza;Saj�kereszt�r;Saj�l�d;Saj�l�szl�falva;Saj�mercse;Saj�n�meti;Saj��r�s;Saj�p�lfala;Saj�petri;Saj�p�sp�ki;Saj�senye;Saj�szentp�ter;Saj�sz�ged;Saj�v�mos;Saj�velezd;Sajtosk�l;Salf�ld;Salg�tarj�n;Salk�vesk�t;Salomv�r;S�ly;S�mod;S�msonh�za;Sand;S�ndorfalva;S�ntos;S�p;S�r�nd;S�razsad�ny;S�rbog�rd;S�regres;S�rfimizd�;S�rhida;S�ris�p;Sarkad;Sarkadkereszt�r;S�rkeresztes;S�rkereszt�r;S�rkeszi;S�rmell�k;S�rok;S�rosd;S�rospatak;S�rpilis;S�rr�tudvari;Sarr�d;S�rszent�gota;S�rszentl�rinc;S�rszentmih�ly;Sarud;S�rv�r;S�sd;S�ska;S�ta;S�toralja�jhely;S�torhely;S�voly;S�;Segesd;Sellye;Selyeb;Semj�n;Semj�nh�za;S�nye;S�ny�;Sereg�lyes;Ser�nyfalva;S�rseksz�l�s;Sik�tor;Sikl�s;Sikl�sbodony;Sikl�snagyfalu;Sima;Simas�g;Simonfa;Simontornya;Si�ag�rd;Si�fok;Si�jut;Sirok;Sitke;Sobor;Sokor�p�tka;Solt;Soltszentimre;Soltvadkert;S�ly;Solym�r;Som;Somberek;Soml�jen�;Soml�sz�l�s;Soml�v�s�rhely;Soml�vecse;Somodor;Somogyacsa;Somogyap�ti;Somogyaracs;Somogyaszal�;Somogybabod;Somogyb�kk�sd;Somogycsics�;Somogyd�r�cske;Somogyegres;Somogyfajsz;Somogygeszti;Somogyh�rs�gy;Somogyhatvan;Somogyj�d;Somogymeggyes;Somogys�mson;Somogys�rd;Somogysimonyi;Somogyszentp�l;Somogyszil;Somogyszob;Somogyt�r;Somogyudvarhely;Somogyv�mos;Somogyv�r;Somogyviszl�;Somogyzsitfa;Sonk�d;Soponya;Sopron;Sopronhorp�cs;Sopronk�vesd;Sopronn�meti;Sorkifalud;Sorkik�polna;Sorm�s;Sorokpol�ny;S�sharty�n;S�sk�t;S�st�falva;S�svertike;S�tony;S�jt�r;S�pte;S�r�d;Sukor�;Sumony;S�r;Surd;S�k�sd;S�lys�p;S�meg;S�megcsehi;S�megpr�ga;S�tt�;Szabadbatty�n;Szabadegyh�za;Szabadh�dv�g;Szabadi;Szabadk�gy�s;Szabadsz�ll�s;Szabadszentkir�ly;Szab�s;Szabolcs;Szabolcsb�ka;Szabolcsveresmart;Szada;Sz�gy;Szajk;Szajla;Szajol;Szak�csi;Szakad�t;Szak�ld;Szak�ly;Szakcs;Szakm�r;Szakny�r;Szakoly;Szakony;Szakonyfalu;Sz�kszend;Szalaf�;Szal�nta;Szalapa;Szalaszend;Szalatnak;Sz�lka;Szalkszentm�rton;Szalmatercs;Szalonna;Szamosangyalos;Szamosbecs;Szamosk�r;Szamoss�lyi;Szamosszeg;Szamostat�rfalva;Szamos�jlak;Szanda;Szank;Sz�nt�d;Szany;Sz�p�r;Szaporca;Sz�r;Sz�r�sz;Sz�razd;Sz�rf�ld;Sz�rliget;Szarvas;Szarvasgede;Szarvaskend;Szarvask�;Sz�szberek;Sz�szfa;Sz�szv�r;Szatm�rcseke;Sz�tok;Szatta;Szatymaz;Szava;Sz�zhalombatta;Szeb�ny;Sz�cs�nke;Sz�cs�ny;Sz�cs�nyfelfalu;Sz�csisziget;Szederk�ny;Szedres;Szeged;Szegerd�;Szeghalom;Szegi;Szegilong;Szegv�r;Sz�kely;Sz�kelyszabar;Sz�kesfeh�rv�r;Sz�kkutas;Szeksz�rd;Szeleste;Szelev�ny;Szell�;Szemely;Szemenye;Szemere;Szendehely;Szendr�;Szendr�l�d;Szenna;Szenta;Szentantalfa;Szentbal�zs;Szentb�kk�lla;Szentborb�s;Szentd�nes;Szentdomonkos;Szente;Szenteg�t;Szentendre;Szentes;Szentg�l;Szentg�losk�r;Szentgotth�rd;Szentgy�rgyv�r;Szentgy�rgyv�lgy;Szentimrefalva;Szentistv�n;Szentistv�nbaksa;Szentjakabfa;Szentkatalin;Szentkir�ly;Szentkir�lyszabadja;Szentkozmadombja;Szentl�szl�;Szentliszl�;Szentl�rinc;Szentl�rinck�ta;Szentmargitfalva;Szentm�rtonk�ta;Szentp�terfa;Szentp�terf�lde;Szentp�terszeg;Szentp�ter�r;Szeny�r;Szepetnek;Szerecseny;Szeremle;Szerencs;Szerep;Szerg�ny;Szigetbecse;Szigetcs�p;Szigethalom;Szigetmonostor;Szigetszentm�rton;Szigetszentmikl�s;Sziget�jfalu;Szigetv�r;Szigliget;Szihalom;Szij�rt�h�za;Sziksz�;Szil;Szil�gy;Szilaspogony;Szils�rk�ny;Szilv�gy;Szilv�s;Szilv�sv�rad;Szilv�sszentm�rton;Szin;Szinpetri;Szir�k;Szirmabeseny�;Szob;Szokolya;Sz�l�d;Szolnok;Szombathely;Szom�d;Szomolya;Szomor;Szorgalmatos;Szorosad;Sz�c;Sz�ce;Sz�d;Sz�dliget;Sz�gliget;Sz�ke;Sz�k�d;Sz�kedencs;Sz�l�sard�;Sz�l�sgy�r�k;Sz�r�ny;Sz�cs;Szuha;Szuhaf�;Szuhak�ll�;Szuhogy;Szulim�n;Szulok;Szurdokp�sp�ki;Sz�csi;Sz�gy;Sz�r;Tab;Tabajd;Tabdi;T�borfalva;T�c;Tagyon;Tahit�tfalu;Tak�csi;T�kos;Taksony;Taktab�j;Taktahark�ny;Taktaken�z;Taktaszada;Tali�nd�r�gd;T�llya;Tam�si;Tanakajd;T�p;T�pi�bicske;T�pi�gy�rgye;T�pi�s�g;T�pi�szecs�;T�pi�szele;T�pi�szentm�rton;T�pi�sz�l�s;T�pl�nszentkereszt;Tapolca;Tapsony;T�pszentmikl�s;Tar;Tarany;Tarcal;Tard;Tardona;Tardos;Tarhos;Tarj�n;Tarj�npuszta;T�rk�ny;Tarnabod;Tarnalelesz;Tarnam�ra;Tarna�rs;Tarnaszentm�ria;Tarnaszentmikl�s;Tarnazsad�ny;T�rnok;T�rnokr�ti;Tarpa;Tarr�s;T�ska;Tass;Tasz�r;T�t;Tata;Tatab�nya;Tatah�za;Tat�rszentgy�rgy;T�zl�r;T�gl�s;T�kes;Teklafalu;Telekes;Telekgerend�s;Teleki;Telki;Telkib�nya;Tengelic;Tengeri;Teng�d;Tenk;T�ny�;T�pe;Terem;Ter�ny;Tereske;Teresztenye;Terpes;T�s;T�sa;T�senfa;T�seny;Tesk�nd;T�t;Tet�tlen;Tevel;Tibolddar�c;Tiborsz�ll�s;Tihany;Tikos;Tilaj;Tim�r;Tinnye;Tiszaadony;Tiszaalp�r;Tiszab�bolna;Tiszabecs;Tiszabercel;Tiszabezd�d;Tiszab�;Tiszabura;Tiszacs�cse;Tiszacsege;Tiszacsermely;Tiszadada;Tiszaderzs;Tiszadob;Tiszadorogma;Tiszaeszl�r;Tiszaf�ldv�r;Tiszaf�red;Tiszagyenda;Tiszagyulah�za;Tiszaigar;Tiszainoka;Tiszajen�;Tiszakany�r;Tiszakar�d;Tiszak�cske;Tiszakerecseny;Tiszakeszi;Tiszak�r�d;Tiszak�rt;Tiszalad�ny;Tiszal�k;Tiszal�c;Tiszamogyor�s;Tiszanagyfalu;Tiszan�na;Tisza�rs;Tiszapalkonya;Tiszap�sp�ki;Tiszar�d;Tiszaroff;Tiszasas;Tiszas�ly;Tiszaszalka;Tiszaszentimre;Tiszaszentm�rton;Tiszasziget;Tiszasz�l�s;Tiszatardos;Tiszatarj�n;Tiszatelek;Tiszateny�;Tiszaug;Tisza�jv�ros;Tiszavalk;Tiszav�rkony;Tiszavasv�ri;Tiszavid;Tisztaberek;Tivadar;T�alm�s;T�falu;T�fej;T�f�;Tokaj;Tokod;Tokodalt�r�;Tokorcs;Tolcsva;Told;Tolm�cs;Tolna;Tolnan�medi;Tomajmonostora;Tomor;Tompa;Tompal�dony;Tordas;Tormaf�lde;Torm�s;Torm�sliget;Tornabarakony;Tornak�polna;Tornan�daska;Tornaszentandr�s;Tornaszentjakab;Tornyiszentmikl�s;Tornyosn�meti;Tornyosp�lca;Torony;Torvaj;T�szeg;T�tkoml�s;T�tszentgy�rgy;T�tszentm�rton;T�tszerdahely;T�t�jfalu;T�tv�zsony;T�k;T�k�l;T�lt�stava;T�m�rd;T�m�rk�ny;T�r�kb�lint;T�r�kkopp�ny;T�r�kszentmikl�s;T�rtel;T�tt�s;Trizs;Tunyogmatolcs;Tura;T�ristv�ndi;T�rkeve;T�rony;T�rricse;Tuzs�r;T�rje;T�skev�r;Tyukod;Udvar;Udvari;Ugod;�jbarok;�jcsan�los;�jdombr�d;�jfeh�rt�;�jharty�n;�jir�z;�jireg;�jken�z;�jk�r;�jk�gy�s;�jlengyel;�jl�ta;�jl�rincfalva;�jpetre;�jr�naf�;�jsolt;�jszalonta;�jsz�sz;�jszentiv�n;�jszentmargita;�jszilv�s;�jtelek;�jtikos;�judvar;�jv�rfalva;Ukk;Und;�ny;Uppony;Ura;Urai�jfalu;�rhida;�ri;�rk�t;Uszka;Usz�d;Uzsa;�ll�s;�ll�;�r�m;V�c;V�cduka;V�cegres;V�charty�n;V�ckis�jfalu;V�cr�t�t;V�cszentl�szl�;Vadna;Vadosfa;V�g;V�g�shuta;Vaja;Vajd�cska;Vajszl�;Vajta;V�l;Valk�;Valkonya;V�llaj;V�llus;V�mosatya;V�moscsal�d;V�mosgy�rk;V�mosmikola;V�mosoroszi;V�mosp�rcs;V�mos�jfalu;V�mosszabadi;V�ncsod;Vanyarc;Vanyola;V�rad;V�ralja;Var�szl�;V�rasz�;V�rbalog;Varb�;Varb�c;V�rda;V�rdomb;V�rf�lde;Varga;V�rgesztes;V�rkesz�;V�rong;V�rosf�ld;V�rosl�d;V�rpalota;Vars�d;Vars�ny;V�rv�lgy;Vasad;Vasalja;V�s�rosb�c;V�s�rosdomb�;V�s�rosfalu;V�s�rosmiske;V�s�rosnam�ny;Vasasszonyfa;Vasboldogasszony;Vasegerszeg;Vashossz�falu;Vaskeresztes;Vask�t;Vasmegyer;Vasp�r;Vassur�ny;Vassz�cseny;Vasszentmih�ly;Vasszilv�gy;Vasv�r;Vaszar;V�szoly;V�t;Vatta;V�zsnok;V�cs;Vecs�s;V�gegyh�za;Vejti;V�k�ny;Vekerd;Velem;Velem�r;Velence;Vel�ny;V�m�nd;V�nek;V�p;Vereb;Veresegyh�z;Ver�ce;Verpel�t;Verseg;Versend;V�rtesacsa;V�rtesbogl�r;V�rteskethely;V�rtessoml�;V�rtestolna;V�rtessz�l�s;V�se;Veszk�ny;Veszpr�m;Veszpr�mfajsz;Veszpr�mgalsa;Veszpr�mvars�ny;V�szt�;Vezseny;Vid;Vig�ntpetend;Vill�ny;Vill�nyk�vesd;Vilm�ny;Vilonya;Vilyvit�ny;Vin�r;Vindornyafok;Vindornyalak;Vindornyasz�l�s;Visegr�d;Visnye;Visonta;Viss;Visz;Visz�k;Viszl�;Visznek;Vitny�d;V�zv�r;Vizsl�s;Vizsoly;Vok�ny;Vonyarcvashegy;V�ck�nd;V�lcsej;V�n�ck;V�r�st�;V�rs;Zabar;Z�dor;Z�dorfalva;Zagyvar�kas;Zagyvasz�nt�;Z�hony;Zajk;Zajta;Z�k�ny;Z�k�nyfalu;Z�k�nysz�k;Zala;Zalaap�ti;Zalabaksa;Zalab�r;Zalaboldogfa;Zalacs�ny;Zalacs�b;Zalaegerszeg;Zalaerd�d;Zalagy�m�r�;Zalahal�p;Zalah�sh�gy;Zalaigrice;Zalaistv�nd;Zalakaros;Zalakom�r;Zalak�vesk�t;Zalal�v�;Zalameggyes;Zalamerenye;Zalas�rszeg;Zalaszabar;Zalasz�nt�;Zalaszegv�r;Zalaszentbal�zs;Zalaszentgr�t;Zalaszentgy�rgy;Zalaszentiv�n;Zalaszentjakab;Zalaszentl�szl�;Zalaszentl�rinc;Zalaszentm�rton;Zalaszentmih�ly;Zalaszombatfa;Zal�ta;Zalat�rnok;Zala�jlak;Zalav�r;Zalav�g;Zalkod;Zam�rdi;Z�moly;Z�nka;Zar�nk;Z�vod;Zebecke;Zebeg�ny;Zempl�nag�rd;Zeng�v�rkony;Zichy�jfalu;Zics;Ziliz;Zim�ny;Zirc;Z�k;Zomba;Zsad�ny;Zs�ka;Zs�mb�k;Zs�mbok;Zsana;Zsaroly�n;Zsebeh�za;Zs�deny;Zselickisfalud;Zselickislak;Zselicszentp�l;Zsennye;Zsira;Zsomb�;Zsujta;Zsurk;Zubogy;F�v�rosi �nkorm�nyzat;Baranya Megyei �nkorm�nyzat;B�cs-Kiskun Megyei �nkorm�nyzat;B�k�s Megyei �nkorm�nyzat;Borsod-Aba�j-Zempl�n Megyei �nkorm�nyzat;Csongr�d Megyei �nkorm�nyzat;Fej�r Megyei �nkorm�nyzat;Gy�r-Moson-Sopron Megyei �nkorm�nyzat;Hajd�-Bihar Megyei �nkorm�nyzat;Heves Megyei �nkorm�nyzat;Kom�rom-Esztergom Megyei �nkorm�nyzat;N�gr�d Megyei �nkorm�nyzat;Pest Megyei �nkorm�nyzat;Somogy Megyei �nkorm�nyzat;Szabolcs-Szatm�r-Bereg Megyei �nkorm�nyzat;J�sz-Nagykun-Szolnok Megyei �nkorm�nyzat;Tolna Megyei �nkorm�nyzat;Vas Megyei �nkorm�nyzat;Veszpr�m Megyei �nkorm�nyzat;Zala Megyei �nkorm�nyzat";
    private const string ResouceName_WorkSheetName_Ugyiratforgalom = "HatosagiStatisztika_WorkSheetName_Ugyiratforgalom";
    private const string ResouceName_WorkSheetName_Onkormanyzat = "HatosagiStatisztika_WorkSheetName_Onkormanyzat";
    private const string ResouceName_WorkSheetName_Allamigazgatas = "HatosagiStatisztika_WorkSheetName_Allamigazgatas";

    private const string ResouceName_TemplateFileName_Ugyiratforgalom = "HatosagiStatisztika_TemplateFileName_Ugyiratforgalom";
    private const string ResouceName_TemplateFileName_Onkormanyzat = "HatosagiStatisztika_TemplateFileName_Onkormanyzat";
    private const string ResouceName_TemplateFileName_Allamigazgatas = "HatosagiStatisztika_TemplateFileName_Allamigazgatas";

    private const string ResouceName_TemplatePath = "HatosagiStatisztika_TemplatePath";

    private const string ResouceBaseName_TargetCell_Ugyiratforgalom = "HatosagiStatisztika_TargetCell_Ugyiratforgalom";
    private const string ResouceBaseName_TargetCell_Onkormanyzat = "HatosagiStatisztika_TargetCell_Onkormanyzat";
    private const string ResouceBaseName_TargetCell_Allamigazgatas = "HatosagiStatisztika_TargetCell_Allamigazgatas";

    private const string ResouceExtendingName_TargetCell_Szekhely = "_Szekhely";
    private const string ResouceExtendingName_TargetCell_Kelt = "_Kelt";
    private const string ResouceExtendingName_TargetCell_Rogzitette = "_Rogzitette";


    #region Utils
    private string GetResourceWithOrWithoutExtension(string ResourceName, string Extension)
    {
        string str = String.Empty;
        if (!String.IsNullOrEmpty(Resources.Templates.ResourceManager.GetString(ResourceName + Extension)))
        {
            str = Resources.Templates.ResourceManager.GetString(ResourceName + Extension);
        }
        else if (!String.IsNullOrEmpty(Resources.Templates.ResourceManager.GetString(ResourceName)))
        {
            str = Resources.Templates.ResourceManager.GetString(ResourceName);
        }
        else
        {
            throw new Exception(Resources.Error.UI_ResourceNotFound + " (" + ResourceName + ")");
        }

        return str;
    }

    #endregion Utils

    public bool IsFelEvVisible
    {
        get
        {
            return IsAfter2014;
        }
    }

    public bool IsDateIntervalVisible
    {
        get
        {
            return IsAfter2014;
        }
    }

    public int SelectedYear
    {
        get
        {
            if (!String.IsNullOrEmpty(ddownEv.SelectedValue))
            {
                int i;

                if(Int32.TryParse(ddownEv.SelectedValue, out i))
                {
                    return i;
                }
            }

            return DateTime.Now.Year;
        }
    }

    public bool IsAfter2014
    {
        get
        {
            int year = SelectedYear;

            return year > 2014;
        }
    }

    public int SelectedFelEv
    {
        get
        {
            if (IsFelEvVisible)
            {
                if (!String.IsNullOrEmpty(ddownFelEv.SelectedValue))
                {
                    int i;
                    if (Int32.TryParse(ddownFelEv.SelectedValue, out i))
                    {
                        return i;
                    }
                }
            }

            return 0;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "HatosagiStatisztika");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        Page.Server.ScriptTimeout = 600;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
        //LoadData();

        if (!String.IsNullOrEmpty(Resources.Templates.ResourceManager.GetString(ResouceName_TemplatePath)))
        {
            templatePath = Resources.Templates.ResourceManager.GetString(ResouceName_TemplatePath);
        }

        int thisYear = System.DateTime.Now.Year;

        if (szekhelyekDictionary == null)
        {
            szekhelyekDictionary = new Dictionary<int, ListItemCollection>();

            for (int i = thisYear; i > thisYear - 5; i--)
            {
                if (!szekhelyekDictionary.ContainsKey(i))
                {
                    string szekhelyekParameter = Rendszerparameterek.Get(Page, Rendszerparameterek.HATOSAGI_STATISZTIKA_ONKORMANYZAT + i.ToString());
                    if (!String.IsNullOrEmpty(szekhelyekParameter))
                    {
                        string[] szekhelyekEv = szekhelyekParameter.Split(targetSeparators.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);
                        ListItemCollection itemsByYear = new ListItemCollection();

                        itemsByYear.AddRange(Array.ConvertAll<string, ListItem>(szekhelyekEv, ListItem.FromString));
                        szekhelyekDictionary.Add(i, itemsByYear);
                    }
                }
            }

            // l�trehozunk egy 0 index� t�telt a k�l�n nem jegyzett �vekre
            ListItemCollection items = new ListItemCollection();
            string szekhelyek = Rendszerparameterek.Get(Page, Rendszerparameterek.HATOSAGI_STATISZTIKA_ONKORMANYZAT);
            if (!String.IsNullOrEmpty(szekhelyek))
            {
                string[] szekhelyekEv = szekhelyek.Split(targetSeparators.ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);
                items.AddRange(Array.ConvertAll<string, ListItem>(szekhelyekEv, ListItem.FromString));
            }
            else
            {
                items.Add(Resources.Error.UIDropDownListFillingError);
            }

            if (!szekhelyekDictionary.ContainsKey(0))
            {
                szekhelyekDictionary.Add(0, items);
            }

        }

        if (!IsPostBack)
        {
            for (int i = thisYear; i > thisYear - 5; i--)
            {
                ddownEv.Items.Add(i.ToString());
            }

            ddownEv_SelectedIndexChanged(ddownEv, new EventArgs());
            TextBoxRogzitette.Text = Contentum.eAdmin.Utility.eAdminService.GetFelhasznaloNevById(FelhasznaloProfil.FelhasznaloId(Page), Page);
        }

        ddownEv.AutoPostBack = true;
        ddownEv.SelectedIndexChanged += new EventHandler(ddownEv_SelectedIndexChanged);
        ddownFelEv.SelectedIndexChanged += new EventHandler(ddownFelEv_SelectedIndexChanged);
    }

    void ddownFelEv_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateDateInterval();
    }

    void ddownEv_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedSzekhely = ddownSzekhely.SelectedValue;
        ddownSzekhely.Items.Clear();
        int selectedYear;

        ListItemCollection items = new ListItemCollection();
        if (Int32.TryParse(ddownEv.SelectedValue, out selectedYear))
        {
            int key = 0;
            if (szekhelyekDictionary.ContainsKey(selectedYear))
            {
                key = selectedYear;
            }
            if (szekhelyekDictionary.TryGetValue(key, out items))
            {
                if (items.Count > 0)
                {
                    ListItem[] itemArray = new ListItem[items.Count];
                    items.CopyTo(itemArray, 0);
                    ddownSzekhely.Items.AddRange(itemArray);

                    if (!String.IsNullOrEmpty(selectedSzekhely))
                    {
                        ListItem selectedItem = ddownSzekhely.Items.FindByValue(selectedSzekhely);
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = true;
                        }
                    }
                }
            }
            else
            {
                ddownSzekhely.Items.Add(Resources.Error.UIDropDownListFillingError);
            }
        }
        else
        {
            ddownSzekhely.Items.Add(Resources.Error.UIDropDownListFillingError);
        }

        UpdateDateInterval();

    }

    void UpdateDateInterval()
    {
        DateTime start;
        DateTime end;
        GetDateInterval(out start, out end);
        DatumIntervallumSearch.DatumKezd = start.ToString();
        DatumIntervallumSearch.DatumVege = end.AddDays(-1).ToString();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        //ne legyen letiltva a "Rendben" gomb (lehet t�bsz�r is klikkelni)
        // egyel�re m�gsem haszn�ljuk, mert a hibapanel nem t�nik el
        FormFooter1.ImageButton_Save.OnClientClick = "";

        ddownFelEv.Visible = IsFelEvVisible;
        DatumIntervallumSearch.Visible = IsFelEvVisible;

    }

    private static readonly DateTime FelEvDate = new DateTime(DateTime.Now.Year, 7, 1);

    private void GetDateInterval(out DateTime startDate, out DateTime endDate)
    {
        int year = SelectedYear;

        int felEv = SelectedFelEv;

        if (SelectedFelEv == 1)
        {
            startDate = new DateTime(year, 1, 1);
            endDate = new DateTime(year, FelEvDate.Month, FelEvDate.Day);
        }
        else if (SelectedFelEv == 2)
        {
            startDate = new DateTime(year, FelEvDate.Month, FelEvDate.Day);
            endDate = new DateTime(year + 1, 1, 1);
        }
        else
        {
            startDate = new DateTime(year, 1, 1);
            endDate = new DateTime(year + 1, 1, 1);
        }
    }

    private void GetSelectedDateInterval(out DateTime startDate, out DateTime endDate)
    { 
        DateTime _start;
        DateTime _end;
        GetDateInterval(out _start, out _end);

        if (IsDateIntervalVisible)
        {
            if (!String.IsNullOrEmpty(DatumIntervallumSearch.DatumKezd))
            {
                DateTime d;

                if (DateTime.TryParse(DatumIntervallumSearch.DatumKezd, out d))
                {
                    _start = d;
                }
            }

            if (!String.IsNullOrEmpty(DatumIntervallumSearch.DatumVege))
            {
                DateTime d;

                if (DateTime.TryParse(DatumIntervallumSearch.DatumVege, out d))
                {
                    _end = new DateTime(d.Year, d.Month, d.Day).AddDays(1);
                }
            }
        }

        startDate = _start;
        endDate = _end;
    }

    private string GetResourceExtension()
    {
        int year = SelectedYear;

        int felEv = SelectedFelEv;

        if (felEv > 0)
        {
            return String.Format("{0}_{1}", year, felEv);
        }
        else
        {
            return year.ToString();
        }
    }

    public void LoadData()
    {
        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        service.Timeout = (int)TimeSpan.FromMinutes(10).TotalMilliseconds;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        //System.DateTime execDateStartYear = new DateTime(int.Parse(ddownEv.SelectedValue), 1, 1);
        //System.DateTime execDateEndYear = execDateStartYear.AddYears(1);

        System.DateTime execDateStartYear;
        System.DateTime execDateEndYear;

        GetSelectedDateInterval(out execDateStartYear, out execDateEndYear);

        string kezdDatum = execDateStartYear.ToString();
        string vegeDatum = execDateEndYear.ToString();

        Result result;

        string statisztikaTipus = rbTipus.SelectedValue;

        string resourceExt = GetResourceExtension();
        try
        {
            switch (statisztikaTipus)
            {
                case "Ugyiratforgalom":
                    //worksheetName = "�nkorm�nyzat_sz�khely";
                    worksheetName = GetResourceWithOrWithoutExtension(ResouceName_WorkSheetName_Ugyiratforgalom, resourceExt);

                    //templateFileName = "HatosagiStatisztikaUgyiratforgalom.xls";
                    templateFileName = GetResourceWithOrWithoutExtension(ResouceName_TemplateFileName_Ugyiratforgalom, resourceExt);

                    #region parenttable cell�k
                    targetCell_Szekhely = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Ugyiratforgalom + ResouceExtendingName_TargetCell_Szekhely, resourceExt);
                    targetCell_Kelt = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Ugyiratforgalom + ResouceExtendingName_TargetCell_Kelt, resourceExt);
                    targetCell_Rogzitette = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Ugyiratforgalom + ResouceExtendingName_TargetCell_Rogzitette, resourceExt);
                    #endregion parenttable cell�k
                    //parentTableCells = worksheetName + ",A2,Szekhely;" + worksheetName + ",C50,Kelt;" + worksheetName + ",C53,Rogzitette";

                    result = service.GetAllForHatosagiStatisztikaUgyiratforgalom(execParam, kezdDatum, vegeDatum, worksheetName);
                    break;
                case "Onkormanyzat":
                    //worksheetName = "�korm�nyzat_sz�khelye"; // sic! (hib�s az Excelben)
                    worksheetName = GetResourceWithOrWithoutExtension(ResouceName_WorkSheetName_Onkormanyzat, resourceExt);

                    //templateFileName = "HatosagiStatisztikaOnkormanyzat.xls";
                    templateFileName = GetResourceWithOrWithoutExtension(ResouceName_TemplateFileName_Onkormanyzat, resourceExt);

                    #region parenttable cell�k
                    targetCell_Szekhely = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Onkormanyzat + ResouceExtendingName_TargetCell_Szekhely, resourceExt);
                    targetCell_Kelt = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Onkormanyzat + ResouceExtendingName_TargetCell_Kelt, resourceExt);
                    targetCell_Rogzitette = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Onkormanyzat + ResouceExtendingName_TargetCell_Rogzitette, resourceExt);
                    #endregion parenttable cell�k
                    //parentTableCells = worksheetName + ",AK2,Szekhely;" + worksheetName + ",C48,Kelt;" + worksheetName + ",AK49,Rogzitette";

                    result = service.GetAllForHatosagiStatisztikaOnkormanyzat(execParam, kezdDatum, vegeDatum, worksheetName);
                    break;
                case "Allamigazgatas":
                    //worksheetName = "�korm�nyzat_sz�khelye"; // sic! (hib�s az Excelben)
                    worksheetName = GetResourceWithOrWithoutExtension(ResouceName_WorkSheetName_Allamigazgatas, resourceExt);

                    //templateFileName = "HatosagiStatisztikaAllamigazgatas.xls";
                    templateFileName = GetResourceWithOrWithoutExtension(ResouceName_TemplateFileName_Allamigazgatas, resourceExt);

                    #region parenttable cell�k
                    targetCell_Szekhely = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Allamigazgatas + ResouceExtendingName_TargetCell_Szekhely, resourceExt);
                    targetCell_Kelt = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Allamigazgatas + ResouceExtendingName_TargetCell_Kelt, resourceExt);
                    targetCell_Rogzitette = GetResourceWithOrWithoutExtension(ResouceBaseName_TargetCell_Allamigazgatas + ResouceExtendingName_TargetCell_Rogzitette, resourceExt);
                    #endregion parenttable cell�k
                    //parentTableCells = worksheetName + ",AR2,Szekhely;" + worksheetName + ",C48,Kelt;" + worksheetName + ",AP49,Rogzitette";

                    result = service.GetAllForHatosagiStatisztikaAllamigazgatas(execParam, kezdDatum, vegeDatum, worksheetName);
                    break;
                default:
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.UI_MissingStatisticsType);
                    ErrorUpdatePanel.Update();
                    return;
                //break;
            }
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, ex.Message);
            ErrorUpdatePanel.Update();
            return;
        }

        parentTableCells = worksheetName + String.Format(",{0},Szekhely;", targetCell_Szekhely)
            + worksheetName + String.Format(",{0},Kelt;", targetCell_Kelt)
            + worksheetName + String.Format(",{0},Rogzitette", targetCell_Rogzitette);

        if (!String.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel.Update();
        }
        else
        {

            try
            {
                //string xml = result.Ds.GetXml();
                //Response.Write(xml);

                FileStream fs;
                BinaryReader br;
                byte[] fileRD;
                string pathToExcelTemplate;
                try
                {
                    pathToExcelTemplate = Server.MapPath(templatePath + templateFileName);
                    fs = new FileStream(pathToExcelTemplate, FileMode.Open, FileAccess.Read);
                    br = new BinaryReader(fs);

                    fileRD = br.ReadBytes((int)fs.Length);

                    br.Close();
                    fs.Close();
                }
                catch (Exception ex)
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, ex.GetType().ToString() + ": " + ex.Message);
                    ErrorUpdatePanel.Update();
                    return;
                }

                DataTable table = new DataTable("ParentTable");
                DataColumn column;
                DataRow row;
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = targetCellsColumnName; // "Cells"
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Szekhely";
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Kelt";
                table.Columns.Add(column);
                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "Rogzitette";
                table.Columns.Add(column);

                // adatok
                row = table.NewRow();
                //row[targetCellsColumnName] = worksheetName + ",AR2,Szekhely;" + worksheetName + ",C48,Kelt;" + worksheetName + ",AP49,Rogzitette";
                row[targetCellsColumnName] = parentTableCells;
                row["Szekhely"] = ddownSzekhely.SelectedValue;
                row["Kelt"] = System.DateTime.Now.ToLongDateString();
                row["Rogzitette"] = TextBoxRogzitette.Text;
                table.Rows.Add(row);

                // hozz�ad�sa DataSethez
                result.Ds.Tables.Add(table);

                // Excel kit�lt�se
                Contentum.eTemplateManager.Service.TemplateManagerService tms = eTemplateManagerService.ServiceFactory.GetTemplateManagerService();
                result = tms.GetExcelDocumentFromDataSet(fileRD, result.Ds, targetCellsColumnName, targetSeparators.ToCharArray(), targetCellSeparators.ToCharArray(), false);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {

                    // Excel adatok streamel�se
                    byte[] res = (byte[])result.Record;

                    Response.Clear();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment;filename=" + templateFileName + ";");
                    
                    Response.AddHeader("Cache-Control", " ");
                    Response.AddHeader("Expires", " Mon, 26 Jul 1997 05:00:00 GMT");
                    Response.AddHeader("Last-Modified", " Mon, 25 Jul 1997 05:00:00 GMT");
                    Response.AddHeader("Cache-Control", "post-check=0, pre-check=0, max-age=0");
                   
                    Response.OutputStream.Write(res, 0, res.Length);
                    Response.OutputStream.Flush();
                    Response.End();
                
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }

            }
            catch (Exception e)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UI_ErrorWhileGeneratingDocument, e.Message);
                ErrorUpdatePanel.Update();
            }

        }   // eredm�nyes volt a lek�rdez�s
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            LoadData();
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

}
