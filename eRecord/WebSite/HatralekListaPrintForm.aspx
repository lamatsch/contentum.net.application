﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="HatralekListaPrintForm.aspx.cs" Inherits="HatralekListaPrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc4" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc15" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox" TagPrefix="uc12" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc3" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IrattarozatlanUgyiratokListaFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
<%--                <asp:UpdatePanel ID="SettingsUpdatePanel" runat="server">
                <ContentTemplate>--%>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label5" runat="server" Text="Időszak:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="true" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <asp:Label ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:UpdatePanel ID="IktatoKonyvUpdatePanel" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="false" OnLoad="IktatoKonyvUpdatePanel_Load">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DatumIntervallum_SearchCalendarControl1" EventName="EvChanged" />
                                    </Triggers>
                                    <ContentTemplate>
                                       <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server" />
<%--                                       <asp:DropDownList ID="Iktatokonyvek_DropDownLis_Ajax" runat="server" CssClass="mrUrlapInputComboBox" Visible="False" />
--%>                                    </ContentTemplate>
                               </asp:UpdatePanel>
                           </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <asp:Label ID="Label34" runat="server" Text="Ügyintéző:"></asp:Label></td>
                            <td class="mrUrlapMezo" width="0%" colspan="1">
                               <uc15:FelhasznaloCsoportTextBox ID="UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                               <asp:Label ID="Label39" runat="server" Text="Irattári tételszám:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                               <uc12:IraIrattariTetelTextBox ID="UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label2" runat="server" Text="Skontróban lévő tételekkel:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <asp:CheckBox ID="cbSkontrobanLevokkel" runat="server" AutoPostBack="false" CssClass="HideCheckBoxText" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label1" runat="server" Text="0 iratszámú ügyiratokkal:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <asp:CheckBox ID="cbAlszamNelkuliekIs" runat="server" AutoPostBack="false" CssClass="HideCheckBoxText" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
<%--                </ContentTemplate>
                </asp:UpdatePanel>--%>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
