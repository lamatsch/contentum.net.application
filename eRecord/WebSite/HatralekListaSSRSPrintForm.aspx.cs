﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Text;

public partial class HatralekListaSSRSPrintForm : Contentum.eUtility.UI.PageBase
{
    //Query string paraméter adatok eltárolása
    private string DateFrom = String.Empty;
    private string DateTo = String.Empty;
    private string Where_EREC = String.Empty;
    private string Where_MIG = String.Empty;
    private string OrderBy = String.Empty;
    private string TopRow = "0";
    private string IktatoKonyv = String.Empty;
    private string ITSZ = String.Empty;
    private string Migralt = String.Empty;
    private string UgyintezoSet = String.Empty;
    private string Ugyintezo = String.Empty;
    private string Title = String.Empty;
    private string ExportFilename = String.Empty;

    #region not used
    private string[] uid = { };
    private string riport_path = String.Empty;
    private string docId = String.Empty;
    private string iratIdsForServices = String.Empty;
    private string executor = String.Empty;
    private string szervezet = String.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //executor kiszedése a query stringből
        if (Request.QueryString["ExecutorUserId"] != null)
        {
            executor = Request.QueryString["ExecutorUserId"].ToString();
        }

        //szervezet kiszedése a query stringből
        if (Request.QueryString["FelhasznaloSzervezet_Id"] != null)
        {
            szervezet = Request.QueryString["FelhasznaloSzervezet_Id"].ToString();
        }

        //DateFrom kiszedése a query stringből
        if (Request.QueryString["DateFrom"] != null)
        {
            DateFrom = Request.QueryString["DateFrom"].ToString();
        }

        //DateTo kiszedése a query stringből
        if (Request.QueryString["DateTo"] != null)
        {
            DateTo = Request.QueryString["DateTo"].ToString();
        }

        //IktatoKonyv kiszedése a query stringből
        if (Request.QueryString["IktatoKonyv"] != null)
        {
            IktatoKonyv = Request.QueryString["IktatoKonyv"].ToString();
        }

        //ITSZ kiszedése a query stringből
        if (Request.QueryString["ITSZ"] != null)
        {
            ITSZ = Request.QueryString["ITSZ"].ToString();
        }

        //Title kiszedése a query stringből
        if (Request.QueryString["Title"] != null)
        {
            Title = Request.QueryString["Title"].ToString();
        }

        //Migrált kiszedése a query stringből
        if (Request.QueryString["Migralt"] != null)
        {
            Migralt = Request.QueryString["Migralt"].ToString();
        }

        //UgyintezoSet kiszedése a query stringből
        if (Request.QueryString["UgyintezoSet"] != null)
        {
            UgyintezoSet = Request.QueryString["UgyintezoSet"].ToString();
        }

        //Ugyintezo kiszedése a query stringből
        if (Request.QueryString["Ugyintezo"] != null)
        {
            Ugyintezo = Request.QueryString["Ugyintezo"].ToString();
        }

        //Where_EREC kiszedése a Session-ből
        if (Session["Where_EREC"] != null)
        {
            Where_EREC = Session["Where_EREC"].ToString();
            Session["Where_EREC"] = null;
        }

        //Where_MIG kiszedése a Session-ből
        if (Session["Where_MIG"] != null)
        {
            Where_MIG = Session["Where_MIG"].ToString();
            Session["Where_MIG"] = null;
        }

        //OrderBy kiszedése a Session-ből
        if (Session["OrderBy"] != null)
        {
            OrderBy = Session["OrderBy"].ToString();
            Session["OrderBy"] = null;
        }

        //ExportFilename kiszedése a query stringből
        if (Request.QueryString["ExportFilename"] != null)
        {
            ExportFilename = Request.QueryString["ExportFilename"].ToString();
        }

    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Report server paraméterek
            ReportViewer1.Reset();
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);

            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;

            //Riport elérési út
            riport_path = ReportViewer1.ServerReport.ReportPath;
            riport_path += "/Lekerdezes/HatralekLista";

            if (!String.IsNullOrEmpty(ExportFilename))
                ReportViewer1.ServerReport.DisplayName = ExportFilename;

            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + riport_path;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            //Riport paraméterek beállítása
            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    //Riport paraméterek beállítása
    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                ReportParameters[0] = new ReportParameter(rpis[0].Name);
                ReportParameters[0].Values.Add(executor);

                ReportParameters[1] = new ReportParameter(rpis[1].Name);
                ReportParameters[1].Values.Add(IktatoKonyv);

                ReportParameters[2] = new ReportParameter(rpis[2].Name);
                ReportParameters[2].Values.Add(szervezet);

                ReportParameters[3] = new ReportParameter(rpis[3].Name);
                ReportParameters[3].Values.Add(Where_EREC);

                ReportParameters[4] = new ReportParameter(rpis[4].Name);
                ReportParameters[4].Values.Add(OrderBy);

                ReportParameters[5] = new ReportParameter(rpis[5].Name);
                ReportParameters[5].Values.Add(DateFrom);

                ReportParameters[6] = new ReportParameter(rpis[6].Name);
                ReportParameters[6].Values.Add(DateTo);

                ReportParameters[7] = new ReportParameter(rpis[7].Name);
                ReportParameters[7].Values.Add(ITSZ);

                ReportParameters[7] = new ReportParameter(rpis[7].Name);
                ReportParameters[7].Values.Add(ITSZ);

                ReportParameters[8] = new ReportParameter(rpis[8].Name);
                ReportParameters[8].Values.Add(Migralt);

                ReportParameters[9] = new ReportParameter(rpis[9].Name);
                ReportParameters[9].Values.Add(TopRow);

                ReportParameters[10] = new ReportParameter(rpis[10].Name);
                ReportParameters[10].Values.Add(Where_MIG);

                ReportParameters[11] = new ReportParameter(rpis[11].Name);
                ReportParameters[11].Values.Add(UgyintezoSet);

                ReportParameters[12] = new ReportParameter(rpis[12].Name);
                ReportParameters[12].Values.Add(Title);

                ReportParameters[13] = new ReportParameter(rpis[13].Name);
                ReportParameters[13].Values.Add(Ugyintezo);
            }

        }

        return ReportParameters;
    }
}

