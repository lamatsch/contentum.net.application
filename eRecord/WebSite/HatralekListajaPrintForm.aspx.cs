﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.Collections.Generic;

public partial class HatralekListajaPrintForm : Contentum.eUtility.UI.PageBase
{

    #region Utility

    #region TemplateManager params
    private int GetPriority()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        int priority = 1;

        Contentum.eAdmin.Service.KRT_CsoportTagokService csop_service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        KRT_CsoportTagokSearch krt_CsoportTagokSearch = new KRT_CsoportTagokSearch();

        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Value = FelhasznaloProfil.FelhasznaloId(Page);
        krt_CsoportTagokSearch.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        krt_CsoportTagokSearch.Tipus.Value = KodTarak.CsoprttagsagTipus.vezeto;
        krt_CsoportTagokSearch.Tipus.Operator = Query.Operators.equals;

        Result csop_result = csop_service.GetAll(execParam, krt_CsoportTagokSearch);

        if (!csop_result.IsError)
        {
            if (csop_result.Ds.Tables[0].Rows.Count > 0)
            {
                priority++;
            }
        }

        return priority;
    }

    private bool IsPdf()
    {
        bool pdf = false;
        if (UI.GetAppSetting("eTemplateManager_IsPdf").Equals("true"))
        {
            pdf = true;
        }
        return pdf;
    }

    private string GetFilename(bool isPdf)
    {
        string filename = String.Format("{0}_{1}_{2}.{3}",
            "Hatralek_lista"
            , System.DateTime.Today.ToString().Replace(" ", "").Substring(0, 10).Replace('.', ' ')
            , System.DateTime.Now.ToLongTimeString().Replace(':', ' ')
            , isPdf ? "pdf" : "doc"
            );

        return filename;
    }

    private string GetTemplateName(bool isMigralasEveSet, bool isUgyintezoSet)
    {
        if (isUgyintezoSet)
        {
            // itt nincs különbség
            if (isMigralasEveSet)
            {
                return "Hatralek lista alszam nelkuli - uj.xml";
            }
            else
            {
                return "Hatralek lista alszam nelkuli - uj.xml";
            }
        }
        else
        {
            return "Hatralek lista ugyintezo nelkul.xml";
        }
    }
    #endregion TemplateManager params

    #region GetAll params
    // Iktatóhely megnevezés + a kijelölt évekhez tartozó Id-k listája
    private KeyValuePair<string, List<string>> GetIktatoKonyvek()
    {
        KeyValuePair<string, List<string>> kvpResult = new KeyValuePair<string, List<string>>();

        // check date and fill variables if possible

        if (DatumIntervallum_SearchCalendarControl1.IsValid_EvKezd
            && DatumIntervallum_SearchCalendarControl1.IsValid_EvVege)
        {
            string startYear = DatumIntervallum_SearchCalendarControl1.EvKezd.ToString();
            string endYear = DatumIntervallum_SearchCalendarControl1.EvVege.ToString();

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

                string[] iktatokonyv_adatok = IraIktatoKonyvekDropDownList1.SelectedValue.Split(new char[] { '|' });
                string iktatohely = iktatokonyv_adatok[0];   // itt biztosan van legalább 0. elem

                ExecParam erec_IraIktatoKonyvek_execParam = UI.SetExecParamDefault(Page, new ExecParam());

                if (iktatokonyv_adatok.Length > 0 && !String.IsNullOrEmpty(iktatokonyv_adatok[0]))
                {
                    erec_IraIktatoKonyvekSearch.Iktatohely.Value = iktatokonyv_adatok[0];
                    erec_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
                }

                if (iktatokonyv_adatok.Length > 1 && !String.IsNullOrEmpty(iktatokonyv_adatok[1]))
                {
                    erec_IraIktatoKonyvekSearch.MegkulJelzes.Value = iktatokonyv_adatok[1];
                    erec_IraIktatoKonyvekSearch.MegkulJelzes.Operator = Query.Operators.equals;
                }

                erec_IraIktatoKonyvekSearch.IktatoErkezteto.Value = Constants.IktatoErkezteto.Iktato; // "I";
                erec_IraIktatoKonyvekSearch.IktatoErkezteto.Operator = Query.Operators.equals;
                erec_IraIktatoKonyvekSearch.Ev.Value = startYear;
                erec_IraIktatoKonyvekSearch.Ev.ValueTo = endYear;
                erec_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.between;

                Result erec_IraIktatoKonyvek_result = erec_IraIktatoKonyvekService.GetAllWithExtension(erec_IraIktatoKonyvek_execParam, erec_IraIktatoKonyvekSearch);

                if (erec_IraIktatoKonyvek_result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, erec_IraIktatoKonyvek_result);
                }
                else
                {
                    kvpResult = new KeyValuePair<string, List<string>>(iktatohely, new List<string>(erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count));
                    foreach (DataRow _row in erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows)
                    {
                        kvpResult.Value.Add(_row["Id"].ToString());
                    }
                }
            }
        }

        return kvpResult;
    }

    private EREC_UgyUgyiratokSearch GetUgyUgyiratokSearch(string iktatokonyv_ids, string ugyintezo_id, string irattaritetel_id)
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        if (!String.IsNullOrEmpty(iktatokonyv_ids))
        {
            erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Value = iktatokonyv_ids;
            erec_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator = Query.Operators.inner;
        }

        if (!String.IsNullOrEmpty(ugyintezo_id))
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = ugyintezo_id;
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(irattaritetel_id))
        {
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value = irattaritetel_id;
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Operator = Query.Operators.equals;
        }

        DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_UgyUgyiratokSearch.Manual_LetrehozasIdo);

        if (!cbAlszamNelkuliekIs.Checked)
        {
            erec_UgyUgyiratokSearch.IratSzam.Value = "0";
            erec_UgyUgyiratokSearch.IratSzam.Operator = Query.Operators.greater;
        }

        string[] innerAllapotokSkontroNelkul = new string[] {
            KodTarak.UGYIRAT_ALLAPOT.Megnyitott_Munkaanyag, // = "0";
            KodTarak.UGYIRAT_ALLAPOT.Szignalt, // = "03";
            KodTarak.UGYIRAT_ALLAPOT.Iktatott, // = "04";
            KodTarak.UGYIRAT_ALLAPOT.Ugyintezes_alatt, //= "06";
            //KodTarak.UGYIRAT_ALLAPOT.A_a_ban_Lezart, //= "09"; // A/a-ban
            //KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott, //= "11";
            KodTarak.UGYIRAT_ALLAPOT.Kolcsonzott, //= "13";
            KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa, //= "52";
            KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert, // = "55"
            KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo, // = "56";
            KodTarak.UGYIRAT_ALLAPOT.Elintezett // = "99";
        };

        string[] innerAllapotokSkontroval = new string[innerAllapotokSkontroNelkul.Length + 1];
        innerAllapotokSkontroval[0] = KodTarak.UGYIRAT_ALLAPOT.Skontroban; //= "07";
        innerAllapotokSkontroNelkul.CopyTo(innerAllapotokSkontroval, 1);

        //KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt //= "50";


        erec_UgyUgyiratokSearch.WhereByManual = String.Format(" and (EREC_UgyUgyiratok.Allapot in ({0}) or (EREC_Ugyugyiratok.Allapot='{1}' and EREC_Ugyugyiratok.TovabbitasAlattAllapot in ({0})))"
            , cbSkontrobanLevokkel.Checked ? Search.GetSqlInnerString(innerAllapotokSkontroval) : Search.GetSqlInnerString(innerAllapotokSkontroNelkul)
            , KodTarak.UGYIRAT_ALLAPOT.Tovabbitas_alatt);

        erec_UgyUgyiratokSearch.OrderBy = String.Concat(String.IsNullOrEmpty(ugyintezo_id) ? "KRT_Felhasznalok.Nev," : ""
            , "EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam");

        return erec_UgyUgyiratokSearch;

    }

    private Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch GetMIGFoszamSearch(string iktatohely, string ugyintezo_id, string ugyintezo_name, string mig_itsz)
    {
        Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch mig_FoszamSearch = new Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch();
        string MIG_where = "";

        // check date and fill variables if possible

        if (DatumIntervallum_SearchCalendarControl1.IsValid_EvKezd
            && DatumIntervallum_SearchCalendarControl1.IsValid_EvVege)
        {
            string startYear = DatumIntervallum_SearchCalendarControl1.EvKezd.ToString();
            string endYear = DatumIntervallum_SearchCalendarControl1.EvVege.ToString();
            //if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            if (!String.IsNullOrEmpty(iktatohely))
            {

                String sav = IktatoKonyvek.GetSavFromIktatohely(iktatohely);

                mig_FoszamSearch.EdokSav.Value = sav;
                mig_FoszamSearch.EdokSav.Operator = Query.Operators.equals;
            }
            else
            {
                mig_FoszamSearch.EdokSav.Value = "";
                mig_FoszamSearch.EdokSav.Operator = Query.Operators.notnull;
            }

            if (!String.IsNullOrEmpty(ugyintezo_name))
            {
                //mig_FoszamSearch.Manual_MIG_Eloado_NAME.Value = ugyintezo_name;
                //mig_FoszamSearch.Manual_MIG_Eloado_NAME.Operator = Query.Operators.equals;

                if (!String.IsNullOrEmpty(ugyintezo_id))
                {
                    //mig_FoszamSearch.WhereByManual += String.Format(" and (MIG_Eloado.NAME='{0}' or MIG_Foszam.EDOK_Ugyintezo_Csoport_Id='{1}')", ugyintezo_name, ugyintezo_id);
                    MIG_where += String.Format("{0} (MIG_Foszam.EDOK_Ugyintezo_Csoport_Id='{2}' or (MIG_Foszam.EDOK_Ugyintezo_Csoport_Id is null and MIG_Eloado.NAME='{1}'))"
                        , String.IsNullOrEmpty(MIG_where) ? "" : " and "
                        , ugyintezo_name
                        , ugyintezo_id
                        );
                }
                else
                {
                    //mig_FoszamSearch.WhereByManual += String.Format(" and (MIG_Eloado.NAME='{0}' or MIG_Foszam.EDOK_Ugyintezo_Nev='{0}')", ugyintezo_name);
                    MIG_where += String.Format("{0} MIG_Foszam.EDOK_Ugyintezo_Csoport_Id is null and MIG_Eloado.NAME='{1}'"
                        , String.IsNullOrEmpty(MIG_where) ? "" : " and "
                        , ugyintezo_name
                        );
                }
            }

            if (!string.IsNullOrEmpty(UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField))
            {
                if (!String.IsNullOrEmpty(mig_itsz))
                {
                    mig_FoszamSearch.UI_IRJ.Group = "111";
                    mig_FoszamSearch.UI_IRJ.GroupOperator = Query.Operators.or;
                    mig_FoszamSearch.UI_IRJ.Value = mig_itsz;
                    mig_FoszamSearch.UI_IRJ.Operator = Query.Operators.likefull;

                    mig_FoszamSearch.IRJ2000.Group = "111";
                    mig_FoszamSearch.IRJ2000.GroupOperator = Query.Operators.or;
                    mig_FoszamSearch.IRJ2000.Value = mig_itsz;
                    mig_FoszamSearch.IRJ2000.Operator = Query.Operators.likefull;

                    //MIG_where = MIG_where + " and (MIG_Foszam.UI_IRJ LIKE '%" + mig_itsz + "%' or MIG_Foszam.IRJ2000 LIKE '%" + mig_itsz + "%') ";
                }

            }

            mig_FoszamSearch.UI_YEAR.Value = startYear;
            mig_FoszamSearch.UI_YEAR.ValueTo = endYear;
            mig_FoszamSearch.UI_YEAR.Operator = Query.Operators.between;

            if (!cbAlszamNelkuliekIs.Checked)
            {
                if (!String.IsNullOrEmpty(MIG_where))
                {
                    MIG_where += " and ";
                }
                MIG_where += " exists(select 1 from MIG_Alszam where MIG_Alszam.MIG_Foszam_Id = MIG_Foszam.Id) ";
            }

            if (cbSkontrobanLevokkel.Checked)
            {
                mig_FoszamSearch.UGYHOL.Value = Search.GetSqlInnerString(new string[] {
                     Constants.MIG_IratHelye.Skontro // "1"EDOK_Ugyintezo_Csoport_Id
                     , Constants.MIG_IratHelye.Osztalyon// "3"
                     , Constants.MIG_IratHelye.IrattarbolElkert // "I"
                });
                mig_FoszamSearch.UGYHOL.Operator = Query.Operators.inner;


                //mig_FoszamSearch.WhereByManual = " MIG_Foszam.UGYHOL in ('1','3','I') and MIG_Foszam.Edok_Utoirat_Id IS NULL and MIG_Foszam.Csatolva_Id IS NULL " + MIG_where;
            }
            else
            {
                mig_FoszamSearch.UGYHOL.Value = Search.GetSqlInnerString(new string[] {
                     Constants.MIG_IratHelye.Osztalyon// "3"
                     , Constants.MIG_IratHelye.IrattarbolElkert // "I"
                });
                mig_FoszamSearch.UGYHOL.Operator = Query.Operators.inner;

                //mig_FoszamSearch.WhereByManual = " MIG_Foszam.UGYHOL in ('3','I') and MIG_Foszam.Edok_Utoirat_Id IS NULL and MIG_Foszam.Csatolva_Id IS NULL " + MIG_where;
            }


            mig_FoszamSearch.Edok_Utoirat_Id.Value = "";
            mig_FoszamSearch.Edok_Utoirat_Id.Operator = Query.Operators.isnull;

            mig_FoszamSearch.Csatolva_Id.Value = "";
            mig_FoszamSearch.Csatolva_Id.Operator = Query.Operators.isnull;

            mig_FoszamSearch.WhereByManual = MIG_where;

            if (String.IsNullOrEmpty(ugyintezo_name))
            {
                mig_FoszamSearch.OrderBy = "KRT_Felhasznalok.NAME,MIG_Foszam.EdokSav,MIG_Foszam.UI_YEAR,MIG_Foszam.UI_NUM";//"KRT_Felhasznalok.NAME,EREC_UgyUgyiratok.UI_YEAR,MIG_Sav.Name,EREC_UgyUgyiratok.UI_NUM";
            }
            else
            {
                mig_FoszamSearch.OrderBy = " MIG_Foszam.EdokSav,MIG_Foszam.UI_YEAR,MIG_Foszam.UI_NUM"; //" MIG_Foszam.UI_YEAR,MIG_Sav.Name,MIG_Foszam.UI_NUM";
            }
        }
        return mig_FoszamSearch;
    }

    private string GetMigIrattariTetelszam()
    {
        string mig_itsz = null;
        if (!string.IsNullOrEmpty(UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField))
        {
            EREC_IraIrattariTetelekService itsz_service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            EREC_IraIrattariTetelekSearch itsz_search = new EREC_IraIrattariTetelekSearch();

            itsz_search.Id.Value = UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField;
            itsz_search.Id.Operator = Query.Operators.equals;

            ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());

            Result itsz_result = itsz_service.GetAllWithExtension(ep, itsz_search);

            if (!itsz_result.IsError)
            {
                string _AgazatiJelek_Kod = itsz_result.Ds.Tables[0].Rows[0]["AgazatiJelek_Kod"].ToString();
                string _IrattariTetelszam = itsz_result.Ds.Tables[0].Rows[0]["IrattariTetelszam"].ToString();
                string _MegorzesiIdo = itsz_result.Ds.Tables[0].Rows[0]["MegorzesiIdo"].ToString();
                string _IrattariJel = itsz_result.Ds.Tables[0].Rows[0]["IrattariJel"].ToString();

                if (!string.IsNullOrEmpty(_IrattariJel))
                {
                    if (string.Equals(_IrattariJel, "L"))
                    {
                        _MegorzesiIdo = "0";
                    }
                }

                mig_itsz = _IrattariTetelszam + _AgazatiJelek_Kod.Substring(0, 1) + _MegorzesiIdo;
            }
        }

        return mig_itsz;
    }
    #endregion GetAll params

    private DataTable CreateParentTable()
    {
        DataTable table = new DataTable("ParentTable");
        DataColumn column;
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "id";
        column.ReadOnly = true;
        column.Unique = true;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Date";
        column.AutoIncrement = false;
        column.Caption = "Date";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Datum";
        column.AutoIncrement = false;
        column.Caption = "Datum";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Iktatokonyv";
        column.AutoIncrement = false;
        column.Caption = "Iktatokonyv";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "Ugyintezo";
        column.AutoIncrement = false;
        column.Caption = "Ugyintezo";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "IrattariTetelszam";
        column.AutoIncrement = false;
        column.Caption = "IrattariTetelszam";
        column.ReadOnly = false;
        column.Unique = false;
        table.Columns.Add(column);
        DataColumn[] PrimaryKeyColumns = new DataColumn[1];
        PrimaryKeyColumns[0] = table.Columns["id"];
        table.PrimaryKey = PrimaryKeyColumns;

        return table;
    }
    #endregion Utility

    #region Page
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
        DatumIntervallum_SearchCalendarControl1.AutoPostback = true;

        if (!IsPostBack)
        {
            DatumIntervallum_SearchCalendarControl1.DatumKezd = new DateTime(System.DateTime.Today.Year, 1, 1).ToShortDateString();
            DatumIntervallum_SearchCalendarControl1.DatumVege = System.DateTime.Today.ToShortDateString();

            //FillIktatoKonyvekList();
        }

        if (IsPostBack)
        {
            if (FormHeader1.ErrorPanel.Visible == true)
            {
                FormHeader1.ErrorPanel.Visible = false;
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        //IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
        //        , false, DatumIntervallum_SearchCalendarControl1.DatumKezd.Remove(4), DatumIntervallum_SearchCalendarControl1.DatumVege.Remove(4)
        //        , true, false, EErrorPanel1);
        //FillIktatoKonyvekList();

        cbAlszamNelkul.Checked = true;
        cbAlszamNelkul.Enabled = false;
    }
    #endregion Page

    private void FillIktatoKonyvekList()
    {
        String selectedValue = IraIktatoKonyvekDropDownList1.SelectedValue;

        IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        if (DatumIntervallum_SearchCalendarControl1.IsValid_EvKezd && DatumIntervallum_SearchCalendarControl1.IsValid_EvVege)
        {
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                    , false, DatumIntervallum_SearchCalendarControl1.EvKezd.ToString(), DatumIntervallum_SearchCalendarControl1.EvVege.ToString()
                    , true, false, FormHeader1.ErrorPanel);
        }

        if (!String.IsNullOrEmpty(selectedValue))
        {
            ListItem liSelected = IraIktatoKonyvekDropDownList1.DropDownList.Items.FindByValue(selectedValue);
            if (liSelected != null)
            {
                liSelected.Selected = true;
            }
        }
    }

    protected void IktatoKonyvUpdatePanel_Load(object sender, EventArgs e)
    {
        FillIktatoKonyvekList();
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (DatumIntervallum_SearchCalendarControl1.IsValid_EvKezd == false
                || DatumIntervallum_SearchCalendarControl1.IsValid_EvVege == false)
            {
                // hiba:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.UIFormatErrorHeader, Resources.Error.UIDateTimeFormatError);
                return;
            }

            int migralasEve = Rendszerparameterek.GetInt(Page, Rendszerparameterek.MIGRALAS_EVE);
            bool isMigralasEveSet = (migralasEve != -1);
            // csak akkor hívjuk a MIG_FoszamService-t, ha migrálásnak megfelelő vagy az előtti év van megadva
            bool isMigralasElotti = isMigralasEveSet && (DatumIntervallum_SearchCalendarControl1.EvKezd <= migralasEve);

            bool isUgyintezoSet = (String.IsNullOrEmpty(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField) == false);
            bool isIrattariTetelSet = (String.IsNullOrEmpty(UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField) == false);
            bool isIktatokonyvSet = (String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue) == false);

            string ugyintezo_id = (isUgyintezoSet ? UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Id_HiddenField : null);
            string irattaritetel_id = (isIrattariTetelSet ? UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Id_HiddenField : null);

            bool pdf = this.IsPdf();
            int priority = this.GetPriority();

            string filename = GetFilename(pdf);
            string ids = null;
            string iktatohely = null;

            string Iktatokonyv = "";
            string Ugyintezo = "";
            string IrattariTetelszam = "";

            string date = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10) + " - " + DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10);

            if (isIktatokonyvSet)
            {
                Iktatokonyv = IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text;

                KeyValuePair<string, List<string>> kvpIktatokonyvek = GetIktatoKonyvek();

                if (!String.IsNullOrEmpty(kvpIktatokonyvek.Key))
                {
                    if (kvpIktatokonyvek.Value != null && kvpIktatokonyvek.Value.Count > 0)
                    {
                        ids = Search.GetSqlInnerString(kvpIktatokonyvek.Value.ToArray());
                    }
                    iktatohely = kvpIktatokonyvek.Key;
                }
            }

            string mig_itsz = null;
            if (isIrattariTetelSet)
            {
                mig_itsz = GetMigIrattariTetelszam();
                IrattariTetelszam = UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text;
            }

            if (isUgyintezoSet)
            {
                Ugyintezo = UI.RemoveEmailAddressFromCsoportNev(UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text);
            }

            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = erec_UgyUgyiratokSearch = GetUgyUgyiratokSearch(ids, ugyintezo_id, irattaritetel_id);
            Contentum.eMigration.Service.MIG_FoszamService MIG_service = null;
            Contentum.eMigration.Query.BusinessDocuments.MIG_FoszamSearch mig_FoszamSearch = null;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            Result result = null;

            if (isMigralasEveSet && isMigralasElotti)
            {
                MIG_service = Contentum.eMigration.eMigrationService.ServiceFactory.GetMIG_FoszamService();
                mig_FoszamSearch = GetMIGFoszamSearch(iktatohely, ugyintezo_id, Ugyintezo, mig_itsz);
            }

            if (isUgyintezoSet)
            {
                //LZS - BUG_9177 / BUG_4747 SSRS riportos megoldás

                //string orderby = erec_UgyUgyiratokSearch.OrderBy;
                string executor = execParam.Felhasznalo_Id;
                string szervezet = execParam.FelhasznaloSzervezet_Id;
                string ITSZ = UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text;
                string title = "Hátraléklista";
                bool migralt = false;

                Query query = new Query();
                query.BuildFromBusinessDocument(erec_UgyUgyiratokSearch);

                Session["Where_EREC"] = query.Where + erec_UgyUgyiratokSearch.WhereByManual;
                //Nem volt jó
                //Session["OrderBy"] = erec_UgyUgyiratokSearch.OrderBy;
                Session["OrderBy"] = "order by KRT_Felhasznalok.Nev,EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam";

                if (isMigralasEveSet && isMigralasElotti)
                {
                    migralt = true;
                    Query queryMIG = new Query();
                    queryMIG.BuildFromBusinessDocument(mig_FoszamSearch);

                    Session["Where_MIG"] = queryMIG.Where + mig_FoszamSearch.WhereByManual;
                }

                string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "DateFrom=" + DatumIntervallum_SearchCalendarControl1.DatumKezd + '&' + "DateTo=" + DatumIntervallum_SearchCalendarControl1.DatumVege + '&' + "IktatoKonyv=" + IraIktatoKonyvekDropDownList1.SelectedValue + '&' + "ITSZ=" + ITSZ + '&' + "Migralt=" + migralt + '&' + "UgyintezoSet=" + isUgyintezoSet + '&' + "Title=" + title + '&' + "Ugyintezo=" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text;
                //Meghívjuk az HatralekListajaSSRSPrintForm.aspx nyomtatóoldalt.
                string js = "javascript:window.open('HatralekListajaSSRSPrintForm.aspx?" + queryString.Trim() + "')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HatralekListajaSSRSPrintForm", js, true);


            }
            else // nincs megadva ügyintéző
            {
                string ParentTable = "";

                if (isIktatokonyvSet)
                {
                    ParentTable = ParentTable + "<Iktatokonyv>" + IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text + "</Iktatokonyv>";
                }
                if (isIrattariTetelSet)
                {
                    ParentTable = ParentTable + "<IrattariTetelszam>" + UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text + "</IrattariTetelszam>";
                }

                ParentTable = ParentTable + "<Datum>" + date + "</Datum>";
                //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

                //string orderby = erec_UgyUgyiratokSearch.OrderBy;
                string executor = execParam.Felhasznalo_Id;
                string szervezet = execParam.FelhasznaloSzervezet_Id;
                string ITSZ = UgyUgyirat_IraIrattariTetel_IraIrattariTetelTextBox.Text;
                string title = "Hátraléklista";
                bool migralt = false;

                Query query = new Query();
                query.BuildFromBusinessDocument(erec_UgyUgyiratokSearch);

                Session["Where_EREC"] = query.Where + erec_UgyUgyiratokSearch.WhereByManual;

                //Nem volt jó
                //Session["OrderBy"] = erec_UgyUgyiratokSearch.OrderBy;
                Session["OrderBy"] = "order by EREC_IraIktatokonyvek.Iktatohely,EREC_IraIktatokonyvek.Ev,EREC_UgyUgyiratok.Foszam";

                if (isMigralasEveSet && isMigralasElotti)
                {
                    migralt = true;
                    Query queryMIG = new Query();
                    queryMIG.BuildFromBusinessDocument(mig_FoszamSearch);

                    Session["Where_MIG"] = queryMIG.Where + mig_FoszamSearch.WhereByManual;
                }

                string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "DateFrom=" + DatumIntervallum_SearchCalendarControl1.DatumKezd + '&' + "DateTo=" + DatumIntervallum_SearchCalendarControl1.DatumVege + '&' + "IktatoKonyv=" + IraIktatoKonyvekDropDownList1.SelectedValue + '&' + "ITSZ=" + ITSZ + '&' + "Migralt=" + migralt + '&' + "UgyintezoSet=" + isUgyintezoSet + '&' + "Title=" + title + '&' + "Ugyintezo=" + UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Text;
                //Meghívjuk az HatralekListajaSSRSPrintForm.aspx nyomtatóoldalt.
                string js = "javascript:window.open('HatralekListajaSSRSPrintForm.aspx?" + queryString.Trim() + "')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HatralekListajaSSRSPrintForm", js, true);


            }
        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}