<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="HelyettesitesekForm.aspx.cs" Inherits="HelyettesitesekForm" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,HelyettesitesekFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel ID="MegbizasUpdatePanel" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelHelyettesitett" runat="server" Text="Helyettesített:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc8:FelhasznaloCsoportTextBox ID="Felhasznalo_ID_helyettesitett" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelCsoport" runat="server" Text="Csoport:" />
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:DropDownList ID="Csoportok_DropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label2" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelHelyettesito" runat="server" Text="Helyettesítő:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc8:FelhasznaloCsoportTextBox ID="Felhasznalo_ID_helyettesito" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="label16" runat="server" Text="Helyettesítés ideje:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="HelyetteseitesiIdo_ErvenyessegCalendarControl"
                                            runat="server" Validate="false" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyzés:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Megjegyzes" runat="server" CssClass="mrUrlapInput" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        &nbsp;
                                    </td>
                                    <td class="mrUrlapMezo">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
