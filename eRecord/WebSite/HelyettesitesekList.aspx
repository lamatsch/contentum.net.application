<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HelyettesitesekList.aspx.cs" Inherits="HelyettesitesekList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="HelyettesitesekCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="HelyettesitesekUpdatePanel" runat="server" OnLoad="HelyettesitesekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="HelyettesitesekCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="HelyettesitesekCPEButton"
                            CollapseControlID="HelyettesitesekCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="HelyettesitesekCPEButton" ExpandedSize="0" ExpandedText="Helyettes�t�sek list�ja"
                            CollapsedText="Helyettes�t�sek list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="HelyettesitesekGridView" runat="server" OnRowCommand="HelyettesitesekGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="HelyettesitesekGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="HelyettesitesekGridView_Sorting"
                                            OnRowDataBound="HelyettesitesekGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Felhasznalo_Nev_helyettesitett" HeaderText="Helyettes�tett"
                                                    SortExpression="KRT_Felhasznalok1.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felhasznalo_ID_helyettesitett"
                                                    SortExpression="Felhasznalo_ID_helyettesitett">
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="150px" />
                                                </asp:BoundField>                                                
                                                <asp:BoundField DataField="Csoport_Nev_helyettesitett" HeaderText="Csoport"
                                                    SortExpression="KRT_Csoportok.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felhasznalo_Nev_helyettesito" HeaderText="Helyettes�t�"
                                                    SortExpression="KRT_Felhasznalok2.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felhasznalo_ID_helyettesito"
                                                    SortExpression="Felhasznalo_ID_helyettesitot">
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="150px" />
                                                </asp:BoundField>                                                 
                                                <asp:BoundField DataField="HelyettesitesKezd" HeaderText="Helyettes�t�s&nbsp;kezdete"
                                                    SortExpression="HelyettesitesKezd">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="HelyettesitesVege" HeaderText="Helyettes�t�s&nbsp;v�ge"
                                                    SortExpression="HelyettesitesVege">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Megjegyzes" HeaderText="Megjegyz�s" SortExpression="Megjegyzes">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
                                                        <%-- nem m�dos�thatja a rekordot, akinek ez nem helyettes�t�se, �s nincs joga mindenki�t m�dos�tani --%>
                                                        <asp:CheckBox ID="cbCurrentUserAllowedToModify" runat="server" Enabled="false" />
                                                        <%-- nem �rv�nytelen�thet� a rekord, ha a helyettes�t�s kezdete m�ltbeli id�pont, azaz a megb�z�s m�r �rv�nybe l�pett --%>
                                                        <asp:CheckBox ID="cbStartedAtPast" runat="server" Checked='<%# (!(Eval("HelyettesitesKezd") is DBNull) && DateTime.Now > (DateTime)(Eval("HelyettesitesKezd"))) ? true : false %>' Enabled="false" />
                                                        <%-- nem �rv�nytelen�thet� a rekord, ha a helyettes�t�s v�ge m�ltbeli id�pont --%>
                                                        <asp:CheckBox ID="cbEndedAtPast" runat="server" Checked='<%# (!(Eval("HelyettesitesVege") is DBNull) && DateTime.Now > (DateTime)(Eval("HelyettesitesVege"))) ? true : false %>' Enabled="false" />
                                                        <%-- nem �rv�nytelen�thet� a rekord, ha a felhaszn�l� nem jogosult, vagy a v�ge m�ltbeli --%>
                                                        <asp:CheckBox ID="cbInvalidationAllowed" runat="server" Enabled="false" />                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
