<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="HelyettesitesekLovList.aspx.cs" Inherits="HelyettesitesekLovList" %>


<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,HelyettesitesekLovListHeaderTitle%>" />
    &nbsp;<br />    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />                    
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
                                <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td>
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">                                                
                                        <asp:Label ID="Label1" runat="server" Text="Helyettes�tett:" Font-Bold="true"></asp:Label><br />
                                        <asp:TextBox ID="TextBoxSearch" runat="server" Width="50%" AutoPostBack="True" OnTextChanged="TextBoxSearch_TextChanged">*</asp:TextBox>&nbsp;
                                        <asp:Button ID="ButtonSearch" runat="server" Text="Keres�s" OnClick="ButtonSearch_Click" />
                                        <asp:Button ID="ButtonAdvancedSearch" runat="server" Text="R�szletes keres�s" />
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">                    
                                        <div class="listaFulFelsoCsikKicsi"><img src="images/hu/design/spacertrans.gif" alt="" /></div>                            
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.jpg" AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>       
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />

</asp:Content>
