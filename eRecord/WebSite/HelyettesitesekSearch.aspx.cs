using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Collections.Generic;

public partial class HelyettesitesekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(KRT_HelyettesitesekSearch);

    private const string funkcio_HelyettesitesFelvetelOsszesDolgozora = "HelyettesitesFelvetelOsszesDolgozora";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
               new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        Felhasznalo_ID_helyettesitett.TextBox.TextChanged += new EventHandler(Felhasznalo_ID_helyettesitett_TextChanged);
        Felhasznalo_ID_helyettesitett.TextBox.AutoPostBack = true;
        //Felhasznalo_ID_helyettesitett.RefreshCallingWindow = true;

        if (!IsPostBack)
        {
            KRT_HelyettesitesekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (KRT_HelyettesitesekSearch)Search.GetSearchObject(Page, new KRT_HelyettesitesekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            searchObject.HelyettesitesVege.Value = DateTime.Now.ToString();
            LoadComponentsFromSearchObject(searchObject);
        }
    }

    protected void Felhasznalo_ID_helyettesitett_TextChanged(object sender, EventArgs e)
    {
        Csoportok_DropDownList.Items.Clear();
        bool bCsakFelhasznaloSzervezetbeli = false;
        bool bHierarchiabanLefele = false;
        if (!String.IsNullOrEmpty(Felhasznalo_ID_helyettesitett.Id_HiddenField))
        {
            if (!FunctionRights.GetFunkcioJog(Page, funkcio_HelyettesitesFelvetelOsszesDolgozora))
            {
                bCsakFelhasznaloSzervezetbeli = true;
                string CsoportTagsagTipus = FelhasznaloProfil.GetCsoportTagsagTipus_CurrentUser(Page);
                if (CsoportTagsagTipus == KodTarak.CsoprttagsagTipus.vezeto)
                {
                    bHierarchiabanLefele = true;
                }
            }

            ListItem[] listItems = GetCsoportokListItemsByFelhasznalo(Felhasznalo_ID_helyettesitett.Id_HiddenField, bCsakFelhasznaloSzervezetbeli, bHierarchiabanLefele);
            if (listItems != null)
            {
                // itt nem k�telez� a csoport, ez�rt hozz�adunk egy �res elemet
                ListItem emptyListItem = new ListItem(Resources.Form.UI_NoSelectedGroup, "");
                Csoportok_DropDownList.Items.Add(emptyListItem);
                Csoportok_DropDownList.Items.AddRange(listItems);
            }
        }
    }

    private ListItem GetCsoportListItemByCsoporttagsag(string CsoportTag_Id)
    {
        ListItem resultList = null;

        if (!String.IsNullOrEmpty(CsoportTag_Id))
        {
            KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
            ExecParam execParam_csoporttagok = UI.SetExecParamDefault(Page, new ExecParam());
            KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();

            search_csoporttagok.Id.Value = CsoportTag_Id;
            search_csoporttagok.Id.Operator = Query.Operators.equals;

            search_csoporttagok.OrderBy = "KRT_CsoportTagok.Csoport_Id ASC, KRT_CsoportTagok.Tipus DESC";

            Result result_csoporttagok = service_csoporttagok.GetAll(execParam_csoporttagok, search_csoporttagok);

            if (String.IsNullOrEmpty(result_csoporttagok.ErrorCode))
            {
                int cnt = result_csoporttagok.Ds.Tables[0].Rows.Count;
                if (cnt > 0)
                {
                    System.Data.DataRow row = result_csoporttagok.Ds.Tables[0].Rows[0];
                    //String CsoportTag_Id = row["Id"].ToString();
                    String Csoport_Id = row["Csoport_Id"].ToString();
                    String Csoport_Nev = UI.RemoveEmailAddressFromCsoportNev(Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Csoport_Id, Page));
                    resultList = new ListItem(Csoport_Nev, CsoportTag_Id);
                }
            }
        }
        return resultList;
    }

    private ListItem[] GetCsoportokListItemsByFelhasznalo(string Felhasznalo_Id, bool bCsakFelhasznaloSzervezetbeli, bool bHierarchiabanLefele)
    {
        List<string> listSzervezetCsoportok = new List<string>();
        if (bCsakFelhasznaloSzervezetbeli == true)
        {
            string FelhasznaloSzervezetId = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
            listSzervezetCsoportok.Add(FelhasznaloSzervezetId);
            if (bHierarchiabanLefele)
            {
                // minden, az aktu�lis felhaszn�l� szervezet�hez tartoz� csoport lek�rdez�se
                KRT_CsoportokService service_csoportok = eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam_csoportok = UI.SetExecParamDefault(Page, new ExecParam());
                KRT_CsoportTagokSearch search_csoportok = new KRT_CsoportTagokSearch();
                search_csoportok.Tipus.Value = KodTarak.CsoprttagsagTipus.alszervezet;
                search_csoportok.Tipus.Operator = Query.Operators.equals;

                KRT_Csoportok szuloCsoport = new KRT_Csoportok();
                szuloCsoport.Id = FelhasznaloSzervezetId;
                Result result_alcsoportok = service_csoportok.GetAllBySzuloCsoport(execParam_csoportok, szuloCsoport, search_csoportok);

                if (String.IsNullOrEmpty(result_alcsoportok.ErrorCode))
                {
                    foreach (System.Data.DataRow row in result_alcsoportok.Ds.Tables[0].Rows)
                    {
                        string Id = row["Csoport_Id_Jogalany"].ToString();
                        if (!String.IsNullOrEmpty(Id))
                        {
                            listSzervezetCsoportok.Add(Id);
                        }
                    }
                }
            }
        }

        ListItem[] resultList = null;

        KRT_CsoportTagokService service_csoporttagok = eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
        ExecParam execParam_csoporttagok = UI.SetExecParamDefault(Page, new ExecParam());
        KRT_CsoportTagokSearch search_csoporttagok = new KRT_CsoportTagokSearch();

        search_csoporttagok.Csoport_Id_Jogalany.Value = Felhasznalo_Id;
        search_csoporttagok.Csoport_Id_Jogalany.Operator = Query.Operators.equals;

        search_csoporttagok.OrderBy = "KRT_CsoportTagok.Csoport_Id ASC, KRT_CsoportTagok.Tipus DESC";

        Result result_csoporttagok = service_csoporttagok.GetAll(execParam_csoporttagok, search_csoporttagok);

        if (String.IsNullOrEmpty(result_csoporttagok.ErrorCode))
        {
            int cnt = result_csoporttagok.Ds.Tables[0].Rows.Count;
            if (cnt > 0)
            {
                string lastCsoport_Id = "";
                resultList = new ListItem[cnt];
                int i = 0;
                foreach (System.Data.DataRow row in result_csoporttagok.Ds.Tables[0].Rows)
                {
                    String Csoport_Id = row["Csoport_Id"].ToString();

                    if (lastCsoport_Id != Csoport_Id)
                    {
                        if (!bCsakFelhasznaloSzervezetbeli || listSzervezetCsoportok.Contains(Csoport_Id))
                        {
                            String CsoportTag_Id = row["Id"].ToString();
                            String Csoport_Nev = UI.RemoveEmailAddressFromCsoportNev(Contentum.eUtility.CsoportNevek_Cache.GetCsoportNevFromCache(Csoport_Id, Page));
                            resultList[i] = (new ListItem(Csoport_Nev, CsoportTag_Id));
                            i++;
                        }
                        lastCsoport_Id = Csoport_Id;
                    }
                }
                Array.Resize(ref resultList, i);
            }
        }
        return resultList;
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = null;
        if (searchObject != null) krt_HelyettesitesekSearch = (KRT_HelyettesitesekSearch)searchObject;

        if (krt_HelyettesitesekSearch != null)
        {
            Felhasznalo_ID_helyettesitett.Id_HiddenField = krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Value;
            Felhasznalo_ID_helyettesitett.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            Felhasznalo_ID_helyettesito.Id_HiddenField = krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesito.Value;
            Felhasznalo_ID_helyettesito.SetFelhasznaloTextBoxById(SearchHeader1.ErrorPanel);

            ListItem listItem = GetCsoportListItemByCsoporttagsag(krt_HelyettesitesekSearch.CsoportTag_ID_helyettesitett.Value);
            if (listItem != null)
            {
                // csak akkor adjuk hozz�, ha m�g nincs benne:
                if (Csoportok_DropDownList.Items.FindByValue(listItem.Value) == null)
                {
                    Csoportok_DropDownList.Items.Add(listItem);
                }
                Csoportok_DropDownList.SelectedValue = listItem.Value;
            }
            else
            {
                // csak akkor adunk hozz� �res elemet, ha m�g nincs benne:
                if (Csoportok_DropDownList.Items.FindByValue("") == null)
                {
                    ListItem emptyListItem = new ListItem(Resources.Form.UI_NoSelectedGroup, "");
                    Csoportok_DropDownList.Items.Add(emptyListItem);
                }
                Csoportok_DropDownList.SelectedValue = "";
            }

            Megjegyzes_TextBox.Text = krt_HelyettesitesekSearch.Megjegyzes.Value;

            Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl.DatumKezdValue = krt_HelyettesitesekSearch.HelyettesitesKezd.Value;
            Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl.DatumVegeValue= krt_HelyettesitesekSearch.HelyettesitesKezd.ValueTo;
            Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
                krt_HelyettesitesekSearch.HelyettesitesKezd);

            HelyettesitesVege_DatumIntervallum_SearchCalendarControl2.DatumKezdValue = krt_HelyettesitesekSearch.HelyettesitesVege.Value;
            HelyettesitesVege_DatumIntervallum_SearchCalendarControl2.DatumVegeValue= krt_HelyettesitesekSearch.HelyettesitesVege.ValueTo;
            HelyettesitesVege_DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(
                krt_HelyettesitesekSearch.HelyettesitesVege);



            //Ervenyesseg_SearchFormComponent1.SetDefault(
            //    krt_HelyettesitesekSearch.ErvKezd, krt_HelyettesitesekSearch.ErvVege);
        }

    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private KRT_HelyettesitesekSearch SetSearchObjectFromComponents()
    {
        KRT_HelyettesitesekSearch krt_HelyettesitesekSearch = (KRT_HelyettesitesekSearch)SearchHeader1.TemplateObject;
        if (krt_HelyettesitesekSearch == null)
        {
            krt_HelyettesitesekSearch = new KRT_HelyettesitesekSearch();
        }

        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Value = Felhasznalo_ID_helyettesitett.Id_HiddenField;
        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesitett.Operator = Search.GetOperatorByLikeCharater(Felhasznalo_ID_helyettesitett.Id_HiddenField);
        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesito.Value = Felhasznalo_ID_helyettesito.Id_HiddenField;
        krt_HelyettesitesekSearch.Felhasznalo_ID_helyettesito.Operator = Search.GetOperatorByLikeCharater(Felhasznalo_ID_helyettesito.Id_HiddenField);

        krt_HelyettesitesekSearch.HelyettesitesMod.Value = KodTarak.HELYETTESITES_MOD.Helyettesites;
        krt_HelyettesitesekSearch.HelyettesitesMod.Operator = Contentum.eQuery.Query.Operators.equals;

        if (!string.IsNullOrEmpty(Csoportok_DropDownList.SelectedValue))
        {
            krt_HelyettesitesekSearch.CsoportTag_ID_helyettesitett.Value = Csoportok_DropDownList.SelectedValue;
            krt_HelyettesitesekSearch.CsoportTag_ID_helyettesitett.Operator = Contentum.eQuery.Query.Operators.equals;
        }

        Helyettesites_Kezd_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            krt_HelyettesitesekSearch.HelyettesitesKezd);

        HelyettesitesVege_DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(
            krt_HelyettesitesekSearch.HelyettesitesVege);

        krt_HelyettesitesekSearch.Megjegyzes.Value = Megjegyzes_TextBox.Text;
                
        //Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
        //    krt_HelyettesitesekSearch.ErvKezd, krt_HelyettesitesekSearch.ErvVege);

        return krt_HelyettesitesekSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }

    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            KRT_HelyettesitesekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            ////kiv�lasztott rekord t�rl�se
            //JavaScripts.RegisterSelectedRecordIdToParent(Page, String.Empty);
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }       
    }


    private KRT_HelyettesitesekSearch GetDefaultSearchObject()
    {
        SearchHeader1.TemplateReset();
        return Contentum.eRecord.Utility.Search.CreateSearchObjWithDefFilter_Helyettesiteek();
    }


    //private void setDefaultValues()
    //{
    //    Felhasznalo_ID_helyettesitett.Text = "*";

    //    Ervenyesseg_SearchFormComponent1.SetDefault();
    //    TalalatokSzama_SearchFormComponent1.SetDefault();
    //}
}
