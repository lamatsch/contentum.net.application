using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

public partial class HistorySearch : Contentum.eUtility.UI.PageBase
{

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private KRT_CsoportokSearch SetSearchObjectFromComponents()
    {
        KRT_CsoportokSearch krt_CsoportokSearch = (KRT_CsoportokSearch)SearchHeader1.TemplateObject;

        return krt_CsoportokSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
    }

    private KRT_CsoportokSearch GetDefaultSearchObject()
    {
        return null;
    }

}

