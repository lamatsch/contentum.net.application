using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;

public partial class HitelesitesiZaradekFormTabPrintForm : Contentum.eUtility.UI.PageBase
{
    private string iratId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        iratId = Request.QueryString.Get(QueryStringVars.IratId);

        if (String.IsNullOrEmpty(iratId))
        {
            // nincs Id megadva:
        }
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents_Iratok()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch();

        erec_IraIratokSearch.Id.Value = iratId;
        erec_IraIratokSearch.Id.Operator = Query.Operators.equals;

        return erec_IraIratokSearch;
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("HitelesitesiZaradekFormSSRS.aspx?IratId="+iratId);
        
    }
}
