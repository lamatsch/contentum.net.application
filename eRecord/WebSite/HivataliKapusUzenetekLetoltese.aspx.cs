﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class HivataliKapusUzenetekLetoltese : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string result = "<result><status>{0}</status>{1}</result>";

        string error = PostaFiokFeldolgozas();

        if (!String.IsNullOrEmpty(error))
        {
            result = String.Format(result, "Error", String.Format("<error>{0}</error>",error));
        }
        else
        {
            result = String.Format(result, "Success", String.Empty);
        }

        WriteResponse(result);
    }

    string PostaFiokFeldolgozas()
    {
        try
        {
            Contentum.eIntegrator.Service.eUzenetService svc = Contentum.eIntegrator.Service.eIntegratorService.ServiceFactory.geteUzenetService();
            svc.PostaFiokFeldolgozas();
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

        return String.Empty;
    }

    void WriteResponse(string xml)
    {
        Response.Clear();
        Response.ContentType = "text/xml";
        Response.ContentEncoding = Encoding.UTF8;
        Response.ContentType = "text/xml; charset=utf-8";
        Response.Write(xml);
        Response.End();
    }
}