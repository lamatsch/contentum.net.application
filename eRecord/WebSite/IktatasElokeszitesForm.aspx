<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IktatasElokeszitesForm.aspx.cs" Inherits="IktatasElokeszitesForm" Title="Untitled Page" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="eRecordComponent/VonalKodTextBox.ascx" TagName="VonalKodTextBox"
    TagPrefix="uc15" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc14" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok"
    TagPrefix="uc12" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc21" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc22" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc23" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="ktddl" %>
<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/EditableCimekTextBox.ascx" TagName="CimekTextBox" TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/FileUploadComponent.ascx" TagName="FileUploadComponent"
    TagPrefix="uc10" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc8" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/TabFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FullCimField.ascx" TagName="FullCimField" TagPrefix="uc11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
    .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS 
    {
        width: 215px;
    }
 
    .width100
    {
        width:86%;
    }

 .NoScroll
    {
        overflow : auto;
    }
</style>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IktatasElokeszitesFormHeaderTitle%>" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress2" runat="server" />
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc7:CustomUpdateProgress id="CustomUpdateProgress1" runat="server">
    </uc7:CustomUpdateProgress>
    <eUI:eFormPanel ID="Adat_Panel" runat="server" CssClass="mrResultPanel" Width="90%">
        <asp:HiddenField ID="Kuldemeny_Id_HiddenField" runat="server"></asp:HiddenField>
        <br />
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tbody>
                <tr>
                    <td style="vertical-align: top; width: 100%; text-align: left">
                        <asp:Panel ID="IraIratFormPanel" runat="server">
                             <table cellspacing="0" cellpadding="0" width="100%" runat="server">
                                <tbody>
                                  <tr class="urlapSor">
                                      <td class="mrUrlapCaption_short">
                                          <asp:Label ID="LabelUgyirat" runat="server" Text="�gyirat:"></asp:Label>
                                      </td>
                                      <td style="width: 710px" class="mrUrlapMezo">
                                      <asp:UpdatePanel ID="iktatasElokeszitesUpdatePanel" runat="server" OnLoad="iktatasElokeszitesUpdatePanel_Load">
                                        <ContentTemplate>
                                             <eUI:eFormPanel ID="Ugyirat_Panel" runat="server" OnLoad="Ugyirat_PanelOnload">
                                             <table cellspacing="0" cellpadding="0" width="100%">
                                             <tbody>
                                               <tr id="tr_lezartazUgyirat" class="urlapSor_kicsi" runat="server" visible="false">
                                                   <td style="font-weight: bold; color: red; text-align: center" colspan="2">
                                                       <asp:Label ID="Label2" runat="server" Text="Figyelem: Lez�rt az el�zm�ny �gyirat!"></asp:Label>
                                                   </td>
                                                   <td style="text-align: left" colspan="2">
                                                   </td>
                                                </tr>
                                               <tr id="tr_lezartazIktatokonyv" class="urlapSor_kicsi" runat="server" visible="false">
                                                   <td style="font-weight: bold; color: red; text-align: center" colspan="2">
                                                   </td>
                                                   <td style="text-align: left" colspan="2">
                                                   </td>
                                               </tr>
                                               <tr id="tr_agazatiJel" class="urlapSor_kicsi" runat="server">
                                                   <td style="height: 22px" class="mrUrlapCaption_short">
                                                       <asp:Label ID="Label49" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapMezo">
                                                       <uc22:IraIktatoKonyvekDropDownList id="ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1"
                                                           runat="server">
                                                       </uc22:IraIktatoKonyvekDropDownList>
                                                       <uc22:IraIktatoKonyvekDropDownList id="IraIktatoKonyvekDropDownList1" runat="server">
                                                       </uc22:IraIktatoKonyvekDropDownList>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapCaption_short">
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapMezo">
                                                       <asp:ImageButton ID="ImageButton_Elozmeny" runat="server" CssClass="highlightit"
                                                           Visible="False" CausesValidation="false" CommandName="" ImageUrl="~/images/hu/trapezgomb/elozmeny.gif"
                                                           ToolTip="El�zm�ny"></asp:ImageButton>
                                                       <asp:ImageButton ID="ImageButton_ElozmenyUgyirat_Megtekintes" runat="server" CssClass="highlightit"
                                                           Visible="False" CommandName="" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg"
                                                           AlternateText="Megtekint�s"></asp:ImageButton>
                                                       <asp:HiddenField ID="ElozmenyUgyiratID_HiddenField" runat="server"></asp:HiddenField>
                                                       <asp:HiddenField ID="MigraltUgyiratID_HiddenField" runat="server"></asp:HiddenField>
                                                   </td>
                                               </tr>
                                               <tr class="urlapSor_kicsi">
                                                   <td class="mrUrlapCaption_short">
                                                       <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                                       <asp:Label ID="Label30" runat="server" Text="F�sz�m:"></asp:Label>
                                                   </td>
                                                   <td class="mrUrlapMezo">
                                                       <uc14:RequiredNumberBox id="Foszam_RequiredTextBox" runat="server" Validate="true">
                                                       </uc14:RequiredNumberBox>
                                                   </td>
                                                   <td class="mrUrlapCaption_short">
                                                       <asp:Label ID="LabelIktatoKonyv" runat="server" Text="*" CssClass="ReqStar">&nbsp;</asp:Label>
                                                   </td>
                                                   <td class="mrUrlapMezo">
                                                       &nbsp;&nbsp;
                                                   </td>
                                               </tr>
                                               <tr id="tr_ugytipus" class="urlapSor_kicsi" runat="server">
                                                   <td style="height: 22px" class="mrUrlapCaption_short">
                                                       <asp:Label ID="Ugyirat_targya_felirat" runat="server" Text="�gyirat t�rgya:"></asp:Label>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapMezo">
                                                       <asp:TextBox ID="Targy_TextBox" runat="server" CssClass="mrUrlapInput" Rows="2" ReadOnly="True"
                                                           TextMode="MultiLine"></asp:TextBox>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapCaption_short">
                                                       <asp:Label ID="Ugyintezo_felirat" runat="server" Text="�gyint�z�:"></asp:Label>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapMezo">
                                                       <uc23:FelhasznaloCsoportTextBox id="FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                                                           runat="server" ReadOnly="true" ViewMode="true">
                                                       </uc23:FelhasznaloCsoportTextBox>
                                                   </td>
                                               </tr>
                                               <tr id="tr1" class="urlapSor_kicsi" runat="server">
                                                   <td style="height: 22px" class="mrUrlapCaption_short">
                                                       <asp:Label ID="Label5" runat="server" Text="Hat�rid�:"></asp:Label>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapMezo">
                                                       <cc:CalendarControl id="Hatarido_CalendarControl" runat="server" ReadOnly="true" Validate="false">
                                                       </cc:CalendarControl>
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapCaption_short">
                                                   </td>
                                                   <td style="height: 22px" class="mrUrlapMezo">
                                                   </td>
                                               </tr>
                                           </tbody>
                                           </table>
                                          </eUI:eFormPanel>
                                              <uc12:UgyiratMasterAdatok id="UgyiratMasterAdatok1" runat="server">
                                              </uc12:UgyiratMasterAdatok>
                                        </ContentTemplate>
                                      </asp:UpdatePanel>
                                      </td>
                                  </tr>
                                  <tr class="urlapSor">
                                      <td class="mrUrlapCaption_short">
                                          <asp:Label ID="Label8" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                          <asp:Label ID="LabelCimzett" runat="server" Text="C�mzett:"></asp:Label>
                                      </td>
                                      <td class="mrUrlapMezo" style="width: 607px">
                                          <uc11:EditablePartnerTextBox ID="EditablePartnerTextBox1" runat="server" 
                                              CssClass="width100" />
                                      </td>
                                  </tr>
                                  <tr class="urlapSor">
                                      <td class="mrUrlapCaption_short" rowspan="1">
                                          <asp:Label ID="Label1" runat="server" Text="C�mzett C�me:"></asp:Label>
                                      </td>
                                      <td class="mrUrlapMezo">
                                          <table cellpadding="0" cellspacing="0" width="100%">
                                              <tr ID="tr_FullCimField" runat="server" class="urlapSor" style="width:99%">
                                                  <td class="mrUrlapMezo">
                                                      <uc11:FullCimField ID="FullCimField1" runat="server" Validate="false" />
                                                  </td>
                                              </tr>
                                              <tr class="urlapSor">
                                                  <td class="mrUrlapMezo" style="width: 607px">
                                                      <uc9:CimekTextBox ID="CimekTextBox1" runat="server" CssClass="width100" />
                                                  </td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                                  <tr class="urlapSor">
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label3" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="LabelTargy" runat="server" Text="T�rgy:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" style="width: 607px">
                                            <uc13:RequiredTextBox ID="TargyRequiredTextBox" runat="server" 
                                                CssClass="width100" Rows="2" TextBoxMode="MultiLine" />
                                        </td>
                                  </tr>
                                  <tr class="urlapSor">
                                      <td class="mrUrlapCaption_short">
                                          <asp:Label ID="LabelVonalkod" runat="server" Text="Vonalk�d:"></asp:Label>
                                      </td>
                                      <td style="width: 607px" class="mrUrlapMezo">
                                          <uc15:VonalKodTextBox ID="VonalkodTextBox" runat="server" __designer:wfdid="w2" 
                                              RequiredValidate="false"/>
                                      </td>
                                  </tr>
                                  <tr class="urlapSor">
                                      <td class="mrUrlapCaption_short">
                                          <asp:Label ID="Label9" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                          <asp:Label ID="Label7" runat="server" Text="Irat �gyint�z�je:"></asp:Label>
                                      </td>
                                      <td style="width: 607px" class="mrUrlapMezo">
                                          <uc23:FelhasznaloCsoportTextBox ID="IratUgyintezo_FelhasznaloCsoportTextBox" 
                                              runat="server" />
                                      </td>
                                  </tr>
                                  <tr class="urlapSor">
                                      <td class="mrUrlapCaption_short">
                                          <asp:Label ID="LabelFile" runat="server" Text="F�jl:"></asp:Label>
                                      </td>
                                      <td style="width: 607px; padding-top: 13px;" class="mrUrlapMezo">
                                          
                                                
                                                       <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                           <tr class="urlapSor">
                                                               <td>
                                                                   <uc10:FileUploadComponent ID="FileUploadComponent1" runat="server" />
                                                               </td>
                                                               <td style="text-align: right; vertical-align: middle;">
                                                                   <asp:Label ID="Labeloldalszam" runat="server" CssClass="mrUrlapCaption" 
                                                                       Text="Oldalsz�m:"></asp:Label>
                                                                   <uc14:RequiredNumberBox ID="OldalszamNumberBox" runat="server" 
                                                                       __designer:wfdid="w3" Validate="false" />
                                                               </td>
                                                           </tr>
                                                       </table>
                                                                                               
                                             
                                      </td>
                                  </tr>
                                </tbody>
                             </table>
                            <%--<asp:CheckBox ID="MunkaPeldanyCheckBox" runat="server" Text="Munkap�ld�ny k�sz�t�se" Checked="true"
                        Visible="True" Width="305px"></asp:CheckBox>--%>
                            <asp:RadioButtonList ID="MunkaPeldany_RadioButtonList" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0">Munkap�ld�ny k�sz�t�se</asp:ListItem>
                                <asp:ListItem Value="1">Irat iktat�sa</asp:ListItem>
                            </asp:RadioButtonList>
                        </asp:Panel>
                    </td>
                </tr>
            </tbody>
        </table>            
    </eUI:eFormPanel>
    <eUI:eFormPanel ID="ResultPanel" runat="server" __designer:dtid="3659174697238781"
        CssClass="mrResultPanel" __designer:wfdid="w1" Visible="false" Width="90%">
        <div class="mrResultPanelText">
            <asp:Label ID="Label_IktatasSuccess" CssClass="mrResultPanelText" runat="server" Text="Az iktat�s sikeresen v�grehajt�dott." Visible="false"></asp:Label>
            <asp:Label ID="Label_MunkaanyagLetrehozasSuccess" runat="server" Text="A munkaanyag l�trej�tt."></asp:Label>
            </div>
        <table class="mrResultTable" cellspacing="0" cellpadding="0" width="800" __designer:dtid="3659174697238783">
            <tbody>
                <tr class="urlapSorBigSize" __designer:dtid="3659174697238784">
                    <td class="mrUrlapCaptionBigSize" __designer:dtid="3659174697238785">
                        <asp:Label ID="Label6" runat="server" __designer:dtid="3659174697238786" Text="�gyirat iktat�sz�ma:"
                            __designer:wfdid="w2"></asp:Label>
                    </td>
                    <td class="mrUrlapMezoBigSize" __designer:dtid="3659174697238787">
                        <asp:Label ID="labelUgyiratFoszam" runat="server" __designer:dtid="3659174697238788"
                            Text="" __designer:wfdid="w3"></asp:Label>
                    </td>
                    <td style="padding-top: 5px" class="mrUrlapCaption" __designer:dtid="3659174697238789">
                        <asp:ImageButton ID="imgUgyiratMegtekint" onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');"
                            onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" runat="server" __designer:dtid="3659174697238790"
                            __designer:wfdid="w4" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg">
                        </asp:ImageButton>
                    </td>
                    <td style="padding-top: 5px" class="mrUrlapMezo" __designer:dtid="3659174697238791">
                        <span style="padding-right: 80px" __designer:dtid="3659174697238792">
                            <asp:ImageButton ID="imgUgyiratModosit" onmouseover="swapByName(this.id,'modositas_trap2.jpg');"
                                onmouseout="swapByName(this.id,'modositas_trap.jpg');" runat="server" __designer:dtid="3659174697238793"
                                __designer:wfdid="w5" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"></asp:ImageButton>
                        </span>
                    </td>
                </tr>
                <tr style="padding-bottom: 20px" class="urlapSorBigSize" __designer:dtid="3659174697238794">
                    <td class="mrUrlapCaptionBigSize" __designer:dtid="3659174697238795">
                        <asp:Label ID="labelIrat" runat="server" Text="Irat iktat�sz�ma:" Visible="false"></asp:Label>
                        <asp:Label ID="labelMunkaanyag" runat="server" Text="Irat(Munkaanyag):"></asp:Label>
                    </td>
                    <td class="mrUrlapMezoBigSize" __designer:dtid="3659174697238797">
                        <asp:Label ID="labelIratFoszam" runat="server" __designer:dtid="3659174697238798"
                            Text="" __designer:wfdid="w7"></asp:Label>
                    </td>
                    <td style="padding-top: 5px" class="mrUrlapCaption" __designer:dtid="3659174697238799">
                        <asp:ImageButton ID="imgIratMegtekint" onmouseover="swapByName(this.id,'megtekintes_trap2.jpg');"
                            onmouseout="swapByName(this.id,'megtekintes_trap.jpg');" runat="server" __designer:dtid="3659174697238800"
                            __designer:wfdid="w8" ImageUrl="~/images/hu/trapezgomb/megtekintes_trap.jpg">
                        </asp:ImageButton>
                    </td>
                    <td style="padding-top: 5px" class="mrUrlapMezo" __designer:dtid="3659174697238801">
                        <span style="padding-right: 80px" __designer:dtid="3659174697238802">
                            <asp:ImageButton ID="imgIratModosit" onmouseover="swapByName(this.id,'modositas_trap2.jpg');"
                                onmouseout="swapByName(this.id,'modositas_trap.jpg');" runat="server" __designer:dtid="3659174697238803"
                                __designer:wfdid="w9" ImageUrl="~/images/hu/trapezgomb/modositas_trap.jpg"></asp:ImageButton>
                        </span>
                    </td>
                </tr>
                <tr style="padding-bottom: 20px" class="urlapSor" __designer:dtid="3659174697238804">
                    <td class="mrUrlapCaptionBigSize" __designer:dtid="3659174697238805">
                    </td>
                    <td class="mrUrlapMezoBigSize" __designer:dtid="3659174697238806">
                    </td>
                    <td style="padding-top: 5px" class="mrUrlapCaption" __designer:dtid="3659174697238807">
                    </td>
                    <td style="padding-top: 5px" class="mrUrlapMezo" __designer:dtid="3659174697238808">
                    </td>
                </tr>
                
            </tbody>
        </table>
    </eUI:eFormPanel>
    <uc2:FormFooter id="FormFooter1" runat="server">
    </uc2:FormFooter>
</asp:Content>
