using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;


public partial class IktatasElokeszitesForm : Contentum.eUtility.UI.PageBase
{
    public const string StHeader = "$Header: IktatasElokeszitesForm.aspx.cs, 30, 2016.12.28. 17:57:48, Tajti M�ty�s$";
    public const string StVersion = "$Revision: 30$";

    private string Command = "";
    private EREC_UgyUgyiratok obj_Ugyirat = null;
    private String UgyiratId = "";
    private String IraIratId = "";

    private ComponentSelectControl compSelector = null;
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private bool EditableUgyiratMasterAdatok = true;

    private IktatasiParameterek iktatasiParameterek = null;

    protected IktatasiParameterek IktatasiParameterek
    {
        get
        {
            if (iktatasiParameterek == null)
                iktatasiParameterek = new IktatasiParameterek();
            return iktatasiParameterek;
        }
        set { iktatasiParameterek = value; }
    }


    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IktatasElokeszites");
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        Logger.Info("IktatasElokeszitesForm Page_Init");
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IktatasElokeszites");
                break;
        }

        FormFooter1.ImageButton_Close.Visible = true;
        FormFooter1.ImageButton_Save.Visible = true;


        if (Command == CommandName.New) // Iktat�s
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.IktatasElokeszitesFormHeaderTitle;
        }

        UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);
        Logger.Info("UgyiratId" + UgyiratId);

        if (String.IsNullOrEmpty(UgyiratId))
        {
            EditableUgyiratMasterAdatok = true;
        }
        else
        {
            EditableUgyiratMasterAdatok = false;
        }

        if (!EditableUgyiratMasterAdatok)
        {
            UgyiratMasterAdatok1.Visible = true;
            Ugyirat_Panel.Visible = false;


           
                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

                UgyiratMasterAdatok1.UgyiratId = UgyiratId;
                obj_Ugyirat = UgyiratMasterAdatok1.SetUgyiratMasterAdatokById(FormHeader1.ErrorPanel);
                

        }
        else
        {
            
            UgyiratMasterAdatok1.Visible = false;
            Ugyirat_Panel.Visible = true;

            IraIktatoKonyvekDropDownList1.FillDropDownList(true, true, Constants.IktatoErkezteto.Iktato,true, true, FormHeader1.ErrorPanel);

            //Iktatokonyvek_DropDownLis_Ajax.Visible = true;
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Visible = false;
            ElozmenyUgyIrat_IraIktatoKonyvekDropDownList1.Enabled = false;

        }
        //Partner �s c�m �sszek�t�se
        EditablePartnerTextBox1.CimTextBox = CimekTextBox1.TextBox;
        EditablePartnerTextBox1.CimHiddenField = CimekTextBox1.HiddenField;

        //Vonalk�d textboxon �lljon a kurzor
        VonalkodTextBox.TextBox.Focus();

        //F�sz�mb�l kil�p�sre t�lti az �gyirat cuccait
        Foszam_RequiredTextBox.TextBox.Attributes["onblur"] += "__doPostBack('" + iktatasElokeszitesUpdatePanel.ClientID + "','" + EventArgumentConst.refreshUgyiratok + "');";

        //Cim �sszek�t�se
        FullCimField1.CimTextBox = CimekTextBox1.TextBox;
        FullCimField1.CimHiddenField = CimekTextBox1.HiddenField;
    }

    protected void iktatasElokeszitesUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshUgyiratok:
                    if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue.ToString()) && !String.IsNullOrEmpty(Foszam_RequiredTextBox.Text))
                    {
                        String IktatokonyvId = IraIktatoKonyvekDropDownList1.SelectedValue.ToString();
                        String Foszam = Foszam_RequiredTextBox.Text;
                        EREC_UgyUgyiratokService _EREC_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

                        _EREC_UgyUgyiratokSearch.Foszam.Filter(Foszam);
                        _EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Filter(IktatokonyvId);

                        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result res_ugyiratGetAll = _EREC_UgyUgyiratokService.GetAll(execparam, _EREC_UgyUgyiratokSearch);
                        if (String.IsNullOrEmpty(res_ugyiratGetAll.ErrorCode))
                        {
                            if (res_ugyiratGetAll.GetCount == 1)
                            {
                                // �llapot ellen�rz�s: lehet-e az �gyirathoz iktatni?
                                EREC_UgyUgyiratok _EREC_UgyUgyiratok = new EREC_UgyUgyiratok();

                                execparam.Record_Id = res_ugyiratGetAll.Ds.Tables[0].Rows[0]["Id"].ToString();

                                Result res_ugyiratGet = _EREC_UgyUgyiratokService.Get(execparam);
                                if (res_ugyiratGet.IsError)
                                {
                                    Targy_TextBox.Text = "";
                                    FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = "";
                                    FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.Text = "";
                                    Hatarido_CalendarControl.Text = "";
                                    Logger.Warn("res_ugyiratGet.error = " + res_ugyiratGet.ErrorMessage);
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res_ugyiratGet);
                                    ErrorUpdatePanel1.Update();
                                }
                                else
                                {
                                    _EREC_UgyUgyiratok = (EREC_UgyUgyiratok)res_ugyiratGet.Record;
                                    Contentum.eRecord.BaseUtility.Ugyiratok.Statusz ugyiratStatusz =
                                        Contentum.eRecord.BaseUtility.Ugyiratok.GetAllapotByBusinessDocument(_EREC_UgyUgyiratok);
                                    ErrorDetails errorDetail = null;

                                    if (Contentum.eRecord.BaseUtility.Ugyiratok.LehetAlszamraIktatni(execparam, ugyiratStatusz, out errorDetail) == false)
                                    {
                                        Targy_TextBox.Text = "";
                                        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = "";
                                        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.Text = "";
                                        Hatarido_CalendarControl.Text = "";
                                        Logger.Warn("�llapotellen�rz�s: alsz�mos iktat�s nem lehets�ges az �gyiratra", execparam);
                                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, ResultError.CreateNewResultWithErrorCode(52109, errorDetail));
                                        ErrorUpdatePanel1.Update();
                                    }
                                    else
                                    {
                                        Targy_TextBox.Text = res_ugyiratGetAll.Ds.Tables[0].Rows[0]["Targy"].ToString();
                                        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = res_ugyiratGetAll.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Ugyintez"].ToString();
                                        if (!String.IsNullOrEmpty(FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
                                        {
                                            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                                        }
                                        Hatarido_CalendarControl.Text = res_ugyiratGetAll.Ds.Tables[0].Rows[0]["Hatarido"].ToString();

                                    }

                                }

                                //return res_ugyiratGet.Ds.Tables[0].Rows[0][Value].ToString();


                            }
                            else
                            {
                                Targy_TextBox.Text = "";
                                FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = "";
                                FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.Text = "";
                                Hatarido_CalendarControl.Text = "";
                                Result result = new Result();
                                Logger.Warn("Nem tal�lt �gyiratot!");
                                result.ErrorCode = "222222";
                                result.ErrorMessage = "Nem tal�lt �gyiratot az al�bbi f�sz�mra: " + Foszam_RequiredTextBox.Text + "!";
                                Foszam_RequiredTextBox.Text = "";
                                //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }
                        else
                        {
                            Targy_TextBox.Text = "";
                            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = "";
                            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.Text = "";
                            Hatarido_CalendarControl.Text = "";
                            Foszam_RequiredTextBox.Text = "";
                            Logger.Error("res_ugyiratGet.error = " + res_ugyiratGetAll.ErrorMessage);
                            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res_ugyiratGet);
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res_ugyiratGetAll);
                            ErrorUpdatePanel1.Update();

                        }
                    }
                    else
                    {
                        Targy_TextBox.Text = "";
                        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = "";
                        FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.TextBox.Text = "";
                        Hatarido_CalendarControl.Text = "";
                        Foszam_RequiredTextBox.Text = "";

                    }
                    //Targy_TextBox.Text = getValueByUgyiratid("Targy");
                    ScriptManager1.SetFocus(EditablePartnerTextBox1.TextBox);
                    //EditablePartnerTextBox1.TextBox.Focus();
                    break;
            }
        }
    }
    protected void Ugyirat_PanelOnload(object sender, EventArgs e)
    {
    //    if (Request.Params["__EVENTARGUMENT"] != null)
    //    {
    //        string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
    //        switch (eventArgument)
    //        {
    //            case "refreshFoszam":
    //                if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue.ToString()) && !String.IsNullOrEmpty(Foszam_RequiredTextBox.Text))
    //                {
    //                    String IktatokonyvId = IraIktatoKonyvekDropDownList1.SelectedValue.ToString();
    //                    String Foszam = Foszam_RequiredTextBox.Text;
    //                    EREC_UgyUgyiratokService _EREC_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
    //                    EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

    //                    _EREC_UgyUgyiratokSearch.Foszam.Value = Foszam;
    //                    _EREC_UgyUgyiratokSearch.Foszam.Operator = Contentum.eQuery.Query.Operators.equals;

    //                    _EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Value = IktatokonyvId;
    //                    _EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;

    //                    ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

    //                    Result res_ugyiratGet = _EREC_UgyUgyiratokService.GetAll(execparam, _EREC_UgyUgyiratokSearch);
    //                    if (String.IsNullOrEmpty(res_ugyiratGet.ErrorCode))
    //                    {
    //                        if (res_ugyiratGet.Ds.Tables[0].Rows.Count == 1)
    //                        {
    //                            //return res_ugyiratGet.Ds.Tables[0].Rows[0][Value].ToString();
    //                            Targy_TextBox.Text = res_ugyiratGet.Ds.Tables[0].Rows[0]["Targy"].ToString();
    //                            FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField = res_ugyiratGet.Ds.Tables[0].Rows[0]["FelhasznaloCsoport_Id_Ugyintez"].ToString();
    //                            if (!String.IsNullOrEmpty(FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
    //                            {
    //                                FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
    //                            }
    //                            Hatarido_CalendarControl.Text = res_ugyiratGet.Ds.Tables[0].Rows[0]["Hatarido"].ToString();


    //                        }
    //                    }
    //                }
    //                //Targy_TextBox.Text = getValueByUgyiratid("Targy");
    //                break;
    //        }
    //    }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        FormFooter1.Command = CommandName.New;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        FileUploadComponent1.Validate = false;
        CimekTextBox1.Validate = false;

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible == true)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FileUploadComponent1.Enabled = false;
        if (Rendszerparameterek.Get(Page, Rendszerparameterek.VONALKOD_KEZELES).Equals("AZONOSITO"))
        {
            VonalkodTextBox.ReadOnly = true;
        }
    }

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }
    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(UgyiratMasterAdatok1);
            compSelector.Add_ComponentOnClick(EditablePartnerTextBox1);
            compSelector.Add_ComponentOnClick(CimekTextBox1);
            compSelector.Add_ComponentOnClick(TargyRequiredTextBox);
            compSelector.Add_ComponentOnClick(VonalkodTextBox);
            //compSelector.Add_ComponentOnClick(FileUploadComponent1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        Logger.Info("FormFooter1ButtonsClick (" + e.CommandName + ")");
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IktatasElokeszites"))
            {
                if (EditableUgyiratMasterAdatok)
                {
                    #region �gyirat meghat�roz�sa iktat�k�nyv �s f�sz�m alapj�n

                    if (String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue.ToString()))
                    {
                        Result result = new Result();
                        Logger.Error("Nincs iktat�k�nyv kiv�lasztva!");
                        result.ErrorCode = "222222";
                        result.ErrorMessage = "Nincs iktat�k�nyv kiv�lasztva!";
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();
                        return;
                    }
                    else
                    {
                        string IktatokonyvId = IraIktatoKonyvekDropDownList1.SelectedValue.ToString();
                        string Foszam = Foszam_RequiredTextBox.Text;
                        EREC_UgyUgyiratokService _EREC_UgyUgyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        EREC_UgyUgyiratokSearch _EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

                        _EREC_UgyUgyiratokSearch.Foszam.Value = Foszam;
                        _EREC_UgyUgyiratokSearch.Foszam.Operator = Contentum.eQuery.Query.Operators.equals;

                        _EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Value = IktatokonyvId;
                        _EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                        ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

                        Result res_ugyiratGet = _EREC_UgyUgyiratokService.GetAll(execparam, _EREC_UgyUgyiratokSearch);
                        if (!String.IsNullOrEmpty(res_ugyiratGet.ErrorCode))
                        {
                            Logger.Error("res_ugyiratGet.Error = " + res_ugyiratGet.ErrorMessage);
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res_ugyiratGet);
                            ErrorUpdatePanel1.Update();
                            return;
                        }
                        else
                        {
                            if (res_ugyiratGet.Ds.Tables[0].Rows.Count == 0)
                            {
                                Logger.Error("Iktat�k�nyv �s f�sz�m alapj�n nem siker�lt azonos�tani �gyiratot!");
                                Result resultUgyiraterror = new Result();
                                resultUgyiraterror.ErrorCode = "1111111";
                                resultUgyiraterror.ErrorMessage = "Iktat�k�nyv �s f�sz�m alapj�n nem siker�lt azonos�tani �gyiratot!";
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultUgyiraterror);
                                ErrorUpdatePanel1.Update();
                                return;
                            }
                            else
                            {
                                System.Data.DataSet ds = res_ugyiratGet.Ds;
                                UgyiratId = ds.Tables[0].Rows[0]["Id"].ToString();
                            }
                        }
                    }
                    #endregion
                }

                if (String.IsNullOrEmpty(UgyiratId))
                {
                    // nem hat�rozhat� meg az �gyirat
                    Result resultUgyiraterror = new Result();
                    resultUgyiraterror.ErrorCode = "1111111";
                    resultUgyiraterror.ErrorMessage = "Nem siker�lt azonos�tani az �gyiratot!";
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultUgyiraterror);
                    ErrorUpdatePanel1.Update();
                    return;
                }

                EREC_IraIratokService service_iratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                #region Irat obj. �ssze�ll�t�s

                EREC_IraIratok iratAdatok = new EREC_IraIratok();

                iratAdatok.Updated.SetValueAll(false);
                iratAdatok.Updated.SetValueAll(false);

                iratAdatok.FelhasznaloCsoport_Id_Ugyintez = IratUgyintezo_FelhasznaloCsoportTextBox.Id_HiddenField;
                iratAdatok.Updated.FelhasznaloCsoport_Id_Ugyintez = true;

                iratAdatok.Targy = TargyRequiredTextBox.Text;
                iratAdatok.Updated.Targy = pageView.GetUpdatedByView(TargyRequiredTextBox);

                #endregion

                #region Iratp�ld�ny obj. �ssze�ll�t�sa

                EREC_PldIratPeldanyok pld_Adatok = new EREC_PldIratPeldanyok();

                pld_Adatok.Updated.SetValueAll(false);
                pld_Adatok.Base.Updated.SetValueAll(false);

                pld_Adatok.Cim_id_Cimzett = CimekTextBox1.Id_HiddenField;
                pld_Adatok.Updated.Cim_id_Cimzett = pageView.GetUpdatedByView(CimekTextBox1);

                pld_Adatok.CimSTR_Cimzett = CimekTextBox1.Text;
                pld_Adatok.Updated.CimSTR_Cimzett = pageView.GetUpdatedByView(CimekTextBox1);

                pld_Adatok.Partner_Id_Cimzett = EditablePartnerTextBox1.Id_HiddenField;
                pld_Adatok.Updated.Partner_Id_Cimzett = pageView.GetUpdatedByView(EditablePartnerTextBox1);

                pld_Adatok.NevSTR_Cimzett = EditablePartnerTextBox1.Text;
                pld_Adatok.Updated.NevSTR_Cimzett = pageView.GetUpdatedByView(EditablePartnerTextBox1);

                pld_Adatok.Eredet = KodTarak.IRAT_FAJTA.Eredeti;
                pld_Adatok.Updated.Eredet = true;

                // Vonalk�d:
                pld_Adatok.BarCode = VonalkodTextBox.Text;
                pld_Adatok.Updated.BarCode = true;

                #endregion

                if (MunkaPeldany_RadioButtonList.SelectedValue == "0")
                {
                    #region Munkap�ld�ny l�trehoz�s

                    Result result = service_iratok.IktatasElokeszites(execParam, UgyiratId, iratAdatok, pld_Adatok);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();
                        return;
                    }

                    IraIratId = result.Uid;

                    #endregion
                }
                else
                {
                    #region Irat iktat�sa

                    Result result = service_iratok.BelsoIratIktatasa_Alszamra(execParam, IraIktatoKonyvekDropDownList1.SelectedValue, UgyiratId,
                            null, iratAdatok, null, pld_Adatok, IktatasiParameterek);
                    if (!String.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();
                        return;
                    }

                    IraIratId = result.Uid;

                    #endregion
                }



                //if (!String.IsNullOrEmpty(VonalkodTextBox.Text))                        
                //{
                //    //van mell�klet
                //    EREC_IratMellekletek _EREC_IratMellekletek = new EREC_IratMellekletek();
                //    EREC_IratMellekletekService _EREC_IratMellekletekService = eRecordService.ServiceFactory.GetEREC_IratMellekletekService();

                //    _EREC_IratMellekletek.IraIrat_Id = IraIratId;
                //    _EREC_IratMellekletek.Updated.IraIrat_Id = true;

                //    _EREC_IratMellekletek.Mennyiseg = "1";
                //    _EREC_IratMellekletek.Updated.Mennyiseg = true;

                //    _EREC_IratMellekletek.AdathordozoTipus = KodTarak.AdathordozoTipus.PapirAlapu;
                //    _EREC_IratMellekletek.Updated.AdathordozoTipus = true;

                //    _EREC_IratMellekletek.MennyisegiEgyseg = KodTarak.MennyisegiEgyseg.Darab;
                //    _EREC_IratMellekletek.Updated.MennyisegiEgyseg = true;

                //    _EREC_IratMellekletek.BarCode = VonalkodTextBox.Text;
                //    _EREC_IratMellekletek.Updated.BarCode = pageView.GetUpdatedByView(VonalkodTextBox);

                //    Logger.Info("EREC_IratMellekletekService.Insert");
                //    Result res_MellekletekInsert = _EREC_IratMellekletekService.Insert(execparam, _EREC_IratMellekletek);
                //    if (!String.IsNullOrEmpty(res_MellekletekInsert.ErrorCode))
                //    {
                //        VoltHiba = true;
                //        Logger.Error("res_MellekletekInsert.Error = " + res_MellekletekInsert.ErrorMessage);
                //        //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res_PldInsert);
                //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res_PldInsert);
                //        ErrorUpdatePanel1.Update();


                //    }
                //}



                if (!String.IsNullOrEmpty(FileUploadComponent1.Value_HiddenField))
                {
                    Logger.Info("Csatolm�ny felt�lt�s!");
                    //van csatolm�ny
                    Result result = new Result();

                    Csatolmany csatolmany = new Csatolmany();
                    String directory = "";
                    String file = "";
                    String[] tomb = FileUploadComponent1.Value_HiddenField.Split(';');
                    if (tomb == null || tomb.Length < 2)
                    {
                        result.ErrorCode = "222222";
                        result.ErrorMessage = "Nincs meg a file!";
                        Logger.Error("Nincs meg a file!");
                        //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                        ErrorUpdatePanel1.Update();

                        return;
                    }
                    directory = tomb[0];
                    if (!directory.EndsWith("\\")) { directory += "\\"; }
                    int tombindex = 1;
                    while (tombindex < tomb.Length)
                    {
                        int filestart = tomb[tombindex].LastIndexOf("\\") + 1;
                        if (filestart > -1)
                        {
                            file = tomb[tombindex].Substring(filestart);
                        }

                        if (!System.IO.Directory.Exists(directory))
                        {
                            result.ErrorCode = "222222";
                            result.ErrorMessage = "Nem l�tezik a k�nyvt�r a szerveren!";
                            Logger.Error("Nem l�tezik a k�nyvt�r a szerveren!");
                            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();

                            return;
                        }
                        if (!System.IO.File.Exists(directory + file))
                        {
                            result.ErrorCode = "222222";
                            result.ErrorMessage = "Nem l�tezik a file a szerveren!";
                            Logger.Error("Nem l�tezik a file a szerveren!");
                            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();

                            return;
                        }

                        try
                        {
                            csatolmany.Tartalom = Contentum.eRecord.Utility.FileFunctions.GetFile(directory + file);
                            csatolmany.Nev = file;
                        }
                        catch (Exception)
                        {
                            result.ErrorCode = "222222";
                            result.ErrorMessage = "Hiba a file beolvas�sakor!";
                            Logger.Error("Hiba a file beolvas�sakor!");
                            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();

                            return;
                        }

                        if (csatolmany.Tartalom == null)
                        {
                            result.ErrorCode = "222222";
                            result.ErrorMessage = "Hiba a file beolvas�sakor: null a tartalom!";
                            Logger.Error("Hiba a file beolvas�sakor: null a tartalom!");
                            //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                            ErrorUpdatePanel1.Update();

                            return;
                        }

                        execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        if (FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.New))
                        {
                            //EREC_IraIratokDokumentumok erec_IraIratokDokumentumok = (EREC_IraIratokDokumentumok)GetBusinessObjectFromComponents();
                            EREC_Csatolmanyok erec_Csatolmanyok = new EREC_Csatolmanyok();

                            erec_Csatolmanyok.IraIrat_Id = IraIratId;
                            erec_Csatolmanyok.Updated.IraIrat_Id = true;

                            erec_Csatolmanyok.Leiras = TargyRequiredTextBox.Text;
                            erec_Csatolmanyok.Updated.Leiras = pageView.GetUpdatedByView(TargyRequiredTextBox);

                            if (tombindex == 1)
                            {
                                csatolmany.BarCode = VonalkodTextBox.Text;

                                erec_Csatolmanyok.Lapszam = OldalszamNumberBox.Text;
                                erec_Csatolmanyok.Updated.Lapszam = pageView.GetUpdatedByView(OldalszamNumberBox);
                            }

                            EREC_IraIratokService erec_IraIratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                            execParam.Record_Id = IraIratId;

                            Logger.Info("erec_IraIratokService.CsatolmanyUpload");
                            result = erec_IraIratokService.CsatolmanyUpload(execParam, erec_Csatolmanyok, csatolmany);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                if (!Contentum.eRecord.Utility.FileFunctions.DeleteFile(directory + file))
                                {
                                    // TODO
                                }
                            }
                            else
                            {
                                Logger.Error("result.Error = " + result.ErrorMessage);
                                //ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                                return;
                            }
                        }
                        else
                        {
                            UI.RedirectToAccessDeniedErrorPage(Page);
                        }
                        tombindex++;
                    }

                }

                // Iratok lista erre �lljon r�:
                JavaScripts.RegisterSelectedRecordIdToParent(Page, IraIratId);


                // ha nem volt hiba, eredm�nyPanel megjelen�t�se:
                this.SetResultPanel(UgyiratId, IraIratId);



            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel1);
            }
        }
    }
       
    /// <summary>
    /// Eredm�nyPanel megjelen�t�se
    /// </summary>    
    private void SetResultPanel(string ugyiratId, string iratId)
    {
        if (String.IsNullOrEmpty(ugyiratId) || String.IsNullOrEmpty(iratId)) return;
        try
        {
            //Panel-ek be�ll�t�sa
            Adat_Panel.Visible = false;
            ResultPanel.Visible = true;

            // C�mk�k megjelen�t�se (Munkap�ld�ny vagy Irat):
            if (MunkaPeldany_RadioButtonList.SelectedValue == "0")
            {
                Label_IktatasSuccess.Visible = false;
                Label_MunkaanyagLetrehozasSuccess.Visible = true;
                labelIrat.Visible = false;
                labelMunkaanyag.Visible = true;
            }
            else
            {
                Label_IktatasSuccess.Visible = true;
                Label_MunkaanyagLetrehozasSuccess.Visible = false;
                labelIrat.Visible = true;
                labelMunkaanyag.Visible = false;
            }

            //F�sz�mok be�ll�t�sa
            ExecParam xcParam = UI.SetExecParamDefault(Page, new ExecParam());
            //Ugyirat
            EREC_UgyUgyiratokService srvUgyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            EREC_UgyUgyiratokSearch searchUgyiratok = new EREC_UgyUgyiratokSearch(); ;
            searchUgyiratok.Id.Value = ugyiratId;
            searchUgyiratok.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            Result res = srvUgyiratok.GetAllWithExtension(xcParam, searchUgyiratok);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new Contentum.eUtility.ResultException(res);
            }
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                throw new Contentum.eUtility.ResultException(52106);
            }
            labelUgyiratFoszam.Text = res.Ds.Tables[0].Rows[0]["Foszam_Merge"].ToString();
            

            //Irat
            EREC_IraIratokService srvIratok = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch searchIratok = new EREC_IraIratokSearch();
            searchIratok.Id.Value = iratId;
            searchIratok.Id.Operator = Contentum.eQuery.Query.Operators.equals;
            res = srvIratok.GetAllWithExtension(xcParam, searchIratok);
            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                throw new Contentum.eUtility.ResultException(res);
            }
            if (res.Ds.Tables[0].Rows.Count == 0)
            {
                throw new Contentum.eUtility.ResultException(52118);
            }
            labelIratFoszam.Text = res.Ds.Tables[0].Rows[0]["IktatoSzam_Merge"].ToString();
            //TabFooter be�ll�t�sa
            FormFooter1.ImageButton_Close.Visible = true;
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue='" + EventArgumentConst.refreshMasterList + "'; window.close(); return false;";
            FormFooter1.Link_SaveAndNew.Visible = true;
            FormFooter1.Link_SaveAndAlszam.Visible = true;
            FormFooter1.ImageButton_Save.Visible = false;
            FormFooter1.ImageButton_Cancel.Visible = false;


            string saveAndNewUrl = String.Empty;
           

            saveAndNewUrl = "~/IktatasElokeszitesForm.aspx?"
            + QueryStringVars.Command + "=" + CommandName.New;
            //+ "&" + QueryStringVars.Mode + "=" + CommandName.ElokeszitettIratIktatas;

            FormFooter1.Link_SaveAndNew.NavigateUrl = saveAndNewUrl;

            string saveAndAlszamUrl = String.Empty;
           
            saveAndAlszamUrl = "~/IktatasElokeszitesForm.aspx?"
                + QueryStringVars.Command + "=" + CommandName.New
                + "&" + QueryStringVars.UgyiratId + "=" + ugyiratId;

            FormFooter1.Link_SaveAndAlszam.NavigateUrl = saveAndAlszamUrl;

            //M�dos�t�s,megtekint�s gombok be�ll�t�sa
            //�gyirat
            imgUgyiratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgUgyiratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + ugyiratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);
            //irat
            imgIratMegtekint.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.View +
                                            "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

            imgIratModosit.OnClientClick = JavaScripts.SetOnCLientClick_NoPostBack("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify +
                                            "&" + QueryStringVars.Id + "=" + iratId, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        }
        catch (Contentum.eUtility.ResultException e)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(e);
            // hiba
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            ErrorUpdatePanel1.Update();
        }


    }

}
