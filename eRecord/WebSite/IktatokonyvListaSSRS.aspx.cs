﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class IktatokonyvListaSSRS : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //UgyUgyirat_FelhCsoportId_Ugyintezo_CsoportTextBox.Reset

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);

        Ev_DropDownList.AutoPostBack = true;
        Ev_DropDownList.SelectedIndexChanged += new EventHandler(DatumIntervallum_SearchCalendarControl1_DatumChanged);

        if (!IsPostBack)
        {
            for (int i = 2009; i <= System.DateTime.Today.Year; i++)
            {
                Ev_DropDownList.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;


            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ShowReportBody = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }
        }
    }

    void DatumIntervallum_SearchCalendarControl1_DatumChanged(object sender, EventArgs e)
    {
        IraIktatoKonyvekDropDownList1.DropDownList.Items.Clear();

        IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, Ev_DropDownList.SelectedValue, Ev_DropDownList.SelectedValue
                , true, false, EErrorPanel1);

        ReportViewer1.ShowReportBody = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ImageButton_Refresh.Click += new ImageClickEventHandler(ImageButton_Refresh_Click);

        if (!IsPostBack)
        {
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, Ev_DropDownList.SelectedValue, Ev_DropDownList.SelectedValue
                , true, false, EErrorPanel1);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    protected void ImageButton_Refresh_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewer1.ServerReport.Refresh();

        ReportViewer1.ShowReportBody = true;
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        string where = "";
        string iktatokonyv = "";

        //string date = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10) + "-tól " + DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10) + "-ig";
        //string _datekezd = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10).Replace(".", "-");
        //string _datevege = DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10).Replace(".", "-");

        if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
        {
            //where = "and EREC_UgyUgyiratok.IraIktatokonyv_Id='" + IraIktatoKonyvekDropDownList1.SelectedValue + "' ";

            EREC_IraIktatoKonyvekService erec_IraIktatoKonyvekService = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam erec_IraIktatoKonyvek_execParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();

            string il = IraIktatoKonyvekDropDownList1.SelectedValue;

            if (il.Contains("|"))
            {
                if (!string.IsNullOrEmpty(il.Substring(il.IndexOf("|") + 1)))
                {
                    erec_IraIktatoKonyvekSearch.MegkulJelzes.Value = il.Substring(il.IndexOf("|") + 1);
                    erec_IraIktatoKonyvekSearch.MegkulJelzes.Operator = Query.Operators.equals;
                }
                erec_IraIktatoKonyvekSearch.Iktatohely.Value = il.Remove(il.IndexOf("|"));
                erec_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
            }
            else
            {
                erec_IraIktatoKonyvekSearch.Iktatohely.Value = il;//il.Remove(il.Length - 1);
                erec_IraIktatoKonyvekSearch.Iktatohely.Operator = Query.Operators.equals;
            }
            erec_IraIktatoKonyvekSearch.IktatoErkezteto.Value = "I";
            erec_IraIktatoKonyvekSearch.IktatoErkezteto.Operator = Query.Operators.equals;
            erec_IraIktatoKonyvekSearch.Ev.Value = Ev_DropDownList.SelectedValue;
            erec_IraIktatoKonyvekSearch.Ev.Operator = Query.Operators.equals;

            Result erec_IraIktatoKonyvek_result = erec_IraIktatoKonyvekService.GetAllWithExtension(erec_IraIktatoKonyvek_execParam, erec_IraIktatoKonyvekSearch);

            if (!erec_IraIktatoKonyvek_result.IsError)
            {
                if (erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows.Count > 0)
                {
                    string ids = "";

                    foreach (DataRow _row in erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows)
                    {
                        ids = ids + "'" + _row["Id"] + "',";
                    }

                    if (ids.Length > 2)
                    {
                        ids = ids.Remove(ids.Length - 1);

                        where = " and EREC_UgyUgyiratok.IraIktatokonyv_Id in (" + ids + ") ";
                    }

                    erec_IraIktatoKonyvek_execParam.Record_Id = erec_IraIktatoKonyvek_result.Ds.Tables[0].Rows[0]["Id"].ToString();
                }
                else
                {
                    Logger.Warn(String.Format("Az iktatókönyv lekérés nem adott vissza eredménysort! Kiválaszottt érték: {0}; Iktatóhely: {1}; MegKulJelzes: {2}; Ev: {3}", il ?? "null", erec_IraIktatoKonyvekSearch.Iktatohely.Value ?? "null", erec_IraIktatoKonyvekSearch.MegkulJelzes.Value ?? "null", erec_IraIktatoKonyvekSearch.Ev.Value ?? "null"));
                }

            }
            else
            {
                Logger.Error(String.Format("Hiba az iktatókönyv lekérés során: {0}", ResultError.GetErrorMessageFromResultObject(erec_IraIktatoKonyvek_result)));
            }

            iktatokonyv = IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text;
        }
        else
        {
            where = where + " and EREC_UgyUgyiratok.LetrehozasIdo BETWEEN '" + Ev_DropDownList.SelectedValue + ".01.01 00:00:00' and '" + Ev_DropDownList.SelectedValue + ".12.31 23:59:59' ";
        }

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "Where":
                            ReportParameters[i].Values.Add(where);
                            break;
                        case "OrderBy":
                            ReportParameters[i].Values.Add(" order by EREC_IraIktatokonyvek_sort.Iktatohely,EREC_IraIktatokonyvek_sort.Ev,EREC_UgyUgyiratdarabok.Foszam,EREC_IraIratok.Alszam ");
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloId(Page));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add("100000");
                            break;
                        case "Iktatokonyv":
                            ReportParameters[i].Values.Add(iktatokonyv);
                            break;
                        case "Ev":
                            ReportParameters[i].Values.Add(Ev_DropDownList.SelectedValue);
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}