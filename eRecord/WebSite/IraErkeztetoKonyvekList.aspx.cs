using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IraErkeztetoKonyvekList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();
    int stat_Index; //bernat.laszlo added
    // CR3355
    int kezelesModja_Index;
    private bool isTUK = false;

    Contentum.eDocument.Service.KRT_DokumentumokService dokService = eDocumentService.ServiceFactory.GetKRT_DokumentumokService();

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "ErkeztetoKonyvekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        // CR3355
        isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

        if (isTUK)
        {
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("KezelesTipusa"))].Visible = true;
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("Terjedelem"))].Visible = true;
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"))].Visible = true;
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("SelejtezesDatuma"))].Visible = true;

        }
        else
        {
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("KezelesTipusa"))].Visible = false;
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("Terjedelem"))].Visible = false;
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"))].Visible = false;
            IraErkeztetoKonyvekGridView.Columns[IraErkeztetoKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("SelejtezesDatuma"))].Visible = false;

            //IraErkeztetoKonyvekGridView.Columns.Remove(getGridViewDataColumn("KezelesTipusa"));
            //IraErkeztetoKonyvekGridView.Columns.Remove(getGridViewDataColumn("Terjedelem"));
            //IraErkeztetoKonyvekGridView.Columns.Remove(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"));
            //IraErkeztetoKonyvekGridView.Columns.Remove(getGridViewDataColumn("SelejtezesDatuma"));
            ////            IraIktatoKonyvekGridView.DataBind();
            //IraErkeztetoKonyvekGridViewBind();
        }
        kezelesModja_Index = 0;

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        CsoportokSubListHeader.RowCount_Changed += new EventHandler(CsoportokSubListHeader_RowCount_Changed);

        CsoportokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsoportokSubListHeader_ErvenyessegFilter_Changed);

        JogosultakSubListHeader.RowCount_Changed += new EventHandler(JogosultakSubListHeader_RowCount_Changed);
        stat_Index = 0; //bernat.laszlo added

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ListHeader1.HeaderLabel = Resources.List.IraErkeztetoKonyvekListHeaderTitle;
        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.ErkeztetoKonyvekSearch;
        ListHeader1.SearchObjectType = typeof(EREC_IraIktatoKonyvekSearch);

        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = true;


        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.IktatokonyvLezarasVisible = true;
        ListHeader1.IktatokonyvAlairasVisible = true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetoKonyvekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetoKonyvekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraErkeztetoKonyvekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IraErkeztetokonyvekListajaPrintForm.aspx");
        //   ListHeader1.PrintOnClientClick = "javascript:window.open('IraErkeztetokonyvekPrintFormSSRS.aspx?erkeztetokonyvId')";
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraErkeztetoKonyvekGridView.ClientID);

        ListHeader1.IktatokonyvLezarasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + IraErkeztetoKonyvekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.IktatokonyvAlairasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + IraErkeztetoKonyvekGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";


        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraErkeztetoKonyvekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraErkeztetoKonyvekGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraErkeztetoKonyvekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IraErkeztetoKonyvekGridView;

        CsoportokSubListHeader.ModifyVisible = false;

        CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsoportokGridView.ClientID);
        CsoportokSubListHeader.ButtonsClick += new CommandEventHandler(CsoportokSubListHeader_ButtonsClick);

        JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JogosultakSubListHeader.DeleteOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(JogosultakGridView.ClientID);
        JogosultakSubListHeader.ButtonsClick += new CommandEventHandler(JogosultakSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        CsoportokSubListHeader.AttachedGridView = CsoportokGridView;
        JogosultakSubListHeader.AttachedGridView = JogosultakGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(IraErkeztetoKonyvekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraIktatoKonyvekSearch(), Constants.CustomSearchObjectSessionNames.ErkeztetoKonyvekSearch);

        if (!IsPostBack) IraErkeztetoKonyvekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.ViewHistory);

        ListHeader1.IktatokonyvLezarasEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.Lezaras);
        ListHeader1.IktatokonyvAlairasEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.Lezaras);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyv" + CommandName.Lock);

        CsoportokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiList");
        /**/
        JogosultakPanel.Visible = true;


        CsoportokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        CsoportokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        CsoportokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Modify);
        CsoportokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);

        JogosultakSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        JogosultakSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        JogosultakSubListHeader.DeleteEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }


    #endregion

    #region Master List

    protected void IraErkeztetoKonyvekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraErkeztetoKonyvekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraErkeztetoKonyvekGridView", ViewState);

        IraErkeztetoKonyvekGridViewBind(sortExpression, sortDirection);
    }

    protected void IraErkeztetoKonyvekGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());


        EREC_IraIktatoKonyvekSearch search = null;
        if (!IsPostBack)
        {
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.ErkeztetoKonyvekSearch))
            {
                search = (EREC_IraIktatoKonyvekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_IraIktatoKonyvekSearch), Page);

                // sessionbe ment�s
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.ErkeztetoKonyvekSearch);
            }
        }

        search =
            //(EREC_IraIktatoKonyvekSearch)Search.GetSearchObject(Page, new EREC_IraIktatoKonyvekSearch());
            (EREC_IraIktatoKonyvekSearch)Search.GetSearchObject_CustomSessionName(
                Page, new EREC_IraIktatoKonyvekSearch(), Constants.CustomSearchObjectSessionNames.ErkeztetoKonyvekSearch);

        search.IktatoErkezteto.Value = Constants.IktatoErkezteto.Erkezteto;
        search.IktatoErkezteto.Operator = Query.Operators.equals;

        // CR3355
        //if (!isTUK)
        //{
        //    search.KezelesTipusa.Value = "E";
        //    search.KezelesTipusa.Operator = Query.Operators.equals;
        //}

        search.OrderBy = Search.GetOrderBy("IraErkeztetoKonyvekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        //Result res = service.GetAllWithExtension(ExecParam, search);
        Result res = service.GetAllWithExtensionAndJogosultsag(ExecParam, search, true);

        UI.GridViewFill(IraErkeztetoKonyvekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


        /*grid1.DataSource = res.Ds;
        grid1.DataBind();*/

    }

    protected void IraErkeztetoKonyvekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraErkeztetoKonyvekGridView.PageIndex;

        IraErkeztetoKonyvekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IraErkeztetoKonyvekGridView.PageCount;

        if (prev_PageIndex != IraErkeztetoKonyvekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IraErkeztetoKonyvekCPE);
            IraErkeztetoKonyvekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IraErkeztetoKonyvekCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IraErkeztetoKonyvekGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraErkeztetoKonyvekGridViewBind();
    }

    protected void IraErkeztetoKonyvekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraErkeztetoKonyvekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            if (Header1.Text.IndexOf(" (") > 0)
                Header1.Text = Header1.Text.Remove(Header1.Text.IndexOf(" ("));
            if (Label1.Text.IndexOf(" (") > 0)
                Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));

            // CR3355
            GridViewRow gvr = (sender as GridView).Rows[selectedRowNumber];
            SetDetailTabByKezelesTipus(gvr, id);

        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            //BLG 4977 - sz�lesebb popup az XML export gomb miatt
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetoKonyvekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetoKonyvekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraIktatoKonyvek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID);

            CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.ErkeztetokonyvId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            ListHeader1.PrintOnClientClick = "javascript:window.open('IraErkeztetoKonyvekPrintForm.aspx?" + QueryStringVars.ErkeztetokonyvId + "=" + id + "')";

            JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    protected void IraErkeztetoKonyvekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraErkeztetoKonyvekGridViewBind();
                    // CR3355
                    GridViewRow gvr = IraErkeztetoKonyvekGridView.SelectedRow;
                    SetDetailTabByKezelesTipus(gvr, UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
                    // ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIraErkeztetoKonyvek();
            IraErkeztetoKonyvekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraErkeztetoKonyvRecords();
                IraErkeztetoKonyvekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraErkeztetoKonyvRecords();
                IraErkeztetoKonyvekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIraErkeztetoKonyvek();
                break;
            case "IktatokonyvLezaras":
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(IraErkeztetoKonyvekGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedIktatokonyvIds"] = sb.ToString();

                js = JavaScripts.SetOnClientClickWithTimeout("IraIktatoKonyvekLezarasForm.aspx", QueryStringVars.Startup + "=" + Constants.Startup.FromErkeztetoKonyvek
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraErkeztetoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIktatokonyvLezaras", js, true);
                break;
            case "IktatokonyvAlairas":
                string[] IraIktatoKonyvekIds_Lezaras = ui.GetGridViewSelectedRows(IraErkeztetoKonyvekGridView, EErrorPanel1, ErrorUpdatePanel).ToArray();

                string alairasStoreInited = "";
                Result resultSign = Contentum.eUtility.AlairasokUtility.SignAndSavetoDocStore(Page, null, IraIktatoKonyvekIds_Lezaras, out alairasStoreInited, false);

                if (resultSign.IsError || !String.IsNullOrEmpty(alairasStoreInited))
                {
                    if (resultSign.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resultSign);
                        ErrorUpdatePanel.Update();
                    }
                    else
                    {
                        Result fakeResult = new Result();
                        fakeResult.ErrorMessage = "Hiba: " + alairasStoreInited;
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, fakeResult);
                    }
                }
                else
                {
                    IraErkeztetoKonyvekGridViewBind();
                }
                break;
        }
    }

    private void LockSelectedIraErkeztetoKonyvRecords()
    {
        LockManager.LockSelectedGridViewRecords(IraErkeztetoKonyvekGridView, "EREC_IraIktatoKonyvek"
            , "ErkeztetoKonyvLock", "ErkeztetoKonyvForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraErkeztetoKonyvRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IraErkeztetoKonyvekGridView, "EREC_IraIktatoKonyvek"
            , "ErkeztetoKonyvLock", "ErkeztetoKonyvForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a IraErkeztetoKonyvekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedIraErkeztetoKonyvek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "ErkeztetoKonyvInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraErkeztetoKonyvekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraErkeztetoKonyvek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IraErkeztetoKonyvekGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraErkeztetoKonyvek");
        }
    }

    protected void IraErkeztetoKonyvekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraErkeztetoKonyvekGridViewBind(e.SortExpression, UI.GetSortToGridView("IraErkeztetoKonyvekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }


    // CR3355
    private void SetDetailTabByKezelesTipus(GridViewRow gvr, String selectedId)
    {
        if (gvr != null)
        {
            //GridViewRow gvr = IraPostaKonyvekGridView.SelectedRow;
            //if (kezelesModja_Index == 0)
            //{
            kezelesModja_Index = GetColumnIndexByName(gvr, "KezelesTipusa");
            //}
            if (kezelesModja_Index < gvr.Cells.Count)
            {
                if (gvr.Cells[kezelesModja_Index].Text == Constants.IktatokonyKezelesTipusa.Papir)
                {
                    TabContainer1.Visible = false;
                    DetailCPEButton.Visible = false;
                    UpdatePanel_Detail.Update();

                    //tr_DetailRow.Visible = false;
                }
                else
                {
                    TabContainer1.Visible = true;
                    DetailCPEButton.Visible = true;
                    UpdatePanel_Detail.Update();

                    //tr_DetailRow.Visible = true;
                    ActiveTabRefresh(TabContainer1, selectedId);
                }
            }
            else
            {
                TabContainer1.Visible = true;
                DetailCPEButton.Visible = true;
                UpdatePanel_Detail.Update();

                //tr_DetailRow.Visible = true;
                ActiveTabRefresh(TabContainer1, selectedId);
            }
        }
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (IraErkeztetoKonyvekGridView.SelectedIndex == -1)
        {
            return;
        }
        string IraErkeztetoKonyvId = UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, IraErkeztetoKonyvId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string IraErkeztetoKonyvId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                CsoportokGridViewBind(IraErkeztetoKonyvId);
                CsoportokPanel.Visible = true;
                break;
            case 1:
                JogosultakGridViewBind(IraErkeztetoKonyvId);
                JogosultakPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(CsoportokGridView);
                break;
            case 1:
                ui.GridViewClear(JogosultakGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        CsoportokSubListHeader.RowCount = RowCount;
        JogosultakSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
        {
            CsoportokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
        {
            JogosultakGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(JogosultakGridView), UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
        }
    }


    #endregion


    #region Csoportok Detail

    private void CsoportokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_IdeErkeztetok();
            CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
        }
    }

    protected void CsoportokGridViewBind(string IraErkeztetoKonyvId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsoportokGridView", ViewState, "Csoport_Iktathat_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsoportokGridView", ViewState);

        CsoportokGridViewBind(IraErkeztetoKonyvId, sortExpression, sortDirection);
    }

    protected void CsoportokGridViewBind(string IraErkeztetoKonyvId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(IraErkeztetoKonyvId))
        {
            EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IraIktatoKonyvek IraErkeztetoKonyv = new EREC_IraIktatoKonyvek();
            IraErkeztetoKonyv.Id = IraErkeztetoKonyvId;


            EREC_Irat_IktatokonyveiSearch search = new EREC_Irat_IktatokonyveiSearch();
            search.OrderBy = Search.GetOrderBy("CsoportokGridView", ViewState, SortExpression, SortDirection);

            CsoportokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByIktatoKonyv(ExecParam, IraErkeztetoKonyv, search);

            UI.GridViewFill(CsoportokGridView, res, CsoportokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Header1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        }
        else


        {
            ui.GridViewClear(CsoportokGridView);
        }
    }


    void CsoportokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
    }

    protected void CsoportokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
                    //{
                    //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
                    //}
                    ActiveTabRefreshDetailList(CsoportokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a CsoportokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_IdeErkeztetok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(CsoportokGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void CsoportokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsoportokGridView, selectedRowNumber, "check");
        }
    }

    private void CsoportokGridView_RefreshOnClientClicks(string erkeztetoKonyvId)
    {
        string id = erkeztetoKonyvId;
        if (!String.IsNullOrEmpty(id))
        {
            CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID);
            CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraErkeztetohelyekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void CsoportokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = CsoportokGridView.PageIndex;

        CsoportokGridView.PageIndex = CsoportokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != CsoportokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, CsoportokCPE);
            CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(CsoportokSubListHeader.Scrollable, CsoportokCPE);
        }
        CsoportokSubListHeader.PageCount = CsoportokGridView.PageCount;
        CsoportokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsoportokGridView);
    }


    void CsoportokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(CsoportokSubListHeader.RowCount);
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
    }

    protected void CsoportokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView)
            , e.SortExpression, UI.GetSortToGridView("CsoportokGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region Jogosultak Detail

    private void JogosultakSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Jogosultak();
            JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
        }
    }

    protected void JogosultakGridViewBind(string IraIktatoKonyvId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);

        JogosultakGridViewBind(IraIktatoKonyvId, sortExpression, sortDirection);
    }

    protected void JogosultakGridViewBind(string IraIktatoKonyvId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(IraIktatoKonyvId))
        {
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
            search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, SortExpression, SortDirection);

            Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, IraIktatoKonyvId, search);

            UI.GridViewFill(JogosultakGridView, res, JogosultakSubListHeader, EErrorPanel1, ErrorUpdatePanel);
            UI.SetTabHeaderRowCountText(res, Label1);

            ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
        }
        else
        {
            ui.GridViewClear(JogosultakGridView);
        }
    }

    private void deleteSelected_Jogosultak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(JogosultakGridView, EErrorPanel1, ErrorUpdatePanel);
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiRemoveCsoportFromJogtargyById(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void JogosultakUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
                    //{
                    //    JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
                    //}
                    ActiveTabRefreshDetailList(JogosultakTabPanel);
                    break;
            }
        }
    }

    protected void JogosultakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JogosultakGridView, selectedRowNumber, "check");
        }
    }

    private void JogosultakGridView_RefreshOnClientClicks(string id, string IktatoKonyvId)
    {
        if (!String.IsNullOrEmpty(id))
        {
            JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                  + "&" + QueryStringVars.IktatokonyvId + "=" + IktatoKonyvId
                , Defaults.PopupWidth, Defaults.PopupHeight, JogosultakUpdatePanel.ClientID);
        }
    }

    protected void JogosultakGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = JogosultakGridView.PageIndex;

        JogosultakGridView.PageIndex = JogosultakSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != JogosultakGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, JogosultakCPE);
            JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(JogosultakSubListHeader.Scrollable, JogosultakCPE);
        }
        JogosultakSubListHeader.PageCount = JogosultakGridView.PageCount;
        JogosultakSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(JogosultakGridView);
    }


    void JogosultakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(JogosultakSubListHeader.RowCount);
        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView));
    }

    protected void JogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraErkeztetoKonyvekGridView)
            , e.SortExpression, UI.GetSortToGridView("JogosultakGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region Statusz �rt�k�nek be�ll�t�sa
    //bernat.laszlo added

    protected void IraErkeztetoKonyvekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page); //Ez eredetileg is bennevolt
        if (stat_Index == 0)
        {
            stat_Index = GetColumnIndexByName(e.Row, "Statusz");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[stat_Index].Text = e.Row.Cells[stat_Index].Text.Equals("1") ? "Akt�v" : "Inakt�v";
        }

        // CR3355
        if (kezelesModja_Index == 0)
        {
            kezelesModja_Index = GetColumnIndexByName(e.Row, "KezelesTipusa");
        }
        if (kezelesModja_Index < e.Row.Cells.Count)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[kezelesModja_Index].Text.Equals("E"))
                {
                    e.Row.Cells[kezelesModja_Index].Text = Constants.IktatokonyKezelesTipusa.Elektronikus;
                }
                else if (e.Row.Cells[kezelesModja_Index].Text.Equals("P"))
                {
                    e.Row.Cells[kezelesModja_Index].Text = Constants.IktatokonyKezelesTipusa.Papir;
                }
            }


        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView row = (DataRowView)e.Row.DataItem;

            if (row["HitelesExport_Dok_ID"] != null && !String.IsNullOrEmpty(row["HitelesExport_Dok_ID"].ToString()))
            {
                string hitelesId = row["HitelesExport_Dok_ID"].ToString();

                ExecParam dokExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                dokExecParam.Record_Id = hitelesId;
                Result dokResult = dokService.Get(dokExecParam);
                if (String.IsNullOrEmpty(dokResult.ErrorCode))
                {
                    KRT_Dokumentumok krt_Dokumentumok = (KRT_Dokumentumok)dokResult.Record;

                    ((HyperLink)e.Row.Cells[14].Controls[1]).NavigateUrl = krt_Dokumentumok.External_Link;

                    bool valid = krt_Dokumentumok.ElektronikusAlairas == KodTarak.DOKUMENTUM_ALAIRAS_PKI.Ervenyes_alairas;

                    if (valid)
                    {
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                        ////e.Row.Cells[16].BorderStyle = BorderStyle.Double;
                        //e.Row.Cells[16].BorderColor = System.Drawing.Color.Green;
                        //e.Row.Cells[16].BorderStyle = BorderStyle.Double;
                        //e.Row.Cells[16].BorderWidth = Unit.Pixel(2);
                        //e.Row.Cells[16].Attributes["style"] = "border-color: #c3cecc;border-style:solid; border-width:1px;";
                        //e.Row.Cells[16].CssClass = "GridViewBoundFieldItemStyle";
                        //e.Row.Cells[16].Style.Add("border-right-color", "black");
                        //e.Row.Cells[16].Style.Add("border-top-color", "red");
                        //e.Row.Cells[16].Style.Add("border-bottom-color", "blue");
                        //e.Row.Cells[16].BorderColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.Red;
                    }
                }
            }
        }

    }

    protected int GetColumnIndexByName(GridViewRow row, string columnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
                if (((BoundField)cell.ContainingField).DataField.Equals(columnName))
                    break;
            columnIndex++;
        }
        return columnIndex;
    }
    #endregion
    //bernat.laszlo eddig
    // CR3355
    private DataControlField getGridViewDataColumn(string name)
    {
        foreach (DataControlField dcf in IraErkeztetoKonyvekGridView.Columns)
        {
            if (dcf is BoundField)
            {
                if (dcf.SortExpression == name)
                {

                    return dcf;
                }
            }
        }
        return null;
    }
}
