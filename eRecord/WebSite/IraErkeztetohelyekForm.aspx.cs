using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class IraErkeztetohelyekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraErkeztetohelyekFormHeaderTitle;
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Irat_Iktatokonyvei" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_Irat_Iktatokonyvei erec_Irat_Iktatokonyvei = (EREC_Irat_Iktatokonyvei)result.Record;
                    LoadComponentsFromBusinessObject(erec_Irat_Iktatokonyvei);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            string erkeztetokonyvId = Request.QueryString.Get(QueryStringVars.ErkeztetokonyvId);
            if (!String.IsNullOrEmpty(erkeztetokonyvId))
            {
                Erkeztetokonyv_IktatokonyvTextBox.Id_HiddenField = erkeztetokonyvId;
                Erkeztetokonyv_IktatokonyvTextBox.SetIktatokonyvekTextBoxById(FormHeader1.ErrorPanel);
                Erkeztetokonyv_IktatokonyvTextBox.ReadOnly = true;
            }
        }

        Erkeztetokonyv_IktatokonyvTextBox.Tipus_IktatoErkezteto = Constants.IktatoErkezteto.Erkezteto;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_Irat_Iktatokonyvei erec_Irat_Iktatokonyvei)
    {
        Erkeztetokonyv_IktatokonyvTextBox.Id_HiddenField = erec_Irat_Iktatokonyvei.IraIktatokonyv_Id;
        Erkeztetokonyv_IktatokonyvTextBox.SetIktatokonyvekTextBoxById(FormHeader1.ErrorPanel);
        Erkeztetokonyv_IktatokonyvTextBox.ReadOnly = true;

        Csoport_Id_Iktathat_CsoportTextBox.Id_HiddenField = erec_Irat_Iktatokonyvei.Csoport_Id_Iktathat;
        Csoport_Id_Iktathat_CsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        Csoport_Id_Iktathat_CsoportTextBox.ReadOnly = true;

        //Sorszam_RequiredNumberBox.Text = erec_Irat_Iktatokonyvei.Sorszam;

        ErvenyessegCalendarControl1.ErvKezd = erec_Irat_Iktatokonyvei.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_Irat_Iktatokonyvei.ErvVege;

        FormHeader1.Record_Ver = erec_Irat_Iktatokonyvei.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_Irat_Iktatokonyvei.Base);

        if (Command == CommandName.View)
        {
            //Sorszam_RequiredNumberBox.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
        }
    }

    // form --> business object
    private EREC_Irat_Iktatokonyvei GetBusinessObjectFromComponents()
    {
        EREC_Irat_Iktatokonyvei erec_Irat_Iktatokonyvei = new EREC_Irat_Iktatokonyvei();
        erec_Irat_Iktatokonyvei.Updated.SetValueAll(false);
        erec_Irat_Iktatokonyvei.Base.Updated.SetValueAll(false);

        erec_Irat_Iktatokonyvei.IraIktatokonyv_Id = Erkeztetokonyv_IktatokonyvTextBox.Id_HiddenField;
        erec_Irat_Iktatokonyvei.Updated.IraIktatokonyv_Id = pageView.GetUpdatedByView(Erkeztetokonyv_IktatokonyvTextBox);

        erec_Irat_Iktatokonyvei.Csoport_Id_Iktathat = Csoport_Id_Iktathat_CsoportTextBox.Id_HiddenField;
        erec_Irat_Iktatokonyvei.Updated.Csoport_Id_Iktathat = pageView.GetUpdatedByView(Csoport_Id_Iktathat_CsoportTextBox);

        //erec_Irat_Iktatokonyvei.Sorszam = Sorszam_RequiredNumberBox.Text;
        //erec_Irat_Iktatokonyvei.Updated.Sorszam = pageView.GetUpdatedByView(Sorszam_RequiredNumberBox);

        //erec_Irat_Iktatokonyvei.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //erec_Irat_Iktatokonyvei.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //erec_Irat_Iktatokonyvei.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //erec_Irat_Iktatokonyvei.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_Irat_Iktatokonyvei, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        erec_Irat_Iktatokonyvei.Base.Ver = FormHeader1.Record_Ver;
        erec_Irat_Iktatokonyvei.Base.Updated.Ver = true;
        
        return erec_Irat_Iktatokonyvei;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Erkeztetokonyv_IktatokonyvTextBox);
            compSelector.Add_ComponentOnClick(Csoport_Id_Iktathat_CsoportTextBox);
            //compSelector.Add_ComponentOnClick(Sorszam_RequiredNumberBox);

            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
                            EREC_Irat_Iktatokonyvei erec_Irat_Iktatokonyvei = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, erec_Irat_Iktatokonyvei);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                //Contentum.eAdmin.Service.RightsService rs = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
                                //Result aclResult = rs.AddCsoportToJogtargy(execParam, erec_Irat_Iktatokonyvei.IraIktatokonyv_Id, erec_Irat_Iktatokonyvei.Csoport_Id_Iktathat, 'I');

                                //// ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                //JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev.Text);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }

                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
                                EREC_Irat_Iktatokonyvei erec_Irat_Iktatokonyvei = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_Irat_Iktatokonyvei);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }
}
