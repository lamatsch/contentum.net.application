using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class IraIktatokonyvekPrintForm : Contentum.eUtility.UI.PageBase
{
    private string erkeztetokonyvId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        erkeztetokonyvId = Request.QueryString.Get(QueryStringVars.ErkeztetokonyvId);

        if (String.IsNullOrEmpty(erkeztetokonyvId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
    }


    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            Response.Redirect("IraErkeztetoKonyvekPrintFormSSRS.aspx?ErkeztetoKonyvId=" + erkeztetokonyvId + "&datumtol=" + DatumIntervallum_SearchCalendarControl1.DatumKezd + "&datumig=" + DatumIntervallum_SearchCalendarControl1.DatumVege + "&szamtol=" + Szam_SzamIntervallum_SearchFormControl1.SzamTol + "&szamig=" + Szam_SzamIntervallum_SearchFormControl1.SzamIg);
        }
    }
}
