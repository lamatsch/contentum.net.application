using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class IraErkeztetoKonyvekPrintFormSSRS : Contentum.eUtility.UI.PageBase
{
    private string erkeztetokonyvId = String.Empty;
    private string datumtol = String.Empty;
    private string datumig = String.Empty;
    private string szamtol = String.Empty;
    private string szamig = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        erkeztetokonyvId = Request.QueryString.Get(QueryStringVars.ErkeztetokonyvId);

        if (String.IsNullOrEmpty(erkeztetokonyvId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }

        string qsSzamtol = Request.QueryString.Get("szamtol");
        int iSzamtol;

        if (!String.IsNullOrEmpty(qsSzamtol) && Int32.TryParse(qsSzamtol, out iSzamtol))
        {
            szamtol = iSzamtol.ToString();
        }

        string qsSzamig = Request.QueryString.Get("szamig");
        int iSzamig;

        if (!String.IsNullOrEmpty(qsSzamig) && Int32.TryParse(qsSzamig, out iSzamig))
        {
            szamig = iSzamig.ToString();
        }



        string qsDatumTol = Request.QueryString.Get("datumtol");
        DateTime dtDatumTol;

        if (!String.IsNullOrEmpty(qsDatumTol) && DateTime.TryParse(qsDatumTol, out dtDatumTol))
        {
            if(dtDatumTol >= System.Data.SqlTypes.SqlDateTime.MinValue.Value)
                datumtol = dtDatumTol.ToShortDateString();
        }

        string qsDatumIg = Request.QueryString.Get("datumig");
        DateTime dtDatumIg;

        if (!String.IsNullOrEmpty(qsDatumIg) && DateTime.TryParse(qsDatumIg, out dtDatumIg))
        {
            if (dtDatumIg <= System.Data.SqlTypes.SqlDateTime.MaxValue.Value)
                datumig = dtDatumIg.ToString();
        }

        if (String.IsNullOrEmpty(datumtol) && String.IsNullOrEmpty(datumig))
        {
            int year = DateTime.Now.Year;
            datumtol = new DateTime(year, 1, 1).ToShortDateString();
            datumig = new DateTime(year, 12, 31, 23, 59, 59).ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            //     ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        // FormFooter1.ImageButton_Save.Visible = true;
    }

    private EREC_KuldKuldemenyekSearch GetSearchObjectFromComponents()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value = erkeztetokonyvId;
        erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;

        if (!String.IsNullOrEmpty(datumtol) || !String.IsNullOrEmpty(datumig))
        {
            if (String.IsNullOrEmpty(datumtol))
            {
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.Value = datumig;
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.Operator = Query.Operators.lessorequal;
            }
            else if (String.IsNullOrEmpty(datumig))
            {
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.Value = datumtol;
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.Operator = Query.Operators.greaterorequal;
            }
            else
            {
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.Value = datumtol;
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.ValueTo = datumig;
                erec_KuldKuldemenyekSearch.BeerkezesIdeje.Operator = Query.Operators.between;
            }
        }

        if (!String.IsNullOrEmpty(szamtol) || !String.IsNullOrEmpty(szamig))
        {
            if (String.IsNullOrEmpty(szamtol))
            {
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value = szamig;
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Operator = Query.Operators.lessorequal;
            }
            else if (String.IsNullOrEmpty(szamig))
            {
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value = szamtol;
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Operator = Query.Operators.greaterorequal;
            }
            else
            {
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Value = szamtol;
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.ValueTo = szamig;
                erec_KuldKuldemenyekSearch.Erkezteto_Szam.Operator = Query.Operators.between;
            }
        }

        return erec_KuldKuldemenyekSearch;
    }


    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_KuldKuldemenyekService service = eRecordService.ServiceFactory.GetEREC_KuldKuldemenyekService();
                EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Fake = true;

                Result result = service.GetAllWithExtension(execParam, erec_KuldKuldemenyekSearch);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));


                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;

                        case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;

                        case "Where_Szamlak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Szamlak"));
                            break;




                        case "Where_KuldKuldemenyek_Csatolt":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt"));
                            }


                            break;

                        case "Where_EREC_IraOnkormAdatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                            break;

                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            //  ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;



                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;


                        case "AlSzervezetId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                            break;

                        case "IdoszakTol":
                            ReportParameters[i].Values.Add(datumtol);
                            break;

                        case "IdoszakIg":
                            ReportParameters[i].Values.Add(datumig);
                            break;

                        case "SzamTol":
                            ReportParameters[i].Values.Add(szamtol);
                            break;

                        case "SzamIg":
                            ReportParameters[i].Values.Add(szamig);
                            break;

                    }
                }




            }

        }
        return ReportParameters;
    }


}
