using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class IraFonyilvantartokonyvList : Contentum.eUtility.UI.PageBase
{

    UI ui = new UI();
    int stat_Index;
    // CR3355
    int kezelesModja_Index;
    private bool isTUK = false;

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FonyilvantartokonyvList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ////CERTOP: lez�rt iktat�k�nyvh�z elmentett iratt�ri t�telek lek�r�se
        //string CommandName = Request.QueryString.Get(QueryStringVars.Command);
        //if (CommandName == "IrattariTetelekXML")
        //{
        //    string IktatokonyvId = Request.QueryString.Get(QueryStringVars.Id);
        //    GetIarattariTetelekXML(IktatokonyvId);
        //}

        //bernat.laszlo added
        stat_Index = 0;

        isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("KezelesTipusa"))].Visible = true;
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("Terjedelem"))].Visible = true;
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"))].Visible = true;
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("SelejtezesDatuma"))].Visible = true;

        }
        else
        {
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("KezelesTipusa"))].Visible = false;
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("Terjedelem"))].Visible = false;
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"))].Visible = false;
            IraFonyilvantartokonyvekGridView.Columns[IraFonyilvantartokonyvekGridView.Columns.IndexOf(getGridViewDataColumn("SelejtezesDatuma"))].Visible = false;

            //    IraFonyilvantartokonyvekGridView.Columns.Remove(getGridViewDataColumn("KezelesTipusa"));
            //    IraFonyilvantartokonyvekGridView.Columns.Remove(getGridViewDataColumn("Terjedelem"));
            //    IraFonyilvantartokonyvekGridView.Columns.Remove(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"));
            //    IraFonyilvantartokonyvekGridView.Columns.Remove(getGridViewDataColumn("SelejtezesDatuma"));
            //    //            IraIktatoKonyvekGridView.DataBind();
            //    IraFonyilvantartokonyvekGridViewBind();
        }
        kezelesModja_Index = 0;

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        //CsoportokSubListHeader.RowCount_Changed += new EventHandler(CsoportokSubListHeader_RowCount_Changed);
        //IrattariTetelekSubListHeader.RowCount_Changed += new EventHandler(IrattariTetelekSubListHeader_RowCount_Changed);
        //JogosultakSubListHeader.RowCount_Changed += new EventHandler(JogosultakSubListHeader_RowCount_Changed);

        //CsoportokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsoportokSubListHeader_ErvenyessegFilter_Changed);
        //IrattariTetelekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IrattariTetelekSubListHeader_ErvenyessegFilter_Changed);
        //ImageButtonAddAllIrattariTetel.Click += new ImageClickEventHandler(ImageButtonAddAllIrattariTetel_Click);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ListHeader1.HeaderLabel = Resources.List.IraFonyilvantartokonyvListHeaderTitle;
        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.FonyilvantartokonyvSearch;
        ListHeader1.SearchObjectType = typeof(EREC_IraIktatoKonyvekSearch);
        // A baloldali, alapb�l invisible ikonok megjelen�t�se:

        #region BUG_4575
        //LZS - Excel Export (Grid)
        //Excel export ikon l�that�s�g�nak be�ll�t�sa �true� �rt�kre.
        ListHeader1.PrintVisible = false;
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
        #endregion

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.IktatokonyvLezarasVisible = true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        //JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        //ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraFonyilvantartokonyvSearch.aspx", ""
             , Defaults.PopupWidth, Defaults.PopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraFonyilvantartokonyvekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
             , Defaults.PopupWidth, Defaults.PopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);


        #region LZS
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        #endregion


        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraFonyilvantartokonyvekGridView.ClientID);

        //LZS BUG_4575
        //ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm ( "IraFonyilvantartokonyvekListajaPrintForm.aspx" );

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraFonyilvantartokonyvekGridView.ClientID);

        ListHeader1.IktatokonyvLezarasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                  + IraFonyilvantartokonyvekGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraFonyilvantartokonyvekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraFonyilvantartokonyvekGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraFonyilvantartokonyvekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IraFonyilvantartokonyvekGridView;





        //        CsoportokSubListHeader.ModifyVisible = false;

        //        CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        CsoportokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsoportokGridView.ClientID);
        //        CsoportokSubListHeader.ButtonsClick += new CommandEventHandler(CsoportokSubListHeader_ButtonsClick);

        //        IrattariTetelekSubListHeader.ModifyVisible = false;

        //        IrattariTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        ImageButtonAddAllIrattariTetel.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        IrattariTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        //IrattariTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //        IrattariTetelekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IrattariTetelekGridView.ClientID);
        //        IrattariTetelekSubListHeader.ButtonsClick += new CommandEventHandler(IrattariTetelekSubListHeader_ButtonsClick);

        ///**/        JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ///**/        JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ///**/        JogosultakSubListHeader.DeleteOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(JogosultakGridView.ClientID);
        ///**/        JogosultakSubListHeader.ButtonsClick += new CommandEventHandler(JogosultakSubListHeader_ButtonsClick);


        //        //selectedRecordId kezel�se
        //        CsoportokSubListHeader.AttachedGridView = CsoportokGridView;
        //        IrattariTetelekSubListHeader.AttachedGridView = IrattariTetelekGridView;
        ///**/        JogosultakSubListHeader.AttachedGridView = JogosultakGridView;
        ui.SetClientScriptToGridViewSelectDeSelectButton(IraFonyilvantartokonyvekGridView);
        //        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        //        ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariTetelekGridView);
        ///**/        ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraIktatoKonyvekSearch(), Constants.CustomSearchObjectSessionNames.FonyilvantartokonyvSearch);

        if (!IsPostBack) IraFonyilvantartokonyvekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.New);


        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.View);

        #region LZS - Mindenk�ppen akt�vv� teszem: 
        ListHeader1.ViewEnabled = true;
        #endregion

        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.ViewHistory);

        ListHeader1.IktatokonyvLezarasEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.Lezaras);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Fonyilvantartokonyv" + CommandName.Lock);

        //        CsoportokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiList");
        ///**/        JogosultakPanel.Visible = true;

        //        CsoportokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        //        CsoportokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        //        CsoportokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Modify);
        //        CsoportokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);

        ///**/        JogosultakSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        ///**/        JogosultakSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        ///**/        JogosultakSubListHeader.DeleteEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);

        //        IrattariTetelekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_IktatokonyvList");

        //        IrattariTetelekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_Iktatokonyv" + CommandName.New);
        //        IrattariTetelekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_Iktatokonyv" + CommandName.View);
        //        IrattariTetelekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_Iktatokonyv" + CommandName.Modify);
        //        IrattariTetelekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_Iktatokonyv" + CommandName.Invalidate);
        //        ImageButtonAddAllIrattariTetel.Enabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel_Iktatokonyv" + CommandName.New);


        #region BLG_4575
        //LZS
        //Excel export ikon aktiv�l�sa.
        ListHeader1.ExportEnabled = true;
        #endregion

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    ActiveTabClear();
            //}
            //ActiveTabRefreshOnClientClicks();
            //ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }

    #endregion

    #region Master List

    protected void IraFonyilvantartokonyvekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraFonyilvantartokonyvekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraFonyilvantartokonyvekGridView", ViewState);

        IraFonyilvantartokonyvekGridViewBind(sortExpression, sortDirection);
    }

    protected void IraFonyilvantartokonyvekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        //Object o = IraFonyilvantartokonyvekGridView.DataSource;
        UI.ClearGridViewRowSelection(IraFonyilvantartokonyvekGridView);

        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraIktatoKonyvekSearch search = null;
        if (!IsPostBack)
        {
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.FonyilvantartokonyvSearch))
            {
                search = (EREC_IraIktatoKonyvekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_IraIktatoKonyvekSearch), Page);

                // sessionbe ment�s
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.FonyilvantartokonyvSearch);
            }
        }

        //EREC_IraIktatoKonyvekSearch search = (EREC_IraIktatoKonyvekSearch)Search.GetSearchObject(Page, new EREC_IraIktatoKonyvekSearch());
        search = (EREC_IraIktatoKonyvekSearch)Search.GetSearchObject_CustomSessionName
             (Page, new EREC_IraIktatoKonyvekSearch(), Constants.CustomSearchObjectSessionNames.FonyilvantartokonyvSearch);


        // CR3355
        //if (!isTUK)
        //{
        //    search.KezelesTipusa.Value = "E";
        //    search.KezelesTipusa.Operator = Query.Operators.equals;
        //}
        //search.IktatoErkezteto.Value = Constants.IktatoErkezteto.Fonyilvantartokonyv;
        //search.IktatoErkezteto.Operator = Query.Operators.equals;


        search.OrderBy = Search.GetOrderBy("IraFonyilvantartokonyvekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtensionAndJogosultsag(ExecParam, search, true);
        //Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(IraFonyilvantartokonyvekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);


        /*grid1.DataSource = res.Ds;
		grid1.DataBind();*/

    }

    protected void IraFonyilvantartokonyvekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraFonyilvantartokonyvekGridView.PageIndex;

        IraFonyilvantartokonyvekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IraFonyilvantartokonyvekGridView.PageCount;

        if (prev_PageIndex != IraFonyilvantartokonyvekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IraIktatoKonyvekCPE);
            IraFonyilvantartokonyvekGridViewBind();
            // ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IraIktatoKonyvekCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IraFonyilvantartokonyvekGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraFonyilvantartokonyvekGridViewBind();
    }

    protected void IraFonyilvantartokonyvekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraFonyilvantartokonyvekGridView, selectedRowNumber, "check");

            #region LZS
            string selectedId = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
            string erkezteto = (sender as GridView).DataKeys[selectedRowNumber]["IktatoErkezteto"].ToString();
            string page = "";

            switch (erkezteto)
            {
                case "I":
                    page = " IraIktatoKonyvekForm.aspx";
                    break;

                case "E":
                    page = " IraErkeztetokonyvekForm.aspx";
                    break;

                case "P":
                    page = " IraPostaKonyvekForm.aspx";
                    break;

                case "S":
                    page = " IraIratkezelesiSegedletekForm.aspx";
                    break;
            }

            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick(page
                     , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + selectedId
                     , Defaults.PopupWidth, Defaults.PopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID);

            #endregion

            //**************************
            // if (Header.Text.IndexOf(" (") > 0)
            //     Header.Text = Header.Text.Remove(Header.Text.IndexOf(" ("));
            // if (headerIrattariTetelek.Text.IndexOf(" (") > 0)
            //     headerIrattariTetelek.Text = headerIrattariTetelek.Text.Remove(headerIrattariTetelek.Text.IndexOf(" ("));
            // if (Label1.Text.IndexOf(" (") > 0)
            //     Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));

            //// TabContainer1.Enabled = false;
            // ActiveTabRefresh(TabContainer1, id);


        }
    }



    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraFonyilvantartokonyvekForm.aspx"
                 , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                 , Defaults.PopupWidth, Defaults.PopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraIktatoKonyvek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                 , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                 , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID);

            //CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIktatohelyekForm.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            //IrattariTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelek_IktatoKonyvek_Form.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, IrattariTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            //ImageButtonAddAllIrattariTetel.OnClientClick = "if(!window.confirm('Biztosan hozz�rendeli a kiv�lasztott iktat�k�nyvh�z az �sszes �rv�nyes iratt�ri t�telsz�mot?')) return false;";

            //JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);


            //LZS BUG_4575
            //ListHeader1.PrintOnClientClick = "javascript:window.open('IraFonyilvantartokonyvekPrintForm.aspx?" + QueryStringVars.IktatokonyvId + "=" + id + "')";


        }
    }

    protected void IraIktatoKonyvekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraFonyilvantartokonyvekGridViewBind();
                    //  ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        #region BLG_4575
        //LZS - Excel Export (Grid)
        //ListGrid export�l�sa Excel  file-ba, a sz�ks�ges oszlopsz�less�geket param�terk�nt �tadva.
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            ex_Export.SaveGridView_ToExcel(exParam, IraFonyilvantartokonyvekGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, 1.1d, browser);
        }
        #endregion

        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIraFonyilvantartokonyvek();
            IraFonyilvantartokonyvekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraFonyilvantartokonyvRecords();
                IraFonyilvantartokonyvekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraFonyilvantartokonyvRecords();
                IraFonyilvantartokonyvekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIraFonyilvantartokonyvek();
                break;
            case "IktatokonyvLezaras":
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(IraFonyilvantartokonyvekGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedIktatokonyvIds"] = sb.ToString();

                js = JavaScripts.SetOnClientClickWithTimeout("IraIktatoKonyvekLezarasForm.aspx", QueryStringVars.Startup + "=" + Constants.Startup.FromIktatoKonyvek
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraFonyilvantartokonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIktatokonyvLezaras", js, true);
                break;
        }
    }

    private void LockSelectedIraFonyilvantartokonyvRecords()
    {
        LockManager.LockSelectedGridViewRecords(IraFonyilvantartokonyvekGridView, "EREC_IraIktatoKonyvek"
             , "FonyilvantartokonyvLock", "FonyilvantartokonyvForceLock"
              , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraFonyilvantartokonyvRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IraFonyilvantartokonyvekGridView, "EREC_IraIktatoKonyvek"
             , "IktatoKonyvLock", "IktatoKonyvForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a IraFonyilvantartokonyvekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedIraFonyilvantartokonyvek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "FonyilvantartokonyvInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraFonyilvantartokonyvekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraFonyilvantartokonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraFonyilvantartokonyvek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IraFonyilvantartokonyvekGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraIktatoKonyvek");
        }
    }

    protected void IraFonyilvantartokonyvekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraFonyilvantartokonyvekGridViewBind(e.SortExpression, UI.GetSortToGridView("IraFonyilvantartokonyvekGridView", ViewState, e.SortExpression));
        // ActiveTabClear();
    }

    ////CERTOP: lez�rt iktat�k�nyvh�z elmentett iratt�ri t�telek lek�r�se
    //private void GetIarattariTetelekXML(string IktatokonyvId)
    //{
    //    if (!String.IsNullOrEmpty(IktatokonyvId))
    //    {
    //        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
    //        ExecParam xpm = UI.SetExecParamDefault(this.Page);
    //        xpm.Record_Id = IktatokonyvId;
    //        Result res = service.Get(xpm);

    //        if (res.IsError)
    //        {
    //            throw new Contentum.eUtility.ResultException(res);
    //        }

    //        EREC_IraIktatoKonyvek _EREC_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)res.Record;
    //        string irattariTetelekXML = _EREC_IraIktatoKonyvek.Base.Note;

    //        if (!String.IsNullOrEmpty(irattariTetelekXML))
    //        {
    //            Response.Clear();
    //            Response.ContentType = "text/xml";
    //            Response.CacheControl = "no-cache";
    //            Response.HeaderEncoding = System.Text.Encoding.UTF8;
    //            Response.Charset = "utf-8";
    //            string iktatokonyvAzonosito = String.Format("{0}_{1}", _EREC_IraIktatoKonyvek.Ev, _EREC_IraIktatoKonyvek.Iktatohely);
    //            if (!String.IsNullOrEmpty(_EREC_IraIktatoKonyvek.MegkulJelzes))
    //            {
    //                iktatokonyvAzonosito += "_" + _EREC_IraIktatoKonyvek.MegkulJelzes;
    //            }
    //            string fileName = String.Format("{0}_IrattariTetelek.xml", iktatokonyvAzonosito);
    //            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + EncodeFileName(fileName) + "\";");
    //            Response.Write(irattariTetelekXML);
    //            Response.End();
    //        }
    //    }
    //}

    //private string EncodeFileName(string fileName)
    //{
    //    if (String.IsNullOrEmpty(fileName))
    //        return String.Empty;

    //    fileName = HttpUtility.UrlEncode(fileName);
    //    fileName = fileName.Replace("+", "%20");

    //    return fileName;
    //}

    #endregion


    #region Detail Tab


    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    //protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    //{
    //    if (TabContainer1.ActiveTab.Equals(tab))
    //    {
    //        ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //        ActiveTabRefreshOnClientClicks();
    //    }
    //}

    //protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    //{
    //    if (IraFonyilvantartokonyvekGridView.SelectedIndex == -1)
    //    {
    //        return;
    //    }
    //    string IraIktatoKonyvId = UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView);
    //    ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, IraIktatoKonyvId);
    //}

    //private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string IraIktatoKonyvId)
    //{
    //    switch (sender.ActiveTab.TabIndex)
    //    {
    //        case 0:
    //            CsoportokGridViewBind(IraIktatoKonyvId);
    //            CsoportokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsoportokGridView));
    //            CsoportokPanel.Visible = true;
    //            break;
    //        case 1:
    //            IrattariTetelekGridViewBind(IraIktatoKonyvId);
    //            IrattariTetelekPanel.Visible = true;
    //            break;
    //        case 2:
    //            JogosultakGridViewBind(IraIktatoKonyvId);
    //            JogosultakPanel.Visible = true;
    //            break;
    //    }
    //}

    //private void ActiveTabClear()
    //{
    //    switch (TabContainer1.ActiveTabIndex)
    //    {
    //        case 0:
    //            ui.GridViewClear(CsoportokGridView);
    //            break;
    //        case 1:
    //            ui.GridViewClear(IrattariTetelekGridView);
    //            break;
    //        case 2:
    //            ui.GridViewClear(JogosultakGridView);
    //            break;
    //    }
    //}

    //protected void SetDetailRowCountOnAllTab(string RowCount)
    //{
    //    Session[Constants.DetailRowCount] = RowCount;

    //    CsoportokSubListHeader.RowCount = RowCount;
    //    IrattariTetelekSubListHeader.RowCount = RowCount;
    //    JogosultakSubListHeader.RowCount = RowCount;
    //}


    //private void ActiveTabRefreshOnClientClicks()
    //{
    //    if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
    //    {
    //        CsoportokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsoportokGridView));
    //    }
    //    else if (TabContainer1.ActiveTab.Equals(IrattariTetelekTabPanel))
    //    {
    //        IrattariTetelekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(IrattariTetelekGridView));
    //    }
    //    else if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
    //    {
    //        JogosultakGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(JogosultakGridView), UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //}


    #endregion


    //#region Csoportok Detail

    //private void CsoportokSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    //{
    //    if (e.CommandName == CommandName.Invalidate)
    //    {
    //        deleteSelected_IdeIktatok();
    //        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //}

    //protected void CsoportokGridViewBind(string IraIktatoKonyvId)
    //{
    //    String sortExpression = Search.GetSortExpressionFromViewState("CsoportokGridView", ViewState, "Csoport_Iktathat_Nev");
    //    SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsoportokGridView", ViewState);

    //    CsoportokGridViewBind(IraIktatoKonyvId, sortExpression, sortDirection);
    //}

    //protected void CsoportokGridViewBind(string IraIktatoKonyvId, String SortExpression, SortDirection SortDirection)
    //{
    //    if (!string.IsNullOrEmpty(IraIktatoKonyvId))
    //    {
    //        EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
    //        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
    //        EREC_IraIktatoKonyvek IraIktatoKonyv = new EREC_IraIktatoKonyvek();
    //        IraIktatoKonyv.Id = IraIktatoKonyvId;


    //        EREC_Irat_IktatokonyveiSearch search = new EREC_Irat_IktatokonyveiSearch();
    //        search.OrderBy = Search.GetOrderBy("CsoportokGridView", ViewState, SortExpression, SortDirection);

    //        CsoportokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

    //        Result res = service.GetAllByIktatoKonyv(ExecParam, IraIktatoKonyv, search);

    //        UI.GridViewFill(CsoportokGridView, res, CsoportokSubListHeader, EErrorPanel1, ErrorUpdatePanel);
    //        UI.SetTabHeaderRowCountText(res, Header);

    //        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
    //    }
    //    else
    //    {
    //        ui.GridViewClear(CsoportokGridView);
    //    }
    //}


    //void CsoportokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    //{
    //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //}

    //protected void CsoportokUpdatePanel_Load(object sender, EventArgs e)
    //{
    //    if (Request.Params["__EVENTARGUMENT"] != null)
    //    {
    //        string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
    //        switch (eventArgument)
    //        {
    //            case EventArgumentConst.refreshDetailList:
    //                //if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
    //                //{
    //                //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraIktatoKonyvekGridView));
    //                //}
    //                ActiveTabRefreshDetailList(CsoportokTabPanel);
    //                break;
    //        }
    //    }
    //}

    ///// <summary>
    ///// T�rli a CsoportokGridView -ban kijel�lt elemeket
    ///// </summary>
    //private void deleteSelected_IdeIktatok()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(CsoportokGridView, EErrorPanel1, ErrorUpdatePanel);
    //        EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiInvalidate(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    //protected void CsoportokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Select")
    //    {
    //        int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
    //        UI.SetGridViewCheckBoxesToSelectedRow(CsoportokGridView, selectedRowNumber, "check");
    //    }
    //}

    //private void CsoportokGridView_RefreshOnClientClicks(string IktatoKonyvId)
    //{
    //    string id = IktatoKonyvId;
    //    if (!String.IsNullOrEmpty(id))
    //    {
    //        CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIktatohelyekForm.aspx"
    //            , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
    //            , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID);
    //        CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraIktatohelyekForm.aspx"
    //            , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
    //            , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
    //    }
    //}

    //protected void CsoportokGridView_PreRender(object sender, EventArgs e)
    //{
    //    int prev_PageIndex = CsoportokGridView.PageIndex;

    //    CsoportokGridView.PageIndex = CsoportokSubListHeader.PageIndex;
    //    string detailRowCount = "";
    //    if (Session[Constants.DetailRowCount] != null)
    //    {
    //        detailRowCount = Session[Constants.DetailRowCount].ToString();
    //    }

    //    if (prev_PageIndex != CsoportokGridView.PageIndex)
    //    {
    //        //scroll �llapot�nak t�rl�se
    //        JavaScripts.ResetScroll(Page, CsoportokCPE);
    //        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //    else
    //    {
    //        UI.GridViewSetScrollable(CsoportokSubListHeader.Scrollable, CsoportokCPE);
    //    }
    //    CsoportokSubListHeader.PageCount = CsoportokGridView.PageCount;
    //    CsoportokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsoportokGridView);
    //}


    //void CsoportokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    //{
    //    SetDetailRowCountOnAllTab(CsoportokSubListHeader.RowCount);
    //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //}

    //protected void CsoportokGridView_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView)
    //        , e.SortExpression, UI.GetSortToGridView("CsoportokGridView", ViewState, e.SortExpression));
    //}

    //#endregion

    //#region IrattariTetelek Detail

    //protected void IrattariTetelekGridViewBind(String IktatoKonyvId)
    //{
    //    String sortExpression = Search.GetSortExpressionFromViewState("IrattariTetelekGridView", ViewState, "IrattariTetelszam");
    //    SortDirection sortDirection = Search.GetSortDirectionFromViewState("IrattariTetelekGridView", ViewState);

    //    IrattariTetelekGridViewBind(IktatoKonyvId, sortExpression, sortDirection);

    //}

    //protected void IrattariTetelekGridViewBind(String IktatoKonyvId, String SortExpression, SortDirection SortDirection)
    //{

    //    if (!String.IsNullOrEmpty(IktatoKonyvId))
    //    {
    //        EREC_IrattariTetel_IktatokonyvService service = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
    //        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

    //        EREC_IrattariTetel_IktatokonyvSearch search = new EREC_IrattariTetel_IktatokonyvSearch();
    //        //search.Iktatokonyv_Id.Value = IktatoKonyvId;
    //        //search.Iktatokonyv_Id.Operator = Query.Operators.equals;

    //        search.OrderBy = Search.GetOrderBy("IrattariTetelekGridView", ViewState, SortExpression, SortDirection);

    //        IrattariTetelekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

    //        Result res = service.GetAllByIktatoKonyv(ExecParam, search, IktatoKonyvId);

    //        UI.GridViewFill(IrattariTetelekGridView, res, IrattariTetelekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
    //        UI.SetTabHeaderRowCountText(res, headerIrattariTetelek);

    //        ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariTetelekGridView);
    //    }
    //    else
    //    {
    //        ui.GridViewClear(IrattariTetelekGridView);
    //    }

    //}

    ////SubListHeader f�ggv�nyei(4)
    //private void IrattariTetelekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    //{
    //    if (e.CommandName == CommandName.Invalidate)
    //    {
    //        deleteSelectedIrattariTetelekFromIktatokonyv();
    //        IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //}

    //private void deleteSelectedIrattariTetelekFromIktatokonyv()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "IrattariTetel_IktatokonyvInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(IrattariTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
    //        EREC_IrattariTetel_IktatokonyvService service = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiInvalidate(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}

    //private void IrattariTetelekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    //{
    //    IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //}

    //private void IrattariTetelekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    //{
    //    SetDetailRowCountOnAllTab(IrattariTetelekSubListHeader.RowCount);
    //    IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //}

    ////GridView esem�nykezel�i(3-4)
    //////specialis, nem mindig kell kell
    //protected void IrattariTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page
    //        , "cb_System", "System");
    //}

    //protected void IrattariTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Select")
    //    {
    //        int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
    //        UI.SetGridViewCheckBoxesToSelectedRow(IrattariTetelekGridView, selectedRowNumber, "check");
    //    }
    //}

    //protected void IrattariTetelekGridView_PreRender(object sender, EventArgs e)
    //{
    //    int prev_PageIndex = IrattariTetelekGridView.PageIndex;

    //    IrattariTetelekGridView.PageIndex = IrattariTetelekSubListHeader.PageIndex;
    //    string detailRowCount = "";
    //    if (Session[Constants.DetailRowCount] != null)
    //    {
    //        detailRowCount = Session[Constants.DetailRowCount].ToString();
    //    }

    //    if (prev_PageIndex != IrattariTetelekGridView.PageIndex)
    //    {
    //        //scroll �llapot�nak t�rl�se
    //        JavaScripts.ResetScroll(Page, IrattariTetelekCPE);
    //        IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //    else
    //    {
    //        UI.GridViewSetScrollable(IrattariTetelekSubListHeader.Scrollable, IrattariTetelekCPE);
    //    }
    //    IrattariTetelekSubListHeader.PageCount = IrattariTetelekGridView.PageCount;
    //    IrattariTetelekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(IrattariTetelekGridView);


    //    ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariTetelekGridView);

    //}

    //protected void IrattariTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView)
    //        , e.SortExpression, UI.GetSortToGridView("IrattariTetelekGridView", ViewState, e.SortExpression));
    //}

    ////UpdatePanel esem�nykezel�i(1)
    //protected void IrattariTetelekUpdatePanel_Load(object sender, EventArgs e)
    //{
    //    if (Request.Params["__EVENTARGUMENT"] != null)
    //    {
    //        string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
    //        switch (eventArgument)
    //        {
    //            case EventArgumentConst.refreshDetailList:
    //                //if (TabContainer1.ActiveTab.Equals(IrattariTetelekTabPanel))
    //                //{
    //                //    IrattariTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraIktatoKonyvekGridView));
    //                //}
    //                ActiveTabRefreshDetailList(IrattariTetelekTabPanel);
    //                break;
    //        }
    //    }
    //}

    ////Gombok klien oldali szkripjeinek friss�t�se(1)
    //private void IrattariTetelekGridView_RefreshOnClientClicks(string Partner_kapcsolat_Id)
    //{
    //    string id = Partner_kapcsolat_Id;
    //    if (!String.IsNullOrEmpty(id))
    //    {
    //        IrattariTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelek_IktatoKonyvek_Form.aspx"
    //            , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
    //            , Defaults.PopupWidth, Defaults.PopupHeight, IrattariTetelekUpdatePanel.ClientID);
    //    }
    //}

    //protected void ImageButtonAddAllIrattariTetel_Click(object sender, ImageClickEventArgs e)
    //{
    //    string iktatokonyvId = UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView);

    //    if (!String.IsNullOrEmpty(iktatokonyvId))
    //    {
    //        EREC_IrattariTetel_IktatokonyvService svc = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
    //        ExecParam xpm = UI.SetExecParamDefault(Page);
    //        Result res = svc.InsertAllByIktatokonyv(xpm, iktatokonyvId);

    //        if (res.IsError)
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
    //        }
    //        else
    //        {
    //            IrattariTetelekGridViewBind(iktatokonyvId);
    //        }

    //    }
    //}

    //#endregion

    //#region Jogosultak Detail

    //private void JogosultakSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    //{
    //    if (e.CommandName == CommandName.Invalidate)
    //    {
    //        deleteSelected_Jogosultak();
    //        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //}

    //protected void JogosultakGridViewBind(string IraIktatoKonyvId)
    //{
    //    String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Nev");
    //    SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);

    //    JogosultakGridViewBind(IraIktatoKonyvId, sortExpression, sortDirection);
    //}

    //protected void JogosultakGridViewBind(string IraIktatoKonyvId, String SortExpression, SortDirection SortDirection)
    //{
    //    if (!string.IsNullOrEmpty(IraIktatoKonyvId))
    //    {
    //        //EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
    //        RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();

    //        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

    //        //EREC_IraIktatoKonyvek IraIktatoKonyv = new EREC_IraIktatoKonyvek();
    //        //IraIktatoKonyv.Id = IraIktatoKonyvId;

    //        Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
    //        search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, SortExpression, SortDirection);

    //        //JogosultakSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

    //        //Result res = service.GetAllByIktatoKonyv(ExecParam, IraIktatoKonyv, search);
    //        Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, IraIktatoKonyvId,search);

    //        UI.GridViewFill(JogosultakGridView, res, JogosultakSubListHeader, EErrorPanel1, ErrorUpdatePanel);
    //        UI.SetTabHeaderRowCountText(res, Label1);

    //        ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
    //    }
    //    else
    //    {
    //        ui.GridViewClear(JogosultakGridView);
    //    }
    //}

    //private void deleteSelected_Jogosultak()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(JogosultakGridView, EErrorPanel1, ErrorUpdatePanel);
    //        RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiRemoveCsoportFromJogtargyById(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.RedirectToAccessDeniedErrorPage(Page);
    //    }
    //}


    //protected void JogosultakUpdatePanel_Load(object sender, EventArgs e)
    //{
    //    if (Request.Params["__EVENTARGUMENT"] != null)
    //    {
    //        string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
    //        switch (eventArgument)
    //        {
    //            case EventArgumentConst.refreshDetailList:
    //                //if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
    //                //{
    //                //    JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraIktatoKonyvekGridView));
    //                //}
    //                ActiveTabRefreshDetailList(JogosultakTabPanel);
    //                break;
    //        }
    //    }
    //}

    //protected void JogosultakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Select")
    //    {
    //        int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
    //        UI.SetGridViewCheckBoxesToSelectedRow(JogosultakGridView, selectedRowNumber, "check");
    //    }
    //}

    //private void JogosultakGridView_RefreshOnClientClicks(string id, string IktatoKonyvId)
    //{
    //    if (!String.IsNullOrEmpty(id))
    //    {
    //        JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
    //            , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
    //              + "&" + QueryStringVars.IktatokonyvId + "=" + IktatoKonyvId
    //            , Defaults.PopupWidth, Defaults.PopupHeight, JogosultakUpdatePanel.ClientID);
    //    }
    //}

    //protected void JogosultakGridView_PreRender(object sender, EventArgs e)
    //{
    //    int prev_PageIndex = JogosultakGridView.PageIndex;

    //    JogosultakGridView.PageIndex = JogosultakSubListHeader.PageIndex;
    //    string detailRowCount = "";
    //    if (Session[Constants.DetailRowCount] != null)
    //    {
    //        detailRowCount = Session[Constants.DetailRowCount].ToString();
    //    }

    //    if (prev_PageIndex != JogosultakGridView.PageIndex)
    //    {
    //        //scroll �llapot�nak t�rl�se
    //        JavaScripts.ResetScroll(Page, JogosultakCPE);
    //        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //    }
    //    else
    //    {
    //        UI.GridViewSetScrollable(JogosultakSubListHeader.Scrollable, JogosultakCPE);
    //    }
    //    JogosultakSubListHeader.PageCount = JogosultakGridView.PageCount;
    //    JogosultakSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(JogosultakGridView);
    //}


    //void JogosultakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    //{
    //    SetDetailRowCountOnAllTab(JogosultakSubListHeader.RowCount);
    //    JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView));
    //}

    //protected void JogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraFonyilvantartokonyvekGridView)
    //        , e.SortExpression, UI.GetSortToGridView("JogosultakGridView", ViewState, e.SortExpression));
    //}

    //#endregion

    #region Statusz �rt�k�nek be�ll�t�sa
    //bernat.laszlo added

    protected void IraFonyilvantartokonyvekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page); //Ez eredetileg is bennevolt
                                                          //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbTitkos", "Titkos"); <-- Ez eredetileg is bennevolt

        if (stat_Index == 0)
        {
            stat_Index = GetColumnIndexByName(e.Row, "Statusz");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[stat_Index].Text = e.Row.Cells[stat_Index].Text.Equals("1") ? "Akt�v" : "Inakt�v";
        }

        // CR3355
        if (kezelesModja_Index == 0)
        {
            kezelesModja_Index = GetColumnIndexByName(e.Row, "KezelesTipusa");
        }
        if (kezelesModja_Index < e.Row.Cells.Count)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[kezelesModja_Index].Text.Equals("E"))
                {
                    e.Row.Cells[kezelesModja_Index].Text = Constants.IktatokonyKezelesTipusa.Elektronikus;
                }
                else if (e.Row.Cells[kezelesModja_Index].Text.Equals("P"))
                {
                    e.Row.Cells[kezelesModja_Index].Text = Constants.IktatokonyKezelesTipusa.Papir;
                }
            }


        }
    }

    protected int GetColumnIndexByName(GridViewRow row, string columnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
                if (((BoundField)cell.ContainingField).DataField.Equals(columnName))
                    break;
            columnIndex++;
        }
        return columnIndex;
    }
    #endregion
    //bernat.laszlo eddig
    // CR3355
    private DataControlField getGridViewDataColumn(string name)
    {
        foreach (DataControlField dcf in IraFonyilvantartokonyvekGridView.Columns)
        {
            if (dcf is BoundField)
            {
                if (dcf.SortExpression == name)
                {

                    return dcf;
                }
            }
        }
        return null;
    }
}

