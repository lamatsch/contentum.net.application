﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraIktatoKonyvekForm.aspx.cs" Inherits="IraIktatoKonyvekForm" Title="Iktatókönyvek karbantartása" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc9" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="~/eRecordComponent/FunkcioGombsor.ascx" TagPrefix="uc17" TagName="FunkcioGombsor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <%--CR3355--%>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tbody>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label7" runat="server" Text="Év:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc11:RequiredNumberBox ID="Ev_RequiredNumberBox" runat="server" MaxLength="4"></uc11:RequiredNumberBox>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label13" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label9" runat="server" Text="Iktatóhely egyedi azonosítója:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:RequiredTextBox ID="Iktatohely_RequiredTextBox" runat="server" MaxLength="20"
                                                Validate="true"></uc3:RequiredTextBox>
                                        </td>
                                    </tr>

                                    <tr id="tr_AutoGeneratePartner_CheckBox" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="Label2" runat="server" Text="Egyedi azonosítási lehetõség:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:RequiredTextBox ID="MegkulJelzes_RequiredTextBox" runat="server" MaxLength="20"
                                                Validate="false"></uc3:RequiredTextBox>
                                        </td>
                                    </tr>

                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="Label10" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label16" runat="server" Text="Megnevezés:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:RequiredTextBox ID="Nev_RequiredTextBox" runat="server"></uc3:RequiredTextBox>
                                        </td>
                                    </tr>
                                    <%--CR3355--%>
                                    <tr class="urlapSor" runat="server" id="tr_KezelesTipusa">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label18" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label21" runat="server" Text="Kezelés típusa:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButton ID="KezelesTipusa_RadioButton_E" runat="server" GroupName="KezelesTipusa_Selector" Text="Elektronikus" Checked="true"
                                                OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                            <asp:RadioButton ID="KezelesTipusa_RadioButton_P" runat="server" GroupName="KezelesTipusa_Selector" Text="Papír" Checked="false"
                                                OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    <tr id="tr_Tipus_dropdown" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label12" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label11" runat="server" Text="Utolsó fõszám:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc11:RequiredNumberBox ID="UtolsoFoszam_RequiredNumberBox" runat="server" MaxLength="9"></uc11:RequiredNumberBox>
                                        </td>
                                    </tr>
                                    <tr id="tr1" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label4" runat="server" Text="Ikt.szám osztás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc10:KodtarakDropDownList ID="IktSzamOsztas_KodtarakDropDownList" runat="server"></uc10:KodtarakDropDownList>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <%--<asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>--%><asp:Label
                                                ID="Label14" runat="server" Text="Azonosító:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="Azonosito_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label8" runat="server" Text="Központi iktatás:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:CheckBox ID="KozpontiIktatas_CheckBox" runat="server" />
                                        </td>
                                    </tr>
                                    <%--CR3355--%>
                                    <tr class="urlapSor" runat="server" id="tr_ITSZ">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Labe20" runat="server" Text="Alapért. irattári tételszám:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc12:IraIrattariTetelTextBox ID="Default_IraIrattariTetelTextBox1" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr id="tr_terjedelem" class="urlapSor" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Terj_ReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label17" runat="server" Text="Terjedelem:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc3:RequiredTextBox ID="Terjedelem_RequiredTextBox" runat="server" MaxLength="20"
                                                Validate="true" LabelRequiredIndicatorID="Terj_ReqStar"></uc3:RequiredTextBox>
                                        </td>
                                    </tr>
                                    <%-- bernat.laszlo modified: Label  átírva , Orig: "Érvényesség:"--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Nyitas_ReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text="Nyitásának dátuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server"></uc6:ErvenyessegCalendarControl>
                                        </td>
                                    </tr>
                                    <%-- bernat.laszlo eddig--%>

                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelLezarasDatuma" runat="server" Text="Lezárásának dátuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:CalendarControl ID="LezarasDatuma_CalendarControl" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <%--Új checkbox control felhelyezése--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label5" runat="server" Text="Automatikus megnyitás a következő<br /> évre azonos adatokkal a lezáráskor"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:CheckBox ID="cbUjranyitando" runat="server" />
                                        </td>
                                    </tr>
                                    <%--CR3355--%>
                                    <tr class="urlapSor" id="tr_selejtezes" runat="server">
                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="label15" runat="server" Text="Selejtezés dátuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:CalendarControl ID="SelejtezesDatuma_CalendarControl" runat="server" Validate="false" />
                                        </td>
                                    </tr>

                                    <%-- bernat.laszlo added: Iktatókönyv státusza--%>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label19" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label20" runat="server" Text="Iktató könyv státusza:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <asp:RadioButton ID="Ikt_Konyv_Aktiv_RadioButton" runat="server" GroupName="Erk_Konyv_Status_Selector" Text="Aktív" Checked="true" />
                                            <asp:RadioButton ID="Ikt_Konyv_Inaktiv_RadioButton" runat="server" GroupName="Erk_Konyv_Status_Selector" Text="Inaktív" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="lblErvenyessegVege" runat="server" Text="Érvényesség vége:"></asp:Label></td>
                                        <td class="mrUrlapMezo">
                                            <uc5:CalendarControl ID="ErvenyessegVege_CalendarControl" runat="server" ReadOnly="true"/>
                                        </td>
                                    </tr>
                                    <%-- bernat.laszlo eddig--%>

                                    <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label9" runat="server" Text="Alapért. irattári tételszám:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:IraIrattariTetelTextBox ID="Default_IraIrattariTetelTextBox1" runat="server" Validate="false" />
                            </td>
                        </tr>--%><%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label18" runat="server" Text="Olvasásra jogosultak:" Enabled="False"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="CsoportTextBox1" runat="server" Enabled="false" Validate="false" />
                            </td>
                        </tr>  --%>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
            <td valign="top" style="width: 0px">
                <div id="divFunkcioGombsor" runat="server" visible="false" style="margin-top: 8px;"></div>
                <uc17:FunkcioGombsor ID="FunkcioGombsor" runat="server" Visible="true" />
            </td>
        </tr>
    </table>
</asp:Content>
