﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class IraIktatoKonyvekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    #region BLG_2967
    private String _ParentForm = "";
    public String ParentForm
    {
        get { return _ParentForm; }
        set
        {
            _ParentForm = value;
            FunkcioGombsor.ParentForm = value;
        }
    }
    #endregion

    private const string kcsKod_IKT_SZAM_OSZTAS = "IKT_SZAM_OSZTAS";
    // CR3355
    private bool isTUK = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        // CR3355
        isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

        Command = Request.QueryString.Get(CommandName.Command);

        //LZS - BUG_5387 – Itt kell beállítani, hogy mindenhol meglegyen a ParentForm.
        ParentForm = this.Form.ID;
        FunkcioGombsor.ParentForm = ParentForm;

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IktatoKonyv" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;
                char jogszint = Command == CommandName.View ? 'O' : 'I';

                Result result = service.GetWithRightCheck(execParam, jogszint);
                //Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result.Record;
                    LoadComponentsFromBusinessObject(erec_IraIktatoKonyvek);

                    #region BLG_2967
                    FunkcioGombsor.HideButtons();
                    FunkcioGombsor.XMLExportVisible = true;
                    FunkcioGombsor.XMLExportEnabled = true;
                    FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.IktatokonyvId + "=" + id + "&tipus=Iktatokonyv')";
                    #endregion
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (!IsPostBack)
            {
                //LZS - BLG_5387 - Új felvitelénél elrejtjük a funkció gombsort
                FunkcioGombsor.HideButtons();
                IktSzamOsztas_KodtarakDropDownList.FillDropDownList(kcsKod_IKT_SZAM_OSZTAS, FormHeader1.ErrorPanel);

                //List<string> filterList = KodTarak.IKT_SZAM_OSZTAS.GetUsedValuesFilterList();
                //IktSzamOsztas_KodtarakDropDownList.FillDropDownList(kcsKod_IKT_SZAM_OSZTAS, filterList, false, FormHeader1.ErrorPanel);
            }
            UtolsoFoszam_RequiredNumberBox.Text = "0"; // új iktatókönyvnél mindig 0, nem módosítható
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraIktatoKonyvekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            SetComponentsVisibility();
            pageView.SetViewOnPage(Command);
        }
    }

    private void SetComponentsVisibility()
    {
        Azonosito_TextBox.ReadOnly = true;
        // CR3355
        //LezarasDatuma_CalendarControl.ReadOnly = true;
        //bernat.laszlo added
        ErvenyessegCalendarControl1.ErvVege_ImageButton.Visible = false;
        ErvenyessegCalendarControl1.ErvVege_TextBox.Visible = false;
        //bernat.laszlo eddig

        //ami View-nál és Modify-nál közös
        if (Command == CommandName.View
            || Command == CommandName.Modify)
        {
            UtolsoFoszam_RequiredNumberBox.ReadOnly = true;
            Ev_RequiredNumberBox.ReadOnly = true;
            IktSzamOsztas_KodtarakDropDownList.ReadOnly = true;
            KozpontiIktatas_CheckBox.Enabled = false;
            //bernat.laszlo added
            Ikt_Konyv_Aktiv_RadioButton.Enabled = false;
            Ikt_Konyv_Inaktiv_RadioButton.Enabled = false;
            //bernat.laszlo eddig
            // CR3355
            KezelesTipusa_RadioButton_E.Enabled = false;
            KezelesTipusa_RadioButton_P.Enabled = false;
        }

        // többi:

        if (Command == CommandName.View)
        {
            Iktatohely_RequiredTextBox.ReadOnly = true;
            MegkulJelzes_RequiredTextBox.ReadOnly = true;
            Nev_RequiredTextBox.ReadOnly = true;


            ErvenyessegCalendarControl1.ReadOnly = true;
            // CR3355
            Terjedelem_RequiredTextBox.ReadOnly = true;
            SelejtezesDatuma_CalendarControl.ReadOnly = true;
            Default_IraIrattariTetelTextBox1.ReadOnly = true;
            LezarasDatuma_CalendarControl.ReadOnly = true;

            //BLG_2549 - LZS
            //View módban az Újranyitandó checkbox-ot disable-re állítjuk.
            cbUjranyitando.Enabled = false;

        }
        // CR3355
        SetKezelesiMod();

    }

    // CR3355
    private void SetKezelesiMod()
    {
        if (isTUK)
        {
            tr_terjedelem.Visible = true;
            tr_selejtezes.Visible = true;
            tr_ITSZ.Visible = true;
            tr_KezelesTipusa.Visible = true;

            if (KezelesTipusa_RadioButton_E.Checked)
            {
                Terjedelem_RequiredTextBox.Enabled = false;
                Terjedelem_RequiredTextBox.Validate = false;
                SelejtezesDatuma_CalendarControl.ReadOnly = true;
                Default_IraIrattariTetelTextBox1.ReadOnly = true;
                LezarasDatuma_CalendarControl.ReadOnly = true;
                Terjedelem_RequiredTextBox.Text = String.Empty;
                SelejtezesDatuma_CalendarControl.Text = String.Empty;
                Default_IraIrattariTetelTextBox1.Id_HiddenField = String.Empty;
                Default_IraIrattariTetelTextBox1.Text = "";
                LezarasDatuma_CalendarControl.Text = String.Empty;


            }
            else
            {

                Terjedelem_RequiredTextBox.Enabled = true;
                Terjedelem_RequiredTextBox.Validate = true;
                SelejtezesDatuma_CalendarControl.ReadOnly = false;
                Default_IraIrattariTetelTextBox1.ReadOnly = false;
                LezarasDatuma_CalendarControl.ReadOnly = false;
                //tr_terjedelem.Visible = true;
                //tr_selejtezes.Visible = true;
            }
        }
        else
        {
            tr_terjedelem.Visible = false;
            tr_selejtezes.Visible = false;
            tr_ITSZ.Visible = false;
            tr_KezelesTipusa.Visible = false;

            //BLG_2549
            //LZS - Szerkeszthető kell legyen a lezárás textbox
            //LezarasDatuma_CalendarControl.ReadOnly = true;
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
    {
        Ev_RequiredNumberBox.Text = erec_IraIktatoKonyvek.Ev;
        Iktatohely_RequiredTextBox.Text = erec_IraIktatoKonyvek.Iktatohely;
        MegkulJelzes_RequiredTextBox.Text = erec_IraIktatoKonyvek.MegkulJelzes;
        Nev_RequiredTextBox.Text = erec_IraIktatoKonyvek.Nev;
        UtolsoFoszam_RequiredNumberBox.Text = erec_IraIktatoKonyvek.UtolsoFoszam;

        IktSzamOsztas_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_IKT_SZAM_OSZTAS, erec_IraIktatoKonyvek.IktSzamOsztas
            , FormHeader1.ErrorPanel);

        Azonosito_TextBox.Text = erec_IraIktatoKonyvek.Azonosito;
        LezarasDatuma_CalendarControl.Text = erec_IraIktatoKonyvek.LezarasDatuma;

        //LZS - BUG_4574 - Akkor jelenítjük meg, ha már érvénytelenítve van.
        if (erec_IraIktatoKonyvek.ErvVege != new DateTime(4700, 12, 31).ToString())
        {
            ErvenyessegVege_CalendarControl.Text = erec_IraIktatoKonyvek.ErvVege;
        }
        else
        {
            ErvenyessegVege_CalendarControl.Text = string.Empty;
        }

        //BLG_2549 - LZS -Újranyitandó checkbox értékének betöltése
        cbUjranyitando.Checked = (erec_IraIktatoKonyvek.Ujranyitando == "1") ? true : false;

        KozpontiIktatas_CheckBox.Checked = (erec_IraIktatoKonyvek.KozpontiIktatasJelzo == "1") ? true : false;

        ErvenyessegCalendarControl1.ErvKezd = erec_IraIktatoKonyvek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_IraIktatoKonyvek.ErvVege;

        #region Érkeztetõ könyv státusza
        //bernat.laszlo added: Az új "Iktatókönyv státusza" mezõ adatainak betöltése
        if (string.IsNullOrEmpty(erec_IraIktatoKonyvek.Statusz.ToString()))
        {
            Ikt_Konyv_Aktiv_RadioButton.Checked = true;
        }
        else
        {
            if (erec_IraIktatoKonyvek.Statusz == "1")
            {
                Ikt_Konyv_Aktiv_RadioButton.Checked = true;
            }
            else
            {
                Ikt_Konyv_Inaktiv_RadioButton.Checked = true;
            }
        }
        #endregion

        // CR3355
        Default_IraIrattariTetelTextBox1.Id_HiddenField = erec_IraIktatoKonyvek.DefaultIrattariTetelszam;
        Default_IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);
        Terjedelem_RequiredTextBox.Text = erec_IraIktatoKonyvek.Terjedelem;
        SelejtezesDatuma_CalendarControl.Text = erec_IraIktatoKonyvek.SelejtezesDatuma;
        if (string.IsNullOrEmpty(erec_IraIktatoKonyvek.KezelesTipusa.ToString()))
        {
            KezelesTipusa_RadioButton_E.Checked = true;
            KezelesTipusa_RadioButton_P.Checked = false;
        }
        else
        {
            if (erec_IraIktatoKonyvek.KezelesTipusa == "E")
            {
                KezelesTipusa_RadioButton_E.Checked = true;
                KezelesTipusa_RadioButton_P.Checked = false;
            }
            else
            {
                KezelesTipusa_RadioButton_E.Checked = false;
                KezelesTipusa_RadioButton_P.Checked = true;
            }
        }
        FormHeader1.Record_Ver = erec_IraIktatoKonyvek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_IraIktatoKonyvek.Base);

    }

    // form --> business object
    private EREC_IraIktatoKonyvek GetBusinessObjectFromComponents()
    {
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = new EREC_IraIktatoKonyvek();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_IraIktatoKonyvek.Updated.SetValueAll(false);
        erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);

        erec_IraIktatoKonyvek.IktatoErkezteto = Constants.IktatoErkezteto.Iktato;
        erec_IraIktatoKonyvek.Updated.IktatoErkezteto = true;

        erec_IraIktatoKonyvek.Iktatohely = Iktatohely_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.Iktatohely = pageView.GetUpdatedByView(Iktatohely_RequiredTextBox);

        erec_IraIktatoKonyvek.MegkulJelzes = MegkulJelzes_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.MegkulJelzes = pageView.GetUpdatedByView(MegkulJelzes_RequiredTextBox);

        erec_IraIktatoKonyvek.Nev = Nev_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.Nev = pageView.GetUpdatedByView(Nev_RequiredTextBox);

        if (KozpontiIktatas_CheckBox.Checked == true)
        {
            erec_IraIktatoKonyvek.KozpontiIktatasJelzo = "1";
        }
        else
        {
            erec_IraIktatoKonyvek.KozpontiIktatasJelzo = "0";
        }
        erec_IraIktatoKonyvek.Updated.KozpontiIktatasJelzo = pageView.GetUpdatedByView(KozpontiIktatas_CheckBox);

        // Nem módosíthatóak:
        if (Command == CommandName.New)
        {
            erec_IraIktatoKonyvek.Ev = Ev_RequiredNumberBox.Text;
            erec_IraIktatoKonyvek.Updated.Ev = pageView.GetUpdatedByView(Ev_RequiredNumberBox);

            erec_IraIktatoKonyvek.UtolsoFoszam = UtolsoFoszam_RequiredNumberBox.Text;
            erec_IraIktatoKonyvek.Updated.UtolsoFoszam = pageView.GetUpdatedByView(UtolsoFoszam_RequiredNumberBox);

            erec_IraIktatoKonyvek.IktSzamOsztas = IktSzamOsztas_KodtarakDropDownList.SelectedValue;
            erec_IraIktatoKonyvek.Updated.IktSzamOsztas = pageView.GetUpdatedByView(IktSzamOsztas_KodtarakDropDownList);

        }




        //// TODO: egyelõre ezek nincsenek töltve:
        //erec_IraIktatoKonyvek.UtolsoNyomtatasIdo = "";
        //erec_IraIktatoKonyvek.MaxMinosites = "";
        //erec_IraIktatoKonyvek.Csoport_Id_Olvaso = "";
        //erec_IraIktatoKonyvek.Csoport_Id_Tulaj = "";

        //BLG_2549 - LZS - Lezárás dátumának felolvasása a felületről
        erec_IraIktatoKonyvek.LezarasDatuma = !String.IsNullOrEmpty(LezarasDatuma_CalendarControl.Text) ? (LezarasDatuma_CalendarControl.Text.Substring(0, 10)) + " 23:59" : "";
        erec_IraIktatoKonyvek.Updated.LezarasDatuma = pageView.GetUpdatedByView(LezarasDatuma_CalendarControl);

        //BLG_2549 - LZS - Újranyitandó érték felolvasása a felületről
        erec_IraIktatoKonyvek.Ujranyitando = cbUjranyitando.Checked ? "1" : "0";
        erec_IraIktatoKonyvek.Updated.Ujranyitando = pageView.GetUpdatedByView(cbUjranyitando);

        //erec_IraIktatoKonyvek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //erec_IraIktatoKonyvek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //erec_IraIktatoKonyvek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //erec_IraIktatoKonyvek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_IraIktatoKonyvek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        #region Iktatókönyv státusza
        //bernat.laszlo added
        if (Ikt_Konyv_Aktiv_RadioButton.Checked)
        {
            erec_IraIktatoKonyvek.Statusz = "1";
            erec_IraIktatoKonyvek.Updated.Statusz = pageView.GetUpdatedByView(Ikt_Konyv_Aktiv_RadioButton);
        }
        else
        {
            erec_IraIktatoKonyvek.Statusz = "0";
            erec_IraIktatoKonyvek.Updated.Statusz = pageView.GetUpdatedByView(Ikt_Konyv_Aktiv_RadioButton);
        }
        #endregion

        // CR3355
        erec_IraIktatoKonyvek.DefaultIrattariTetelszam = Default_IraIrattariTetelTextBox1.Id_HiddenField;
        erec_IraIktatoKonyvek.Updated.DefaultIrattariTetelszam = pageView.GetUpdatedByView(Default_IraIrattariTetelTextBox1);

        erec_IraIktatoKonyvek.Terjedelem = Terjedelem_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.Terjedelem = pageView.GetUpdatedByView(Terjedelem_RequiredTextBox);

        erec_IraIktatoKonyvek.SelejtezesDatuma = SelejtezesDatuma_CalendarControl.Text;
        erec_IraIktatoKonyvek.Updated.SelejtezesDatuma = pageView.GetUpdatedByView(SelejtezesDatuma_CalendarControl);

        if (KezelesTipusa_RadioButton_E.Checked)
        {
            erec_IraIktatoKonyvek.KezelesTipusa = "E";
            erec_IraIktatoKonyvek.Updated.KezelesTipusa = pageView.GetUpdatedByView(KezelesTipusa_RadioButton_E);
        }
        else
        {
            erec_IraIktatoKonyvek.KezelesTipusa = "P";
            erec_IraIktatoKonyvek.Updated.KezelesTipusa = pageView.GetUpdatedByView(KezelesTipusa_RadioButton_P);
        }


        erec_IraIktatoKonyvek.Base.Ver = FormHeader1.Record_Ver;
        erec_IraIktatoKonyvek.Base.Updated.Ver = true;

        return erec_IraIktatoKonyvek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Iktatohely_RequiredTextBox);
            compSelector.Add_ComponentOnClick(Ev_RequiredNumberBox);
            compSelector.Add_ComponentOnClick(MegkulJelzes_RequiredTextBox);
            compSelector.Add_ComponentOnClick(Nev_RequiredTextBox);
            compSelector.Add_ComponentOnClick(UtolsoFoszam_RequiredNumberBox);
            compSelector.Add_ComponentOnClick(LezarasDatuma_CalendarControl);
            compSelector.Add_ComponentOnClick(IktSzamOsztas_KodtarakDropDownList);

            //bernat.laszlo removed
            //compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            compSelector.Add_ComponentOnClick(KozpontiIktatas_CheckBox);
            #region Érkeztetõ könyv státusza
            //bernat.laszlo added
            compSelector.Add_ComponentOnClick(Ikt_Konyv_Aktiv_RadioButton);
            compSelector.Add_ComponentOnClick(Ikt_Konyv_Inaktiv_RadioButton);
            #endregion

            // CR3355
            compSelector.Add_ComponentOnClick(Terjedelem_RequiredTextBox);
            compSelector.Add_ComponentOnClick(SelejtezesDatuma_CalendarControl);
            compSelector.Add_ComponentOnClick(KezelesTipusa_RadioButton_E);
            compSelector.Add_ComponentOnClick(KezelesTipusa_RadioButton_P);
            compSelector.Add_ComponentOnClick(Default_IraIrattariTetelTextBox1);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IktatoKonyv" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                            EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.Insert(execParam, erec_IraIktatoKonyvek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_RequiredTextBox.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    Contentum.eUtility.JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);

                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                                EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_IraIktatoKonyvek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    // CR3355
    protected void KezelesTipusa_RadioButton_CheckedChanged(object sender, EventArgs e)
    {
        SetKezelesiMod();
    }
}
