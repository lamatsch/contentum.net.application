using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;
using Microsoft.Reporting.WebForms;

public partial class IraIktatokonyvekPrintForm : Contentum.eUtility.UI.PageBase
{
    private string iktatokonyvId = String.Empty;
    private string datumtol = String.Empty;
    private string datumig = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        iktatokonyvId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);
        if (!String.IsNullOrEmpty(Request.QueryString["datumtol"]))
        {
            datumtol = Convert.ToDateTime(Request.QueryString["datumtol"]).ToString("yyyy.MM.dd");
        }

        else
        {
            datumtol = "1900.01.01";
        }

        if (!String.IsNullOrEmpty(Request.QueryString["datumig"]))
        {
            datumig = Convert.ToDateTime(Request.QueryString["datumig"]).ToString("yyyy.MM.dd");
        }

        else
        {
            datumig = "2100.01.01";
        }


        if (String.IsNullOrEmpty(iktatokonyvId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

       //     ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        // FormFooter1.ImageButton_Save.Visible = true;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.IraIktatokonyv_Id.Value = iktatokonyvId;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
       // DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_IraIratokSearch.IktatasDatuma);

        return erec_IraIratokSearch;
    }


    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;



        if (rpis != null)
        {
            if (rpis.Count > 0)

            {


                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                EREC_IraIratokSearch erec_IraIratokSearch = GetSearchObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Fake = true;
                Result result = service.GetAllWithExtension(execParam, erec_IraIratokSearch);





                ReportParameters = new ReportParameter[rpis.Count];



                for (int i = 0; i < rpis.Count; i++)
                {


                    ReportParameters[i] = new ReportParameter(rpis[i].Name);



                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));


                            }
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where")+ " and EREC_IraIratok.LetrehozasIdo between '"+ datumtol + "' and '"+datumig+"'");
                            break;

                        case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;

                        case "Where_Szamlak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Szamlak"));
                            break;




                        case "Where_KuldKuldemenyek_Csatolt":
                            if (!string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt")))
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek_Csatolt"));
                            }


                            break;

                        case "Where_EREC_IraOnkormAdatok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_EREC_IraOnkormAdatok"));
                            break;

                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            //  ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;



                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;


                        case "AlSzervezetId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@AlSzervezetId"));
                            break;

                        case "IdoszakTol":
                            ReportParameters[i].Values.Add(datumtol);
                            break;

                        case "IdoszakIg":
                            ReportParameters[i].Values.Add(datumig);
                            break;



                    }
                }




            }

        }
        return ReportParameters;
    }


}
