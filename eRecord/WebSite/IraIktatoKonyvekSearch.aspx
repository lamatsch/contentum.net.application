<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IraIktatoKonyvekSearch.aspx.cs" Inherits="IraIktatoKonyvekSearch" Title="Iktat�k�nyvek keres�se" %>

<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc13" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>


<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 868px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                     <%--CR3355--%>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label7" runat="server" Text="�v:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc14:EvIntervallum_SearchFormControl ID="Ev_EvIntervallum_SearchFormControl" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label id="labelIktatohely" runat="server" Text="Iktat�hely:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox id="Iktatohely_RequiredTextBox" runat="server" MaxLength="20" Validate="false" />
                                </td>
                            </tr>
                            <tr id="tr_AutoGeneratePartner_CheckBox" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label2" runat="server" Text="Egyedi azonos�t�si lehet�s�g:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="MegkulJelzes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label16" runat="server" Text="Megnevez�s:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput" Width="390px"></asp:TextBox></td>
                            </tr>
                            <tr id="tr_Tipus_dropdown" class="urlapSor" runat="server">
                                <td style="height: 24px" class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label11" runat="server" Text="Utols� f�sz�m:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc13:SzamIntervallum_SearchFormControl ID="UtolsoFoszam_SzamIntervallum_SearchFormControl"
                                        runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label1" runat="server" Text="Ikt.sz�m oszt�s:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc11:KodtarakDropDownList ID="IktSzamOsztas_KodtarakDropDownList" runat="server"></uc11:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label14" runat="server" Text="Azonos�t�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Azonosito_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                             <%--CR3355--%>
                             <tr class="urlapSor" runat="server" id="tr_KezelesTipusa">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label21" runat="server" Text="Kezel�s t�pusa:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButton ID="KezelesTipusa_RadioButton_E" runat="server" GroupName="KezelesTipusa_Selector" Text="Elektronikus" Checked="false"  
                                            OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                    <asp:RadioButton ID="KezelesTipusa_RadioButton_P" runat="server" GroupName="KezelesTipusa_Selector" Text="Pap�r"        Checked="false" 
                                            OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                     <asp:RadioButton ID="KezelesTipusa_RadioButton_O" runat="server" GroupName="KezelesTipusa_Selector" Text="�sszes"        Checked="true" 
                                            OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                </td>
                            </tr>
                          
                             <tr class="urlapSor"  runat="server" id="tr_ITSZ">
                                <td class="mrUrlapCaption">
                                    Alap�rt. iratt�ri t�telsz�m</td>
                                <td class="mrUrlapMezo">
                                    <uc12:IraIrattariTetelTextBox ID="Default_IraIrattariTetelTextBox1" runat="server" Validate="false" SearchMode="true"></uc12:IraIrattariTetelTextBox>
                                </td>
                            </tr>
                              <tr id="tr_terjedelem" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    
                                    <asp:Label ID="Label17" runat="server" Text="Terjedelem:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Terjedelem_TextBox" runat="server" CssClass="mrUrlapInput" MaxLength="20"></asp:TextBox>
                                </td>
                            </tr>
                             <%--CR3355--%>
                              <tr class="urlapSor" id="tr_selejtezes" runat="server">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="label15" runat="server" Text="Selejtez�s d�tuma:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                     <uc13:DatumIntervallum_SearchCalendarControl ID="SelejtezesDatuma_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                                    <%--<uc5:CalendarControl ID="SelejtezesDatuma_CalendarControl" runat="server" Validate="false" />--%>
                                </td>
                            </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="labelLezarasDatuma" runat="server" Text="Lez�r�s d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc13:DatumIntervallum_SearchCalendarControl ID="LezarasDatuma_DatumIntervallum_SearchCalendarControl"
                                    runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                            <%--  <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    Alap�rt. iratt�ri t�telsz�m</td>
                                <td class="mrUrlapMezo">
                                    <uc12:IraIrattariTetelTextBox ID="Default_IraIrattariTetelszam_IraIrattariTetelTextBox"
                                        runat="server" Validate="false" SearchMode="true"></uc12:IraIrattariTetelTextBox>
                                </td>
                            </tr>--%>
                            <tr class="urlapSor">
                                <td colspan="2">
                                    &nbsp;<uc9:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1"
                                        runat="server"></uc9:Ervenyesseg_SearchFormComponent>
                                    &nbsp;</td>
                            </tr>
                            <tr class="urlapSor">
                                <td colspan="2">
                                    <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                        runat="server"></uc10:TalalatokSzama_SearchFormComponent>
                                    &nbsp; &nbsp; &nbsp;&nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </ContentTemplate>
            </asp:UpdatePanel>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            &nbsp;</td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
