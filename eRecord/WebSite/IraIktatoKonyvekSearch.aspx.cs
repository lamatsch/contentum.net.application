using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class IraIktatoKonyvekSearch : Contentum.eUtility.UI.PageBase

{
    private Type _type = typeof(EREC_IraIktatoKonyvekSearch);
    private const string kcsKod_IKT_SZAM_OSZTAS = "IKT_SZAM_OSZTAS";
    // CR3355
    private bool isTUK = false;


    protected void Page_Init(object sender, EventArgs e)
    {
        // CR3355
        isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.IktatoKonyvekSearch;
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.IraIktatoKonyvekSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IraIktatoKonyvekSearch searchObject = null;
            if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.IktatoKonyvekSearch))
            {
                //searchObject = (EREC_IraIktatoKonyvekSearch)Search.GetSearchObject(Page, new EREC_IraIktatoKonyvekSearch());
                searchObject = (EREC_IraIktatoKonyvekSearch)Search.GetSearchObject_CustomSessionName
                            (Page, new EREC_IraIktatoKonyvekSearch(), Constants.CustomSearchObjectSessionNames.IktatoKonyvekSearch);
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
            SetKezelesiMod();
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = null;
        if (searchObject != null) erec_IraIktatoKonyvekSearch = (EREC_IraIktatoKonyvekSearch)searchObject;

        if (erec_IraIktatoKonyvekSearch != null)
        {
            Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_IraIktatoKonyvekSearch.Ev);

            Iktatohely_RequiredTextBox.Text = erec_IraIktatoKonyvekSearch.Iktatohely.Value;

            MegkulJelzes_TextBox.Text = erec_IraIktatoKonyvekSearch.MegkulJelzes.Value;
            Nev_TextBox.Text = erec_IraIktatoKonyvekSearch.Nev.Value;

            UtolsoFoszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                erec_IraIktatoKonyvekSearch.UtolsoFoszam);
            //UtolsoFoszam_TextBox.Text = erec_IraIktatoKonyvekSearch.UtolsoFoszam.Value;

            IktSzamOsztas_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_IKT_SZAM_OSZTAS,
                erec_IraIktatoKonyvekSearch.IktSzamOsztas.Value, true, SearchHeader1.ErrorPanel);
            //List<string> filterList = KodTarak.IKT_SZAM_OSZTAS.GetUsedValuesFilterList();
            //IktSzamOsztas_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_IKT_SZAM_OSZTAS,
            //    erec_IraIktatoKonyvekSearch.IktSzamOsztas.Value, filterList, true, SearchHeader1.ErrorPanel);


            Azonosito_TextBox.Text = erec_IraIktatoKonyvekSearch.Azonosito.Value;

            LezarasDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_IraIktatoKonyvekSearch.LezarasDatuma);

            //Default_IraIrattariTetelszam_IraIrattariTetelTextBox.Id_HiddenField = erec_IraIktatoKonyvekSearch.DefaultIrattariTetelszam.Value;
            //Default_IraIrattariTetelszam_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);
            
            Ervenyesseg_SearchFormComponent1.SetDefault(
                erec_IraIktatoKonyvekSearch.ErvKezd, erec_IraIktatoKonyvekSearch.ErvVege);

            // CR3355
            Default_IraIrattariTetelTextBox1.Id_HiddenField = erec_IraIktatoKonyvekSearch.DefaultIrattariTetelszam.Value;
            Default_IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);
            Terjedelem_TextBox.Text = erec_IraIktatoKonyvekSearch.Terjedelem.Value;
            SelejtezesDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_IraIktatoKonyvekSearch.SelejtezesDatuma);
            if (isTUK)
            {
                if (string.IsNullOrEmpty(erec_IraIktatoKonyvekSearch.KezelesTipusa.Value))
                {

                    KezelesTipusa_RadioButton_O.Checked = true;
                    KezelesTipusa_RadioButton_E.Checked = false;
                    KezelesTipusa_RadioButton_P.Checked = false;
                }
                else
                {
                    if (erec_IraIktatoKonyvekSearch.KezelesTipusa.Value == "E")
                    {
                        KezelesTipusa_RadioButton_E.Checked = true;
                        KezelesTipusa_RadioButton_P.Checked = false;
                        KezelesTipusa_RadioButton_O.Checked = false;
                    }
                    else
                    {
                        KezelesTipusa_RadioButton_E.Checked = false;
                        KezelesTipusa_RadioButton_P.Checked = true;
                        KezelesTipusa_RadioButton_O.Checked = false;
                    }
                }

            }
            else
            {
                KezelesTipusa_RadioButton_E.Checked = false;
                KezelesTipusa_RadioButton_P.Checked = false;
                KezelesTipusa_RadioButton_O.Checked = true;
            }
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IraIktatoKonyvekSearch SetSearchObjectFromComponents()
    {
        EREC_IraIktatoKonyvekSearch erec_IraIktatoKonyvekSearch = (EREC_IraIktatoKonyvekSearch)SearchHeader1.TemplateObject;
        if (erec_IraIktatoKonyvekSearch == null)
        {
            erec_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
        }

        Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_IraIktatoKonyvekSearch.Ev);

        if (!String.IsNullOrEmpty(Iktatohely_RequiredTextBox.Text))
        {
            erec_IraIktatoKonyvekSearch.Iktatohely.Value = Iktatohely_RequiredTextBox.Text;
            erec_IraIktatoKonyvekSearch.Iktatohely.Operator = Search.GetOperatorByLikeCharater(Iktatohely_RequiredTextBox.Text);
        }

        if (!String.IsNullOrEmpty(MegkulJelzes_TextBox.Text))
        {
            erec_IraIktatoKonyvekSearch.MegkulJelzes.Value = MegkulJelzes_TextBox.Text;
            erec_IraIktatoKonyvekSearch.MegkulJelzes.Operator = Search.GetOperatorByLikeCharater(MegkulJelzes_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            erec_IraIktatoKonyvekSearch.Nev.Value = Nev_TextBox.Text;
            erec_IraIktatoKonyvekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }

        UtolsoFoszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
            erec_IraIktatoKonyvekSearch.UtolsoFoszam);

        //if (!String.IsNullOrEmpty(UtolsoFoszam_TextBox.Text))
        //{
        //    erec_IraIktatoKonyvekSearch.UtolsoFoszam.Value = UtolsoFoszam_TextBox.Text;
        //    erec_IraIktatoKonyvekSearch.UtolsoFoszam.Operator = Search.GetOperatorByLikeCharater(UtolsoFoszam_TextBox.Text);
        //}

        if (!String.IsNullOrEmpty(IktSzamOsztas_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIktatoKonyvekSearch.IktSzamOsztas.Value = IktSzamOsztas_KodtarakDropDownList.SelectedValue;
            erec_IraIktatoKonyvekSearch.IktSzamOsztas.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Azonosito_TextBox.Text))
        {
            erec_IraIktatoKonyvekSearch.Azonosito.Value = Azonosito_TextBox.Text;
            erec_IraIktatoKonyvekSearch.Azonosito.Operator = Search.GetOperatorByLikeCharater(Azonosito_TextBox.Text);
        }

        //if (!String.IsNullOrEmpty(Default_IraIrattariTetelszam_IraIrattariTetelTextBox.Text))
        //{
        //    erec_IraIktatoKonyvekSearch.DefaultIrattariTetelszam.Value = Default_IraIrattariTetelszam_IraIrattariTetelTextBox.Id_HiddenField;
        //    erec_IraIktatoKonyvekSearch.DefaultIrattariTetelszam.Operator = Query.Operators.equals;
        //}       

        LezarasDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraIktatoKonyvekSearch.LezarasDatuma);

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            erec_IraIktatoKonyvekSearch.ErvKezd, erec_IraIktatoKonyvekSearch.ErvVege);


        // CR3355
        if (!String.IsNullOrEmpty(Terjedelem_TextBox.Text))
        {
            erec_IraIktatoKonyvekSearch.Terjedelem.Value = Terjedelem_TextBox.Text;
            erec_IraIktatoKonyvekSearch.Terjedelem.Operator = Search.GetOperatorByLikeCharater(Terjedelem_TextBox.Text);
        }

        SelejtezesDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_IraIktatoKonyvekSearch.SelejtezesDatuma);

        if (KezelesTipusa_RadioButton_E.Checked)
        {
            erec_IraIktatoKonyvekSearch.KezelesTipusa.Value = "E";
            erec_IraIktatoKonyvekSearch.KezelesTipusa.Operator = Query.Operators.equals;
        }
        else
        if (KezelesTipusa_RadioButton_P.Checked)
        {
            erec_IraIktatoKonyvekSearch.KezelesTipusa.Value = "P";
            erec_IraIktatoKonyvekSearch.KezelesTipusa.Operator = Query.Operators.equals;
        }

        return erec_IraIktatoKonyvekSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added (Mentett találati listák)
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            SearchHeader1.NewResultList(SetSearchObjectFromComponents(), execParam);
        }
        //bernat.laszlo eddig
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraIktatoKonyvekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                //Search.RemoveSearchObjectFromSession(Page, _type);
                Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.IktatoKonyvekSearch);
            }
            else
            {
                //Search.SetSearchObject(Page, searchObject);
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.IktatoKonyvekSearch);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IraIktatoKonyvekSearch GetDefaultSearchObject()
    {
        return new EREC_IraIktatoKonyvekSearch();
    }

    // CR3355
    protected void KezelesTipusa_RadioButton_CheckedChanged(object sender, EventArgs e)
    {
        SetKezelesiMod();
    }

    // CR3355
    private void SetKezelesiMod()
    {
        if (isTUK)
        {
            tr_terjedelem.Visible = true;
            tr_selejtezes.Visible = true;
            tr_ITSZ.Visible = true;
            tr_KezelesTipusa.Visible = true;

            if (KezelesTipusa_RadioButton_E.Checked)
            {
                Terjedelem_TextBox.Enabled = false;
                SelejtezesDatuma_DatumIntervallum_SearchCalendarControl.Enabled = false;
                Default_IraIrattariTetelTextBox1.ReadOnly = true;
                LezarasDatuma_DatumIntervallum_SearchCalendarControl.Enabled = false;
                Terjedelem_TextBox.Text = String.Empty;
                SelejtezesDatuma_DatumIntervallum_SearchCalendarControl.Clear();
                Default_IraIrattariTetelTextBox1.Id_HiddenField = String.Empty;
                Default_IraIrattariTetelTextBox1.Text = "";
                LezarasDatuma_DatumIntervallum_SearchCalendarControl.DatumKezd = String.Empty;
                LezarasDatuma_DatumIntervallum_SearchCalendarControl.DatumVege = String.Empty;

            }
            else
            {

                Terjedelem_TextBox.Enabled = true;
                SelejtezesDatuma_DatumIntervallum_SearchCalendarControl.Enabled = true;
                Default_IraIrattariTetelTextBox1.ReadOnly = false;
                LezarasDatuma_DatumIntervallum_SearchCalendarControl.Enabled = true;
                //tr_terjedelem.Visible = true;
                //tr_selejtezes.Visible = true;
            }
        }
        else
        {
            tr_terjedelem.Visible = false;
            tr_selejtezes.Visible = false;
            tr_ITSZ.Visible = false;
            tr_KezelesTipusa.Visible = false;
            LezarasDatuma_DatumIntervallum_SearchCalendarControl.Enabled = false;

        }
    }
}
