<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IraIktatokonyvJogosultakForm.aspx.cs" Inherits="IraIktatokonyvJogosultakForm" EnableEventValidation="false" %>

<%@ Register Src="eRecordComponent/IktatokonyvTextBox.ascx" TagName="IktatokonyvTextBox"
    TagPrefix="uc10" %>

    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>

<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc9" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>



<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>

    <uc1:FormHeader ID="FormHeader1" runat="server"/>

    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label_Iktatokonyv" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc10:IktatokonyvTextBox ID="Iktatokonyv_IktatokonyvTextBox" runat="server" LabelID="Label_Iktatokonyv"/>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="JogosultRow" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label_Csoport_Jogosult" runat="server" Text="Jogosult:"></asp:Label>&nbsp;</td>
                            <td class="mrUrlapMezo">
                                <uc7:CsoportTextBox ID="Csoport_Id_JogosultTextBox" runat="server" />
                                </td>
                        </tr>
                    </table>
                    <asp:UpdatePanel runat="server" ID="ListUpdatePanel" Visible="false">
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;margin-top:10px; margin-bottom:10px;">
                                  <tr>
                                      <td style="width:50%;padding: 0px 10px;">
                                          <div style="text-align:left;padding-bottom: 2px;">
                                             <table cellpadding="0" cellspacing="0">
                                                 <tr>
                                                     <td style="width:auto">
                                                          <span style="font-weight:bold; text-decoration:underline;">Csoportok:</span>
                                                     </td>
                                                     <td style="width:100%; padding-left:5px;padding-right:8px">
                                                         <asp:TextBox ID="TextBoxSearch" runat="server" Width="100%" CausesValidation="false" OnTextChanged="TextBoxSearch_TextChanged" AutoPostBack="true"/>
                                                     </td>
                                                     <td style="width:auto">
                                                         <asp:ImageButton id="ButtonSearch" onclick="ButtonSearch_Click" runat="server" AlternateText="Keres�s" ImageUrl="~/images/hu/egyeb/kereses.gif" CausesValidation="false">
                                                         </asp:ImageButton>
                                                     </td>
                                                 </tr>
                                             </table>                
                                        </div>

                                      </td>
                                      <td>
                                      </td>
                                      <td style="width:50%;padding: 0px 10px;">
                                          <div style="text-align:left;padding-bottom: 2px;">
                                            <span style="font-weight:bold; text-decoration:underline;">Jogosultak:</span>
                                        </div>
                                      </td>
                                  </tr>
                                  <tr>
                                    <td style="width:50%;padding:0px 10px;">
                                        <asp:ListBox ID="CsoportokList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td>
                                        <asp:Button runat="server" ID="AddButton" Text=">" Width="50px" style="margin-bottom: 5px;" ToolTip="Hozz�ad" CausesValidation="false" OnClick="AddButton_Click"/>
                                        <asp:Button runat="server" ID="RemoveButton" Text="<" Width="50px" style="margin-top: 5px;" ToolTip="T�r�l" CausesValidation="false" OnClick="RemoveButton_Click"/>
                                    </td>
                                    <td style="width:50%;padding:0px 10px;">
                                        <asp:ListBox ID="JogosultakList" runat="server" Rows="20" Width="100%" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                </tr> 
                                <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                    <td colspan="3" style="padding:10px;">
                                        <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                    </td>
                                </tr>
                             </table>
                         </ContentTemplate>
                    </asp:UpdatePanel>
                    </eUI:eFormPanel>                
                    <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>

