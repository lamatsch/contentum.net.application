using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IraIktatokonyvJogosultakForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraIktatokonyvJogosultakFormHeaderTitle;
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Irat_Iktatokonyvei" + Command);
                break;
        }


        string iktatokonyvId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);

        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                KRT_JogosultakService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_JogosultakService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;
                
                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    KRT_Jogosultak krt_jogosultak = (KRT_Jogosultak)result.Record;
                    LoadComponentsFromBusinessObject(krt_jogosultak);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            
            if (!String.IsNullOrEmpty(iktatokonyvId))
            {
                Iktatokonyv_IktatokonyvTextBox.Id_HiddenField = iktatokonyvId;
                Iktatokonyv_IktatokonyvTextBox.SetIktatokonyvekTextBoxById(FormHeader1.ErrorPanel);
                Iktatokonyv_IktatokonyvTextBox.ReadOnly = true;
            }

            if (!IsPostBack)
            {
                FillCsoportokList();
                FillJogosultakList(iktatokonyvId);
            }

            JogosultRow.Visible = false;
            ListUpdatePanel.Visible = true;
        }        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(KRT_Jogosultak krt_jogosultak)
    {
        Iktatokonyv_IktatokonyvTextBox.Id_HiddenField = Request.QueryString.Get(QueryStringVars.IktatokonyvId);
        Iktatokonyv_IktatokonyvTextBox.SetIktatokonyvekTextBoxById(FormHeader1.ErrorPanel);
        Iktatokonyv_IktatokonyvTextBox.ReadOnly = true;

        Csoport_Id_JogosultTextBox.Id_HiddenField = krt_jogosultak.Csoport_Id_Jogalany;
        Csoport_Id_JogosultTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        Csoport_Id_JogosultTextBox.ReadOnly = true;

        FormHeader1.Record_Ver = krt_jogosultak.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(krt_jogosultak.Base);

    }

    // form --> business object
    private KRT_Jogosultak GetBusinessObjectFromComponents()
    {
        KRT_Jogosultak krt_jogosultak = new KRT_Jogosultak();
        krt_jogosultak.Updated.SetValueAll(false);
        krt_jogosultak.Base.Updated.SetValueAll(false);

        krt_jogosultak.Csoport_Id_Jogalany = Csoport_Id_JogosultTextBox.Id_HiddenField; ;
        krt_jogosultak.Updated.Csoport_Id_Jogalany = pageView.GetUpdatedByView(Csoport_Id_JogosultTextBox);

        krt_jogosultak.Base.Ver = FormHeader1.Record_Ver;
        krt_jogosultak.Base.Updated.Ver = true;

        return krt_jogosultak;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Iktatokonyv_IktatokonyvTextBox);
            compSelector.Add_ComponentOnClick(Csoport_Id_JogosultTextBox);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            List<string> csoportIds = new List<string>();

                            foreach (ListItem item in JogosultakList.Items)
                            {
                                csoportIds.Add(item.Value);
                            }

                            Result result = service.SetCsoportokToJogtargy(execParam, Iktatokonyv_IktatokonyvTextBox.Id_HiddenField, csoportIds.ToArray(), 'I');

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                string[] addedCsoportIds = (string[])result.Record;
                                EREC_Irat_IktatokonyveiService iis = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
                                iis.AddCsoportokToIktatokonyv(execParam, Iktatokonyv_IktatokonyvTextBox.Id_HiddenField, addedCsoportIds);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    void FillCsoportokList()
    {
        CsoportokList.Items.Clear();

        KRT_CsoportokService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        KRT_CsoportokSearch search = new KRT_CsoportokSearch();
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;
        search.OrderBy = "Nev";
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result  = service.GetAll(ExecParam, search);

        UI.ListBoxFill(CsoportokList, result, "Nev", FormHeader1.ErrorPanel, null);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    void FillJogosultakList(string iktatokonyvId)
    {
        JogosultakList.Items.Clear();

        RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        RowRightsCsoportSearch search = new RowRightsCsoportSearch();
        search.OrderBy = "Nev";

        Result result = service.GetAllRightedCsoportByJogtargy(ExecParam, iktatokonyvId, search);

        UI.ListBoxFill(JogosultakList, result, "Csoport_Id_Jogalany", "Nev", FormHeader1.ErrorPanel, null);
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(CsoportokList);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = JogosultakList.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                JogosultakList.Items.Add(item);
            }
        }
    }

    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        List<ListItem> selectedItems = GetSelectedItems(JogosultakList);
        foreach (ListItem item in selectedItems)
        {
            JogosultakList.Items.Remove(item);
        }
    }

    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        List<ListItem> selectedItems = new List<ListItem>();

        foreach (ListItem item in listBox.Items)
        {
            if (item.Selected)
            {
                selectedItems.Add(item);
            }
        }

        return selectedItems;
    }

    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokList();
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokList();
    }
}
