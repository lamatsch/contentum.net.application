<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraIktatokonyvekLezarasForm.aspx.cs" Inherits="IraIktatokonyvekLezarasForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc9" %>
<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup"
    TagPrefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <!-- K�tf�le checkboxot figyel�nk. A "select" checkbox a saj�t "�j iktat�k�nyv" checkbox�t is v�ltoztatja, viszont ez ford�tva nem igaz. Ha a "select" unchecked, a m�sikat se lehet bejel�lni. -->
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
                if ($("input[type=checkbox][id$='cbUjIktatoKonyv']").length > 0) {
                    var cb = $(this);
                    var cbUj = cb.closest('tr').find("input[type=checkbox][id$='cbUjIktatoKonyv']").first();
                    if (cbUj.length == 1) {
                        cbUj.prop("checked", cb.is(":checked"));
                        cbUj.prop("disabled", !cb.is(":checked"));
                        var countUj = $("input[type=checkbox][id$='cbUjIktatoKonyv']:checked").length;
                        $("span[id$='labelUjTetelekSzamaDb']").text(countUj.toString());
                    }
                }
            });

            $("input[type=checkbox][id$='cbUjIktatoKonyv']").click(function (e) {
                var countUj = $("input[type=checkbox][id$='cbUjIktatoKonyv']:checked").length;
                $("span[id$='labelUjTetelekSzamaDb']").text(countUj.toString());
            });
        }
    </script>

    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <div>
        <asp:Panel ID="MainPanel" runat="server">
            <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
            <table cellpadding="0" cellspacing="0" width="90%">
                <tr>
                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                        <asp:Panel ID="IraIktatokonyvekListPanel" runat="server" Visible="false">
                            <asp:GridView ID="IraIktatoKonyvekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                OnRowDataBound="IraIktatoKonyvekGridView_RowDataBound" AllowSorting="False" AutoGenerateColumns="False"
                                DataKeyNames="Id">
                                <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                <HeaderStyle CssClass="GridViewHeaderStyle" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Lez�rand�">
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <%-- <HeaderTemplate>
                                    <div class="DisableWrap">
                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                    &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                    </div>
                                </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="K�vetkez� �vre l�trehoz">
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbUjIktatoKonyv" runat="server" CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                Visible="false" OnClientClick="return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Ev" HeaderText="�v" SortExpression="Ev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Iktatohely" HeaderText="Iktat�hely" SortExpression="Iktatohely">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MegkulJelzes" HeaderText="Egyedi azonos�t�" SortExpression="MegkulJelzes">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UtolsoFoszam" HeaderText="Utols� f�sz�m" SortExpression="UtolsoFoszam">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="LezarasDatuma" HeaderText="Iktat�k�nyv lez�r�s�nak d�tuma" SortExpression="LezarasDatuma">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IKT_SZAM_OSZTAS" HeaderText="Ikt.sz�m oszt�s" SortExpression="IKT_SZAM_OSZTAS">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Azonosito" HeaderText="Azonos�t�" SortExpression="Azonosito">
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <%-- BEGIN: ellen�rz�shez kellenek, nem l�that�k  --%>
                                    <asp:BoundField DataField="IktatoErkezteto" HeaderText="" SortExpression="IktatoErkezteto">
                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="60px" />
                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Zarolo_id" HeaderText="" SortExpression="Zarolo_id">
                                        <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" Width="60px" />
                                        <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    </asp:BoundField>
                                    <%-- END: ellen�rz�shez kellenek, nem l�that�k  --%>
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                        <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="labelUjTetelekSzama" Text="L�trehozand� t�telek sz�ma: " runat="server" />
                        <asp:Label ID="labelUjTetelekSzamaDb" Text="0" runat="server" />
                    </td>
                </tr>
            <tr>
            <td>
            <br />
                <asp:Panel ID="Panel_Warning_IktatoKonyv" runat="server" Visible="false">                
                <asp:Label ID="Label_Warning_IktatoKonyv" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                <br />
                </asp:Panel>
                <br />                
            </td>
            </tr>
                <tr>
                    <td>
                        <eUI:eFormPanel ID="EFormPanel_Warning_General" runat="server">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel_Warning_General" runat="server">
                                            <asp:Label ID="Label_Warning_General" runat="server" Text="Label" style="font-weight:bold;"></asp:Label>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </eUI:eFormPanel>
                        <br />
                        <br />
                        <uc2:FormFooter ID="FormFooter1" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
