﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IraIktatokonyvekLezarasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Startup = "";

    private UI ui = new UI();

    private String IraIktatoKonyvekId = "";  // id-k összefûzve

    private String[] IraIktatoKonyvekArray;

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private const String FunkcioKod_IktatoKonyvLezaras = "IktatoKonyv" + CommandName.Lezaras;
    private const String FunkcioKod_ErkeztetoKonyvLezaras = "ErkeztetoKonyv" + CommandName.Lezaras;

    public string maxTetelszam = "0";    

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        Startup = Page.Request.QueryString.Get(QueryStringVars.Startup);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        // Jogosultságellenõrzés:
        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                if (Startup == Constants.Startup.FromIktatoKonyvek)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IktatoKonyvLezaras);
                }
                else if (Startup == Constants.Startup.FromErkeztetoKonyvek)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_ErkeztetoKonyvLezaras);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IktatoKonyvLezaras);
                }
                break;
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IktatokonyvId)))
        {
            if (ViewState["SelectedIktatokonyvIds"] != null)
            {
                IraIktatoKonyvekId = ViewState["SelectedIktatokonyvIds"].ToString();
            }
            else if (Session["SelectedIktatokonyvIds"] != null)
            {
                IraIktatoKonyvekId = Session["SelectedIktatokonyvIds"].ToString();
                ViewState["SelectedIktatokonyvIds"] = IraIktatoKonyvekId;
                Session.Remove("SelectedIktatokonyvIds");
            }
        }
        else IraIktatoKonyvekId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);


        if (!String.IsNullOrEmpty(IraIktatoKonyvekId))
        {
            IraIktatoKonyvekArray = IraIktatoKonyvekId.Split(',');
        }
        else
        {

        }

        if (!IsPostBack)
        {
            LoadFormComponents();
        }

        if (Startup == Constants.Startup.FromErkeztetoKonyvek)
        {
            FormHeader1.HeaderTitle = Resources.Form.IraErkeztetoKonyvekLezarasFormHeaderTitle;
            Label_Warning_General.Text = Resources.Form.UI_Warning_LezartErkeztetokonyvbeErkeztetesNemMegengedett;
        }
        else if (Startup == Constants.Startup.FromIktatoKonyvek)
        {
            FormHeader1.HeaderTitle = Resources.Form.IraIktatoKonyvekLezarasFormHeaderTitle;
            Label_Warning_General.Text = Resources.Form.UI_Warning_LezartIktatokonybeIktatasNemMegengedett;
        }
        else
        {
            // default: iktatókönyv mód
            FormHeader1.HeaderTitle = Resources.Form.IraIktatoKonyvekLezarasFormHeaderTitle;
            Label_Warning_General.Text = Resources.Form.UI_Warning_LezartIktatokonybeIktatasNemMegengedett;
        }

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } ";
        FormFooter1.ImageButton_Save.OnClientClick += " $get('" + CustomUpdateProgress1.UpdateProgress.ClientID + "').style.display = 'block'; ";

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        int cntKijelolt_Lezaras = ui.GetGridViewSelectedRows(IraIktatoKonyvekGridView, FormHeader1.ErrorPanel, null).Count;
        int cntKijelolt_Masolas = ui.GetGridViewSelectedAndExecutableRows(IraIktatoKonyvekGridView, "cbUjIktatoKonyv", FormHeader1.ErrorPanel, null).Count;

        labelTetelekSzamaDb.Text = cntKijelolt_Lezaras.ToString();
        labelUjTetelekSzamaDb.Text = cntKijelolt_Masolas.ToString();
    }


    private void LoadFormComponents()
    {

        #region IktatoKonyvLezaras
        if (String.IsNullOrEmpty(IraIktatoKonyvekId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {
            IraIktatokonyvekListPanel.Visible = true;
            FillIktatoKonyvekGridView();

        }
        #endregion

    }

    private void SetGridViewHeaderAndVisibility(string Startup)
    {
        switch (Startup)
        {
            case Constants.Startup.FromErkeztetoKonyvek:
                {
                    int indexIktatohely = UI.GetGridViewColumnIndex(IraIktatoKonyvekGridView, "Iktatohely");
                    int indexUtolsoFoszam = UI.GetGridViewColumnIndex(IraIktatoKonyvekGridView, "UtolsoFoszam");
                    int indexMegkulJelzes = UI.GetGridViewColumnIndex(IraIktatoKonyvekGridView, "MegkulJelzes");
                    int indexIktatoszamOsztas = UI.GetGridViewColumnIndex(IraIktatoKonyvekGridView, "IKT_SZAM_OSZTAS");

                    if (indexUtolsoFoszam > -1)
                    {
                        IraIktatoKonyvekGridView.Columns[indexUtolsoFoszam].HeaderText = Resources.Form.UI_IktatokonyvLezaras_ErkeztetokonyvUtolsoFoszam;
                    }
                    if (indexMegkulJelzes > -1)
                    {
                        IraIktatoKonyvekGridView.Columns[indexMegkulJelzes].HeaderText = Resources.Form.UI_IktatokonyvLezaras_ErkeztetokonyvMegkulJelzes;
                    }

                    if (indexIktatohely > -1)
                    {
                        IraIktatoKonyvekGridView.Columns[indexIktatohely].Visible = false;
                    }
                    if (indexIktatoszamOsztas > -1)
                    {
                        IraIktatoKonyvekGridView.Columns[indexIktatoszamOsztas].Visible = false;
                    }
                }
                break;
            default:
                break;

        }
    }


    private void FillIktatoKonyvekGridView()
    {
        DataSet ds = IktatoKonyvekGridViewBind();

        // ellenõrzés:
        if (ds != null && ds.Tables[0].Rows.Count != IraIktatoKonyvekArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        // Van-e olyan rekord, ami nem zárható le? 
        // CheckBoxok vizsgálatával:
        int count_lezarhatok = UI.GetGridViewSelectedCheckBoxesCount(IraIktatoKonyvekGridView, "check");

        int count_NEMlezarhatok = IraIktatoKonyvekArray.Length - count_lezarhatok;

        if (count_NEMlezarhatok > 0)
        {
            Label_Warning_IktatoKonyv.Text = String.Format(Resources.List.UI_Iktatokonyv_NemZarhatoLeWarning, count_NEMlezarhatok);
            Panel_Warning_IktatoKonyv.Visible = true;
        }
    }


    protected DataSet IktatoKonyvekGridViewBind()
    {
        if (IraIktatoKonyvekArray != null && IraIktatoKonyvekArray.Length > 0)
        {
            SetGridViewHeaderAndVisibility(Startup);

            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
            // keresési objektum beállítása: szûrés az Id-kra:
            //for (int i = 0; i < IraIktatoKonyvekArray.Length; i++)
            //{
            //    if (i == 0)
            //    {
            //        search.Id.Value = "'" + IraIktatoKonyvekArray[i] + "'";
            //    }
            //    else
            //    {
            //        search.Id.Value += ",'" + IraIktatoKonyvekArray[i] + "'";
            //    }
            //}
            search.Id.In(IraIktatoKonyvekArray);

            if (Startup == Constants.Startup.FromErkeztetoKonyvek)
            {
                search.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Erkezteto);
            }
            else if (Startup == Constants.Startup.FromIktatoKonyvek)
            {
                search.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Iktato);
            }
            else
            {
                // default mód: iktatókönyv
                search.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Iktato);
            }

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(IraIktatoKonyvekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void IraIktatoKonyvekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        IktatoKonyvek.IraIktatokonyvekGridView_RowDataBound_CheckLezaras(e, Page);
    }


    public override void Load_ComponentSelectModul()
    {
        //if (compSelector == null) { return; }
        //else
        //{
        //    compSelector.Enabled = true;

        //    compSelector.Add_ComponentOnClick(requiredTextBoxUserNev);

        //    FormFooter1.SaveEnabled = false;

        //}
    }


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if ((Startup == Constants.Startup.FromIktatoKonyvek
                 && FunctionRights.GetFunkcioJog(Page, FunkcioKod_IktatoKonyvLezaras))
                || (Startup == Constants.Startup.FromErkeztetoKonyvek
                    && FunctionRights.GetFunkcioJog(Page, FunkcioKod_ErkeztetoKonyvLezaras))
                )
            {
                List<string> selectedItemsList_Lezaras = ui.GetGridViewSelectedRows(IraIktatoKonyvekGridView, FormHeader1.ErrorPanel, null);

                int selectedIds = selectedItemsList_Lezaras.Count;
                if (selectedIds > int.Parse(maxTetelszam))
                {
                    string javaS = "alert ('{alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "'); return false; } ";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                }
                else if (selectedIds == 0)
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, "", Resources.Error.UINoSelectedRow);
                    return;
                }

                if (String.IsNullOrEmpty(IraIktatoKonyvekId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    // Lezárás

                    EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    List<string> selectedItemsList_Masolas = ui.GetGridViewSelectedAndExecutableRows(IraIktatoKonyvekGridView, "cbUjIktatoKonyv", FormHeader1.ErrorPanel, null);

                    String[] IraIktatoKonyvekIds_Lezaras = selectedItemsList_Lezaras.ToArray();
                    String[] IraIktatoKonyvekIds_Masolas = selectedItemsList_Masolas.ToArray();

                    string IktatoErkezteto = "";
                    if (Startup == Constants.Startup.FromErkeztetoKonyvek)
                    {
                        IktatoErkezteto = Constants.IktatoErkezteto.Erkezteto;
                    }
                    else if (Startup == Constants.Startup.FromIktatoKonyvek)
                    {
                        IktatoErkezteto = Constants.IktatoErkezteto.Iktato;
                    }

                    Result result = service.LezarasMasolas_Tomeges(execParam, IraIktatoKonyvekIds_Lezaras, IraIktatoKonyvekIds_Masolas, IktatoErkezteto, null);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        //CERTOP: Iktatókönyv lezárásánál az irattári tételeket menteni hozzá
                        Contentum.eRecord.BaseUtility.IrattariTetelekExport _IrattariTetelekExport = new Contentum.eRecord.BaseUtility.IrattariTetelekExport(Page);
                        Result resultExport = _IrattariTetelekExport.Export(IraIktatoKonyvekIds_Lezaras);

                        if (resultExport.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultExport);
                        }
                        else
                        {

                            string alairasStoreInited = "";
                            Result resultSign = Contentum.eUtility.AlairasokUtility.SignAndSavetoDocStore(Page, null, IraIktatoKonyvekIds_Lezaras, out alairasStoreInited, true);

                            if (resultSign.IsError || !String.IsNullOrEmpty(alairasStoreInited))
                            {
                                if (resultSign.IsError)
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultSign);
                                }
                                else
                                {
                                    Result fakeResult = new Result();
                                    fakeResult.ErrorMessage = "Failed to initialize certificate store: " + alairasStoreInited;
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, fakeResult);
                                }
                            }
                            else
                            {
                                if (IraIktatoKonyvekIds_Lezaras.Length > 0)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, IraIktatoKonyvekIds_Lezaras[0]);
                                }
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                                MainPanel.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(IraIktatoKonyvekGridView, GetCheckBoxNamesAndBoolCheckIfFailedPairs(), result);
                    }

                }

            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }



    protected Pair[] GetCheckBoxNamesAndBoolCheckIfFailedPairs()
    {
        List<Pair> listOfPairs = new List<Pair>();
        listOfPairs.Add(new Pair("check", false));
        listOfPairs.Add(new Pair("cbUjIktatoKonyv", false));

        return listOfPairs.ToArray();
    }


}
