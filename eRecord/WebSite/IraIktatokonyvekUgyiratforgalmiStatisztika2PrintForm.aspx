﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraIktatokonyvekUgyiratforgalmiStatisztika2PrintForm.aspx.cs" Inherits="IraIktatokonyvekUgyiratforgalmiStatisztika2PrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc3" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel id="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <contenttemplate>
            <eUI:eErrorPanel id="EErrorPanel1" runat="server" Visible="false">
                </eUI:eErrorPanel>
            </contenttemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,UgyiratforgalmiStatisztikaFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label5" runat="server" Text="Dátum:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <uc3:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1"
                                    runat="server" Validate="false" ValidateDateFormat="true"></uc3:DatumIntervallum_SearchCalendarControl>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

