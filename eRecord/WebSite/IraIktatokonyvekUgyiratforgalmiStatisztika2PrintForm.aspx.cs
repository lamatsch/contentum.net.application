﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class IraIktatokonyvekUgyiratforgalmiStatisztika2PrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        //DatumCalendarControl1.Text = System.DateTime.Today.ToString();
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            //string _date = DatumCalendarControl1.Text.Replace(" ", "").Remove(10).Replace(".", "-");

            string _KezdDate = "";
            string _VegeDate = "";

            if (string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumKezd))
            {
                _KezdDate = "1900.01.01";
            }
            else
            {
                _KezdDate = DatumIntervallum_SearchCalendarControl1.DatumKezd.Replace(" ", "").Remove(10);
            }

            if (string.IsNullOrEmpty(DatumIntervallum_SearchCalendarControl1.DatumVege))
            {
                _VegeDate = "4700.12.31";
            }
            else
            {
                _VegeDate = DatumIntervallum_SearchCalendarControl1.DatumVege.Replace(" ", "").Remove(10);
            }

            string KezdDate = _KezdDate.Replace(".", "-") + " 00:00:00";
            string VegeDate = _VegeDate.Replace(".", "-") + " 23:59:59";
            //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

            //string orderby = erec_UgyUgyiratokSearch.OrderBy;
            string executor = execParam.Felhasznalo_Id;
            string szervezet = execParam.FelhasznaloSzervezet_Id;

            Query query = new Query();
            query.BuildFromBusinessDocument(search);

            //Where törlése a Session-ből
            if (Session["Where"] != null)
            {
                Session["Where"] = null;
            }

            Session["Where"] = query.Where;
            string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "DateFrom=" + KezdDate + '&' + "DateTo=" + VegeDate + '&' + "RiportType=2" + '&' + "Sztorno=false";

            //Meghívjuk az IraIktatokonyvekUgyiratforgalmiStatisztikaSSRSPrintForm.aspx nyomtatóoldalt, .
            string js = "javascript:window.open('UgyiratForgalmiStatisztikaPrintFormSSRS.aspx?" + queryString.Trim() + "')";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyiratForgalmiStatisztikaPrintFormSSRS", js, true);


        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
