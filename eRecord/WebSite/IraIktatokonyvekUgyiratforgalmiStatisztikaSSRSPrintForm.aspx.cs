using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public class UgyiratForgalmiStatReportViewerCredentials : IReportServerCredentials
{
    [DllImport("advapi32.dll", SetLastError = true)]
    public extern static bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public extern static bool CloseHandle(IntPtr handle);

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
        int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);

    public UgyiratForgalmiStatReportViewerCredentials()
    {
    }

    public UgyiratForgalmiStatReportViewerCredentials(string username)
    {
        this.Username = username;
    }


    public UgyiratForgalmiStatReportViewerCredentials(string username, string password)
    {
        this.Username = username;
        this.Password = password;
    }


    public UgyiratForgalmiStatReportViewerCredentials(string username, string password, string domain)
    {
        this.Username = username;
        this.Password = password;
        this.Domain = domain;
    }


    public string Username
    {
        get
        {
            return this.username;
        }
        set
        {
            string username = value;
            if (username.Contains("\\"))
            {
                this.domain = username.Substring(0, username.IndexOf("\\"));
                this.username = username.Substring(username.IndexOf("\\") + 1);
            }
            else
            {
                this.username = username;
            }
        }
    }
    private string username;



    public string Password
    {
        get
        {
            return this.password;
        }
        set
        {
            this.password = value;
        }
    }
    private string password;


    public string Domain
    {
        get
        {
            return this.domain;
        }
        set
        {
            this.domain = value;
        }
    }
    private string domain;




    #region IReportServerCredentials Members

    public bool GetBasicCredentials(out string basicUser, out string basicPassword, out string basicDomain)
    {
        basicUser = username;
        basicPassword = password;
        basicDomain = domain;
        return username != null && password != null && domain != null;
    }

    public bool GetFormsCredentials(out string formsUser, out string formsPassword, out string formsAuthority)
    {
        formsUser = username;
        formsPassword = password;
        formsAuthority = domain;
        return username != null && password != null && domain != null;

    }

    public bool GetFormsCredentials(out Cookie authCookie,
  out string user, out string password, out string authority)
    {
        authCookie = null;
        user = password = authority = null;
        return false;  // Not implemented
    }


    public WindowsIdentity ImpersonationUser
    {
        get
        {

            string[] args = new string[3] { this.Domain.ToString(), this.Username.ToString(), this.Password.ToString() };
            IntPtr tokenHandle = new IntPtr(0);
            IntPtr dupeTokenHandle = new IntPtr(0);

            const int LOGON32_PROVIDER_DEFAULT = 0;
            //This parameter causes LogonUser to create a primary token.
            const int LOGON32_LOGON_INTERACTIVE = 2;
            const int SecurityImpersonation = 2;

            tokenHandle = IntPtr.Zero;
            dupeTokenHandle = IntPtr.Zero;
            try
            {
                // Call LogonUser to obtain an handle to an access token.
                bool returnValue = LogonUser(args[1], args[0], args[2],
                    LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                    ref tokenHandle);

                if (false == returnValue)
                {
                    Console.WriteLine("LogonUser failed with error code : {0}",
                        Marshal.GetLastWin32Error());
                    return null;
                }

                // Check the identity.
                System.Diagnostics.Trace.WriteLine("Before impersonation: "
                    + WindowsIdentity.GetCurrent().Name);


                bool retVal = DuplicateToken(tokenHandle, SecurityImpersonation, ref dupeTokenHandle);
                if (false == retVal)
                {
                    CloseHandle(tokenHandle);
                    Console.WriteLine("Exception in token duplication.");
                    return null;
                }


                // The token that is passed to the following constructor must
                // be a primary token to impersonate.
                WindowsIdentity newId = new WindowsIdentity(dupeTokenHandle);
                WindowsImpersonationContext impersonatedUser = newId.Impersonate();


                // Free the tokens.
                if (tokenHandle != IntPtr.Zero)
                    CloseHandle(tokenHandle);
                if (dupeTokenHandle != IntPtr.Zero)
                    CloseHandle(dupeTokenHandle);

                // Check the identity.
                System.Diagnostics.Trace.WriteLine("After impersonation: "
                    + WindowsIdentity.GetCurrent().Name);

                return newId;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred. " + ex.Message);
            }

            return null;
        }
    }

    public ICredentials NetworkCredentials
    {
        get
        {
            return null;  // Not using NetworkCredentials to authenticate.
        }
    }


    #endregion
}

public partial class IraIktatokonyvekUgyiratforgalmiStatisztikaSSRSPrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            DatumIntervallum_SearchCalendarControl1.DatumKezd = System.DateTime.Now.Year.ToString() + ".01.01.";
            DatumIntervallum_SearchCalendarControl1.DatumVege = System.DateTime.Now.ToString();

            UgyiratForgalmiStatReportViewerCredentials rvc = new UgyiratForgalmiStatReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ImageButton_Refresh.Click += new ImageClickEventHandler(ImageButton_Refresh_Click);
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }

    protected void ImageButton_Refresh_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewer1.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewer1.ServerReport.Refresh();
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "KezdDate":
                            ReportParameters[i].Values.Add(DatumIntervallum_SearchCalendarControl1.DatumKezd.ToString().Remove(10).Replace(".", "-") + " 00:00:00:000");
                            break;
                        case "VegeDate":
                            ReportParameters[i].Values.Add(DatumIntervallum_SearchCalendarControl1.DatumVege.ToString().Remove(10).Replace(".", "-") + " 23:59:59:000");
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add("((((EREC_IraIktatoKonyvek.ErvKezd <= getdate()) and (EREC_IraIktatoKonyvek.ErvVege >= getdate()))))");
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(" ");
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add("D8339433-41C0-4452-B489-CE7DAAB06938");
                            break;
                        case "OrderBy":
                            ReportParameters[i].Values.Add(" order by  EREC_IraIktatoKonyvek.Iktatohely,EREC_IraIktatoKonyvek.Ev");
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(" ");
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
