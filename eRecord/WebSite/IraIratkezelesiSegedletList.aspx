<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IraIratkezelesiSegedletList.aspx.cs" Inherits="IraIratkezelesiSegedletList" Title="Iratkezel�si Seg�dletek lek�rdez�se" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/JogosultakSubListHeader.ascx" TagName="JogosultakSubListHeader" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/JogosultakTabPage.ascx" TagName="JogosultakTabPage" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />


    <%--HiddenFields--%>

    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="IraIktatoKonyvekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="IraIratkezelesiSegedletekUpdatePanel" runat="server" OnLoad="IraIratkezelesiSegedletekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="IraIktatoKonyvekCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="IraIktatoKonyvekCPEButton" CollapseControlID="IraIktatoKonyvekCPEButton"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="IraIktatoKonyvekCPEButton"
                            ExpandedSize="0" ExpandedText="K�ldem�nyek list�ja" CollapsedText="K�ldem�nyek list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="IraIratkezelesiSegedletekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="IraIratkezelesiSegedletekGridView_RowCommand" OnPreRender="IraIratkezelesiSegedletekGridView_PreRender"
                                            OnSorting="IraIratkezelesiSegedletekGridView_Sorting" OnRowDataBound="IraIratkezelesiSegedletekGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage"
                                                    ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="Ev" HeaderText="�v" SortExpression="Ev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Iktatohely" HeaderText="Iktat�hely" SortExpression="Iktatohely">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MegkulJelzes" HeaderText="Egyedi&nbsp;azonos�t�si lehet�s�g" SortExpression="MegkulJelzes">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UtolsoFoszam" HeaderText="Utols�&nbsp;f�sz�m" SortExpression="UtolsoFoszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="LezarasDatuma" HeaderText="Lez�r�s&nbsp;d�tuma" SortExpression="LezarasDatuma">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>

                                                <%--<asp:TemplateField>
                            <HeaderStyle  CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <HeaderTemplate>
                                <asp:LinkButton ID="linkTitkos" runat="server">Nem nyilv�nos</asp:LinkButton>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbTitkos" runat="server" Enabled="false"/>
                            </ItemTemplate>
                        </asp:TemplateField>    --%>
                                                <%--<asp:BoundField DataField="Titkos" HeaderText="Nem nyilv�nos" SortExpression="Titkos">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>--%>

                                                <asp:BoundField DataField="IKT_SZAM_OSZTAS" HeaderText="Ikt.sz�m&nbsp;oszt�s" SortExpression="IKT_SZAM_OSZTAS">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Azonosito" HeaderText="Azonos�t�" SortExpression="Azonosito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%-- bernat.laszlo added: �rkeztet� k�nyv st�tusza --%>
                                                <asp:BoundField DataField="Statusz" HeaderText="St�tusz" SortExpression="Statusz">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%-- bernat.laszlo eddig--%>
                                                <%--CR3355--%>
                                                <asp:BoundField DataField="KezelesTipusa" HeaderText="Kezel�s t�pusa" SortExpression="KezelesTipusa" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="ITSZ" SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Terjedelem" HeaderText="Terjedelem" SortExpression="Terjedelem" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SelejtezesDatuma" HeaderText="Selejtez�s d�tuma" SortExpression="SelejtezesDatuma" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>

                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="IrattariTetelekXMLLink" Title="Iratt�ri t�telek"
                                                            runat="server" ImageUrl="~/images/hu/egyeb/xml.gif" NavigateUrl='<%#"~/IraIktatoKonyvekList.aspx?Command=IrattariTetelekXML&Id=" + Eval("Id") %>'
                                                            Visible='<%#!String.IsNullOrEmpty(Eval("Note").ToString()) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2"></td>
        </tr>
        <tr runat="server" id="DetailRow">
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="UpdatePanel_Detail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left" colspan="2">

                                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                        TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                        CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                        AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                        ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                                    </ajaxToolkit:CollapsiblePanelExtender>

                                    <asp:Panel ID="Panel8" runat="server">
                                        <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%"
                                            OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                            OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">

                                            <ajaxToolkit:TabPanel ID="CsoportokTabPanel" runat="server" TabIndex="0">
                                                <HeaderTemplate>
                                                    <asp:UpdatePanel ID="LabelUpdatePanelIdeIktatok" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Label ID="Header1" runat="server" Text="Ide iktat�k"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </HeaderTemplate>
                                                <ContentTemplate>
                                                    <asp:UpdatePanel ID="CsoportokUpdatePanel" runat="server" OnLoad="CsoportokUpdatePanel_Load">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="CsoportokPanel" runat="server" Visible="false" Width="100%">
                                                                <uc1:SubListHeader ID="CsoportokSubListHeader" runat="server" />
                                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                            <ajaxToolkit:CollapsiblePanelExtender ID="CsoportokCPE" runat="server" TargetControlID="Panel2"
                                                                                CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                AutoExpand="false" ExpandedSize="0">
                                                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                                            <asp:Panel ID="Panel2" runat="server">
                                                                                <asp:GridView ID="CsoportokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                                    AllowSorting="True" AutoGenerateColumns="false" OnSorting="CsoportokGridView_Sorting"
                                                                                    OnPreRender="CsoportokGridView_PreRender" OnRowCommand="CsoportokGridView_RowCommand"
                                                                                    DataKeyNames="Id">
                                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                            <HeaderTemplate>
                                                                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                &nbsp;&nbsp;
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                            <HeaderStyle Width="25px" />
                                                                                            <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:CommandField>
                                                                                        <asp:BoundField DataField="Csoport_Iktathat_Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            SortExpression="Csoport_Iktathat_Nev" HeaderStyle-Width="250px" />
                                                                                        <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s&nbsp;ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="IrattariTetelekTabPanel" runat="server" TabIndex="1">
                                                <HeaderTemplate>
                                                    <asp:UpdatePanel ID="LabelUpdatePanelIrattariTetelek" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Label ID="headerIrattariTetelek" runat="server" Text="Iratt�ri t�telek"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </HeaderTemplate>
                                                <ContentTemplate>
                                                    <asp:UpdatePanel ID="IrattariTetelekUpdatePanel" runat="server" OnLoad="IrattariTetelekUpdatePanel_Load">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="IrattariTetelekPanel" runat="server" Visible="true" Width="100%">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:ImageButton ID="ImageButtonAddAllIrattariTetel" runat="server" ImageUrl="~/images/hu/trapezgomb/osszes_trap.jpg"
                                                                                onmouseover="swapByName(this.id,'osszes_trap2.jpg')" onmouseout="swapByName(this.id,'osszes_trap.jpg')"
                                                                                CommandName="All" CausesValidation="false" ToolTip="�sszes �rv�nyes iratt�ri t�telsz�m hozz�rendel�se" />
                                                                        </td>
                                                                        <td>
                                                                            <uc1:SubListHeader ID="IrattariTetelekSubListHeader" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                            <ajaxToolkit:CollapsiblePanelExtender ID="IrattariTetelekCPE" runat="server" TargetControlID="panelIrattariTetelekList"
                                                                                CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                AutoExpand="false" ExpandedSize="0">
                                                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                                            <asp:Panel ID="panelIrattariTetelekList" runat="server">
                                                                                <asp:GridView ID="IrattariTetelekGridView" runat="server" AutoGenerateColumns="False"
                                                                                    CellPadding="0" CellSpacing="0" GridLines="None" BorderWidth="1px" BorderStyle="Solid"
                                                                                    BorderColor="#e6e6e6" AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True"
                                                                                    OnPreRender="IrattariTetelekGridView_PreRender" OnRowCommand="IrattariTetelekGridView_RowCommand"
                                                                                    DataKeyNames="Id" OnSorting="IrattariTetelekGridView_Sorting" OnRowDataBound="IrattariTetelekGridView_RowDataBound">
                                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                            <HeaderTemplate>
                                                                                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="check" runat="server" AutoPostBack="false" Text='<%# Eval("Nev") %>'
                                                                                                    CssClass="HideCheckBoxText" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                                                            HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                                                            SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                            <HeaderStyle Width="25px" />
                                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:CommandField>
                                                                                        <asp:BoundField DataField="AgazatiJelek_Kod" HeaderText="�gazati&nbsp;jel" SortExpression="AgazatiJelek_Kod">
                                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="IrattariTetelszam" HeaderText="T�telsz�m" SortExpression="IrattariTetelszam">
                                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="300px" />
                                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="IRATTARI_JEL" HeaderText="Iratt�ri&nbsp;jel" SortExpression="IRATTARI_JEL">
                                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="MegorzesiIdo_Nev" HeaderText="Meg�rz�si&nbsp;id�" SortExpression="MegorzesiIdo_Nev">
                                                                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                        </asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                            <ajaxToolkit:TabPanel ID="JogosultakTabPanel" runat="server" TabIndex="2">
                                                <HeaderTemplate>
                                                    <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text="Jogosultak"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </HeaderTemplate>
                                                <ContentTemplate>
                                                    <asp:UpdatePanel ID="JogosultakUpdatePanel" runat="server" OnLoad="JogosultakUpdatePanel_Load">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="JogosultakPanel" runat="server" Visible="false" Width="100%">
                                                                <uc2:JogosultakSubListHeader ID="JogosultakSubListHeader" runat="server" />
                                                                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                            <ajaxToolkit:CollapsiblePanelExtender ID="JogosultakCPE" runat="server" TargetControlID="Panel4"
                                                                                CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                AutoExpand="false" ExpandedSize="0">
                                                                            </ajaxToolkit:CollapsiblePanelExtender>
                                                                            <asp:Panel ID="Panel4" runat="server">
                                                                                <asp:GridView ID="JogosultakGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                                    AllowSorting="True" AutoGenerateColumns="false" OnSorting="JogosultakGridView_Sorting"
                                                                                    OnPreRender="JogosultakGridView_PreRender" OnRowCommand="JogosultakGridView_RowCommand"
                                                                                    DataKeyNames="Id">
                                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                    <Columns>
                                                                                        <asp:TemplateField>
                                                                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                            <HeaderTemplate>
                                                                                                <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                                    runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                &nbsp;&nbsp;
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                            <HeaderStyle Width="25px" />
                                                                                            <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:CommandField>
                                                                                        <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            SortExpression="Nev" HeaderStyle-Width="250px" />
                                                                                        <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s&nbsp;ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </ajaxToolkit:TabPanel>

                                        </ajaxToolkit:TabContainer>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

