<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraIratkezelesiSegedletekForm.aspx.cs" Inherits="IraIratkezelesiSegedletekForm" Title="Iratkezel�si Seg�dletek karbantart�sa" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc9" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <%--CR3355--%>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
                        <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label7" runat="server" Text="�v:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc11:RequiredNumberBox ID="Ev_RequiredNumberBox" runat="server" MaxLength="4"></uc11:RequiredNumberBox>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label13" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label9" runat="server" Text="Iktat�hely egyedi azonos�t�ja:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="Iktatohely_RequiredTextBox" runat="server" MaxLength="20"
                                        Validate="true"></uc3:RequiredTextBox>
                                </td>
                            </tr>
                            
							<tr id="tr_AutoGeneratePartner_CheckBox" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    &nbsp;
                                    <asp:Label ID="Label2" runat="server" Text="Egyedi azonos�t�si lehet�s�g:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="MegkulJelzes_RequiredTextBox" runat="server" MaxLength="20"
                                        Validate="false"></uc3:RequiredTextBox>
                                </td>
                            </tr>
                            
							<tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;
                                    <asp:Label ID="Label10" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label16" runat="server" Text="Megnevez�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="Nev_RequiredTextBox" runat="server"></uc3:RequiredTextBox>
                                </td>
                            </tr>
                            <%--CR3355--%>
                             <tr class="urlapSor" runat="server" id="tr_KezelesTipusa">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label18" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label21" runat="server" Text="Kezel�s t�pusa:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButton ID="KezelesTipusa_RadioButton_E" runat="server" GroupName="KezelesTipusa_Selector" Text="Elektronikus" Checked="true"  
                                            OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                    <asp:RadioButton ID="KezelesTipusa_RadioButton_P" runat="server" GroupName="KezelesTipusa_Selector" Text="Pap�r"        Checked="false" 
                                            OnCheckedChanged="KezelesTipusa_RadioButton_CheckedChanged" AutoPostBack="True" />
                                </td>
                            </tr>
                            <tr id="tr_Tipus_dropdown" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label12" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label11" runat="server" Text="Utols� f�sz�m:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc11:RequiredNumberBox ID="UtolsoFoszam_RequiredNumberBox" runat="server" MaxLength="9">
                                    </uc11:RequiredNumberBox>
                                </td>
                            </tr>
                            <tr id="tr1" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label4" runat="server" Text="Ikt.sz�m oszt�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc10:KodtarakDropDownList ID="IktSzamOsztas_KodtarakDropDownList" runat="server">
                                    </uc10:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <%--<asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>--%><asp:Label
                                        ID="Label14" runat="server" Text="Azonos�t�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Azonosito_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label8" runat="server" Text="K�zponti iktat�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="KozpontiIktatas_CheckBox" runat="server" />
                                </td>
                            </tr>
                            <%--CR3355--%>
                           <tr class="urlapSor"  runat="server" id="tr_ITSZ">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Labe20" runat="server" Text="Alap�rt. iratt�ri t�telsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:IraIrattariTetelTextBox ID="Default_IraIrattariTetelTextBox1" runat="server" Validate="false" />
                            </td>
                        </tr>
                            <tr id="tr_terjedelem" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                     <asp:Label ID="Terj_ReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label17" runat="server" Text="Terjedelem:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc3:RequiredTextBox ID="Terjedelem_RequiredTextBox" runat="server" MaxLength="20"
                                        Validate="true" LabelRequiredIndicatorID="Terj_ReqStar"></uc3:RequiredTextBox>
                                </td>
                            </tr>
                            <%-- bernat.laszlo modified: Label  �t�rva , Orig: "�rv�nyess�g:"--%>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Nyitas_ReqStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label3" runat="server" Text="Nyit�s�nak d�tuma:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" >
                                    </uc6:ErvenyessegCalendarControl>
                                </td>
                            </tr>
                            <%-- bernat.laszlo eddig--%>
                            
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="labelLezarasDatuma" runat="server" Text="Lez�r�s�nak d�tuma:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:CalendarControl ID="LezarasDatuma_CalendarControl" runat="server" Validate="false" />
                                </td>
                            </tr>
                             <%--CR3355--%>
                              <tr class="urlapSor" id="tr_selejtezes" runat="server">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="label15" runat="server" Text="Selejtez�s d�tuma:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc5:CalendarControl ID="SelejtezesDatuma_CalendarControl" runat="server" Validate="false" />
                                </td>
                            </tr>
                            
                            <%-- bernat.laszlo added: Iktat�k�nyv st�tusza--%>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label19" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                    <asp:Label ID="Label20" runat="server" Text="Iktat� k�nyv st�tusza:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButton ID="Ikt_Konyv_Aktiv_RadioButton" runat="server" GroupName="Erk_Konyv_Status_Selector" Text="Akt�v" Checked="true"/>
                                    <asp:RadioButton ID="Ikt_Konyv_Inaktiv_RadioButton" runat="server" GroupName="Erk_Konyv_Status_Selector" Text="Inakt�v" />
                                </td>
                            </tr>
                            <%-- bernat.laszlo eddig--%>

                            <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label9" runat="server" Text="Alap�rt. iratt�ri t�telsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc12:IraIrattariTetelTextBox ID="Default_IraIrattariTetelTextBox1" runat="server" Validate="false" />
                            </td>
                        </tr>--%><%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label18" runat="server" Text="Olvas�sra jogosultak:" Enabled="False"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:CsoportTextBox ID="CsoportTextBox1" runat="server" Enabled="false" Validate="false" />
                            </td>
                        </tr>  --%></tbody></table>
            </ContentTemplate>
            </asp:UpdatePanel>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
