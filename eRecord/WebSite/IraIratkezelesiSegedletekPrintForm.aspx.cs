using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class IraIratkezelesiSegedletekPrintForm : Contentum.eUtility.UI.PageBase
{
    private string iktatokonyvId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        iktatokonyvId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);

        if (String.IsNullOrEmpty(iktatokonyvId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
    }

    private EREC_IraIratokSearch GetSearchObjectFromComponents()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch(true);

        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.IraIktatokonyv_Id.Value = iktatokonyvId;
        erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
        DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_IraIratokSearch.IktatasDatuma);
        
        return erec_IraIratokSearch;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            Response.Redirect("IraIratkezelesiSegedletekPrintFormSSRS.aspx?iktatokonyvId="+iktatokonyvId+"&idoszaktol="+DatumIntervallum_SearchCalendarControl1.DatumKezd+"&idoszakig="+DatumIntervallum_SearchCalendarControl1.DatumVege);
        }
    }
}
