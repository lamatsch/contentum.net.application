<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="IraIratokForm.aspx.cs" Inherits="IraIratokForm" Title="Iratok karbantart�sa" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/IraIratFormTab.ascx" TagName="IraIratFormTab"
    TagPrefix="tp1" %>
<%@ Register Src="eRecordComponent/TovabbiBekuldokTab.ascx" TagName="TovabbiBekuldokTab"
    TagPrefix="tp2" %>
<%@ Register Src="eRecordComponent/MellekletekTab.ascx" TagName="MellekletekTab"
    TagPrefix="tp3" %>
<%@ Register Src="eRecordComponent/FeljegyzesekTab.ascx" TagName="FeljegyzesekTab"
    TagPrefix="tp4" %>
<%@ Register Src="eRecordComponent/CsatolmanyokTab.ascx" TagName="CsatolmanyokTab"
    TagPrefix="tp5" %>
<%@ Register Src="eRecordComponent/CsatolasokTab.ascx" TagName="CsatolasokTab" TagPrefix="tp6" %>
<%@ Register Src="eRecordComponent/BontasTab.ascx" TagName="BontasTab" TagPrefix="tp7" %>
<%@ Register Src="eRecordComponent/TortenetTab.ascx" TagName="TortenetTab" TagPrefix="tp8" %>
<%@ Register Src="eRecordComponent/KapcsolatokTab.ascx" TagName="KapcsolatokTab"
    TagPrefix="tp9" %>
<%@ Register Src="eRecordComponent/FeladatokTab.ascx" TagName="FeladatokTab" TagPrefix="tp10" %>
<%@ Register Src="eRecordComponent/AlairokTab.ascx" TagName="AlairokTab" TagPrefix="tp12" %>
<%@ Register Src="eRecordComponent/UgyiratTerkepTab.ascx" TagName="UgyiratTerkepTab"
    TagPrefix="tp12" %>
<%@ Register Src="eRecordComponent/JogosultakTab.ascx" TagName="JogosultakTab" TagPrefix="tp13" %>
<%@ Register Src="eRecordComponent/TargyszavakTab.ascx" TagName="TargyszavakTab" TagPrefix="tp14" %>
<%@ Register Src="eRecordComponent/ElintezesAdatok.ascx" TagName="ElintezesAdatok"
    TagPrefix="uc" %>
<%@ Register Src="eRecordComponent/MinositesFelulvizsgalatTab.ascx" TagName="MinositesFelulvizsgalatTab" TagPrefix="mf" %>
<%@ Register Src="~/eRecordComponent/IraIratJellemzokFormTab.ascx" TagName="IraIratJellemzokFormTab" TagPrefix="tp" %>

<%@ Register Src="~/eRecordComponent/eBeadvanyKuldesiAdatokTab.ascx" TagName="eBeadvanyKuldesiAdatokTab"
    TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/IratElosztoIvListaUserControl.ascx" TagPrefix="tp" TagName="IratElosztoIvListaUserControl" %>


<%@ Register Src="~/eRecordComponent/IratFizetesiInformaciok.ascx" TagName="IratFizetesiInformaciok" TagPrefix="tp" %>


<%@ Register Src="~/eRecordComponent/HatosagiStatisztikaTab.ascx" TagName="HatosagiStatisztikaTab" TagPrefix="uc_HS" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        @media screen and (min-width: 1100px) {
            .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
                width: 190px;
            }

            body, html {
                /*overflow: auto;*/
                /*overflow-y: scroll;*/
                overflow-x: hidden;
            }
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScripts/json2.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/libs/jquery/jquery.js" />
            <asp:ScriptReference Path="https://proxy.microsigner.com/esign/js/esign_v2.js" />
            <asp:ScriptReference Path="~/JavaScripts/Microsec.js" />
        </Scripts>
        <Services>
            <asp:ServiceReference Path="~/WrappedWebService/MicroSignerCallbackService.asmx" />
        </Services>
    </asp:ScriptManager>



    <asp:UpdatePanel ID="FormHeaderUpdatePanel" runat="server" UpdateMode="Conditional" RenderMode="Inline">
        <ContentTemplate>
            <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IratokFormHeaderTitle%>" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
     
    <table border="0" cellpadding="0" cellspacing="0" width="96%">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:Panel ID="IraIratFormPanel" runat="server">
                    <%-- OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                              OnClientActiveTabChanged="ActiveTabChanged" --%>
                    <ajaxToolkit:TabContainer ID="IraIratokTabContainer" runat="server" Width="100%"
                        OnActiveTabChanged="IraIratokTabContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged">
                        <ajaxToolkit:TabPanel ID="TabIratPanel" runat="server">
                            <HeaderTemplate>
                                <asp:Label ID="TabIratPanelHeader" runat="server" Text="Irat"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp1:IraIratFormTab ID="IratTab1" runat="server"></tp1:IraIratFormTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabIratJellemzoPanel" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                <asp:Label ID="Label3" runat="server" Text="Irat jellemz�k" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp:IraIratJellemzokFormTab ID="IraIratJellemzokFormTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabUgyiratTerkepPanel" runat="server" TabIndex="2">
                            <HeaderTemplate>
                                <asp:Label ID="Label1" runat="server" Text="�gyiratt�rk�p"/>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp12:UgyiratTerkepTab ID="UgyiratTerkepTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--<ajaxToolkit:TabPanel ID="TabOnkormanyzatiAdatokPanel" runat="server" TabIndex="1">
							<HeaderTemplate>
								<asp:Label ID="TabOnkormanyzatiAdatokPanelHeader" runat="server" Text="�nkorm�nyzati adatok"></asp:Label>
							</HeaderTemplate>
							<ContentTemplate>
								<tp11:OnkormanyzatTab ID="OnkormanyzatiAdatokTab1" runat="server"></tp11:OnkormanyzatTab>
							</ContentTemplate>
						</ajaxToolkit:TabPanel>--%>
                        <ajaxToolkit:TabPanel ID="TabMellekletekPanel" runat="server" TabIndex="3">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelIratElemek" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabMellekletekPanelHeader" runat="server" Text="Irat elemek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp3:MellekletekTab ID="MellekletekTab1" runat="server"></tp3:MellekletekTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabCsatolmanyokPanel" runat="server" TabIndex="4">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelCsatolmanyok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabCsatolmanyokPanelHeader" runat="server" Text="Csatolm�nyok"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp5:CsatolmanyokTab ID="CsatolmanyokTab1" runat="server"></tp5:CsatolmanyokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabTargyszavakPanel" runat="server" TabIndex="5">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelTargyszavak" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabTargyszavakPanelHeader" runat="server" Text="T�rgyszavak"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp14:TargyszavakTab ID="TargyszavakTab" runat="server"></tp14:TargyszavakTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabCsatolasPanel" runat="server">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelKapcsolatok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="Label5" runat="server" Text="Kapcsolatok" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <%-- Kapcsolatok --%>
                                <tp6:CsatolasokTab ID="CsatolasokTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabTortenetPanel" runat="server" TabIndex="7">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelTortenet" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabTortenetPanelHeader" runat="server" Text="T�rt�net"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp8:TortenetTab ID="TortenetTab1" runat="server"></tp8:TortenetTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabFeladatokPanel" runat="server" TabIndex="9">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="updatePanelFeladatokHeader" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabFeladatokPanelHeader" runat="server" Text="Feladatok/Kezel�si feljegyz�sek"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp10:FeladatokTab ID="FeladatokTab" runat="server"></tp10:FeladatokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabAlairokPanel" runat="server" TabIndex="10">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelAlairok" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="TabAlairokPanelHeader" runat="server" Text="Al��r�k"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp12:AlairokTab ID="AlairokTab" runat="server"></tp12:AlairokTab>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabJogosultakPanel" runat="server" TabIndex="6">
                            <HeaderTemplate>
                                <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="Label6" runat="server" Text="Jogosultak" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp13:JogosultakTab ID="JogosultakTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabElintezesAdatokPanel" runat="server" TabIndex="12">
                            <HeaderTemplate>
                                <asp:Label ID="Label2" runat="server" Text="Elint�z�s" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <uc:ElintezesAdatok ID="ElintezesAdatokTab" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabKuldesiAdatokPanel" runat="server" TabIndex="13">
                            <HeaderTemplate>
                                <asp:Label ID="LabelKuldesiAdatok" runat="server" Text="�zenet metaadatok" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <uc:eBeadvanyKuldesiAdatokTab ID="KuldesiAdatokTab" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel ID="TabPanelCimzettLista" runat="server" TabIndex="14">
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="C�mzettlista" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp:IratElosztoIvListaUserControl runat="server" ID="IratElosztoIvListaUserControlInstance" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanelMinositesFelulvizsgalat" runat="server" TabIndex="15">
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Min�s�t�s fel�lvizsg�lata" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <mf:MinositesFelulvizsgalatTab runat="server" ID="MinositesFelulvizsgalatTab" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabIratFizetesiInformaciokPanel" runat="server" TabIndex="12">
                            <HeaderTemplate>
                                <asp:Label ID="labelIratFizetesiInformaciok" runat="server" Text="Fizet�si inform�ci�k" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp:IratFizetesiInformaciok ID="IratFizetesiInformaciokTab" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                        <ajaxToolkit:TabPanel ID="TabPanel_HatosagiStatisztikaPanel" runat="server" TabIndex="16">
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="<%$Resources:Form,HatosagiStatisztikaTitle%>" />
                            </HeaderTemplate>
                            <ContentTemplate>
                                <uc_HS:HatosagiStatisztikaTab ID="HatosagiStatisztikaTabA" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>

                    </ajaxToolkit:TabContainer>

                </asp:Panel>
            </td>
        </tr>
    </table>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
