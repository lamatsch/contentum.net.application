using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class IraIratokForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    Contentum.eUtility.PageView pageView = null;

    private const string kcs_IRATMINOSITES = "IRAT_MINOSITES";

    // A sz�ban forg� irat objektum (megtekint�s, m�dos�t�s eset�n haszn�latos)
    private EREC_IraIratok obj_Irat = null;
    private EREC_UgyUgyiratdarabok obj_UgyiratDarab = null;
    private EREC_UgyUgyiratok obj_Ugyirat = null;
    private EREC_PldIratPeldanyok obj_elsoIratPeldany = null;

    protected void Page_PreInit(object sender, EventArgs e)
    {

        Command = Request.QueryString.Get(CommandName.Command);

        /// ha nincs megadva a Mode param�ter, akkor ha Command=New, BejovoIratIktatas-k�nt kezelj�k
        String Mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (String.IsNullOrEmpty(Mode) && Command == CommandName.New)
        {
            // default �rt�k a BejovoIratIktatas
            Mode = CommandName.BejovoIratIktatas;
        }

        #region Funkci�jog ellen�rz�s
        if (Command == CommandName.New)
        {
            if (Mode == CommandName.InfoszabIktatas)
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BelsoIratIktatas");
            }
            else if (Mode == CommandName.BelsoIratIktatas)
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BelsoIratIktatas");
            }
            else if (Mode == CommandName.KimenoEmailIktatas)
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "EmailIktatas");
            }
            else if (Mode == CommandName.ElokeszitettIratIktatas)
            {
                // K�tf�le funkci�joga lehet: MunkapeldanyBeiktatas_Bejovo vagy MunkapeldanyBeiktatas_Belso
                // ha egyikhez sincs joga:
                if (!FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Bejovo")
                    && !FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Belso"))
                {
                    FunctionRights.RedirectErrorPage(Page);
                }
            }
            else if (Mode == CommandName.AtIktatas)
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AtIktatas");
            }
            // CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
            else if (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MunkapeldanyBeiktatas_Bejovo");
            }
            else
            {
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "BejovoIratIktatas");
            }
        }
        else
        {
            // ha Modify m�dban pr�b�ljuk megnyitni, de csak View joga van, �tir�ny�tjuk, ha egyik sincs, hiba)
            if (Command == CommandName.Modify
                && !FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.Modify)
                && FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View))
            {
                string qs = Contentum.eUtility.Utils.GetViewRedirectQs(Page, null);

                Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
            }
            else
            {
                if (Command == CommandName.DesignView)
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                }
                else
                {
                    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IraIrat" + Command);
                }

            }
        }
        #endregion
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eUtility.PageView(Page, ViewState, true);

        JavaScripts.RegisterActiveTabChangedClientScript(Page, IraIratokTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(IraIratokTabContainer);



        // FormHeader be�ll�t�sa az IratTab1 -nek:
        IratTab1.FormHeader = FormHeader1;
        IratTab1.FormHeaderUpdatePanel = FormHeaderUpdatePanel;

        ElintezesAdatokTab.FormHeader = FormHeader1;

        // Template kezel�s csak CommandName.New eset�n
        if (Command == CommandName.New)
        {
            FormHeader1.TemplateObjectType = typeof(IktatasFormTemplateObject);
            FormHeader1.FormTemplateLoader1_Visibility = true;
        }

        // Template kezel�s CommandName.Modify eset�n csak a hat�s�gi adatokra vonatkoz�an

        if (Command == CommandName.Modify)
        {
            //FormHeader1.TemplateObjectType = typeof(HatosagiStatisztikaTargyszavakTemplate);
            //FormHeader1.FormTemplateLoader1_Visibility = false; // csak akkor lesz l�that�, ha az OnkormanyzatTab akt�v
        }

        //if (Command != CommandName.View)
        //{
        FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue=true;" +
      "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
 + "window.close();";
        //}


        /// ha nincs megadva a Mode param�ter, akkor ha Command=New, BejovoIratIktatas-k�nt kezelj�k
        String Mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (String.IsNullOrEmpty(Mode) && Command == CommandName.New)
        {
            // default �rt�k a BejovoIratIktatas
            Mode = CommandName.BejovoIratIktatas;
        }


        SetEnabledAllTabsByFunctionRights();

        if (Command == CommandName.New) // Iktat�s
        {

            // Ha uj rekorodot vesznek fel, akkor csak az elso tab aktiv, addig amig nem nyomnak mentest!
            SetEnabledAllTabs(false);
            TabIratPanel.Enabled = true;

            // Header c�m�nek be�ll�t�sa
            /// ha nincs megadva a Mode param�ter, akkor ha Command=New, BejovoIratIktatas-k�nt kezelj�k            

            if (Mode == CommandName.InfoszabIktatas)
            {
                FormHeader1.FullManualHeaderTitle = Resources.Form.BelsoIratIktatasaFormHeaderTitle;
            }
            else if (Mode == CommandName.BelsoIratIktatas)
            {
                bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
                if (isTUK)
                {
                    FormHeader1.FullManualHeaderTitle = Resources.Form.BelsoIratIktatasaFormHeaderTitle_TUK;
                }
                else
                    FormHeader1.FullManualHeaderTitle = Resources.Form.BelsoIratIktatasaFormHeaderTitle;
            }
            else if (Mode == CommandName.KimenoEmailIktatas)
            {
                FormHeader1.FullManualHeaderTitle = Resources.Form.KimenoEmailIktatasaHeaderTitle;
            }
            else if (Mode == CommandName.AtIktatas)
            {
                FormHeader1.FullManualHeaderTitle = Resources.Form.AtIktatasFormHeaderTitle;
            }
            else if (Mode == CommandName.ElokeszitettIratIktatas)
            {
                FormHeader1.FullManualHeaderTitle = Resources.Form.MunkaPeldanyBeiktatasaHeaderTitle;
            }
            // CR 3110: Iktat�s Munkap�ld�nyra Rendszerparam�ter alapj�n (BEJOVO_IRAT_MUNKAIRATBA_IKTATAS), CommandName.MunkapeldanyBeiktatas_Bejovo alapj�n
            else if (Mode == CommandName.MunkapeldanyBeiktatas_Bejovo)
            {
                // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
                FormHeader1.FullManualHeaderTitle = Resources.Form.SzignalasElokeszites_MunkaPeldanyBeiktatasaHeaderTitle;
                //FormHeader1.FullManualHeaderTitle = Resources.Form.MunkaPeldanyBeiktatasaHeaderTitle;
            }
            else
            {
                FormHeader1.FullManualHeaderTitle = Resources.Form.BejovoIratIktatasaFormHeaderTitle;
            }
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {

                if (!IsPostBack)
                {
                    CheckRights(id);
                    DisableTabsByMissingObjectRights(this.obj_Irat);
                }


            }
        }

        // Feliratkoz�s az objektum megv�ltoz�s�t jelz� esem�nyekre:
        IratTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        UgyiratTerkepTab1.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);
        AlairokTab.OnChangedObjectProperties += new EventHandler(CallBack_OnChangedObjectProperties);

        #region CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz
        if (Mode == CommandName.Alairas)
        {
            IratTab1.Active = false;
            IratTab1.ParentForm = Constants.ParentForms.IraIrat;
        }
        else
        {
            IratTab1.Active = true;
            IratTab1.ParentForm = Constants.ParentForms.IraIrat;
        }
        #endregion CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz


        UgyiratTerkepTab1.Active = false;
        UgyiratTerkepTab1.ParentForm = Constants.ParentForms.IraIrat;
        UgyiratTerkepTab1.ParentTabContainer = IraIratokTabContainer;

        MellekletekTab1.Active = false;
        MellekletekTab1.ParentForm = Constants.ParentForms.IraIrat;
        MellekletekTab1.TabHeader = TabMellekletekPanelHeader;

        #region CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz
        if (Mode == CommandName.Alairas)
        {
            CsatolmanyokTab1.Active = true;
            CsatolmanyokTab1.ParentForm = Constants.ParentForms.IraIrat;
            CsatolmanyokTab1.TabHeader = TabCsatolmanyokPanelHeader;
            CsatolmanyokTab1.Focus();
        }
        else
        {
            CsatolmanyokTab1.Active = false;
            CsatolmanyokTab1.ParentForm = Constants.ParentForms.IraIrat;
            CsatolmanyokTab1.TabHeader = TabCsatolmanyokPanelHeader;
        }
        #endregion CR3129 - Sz�ks�g lenne egy �j ikonra az al��r�shoz


        TargyszavakTab.Active = false;
        TargyszavakTab.ParentForm = Constants.ParentForms.IraIrat;
        TargyszavakTab.TabHeader = TabTargyszavakPanelHeader;

        CsatolasokTab1.Active = false;
        CsatolasokTab1.ParentForm = Constants.ParentForms.IraIrat;
        CsatolasokTab1.TabHeader = Label5;

        TortenetTab1.Active = false;
        TortenetTab1.ParentForm = Constants.ParentForms.IraIrat;
        TortenetTab1.TabHeader = TabTortenetPanelHeader;

        FeladatokTab.Active = false;
        FeladatokTab.ParentForm = Constants.ParentForms.IraIrat;
        FeladatokTab.TabHeader = TabFeladatokPanelHeader;

        AlairokTab.Active = false;
        AlairokTab.ParentForm = Constants.ParentForms.IraIrat;
        AlairokTab.TabHeader = TabAlairokPanelHeader;

        JogosultakTab1.Active = false;
        JogosultakTab1.ParentForm = Constants.ParentForms.IraIrat;
        JogosultakTab1.TabHeader = Label6;

        ElintezesAdatokTab.Active = false;
        ElintezesAdatokTab.ParentForm = Constants.ParentForms.IraIrat;

        IraIratJellemzokFormTab1.Active = false;
        IraIratJellemzokFormTab1.ParentForm = Constants.ParentForms.IraIrat;

        KuldesiAdatokTab.Active = false;
        KuldesiAdatokTab.ParentForm = Constants.ParentForms.IraIrat;

        //BLG 1722 Rendszerparameterek.IsTUK(Page);
        MinositesFelulvizsgalatTab.Active = Rendszerparameterek.IsTUK(Page);
        MinositesFelulvizsgalatTab.ParentForm = Constants.ParentForms.IraIrat;

        IratFizetesiInformaciokTab.Active = false;
        IratFizetesiInformaciokTab.ParentForm = Constants.ParentForms.IraIrat;

        //BUG 6380 - 1.
        if (!IsPostBack)
        {
            if (this.obj_Irat != null)
            {
                AlairokTab.Visible = this.obj_Irat.PostazasIranya != KodTarak.POSTAZAS_IRANYA.Bejovo;
                TabAlairokPanel.Enabled = AlairokTab.Visible;
            }
        }
        HatosagiStatisztikaTabA.Active = true;
        HatosagiStatisztikaTabA.ParentForm = Constants.ParentForms.IraIrat;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            // Default panel beallitasa
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);
            //if (!String.IsNullOrEmpty(selectedTab) && selectedTab == "Mellekletek")
            //{
            //    IraIratokTabContainer.ActiveTab = TabMellekletekPanel;
            //}
            if (!String.IsNullOrEmpty(selectedTab))
            {
                if (selectedTab.Equals(Constants.Tabs.HatosagiAdatokTab, StringComparison.InvariantCultureIgnoreCase) &&
                    TabPanel_HatosagiStatisztikaPanel != null && TabPanel_HatosagiStatisztikaPanel.Enabled && TabPanel_HatosagiStatisztikaPanel.Visible)
                {
                    selectedTab = TabPanel_HatosagiStatisztikaPanel.ID;
                }

                bool selectedTabFound = false;
                foreach (AjaxControlToolkit.TabPanel tab in IraIratokTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()) && tab.Enabled)
                    {
                        selectedTabFound = true;
                        IraIratokTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    IraIratokTabContainer.ActiveTab = TabIratPanel;
                }
            }
            else
            {
                IraIratokTabContainer.ActiveTab = TabIratPanel;
            }
            ActiveTabRefresh(IraIratokTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;

        #region BLG_612
        if (!IsPostBack)
        {
            if (this.obj_Irat != null)
            {
                IratElosztoIvListaUserControlInstance.InitializeWithIrat(this.obj_Irat.Id);
                IratElosztoIvListaUserControlInstance.LoadData();
            }
            else
            {
                string Id = Request.QueryString.Get(QueryStringVars.Id);
                if (!string.IsNullOrEmpty(Id))
                {
                    IratElosztoIvListaUserControlInstance.InitializeWithIrat(Id);
                    IratElosztoIvListaUserControlInstance.LoadData();
                }
            }
        }
        #endregion BLG_612

        if (Command == CommandName.New)
            HatosagiStatisztikaTabA.Visible = false;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //Iktat�s eset�n bez�r gomb elt�ntet�se
        if (Command == CommandName.New)
        {
            FormFooter1.ImageButton_Close.Visible = false;
        }
    }



    /// <summary>
    /// Ha v�ltozott valamilyen tulajdons�ga az iratnak, jogosults�gellen�rz�s
    /// </summary>    
    void CallBack_OnChangedObjectProperties(object sender, EventArgs e)
    {
        string id = Request.QueryString.Get(QueryStringVars.Id);
        CheckRights(id);
        DisableTabsByMissingObjectRights(this.obj_Irat);

        // a lek�rt objektumok tov�bbad�sa, hogy ott ne kelljen �jra lek�rni:
        IratTab1.obj_Irat = this.obj_Irat;
        IratTab1.obj_UgyiratDarab = this.obj_UgyiratDarab;
        IratTab1.obj_Ugyirat = this.obj_Ugyirat;
    }



    private void CheckRights(string id)
    {
        //char jogszint = Command == CommandName.View ? 'O' : 'I';

        // egyel�re ideiglenesen csak olvas�si jogszint ellen�rz�s
        // amiatt, hogyha valaki m�sn�l l�v� �gyiratba iktat egy iratot, nem kap k�l�n �r�si jogot az iratra az ACL-ben
        char jogszint = 'O';

        ExecParam ep = new ExecParam();
        EREC_IraIratokService service = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_IraIratokService();

        ExecParam eParam = UI.SetExecParamDefault(Page, new ExecParam());
        eParam.Record_Id = id;

        //Result result = service.GetWithRightCheck(eParam, jogszint);
        Result result = service.GetIratHierarchiaWithRightCheck(eParam, jogszint);
        if (!string.IsNullOrEmpty(result.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            IraIratFormPanel.Visible = false;
            return;
        }
        else
        {
            IratHierarchia iratHierarchia = (IratHierarchia)result.Record;

            //obj_Irat = (EREC_IraIratok)result.Record;                    
            obj_Irat = iratHierarchia.IratObj;
            obj_UgyiratDarab = iratHierarchia.UgyiratDarabObj;
            obj_Ugyirat = iratHierarchia.UgyiratObj;
        }

        #region �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa
        if (Command == CommandName.Modify || Command == CommandName.View)
        {
            bool bIratModosithato = false;
            String linkURL = String.Empty;

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            // �gyiratdarab �s �gyirat lek�r�se a st�tuszokhoz:
            if (obj_UgyiratDarab == null)
            {
                EREC_UgyUgyiratdarabokService service_ugyiratDarab = eRecordService.ServiceFactory.GetEREC_UgyUgyiratdarabokService();
                execParam.Record_Id = obj_Irat.UgyUgyIratDarab_Id;

                Result result_ugyiratDarabGet = service_ugyiratDarab.Get(execParam);
                if (!String.IsNullOrEmpty(result_ugyiratDarabGet.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratDarabGet);
                    IraIratFormPanel.Visible = false;
                    return;
                }
                else
                {
                    obj_UgyiratDarab = (EREC_UgyUgyiratdarabok)result_ugyiratDarabGet.Record;
                }
            }

            if (obj_Ugyirat == null)
            {
                EREC_UgyUgyiratokService service_ugyirat = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                execParam.Record_Id = obj_UgyiratDarab.UgyUgyirat_Id;

                Result result_ugyiratGet = service_ugyirat.Get(execParam);
                if (!String.IsNullOrEmpty(result_ugyiratGet.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_ugyiratGet);
                    IraIratFormPanel.Visible = false;
                    return;
                }
                else
                {
                    obj_Ugyirat = (EREC_UgyUgyiratok)result_ugyiratGet.Record;
                }
            }

            // els� iratp�ld�ny:
            if (obj_elsoIratPeldany == null)
            {
                EREC_PldIratPeldanyokService service_Pld = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                Result result_elsoPldGet = service_Pld.GetElsoIratPeldanyByIraIrat(UI.SetExecParamDefault(Page)
                    , id);
                if (result_elsoPldGet.IsError)
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result_elsoPldGet);
                    IraIratFormPanel.Visible = false;
                    return;
                }
                else
                {
                    obj_elsoIratPeldany = (EREC_PldIratPeldanyok)result_elsoPldGet.Record;
                }
            }

            #region Irat m�dos�that�-e
            //Iratok.Statusz iratStatusz = Iratok.GetAllapotById(id, execParam, FormHeader1.ErrorPanel);
            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_Irat, obj_elsoIratPeldany);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_UgyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_Ugyirat);
            ErrorDetails errorDetail;
            //#region CR3106 irat kiadm�nyozott st�tuszban csak megtekint�sre nyithat� meg �s olyankor nem lehet csatolm�nyt hozz� felt�lteni, pedig fontos lenne
            //if (FelhasznaloProfil.OrgKod(Page) != Constants.OrgKod.BOPMH)
            //    bIratModosithato = Iratok.Modosithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            //else if( FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH && iratStatusz.Allapot!=KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            //    bIratModosithato = Iratok.Modosithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            //else if (FelhasznaloProfil.OrgKod(Page) == Constants.OrgKod.BOPMH && iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
            //    bIratModosithato = true;
            //#endregion
            bIratModosithato = Iratok.Modosithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            #endregion Irat m�dos�that�-e

            // M�dos�that�s�g ellen�rz�se:
            if (Command == CommandName.Modify)
            {
                if (bIratModosithato == false)
                {
                    string qs = QueryStringVars.Id + "=" + id + "&"
                        + Contentum.eUtility.Utils.GetViewRedirectQs(Page, new string[] { QueryStringVars.Id });

                    // Nem m�dos�that� a rekord, �tir�ny�t�s a megtekint�s oldalra
                    if (!IsPostBack)
                    {
                        Response.Redirect(Page.Request.Url.AbsolutePath + "?" + qs);
                    }
                    else
                    {
                        UI.popupRedirect(Page, "IraIratokForm.aspx?" + qs);
                    }
                }

            }
            else if (Command == CommandName.View)
            {
                if (bIratModosithato == true)
                {
                    String funkcioJog = "IraIrat" + CommandName.Modify;
                    //jogosults�g ellen�rz�s
                    if (FunctionRights.GetFunkcioJog(Page, funkcioJog))
                    {
                        // Original QueryString without Command and Id and SelectedTab
                        string qsOriginalParts = QueryStringVars.Id + "=" + id + "&"
                            + Contentum.eUtility.Utils.GetQueryStringOriginalParts(Page, new string[] { QueryStringVars.Command, QueryStringVars.Id, QueryStringVars.SelectedTab });

                        linkURL = "IraIratokForm.aspx?"
                                                + QueryStringVars.Command + "=" + CommandName.Modify
                                                + "&" + qsOriginalParts
                                                + "&" + QueryStringVars.SelectedTab + "=" + "' + getActiveTab('" + IraIratokTabContainer.ClientID + "') + '";
                    }
                }

            }
            // ModifyLink be�ll�t�sa
            JavaScripts.RegisterSetLinkOnForm(Page, (String.IsNullOrEmpty(linkURL) ? false : true), linkURL, FormHeader1.ModifyLink);
        }
        #endregion �tir�ny�t�s View m�dba vagy ModifyLink be�ll�t�sa

    }

    #region Forms Tab

    protected void IraIratokTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        IratTab1.Active = false;
        UgyiratTerkepTab1.Active = false;
        MellekletekTab1.Active = false;
        CsatolmanyokTab1.Active = false;

        //TovabbiBekuldokTab1.Active = false;
        TargyszavakTab.Active = false;
        CsatolasokTab1.Active = false;
        //KapcsolatokTab1.Active = false;
        TortenetTab1.Active = false;
        //KapjakmegTab1.Active=false;
        FeladatokTab.Active = false;
        AlairokTab.Active = false;
        JogosultakTab1.Active = false;
        ElintezesAdatokTab.Active = false;
        IraIratJellemzokFormTab1.Active = false;
        KuldesiAdatokTab.Active = false;

        IratFizetesiInformaciokTab.Active = false;
        HatosagiStatisztikaTabA.Active = false;

        // Command == CommandName.New eset�n az iktat�si adatokra vonatkozik �s mindig l�that�
        // Command == CommandName.Modify eset�n jelenleg csak a hat�s�gi adatokn�l haszn�ljuk, �s csak akkor l�that�
        // ha az OnkormanyzatTab akt�v - elt�ntetj�k, miel�tt ellen�rizz�k az akt�v tabot
        if (Command == CommandName.Modify)
        {
            FormHeader1.FormTemplateLoader1_Visibility = false;
            FormHeaderUpdatePanel.Update();
        }

        AjaxControlToolkit.TabPanel RealActiveTab = IraIratokTabContainer.GetRealActiveTab();
        if (RealActiveTab == null) return;

        if (RealActiveTab.Equals(TabIratPanel))
        {
            IratTab1.Active = true;

            // a m�r esetleg lek�rt irat objektum �tad�sa (New-n�l null)
            IratTab1.obj_Irat = this.obj_Irat;
            IratTab1.obj_UgyiratDarab = this.obj_UgyiratDarab;
            IratTab1.obj_Ugyirat = this.obj_Ugyirat;
            IratTab1.obj_elsoIratPeldany = this.obj_elsoIratPeldany;

            IratTab1.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabUgyiratTerkepPanel))
        {
            UgyiratTerkepTab1.Active = true;
            UgyiratTerkepTab1.ReLoadTab();
        }

        if (RealActiveTab.Equals(TabMellekletekPanel))
        {
            MellekletekTab1.Active = true;
            MellekletekTab1.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabCsatolmanyokPanel))
        {
            //BUG 6380
            CsatolmanyokTab1.SignerCondition = AlairokTab.EnableSignForLoggedUser();

            CsatolmanyokTab1.Active = true;
            CsatolmanyokTab1.ReLoadTab();
        }
        // Ha az al��r�s gombra kattinti, akkor mindek�ppen friss�lj�n. (Az�rt csak View mert a modify mindig friss�t.)
        if (Command == CommandName.View && CsatolmanyokTab1.Active && !IsPostBack)
        {
            FormFooter1.ImageButton_Close.OnClientClick = "window.returnValue=true;" +
"if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
+ "window.close();";
        }
        //if (RealActiveTab.Equals(TabTovabbiBekuldokPanel))
        //{
        //    TovabbiBekuldokTab1.Active = true;
        //    TovabbiBekuldokTab1.ReLoadTab();
        //}
        if (RealActiveTab.Equals(TabTargyszavakPanel))
        {
            TargyszavakTab.Active = true;
            TargyszavakTab.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabCsatolasPanel))
        {
            CsatolasokTab1.Active = true;
            CsatolasokTab1.ReLoadTab();
        }
        //if (RealActiveTab.Equals(TabKapcsolatokPanel))
        //{
        //    KapcsolatokTab1.Active = true;
        //    KapcsolatokTab1.ReLoadTab();
        //}
        if (RealActiveTab.Equals(TabTortenetPanel))
        {
            TortenetTab1.Active = true;
            TortenetTab1.ReLoadTab();
        }
        //if (RealActiveTab.Equals(TabKapjakmegPanel))
        //{
        //    KapjakmegTab1.Active = true;
        //    KapjakmegTab1.ReLoadTab();
        //}
        if (RealActiveTab.Equals(TabFeladatokPanel))
        {
            FeladatokTab.Active = true;
            FeladatokTab.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabAlairokPanel))
        {
            AlairokTab.Active = true;
            AlairokTab.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabJogosultakPanel))
        {
            JogosultakTab1.Active = true;
            JogosultakTab1.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabElintezesAdatokPanel))
        {
            ElintezesAdatokTab.Active = true;
            ElintezesAdatokTab.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabIratJellemzoPanel))
        {
            IraIratJellemzokFormTab1.Active = true;
            IraIratJellemzokFormTab1.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabKuldesiAdatokPanel))
        {
            KuldesiAdatokTab.Active = true;
            KuldesiAdatokTab.ReLoadTab();
        }
        if (RealActiveTab.Equals(TabPanel_HatosagiStatisztikaPanel))
        {
            HatosagiStatisztikaTabA.Active = true;
            HatosagiStatisztikaTabA.ReLoadTab();
        }
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            TabIratFizetesiInformaciokPanel.Visible = false;
            TabIratFizetesiInformaciokPanel.Enabled = false;
        }
        else
        {
            if (RealActiveTab.Equals(TabIratFizetesiInformaciokPanel))
            {
                IratFizetesiInformaciokTab.Active = true;
                IratFizetesiInformaciokTab.ReLoadTab();
            }
        }
    }

    #endregion

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private void SetEnabledAllTabs(Boolean value)
    {
        TabIratPanel.Enabled = value;
        TabUgyiratTerkepPanel.Enabled = value;
        TabMellekletekPanel.Enabled = value;
        TabCsatolmanyokPanel.Enabled = value;
        //IraIratokTabContainer.Tabs[TabTovabbiBekuldokPanel.TabIndex].Enabled = value;
        TabTargyszavakPanel.Enabled = value;
        TabCsatolasPanel.Enabled = value;
        //IraIratokTabContainer.Tabs[TabKapcsolatokPanel.TabIndex].Enabled = value;
        TabTortenetPanel.Enabled = value;
        //IraIratokTabContainer.Tabs[TabKapjakmegPanel.TabIndex].Enabled = value;
        TabFeladatokPanel.Enabled = value;
        TabAlairokPanel.Enabled = value;
        TabJogosultakPanel.Enabled = value;
        TabElintezesAdatokPanel.Enabled = value;
        TabIratJellemzoPanel.Enabled = value;
        TabKuldesiAdatokPanel.Enabled = value;
        TabPanelCimzettLista.Enabled = value;
        //BLG 1722
        TabPanelMinositesFelulvizsgalat.Enabled = value;
        TabIratFizetesiInformaciokPanel.Enabled = value;
        TabPanel_HatosagiStatisztikaPanel.Enabled = value;
    }

    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni!
        TabIratPanel.Enabled = true;
        TabUgyiratTerkepPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
        TabMellekletekPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIratMelleklet" + CommandName.List);
        TabCsatolmanyokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIratCsatolmany" + CommandName.List);
        TabTargyszavakPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIratTargyszo" + CommandName.List);
        TabTortenetPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIratTortenet" + CommandName.List);
        TabCsatolasPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolas" + CommandName.List);
        TabFeladatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "Feladatok" + CommandName.List);
        TabAlairokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIratAlairo" + CommandName.List);
        // TODO:
        //TabJogosultakPanel.Enabled = ...
        TabElintezesAdatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IratElintezes");
        TabIratJellemzoPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
        TabKuldesiAdatokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "eBeadvanyokView");

        //BLG 1722 - ebben az esetben T�K kezel�nek adunk jogot, nem funkci�jog!
        TabPanelMinositesFelulvizsgalat.Enabled = Rendszerparameterek.IsTUK(Page);
        TabIratFizetesiInformaciokPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
        TabPanel_HatosagiStatisztikaPanel.Enabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
    }

    // ellen�rz�s �s sz�ks�g eset�n tabf�l tilt�s az aktu�lis irat alapj�n
    private void DisableTabsByMissingObjectRights(string id)
    {
        if (!Iratok.CheckRights_Csatolmanyok(Page, id))
        {
            TabCsatolmanyokPanel.Enabled = false;
        }
    }

    // ellen�rz�s �s sz�ks�g eset�n tabf�l tilt�s az aktu�lis irat alapj�n
    private void DisableTabsByMissingObjectRights(EREC_IraIratok erec_IraIratok)
    {
        if (erec_IraIratok == null)
            return;
        if (!Iratok.CheckRights_Csatolmanyok(Page, erec_IraIratok, false))
        {
            TabCsatolmanyokPanel.Enabled = false;
        }

        //elint�zett tab csak az elint�zett iratokn�l jelenjen meg
        if (erec_IraIratok.Elintezett != "1")
        {
            TabElintezesAdatokPanel.Enabled = false;
        }

        bool statisztika = Rendszerparameterek.GetBoolean(Page, "UGYTIPUS_STATISZTIKA", false);

        if (!statisztika)
        {
            IraIratJellemzokFormTab1.Visible = false;
            TabIratJellemzoPanel.Enabled = false;
        }
        SetFizetesiInformaciokTabVisibility(erec_IraIratok);

        SetHatosagiStatisztikaTabVisibility(erec_IraIratok);
    }

    public override void Load_ComponentSelectModul()
    {
        base.Load_ComponentSelectModul();

        if (pageView.CompSelector == null) { return; }
        else
        {
            pageView.CompSelector.Enabled = true;
            SetEnabledAllTabs(false);
            TabIratPanel.Enabled = true;
            pageView.CompSelector.Add_ComponentOnClick(TabIratPanel);
            pageView.CompSelector.Add_ComponentOnClick(IratTab1);
            pageView.CompSelector.Add_ComponentOnClick(TabUgyiratTerkepPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabMellekletekPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolmanyokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabTargyszavakPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabCsatolasPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabTortenetPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabFeladatokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabAlairokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabJogosultakPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabElintezesAdatokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabIratJellemzoPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabKuldesiAdatokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabPanelCimzettLista);
            //BLG 1722
            pageView.CompSelector.Add_ComponentOnClick(TabPanelMinositesFelulvizsgalat);

            pageView.CompSelector.Add_ComponentOnClick(TabIratFizetesiInformaciokPanel);
            pageView.CompSelector.Add_ComponentOnClick(TabPanel_HatosagiStatisztikaPanel);
            FormFooter1.SaveEnabled = false;
        }
    }

    /// <summary>
    /// SetHatosagiStatisztikaTabVisibility
    /// </summary>
    /// <param name="erec_IraIratok"></param>
    private void SetHatosagiStatisztikaTabVisibility(EREC_IraIratok erec_IraIratok)
    {
        if (erec_IraIratok == null)
            return;
        bool enabled = true;
        bool hasRight = FunctionRights.GetFunkcioJog(Page, "IraIratOnkormanyzatiAdat" + CommandName.List);

        if (string.IsNullOrEmpty(erec_IraIratok.Ugy_Fajtaja) || !HatosagiStatisztikaTabA.IsValidTab)
        {
            enabled = false;
        }

        if (TabPanel_HatosagiStatisztikaPanel.Enabled != enabled)
        {
            TabPanel_HatosagiStatisztikaPanel.Enabled = enabled;

            if (IsPostBack)
            {
                string js = String.Format("(function() {{ var tab = $find('{0}'); if(tab) tab.set_enabled({1}); }})();", TabPanel_HatosagiStatisztikaPanel.ClientID, enabled.ToString().ToLower());
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
            }
        }

        //LZS - BUG_11188
        if (enabled && erec_IraIratok.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo)
        {
            bool isHATOSAGI_ADATOK_BEJOVOHOZ = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.HATOSAGI_ADATOK_BEJOVOHOZ, false);
            TabPanel_HatosagiStatisztikaPanel.Enabled =
                TabPanel_HatosagiStatisztikaPanel.Visible = isHATOSAGI_ADATOK_BEJOVOHOZ;
        }


    }


    private void SetFizetesiInformaciokTabVisibility(EREC_IraIratok erec_IraIratok)
    {
        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            TabIratFizetesiInformaciokPanel.Visible = false;
            TabIratFizetesiInformaciokPanel.Enabled = false;
            return;
        }

        bool enabled = false;
        string ertek = GetTargyszoErtek(erec_IraIratok.Id, "illetek_fizetes");

        if ("True".Equals(ertek, StringComparison.InvariantCultureIgnoreCase))
        {
            enabled = true;
        }

        if (TabIratFizetesiInformaciokPanel.Enabled != enabled)
        {
            TabIratFizetesiInformaciokPanel.Enabled = enabled;

            if (IsPostBack)
            {
                string js = String.Format("(function() {{ var tab = $find('{0}'); if(tab) tab.set_enabled({1}); }})();", TabIratFizetesiInformaciokPanel.ClientID, enabled.ToString().ToLower());
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
            }
        }

    }

    private string GetTargyszoErtek(string iratId, string belsoAzonosito)
    {
        EREC_ObjektumTargyszavaiService service_ot = Contentum.eRecord.Utility.eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

        Result result = service_ot.GetAllMetaByObjMetaDefinicio(execParam, search, iratId, null, Constants.TableNames.EREC_IraIratok, null, null, false, null, true, false);

        if (!result.IsError)
        {
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string azonosito = row["BelsoAzonosito"].ToString();
                if (azonosito == belsoAzonosito)
                {
                    string ertek = row["Ertek"].ToString();
                    return ertek;
                }
            }
        }

        return String.Empty;
    }

}
