<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IraIratokList.aspx.cs" Inherits="IraIratokList" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="slh" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<%@ Import Namespace="Contentum.eRecord.Utility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>

    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="IraIratokCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="IraIratokUpdatePanel" runat="server" OnLoad="IraIratokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="IraIratokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="IraIratokCPEButton"
                            CollapseControlID="IraIratokCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="IraIratokCPEButton" ExpandedSize="0" ExpandedText="Felhaszn�l�k list�ja"
                            CollapsedText="Felhaszn�l�k list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                        <asp:GridView ID="IraIratokGridView" runat="server" OnRowCommand="IraIratokGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="IraIratokGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id,ElsoIratPeldany_Id" OnSorting="IraIratokGridView_Sorting"
                                            OnRowDataBound="IraIratokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="Csny" HeaderText="Csny.">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="K" HeaderText="K.">
                                                    <ItemTemplate>
                                                        <asp:Image ID="CsatoltImage" AlternateText="Kapcsolat" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" Style="cursor: pointer"
                                                            onmouseover="Utility.UI.SetElementOpacity(this,0.7);" onmouseout="Utility.UI.SetElementOpacity(this,1);" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="F" HeaderText="F.">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="FeladatImage" AlternateText="Kezel�si feljegyz�s" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IktatoHely" AccessibleHeaderText="Iktatokonyv" HeaderText="Iktat�k�nyv" SortExpression="EREC_IraIktatokonyvek.Iktatohely" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Foszam" AccessibleHeaderText="Foszam" HeaderText="F�sz�m" SortExpression="EREC_UgyUgyiratok.Foszam" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Alszam" AccessibleHeaderText="Alszam"  HeaderText="Alsz�m" SortExpression="EREC_IraIratok.Alszam" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ev" AccessibleHeaderText="Ev" HeaderText="�v" SortExpression="EREC_IraIktatokonyvek.Ev" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--BLG_619--%>
                                                <asp:BoundField DataField="IratFelelos_SzervezetKod" AccessibleHeaderText="IratFelelos_SzervezetKod" HeaderText="<%$Forditas:BoundFieldIratFelelos_SzervezetKod|Szk%>" SortExpression="Csoportok_IratFelelosNev.Kod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="20px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MinositesNev" AccessibleHeaderText="MinositesNev" HeaderText="Min�s�t�s" SortExpression="EREC_IraIratok.Minosites" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IktatoSzam_Merge" AccessibleHeaderText="IktatoSzam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam, EREC_IraIratok.Alszam">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Targy1" AccessibleHeaderText="Targy1" HeaderText="Irat&nbsp;t�rgy" SortExpression="EREC_IraIratok.Targy">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyirat_targy" AccessibleHeaderText="Ugyirat_targy" HeaderText="�gyirat&nbsp;t�rgy" Visible="false" SortExpression="EREC_UgyUgyiratok.Targy">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni --%>
                                                <%--BLG_1014--%>
                                                <%-- <asp:BoundField DataField="Ugyintezo_Nev" HeaderText="�gyirat �gyint�z�" Visible="false" SortExpression="Csoportok_UgyiratUgyintezoNev.Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>    --%>
                                                <asp:BoundField DataField="Ugyintezo_Nev" AccessibleHeaderText="Ugyintezo_Nev"  HeaderText="<%$Forditas:BoundField_UgyiratUgyintezo|�gyirat �gyint�z�%>" Visible="false" SortExpression="Csoportok_UgyiratUgyintezoNev.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyirat_Hatarido" AccessibleHeaderText="Ugyirat_Hatarido" HeaderText="�gyirat �gyint�z�si hat�rideje" Visible="false" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UgyintezesModja" AccessibleHeaderText="UgyintezesModja" HeaderText="�gyint�z�s m�dja" Visible="false" SortExpression="UgyintezesModjaKodtarak.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni --%>
                                                <%--nekrisz 2017.09.05 Laur�val egyeztetve kiszedve--%>
                                                <%-- <asp:BoundField DataField="Eloirat" HeaderText="El�irat" Visible="false" SortExpression="Eloirat">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="Utoirat" HeaderText="Ut�irat" Visible="false" SortExpression="Utoirat">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="MellekletekSzama" HeaderText="Mell�kletek sz�ma" Visible="false" SortExpression="MellekletekSzama">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>   
                                        <asp:BoundField DataField="MellekletekAdathordozoTipusa" HeaderText="Mell�kletek (adathordoz�) t�pusa" Visible="false" SortExpression="MellekletekAdathordozoTipusa">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>   
                                        <asp:BoundField DataField="MellekletekAdathordozoFajtaja" HeaderText="Mell�kletek (adathordoz�) fajt�ja" Visible="false" SortExpression="MellekletekAdathordozoFajtaja">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>                                    --%>
                                                <asp:BoundField DataField="KezelesiFelj" AccessibleHeaderText="KezelesiFelj" HeaderText="Kezel�si feljegyz�s" Visible="false" SortExpression="IratKezelesiFeljegyzesek.KezelesiFeljegyzes">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ITSZ" AccessibleHeaderText="ITSZ" HeaderText="ITSZ" Visible="false" SortExpression="ITSZ.ITSZ">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni v�ge --%>
                                                <asp:BoundField DataField="ElintezesDat" AccessibleHeaderText="ElintezesDat" HeaderText="�gyirat elint�z�si id�pontja" Visible="false" SortExpression="EREC_UgyUgyiratok.ElintezesDat">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LezarasDat" AccessibleHeaderText="LezarasDat" HeaderText="�gyirat lez�r�si id�pontja" Visible="false" SortExpression="EREC_UgyUgyiratok.LezarasDat">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IrattarbaHelyezesDat" AccessibleHeaderText="IrattarbaHelyezesDat" HeaderText="Iratt�rba helyez�s d�tuma" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaVetelDat">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AdathordozoTipusa_Nev" AccessibleHeaderText="AdathordozoTipusa_Nev" HeaderText="Adathord.&nbsp;t�p." SortExpression="AdathordozoTipusaKodTarak.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Iktato_Nev" AccessibleHeaderText="FelhasznaloCsoport_Id_Iktato_Nev" HeaderText="Iktat�" SortExpression="Csoportok_IktatoNev.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni --%>
                                                <%--BLG_1014--%>
                                                <%-- <asp:BoundField DataField="FelhasznaloCsoport_Id_Ugyintezo_Nev" HeaderText="�gyint�z�" SortExpression="Csoportok_UgyintezoNev.Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>--%>
                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Ugyintezo_Nev" AccessibleHeaderText="FelhasznaloCsoport_Id_Ugyintezo_Nev" HeaderText="<%$Forditas:BoundField_IratUgyintezo|�gyint�z�%>" SortExpression="Csoportok_UgyintezoNev.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni --%>
                                                <asp:BoundField DataField="PostazasDatuma" AccessibleHeaderText="PostazasDatuma" HeaderText="K�ld�s id�pontja" Visible="false" SortExpression="EREC_PldIratPeldanyok.PostazasDatuma">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni --%>
                                                <asp:BoundField DataField="PeldanyKuldesMod" AccessibleHeaderText="PeldanyKuldesMod" HeaderText="Kimen� k�ld. k�ld�s m�dja" Visible="false" SortExpression="PeldanyKuldesModjaKodTarak.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni v�ge --%>
                                                <%-- nekrisz CR 2997 Post�z�s ir�ny�nak megjelen�t�se n�vvel --%>
                                                <%-- <asp:BoundField DataField="PostazasIranya" HeaderText="PostazasIranya" Visible="false" SortExpression="EREC_KuldKuldemenyek.PostazasIranya">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>--%>
                                                <asp:BoundField DataField="PostazasIranyaNev" AccessibleHeaderText="PostazasIranyaNev" HeaderText="Post�z�s ir�nya" SortExpression="PostazasIranyaKodTarak.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- nekrisz CR 2997 --%>
                                                <asp:BoundField DataField="ErkeztetoSzam" AccessibleHeaderText="ErkeztetoSzam" HeaderText="�rkeztet�si azonos�t�" Visible="false" SortExpression="EREC_KuldKuldemenyek.Erkezteto_Szam">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NevSTR_Bekuldo" AccessibleHeaderText="NevSTR_Bekuldo" HeaderText="K�ld�/felad� neve" Visible="false" SortExpression="EREC_KuldKuldemenyek.NevSTR_Bekuldo">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CimSTR_Bekuldo" AccessibleHeaderText="CimSTR_Bekuldo" HeaderText="K�ld�/felad� c�me" Visible="false" SortExpression="EREC_KuldKuldemenyek.CimSTR_Bekuldo">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BeerkezesIdeje" AccessibleHeaderText="BeerkezesIdeje" HeaderText="Be�rkez�s id�pontja" Visible="false" SortExpression="EREC_KuldKuldemenyek.BeerkezesIdeje">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="HivatkozasiSzam" AccessibleHeaderText="HivatkozasiSzam" HeaderText="Hivatkoz�si sz�m" Visible="false" SortExpression="EREC_KuldKuldemenyek.HivatkozasiSzam">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni --%>
                                                <asp:BoundField DataField="KuldesMod" AccessibleHeaderText="KuldesMod" HeaderText="Be�rkez�s m�dja" Visible="false" SortExpression="KuldesModjaKodTarak.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KezbesitesModja_Nev" AccessibleHeaderText="KezbesitesModja_Nev" HeaderText="K�zbes�t�s m�dja" Visible="false" SortExpression="KezbesitesModjaKodtarak.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                </asp:BoundField>
                                                <%-- ki kell eg�sz�teni v�ge --%>
                                                <%-- Csak k�zgy�l�si el�terjeszt�s START --%>
                                                <asp:BoundField DataField="ElsoIratPeldanyOrzo_Nev" AccessibleHeaderText="ElsoIratPeldanyOrzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_Peldany_OrzoNev.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ElsoIratPeldanyFelelos_Nev" AccessibleHeaderText="ElsoIratPeldanyFelelos_Nev" HeaderText="Kezel�" SortExpression="Csoportok_Peldany_FelelosNev.Nev" Visible="false">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ElsoIratPeldanyBarCode" AccessibleHeaderText="ElsoIratPeldanyBarCode" HeaderText="Vonalk�d" SortExpression="EREC_PldIratPeldanyok.BarCode">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NevSTR_Cimzett" AccessibleHeaderText="NevSTR_Cimzett" HeaderText="C�mzett neve" Visible="false" SortExpression="EREC_PldIratPeldanyok.NevSTR_Cimzett">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CimSTR_Cimzett" AccessibleHeaderText="CimSTR_Cimzett" HeaderText="C�mzett c�me" Visible="false" SortExpression="EREC_PldIratPeldanyok.CimSTR_Cimzett">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <%-- Csak k�zgy�l�si el�terjeszt�s END --%>
                                                <asp:TemplateField HeaderText="Iktat�s id�pontja" AccessibleHeaderText="IktatasDatuma" SortExpression="EREC_IraIratok.IktatasDatuma">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelIktatva" runat="server" Text='<%# string.Concat(Eval("IktatasDatuma"), (Eval("RegiIktatoSzam") as string) != null ? "<br /><i>(" + (Eval("RegiIktatoSzam") as string).Replace(" ", "&nbsp;") + " �tiktatva)</i>" : "") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Hatarido" AccessibleHeaderText="Hatarido" HeaderText="Irat �gyint�z�si hat�rideje" Visible="false" SortExpression="EREC_IraIratok.Hatarido">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SztornirozasDat" AccessibleHeaderText="SztornirozasDat" HeaderText="Sztorn�rozva" SortExpression="EREC_IraIratok.SztornirozasDat">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Munkaallomas" AccessibleHeaderText="Munkaallomas" HeaderText="Munka�llom�s" SortExpression="EREC_IraIratok.Munkaallomas">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="�llapot" AccessibleHeaderText="Allapot" SortExpression="AllapotKodTarak.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <%-- �tiktatott �llapot eset�n (06) �tiktat�s ut�ni �j iktat�sz�m megjelen�t�se --%>
                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%# string.Concat(Eval("Allapot_Nev"), (Eval("Allapot") as string) == "06" && (Eval("UjIktatoSzam") as string) != null ? "<br>": "") %>' />
                                                        <asp:Label ID="labelAllapotIkt" runat="server" Text='<%# string.Concat((Eval("Allapot") as string) == "06" && (Eval("UjIktatoSzam") as string) != null ? "(" + (Eval("UjIktatoSzam") as string).Replace(" ", "&nbsp;") + ")" : "") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" AccessibleHeaderText="" HeaderStyle-CssClass="GridViewInfoImage">
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" ID="AtiktatasiLanc_ImageButton1" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                            OnClientClick='<%# this.GetPopupString_AtiktatasiLanc(Eval("Id").ToString()) %>'
                                                            Visible='<%#(Eval("RegiIktatoSzam") as string) != null || (Eval("UjIktatoSzam") as string) != null ? true : false %>'
                                                            ToolTip="�tiktat�si l�nc" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Elint." AccessibleHeaderText="Elintezett" SortExpression="EREC_IraIratok.Elintezett">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelElintezett" runat="server" Text='<%# Eval("Elintezett").ToString() == "1" ? "E" : "" %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Elint. Id�" AccessibleHeaderText="IntezesIdopontja" SortExpression="EREC_IraIratok.IntezesIdopontja">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelElintezettDat" runat="server" Text='<%# Eval("Elintezett").ToString() == "1" ? Eval("IntezesIdopontja") : ""%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IratHatasaUgyintezesreNev" AccessibleHeaderText="IratHatasaUgyintezesreNev" HeaderText="Irat hat�sa" Visible="true" SortExpression="IratHatasaUgyintezesreNev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Font-Size="X-Small"/>
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" AccessibleHeaderText="IratHatasaUgyintezesreTipus" HeaderText="IH" >
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%# GetIratHatasaTipusIkon(Eval("IratHatasaUgyintezesreTipus")) %>' Font-Size="Large"
                                                            ToolTip='<%# Eval("IratHatasaUgyintezesreTipus") %>'
                                                            ForeColor='<%# GetIratHatasaTipusColor(Eval("IratHatasaUgyintezesreTipus")) %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" AccessibleHeaderText="HatralevoNapok" HeaderText="H�tral�v� napok" SortExpression="HatralevoNapok"
                                                    HeaderStyle-Font-Size="X-Small">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%# HatralevoNapokSzovegesen(Eval("HatralevoNapok")) %>' Font-Size="X-Small"
                                                            ForeColor='<%# HatralevoNapokSzinesen(Eval("HatralevoNapok")) %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage" AccessibleHeaderText="HatralevoMunkaNapok" HeaderText="H�tral�v� munkanapok" SortExpression="HatralevoMunkaNapok"
                                                    HeaderStyle-Font-Size="X-Small">
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%# HatralevoMunkaNapokSzovegesen(Eval("HatralevoMunkaNapok")) %>' Font-Size="X-Small"
                                                            ForeColor='<%# HatralevoNapokSzinesen(Eval("HatralevoMunkaNapok")) %>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField AccessibleHeaderText="EljarasiSzakaszFokNev" DataField="EljarasiSzakaszFokNev" HeaderText="Elj�r�si fok" SortExpression="EljarasiSzakaszFokNev.Nev" >
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Font-Size="X-Small"/>
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                             

                                                <%-- Csak k�zgy�l�si el�terjeszt�s START --%>
                                                <asp:BoundField AccessibleHeaderText="ElsoIratPeldanyAllapot_Nev" DataField="ElsoIratPeldanyAllapot_Nev" HeaderText="Els� pld. �llapot" SortExpression="KodTarak_Peldany_AllapotNev.Nev">
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                </asp:BoundField>
                                                <%-- Csak k�zgy�l�si el�terjeszt�s END --%>
                                                <asp:TemplateField AccessibleHeaderText="UgyFajtaja" HeaderText="�gy fajt�ja" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:Label ID="labelUgyFajtajaCaption" runat="server" Text="�." ToolTip="�gy fajt�ja" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="labelUgyFajtaja" runat="server" Text='<%# UI.GetUgyFajtajaLabel(Page, Eval("Ugy_Fajtaja").ToString())%>'
                                                            ToolTip='<%# UI.GetUgyFajtajaToolTip(Page, Eval("Ugy_Fajtaja").ToString())%>'
                                                            NavigateUrl='<%# UI.GetHatosagiAdatokUrl(Eval("Id").ToString(), false) %>' Target="_blank"
                                                            onclick='<%# UI.OpenHatosagiAdatok(Eval("Id").ToString(), IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList) %>'
                                                            CssClass="linkStyle">
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <ItemTemplate>
                                                        <input runat="server" type="button" id="ButtonLotusNotesPostBack" value="&#9852;" title="&#9852;"
                                                            visible='<%# VisiblePostBackCommandForLotusNotes() %>'
                                                            onclick='<%# GetPostBackCommandForLotusNotes(Container.DataItem) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; height: 8px;" colspan="2"></td>
        </tr>
        <tr id="tr_Esemenyek" runat="server">
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                            </ajaxToolkit:CollapsiblePanelExtender>
                            <asp:Panel ID="Panel8" runat="server">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                    OnClientActiveTabChanged="ActiveTabChanged" Width="100%">
                                    <ajaxToolkit:TabPanel ID="EsemenyekTabPanel" runat="server" TabIndex="0">
                                        <HeaderTemplate>
                                            <asp:Label ID="Header1" runat="server" Text="Iratmozg�s esem�nyek"></asp:Label>
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="EsemenyekUpdatePanel" runat="server" OnLoad="EsemenyekUpdatePanel_Load">
                                                <ContentTemplate>
                                                    <asp:Panel ID="EsemenyekPanel" runat="server" Visible="false" Width="100%">
                                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <slh:SubListHeader ID="EsemenyekSubListHeader" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                    <ajaxToolkit:CollapsiblePanelExtender ID="EsemenyCPE" runat="server" TargetControlID="Panel2"
                                                                        CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                        AutoExpand="false" ExpandedSize="0">
                                                                    </ajaxToolkit:CollapsiblePanelExtender>
                                                                    <asp:Panel ID="Panel2" runat="server">
                                                                        <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                        <asp:GridView ID="EsemenyekGridView" runat="server" CellPadding="0" BorderWidth="1px"
                                                                            AllowPaging="True" PagerSettings-Visible="false" AllowSorting="True" AutoGenerateColumns="false"
                                                                            OnSorting="EsemenyekGridView_Sorting" OnPreRender="EsemenyekGridView_PreRender"
                                                                            OnRowCommand="EsemenyekGridView_RowCommand" DataKeyNames="Id" OnSelectedIndexChanging="EsemenyekGridView_SelectedIndexChanging"
                                                                            OnRowUpdating="EsemenyekGridView_RowUpdating" OnRowCancelingEdit="EsemenyekGridView_RowCancelingEdit"
                                                                            OnRowDataBound="EsemenyekGridView_RowDataBound">
                                                                            <PagerSettings Visible="False" />
                                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                            <Columns>
                                                                                <%--<asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                    <HeaderTemplate>
                                                                                        <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                        &nbsp;&nbsp;
                                                                                        <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                                            AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>
                                                                                <asp:TemplateField>
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" BorderWidth="1" HorizontalAlign="Center"
                                                                                        VerticalAlign="Middle" />
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="25px" />
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ID="ibtnSelect" CommandName="Select" ImageUrl="~/images/hu/Grid/3Drafts.gif"
                                                                                            AlternateText="<%$Resources:List,AlternateText_RowSelectButton%>" />
                                                                                        <asp:ImageButton runat="server" ID="ibtnUpdate" CommandName="Update" ImageUrl="~/images/hu/Grid/saverow.gif"
                                                                                            AlternateText="Elment" Visible="false" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <%--                                                                                <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif" >
                                                                                    <HeaderStyle Width="25px" />
                                                                                    <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                </asp:CommandField> --%>
                                                                                <asp:BoundField DataField="FunkcioNev" HeaderText="Esem�ny" SortExpression="KRT_Esemenyek.FunkcioNev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="VegrehajtoNev" HeaderText="V�grehajt� felh." SortExpression="KRT_Felhasznalok.Nev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Iratkezel�si elem" SortExpression="IratMozgas.ObjTipusNev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelObjTipusNev" runat="server" Text='<%# (Eval("ObjTipusNev") as string) == "EREC_UgyUgyiratok" ? "�gyirat"
                                                                                         : (Eval("ObjTipusNev") as string) ==  "EREC_IraIratok" ? "Irat"
                                                                                         : (Eval("ObjTipusNev") as string) == "EREC_PldIratPeldanyok" ? "Iratp�ld�ny"
                                                                                         : "???" %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Mozg�s" SortExpression="IratMozgas.FieldChanged {0}, IratMozgas.FromField {0}, IratMozgas.ToField">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelMezoNev" runat="server" Text='<%# String.Format("{0}: {1} &rArr; {2}" 
                                                                                            , (Eval("FieldChanged") as string) == "Csoport_Id_Felelos" ? "Felel�s" : ((Eval("FieldChanged") as string) == "FelhasznaloCsoport_Id_Orzo") ? "Irat helye" : "???"
                                                                                            , Eval("FromField")
                                                                                            , Eval("ToField"))
                                                                                            %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <%--                                                                                <asp:BoundField DataField="OrzoNev_Old" HeaderText="El�z� irat helye" SortExpression="OrzoNev_Old">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="OrzoNev" HeaderText="Irat helye" SortExpression="OrzoNev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FelelosNev_Old" HeaderText="El�z� kezel�" SortExpression="FelelosNev_Old">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FelelosNev" HeaderText="Kezel�" SortExpression="FelelosNev">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>--%>
                                                                                <asp:BoundField DataField="HistoryVegrehajtasIdo" HeaderText="V�grehajt�s ideje" SortExpression="IratMozgas.HistoryVegrehajtasIdo">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField HeaderText="Megjegyz�s" SortExpression="KRT_Esemenyek.Note">
                                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Width="150px" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelNote" runat="server" Text='<%# Eval("EsemenyNote") %>'></asp:Label>
                                                                                        <asp:TextBox ID="txtNote" runat="server" Text='<%# Bind("EsemenyNote") %>' Visible="false"
                                                                                            Style="width: 90%"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="labelId" runat="server" Text='<%# Eval("EsemenyId") %>'></asp:Label>
                                                                                        <asp:Label ID="labelVer" runat="server" Text='<%# Eval("EsemenyVer") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br />


</asp:Content>

