﻿using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IraIratokList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    private readonly string[] InvisibleBoundFields_KozgyulesiEloterjesztes = new string[] {
        "AdathordozoTipusa_Nev", "FelhasznaloCsoport_Id_Iktato_Nev", "SztornirozasDat" };

    private readonly string[] VisibleBoundFields_KozgyulesiEloterjesztes = new string[] {
        "ElsoIratPeldanyOrzo_Nev", "ElsoIratPeldanyBarCode","ElsoIratPeldanyAllapot_Nev" };

    private const string GridViewSpecialRowStyle = "GridViewSpecialRowStyle";
    private const string GridViewSpecialSelectedRowStyle = "GridViewSpecialSelectedRowStyle";

    //private const string default_SearchExpression =
    //"EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam";

    private string Startup = "";
    private string Mode = String.Empty;

    private const string lockColumnName = "LockField";
    private const string lockedImageName = "LockedImage";

    private const string stringKuldoNeve = "Küldő/feladó neve";
    private const string stringKuldoCime = "Küldő/feladó címe";

    private const string stringCimzettNeve = "Címzett neve";
    private const string stringCimzettCime = "Címzett címe";

    private enum RowModeType { NotEditableSelected = -2, NotEditableNormal = -1, EditableNormal = 0, EditableInEditingMode = 1 }

    public int LockColumnIndex
    {
        get
        {
            if (ViewState["LockColumnIndex"] != null)
            {
                return (int)ViewState["LockColumnIndex"];
            }
            return -1;
        }

        set { ViewState["LockColumnIndex"] = value; }
    }

    #region Dinamikus TemplateFieldek mentése

    private Dictionary<string, string> TemplateFieldSources
    {
        get
        {
            if (ViewState["TemplateFieldSources"] != null)
            {
                return (Dictionary<string, string>)ViewState["TemplateFieldSources"];
            }
            else
            {
                ViewState["TemplateFieldSources"] = new Dictionary<string, string>();
            }
            return (Dictionary<string, string>)ViewState["TemplateFieldSources"];
        }

        set
        {
            ViewState["TemplateFieldSources"] = value;
        }
    }

    private void SaveTemplateFieldSourceToViewState(string key, string controlTypeSource)
    {
        if (!TemplateFieldSources.ContainsKey(key))
        {
            TemplateFieldSources.Add(key, controlTypeSource);
        }
        else
        {
            TemplateFieldSources[key] = controlTypeSource;
        }
    }

    private string GetTemplateFieldSourceFromViewState(string key)
    {
        if (!TemplateFieldSources.ContainsKey(key))
        {
            return null;
        }
        else
        {
            return TemplateFieldSources[key];
        }
    }
    #endregion Dinamikus TemplateFieldek mentése

    #region Tárgyszó/Meta oszlopok hozzáadása
    private void AddMetaColumnsToGridView(Result result, GridView gv, bool visible)
    {
        if (!result.IsError)
        {
            if (result.Ds.Tables.Contains(Constants.TableNames.EREC_ObjektumTargyszavai))
            {
                DataTable table = result.Ds.Tables[Constants.TableNames.EREC_ObjektumTargyszavai];

                ArrayList lstGridViewColumns = new ArrayList(gv.Columns);
                DataControlField[] arrayGridViewColumns = (DataControlField[])lstGridViewColumns.ToArray(typeof(DataControlField));

                foreach (DataRow row in table.Rows)
                {
                    string Targyszo = row["Targyszo"].ToString();
                    if (!String.IsNullOrEmpty(Targyszo) && result.Ds.Tables[0].Columns.Contains(String.Format("{0}", Targyszo)))
                    {
                        if (!Array.Exists<DataControlField>(arrayGridViewColumns,
                                delegate (DataControlField dcf) { return dcf.AccessibleHeaderText == Targyszo; }))
                        {
                            string ControlTypeSource = row[Constants.ColumnNames.EREC_ObjektumTargyszavai.WithExtension.ControlTypeSource].ToString();

                            //int positionBeforeLastColumn = gv.Columns.Count - 1;
                            //if (positionBeforeLastColumn < 0)
                            //{
                            //    positionBeforeLastColumn = 0;
                            //}

                            string ControlTypeSourceForGrid = KodTarak.CONTROLTYPE_SOURCE.MapForGrid(ControlTypeSource);
                            if (ControlTypeSourceForGrid == KodTarak.CONTROLTYPE_SOURCE.BoundField)
                            {
                                BoundField boundField = new BoundField();
                                boundField.HeaderText = Targyszo;
                                boundField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                boundField.HeaderStyle.CssClass = "GridViewBorderHeaderWrapped";
                                boundField.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
                                boundField.DataField = String.Format("{0}", Targyszo);
                                boundField.AccessibleHeaderText = Targyszo;
                                boundField.Visible = visible;

                                //gv.Columns.Insert(positionBeforeLastColumn, boundField);
                                gv.Columns.Add(boundField);
                            }
                            else if (ControlTypeSourceForGrid == KodTarak.CONTROLTYPE_SOURCE.CheckBoxField)
                            {
                                // ha van hibás formátumú érték, BoudFieldet csinálunk CheckBoxField helyett
                                if (result.Ds.Tables[0].Select(String.Format("IsNull([{0}],{1}) NOT IN ({1}, {2})", Targyszo, Boolean.FalseString, Boolean.TrueString)).Length == 0)
                                {
                                    CheckBoxField checkboxField = new CheckBoxField();
                                    checkboxField.HeaderText = Targyszo;
                                    checkboxField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                    checkboxField.HeaderStyle.CssClass = "GridViewCheckBoxTemplateHeaderStyle";
                                    checkboxField.ItemStyle.CssClass = "GridViewCheckBoxTemplateItemStyle";
                                    checkboxField.DataField = String.Format("{0}", Targyszo);
                                    checkboxField.AccessibleHeaderText = Targyszo;

                                    //gv.Columns.Insert(positionBeforeLastColumn, checkboxField);
                                    gv.Columns.Add(checkboxField);
                                }
                                else
                                {
                                    BoundField boundField = new BoundField();
                                    boundField.HeaderText = Targyszo;
                                    boundField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                    boundField.HeaderStyle.CssClass = "GridViewBorderHeaderWrapped";
                                    boundField.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
                                    boundField.DataField = String.Format("{0}", Targyszo);
                                    boundField.AccessibleHeaderText = Targyszo;

                                    //gv.Columns.Insert(positionBeforeLastColumn, boundField);
                                    gv.Columns.Add(boundField);
                                }
                            }
                            else
                            {
                                TemplateField templateField = new TemplateField();
                                templateField.HeaderText = Targyszo;
                                templateField.SortExpression = String.Format("EREC_ObjektumTargyszavai.[{0}]", Targyszo);
                                templateField.HeaderStyle.CssClass = "GridViewBorderHeaderWrapped";
                                templateField.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
                                templateField.ItemTemplate = new DynamicValueControlGridViewTemplate(Targyszo
                                    , DataControlRowType.DataRow
                                    , ControlTypeSourceForGrid);
                                templateField.AccessibleHeaderText = Targyszo;

                                //gv.Columns.Insert(positionBeforeLastColumn, templateField);
                                gv.Columns.Add(templateField);
                            }

                            // mentés a ViewState-be
                            SaveTemplateFieldSourceToViewState(Targyszo, ControlTypeSource);
                        }
                        else if (IsPostBack)
                        {
                            #region Template újragenerálás
                            // .NET framework hiba: template field esetén postback után elveszik a template
                            // workaround: újra generáljuk a template-et
                            DataControlField currentColumn = Array.Find<DataControlField>(arrayGridViewColumns,
                                delegate (DataControlField dcf) { return dcf.AccessibleHeaderText == Targyszo; });
                            if (currentColumn != null)
                            {
                                string ControlTypeSource = row[Constants.ColumnNames.EREC_ObjektumTargyszavai.WithExtension.ControlTypeSource].ToString();
                                string ControlTypeSourceForGrid = KodTarak.CONTROLTYPE_SOURCE.MapForGrid(ControlTypeSource);
                                if (currentColumn is TemplateField)
                                {
                                    ((TemplateField)currentColumn).ItemTemplate = new DynamicValueControlGridViewTemplate(Targyszo
                                        , DataControlRowType.DataRow
                                        , ControlTypeSourceForGrid);
                                }
                            }
                            #endregion Template újragenerálás
                        }
                    }
                }
            }
        }
    }
    #endregion Tárgyszó/Meta oszlopok hozzáadása

    #region Esemenyek Utility

    private bool IsEsemenyekSublistEnabled
    {
        get
        {
            return Startup == Constants.Startup.FromKozgyulesiEloterjesztesek
            || Startup == Constants.Startup.FromBizottsagiEloterjesztesek;
        }
    }

    private bool IsGridViewRowBoundToEsemeny(GridViewRow row, bool defaultValue)
    {
        if (row != null)
        {
            if (row.DataItem != null)
            {
                if (((DataRowView)row.DataItem).Row.Table.Columns.Contains("EsemenyId"))
                {
                    return !String.IsNullOrEmpty(((DataRowView)row.DataItem).Row["EsemenyId"].ToString());
                }
            }
            else
            {
                GridView gv = row.NamingContainer as GridView;
                if (gv.Equals(EsemenyekGridView))
                {
                    Label labelId = row.FindControl("labelId") as Label;
                    if (labelId != null)
                    {
                        return !String.IsNullOrEmpty(labelId.Text);
                    }
                }
            }
        }

        return defaultValue;
    }

    private bool IsGridViewModeEditable(RowModeType mode)
    {
        return mode >= 0;
    }

    private RowModeType MapGridViewModeByEsemenyBinding(GridViewRow row, RowModeType mode)
    {
        if (!IsGridViewModeEditable(mode) || IsGridViewRowBoundToEsemeny(row, false))
        {
            return mode;
        }

        // nem szerkeszthető sor
        switch (mode)
        {
            case RowModeType.EditableNormal: // 0:
                return RowModeType.NotEditableNormal; //-1;
            case RowModeType.EditableInEditingMode: //1:
                return RowModeType.NotEditableSelected;//-2;
        }

        return mode;
    }

    private void SetGridViewRowNormalMode(GridViewRow row)
    {
        RowModeType mode = MapGridViewModeByEsemenyBinding(row, RowModeType.EditableNormal); //0);
        SetGridViewRowMode(row, mode);
    }

    private void SetGridViewRowEditMode(GridViewRow row)
    {
        RowModeType mode = MapGridViewModeByEsemenyBinding(row, RowModeType.EditableInEditingMode); //1);
        SetGridViewRowMode(row, mode);
    }

    private void SetGridViewRowMode(GridViewRow row, RowModeType mode)
    {
        if (row != null)
        {
            TextBox txtNote = (TextBox)row.FindControl("txtNote");
            Label labelNote = (Label)row.FindControl("labelNote");
            ImageButton ibtnSelect = (ImageButton)row.FindControl("ibtnSelect");
            ImageButton ibtnUpdate = (ImageButton)row.FindControl("ibtnUpdate");

            // leképezés eseményhez kötöttség és kiválasztottság szerint
            mode = MapGridViewModeByEsemenyBinding(row, mode);

            if (labelNote != null)
            {
                switch (mode)
                {
                    case RowModeType.EditableNormal: //0:
                        labelNote.Visible = true;
                        break;
                    case RowModeType.EditableInEditingMode: //1:
                        labelNote.Visible = false;
                        break;
                    case RowModeType.NotEditableNormal: //-1:
                    case RowModeType.NotEditableSelected: //-2:
                    default:
                        labelNote.Visible = false;
                        break;
                }
            }
            if (txtNote != null)
            {
                switch (mode)
                {
                    case RowModeType.EditableNormal: //0:
                        txtNote.Visible = false;
                        break;
                    case RowModeType.EditableInEditingMode: //1:
                        if (labelNote != null)
                        {
                            txtNote.Text = labelNote.Text;
                        }
                        txtNote.Visible = true;
                        JavaScripts.SetOnClientEnterPressed(txtNote, ClientScript.GetPostBackEventReference(ibtnUpdate, "click"));
                        ScriptManager1.SetFocus(txtNote);
                        break;
                    case RowModeType.NotEditableNormal: //-1:
                    case RowModeType.NotEditableSelected: //-2:
                    default:
                        txtNote.Visible = false;
                        break;
                }
            }

            if (ibtnSelect != null)
            {
                switch (mode)
                {
                    case RowModeType.EditableNormal: //0:
                        ibtnSelect.Visible = true;
                        break;
                    case RowModeType.EditableInEditingMode: //1:
                        ibtnSelect.Visible = false;
                        break;
                    case RowModeType.NotEditableNormal: //-1:
                    case RowModeType.NotEditableSelected: //-2:
                    default:
                        ibtnSelect.Visible = true;
                        break;
                }
            }

            if (ibtnUpdate != null)
            {
                switch (mode)
                {

                    case RowModeType.EditableNormal: //0:
                        ibtnUpdate.Visible = false;
                        break;
                    case RowModeType.EditableInEditingMode: //1:
                        ibtnUpdate.Visible = true;
                        break;
                    case RowModeType.NotEditableNormal: //-1:
                    case RowModeType.NotEditableSelected: //-2:
                    default:
                        ibtnUpdate.Visible = false;
                        break;
                }
            }

            SetGridViewRowBackGroundColor(row, mode);
        }
    }

    private void SetGridViewRowBackGroundColor(GridViewRow row, RowModeType mode)
    {
        GridView gv = row.NamingContainer as GridView;
        if (gv != null && gv.Equals(EsemenyekGridView))
        {
            // leképezés eseményhez kötöttség és kiválasztottság szerint
            mode = MapGridViewModeByEsemenyBinding(row, mode);
            switch (mode)
            {
                case RowModeType.EditableNormal: //0:
                    //row.BackColor = (row.NamingContainer as GridView).RowStyle.BackColor;
                    row.CssClass = gv.RowStyle.CssClass;
                    break;
                case RowModeType.EditableInEditingMode: //1:
                    //row.BackColor = (row.NamingContainer as GridView).SelectedRowStyle.BackColor;
                    row.CssClass = gv.SelectedRowStyle.CssClass;
                    break;
                case RowModeType.NotEditableNormal: //-1:
                    row.CssClass = GridViewSpecialRowStyle;
                    break;
                case RowModeType.NotEditableSelected: //-2:
                    row.CssClass = GridViewSpecialSelectedRowStyle;
                    break;
                default:
                    //row.BackColor = (row.NamingContainer as GridView).SelectedRowStyle.BackColor;
                    row.CssClass = gv.SelectedRowStyle.CssClass;
                    break;
            }
        }
    }

    #endregion Esemenyek Utility

    private bool IsEloterjesztes
    {
        get
        {
            return Startup == Constants.Startup.FromKozgyulesiEloterjesztesek
            || Startup == Constants.Startup.FromBizottsagiEloterjesztesek;
        }
    }

    // BUG_6466
    private bool IratSzignalasElerheto()
    {
        return Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRAT_SZIGNALAS_ELERHETO, false);
    }

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName) ?? String.Empty;

        //if (Startup == "IktatasElokeszites" && Session["IktatasElokeszitesFormStartup"] == null)
        //{
        //    Session["IktatasElokeszitesFormStartup"] = Startup;
        //    Response.Clear();
        //    Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
        //}

        if ((Startup == Constants.Startup.SearchForm || Startup == Constants.Startup.FastSearchForm
            || Startup == "IktatasElokeszites")
            && (Session["IratokListStartup"] == null))
        {
            Session["IratokListStartup"] = Page.Request.QueryString.Get(Constants.Startup.StartupName);
            Response.Clear();
            Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
        }

        Mode = Request.QueryString.Get(QueryStringVars.Mode) ?? String.Empty;

        if (Mode == QueryStringVars.HivataliKapusMunkaUgyiratokIratai)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "HivataliKapusMunkaUgyiratokIrataiList");
        }
        else if (Mode == QueryStringVars.MunkaUgyiratokIratai)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MunkaUgyiratokIrataiList");
        }
        // CR3221 Szignálás(előkészítés) kezelés
        else if (Startup == Constants.Startup.FromMunkairatSzignalas)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "MunkairatSzignalas");
        }
        else
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IraIratokList");
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        if (IsEsemenyekSublistEnabled)
        {
            EsemenyekSubListHeader.RowCount_Changed += new EventHandler(EsemenyekSubListHeader_RowCount_Changed);

            EsemenyekSubListHeader.PagingButtonsClick += new EventHandler(EsemenyekSubListHeader_PagingButtonsClick);

            //EsemenyekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(EsemenyekSubListHeader_ErvenyessegFilter_Changed);
            EsemenyekSubListHeader.ValidFilterVisible = false;
        }

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, IraIratokGridView);

        if (IsEloterjesztes)
        {
            // spec. közgyűlési előterjesztés oszlopok láthatósága
            foreach (DataControlField dcf in IraIratokGridView.Columns)
            {
                if (dcf is BoundField)
                {
                    BoundField bf = (BoundField)dcf;

                    if (Array.IndexOf(InvisibleBoundFields_KozgyulesiEloterjesztes, bf.DataField) > -1)
                    {
                        bf.Visible = false;
                    }
                    else if (Array.IndexOf(VisibleBoundFields_KozgyulesiEloterjesztes, bf.DataField) > -1)
                    {
                        bf.Visible = true;
                    }
                }


                //Irat állapot eltüntetése
                if (dcf.SortExpression == "AllapotKodTarak.Nev")
                {
                    dcf.Visible = false;
                }
            }
        }
        else
        {
            // spec. közgyűlési előterjesztés oszlopok láthatósága
            foreach (DataControlField dcf in IraIratokGridView.Columns)
            {
                if (dcf is BoundField)
                {
                    BoundField bf = (BoundField)dcf;

                    if (Array.IndexOf(InvisibleBoundFields_KozgyulesiEloterjesztes, bf.DataField) > -1)
                    {
                        bf.Visible = true;
                    }
                    else if (Array.IndexOf(VisibleBoundFields_KozgyulesiEloterjesztes, bf.DataField) > -1)
                    {
                        bf.Visible = false;
                    }
                }
            }
        }

        // TÜK-ös mezők:
        this.SetGridTukColumns();

        // BLG_619
        if (Rendszerparameterek.GetBoolean(Page, "FELELOS_SZERV_KOD_MEGJELENITES", false))
        {
            UI.SetGridViewColumnVisiblity(IraIratokGridView, "IratFelelos_SzervezetKod", true);
        }
        else UI.SetGridViewColumnVisiblity(IraIratokGridView, "IratFelelos_SzervezetKod", false);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Mode == QueryStringVars.HivataliKapusMunkaUgyiratokIratai)
        {
            ListHeader1.HeaderLabel = "Hivatali kapuról érkeztetett főszámkezdeményező munkapéldányok";
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch;
        }
        else if (Mode == QueryStringVars.MunkaUgyiratokIratai)
        {
            ListHeader1.HeaderLabel = Resources.List.MunkaUgyiratokIrataiHeaderTitle;
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch;
        }

        else
        {
            if (Startup == Constants.Startup.FromAlairandok)
            {
                ListHeader1.HeaderLabel = Resources.List.IraIratok_Alairandok_ListHeaderTitle;
                //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
                ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.IratAlairandokSearch;
            }
            else if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
            {
                ListHeader1.HeaderLabel = Resources.List.IraIratok_KozgyulesiEloterjesztesek_ListHeaderTitle;
                //Sorrend fontos!!! 1. Custom Session Name 2. SearchObject Type (ez a template miatt kell)
                ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch;
            }
            else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
            {
                ListHeader1.HeaderLabel = Resources.List.IraIratok_BizottsagiEloterjesztesek_ListHeaderTitle;
                //Sorrend fontos!!! 1. Custom Session Name 2. SearchObject Type (ez a template miatt kell)
                ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch;
            }
            // CR3221 Szignálás(előkészítés) kezelés
            else if (Startup == Constants.Startup.FromMunkairatSzignalas)
            {
                ListHeader1.HeaderLabel = Resources.List.MunkairatSzignalasHeaderTitle;
                ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch;
            }
            // CR3289 Vezetői panel kezelés
            else if (Startup == Constants.Startup.FromVezetoiPanel_Irat)
            {
                ListHeader1.HeaderLabel = Resources.List.IraIratokListHeaderTitle;
                ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch;
            }
            else
            {
                ListHeader1.HeaderLabel = Resources.List.IraIratokListHeaderTitle;
            }
        }

        ListHeader1.SearchObjectType = typeof(EREC_IraIratokSearch);


        ListHeader1.NewVisible = false;
        ListHeader1.InvalidateVisible = false;

        #region CR3192 - Hatósági ügyben született iratok listája
        //LZS - BUG_11640
        bool isNMHH = FelhasznaloProfil.OrgIsNMHH(Page);
        if (isNMHH)
        {
            ListHeader1.CSVExportVisible =
            ListHeader1.CSVExportEnabled = false;
        }
        else
        {
            ListHeader1.CSVExportVisible = Startup != Constants.Startup.FromAlairandok; // BUG 6447 - aláírandók esetén ne látszódjon
            ListHeader1.CSVExportToolTip = Resources.Buttons.CSVExportHatosagiIratok;
        }

        #endregion
        //bernat.laszlo added : Excel Export (Grid)
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
        //bernat.laszlo eddig

        ListHeader1.SetRightFunctionButtonsVisible(false);
        if (Startup == Constants.Startup.FromAlairandok)
        {
            ListHeader1.NumericSearchVisible = false;

            ListHeader1.PrintVisible = false;
            ListHeader1.SSRSPrintVisible = true;

            //ListHeader1.BejovoIratIktatasVisible = false;
            //ListHeader1.BelsoIratIktatasVisible = false;

            ListHeader1.AtIktatasVisible = false;

            ListHeader1.MunkapeldanyIktatasaVisible = false;
            ListHeader1.IktatasVisible = false;
            ListHeader1.CsatolasVisible = false;
            ListHeader1.FelszabaditasVisible = false;

            #region Hatósági ügyben született iratok listája
            ListHeader1.CSVExportVisible = false;
            #endregion
        }
        else
        {
            ListHeader1.NumericSearchVisible = true;

            ListHeader1.PrintVisible = true;
            ListHeader1.SSRSPrintVisible = true;
            ListHeader1.UgyiratTerkepVisible = true;

            //ListHeader1.BejovoIratIktatasVisible = true;
            //ListHeader1.BelsoIratIktatasVisible = true;

            ListHeader1.AtIktatasVisible = true;
            ListHeader1.ValaszIratVisible = true;
            //CR3128
            ListHeader1.IratpeldanyLetrehozasVisible = true;
            //CR3128
            //CR3129
            //BUG#4751: "Feladat: leszedni az Alszámra iktatott iratok listájáról mind a sima Aláírás, mind a Tömeges aláírás gombot."
            ListHeader1.IratCsatolmanyAlairasVisible = false;
            //CR3129
            //BL 63.Task:299
            //BUG#4751: "Feladat: leszedni az Alszámra iktatott iratok listájáról mind a sima Aláírás, mind a Tömeges aláírás gombot."
            ListHeader1.TomegesIratCsatolmanyAlairasVisible = false;
            ListHeader1.MunkapeldanyIktatasaVisible = true;
            ListHeader1.IktatasVisible = true;
            ListHeader1.CsatolasVisible = true;
            ListHeader1.FelszabaditasVisible = true;
            ListHeader1.ElintezetteNyilvanitasVisible = true;
            ListHeader1.ElintezetteNyilvanitasVisszavonVisible = false;
            ListHeader1.ElintezetteNyilvanitasTomegesVisible = true;

            // CR3137 : Munkaügyiratok listáján kinn legyen az átadásra kijelölés gomb
            if (Mode == QueryStringVars.MunkaUgyiratokIratai)
            {
                ListHeader1.AtadasraKijelolVisible = true;
                ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                  + IraIratokGridView.ClientID + "','check'); "
                  + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
                ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtadas");
            }

            ListHeader1.ExpedialasVisible = true;
            ListHeader1.TomegesOlvasasiJogVisible = true;
        }

        if (IsEloterjesztes)
        {
            ListHeader1.AtadasraKijelolVisible = true;
            ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + IraIratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
            ListHeader1.AtadasraKijelolEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtadas");
            ListHeader1.AtvetelVisible = true;
            ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + IraIratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
            ListHeader1.AtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "IratPeldanyAtvetel");
        }

        // BUG_6466
        bool iratSzignalasElerheto = IratSzignalasElerheto();
        ListHeader1.SzignalasVisible = iratSzignalasElerheto;
        ListHeader1.TomegesSzignalasVisible = iratSzignalasElerheto;

        if (iratSzignalasElerheto)
        { // BUG_6466
            ListHeader1.SzignalasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.TomegesSzignalasOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraIratokGridView.ClientID);
        }

        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        if (IsEsemenyekSublistEnabled)
        {
            JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        }

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);


        if (Mode == QueryStringVars.HivataliKapusMunkaUgyiratokIratai)
        {
            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai,
            "", Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        }
        else if (Mode == QueryStringVars.MunkaUgyiratokIratai)
        {
            ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromMunkaUgyiratokIratai
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromMunkaUgyiratokIratai,
            "", Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        }

        else
        {
            if (Startup == Constants.Startup.FromAlairandok)
            {
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", Constants.Startup.StartupName + "=" + Startup
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else if (IsEloterjesztes)
            {
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", Constants.Startup.StartupName + "=" + Startup
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Startup,
                    "", Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            // CR3221 Szignálás(előkészítés) kezelés
            else if (Startup == Constants.Startup.FromMunkairatSzignalas)
            {
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromMunkairatSzignalas
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromMunkairatSzignalas,
                "", Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            // CR3289 Vezeti Panel kezelés
            else if (Startup == Constants.Startup.FromVezetoiPanel_Irat)
            {
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromVezetoiPanel_Irat
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromVezetoiPanel_Irat,
                "", Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", ""
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromIrat,
            "", Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }

        }


        //ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx", QueryStringVars.Command + "=" + CommandName.New
        //    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraIratokGridView.ClientID);

        ListHeader1.ElintezetteNyilvanitasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ElintezetteNyilvanitasTomegesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                      + IraIratokGridView.ClientID + "','check'); "
                                      + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        #region CR3192 - Hatósági ügyben született iratok listája
        //ListHeader1.CSVExportOnClientClick = "var count = getSelectedCheckBoxesCount('"
        //        + IraIratokGridView.ClientID + "','check'); "
        //        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        ListHeader1.CSVExportOnClientClick = "javascript:window.open('IraIratokSSRSHatosagi.aspx');return false;";
        // ListHeader1.CSVExportToolTip = Resources.List.;

        #endregion

        //ListHeader1.BelsoIratIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.AtIktatasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.ValaszIratOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.MunkapeldanyIktatasaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        #region CR3128 -Iratpéldány létrehozás nyomógomb az iratok listájára

        ListHeader1.IratpeldanyLetrehozasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        #endregion CR3128 -Iratpéldány létrehozás nyomógomb az iratok listájára

        #region CR3129 - Szükség lenne egy új ikonra az aláíráshoz

        ListHeader1.IratCsatolmanyAlairasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        #endregion CR3129 - Szükség lenne egy új ikonra az aláíráshoz

        #region BL63.Task299 - Szükség lenne egy új ikonra a tömeges aláíráshoz

        ListHeader1.TomegesIratCsatolmanyAlairasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + IraIratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        #endregion
        ListHeader1.IktatasOnClientClick = JavaScripts.SetOnClientClick("IktatasElokeszitesForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.New
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IraIratokListajaPrintForm.aspx?"
            + "&" + Constants.Startup.StartupName + "=" + Startup);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraIratokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraIratokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraIratokGridView.ClientID);

        //ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx",
        //  QueryStringVars.Command + "=" + CommandName.New
        //  , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID,EventArgumentConst.refreshMasterList);

        //ListHeader1.BelsoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx",
        //    QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
        //    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);


        ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.FelszabaditasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraIratokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, "", true);

        ListHeader1.ExpedialasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.TomegesOlvasasiJogOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + IraIratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = IraIratokGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraIratokSearch(true));

        //if (!IsPostBack) IraIratokGridViewBind();

        if (IsEsemenyekSublistEnabled)
        {
            // Iratmozgási események
            tr_Esemenyek.Visible = true;

            //EsemenyekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            EsemenyekSubListHeader.NewVisible = false;
            //EsemenyekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            EsemenyekSubListHeader.ViewVisible = false;
            //EsemenyekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            EsemenyekSubListHeader.ModifyVisible = false;
            //EsemenyekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(EsemenyekGridView.ClientID);
            EsemenyekSubListHeader.InvalidateVisible = false;
            EsemenyekSubListHeader.ButtonsClick += new CommandEventHandler(EsemenyekSubListHeader_ButtonsClick);

            //selectedRecordId kezelése
            EsemenyekSubListHeader.AttachedGridView = EsemenyekGridView;
        }
        else
        {
            tr_Esemenyek.Visible = false;
        }

        // BUG_8003
        bool iratIntezkedesreAtvetelElerheto = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRAT_INTEZKEDESREATVETEL_ELERHETO, false);
        ListHeader1.IratAtvetelIntezkedesreVisible = iratIntezkedesreAtvetelElerheto;
        if (iratIntezkedesreAtvetelElerheto)
        {
            ListHeader1.IratAtvetelIntezkedesreEnabled = FunctionRights.GetFunkcioJog(Page, "IratAtvetelIntezkedesre");
            if (ListHeader1.IratAtvetelIntezkedesreEnabled)
            {
                ListHeader1.IratAtvetelIntezkedesreOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                        + IraIratokGridView.ClientID + "','check'); "
                                        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
            }
        }

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);

        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["IratokListStartup"] != null &&
            (Session["IratokListStartup"].ToString() == Constants.Startup.SearchForm
            || Session["IratokListStartup"].ToString() == Constants.Startup.FastSearchForm
            || Session["IratokListStartup"].ToString() == "IktatasElokeszites"))
        {
            string script = "OpenNewWindow(); function OpenNewWindow() { ";
            if (Session["IratokListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("IraIratokSearch.aspx", ""
                , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                , CustomUpdateProgress1.UpdateProgress.ClientID);
            }
            else if (Session["IratokListStartup"].ToString() == Constants.Startup.FastSearchForm)
            {
                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyiratokNumericSearch.aspx"
                    , Constants.Startup.StartupName + "=" + Constants.Startup.FromIrat, Defaults.PopupWidth, Defaults.PopupHeight
                    , IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, CustomUpdateProgress1.UpdateProgress.ClientID);
            }
            else if (Session["IratokListStartup"].ToString() == "IktatasElokeszites")
            {
                script +=
                    JavaScripts.SetOnClientClick("IktatasElokeszitesForm.aspx", QueryStringVars.Command + "=" + CommandName.New
                , 1024, 768, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            }
            script += "}";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "IratokListStartup", script, true);
            Session.Remove("IratokListStartup");

            popupNyitas = true;
        }

        // BLG_7742
        bool ugyintezesOszlopokLathatoak = !Rendszerparameterek.GetBoolean(Page, "UGYINTEZES_OSZLOPOK_ELREJTESE", false);
        UI.SetGridViewColumnVisiblity(IraIratokGridView, "IratHatasaUgyintezesreNev", ugyintezesOszlopokLathatoak);
        UI.SetGridViewColumnVisiblity(IraIratokGridView, "EljarasiSzakaszFokNev", ugyintezesOszlopokLathatoak);

        UI.SetGridViewTemplateColumnVisiblity(IraIratokGridView, "IH", ugyintezesOszlopokLathatoak); // IratHatasaUgyintezesreTipus
        UI.SetGridViewTemplateColumnVisiblity(IraIratokGridView, "Hátralévő napok", ugyintezesOszlopokLathatoak); // HatralevoNapok

        if (Rendszerparameterek.GetInt(Page, Rendszerparameterek.UGYINTEZESI_HATARIDO_MASOLAS_UGYROL) == 1)
        {
            UI.SetGridViewTemplateColumnVisiblity(IraIratokGridView, "Hátralévő munkanapok", false);
        }
        else
        {
            UI.SetGridViewTemplateColumnVisiblity(IraIratokGridView, "Hátralévő munkanapok", ugyintezesOszlopokLathatoak);
        }

        // Ha kereső képernyőt is kell nyitni, nem töltjük fel a listát:
        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            IraIratokGridViewBind();
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.New);
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.Modify);
        ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.View);
        //ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.Invalidate);
        //bernat.laszlo added
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.ExcelExport);
        //bernat.laszlo eddig
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.ViewHistory);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        bool iratSzignalasElerheto = IratSzignalasElerheto() && FunctionRights.GetFunkcioJog(Page, "IratSzignalas"); // BUG_6466
        ListHeader1.SzignalasEnabled = iratSzignalasElerheto;
        ListHeader1.TomegesSzignalasEnabled = iratSzignalasElerheto;

        //ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "BejovoIratIktatas");
        //ListHeader1.BelsoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas");

        if (IsEsemenyekSublistEnabled)
        {
            EsemenyekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Esemeny" + CommandName.New);
            EsemenyekSubListHeader.ViewEnabled = false;
            EsemenyekSubListHeader.ModifyEnabled = false;
            EsemenyekSubListHeader.InvalidateEnabled = false;
        }

        if (Startup != Constants.Startup.FromAlairandok)
        {
            ListHeader1.AtIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "AtIktatas");

            ListHeader1.ValaszIratEnabled = FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas");

            ListHeader1.IktatasEnabled = FunctionRights.GetFunkcioJog(Page, "IktatasElokeszites");

            // VAGY MunkapeldanyBeiktatas_Bejovo, VAGY MunkapeldanyBeiktatas_Belso funkciójog kell:
            ListHeader1.MunkapeldanyIktatasaEnabled = FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Bejovo")
                    || FunctionRights.GetFunkcioJog(Page, "MunkapeldanyBeiktatas_Belso");

            ListHeader1.CsatolasEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratCsatolasNew");

            ListHeader1.FelszabaditasEnabled = FunctionRights.GetFunkcioJog(Page, "IratFelszabaditas");

            ListHeader1.ElintezetteNyilvanitasEnabled = FunctionRights.GetFunkcioJog(Page, "IratElintezes");
            ListHeader1.ElintezetteNyilvanitasVisszavonEnabled = FunctionRights.GetFunkcioJog(Page, "IratElintezesVisszavonas");
            ListHeader1.ElintezetteNyilvanitasTomegesEnabled = ListHeader1.ElintezetteNyilvanitasEnabled;

            ListHeader1.ExpedialasEnabled = FunctionRights.GetFunkcioJog(Page, "Expedialas");
            ListHeader1.TomegesOlvasasiJogEnabled = FunctionRights.GetFunkcioJog(Page, "TomegesOlvasasiJog");

        }
        else
        {
            ListHeader1.AtIktatasEnabled = false;
            ListHeader1.ValaszIratEnabled = false;
            ListHeader1.IktatasEnabled = false;
            ListHeader1.MunkapeldanyIktatasaEnabled = false;
            ListHeader1.CsatolasEnabled = false;
            ListHeader1.FelszabaditasEnabled = false;
            //CR3129
            ListHeader1.IratCsatolmanyAlairasVisible = true;
            //CR3129
            //BL 63.Task:299
            ListHeader1.TomegesIratCsatolmanyAlairasVisible = true;
        }

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "IraIrat" + CommandName.Lock);

        ////if (IsPostBack)
        //{
        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraIratokGridView);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        if (IsEsemenyekSublistEnabled)
        {
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            ActiveTabRefreshOnClientClicks();
            // ez itt megakadályozná a detail gridben a szerkesztést (megjegyzések hozzáadását)
            //ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
        //}

        ui.SetClientScriptToGridViewSelectDeSelectButton(IraIratokGridView);

        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IraIratokGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.IratokListColumnNames().GetType()).ToCustomString();

        if (Mode == QueryStringVars.MunkaUgyiratokIratai)
        {
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSMunkaugyiratokIratai] = visibilityState;
            ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRSMunkaugyiratokIratai.aspx", string.Empty);
        }
        else if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
        {
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSKozgyulesiEloterjesztesek] = visibilityState;

            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRSKozgyulesiEloterjesztesek.aspx", string.Empty);
            }
            else
            {
                String sortExpression = Search.GetSortExpressionFromViewState("EsemenyekGridView", ViewState, "IratMozgas.HistoryVegrehajtasIdo");
                SortDirection sortDirection = Search.GetSortDirectionFromViewState("EsemenyekGridView", ViewState);
                ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRSKozgyulesiEloterjesztesekIratmozgas.aspx"
                    , QueryStringVars.Id + "=" + MasterListSelectedRowId +
                    "&" + QueryStringVars.OrderBy + "=" + Search.GetOrderBy("EsemenyekGridView", ViewState, sortExpression, sortDirection)
                    );
            }
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
        {

            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSBizottsagiEloterjesztesek] = visibilityState;

                ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRSBizottsagiEloterjesztesek.aspx"
                    , string.Empty);
            }
            else
            {
                Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSBizottsagiEloterjesztesekIratmozgas] = visibilityState;

                String sortExpression = Search.GetSortExpressionFromViewState("EsemenyekGridView", ViewState, "IratMozgas.HistoryVegrehajtasIdo");
                SortDirection sortDirection = Search.GetSortDirectionFromViewState("EsemenyekGridView", ViewState);
                ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRSBizottsagiEloterjesztesekIratmozgas.aspx"
                    , QueryStringVars.Id + "=" + MasterListSelectedRowId +
                    "&" + QueryStringVars.OrderBy + "=" + Search.GetOrderBy("EsemenyekGridView", ViewState, sortExpression, sortDirection)
                    );
            }
        }

        else if (Mode == QueryStringVars.HivataliKapusMunkaUgyiratokIratai)
        {
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRS] = visibilityState;
            ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRS.aspx"
                , QueryStringVars.Id + "=" + MasterListSelectedRowId + "&" + Constants.Startup.StartupName + "=" + Startup + "&Title=" + ListHeader1.HeaderLabel);
        }

        else
        {
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRS] = visibilityState;
            ListHeader1.SSRSPrintOnClientClick = JavaScripts.SetSSRSPrintOnClientClick("IraIratokSSRS.aspx"
                , Constants.Startup.StartupName + "=" + Startup + "&Title=" + ListHeader1.HeaderLabel);
        }

        // BUG_8523
        var isBOPMH = FelhasznaloProfil.OrgIsBOPMH(Page);
        ListHeader1.HatosagiTomegesEnabled = isBOPMH;
        ListHeader1.HatosagiTomegesVisible = isBOPMH;

        if (ListHeader1.HatosagiTomegesEnabled)
        {
            ListHeader1.HatosagiTomegesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                    + IraIratokGridView.ClientID + "','check'); "
                                    + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }
    }

    #endregion

    #region Master List

    public string GetDefaultSortExpression()
    {
        string sortExpression;

        if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
        {
            //Kögyűlési előterjesztéseknél az alapértelmezett rendezés közgyűlés tervezett időpontja szerint csökkenő, iktatás dátuma csökkenő
            sortExpression = "EREC_ObjektumTargyszavai.[Közgyűlés tervezett időpontja] DESC, EREC_IraIratok.IktatasDatuma";
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
        {
            //Kögyűlési előterjesztéseknél az alapértelmezett rendezés bizottsági ülés tervezett időpontja szerint csökkenő, iktatás dátuma csökkenő
            sortExpression = "EREC_ObjektumTargyszavai.[Bizottsági ülés tervezett időpontja] DESC, EREC_IraIratok.IktatasDatuma";
        }
        else
        {
            // BUG_6459
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.LISTA_RENDEZETTSEG_IKTATOSZAM, false))
            {
                sortExpression = Search.FullSortExpression + "EREC_IraIktatokonyvek.Ev DESC, EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam";
            }
            else
            {
                sortExpression = "EREC_IraIratok.LetrehozasIdo";
            }
        }

        return sortExpression;
    }

    protected void IraIratokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraIratokGridView", ViewState, this.GetDefaultSortExpression());
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraIratokGridView", ViewState, SortDirection.Descending);    // alapértelmezésnél most csak a sorszámra vonatkozik...

        IraIratokGridViewBind(sortExpression, sortDirection);
    }

    /// <summary>
    /// Adatkötés
    /// </summary>
    protected void IraIratokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponent.ClearDokumentElements();

        // BUG_8359
        if (SortExpression == Contentum.eUtility.Search.FullSortExpression)
        {
            SortExpression = this.GetDefaultSortExpression();
            SortDirection = SortDirection.Descending;
        }

        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraIratokSearch search = null;

        if (!IsPostBack)
        {
            if (Startup == Constants.Startup.FromVezetoiPanel_Irat) // ha vezetoi panelról indítottuk
            {
                Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
            }
        }

        if (Mode == QueryStringVars.HivataliKapusMunkaUgyiratokIratai)
        {
            // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch))
            {
                search = new EREC_IraIratokSearch(true);
            }
            else
            {
                search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch);
            }

            if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                )
            {
                search.Manual_Alszam_Sztornozott.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);
            }

            search.Manual_UgyiratFoszam_MunkaanyagFilter.IsNull();

            //Hivatali kapuról érkezett
            search.Extended_EREC_KuldKuldemenyekSearch.KuldesMod.In(new string[] { KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu, KodTarak.KULDEMENY_KULDES_MODJA.Elhisz });
        }
        else if (Mode == QueryStringVars.MunkaUgyiratokIratai)
        {
            // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch))
            {
                search = Search.GetDefaultSearchObject_EREC_IraIratokSearchForMunkaUgyiratok(Page,true);
                Search.SetSearchObject(Page, search);
            }
            else
            {
                search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch);
            }

            if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                )
            {
                search.Manual_Alszam_Sztornozott.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);
            }

            search.Manual_UgyiratFoszam_MunkaanyagFilter.IsNull();

            //Nem Hivatali kapuról érkezett
            search.Extended_EREC_KuldKuldemenyekSearch.WhereByManual = "IsNULL(Erec_KuldKuldemenyek.KuldesMod,'0') != " + KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
        }
        // CR3221 Szignálás(előkészítés) kezelés
        else if (Startup == Constants.Startup.FromMunkairatSzignalas)
        {
            // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
            if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch))
            {
                search = new EREC_IraIratokSearch(true);
            }
            else
            {
                search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch);
            }

            if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                )
            {
                search.Manual_Alszam_Sztornozott.NotEquals(KodTarak.UGYIRAT_ALLAPOT.Sztornozott);
            }
            search.Manual_Alszam_MunkaanyagFilter.IsNull();
            //search.Manual_UgyiratFoszam_MunkaanyagFilter.Operator = Query.Operators.isnull;

            ////Nem Hivatali kapuról érkezett
            //search.Extended_EREC_KuldKuldemenyekSearch.WhereByManual = "IsNULL(Erec_KuldKuldemenyek.KuldesMod,'0') != " + KodTarak.KULDEMENY_KULDES_MODJA.HivataliKapu;
        }
        else
        {
            if (Startup == Constants.Startup.FromAlairandok)
            {
                search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.IratAlairandokSearch);

                // Aláírandó állapotú és a felhasználó szerepel az aláírók között
                search.Extended_EREC_IratAlairokSearch.Allapot.Filter(KodTarak.IRATALAIRAS_ALLAPOT.Alairando);
                //search.Allapot.Value = KodTarak.IRAT_ALLAPOT.Atiktatott;
                //search.Allapot.Operator = Query.Operators.notequals;
                search.Extended_EREC_IratAlairokSearch.FelhasznaloCsoport_Id_Alairo.Filter(FelhasznaloProfil.FelhasznaloId(Page));
                search.Extended_EREC_IratAlairokSearch.FelhasznaloCsoport_Id_Alairo.OrGroup("alairo");

                search.Extended_EREC_IratAlairokSearch.FelhaszCsop_Id_HelyettesAlairo.Filter(FelhasznaloProfil.FelhasznaloId(Page));
                search.Extended_EREC_IratAlairokSearch.FelhaszCsop_Id_HelyettesAlairo.OrGroup("alairo");

                string alairasVisszautasitasKezelese = Rendszerparameterek.Get(Page, Rendszerparameterek.ALAIRAS_VISSZAUTASITAS_KEZELES);
                search.Extended_EREC_IratAlairokSearch.WhereByManual = " AND dbo.fn_isElozetesAlairasAlairando(EREC_IratAlairok.Id, " + alairasVisszautasitasKezelese + ") > 0 AND dbo.fn_isAlairhato(EREC_IratAlairok.Id, " + alairasVisszautasitasKezelese + ") = 0";
                if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                )
                {
                    search.Manual_Alszam_Sztornozott.NotIn(new string[]
                        { KodTarak.UGYIRAT_ALLAPOT.Sztornozott, KodTarak.IRAT_ALLAPOT.Atiktatott });
                }

                // BUG_6209
                if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ELINTEZETTE_NYILV_KIADM_FELTETELE, false))
                {
                    // BUG_8805
                    //search.Extended_EREC_IratAlairokSearch.WhereByManual += " AND dbo.fn_KiadmanyozasElintezettVizsgalat(EREC_IratAlairok.Id) > 0";
                    search.Extended_EREC_IratAlairokSearch.WhereByManual += " AND dbo.fn_KiadmanyozasElintezettVizsgalat(EREC_IratAlairok.Obj_id) > 0";
                }

                #region érvényesség szűrés
                search.Extended_EREC_IratAlairokSearch.ErvKezd.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                search.Extended_EREC_IratAlairokSearch.ErvKezd.Operator = Contentum.eQuery.Query.Operators.lessorequal;
                search.Extended_EREC_IratAlairokSearch.ErvKezd.Group = "111";
                search.Extended_EREC_IratAlairokSearch.ErvKezd.GroupOperator = Contentum.eQuery.Query.Operators.and;

                search.Extended_EREC_IratAlairokSearch.ErvVege.Value = Contentum.eQuery.Query.SQLFunction.getdate;
                search.Extended_EREC_IratAlairokSearch.ErvVege.Operator = Contentum.eQuery.Query.Operators.greaterorequal;
                search.Extended_EREC_IratAlairokSearch.ErvVege.Group = "111";
                search.Extended_EREC_IratAlairokSearch.ErvVege.GroupOperator = Contentum.eQuery.Query.Operators.and;
                #endregion érvényesség szűrés


                // sessionbe mentés
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.IratAlairandokSearch);
            }
            else if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
            {
                // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
                if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch))
                {
                    search = (EREC_IraIratokSearch)Search.CreateSearchObjWithSpecFilter_Irat_KozgyulesiEloterjesztes(Page);

                    if (!Search.IsDefaultSearchObject(typeof(EREC_IraIratokSearch), search))
                    {
                        // sessionbe mentés
                        Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch);
                    }
                }
                else
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch);
                }
            }
            else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
            {
                // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
                if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch))
                {
                    search = (EREC_IraIratokSearch)Search.CreateSearchObjWithSpecFilter_Irat_BizottsagiEloterjesztes(Page);

                    if (!Search.IsDefaultSearchObject(typeof(EREC_IraIratokSearch), search))
                    {
                        // sessionbe mentés
                        Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch);
                    }
                }
                else
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch);
                }
            }
            // CR3289 Vezetői panel kezelés
            else if (Startup == Constants.Startup.FromVezetoiPanel_Irat)
            {
                // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
                if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch))
                {
                    search = (EREC_IraIratokSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_IraIratokSearch), Page);

                    Iratok.FilterObject filterObject = new Iratok.FilterObject();
                    filterObject.QsDeserialize(Page.Request.QueryString);
                    filterObject.SetSearchObject(ExecParam, ref search);

                    if (!Search.IsDefaultSearchObject(typeof(EREC_IraIratokSearch), search))
                    {
                        // sessionbe mentés
                        Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                    }
                }
                else
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                    Iratok.FilterObject filterObject = new Iratok.FilterObject();
                    filterObject.QsDeserialize(Page.Request.QueryString);
                    filterObject.SetSearchObject(ExecParam, ref search);
                    Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                }
            }
            else
            {
                // Keresési objektum kivétele a sessionből; ha nincs benne, a default szűréssel beletesszük
                if (!Search.IsSearchObjectInSession(Page, typeof(EREC_IraIratokSearch)))
                {
                    search = (EREC_IraIratokSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_IraIratokSearch), Page);
                    var defaultSearch = new EREC_IraIratokSearch(true);
                    defaultSearch.CsatolmanySzures = search.CsatolmanySzures; // BUG_6181
                    // sessionbe mentés
                    Search.SetSearchObject(Page, search);
                }
                else
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject(Page, new EREC_IraIratokSearch(true));
                }
            }
        }

        search.OrderBy = Search.GetOrderBy("IraIratokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        // Lapozás beállítása:
        UI.SetPaging(ExecParam, ListHeader1);

        Result res = null;
        if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
        {
            res = service.GetAllWithExtensionAndMetaAndJogosultak(ExecParam, search
                , null, null
                , Constants.ColumnNames.EREC_IraIratok.Irattipus, new string[] { KodTarak.IRATTIPUS.KozgyulesiEloterjesztes }, false
                , null, false, false
                , true);

            AddMetaColumnsToGridView(res, IraIratokGridView, true);
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
        {
            res = service.GetAllWithExtensionAndMetaAndJogosultak(ExecParam, search
                , null, null
                , Constants.ColumnNames.EREC_IraIratok.Irattipus, new string[] { KodTarak.IRATTIPUS.BizottsagiEloterjesztes }, false
                , null, false, false
                , true);

            AddMetaColumnsToGridView(res, IraIratokGridView, true);
        }
        // BUG_8847
        else if (Startup == Constants.Startup.FromVezetoiPanel_Irat)
        {
            res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, false);

            // AddMetaColumnsToGridView(res, IraIratokGridView, true);
        }
        else
        {
            //LZS - BLG_3216
            if (string.IsNullOrEmpty(search.CsatolmanySzures.Value))
            {
                search.CsatolmanySzures.Value = KodTarak.CSATOLMANY_VIZSGALAT.MIND;
            }

            res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);
            //res = service.GetAllWithExtensionAndMetaAndJogosultak(ExecParam, search
            //    , null, null
            //    , null, null, false
            //    , "B3", false, false
            //    , true);

            //AddMetaColumnsToGridView(res, IraIratokGridView, false);
            //Result res = service.GetAllWithExtension(ExecParam, search);
        }

        // zárolás oszlop hozzáadása a grid végére (image field: a template field template-jeitől eltérően
        // nem vész el a postback során, kivéve a generált Image kódból definiált ID-ját)
        // modified nekrisz : nem minden esetben került fel a zárolás oszlop, és ezért a Report túlindexelt (CR 3027 kapcsán)
        bool found = false;
        for (int i = 0; i < IraIratokGridView.Columns.Count && !found; i++)
            found = (IraIratokGridView.Columns[i].HeaderText == Resources.Buttons.Zarolas);
        if (!found)
        //   if (!IsPostBack)  
        {
            ImageField imageField = new ImageField();
            imageField.HeaderStyle.CssClass = "GridViewLockedImage";
            imageField.ItemStyle.CssClass = "GridViewLockedImage";
            imageField.HeaderImageUrl = "~/images/hu/egyeb/locked.gif";
            imageField.HeaderText = Resources.Buttons.Zarolas; // "Z&aacute;rol&aacute;s"

            imageField.DataImageUrlField = "Zarolo_id";
            imageField.DataImageUrlFormatString = "~/images/hu/egyeb/locked.gif";
            imageField.AccessibleHeaderText = lockColumnName;

            IraIratokGridView.Columns.Add(imageField);

            LockColumnIndex = IraIratokGridView.Columns.Count - 1;
        }

        try
        {
            if (res.GetCount < 1)
            {
                if (res.Ds != null)
                    res.Ds.Clear();
                UI.GridViewFill(IraIratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
            }
            else
            {
                UI.GridViewFill(IraIratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
            }
        }
        catch (Exception exc)
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba", exc.Message);
            ErrorUpdatePanel.Update();
        }
    }

    protected void IraIratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetVegyesInfo(e);
        GridView_RowDataBound_SetCsatolasInfo(e);
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        GridView_RowDataBound_SetLockingInfoForImageField(e, Page, this.LockColumnIndex);
        UI.SetFeladatInfo(e, Constants.TableNames.EREC_IraIratok);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }
        System.Data.DataRowView dataRowView = (System.Data.DataRowView)e.Row.DataItem;
        string csatolmanyCount = String.Empty;

        if (dataRowView.Row.Table.Columns.Contains("CsatolmanyCount") && dataRowView["CsatolmanyCount"] != null)
        {
            csatolmanyCount = dataRowView["CsatolmanyCount"].ToString();
        }
        ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");

        if (CsatolmanyImage == null)
        {
            return;
        }
        if (string.IsNullOrEmpty(csatolmanyCount))
        {
            CsatolmanyImage.OnClientClick = "";
            CsatolmanyImage.Visible = false;
            return;
        }


        CsatolmanyImage.OnClientClick = "";
        int count = 0;
        Int32.TryParse(csatolmanyCount, out count);

        if (count == 0)
        {
            CsatolmanyImage.Visible = false;
            return;
        }

        //tárolt eljárás vizsgálja
        //#region BLG_577
        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
        //{   /*NINCS JOGA*/
        //    CsatolmanyImage.Visible = false;
        //    return;
        //}
        //#endregion

        CsatolmanyImage.Visible = true;

        bool skipRightCheck = Startup == Constants.Startup.FromAlairandok;
        string iratHelye = dataRowView.Row["FelhasznaloCsoport_Id_Orzo"] == null ? null : dataRowView.Row["FelhasznaloCsoport_Id_Orzo"].ToString();
        if (!skipRightCheck && !Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, iratHelye))
        {
            CsatolmanyImage.ToolTip = Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight;
            CsatolmanyImage.Enabled = false;
            CsatolmanyImage.OnClientClick = "";
            return;
        }

        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

        if (count == 1 && dataRowView.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(dataRowView["Dokumentum_Id"].ToString()))
        {
            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", dataRowView["Dokumentum_Id"]);
            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, dataRowView["Dokumentum_Id"].ToString());
        }
        else
        {
            // set "link"
            string id = "";

            if (dataRowView["Id"] != null)
            {
                id = dataRowView["Id"].ToString();
            }

            if (!String.IsNullOrEmpty(id))
            {
                // Modify->View átirányítás jog szerint a formon, ezért itt nem vizsgáljuk feleslegesen
                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                    , Defaults.PopupWidth_1360, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
        }
    }

    private void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }
        System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
        string csatolasCount = String.Empty;

        if (drw["CsatolasCount"] != null)
        {
            csatolasCount = drw["CsatolasCount"].ToString();
        }
        Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

        if (string.IsNullOrEmpty(csatolasCount))
        {
            if (CsatoltImage != null)
            {
                CsatoltImage.Visible = false;
            }
            return;
        }

        if (CsatoltImage == null)
        {
            CsatoltImage.Visible = false;
        }
        CsatoltImage.Visible = false;
        int count = 0;
        Int32.TryParse(csatolasCount, out count);
        if (count > 0)
        {
            CsatoltImage.Visible = true;
            CsatoltImage.ToolTip = String.Format("Kapcsolt: {0} darab", csatolasCount);
            string qsStartup = (String.IsNullOrEmpty(Startup) ? String.Empty : String.Format("&{0}={1}", QueryStringVars.Startup, Startup));
            if (drw["Id"] != null)
            {
                string iratId = drw["Id"].ToString();
                string onclick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + iratId +
                "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolasTab + qsStartup
                 , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID);
                CsatoltImage.Attributes.Add("onclick", onclick);

            }
        }
    }

    protected void IraIratokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, IraIratokCPE);
        ListHeader1.RefreshPagerLabel();
    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraIratokGridViewBind();
        //ActiveTabClear();
        if (IsEsemenyekSublistEnabled)
        {
            //ActiveTabClear();
        }
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, IraIratokCPE);
        IraIratokGridViewBind();
        if (IsEsemenyekSublistEnabled)
        {
            ActiveTabClear();
        }
    }

    protected void IraIratokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraIratokGridView, selectedRowNumber, "check");

            if (IsEsemenyekSublistEnabled)
            {
                string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
                ActiveTabRefresh(TabContainer1, id);
            }

        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID);
            string tableName = "EREC_IraIratok";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraIratokUpdatePanel.ClientID);

            #region irat, ügyirat státuszának lekérése:

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            //Result result_iratGet = iratService.Get(execParam);
            //if (!String.IsNullOrEmpty(result_iratGet.ErrorCode))
            //{
            //    // hiba:
            //    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1,result_iratGet);
            //    ErrorUpdatePanel.Update();
            //}

            EREC_IraIratok obj_irat = null;
            EREC_UgyUgyiratdarabok obj_ugyiratDarab = null;
            EREC_UgyUgyiratok obj_ugyirat = null;
            EREC_PldIratPeldanyok obj_elsoIratPeldany = null;

            Iratok.GetObjectsHierarchyByIrat(execParam, id, out obj_irat, out obj_ugyiratDarab, out obj_ugyirat, out obj_elsoIratPeldany, EErrorPanel1);

            if (obj_irat == null || obj_ugyirat == null)
            {
                // hiba:
                return;
            }

            Iratok.Statusz iratStatusz = Iratok.GetAllapotByBusinessDocument(obj_irat, obj_elsoIratPeldany);
            UgyiratDarabok.Statusz ugyiratDarabStatusz = UgyiratDarabok.GetAllapotByBusinessDocument(obj_ugyiratDarab);
            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(obj_ugyirat);
            ErrorDetails errorDetail;

            #endregion

            bool bElintezett = Iratok.isElintezett(iratStatusz, out errorDetail);

            if (bElintezett)
            {
                ListHeader1.ElintezetteNyilvanitasVisible = false;
                ListHeader1.ElintezetteNyilvanitasVisszavonVisible = true;
                ListHeader1.ElintezetteNyilvanitasVisszavonOnClientClick = JavaScripts.SetOnClientClickElintezesVisszavonasConfirm();
            }
            else
            {
                ListHeader1.ElintezetteNyilvanitasEnabled &= !Iratok.IsIratIntezkedesreAtvetelElerheto(execParam) || iratStatusz.Allapot == KodTarak.IRAT_ALLAPOT.IntezkedesAlatt;
                ListHeader1.ElintezetteNyilvanitasVisible = true;
                ListHeader1.ElintezetteNyilvanitasVisszavonVisible = false;
                ListHeader1.ElintezetteNyilvanitasOnClientClick =
              JavaScripts.SetOnClientClick("IratElintezesForm.aspx", QueryStringVars.Id + "=" + id + "&" + CommandName.Command + "=" + CommandName.Modify
               , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }

            bool bIratModosithato = Iratok.Modosithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail);
            // Módosítás
            if (!bIratModosithato)
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_IratMegtekintes
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" +
                    JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                        + "} else return false;";

                // ügyirattérkép View módban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                       + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID);

            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                // ügyirattérkép Modify módban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                       + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID);

            }

            if (Iratok.AtIktathato(iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.AtIktatasOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.AtIktatas
                          + "&" + QueryStringVars.IratId + "=" + id
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.AtIktatasOnClientClick = "alert('" + Resources.Error.ErrorCode_52621
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            if (Iratok.ValaszIratIktathato(obj_irat, out errorDetail))
            {
                ListHeader1.ValaszIratOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.New
                         + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
                         + "&" + QueryStringVars.UgyiratId + "=" + iratStatusz.UgyiratId
                         + "&" + QueryStringVars.IratId + "=" + id
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.ValaszIratOnClientClick = "alert('Nem iktatható válaszirat!"
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            if (Iratok.BeiktathatoAMunkairat(iratStatusz, out errorDetail))
            {
                string msg = null;
                if (ugyiratStatusz.Munkaanyag)
                {
                    if (obj_irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Bejovo && FunctionRights.GetFunkcioJog(Page, "BejovoIratIktatasCsakAlszamra"))
                    {
                        msg = Resources.Error.UIDontHaveFunctionRight_MunkairatElesitesBejovo;
                    }
                    else if (obj_irat.PostazasIranya == KodTarak.POSTAZAS_IRANYA.Belso && FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatasCsakAlszamra"))
                    {
                        msg = Resources.Error.UIDontHaveFunctionRight_MunkairatElesitesBelso;
                    }
                }

                if (!String.IsNullOrEmpty(msg))
                {
                    ListHeader1.MunkapeldanyIktatasaOnClientClick = String.Format("if (confirm('{0}\\n{1}')) {{{2}}}  else return false;"
                            , msg
                            , Resources.Question.UIConfirmHeader_MunkairatNemIktathatoBe
                            , JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                                , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                           );
                }
                else
                {

                    ListHeader1.MunkapeldanyIktatasaOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.ElokeszitettIratIktatas + "&" + QueryStringVars.Id + "=" + id
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }

            }
            else
            {
                ListHeader1.MunkapeldanyIktatasaOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_MunkairatNemIktathatoBe
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "')) {" +
                    JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                        , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                        , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                        + "} else return false;";

            }


            // Szignálás
            if (IratSzignalasElerheto()) // BUG_6466
            {
                if (Iratok.Szignalhato(iratStatusz, execParam, out errorDetail))
                {
                    ListHeader1.SzignalasOnClientClick = JavaScripts.SetOnClientClick("SzignalasForm.aspx",
                         QueryStringVars.IratId + "=" + id
                         , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                }
                else
                {
                    ListHeader1.SzignalasOnClientClick = "alert('" + Resources.Error.ErrorCode_52951
                         + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                         + "'); return false;";
                }
            }

            // Csatolás:
            if (Iratok.Csatolhato(iratStatusz, out errorDetail))
            {
                ListHeader1.CsatolasOnClientClick = JavaScripts.SetOnClientClick("CsatolasForm.aspx",
                    QueryStringVars.Id + "=" + id + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromIrat
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                ListHeader1.CsatolasOnClientClick = "alert('" + Resources.Error.ErrorCode_55020
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }


            // Felszabadítás:
            if (Iratok.Felszabadithato(iratStatusz, ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.FelszabaditasOnClientClick = JavaScripts.SetOnClientClick("FelszabaditasForm.aspx"
                , QueryStringVars.IratId + "=" + id
                , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // Nem szabadítható fel:
                ListHeader1.FelszabaditasOnClientClick = "alert('" + Resources.Error.ErrorCode_52661
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }

            bool kiadmanyozando = obj_irat.KiadmanyozniKell == "1";

            if (Iratok.Expedialhato(iratStatusz, kiadmanyozando
                        , ugyiratStatusz, execParam, out errorDetail))
            {
                ListHeader1.ExpedialasOnClientClick = JavaScripts.SetOnClientClick("ExpedialasForm.aspx"
                    , QueryStringVars.IratId + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                // Nem expediálható
                ListHeader1.ExpedialasOnClientClick = "alert('" + Resources.Error.ErrorCode_52502
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "'); return false;";
            }

            if (IsEsemenyekSublistEnabled)
            {
                EsemenyekSubListHeader.NewVisible = false;
                EsemenyekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
                    , CommandName.Command + "=" + CommandName.New + "&" + QueryStringVars.ObjektumId + "=" + id
                    + "&" + QueryStringVars.Mode + "=" + CommandName.Munkanaplo
                    , Defaults.PopupWidth, Defaults.PopupHeight, EsemenyekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            }

            #region CR3128 -Iratpéldány létrehozás nyomógomb az iratok listájára

            Iratok.SetIratPeldanyLetrehozasa_ImageButton(id, ListHeader1.IratpeldanyLetrehozasButton);
            #endregion CR3128 -Iratpéldány létrehozás nyomógomb az iratok listájára

            #region CR3129 - Szükség lenne egy új ikonra az aláíráshoz
            ListHeader1.IratCsatolmanyAlairasOnClientClick = JavaScripts.SetOnClientClick("IraIratokForm.aspx"
                    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + QueryStringVars.Mode + "=" + CommandName.Alairas
                    + "&" + QueryStringVars.SelectedTab + "=TabCsatolmanyokPanel"
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            #endregion CR3129 - Szükség lenne egy új ikonra az aláíráshoz
        }
        else
        {
            if (IsEsemenyekSublistEnabled)
            {
                EsemenyekSubListHeader.NewVisible = false;
            }
        }
    }

    protected void IraIratokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraIratokGridViewBind();
                    if (IsEsemenyekSublistEnabled)
                    {
                        ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraIratokGridView));
                    }
                    break;

                default:
                    #region Template újragenerálás
                    // .NET framework hiba: template field esetén postback után elveszik a template
                    // workaround: újra generáljuk a template-et
                    foreach (DataControlField currentColumn in IraIratokGridView.Columns)
                    {
                        if (currentColumn is TemplateField && !String.IsNullOrEmpty(currentColumn.AccessibleHeaderText))
                        {
                            if (TemplateFieldSources.ContainsKey(currentColumn.AccessibleHeaderText))
                            {
                                int colindex = IraIratokGridView.Columns.IndexOf(currentColumn);

                                string ControlTypeSource = GetTemplateFieldSourceFromViewState(currentColumn.AccessibleHeaderText);
                                string ControlTypeSourceForGrid = KodTarak.CONTROLTYPE_SOURCE.MapForGrid(ControlTypeSource);
                                if (((TemplateField)currentColumn).ItemTemplate == null)
                                {
                                    ((TemplateField)currentColumn).ItemTemplate = new DynamicValueControlGridViewTemplate(currentColumn.AccessibleHeaderText
                                        , DataControlRowType.DataRow
                                        , ControlTypeSourceForGrid);


                                    foreach (GridViewRow row in IraIratokGridView.Rows)
                                    {
                                        ((TemplateField)currentColumn).ItemTemplate.InstantiateIn(row.Cells[colindex]);
                                    }
                                }
                            }
                        }
                    }
                    #endregion Template újragenerálás
                    //if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
                    //{
                    //    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraIratokGridView));
                    //}
                    break;
            }

            //ArrayList lstGridViewColumns = new ArrayList(DokumentumokGridView.Columns);
            //DataControlField[] arrayGridViewColumns = (DataControlField[])lstGridViewColumns.ToArray(typeof(DataControlField));

            //int lockColumnIndex = Array.FindIndex<DataControlField>(arrayGridViewColumns,
            //    delegate(DataControlField dcf) { return dcf.AccessibleHeaderText == lockColumnName; });

            int lockColumnIndex = this.LockColumnIndex;

            if (lockColumnIndex >= 0)
            {
                foreach (GridViewRow row in IraIratokGridView.Rows)
                {
                    if (row.Cells[lockColumnIndex].Controls.Count > 0)
                    {
                        if (row.Cells[lockColumnIndex].Controls[0] is Image)
                        {
                            //Image lockImage = (Image)row.Cells[lockColumnIndex].Controls[0];
                            //lockImage.ID = lockedImageName;
                            row.Cells[lockColumnIndex].Controls[0].ID = lockedImageName;
                        }
                    }
                }
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    //deleteSelectedIraIratok();
        //    //IraIratokGridViewBind();
        //}
        //}
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            //LZS BUG_10224
            ex_Export.SaveGridView_ToExcel(exParam, IraIratokGridView, ListHeader1.HeaderLabel.Replace('/', '_'), Constants.ExcelHeaderType.Standard, browser);
            //LZS
        }
        //bernat.laszlo eddig
        #region CSV export
        if (e.CommandName == CommandName.CSVExport)
        {
            Contentum.eUtility.CSVExport csvExport = new Contentum.eUtility.CSVExport(true);
            //csvExport.ExportToCSVFromDb(UI.SetExecParamDefault(Page), "select * from EREC_KuldKuldemenyek", "EREC_KuldKuldemenyek");
            csvExport.ExportToCSVFromDataGridViewWithProcedure(UI.SetExecParamDefault(Page), IraIratokGridView, System.Web.Configuration.WebConfigurationManager.ConnectionStrings["AuditLogConnectionString"].ConnectionString, Constants.CSVParams.sp_CSVExport_IratokWithHatosagi, Constants.CSVParams.EREC_IraIratokTable);
        }
        #endregion
    }


    //A kiválasztott iratokhoz tartozó elsődleges iratpéldányokat adja vissza
    private List<string> GetSelectedIratPeldanyok()
    {
        List<string> selectedIratPeldanyokList = new List<string>();
        List<string> selectedIratokList = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);

        for (int i = 0; i < IraIratokGridView.DataKeys.Count; i++)
        {
            DataKey dataKey = IraIratokGridView.DataKeys[i];
            string iratId = dataKey.Value.ToString();
            if (selectedIratokList.Contains(iratId))
            {
                string iratPeldanyId = dataKey.Values["ElsoIratPeldany_Id"].ToString();
                selectedIratPeldanyokList.Add(iratPeldanyId);
            }
        }

        return selectedIratPeldanyokList;
    }

    //A kiválasztott irathoz tartozó elsődleges iratpéldányt adja vissza
    private string GetSelectedIratPeldanyId()
    {
        string selectedIratPeldanyId = String.Empty;
        string selectedIratId = UI.GetGridViewSelectedRecordId(IraIratokGridView);

        for (int i = 0; i < IraIratokGridView.DataKeys.Count; i++)
        {
            DataKey dataKey = IraIratokGridView.DataKeys[i];
            string iratId = dataKey.Value.ToString();
            if (selectedIratId == iratId)
            {
                string iratPeldanyId = dataKey.Values["ElsoIratPeldany_Id"].ToString();
                selectedIratPeldanyId = iratPeldanyId;
                break;
            }
        }

        return selectedIratPeldanyId;
    }

    #region BUG 7471
    private bool IsPkiFile(string formatum)
    {
        if (!String.IsNullOrEmpty(formatum))
        {
            string pkiFileTypes = Rendszerparameterek.Get(Page, "PKI_FILETYPES");

            if (String.IsNullOrEmpty(pkiFileTypes))
            {
                pkiFileTypes = "pdf";
            }
            pkiFileTypes += ",kr";
            if (pkiFileTypes.ToLower().Contains(formatum.ToLower()))
                return true;
        }

        return false;
    }
    #endregion

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraIratRecords();
                IraIratokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraIratRecords();
                IraIratokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIraIratok();
                break;
            case CommandName.AtadasraKijeloles:
                {
                    List<string> selectedIratPeldanyokList = this.GetSelectedIratPeldanyok();
                    Session["SelectedIratPeldanyIds"] = String.Join(",", selectedIratPeldanyokList.ToArray());

                    Session["SelectedKuldemenyIds"] = null;
                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedDosszieIds"] = null;

                    string js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyAtadas", js, true);

                    break;
                }
            case CommandName.TomegesAlairas:
                {
                    List<string> selectedIratPeldanyokList = this.GetSelectedIratPeldanyok();
                    List<string> selectedIraIratokList = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session["SelectedIratPeldanyIds"] = String.Join(",", selectedIratPeldanyokList.ToArray());
                    Session["SelectedIraIratIds"] = String.Join(",", selectedIraIratokList.ToArray());

                    Session["SelectedKuldemenyIds"] = null;
                    Session["SelectedBarcodeIds"] = null;
                    Session["SelectedUgyiratIds"] = null;
                    Session["SelectedDosszieIds"] = null;
                    //todo - iratpéldányokból csatolmányok

                    #region getCsatolmanyok
                    //BUG 2509 - nem kell csatolmanyId URL -be!
                    //String csatolManyIds = "";

                    EREC_CsatolmanyokService service_csatolmanyok = eRecordService.ServiceFactory.GetEREC_CsatolmanyokService();
                    //EREC_CsatolmanyokService service_csat = new EREC_CsatolmanyokService(this.dataContext);

                    EREC_CsatolmanyokSearch search_csatolmanyok = new EREC_CsatolmanyokSearch();

                    search_csatolmanyok.IraIrat_Id.In(selectedIraIratokList);

                    Result result_csatGetAll = service_csatolmanyok.GetAll(UI.SetExecParamDefault(Page), search_csatolmanyok);
                    #region createAlairasFolyamat
                    Contentum.eRecord.Service.EREC_Alairas_FolyamatokService service = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_FolyamatokService();

                    EREC_Alairas_Folyamatok erec_AlairasFolyamatok = new EREC_Alairas_Folyamatok();
                    erec_AlairasFolyamatok.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.folyamat_rogzitve;
                    ExecParam execParam_Insert = Contentum.eUtility.UI.SetExecParamDefault(Page);
                    Result result_Insert = service.Insert(execParam_Insert, erec_AlairasFolyamatok);
                    if (result_Insert.IsError)
                    {
                        Logger.Error("Hiba: EREC_Alairas_FolyamatokService.Insert", execParam_Insert, result_Insert);
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert);
                        return;
                    }
                    string procId = result_Insert.Uid;

                    #endregion

                    int rowInd = 0;
                    if (!result_csatGetAll.IsError)
                    {
                        //BUG 7471
                        ExecParam execParamRP = UI.SetExecParamDefault(Page, new ExecParam());
                        string csakFodok = Rendszerparameterek.Get(execParamRP, "ALAIRAS_CSAK_FODOKUMENTUM");

                        foreach (DataRow row in result_csatGetAll.Ds.Tables[0].Rows)
                        {
                            #region BUG 7471
                            Boolean NeedFile = true;
                            string DokumentumSzerep = row["DokumentumSzerep"].ToString();
                            if ("1".Equals(csakFodok))
                            {
                                if (KodTarak.DOKUMENTUM_SZEREP.Fodokumentum.Equals(DokumentumSzerep))
                                {

                                    String DokId = row["Dokumentum_Id"].ToString();
                                    Contentum.eDocument.Service.KRT_DokumentumokService dokServ = Contentum.eRecord.BaseUtility.eDocumentService.ServiceFactory.GetKRT_DokumentumokService();
                                    ExecParam execParamDok = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParamDok.Record_Id = DokId;
                                    Result resDok = dokServ.Get(execParamDok);
                                    if (String.IsNullOrEmpty(resDok.ErrorCode))
                                    {
                                        KRT_Dokumentumok dok = (KRT_Dokumentumok)resDok.Record;
                                        NeedFile = IsPkiFile(dok.Formatum);
                                    }
                                }
                                else
                                {
                                    NeedFile = false;
                                }
                            }
                            #endregion
                            if (NeedFile)
                            {
                                string row_Id = row["Id"].ToString();
                                //BUG 2509 - nem kell csatolmanyId URL -be!
                                //csatolManyIds += rowInd == 0 ? row_Id : "," + row_Id;

                                #region createAlairasFolyamatTetelek
                                Contentum.eRecord.Service.EREC_Alairas_Folyamat_TetelekService tetelekService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_Alairas_Folyamat_TetelekService();
                                EREC_Alairas_Folyamat_Tetelek erec_Alairas_Folyamat_Tetelek = new EREC_Alairas_Folyamat_Tetelek();
                                erec_Alairas_Folyamat_Tetelek.AlairasFolyamat_Id = procId;
                                erec_Alairas_Folyamat_Tetelek.Csatolmany_Id = row_Id;
                                erec_Alairas_Folyamat_Tetelek.IraIrat_Id = row["IraIrat_Id"].ToString();
                                erec_Alairas_Folyamat_Tetelek.AlairasStatus = KodTarak.TOMEGES_ALAIRAS_FOLYAMAT_STATUS.folyamat_rogzitve;
                                ExecParam execParam_InsertTetel = Contentum.eUtility.UI.SetExecParamDefault(Page);
                                Result result_Insert_Tetel = tetelekService.Insert(execParam_InsertTetel, erec_Alairas_Folyamat_Tetelek);
                                if (result_Insert_Tetel.IsError)
                                {
                                    Logger.Error("Hiba: EREC_Alairas_Folyamat_TetelekService.Insert", execParam_InsertTetel, result_Insert_Tetel);
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_Insert_Tetel);
                                    return;
                                }
                                #endregion

                                rowInd++;
                            }
                        }
                    }
                    #endregion

                    //teszt adatok
                    //var queryString = QueryStringVars.CsatolmanyId + "=" + csatolManyIdsList + "&Proc_Id=0d1295ed-7ef3-433a-bf2b-b2bb8099f09f";
                    //BUG 2509 - nem kell csatolmanyId URL -be!
                    //var queryString = QueryStringVars.CsatolmanyId + "=" + csatolManyIds + "&" + QueryStringVars.ProcId + "=" + procId;
                    var queryString = QueryStringVars.ProcId + "=" + procId + "&" + "PostBackClientID" + "=" + IraIratokUpdatePanel.ClientID;  // TODO use QueryStringVars.PostBackClientID
                    //Netlock kliens oldali aláírás nem megy dialog ablakban
                    string pkiVendor = Rendszerparameterek.Get(UI.SetExecParamDefault(Page), "PKI_VENDOR");
                    bool noDialog = "NETLOCK".Equals(pkiVendor, StringComparison.InvariantCultureIgnoreCase);
                    string js = JavaScripts.SetOnClientClickWithTimeout("DokumentumAlairas.aspx", queryString, Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, 10, noDialog).TrimEnd("return false;".ToCharArray());

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyAtadas", js, true);

                    break;
                }

            case CommandName.Atvetel:
                {
                    string selectedIratPeldanyId = this.GetSelectedIratPeldanyId();
                    List<string> selectedIratPeldanyokList = this.GetSelectedIratPeldanyok();
                    // elvileg nem biztos, hogy a kiválasztott sor egyben a bepipált sor is
                    if (UI.GetGridViewSelectedCheckBoxesCount(IraIratokGridView, "check") == 1
                        && !String.IsNullOrEmpty(selectedIratPeldanyId)
                        && selectedIratPeldanyokList.Contains(selectedIratPeldanyId))
                    {
                        if (selectedIratPeldanyokList.Count > 0)
                        {
                            ErrorDetails errorDetail;
                            bool atveheto = IratPeldanyok.CheckAtvehetoWithFunctionRight(Page, selectedIratPeldanyId, out errorDetail);

                            if (!atveheto)
                            {
                                string js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52292, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemVehetoAt", js, true);
                            }
                            else
                            {
                                IratPeldanyok.Atvetel(selectedIratPeldanyId, Page, EErrorPanel1, ErrorUpdatePanel);
                                IraIratokGridViewBind();
                            }
                        }
                    }
                    else
                    {
                        Session["SelectedIratPeldanyIds"] = String.Join(",", selectedIratPeldanyokList.ToArray());

                        Session["SelectedKuldemenyIds"] = null;
                        Session["SelectedBarcodeIds"] = null;
                        Session["SelectedUgyiratIds"] = null;
                        Session["SelectedDosszieIds"] = null;
                        //ne okozzon gondot, ha korábban hívtuk a tük visszavétel funkciót
                        Session["TUKVisszavetel"] = null;

                        string js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratPeldanyAtvetel", js, true);
                    }
                }
                break;
            case CommandName.ElintezetteNyilvanitas:
                {
                    ///CR3421
                }
                break;
            case CommandName.ElintezetteNyilvanitasVisszavon:
                {
                    string selectedRow = UI.GetGridViewSelectedRecordId(IraIratokGridView);
                    List<string> listIds = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (!listIds.Contains(selectedRow)) listIds.Add(selectedRow);
                    foreach (string selectedIratId in listIds)
                    {
                        string errorText = String.Empty;
                        if (selectedIratId != null)
                        {
                            ErrorDetails errorDetail;
                            bool elintezheto = Iratok.CheckElintezhetoVisszavonWithFunctionRight(Page, selectedIratId, out errorDetail);
                            // BUG_6209
                            if (elintezheto && (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.ELINTEZETTE_NYILV_KIADM_FELTETELE, false)))
                            {
                                Iratok.Statusz statusz = Iratok.GetAllapotById(selectedIratId, UI.SetExecParamDefault(Page), EErrorPanel1);
                                if (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Kiadmanyozott)
                                {
                                    elintezheto = false;
                                    errorText = Resources.Error.ErrorCode_52952;
                                }
                            }

                            if (!elintezheto)
                            {
                                if (String.IsNullOrEmpty(errorText))
                                    errorText = Resources.Error.ErrorCode_52240 + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page);

                                //string js = String.Format("alert('{0}{1}');", Resources.Error.ErrorCode_52240, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                                string js = String.Format("alert('{0}');", errorText);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemIntezhetoEl", js, true);
                            }
                            else
                            {
                                Iratok.ElintezesVisszavon(selectedIratId, Page, EErrorPanel1, ErrorUpdatePanel);
                                IraIratokGridViewBind();
                            }
                        }
                    }

                }
                break;
            case CommandName.TomegesSzignalas:
                {
                    string js = string.Empty;
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (lstGridViewSelectedRows.Count > 0)
                    {
                        Session[Constants.SelectedIratIds] = string.Join(",", lstGridViewSelectedRows.ToArray());
                        Session[Constants.SelectedUgyiratIds] = null;
                        Session[Constants.SelectedBarcodeIds] = null;
                        Session[Constants.SelectedKuldemenyIds] = null;
                        Session[Constants.SelectedIratPeldanyIds] = null;
                        Session[Constants.SelectedDosszieIds] = null;

                        js = JavaScripts.SetOnClientClickWithTimeout("TomegesSzignalas.aspx", "Mode=Irat", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TomegesSzignalasScript", js, true);
                    }
                }
                break;
            case "TomegesOlvasasiJog":
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session[Constants.SelectedIratIds] = String.Join(",", lstGridViewSelectedRows.ToArray());
                    string js = JavaScripts.SetOnClientClickWithTimeout("TomegesOlvasasiJog.aspx", QueryStringVars.ObjektumType + "=" + Constants.TableNames.EREC_IraIratok, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TomegesOlvasasiJog", js, true);
                }
                break;


            case "HatosagiAdatokTomegesModositasa":
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session[Constants.SelectedIratIds] = String.Join(",", lstGridViewSelectedRows.ToArray());
                    string js = JavaScripts.SetOnClientClickWithTimeout("HatosagiAdatokTomegesForm.aspx", QueryStringVars.ObjektumType + "=" + Constants.TableNames.EREC_IraIratok, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HatosagiAdatokTomegesForm", js, true);
                    break;
                }

            case "ElintezetteNyilvanitasTomeges":
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session[Constants.SelectedIratIds] = String.Join(",", lstGridViewSelectedRows.ToArray());
                    var js = JavaScripts.SetOnClientClickWithTimeout("IratElintezettTomegesForm.aspx", QueryStringVars.ObjektumType + "=" + Constants.TableNames.EREC_IraIratok, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "UgyiratElintezettTomegesForm", js, true);
                    break;
                }
            case CommandName.IratAtvetelIntezkedesre:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session[Constants.SelectedIratIds] = String.Join(",", lstGridViewSelectedRows.ToArray());
                    string js = JavaScripts.SetOnClientClickWithTimeout("IratAtvetelIntezkedesreTomegesForm.aspx", QueryStringVars.ObjektumType + "=" + Constants.TableNames.EREC_IraIratok, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraIratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "IratAtvetelIntezkedesreTomegesForm", js, true);
                    break;
                }
        }
    }

    private void LockSelectedIraIratRecords()
    {
        LockManager.LockSelectedGridViewRecords(IraIratokGridView, "EREC_IraIratok"
            , "IraIratLock", "IraIratForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraIratRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IraIratokGridView, "EREC_IraIratok"
            , "IraIratLock", "IraIratForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// Elkuldi emailben a IraIratokGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraIratok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IraIratokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraIratok");
        }
    }

    protected void IraIratokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraIratokGridViewBind(e.SortExpression, UI.GetSortToGridView("IraIratokGridView", ViewState, e.SortExpression));
        if (IsEsemenyekSublistEnabled)
        {
            ActiveTabClear();
        }
    }

    #region BLG#1402
    /// <summary>
    /// TÜK specifikus oszlopok hozzadása a gridhez
    /// </summary>
    private void SetGridTukColumns()
    {
        bool isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);
        if (!isTuk)
            return;

        //BoundField bfMinosites = new BoundField();
        //bfMinosites.DataField = "MinositesNev";
        //bfMinosites.HeaderText = "Minősítés";
        //bfMinosites.SortExpression = "EREC_IraIratok.Minosites";
        //bfMinosites.HeaderStyle.CssClass = "GridViewBorderHeader";
        //bfMinosites.HeaderStyle.Width = new Unit("80");
        //bfMinosites.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        //bfMinosites.Visible = true;

        ////IraIratokGridView.Columns.Add(bfMinosites);
        ////LZS - BUG_7245
        //IraIratokGridView.Columns.Insert(10, bfMinosites);
        UI.SetGridViewColumnVisiblity(IraIratokGridView, "MinositesNev", true);


        BoundField bfMinositoNev = new BoundField();
        bfMinositoNev.DataField = "MinositoNev";
        bfMinositoNev.HeaderText = "Minősítő";
        bfMinositoNev.SortExpression = "EREC_IraIratok.Minosito";
        bfMinositoNev.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinositoNev.HeaderStyle.Width = new Unit("80");
        bfMinositoNev.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinositoNev.Visible = false;
        IraIratokGridView.Columns.Add(bfMinositoNev);

        BoundField bfMinositoSzervezet = new BoundField();
        bfMinositoSzervezet.DataField = "MinositoSzervezetNev";
        bfMinositoSzervezet.HeaderText = "Minősítő szervezet";
        bfMinositoSzervezet.SortExpression = "EREC_IraIratok.MinositoSzervezet";
        bfMinositoSzervezet.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinositoSzervezet.HeaderStyle.Width = new Unit("80");
        bfMinositoSzervezet.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinositoSzervezet.Visible = false;
        IraIratokGridView.Columns.Add(bfMinositoSzervezet);


        BoundField bfMinErvIdo = new BoundField();
        bfMinErvIdo.DataField = "MinositesErvenyessegiIdeje";
        bfMinErvIdo.HeaderText = "Min.érv.idő";
        bfMinErvIdo.SortExpression = "EREC_IraIratok.MinositesErvenyessegiIdeje";
        bfMinErvIdo.DataFormatString = "{0:yyyy.MM.dd}";
        bfMinErvIdo.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinErvIdo.HeaderStyle.Width = new Unit("80");
        bfMinErvIdo.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinErvIdo.Visible = false;
        IraIratokGridView.Columns.Add(bfMinErvIdo);

        BoundField bfTerjedelem = new BoundField();
        bfTerjedelem.DataField = "TerjedelemMennyiseg";
        bfTerjedelem.HeaderText = "Terjedelem";
        bfTerjedelem.SortExpression = "EREC_IraIratok.TerjedelemMennyiseg";
        bfTerjedelem.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfTerjedelem.HeaderStyle.Width = new Unit("80");
        bfTerjedelem.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfTerjedelem.Visible = false;
        IraIratokGridView.Columns.Add(bfTerjedelem);

        BoundField bfMellekletekSzama = new BoundField();
        bfMellekletekSzama.DataField = "MellekletekSzama";
        bfMellekletekSzama.HeaderText = "Mellékletek száma";
        bfMellekletekSzama.SortExpression = null;
        bfMellekletekSzama.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMellekletekSzama.HeaderStyle.Width = new Unit("80");
        bfMellekletekSzama.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMellekletekSzama.Visible = false;
        IraIratokGridView.Columns.Add(bfMellekletekSzama);

        #region BLG 5889 TÜK esetén minősítés jelölés alapja
        BoundField bfMinositesJelolesAlapja = new BoundField();
        bfMinositesJelolesAlapja.DataField = "MinositesJelolesAlapja";
        bfMinositesJelolesAlapja.HeaderText = "Minősítés jelölés alapja";
        bfMinositesJelolesAlapja.SortExpression = null;
        bfMinositesJelolesAlapja.HeaderStyle.CssClass = "GridViewBorderHeader";
        bfMinositesJelolesAlapja.HeaderStyle.Width = new Unit("120");
        bfMinositesJelolesAlapja.ItemStyle.CssClass = "GridViewBoundFieldItemStyle";
        bfMinositesJelolesAlapja.Visible = true;
        IraIratokGridView.Columns.Add(bfMinositesJelolesAlapja);
        #endregion
    }
    #endregion

    #endregion

    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraIratokGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (EsemenyekGridView.SelectedIndex == -1)
        {
            return;
        }
        string iratId = UI.GetGridViewSelectedRecordId(IraIratokGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, iratId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string iratId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                EsemenyekGridViewBind(iratId);
                EsemenyekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(EsemenyekGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        EsemenyekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(EsemenyekTabPanel))
        {
            EsemenyekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(EsemenyekGridView));
        }

    }

    #endregion

    #region Esemenyek Detail

    private void EsemenyekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView));
        //}
    }

    protected void EsemenyekGridViewBind(string iratId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("EsemenyekGridView", ViewState, "IratMozgas.HistoryVegrehajtasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("EsemenyekGridView", ViewState);

        EsemenyekGridViewBind(iratId, sortExpression, sortDirection);
    }


    private string prevIratId = String.Empty;
    protected void EsemenyekGridViewBind(string iratId, String SortExpression, SortDirection SortDirection)
    {
        if (!String.IsNullOrEmpty(iratId))
        {
            if (iratId != prevIratId)
            {
                prevIratId = iratId;

                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                EREC_IraIratokSearch search = new EREC_IraIratokSearch();

                search.OrderBy = Search.GetOrderBy("EsemenyekGridView", ViewState, SortExpression, SortDirection);

                //// Lapozás beállítása:
                UI.SetPaging(execParam, EsemenyekSubListHeader);

                Result res = service.GetAllIratmozgasWithExtension(execParam, iratId, search);

                object oldDataKey = EsemenyekGridView.SelectedValue;
                int oldIndex = EsemenyekGridView.SelectedIndex;
                UI.GridViewFill(EsemenyekGridView, res, EsemenyekSubListHeader, EErrorPanel1, ErrorUpdatePanel);
                object newDataKey = EsemenyekGridView.SelectedValue;
                int newIndex = EsemenyekGridView.SelectedIndex;
                RaiseGridViewSelectedIndexChanged(EsemenyekGridView, oldIndex, oldDataKey, newIndex, newDataKey);

                UI.SetTabHeaderRowCountText(res, Header1);

                //ui.SetClientScriptToGridViewSelectDeSelectButton(EsemenyekGridView);
            }
        }
        else
        {
            ui.GridViewClear(EsemenyekGridView);
        }
    }


    void EsemenyekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView));
    }

    protected void EsemenyekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(EsemenyekTabPanel))
                    //{
                    //    EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView));
                    //}
                    ActiveTabRefreshDetailList(EsemenyekTabPanel);
                    break;
            }
        }
    }

    protected void EsemenyekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Select")
        //{
        //    int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
        //    UI.SetGridViewCheckBoxesToSelectedRow(EsemenyekGridView, selectedRowNumber, "check");
        //}
    }

    private void EsemenyekGridView_RefreshOnClientClicks(string esemenyId)
    {
        //string id = esemenyId;
        //if (!String.IsNullOrEmpty(id))
        //{
        //    EsemenyekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("EsemenyekForm.aspx"
        //        , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
        //        , Defaults.PopupWidth, Defaults.PopupHeight, EsemenyekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        //}
    }

    protected void EsemenyekGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(EsemenyekSubListHeader.Scrollable, EsemenyCPE);

        EsemenyekSubListHeader.RefreshPagerLabel();
    }

    void EsemenyekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(EsemenyekSubListHeader.RowCount);
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView));
    }

    void EsemenyekSubListHeader_PagingButtonsClick(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, EsemenyCPE);
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView));
    }

    protected void EsemenyekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView)
            , e.SortExpression, UI.GetSortToGridView("EsemenyekGridView", ViewState, e.SortExpression));
    }

    protected void EsemenyekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RowModeType mode = MapGridViewModeByEsemenyBinding(e.Row, RowModeType.EditableNormal); //0);
            SetGridViewRowBackGroundColor(e.Row, mode);
        }
    }

    protected void EsemenyekGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int oldSelectedIndex = EsemenyekGridView.SelectedIndex;
        int newSelectedIndex = e.NewSelectedIndex;

        if (oldSelectedIndex > -1)
        {
            GridViewRow oldRow = EsemenyekGridView.Rows[EsemenyekGridView.SelectedIndex];
            SetGridViewRowNormalMode(oldRow);
        }

        EsemenyekGridView.SelectedIndex = newSelectedIndex;

        if (newSelectedIndex > -1)
        {
            GridViewRow row = EsemenyekGridView.Rows[newSelectedIndex];
            SetGridViewRowEditMode(row);
        }

    }

    protected void EsemenyekGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        bool updated = false;
        int index = e.RowIndex;
        GridViewRow row = EsemenyekGridView.Rows[index];

        if (!e.Cancel)
        {
            TextBox txtNote = (TextBox)row.FindControl("txtNote");
            Label labelNote = (Label)row.FindControl("labelNote");
            Label labelVer = (Label)row.FindControl("labelVer");
            Label labelId = (Label)row.FindControl("labelId");
            //string id = EsemenyekGridView.DataKeys[index].Value.ToString();
            if (txtNote != null && labelVer != null && labelId != null)
            {
                string id = labelId.Text;
                if (labelNote.Text != txtNote.Text && !String.IsNullOrEmpty(id))
                {
                    KRT_EsemenyekService svc = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_EsemenyekService();
                    ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
                    xpm.Record_Id = id;
                    KRT_Esemenyek record = new KRT_Esemenyek();
                    record.Updated.SetValueAll(false);
                    record.Base.Updated.SetValueAll(false);
                    record.Base.Ver = labelVer.Text;
                    record.Base.Updated.Ver = true;
                    record.Base.Note = txtNote.Text;
                    record.Base.Updated.Note = true;
                    Result res = svc.Update(xpm, record);
                    if (res.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                        ErrorUpdatePanel.Update();
                    }
                    else
                    {
                        updated = true;
                    }
                }
            }
        }

        if (updated)
        {
            EsemenyekGridViewBind(UI.GetGridViewSelectedRecordId(IraIratokGridView));
        }
        else
        {
            //EsemenyekGridView.SelectedIndex = -1;
            SetGridViewRowNormalMode(row);
        }
    }

    protected void EsemenyekGridView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
    }

    private void RaiseGridViewSelectedIndexChanged(GridView gridView, int oldIndex, object oldDataKey, int newIndex, object newDataKey)
    {
        string oldDataKeyString = (oldDataKey ?? String.Empty).ToString();
        string newDataKeyString = (newDataKey ?? String.Empty).ToString();

        if (newDataKeyString != oldDataKeyString)
        {
            GridViewSelectEventArgs gsea = new GridViewSelectEventArgs(newIndex);

            gridView.SelectedIndex = -1;

            if (gridView == EsemenyekGridView)
                EsemenyekGridView_SelectedIndexChanging(gridView, gsea);

        }
    }
    #endregion

    protected string GetPopupString_AtiktatasiLanc(string iratId)
    {
        string popupString = "popup('AtiktatasiLanc.aspx?IratId=" + iratId + "',"
            + Defaults.PopupWidth.ToString() + "," + Defaults.PopupHeight.ToString() + "); return false;";

        return popupString;
    }

    #region Dynamic LockField
    // ha dinamikusan adunk hozzá oszlopokat a GridViewhoz, a postback során elveszik a templatefield
    // sajnos a workaround sem igazán működik...
    // ImageField esetén csak az Image control kódból hozzáadott ID-ja ill. CLientID-ja vész el

    // lokálisan megkeressük a zárolás oszlopot, és utána hívjuk meg az UI.SetLockingInfo metódust
    // az oszlop indexét bekérjük, hogy ne kelljen minden sorra elvégezni a cast + keresés folyamatokat
    public static void GridView_RowDataBound_SetLockingInfoForImageField(GridViewRowEventArgs e, Page page, int lockColumnIndex)
    {
        if (e == null || page == null) return;

        if (lockColumnIndex < 0) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;

            if (drv != null)
            {
                DataRow row = drv.Row;

                // A LockField oszlop megkeresése
                GridView gv = (GridView)e.Row.NamingContainer;
                if (gv != null)
                {
                    //ArrayList lstGridViewColumns = new ArrayList(gv.Columns);
                    //DataControlField[] arrayGridViewColumns = (DataControlField[])lstGridViewColumns.ToArray(typeof(DataControlField));

                    //int lockColumnIndex = Array.FindIndex<DataControlField>(arrayGridViewColumns,
                    //    delegate(DataControlField dcf) { return dcf.AccessibleHeaderText == lockColumnName; });

                    if (gv.Columns.Count < lockColumnIndex + 1) return;

                    if (e.Row.Cells[lockColumnIndex].Controls.Count > 0)
                    {
                        if (e.Row.Cells[lockColumnIndex].Controls[0] is Image)
                        {
                            Image lockImage = (Image)e.Row.Cells[lockColumnIndex].Controls[0];
                            lockImage.ID = lockedImageName;
                            UI.SetLockingInfo(row, page, lockImage);
                        }
                    }
                }
            }

        }
    }

    #endregion Dynamic LockField

    #region IRAT ELINTÉZÉS
    private string HelperMergeStringList(List<string> items)
    {
        StringBuilder sb = new StringBuilder();
        if (items == null || items.Count < 1)
            return null;
        bool first = true;
        foreach (string item in items)
        {
            if (first)
                first = false;
            else
                sb.Append(",");

            sb.Append(item);
        }
        return sb.ToString();
    }
    #endregion

    /// <summary>
    /// VisiblePostBackCommandForLotusNotes
    /// </summary>
    /// <returns></returns>
    protected bool VisiblePostBackCommandForLotusNotes()
    {
        if (string.IsNullOrEmpty(Request.QueryString["Lotus"]))
        {
            return false;
        }
        return true;
    }


    /// <summary>
    /// GetPostBackCommandForLotusNotes
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected string GetPostBackCommandForLotusNotes(object type)
    {
        if (string.IsNullOrEmpty(Request.QueryString["Lotus"]))
            return string.Empty;

        if (type == null || !(type is System.Data.DataRowView))
            return string.Empty;

        System.Data.DataRowView view = (System.Data.DataRowView)type;
        DataRow row = view.Row;
        LotusNotesIratObj obj = new LotusNotesIratObj();
        obj.IratID = row["Id"].ToString();
        obj.Iktatoszam = row["Foszam_Merge"].ToString();
        obj.UgyiratID = row["Ugyirat_Id"].ToString();
        obj.UgyiratTargy = row["Ugyirat_Targy"].ToString();
        if (!string.IsNullOrEmpty(row["Targy"].ToString()))
            obj.IratTargy = row["Targy"].ToString();
        else
            obj.IratTargy = row["Targy1"].ToString();
        obj.IratVonalkod = row["ElsoIratPeldanyBarCode"].ToString();
        string json = JSONFunctions.SerializeLotusNotesIratObj(obj);
        return string.Format("window.parent.postMessage({0}, \"*\");", json);
    }

    /// <summary>
    /// GetIratHatasaTipusIkon
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected string GetIratHatasaTipusIkon(object type)
    {
        return Contentum.eUtility.Sakkora.GetIratHatasaTipusIkon(type);
    }
    /// <summary>
    /// GetIratHatasaTipusColor
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public System.Drawing.Color GetIratHatasaTipusColor(object type)
    {
        return Contentum.eUtility.Sakkora.GetIratHatasaTipusColor(type);
    }


    /// <summary>
    /// HatralevoNapokSzovegesen
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string HatralevoNapokSzovegesen(object type)
    {
        if (type == null)
            return string.Empty;
        int value;
        if (int.TryParse(type.ToString(), out value))
        {
            if (value > 0)
                return string.Format(Resources.Form.HatralevoNapokSzovegesenPlusFormat, value);
            else
                return string.Format(Resources.Form.HatralevoNapokSzovegesenMinusFormat, Math.Abs(value));
        }
        else
            return string.Empty;

    }
    /// <summary>
    /// HatralevoMunkaNapokSzovegesen
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string HatralevoMunkaNapokSzovegesen(object type)
    {
        if (type == null)
            return string.Empty;
        int value;
        if (int.TryParse(type.ToString(), out value))
        {
            if (value > 0)
                return string.Format(Resources.Form.HatralevoMunkaNapokSzovegesenPlusFormat, value);
            else
                return string.Format(Resources.Form.HatralevoMunkaNapokSzovegesenMinusFormat, Math.Abs(value));
        }
        else
            return string.Empty;
    }
    /// <summary>
    /// HatralevoNapokSzinesen
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static System.Drawing.Color HatralevoNapokSzinesen(object type)
    {
        if (type == null)
            return System.Drawing.Color.Black;
        int value;
        if (int.TryParse(type.ToString(), out value))
        {
            if (value > 0)
                return System.Drawing.Color.DarkGreen;
            else
                return System.Drawing.Color.Red;
        }
        else
            return System.Drawing.Color.Black;
    }
}
