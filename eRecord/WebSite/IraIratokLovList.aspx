<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IraIratokLovList.aspx.cs" Inherits="IraIratokLovList" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>

<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc3" %>
    
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc5" %>

<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc4" %>
    
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
    
 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 50px;
            min-width: 50px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" >
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,IraIratokLovListHeaderTitle%>" />
    &nbsp;<br />    
    <table border="0" cellpadding="0" cellspacing="0" style="width: 95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />                    
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 95%">
                             <tr>
                             <td>                                    
                             <table>
                                    <tr ID="trMaxRowWarning" runat="server" class="LovListWarningRow" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                          <asp:Label ID="Label2" runat="server" CssClass="mrUrlapCaption" Text="�v:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                             <uc3:EvIntervallum_SearchFormControl ID="Iktatas_EvIntervallum_SearchFormControl" 
                                             runat="server" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label4" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" style="width:340px">
                                            <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" EvIntervallum_SearchFormControlId="Iktatas_EvIntervallum_SearchFormControl"
                                             Mode="Iktatokonyvek"  runat="server" IsMultiSearchMode="true"/>
                                        </td>   
                                    <tr>
                                    <tr class="urlapSor"> 
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label5" runat="server" Text="F�sz�m:"></asp:Label>
                                            </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:SzamIntervallum_SearchFormControl ID="FoSzam_SzamIntervallum_SearchFormControl"
                                                runat="server" />
                                            </td>                                           
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label3" runat="server" Text="Alsz�m:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc5:SzamIntervallum_SearchFormControl ID="AlSzam_SzamIntervallum_SearchFormControl"
                                                runat="server" />
                                        </td>
                                     </tr>
                                     <tr class="urlapSor">                                            
                                        <td class="mrUrlapCaption">
                                           <asp:Label ID="Label1" runat="server" Text="T�rgy:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="Targy_TextBox" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            </td>
                                        <td class="mrUrlapMezo">
                                            </td>
                                     </tr>
                                     <tr class="urlapSor">
                                        <td colspan="4">
                                            <asp:ImageButton ID="ButtonSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg" AlternateText="Keres�s" OnClick="ButtonSearch_Click" />
                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg" AlternateText="R�szletes keres�s" /></td>
                                      </tr>
                                 </table>
                                 </td>
                                 </tr>
                                 <tr>
                                    <td style="text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 5px;">                    
                                        <div class="listaFulFelsoCsikKicsi"><img src="images/hu/design/spacertrans.gif" alt="" /></div>                            
                                    </td>
                                </tr>    
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <ajaxToolkit:CollapsiblePanelExtender ID="GridViewCPE" runat="server" TargetControlID="Panel1">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel1" runat="server">
                                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                    
                                                    <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                    <asp:HiddenField ID="GridViewSelectedIndex" runat="server" />
                                                    <asp:HiddenField ID="GridViewSelectedId" runat="server" />
                                                    
                                                        <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False"
                                                            DataKeyNames="Id">
                                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                            <HeaderStyle CssClass="GridViewLovListHeaderStyle" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Id" SortExpression="Id">
                                                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                                    <HeaderStyle CssClass = "GridViewLovListInvisibleCoulumnStyle" />
                                                                </asp:BoundField>  
                                                                <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktat�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Targy1" HeaderText="T�rgy">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot">
                                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FelhasznaloCsoport_Id_Iktato_Nev" HeaderText="Iktat�">
                                                                   <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                                   <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <br />
                                        <asp:ImageButton ID="ImageButtonReszletesAdatok" runat="server" 
                                        ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                        onmouseover="swapByName(this.id,'reszletesadatok2.png')" onmouseout="swapByName(this.id,'reszletesadatok.png')"
                                        AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se" />
                                    </td>
                                </tr>
                            </table>       
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />

</asp:Content>

