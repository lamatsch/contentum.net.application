using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class IraIratokLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private const string default_SearchExpression =
    "EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam";
    private string filterByObjektumKapcsolat;
    private string ObjektumId;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IraIratokList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);

        if (!IsPostBack)
        {
            Iktatas_EvIntervallum_SearchFormControl.SetDefaultEvTol = true;
            Iktatas_EvIntervallum_SearchFormControl.SetDefaultEvIg = true;
            Iktatas_EvIntervallum_SearchFormControl.SetDefault();
        }

        filterByObjektumKapcsolat = Request.QueryString.Get(QueryStringVars.ObjektumKapcsolatTipus);
        ObjektumId = Request.QueryString.Get(QueryStringVars.ObjektumId);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        //BUG_3592 - LZS 
        //800�600 helyett Defaults.PopupWidth_Max, Defaults.PopupHeight_Max
        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("IraIratokSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("IraIratokForm.aspx", "", GridViewSelectedId.ClientID, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

        if (!IsPostBack)
        {
            IraIktatoKonyvekDropDownList1.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato, Iktatas_EvIntervallum_SearchFormControl.EvTol, Iktatas_EvIntervallum_SearchFormControl.EvIg,
                true, false, LovListHeader1.ErrorPanel);
        }

    }
    protected void ButtonSearch_Click(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, GridViewCPE);
        FillGridViewSearchResult(false);
        disable_refreshLovList = true;
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        EREC_IraIratokSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_IraIratokSearch)Search.GetSearchObject(Page, new EREC_IraIratokSearch());
        }
        else
        {
            search = new EREC_IraIratokSearch(true);

            if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                IraIktatoKonyvekDropDownList1.SetSearchObject(search.Extended_EREC_IraIktatoKonyvekSearch);
            }

            Iktatas_EvIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_IraIktatoKonyvekSearch.Ev);

            if (!FoSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
            {
                FoSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);
            }

            if (!AlSzam_SzamIntervallum_SearchFormControl.BothTextBoxesAreEmpty())
            {
                AlSzam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(search.Alszam);
            }

            if (!String.IsNullOrEmpty(Targy_TextBox.Text))
            {
                //search.fts_targy = new FullTextSearchField();
                //search.fts_targy.Filter = Targy_TextBox.Text;
                search.Targy.Value = Targy_TextBox.Text;
                search.Targy.Operator =  Query.Operators.contains; //Search.GetOperatorByLikeCharater(Targy_TextBox.Text); // BUG_3944 kapcs�n visszarakva
            }
        }

        if (!String.IsNullOrEmpty(filterByObjektumKapcsolat) && !String.IsNullOrEmpty(ObjektumId))
        {
            switch (filterByObjektumKapcsolat)
            {
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.Iratfordulat:
                    ExecParam xpmIrat = UI.SetExecParamDefault(Page, new ExecParam());
                    Iratok.Statusz iratStatusz = Iratok.GetAllapotById(ObjektumId, xpmIrat, LovListHeader1.ErrorPanel, true);
                    if (!String.IsNullOrEmpty(iratStatusz.UgyiratId))
                    {
                        search.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = iratStatusz.UgyiratId;
                        search.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;
                    }
                    search.Id.Value = ObjektumId;
                    search.Id.Operator = Query.Operators.notequals;
                    Iratok.SetCsatolhatoSearchObject(search);
                    break;
                case KodTarak.UGYIRAT_OBJ_KAPCSOLATTIPUS.MegkeresesValasz:
                    ExecParam xpmIratPeldany = UI.SetExecParamDefault(Page, new ExecParam());
                    IratPeldanyok.Statusz iratPeldanyStatusz = IratPeldanyok.GetAllapotById(ObjektumId, xpmIratPeldany, LovListHeader1.ErrorPanel, true);
                    if (!String.IsNullOrEmpty(iratPeldanyStatusz.UgyiratId))
                    {
                        search.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value = iratPeldanyStatusz.UgyiratId;
                        search.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Operator = Query.Operators.equals;
                    }
                    if (!String.IsNullOrEmpty(iratPeldanyStatusz.Irat_Id))
                    {
                        search.Id.Value = iratPeldanyStatusz.Irat_Id;
                        search.Id.Operator = Query.Operators.notequals;
                    }
                    Iratok.SetCsatolhatoSearchObject(search);
                    break;
            }
        }

        search.OrderBy = default_SearchExpression;

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {

        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                    string selectedText = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, GridViewCPE);
    }

    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }
}
