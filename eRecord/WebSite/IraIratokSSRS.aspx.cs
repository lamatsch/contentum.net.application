﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;


public partial class IraIratokSSRS : Contentum.eUtility.UI.PageBase
{

    private string cim = String.Empty;
    Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

    protected void Page_Init(object sender, EventArgs e)
    {
        string visibilityFilter = (string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRS];
        visibilityState.LoadFromCustomString(visibilityFilter);

        if (Request.QueryString["Title"] != null)
        {
            cim = Request.QueryString["Title"];
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraIratokSearch search = null;

                string Startup = Request.QueryString.Get(Constants.Startup.StartupName);
                if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch);
                }
                else if (Startup == Constants.Startup.FromAlairandok)
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.IratAlairandokSearch);
                }
                // CR3221 Szignálás(előkészítés) kezelés
                else if (Startup == Constants.Startup.FromMunkairatSzignalas)
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch);

                    if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                        && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                        )
                    {
                        search.Manual_Alszam_Sztornozott.Value = KodTarak.UGYIRAT_ALLAPOT.Sztornozott;
                        search.Manual_Alszam_Sztornozott.Operator = Query.Operators.notequals;
                    }
                    search.Manual_Alszam_MunkaanyagFilter.Value = "1";
                    search.Manual_Alszam_MunkaanyagFilter.Operator = Query.Operators.isnull;
                }

                else if (Startup == Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai)
                {

                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch);
                }

                else
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject(Page, new EREC_IraIratokSearch(true));
                }

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_Alairok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Alairok"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_Pld":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Pld"));
                            break;
                        case "ObjektumTargyszavai_ObjIdFilter":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                            break;
                        case "eRecordWebSiteUrl":
                            ReportParameters[i].Values.Add(GeteRecordWebSiteUrl());
                            break;
                        case "CsnyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Csny));
                            break;
                        case "KVisibillity":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.K));
                            break;
                        case "FVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.F));
                            break;
                        case "IktatokonyvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Iktatohely));
                            break;
                        case "FoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Foszam));
                            break;
                        case "AlszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Alszam));
                            break;
                        case "EvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ev));
                            break;
                        case "SzkVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IratFelelos_SzervezetKod));
                            break;
                        case "IktatoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatoSzam_Merge));
                            break;
                        case "IratTargyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Targy1));
                            break;
                        case "UgyiratTargyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyirat_targy));
                            break;
                        case "UgyiratUgyintezoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyintezo_Nev));
                            break;
                        case "HataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Hatarido));
                            break;
                        case "UgyintezesModVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.UgyintezesModja));
                            break;
                        case "KezelesiFeljegyzesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KezelesiFelj));
                            break;
                        case "ITSZVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ITSZ));
                            break;
                        case "UgyiratElintezesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElintezesDat));
                            break;
                        case "UgyiratLezarasIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.LezarasDat));
                            break;
                        case "IrattarbaHelyezesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IrattarbaHelyezesDat));
                            break;
                        case "AdathordTipusaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.AdathordozoTipusa_Nev));
                            break;
                        case "IktatoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.FelhasznaloCsoport_Id_Iktato_Nev));
                            break;
                        case "UgyintezoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.FelhasznaloCsoport_Id_Ugyintezo_Nev));
                            break;
                        case "KuldesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PostazasDatuma));
                            break;
                        case "KimenoKuldesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PeldanyKuldesMod));
                            break;
                        case "PostazasIranyaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PostazasIranyaNev));
                            break;
                        case "ErkeztetesiAzonositoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ErkeztetoSzam));
                            break;
                        case "KuldoNeveVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.NevSTR_Bekuldo));
                            break;
                        case "KuldoCimeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.CimSTR_Bekuldo));
                            break;
                        case "BeerkezesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.BeerkezesIdeje));
                            break;
                        case "HivatkozasiSzamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.HivatkozasiSzam));
                            break;
                        case "BeerkezesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KuldesMod));
                            break;
                        case "KezbesitesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KezbesitesModja_Nev));
                            break;
                        case "IratHelyeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyOrzo_Nev));
                            break;
                        case "KezeloVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyFelelos_Nev));
                            break;
                        case "VonalkodVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyBarCode));
                            break;
                        case "CimzettNeveVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.NevSTR_Cimzett));
                            break;
                        case "CimzettCimeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.CimSTR_Cimzett));
                            break;
                        case "IktatvaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatasDatuma));
                            break;
                        case "IratHataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Hatarido));
                            break;
                        case "SztornirozvaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.SztornirozasDat));
                            break;
                        case "MunkaallomasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Munkaallomas));
                            break;
                        case "AllapotVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Allapot));
                            break;
                        case "ElintezettVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Elintezett));
                            break;
                        case "IntezesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IntezesIdopontja));
                            break;
                        case "IratHatasaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IratHatasaUgyintezesreNev));
                            break;
                        case "IHVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Munkaallomas));
                            break;
                        case "HatralevoNapokVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.HatralevoNapok));
                            break;
                        case "HatralevoMunkanapokVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.HatralevoMunkaNapok));
                            break;
                        case "EljarasiFokVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.EljarasiSzakaszFokNev));
                            break;
                        case "ElsoPeldAllapotVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyAllapot_Nev));
                            break;
                        case "UgyFajtajaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.UgyFajtaja));
                            break;
                        case "MinositesNevVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.MinositesNev));
                            break;
                        case "EloiratVisibility":
                            ReportParameters[i].Values.Add(Boolean.FalseString);
                            break;
                        case "UtoiratVisibility":
                            ReportParameters[i].Values.Add(Boolean.FalseString);
                            break;
                        case "ZarolasVisibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                        case "MellekletekSzamaVisibility":
                            ReportParameters[i].Values.Add(Boolean.FalseString);
                            break;
                        case "MellekletekTipusaVisibility":
                            ReportParameters[i].Values.Add(Boolean.FalseString);
                            break;
                        case "MellekletekAdathordozoFajtajaVisibility":
                            ReportParameters[i].Values.Add(Boolean.FalseString);
                            break;
                        case "Title":
                            ReportParameters[i].Values.Add(cim);
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }

    private string GeteRecordWebSiteUrl()
    {
        string port = Request.Url.Port == 80 ? String.Empty : ":" + Request.Url.Port.ToString();
        string url = String.Format("{0}://{1}{2}{3}/", Request.Url.Scheme, Request.Url.Host, port, Request.ApplicationPath);
        return url;
    }
}
