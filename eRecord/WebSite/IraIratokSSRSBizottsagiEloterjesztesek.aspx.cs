﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class IraIratokSSRSBizottsagiEloterjesztesek : Contentum.eUtility.UI.ListSSRSPageBase
{
    protected override ReportViewer ReportViewer { get { return ReportViewer1; } }
    protected override String DefaultOrderBy { get { return " order by EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC"; } }
    protected override Contentum.eUIControls.eErrorPanel ErrorPanel { get { return EErrorPanel1; } }

    protected override Result GetData()
    {
        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        execParam.Fake = true;

        EREC_IraIratokSearch search = (EREC_IraIratokSearch)this.SearchObject;

        Result result = service.GetAllWithExtensionAndMetaAndJogosultak(execParam, search
            , null, null
            , Constants.ColumnNames.EREC_IraIratok.Irattipus, new string[] { KodTarak.IRATTIPUS.BizottsagiEloterjesztes }, false
            , KodTarak.OBJMETADEFINICIO_TIPUS.B2, false, false
            , true);

        return result;
    }

    protected override BaseSearchObject GetSearchObject()
    {
        EREC_IraIratokSearch search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch);

        search.TopRow = 0;

        return search;
    }

    protected override Dictionary<string, int> CreateVisibilityColumnsInfoDictionary()
    {
        Dictionary<string, int> dict = new Dictionary<string, int>();         
		int index=51;
 
        List<string> lstBelsoAzonositok = BizottsagiEloterjesztesBelsoAzonositok;
        if (lstBelsoAzonositok != null && lstBelsoAzonositok.Count > 0)
        {
            foreach (string columnName in lstBelsoAzonositok)
            {
                dict.Add(String.Concat(columnName, "Visibility"), index);
                index++;
            }
        }

        dict.Add("ZarolasVisibility", index); // vagy: vis.Length - 1?

        return dict;
    }

    protected override void SetSpecificReportParameter(ReportParameter rp)
    {
        try { 
        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();
        string visibilityFilter = (string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSBizottsagiEloterjesztesek];
        visibilityState.LoadFromCustomString(visibilityFilter);

            if (rp != null && !String.IsNullOrEmpty(rp.Name) && ResultOfDataQuery != null)
            {
                switch (rp.Name)
                {
                    case "ColumnNames":
                        if (!String.IsNullOrEmpty(BizottsagiEloterjesztesColumnNamesString))
                        {
                            rp.Values.Add(BizottsagiEloterjesztesColumnNamesString);
                        }
                        break;
                    case "Where_Alairok":
                        rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_Alairok"));
                        break;
                    case "Where_Pld":
                        rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@Where_Pld"));
                        break;
                    case "ObjektumTargyszavai_ObjIdFilter":
                        rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                        break;
                    case "FelhasznaloSzervezet_Id":
                        rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                        break;
                    case "CsnyVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Csny));
                        break;
                    case "KVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.K));
                        break;
                    case "FVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.F));
                        break;
                    case "IktatokonyvVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Iktatohely));
                        break;
                    case "FoszamVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Foszam));
                        break;
                    case "AlszamVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Alszam));
                        break;
                    case "EvVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ev));
                        break;
                    case "IktatoszamVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatoSzam_Merge));
                        break;
                    case "IratTargyVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Targy1));
                        break;
                    case "UgyiratTargyVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyirat_targy));
                        break;
                    case "UgyiratUgyintezoVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyintezo_Nev));
                        break;
                    case "HataridoVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Hatarido));
                        break;
                    case "KezelesiFeljegyzesVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KezelesiFelj));
                        break;
                    case "ITSZVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ITSZ));
                        break;
                    case "AdathordTipusaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.AdathordozoTipusa_Nev));
                        break;
                    case "IktatoVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.FelhasznaloCsoport_Id_Iktato_Nev));
                        break;
                    case "UgyintezoVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyintezo_Nev));
                        break;
                    case "KuldesIdopontjaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PostazasDatuma));
                        break;
                    case "KimenoKuldesModjaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PeldanyKuldesMod));
                        break;
                    case "PostazasIranyaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PostazasIranyaNev));
                        break;
                    case "ErkeztetesiAzonositoVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ErkeztetoSzam));
                        break;
                    case "BeerkezesIdopontjaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.BeerkezesIdeje));
                        break;
                    case "HivatkozasiSzamVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.HivatkozasiSzam));
                        break;
                    case "BeerkezesModjaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KuldesMod));
                        break;
                    case "KezbesitesModjaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KezbesitesModja_Nev));
                        break;
                    case "IratHelyeVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyOrzo_Nev));
                        break;
                    case "KezeloVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyFelelos_Nev));
                        break;
                    case "VonalkodVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyBarCode));
                        break;
                    case "CimzettNeveVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.NevSTR_Cimzett));
                        break;
                    case "CimzettCimeVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.CimSTR_Cimzett));
                        break;
                    case "IktatvaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatasDatuma));
                        break;
                    case "IratHataridoVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Hatarido));
                        break;
                    case "SztornirozvaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.SztornirozasDat));
                        break;
                    case "MunkaallomasVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Munkaallomas));
                        break;
                    case "AllapotVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Allapot));
                        break;
                    case "IntezesIdopontjaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IntezesIdopontja));
                        break;
                    case "UgyFajtajaVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.UgyFajtaja));
                        break;
                    case "UgyintezesModjaNevVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.UgyintezesModja));
                        break;
                    case "MellekletekSzamaVisibility":
                        rp.Values.Add(Boolean.FalseString);
                        break;
                    case "EloiratVisibility":
                        rp.Values.Add(Boolean.FalseString);
                        break;
                    case "UtoiratVisibility":
                        rp.Values.Add(Boolean.FalseString);
                        break;
                    case "MellekletekAdathordozoFajtajaVisibility":
                        rp.Values.Add(Boolean.FalseString);
                        break;
                    case "LezarasDatVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.LezarasDat));
                        break;
                    case "IrattarbaHelyezesDatVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IrattarbaHelyezesDat));
                        break;
                    case "AdathordozoTipusaNevVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.AdathordozoTipusa_Nev));
                        break;
                    case "KuldoFeladoNeveVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.NevSTR_Bekuldo));
                        break;
                    case "KuldoFeladoCimeVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.CimSTR_Bekuldo));
                        break;
                    case "ElsoIratPeldanyAllapotVisibility":
                        rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyAllapot_Nev));
                        break;
                }
            }
        }catch(Exception e)
        {
            Logger.Error("SetSpecificReportParameter", e);
        }
    }

    // tárgyszavak belső azonosítóinak visszadaása listában
    private Result BizottsagiEloterjesztesTargyszavaiResult
    {
        get
        {
            if (ViewState["OTResult"] == null)
            {
                // metaadatok lekérése
                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                ExecParam execParam = UI.SetExecParamDefault(Page);

                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

                Result result = service.GetAllMetaByObjMetaDefinicio(execParam
                    , search, null, null
                    , Contentum.eUtility.Constants.TableNames.EREC_IraIratok
                    , Constants.ColumnNames.EREC_IraIratok.Irattipus, new string[] { KodTarak.IRATTIPUS.BizottsagiEloterjesztes }
                    , false
                    , KodTarak.OBJMETADEFINICIO_TIPUS.B2, false, true);

                if (result.IsError)
                {
                    throw new Exception(ResultError.GetErrorMessageFromResultObject(result));
                }
                ViewState["OTResult"] = result;
            }

            return ViewState["OTResult"] as Result;
        }
    }

    // tárgyszavak visszadaása listában, szögletes zárójelek között
    private String BizottsagiEloterjesztesColumnNamesString
    {
        get
        {
            if (ViewState["OTColumnNamesString"] == null)
            {
                // metaadatok lekérése
                Result result = BizottsagiEloterjesztesTargyszavaiResult;

                if (result != null && !result.IsError)
                {
                    var lstColumNames = new List<string>();
                    foreach (System.Data.DataRow row in BizottsagiEloterjesztesTargyszavaiResult.Ds.Tables[0].Rows)
                    {
                        string Targyszo = row["Targyszo"].ToString();

                        if (!String.IsNullOrEmpty(Targyszo) && !lstColumNames.Contains(String.Format("[{0}]", Targyszo)))
                        {
                            lstColumNames.Add(String.Format("[{0}]", Targyszo));
                        }
                    }

                    ViewState["OTColumnNamesString"] = String.Join(",", lstColumNames.ToArray());
                }
            }

            return ViewState["OTColumnNamesString"] as String;
        }
    }

    // tárgyszavak belső azonosítóinak visszaadása listában
    private List<string> BizottsagiEloterjesztesBelsoAzonositok
    {
        get
        {
            if (ViewState["OTBelsoAzonositok"] == null)
            {
                // metaadatok lekérése
                Result result = BizottsagiEloterjesztesTargyszavaiResult;

                if (result != null && !result.IsError)
                {
                    var lstColumNames = new List<string>();
                    foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string BelsoAzonosito = row["BelsoAzonosito"].ToString();

                        if (!String.IsNullOrEmpty(BelsoAzonosito) && !lstColumNames.Contains(BelsoAzonosito))
                        {
                            lstColumNames.Add(BelsoAzonosito);
                        }
                    }

                    ViewState["OTBelsoAzonositok"] = lstColumNames;
                }
            }

            return ViewState["OTBelsoAzonositok"] as List<string>;
        }
    }
}