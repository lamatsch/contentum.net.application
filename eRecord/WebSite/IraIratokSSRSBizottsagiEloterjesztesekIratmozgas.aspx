﻿<%@ Page Title="Bizottsági előterjesztések iratmozgás" Language="C#" MasterPageFile="~/PrintFormMasterPage.master" AutoEventWireup="true" CodeFile="IraIratokSSRSBizottsagiEloterjesztesekIratmozgas.aspx.cs" Inherits="IraIratokSSRSBizottsagiEloterjesztesekIratmozgas" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Width="100%"
                    Height="500px" CssClass="ReportViewer" EnableTelemetry="false">
                    <ServerReport ReportPath="/BizottsagiEloterjesztesek/BizottsagiEloterjesztesekIratmozgas" />
                    <%--<ServerReport ReportServerUrl="http://sql3/reportserver" ReportPath="/edokintrateszt riportok/BizottsagiEloterjesztesek/BizottsagiEloterjesztesekIratmozgas" />--%>
                    <%--<ServerReport ReportServerUrl="http://sql3/reportserver" ReportPath="/edok/BizottsagiEloterjesztesek/BizottsagiEloterjesztesekIratmozgas" />--%>
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>

