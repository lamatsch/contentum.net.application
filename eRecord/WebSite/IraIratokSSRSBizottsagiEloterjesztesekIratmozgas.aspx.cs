﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class IraIratokSSRSBizottsagiEloterjesztesekIratmozgas : Contentum.eUtility.UI.ListSSRSPageBase
{
    protected override ReportViewer ReportViewer { get { return ReportViewer1; } }
    protected override String DefaultOrderBy { get { return " order by IratMozgas.HistoryVegrehajtasIdo"; } }
    protected override Contentum.eUIControls.eErrorPanel ErrorPanel { get { return EErrorPanel1; } }

    protected override Result GetData()
    {
        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        execParam.Fake = true;

        EREC_IraIratokSearch search = (EREC_IraIratokSearch)this.SearchObject;

        String IratId = Request.QueryString.Get(QueryStringVars.Id);
        Result result = service.GetAllIratmozgasWithExtension(execParam, IratId, search);

        return result;
    }

    protected override BaseSearchObject GetSearchObject()
    {
        EREC_IraIratokSearch search = new EREC_IraIratokSearch();

        search.TopRow = 0;

        String OrderBy = Request.QueryString.Get(QueryStringVars.OrderBy);
        search.OrderBy = String.IsNullOrEmpty(OrderBy) ? DefaultOrderBy : OrderBy;

        return search;
    }

    protected override Dictionary<string, int> CreateVisibilityColumnsInfoDictionary()
    {
        Dictionary<string, int> dict = new Dictionary<string, int>();        // Közgyűlési előterjesztés tárgyszavai
        int index = 25;
        List<string> lstBelsoAzonositok = BizottsagiEloterjesztesBelsoAzonositok;
        if (lstBelsoAzonositok != null && lstBelsoAzonositok.Count > 0)
        {
            foreach (string columnName in lstBelsoAzonositok)
            {
                dict.Add(String.Concat(columnName, "Visibility"), index);
                index++;
            }
        }

        dict.Add("ZarolasVisibility", index); // vagy: vis.Length - 1?

        return dict;
    }

    protected override void SetSpecificReportParameter(ReportParameter rp)
    {
        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

        string visibilityFilter = (string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSBizottsagiEloterjesztesekIratmozgas];
        visibilityState.LoadFromCustomString(visibilityFilter);
        
        if (rp != null && !String.IsNullOrEmpty(rp.Name) && ResultOfDataQuery != null)
        {
            switch (rp.Name)
            {
                case "IratId":
                    rp.Values.Add(ResultOfDataQuery.SqlCommand.GetParamValue("@IratId"));
                    break;
                case "ColumnNames":
                    if (!String.IsNullOrEmpty(BizottsagiEloterjesztesColumnNamesString))
                    {
                        rp.Values.Add(BizottsagiEloterjesztesColumnNamesString);
                    }
                    break;
                case "FelhasznaloSzervezet_Id":
                    rp.Values.Add(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                    break;
                case "CsnyVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Csny));
                    break;
                case "KVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.K));
                    break;
                case "FVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.F));
                    break;
                case "IktatokonyvVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Iktatohely));
                    break;
                case "FoszamVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Foszam));
                    break;
                case "AlszamVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Alszam));
                    break;
                case "EvVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ev));
                    break;
                case "IktatoszamVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatoSzam_Merge));
                    break;
                case "IratTargyVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Targy1));
                    break;
                case "UgyiratTargyVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyirat_targy));
                    break;
                case "AdathordTipusaVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.AdathordozoTipusa_Nev));
                    break;
                case "IktatoVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.FelhasznaloCsoport_Id_Iktato_Nev));
                    break;
                case "UgyintezoVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyintezo_Nev));
                    break;
                case "IratHelyeVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyOrzo_Nev));
                    break;
                case "KezeloVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyFelelos_Nev));
                    break;
                case "VonalkodVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyBarCode));
                    break;
                case "IktatvaVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatasDatuma));
                    break;
                case "IratHataridoVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Hatarido));
                    break;
                case "SztornirozvaVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.SztornirozasDat));
                    break;
                case "AllapotVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Allapot));
                    break;
                case "ElsoIratPeldanyAllapotVisibility":
                    rp.Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyAllapot_Nev));
                    break;
            }
        }
    }

    // tárgyszavak belső azonosítóinak visszadaása listában
    private Result BizottsagiEloterjesztesTargyszavaiResult
    {
        get
        {
            if (ViewState["OTResult"] == null)
            {
                // metaadatok lekérése
                EREC_ObjektumTargyszavaiService service = eRecordService.ServiceFactory.GetEREC_ObjektumTargyszavaiService();
                ExecParam execParam = UI.SetExecParamDefault(Page);

                EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();

                Result result = service.GetAllMetaByObjMetaDefinicio(execParam
                    , search, null, null
                    , Contentum.eUtility.Constants.TableNames.EREC_IraIratok
                    , Constants.ColumnNames.EREC_IraIratok.Irattipus, new string[] { KodTarak.IRATTIPUS.BizottsagiEloterjesztes }
                    , false
                    , KodTarak.OBJMETADEFINICIO_TIPUS.B2, false, true);

                if (result.IsError)
                {
                    throw new Exception(ResultError.GetErrorMessageFromResultObject(result));
                }
                ViewState["OTResult"] = result;
            }

            return ViewState["OTResult"] as Result;
        }
    }

    // tárgyszavak visszadaása listában, szögletes zárójelek között
    private String BizottsagiEloterjesztesColumnNamesString
    {
        get
        {
            if (ViewState["OTColumnNamesString"] == null)
            {
                // metaadatok lekérése
                Result result = BizottsagiEloterjesztesTargyszavaiResult;

                if (result != null && !result.IsError)
                {
                    System.Collections.Generic.List<string> lstColumNames = new System.Collections.Generic.List<string>();
                    foreach (System.Data.DataRow row in BizottsagiEloterjesztesTargyszavaiResult.Ds.Tables[0].Rows)
                    {
                        string Targyszo = row["Targyszo"].ToString();

                        if (!String.IsNullOrEmpty(Targyszo) && !lstColumNames.Contains(String.Format("[{0}]", Targyszo)))
                        {
                            lstColumNames.Add(String.Format("[{0}]", Targyszo));
                        }
                    }

                    ViewState["OTColumnNamesString"] = String.Join(",", lstColumNames.ToArray());
                }
            }

            return ViewState["OTColumnNamesString"] as String;
        }
    }

    // tárgyszavak belső azonosítóinak visszaadása listában
    private List<string> BizottsagiEloterjesztesBelsoAzonositok
    {
        get
        {
            if (ViewState["OTBelsoAzonositok"] == null)
            {
                // metaadatok lekérése
                Result result = BizottsagiEloterjesztesTargyszavaiResult;

                if (result != null && !result.IsError)
                {
                    System.Collections.Generic.List<string> lstColumNames = new System.Collections.Generic.List<string>();
                    foreach (System.Data.DataRow row in result.Ds.Tables[0].Rows)
                    {
                        string BelsoAzonosito = row["BelsoAzonosito"].ToString();

                        if (!String.IsNullOrEmpty(BelsoAzonosito) && !lstColumNames.Contains(BelsoAzonosito))
                        {
                            lstColumNames.Add(BelsoAzonosito);
                        }
                    }

                    ViewState["OTBelsoAzonositok"] = lstColumNames;
                }
            }

            return ViewState["OTBelsoAzonositok"] as List<string>;
        }
    }
}