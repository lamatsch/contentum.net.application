﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class IraIratokSSRS : Contentum.eUtility.UI.PageBase
{
    private string vis = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        //vis = Request.QueryString.Get(QueryStringVars.Filter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraIratokSearch search = null;

                string Startup = Request.QueryString.Get(Constants.Startup.StartupName);
                if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch);
                }
                else if (Startup == Constants.Startup.FromAlairandok)
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.IratAlairandokSearch);
                }
                // CR3221 Szignálás(előkészítés) kezelés
                else if (Startup == Constants.Startup.FromMunkairatSzignalas)
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch);

                    if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                        && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                        )
                    {
                        search.Manual_Alszam_Sztornozott.Value = KodTarak.UGYIRAT_ALLAPOT.Sztornozott;
                        search.Manual_Alszam_Sztornozott.Operator = Query.Operators.notequals;
                    }
                    search.Manual_Alszam_MunkaanyagFilter.Value = "1";
                    search.Manual_Alszam_MunkaanyagFilter.Operator = Query.Operators.isnull;
                }
                else
                {
                    search = (EREC_IraIratokSearch)Search.GetSearchObject(Page, new EREC_IraIratokSearch(true));
                }

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_Alairok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Alairok"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@ReadableWhere")))
                            {
                                ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ReadableWhere"));
                            }
                            break;
                        case "Where_Pld":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Pld"));
                            break;
                        case "ObjektumTargyszavai_ObjIdFilter":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                            break;
                    //    case "eRecordWebSiteUrl":
                    //        ReportParameters[i].Values.Add(GeteRecordWebSiteUrl());
                    //        break;
                    //    case "CsnyVisibility":
                    //        if (vis.Substring(2, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                    //    case "KVisibility":
                    //        if (vis.Substring(3, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                    //    case "FVisibility":
                    //        if (vis.Substring(4, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                    //    case "IktatokonyvVisibility":
                    //        if (vis.Substring(5, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                    //    case "FoszamVisibility":
                    //        if (vis.Substring(6, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        } break;
                    //    case "AlszamVisibility":
                    //        if (vis.Substring(7, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        } break;
                    //    case "EvVisibility":
                    //        if (vis.Substring(8, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        } break;
                    //    case "IktatoszamVisibility":
                    //        if (vis.Substring(9, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                    //    case "IratTargyVisibility":
                    //        if (vis.Substring(10, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                    //    case "UgyiratTargyVisibility":
                    //        if (vis.Substring(11, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
							
                    //    case "UgyiratUgyintezoVisibility":
                    //        if (vis.Substring(12, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "HataridoVisibility":
                    //        if (vis.Substring(13, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "UgyintezesModVisibility":
                    //        if (vis.Substring(14, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "EloiratVisibility":
                    //        if (vis.Substring(15, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "UtoiratVisibility":
                    //        if (vis.Substring(16, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "MellekletekSzamaVisibility":
                    //        if (vis.Substring(17, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "MellekletekTipusaVisibility":
                    //        if (vis.Substring(18, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "MellekletekAdathordozoFajtajaVisibility":
                    //        if (vis.Substring(19, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KezelesiFeljegyzesVisibility":
                    //        if (vis.Substring(20, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "ITSZVisibility":
                    //        if (vis.Substring(21, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "UgyiratElintezesIdopontjaVisibility":
                    //        if (vis.Substring(22, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "UgyiratLezarasIdopontjaVisibility":
                    //        if (vis.Substring(23, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "IrattarbaHelyezesVisibility":
                    //        if (vis.Substring(24, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "AdathordTipusaVisibility":
                    //        if (vis.Substring(25, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "IktatoVisibility":
                    //        if (vis.Substring(26, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "UgyintezoVisibility":
                    //        if (vis.Substring(27, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KuldesIdopontjaVisibility":
                    //        if (vis.Substring(28, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KimenoKuldesModjaVisibility":
                    //        if (vis.Substring(29, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "PostazasIranyaVisibility":
                    //        if (vis.Substring(30, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "ErkeztetesiAzonositoVisibility":
                    //        if (vis.Substring(31, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KuldoNeveVisibility":
                    //        if (vis.Substring(32, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KuldoCimeVisibility":
                    //        if (vis.Substring(33, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "BeerkezesIdopontjaVisibility":
                    //        if (vis.Substring(34, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "HivatkozasiSzamVisibility":
                    //        if (vis.Substring(35, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "BeerkezesModjaVisibility":
                    //        if (vis.Substring(36, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KezbesitesModjaVisibility":
                    //        if (vis.Substring(37, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "IratHelyeVisibility":
                    //        if (vis.Substring(38, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "KezeloVisibility":
                    //        if (vis.Substring(39, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "VonalkodVisibility":
                    //        if (vis.Substring(40, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "CimzettNeveVisibility":
                    //        if (vis.Substring(41, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "CimzettCimeVisibility":
                    //        if (vis.Substring(42, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "IktatvaVisibility":
                    //        if (vis.Substring(43, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "IratHataridoVisibility":
                    //        if (vis.Substring(44, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                        
                        
                    //    case "SztornirozvaVisibility":
                    //        if (vis.Substring(45, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "MunkaallomasVisibility":
                    //        if (vis.Substring(46, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                       
						
                    //    case "AllapotVisibility":
                    //        if (vis.Substring(47, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "ElsoPeldAllapotVisibility":
                    //        if (vis.Substring(49, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "UgyFajtajaVisibility":
                    //        if (vis.Substring(50, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
							
                    //    case "ZarolasVisibility":
                    //        if (vis.Substring(51, 1).Equals("1"))
                    //        {
                    //            ReportParameters[i].Values.Add("true");
                    //        }
                    //        else
                    //        {
                    //            ReportParameters[i].Values.Add("false");
                    //        }
                    //        break;
                        
                    }
                }
            }
        }
        return ReportParameters;
    }

    private string GeteRecordWebSiteUrl()
    {
        string port = Request.Url.Port == 80 ? String.Empty : ":" + Request.Url.Port.ToString();
        string url = String.Format("{0}://{1}{2}{3}/", Request.Url.Scheme, Request.Url.Host, port, Request.ApplicationPath);
        return url;
    }
}
