﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

public partial class IraIratokSSRSHivataliKapu : Contentum.eUtility.UI.PageBase
{
    /*
     * 
     * 
     *     
  _   _   _____   _   _    _____    _____     _    _               _____   ______  _   _              _                   _______   ____               _   _ 
 | \ | | |_   _| | \ | |  / ____|  / ____|   | |  | |     /\      / ____| |___  / | \ | |     /\     | |          /\     |__   __| |  _ \      /\     | \ | |
 |  \| |   | |   |  \| | | |      | (___     | |__| |    /  \    | (___      / /  |  \| |    /  \    | |         /  \       | |    | |_) |    /  \    |  \| |
 | . ` |   | |   | . ` | | |       \___ \    |  __  |   / /\ \    \___ \    / /   | . ` |   / /\ \   | |        / /\ \      | |    |  _ <    / /\ \   | . ` |
 | |\  |  _| |_  | |\  | | |____   ____) |   | |  | |  / ____ \   ____) |  / /__  | |\  |  / ____ \  | |____   / ____ \     | |    | |_) |  / ____ \  | |\  |
 |_| \_| |_____| |_| \_|  \_____| |_____/    |_|  |_| /_/    \_\ |_____/  /_____| |_| \_| /_/    \_\ |______| /_/    \_\    |_|    |____/  /_/    \_\ |_| \_|    

     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    private string vis = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        vis = Request.QueryString.Get(QueryStringVars.Filter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            ReportViewer1.ShowRefreshButton = false;
         //   ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraIratokSearch search = null;

                string Startup = Request.QueryString.Get(Constants.Startup.StartupName);
                search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch);
                            

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];
				
				

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_Alairok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Alairok"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_Pld":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Pld"));
                            break;
                        case "ObjektumTargyszavai_ObjIdFilter":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                            break;
                        case "CsnyVisibility":
                            if (vis.Substring(2, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
								
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
							
							
                            break;
                        case "KVisibility":
                            if (vis.Substring(3, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "FVisibility":
                            if (vis.Substring(4, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IktatokonyvVisibility":
                            if (vis.Substring(5, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "FoszamVisibility":
                            if (vis.Substring(6, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "AlszamVisibility":
                            if (vis.Substring(7, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "EvVisibility":
                            if (vis.Substring(8, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "IktatoszamVisibility":
                            if (vis.Substring(9, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IratTargyVisibility":
                            if (vis.Substring(10, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "UgyiratTargyVisibility":
                            if (vis.Substring(11, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
						case "UgyintezoVisibility":
                            if (vis.Substring(12, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
						
													
						case "UgyiratHataridoVisibility":
                            if (vis.Substring(13, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;		
							
						case "UgyintezesModjaVisibility":
                            if (vis.Substring(14, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "EloiratVisibility":
                            if (vis.Substring(15, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "UtoiratVisibility":
                            if (vis.Substring(16, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
						
						case "MellekletekSzamaVisibility":
                            if (vis.Substring(17, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "AdathordTipusaVisibility":
                            if (vis.Substring(18, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
						
						case "MellekletekAdathordozoFajtajaVisibility":
                            if (vis.Substring(19, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "KezelesiFeljegyzesVisibility":
                            if (vis.Substring(20, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "ITSZVisibility":
                            if (vis.Substring(21, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "IntezesIdopontjaVisibility":
                            if (vis.Substring(22, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "LezarasDatVisibility":
                            if (vis.Substring(23, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "IrattarbaHelyezesDatVisibility":
                            if (vis.Substring(24, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "AdathordozoTipusa_NevVisibility":
                            if (vis.Substring(25, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
											
						
                        case "IktatoVisibility":
                            if (vis.Substring(26, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
                        case "Ugyintezo2Visibility":
                            if (vis.Substring(27, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
						case "PostazasDatumaVisibility":
                            if (vis.Substring(28, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "KuldesModVisibility":
                            if (vis.Substring(29, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "PostazasIranyaVisibility":
                            if (vis.Substring(30, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "ErkeztetoSzamVisibility":
                            if (vis.Substring(31, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "FeladoNeveVisibility":
                            if (vis.Substring(32, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "FeladoCimeVisibility":
                            if (vis.Substring(33, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "BeerkezesIdejeVisibility":
                            if (vis.Substring(34, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "HivatkozasiSzamVisibility":
                            if (vis.Substring(35, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "BeerkezesModjaVisibility":
                            if (vis.Substring(36, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "KezbesitesModja_NevVisibility":
                            if (vis.Substring(37, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
                        // Közgyűlési előterjesztések
                        case "IratHelyeVisibility": //ElsoIratPeldanyOrzo_Nev
                            if (vis.Substring(38, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
						
						
                        case "KezeloVisibility": //ElsoIratPeldanyFelelos_Nev
                            if (vis.Substring(39, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "VonalkodVisibility": //ElsoIratPeldanyBarCode
                            if (vis.Substring(40, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
						case "Csoport_Id_CimzettNevVisibility":
                            if (vis.Substring(41, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
						case "Csoport_Id_CimzettVisibility":
                            if (vis.Substring(42, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
							
							
                        case "IktatvaVisibility":
                            if (vis.Substring(43, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IratHataridoVisibility":
                            if (vis.Substring(44, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
                        case "SztornirozvaVisibility":
                            if (vis.Substring(45, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
						case "MunkaallomasVisibility":
                            if (vis.Substring(46, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
							
							
                            break;	
							
                        case "AllapotVisibility":
                            if (vis.Substring(47, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
							
							
                            break;
							
						
							
						
												
                        case "ElsoIratPeldanyAllapotVisibility":
						
						     
                            if (vis.Substring(49, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
							
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
							
							
                            break;
                        // Közgyűlési előterjesztés tárgyszavai
                        //case ...
                        
						
						case "UgyFajtajaVisibility":
                            if (vis.Substring(50, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;	
						
						case "ZarolasVisibility":
                           // if (vis.Substring(vis.Length - 1, 1).Equals("1"))
							if (vis.Substring(51,1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
							
						
                    }
                }
            }
        }
        return ReportParameters;
    }
}
