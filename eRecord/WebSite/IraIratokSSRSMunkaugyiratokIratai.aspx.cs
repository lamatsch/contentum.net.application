﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class IraIratokSSRSMunkaugyiratokIratai : Contentum.eUtility.UI.PageBase
{
    Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

    protected void Page_Init(object sender, EventArgs e)
    {
        string visibilityFilter = (string)Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRS];
        visibilityState.LoadFromCustomString(visibilityFilter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraIratokSearch search = null;

                search = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch);

                if ((String.IsNullOrEmpty(search.Allapot.Value) || String.IsNullOrEmpty(search.Allapot.Operator))
                    && (String.IsNullOrEmpty(search.SztornirozasDat.Value) || (String.IsNullOrEmpty(search.SztornirozasDat.Operator)))
                    )
                {
                    search.Manual_Alszam_Sztornozott.Value = KodTarak.UGYIRAT_ALLAPOT.Sztornozott;
                    search.Manual_Alszam_Sztornozott.Operator = Query.Operators.notequals;
                }

                search.Manual_UgyiratFoszam_MunkaanyagFilter.Operator = Query.Operators.isnull;

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];



                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.Foszam DESC, EREC_IraIratok.Alszam DESC, EREC_IraIratok.Sorszam DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_Alairok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Alairok"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_Pld":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Pld"));
                            break;
                        case "ObjektumTargyszavai_ObjIdFilter":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                            break;
                        case "CsnyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Csny));
                            break;
                        case "KVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.K));
                            break;
                        case "FVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.F));
                            break;
                        case "IktatokonyvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Iktatohely));
                            break;
                        case "FoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Foszam));
                            break;
                        case "AlszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Alszam));
                            break;
                        case "EvVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ev));
                            break;
                        case "SzkVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IratFelelos_SzervezetKod));
                            break;
                        case "IktatoszamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatoSzam_Merge));
                            break;
                        case "IratTargyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Targy1));
                            break;
                        case "UgyiratTargyVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyirat_targy));
                            break;
                        case "UgyiratUgyintezoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyintezo_Nev));
                            break;
                        case "UgyintezoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyintezo_Nev));
                            break;
                        case "HataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyirat_Hatarido));
                            break;
                        case "UgyiratHataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Ugyirat_Hatarido));
                            break;
                        case "UgyintezesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.UgyintezesModja));
                            break;
                        case "EloiratVisibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                        case "UtoiratVisibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                        case "MellekletekSzamaVisibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                        case "AdathordTipusaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.AdathordozoTipusa_Nev));
                            break;
                        case "MellekletekAdathordozoFajtajaVisibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                        case "KezelesiFeljegyzesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KezelesiFelj));
                            break;
                        case "ITSZVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ITSZ));
                            break;
                        case "IntezesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElintezesDat));
                            break;
                        case "LezarasDatVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.LezarasDat));
                            break;
                        case "IrattarbaHelyezesDatVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IrattarbaHelyezesDat));
                            break;
                        case "AdathordozoTipusaNevVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.AdathordozoTipusa_Nev));
                            break;
                        case "IktatoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.FelhasznaloCsoport_Id_Iktato_Nev));
                            break;
                        case "Ugyintezo2Visibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                        case "KuldesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PostazasDatuma));
                            break;
                        case "KimenoKuldesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KuldesMod));
                            break;
                        case "KuldesModVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KuldesMod));
                            break;
                        case "PostazasIranyaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.PostazasIranyaNev));
                            break;
                        case "ErkeztetoSzamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ErkeztetoSzam));
                            break;
                        case "KuldoFeladoNeveVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.NevSTR_Bekuldo));
                            break;
                        case "KuldoFeladoCimeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.CimSTR_Bekuldo));
                            break;
                        case "BeerkezesIdopontjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.BeerkezesIdeje));
                            break;
                        case "BeerkezesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KuldesMod));
                            break;
                        case "HivatkozasiSzamVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.HivatkozasiSzam));
                            break;                     
                        case "KezbesitesModjaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.KezbesitesModja_Nev));
                            break;
                        // Közgyűlési előterjesztések
                        case "IratHelyeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyOrzo_Nev));
                            break;
                        case "KezeloVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.FelhasznaloCsoport_Id_Iktato_Nev));
                            break;
                        case "VonalkodVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyBarCode));
                            break;
                        case "CimzettNeveVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.NevSTR_Cimzett));
                            break;
                        case "CimzettCimeVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.CimSTR_Cimzett));
                            break;
                        case "IktatvaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.IktatasDatuma));
                            break;
                        case "IratHataridoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Hatarido));
                            break;
                        case "SztornirozvaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.SztornirozasDat));
                            break;
                        case "MunkaallomasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Munkaallomas));
                            break;
                        case "AllapotVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.Allapot));
                            break;
                        case "ElsoIratPeldanyAllapotVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.ElsoIratPeldanyAllapot_Nev));
                            break;
                        case "UgyFajtajaVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IratokListColumnNames.UgyFajtaja));
                            break;
                        case "EloterjesztoVisibility":
                            ReportParameters[i].Values.Add("true");
                            break;
                        case "Bizottsag_tervezett_idopontjaVisibility":
                            ReportParameters[i].Values.Add("true");
                            break;
                        case "Bizottsag_megnevezeseVisibility":
                            ReportParameters[i].Values.Add("true");
                            break;                      
                        case "ZarolasVisibility":
                            ReportParameters[i].Values.Add("false");
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
