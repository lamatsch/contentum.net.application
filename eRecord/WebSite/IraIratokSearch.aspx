<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraIratokSearch.aspx.cs" Inherits="IraIratokSearch" Title="Iratok keres�se" EnableEventValidation="false" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc21" %>

<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc20" %>

<%@ Register Src="Component/EditablePartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc19" %>

<%@ Register Src="eRecordComponent/UgyiratDarabTextBox.ascx" TagName="UgyiratDarabTextBox"
    TagPrefix="uc18" %>

<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc9" %>

<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc6" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc7" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc17" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc16" %>
<%@ Register Src="eRecordComponent/FoszamIntervallum_SearchFormControl.ascx" TagName="FoszamIntervallum_SearchFormControl"
    TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/AlszamIntervallum_SearchFormControl.ascx" TagName="AlszamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc15" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc12" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc11" %>

<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<%@ Register Src="eRecordComponent/TargySzavakTextBox.ascx" TagName="TargySzavakTextBox" TagPrefix="tsztb" %>
<%@ Register Src="eRecordComponent/StandardObjektumTargyszavak.ascx" TagName="StandardObjektumTargyszavak" TagPrefix="sot" %>
<%@ Register Src="eRecordComponent/ObjektumTargyszavaiPanel.ascx" TagName="ObjektumTargyszavaiPanel" TagPrefix="otp" %>
<%@ Register Src="eRecordComponent/ObjektumTargyszavakMultiSearch.ascx" TagName="ObjektumTargyszavakMultiSearch" TagPrefix="tszms" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>

<%@ Register Src="Component/DatumIdoIntervallum_SearchCalendarControl.ascx" TagName="DatumIdoIntervallum_SearchCalendarControl" TagPrefix="uc22" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc23" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
    .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
    width: 180px;
}
     .mrUrlapCaption_short 
    {
        font-weight: bold;
        padding-right: 1px;
        padding-left: 1px;
        vertical-align: middle;
        text-align: right;
        color: #335674;
        width: 1px;
        min-width: 1px;
        white-space: nowrap;
      }       

    .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas 
    {
        font-weight: bold;
        padding-right: 1px;
        vertical-align: middle;
        text-align: right;
        color: #335674;
        white-space: nowrap;
    }
    
    body, html {
        overflow : auto;
    }
</style>

    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <%-- TODO: jelenleg nem lehet az �gyirat t�rgyszavai alapj�n az iratra keresni! --%>
                <%--<eUI:EFormPanel ID="StandardUgyiratTargyszavakPanel" runat="server" Visible="true">
                    <sot:StandardObjektumTargyszavak ID="sotUgyiratTargyszavak" runat="server" />
                </eUI:EFormPanel> --%>
                <%--<eUI:eFormPanel ID="StandardIratTargyszavakPanel" runat="server" Visible="true">
                    <sot:StandardObjektumTargyszavak ID="sotIratTargyszavak" runat="server" SearchMode="true" />
                </eUI:eFormPanel>--%>
                <%--                 <eUI:eFormPanel ID="EFormPanelFTS" runat="server">  
				    <table cellspacing="0" cellpadding="0" width="90%">
					    <tr class="urlapSor_kicsi">
						    <td class="mrUrlapCaption">
							    <asp:Label ID="Label20" runat="server" Text="T�rgysz�:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <tsztb:TargySzavakTextBox ID="Targyszavak_TextBoxTargyszo" runat="server"
							        Validate="false" SearchMode="true" />
						    </td>
						    <td class="mrUrlapCaption">
							    <asp:Label ID="Label21" runat="server" Text="�rt�k:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <asp:TextBox ID="Targyszavak_TextBoxErtek" runat="server"></asp:TextBox>
						    </td>
						    <td class="mrUrlapCaption">
							    <asp:Label ID="Label31" runat="server" Text="Megjegyz�s:"></asp:Label>
						    </td>
						    <td class="mrUrlapMezo">
							    <asp:TextBox ID="Targyszavak_TextBoxNote" runat="server"></asp:TextBox>
						    </td>														
					    </tr>
			        </table>
			    </eUI:eFormPanel>--%>
                <eUI:eFormPanel ID="EFormPanelOTMultiSearch" runat="server">
                    <tszms:ObjektumTargyszavakMultiSearch ID="tszmsEgyebTargyszavak" runat="server" />
                </eUI:eFormPanel>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label4" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc15:IraIktatoKonyvekDropDownList ID="UgyirDarab_IraIktatokonyvID_IraIktatoKonyvekDropDownList"
                                        runat="server" Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="IktKonyv_Ev_EvIntervallum_SearchFormControl1"
                                        Filter_IdeIktathat="false" IsMultiSearchMode="true"></uc15:IraIktatoKonyvekDropDownList>
                                </td>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label7" runat="server" Text="�v:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc11:EvIntervallum_SearchFormControl ID="IktKonyv_Ev_EvIntervallum_SearchFormControl1"
                                        runat="server"></uc11:EvIntervallum_SearchFormControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label5" runat="server" Text="F�sz�m:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc20:SzamIntervallum_SearchFormControl ID="UgyirDarab_Foszam_SzamIntervallum_SearchFormControl1"
                                        runat="server"></uc20:SzamIntervallum_SearchFormControl>
                                </td>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label6" runat="server" Text="Alsz�m:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc20:SzamIntervallum_SearchFormControl ID="Irat_Alszam_SzamIntervallum_SearchFormControl2"
                                        runat="server"></uc20:SzamIntervallum_SearchFormControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label26" runat="server" Text="�gyirat:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc9:UgyiratTextBox ID="UgyirDarab_UgyUgyirat_Id_UgyiratTextBox" runat="server" SearchMode="true"
                                        Validate="false"></uc9:UgyiratTextBox>
                                </td>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label27" runat="server" Text="�gyirat darab:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc18:UgyiratDarabTextBox ID="Irat_UgyUgyirDarab_UgyiratDarabTextBox" runat="server"
                                        SearchMode="true" Validate="false"></uc18:UgyiratDarabTextBox>
                                </td>
                            </tr>
                            
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label1" runat="server" Text="K�ld�/felad� neve:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc19:PartnerTextBox ID="Kuld_PartnerId_Bekuldo_PartnerTextBox" runat="server" SearchMode="true"></uc19:PartnerTextBox>
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label10" runat="server" Text="Hivatkoz�si sz�m:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Irat_Hivatkozasiszam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                            </tr>
                            
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label20" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc23:IraIrattariTetelTextBox ID="Irat_IraIrattariTetelTextBox" runat="server" SearchMode="true" />
                                </td>
                                <td class="mrUrlapCaption">
                                    &nbsp;
                                <td class="mrUrlapMezo">
                                    &nbsp;
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="Label3" runat="server" Text="Irat t�rgya:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <asp:TextBox ID="Irat_Targy_TextBox" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox></td>
                                <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="labelIratTipus" runat="server" Text="Irat t�pus:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="Irat_Tipus_KodtarakDropDownList" runat="server" />
                                    &nbsp;</td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label9" runat="server" Text="Iratp�ld�ny l�trehoz�s�nak d�tuma:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc7:DatumIntervallum_SearchCalendarControl ID="Irat_LetrehozasIdo_DatumIntervallum_SearchCalendarControl"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallum_SearchCalendarControl>
                                </td>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label11" runat="server" Text="Expedi�l�s m�dja:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="Kuld_KuldesMod_KodtarakDropDownList" runat="server"></uc12:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label15" runat="server" Text="Iktat�s id�pontja:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc22:DatumIdoIntervallum_SearchCalendarControl ID="DatumIdoIntervallum_SearchCalendarControl_IktatasDatuma" runat="server" />
                                    <%--  <uc7:DatumIntervallum_SearchCalendarControl ID="Irat_IktatasDatuma_DatumIntervallum_SearchCalendarControl"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallum_SearchCalendarControl>--%>
                                </td>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label12" runat="server" Text="Jelleg:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="Irat_Jelleg_KodtarakDropDownList" runat="server"></uc12:KodtarakDropDownList>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label2" runat="server" Text="Sztorn�z�s:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc7:DatumIntervallum_SearchCalendarControl ID="Irat_SztornirozasDat_DatumIntervallum_SearchCalendarControl"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallum_SearchCalendarControl>
                                </td>
                                <%--CR3289 Vezet�i panelr�l h�vva lej�rat sz�r�s elveszett--%>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label13" runat="server" Text="�gyint�z�si hat�rid�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc7:DatumIntervallum_SearchCalendarControl ID="Irat_Hatarido_DatumIntervallum_SearchCalendarControl"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label8" runat="server" Text="Elint�z�s id�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc7:DatumIntervallum_SearchCalendarControl ID="Irat_Elintezes_DatumIntervallum_SearchCalendarControl"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc7:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelAdathordozoTipusa" runat="server" Text="K�ldem�ny t�pusa:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="Irat_AdathordozoTipusa_KodtarakDropDownList" runat="server" />
                                </td>
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelElsodlegesAdathordozo" runat="server" Text="Els�dleges adathordoz� t�pusa:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="Irat_UgyintezesAlapja_KodtarakDropDownList" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label14" runat="server" Text="Kezel�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc8:CsoportTextBox ID="Irat_CsoportId_Felelos_CsoportTextBox" runat="server" SearchMode="true"
                                        Validate="false"></uc8:CsoportTextBox>
                                </td>
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label16" runat="server" Text="Irat helye:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc21:FelhasznaloCsoportTextBox ID="Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox"
                                        runat="server" SearchMode="true"></uc21:FelhasznaloCsoportTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="Label17" runat="server" Text="Iktat�:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc21:FelhasznaloCsoportTextBox ID="Irat_FelhCsopId_Iktato_FelhasznaloCsoportTextBox"
                                        runat="server" SearchMode="true"></uc21:FelhasznaloCsoportTextBox>
                                </td>
                                <td class="mrUrlapCaption">
                                    <%--BLG_1014--%>
                                    <%--<asp:Label ID="Label19" runat="server" Text="�gyint�z�:"></asp:Label>--%>
                                    <asp:Label ID="labelIratUgyintezo" runat="server" Text="<%$Forditas:labelIratUgyintezo|�gyint�z�:%>"></asp:Label>&nbsp;
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc21:FelhasznaloCsoportTextBox ID="Irat_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox"
                                        runat="server" SearchMode="true"></uc21:FelhasznaloCsoportTextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label18" runat="server" Text="�llapot:"></asp:Label></td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="Irat_Allapot_KodtarakDropDownList" runat="server"></uc12:KodtarakDropDownList>
                                    &nbsp;</td>
                                <td class="mrUrlapCaption">&nbsp;
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="Irat_Kiadmanyoznikell_CheckBox" runat="server" Text="Kiadm�nyozni kell"></asp:CheckBox>&nbsp;</td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="LabelPostazasIranya" runat="server" Text="Post�z�s ir�nya" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="KodtarakDropDownListPostazasIraanya" runat="server" />
                                    &nbsp;
                                </td>
                                <td class="mrUrlapCaption" />
                                <td class="mrUrlapMezo" />
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;
                                    <asp:Label ID="Label19" runat="server" Text="Csatolm�ny" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="KodtarakDropDownListCsatolmany" runat="server" />
                                    &nbsp;
                                </td>
                                <td class="mrUrlapCaption" />
                                <td class="mrUrlapMezo" />
                            </tr>

                            <eUI:eFormPanel ID="StandardTargyszavakPanel_Iratok" runat="server" Visible="true" RenderMode="Panel" style="padding-top: 15px; padding-bottom: 15px">
                                <otp:ObjektumTargyszavaiPanel ID="otpStandardTargyszavak_Iratok" RepeatColumns="2" RepeatDirection="Horizontal" SearchMode="true" 
                                Width="100%" runat="server" RepeatLayout="Flow" CssClass="mrUrlapInputShort" />
                            </eUI:eFormPanel>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption"><asp:Label runat="server" Text="Elint�zett:" /></td>
                                <td class="mrUrlapMezo">
                                    <asp:RadioButtonList ID="RadioButtonList_Elintezett" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1">Igen</asp:ListItem>
                                        <asp:ListItem Value="0">Nem</asp:ListItem>
                                        <asp:ListItem Value="">�sszes</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption"></td>
                                <td class="mrUrlapMezo" style="font-weight: bold">
                                    <asp:CheckBox ID="CheckBoxMunkairatok" runat="server" Text="Munkairatok" />
                                    <asp:CheckBox ID="cbMunkaUgyiratokIratai" runat="server" Text="Munka�gyiratok iratai"
                                        Style="padding-left: 30px;" /><br />
                                </td>
                                <td class="mrUrlapCaption" style="width: 300px">
                                    <asp:Label ID="labelUgyFajtaja" runat="server" Text="�gy fajt�ja:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc12:KodtarakDropDownList ID="UgyFajtaja_KodtarakDropDownList" runat="server" />
                                </td>
                            </tr>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption"></td>
                                <td class="mrUrlapMezo"></td>
                                <td class="mrUrlapCaption" />
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="NemCsoporttagokkal_CheckBox" runat="server" Text="Saj�t jogon l�that� t�telek"/><br />
                                    <asp:CheckBox ID="CsakAktivIrat_CheckBox" runat="server" Text="Akt�v �llapotban l�v� t�telek"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>

                <table cellspacing="0" cellpadding="0">
                    <tr class="urlapSor">
                        <td colspan="2">
                            <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                runat="server"></uc10:TalalatokSzama_SearchFormComponent>
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
</asp:Content>

