using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.FullTextSearch;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System.Data;

public partial class IraIratokSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_IraIratokSearch);

    // Irat kateg�ria helyett iratt�pus:
    //private const string kcs_IRATKATEGORIA = "IRATKATEGORIA";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_KULDEMENY_KULDES_MODJA = "KULDEMENY_KULDES_MODJA";
    private const string kcs_UGYINTEZES_ALAPJA = "UGYINTEZES_ALAPJA";
    private const string kcs_IRAT_ALLAPOT = "IRAT_ALLAPOT";
    private const string kcs_IRAT_JELLEG = "IRAT_JELLEG";
    private const string kcs_POSTAZAS_IRANYA = "POSTAZAS_IRANYA";
    private const string kcs_CSATOLMANY = "CSATOLMANY_VIZSGALAT";

    private string Startup = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;

        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName) ?? String.Empty;

        switch (Startup)
        {
            case Constants.Startup.FromAlairandok:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.IratAlairandokSearch;
                break;
            case Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch;
                break;
            case Constants.Startup.FromMunkaUgyiratokIratai:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch;
                break;
            case Constants.Startup.FromKozgyulesiEloterjesztesek:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch;
                break;
            case Constants.Startup.FromBizottsagiEloterjesztesek:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch;
                break;
            // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
            case Constants.Startup.FromMunkairatSzignalas:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch;
                break;
            // CR3289 Vezeti Panel kezel�s
            case Constants.Startup.FromVezetoiPanel_Irat:
                SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch;
                break;

        }

        bool isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            // Jelezz�k az iktat�k�nyv komponensnek, hogy Id-kat kell belet�lteni, nem iktat�hely �rt�ket: (BUG#4290)
            UgyirDarab_IraIktatokonyvID_IraIktatoKonyvekDropDownList.ValuesFilledWithId = true;                
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Startup == Constants.Startup.FromAlairandok)
        {
            SearchHeader1.HeaderTitle = Resources.Search.IraIratok_Alairandok_SearchHeaderTitle;
        }
        else if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
        {
            SearchHeader1.HeaderTitle = Resources.Search.IraIratok_KozgyulesiEloterjesztesek_SearchHeaderTitle;
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
        {
            SearchHeader1.HeaderTitle = Resources.Search.IraIratok_BizottsagiEloterjesztesek_SearchHeaderTitle;
        }
        // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
        if (Startup == Constants.Startup.FromMunkairatSzignalas)
        {
            SearchHeader1.HeaderTitle = Resources.Search.MunkairatSzignalas_SearchHeaderTitle;
        }
        else
        {
            SearchHeader1.HeaderTitle = Resources.Search.IraIratokSearchHeaderTitle;
        }

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IraIratokSearch searchObject = null;
            switch (Startup)
            {
                case Constants.Startup.FromAlairandok:
                    {
                        if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.IratAlairandokSearch))
                        {
                            searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.IratAlairandokSearch);
                        }
                        else
                        {
                            searchObject = GetDefaultSearchObject();
                        }
                    }
                    break;
                case Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.HivataliKapusMunkaUgyiratokIrataiSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromMunkaUgyiratokIratai:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkaUgyiratokIrataiSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromKozgyulesiEloterjesztesek:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.KozgyulesiEloterjesztesekSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                case Constants.Startup.FromBizottsagiEloterjesztesek:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.BizottsagiEloterjesztesekSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
                case Constants.Startup.FromMunkairatSzignalas:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.MunkairatSzignalasSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                // CR3289 Vezet�i Panel kezel�s
                case Constants.Startup.FromVezetoiPanel_Irat:
                    if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch))
                    {
                        searchObject = (EREC_IraIratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_IraIratokSearch(true), Constants.CustomSearchObjectSessionNames.VezetoiPanelSearch);
                    }
                    else
                    {
                        searchObject = GetDefaultSearchObject();
                    }
                    break;
                default:
                    {
                        if (Search.IsSearchObjectInSession(Page, _type))
                        {
                            searchObject = (EREC_IraIratokSearch)Search.GetSearchObject(Page, new EREC_IraIratokSearch(true));
                        }
                        else
                        {
                            searchObject = GetDefaultSearchObject();
                        }
                    }
                    break;
            }
            // BLG_1421
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                labelUgyFajtaja.Visible = true;
                UgyFajtaja_KodtarakDropDownList.Visible = true;
            }
            else
            {
                labelUgyFajtaja.Visible = false;
                UgyFajtaja_KodtarakDropDownList.Visible = false;
            }
                LoadComponentsFromSearchObject(searchObject);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        switch (Startup)
        {
            case Constants.Startup.FromMunkaUgyiratokIratai:
            case Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai:
                cbMunkaUgyiratokIratai.Enabled = false;
                break;
            case Constants.Startup.FromMunkairatSzignalas:
                CheckBoxMunkairatok.Enabled = false;
                break;
            case Constants.Startup.FromKozgyulesiEloterjesztesek:
            case Constants.Startup.FromBizottsagiEloterjesztesek:
                Irat_Tipus_KodtarakDropDownList.ReadOnly = true;
                break;
        }
    }

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraIratokSearch erec_IraIratokSearch = null;
        if (searchObject != null) erec_IraIratokSearch = (EREC_IraIratokSearch)searchObject;

        if (erec_IraIratokSearch != null)
        {
            IktKonyv_Ev_EvIntervallum_SearchFormControl1.SetComponentFromSearchObjectFields(
                erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

            #region 4242

            
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            bool isTUK = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.TUK, false);
            if (isTUK)
            {
                //T�K eset�n az id lesz az �rt�k mez�nek felt�ltve, mivel itt Id szerinti keres�s lesz
                string id = erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id == null ? null : erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Id.Value;
                UgyirDarab_IraIktatokonyvID_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetIdToValues(Constants.IktatoErkezteto.Iktato
                , false, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvTol, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvIg, true, false, id, SearchHeader1.ErrorPanel);
            }
            else
            {
                // Iktatokonyvek megk�l�nb�ztet� jelz�s alapj�n val� felt�lt�se:
                UgyirDarab_IraIktatokonyvID_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                , false, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvTol, IktKonyv_Ev_EvIntervallum_SearchFormControl1.EvIg
                , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
            }
            #endregion

            UgyirDarab_Foszam_SzamIntervallum_SearchFormControl1.SetComponentFromSearchObjectFields(
                erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

            Irat_Alszam_SzamIntervallum_SearchFormControl2.SetComponentFromSearchObjectFields(
                erec_IraIratokSearch.Alszam);

            UgyirDarab_UgyUgyirat_Id_UgyiratTextBox.Id_HiddenField =
                erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Value;
            UgyirDarab_UgyUgyirat_Id_UgyiratTextBox.SetUgyiratTextBoxById(SearchHeader1.ErrorPanel, null);

            Irat_UgyUgyirDarab_UgyiratDarabTextBox.Id_HiddenField =
                erec_IraIratokSearch.UgyUgyIratDarab_Id.Value;
            Irat_UgyUgyirDarab_UgyiratDarabTextBox.SetUgyiratDarabTextBoxById(SearchHeader1.ErrorPanel);

            Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField = erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Value;
            Kuld_PartnerId_Bekuldo_PartnerTextBox.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
            if (erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value != String.Empty)
                Kuld_PartnerId_Bekuldo_PartnerTextBox.Text = erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value;

            Irat_Hivatkozasiszam_TextBox.Text = erec_IraIratokSearch.HivatkozasiSzam.Value;

            //if (erec_IraIratokSearch.fts_targy != null)
            //    Irat_Targy_TextBox.Text = erec_IraIratokSearch.fts_targy.Filter;
            Irat_Targy_TextBox.Text = erec_IraIratokSearch.Targy.Value;

            //Irat_Kategoria_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATKATEGORIA, erec_IraIratokSearch.Kategoria.Value,
            //    true, SearchHeader1.ErrorPanel);
            // Irat kateg�ria helyett iratt�pus:
            Irat_Tipus_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRATTIPUS, erec_IraIratokSearch.Irattipus.Value,
                true, SearchHeader1.ErrorPanel);

            Irat_LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_IraIratokSearch.Manual_LetrehozasIdo);
            Irat_Elintezes_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_IraIratokSearch.IntezesIdopontja);

            Kuld_KuldesMod_KodtarakDropDownList.FillAndSetSelectedValue(kcs_KULDEMENY_KULDES_MODJA,
                erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.KuldesMod.Value, true, SearchHeader1.ErrorPanel);

            #region IKTATAS
            DatumIdoIntervallum_SearchCalendarControl_IktatasDatuma.DatumKezdValue = erec_IraIratokSearch.IktatasDatuma.Value;
            DatumIdoIntervallum_SearchCalendarControl_IktatasDatuma.DatumVegeValue = erec_IraIratokSearch.IktatasDatuma.ValueTo;
            DatumIdoIntervallum_SearchCalendarControl_IktatasDatuma.SetSearchObjectFields(erec_IraIratokSearch.IktatasDatuma);

            ////Irat_IktatasDatuma_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(erec_IraIratokSearch.IktatasDatuma);
            #endregion

            // TODO: Jelleg ???
            //Irat_Jelleg_KodtarakDropDownList.FillAndSetSelectedValue

            Irat_SztornirozasDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_IraIratokSearch.SztornirozasDat);

            // CR3289 Vezet�i panel kezel�s
            // Hat�rid�-sz�r�s felker�lt a keres�shez
            Irat_Hatarido_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
               erec_IraIratokSearch.Hatarido);

            Irat_AdathordozoTipusa_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
                erec_IraIratokSearch.AdathordozoTipusa.Value, true, SearchHeader1.ErrorPanel);

            Irat_UgyintezesAlapja_KodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYINTEZES_ALAPJA,
                erec_IraIratokSearch.UgyintezesAlapja.Value, true, SearchHeader1.ErrorPanel);

            // csak ha t�nyleg egy Id van benne
            string Csoport_Id_Felelos = erec_IraIratokSearch.Csoport_Id_Felelos.Value.Replace("'", "");
            string[] separator = new string[] { "," };
            int cntIds = Csoport_Id_Felelos.Split(separator, StringSplitOptions.None).Length;
            if (cntIds < 2)
            {
                Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField = Csoport_Id_Felelos;
            }
            else
            {
                Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField = "";
            }

            //Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField =
            //    erec_IraIratokSearch.Csoport_Id_Felelos.Value;
            Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField =
                erec_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch.Csoport_Id_Felelos.Value;
            Irat_CsoportId_Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField =
            //    erec_IraIratokSearch.FelhasznaloCsoport_Id_Orzo.Value;
            Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField =
                erec_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch.FelhasznaloCsoport_Id_Orzo.Value;
            Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            Irat_FelhCsopId_Iktato_FelhasznaloCsoportTextBox.Id_HiddenField =
                erec_IraIratokSearch.FelhasznaloCsoport_Id_Iktato.Value;
            Irat_FelhCsopId_Iktato_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            Irat_Allapot_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRAT_ALLAPOT,
                erec_IraIratokSearch.Allapot.Value, true, SearchHeader1.ErrorPanel);

            KodtarakDropDownListPostazasIraanya.FillAndSetSelectedValue(kcs_POSTAZAS_IRANYA,
                erec_IraIratokSearch.PostazasIranya.Value, false, SearchHeader1.ErrorPanel);

            //LZS
            //erec_IraIratokSearch.CsatolmanySzures = int.Parse(KodTarak.CSATOLMANY_VIZSGALAT.MIND);
            KodtarakDropDownListCsatolmany.FillAndSetSelectedValue(kcs_CSATOLMANY, /*KodTarak.CSATOLMANY_VIZSGALAT.MIND*/ erec_IraIratokSearch.CsatolmanySzures.Value, false, SearchHeader1.ErrorPanel);
            //funkci�jog
            KodtarakDropDownListCsatolmany.Enabled = FunctionRights.GetFunkcioJog(Page, "CSATOLMANY_VIZSGALAT");

            //LZS - Default �rt�k a MIND "0" legyen, akkor ha m�g nem volt kiv�lasztva semmi
            if (string.IsNullOrEmpty(KodtarakDropDownListCsatolmany.SelectedValue))
            {
                KodtarakDropDownListCsatolmany.SelectedValue = KodTarak.CSATOLMANY_VIZSGALAT.MIND;
            }


            Irat_Kiadmanyoznikell_CheckBox.Checked = erec_IraIratokSearch.KiadmanyozniKell.Value == "1";

            Irat_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField =
                erec_IraIratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value;
            Irat_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            // Az oper�tort kell figyelni (isNull elvileg)
            CheckBoxMunkairatok.Checked = !String.IsNullOrEmpty(erec_IraIratokSearch.Manual_Alszam_MunkaanyagFilter.Operator);

            // BLG_7794
            if (erec_IraIratokSearch.Elintezett.Value == "1" && erec_IraIratokSearch.Elintezett.Operator == Query.Operators.equals)
            {
                RadioButtonList_Elintezett.SelectedValue = "1";
            }
            else
            {
                RadioButtonList_Elintezett.SelectedValue = String.IsNullOrEmpty(erec_IraIratokSearch.Elintezett.Operator) ? "" : "0";
            }
 
            cbMunkaUgyiratokIratai.Checked = !String.IsNullOrEmpty(erec_IraIratokSearch.Manual_UgyiratFoszam_MunkaanyagFilter.Operator);

            Irat_Jelleg_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRAT_JELLEG, erec_IraIratokSearch.Jelleg.Value, true, SearchHeader1.ErrorPanel);

            #region standard objektumf�gg� t�rgyszavak
            // TODO: jelenleg nem lehet az �gyirat t�rgyszavai alapj�n iratra keresni!
            //// �gyiratt�l �r�k�lt
            //sotUgyiratTargyszavak.FillStandardTargyszavak(null, null, Constants.TableNames.EREC_UgyUgyiratok, SearchHeader1.ErrorPanel);

            //if (sotUgyiratTargyszavak.Count > 0)
            //{
            //    StandardUgyiratTargyszavakPanel.Visible = true;

            //    // vigy�zat, felt�telezz�k, hogy felv�ltva lett hozz�adva t�rgysz� �s �rt�k...!
            //    // tov�bb� a t�rgysz�t k�pvisel� mez� nev�t vissza kell alak�tani (levenni az id�z�jelet)
            //    char Separator = '|';
            //    if (erec_IraIratokSearch.fts_tree_ugyirat_auto != null)
            //    {
            //        string[] items = erec_IraIratokSearch.fts_tree_ugyirat_auto.GetLeafList(Separator);
            //        if (items != null)
            //        {
            //            int i = 0;
            //            while (i + 1 < items.Length)
            //            {
            //                string items_targyszo = items[i];
            //                string items_ertek = items[i + 1];
            //                string[] item_elements_targyszo = items_targyszo.Split(Separator);
            //                string[] item_elements_ertek = items_ertek.Split(Separator);
            //                // t�rgysz� �rt�k�nek be�r�sa a mez�be
            //                sotUgyiratTargyszavak.TryFillFieldWithValue(item_elements_targyszo[1], item_elements_ertek[1]);

            //                i += 2;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    StandardUgyiratTargyszavakPanel.Visible = false;
            //}

            // irat saj�t auto
            /*sotIratTargyszavak.FillStandardTargyszavak(null, null, Constants.TableNames.EREC_IraIratok, SearchHeader1.ErrorPanel);

            if (sotIratTargyszavak.Count > 0)
            {
                StandardIratTargyszavakPanel.Visible = true;

                //// vigy�zat, felt�telezz�k, hogy felv�ltva lett hozz�adva t�rgysz� �s �rt�k...!
                //if (erec_IraIratokSearch.fts_tree_irat_auto != null)
                //{
                //    List<string> items = erec_IraIratokSearch.fts_tree_irat_auto.GetLeafList();
                //    if (items != null)
                //    {
                //        int i = 0;
                //        while (i + 1 < items.Count)
                //        {
                //            string items_targyszo = items[i];
                //            string items_ertek = items[i + 1];
                //            string[] item_elements_targyszo = items_targyszo.Split(FullTextSearchTree.FieldValueSeparator);
                //            string[] item_elements_ertek = items_ertek.Split(FullTextSearchTree.FieldValueSeparator);
                //            // t�rgysz� �rt�k�nek be�r�sa a mez�be
                //            sotIratTargyszavak.TryFillFieldWithValue(item_elements_targyszo[1], item_elements_ertek[1]);

                //            i += 2;
                //        }
                //    }
                //}
                sotIratTargyszavak.FillFromFTSTree(erec_IraIratokSearch.fts_tree_irat_auto);
            }
            else
            {
                StandardIratTargyszavakPanel.Visible = false;
            }*/
            #endregion standard objektumf�gg� t�rgyszavak

            #region standard objektumf�gg� t�rgyszavak irathoz
            FillObjektumTargyszavaiPanel_Iratok(null);
            if (otpStandardTargyszavak_Iratok.Count > 0)
            {
                StandardTargyszavakPanel_Iratok.Visible = true;
                otpStandardTargyszavak_Iratok.FillFromFTSTree(erec_IraIratokSearch.fts_tree_irat_auto);
            }
            else
            {
                StandardTargyszavakPanel_Iratok.Visible = false;
            }

            #endregion standard objektumf�gg� t�rgyszavak

            #region egy�b t�rgyszavak
            tszmsEgyebTargyszavak.FillFromFTSTree(erec_IraIratokSearch.fts_tree_irat_egyeb);
            #endregion egy�b t�rgyszavak


            //if (erec_IraIratokSearch.fts_targyszavak != null)
            //{
            //    Targyszavak_TextBoxTargyszo.Text = erec_IraIratokSearch.fts_targyszavak.Filter;
            //}
            //else
            //{
            //    Targyszavak_TextBoxTargyszo.Text = "";
            //}

            //if (erec_IraIratokSearch.fts_ertek != null)
            //{
            //    Targyszavak_TextBoxErtek.Text = erec_IraIratokSearch.fts_ertek.Filter;
            //}
            //else
            //{
            //    Targyszavak_TextBoxErtek.Text = "";
            //}

            //if (erec_IraIratokSearch.fts_note != null)
            //{
            //    Targyszavak_TextBoxNote.Text = erec_IraIratokSearch.fts_note.Filter;
            //}
            //else
            //{
            //    Targyszavak_TextBoxNote.Text = "";
            //}

            // BLG_1421
            if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
            {
                UgyFajtaja_KodtarakDropDownList.FillAndSetSelectedValue(KodTarak.UGY_FAJTAJA.KodcsoportKod,
                erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch != null ?
                erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch.UgyFajtaja.Value
                : String.Empty,
                true, SearchHeader1.ErrorPanel);
            }
            NemCsoporttagokkal_CheckBox.Checked = !erec_IraIratokSearch.Csoporttagokkal;
            CsakAktivIrat_CheckBox.Checked = erec_IraIratokSearch.CsakAktivIrat;

            // BUG_8523
            var itsz = "";
            if (!String.IsNullOrEmpty(erec_IraIratokSearch.WhereByManual))
            {
                var manual_IraIrattariTetel_Id = "EREC_UgyUgyiratok.IraIrattariTetel_Id='";
                var p = erec_IraIratokSearch.WhereByManual.IndexOf(manual_IraIrattariTetel_Id);
                if (p >= 0)
                {
                    itsz = erec_IraIratokSearch.WhereByManual.Substring(p + manual_IraIrattariTetel_Id.Length, Guid.Empty.ToString().Length);
                }
            }

            Irat_IraIrattariTetelTextBox.Id_HiddenField = itsz;
            Irat_IraIrattariTetelTextBox.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);
        }
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private EREC_IraIratokSearch SetSearchObjectFromComponents()
    {
        EREC_IraIratokSearch erec_IraIratokSearch = (EREC_IraIratokSearch)SearchHeader1.TemplateObject;
        if (erec_IraIratokSearch == null)
        {
            erec_IraIratokSearch = new EREC_IraIratokSearch(true);
        }

        if (!String.IsNullOrEmpty(UgyirDarab_IraIktatokonyvID_IraIktatoKonyvekDropDownList.SelectedValue))
        {
            // BUG 4242
            // Ez a r�sz kit�r�lve, mert a SetSearchObject-ben van lekezelve...


            UgyirDarab_IraIktatokonyvID_IraIktatoKonyvekDropDownList.SetSearchObject(erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch);            
        }

        IktKonyv_Ev_EvIntervallum_SearchFormControl1.SetSearchObjectFields(
            erec_IraIratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

        UgyirDarab_Foszam_SzamIntervallum_SearchFormControl1.SetSearchObjectFields(
            erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

        Irat_Alszam_SzamIntervallum_SearchFormControl2.SetSearchObjectFields(
            erec_IraIratokSearch.Alszam);

        if (!String.IsNullOrEmpty(UgyirDarab_UgyUgyirat_Id_UgyiratTextBox.Id_HiddenField))
        {
            erec_IraIratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.UgyUgyirat_Id.Filter(
                UgyirDarab_UgyUgyirat_Id_UgyiratTextBox.Id_HiddenField);
        }
        if (!String.IsNullOrEmpty(Irat_UgyUgyirDarab_UgyiratDarabTextBox.Id_HiddenField))
        {
            erec_IraIratokSearch.UgyUgyIratDarab_Id.Filter(
                Irat_UgyUgyirDarab_UgyiratDarabTextBox.Id_HiddenField);
        }

        if (!String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField))
        {
            erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.Partner_Id_Bekuldo.Filter(
                Kuld_PartnerId_Bekuldo_PartnerTextBox.Id_HiddenField);
        }
        else
        {
            if (!String.IsNullOrEmpty(Kuld_PartnerId_Bekuldo_PartnerTextBox.Text))
            {

                erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value = Kuld_PartnerId_Bekuldo_PartnerTextBox.Text;
                erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Operator = Search.GetOperatorByLikeCharater(Kuld_PartnerId_Bekuldo_PartnerTextBox.Text);
            }
        }

        if (!String.IsNullOrEmpty(Irat_Hivatkozasiszam_TextBox.Text))
        {
            erec_IraIratokSearch.HivatkozasiSzam.Value =
                Irat_Hivatkozasiszam_TextBox.Text;
            erec_IraIratokSearch.HivatkozasiSzam.Operator =
                Search.GetOperatorByLikeCharater(Irat_Hivatkozasiszam_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Irat_Tipus_KodtarakDropDownList.SelectedValue))
        {
            //erec_IraIratokSearch.Kategoria.Value = Irat_Kategoria_KodtarakDropDownList.SelectedValue;
            //erec_IraIratokSearch.Kategoria.Operator = Query.Operators.equals;
            // Irat kateg�ria helyett iratt�pus:
            erec_IraIratokSearch.Irattipus.Filter(Irat_Tipus_KodtarakDropDownList.SelectedValue);
        }

        Irat_LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraIratokSearch.Manual_LetrehozasIdo);
        Irat_Elintezes_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_IraIratokSearch.IntezesIdopontja);


        if (!String.IsNullOrEmpty(Kuld_KuldesMod_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIratokSearch.Extended_EREC_KuldKuldemenyekSearch.KuldesMod.Filter(
                Kuld_KuldesMod_KodtarakDropDownList.SelectedValue);
        }
        #region IKTATAS DATUMA
        ////Irat_IktatasDatuma_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(erec_IraIratokSearch.IktatasDatuma);
        DatumIdoIntervallum_SearchCalendarControl_IktatasDatuma.SetSearchObjectFields(erec_IraIratokSearch.IktatasDatuma);
        #endregion

        if (!String.IsNullOrEmpty(Irat_Jelleg_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIratokSearch.Jelleg.Filter(Irat_Jelleg_KodtarakDropDownList.SelectedValue);
        }

        Irat_SztornirozasDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraIratokSearch.SztornirozasDat);

        // CR3289: Vezet�i panelen lej�rt iratok keres�se a sz�r�skor elromlik
        // Nem volt lej�rat sz�r�s 
        Irat_Hatarido_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraIratokSearch.Hatarido);

        if (!String.IsNullOrEmpty(Irat_AdathordozoTipusa_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIratokSearch.AdathordozoTipusa.Filter(
                Irat_AdathordozoTipusa_KodtarakDropDownList.SelectedValue);
        }
        if (!String.IsNullOrEmpty(Irat_UgyintezesAlapja_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIratokSearch.UgyintezesAlapja.Filter(
                Irat_UgyintezesAlapja_KodtarakDropDownList.SelectedValue);
        }
        if (!String.IsNullOrEmpty(Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField))
        {
            //erec_IraIratokSearch.Csoport_Id_Felelos.Filter(
            //    Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField);

            // az irat helye �nmag�ban nem l�tezik, helyette a p�ld�nyokban keres�nk:
            // el�g, ha valamelyik p�ld�ny n�la van
            erec_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch.Csoport_Id_Felelos.Filter(
                Irat_CsoportId_Felelos_CsoportTextBox.Id_HiddenField);
        }
        if (!String.IsNullOrEmpty(Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            //erec_IraIratokSearch.FelhasznaloCsoport_Id_Orzo.Value =
            //    Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField;
            //erec_IraIratokSearch.FelhasznaloCsoport_Id_Orzo.Operator =
            //    Query.Operators.equals;

            // az irat helye �nmag�ban nem l�tezik, helyette a p�ld�nyokban keres�nk:
            // el�g, ha valamelyik p�ld�ny n�la van
            erec_IraIratokSearch.Extended_EREC_PldIratPeldanyokSearch.FelhasznaloCsoport_Id_Orzo.Filter(
                Irat_FelhCsoportId_Orzo_FelhasznaloCsoportTextBox.Id_HiddenField);

        }
        if (!String.IsNullOrEmpty(Irat_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_IraIratokSearch.FelhasznaloCsoport_Id_Ugyintez.Filter(
                Irat_FelhCsopId_Ugyintezo_FelhasznaloCsoportTextBox.Id_HiddenField);
        }

        if (!String.IsNullOrEmpty(Irat_Allapot_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIratokSearch.Allapot.Filter(Irat_Allapot_KodtarakDropDownList.SelectedValue);
        }
        if (!String.IsNullOrEmpty(KodtarakDropDownListPostazasIraanya.SelectedValue))
        {
            erec_IraIratokSearch.PostazasIranya.Filter(KodtarakDropDownListPostazasIraanya.SelectedValue);
        }

        if (!String.IsNullOrEmpty(Irat_FelhCsopId_Iktato_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_IraIratokSearch.FelhasznaloCsoport_Id_Iktato.Filter(
                Irat_FelhCsopId_Iktato_FelhasznaloCsoportTextBox.Id_HiddenField);
        }

        if (Irat_Kiadmanyoznikell_CheckBox.Checked)
        {
            erec_IraIratokSearch.KiadmanyozniKell.Filter("1");
        }

        if (CheckBoxMunkairatok.Checked)
        {
            // Akkor munkairat, ha az Alszam is null
            erec_IraIratokSearch.Manual_Alszam_MunkaanyagFilter.IsNull();
        }

        if (cbMunkaUgyiratokIratai.Checked)
        {
            erec_IraIratokSearch.Manual_UgyiratFoszam_MunkaanyagFilter.IsNull();
        }

        // BLG_7794
        if (RadioButtonList_Elintezett.SelectedValue == "1")
        {
            erec_IraIratokSearch.Elintezett.Filter("1");
        }
        else if (RadioButtonList_Elintezett.SelectedValue == "0")
        {
            erec_IraIratokSearch.Elintezett.IsNullOrNotIn(new string[] { "1" });
        }
        else
        {
            erec_IraIratokSearch.Elintezett.Clear();
        }

        #region FTS

        #region standard objektumf�gg� t�rgyszavak
        // TODO: jelenleg nem lehet �gyirat t�rgyszavai alapj�n iratra keresni!
        //// �gyiratt�l �r�k�lt
        //try
        //{
        //    FullTextSearchTree FTSTree = sotUgyiratTargyszavak.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);
        //    if (FTSTree != null)
        //    {
        //        erec_IraIratokSearch.fts_tree_ugyirat_auto = FTSTree;
        //        erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter = FTSTree.TransformToFTSContainsConditions();
        //    }
        //}
        //catch (FullTextSearchException e)
        //{
        //    ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        //}

        // irat saj�t auto
        try
        {
            //FullTextSearchTree FTSTree = sotIratTargyszavak.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);
            //if (FTSTree != null)
            //{
            //    erec_IraIratokSearch.fts_tree_irat_auto = FTSTree;
            //    string ObjektumTargyszavai_ObjIdFilter_Irat = FTSTree.TransformToFTSContainsConditions();
            //    if (!String.IsNullOrEmpty(ObjektumTargyszavai_ObjIdFilter_Irat))
            //    {
            //        if (!String.IsNullOrEmpty(erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter))
            //        {
            //            erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter += " INTERSECT ";
            //        }
            //        erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter += ObjektumTargyszavai_ObjIdFilter_Irat;
            //    }
            //}

            erec_IraIratokSearch.fts_tree_irat_auto = otpStandardTargyszavak_Iratok.BuildFTSTreeFromList(SearchHeader1.ErrorPanel);
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion standard objektumf�gg� t�rgyszavak

        #region egy�b t�rgyszavak
        try
        {
            erec_IraIratokSearch.fts_tree_irat_egyeb = tszmsEgyebTargyszavak.BuildFTSTree(SearchHeader1.ErrorPanel);
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion egy�b t�rgyszavak

        #region merge standard �s egy�b t�rgyszavak
        try
        {
            FullTextSearchTree mergedTree = FullTextSearchTree.MergeFTSTrees(erec_IraIratokSearch.fts_tree_irat_auto
                , erec_IraIratokSearch.fts_tree_irat_egyeb, FullTextSearchTree.Operator.Intersect);
            if (erec_IraIratokSearch.fts_tree_ugyirat_auto != null)
            {
                mergedTree = FullTextSearchTree.MergeFTSTrees(mergedTree
                    , erec_IraIratokSearch.fts_tree_ugyirat_auto, FullTextSearchTree.Operator.Intersect);
            }

            if (mergedTree != null)
            {
                erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter = mergedTree.TransformToFTSContainsConditions();
            }
            else
            {
                erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter = "";
            }
        }
        catch (FullTextSearchException e)
        {
            ResultError.DisplayErrorOnErrorPanel(SearchHeader1.ErrorPanel, e.ErrorCode, e.ErrorMessage);
        }
        #endregion merge standard �s egy�b t�rgyszavak

        if (!String.IsNullOrEmpty(Irat_Targy_TextBox.Text))
        {
            //erec_IraIratokSearch.fts_targy = new FullTextSearchField();
            //erec_IraIratokSearch.fts_targy.Filter = Irat_Targy_TextBox.Text;

            erec_IraIratokSearch.Targy.Value = Irat_Targy_TextBox.Text;
            erec_IraIratokSearch.Targy.Operator = Query.Operators.contains; // Search.GetOperatorByLikeCharater(Irat_Targy_TextBox.Text); // BUG_3944 kapcs�n visszarakva

            //string strContains = new SQLContainsCondition(Irat_Targy_TextBox.Text).Normalized;
            //if (!string.IsNullOrEmpty(erec_IraIratokSearch.WhereByManual))
            //{
            //    erec_IraIratokSearch.WhereByManual += " and ";
            //}
            //erec_IraIratokSearch.WhereByManual += "contains( EREC_IraIratok.Targy , '" + strContains + "' )";

        }

        //string strObjektumTargyszavai = ""; // kieg�sz�t� felt�tel az EREC_ObjektumTargyszavai t�bl�ra
        //if (!String.IsNullOrEmpty(Targyszavak_TextBoxTargyszo.Text))
        //{
        //    erec_IraIratokSearch.fts_targyszavak = new FullTextSearchField();
        //    erec_IraIratokSearch.fts_targyszavak.Filter = Targyszavak_TextBoxTargyszo.Text;

        //    string strContains = new SQLContainsCondition(Targyszavak_TextBoxTargyszo.Text).Normalized;
        //    strObjektumTargyszavai += "contains( EREC_ObjektumTargyszavai.Targyszo, '" + strContains + "' )";

        //}

        //if (!String.IsNullOrEmpty(Targyszavak_TextBoxErtek.Text))
        //{
        //    erec_IraIratokSearch.fts_ertek = new FullTextSearchField();
        //    erec_IraIratokSearch.fts_ertek.Filter = Targyszavak_TextBoxErtek.Text;

        //    string strContains = new SQLContainsCondition(Targyszavak_TextBoxErtek.Text).Normalized;
        //    if (!string.IsNullOrEmpty(strObjektumTargyszavai))
        //    {
        //        strObjektumTargyszavai += " and ";
        //    }
        //    strObjektumTargyszavai += "contains( EREC_ObjektumTargyszavai.Ertek, '" + strContains + "' )";

        //}

        //if (!String.IsNullOrEmpty(Targyszavak_TextBoxNote.Text))
        //{
        //    erec_IraIratokSearch.fts_note = new FullTextSearchField();
        //    erec_IraIratokSearch.fts_note.Filter = Targyszavak_TextBoxNote.Text;

        //    string strContains = new SQLContainsCondition(Targyszavak_TextBoxNote.Text).Normalized;
        //    if (!string.IsNullOrEmpty(strObjektumTargyszavai))
        //    {
        //        strObjektumTargyszavai += " and ";
        //    }
        //    strObjektumTargyszavai += "contains( EREC_ObjektumTargyszavai.Note, '" + strContains + "' )";

        //}

        //if (!String.IsNullOrEmpty(strObjektumTargyszavai))
        //{
        //    strObjektumTargyszavai = "SELECT Obj_Id FROM EREC_ObjektumTargyszavai WHERE " + strObjektumTargyszavai;

        //    if (!String.IsNullOrEmpty(erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter))
        //    {
        //        strObjektumTargyszavai = " INTERSECT " + strObjektumTargyszavai;
        //    }

        //    erec_IraIratokSearch.ObjektumTargyszavai_ObjIdFilter += strObjektumTargyszavai;
        //}

        #endregion

        // BLG_1421
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IKTATAS_UGYTIPUS_IRATMETADEFBOL, true))
        {
            if (!String.IsNullOrEmpty(UgyFajtaja_KodtarakDropDownList.SelectedValue))
            {
                if (erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch == null)
                {
                    erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch = new EREC_IraOnkormAdatokSearch();
                    erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch.ErvKezd.Clear();
                    erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch.ErvVege.Clear();
                }
                erec_IraIratokSearch.Extended_EREC_IraOnkormAdatokSearch.UgyFajtaja.Filter(UgyFajtaja_KodtarakDropDownList.SelectedValue);
            }
        }

        erec_IraIratokSearch.Csoporttagokkal = !NemCsoporttagokkal_CheckBox.Checked;
        erec_IraIratokSearch.CsakAktivIrat = CsakAktivIrat_CheckBox.Checked;
        Search.SetFilter_NemLezartUgyirat(erec_IraIratokSearch);
        //erec_IraIratokSearch.CsatolmanySzures = Convert.ToInt32(KodtarakDropDownListCsatolmany.SelectedValue);

        if (!String.IsNullOrEmpty(KodtarakDropDownListCsatolmany.SelectedValue))
        {
            erec_IraIratokSearch.CsatolmanySzures.Filter(KodtarakDropDownListCsatolmany.SelectedValue);
        }

        // BUG_8523
        if (!String.IsNullOrEmpty(Irat_IraIrattariTetelTextBox.Id_HiddenField))
        {
            erec_IraIratokSearch.WhereByManual = Search.ConcatWhereExpressions(erec_IraIratokSearch.WhereByManual,
                 "EREC_UgyUgyiratok.IraIrattariTetel_Id='" + Irat_IraIrattariTetelTextBox.Id_HiddenField + "' ");
        }

        return erec_IraIratokSearch;
    }

    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
        //bernat.laszlo added: Tal�lati list�k ment�se
        else if (e.CommandName == CommandName.NewResultList)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            SearchHeader1.NewResultList(SetSearchObjectFromComponents(), execParam);
        }
        //bernat.laszlo eddig
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
            SearchFooter1.SetDefaultButton();
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraIratokSearch searchObject = SetSearchObjectFromComponents();

            if (Search.IsDefaultSearchObject_EREC_IraIratokSearch(searchObject, GetDefaultSearchObject())) // �sszehasonl�t�s az "�res" default objektummal
            {
                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);

                if (!String.IsNullOrEmpty(SearchHeader1.CustomTemplateTipusNev))
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, SearchHeader1.CustomTemplateTipusNev);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }

            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);

            #region panelek elt�ntet�se, hogy ne k�ldj�nk vissza feleslegesen adatokat
            //StandardUgyiratTargyszavakPanel.Visible = false;
            StandardTargyszavakPanel_Iratok.Visible = false;
            //EFormPanelFTS.Visible = false;
            EFormPanelOTMultiSearch.Visible = false;
            EFormPanel1.Visible = false;
            #endregion
        }
    }

    private EREC_IraIratokSearch GetDefaultSearchObject()
    {
        ////SearchHeader1.TemplateReset();
        //return new EREC_IraIratokSearch(true);

        EREC_IraIratokSearch defaultSearch = (EREC_IraIratokSearch)Search.GetDefaultSearchObject(_type);

        if (Startup == Constants.Startup.FromMunkaUgyiratokIratai)
        {
            defaultSearch = Search.GetDefaultSearchObject_EREC_IraIratokSearchForMunkaUgyiratok(Page,false);//new EREC_IraIratokSearch(true);
        }
        else if (Startup == Constants.Startup.FromKozgyulesiEloterjesztesek)
        {
            Search.SetSearchObjectSpecFilter_Irat_KozgyulesiEloterjesztes(defaultSearch);
        }
        else if (Startup == Constants.Startup.FromBizottsagiEloterjesztesek)
        {
            Search.SetSearchObjectSpecFilter_Irat_BizottsagiEloterjesztes(defaultSearch);
        }
        // CR3221 Szign�l�s(el�k�sz�t�s) kezel�s
        else if (Startup == Constants.Startup.FromMunkairatSzignalas)
        {
            defaultSearch = new EREC_IraIratokSearch(true);
        }
        else if (Startup == Constants.Startup.FromHivataliKapusMunkaUgyiratokIratai)
        {
            defaultSearch = new EREC_IraIratokSearch(true);
        }
        return defaultSearch;
    }

    protected void FillObjektumTargyszavaiPanel_Iratok(String id)
    {
        EREC_ObjektumTargyszavaiSearch search = new EREC_ObjektumTargyszavaiSearch();
        otpStandardTargyszavak_Iratok.FillObjektumTargyszavai(search, id, null
            , Constants.TableNames.EREC_IraIratok, null, null, false
            , KodTarak.OBJMETADEFINICIO_TIPUS.B1, false, true, SearchHeader1.ErrorPanel);
    }

}
