<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IraIrattariTetelekForm.aspx.cs" Inherits="IraIrattariTetelekForm" Title="Iratt�ri t�telsz�mok karbantart�sa" %>

<%@ Register Src="eRecordComponent/AgazatiJelTextBox.ascx" TagName="AgazatiJelTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %> 
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<%@ Register Src="~/Component/FuggoKodtarakDropDownList.ascx" TagPrefix="uc1" TagName="FuggoKodtarakDropDownList" %>
<%@ Register Src="~/eRecordComponent/FunkcioGombsor.ascx" TagPrefix="uc17" TagName="FunkcioGombsor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 868px">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label9" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="Label3" runat="server" Text="�gazati jel:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" colspan="2">
                                    <uc7:AgazatiJelTextBox ID="AgazatiJelTextBox1" runat="server" TextMode="MultiLine"
                                        Rows="3"></uc7:AgazatiJelTextBox>
                                </td>
                            </tr>
                            <tr id="tr_AutoGeneratePartner_CheckBox" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="Label2" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" colspan="2">
                                    <asp:TextBox ID="IrattariTetelszam_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Label5" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    <asp:Label ID="Label16" runat="server" Text="ITSZ megnevez�se:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" colspan="2">
                                    <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput" TextMode="MultiLine"
                                        Rows="3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="tr_Tipus_dropdown" class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    &nbsp;<asp:Label ID="Label11" runat="server" Text="Meg�rz�si m�d:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" colspan="2">
                                    <uc11:KodtarakDropDownList ID="IrattariJel_KodtarakDropDownList" runat="server">
                                    </uc11:KodtarakDropDownList>
                                </td>
                            </tr>
                            <%--BLG_2156--%>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label6" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                    &nbsp;<asp:Label ID="Label1" runat="server" Text="�rz�si id�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" style="width: 100px">
                                    <uc11:KodtarakDropDownList ID="Megorzesi_Ido_KodtarakDropDownList" runat="server"
                                        __designer:wfdid="w1" CssClass="mrUrlapInputFullComboBox"></uc11:KodtarakDropDownList>
                                    </td>
                                <td class="mrUrlapMezo">
                                    <uc1:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" KodcsoportKod="IDOEGYSEG_FELHASZNALAS"   CssClass="hiddenitem" />
                                    <uc1:FuggoKodtarakDropDownList runat="server" ID="FuggoKodtarakDropDownList_Idoegyseg" KodcsoportKod="IDOEGYSEG"  ParentControlId="FuggoKodtarakDropDownList_IdoegysegFelhasznalas" CssClass="mrUrlapInputComboBoxKozepes" />
 
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="Label12" runat="server" Text="�ven t�li iktat�s:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo" colspan="2">
                                    <asp:CheckBox runat="server" ID="cbEvenTuliIktatas" CssClass="urlapCheckbox" />
                                </td>
                            </tr>
                            <%--<tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="Foly� cm:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Folyo_CM_TextBox" runat="server" Width="97px"></asp:TextBox></td>
                        </tr>--%><tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label8" runat="server" Text="�rv�nyess�g:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo" colspan="2">
                                <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server">
                                </uc6:ErvenyessegCalendarControl>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
                &nbsp; &nbsp;&nbsp; &nbsp;
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
            <td valign="top" style="width: 0px">
                <div id="divFunkcioGombsor" runat="server" visible="false" style="margin-top: 8px;"></div>
                <uc17:funkciogombsor ID="FunkcioGombsor" runat="server" Visible="true" />
            </td>
        </tr>
    </table>
</asp:Content>

