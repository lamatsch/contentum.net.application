using System;

using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class IraIrattariTetelekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    #region BLG_2968
    private String _ParentForm = "";
    public String ParentForm
    {
        get { return _ParentForm; }
        set
        {
            _ParentForm = value;
            FunkcioGombsor.ParentForm = value;
        }
    }
    #endregion

    private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";
    private const string kcs_SELEJTEZESI_IDO = "SELEJTEZESI_IDO";
    // BLG_2156
    private const string kcs_IDOEGYSEG_FELHASZNALAS = "IDOEGYSEG_FELHASZNALAS";

    protected void Page_Init(object sender, EventArgs e)
    {
       
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariTetel" + Command);
                break;
        }

        FunkcioGombsor.HideButtons();
        ParentForm = this.Form.ID;
        FunkcioGombsor.ParentForm = ParentForm;

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraIrattariTetelek EREC_IraIrattariTetelek = (EREC_IraIrattariTetelek)result.Record;
                    LoadComponentsFromBusinessObject(EREC_IraIrattariTetelek);


                    #region BLG_2968
                    FunkcioGombsor.XMLExportVisible = true;
                    FunkcioGombsor.XMLExportEnabled = true;
                    FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.IrattariTetelId + "=" + id + "&tipus=IrattariTetel')";
                    #endregion
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            if (!IsPostBack)
            {
                IrattariJel_KodtarakDropDownList.FillDropDownList(kcsKod_IRATTARI_JEL, FormHeader1.ErrorPanel);
                // BLG_2156
                Megorzesi_Ido_KodtarakDropDownList.FillDropDownList(kcs_SELEJTEZESI_IDO, false, FormHeader1.ErrorPanel);
                FuggoKodtarakDropDownList_IdoegysegFelhasznalas.SelectedValue = KodTarak.IdoegysegFelhasznalas.Selejtezes;
            }

            String AgazatiJel_Id = Request.QueryString.Get(QueryStringVars.AgazatiJelId);
            if (!String.IsNullOrEmpty(AgazatiJel_Id))
            {
                AgazatiJelTextBox1.Id_HiddenField = AgazatiJel_Id;
                AgazatiJelTextBox1.SetAgazatiJelTextBoxById(FormHeader1.ErrorPanel);
                AgazatiJelTextBox1.ReadOnly = true;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraIrattariTetelekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraIrattariTetelek erec_IraIrattariTetelek)
    {
        AgazatiJelTextBox1.Id_HiddenField = erec_IraIrattariTetelek.AgazatiJel_Id;
        AgazatiJelTextBox1.SetAgazatiJelTextBoxById(FormHeader1.ErrorPanel);

        IrattariTetelszam_TextBox.Text = erec_IraIrattariTetelek.IrattariTetelszam;
        Nev_TextBox.Text = erec_IraIrattariTetelek.Nev;
        IrattariJel_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_IRATTARI_JEL, erec_IraIrattariTetelek.IrattariJel, FormHeader1.ErrorPanel);

        
        if (!erec_IraIrattariTetelek.Base.Typed.LetrehozasIdo.IsNull)
            Megorzesi_Ido_KodtarakDropDownList.Ervenyesseg = erec_IraIrattariTetelek.Base.Typed.LetrehozasIdo.Value;
        Megorzesi_Ido_KodtarakDropDownList.FillAndSetSelectedValue(kcs_SELEJTEZESI_IDO,
            erec_IraIrattariTetelek.MegorzesiIdo, false, FormHeader1.ErrorPanel);
        // BLG_2156
        FuggoKodtarakDropDownList_IdoegysegFelhasznalas.SelectedValue = KodTarak.IdoegysegFelhasznalas.Selejtezes;
         FuggoKodtarakDropDownList_Idoegyseg.SelectedValue = erec_IraIrattariTetelek.Idoegyseg;


        cbEvenTuliIktatas.Checked = "1".Equals(erec_IraIrattariTetelek.EvenTuliIktatas);
        
        ErvenyessegCalendarControl1.ErvKezd = erec_IraIrattariTetelek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_IraIrattariTetelek.ErvVege;

        FormHeader1.Record_Ver = erec_IraIrattariTetelek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_IraIrattariTetelek.Base);

        if (Command == CommandName.View)
        {
            AgazatiJelTextBox1.ReadOnly = true;
            IrattariTetelszam_TextBox.ReadOnly = true;
            Nev_TextBox.ReadOnly = true;
            IrattariJel_KodtarakDropDownList.ReadOnly = true;
            Megorzesi_Ido_KodtarakDropDownList.ReadOnly = true;
            // BLG_2156
            FuggoKodtarakDropDownList_Idoegyseg.ReadOnly = true;
            
            ErvenyessegCalendarControl1.ReadOnly = true;
            cbEvenTuliIktatas.Enabled = false;
        }
    }

    // form --> business object
    private EREC_IraIrattariTetelek GetBusinessObjectFromComponents()
    {
        EREC_IraIrattariTetelek erec_IraIrattariTetelek = new EREC_IraIrattariTetelek();
        // �sszes mez� update-elhet�s�g�t kezdetben letiltani:
        erec_IraIrattariTetelek.Updated.SetValueAll(false);
        erec_IraIrattariTetelek.Base.Updated.SetValueAll(false);

        erec_IraIrattariTetelek.AgazatiJel_Id = AgazatiJelTextBox1.Id_HiddenField;
        erec_IraIrattariTetelek.Updated.AgazatiJel_Id = pageView.GetUpdatedByView(AgazatiJelTextBox1);

        erec_IraIrattariTetelek.IrattariTetelszam = IrattariTetelszam_TextBox.Text;
        erec_IraIrattariTetelek.Updated.IrattariTetelszam = pageView.GetUpdatedByView(IrattariTetelszam_TextBox);

        erec_IraIrattariTetelek.Nev = Nev_TextBox.Text;
        erec_IraIrattariTetelek.Updated.Nev = pageView.GetUpdatedByView(Nev_TextBox);

        erec_IraIrattariTetelek.IrattariJel = IrattariJel_KodtarakDropDownList.SelectedValue;
        erec_IraIrattariTetelek.Updated.IrattariJel = pageView.GetUpdatedByView(IrattariJel_KodtarakDropDownList);

        erec_IraIrattariTetelek.MegorzesiIdo = Megorzesi_Ido_KodtarakDropDownList.SelectedValue;
        erec_IraIrattariTetelek.Updated.MegorzesiIdo = pageView.GetUpdatedByView(Megorzesi_Ido_KodtarakDropDownList);
        // BLG_2156
        erec_IraIrattariTetelek.Idoegyseg = FuggoKodtarakDropDownList_Idoegyseg.SelectedValue;
        erec_IraIrattariTetelek.Updated.Idoegyseg = pageView.GetUpdatedByView(FuggoKodtarakDropDownList_Idoegyseg);

        erec_IraIrattariTetelek.EvenTuliIktatas = cbEvenTuliIktatas.Checked ? "1" : "0";
        erec_IraIrattariTetelek.Updated.EvenTuliIktatas = pageView.GetUpdatedByView(cbEvenTuliIktatas);

        /// fel�letr�l lev�ve, nem tom mi a b�natra j�
        if (Command == CommandName.New)
        {
            erec_IraIrattariTetelek.Folyo_CM = "0";
            erec_IraIrattariTetelek.Updated.Folyo_CM = true;
        }
        //erec_IraIrattariTetelek.Updated.Folyo_CM = pageView.GetUpdatedByView(Folyo_CM_TextBox);
        ///

        //erec_IraIrattariTetelek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //erec_IraIrattariTetelek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //erec_IraIrattariTetelek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //erec_IraIrattariTetelek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_IraIrattariTetelek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        erec_IraIrattariTetelek.Base.Ver = FormHeader1.Record_Ver;
        erec_IraIrattariTetelek.Base.Updated.Ver = true;

        return erec_IraIrattariTetelek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(AgazatiJelTextBox1);
            compSelector.Add_ComponentOnClick(IrattariTetelszam_TextBox);
            compSelector.Add_ComponentOnClick(Nev_TextBox);
            compSelector.Add_ComponentOnClick(IrattariJel_KodtarakDropDownList);
            compSelector.Add_ComponentOnClick(Megorzesi_Ido_KodtarakDropDownList);
            // BLG_2156
            compSelector.Add_ComponentOnClick(FuggoKodtarakDropDownList_Idoegyseg);

            //compSelector.Add_ComponentOnClick(Folyo_CM_TextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);
            
            FormFooter1.SaveEnabled = false;            
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                            EREC_IraIrattariTetelek erec_IraIrattariTetelek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_IraIrattariTetelek);
                           
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                                {
                                    String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                    if (!String.IsNullOrEmpty(refreshCallingWindow)
                                        && refreshCallingWindow == "1"
                                        )
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                    }
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                                EREC_IraIrattariTetelek erec_IraIrattariTetelek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_IraIrattariTetelek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
