<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="IraIrattariTetelekList.aspx.cs" Inherits="IraIrattariTetelekList" Title="Iratt�ri t�telsz�mok lek�rdez�se" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
<%--HiddenFields--%> 

   
<%--HiddenFields--%> 
<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="IraIrattariTetelekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="IraIrattariTetelekUpdatePanel" runat="server" OnLoad="IraIrattariTetelekUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="IraIrattariTetelekCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="IraIrattariTetelekCPEButton"
                            CollapseControlID="IraIrattariTetelekCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="IraIrattariTetelekCPEButton" ExpandedSize="0" ExpandedText="Iratt�ri t�telsz�mok list�ja"
                            CollapsedText="Iratt�ri t�telsz�mok list�ja">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <asp:GridView ID="IraIrattariTetelekGridView" runat="server" OnRowCommand="IraIrattariTetelekGridView_RowCommand"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
                                            PagerSettings-Visible="false" AllowSorting="True" OnPreRender="IraIrattariTetelekGridView_PreRender"
                                            AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="IraIrattariTetelekGridView_Sorting"
                                            OnRowDataBound="IraIrattariTetelekGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <div class="DisableWrap">
                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:BoundField DataField="AgazatiJelek_Kod" HeaderText="�gazati&nbsp;jel" SortExpression="AgazatiJelek_Kod">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IrattariTetelszam" HeaderText="Iratt�ri&nbsp;t�telsz�m" SortExpression="IrattariTetelszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nev" HeaderText="ITSZ megnevez�se" SortExpression="Nev" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="400px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IRATTARI_JEL" HeaderText="Iratt�ri&nbsp;jel" SortExpression="IRATTARI_JEL" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>  
                                                <%--BLG_2156--%>
                                                <asp:BoundField DataField="MegorzesiIdo_Nev" HeaderText="�rz�si&nbsp;id�" SortExpression="MegorzesiIdo_Nev"  >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>  
                                                <asp:BoundField DataField="Idoegyseg_Nev" HeaderText="Id�egys�g" SortExpression="Idoegyseg_Nev"  >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>  
                                                <%--<asp:BoundField DataField="Folyo_CM" HeaderText="Foly� cm" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                    SortExpression="Folyo_CM" HeaderStyle-Width="100px" />--%>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <%--<asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />--%>
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            
                                       <%-- <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                            TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                            CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                                        </ajaxToolkit:CollapsiblePanelExtender>--%>
                                        <asp:Panel ID="Panel8" runat="server" Visible="false">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server"  Width="100%">
                                                <ajaxToolkit:TabPanel ID="IraIktatoKonyvekTabPanel" runat="server" TabIndex="0">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Header1" runat="server" Text="Hozz�rendelt iktat�k�nyvek"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <asp:UpdatePanel ID="IraIktatoKonyvekUpdatePanel" runat="server" OnLoad="IraIktatoKonyvekUpdatePanel_Load">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="IraIktatoKonyvekPanel" runat="server" Visible="false" Width="100%">
                                                                    <uc1:SubListHeader ID="IraIktatoKonyvekSubListHeader" runat="server" />
                                                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                                <ajaxToolkit:CollapsiblePanelExtender ID="IraIktatoKonyvekCPE" runat="server" TargetControlID="Panel2"
                                                                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                    AutoExpand="false" ExpandedSize="0">
                                                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                                                <asp:Panel ID="Panel2" runat="server">
                                                                                    <asp:GridView ID="IraIktatoKonyvekGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                                        AllowSorting="True" AutoGenerateColumns="false" OnSorting="IraIktatoKonyvekGridView_Sorting"
                                                                                        OnPreRender="IraIktatoKonyvekGridView_PreRender" OnRowCommand="IraIktatoKonyvekGridView_RowCommand"
                                                                                        DataKeyNames="Id">
                                                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                                <HeaderTemplate>
                                                                                                    <div class="DisableWrap">
                                                                                                    <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                    &nbsp;&nbsp;
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                                    </div>
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                                <HeaderStyle Width="25px" />
                                                                                                <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            </asp:CommandField>
                                                                                            <asp:BoundField DataField="Ev" HeaderText="�v" SortExpression="Ev" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="120px" />
                                                                                            <asp:BoundField DataField="MegkulJelzes" HeaderText="Megk�l.&nbsp;jelz�s" SortExpression="MegkulJelzes" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="150px" />
                                                                                            <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="150px" />
                                                                                            <asp:BoundField DataField="UtolsoFoszam" HeaderText="Utols�&nbsp;f�sz�m" SortExpression="UtolsoFoszam" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="120px" />
                                                                                            
                                                                                            <asp:TemplateField HeaderText="Lez�r�s&nbsp;d�tuma" HeaderStyle-Width="150px"
                                                                                                 HeaderStyle-CssClass="GridViewHeaderStyle">                                                                                                  
                                                                                           </asp:TemplateField>
                       
                                                                                            <%--<asp:BoundField DataField="Titkos" HeaderText="Nem nyilv�nos" SortExpression="Titkos" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                            HeaderStyle-Width="100px" />
                                                                                            <asp:BoundField DataField="IktSzamOsztas" HeaderText="Ikt.sz�m oszt�s" SortExpression="IktSzamOsztas" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                             HeaderStyle-Width="200px" />--%>
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />

</asp:Content>

