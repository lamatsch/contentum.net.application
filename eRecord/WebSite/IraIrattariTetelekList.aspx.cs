using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class IraIrattariTetelekList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariTetelekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        IraIktatoKonyvekSubListHeader.RowCount_Changed += new EventHandler(IraIktatoKonyvekSubListHeader_RowCount_Changed);

        IraIktatoKonyvekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IraIktatoKonyvekSubListHeader_ErvenyessegFilter_Changed);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.IraIrattariTetelekListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_IraIrattariTetelekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;
        ListHeader1.PrintVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, IraIrattariTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, IraIrattariTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraIrattariTetelekGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraIrattariTetelekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraIrattariTetelekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraIrattariTetelekGridView.ClientID);
       // ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IraIrattariTetelekListajaPrintForm.aspx");
       ListHeader1.PrintOnClientClick = "javascript:window.open('IraIrattariTetelekListajaPrintFormSSRS.aspx')";

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraIrattariTetelekGridView.ClientID);
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IraIrattariTetelekUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView =IraIrattariTetelekGridView;

        IraIktatoKonyvekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IraIktatoKonyvekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IraIktatoKonyvekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IraIktatoKonyvekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraIktatoKonyvekGridView.ClientID);
        IraIktatoKonyvekSubListHeader.ButtonsClick += new CommandEventHandler(IraIktatoKonyvekSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        IraIktatoKonyvekSubListHeader.AttachedGridView = IraIktatoKonyvekGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(IraIrattariTetelekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(IraIktatoKonyvekGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraIrattariTetelekSearch());


        if (!IsPostBack) IraIrattariTetelekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.ViewHistory);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + CommandName.Lock);

        IraIktatoKonyvekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "IktatoKonyvekList");

        // a Modify jogot ellen�rizz�k a New-ra, mivel itt az �j kapcsolatn�l az IktatoKonyv.DefaultIrattariTetelszam 
        // m�dos�t�sa t�rt�nik
        IraIktatoKonyvekSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IktatoKonyv" + CommandName.Modify);
        IraIktatoKonyvekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IktatoKonyv" + CommandName.View);

        // m�dos�t�snak nincs �rtelme az Iktat�k�nyv - Iratt�riT�telsz�m kapcsolat formon
        IraIktatoKonyvekSubListHeader.ModifyEnabled = false;
        //IraIktatoKonyvekSubListHeader.ModifyVisible = false;
        //IraIktatoKonyvekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IktatoKonyv" + CommandName.Modify);

        // a Modify jogot ellen�rizz�k a kapcsolat Invalidate-ra, mivel itt az �j kapcsolatn�l az IktatoKonyv.DefaultIrattariTetelszam 
        // m�dos�t�sa t�rt�nik
        IraIktatoKonyvekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IktatoKonyv" + CommandName.Modify);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }
    }

    #endregion


    #region Master List

    protected void IraIrattariTetelekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraIrattariTetelekGridView", ViewState, "AgazatiJelek_Kod, IrattariTetelszam, Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraIrattariTetelekGridView", ViewState);

        IraIrattariTetelekGridViewBind(sortExpression, sortDirection);
    }

    protected void IraIrattariTetelekGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraIrattariTetelekSearch search = (EREC_IraIrattariTetelekSearch)Search.GetSearchObject(Page, new EREC_IraIrattariTetelekSearch());
        search.OrderBy = Search.GetOrderBy("IraIrattariTetelekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(IraIrattariTetelekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void IraIrattariTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void IraIrattariTetelekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraIrattariTetelekGridView.PageIndex;

        IraIrattariTetelekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IraIrattariTetelekGridView.PageCount;

        if (prev_PageIndex != IraIrattariTetelekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page,IraIrattariTetelekCPE);
            IraIrattariTetelekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IraIrattariTetelekCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IraIrattariTetelekGridView);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraIrattariTetelekGridViewBind();
    }

    protected void IraIrattariTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraIrattariTetelekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            ActiveTabRefresh(TabContainer1, id);

            //IraIrattariTetelekGridView_SelectRowCommand(id);
        }
    }

    //private void IraIrattariTetelekGridView_SelectRowCommand(string felhasznaloId)
    //{
    //    //ActiveTabRefresh(TabContainer1, felhasznaloId);
    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth_1280, Defaults.PopupHeight, IraIrattariTetelekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth_1280, Defaults.PopupHeight, IraIrattariTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraIrattariTetelek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraIrattariTetelekUpdatePanel.ClientID);

            IraIktatoKonyvekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelek_IktatoKonyvek_Form.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.IrattariTetelId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IraIktatoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void IraIrattariTetelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraIrattariTetelekGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIraIrattariTetelek();
            IraIrattariTetelekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraIrattariTetelekRecords();
                IraIrattariTetelekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraIrattariTetelekRecords();
                IraIrattariTetelekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIraIrattariTetelek();
                break;
        }
    }

    private void LockSelectedIraIrattariTetelekRecords()
    {
        LockManager.LockSelectedGridViewRecords(IraIrattariTetelekGridView, "EREC_IraIrattariTetelek"
            , "IrattariTetelLock", "IrattariTetelForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraIrattariTetelekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IraIrattariTetelekGridView, "EREC_IraIrattariTetelek"
            , "IrattariTetelLock", "IrattariTetelForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a IraIrattariTetelekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedIraIrattariTetelek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraIrattariTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraIrattariTetelek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IraIrattariTetelekGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraIrattariTetelek");
        }
    }

    protected void IraIrattariTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraIrattariTetelekGridViewBind(e.SortExpression, UI.GetSortToGridView("IraIrattariTetelekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (IraIrattariTetelekGridView.SelectedIndex == -1)
        {
            return;
        }
        string irattariTetelId = UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, irattariTetelId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string irattariTetelId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                IraIktatoKonyvekGridViewBind(irattariTetelId);
                IraIktatoKonyvekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(IraIktatoKonyvekGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        IraIktatoKonyvekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(IraIktatoKonyvekTabPanel))
        {
            IraIktatoKonyvekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(IraIktatoKonyvekGridView));
        }
    }

    #endregion


    #region IraIktatoKonyvek Detail

    private void IraIktatoKonyvekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_IktatoKonyv_IrattariTetel_Pairs();
            IraIktatoKonyvekGridViewBind(UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
        }
    }

    protected void IraIktatoKonyvekGridViewBind(string irattariTetelId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraIktatoKonyvekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraIktatoKonyvekGridView", ViewState);
        
        IraIktatoKonyvekGridViewBind(irattariTetelId, sortExpression, sortDirection);
    }

    protected void IraIktatoKonyvekGridViewBind(string irattariTetelId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(irattariTetelId))
        {
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
           
            EREC_IraIktatoKonyvekSearch search = new EREC_IraIktatoKonyvekSearch();
                        
            search.DefaultIrattariTetelszam.Value = irattariTetelId;
            search.DefaultIrattariTetelszam.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("IraIktatoKonyvekGridView", ViewState, SortExpression, SortDirection);
            
            IraIktatoKonyvekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);
            UI.GridViewFill(IraIktatoKonyvekGridView, res, IraIktatoKonyvekSubListHeader, EErrorPanel1, ErrorUpdatePanel);

        }
        else
        {
            ui.GridViewClear(IraIktatoKonyvekGridView);
        }
    }


    void IraIktatoKonyvekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        IraIktatoKonyvekGridViewBind(UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
    }

    protected void IraIktatoKonyvekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(IraIktatoKonyvekTabPanel))
                    //{
                    //    IraIktatoKonyvekGridViewBind(UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
                    //}
                    ActiveTabRefreshDetailList(IraIktatoKonyvekTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a kijel�lt hozz�rendel�seket, vagyis az EREC_IraIktatoKonyvek.DefaultIrattariTetelszam mez�t (update m�velet val�j�ban)
    /// </summary>
    private void deleteSelected_IktatoKonyv_IrattariTetel_Pairs()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IktatoKonyvModify"))
        {
            
            string irattariTetelId = UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView);            

            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraIktatoKonyvekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            //ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = deletableItemsList[i];

                // le kell k�rni a rekordot (nem tudjuk a verzi�j�t), �s le kell ellen�rizni, hogy biztosan az az
                // iratt�rit�telsz�m van-e be�ll�tva
                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraIktatoKonyvek iktatoKonyv = (EREC_IraIktatoKonyvek)result.Record;
                    if (iktatoKonyv != null && iktatoKonyv.DefaultIrattariTetelszam == irattariTetelId)
                    {
                        // DefaultIrattariTetelszam t�rl�se
                        iktatoKonyv.Updated.SetValueAll(false);
                        iktatoKonyv.Base.Updated.SetValueAll(false);

                        iktatoKonyv.DefaultIrattariTetelszam = Constants.BusinessDocument.nullString;
                        iktatoKonyv.Updated.DefaultIrattariTetelszam = true;

                        iktatoKonyv.Base.Updated.Ver = true;

                        Result result_update = service.Update(execParam, iktatoKonyv);
                        if (!String.IsNullOrEmpty(result_update.ErrorCode))
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_update);
                            ErrorUpdatePanel.Update();
                        }
                    }
                    else
                    {
                        // nem t�r�lj�k a mez�t, valami hiba volt a lek�r�s sor�n:
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
                                    , Resources.Error.UIDeleteError_Text);
                        ErrorUpdatePanel.Update();
                        break;
                    }
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }            
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void IraIktatoKonyvekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraIktatoKonyvekGridView, selectedRowNumber, "check");
        }
    }

    private void IraIktatoKonyvekGridView_RefreshOnClientClicks(string IraIktatoKonyvId)
    {
        string id = IraIktatoKonyvId;
        if (!String.IsNullOrEmpty(id))
        {
            IraIktatoKonyvekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelek_IktatoKonyvek_Form.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IraIktatoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            /// m�dos�t�snak nincs �rtelme itt
            //IraIktatoKonyvekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("CsoporttagokForm.aspx"
            //    , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, IraIktatoKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void IraIktatoKonyvekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraIktatoKonyvekGridView.PageIndex;

        IraIktatoKonyvekGridView.PageIndex = IraIktatoKonyvekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != IraIktatoKonyvekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IraIktatoKonyvekCPE);
            IraIktatoKonyvekGridViewBind(UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(IraIktatoKonyvekSubListHeader.Scrollable, IraIktatoKonyvekCPE);
        }
        IraIktatoKonyvekSubListHeader.PageCount = IraIktatoKonyvekGridView.PageCount;
        IraIktatoKonyvekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(IraIktatoKonyvekGridView);
    }


    void IraIktatoKonyvekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(IraIktatoKonyvekSubListHeader.RowCount);
        IraIktatoKonyvekGridViewBind(UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView));
    }

    protected void IraIktatoKonyvekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraIktatoKonyvekGridViewBind(UI.GetGridViewSelectedRecordId(IraIrattariTetelekGridView)
            , e.SortExpression, UI.GetSortToGridView("IraIktatoKonyvekGridView", ViewState, e.SortExpression));
    }

    #endregion

}
