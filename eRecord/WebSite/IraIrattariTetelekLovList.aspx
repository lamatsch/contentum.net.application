<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    EnableEventValidation="True" CodeFile="IraIrattariTetelekLovList.aspx.cs" Inherits="IraIrattariTetelekLovList"
    Title="Iratt�ri t�telek kiv�laszt�sa" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,IraIrattariTetelekLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr class="urlapSor">
                                                        <td class="mrUrlapCaption">
                                                            <asp:Label ID="Label1" runat="server" Text="�gyk�r k�d:" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="Tetelszam_TextBoxSearch" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td class="mrUrlapCaption">
                                                            <asp:Label ID="Label2" runat="server" Text="ITSZ megnevez�se:" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="Nev_TextBox" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="4">
                                                            <uc9:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1"
                                                                runat="server" />
                                                            &nbsp;
                                                        </td>
<%--                                                        <td class="mrUrlapCaption">
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                        </td>--%>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="4">
                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                                AlternateText="Keres�s"></asp:ImageButton>
                                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                                AlternateText="R�szletes keres�s"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" /></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="IraIrattariTetelekCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table style="width: 98%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                                                <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField ID="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    CellPadding="0" BorderWidth="1px" AllowPaging="True" PagerSettings-Visible="false"
                                                                    AutoGenerateColumns="False" DataKeyNames="Id">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Merge_IrattariTetelszam" SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam"
                                                                            HeaderText="T&#233;telsz&#225;m">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="200px"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Nev" SortExpression="Nev" HeaderText="Megnevez&#233;s">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="80%"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IrattariJel" SortExpression="IrattariJel">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IrattariTetelszam" SortExpression="IrattariTetelszam">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                            <%--
                                        <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                        </asp:ListBox>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            &nbsp;
                                            <br />
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" onmouseover="swapByName(this.id,'reszletesadatok2.png')"
                                                onmouseout="swapByName(this.id,'reszletesadatok.png')" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
