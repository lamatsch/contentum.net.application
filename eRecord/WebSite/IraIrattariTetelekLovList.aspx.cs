
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using Contentum.eAdmin.Utility;
using Contentum.eRecord.Utility;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class IraIrattariTetelekLovList : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private bool disable_refreshLovList = false;

    private const string kcs_IRATTARI_JEL = "IRATTARI_JEL";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariTetelekList");        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
                
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("IraIrattariTetelekForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("IraIrattariTetelekSearch.aspx", ""
            , 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (QueryStringVars.ErvenyessegSearchParameters.IsQSFilteredByErvenyesseg(Request.QueryString))
        {
            Ervenyesseg_SearchFormComponent1.Visible = false;

            if (!IsPostBack)
            {
                EREC_IraIrattariTetelekSearch search = (EREC_IraIrattariTetelekSearch)Search.GetSearchObject(Page, new EREC_IraIrattariTetelekSearch());
                QueryStringVars.ErvenyessegSearchParameters.SetSearchObjectFieldsFromQS(Request.QueryString, search.ErvKezd, search.ErvVege);
                Search.SetSearchObject(Page, search);            
            }
        }
        else
        {
            Ervenyesseg_SearchFormComponent1.Visible = true;
        }

        if (!IsPostBack)
        {
            FillGridViewSearchResult(false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, IraIrattariTetelekCPE,15);
             
    }


    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, IraIrattariTetelekCPE);
        FillGridViewSearchResult(false);
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_IraIrattariTetelekService service = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
        EREC_IraIrattariTetelekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_IraIrattariTetelekSearch)Search.GetSearchObject(Page, new EREC_IraIrattariTetelekSearch());

            // ha volt sz�r�s az iktat�s d�tuma szerint, akkor j�rul�kosan figyelembe vessz�k
            if (QueryStringVars.ErvenyessegSearchParameters.IsQSFilteredByErvenyesseg(Request.QueryString))
            {
                string WhereErvenyessegFilter =
                    QueryStringVars.ErvenyessegSearchParameters.GetErvenyessegFilterFromQSAsSQLCondition(Request.QueryString, search.ErvKezd.Name, search.ErvVege.Name);
                if (!String.IsNullOrEmpty(WhereErvenyessegFilter))
                {
                    if (!String.IsNullOrEmpty(search.WhereByManual))
                    {
                        search.WhereByManual = String.Format("{0} and ({1})", search.WhereByManual, WhereErvenyessegFilter);
                    }
                    else
                    {
                        search.WhereByManual = WhereErvenyessegFilter;
                    }
                }
            }

        }
        else
        {
            search = new EREC_IraIrattariTetelekSearch();

            if (!String.IsNullOrEmpty(Tetelszam_TextBoxSearch.Text))
            {
                search.IrattariTetelszam.Value = Tetelszam_TextBoxSearch.Text;
                search.IrattariTetelszam.Operator = Search.GetOperatorByLikeCharater(Tetelszam_TextBoxSearch.Text);
            }

            if (!String.IsNullOrEmpty(Nev_TextBox.Text))
            {
                search.Nev.Value = Nev_TextBox.Text;
                search.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
            }

            // ha volt sz�r�s az iktat�s d�tuma szerint, akkor j�rul�kosan figyelembe vessz�k
            if (QueryStringVars.ErvenyessegSearchParameters.IsQSFilteredByErvenyesseg(Request.QueryString))
            {
                QueryStringVars.ErvenyessegSearchParameters.SetSearchObjectFieldsFromQS(Request.QueryString, search.ErvKezd, search.ErvVege);
            }
            else
            {
                Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
                    search.ErvKezd, search.ErvVege);
            }
        }

        //LZS - BUG_12142 
        //Kiszedve ez a r�sz, �s �thelyezve az sql t�roltba, mivel az id lista m�r t�ll�pte a 4000-es karakterhosszt:
        //-----------------------------------------------------------------------------------------------------------
        // sz�r�s iktat�k�nyv szerint (ha sz�ks�ges)
        //string IrattariTetelekByIktatokonyv_Ids = GetAllIrattariTetelekByQSIktatokonyvId();
        //if (!String.IsNullOrEmpty(IrattariTetelekByIktatokonyv_Ids))
        //{
        //    search.Id.Value = IrattariTetelekByIktatokonyv_Ids;
        //    search.Id.Operator = Query.Operators.inner;
        //}

        //search.OrderBy = "Merge_IrattariTetelszam"; //"IrattariTetelszam";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        //LZS - BUG_12142 
        //Iktat�k�nyv �tad�s�val lek�rdez�s a GetAllWithExtensionWithIktatoKonyvId() webmethode-al:
        string iktatokonyvId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);
        Result result = service.GetAllWithExtensionWithIktatoKonyvId(ExecParam, search, iktatokonyvId);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        
     }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string tetelszam = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string nev = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);

                    string irattarijel = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);

                    string irattaritetelszam = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 4); ;
                    string selectedText = "";
                    string fromIratMetaDefiniciok = Request.QueryString.Get("FromIratMetaDefiniciok");
                    if (fromIratMetaDefiniciok == "1")
                    {
                        //csak a t�telsz�m
                        selectedText = irattaritetelszam;
                    }
                    else
                    {
                        //ez itt a merge_irattaritetelszam
                        selectedText = tetelszam;// +" (" + nev + ")";
                    }

                    //bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);

                    bool tryFireChangeEvent = (Page.Request.QueryString.Get(QueryStringVars.TryFireChangeEvent) == "1") ? true : false;
                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText, false, tryFireChangeEvent);
                    
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    Dictionary<string, string> returnLovValues = new Dictionary<string, string>();
                    string textBoxNevId = Request.QueryString.Get(QueryStringVars.NevTextBoxId);
                    string textBoxIrattariJelId = Request.QueryString.Get(QueryStringVars.IrattariJelTextBoxId);

                    if (textBoxNevId != null)
                    {
                        returnLovValues.Add(textBoxNevId, nev);
                    }

                    if (textBoxIrattariJelId != null)
                    {
                        string irattarijel_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(kcs_IRATTARI_JEL, irattarijel, Page);
                        returnLovValues.Add(textBoxIrattariJelId, irattarijel_nev);
                    }

                    if (returnLovValues.Count > 0)
                    {
                        JavaScripts.RegisterReturnValuesToParentWindowClientScript(Page, returnLovValues, "ReturnValuesToParentWindowTipus", false);
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    

    

    
    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }

    private string GetAllIrattariTetelekByQSIktatokonyvId()
    {
        if (!String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IktatokonyvId)))
        {
            string IrattariTetelekByIktatokonyv_Ids = (string)ViewState["IrattariTetelekByIktatokonyv_Ids"];
            if (IrattariTetelekByIktatokonyv_Ids == null)
            {
                EREC_IrattariTetel_IktatokonyvService service_irattaritetel_iktatokonyv = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
                ExecParam ExecParam_irattaritetel_iktatokonyv = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IrattariTetel_IktatokonyvSearch search_irattaritetel_iktatokonyv = new EREC_IrattariTetel_IktatokonyvSearch();

                if (QueryStringVars.ErvenyessegSearchParameters.IsQSFilteredByErvenyesseg(Request.QueryString))
                {
                    QueryStringVars.ErvenyessegSearchParameters.SetSearchObjectFieldsFromQS(Request.QueryString
                        , search_irattaritetel_iktatokonyv.ErvKezd, search_irattaritetel_iktatokonyv.ErvVege);
                    // CR 3042 Iratt�ri jel-iktat�k�nyv hozz�rendel�sn�l az �rv�nyess�gi id�b�l csak a v�ge d�tumot vizsg�ljuk
                    // nekrisz
                    search_irattaritetel_iktatokonyv.ErvKezd.Clear();
                }
                else
                {
                    #region �rv�nyess�g ellen�rz�s kikapcsol�sa
                    search_irattaritetel_iktatokonyv.ErvKezd.Clear();
                    search_irattaritetel_iktatokonyv.ErvVege.Clear();
                    #endregion �rv�nyess�g ellen�rz�s kikapcsol�sa
                }

                Result result_irattaritetel_iktatokonyv = service_irattaritetel_iktatokonyv.GetAllByIktatoKonyv(ExecParam_irattaritetel_iktatokonyv
                    , search_irattaritetel_iktatokonyv, Request.QueryString.Get(QueryStringVars.IktatokonyvId));

                if (!result_irattaritetel_iktatokonyv.IsError)
                {
                    if (result_irattaritetel_iktatokonyv.Ds.Tables[0].Rows.Count > 0)
                    {
                        string[] IrattariTetel_Ids = new string[result_irattaritetel_iktatokonyv.Ds.Tables[0].Rows.Count];
                        for (int i = 0; i < result_irattaritetel_iktatokonyv.Ds.Tables[0].Rows.Count; i++)
                        {
                            IrattariTetel_Ids[i] = result_irattaritetel_iktatokonyv.Ds.Tables[0].Rows[i]["IrattariTetel_Id"].ToString();
                        }

                        IrattariTetelekByIktatokonyv_Ids = Search.GetSqlInnerString(IrattariTetel_Ids);
                    }
                    else
                    {
                        IrattariTetelekByIktatokonyv_Ids = "";
                    }
                }

                ViewState["IrattariTetelekByIktatokonyv_Ids"] = IrattariTetelekByIktatokonyv_Ids;
            }

            return IrattariTetelekByIktatokonyv_Ids;
        }
        else
        {
            return "";
        }
    
    }
}
