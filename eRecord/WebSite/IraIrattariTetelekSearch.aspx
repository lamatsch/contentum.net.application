<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IraIrattariTetelekSearch.aspx.cs" Inherits="IraIrattariTetelekSearch" Title="Iratt�ri t�telsz�mok keres�se" %>

<%@ Register Src="eRecordComponent/AgazatiJelTextBox.ascx" TagName="AgazatiJelTextBox"
    TagPrefix="uc7" %>

<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %> 
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="width: 868px">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr runat="server" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="�gazati jel:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:AgazatiJelTextBox ID="AgazatiJelTextBox1" runat="server" SearchMode="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_AutoGeneratePartner_CheckBox" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="IrattariTetelszam_TextBox" runat="server" CssClass="mrUrlapInput" Width="287px"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;&nbsp;
                                <asp:Label ID="Label16" runat="server" Text="ITSZ megnevez�se:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="Nev_TextBox" runat="server" Width="287px"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor" id="tr_Tipus_dropdown" runat="server">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label11" runat="server" Text="Meg�rz�si m�d:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc11:KodtarakDropDownList ID="IrattariJel_KodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;<asp:Label ID="Label1" runat="server" Text="�rz�si id�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="MegorzesiIdo_TextBox" runat="server" Width="77px"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" >
                                &nbsp;<uc9:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1"
                                    runat="server" />
                                &nbsp;</td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td colspan="2">
                                &nbsp;</td>
                            <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>                      
                      </table>

                </td>
            </tr>
        </table>
</asp:Content>

