using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

// a teljes kód kidolgozandó!!!

public partial class IraIrattariTetelekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_IraIrattariTetelekSearch);
    private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.IraIrattariTetelekSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IraIrattariTetelekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_IraIrattariTetelekSearch)Search.GetSearchObject(Page, new EREC_IraIrattariTetelekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraIrattariTetelekSearch erec_IraIrattariTetelekSearch = null;
        if (searchObject != null) erec_IraIrattariTetelekSearch = (EREC_IraIrattariTetelekSearch)searchObject;

        if (erec_IraIrattariTetelekSearch != null)
        {
            AgazatiJelTextBox1.Id_HiddenField = erec_IraIrattariTetelekSearch.AgazatiJel_Id.Value;
            AgazatiJelTextBox1.SetAgazatiJelTextBoxById(SearchHeader1.ErrorPanel);

            IrattariTetelszam_TextBox.Text = erec_IraIrattariTetelekSearch.IrattariTetelszam.Value;
            Nev_TextBox.Text = erec_IraIrattariTetelekSearch.Nev.Value;
            IrattariJel_KodtarakDropDownList.FillAndSetSelectedValue(kcsKod_IRATTARI_JEL
                        , erec_IraIrattariTetelekSearch.IrattariJel.Value,true, SearchHeader1.ErrorPanel);
            MegorzesiIdo_TextBox.Text = erec_IraIrattariTetelekSearch.MegorzesiIdo.Value;

            Ervenyesseg_SearchFormComponent1.SetDefault(
                erec_IraIrattariTetelekSearch.ErvKezd, erec_IraIrattariTetelekSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IraIrattariTetelekSearch SetSearchObjectFromComponents()
    {
        EREC_IraIrattariTetelekSearch erec_IraIrattariTetelekSearch = (EREC_IraIrattariTetelekSearch)SearchHeader1.TemplateObject;
        if (erec_IraIrattariTetelekSearch == null)
        {
            erec_IraIrattariTetelekSearch = new EREC_IraIrattariTetelekSearch();
        }

        if (!String.IsNullOrEmpty(AgazatiJelTextBox1.Id_HiddenField))
        {
            erec_IraIrattariTetelekSearch.AgazatiJel_Id.Value = AgazatiJelTextBox1.Id_HiddenField;
            erec_IraIrattariTetelekSearch.AgazatiJel_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(IrattariTetelszam_TextBox.Text))
        {
            erec_IraIrattariTetelekSearch.IrattariTetelszam.Value = IrattariTetelszam_TextBox.Text;
            erec_IraIrattariTetelekSearch.IrattariTetelszam.Operator = Search.GetOperatorByLikeCharater(IrattariTetelszam_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            erec_IraIrattariTetelekSearch.Nev.Value = Nev_TextBox.Text;
            erec_IraIrattariTetelekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(IrattariJel_KodtarakDropDownList.SelectedValue))
        {
            erec_IraIrattariTetelekSearch.IrattariJel.Value = IrattariJel_KodtarakDropDownList.SelectedValue;
            erec_IraIrattariTetelekSearch.IrattariJel.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(MegorzesiIdo_TextBox.Text))
        {
            erec_IraIrattariTetelekSearch.MegorzesiIdo.Value = MegorzesiIdo_TextBox.Text;
            erec_IraIrattariTetelekSearch.MegorzesiIdo.Operator = Search.GetOperatorByLikeCharater(MegorzesiIdo_TextBox.Text);
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(
            erec_IraIrattariTetelekSearch.ErvKezd, erec_IraIrattariTetelekSearch.ErvVege);

        return erec_IraIrattariTetelekSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraIrattariTetelekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IraIrattariTetelekSearch GetDefaultSearchObject()
    {
        return new EREC_IraIrattariTetelekSearch();
    }

}
