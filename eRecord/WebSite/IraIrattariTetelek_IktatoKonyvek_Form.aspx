<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master"  AutoEventWireup="true" CodeFile="IraIrattariTetelek_IktatoKonyvek_Form.aspx.cs" Inherits="IraIrattariTetelek_IktatoKonyvek_Form" %>

<%@ Register Src="eRecordComponent/IktatokonyvTextBox.ascx" TagName="IktatokonyvTextBox"
    TagPrefix="uc8" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>



<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>

<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    
    <uc1:FormHeader ID="FormHeader1" runat="server"/>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label_Iktatokonyv" runat="server" Text="Iktat�k�nyv:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc8:IktatokonyvTextBox ID="IraIktatokonyv_IktatokonyvTextBox" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                &nbsp;<asp:Label ID="Label12" runat="server" Text="Iratt�ri t�tel:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" />
                                </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>                &nbsp;
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
</asp:Content>




