using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class IraIrattariTetelek_IktatoKonyvek_Form : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IrattariTetel_IktatoKonyv_FormHeaderTitle;
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariTetel_Iktatokonyv" + Command);
                break;
        }


        if (Command == CommandName.View)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IrattariTetel_IktatokonyvService service = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IrattariTetel_Iktatokonyv erec_IrattariTetel_Iktatokonyv = (EREC_IrattariTetel_Iktatokonyv)result.Record;
                    LoadComponentsFromBusinessObject(erec_IrattariTetel_Iktatokonyv);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.Modify)
        {
            // módosításnak itt nincs értelme
        }
        else if (Command == CommandName.New)
        {
            string irattariTetelId = Request.QueryString.Get(QueryStringVars.IrattariTetelId);
            string iktatoKonyvId = Request.QueryString.Get(QueryStringVars.IktatokonyvId);

            if (!String.IsNullOrEmpty(irattariTetelId))
            {
                IraIrattariTetelTextBox1.Id_HiddenField = irattariTetelId;
                IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);
                IraIrattariTetelTextBox1.ReadOnly = true;
            }

            if (!String.IsNullOrEmpty(iktatoKonyvId))
            {
                IraIktatokonyv_IktatokonyvTextBox.Id_HiddenField = iktatoKonyvId;
                IraIktatokonyv_IktatokonyvTextBox.SetIktatokonyvekTextBoxById(FormHeader1.ErrorPanel);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IrattariTetel_Iktatokonyv erec_IrattariTetel_Iktatokonyv)
    {
        IraIktatokonyv_IktatokonyvTextBox.Id_HiddenField = erec_IrattariTetel_Iktatokonyv.Iktatokonyv_Id;
        IraIktatokonyv_IktatokonyvTextBox.SetIktatokonyvekTextBoxById(FormHeader1.ErrorPanel);

        IraIrattariTetelTextBox1.Id_HiddenField = erec_IrattariTetel_Iktatokonyv.IrattariTetel_Id;
        IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

        FormHeader1.Record_Ver = erec_IrattariTetel_Iktatokonyv.Base.Ver;

        if (Command == CommandName.View)
        {
            IraIktatokonyv_IktatokonyvTextBox.ReadOnly = true;
            IraIrattariTetelTextBox1.ReadOnly = true;
        }
    }

    // form --> business object
    private EREC_IrattariTetel_Iktatokonyv GetBusinessObjectFromComponents()
    {

        EREC_IrattariTetel_Iktatokonyv erec_IrattariTetel_Iktatokonyv = new EREC_IrattariTetel_Iktatokonyv();
        erec_IrattariTetel_Iktatokonyv.Updated.SetValueAll(false);
        erec_IrattariTetel_Iktatokonyv.Base.Updated.SetValueAll(false);
                
        erec_IrattariTetel_Iktatokonyv.IrattariTetel_Id = IraIrattariTetelTextBox1.Id_HiddenField;
        erec_IrattariTetel_Iktatokonyv.Updated.IrattariTetel_Id = pageView.GetUpdatedByView(IraIrattariTetelTextBox1);

        erec_IrattariTetel_Iktatokonyv.Iktatokonyv_Id = IraIktatokonyv_IktatokonyvTextBox.Id_HiddenField;
        erec_IrattariTetel_Iktatokonyv.Updated.Iktatokonyv_Id = pageView.GetUpdatedByView(IraIktatokonyv_IktatokonyvTextBox);

        erec_IrattariTetel_Iktatokonyv.Base.Ver = FormHeader1.Record_Ver;
        erec_IrattariTetel_Iktatokonyv.Base.Updated.Ver = true;

        return erec_IrattariTetel_Iktatokonyv;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(IraIrattariTetelTextBox1);
            compSelector.Add_ComponentOnClick(IraIktatokonyv_IktatokonyvTextBox);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IrattariTetel_Iktatokonyv" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IrattariTetel_IktatokonyvService service = eRecordService.ServiceFactory.GetEREC_IrattariTetel_IktatokonyvService();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            EREC_IrattariTetel_Iktatokonyv erec_IrattariTetel_Iktatokonyv =
                                GetBusinessObjectFromComponents();

                            Result result = service.Insert(execParam, erec_IrattariTetel_Iktatokonyv);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                JavaScripts.RegisterCloseWindowClientScript(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }

                            break;
                        }


                }

            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }
}
