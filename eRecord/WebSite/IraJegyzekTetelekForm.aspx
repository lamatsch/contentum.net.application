<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraJegyzekTetelekForm.aspx.cs" Inherits="IraJegyzekTetelekForm" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/JegyzekTextBox.ascx" TagName="JegyzekTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBoxTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/PldIratPeldanyokTextBox.ascx" TagName="PldIratPeldanyokTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelJegyzek" runat="server" Text="Jegyz�k:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:JegyzekTextBox ID="JegyzekTextBox1" runat="server" CssClass="mrUrlapInputSzeles" />
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="trUgyirat" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyirat" runat="server" Text="�gyirat:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:UgyiratTextBox ID="UgyiratTextBox1" runat="server" CssClass="mrUrlapInput" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor" runat="server" id="trPeldany" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelP�ld�ny" runat="server" Text="Iratp�ld�ny:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc1:PldIratPeldanyokTextBox ID="PldIratPeldanyokTextBox1" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Validate="false"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegjegyzes" runat="server" Text="Megjegyz�s:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtMegjegyzes" runat="server" CssClass="mrUrlapInputSzeles" TextMode="MultiLine"
                                    Rows="3" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelSztornozasDatuma" runat="server" Text="Sztorn�z�s d�tuma:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControl_Sztornozasdatuma" ReadOnly="true" runat="server"
                                    TimeVisible="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <!-- a Note mez�t haszn�ljuk az �tvev� iktat�sz�ma t�rol�s�ra -->
                                <asp:Label ID="labelNote" runat="server" Text="�tvev� iktat�sz�ma:" />
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtNote" ReadOnly="true" runat="server" CssClass="mrUrlapInputSzeles" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
