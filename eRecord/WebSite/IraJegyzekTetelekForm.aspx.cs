using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI.WebControls;

// TELJES K�D KIDOLGOZAND�!

public partial class IraJegyzekTetelekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private void SetNewControls()
    {
    }

    private void SetViewControls()
    {
        JegyzekTextBox1.ReadOnly = true;
        UgyiratTextBox1.ReadOnly = true;
        txtMegjegyzes.ReadOnly = true;
        CalendarControl_Sztornozasdatuma.ReadOnly = true;
        txtNote.ReadOnly = true;
    }

    private void SetModifyControls()
    {
        JegyzekTextBox1.ReadOnly = true;
        UgyiratTextBox1.ReadOnly = true;
        txtMegjegyzes.ReadOnly = false;
        CalendarControl_Sztornozasdatuma.ReadOnly = true;
        txtNote.ReadOnly = false;
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekTetel" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraJegyzekTetelek erec_IraJegyzekTetelek = (EREC_IraJegyzekTetelek)result.Record;
                    LoadComponentsFromBusinessObject(erec_IraJegyzekTetelek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            this.SetNewControls();
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraJegyzekTetelekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if ((!String.IsNullOrEmpty(JegyzekTextBox1.Id_HiddenField)
                && JegyzekTextBox1.JegyzekTipus != UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
            || !String.IsNullOrEmpty(CalendarControl_Sztornozasdatuma.Text))
        {
            txtNote.ReadOnly = true;
        }

        // sztorn�zott nem m�dos�that� (sztorn�z�s: itt �rv�nytelen�t�s)
        if (!String.IsNullOrEmpty(CalendarControl_Sztornozasdatuma.Text))
        {
            SetViewControls();
            FormFooter1.SaveEnabled = false;
        }
    }

    private void LoadComponentsFromBusinessObject(EREC_IraJegyzekTetelek erec_IraJegyzekTetelek)
    {
        JegyzekTextBox1.Id_HiddenField = erec_IraJegyzekTetelek.Jegyzek_Id;
        JegyzekTextBox1.SetTextBoxById(FormHeader1.ErrorPanel);

        if (erec_IraJegyzekTetelek.Obj_type == Constants.TableNames.EREC_PldIratPeldanyok)
        {
            PldIratPeldanyokTextBox1.Id_HiddenField = erec_IraJegyzekTetelek.Obj_Id;
            PldIratPeldanyokTextBox1.SetTextBoxById(FormHeader1.ErrorPanel, null);
            trPeldany.Visible = true;
        }
        else
        {
            UgyiratTextBox1.Id_HiddenField = erec_IraJegyzekTetelek.Obj_Id;
            UgyiratTextBox1.SetTextBoxById(FormHeader1.ErrorPanel, null);
            trUgyirat.Visible = true;
        }

        txtMegjegyzes.Text = erec_IraJegyzekTetelek.Megjegyzes;

        //CalendarControl_Sztornozasdatuma.Text = rec_IraJegyzekTetelek.SztornozasDat;
        DateTime dt;
        if (DateTime.TryParse(erec_IraJegyzekTetelek.ErvVege, out dt) && DateTime.Compare(dt, DateTime.Now) < 0)
        {
            CalendarControl_Sztornozasdatuma.Text = erec_IraJegyzekTetelek.ErvVege;
        }
        else
        {
            CalendarControl_Sztornozasdatuma.Text = String.Empty;
        }

        txtNote.Text = erec_IraJegyzekTetelek.Base.Note;

        FormHeader1.Record_Ver = erec_IraJegyzekTetelek.Base.Ver;
        FormPart_CreatedModified1.SetComponentValues(erec_IraJegyzekTetelek.Base);
    }

    private EREC_IraJegyzekTetelek GetBusinessObjectFromComponents()
    {
        EREC_IraJegyzekTetelek erec_IraJegyzekTetelek = new EREC_IraJegyzekTetelek();

        erec_IraJegyzekTetelek.Updated.SetValueAll(false);
        erec_IraJegyzekTetelek.Base.Updated.SetValueAll(false);

        erec_IraJegyzekTetelek.Jegyzek_Id = JegyzekTextBox1.Id_HiddenField;
        erec_IraJegyzekTetelek.Updated.Jegyzek_Id = pageView.GetUpdatedByView(JegyzekTextBox1);

        erec_IraJegyzekTetelek.Megjegyzes = txtMegjegyzes.Text;
        erec_IraJegyzekTetelek.Updated.Megjegyzes = pageView.GetUpdatedByView(txtMegjegyzes);

        //erec_IraJegyzekTetelek.SztornozasDat = CalendarControl_Sztornozasdatuma.Text;
        //erec_IraJegyzekTetelek.Updated.SztornozasDat = pageView.GetUpdatedByView(CalendarControl_Sztornozasdatuma);

        erec_IraJegyzekTetelek.Base.Note = txtNote.Text;
        erec_IraJegyzekTetelek.Base.Updated.Note = pageView.GetUpdatedByView(txtNote);

        erec_IraJegyzekTetelek.Base.Ver = FormHeader1.Record_Ver;
        erec_IraJegyzekTetelek.Base.Updated.Ver = true;

        return erec_IraJegyzekTetelek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(JegyzekTextBox1);
            compSelector.Add_ComponentOnClick(UgyiratTextBox1);
            compSelector.Add_ComponentOnClick(txtMegjegyzes);
            compSelector.Add_ComponentOnClick(txtNote);
            compSelector.Add_ComponentOnClick(CalendarControl_Sztornozasdatuma);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                            EREC_IraJegyzekTetelek erec_IraJegyzekTetelek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_IraJegyzekTetelek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, txtNote.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
                                EREC_IraJegyzekTetelek erec_IraJegyzekTetelek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_IraJegyzekTetelek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

}
