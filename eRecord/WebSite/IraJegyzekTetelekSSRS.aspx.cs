﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class IraJegyzekTetelekSSRS : Contentum.eUtility.UI.PageBase
{
    private string JegyzekId = null;
    private string OrderBy = null;
    GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();
    GridViewColumnVisibilityState tetelekVisibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();


    protected void Page_Init(object sender, EventArgs e)
    {
        JegyzekId = Request.QueryString.Get(QueryStringVars.Id);
        OrderBy = Request.QueryString.Get(QueryStringVars.OrderBy);

        visibilityState.LoadFromCustomString((string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.IraIratokSSRSKozgyulesiEloterjesztesek]);
        tetelekVisibilityState.LoadFromCustomString((string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.IraJegyzekekSSRSTetelek]);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                #region Jegyzekek Where
                EREC_IraJegyzekekSearch search_jegyzek = new EREC_IraJegyzekekSearch();
                search_jegyzek.Id.Value = JegyzekId;
                search_jegyzek.Id.Operator = Query.Operators.equals;

                search_jegyzek.TopRow = 0;
                search_jegyzek.OrderBy = " order by EREC_IraJegyzekek.LetrehozasIdo";

                Query query_jegyzek = new Query();
                query_jegyzek.BuildFromBusinessDocument(search_jegyzek);

                string WhereJegyzek = query_jegyzek.Where;
                #endregion Jegyzekek Where

                #region Jegyzek tetelek Where
                EREC_IraJegyzekTetelekSearch search_jegyzektetelek = new EREC_IraJegyzekTetelekSearch();
                search_jegyzektetelek.Jegyzek_Id.Value = JegyzekId;
                search_jegyzektetelek.Jegyzek_Id.Operator = Query.Operators.equals;

                search_jegyzektetelek.TopRow = 0;
                search_jegyzektetelek.OrderBy = String.IsNullOrEmpty(OrderBy) ? " order by IrattariTetelszam, Note " : String.Format(" order by {0}", OrderBy);

                Query query_jegyzektetelek = new Query();
                query_jegyzektetelek.BuildFromBusinessDocument(search_jegyzektetelek);

                string WhereJegyzekTetelek = query_jegyzektetelek.Where;
                #endregion Jegyzek tetelek Where

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "WhereJegyzek":
                            ReportParameters[i].Values.Add(WhereJegyzek);
                            break;
                        case "OrderByJegyzek":
                            ReportParameters[i].Values.Add(search_jegyzek.OrderBy);
                            break;
                        case "TopRowJegyzek":
                            ReportParameters[i].Values.Add(Convert.ToString(search_jegyzek.TopRow));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloId(Page));
                            break;
                        case "Id":
                            ReportParameters[i].Values.Add(JegyzekId);
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloSzerverzetId(Page));
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(null);
                            break;
                        case "WhereJegyzekTetelek":
                            ReportParameters[i].Values.Add(WhereJegyzekTetelek);
                            break;
                        case "OrderByJegyzekTetelek":
                            ReportParameters[i].Values.Add(search_jegyzektetelek.OrderBy);
                            break;
                        case "TopRowJegyzekTetelek":
                            ReportParameters[i].Values.Add(Convert.ToString(search_jegyzektetelek.TopRow));
                            break;
                        case "TipusVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Tipus));
                            break;
                        case "JegyzekVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Nev));
                            break;
                        case "KezeloVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Csoport_Id_Felelos_Nev));
                            break;
                        case "VegrehajtoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.FelhasznaloCsoport_Id_Vegrehajto_Nev));
                            break;
                        case "AtvevoVisibility"://[Partner_Id_LeveltariAtvevo_Nev]
                            ReportParameters[i].Values.Add(Boolean.TrueString);
                            break;
                        case "LetrehozasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.LetrehozasIdo));
                            break;
                        case "LezarasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.LezarasDatuma));
                            break;
                        case "MegsemmisitesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.MegsemmisitesDatuma));
                            break;
                        case "StornoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.SztornirozasDat));
                            break;
                        case "ZarolasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Zarolas));
                            break;
                        case "AtvevoIktatoszamVisibility":
                            ReportParameters[i].Values.Add(tetelekVisibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekTetelekColumnName.Note));
                            break;
                        case "AtadasDatumaVisibility":
                            ReportParameters[i].Values.Add(tetelekVisibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekTetelekColumnName.AtadasDatuma));
                            break;

                    }
                }
            }
        }
        return ReportParameters;
    }
}
