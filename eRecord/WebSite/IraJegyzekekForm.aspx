<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IraJegyzekekForm.aspx.cs" Inherits="IraJegyzekekForm" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc23" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBoxTextBox" TagPrefix="uc6" %>
<%@ Register Src="~/eRecordComponent/FunkcioGombsor.ascx" TagPrefix="uc1" TagName="FunkcioGombsor" %>
<%@ Register Src="~/Component/TUKFelhasznaloMultiSelect.ascx" TagName="TUKFelhasznaloMultiSelect" TagPrefix="uc" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label16" runat="server" Text="Jegyz�k megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc6:RequiredTextBoxTextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput">
                                </uc6:RequiredTextBoxTextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label11" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:DropDownList ID="TipusDropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Kezel�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:CsoportTextBox ID="CsoportTextBoxFelelos" ReadOnly="true" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelKezeloSzervezete" runat="server" Text="<%$Forditas:labelKezeloSzervezete|Kezel� szervezete:%>"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:CsoportTextBox ID="KezeloSzervezete" ReadOnly="true" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trLezaras" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Lez�r�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControlLezaras" runat="server" ReadOnly="true" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trVegrehajto" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="Selejtez�/Lev�lt�rnak �tad�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc7:CsoportTextBox ID="CsoportTextBoxVegrehajto" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trAtvevo" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAtvevoEllenor" runat="server" Text="�tvev�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textAtvevoEllenor" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr id="trBizottsagiTagok" runat="server" visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelBizottsagiTagok" runat="server" Text="Bizotts�gi tagok:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc:TUKFelhasznaloMultiSelect ID="BizottsagiTagok" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trMegsemmesites" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="Selejtez�s/ Lev�lt�rba ad�s id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControlMegsemmisites" runat="server" Validate="false" TimeVisible="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trSztornozas" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Sztorn�z�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc5:CalendarControl ID="CalendarControlStorno" runat="server" ReadOnly="true" Validate="false" />
                            </td>
                        </tr>

                    </table>
                </eUI:eFormPanel>
                <%--      <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" OnLoad="FormPart_CreatedModified1_Load" /> --%>
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
            <td valign="top" style="width: 0px">
                <div id="divFunkcioGombsor" runat="server" visible="false" style="margin-top: 8px;"></div>
                <uc1:FunkcioGombsor ID="FunkcioGombsor" runat="server" Visible="true" />
            </td>
        </tr>
    </table>

</asp:Content>

