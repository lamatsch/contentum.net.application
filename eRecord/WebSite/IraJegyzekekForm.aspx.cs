using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class IraJegyzekekForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    private String _ParentForm = "";
    public String ParentForm
    {
        get { return _ParentForm; }
        set
        {
            _ParentForm = value;
            FunkcioGombsor.ParentForm = value;
        }
    }

    protected bool IsBizottsagiTagokVisible { get; set; }

    private void SetNewControls()
    {
        CsoportTextBoxFelelos.Id_HiddenField = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(FelhasznaloProfil.FelhasznaloId(Page));
        CsoportTextBoxFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        trVegrehajto.Visible = false;
        trAtvevo.Visible = false;
        trLezaras.Visible = false;
        trMegsemmesites.Visible = false;
        trSztornozas.Visible = false;
        KezeloSzervezete.Id_HiddenField = FelhasznaloProfil.FelhasznaloSzerverzetId(Page);
        KezeloSzervezete.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        #region BUG_4303
        //LZS
        FunkcioGombsor.Visible = false;
        #endregion
    }

    private void SetViewControls()
    {
        Nev_TextBox.ReadOnly = true;
        TipusDropDownList.Enabled = false;
        CsoportTextBoxFelelos.ReadOnly = true;
        CsoportTextBoxVegrehajto.ReadOnly = true;
        textAtvevoEllenor.ReadOnly = true;
        CalendarControlMegsemmisites.ReadOnly = true;
        KezeloSzervezete.ReadOnly = true;
        BizottsagiTagok.ReadOnly = true;
    }

    private void SetModifyControls()
    {
        TipusDropDownList.Enabled = false;
        CsoportTextBoxFelelos.ReadOnly = true;
        KezeloSzervezete.ReadOnly = true;
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Jegyzek" + Command);
                break;
        }

        if (!IsPostBack)
        {
            UI.JegyzekTipus.FillDropDownList(TipusDropDownList);
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraJegyzekek EREC_IraJegyzekek = (EREC_IraJegyzekek)result.Record;
                    LoadComponentsFromBusinessObject(EREC_IraJegyzekek);

                    #region BLG_2951
                    //LZS
                    FunkcioGombsor.HideButtons();
                    FunkcioGombsor.XMLExportVisible = true;
                    FunkcioGombsor.XMLExportEnabled = true;
                    ParentForm = this.Form.ID;
                    FunkcioGombsor.ParentForm = ParentForm;
                    FunkcioGombsor.XMLExportOnClientClick = "javascript:window.open('XMLExportFormTabPrintForm.aspx?" + QueryStringVars.Jegyzek_Id + "=" + id + "&tipus=Jegyzek')";
                    #endregion
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else if (Command == CommandName.New)
        {
            this.SetNewControls();
        }

        if (Command == CommandName.Modify)
        {
            this.SetModifyControls();
        }

        if (Command == CommandName.View)
        {
            this.SetViewControls();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = Resources.Form.IraJegyzekekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (IsBizottsagiTagokVisible)
        {
            trBizottsagiTagok.Visible = true;
            trAtvevo.Visible = false;
        }
    }

    private void LoadComponentsFromBusinessObject(EREC_IraJegyzekek erec_IraJegyzekek)
    {
        Nev_TextBox.Text = erec_IraJegyzekek.Nev;

        TipusDropDownList.SelectedValue = erec_IraJegyzekek.Tipus;

        CsoportTextBoxFelelos.Id_HiddenField = erec_IraJegyzekek.Csoport_Id_felelos;
        CsoportTextBoxFelelos.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        CalendarControlLezaras.Text = erec_IraJegyzekek.LezarasDatuma;

        CsoportTextBoxVegrehajto.Id_HiddenField = erec_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj;
        CsoportTextBoxVegrehajto.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        //PartnerTextBoxAtvevo.Id_HiddenField = erec_IraJegyzekek.Partner_Id_LeveltariAtvevo;
        //PartnerTextBoxAtvevo.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        textAtvevoEllenor.Text = erec_IraJegyzekek.Base.Note;
        BizottsagiTagok.SetSelectedText(erec_IraJegyzekek.Base.Note);
        if (erec_IraJegyzekek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value)
        {
            labelAtvevoEllenor.Text = "Ellen�r";
            IsBizottsagiTagokVisible = Rendszerparameterek.IsTUK(Page);
        }

        CalendarControlMegsemmisites.Text = erec_IraJegyzekek.VegrehajtasDatuma;

        CalendarControlStorno.Text = erec_IraJegyzekek.SztornirozasDat;

        KezeloSzervezete.Id_HiddenField = erec_IraJegyzekek.Csoport_Id_FelelosSzervezet;
        KezeloSzervezete.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

        FormHeader1.Record_Ver = erec_IraJegyzekek.Base.Ver;
    }

    private EREC_IraJegyzekek GetBusinessObjectFromComponents()
    {
        EREC_IraJegyzekek erec_IraJegyzekek = new EREC_IraJegyzekek();

        erec_IraJegyzekek.Updated.SetValueAll(false);
        erec_IraJegyzekek.Base.Updated.SetValueAll(false);

        erec_IraJegyzekek.Nev = Nev_TextBox.Text;
        erec_IraJegyzekek.Updated.Nev = pageView.GetUpdatedByView(Nev_TextBox);

        erec_IraJegyzekek.Tipus = TipusDropDownList.SelectedValue;
        erec_IraJegyzekek.Updated.Tipus = pageView.GetUpdatedByView(TipusDropDownList);

        erec_IraJegyzekek.Csoport_Id_felelos = CsoportTextBoxFelelos.Id_HiddenField;
        erec_IraJegyzekek.Updated.Csoport_Id_felelos = pageView.GetUpdatedByView(CsoportTextBoxFelelos);

        erec_IraJegyzekek.LezarasDatuma = CalendarControlLezaras.Text;
        erec_IraJegyzekek.Updated.LezarasDatuma = pageView.GetUpdatedByView(CalendarControlLezaras);

        erec_IraJegyzekek.FelhasznaloCsoport_Id_Vegrehaj = CsoportTextBoxVegrehajto.Id_HiddenField;
        erec_IraJegyzekek.Updated.FelhasznaloCsoport_Id_Vegrehaj = pageView.GetUpdatedByView(CsoportTextBoxVegrehajto);

        //erec_IraJegyzekek.Partner_Id_LeveltariAtvevo = PartnerTextBoxAtvevo.Id_HiddenField;
        //erec_IraJegyzekek.Updated.Partner_Id_LeveltariAtvevo = pageView.GetUpdatedByView(PartnerTextBoxAtvevo);
        erec_IraJegyzekek.Base.Note = GetAtvevo();
        erec_IraJegyzekek.Base.Updated.Note = pageView.GetUpdatedByView(textAtvevoEllenor);

        erec_IraJegyzekek.VegrehajtasDatuma = CalendarControlMegsemmisites.Text;
        erec_IraJegyzekek.Updated.VegrehajtasDatuma = pageView.GetUpdatedByView(CalendarControlMegsemmisites);

        erec_IraJegyzekek.SztornirozasDat = CalendarControlStorno.Text;
        erec_IraJegyzekek.Updated.SztornirozasDat = pageView.GetUpdatedByView(CalendarControlStorno);

        erec_IraJegyzekek.Csoport_Id_FelelosSzervezet = KezeloSzervezete.Id_HiddenField;
        erec_IraJegyzekek.Updated.Csoport_Id_FelelosSzervezet = pageView.GetUpdatedByView(KezeloSzervezete);

        erec_IraJegyzekek.Base.Ver = FormHeader1.Record_Ver;
        erec_IraJegyzekek.Base.Updated.Ver = true;

        return erec_IraJegyzekek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev_TextBox);
            compSelector.Add_ComponentOnClick(TipusDropDownList);
            compSelector.Add_ComponentOnClick(CsoportTextBoxFelelos);
            compSelector.Add_ComponentOnClick(CalendarControlLezaras);
            compSelector.Add_ComponentOnClick(CsoportTextBoxVegrehajto);
            compSelector.Add_ComponentOnClick(textAtvevoEllenor);
            compSelector.Add_ComponentOnClick(CalendarControlMegsemmisites);
            compSelector.Add_ComponentOnClick(CalendarControlStorno);
            compSelector.Add_ComponentOnClick(KezeloSzervezete);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "Jegyzek" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                            EREC_IraJegyzekek erec_IraJegyzekek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_IraJegyzekek);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy m�sik formr�l h�vt�k meg, adatok visszak�ld�se:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                                EREC_IraJegyzekek erec_IraJegyzekek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_IraJegyzekek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    string GetAtvevo()
    {
        if (!IsBizottsagiTagokVisible)
            return textAtvevoEllenor.Text;
        else
            return BizottsagiTagok.GetSelectedText();
    }

}
