﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IraJegyzekekList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();
    private bool IsSztornoEnabled = false;
    private bool isTuk = false;
    string Mode;

    bool IsMegsemmisitesiNaplo
    {
        get
        {
            return Mode == "MegsemmisitesiNaplo";
        }
    }

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        IraJegyzekTetelekSubListHeader.RowCount_Changed += new EventHandler(IraJegyzekTetelekSubListHeader_RowCount_Changed);

        IraJegyzekTetelekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IraJegyzekTetelekSubListHeader_ErvenyessegFilter_Changed);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsMegsemmisitesiNaplo)
        {
            ListHeader1.HeaderLabel = Resources.List.MegsemmisitesiNaploListHeaderTitle;
        }
        else
        {
            ListHeader1.HeaderLabel = Resources.List.IraJegyzekekListHeaderTitle;
            //BLG 2954
            //SetGridViewColumnVisiblityBySortExpression(IraJegyzekekGridView, "MegsemmisitesDatuma", true);
        }

        isTuk = Rendszerparameterek.IsTUK(Page);

        if (!isTuk)
        {
            UI.SetGridViewColumnVisiblity(IraJegyzekekGridView, "Azonosito", false);
        }

        ListHeader1.SearchObjectType = typeof(EREC_IraJegyzekekSearch);

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = false;
        ListHeader1.UnlockVisible = false;
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.AtadasVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraJegyzekekGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraJegyzekekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraJegyzekekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraJegyzekekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IraJegyzekekListajaPrintForm.aspx");

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraJegyzekekGridView.ClientID);
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, "", true);

        ListHeader1.AtadasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                 + IraJegyzekTetelekGridView.ClientID + "','check'); "
                 + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

        
        if (IsMegsemmisitesiNaplo)
        {
            ListHeader1.JegyzekMegsemmisitesVisible = true;
            ListHeader1.NewVisible = false;
        }
        else
        {
            //BLG 2954 nem TÜK környezetben kell a megsemmisítés
            ListHeader1.JegyzekMegsemmisitesVisible = !isTuk;
            ListHeader1.JegyzekZarolasVisible = true;
            ListHeader1.JegyzekVegrehajtasVisible = true;
            ListHeader1.SztornoVisible = true;
        }

        ListHeader1.JegyzekZarolasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.JegyzekVegrehajtasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.JegyzekVegrehajtasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.MegsJegyzNyomtatasOnClientClick =
        ListHeader1.JegyzekMegsemmisitesOnClientClick =
            JavaScripts.SetOnClientClickNoSelectedRow();

        //selectedRecordId kezelése
        ListHeader1.AttachedGridView = IraJegyzekekGridView;

        IraJegyzekTetelekSubListHeader.NewVisible = false;
        IraJegyzekTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IraJegyzekTetelekSubListHeader.ModifyVisible = true;
        IraJegyzekTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        IraJegyzekTetelekSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraJegyzekTetelekGridView.ClientID);
        IraJegyzekTetelekSubListHeader.ButtonsClick += new CommandEventHandler(IraJegyzekTetelekSubListHeader_ButtonsClick);

        #region BLG2102 - Megsemmisítési jegyzék nyomtatása
        ListHeader1.MegsJegyzNyomtatasEnabled = isTuk;
        ListHeader1.MegsJegyzNyomtatasVisible = isTuk;
        if (isTuk)
        {
            ListHeader1.MegsJegyzNyomtatasOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + IraJegyzekekGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }
        #endregion
        //selectedRecordId kezelése
        IraJegyzekTetelekSubListHeader.AttachedGridView = IraJegyzekTetelekGridView;
        
        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraJegyzekekSearch());

        if (isTuk)
        {
            BoundField bf = UI.GetGridViewColumn(IraJegyzekekGridView, "Note");

            if (bf != null)
            {
                bf.HeaderText = "Átvevő/Bizottsági tagok";
            }
        }


        if (!IsPostBack) IraJegyzekekGridViewBind();

        //scroll állapotának mentése
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.ViewHistory);
        //bernat.laszlo added : Excel Export (Grid)
        ListHeader1.ExportVisible = true;
        ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
        ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.ExcelExport);
        //bernat.laszlo eddig

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Jegyzek" + CommandName.Lock);

        ListHeader1.JegyzekZarolasEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekZarolas");
        ListHeader1.JegyzekVegrehajtasEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekVegrehajtas");

        ListHeader1.JegyzekMegsemmisitesEnabled = isTuk && FunctionRights.GetFunkcioJog(Page, "JegyzekMegsemmisites");

        IraJegyzekTetelekPanel.Visible = FunctionRights.GetFunkcioJog(Page, "JegyzekTetelekList");

        ListHeader1.AtadasEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetelAtadas");

        // a Modify jogot ellenőrizzük a New-ra, mivel itt az új kapcsolatnál az IktatoKonyv.DefaultIrattariTetelszam 
        // módosítása történik
        IraJegyzekTetelekSubListHeader.NewEnabled = false;
        IraJegyzekTetelekSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + CommandName.View);
        IraJegyzekTetelekSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + CommandName.Modify); ;
        IraJegyzekTetelekSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "JegyzekTetel" + CommandName.Invalidate);

        //if (IsPostBack)
        //{
        String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraJegyzekekGridView);

        RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        if (String.IsNullOrEmpty(MasterListSelectedRowId))
        {
            ActiveTabClear();
        }
        //ActiveTabRefreshOnClientClicks();
        ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        //}

        if (String.IsNullOrEmpty(MasterListSelectedRowId))
        {                         
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraJegyzekekSSRS] = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IraJegyzekekGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.IraJegyzekekColumnNames().GetType()).ToCustomString();

            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraJegyzekekSSRS.aspx')";
        }
        else
        {
            #region BLG 2954 nem TÜK környezetben csak Végrehajtott állapotú jegyzéken lehessen bejegyezni a megsemmisítés időpontját 
            if (!isTuk && FunctionRights.GetFunkcioJog(Page, "JegyzekMegsemmisites"))
            {
                try
                {
                    EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();

                    ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    ExecParam.Record_Id = MasterListSelectedRowId;
                    Result result = service.Get(ExecParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_IraJegyzekek EREC_IraJegyzekek = (EREC_IraJegyzekek)result.Record;
                        if (EREC_IraJegyzekek != null && !string.IsNullOrEmpty(EREC_IraJegyzekek.Allapot))
                        {
                            ListHeader1.JegyzekMegsemmisitesEnabled = EREC_IraJegyzekek.Allapot == KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("IraJegyzekekList.Page_PreRender error:" + ex.Message);
                }
            }
            #endregion
        }
    }

    #endregion


    #region Master List

    protected void IraJegyzekekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraJegyzekekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraJegyzekekGridView", ViewState);

        IraJegyzekekGridViewBind(sortExpression, sortDirection);
    }

    protected void IraJegyzekekGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraJegyzekekSearch search = (EREC_IraJegyzekekSearch)Search.GetSearchObject(Page, Search.GetDefaultSearchObject_EREC_IraJegyzekekSearch(Page));
        search.OrderBy = Search.GetOrderBy("IraJegyzekekGridView", ViewState, SortExpression, SortDirection);

        if (IsMegsemmisitesiNaplo)
        {
            search.VegrehajtasDatuma.Operator = Query.Operators.notnull;
        }
        else
        {
            search.VegrehajtasDatuma.Operator = "";
        }

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(IraJegyzekekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void IraJegyzekekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.JegyzekTipus.SetGridViewRow(e, "labelJegyzekTipus");
        UI.SetGridViewDateTime(e, "labelLetrehozasDate");
        UI.SetGridViewDateTime(e, "labelLezarasDate");
        UI.SetGridViewDateTime(e, "labelVegrehajtasDate");
        UI.SetGridViewDateTime(e, "labelSztornoDate");
        UI.SetGridViewDateTime(e, "labelMegsemmisitesDatuma");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void IraJegyzekekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraJegyzekekGridView.PageIndex;

        IraJegyzekekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IraJegyzekekGridView.PageCount;

        if (prev_PageIndex != IraJegyzekekGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, IraJegyzekekCPE);
            IraJegyzekekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IraJegyzekekCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IraJegyzekekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(IraJegyzekekGridView);
    }

    private void GridView_RowDataBound_DisableSzereltCheckbox(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string allapot = String.Empty;

            if (drw["Allapot"] != null)
            {
                allapot = drw["Allapot"].ToString();
            }

            if (!String.IsNullOrEmpty(allapot))
            {
                CheckBox check = (CheckBox)e.Row.FindControl("check");

                if (check != null)
                {
                    if (allapot == KodTarak.UGYIRAT_ALLAPOT.Szerelt)
                    {
                        check.Enabled = false;
                    }
                }

            }
        }
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraJegyzekekGridViewBind();
    }

    protected void IraJegyzekekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraJegyzekekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
            
            ActiveTabRefresh(TabContainer1, id);
            //IraJegyzekekGridView_SelectRowCommand(id);
        }
    }

    //private void IraJegyzekekGridView_SelectRowCommand(string felhasznaloId)
    //{
    //    //ActiveTabRefresh(TabContainer1, felhasznaloId);
    //}

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekekForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraJegyzekek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraJegyzekekUpdatePanel.ClientID);

            IraJegyzekTetelekSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekek_IktatoKonyvek_Form.aspx"
                , "Command=" + CommandName.New + "&" + QueryStringVars.IrattariTetelId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

            ListHeader1.SztornoOnClientClick = "if(!confirm('Biztosan sztornózni kívánja a jegyzéket?')) return false;";
            IsSztornoEnabled = true;

            SetMegSemmisitesButtonCommand(id);

            GridViewRow row = IraJegyzekekGridView.SelectedRow;
            if (row != null)
            {
                bool isTetelInvalidable = true;

                Label labelSztornoDate = (Label)row.FindControl("labelSztornoDate");
                if (labelSztornoDate != null)
                {
                    if (!String.IsNullOrEmpty(labelSztornoDate.Text))
                    {
                        isTetelInvalidable = false;
                        ListHeader1.JegyzekZarolasOnClientClick = "alert('A jegyzék nem zárható le, mert sztornózott!'); return false;";
                        ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék nem hajtható végre, mert sztornózott!'); return false;";
                        ListHeader1.SztornoOnClientClick = "alert('A jegyzék már sztornózva van.');return false;";
                        ListHeader1.ModifyOnClientClick = "alert('A jegyzék nem módosítható, mert sztornózott!'); return false;";
                        IsSztornoEnabled = false;
                        return;
                    }
                }
                bool isZarolva = false;
                Label labelLezarasDate = (Label)row.FindControl("labelLezarasDate");
                if (labelLezarasDate != null)
                {
                    string date = Server.HtmlDecode(labelLezarasDate.Text).Trim();
                    if (String.IsNullOrEmpty(date))
                    {
                        ListHeader1.JegyzekZarolasOnClientClick = "if(confirm('Biztosan zárolja a jegyzéket?') == true) return true; else return false;";
                        ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék nem hajtható végre, mert nincs zárolva!'); return false;";
                        ListHeader1.AtadasOnClientClick= "alert('A jegyzék nem adható át, mert nincs zárolva!'); return false;";

                        //LZS - BUG_6373
                        ListHeader1.AtadasEnabled = false;
                    }
                    else
                    {
                        ListHeader1.AtadasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                 + IraJegyzekTetelekGridView.ClientID + "','check'); "
                 + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
                        ListHeader1.JegyzekZarolasOnClientClick = "alert('A jegyzék már zárolva van.'); return false;";
                        isZarolva = true;
                        
                        //LZS - BUG_6373
                        ListHeader1.AtadasEnabled = true;
                        
                    }
                }

                if (isZarolva)
                {
                    isTetelInvalidable = false;
                    Label labelAllapot = row.FindControl("labelAllapot") as Label;

                    if (labelAllapot != null
                        && labelAllapot.Text == KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban)
                    {
                        ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék végrehajtása folyamatban van.'); return false;";
                        ListHeader1.SztornoOnClientClick = "alert('A jegyzék nem sztornózható, mert a végrehajtása folyamatban van!');return false;";
                        IsSztornoEnabled = false;
                    }
                    else
                    {


                        Label labelVegrehajtasDate = (Label)row.FindControl("labelVegrehajtasDate");

                        if (labelVegrehajtasDate != null)
                        {
                            string date = Server.HtmlDecode(labelVegrehajtasDate.Text).Trim();
                            if (String.IsNullOrEmpty(date))
                            {
                                ListHeader1.JegyzekVegrehajtasOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekekVegrehajtasForm.aspx", QueryStringVars.Id + "=" + id
                               , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                            else
                            {
                                ListHeader1.JegyzekVegrehajtasOnClientClick = "alert('A jegyzék már végre van hajtva.'); return false;";
                                ListHeader1.SztornoOnClientClick = "alert('A jegyzék nem sztornózható, mert már végre van hajtva!');return false;";
                                IsSztornoEnabled = false;
                            }
                        }
                    }
                }

                IraJegyzekTetelekSubListHeader.InvalidateEnabled = isTetelInvalidable;

                Label labelMegsemmisitesDatuma = (Label)row.FindControl("labelMegsemmisitesDatuma");

                if (!String.IsNullOrEmpty(labelMegsemmisitesDatuma.Text))
                {
                    //ListHeader1.MegsJegyzNyomtatasOnClientClick = 
                    ListHeader1.JegyzekMegsemmisitesOnClientClick =
                    "alert('A jegyzék már megsemmisítésre került!'); return false;";
                }
            }

            var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IraJegyzekekGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.IraJegyzekekColumnNames().GetType()).ToCustomString();
            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraJegyzekTetelekSSRS] = visibilityState;
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraJegyzekTetelekSSRS.aspx?" + QueryStringVars.Id + "=" + id +
                "&" + QueryStringVars.OrderBy + "=" + "IrattariTetelszam,Note"               
                + "')";

        }
    }

    // jegyzék tételek rendezési sorrendjének visszaolvasása a viewstate-ből (Search.GetOrderBy "visszaforgatása")
    private string GetOrderBy(String GridViewName)
    {
        string _ret = "";
        if (!String.IsNullOrEmpty(GridViewName))
        {
            String SortExpression = ViewState[GridViewName + "SortExpression"] as String;
            SortDirection SortDirection = (ViewState[GridViewName + "SortDirection"] != null) ? (SortDirection)ViewState[GridViewName + "SortDirection"] : SortDirection.Ascending;

            if (SortExpression == null) return _ret;

            if (SortExpression != "")
            {
                _ret = SortExpression;

                // ha [FullSortExpression] stringgel kezdődik a kifejezés, akkor ez már tartalmazza a rendezési irányokat is,
                // nem kell azt hozzácsapni (mivel lehet hogy több oszlopot is megadtunk, mindegyikhez külön rendezési iránnyal)
                if (_ret.StartsWith(Search.FullSortExpression))
                {
                    // [FullSortExpression] string levágása:
                    _ret = _ret.Remove(0, Search.FullSortExpression.Length);
                }
                else
                {
                    switch (SortDirection)
                    {
                        case SortDirection.Ascending:
                            _ret = String.Format(_ret, "ASC");  // ha több mezőből áll a rendezés, megadható {0} is az egyes mezőkhöz
                            _ret += " ASC";
                            break;
                        case SortDirection.Descending:
                            _ret = String.Format(_ret, "DESC");  // ha több mezőből áll a rendezés, megadható {0} is az egyes mezőkhöz
                            _ret += " DESC";
                            break;
                    }
                }
            }
        }
        return _ret;
    }

    protected void IraJegyzekekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraJegyzekekGridViewBind();
                    ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraJegyzekekGridView));
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIraJegyzekek();
            IraJegyzekekGridViewBind();
        }
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, IraJegyzekekGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
        //bernat.laszlo eddig
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraJegyzekekRecords();
                IraJegyzekekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraJegyzekekRecords();
                IraJegyzekekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIraJegyzekek();
                break;
            case CommandName.JegyzekZarolas:
                ZarolSelectedJegyzek();
                IraJegyzekekGridViewBind();
                break;
            case CommandName.JegyzekVegrehajtas:
                VegrehajtSelectedJegyzek();
                IraJegyzekekGridViewBind();
                break;
            case CommandName.Sztorno:
                SztornozSelectedJegyzek();
                IraJegyzekekGridViewBind();
                break;
            case CommandName.MegsJegyzNyomtatas:
                #region BLG2102 Megsemmisítési jegyzék nyomtatása
                String id = UI.GetGridViewSelectedRecordId(IraJegyzekekGridView);
                             
                string js = "javascript:window.open('MegsemmisitesiJegyzekPrintFormSSRS.aspx?" + QueryStringVars.Id + "=" + id + "')";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "megsJegyzekNyomtatas", js, true);

                #endregion
                break;
            case CommandName.Atadas:
                {
                    List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IraJegyzekTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (lstGridViewSelectedRows.Count > 0)
                    {
                        Session[Constants.SelectedJegyzekTetelIds] = string.Join(",", lstGridViewSelectedRows.ToArray());

                        js = JavaScripts.SetOnClientClickWithTimeout("TomegesAtadas.aspx", String.Empty, Defaults.PopupWidth_Max, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TomegesAtadasScript", js, true);
                    }
                }
                break;
        }
    }


    private void ZarolSelectedJegyzek()
    {
        string id = UI.GetGridViewSelectedRecordId(IraJegyzekekGridView);

        if (!String.IsNullOrEmpty(id))
        {
            EREC_IraJegyzekekService svc = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = id;

            Result res = svc.JegyzekLezarasa(xpm);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel.Update();
                EErrorPanel1.Visible = true;
            }
        }

    }

    private void VegrehajtSelectedJegyzek()
    {
        string id = UI.GetGridViewSelectedRecordId(IraJegyzekekGridView);
        string leveltariAtvevo = String.Empty;

        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            leveltariAtvevo = Request.Params["__EVENTARGUMENT"].ToString();
        }

        if (!String.IsNullOrEmpty(id))
        {
            EREC_IraJegyzekekService svc = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = id;

            Result res = svc.JegyzekVegrehajtasa(xpm, leveltariAtvevo, String.Empty, false);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel.Update();
                EErrorPanel1.Visible = true;
            }
        }
    }

    private void SztornozSelectedJegyzek()
    {
        string id = UI.GetGridViewSelectedRecordId(IraJegyzekekGridView);

        if (!String.IsNullOrEmpty(id))
        {
            EREC_IraJegyzekekService svc = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());
            xpm.Record_Id = id;

            Result res = svc.JegyzekSztornozas(xpm);

            if (!String.IsNullOrEmpty(res.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, res);
                ErrorUpdatePanel.Update();
                EErrorPanel1.Visible = true;
            }
        }

    }

    private void LockSelectedIraJegyzekekRecords()
    {
        LockManager.LockSelectedGridViewRecords(IraJegyzekekGridView, "EREC_IraJegyzekek"
            , "IrattariTetelLock", "IrattariTetelForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraJegyzekekRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IraJegyzekekGridView, "EREC_IraJegyzekek"
            , "IrattariTetelLock", "IrattariTetelForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// Törli a IraJegyzekekGridView -ban kijelölt elemeket
    /// </summary>
    private void deleteSelectedIraJegyzekek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "IrattariTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraJegyzekekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijelölt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraJegyzekek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IraJegyzekekGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraJegyzekek");
        }
    }

    protected void IraJegyzekekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraJegyzekekGridViewBind(e.SortExpression, UI.GetSortToGridView("IraJegyzekekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    #endregion


    #region Detail Tab

    // az átadott tabpanel tartalmát frissíti, ha az az aktív tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraJegyzekekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (IraJegyzekekGridView.SelectedIndex == -1)
        {
            return;
        }
        string irattariTetelId = UI.GetGridViewSelectedRecordId(IraJegyzekekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, irattariTetelId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string irattariTetelId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                IraJegyzekTetelekGridViewBind(irattariTetelId);
                IraJegyzekTetelekPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(IraJegyzekTetelekGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        IraJegyzekTetelekSubListHeader.RowCount = RowCount;
    }

    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(IraJegyzekTetelekTabPanel))
        {
            IraJegyzekTetelekGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(IraJegyzekTetelekGridView));
        }
    }

    #endregion


    #region IraJegyzekTetelek Detail

    private void IraJegyzekTetelekSubListHeader_ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedJegyzekTetelek(); ;
            IraJegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraJegyzekekGridView));
        }
    }

    protected void IraJegyzekTetelekGridViewBind(string irattariTetelId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraJegyzekTetelekGridView", ViewState, "Azonosito");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraJegyzekTetelekGridView", ViewState);

        IraJegyzekTetelekGridViewBind(irattariTetelId, sortExpression, sortDirection);
    }

    protected void IraJegyzekTetelekGridViewBind(string irattariTetelId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(irattariTetelId))
        {
            EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraJegyzekTetelekSearch search = new EREC_IraJegyzekTetelekSearch();

            search.Jegyzek_Id.Value = irattariTetelId;
            search.Jegyzek_Id.Operator = Query.Operators.equals;

            search.OrderBy = Search.GetOrderBy("IraJegyzekTetelekGridView", ViewState, SortExpression, SortDirection);

            IraJegyzekTetelekSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllWithExtension(ExecParam, search);
            UI.GridViewFill(IraJegyzekTetelekGridView, res, IraJegyzekTetelekSubListHeader, EErrorPanel1, ErrorUpdatePanel);

            // Típustól függőn elrejtünk oszlopokat
            EREC_IraJegyzekekService iraJegyzekekService = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam iraJegyzekExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            iraJegyzekExecParam.Record_Id = irattariTetelId;
            Result iraJegyzekekResult = iraJegyzekekService.Get(iraJegyzekExecParam);
            if (iraJegyzekekResult.Record != null && (iraJegyzekekResult.Record as EREC_IraJegyzekek).Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value) // "S"
            {
                UI.SetGridViewColumnVisiblity(IraJegyzekTetelekGridView, "Note", false);
                UI.SetGridViewColumnVisiblity(IraJegyzekTetelekGridView, "AtadasDatuma", false);
            }
            else
            {
                UI.SetGridViewColumnVisiblity(IraJegyzekTetelekGridView, "Note", true);
                UI.SetGridViewColumnVisiblity(IraJegyzekTetelekGridView, "AtadasDatuma", true);
            }

            Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IraJegyzekekSSRSTetelek] = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IraJegyzekTetelekGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.IraJegyzekTetelekColumnName().GetType()).ToCustomString();

            UI.SetTabHeaderRowCountText(res, Header1);
        }
        else
        {
            ui.GridViewClear(IraJegyzekTetelekGridView);
        }
    }


    void IraJegyzekTetelekSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        IraJegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraJegyzekekGridView));
    }

    protected void IraJegyzekTetelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(IraJegyzekTetelekTabPanel))
                    //{
                    //    IraJegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraJegyzekTetelekGridView));
                    //}
                    ActiveTabRefreshDetailList(IraJegyzekTetelekTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// Törli a kijelölt hozzárendeléseket, vagyis az EREC_IraJegyzekTetelek.DefaultIrattariTetelszam mezőt (update művelet valójában)
    /// </summary>
    private void deleteSelectedJegyzekTetelek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "JegyzekTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraJegyzekTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraJegyzekTetelekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];

            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }

            Result result = service.MultiInvalidate(execParams);

            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void IraJegyzekTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraJegyzekTetelekGridView, selectedRowNumber, "check");
        }
    }

    private void IraJegyzekTetelekGridView_RefreshOnClientClicks(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            // ha nem ellenőrizzük, a SelectedRow értéke kívül eshet a tartományon (exception!)
            if (IraJegyzekTetelekGridView.Rows.Count > 0 && IraJegyzekTetelekGridView.SelectedIndex < IraJegyzekTetelekGridView.Rows.Count)
            {
                if (IraJegyzekTetelekGridView.SelectedRow != null)
                {
                    Label labelObjId = (Label)IraJegyzekTetelekGridView.SelectedRow.FindControl("labelObjId");
                    Label labelObjType = (Label)IraJegyzekTetelekGridView.SelectedRow.FindControl("labelObjType");
                    if (labelObjId != null && labelObjType != null)
                    {
                        string formName = "UgyUgyiratokForm.aspx";
                        if (labelObjType.Text == Constants.TableNames.EREC_PldIratPeldanyok)
                        {
                            formName = "PldIratPeldanyokForm.aspx";
                        }
                        IraJegyzekTetelekSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick(formName
                            , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + labelObjId.Text + "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromJegyzekTetel
                            , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IraJegyzekTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

                    }
                }
            }
            // módosítás: csak a megjegyzés és esetleg az átvevő iktatószáma (Note mező)
            IraJegyzekTetelekSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraJegyzekTetelekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekTetelekUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }

        int jegyzekTetelekCount = IraJegyzekTetelekSubListHeader.RecordNumber;
        if (jegyzekTetelekCount > 0 && IsSztornoEnabled)
        {
            ListHeader1.SztornoOnClientClick = "alert('A jegyzék nem sztornózható, mert a jegyzékhez tartoznak tételek!');return false;";
        }
    }

    protected void IraJegyzekTetelekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraJegyzekTetelekGridView.PageIndex;

        IraJegyzekTetelekGridView.PageIndex = IraJegyzekTetelekSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != IraJegyzekTetelekGridView.PageIndex)
        {
            //scroll állapotának törlése
            JavaScripts.ResetScroll(Page, IraJegyzekTetelekCPE);
            IraJegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraJegyzekekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(IraJegyzekTetelekSubListHeader.Scrollable, IraJegyzekTetelekCPE);
        }
        IraJegyzekTetelekSubListHeader.PageCount = IraJegyzekTetelekGridView.PageCount;
        IraJegyzekTetelekSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(IraJegyzekTetelekGridView);

        ui.SetClientScriptToGridViewSelectDeSelectButton(IraJegyzekTetelekGridView);
    }


    void IraJegyzekTetelekSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(IraJegyzekTetelekSubListHeader.RowCount);
        IraJegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraJegyzekekGridView));
    }

    protected void IraJegyzekTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraJegyzekTetelekGridViewBind(UI.GetGridViewSelectedRecordId(IraJegyzekekGridView)
            , e.SortExpression, UI.GetSortToGridView("IraJegyzekTetelekGridView", ViewState, e.SortExpression));
    }

    #endregion

    public static void SetGridViewColumnVisiblityBySortExpression(GridView gridView, string sortExpression, bool visible)
    {
        for (int i = 0; i < gridView.Columns.Count; i++)
        {
            DataControlField field = gridView.Columns[i];

            if (field.SortExpression == sortExpression)
            {
                field.Visible = visible;
                break;
            }
        }
    }

    private void SetMegSemmisitesButtonCommand(string id)
    {
        //ListHeader1.MegsJegyzNyomtatasOnClientClick = 
        ListHeader1.JegyzekMegsemmisitesOnClientClick =
        JavaScripts.SetOnClientClick("IraJegyzekekMegsemmisites.aspx"
            , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            , Defaults.PopupWidth, Defaults.PopupHeight, IraJegyzekekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
    }
}
