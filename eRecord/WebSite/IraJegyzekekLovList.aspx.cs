using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using System.Collections.Generic;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;

public partial class IraJegyzekekLovList : System.Web.UI.Page
{
    private bool disable_refreshLovList = false;
    private string startup;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "JegyzekekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        startup = Request.QueryString.Get(Constants.Startup.StartupName);

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("IraJegyzekekSearch.aspx", "", Defaults.PopupWidth, Defaults.PopupHeight, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickLovListSelectItem(ListBoxSearchResult.ClientID)
        + JavaScripts.SetOnClientClick("IraJegyzekekForm.aspx", QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "='+ document.forms[0]." + ListBoxSearchResult.ClientID + ".value +'", Defaults.PopupWidth, Defaults.PopupHeight, null);

        if (!IsPostBack)
        {
            FillListBoxSearchResult(TextBoxSearch.Text, false);
        }
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        FillListBoxSearchResult(TextBoxSearch.Text, false);
    }

    protected void FillListBoxSearchResult(string SearchKey, bool searchObjectFromSession)
    {
        ListBoxSearchResult.Items.Clear();

        EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
        EREC_IraJegyzekekSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_IraJegyzekekSearch)Search.GetSearchObject(Page, Search.GetDefaultSearchObject_EREC_IraJegyzekekSearch(Page));
            TextBoxSearch.Text = "";
        }
        else
        {
            search = Search.GetDefaultSearchObject_EREC_IraJegyzekekSearch(Page);
            search.Nev.Value = SearchKey;
            search.Nev.Operator = Search.GetOperatorByLikeCharater(SearchKey);
        }
        search.OrderBy = "Nev";

        if (!String.IsNullOrEmpty(startup))
        {
            if (startup == Constants.Startup.FromSelejtezes)
            {
                search.Tipus.Value = UI.JegyzekTipus.SelejtezesiJegyzek.Value;
                search.Tipus.Operator = Query.Operators.equals;
            }
            else if (startup == Constants.Startup.FromLeveltarbaAdas)
            {
                search.Tipus.Value = UI.JegyzekTipus.LeveltariAtadasJegyzek.Value;
                search.Tipus.Operator = Query.Operators.equals;
            }
            else if (startup == Constants.Startup.FromEgyebSzervezetnekAtadas)
            {
                search.Tipus.Value = UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value;
                search.Tipus.Operator = Query.Operators.equals;
            }
        }

        //lez�rt �s v�grehajtott jegyz�keket nem hozza
        search.LezarasDatuma.Operator = Query.Operators.isnull;
        search.VegrehajtasDatuma.Operator = Query.Operators.isnull;

        //sztorn�zott jegyz�keket nem hozza
        search.SztornirozasDat.Operator = Query.Operators.isnull;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(execParam, search);

        UI.ListBoxFill(ListBoxSearchResult, result, "Nev", LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (ListBoxSearchResult.SelectedItem != null)
            {
                string selectedId = ListBoxSearchResult.SelectedItem.Value;
                string selectedText = ListBoxSearchResult.SelectedItem.Text;

                JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                disable_refreshLovList = true;
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillListBoxSearchResult("", true);
                    }
                    break;
            }
        }

        //c�m �ll�t�s
        if (!String.IsNullOrEmpty(startup))
        {
            if (startup == Constants.Startup.FromSelejtezes)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.SelejtezesiJegyzekekLovListHeaderTitle;
            }
            else if (startup == Constants.Startup.FromLeveltarbaAdas)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.LeveltariJegyzekekLovListHeaderTitle;
            }
            else if (startup == Constants.Startup.FromEgyebSzervezetnekAtadas)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.EgyebSzervezetnekAtadasJegyzekekLovListHeaderTitle;
            }
        }
    }
}
