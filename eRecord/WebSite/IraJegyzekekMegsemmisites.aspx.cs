using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

// TELJES K�D KIDOLGOZAND�!

public partial class IraJegyzekekMegsemmisites : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;


    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Jegyzek" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraJegyzekek EREC_IraJegyzekek = (EREC_IraJegyzekek)result.Record;
                    LoadComponentsFromBusinessObject(EREC_IraJegyzekek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.FullManualHeaderTitle = "Jegyz�k t�telek megsemmis�t�se";

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void LoadComponentsFromBusinessObject(EREC_IraJegyzekek erec_IraJegyzekek)
    {
        Nev_TextBox.Text = erec_IraJegyzekek.Nev;
        MegsemmisitesDatuma.Text = DateTime.Now.ToString();
        hfVegrehajtasDatuma.Value = erec_IraJegyzekek.VegrehajtasDatuma;

        FormHeader1.Record_Ver = erec_IraJegyzekek.Base.Ver;
    }

    private EREC_IraJegyzekek GetBusinessObjectFromComponents()
    {
        EREC_IraJegyzekek erec_IraJegyzekek = new EREC_IraJegyzekek();

        erec_IraJegyzekek.Updated.SetValueAll(false);
        erec_IraJegyzekek.Base.Updated.SetValueAll(false);

        erec_IraJegyzekek.MegsemmisitesDatuma = MegsemmisitesDatuma.Text;
        erec_IraJegyzekek.Updated.MegsemmisitesDatuma = pageView.GetUpdatedByView(MegsemmisitesDatuma);

        erec_IraJegyzekek.Base.Ver = FormHeader1.Record_Ver;
        erec_IraJegyzekek.Base.Updated.Ver = true;

        return erec_IraJegyzekek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Nev_TextBox);

            FormFooter1.SaveEnabled = false;
        }
    }

    #region BLG 2954 seg�df�ggv�nyek

    Result MegsemmisitIratPeldanyok(EREC_PldIratPeldanyok iratPeldany, String MegsemmisitveId, int currentVersion)
    {
        EREC_PldIratPeldanyokService iratPeldanyokServiceUpdate = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
        iratPeldany.Updated.SetValueAll(false);
        iratPeldany.Base.Updated.SetValueAll(false);

        iratPeldany.Csoport_Id_Felelos = MegsemmisitveId;
        iratPeldany.Updated.Csoport_Id_Felelos = true;

        iratPeldany.FelhasznaloCsoport_Id_Orzo = MegsemmisitveId;
        iratPeldany.Updated.FelhasznaloCsoport_Id_Orzo = true;

        ExecParam epUpdate = UI.SetExecParamDefault(Page, new ExecParam());
        epUpdate.Record_Id = iratPeldany.Id;

        iratPeldany.Base.Ver = currentVersion.ToString();
        iratPeldany.Base.Updated.Ver = true;

        return iratPeldanyokServiceUpdate.Update(epUpdate, iratPeldany);
    }

    Result MegsemmisitUgyiratok(EREC_UgyUgyiratok ugyirat, String MegsemmisitveId, int currentVersion)
    {
        EREC_UgyUgyiratokService ugyiratokServiceUpdate = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ugyirat.Updated.SetValueAll(false);
        ugyirat.Base.Updated.SetValueAll(false);

        ugyirat.Csoport_Id_Felelos = MegsemmisitveId;
        ugyirat.Updated.Csoport_Id_Felelos = true;

        ugyirat.FelhasznaloCsoport_Id_Orzo = MegsemmisitveId;
        ugyirat.Updated.FelhasznaloCsoport_Id_Orzo = true;

        ExecParam epUpdate = UI.SetExecParamDefault(Page, new ExecParam());
        epUpdate.Record_Id = ugyirat.Id;

        ugyirat.Base.Ver = currentVersion.ToString();
        ugyirat.Base.Updated.Ver = true;

        return ugyiratokServiceUpdate.Update(epUpdate, ugyirat);
    }

    Result MegsemmisitIratok(EREC_IraIratok irat, String MegsemmisitveId, int currentVersion)
    {
        EREC_IraIratokService iratokServiceUpdate = eRecordService.ServiceFactory.GetEREC_IraIratokService();

        irat.Updated.SetValueAll(false);
        irat.Base.Updated.SetValueAll(false);

        irat.Csoport_Id_Felelos = MegsemmisitveId;
        irat.Updated.Csoport_Id_Felelos = true;

        irat.FelhasznaloCsoport_Id_Orzo = MegsemmisitveId;
        irat.Updated.FelhasznaloCsoport_Id_Orzo = true;

        ExecParam epUpdate = UI.SetExecParamDefault(Page, new ExecParam());
        epUpdate.Record_Id = irat.Id;

        irat.Base.Ver = currentVersion.ToString();
        irat.Base.Updated.Ver = true;

        return iratokServiceUpdate.Update(epUpdate, irat);
    }

    private bool MegsemmisitesBejegyzese(String recordId)
    {
        try
        {
            String MegsemmisitveId = GetMegsemmisitveId();
            if (String.IsNullOrEmpty(MegsemmisitveId))
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "'Megsemmis�tve' Csoport nem tal�lhat�");
                return false;
            }
            EREC_IraJegyzekTetelekService jegyzekTetelekService = eRecordService.ServiceFactory.GetEREC_IraJegyzekTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraJegyzekTetelekSearch jegyzekTetelekSearch = new EREC_IraJegyzekTetelekSearch();

            jegyzekTetelekSearch.Jegyzek_Id.Value = recordId;
            jegyzekTetelekSearch.Jegyzek_Id.Operator = Query.Operators.equals;

            Result jegyzekTetelekRes = jegyzekTetelekService.GetAllWithExtension(ExecParam, jegyzekTetelekSearch);

            if (String.IsNullOrEmpty(jegyzekTetelekRes.ErrorCode))
            {
                if (jegyzekTetelekRes.Ds != null && jegyzekTetelekRes.Ds.Tables.Count > 0)
                {
                    Dictionary<String, Boolean> IratIdsDictionary = new Dictionary<String, Boolean>();
                    foreach (System.Data.DataRow row in jegyzekTetelekRes.Ds.Tables[0].Rows)
                    {
                        String ObjType = row["Obj_type"].ToString();
                        String ObjId = row["Obj_Id"].ToString();
                        if (!String.IsNullOrEmpty(ObjType) && !String.IsNullOrEmpty(ObjId))
                        {
                            ExecParam execParamSelect = UI.SetExecParamDefault(Page, new ExecParam());
                            execParamSelect.Record_Id = ObjId;

                            if (ObjType.Equals("EREC_PldIratPeldanyok", StringComparison.InvariantCultureIgnoreCase))
                            {
                                EREC_PldIratPeldanyokService iratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();

                                Result resIRatPld = iratPeldanyokService.Get(execParamSelect);

                                if (String.IsNullOrEmpty(resIRatPld.ErrorCode))
                                {
                                    EREC_PldIratPeldanyok iratPeldany = (EREC_PldIratPeldanyok)resIRatPld.Record;
                                    String iratId = iratPeldany.IraIrat_Id;
                                    if (!IratIdsDictionary.ContainsKey(iratId))
                                        IratIdsDictionary.Add(iratId, false);
                                    int currentVersion = Int32.Parse(iratPeldany.Base.Ver);
                                    Result updateRes = MegsemmisitIratPeldanyok(iratPeldany, MegsemmisitveId, currentVersion);
                                    if (!String.IsNullOrEmpty(updateRes.ErrorCode))
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, updateRes);
                                        return false;
                                    }
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resIRatPld);
                                    return false;
                                }

                            }
                            else if (ObjType.Equals("EREC_UgyUgyiratok", StringComparison.InvariantCultureIgnoreCase))
                            {
                                EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                                Result resUgyirat = ugyiratokService.Get(execParamSelect);

                                if (String.IsNullOrEmpty(resUgyirat.ErrorCode))
                                {
                                    EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)resUgyirat.Record;
                                    int currentVersion = Int32.Parse(ugyirat.Base.Ver);
                                    Result updateRes = MegsemmisitUgyiratok(ugyirat, MegsemmisitveId, currentVersion);
                                    if (!String.IsNullOrEmpty(updateRes.ErrorCode))
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, updateRes);
                                        return false;
                                    }
                                    else
                                    {
                                        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                                        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();

                                        iratokSearch.Ugyirat_Id.Value = ObjId;
                                        iratokSearch.Ugyirat_Id.Operator = Query.Operators.equals;

                                        Result resIratokGetAll = iratokService.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), iratokSearch);
                                        if (!String.IsNullOrEmpty(resIratokGetAll.ErrorCode))
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resIratokGetAll);
                                            return false;
                                        }
                                        else
                                        {
                                            if (resIratokGetAll.Ds != null && resIratokGetAll.Ds.Tables.Count > 0)
                                            {
                                                foreach(System.Data.DataRow rowIrat in resIratokGetAll.Ds.Tables[0].Rows)
                                                {
                                                    String iratId = rowIrat["Id"].ToString();
                                                    if (!IratIdsDictionary.ContainsKey(iratId))
                                                        IratIdsDictionary.Add(iratId, true);
                                                }
                                            }
                                        }

                                    }
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resUgyirat);
                                    return false;
                                }
                            }
                        }
                    }

                    foreach(KeyValuePair<String, Boolean> pair in IratIdsDictionary)
                    {
                        if(!updateIrat(pair.Key, pair.Value, MegsemmisitveId))
                        {
                            return false;
                        }
                    }
                }

            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, jegyzekTetelekRes);
                return false;
            }
        }
        catch (Exception ex)
        {
            Logger.Error("IraJegyzekekMegsemmisites.FormFooter1ButtonsClick error:" + ex.Message);
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorLabel, "Hiba a megsemmis�t�s bejegyz�se sor�n:" + ex.Message);
            return false;
        }

        return true;
    }

    private Boolean updateIrat(String iratId, Boolean needUpdateIratPeldany, String MegsemmisitveId)
    {
        EREC_IraIratokService iratokService = eRecordService.ServiceFactory.GetEREC_IraIratokService();

        ExecParam execParamSelect = UI.SetExecParamDefault(Page, new ExecParam());
        execParamSelect.Record_Id = iratId;

        Result resIrat = iratokService.Get(execParamSelect);

        if (!String.IsNullOrEmpty(resIrat.ErrorCode))
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resIrat);
            return false;
        }
        else
        {
            EREC_IraIratok irat = (EREC_IraIratok)resIrat.Record;
            int currentVersion = Int32.Parse(irat.Base.Ver);
            Result updateRes = MegsemmisitIratok(irat, MegsemmisitveId, currentVersion);
            if (!String.IsNullOrEmpty(updateRes.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, updateRes);
                return false;
            }
            else
            {
                if (needUpdateIratPeldany)
                {
                    EREC_PldIratPeldanyokService iratPeldanyokService = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    EREC_PldIratPeldanyokSearch iratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();

                    iratPeldanyokSearch.IraIrat_Id.Value = iratId;
                    iratPeldanyokSearch.IraIrat_Id.Operator = Query.Operators.equals;

                    Result iratPeldanyokRes = iratPeldanyokService.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), iratPeldanyokSearch);
                    if (!String.IsNullOrEmpty(iratPeldanyokRes.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, iratPeldanyokRes);
                        return false;
                    }
                    else
                    {
                        if (iratPeldanyokRes.Ds != null && iratPeldanyokRes.Ds.Tables.Count > 0)
                        {
                            foreach(System.Data.DataRow row in iratPeldanyokRes.Ds.Tables[0].Rows)
                            {
                                String iratPldId = row["Id"].ToString();
                                if (!String.IsNullOrEmpty(iratPldId))
                                {
                                    EREC_PldIratPeldanyokService iratPeldanyokServiceGet = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                                    ExecParam execParamSelectPld = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParamSelectPld.Record_Id = iratPldId;

                                    Result getIratPld = iratPeldanyokServiceGet.Get(execParamSelectPld);
                                    if (!String.IsNullOrEmpty(getIratPld.ErrorCode))
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, getIratPld);
                                        return false;
                                    }
                                    else
                                    {
                                        EREC_PldIratPeldanyok iratPld = (EREC_PldIratPeldanyok)getIratPld.Record;
                                        try
                                        {
                                            int currentVersionPld = Int32.Parse(iratPld.Base.Ver);
                                            Result iratPldMegsRes = MegsemmisitIratPeldanyok(iratPld, MegsemmisitveId, currentVersionPld);
                                            if (!String.IsNullOrEmpty(iratPldMegsRes.ErrorCode))
                                            {
                                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, iratPldMegsRes);
                                                return false;
                                            }
                                        }catch(Exception ex)
                                        {
                                            Logger.Error("updateIrat", ex);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return true;
    }
    private String GetMegsemmisitveId()
    {
        try
        {
            KRT_CsoportokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            KRT_CsoportokSearch search_csoportok = new KRT_CsoportokSearch();

            search_csoportok.Nev.Value = "Megsemmis�tve";
            search_csoportok.Nev.Operator = Query.Operators.equals;

            Result res = service.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), search_csoportok);
            if (String.IsNullOrEmpty(res.ErrorCode))
            {
                if (res.Ds != null && res.Ds.Tables.Count > 0)
                {
                    System.Data.DataRow row = res.Ds.Tables[0].Rows[0];
                    return row["Id"].ToString();
                }
            }

        }
        catch(Exception ex)
        {
            Logger.Error("IraJegyzekekMegsemmisites.GetMegsemmisitveId error:" + ex.Message);
        }
        return String.Empty;
    }
    #endregion


    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "JegyzekMegsemmisites"))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                if (MegsemmisitesBejegyzese(recordId))
                                {
                                    EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
                                    EREC_IraJegyzekek erec_IraJegyzekek = GetBusinessObjectFromComponents();

                                    if (!CheckDatum(erec_IraJegyzekek))
                                        return;

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.JegyzekMegsemmisitese(execParam, erec_IraJegyzekek);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    bool CheckDatum(EREC_IraJegyzekek erec_IraJegyzekek)
    {
        string vegrehajtasDatuma = hfVegrehajtasDatuma.Value;

        if (!String.IsNullOrEmpty(vegrehajtasDatuma))
        {
            DateTime dtVegrehajtasDatuma;

            if (DateTime.TryParse(vegrehajtasDatuma, out dtVegrehajtasDatuma))
            {
                if (!erec_IraJegyzekek.Typed.MegsemmisitesDatuma.IsNull)
                {
                    if (erec_IraJegyzekek.Typed.MegsemmisitesDatuma.Value.Date < dtVegrehajtasDatuma.Date)
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, "Hiba", "A megsemmis�t�s id�pontja nem lehet kor�bbi, mint a selejtez�s id�pontja!");
                        return false;
                    }
                }
            }
        }

        return true;
    }

}
