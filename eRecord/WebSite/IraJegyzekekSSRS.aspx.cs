﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using GridViewColumns = Contentum.eRecord.BaseUtility.GridViewColumns;
using GridViewColumnVisibilityState = Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState;

public partial class IraJegyzekekSSRS : Contentum.eUtility.UI.PageBase
{
    GridViewColumnVisibilityState visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState();

    protected void Page_Init(object sender, EventArgs e)
    {
        string visibilityFilter = (string)Page.Session[GridViewColumnVisibilityState.Constants.SessionVariables.IraJegyzekekSSRS];
        visibilityState.LoadFromCustomString(visibilityFilter);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraJegyzekekSearch search = null;

                search = (EREC_IraJegyzekekSearch)Search.GetSearchObject(Page, new EREC_IraJegyzekekSearch());

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.GetAllWithExtension(ExecParam, search);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_IraJegyzekek.LetrehozasIdo DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Id":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@Id")))
                            {
                                ReportParameters[i].Values.Add(null);
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Id"));
                            }
                            break;
                        case "FelhasznaloSzervezet_Id":
                            //ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id")))
                            {
                                ReportParameters[i].Values.Add(ExecParam.FelhasznaloSzervezet_Id);
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "TipusVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Tipus));
                            break;
                        case "JegyzekVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Nev));
                            break;
                        case "KezeloVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Csoport_Id_Felelos_Nev));
                            break;
                        case "VegrehajtoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.FelhasznaloCsoport_Id_Vegrehajto_Nev));
                            break;
                        case "AtvevoVisibility"://[Partner_Id_LeveltariAtvevo_Nev]
                            ReportParameters[i].Values.Add(Boolean.TrueString);
                            break;
                        case "LetrehozasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.LetrehozasIdo));
                            break;
                        case "LezarasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.LezarasDatuma));
                            break;
                        case "MegsemmisitesVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.MegsemmisitesDatuma));
                            break;
                        case "StornoVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.SztornirozasDat));
                            break;
                        case "ZarolasVisibility":
                            ReportParameters[i].Values.Add(visibilityState.GetIsVisibleValue(GridViewColumns.IraJegyzekekColumnNames.Zarolas));
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
