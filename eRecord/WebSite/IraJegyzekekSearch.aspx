<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IraJegyzekekSearch.aspx.cs" Inherits="IraJegyzekekSearch" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc5" %>

<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl" TagPrefix="uc17" %>
<%@ Register Src="eRecordComponent/AlszamIntervallum_SearchFormControl.ascx" TagName="AlszamIntervallum_SearchFormControl" TagPrefix="uc15" %>
<%@ Register Src="eRecordComponent/FoszamIntervallum_SearchFormControl.ascx" TagName="FoszamIntervallum_SearchFormControl" TagPrefix="uc16" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc13" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc14" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc10" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %> 
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="width: 1027px">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="99%">
                        <tr class="urlapSor" id="tr_AutoGeneratePartner_CheckBox" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:DropDownList ID="TipusDropDownList" CssClass="mrUrlapInputComboBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="Jegyz�k megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:TextBox ID="JegyzekMegnevezes_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="�llapot:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:DropDownList ID="rbListAllapot" RepeatDirection="Horizontal" runat="server" CssClass="mrUrlapInputComboBox">
                                    <asp:ListItem Text="�sszes" Value="Osszes"></asp:ListItem>
                                    <asp:ListItem Text="Nyitott" Value="Nyitott"></asp:ListItem>
                                    <asp:ListItem Text="Lez�rt" Value="Lezart"></asp:ListItem>
                                    <asp:ListItem Text="V�grehajt�s folyamatban" Value="VegrehajtasFolyamatban"></asp:ListItem>
                                    <asp:ListItem Text="V�grehajtott" Value="Vegrehajtott"></asp:ListItem>
                                    <asp:ListItem Text="Sztorn�zott" Value="Sztornozott"></asp:ListItem>
                                </asp:DropDownList>
                                </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="Kezel�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <uc3:CsoportTextBox ID="Felelos_CsoportTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trVegrehajto" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Selejtez�/Lev�lt�rnak �tad�:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:CsoportTextBox ID="Vegrehajto_CsoportTextBox" runat="server" Validate="false" />
                            </td>                            
                        </tr>
                        <tr class="urlapSor" id="trAtvevo" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAtvevoEllenor" runat="server" Text="Lev�lt�ri �tvev�/Ellen�r:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="textAtvevoellenor" runat="server" CssClass="mrUrlapInput" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="Lez�r�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc14:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl2" runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label9" runat="server" Text="Selejtez�s / Lev�lt�rba ad�s id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc14:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl1" runat="server" Validate="false" ValidateDateFormat="true" />
                             </td>   
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="Sztorn�z�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc14:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControlSztornozas" runat="server" Validate="false" ValidateDateFormat="true" />
                            </td>    
                        </tr>

                        <asp:PlaceHolder ID="phMegsemmisites" runat="server">
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="MegsemmisitesiNaploLabel" runat="server" Text="Megsemmis�t�si napl�:"></asp:Label>
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:DropDownList ID="MegsemmisitesiNaplo" runat="server" CssClass="mrUrlapInputComboBox"></asp:DropDownList>
                                </td>
                            </tr>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;</td>
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="CheckBox_MegsemmisitesreVar" runat="server" Text="Megsemmis�t�sre v�r"></asp:CheckBox>
                                </td>
                            </tr>

                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">&nbsp;<asp:Label ID="Label6" runat="server" Text="Megsemmis�t�s d�tuma:"></asp:Label></td>
                                <td class="mrUrlapMezoVAlignTop">
                                    <uc14:DatumIntervallum_SearchCalendarControl ID="DatumIntervallum_SearchCalendarControl_Megsemmisites"
                                        runat="server" Validate="false" ValidateDateFormat="true"></uc14:DatumIntervallum_SearchCalendarControl>
                                </td>
                            </tr>
                        </asp:PlaceHolder>

                        <tr class="urlapSor">
                            <td colspan="4">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td colspan="2">
                                &nbsp;</td>
                            <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>                      
                      </table>

                </td>
            </tr>
        </table>
</asp:Content>

