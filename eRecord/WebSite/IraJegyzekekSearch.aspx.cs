using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using System.Data;
using Contentum.eRecord.Utility;
using Contentum.eRecord.Service;
//using Contentum.eRecord.Utility;

// a teljes kód kidolgozandó!!!

public partial class IraJegyzekekSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_IraJegyzekekSearch);
    //private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";

    bool IsMegsemmisitesiNaplo
    {
        get
        {
            return Rendszerparameterek.IsTUK(Page);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
        registerJavascripts();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.HeaderTitle = Resources.Search.IraJegyzekekSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IraJegyzekekSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_IraJegyzekekSearch)Search.GetSearchObject(Page, new EREC_IraJegyzekekSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

        labelAtvevoEllenor.Text = "Átvevő/Bizottsági tagok:";

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraJegyzekekSearch erec_IraJegyzekekSearch = null;
        if (searchObject != null) erec_IraJegyzekekSearch = (EREC_IraJegyzekekSearch)searchObject;

        if (erec_IraJegyzekekSearch != null)
        {

            Contentum.eRecord.Utility.UI.JegyzekTipus.FillDropDownListWithEmptyValue(TipusDropDownList);
            //TipusDropDownList.Items.Add(new ListItem("[Nincs kiválasztva]", ""));
            TipusDropDownList.SelectedValue = erec_IraJegyzekekSearch.Tipus.Value;

            JegyzekMegnevezes_TextBox.Text = erec_IraJegyzekekSearch.Nev.Value;

            Felelos_CsoportTextBox.Id_HiddenField = erec_IraJegyzekekSearch.Csoport_Id_felelos.Value;
            Felelos_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            Vegrehajto_CsoportTextBox.Id_HiddenField = erec_IraJegyzekekSearch.FelhasznaloCsoport_Id_Vegrehaj.Value;
            Vegrehajto_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            //Atvevo_PartnerTextBox.Id_HiddenField = erec_IraJegyzekekSearch.Partner_Id_LeveltariAtvevo.Value;
            //Atvevo_PartnerTextBox.SetPartnerTextBoxById(SearchHeader1.ErrorPanel);
            textAtvevoellenor.Text = erec_IraJegyzekekSearch.Note.Value;

            rbListAllapot.SelectedValue = "Osszes";
            if (erec_IraJegyzekekSearch.LezarasDatuma.Operator == Query.Operators.notnull)
            {
                rbListAllapot.SelectedValue = "Lezart";
            }

            if (erec_IraJegyzekekSearch.VegrehajtasDatuma.Operator == Query.Operators.notnull)
            {
                rbListAllapot.SelectedValue = "Vegrehajtott";
            }

            if (erec_IraJegyzekekSearch.SztornirozasDat.Operator == Query.Operators.notnull)
            {
                rbListAllapot.SelectedValue = "Sztornozott";
            }

            if (erec_IraJegyzekekSearch.LezarasDatuma.Operator == Query.Operators.isnull &&
               erec_IraJegyzekekSearch.VegrehajtasDatuma.Operator == Query.Operators.isnull &&
               erec_IraJegyzekekSearch.SztornirozasDat.Operator == Query.Operators.isnull)
            {
                rbListAllapot.SelectedValue = "Nyitott";
            }

            if (erec_IraJegyzekekSearch.Allapot.Value == KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban)
            {
                rbListAllapot.SelectedValue = "VegrehajtasFolyamatban";
            }
            DatumIntervallum_SearchCalendarControl2.SetComponentFromSearchObjectFields(erec_IraJegyzekekSearch.LezarasDatuma);

            DatumIntervallum_SearchCalendarControl1.SetComponentFromSearchObjectFields(erec_IraJegyzekekSearch.VegrehajtasDatuma);

            DatumIntervallum_SearchCalendarControlSztornozas.SetComponentFromSearchObjectFields(erec_IraJegyzekekSearch.SztornirozasDat);

            phMegsemmisites.Visible = IsMegsemmisitesiNaplo;
            if (IsMegsemmisitesiNaplo)
            {
                FillMegsemmisitesiNaplo();
                MegsemmisitesiNaplo.SelectedValue = erec_IraJegyzekekSearch.IraIktatokonyv_Id.Value;

                CheckBox_MegsemmisitesreVar.Checked = erec_IraJegyzekekSearch.MegsemmisitesDatuma != null &&
                    erec_IraJegyzekekSearch.MegsemmisitesDatuma.Operator == Query.Operators.isnull;
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "initEnableOrDisable_CheckBox_MegsemmisitesreVar", CheckBox_MegsemmisitesreVar.Attributes["onclick"], true);
            }

            DatumIntervallum_SearchCalendarControl_Megsemmisites.SetComponentFromSearchObjectFields(erec_IraJegyzekekSearch.MegsemmisitesDatuma);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IraJegyzekekSearch SetSearchObjectFromComponents()
    {
        EREC_IraJegyzekekSearch erec_IraJegyzekekSearch = (EREC_IraJegyzekekSearch)SearchHeader1.TemplateObject;
        if (erec_IraJegyzekekSearch == null)
        {
            erec_IraJegyzekekSearch = GetDefaultSearchObject();
        }

        if (!String.IsNullOrEmpty(TipusDropDownList.SelectedValue))
        {
            erec_IraJegyzekekSearch.Tipus.Value = TipusDropDownList.SelectedValue;
            erec_IraJegyzekekSearch.Tipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(JegyzekMegnevezes_TextBox.Text))
        {
            erec_IraJegyzekekSearch.Nev.Value = JegyzekMegnevezes_TextBox.Text;
            erec_IraJegyzekekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(JegyzekMegnevezes_TextBox.Text);
        }

        if (!String.IsNullOrEmpty(Felelos_CsoportTextBox.Id_HiddenField))
        {
            erec_IraJegyzekekSearch.Csoport_Id_felelos.Value = Felelos_CsoportTextBox.Id_HiddenField;
            erec_IraJegyzekekSearch.Csoport_Id_felelos.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(Vegrehajto_CsoportTextBox.Id_HiddenField))
        {
            erec_IraJegyzekekSearch.FelhasznaloCsoport_Id_Vegrehaj.Value = Vegrehajto_CsoportTextBox.Id_HiddenField;
            erec_IraJegyzekekSearch.FelhasznaloCsoport_Id_Vegrehaj.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(textAtvevoellenor.Text))
        {
            erec_IraJegyzekekSearch.Note.Value = textAtvevoellenor.Text;
            erec_IraJegyzekekSearch.Note.Operator = Search.GetOperatorByLikeCharater(textAtvevoellenor.Text);
        }


        switch (rbListAllapot.SelectedValue)
        {
            case "Osszes":
                DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_IraJegyzekekSearch.VegrehajtasDatuma);
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(erec_IraJegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.SetSearchObjectFields(erec_IraJegyzekekSearch.SztornirozasDat);
                break;
            case "Nyitott":
                erec_IraJegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Nyitott;
                erec_IraJegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                erec_IraJegyzekekSearch.LezarasDatuma.Operator = Query.Operators.isnull;
                erec_IraJegyzekekSearch.VegrehajtasDatuma.Operator = Query.Operators.isnull;
                erec_IraJegyzekekSearch.SztornirozasDat.Operator = Query.Operators.isnull;
                DatumIntervallum_SearchCalendarControl1.Clear();
                DatumIntervallum_SearchCalendarControl2.Clear();
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                break;
            case "Lezart":
                erec_IraJegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Lezart;
                erec_IraJegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                erec_IraJegyzekekSearch.LezarasDatuma.Operator = Query.Operators.notnull;
                DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_IraJegyzekekSearch.VegrehajtasDatuma);
                DatumIntervallum_SearchCalendarControl2.Clear();
                DatumIntervallum_SearchCalendarControlSztornozas.SetSearchObjectFields(erec_IraJegyzekekSearch.SztornirozasDat);
                break;
            case "Vegrehajtott":
                erec_IraJegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Vegrahajtott;
                erec_IraJegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                erec_IraJegyzekekSearch.VegrehajtasDatuma.Operator = Query.Operators.notnull;
                DatumIntervallum_SearchCalendarControl1.Clear();
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(erec_IraJegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                break;
            case "Sztornozott":
                erec_IraJegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.Sztornozott;
                erec_IraJegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                erec_IraJegyzekekSearch.SztornirozasDat.Operator = Query.Operators.notnull;
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(erec_IraJegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                DatumIntervallum_SearchCalendarControl2.Clear();
                break;
            case "VegrehajtasFolyamatban":
                erec_IraJegyzekekSearch.Allapot.Value = KodTarak.JEGYZEK_ALLAPOT.VegrehajtasFolyamatban;
                erec_IraJegyzekekSearch.Allapot.Operator = Query.Operators.equals;
                DatumIntervallum_SearchCalendarControl1.Clear();
                DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(erec_IraJegyzekekSearch.LezarasDatuma);
                DatumIntervallum_SearchCalendarControlSztornozas.Clear();
                break;
        }

        if (!String.IsNullOrEmpty(MegsemmisitesiNaplo.SelectedValue))
        {
            erec_IraJegyzekekSearch.IraIktatokonyv_Id.Value = MegsemmisitesiNaplo.SelectedValue;
            erec_IraJegyzekekSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
        }

        if (Rendszerparameterek.IsTUK(Page))
        {

            if (CheckBox_MegsemmisitesreVar.Checked)
            {
                erec_IraJegyzekekSearch.MegsemmisitesDatuma.Operator = Query.Operators.isnull;
                DatumIntervallum_SearchCalendarControl_Megsemmisites.Clear();
            }
            else
            {
                DatumIntervallum_SearchCalendarControl_Megsemmisites.SetSearchObjectFields(erec_IraJegyzekekSearch.MegsemmisitesDatuma);
            }
        }
        else
        {
            DatumIntervallum_SearchCalendarControl_Megsemmisites.Clear();
            erec_IraJegyzekekSearch.MegsemmisitesDatuma.Clear();
        }

        return erec_IraJegyzekekSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraJegyzekekSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IraJegyzekekSearch GetDefaultSearchObject()
    {
        return Contentum.eRecord.Utility.Search.GetDefaultSearchObject_EREC_IraJegyzekekSearch(Page);
    }

    void FillMegsemmisitesiNaplo()
    {
        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
        Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
        Search.IktatoErkezteto.Value = "S";
        Search.OrderBy = "Iktatohely ASC, Ev DESC";

        Result result = service.GetAllWithExtensionAndJogosultsag(execParam, Search, true);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(SearchHeader1.ErrorPanel, result);
        }
        else
        {
            MegsemmisitesiNaplo.Items.Clear();
            MegsemmisitesiNaplo.Items.Add(new ListItem(Resources.Form.EmptyListItem, ""));

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string text = IktatoKonyvek.GetIktatokonyvText(row);
                MegsemmisitesiNaplo.Items.Add(new ListItem(text, id));
            }
        }
    }

    private void registerJavascripts()
    {
        DatumIntervallum_SearchCalendarControl_Megsemmisites.EnableOrDisableByCheckBox(CheckBox_MegsemmisitesreVar, true);
    }
}
