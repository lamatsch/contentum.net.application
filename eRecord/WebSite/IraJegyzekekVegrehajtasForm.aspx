﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IraJegyzekekVegrehajtasForm.aspx.cs" Inherits="IraJegyzekekVegrehajtasForm" Title="Untitled Page" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc" %>
<%@ Register Src="~/Component/TUKFelhasznaloMultiSelect.ascx" TagName="TUKFelhasznaloMultiSelect" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />
    <div>
       <iframe name="download" style="width:0px; height: 0px;" scrolling="no"></iframe>
    </div>
    <asp:UpdatePanel ID="FormUpdatePanel" Runat="server">
        <ContentTemplate>
            <uc:FormHeader ID="FormHeader1" runat="server" />
            <br />
            <table cellpadding="0" cellspacing="0" width="90%" style="margin:auto">
                    <tr>
                        <td>
                            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="Label1" runat="server" Text="Jegyzék megnevezés:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput" ReadOnly="true">
                                        </asp:TextBox>
                                        <asp:HiddenField ID="hfTipus" runat="server" />
                                    </td>
                                </tr>
                                    <%if (!IsBizottsagiTagokVisible)
                                        { %>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="LeveltariAtvevoLabel" runat="server" Text="Levéltári átvevő:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="LeveltariAtvevo" runat="server" CssClass="mrUrlapInput">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <%} else { %>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelBizottsagiTagok" runat="server" Text="Bizottsági tagok:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc:TUKFelhasznaloMultiSelect ID="BizottsagiTagok" runat="server" />
                                        </td>
                                    </tr>
                                    <%} %>
                                    <tr class="urlapSor" id="trMegsemmisitesiNaplo" runat="server" visible="false">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="MegsemmisitesiNaploReqStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                            <asp:Label ID="MegsemmisitesiNaploLabel" runat="server" Text="Megsemmisítési napló:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:DropDownList ID="MegsemmisitesiNaplo" runat="server" CssClass="mrUrlapInputComboBox"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="MegsemmisitesiNaploValidator" runat="server" ControlToValidate="MegsemmisitesiNaplo" SetFocusOnError="true"
                                                Display="None" ErrorMessage="<%$Resources:Form,RequiredFieldValidationMessage%>"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender
                                                ID="MegsemmisitesiNaploValidatorCalloutExtender" runat="server" TargetControlID="MegsemmisitesiNaploValidator">
                                                <Animations>
                                                <OnShow>
                                                <Sequence>
                                                    <HideAction Visible="true" />
                                                    <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                </Sequence>    
                                                </OnShow>
                                                </Animations>
                                            </ajaxToolkit:ValidatorCalloutExtender>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                        </td>
                                        <td class="mrUrlapMezoBold">
                                            <asp:CheckBox ID="cbAszinkron" runat="server" Text="Csatolmányok törlése háttérben fusson le" CssClass="urlapCheckbox"/>
                                        </td>
                                    </tr>
                                </table>
                            </eUI:eFormPanel>
                            <eUI:eFormPanel ID="ResultPanel" runat="server" Visible="false">
                                <div class="mrResultPanelText">A jegyzék végrehajtása sikeresen lefutott.</div>
                                <div style="text-align:left;padding-left: 10px;">
                                    <div style="font-weight:bold;">
                                        A levéltári átadási csomag:
                                    </div>
				                    <asp:Repeater ID="filesRepeater" runat="server">
                                        <HeaderTemplate><ul></HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <a href="<%#Eval("Url") %>" style="text-decoration:underline;" target="download"><%#Eval("Name") %></a>
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate></ul></FooterTemplate>
				                    </asp:Repeater>	
                                </div>
			                </eUI:eFormPanel>
                            <br />
                            <uc:FormFooter ID="FormFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
        </ContentTemplate>
   </asp:UpdatePanel>  
</asp:Content>
