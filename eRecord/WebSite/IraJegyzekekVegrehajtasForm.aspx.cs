﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;

public partial class IraJegyzekekVegrehajtasForm : System.Web.UI.Page
{
    private string Id = "";

    bool IsMegsemmisitesiNaplo
    {
        get
        {
            return Rendszerparameterek.IsTUK(Page);
        }
    }

    protected bool IsBizottsagiTagokVisible { get; set; }

    protected void Page_Init(object sender, EventArgs e)
    {
        Id = Request.QueryString.Get(QueryStringVars.Id);
        if (String.IsNullOrEmpty(Id))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else
        {

            EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = Id;

            Result result = service.Get(execParam);
            if (String.IsNullOrEmpty(result.ErrorCode))
            {
                EREC_IraJegyzekek EREC_IraJegyzekek = (EREC_IraJegyzekek)result.Record;
                LoadComponentsFromBusinessObject(EREC_IraJegyzekek);
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }

        FormFooter1.Command = CommandName.Modify;
    }

    private void LoadComponentsFromBusinessObject(EREC_IraJegyzekek erec_IraJegyzekek)
    {
        Nev_TextBox.Text = erec_IraJegyzekek.Nev;

        if (erec_IraJegyzekek.Tipus == UI.JegyzekTipus.SelejtezesiJegyzek.Value)
        {
            LeveltariAtvevoLabel.Text = "Ellenőr:";
            IsBizottsagiTagokVisible = Rendszerparameterek.IsTUK(Page);
        }
        else if (erec_IraJegyzekek.Tipus == UI.JegyzekTipus.EgyebSzervezetAtadasJegyzek.Value)
        {
            LeveltariAtvevoLabel.Text = "Átvevő:";
        }

        hfTipus.Value = erec_IraJegyzekek.Tipus;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.FullManualHeaderTitle = "Jegyzék végrehajtása";

        FormFooter1.ButtonsClick += new
           CommandEventHandler(FormFooter1ButtonsClick);

        if (IsMegsemmisitesiNaplo)
        {
            trMegsemmisitesiNaplo.Visible = true;
            if (!IsPostBack)
            {
                FillMegsemmisitesiNaplo();
            }
        }

    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            bool createAdatCsomag = Rendszerparameterek.GetBoolean(Page, "CREATE_LEVELTARI_ADATCSOMAG", false);

            if (hfTipus.Value == UI.JegyzekTipus.LeveltariAtadasJegyzek.Value && createAdatCsomag)
            {
                string[] csomagFajlok;
                Result resultCsomag = CreateLeveltariAdatCsomag(out csomagFajlok);

                if (resultCsomag.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultCsomag);
                }
                else
                {
                    Result resultVegrahajtas = JegyzekVegrehajtas();

                    if (resultVegrahajtas.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultVegrahajtas);
                    }
                    else
                    {
                        ShowResult(csomagFajlok);
                    }
                }
            }
            else
            {
                Result resultVegrahajtas = JegyzekVegrehajtas();

                if (resultVegrahajtas.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultVegrahajtas);
                }
                else
                {
                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page, true);
                }
            }
        }
    }

    class ResultFile
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

    private Result CreateLeveltariAdatCsomag(out string[] csomagFajlok)
    {
        EREC_IraJegyzekekService service = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        Result result = service.CreateLeveltariAdatCsomag(execParam, Id, null, out csomagFajlok);

        return result;
    }

    private Result JegyzekVegrehajtas()
    {
        EREC_IraJegyzekekService svc = eRecordService.ServiceFactory.GetEREC_IraJegyzekekService();
        ExecParam xpm = UI.SetExecParamDefault(Page);
        xpm.Record_Id = Id;

        Result res = svc.JegyzekVegrehajtasa(xpm, GetAtvevo(), MegsemmisitesiNaplo.SelectedValue, cbAszinkron.Checked);

        return res;
    }

    private void ShowResult(string[] csomagFajlok)
    {
        List<ResultFile> list = new List<ResultFile>();

        foreach (string fajl in csomagFajlok)
        {
            list.Add(new ResultFile { Name = System.IO.Path.GetFileName(fajl), Url = fajl });
        }

        filesRepeater.DataSource = list;
        filesRepeater.DataBind();

        EFormPanel1.Visible = false;
        ResultPanel.Visible = true;
        FormFooter1.ImageButton_Close.Visible = true;
        FormFooter1.ImageButton_Close.OnClientClick = JavaScripts.GetOnCloseClientClickScript();
        FormFooter1.ImageButton_Save.Visible = false;
        FormFooter1.ImageButton_Cancel.Visible = false;
    }

    void FillMegsemmisitesiNaplo()
    {
        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
        ExecParam execParam = UI.SetExecParamDefault(Page);

        EREC_IraIktatoKonyvekSearch Search = new EREC_IraIktatoKonyvekSearch();
        Search.IktatoErkezteto.Operator = Contentum.eQuery.Query.Operators.equals;
        Search.IktatoErkezteto.Value = "S";
        Search.OrderBy = "Iktatohely ASC, Ev DESC";

        Result result = service.GetAllWithIktathat(execParam, Search);

        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
        else
        {
            MegsemmisitesiNaplo.Items.Clear();

            int rowsCount = result.Ds.Tables[0].Rows.Count;
            if (rowsCount > 1)
            {
                MegsemmisitesiNaplo.Items.Add(new ListItem(Resources.Form.EmptyListItem, ""));
            }

            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                string id = row["Id"].ToString();
                string text = IktatoKonyvek.GetIktatokonyvText(row);
                MegsemmisitesiNaplo.Items.Add(new ListItem(text, id));
            }
        }
    }

    string GetAtvevo()
    {
        if (!IsBizottsagiTagokVisible)
            return LeveltariAtvevo.Text;
        else
            return BizottsagiTagok.GetSelectedText();
    }
}