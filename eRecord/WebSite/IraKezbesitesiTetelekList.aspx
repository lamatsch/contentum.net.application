<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
	CodeFile="IraKezbesitesiTetelekList.aspx.cs" Inherits="IraKezbesitesiTetelekList" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="~/Component/FunctionKeysManager.ascx" TagName="FunctionKeysManager" TagPrefix="fm" %>
<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/VisszakuldesPopup.ascx" TagName="VisszakuldesPopup" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<uc2:ListHeader ID="ListHeader1" runat="server" SztornoButtonToolTip="<%$Resources:Buttons,KezbesitesiTetelSztotno%>" />
	<asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<%--Hiba megjelenites--%>
			<eUI:eErrorPanel ID="EErrorPanel1" runat="server">
			</eUI:eErrorPanel>
		</ContentTemplate>
	</asp:UpdatePanel>
	<uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
	<%--/Hiba megjelenites--%>

	<%--HiddenFields--%>
	<asp:HiddenField
		ID="HiddenField1" runat="server" />
	<asp:HiddenField
		ID="MessageHiddenField" runat="server" />
	<%--/HiddenFields--%>

	<%--Tablazat / Grid--%>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="text-align: left; vertical-align: top; width: 0px;">
				<asp:ImageButton runat="server" ID="IraKezbesitesiTetelekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
			<td style="text-align: left; vertical-align: top; width: 100%;">
				<asp:UpdatePanel ID="IraKezbesitesiTetelekUpdatePanel" runat="server" OnLoad="IraKezbesitesiTetelekUpdatePanel_Load">
					<ContentTemplate>
						<asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
						<ajaxToolkit:CollapsiblePanelExtender ID="IraKezbesitesiTetelekCPE" runat="server" TargetControlID="Panel1"
							CollapsedSize="20" Collapsed="False" ExpandControlID="IraKezbesitesiTetelekCPEButton"
							CollapseControlID="IraKezbesitesiTetelekCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
							AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
							ImageControlID="IraKezbesitesiTetelekCPEButton" ExpandedSize="0" ExpandedText="K�zbes�t�si t�telek list�ja"
							CollapsedText="K�zbes�t�si t�telek list�ja">
						</ajaxToolkit:CollapsiblePanelExtender>
						<asp:Panel ID="Panel1" runat="server">
							<uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
							<table style="width: 98%;" cellpadding="0" cellspacing="0">
								<tr>
									<td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
										<uc:VisszakuldesPopup runat="server" ID="VisszakuldesPopup" />
										<asp:GridView ID="IraKezbesitesiTetelekGridView" runat="server" OnRowCommand="IraKezbesitesiTetelekGridView_RowCommand"
											CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="True"
											PagerSettings-Visible="false" AllowSorting="True" OnPreRender="IraKezbesitesiTetelekGridView_PreRender"
											AutoGenerateColumns="False" DataKeyNames="Id" OnSorting="IraKezbesitesiTetelekGridView_Sorting"
											OnRowDataBound="IraKezbesitesiTetelekGridView_RowDataBound">
											<RowStyle CssClass="GridViewRowStyle" Wrap="True" />
											<SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
											<HeaderStyle CssClass="GridViewHeaderStyle" />
											<Columns>
												<asp:TemplateField>
													<HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
													<ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
													<HeaderTemplate>
														<div class="DisableWrap">
															<asp:ImageButton ID="SelectingRowsImageButton"
																runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
															&nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton"
																			  runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
														</div>
													</HeaderTemplate>
													<ItemTemplate>
														<asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
													HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
													<HeaderStyle Width="25px" />
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
												</asp:CommandField>
												<asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
													<ItemTemplate>
														<asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
															Visible="false" OnClientClick="return false;" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
													HeaderStyle-Width="25px" HeaderText="Csny.">
													<ItemTemplate>
														<asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolm�ny" Height="20px" Width="25px"
															runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
													HeaderStyle-Width="25px" HeaderText="F.">
													<ItemTemplate>
														<asp:ImageButton ID="FeladatImage" AlternateText="Kezel�si feljegyz�s" runat="server" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:BoundField DataField="Atado_Nev" HeaderText="�tad�" SortExpression="Atado_Nev">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="Cimzett_Nev" HeaderText="C�mzett" SortExpression="Cimzett_Nev">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<%--<asp:BoundField DataField="Azonosito" HeaderText="Azonos�t�" SortExpression="Azonosito" >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="220px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>--%>
												<asp:TemplateField HeaderText="Azonos�t�" SortExpression="Azonosito">
													<HeaderStyle CssClass="GridViewBorderHeader" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
													<ItemTemplate>
														<asp:Label ID="labelAzonosito" runat="server" Text='<%#                                        
                                                                string.Concat(                                                                                                        
                                                                  (Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok ? "<a href=\"UgyUgyiratokList.aspx?Id="+ Eval("Obj_Id") as string + "\" style=\"text-decoration:underline\">" : ""
                                                                 ,(Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok ? "<a href=\"PldIratPeldanyokList.aspx?Id="+ Eval("Obj_Id") as string + "\" style=\"text-decoration:underline\">" : ""
                                                                 ,(Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek ? "<a href=\"KuldKuldemenyekList.aspx?Id="+ Eval("Obj_Id") as string + "\" style=\"text-decoration:underline\">" : ""
                                                                 , Eval("Azonosito")
                                                                 , ((Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_UgyUgyiratok
                                                                   || (Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_PldIratPeldanyok
                                                                   || (Eval("Obj_type") as string) == Contentum.eUtility.Constants.TableNames.EREC_KuldKuldemenyek) ? "<a />" : "") 
                                                                 %>' />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="Barkod" HeaderText="Vonalk�d" SortExpression="Barkod">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="110px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:TemplateField HeaderText="�gyirat �llapota" AccessibleHeaderText="UgyiratAllapota" Visible="false" SortExpression="UgyiratAllapota">
													<HeaderStyle CssClass="GridViewBorderHeader" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
													<ItemTemplate>
														<asp:Label ID="labelUgyiratAllapotaNev" runat="server" Text='<%#string.Concat(Eval("UgyiratAllapota"), Eval("UgyiratTovabbitasAlattiAllapota"), Eval("Irat_Ugyirat_Allapota"), Eval("Irat_Ugyirat_TovabbitasAlattiAllapota"))%>' />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Irat �llapota" AccessibleHeaderText="IratAllapota" Visible="false" SortExpression="IratAllapota">
													<HeaderStyle CssClass="GridViewBorderHeader" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
													<ItemTemplate>
														<asp:Label ID="labelIratAllapotaNev" runat="server" Text='<%#Eval("IratAllapota")%>' />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="Iratp�ld�ny �llapota" AccessibleHeaderText="IratpeldanyAllapota" Visible="false" SortExpression="IratpeldanyAllapota">
													<HeaderStyle CssClass="GridViewBorderHeader" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
													<ItemTemplate>
														<asp:Label ID="labelIratPeldanyAllapotaNev" runat="server" Text='<%#string.Concat(Eval("IratpeldanyAllapota"), Eval("IratpeldanyTovabbitasAlattiAllapota"))%>' />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="T�pus" SortExpression="Obj_type">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
													<ItemTemplate>
														<asp:Label ID="labelTipus" runat="server" Text='<%#Eval("Obj_type")%>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<%-- Rejtett inform�ci�k --%>
												<asp:BoundField DataField="Obj_type">
													<HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
												</asp:BoundField>
                                                <%--BUG_5993--%>
												<asp:TemplateField HeaderText="Obj_Id" SortExpression="Obj_id">
													<HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                                    <ItemTemplate>
														<asp:Label ID="labelObj_id" runat="server" Text='<%#Eval("Obj_id")%>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<%--<asp:BoundField DataField="Megjegyzes" HeaderText="T�rgy" SortExpression="Targy"  >
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>  --%>
												<asp:BoundField DataField="LetrehozasIdo" HeaderText="�tir.&nbsp;d�tuma" SortExpression="LetrehozasIdo">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="AtadasDat" HeaderText="�tad�s&nbsp;d�tuma" SortExpression="AtadasDat">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<%--                                                <asp:TemplateField HeaderText="�tv�tel&nbsp;d�tuma" SortExpression="AtvetelDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                                   
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelAtvetelDat" runat="server" Text='<%#Eval("AtvetelDat")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
												<asp:BoundField DataField="AtvetelDat" HeaderText="�tv�tel&nbsp;d�tuma" SortExpression="AtvetelDat">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="Atvevo_Nev" HeaderText="�tvev�" SortExpression="Atvevo_Nev">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:TemplateField>
													<HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemTemplate>
														<asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField>
													<HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemTemplate>
														<asp:Label ID="labelAllapot" runat="server" Text='<%#Eval("Allapot")%>'></asp:Label>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField HeaderText="�llapot" SortExpression="Allapot">
													<HeaderStyle CssClass="GridViewBorderHeader" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
													<ItemTemplate>
														<asp:Label ID="labelAllapotNev" runat="server" Text='<%#Eval("Allapot")%>' />
													</ItemTemplate>
												</asp:TemplateField>
                                                <%--BLG_4233--%>
                                               <asp:BoundField DataField="Megjegyzes" HeaderText="Nyilv�ntart�si sz�m" SortExpression="Megjegyzes">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="Note" HeaderText="Megj." SortExpression="Note" Visible="false">
													<HeaderStyle CssClass="GridViewBorderHeader" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:BoundField DataField="KezelesTipusNev" HeaderText="Kezel�si �tas�t�s">
													<HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
													<ItemStyle CssClass="GridViewBoundFieldItemStyle" />
												</asp:BoundField>
												<asp:TemplateField HeaderText="Z�rol�s"
													ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
													<HeaderTemplate>
														<asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
													</HeaderTemplate>
													<ItemTemplate>
														<asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
													</ItemTemplate>
												</asp:TemplateField>
												<%-- Seg�d mez�k (�ltal�nos keres�shez, ahol objektumok is megengedettek, k�zbes�t�si t�tel n�lk�l) --%>
												<asp:TemplateField>
													<HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
													<ItemTemplate>
														<%-- K�zbes�t�si t�tel, ha van Id --%>
														<asp:CheckBox ID="cbKezbesitesiTetel" runat="server" Enabled="false" />
														<%-- �tvehet�, ha k�zbes�t�si t�tel, �llapota �tadott �s van funkci�jog --%>
														<asp:CheckBox ID="cbAtveheto" runat="server" Enabled="false" />
                                                        <%--BUG_5993--%>
                                                        <asp:CheckBox ID="cbAtvehetoUgyintezesre" runat="server" Enabled="false" />
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
											<PagerSettings Visible="False" />
										</asp:GridView>
									</td>
								</tr>
							</table>
						</asp:Panel>
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
	</table>
	<br />

</asp:Content>

