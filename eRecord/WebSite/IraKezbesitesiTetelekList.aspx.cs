using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IraKezbesitesiTetelekList : Contentum.eUtility.UI.PageBase
{
    // TODO: az el�gaz�s megsz�ntet�se, ahol az isBranched jel�li az el�gaz�st
    // (egyel�re megtartjuk a r�gi k�zbes�t�si t�teles lek�rdez�seket (�tadand�k, �tveend�k, K�zbes�t�si t�telek list�ja))
    // ha igaz, az AltalanosKereses node kiv�tel�vel a r�gi dolgokat haszn�ljuk
    // ha hamis, minden �g az �j lek�rdez�si m�dot haszn�lja
    //private const bool isBranched = false;

    private UI ui = new UI();
    private String Mode = ""; // �rt�kei: "", "Atadandok" vagy "Atveendok", "Altalanos Kereses", "NapiPosta"
    private String Startup = ""; // �rt�kei: "", "FromFeladataim"
    private String TableName = ""; // �rt�kei: Constants.TableNames.EREC_KuldKuldemenyek/EREC_PldIratPeldanyok/EREC_UgyUgyiratok/KRT_Mappak
    private String Filter = ""; // �rt�kei: Constants.FilterType.IraKezbesitesiTetelekFilter.Sajat/Szervezet/All
    //BLG 1130  �tvev� jegyz�k nyomtat�sa
    private bool isTuk = false; //csak T�K kezel�s z�m�ra el�rhet� funkci�k kezel�s�hez

    private string filteredBy = ""; // a Filter �s TableName alapj�n k�pezz�k �s a c�msorba �rjuk

    public const string kuldemeny = "K�ldem�ny";
    public const string iratpeldany = "Iratp�ld�ny";
    public const string ugyirat = "�gyirat";
    public const string dosszie = "Dosszi�";

    public string maxTetelszam = "0";

    private const string cbKezbesitesiTetelId = "cbKezbesitesiTetel";
    private const string cbAtvehetolId = "cbAtveheto";
    // BUG_5993
    private const string cbAtvehetoUgyintezesreId = "cbAtvehetoUgyintezesre";

    private const string CommandName_NapiPosta = "NapiPosta"; // TODO: kiemelni
    private const string QueryStringVars_Date = "Date"; // TODO: kiemelni

    private const string FunkcioKod_UgyiratAtvetel = "UgyiratAtvetel";
    private const string FunkcioKod_IratPeldanyAtvetel = "IratPeldanyAtvetel";
    private const string FunkcioKod_KuldemenyAtvetel = "KuldemenyAtvetel";
    private const String FunkcioKod_DosszieAtvetel = "MappakAtvetel";
    private const String FunkcioKod_AtvetelSzervezettol = "AtvetelSzervezettol";
    private const String FunkcioKod_UgyiratAtadas = "UgyiratAtadas";
    private const String FunkcioKod_IratPeldanyAtadas = "IratPeldanyAtadas";
    private const String FunkcioKod_KuldemenyAtadas = "KuldemenyAtadas";
    private const String FunkcioKod_DosszieAtadas = "MappakAtadas";

    // BUG_5993
    private const String FunkcioKod_UgyiratAtvetelUgyintezesre = "UgyiratAtvetelUgyintezesre";

    #region Utils


    private void ClearSearch_Manual_Obj_type_Fields(EREC_IraKezbesitesiTetelekSearch search)
    {
        search.Manual_Obj_type_Kuldemeny.Clear();
        search.Manual_Obj_type_Ugyirat.Clear();
        search.Manual_Obj_type_IratPeldany.Clear();
    }

    private Ugyiratok.Statusz GetUgyiratStatusz(string id, out string errorMessage)
    {
        errorMessage = String.Empty;
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = id;

        EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        Result result_ugyiratGet = service_ugyiratok.Get(execParam);
        if (result_ugyiratGet.IsError)
        {
            // hiba:
            errorMessage = ResultError.GetErrorMessageFromResultObject(result_ugyiratGet);
            return null;
        }

        EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

        return Ugyiratok.GetAllapotByBusinessDocument(ugyiratObj);
    }

    // BUG_5993
    //private void SetUgyintezesreAtveheto(string id)
    //{
    //    string errorMessage;
    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

    //    Ugyiratok.Statusz ugyiratStatusz = GetUgyiratStatusz(id, out errorMessage);


    //    if (!String.IsNullOrEmpty(errorMessage))
    //    {
    //        ListHeader1.AtvetelUgyintezesreOnClientClick = "alert('" + Resources.Error.ErrorCode_52161
    //             + errorMessage
    //             + "'); return false;";
    //    }
    //    else if (ugyiratStatusz != null)
    //    {
    //        ErrorDetails errorDetail;

    //        // �tv�tel �gyint�z�sre
    //        execParam.Record_Id = ugyiratStatusz.Id; // == id
    //        if (Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam, out errorDetail))
    //        {
    //            ListHeader1.AtvetelUgyintezesreOnClientClick = JavaScripts.SetOnClientClick("AtvetelUgyintezesreForm.aspx",
    //                 QueryStringVars.UgyiratId + "=" + id
    //                 , Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
    //        }
    //        else
    //        {
    //            ListHeader1.AtvetelUgyintezesreOnClientClick = "alert('" + Resources.Error.ErrorCode_52161
    //                 + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
    //                 + "'); return false;";
    //        }
    //    }
    //}
    #endregion Utils

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(Constants.Startup.StartupName) ?? "";

        Mode = Request.QueryString.Get(QueryStringVars.Mode);
        if (Mode == null) { Mode = ""; }
        if (Mode == CommandName.Atadandok)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Atadandok");
        }
        else if (Mode == CommandName.Atveendok)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Atveendok");
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
#if isBranched
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KezbesitesiTetelekAltalanosKeresesList");
#else
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KezbesitesiTetelekList");
#endif
        }
        else
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KezbesitesiTetelekList");
        }

        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["IraKezbesitesiTetelekListStartup"] == null)
        {
            if (Startup == Constants.Startup.SearchForm)
            {
                Session["IraKezbesitesiTetelekListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath);
            }
        }

        //BLG1130  �tvev� jegyz�k nyomtat�sa
        isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
#if isBranched
            if (Mode == CommandName.AltalanosKereses)
            {
                // a r�gi lapoz�s n�lk�li v�ltozathoz nem kell
                ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
            }
#else
        // a r�gi lapoz�s n�lk�li v�ltozathoz nem kell
        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);
#endif

        TableName = (Request.QueryString.Get(QueryStringVars.TableName) ?? "");
        Filter = (Request.QueryString.Get(QueryStringVars.Filter) ?? "");

        List<String> filteredByList = new List<string>();

        switch (TableName)
        {
            case Constants.TableNames.EREC_UgyUgyiratok:
                filteredByList.Add(ugyirat.ToLower());
                break;
            case Constants.TableNames.EREC_KuldKuldemenyek:
                filteredByList.Add(kuldemeny.ToLower());
                break;
            case Constants.TableNames.EREC_PldIratPeldanyok:
                filteredByList.Add(iratpeldany.ToLower());
                break;
            case Constants.TableNames.KRT_Mappak:
                filteredByList.Add(dosszie.ToLower());
                break;
        }

        switch (Filter)
        {
            case Constants.FilterType.IraKezbesitesiTetelekFilter.Sajat:
                filteredByList.Add("szem�lyre");
                break;
            case Constants.FilterType.IraKezbesitesiTetelekFilter.Szervezet:
                filteredByList.Add("szervezetre");
                break;
        }

        if (filteredByList.Count > 0)
        {
            filteredBy = ": " + String.Join(", ", filteredByList.ToArray());
        }

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, IraKezbesitesiTetelekGridView);

        if (Mode == CommandName.Atadandok)
        {
            int indexAtvetelDat = UI.GetGridViewColumnIndex(IraKezbesitesiTetelekGridView, "AtvetelDat");
            int indexAtvevo_Nev = UI.GetGridViewColumnIndex(IraKezbesitesiTetelekGridView, "Atvevo_Nev");
            if (indexAtvetelDat > -1 && IraKezbesitesiTetelekGridView.Columns.Count > indexAtvetelDat)
            {
                IraKezbesitesiTetelekGridView.Columns[indexAtvetelDat].Visible = false;
            }
            if (indexAtvevo_Nev > -1 && IraKezbesitesiTetelekGridView.Columns.Count > indexAtvevo_Nev)
            {
                IraKezbesitesiTetelekGridView.Columns[indexAtvevo_Nev].Visible = false;
            }
        }

        if (!FelhasznaloProfil.OrgIsBOPMH(Page))
        {
            UI.SetGridViewColumnVisiblity(IraKezbesitesiTetelekGridView, "KezelesTipusNev", false);
        }
        // BUG_4233
        UI.SetGridViewColumnVisiblity(IraKezbesitesiTetelekGridView, "Megjegyzes", isTuk);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.IraKezbesitesiTetelekListHeaderTitle;


        if (Mode == CommandName.Atadandok)
        {
            //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.AtadandokSearch;
            ListHeader1.SearchObjectType = typeof(EREC_IraKezbesitesiTetelekSearch);
        }
        else if (Mode == CommandName.Atveendok)
        {
            //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.AtveendokSearch;
            ListHeader1.SearchObjectType = typeof(EREC_IraKezbesitesiTetelekSearch);
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch;
            ListHeader1.SearchObjectType = typeof(EREC_IraKezbesitesiTetelekSearch);
        }
        else
        {
            ListHeader1.SearchObjectType = typeof(EREC_IraKezbesitesiTetelekSearch);
        }

        #region BLG1131 - Ut�lagosan administr�lt bels� iratmozg�sok

        //a funkci�t T�K kezel� haszn�lhatja
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
        {
            ListHeader1.IratmozgasAdminisztralasaVisible = true;
            ListHeader1.IratmozgasAdminisztralasaEnabled = true;
            ListHeader1.IratmozgasAdminisztralasaOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }

        #endregion

        ListHeader1.NewVisible = false;
        //ListHeader1.ViewVisible = false;
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyVisible = false;
        ListHeader1.UgyiratTerkepVisible = true;
        ListHeader1.InvalidateVisible = false;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;

        #region BLG1130 �tvev� jegyz�k / T�meges �tvev� jegyz�k nyomtat�sa
        if (Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK))
        {
            ListHeader1.AtvevoJegyzekNyomtatasaVisible = true;
            ListHeader1.AtvevoJegyzekNyomtatasaEnabled = true;
            ListHeader1.AtvevoJegyzekNyomtatasaOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
            ListHeader1.AtvevoJegyzekNyomtatasaTomegesVisible = true;
            ListHeader1.AtvevoJegyzekNyomtatasaTomegesEnabled = true;
            ListHeader1.AtvevoJegyzekNyomtatasaTomegesOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";
        }
        #endregion

        if (Mode == String.Empty)
        {
            ListHeader1.SztornoVisible = true;
            ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            #region LZS - Excel ikon a Skontr�hoz
            ListHeader1.ExportVisible = true;
            ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
            ListHeader1.ExportToolTip = Resources.Buttons.Export;

            #endregion
        }

        if (Mode == CommandName.Atadandok)
        {
            ListHeader1.AtadasVisible = true;
            ListHeader1.AtadasListavalVisible = true;
            ListHeader1.SztornoVisible = true;
            if (String.IsNullOrEmpty(filteredBy))
            {
                ListHeader1.HeaderLabel = Resources.List.IraKezbesitesiTetelek_Atadandok_ListHeaderTitle;
            }
            else
            {
                ListHeader1.HeaderLabel = String.Format(Resources.List.IraKezbesitesiTetelek_Atadandok_ListHeaderTitle_Filtered, filteredBy);
            }
            if (FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"))
            {
                ListHeader1.UgyiratpotlonyomtatasVisible = true;
            }



        }
        else if (Mode == CommandName.Atveendok)
        {
            ListHeader1.AtvetelVisible = true;
            ListHeader1.AtvetelListavalVisible = true;
            ListHeader1.AtvetelUgyintezesreVisible = true;
            ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;
            if (String.IsNullOrEmpty(filteredBy))
            {
                ListHeader1.HeaderLabel = Resources.List.IraKezbesitesiTetelek_Atveendok_ListHeaderTitle;
            }
            else
            {
                ListHeader1.HeaderLabel = String.Format(Resources.List.IraKezbesitesiTetelek_Atveendok_ListHeaderTitle_Filtered, filteredBy);
            }

        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            ListHeader1.AtvetelVisible = true;
            ListHeader1.AtvetelListavalVisible = true;
            ListHeader1.AtvetelUgyintezesreVisible = true;
            ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_ENABLED, false); //true;

            //ListHeader1.AtadasVisible = true;
            //ListHeader1.AtadasListavalVisible = true;
            ListHeader1.SztornoVisible = true;

            ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            // K�zbes�t�si t�telek/Iratkezel�si objektumok
            ListHeader1.HeaderLabel = Resources.List.IraKezbesitesiTetelek_AltalanosKereses_ListHeaderTitle;
        }

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        string queryString_search = "";
        if (Mode == CommandName.Atadandok)
        {
            queryString_search = QueryStringVars.Mode + "=" + CommandName.Atadandok;
        }
        else if (Mode == CommandName.Atveendok)
        {
            queryString_search = QueryStringVars.Mode + "=" + CommandName.Atveendok;
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            queryString_search = QueryStringVars.Mode + "=" + CommandName.AltalanosKereses;
        }

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraKezbesitesiTetelekSearch.aspx", queryString_search
             , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        //ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraKezbesitesiTetelekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
        //    , Defaults.PopupWidth, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        //ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        //ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraKezbesitesiTetelekGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraKezbesitesiTetelekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IraKezbesitesiTetelekListajaPrintForm.aspx?" + QueryStringVars.Mode + "=" + Mode);

        string column_v = "";
        for (int i = 0; i < IraKezbesitesiTetelekGridView.Columns.Count; i++)
        {
            if (IraKezbesitesiTetelekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";

        if (Mode == CommandName.Atadandok)
        {
            //ListHeader1.AtadasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();


            //ListHeader1.AtadasOnClientClick = "var count = getSelectedCheckBoxesCount('"
            //        + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            //        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
            //        + " else if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            //        + " else if (confirm('" + Resources.Question.UIConfirmHeader_Atadas_Tomeges
            //        + "\\n(" + Resources.List.UI_SelectedItemsCount + " '+count+')"
            //        + "')) {alert('Amennyiben az �tadott objektumok tartalmaznak pap�ron l�tez� inform�ci�kat, az �tad�st fizikailag is hajtsa v�gre!');} else {return false;}";

            //ListHeader1.AtadasListavalOnClientClick = "var count = getSelectedCheckBoxesCount('"
            //        + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            //        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
            //        + " else if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            //        + " else if (confirm('" + Resources.Question.UIConfirmHeader_Atadas_Tomeges
            //        + "\\n(" + Resources.List.UI_SelectedItemsCount + " '+count+')"
            //        + "')) {alert('Amennyiben az �tadott objektumok tartalmaznak pap�ron l�tez� inform�ci�kat, az �tad�st fizikailag is hajtsa v�gre!');} else {return false;}";

            ListHeader1.AtadasOnClientClick = "var count = getSelectedAndCheckboxChecked('" + IraKezbesitesiTetelekGridView.ClientID + "','check', 'cbUgyintezesModja');"
                      + " if (count.selected==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
                      + " else if (count.selected>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                      + " else if (confirm('" + Resources.Question.UIConfirmHeader_Atadas_Tomeges
                      + "\\n(" + Resources.List.UI_SelectedItemsCount + " '+count.selected+')"
                      + "')) {"
                      + "if (count.checked < count.selected) { alert('" + Resources.List.UI_Atadas_AtadasFizikailag + "');}} else {return false;}";

            ListHeader1.AtadasListavalOnClientClick = "var count = getSelectedAndCheckboxChecked('" + IraKezbesitesiTetelekGridView.ClientID + "','check', 'cbUgyintezesModja');"
                      + " if (count.selected==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
                      + " else if (count.selected>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                      + " else if (confirm('" + Resources.Question.UIConfirmHeader_Atadas_Tomeges
                      + "\\n(" + Resources.List.UI_SelectedItemsCount + " '+count.selected+')"
                      + "')) {"
                      + "if (count.checked < count.selected) { alert('" + Resources.List.UI_Atadas_AtadasFizikailag + "');}} else {return false;}";


            ListHeader1.SztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            ListHeader1.UgyiratpotlonyomtatasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                      + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
                      + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} ";

            column_v = "";
            for (int i = 0; i < IraKezbesitesiTetelekGridView.Columns.Count; i++)
            {
                if (IraKezbesitesiTetelekGridView.Columns[i].Visible)
                {
                    column_v = column_v + "1";
                }
                else
                {
                    column_v = column_v + "0";
                }
            }

            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekSSRSAtadandok.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Mode == CommandName.Atveendok)
        {
            //ListHeader1.AtvetelOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            //ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
            //        + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            //        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
            //        + " else if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            //        + " else if (confirm('" + Resources.Question.UIConfirmHeader_Atvetel_Tomeges
            //        + "\\n(" + Resources.List.UI_SelectedItemsCount + " '+count+')"
            //        + "')) {} else {return false;}";

            //ListHeader1.AtvetelListavalOnClientClick = "var count = getSelectedCheckBoxesCount('"
            //        + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
            //        + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
            //        + " else if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            //        + " else if (confirm('" + Resources.Question.UIConfirmHeader_Atvetel_Tomeges
            //        + "\\n(" + Resources.List.UI_SelectedItemsCount + " '+count+')"
            //        + "')) {} else {return false;}";

            // return true: visszat�r�skor megjel�li a nem v�grehajthat� rekordokat akkor is, ha 
            ListHeader1.AtvetelOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
                 IraKezbesitesiTetelekGridView.ClientID, "check", cbAtvehetolId, true, Int32.Parse(maxTetelszam)
                  , "�tv�tel", Resources.Question.UIConfirmHeader_Atvetel_Tomeges, true);

            ListHeader1.AtvetelListavalOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
                 IraKezbesitesiTetelekGridView.ClientID, "check", cbAtvehetolId, true, Int32.Parse(maxTetelszam)
                  , "�tv�tel list�val", Resources.Question.UIConfirmHeader_Atvetel_Tomeges, true);

            // BUG_5993
            //ListHeader1.AtvetelUgyintezesreOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.AtvetelUgyintezesreOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                       + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
                                       + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            column_v = "";
            for (int i = 0; i < IraKezbesitesiTetelekGridView.Columns.Count; i++)
            {
                if (IraKezbesitesiTetelekGridView.Columns[i].Visible)
                {
                    column_v = column_v + "1";
                }
                else
                {
                    column_v = column_v + "0";
                }
            }

            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekSSRSAtveendok.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            ListHeader1.AtvetelOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
                 IraKezbesitesiTetelekGridView.ClientID, "check", cbAtvehetolId, true, Int32.Parse(maxTetelszam)
                  , "�tv�tel", Resources.Question.UIConfirmHeader_Atvetel_Tomeges, true);

            ListHeader1.AtvetelListavalOnClientClick = JavaScripts.SetOnClientClickOperationAllowedConfirm(
                 IraKezbesitesiTetelekGridView.ClientID, "check", cbAtvehetolId, true, Int32.Parse(maxTetelszam)
                  , "�tv�tel list�val", Resources.Question.UIConfirmHeader_Atvetel_Tomeges, true);

            // BUG_5993
            //ListHeader1.AtvetelUgyintezesreOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.AtvetelUgyintezesreOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                       + IraKezbesitesiTetelekGridView.ClientID + "','check'); "
                                       + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            column_v = "";
            for (int i = 0; i < IraKezbesitesiTetelekGridView.Columns.Count; i++)
            {
                if (IraKezbesitesiTetelekGridView.Columns[i].Visible)
                {
                    column_v = column_v + "1";
                }
                else
                {
                    column_v = column_v + "0";
                }
            }

            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekAltalanosKeresesSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }

        if (Mode == CommandName.AltalanosKereses)
        {
            ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraKezbesitesiTetelekGridView.ClientID, cbKezbesitesiTetelId);
            ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraKezbesitesiTetelekGridView.ClientID, "check", cbKezbesitesiTetelId);
        }
        else
        {
            ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraKezbesitesiTetelekGridView.ClientID);
            ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraKezbesitesiTetelekGridView.ClientID);
        }
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraKezbesitesiTetelekGridView.ClientID);

        //ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraKezbesitesiTetelekGridView.ClientID);
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
            QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                        , Defaults.PopupWidth, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, "", true);

        //BLG_2334
        if (Mode == CommandName.Atadandok || Mode == CommandName.Atveendok)
        {
            bool hivatkozasEnabled = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.HIVATKOZAS_KULDES_KEZBESITESITETEL_ENABLED, false);

            if (hivatkozasEnabled)
            {
                string js = ListHeader1.SendObjectsOnClientClick;

                ListHeader1.SendObjectsOnClientClick = "var count = getSelectedAndCheckboxChecked('" + IraKezbesitesiTetelekGridView.ClientID + "','check', 'cbUgyintezesModja');"
                 + " if (count.selected==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;} "
                 + " else {alert('" + Resources.List.UI_KezbesitesiTetelekHivatkozasokKuldese + "');" + js + "}";
            }
            else
            {
                ListHeader1.SendObjectsEnabled = ListHeader1.SendObjectsVisible = hivatkozasEnabled;
            }
        }

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IraKezbesitesiTetelekGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraKezbesitesiTetelekSearch(true));

        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["IraKezbesitesiTetelekListStartup"] != null)
        {
            if (Session["IraKezbesitesiTetelekListStartup"].ToString() == Constants.Startup.SearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";

                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("IraKezbesitesiTetelekSearch.aspx", String.Empty
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                     , CustomUpdateProgress1.UpdateProgress.ClientID);

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "IraKezbesitesiTetelekSearch", script, true);
                Session.Remove("IraKezbesitesiTetelekListStartup");

                popupNyitas = true;
            }
        }

        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            IraKezbesitesiTetelekGridViewBind();
        }

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = false;
        ListHeader1.ViewEnabled = true;
        ListHeader1.ModifyEnabled = false;
        ListHeader1.UgyiratTerkepEnabled = true;
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetel" + CommandName.ViewHistory);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetel" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetel" + CommandName.Lock);

        ListHeader1.AtadasEnabled = (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtadas)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtadas)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtadas)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtadas));
        ListHeader1.AtvetelEnabled = (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtvetel)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtvetel)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtvetel)
                  || FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol));
        ListHeader1.AtvetelUgyintezesreEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetelUgyintezesre");

        ListHeader1.VisszakuldesEnabled = ListHeader1.AtvetelEnabled; // egyel�re az �tv�tel jog�hoz k�tj�k

        ListHeader1.SztornoEnabled = FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetelSztorno");

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        ui.SetClientScriptToGridViewSelectDeSelectButton(IraKezbesitesiTetelekGridView);

        string column_v = "";
        for (int i = 0; i < IraKezbesitesiTetelekGridView.Columns.Count; i++)
        {
            if (IraKezbesitesiTetelekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        if (Mode == CommandName.Atadandok)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekSSRSAtadandok.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Mode == CommandName.Atveendok)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekSSRSAtveendok.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekAltalanosKeresesSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraKezbesitesiTetelekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }

    }

    #endregion


    #region Master List

    protected void IraKezbesitesiTetelekGridViewBind()
    {
        //String sortExpression = Search.GetSortExpressionFromViewState("IraKezbesitesiTetelekGridView", ViewState, "EREC_IraKezbesitesiTetelek.LetrehozasIdo");
        String sortExpression = Search.GetSortExpressionFromViewState("IraKezbesitesiTetelekGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraKezbesitesiTetelekGridView", ViewState, SortDirection.Descending);

        IraKezbesitesiTetelekGridViewBind(sortExpression, sortDirection);
    }

    protected void IraKezbesitesiTetelekGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        Contentum.eRecord.Service.EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IraKezbesitesiTetelekSearch search = null;
        string Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);
        if (Mode == CommandName.Atadandok)
        {
            if (!IsPostBack && Startup == Constants.Startup.FromFeladataim) // ha feladataim panelr�l ind�tottuk
            {
                // �j search objektumot hozunk l�tre, �s sessionbe tessz�k
                search = new EREC_IraKezbesitesiTetelekSearch(true);
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AtadandokSearch);
            }

            search = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                 Page, new EREC_IraKezbesitesiTetelekSearch(true), Constants.CustomSearchObjectSessionNames.AtadandokSearch);

            // Sz�r�s csak az �tadand�kra:
            search.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt;
            search.Allapot.Operator = Query.Operators.equals;

            if (!String.IsNullOrEmpty(TableName))
            {
                // ha van sz�r�s t�bl�ra, kitakar�tjuk a mez�ket
                ClearSearch_Manual_Obj_type_Fields(search);
            }

            // Sz�r�s a t�pusra, ha sz�ks�ges
            switch (TableName)
            {
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    search.Manual_Obj_type_Kuldemeny.Value = Constants.TableNames.EREC_KuldKuldemenyek;
                    search.Manual_Obj_type_Kuldemeny.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.EREC_UgyUgyiratok:
                    search.Manual_Obj_type_Ugyirat.Value = Constants.TableNames.EREC_UgyUgyiratok;
                    search.Manual_Obj_type_Ugyirat.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    search.Manual_Obj_type_IratPeldany.Value = Constants.TableNames.EREC_PldIratPeldanyok;
                    search.Manual_Obj_type_IratPeldany.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.KRT_Mappak:
                    search.Manual_Obj_type_Dosszie.Value = Constants.TableNames.KRT_Mappak;
                    search.Manual_Obj_type_Dosszie.Operator = Query.Operators.equals;
                    break;
            }

            // Sz�r�s azokra a rekordokra, ahol a felhaszn�l� az �tad�
            search.Felhasznalo_Id_Atado_USER.Value = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam);
            search.Felhasznalo_Id_Atado_USER.Operator = Query.Operators.equals;

            // Sz�r�s iratt�rb�l k�lcs�nz�tt t�telekre
            if (!FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"))
            {
                if (search.Extended_EREC_UgyUgyiratokSearch == null)
                {
                    search.Extended_EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
                }

            }

            // vissza�r�s a sessionbe
            Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AtadandokSearch);
        }
        else if (Mode == CommandName.Atveendok)
        {
            if (!IsPostBack && Startup == Constants.Startup.FromFeladataim) // ha feladataim panelr�l ind�tottuk
            {
                // �j search objektumot hozunk l�tre, �s sessionbe tessz�k
                search = new EREC_IraKezbesitesiTetelekSearch(true);
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AtveendokSearch);
            }

            search = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                 Page, new EREC_IraKezbesitesiTetelekSearch(true), Constants.CustomSearchObjectSessionNames.AtveendokSearch);

            // Sz�r�s csak az �tveend�kre:
            if (Startup == Constants.Startup.FromFeladataim)
            {
                search.Allapot.Value = Search.GetSqlInnerString(new string[] {
                     KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott});
                search.Allapot.Operator = Query.Operators.inner;
            }
            else
            {
                search.Allapot.Value = Search.GetSqlInnerString(new string[] {
                     KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                     , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott});
                search.Allapot.Operator = Query.Operators.inner;
            }

            if (!String.IsNullOrEmpty(TableName))
            {
                // ha van sz�r�s t�bl�ra, kitakar�tjuk a mez�ket
                ClearSearch_Manual_Obj_type_Fields(search);
            }

            // Sz�r�s a t�pusra, ha sz�ks�ges
            switch (TableName)
            {
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    search.Manual_Obj_type_Kuldemeny.Value = Constants.TableNames.EREC_KuldKuldemenyek;
                    search.Manual_Obj_type_Kuldemeny.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.EREC_UgyUgyiratok:
                    search.Manual_Obj_type_Ugyirat.Value = Constants.TableNames.EREC_UgyUgyiratok;
                    search.Manual_Obj_type_Ugyirat.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    search.Manual_Obj_type_IratPeldany.Value = Constants.TableNames.EREC_PldIratPeldanyok;
                    search.Manual_Obj_type_IratPeldany.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.KRT_Mappak:
                    search.Manual_Obj_type_Dosszie.Value = Constants.TableNames.KRT_Mappak;
                    search.Manual_Obj_type_Dosszie.Operator = Query.Operators.equals;
                    break;
            }

            // Sz�r�s az iratt�rba k�ld�tt �llapotra, ha sz�ks�ges (�gyiratokn�l)
            switch (TableName)
            {
                case Constants.TableNames.EREC_UgyUgyiratok:
                case "":
                    if (search.Extended_EREC_UgyUgyiratokSearch == null)
                    {
                        search.Extended_EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
                    }
                    search.Extended_EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.Value = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
                    search.Extended_EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.Operator = Query.Operators.notequals;
                    break;
            }

            // Sz�r�s saj�tra, a csoportra, vagy mindre
            switch (Filter)
            {
                case Constants.FilterType.IraKezbesitesiTetelekFilter.Sajat:
                    search.Csoport_Id_Cel.Value = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam);
                    search.Csoport_Id_Cel.Operator = Query.Operators.equals;
                    break;
                case Constants.FilterType.IraKezbesitesiTetelekFilter.Szervezet:
                    if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id) && FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol))
                    {
                        search.Csoport_Id_Cel.Value = ExecParam.FelhasznaloSzervezet_Id;
                        search.Csoport_Id_Cel.Operator = Query.Operators.equals;
                    }
                    break;
                case Constants.FilterType.IraKezbesitesiTetelekFilter.All:
                default:
                    // Sz�r�s azokra a rekordokra, ahol a felhaszn�l� benne van a C�mzett csoportban:
                    // TODO: m�g csak a saj�t csoportra m�k�dik:
                    search.Csoport_Id_Cel.Value = "'" + Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam) + "'";
                    if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id) && FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol))
                    {
                        search.Csoport_Id_Cel.Value += ",'" + ExecParam.FelhasznaloSzervezet_Id + "'";
                    }
                    search.Csoport_Id_Cel.Operator = Query.Operators.inner;
                    break;
            }
            //}

            // vissza�r�s a sessionbe
            Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AtveendokSearch);

        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            if (!IsPostBack && !Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch))
            {

                // �j search objektumot hozunk l�tre, �s sessionbe tessz�k
                search = new EREC_IraKezbesitesiTetelekSearch(true);

                // Sz�r�s csak az �tveend�kre:
                search.Allapot.Value = Search.GetSqlInnerString(new string[] {
                          KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                          , KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott});
                search.Allapot.Operator = Query.Operators.inner;

                search.Extended_EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.Value = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
                search.Extended_EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.Operator = Query.Operators.notequals;

                search.isObjectMode = false;

                // Sz�r�s azokra a rekordokra, ahol a felhaszn�l� benne van a C�mzett csoportban:
                // TODO: m�g csak a saj�t csoportra m�k�dik:
                search.Csoport_Id_Cel.Value = "'" + Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam) + "'";
                if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id) && FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol))
                {
                    search.Csoport_Id_Cel.Value += ",'" + ExecParam.FelhasznaloSzervezet_Id + "'";
                }
                search.Csoport_Id_Cel.Operator = Query.Operators.inner;

                search.ReadableWhere = Resources.Search.ReadableWhere_AltalanosKereses_Default;

                // vissza�r�s a sessionbe
                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch);
            }

            search = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                 Page, new EREC_IraKezbesitesiTetelekSearch(true), Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch);

            //// vissza�r�s a sessionbe
            //Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch);

        }
        else if (!IsPostBack && Startup == Constants.Startup.FromFeladataim && Mode == CommandName_NapiPosta)
        {
            // ha a FeladatRiportb�l �rkez�nk, az els� h�v�skor (de csak akkor) az alaplist�t sz�rj�k

            search = new EREC_IraKezbesitesiTetelekSearch();
            // Sz�r�s csak az �tvettekre:
            search.Allapot.Value = KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett;
            search.Allapot.Operator = Query.Operators.equals;

            if (!String.IsNullOrEmpty(TableName))
            {
                // ha van sz�r�s t�bl�ra, kitakar�tjuk a mez�ket
                ClearSearch_Manual_Obj_type_Fields(search);
            }

            // Sz�r�s a t�pusra, ha sz�ks�ges
            switch (TableName)
            {
                case Constants.TableNames.EREC_KuldKuldemenyek:
                    search.Manual_Obj_type_Kuldemeny.Value = Constants.TableNames.EREC_KuldKuldemenyek;
                    search.Manual_Obj_type_Kuldemeny.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.EREC_UgyUgyiratok:
                    search.Manual_Obj_type_Ugyirat.Value = Constants.TableNames.EREC_UgyUgyiratok;
                    search.Manual_Obj_type_Ugyirat.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.EREC_PldIratPeldanyok:
                    search.Manual_Obj_type_IratPeldany.Value = Constants.TableNames.EREC_PldIratPeldanyok;
                    search.Manual_Obj_type_IratPeldany.Operator = Query.Operators.equals;
                    break;
                case Constants.TableNames.KRT_Mappak:
                    search.Manual_Obj_type_Dosszie.Value = Constants.TableNames.KRT_Mappak;
                    search.Manual_Obj_type_Dosszie.Operator = Query.Operators.equals;
                    break;
            }

            // Sz�r�s az iratt�rba k�ld�tt �llapotra, ha sz�ks�ges (�gyiratokn�l)
            switch (TableName)
            {
                case Constants.TableNames.EREC_UgyUgyiratok:
                case "":
                    if (search.Extended_EREC_UgyUgyiratokSearch == null)
                    {
                        search.Extended_EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
                    }
                    search.Extended_EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.Value = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
                    search.Extended_EREC_UgyUgyiratokSearch.Manual_Allapot_Filter.Operator = Query.Operators.notequals;
                    break;
            }

            // Sz�r�s saj�tra, a csoportra, vagy mindre
            switch (Filter)
            {
                case Constants.FilterType.IraKezbesitesiTetelekFilter.Sajat:
                    search.Csoport_Id_Cel.Value = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam);
                    search.Csoport_Id_Cel.Operator = Query.Operators.equals;
                    break;
                case Constants.FilterType.IraKezbesitesiTetelekFilter.Szervezet:
                    if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id) && FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol))
                    {
                        search.Csoport_Id_Cel.Value = ExecParam.FelhasznaloSzervezet_Id;
                        search.Csoport_Id_Cel.Operator = Query.Operators.equals;
                    }
                    break;
                case Constants.FilterType.IraKezbesitesiTetelekFilter.All:
                default:
                    // Sz�r�s azokra a rekordokra, ahol a felhaszn�l� benne van a C�mzett csoportban:
                    // TODO: m�g csak a saj�t csoportra m�k�dik:
                    search.Csoport_Id_Cel.Value = "'" + Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam) + "'";
                    if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id) && FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol))
                    {
                        search.Csoport_Id_Cel.Value += ",'" + ExecParam.FelhasznaloSzervezet_Id + "'";
                    }
                    search.Csoport_Id_Cel.Operator = Query.Operators.inner;
                    break;
            }

            string strDatum = Request.QueryString.Get(QueryStringVars_Date);

            if (!String.IsNullOrEmpty(strDatum))
            {
                DateTime dateFilter;
                if (DateTime.TryParse(strDatum, out dateFilter))
                {
                    DateTime dateFilterTonight = dateFilter.Date.AddHours(23.0).AddMinutes(59.0).AddSeconds(59.0);
                    // sz�r�s megadott d�tumra
                    search.AtvetelDat.Value = dateFilter.Date.ToString();
                    search.AtvetelDat.ValueTo = dateFilterTonight.ToString();
                    search.AtvetelDat.Operator = Query.Operators.between;
                }
                else
                {
                    // hiba
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.UIFormatErrorHeader, String.Format(Resources.Error.UISpecDateTimeFormatError, strDatum));
                    ErrorUpdatePanel.Update();

                    // sz�r�s aktu�lis d�tumra
                    search.AtvetelDat.Value = DateTime.Today.ToString();
                    search.AtvetelDat.ValueTo = "";
                    search.AtvetelDat.Operator = Query.Operators.greaterorequal;

                    strDatum = ""; // t�r�lj�k, hogy ne vezessen f�lre
                }
            }
            else
            {
                // sz�r�s aktu�lis d�tumra
                search.AtvetelDat.Value = DateTime.Today.ToString();
                search.AtvetelDat.ValueTo = "";
                search.AtvetelDat.Operator = Query.Operators.greaterorequal;
            }

            // ReadableWhere
            search.ReadableWhere = "Napi posta " + filteredBy + (String.IsNullOrEmpty(strDatum) ? "" : " (" + strDatum + ")");

            // vissza�r�s a sessionbe
            Search.SetSearchObject(Page, search);
        }
        else
        {
            search = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject(Page, new EREC_IraKezbesitesiTetelekSearch());
        }

        search.OrderBy = Search.GetOrderBy("IraKezbesitesiTetelekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

#if isBranched
            if (Mode == CommandName.AltalanosKereses)
            {
                // Lapoz�s be�ll�t�sa:
                UI.SetPaging(ExecParam, ListHeader1);
                Result res = service.AltalanosKeresesGetAllWithExtensionAndJogosultak(ExecParam, search, true);
                UI.GridViewFill(IraKezbesitesiTetelekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
            }
            else
            {
                // no paging (old version)
                Result res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);
                UI.GridViewFill(IraKezbesitesiTetelekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
            }
#else
        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);
        Result res = service.AltalanosKeresesGetAllWithExtensionAndJogosultak(ExecParam, search, true);
        UI.GridViewFill(IraKezbesitesiTetelekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
#endif
    }

    protected void IraKezbesitesiTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //T�pus ki�r�sa
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelTipus = (Label)e.Row.FindControl("labelTipus");
            if (labelTipus != null && labelTipus.Text.Trim() != String.Empty)
            {
                switch (labelTipus.Text.Trim())
                {
                    case Constants.TableNames.EREC_KuldKuldemenyek:
                        labelTipus.Text = kuldemeny;
                        break;
                    case Constants.TableNames.EREC_PldIratPeldanyok:
                        labelTipus.Text = iratpeldany;
                        break;
                    case Constants.TableNames.EREC_UgyUgyiratok:
                        labelTipus.Text = ugyirat;
                        break;
                    case Constants.TableNames.KRT_Mappak:
                        labelTipus.Text = dosszie;
                        break;
                }
            }
        }

        GridView_RowDataBound_SetVegyesInfo(e);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");
        UI.SetFeladatInfo(e, GetObjType(e), GetObjId(e));

        GridView_RowDataBound_SetKezbesitesiTetelAllapot(e);

        if (Mode == CommandName.AltalanosKereses || Mode == CommandName.Atveendok)
        {
            GridView_RowDataBound_SetKezbesitesiTetelInfo(e);
        }

        //int colIndex = 0;

        //if( Mode != CommandName.Atadandok && Mode != CommandName.Atveendok )
        //{
        //	foreach( DataControlField col in IraKezbesitesiTetelekGridView.Columns )
        //	{
        //		if( col.AccessibleHeaderText.Trim( ) == "UgyiratAllapota" 
        //			|| col.AccessibleHeaderText.Trim( ) == "IratpeldanyAllapota"
        //			|| col.AccessibleHeaderText.Trim( ) == "IratAllapota" )
        //		{
        //			colIndex = IraKezbesitesiTetelekGridView.Columns.IndexOf( col );
        //		}
        //	}

        //	IraKezbesitesiTetelekGridView.Columns.RemoveAt( colIndex );
        //}

        //if( Mode == CommandName.Atadandok && Mode == CommandName.Atveendok )
        //{
        //	IraKezbesitesiTetelekGridView.Columns[ colIndex ].Visible = true;
        //}
    }
    private string GetObjType(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string Obj_type = String.Empty;
                if (drw.Row.Table.Columns.Contains("Obj_type"))
                {
                    Obj_type = drw["Obj_type"].ToString();
                }

                return Obj_type;
            }
        }

        return String.Empty;
    }

    private string GetObjId(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string Obj_Id = String.Empty;
                if (drw.Row.Table.Columns.Contains("Obj_Id"))
                {
                    Obj_Id = drw["Obj_Id"].ToString();
                }

                return Obj_Id;
            }
        }

        return String.Empty;
    }

    private string GetKezbesitesiTetelAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string allapot = null;
                string allapot_nev = null;
                if (drw.Row.Table.Columns.Contains("Allapot"))
                {
                    allapot = drw["Allapot"].ToString();
                    if (!String.IsNullOrEmpty(allapot))
                    {
                        allapot_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.KEZBESITESITETEL_ALLAPOT.KodcsoportKod, allapot, Page);
                    }
                }

                return allapot_nev;
            }
        }

        return String.Empty;
    }

    private string GetUgyiratAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string ugyiratAllapota = null;
                string ugyiratAllapota_nev = null;
                string ugyiratTovabbitasAlattiAllapota = null;
                string irat_ugyiratAllapota = null;
                string irat_ugyiratTovabbitasAlattiAllapota = null;

                if (drw.Row.Table.Columns.Contains("UgyiratAllapota"))
                {
                    ugyiratAllapota = drw["UgyiratAllapota"].ToString();
                    ugyiratTovabbitasAlattiAllapota = drw["UgyiratTovabbitasAlattiAllapota"].ToString();
                    irat_ugyiratAllapota = drw["Irat_Ugyirat_Allapota"].ToString();
                    irat_ugyiratTovabbitasAlattiAllapota = drw["Irat_Ugyirat_TovabbitasAlattiAllapota"].ToString();

                    if (!String.IsNullOrEmpty(ugyiratAllapota))
                    {
                        ugyiratAllapota_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, ugyiratAllapota, Page);
                    }
                    else
                    {
                        ugyiratAllapota_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, irat_ugyiratAllapota, Page);
                    }

                    if (!String.IsNullOrEmpty(ugyiratTovabbitasAlattiAllapota))
                    {
                        ugyiratAllapota_nev += string.Concat("(", Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, ugyiratTovabbitasAlattiAllapota, Page), ")");
                    }
                    else if (!String.IsNullOrEmpty(irat_ugyiratTovabbitasAlattiAllapota))
                    {
                        ugyiratAllapota_nev += string.Concat("(", Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.UGYIRAT_ALLAPOT.KodcsoportKod, irat_ugyiratTovabbitasAlattiAllapota, Page), ")");
                    }
                }

                return ugyiratAllapota_nev;
            }
        }

        return String.Empty;

    }

    private string GetIratAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string iratAllapota = null;
                string iratAllapota_nev = null;

                if (drw.Row.Table.Columns.Contains("IratAllapota"))
                {
                    iratAllapota = drw["IratAllapota"].ToString();

                    if (!String.IsNullOrEmpty(iratAllapota))
                    {
                        iratAllapota_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.IRAT_ALLAPOT.KodcsoportKod, iratAllapota, Page);
                    }
                }



                return iratAllapota_nev;
            }
        }


        return String.Empty;


    }

    private string GetiratPeldanyAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string iratPeldanyAllapota = null;
                string iratPeldanyAllapota_nev = null;
                string iratPeldanyTovabbitasAlattiAllapota = null;

                if (drw.Row.Table.Columns.Contains("IratpeldanyAllapota"))
                {
                    iratPeldanyAllapota = drw["IratpeldanyAllapota"].ToString();
                    iratPeldanyTovabbitasAlattiAllapota = drw["IratpeldanyTovabbitasAlattiAllapota"].ToString();

                    if (!String.IsNullOrEmpty(iratPeldanyAllapota))
                    {
                        iratPeldanyAllapota_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.IRATPELDANY_ALLAPOT.KodcsoportKod, iratPeldanyAllapota, Page);
                    }

                    if (!String.IsNullOrEmpty(iratPeldanyTovabbitasAlattiAllapota))
                    {
                        iratPeldanyAllapota_nev += string.Concat("(", Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.IRATPELDANY_ALLAPOT.KodcsoportKod, iratPeldanyTovabbitasAlattiAllapota, Page), ")");
                        iratPeldanyAllapota_nev = iratPeldanyAllapota_nev.Replace("()", "");
                    }
                }



                return iratPeldanyAllapota_nev;
            }
        }


        return String.Empty;


    }

    private void GridView_RowDataBound_SetKezbesitesiTetelAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;


            // megjel�lj�k a k�zbes�t�si t�telekk sorait
            bool isKezbesitesiTetel = false;
            isKezbesitesiTetel = (drw.Row.Table.Columns.Contains("Id") && !String.IsNullOrEmpty(drw["Id"].ToString()));

            if (isKezbesitesiTetel)
            {
                string allapotnev = GetKezbesitesiTetelAllapot(e);
                Label lbAllapotNev = (Label)e.Row.FindControl("labelAllapotNev");
                if (lbAllapotNev != null)
                    lbAllapotNev.Text = allapotnev;
            }

            #region �gyirat �llapot, �gyirat tov�bb�t�s alatti �llapot
            string ugyiratallapotanev = GetUgyiratAllapot(e);
            Label lbUgyiratAllapotaNev = (Label)e.Row.FindControl("labelUgyiratAllapotaNev");
            if (lbUgyiratAllapotaNev != null)
                lbUgyiratAllapotaNev.Text = ugyiratallapotanev;
            #endregion

            #region Irat�llapot
            //IratAllapota
            string iratallapotanev = GetIratAllapot(e);
            Label lbiratAllapotaNev = (Label)e.Row.FindControl("labelIratAllapotaNev");
            if (lbiratAllapotaNev != null)
                lbiratAllapotaNev.Text = iratallapotanev;
            #endregion

            #region Iratp�ld�ny �llapot
            string iratpeldanyallapotanev = GetiratPeldanyAllapot(e);
            Label lbiratPeldanyAllapotaNev = (Label)e.Row.FindControl("labelIratPeldanyAllapotaNev");
            if (lbiratPeldanyAllapotaNev != null)
                lbiratPeldanyAllapotaNev.Text = iratpeldanyallapotanev;
            #endregion



        }
    }

    private void GridView_RowDataBound_SetKezbesitesiTetelInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string infoMessage = String.Empty;
            string Obj_type = GetObjType(e);
            string Obj_id = GetObjId(e);
            // megjel�lj�k a k�zbes�t�si t�telek, illetve az �tvehet� t�telek sorait
            bool isKezbesitesiTetel = false;
            CheckBox cbKezbesitesiTetel = (CheckBox)e.Row.FindControl(cbKezbesitesiTetelId);
            if (cbKezbesitesiTetel != null && drw.Row.Table.Columns.Contains("Id"))
            {
                cbKezbesitesiTetel.Checked = !String.IsNullOrEmpty(drw["Id"].ToString());
                isKezbesitesiTetel = cbKezbesitesiTetel.Checked;

                if (!isKezbesitesiTetel)
                {
                    // nem k�zbes�t�si t�tel
                    infoMessage = Resources.Error.ErrorCode_52850;
                }
            }


            if (Mode == CommandName.Atveendok || Mode == CommandName.AltalanosKereses)
            {
                #region �tvehet�
                CheckBox cbAtveheto = (CheckBox)e.Row.FindControl(cbAtvehetolId);
                if (cbAtveheto != null)
                {
                    bool bAtveheto = false;
                    if (isKezbesitesiTetel)
                    {
                        if (drw.Row.Table.Columns.Contains("Allapot") && drw.Row.Table.Columns.Contains("Id") && drw.Row.Table.Columns.Contains("Csoport_Id_Cel"))
                        {
                            bAtveheto = (!String.IsNullOrEmpty(drw["Id"].ToString()) && (drw["Allapot"].ToString() == KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott
                                 || drw["Allapot"].ToString() == KodTarak.KEZBESITESITETEL_ALLAPOT.Visszakuldott));

                            if (bAtveheto)
                            {
                                ExecParam ExecParam = UI.SetExecParamDefault(Page);
                                bool isJogosult = false;
                                string Csoport_Id_Cel = drw["Csoport_Id_Cel"].ToString();

                                if (!String.IsNullOrEmpty(Csoport_Id_Cel)
                                     && Csoport_Id_Cel == Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam))
                                {
                                    // nincs joga �tvenni
                                    switch (Obj_type)
                                    {
                                        case Constants.TableNames.EREC_UgyUgyiratok:
                                            isJogosult = FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel);
                                            if (!isJogosult)
                                            {
                                                infoMessage = Resources.Error.ErrorCode_52851;
                                            }
                                            break;
                                        case Constants.TableNames.EREC_KuldKuldemenyek:
                                            isJogosult = FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtvetel);
                                            if (!isJogosult)
                                            {
                                                infoMessage = Resources.Error.ErrorCode_52852;
                                            }
                                            break;
                                        case Constants.TableNames.EREC_PldIratPeldanyok:
                                            isJogosult = FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtvetel);
                                            if (!isJogosult)
                                            {
                                                infoMessage = Resources.Error.ErrorCode_52853;
                                            }
                                            break;
                                        case Constants.TableNames.KRT_Mappak:
                                            isJogosult = FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtvetel);
                                            if (!isJogosult)
                                            {
                                                infoMessage = Resources.Error.ErrorCode_52854;
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                    //isJogosult = true;
                                }
                                else if (!String.IsNullOrEmpty(ExecParam.FelhasznaloSzervezet_Id)
                                     && Csoport_Id_Cel == ExecParam.FelhasznaloSzervezet_Id)
                                {
                                    isJogosult = FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol);
                                    if (!isJogosult)
                                    {
                                        switch (Obj_type)
                                        {
                                            case Constants.TableNames.EREC_UgyUgyiratok:
                                                infoMessage = Resources.Error.ErrorCode_52236;
                                                break;
                                            case Constants.TableNames.EREC_KuldKuldemenyek:
                                                infoMessage = Resources.Error.ErrorCode_52394;
                                                break;
                                            case Constants.TableNames.EREC_PldIratPeldanyok:
                                                infoMessage = Resources.Error.ErrorCode_52296;
                                                break;
                                            case Constants.TableNames.KRT_Mappak:
                                                infoMessage = Resources.Error.ErrorCode_64033;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    // nem a felhaszn�l� a c�mzett
                                    switch (Obj_type)
                                    {
                                        case Constants.TableNames.EREC_UgyUgyiratok:
                                            infoMessage = Resources.Error.ErrorCode_52856;
                                            break;
                                        case Constants.TableNames.EREC_KuldKuldemenyek:
                                            infoMessage = Resources.Error.ErrorCode_52857;
                                            break;
                                        case Constants.TableNames.EREC_PldIratPeldanyok:
                                            infoMessage = Resources.Error.ErrorCode_52858;
                                            break;
                                        case Constants.TableNames.KRT_Mappak:
                                            infoMessage = Resources.Error.ErrorCode_52859;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                bAtveheto &= isJogosult;
                            }
                            else
                            {
                                // nem tov�bb�tott�k
                                infoMessage = Resources.Error.ErrorCode_52224;
                            }

                        }
                        cbAtveheto.Checked = bAtveheto;
                    }
                    else
                    {
                        // nem k�zbes�t�si t�tel
                        cbAtveheto.Checked = false;
                    }
                }
                #endregion �tvehet�
                // BUG_5993
                #region �tvehet��gyint�z�sre
                CheckBox cbAtvehetoUgyintezesre = (CheckBox)e.Row.FindControl(cbAtvehetoUgyintezesreId);
                if (cbAtvehetoUgyintezesre != null)
                {
                    bool bAtvehetoUgyintezesre = false;

                    if (isKezbesitesiTetel)
                    {
                        if (Obj_type == Constants.TableNames.EREC_UgyUgyiratok)
                        {
                            if (drw.Row.Table.Columns.Contains("Allapot") && drw.Row.Table.Columns.Contains("Obj_Id") && drw.Row.Table.Columns.Contains("Csoport_Id_Cel"))
                            {
                                string errorMessage;
                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Ugyiratok.Statusz ugyiratStatusz = GetUgyiratStatusz(drw["Obj_Id"].ToString(), out errorMessage);


                                if (String.IsNullOrEmpty(errorMessage) && (ugyiratStatusz != null))
                                {
                                    ErrorDetails errorDetail;
                                    execParam.Record_Id = ugyiratStatusz.Id; // == id

                                    bAtvehetoUgyintezesre = Ugyiratok.AtvehetoUgyintezesre(ugyiratStatusz, execParam, out errorDetail);

                                    if (!bAtvehetoUgyintezesre)
                                    //{
                                    //    ExecParam ExecParam = UI.SetExecParamDefault(Page);
                                    //    bool isJogosult = false;
                                    //    string Csoport_Id_Cel = drw["Csoport_Id_Cel"].ToString();

                                    //    if (!String.IsNullOrEmpty(Csoport_Id_Cel)
                                    //            && Csoport_Id_Cel == Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(ExecParam))
                                    //    {
                                    //        // nincs joga �tvenni
                                    //        isJogosult = FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel);
                                    //        if (!isJogosult)
                                    //        {
                                    //            infoMessage = Resources.Error.ErrorCode_52851;
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        // nem a felhaszn�l� a c�mzett
                                    //        infoMessage = Resources.Error.ErrorCode_52856;
                                    //    }
                                    //    bAtvehetoUgyintezesre &= isJogosult;
                                    //}
                                    //else
                                    {

                                        infoMessage = Resources.Error.ErrorCode_52161; // Nem vehet� �t
                                    }

                                }
                            }
                        }
                        else
                        {
                            bAtvehetoUgyintezesre = false;
                            infoMessage = Resources.Error.ErrorCode_52165; // Csak �gyirat vehet� �t �gyint�z�sre! 
                        }
                        cbAtvehetoUgyintezesre.Checked = bAtvehetoUgyintezesre;
                    }
                    else
                    {
                        // nem k�zbes�t�si t�tel
                        cbAtvehetoUgyintezesre.Checked = false;
                    }
                }
                #endregion �tvehet��gyint�z�sre
            }


            // Info Image �ll�t�sa, hogy mi�rt nem lehet �tvenni
            ImageButton infoImageButton = (ImageButton)e.Row.FindControl("InfoImage");
            if (infoImageButton != null && !String.IsNullOrEmpty(infoMessage))
            {
                // csak a m�velet v�grehajt�s ut�n jelen�tj�k meg
                //infoImageButton.Visible = true;
                infoImageButton.ToolTip = infoMessage;

                infoImageButton.OnClientClick = "imp_showPopup('" + infoMessage + "','"
                     + Obj_type + "','" + Obj_id + "','"
                     + Obj_type + "','" + Obj_id + "'); return false;";
            }
        }
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            return;
        }
        System.Data.DataRowView dataRowView = (System.Data.DataRowView)e.Row.DataItem;
        string csatolmanyCount = String.Empty;

        if (dataRowView.Row.Table.Columns.Contains("CsatolmanyCount") && dataRowView["CsatolmanyCount"] != null)
        {
            csatolmanyCount = dataRowView["CsatolmanyCount"].ToString();
        }
        ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
        if (string.IsNullOrEmpty(csatolmanyCount) || CsatolmanyImage == null)
        {
            if (CsatolmanyImage != null)
            {
                CsatolmanyImage.OnClientClick = "";
                CsatolmanyImage.Visible = false;
            }
            return;
        }

        CsatolmanyImage.OnClientClick = "";
        int count = 0;
        Int32.TryParse(csatolmanyCount, out count);
        if (count == 0)
        {
            CsatolmanyImage.Visible = false;
            return;
        }

        string orzoId = dataRowView.Row["FelhasznaloCsoport_Id_Orzo"] == null ? string.Empty : dataRowView.Row["FelhasznaloCsoport_Id_Orzo"].ToString();
        if (!Contentum.eRecord.BaseUtility.UIJogosultsagok.CanUserViewAttachements(Page, orzoId))
        {
            CsatolmanyImage.ToolTip = Resources.Form.UI_CsatolmanyImage_ToolTip_UserHasNoRight;
            CsatolmanyImage.Enabled = false;
            CsatolmanyImage.OnClientClick = "";
            return;
        }
        CsatolmanyImage.Visible = true;
        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);


        if (count == 1 && dataRowView.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(dataRowView["Dokumentum_Id"].ToString()))
        {
            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", dataRowView["Dokumentum_Id"]);
        }
        else
        {
            // set "link"
            string id = "";

            if (dataRowView.Row.Table.Columns.Contains("Obj_Id"))
            {
                id = dataRowView["Obj_Id"].ToString();
            }

            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            string Obj_type = "";
            if (dataRowView.Row.Table.Columns.Contains("Obj_type"))
            {
                Obj_type = dataRowView["Obj_type"].ToString();
            }

            string popupForm = "";
            string qs_popup = QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                      + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab;

            // csak �gyiratn�l haszn�ljuk
            string Azonosito = ""; // iktat�sz�m, a ReadableWhere fogja haszn�lni a dokumentumok list�j�n
            if (dataRowView.Row.Table.Columns.Contains("Azonosito"))
            {
                Azonosito = dataRowView["Azonosito"].ToString();
            }
            string qs_list = QueryStringVars.Startup + "=" + Constants.Startup.FromUgyirat
                 + "&" + QueryStringVars.ObjektumId + "=" + id
                 + "&" + QueryStringVars.Azonosito + "=" + System.Web.HttpUtility.UrlEncode(Azonosito);


            bool bIsNormalForm = true;

            if (Obj_type == Constants.TableNames.EREC_PldIratPeldanyok)
            {
                popupForm = "PldIratPeldanyokForm.aspx";
            }
            else if (Obj_type == Constants.TableNames.EREC_IraIratok)
            {
                // elvileg most itt nem lehetnek iratok
                popupForm = "IraIratokForm.aspx";
            }
            else if (Obj_type == Constants.TableNames.EREC_KuldKuldemenyek)
            {
                popupForm = "KuldKuldemenyekForm.aspx";
            }
            else if (Obj_type == Constants.TableNames.EREC_UgyUgyiratok)
            {
                popupForm = "UgyUgyiratokForm.aspx";
            }
            else if (Obj_type == Constants.TableNames.KRT_Mappak)
            {
                // elvileg itt most nem lehetnek dosszi�k
                popupForm = "DosszieForm.aspx";
            }

            if (string.IsNullOrEmpty(popupForm))
            {
                return;
            }
            if (bIsNormalForm)
            {
                // Modify->View �tir�ny�t�s jog szerint a formon, ez�rt itt nem vizsg�ljuk feleslegesen
                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick(popupForm, qs_popup
                     , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            }
            else
            {
                //// lista h�v�s
                string redirectUrl = popupForm + "?" + qs_list;
                CsatolmanyImage.OnClientClick = JavaScripts.GetRedirectPageScript(redirectUrl);
            }
        }
    }

    protected void IraKezbesitesiTetelekGridView_PreRender(object sender, EventArgs e)
    {
#if isBranched
            if (Mode == CommandName.AltalanosKereses)
            {
                UI.GridViewSetScrollable(ListHeader1.Scrollable, IraKezbesitesiTetelekCPE);
                ListHeader1.RefreshPagerLabel();
            }
            else
            {
                int prev_PageIndex = IraKezbesitesiTetelekGridView.PageIndex;

                IraKezbesitesiTetelekGridView.PageIndex = ListHeader1.PageIndex;
                ListHeader1.PageCount = IraKezbesitesiTetelekGridView.PageCount;

                if (prev_PageIndex != IraKezbesitesiTetelekGridView.PageIndex)
                {
                    //scroll �llapot�nak t�rl�se
                    JavaScripts.ResetScroll(Page, IraKezbesitesiTetelekCPE);
                    IraKezbesitesiTetelekGridViewBind();
                }
                else
                {
                    UI.GridViewSetScrollable(ListHeader1.Scrollable, IraKezbesitesiTetelekCPE);
                }

                ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IraKezbesitesiTetelekGridView);
            }
#else
        UI.GridViewSetScrollable(ListHeader1.Scrollable, IraKezbesitesiTetelekCPE);
        ListHeader1.RefreshPagerLabel();
#endif
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraKezbesitesiTetelekGridViewBind();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, IraKezbesitesiTetelekCPE);
        IraKezbesitesiTetelekGridViewBind();
    }

    protected void IraKezbesitesiTetelekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraKezbesitesiTetelekGridView, selectedRowNumber, "check");
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        //A kiv�lasztott sor lek�r�se
        GridViewRow row = IraKezbesitesiTetelekGridView.SelectedRow;

        //a megfelel� form bek�t�se
        string url = String.Empty;
        string tipusId = String.Empty;
        if (row != null)
        {
            Label labelTipus = (Label)row.FindControl("labelTipus");
            string obj_id = UI.GetGridViewColumnText(row, "Obj_Id");

            if (string.IsNullOrEmpty(obj_id))
                obj_id = UI.GetGridViewColumnValueOfSelectedRow(IraKezbesitesiTetelekGridView, "labelObj_Id");

            if (labelTipus != null && labelTipus.Text.Trim() != String.Empty && !String.IsNullOrEmpty(obj_id))
            {
                switch (labelTipus.Text.Trim())
                {
                    case kuldemeny:
                        url = "KuldKuldemenyekForm.aspx";
                        break;
                    case iratpeldany:
                        url = "PldIratPeldanyokForm.aspx";
                        break;
                    case ugyirat:
                        url = "UgyUgyiratokForm.aspx";
                        //isUgyirat = true;
                        break;
                    case dosszie:
                        url = "DosszieForm.aspx";
                        break;
                }

                tipusId = obj_id;
            }

            // mivel m�shol sem tiltjuk le a gombokat, itt sem, ink�bb hiba�zenet
            //ListHeader1.AtvetelUgyintezesreEnabled = (isUgyirat && FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetelUgyintezesre"));

            #region FunkctionKeys
            FunctionKeysManager fm = FunctionKeysManager.GetCurrent(Page);
            if (fm != null)
            {
                //string obj_id = UI.GetGridViewColumnText(row, "Obj_Id");
                string obj_type = UI.GetGridViewColumnText(row, "Obj_type");

                if (string.IsNullOrEmpty(obj_type))
                    obj_type = UI.GetGridViewColumnValueOfSelectedRow(IraKezbesitesiTetelekGridView, "labelTipus");

                if (!String.IsNullOrEmpty(obj_id) && !String.IsNullOrEmpty(obj_type))
                {
                    fm.ObjektumId = obj_id;
                    fm.ObjektumType = obj_type;
                    fm.IsFinal = true;
                }
            }
            #endregion FunkctionKeys
        }

        if (!String.IsNullOrEmpty(url))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick(url
                 , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + tipusId
                 , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraKezbesitesiTetelekUpdatePanel.ClientID);

            // �gyiratt�rk�p View m�dban
            ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick(url
                 , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + tipusId
                 + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                 , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IraKezbesitesiTetelekUpdatePanel.ClientID);
        }

        if (!String.IsNullOrEmpty(id))
        {
            ////A kiv�lasztott sor lek�r�se
            //GridViewRow row = IraKezbesitesiTetelekGridView.SelectedRow;
            //ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraKezbesitesiTetelekForm.aspx"
            //   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
            //   , Defaults.PopupWidth, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID);
            //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraKezbesitesiTetelekForm.aspx"
            //    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //    , Defaults.PopupWidth, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraKezbesitesiTetelek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                 , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                 , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID);

            //if (Mode == String.Empty)
            //{
            //    ListHeader1.SztornoOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_KezbesitesiTetelSztorno
            //        + "')) {} else {return false;}";
            //}

            if (Mode == CommandName.Atadandok || Mode == String.Empty)
            {
                //ListHeader1.AtadasOnClientClick = "if (confirm('"+Resources.Question.UIConfirmHeader_Atadas
                //    +"')) {} else {return false;}";

                ListHeader1.SztornoOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_KezbesitesiTetelSztorno
                     + "')) {} else {return false;}";

                if (row != null)
                {
                    Label labelAllapot = (Label)row.FindControl("labelAllapot");
                    if (labelAllapot != null)
                    {
                        string allapot = Server.UrlDecode(labelAllapot.Text).Trim();
                        if (allapot == KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett)
                        {
                            ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.UINemSztornozhatoKezbesitesiTetel + "');return false;";
                        }
                    }
                }
            }
            else if (Mode == CommandName.AltalanosKereses)
            {
                //CheckBox cbAtveheto = (CheckBox)row.FindControl(cbAtvehetolId);
                //if (cbAtveheto != null)
                //{
                //    if (cbAtveheto.Checked)
                //    {
                //        ListHeader1.AtvetelOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_Atvetel
                //            + "')) {} else {return false;}";
                //    }
                //    else
                //    {
                //        if (!String.IsNullOrEmpty(cbAtveheto.Text))
                //        {
                //            ListHeader1.AtvetelOnClientClick = String.Format("alert('{0}');return false;", cbAtveheto.Text);
                //        }
                //        else
                //        {
                //            ListHeader1.AtvetelOnClientClick = String.Format("alert('{0}');return false;", Resources.List.UI_NoExecutableItem);
                //        }
                //    }
                //}

                // �tv�tel �gyint�z�sre
                // BUG_5993
                //if (isUgyirat)
                //{
                //    SetUgyintezesreAtveheto(tipusId);
                //}
                //else
                //{
                //    ListHeader1.AtvetelUgyintezesreOnClientClick = "alert('" + Resources.Error.ErrorCode_52163
                //         + "'); return false;";
                //}

                ListHeader1.SztornoOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_KezbesitesiTetelSztorno
                     + "')) {} else {return false;}";

                if (row != null)
                {
                    Label labelAllapot = (Label)row.FindControl("labelAllapot");
                    if (labelAllapot != null)
                    {
                        string allapot = Server.UrlDecode(labelAllapot.Text).Trim();
                        if (allapot == KodTarak.KEZBESITESITETEL_ALLAPOT.Atvett)
                        {
                            ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.UINemSztornozhatoKezbesitesiTetel + "');return false;";
                        }

                        if (allapot == KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott)
                        {
                            VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
                        }
                        else
                        {
                            // A t�tel nem k�ldhet� vissza, mert a k�zbes�t�s �llapota nem megfelel�.
                            ListHeader1.VisszakuldesOnClientClick = String.Format("alert('{0}');return false;", Resources.Error.ErrorCode_53701);
                        }
                    }
                }
            }
            else if (Mode == CommandName.Atveendok)
            {
                //ListHeader1.AtvetelOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_Atvetel
                //    + "')) {} else {return false;}";

                // �tv�tel �gyint�z�sre
                // BUG_5993
                //if (isUgyirat)
                //{
                //    SetUgyintezesreAtveheto(tipusId);
                //}
                //else
                //{
                //    ListHeader1.AtvetelUgyintezesreOnClientClick = "alert('" + Resources.Error.ErrorCode_52163
                //         + "'); return false;";
                //}

                if (row != null)
                {
                    Label labelAllapot = (Label)row.FindControl("labelAllapot");
                    if (labelAllapot != null)
                    {
                        string allapot = Server.UrlDecode(labelAllapot.Text).Trim();
                        if (allapot == KodTarak.KEZBESITESITETEL_ALLAPOT.Atadott)
                        {
                            VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
                        }
                        else
                        {
                            // A t�tel nem k�ldhet� vissza, mert a k�zbes�t�s �llapota nem megfelel�.
                            ListHeader1.VisszakuldesOnClientClick = String.Format("alert('{0}');return false;", Resources.Error.ErrorCode_53701);
                        }

                    }
                }
            }
        }
        else if (row != null)
        {
            // iratkezel�si objektum, k�zbes�t�si t�tele nem sztorn�zhat�
            ListHeader1.SztornoOnClientClick = "alert('" + Resources.Error.UI_AltalanosKereses_NemKezbesitesiTetelNemSztornozhato + "');return false;";

            ListHeader1.VisszakuldesOnClientClick = "alert('" + Resources.Error.UINemVisszakuldhetoTetel + "');return false;";
        }

    }

    protected void IraKezbesitesiTetelekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraKezbesitesiTetelekGridViewBind();
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedIraKezbesitesiTetelek();
        //    IraKezbesitesiTetelekGridViewBind();
        //}

        #region LZS - Excel export
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<double> columnSizeList = new List<double>() { 5.0, 4.71, 13.29, 13.29, 13.86, 14.86, 10.57, 22.29, 11, 13.3, 18 };

            //Hide empty columns
            GridView tmpToExcel = IraKezbesitesiTetelekGridView;

            foreach (DataControlField columnItem in tmpToExcel.Columns)
            {
                if (string.IsNullOrEmpty(columnItem.HeaderText))
                {
                    columnItem.Visible = false;
                }
            }

            ex_Export.SaveGridView_ToExcel(exParam, tmpToExcel, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, columnSizeList, browser);
        }
        #endregion
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Atadas:
                Atadas();
                //IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.AtadasListaval:
                AtadasListaval();
                //IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.Atvetel:
                Atvetel();
                //IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.AtvetelListaval:
                AtvetelListaval();
                //IraKezbesitesiTetelekGridViewBind();
                break;
            // BUG_5993
            case CommandName.AtvetelUgyintezesre:
                AtvetelUgyintezesreTomeges();
                break;
            case CommandName.Visszakuldes:
                Visszakuldes();
                IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.Sztorno:
                Sztorno();
                IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.UgyiratpotloNyomtatasa:
                UgyiratpotloNyomtatasa();
                //IraKezbesitesiTetelekGridViewBind();
                break;

            case CommandName.Lock:
                LockSelectedIraKezbesitesiTetelekRecords();
                IraKezbesitesiTetelekGridViewBind();
                break;
            case CommandName.Unlock:
                UnlockSelectedIraKezbesitesiTetelekRecords();
                IraKezbesitesiTetelekGridViewBind();
                break;

            case CommandName.SendObjects:
                SendMailSelectedIraKezbesitesiTetelek();
                break;

            case CommandName.IratmozgasAdminisztralasa:
                //BLG1131
                IratmozgasAdminisztralasa();
                break;
            case CommandName.AtvevoJegyzekNyomtatasa:
                //BLG1130 �tvev� jegyz�k nyomtat�sa
                AtvevoJegyzekNyomtatasa();
                break;
            case CommandName.AtvevoJegyzekNyomtatasaTomeges:
                //BLG1130 T�meges �tvev� jegyz�k nyomtat�sa
                AtvevoJegyzekNyomtatasaTomeges();
                break;

        }
    }

    // �tad�s a kijel�lt k�zbes�t�si t�telre:
    private Result Atadas()
    {
        //String tetel_Id = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);
        //if (!String.IsNullOrEmpty(tetel_Id))
        //{
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtadas)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtadas)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtadas)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtadas))
        {
            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            String[] tetelIds = new String[selectedItemsList.Count];

            for (int i = 0; i < tetelIds.Length; i++)
            {
                tetelIds[i] = selectedItemsList[i];
            }

            Result result = null;
            if (tetelIds.Length > 0)
            {
                result = service.Atadas_Tomeges(execParam, tetelIds);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    MarkFailedRecord(result, service);
                    ErrorUpdatePanel.Update();
                }
                else
                {
                    IraKezbesitesiTetelekGridViewBind();

                    /*EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
					EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

					erec_IraKezbesitesiTetelekSearch.WhereByManual = " and EREC_IraKezbesitesiTetelek.KezbesitesFej_Id='" + result.Uid + "'";

					execParam = UI.SetExecParamDefault(Page, new ExecParam());

					Result tetel_result = service.GetAllWithExtension(execParam, erec_IraKezbesitesiTetelekSearch);

					string ugyirat_id = "";

					foreach (DataRow _row in tetel_result.Ds.Tables[0].Rows)
					{
						 ugyirat_id = ugyirat_id + "'" + _row["Obj_Id"].ToString() + "',";
					}

					ugyirat_id = ugyirat_id.TrimEnd(',');

					EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
					EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

					erec_UgyUgyiratokSearch.Id.Value = ugyirat_id;
					erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

					Result ugyresult = ugyservice.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

					Boolean kolcs = false;

					foreach (DataRow _row in ugyresult.Ds.Tables[0].Rows)
					{
						 if (_row["TovabbitasAlattAllapot"].ToString().Equals("56"))
						 {
							  kolcs = true;
						 }
					}

					if (FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa") && kolcs)
					{
						 string js1 = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratpotloLapPrintForm.aspx?fejId=" + result.Uid);
						 ScriptManager.RegisterStartupScript(Page, Page.GetType(), "uj_valami", js1, true);
					}*/
                }
            }

            return result;
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);

            return null;
        }
        //}
        //else
        //{
        //    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
        //        , Resources.Error.UINoSelectedRow);
        //    ErrorUpdatePanel.Update();
        //}
    }

    // �tad�s a kijel�lt k�zbes�t�si t�telre �s �tad�si lista nyomtat�sa:
    private void AtadasListaval()
    {
        ////String tetel_Id = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);
        ////if (!String.IsNullOrEmpty(tetel_Id))
        ////{
        //if (FunctionRights.GetFunkcioJog(Page, "UgyiratAtadas"))
        //{
        //    EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
        //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        //    List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
        //    String[] tetelIds = new String[selectedItemsList.Count];

        //    for (int i = 0; i < tetelIds.Length; i++)
        //    {
        //        tetelIds[i] = selectedItemsList[i];
        //    }

        //    Result result = service.Atadas_Tomeges(execParam, tetelIds);
        //    if (!String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //        MarkFailedRecord(result, service);
        //        ErrorUpdatePanel.Update();
        //    }
        //    else
        //    {
        //        IraKezbesitesiTetelekGridViewBind();

        Result result = Atadas();

        if (result != null && !result.IsError)
        {
            //blg 1868 string js = JavaScripts.SetOnClientClickWithTimeout("AtadoAtvevoListaPrintForm.aspx?fejId=" + result.Uid, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
            //string js = "javascript:window.open('AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + result.Uid + "')";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyExpedialas", js, true);
            string js = JavaScripts.SetOnClientClickWithTimeout("AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + result.Uid, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AtadoAtvevoJegyzekNyomtatas", js, true);
        }

        //        /*EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

        //        erec_IraKezbesitesiTetelekSearch.WhereByManual = " and EREC_IraKezbesitesiTetelek.KezbesitesFej_Id='" + result.Uid + "'";

        //        execParam = UI.SetExecParamDefault(Page, new ExecParam());

        //        Result tetel_result = service.GetAllWithExtension(execParam, erec_IraKezbesitesiTetelekSearch);

        //        string ugyirat_id = "";

        //        foreach (DataRow _row in tetel_result.Ds.Tables[0].Rows)
        //        {
        //            ugyirat_id = ugyirat_id + "'" + _row["Obj_Id"].ToString() + "',";
        //        }

        //        ugyirat_id = ugyirat_id.TrimEnd(',');

        //        EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        //        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        //        erec_UgyUgyiratokSearch.Id.Value = ugyirat_id;
        //        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

        //        Result ugyresult = ugyservice.GetAllWithExtension(execParam, erec_UgyUgyiratokSearch);

        //        Boolean kolcs = false;

        //        foreach (DataRow _row in ugyresult.Ds.Tables[0].Rows)
        //        {
        //            if (_row["TovabbitasAlattAllapot"].ToString().Equals("56"))
        //            {
        //                kolcs = true;
        //            }
        //        }

        //        if (FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa") && kolcs)
        //        {
        //            string js1 = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratpotloLapPrintForm.aspx?fejId=" + result.Uid);
        //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "uj_valami", js1, true);
        //        }*/
        //    }
        //}
        //else
        //{
        //    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        //}
        ////}
        ////else
        ////{
        ////    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
        ////        , Resources.Error.UINoSelectedRow);
        ////    ErrorUpdatePanel.Update();
        ////}
    }

    // �tv�tel a kijel�lt k�zbes�t�si t�telre:
    private Result Atvetel()
    {
        //String tetel_Id = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);
        //if (!String.IsNullOrEmpty(tetel_Id))
        //{
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtvetel)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtvetel)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtvetel)
             || FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol))
        {
            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            //String[] tetelIds = new String[selectedItemsList.Count];
            String[] tetelIds = null;
            //if (Mode == CommandName.AltalanosKereses)
            //{
            List<string> atvehetoTetelList = new List<string>();
            List<string> nemAtvehetoTetelList = new List<string>();
            for (int i = 0; i < selectedItemsList.Count; i++)
            {
                bool bAtveheto = UI.isGridViewCheckBoxChecked(IraKezbesitesiTetelekGridView, cbAtvehetolId, selectedItemsList[i]);
                if (bAtveheto)
                {
                    atvehetoTetelList.Add(selectedItemsList[i]);
                }
                else
                {
                    nemAtvehetoTetelList.Add(selectedItemsList[i]);
                }
            }
            tetelIds = atvehetoTetelList.ToArray();

            //}
            //else
            //{
            //    //for (int i = 0; i < tetelIds.Length; i++)
            //    //{
            //    //    tetelIds[i] = selectedItemsList[i];
            //    //}
            //tetelIds = selectedItemsList.ToArray();
            //}
            Result result = null;
            if (tetelIds.Length > 0)
            {
                result = service.Atvetel_Tomeges(execParam, tetelIds);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    MarkFailedRecord(result, service);
                    ErrorUpdatePanel.Update();
                }
                else
                {
                    IraKezbesitesiTetelekGridViewBind();
                }
            }
            MarkNotExecutableRecords(nemAtvehetoTetelList);

            return result;
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);

            return null;
        }
        //}
        //else
        //{
        //    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
        //        , Resources.Error.UINoSelectedRow);
        //    ErrorUpdatePanel.Update();
        //}


    }

    // BUG_5993
    private void AtvetelUgyintezesreTomeges()
    {
        if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetelUgyintezesre))
        {
            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            List<string> selectedObjIdList = UI.GetGridViewColumnValuesOfSelectedRows(IraKezbesitesiTetelekGridView, "labelObj_Id");
            String[] tetelIds = null;

            List<string> atvehetoTetelList = new List<string>();
            List<string> nemAtvehetoTetelList = new List<string>();
            for (int i = 0; i < selectedItemsList.Count; i++)
            {
                bool bAtvehetoUgyintezesre = UI.isGridViewCheckBoxChecked(IraKezbesitesiTetelekGridView, cbAtvehetoUgyintezesreId, selectedItemsList[i]);
                if (bAtvehetoUgyintezesre)
                {

                    atvehetoTetelList.Add(selectedObjIdList[i]);
                }
                else
                {
                    nemAtvehetoTetelList.Add(selectedItemsList[i]);
                }
            }
            if (nemAtvehetoTetelList.Count > 0)
            {
                ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader, Resources.Error.ErrorCode_52164);
                MarkNotExecutableRecords(nemAtvehetoTetelList);
                ErrorUpdatePanel.Update();
            }
            else
            {
                tetelIds = atvehetoTetelList.ToArray();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                string js = string.Empty;
                sb = new System.Text.StringBuilder();
                foreach (string s in tetelIds)
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session[Constants.SelectedUgyiratIds] = sb.ToString();

                js = JavaScripts.SetOnClientClickWithTimeout("AtvetelUgyintezesreTomegesForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtvetelUgyintezesre", js, true);

            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);

        }

    }
    // �tv�tel a kijel�lt k�zbes�t�si t�telre �s �tv�teli lista nyomtat�sa:
    private void AtvetelListaval()
    {
        ////String tetel_Id = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);
        ////if (!String.IsNullOrEmpty(tetel_Id))
        ////{
        //if (FunctionRights.GetFunkcioJog(Page, "UgyiratAtvetel"))
        //{
        //    EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
        //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        //    List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
        //    String[] tetelIds = null;
        //    if (Mode == CommandName.AltalanosKereses)
        //    {
        //        List<string> atvehetoTetelList = new List<string>();
        //        for (int i = 0; i < selectedItemsList.Count; i++)
        //        {
        //            bool bAtveheto = UI.isGridViewCheckBoxChecked(IraKezbesitesiTetelekGridView, cbAtvehetolId, selectedItemsList[i]);
        //            if (bAtveheto)
        //            {
        //                atvehetoTetelList.Add(selectedItemsList[i]);
        //            }
        //        }
        //        tetelIds = atvehetoTetelList.ToArray();
        //    }
        //    else
        //    {
        //        //for (int i = 0; i < tetelIds.Length; i++)
        //        //{
        //        //    tetelIds[i] = selectedItemsList[i];
        //        //}
        //        tetelIds = selectedItemsList.ToArray();
        //    }

        //    Result result = service.Atvetel_Tomeges(execParam, tetelIds);
        //    if (!String.IsNullOrEmpty(result.ErrorCode))
        //    {
        //        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
        //        MarkFailedRecord(result, service);
        //        ErrorUpdatePanel.Update();
        //    }
        //    else
        //        IraKezbesitesiTetelekGridViewBind();

        Result result = Atvetel();

        if (result != null && !result.IsError)
        {

            //blg 1868string js = JavaScripts.SetOnClientClickIFramePrintForm ( "AtadoAtvevoListaPrintForm.aspx?fejId=" + result.Uid );
            //string js = "javascript:window.open('AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + result.Uid + "')";
            //ScriptManager.RegisterStartupScript ( Page, Page.GetType ( ), "valami", js, true );
            string js = JavaScripts.SetOnClientClickWithTimeout("AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + result.Uid, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AtadoAtvevoJegyzekNyomtatas", js, true);
        }
        //}
        //else
        //{
        //    UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        //}
        //}
        //else
        //{
        //    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
        //        , Resources.Error.UINoSelectedRow);
        //    ErrorUpdatePanel.Update();
        //}
    }

    private void UgyiratpotloNyomtatasa()
    {
        List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
        string tetelIds = "";
        for (int i = 0; i < selectedItemsList.Count; i++)
        {
            tetelIds = tetelIds + "'" + selectedItemsList[i] + "',";
        }
        tetelIds = tetelIds.TrimEnd(',');

        EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
        EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch();

        EREC_UgyUgyiratokService ugyservice = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

        erec_IraKezbesitesiTetelekSearch.Id.Value = tetelIds;
        erec_IraKezbesitesiTetelekSearch.Id.Operator = Query.Operators.inner;

        Result iraresult = service.GetAll(execParam, erec_IraKezbesitesiTetelekSearch);

        tetelIds = "";

        foreach (DataRow _row in iraresult.Ds.Tables[0].Rows)
        {
            tetelIds = tetelIds + "'" + _row["Obj_Id"] + "',";
        }
        tetelIds = tetelIds.TrimEnd(',');

        erec_UgyUgyiratokSearch.Id.Value = tetelIds;
        erec_UgyUgyiratokSearch.Id.Operator = Query.Operators.inner;

        Result ugyresult = ugyservice.GetAll(execParam, erec_UgyUgyiratokSearch);

        tetelIds = "";

        foreach (DataRow _row in ugyresult.Ds.Tables[0].Rows)
        {
            if (_row["TovabbitasAlattAllapot"].ToString().Equals("56"))
            {
                tetelIds = tetelIds + "?" + _row["Id"] + "?,";
            }
        }

        string js1 = "";
        if (string.IsNullOrEmpty(tetelIds))
        {
            js1 = "alert('Nincs a kijel�lt t�telek k�z�tt kik�lcs�nz�tt!')";
        }
        else
        {
            tetelIds = tetelIds.TrimEnd(',');
            js1 = JavaScripts.SetOnClientClickIFramePrintForm("UgyiratpotloLapPrintForm.aspx?" + QueryStringVars.UgyiratId + "=" + tetelIds);
        }

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "uj_valami", js1, true);
    }

    // K�zbes�t�si t�tel visszak�ld�s
    // (az eredeti k�ld� lesz a c�mzett)
    private void Visszakuldes()
    {
        String tetel_Id = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);
        if (!String.IsNullOrEmpty(tetel_Id))
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtvetel)
                 || FunctionRights.GetFunkcioJog(Page, FunkcioKod_KuldemenyAtvetel)
                 || FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratPeldanyAtvetel)
                 || FunctionRights.GetFunkcioJog(Page, FunkcioKod_DosszieAtvetel)
                 //|| FunctionRights.GetFunkcioJog(Page, FunkcioKod_AtvetelSzervezettol)
                 )
            {
                EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.Visszakuldes(execParam, tetel_Id, VisszakuldesPopup.VisszakuldesIndoka);
                if (result.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
                 , Resources.Error.UINoSelectedRow);
            ErrorUpdatePanel.Update();
        }
    }

    // K�zbes�t�si t�tel sztorn�z�sa
    // (Ezzel a felel�s vissza�ll az el�z� felel�sre)
    private void Sztorno()
    {
        String tetel_Id = UI.GetGridViewSelectedRecordId(IraKezbesitesiTetelekGridView);
        if (!String.IsNullOrEmpty(tetel_Id))
        {
            if (FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetelSztorno"))
            {
                EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service.Sztorno(execParam, tetel_Id);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    ErrorUpdatePanel.Update();
                }
            }
            else
            {
                UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, Resources.Error.DefaultErrorHeader
                 , Resources.Error.UINoSelectedRow);
            ErrorUpdatePanel.Update();
        }
    }


    private void LockSelectedIraKezbesitesiTetelekRecords()
    {
        if (Mode == CommandName.AltalanosKereses)
        {
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            List<string> kezbesitesiTetelList = new List<string>();
            for (int i = 0; i < selectedItemsList.Count; i++)
            {
                bool bKezbesitesiTetel = UI.isGridViewCheckBoxChecked(IraKezbesitesiTetelekGridView, cbKezbesitesiTetelId, selectedItemsList[i]);
                if (bKezbesitesiTetel)
                {
                    kezbesitesiTetelList.Add(selectedItemsList[i]);
                }
            }

            if (kezbesitesiTetelList.Count > 0)
            {
                LockManager.LockSelectedRecords(kezbesitesiTetelList, "EREC_IraKezbesitesiTetelek"
                     , "KezbesitesiTetelLock", "KezbesitesiTetelForceLock"
                      , Page, EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            LockManager.LockSelectedGridViewRecords(IraKezbesitesiTetelekGridView, "EREC_IraKezbesitesiTetelek"
                 , "KezbesitesiTetelLock", "KezbesitesiTetelForceLock"
                  , Page, EErrorPanel1, ErrorUpdatePanel);
        }
    }

    private void UnlockSelectedIraKezbesitesiTetelekRecords()
    {
        if (Mode == CommandName.AltalanosKereses)
        {
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            List<string> kezbesitesiTetelList = new List<string>();
            for (int i = 0; i < selectedItemsList.Count; i++)
            {
                bool bKezbesitesiTetel = UI.isGridViewCheckBoxChecked(IraKezbesitesiTetelekGridView, cbKezbesitesiTetelId, selectedItemsList[i]);
                if (bKezbesitesiTetel)
                {
                    kezbesitesiTetelList.Add(selectedItemsList[i]);
                }
            }

            if (kezbesitesiTetelList.Count > 0)
            {
                LockManager.UnlockSelectedRecords(kezbesitesiTetelList, "EREC_IraKezbesitesiTetelek"
                     , "KezbesitesiTetelLock", "KezbesitesiTetelForceLock"
                      , Page, EErrorPanel1, ErrorUpdatePanel);
            }
        }
        else
        {
            LockManager.UnlockSelectedGridViewRecords(IraKezbesitesiTetelekGridView, "EREC_IraKezbesitesiTetelek"
                 , "KezbesitesiTetelLock", "KezbesitesiTetelForceLock"
                 , Page, EErrorPanel1, ErrorUpdatePanel);
        }
    }

    ///// <summary>
    ///// T�rli a IraKezbesitesiTetelekGridView -ban kijel�lt elemeket
    ///// </summary>
    //private void deleteSelectedIraKezbesitesiTetelek()
    //{
    //    if (FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetelInvalidate"))
    //    {
    //        List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
    //        Contentum.eRecord.Service.EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
    //        ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
    //        for (int i = 0; i < deletableItemsList.Count; i++)
    //        {
    //            execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
    //            execParams[i].Record_Id = deletableItemsList[i];
    //        }
    //        Result result = service.MultiInvalidate(execParams);
    //        if (!String.IsNullOrEmpty(result.ErrorCode))
    //        {
    //            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
    //            ErrorUpdatePanel.Update();
    //        }
    //    }
    //    else
    //    {
    //        UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
    //    }
    //}

    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraKezbesitesiTetelek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            List<string> kezbesitesiTetelList = new List<string>();
            List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
            if (Mode == CommandName.AltalanosKereses)
            {
                for (int i = 0; i < selectedItemsList.Count; i++)
                {
                    bool bKezbesitesiTetel = UI.isGridViewCheckBoxChecked(IraKezbesitesiTetelekGridView, cbKezbesitesiTetelId, selectedItemsList[i]);
                    if (bKezbesitesiTetel)
                    {
                        kezbesitesiTetelList.Add(selectedItemsList[i]);
                    }
                }
            }
            else
            {
                kezbesitesiTetelList = selectedItemsList;
            }

            Notify.SendSelectedItemsByEmail(Page, kezbesitesiTetelList
                 , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraKezbesitesiTetelek");

        }
    }

    private string getSelectedIdsString()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        foreach (string s in ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel))
            sb.Append(s + ",");
        sb.Remove(sb.Length - 1, 1);

        return sb.ToString();
    }

    #region 1130 �tvev� jegyz�k / T�meges �tvev� jegyz�k nyomtat�sa
    private void AtvevoJegyzekNyomtatasa()
    {
        EREC_IraKezbesitesiTetelekService tetel_service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = IraKezbesitesiTetelekGridView.SelectedValue.ToString();

        Result tetelResult = tetel_service.Get(execParam);

        if (String.IsNullOrEmpty(tetelResult.ErrorCode))
        {
            EREC_IraKezbesitesiTetelek erec_KezbesitesiTetelek = (EREC_IraKezbesitesiTetelek)tetelResult.Record;

            if (!string.IsNullOrEmpty(erec_KezbesitesiTetelek.AtveteliFej_Id))
            {
                //string js = "javascript:window.open('AtadoAtvevoListaPrintFormSSRS.aspx?fejId=" + erec_KezbesitesiTetelek.AtveteliFej_Id + "')";
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratpeldanyExpedialas", js, true);
                Session["fejId"] = erec_KezbesitesiTetelek.AtveteliFej_Id;
                string js = JavaScripts.SetOnClientClickWithTimeout("AtadoAtvevoListaPrintFormSSRS.aspxfejId=" + erec_KezbesitesiTetelek.AtveteliFej_Id, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AtadoAtvevoJegyzekNyomtatas", js, true);
            }
            else
            {
                ResultError.DisplayNoIdParamError(EErrorPanel1);
            }
        }
        else
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, tetelResult);
        }

    }

    private void AtvevoJegyzekNyomtatasaTomeges()
    {
        Session["selectedIraKezbesitesiTetelekIds"] = getSelectedIdsString();
        string js = JavaScripts.SetOnClientClickWithTimeout("AtvevoJegyzekNyomtatasaForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedAtvevoJegyzekNyomtatasa", js, true);
    }
    #endregion

    #region 1131 Ut�lagosan administr�lt bels� iratmozg�sok
    private void IratmozgasAdminisztralasa()
    {
        Session["selectedItemIds"] = getSelectedIdsString();
        string js = JavaScripts.SetOnClientClickWithTimeout("IratmozgasAdminisztralasaForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, IraKezbesitesiTetelekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIratmozgasAdminisztralas", js, true);
    }

    #endregion
    protected void IraKezbesitesiTetelekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraKezbesitesiTetelekGridViewBind(e.SortExpression, UI.GetSortToGridView("IraKezbesitesiTetelekGridView", ViewState, e.SortExpression));
    }

    /*protected string Print()
	{
		 List<string> selectedItemsList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, EErrorPanel1, ErrorUpdatePanel);
		 String[] tetelIds = new String[selectedItemsList.Count];

		 for (int i = 0; i < tetelIds.Length; i++)
		 {
			  tetelIds[i] = selectedItemsList[i];
		 }

		 string qs_tetelIds = "";

		 for (int i = 0; i < tetelIds.Length; i++)
		 {
			  qs_tetelIds = qs_tetelIds + "$" + tetelIds[i] + "$,";
		 }

		 qs_tetelIds = qs_tetelIds.Remove(qs_tetelIds.Length-1);

		 //Ez kell az �j ablakhoz  
		 string js = "window.open('AtadoListaPrintForm.aspx?Id=" + qs_tetelIds + "')";
		 ScriptManager.RegisterStartupScript(Page, Page.GetType(), "valami", js, true);

		 return "";
	}*/

    private int MarkFailedRecord(Result result, EREC_IraKezbesitesiTetelekService service)
    {
        if (result.Ds == null || result.Ds.Tables.Count == 0 || result.Ds.Tables[0].Rows.Count == 0)
            return 0;

        int recordNumber = 0;

        //string ids = "";
        //for (int i=0; i<IraKezbesitesiTetelekGridView.Rows.Count; i++)
        //{
        //    ids += "'" + IraKezbesitesiTetelekGridView.DataKeys[i].Value.ToString() + "',";
        //}

        //string objIds = "";
        //for (int i=0; i<result.Ds.Tables[0].Rows.Count; i++)
        //{
        //    objIds += "'" + result.Ds.Tables[0].Rows[i]["Id"].ToString() + "',";
        //}

        //ids = ids.TrimEnd(',');
        //objIds = objIds.TrimEnd(',');

        System.Text.StringBuilder sbIds = new System.Text.StringBuilder();
        for (int i = 0; i < IraKezbesitesiTetelekGridView.Rows.Count; i++)
        {
            sbIds.AppendFormat("'{0}',", IraKezbesitesiTetelekGridView.DataKeys[i].Value.ToString());
        }

        System.Text.StringBuilder sbObjIds = new System.Text.StringBuilder();
        for (int i = 0; i < result.Ds.Tables[0].Rows.Count; i++)
        {
            sbObjIds.AppendFormat("'{0}',", result.Ds.Tables[0].Rows[i]["Id"].ToString());
        }

        string ids = sbIds.ToString().TrimEnd(',');
        string objIds = sbObjIds.ToString().TrimEnd(',');

        if (service == null)
            service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

        EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch();
        search.Id.Value = ids;
        search.Id.Operator = Query.Operators.inner;

        search.Obj_Id.Value = objIds;
        search.Obj_Id.Operator = Query.Operators.inner;

        Result kezbTetelekGetAllResult = service.GetAll(UI.SetExecParamDefault(Page, new ExecParam()), search);
        if (string.IsNullOrEmpty(kezbTetelekGetAllResult.ErrorCode) && kezbTetelekGetAllResult.Ds.Tables[0].Rows.Count > 0)
        {
            foreach (GridViewRow r in IraKezbesitesiTetelekGridView.Rows)
            {
                for (int i = 0; i < kezbTetelekGetAllResult.Ds.Tables[0].Rows.Count; i++)
                {
                    if (IraKezbesitesiTetelekGridView.DataKeys[r.RowIndex].Value.ToString() == kezbTetelekGetAllResult.Ds.Tables[0].Rows[i]["Id"].ToString())
                    {
                        CheckBox checkBox = (CheckBox)r.FindControl("check");
                        r.BackColor = System.Drawing.Color.Red;
                        if (checkBox != null)
                        {
                            checkBox.Checked = false;
                        }
                        // Info Image megjelen�t�se
                        ImageButton infoImageButton = (ImageButton)r.FindControl("InfoImage");
                        if (infoImageButton != null && !String.IsNullOrEmpty(infoImageButton.ToolTip))
                        {
                            infoImageButton.Visible = true;
                        }

                        recordNumber++;
                        break;
                    }
                    else
                    {
                        r.BackColor = System.Drawing.Color.White;
                        // Info Image elrejt�se, ha kor�bban meg volt jelen�tve
                        ImageButton infoImageButton = (ImageButton)r.FindControl("InfoImage");
                        if (infoImageButton != null)
                        {
                            infoImageButton.Visible = false;
                        }
                    }
                }
            }

        }

        return recordNumber;
    }

    private int MarkNotExecutableRecords(List<string> lstRecordIds)
    {
        int recordNumber = 0;

        foreach (GridViewRow r in IraKezbesitesiTetelekGridView.Rows)
        {
            foreach (string id in lstRecordIds)
            {
                if (IraKezbesitesiTetelekGridView.DataKeys[r.RowIndex].Value.ToString() == id)
                {
                    CheckBox checkBox = (CheckBox)r.FindControl("check");
                    if (checkBox != null)
                    {
                        checkBox.Checked = false;
                    }
                    r.BackColor = System.Drawing.Color.Red;


                    // Info Image megjelen�t�se
                    ImageButton infoImageButton = (ImageButton)r.FindControl("InfoImage");
                    if (infoImageButton != null && !String.IsNullOrEmpty(infoImageButton.ToolTip))
                    {
                        infoImageButton.Visible = true;
                    }

                    recordNumber++;
                    break;
                }
                else
                {
                    r.BackColor = System.Drawing.Color.White;
                    // Info Image elrejt�se, ha kor�bban meg volt jelen�tve
                    ImageButton infoImageButton = (ImageButton)r.FindControl("InfoImage");
                    if (infoImageButton != null)
                    {
                        infoImageButton.Visible = false;
                    }
                }
            }
        }
        return recordNumber;
    }
    #endregion


}
