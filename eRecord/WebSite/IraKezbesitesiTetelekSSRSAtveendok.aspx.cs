﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;

public partial class IraKezbesitesiTetelekSSRSAtveendok : Contentum.eUtility.UI.PageBase
{
    private string vis = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        vis = Request.QueryString.Get(QueryStringVars.Filter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                Contentum.eRecord.Service.EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraKezbesitesiTetelekSearch search = null;

                search = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new EREC_IraKezbesitesiTetelekSearch(true), Constants.CustomSearchObjectSessionNames.AtveendokSearch);

                // Szűrés csak az átveendőkre:
                search.Allapot.Value = Constants.KezbesitesiTetel_Allapot.Atadott;
                search.Allapot.Operator = Query.Operators.equals;

                Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.AtveendokSearch);

                search.TopRow = 0;

                ExecParam.Fake = true;

                Result result = service.AltalanosKeresesGetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by LetrehozasIdo DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_KuldKuldemenyek":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_UgyUgyiratok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_UgyUgyiratok"));
                            break;
                        case "Where_PldIratPeldanyok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_PldIratPeldanyok"));
                            break;
                        case "isObjectMode":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@isObjectMode"));
                            break;
                        case "Where_Mappak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Mappak"));
                            break;
                        case "KezbesitesiTetelekSearchMode":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@KezbesitesiTetelekSearchMode"));
                            break;
                        case "CsnyVisibility":
                            if (vis.Substring(3, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "FVisibility":
                            if (vis.Substring(4, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AtadoVisibility":
                            if (vis.Substring(5, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "CimzettVisibility":
                            if (vis.Substring(6, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AzonositoVisibility":
                            if (vis.Substring(7, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "TargyVisibility":
                            if (vis.Substring(8, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "VonalkodVisibility":
                            if (vis.Substring(9, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "TipusVisibility":
                            if (vis.Substring(10, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AtirDatumaVisibility":
                            if (vis.Substring(13, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AtadasDatumaVisibility":
                            if (vis.Substring(14, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AtvetelDatumaVisibility":
                            if (vis.Substring(15, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AtvevoVisibility":
                            if (vis.Substring(16, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AllapotVisibility":
                            if (vis.Substring(19, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "MegjegyzesVisibility":
                            if (vis.Substring(20, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ZarolasVisibility":
                            if (vis.Substring(21, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
