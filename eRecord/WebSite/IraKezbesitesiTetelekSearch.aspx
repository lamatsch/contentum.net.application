<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IraKezbesitesiTetelekSearch.aspx.cs" Inherits="IraKezbesitesiTetelek" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc6" %>
<%@ Register Src="eRecordComponent/IktatokonyvTextBox.ascx" TagName="IktatokonyvTextBox"
    TagPrefix="uc5" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc3" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc17" %>
<%@ Register Src="eRecordComponent/AlszamIntervallum_SearchFormControl.ascx" TagName="AlszamIntervallum_SearchFormControl"
    TagPrefix="uc15" %>
<%@ Register Src="eRecordComponent/FoszamIntervallum_SearchFormControl.ascx" TagName="FoszamIntervallum_SearchFormControl"
    TagPrefix="uc16" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc13" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc14" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/VonalKodListBox.ascx" TagName="VonalKodListBox"
    TagPrefix="uc4" %>
    
<%@ Register Src="eRecordComponent/IratPeldanySimpleSearchComponent.ascx" TagName="IratPeldanySimpleSearchComponent"
    TagPrefix="ssc" %>
<%@ Register Src="eRecordComponent/UgyiratSimpleSearchComponent.ascx" TagName="UgyiratSimpleSearchComponent"
    TagPrefix="ssc" %>
<%@ Register Src="eRecordComponent/KuldemenySimpleSearchComponent.ascx" TagName="KuldemenySimpleSearchComponent"
    TagPrefix="ssc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style>
.mrUrlapCaption_short 
{
    font-weight: bold;
    padding-right: 6px;
    padding-left: 6px;
    vertical-align: middle;
    text-align: right;
    color: #335674;
    width: 70px;
    min-width: 70px;
    white-space: nowrap;
}

.mrUrlapCaptionBal_nowidth 
{
    font-weight: bold;
    padding-right: 6px;
    vertical-align: middle;
    text-align: left;
    color: #335674;
    width: 0px;
}
   
body, html
{
    overflow : auto;
}
</style>
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="99%">
                        <tr class="urlapsor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth" colspan="4">
                                <asp:RadioButtonList ID="rblObjectMode" runat="server" RepeatDirection="Horizontal" Visible="false">
<%--                                    <asp:ListItem Value="KT" Text="K�zbes�t�si t�telek" Selected="true" />
                                    <asp:ListItem Value="O" Text="Iratkezel�si objektumok" />--%>
                                </asp:RadioButtonList>
                            </td>
                            <td class="mrUrlapCaption_short">
                            </td>
                            <td class="mrUrlapMezo">
                            </td>
                        </tr>
                        <tr class="urlapsor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                            <asp:Label ID="Label2" runat="server" Text="Vonalk�dok:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc4:VonalKodListBox runat="server" ID="VonalKodListBox1" MaxElementsNumber="500"
                                                Rows="3" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                        </td>
                                        <td class="mrUrlapMezo">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1">             
                            </td>
                            <td colspan="2" >
                                <hr />
                            </td>
                        </tr>
                        <%--BUG_4233--%>
                        <tr class="urlapSor_kicsi" id="Nyilvantartassz_row" runat="server">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                
                                <table>
                                    <%-- INNER TABLE --%>
                                     <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td  class="mrUrlapCaption_short">
                                             <asp:Label ID="Label1" runat="server" Text="Nyilv�ntartart�si sz�m:"></asp:Label>
                                        </td>
                                        <td colspan="4" class="mrUrlapMezo">
                                            <asp:TextBox ID="NyilvantartSzTextBox" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                     </table>
                                <%-- /INNER TABLE --%>
                                </td>
                                </tr>
                        <tr class="urlapSor_kicsi" >
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">

                                  <table>
                                      
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label_Atado1" runat="server" Text="�tad�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc13:FelhasznaloCsoportTextBox ID="Atado_FelhasznaloCsoportTextBox" runat="server"
                                                Validate="false" SearchMode="true" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label6" runat="server" Text="�tad�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc14:DatumIntervallum_SearchCalendarControl ID="AtadasDat_DatumIntervallum_SearchCalendarControl"
                                                runat="server" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label_Cimzett1" runat="server" Text="C�mzett:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc6:CsoportTextBox ID="CsoportId_Cel_CsoportTextBox" runat="server" Validate="false"
                                                SearchMode="true" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label8" runat="server" Text="�tir�ny�t�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc14:DatumIntervallum_SearchCalendarControl ID="LetrehozasIdo_DatumIntervallum_SearchCalendarControl"
                                                runat="server" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label9" runat="server" Text="�tvev�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc13:FelhasznaloCsoportTextBox ID="Atvevo_FelhasznaloCsoportTextBox" runat="server"
                                                Validate="false" SearchMode="true" />
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label10" runat="server" Text="�tv�tel d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <uc14:DatumIntervallum_SearchCalendarControl ID="AtvetelDat_DatumIntervallum_SearchCalendarControl"
                                                runat="server" Validate="false" ValidateDateFormat="true" />
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label_Atado2" runat="server" Text="�tad�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:RadioButton ID="Ch_Atado_Sajat" runat="server" Text="Saj�t szem�ly" GroupName="Ch_Atado" />
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_Atado_Sajat_Szervezet" runat="server" Text="Saj�t szervezet"
                                                GroupName="Ch_Atado" />
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_Atado_Sajat_Szervezet_Osszesen" runat="server"
                                                Text="Saj�t szervezet �sszesen" GroupName="Ch_Atado" Checked="true" />&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label_Cimzett2" runat="server" Text="C�mzett:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:RadioButton ID="Ch_Cimzett_Sajat" runat="server" Text="Saj�t szem�ly" GroupName="Ch_Cimzett" />
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_Cimzett_Sajat_Szervezet" runat="server" Text="Saj�t szervezet"
                                                GroupName="Ch_Cimzett" />
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_Cimzett_Sajat_Szervezet_Osszesen" runat="server"
                                                Text="Saj�t szervezet �sszesen" GroupName="Ch_Cimzett" Checked="true" />&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                            <asp:Label ID="Label20" runat="server" Text="�tvev�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:RadioButton ID="Ch_Atvevo_Sajat" runat="server" Text="Saj�t szem�ly" GroupName="Ch_Atvevo" />
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_Atvevo_Sajat_Szervezet" runat="server" Text="Saj�t szervezet"
                                                GroupName="Ch_Atvevo" />
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_Atvevo_Sajat_Szervezet_Osszesen" runat="server"
                                                Text="Saj�t szervezet �sszesen" GroupName="Ch_Atvevo" Checked="true" />&nbsp;
                                            &nbsp; &nbsp;<asp:RadioButton ID="Ch_AtNemVett" runat="server"
                                                Text="�t nem vett" GroupName="Ch_Atvevo" />&nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
<%--                        <tr class="urlapSor_kicsi" id="tr_CheckBoxes" >
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaptionBal_nowidth">
                                <table> --%>
                                    <%-- INNER TABLE --%>
                                    <%-- <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_short">
                                        </td>
                                        <td class="mrUrlapMezo" colspan="3">
                                            <asp:CheckBox ID="cbAtNemVettek" runat="server" Text="�t nem vettek" />
                                        </td>
                                    </tr>
                                </table> --%>
                                <%-- /INNER TABLE --%>
                            <%-- </td>
                        </tr>--%>
                        <tr>
                            <td colspan="1">
                            </td>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" id="tr_KezbesitesiTetelekSearchMode" runat="server" visible="false">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td>
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal">
                                            <asp:Label ID="labelKezbesitesiTetelekSearchMode" runat="server" Text="Objektum k�zbes�t�si t�telei:" />
                                        </td> 
                                        <td>
                                            <asp:RadioButtonList ID="rblKezbesitesiTetelekSearchMode" runat="server"
                                                    RepeatDirection="Horizontal" Visible="true">
<%--                                                <asp:ListItem Value="LastValidIfExists" Text="Legut�bbi" Selected="true" />
                                                <asp:ListItem Value="AllValidIfExists" Text="Mind" />
                                                <asp:ListItem Value="None" Text="Csak objektum" />
                                                <asp:ListItem Value="ObjectHasNoValidItem" Text="K�zbes�t�si t�tel n�lk�li objektumok" />
--%>                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
<%--                        <tr>
                            <td colspan="1">
                            </td>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>--%>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td>
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr>
                                        <td class="mrUrlapCaptionBal_nowidth">
                                        </td>
                                        <td class="mrUrlapCaption_nowidth" colspan="2">
                                            <asp:CheckBox ID="Ch_Kuldemeny" runat="server" Text="K�ldem�ny" />&nbsp;&nbsp;
                                            <asp:CheckBox ID="Ch_Ugyirat" runat="server" Text="�gyirat" />&nbsp;&nbsp;
                                            <asp:CheckBox ID="Ch_Iratpeldany" runat="server" Text="Iratp�ld�ny" />&nbsp;&nbsp;
                                            <asp:CheckBox ID="Ch_Dosszie" runat="server" Text="Dosszi�" />&nbsp;&nbsp;&nbsp;&nbsp;
<%--                                            <br />
                                            <br />--%>
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi" id="tr_TargyFTS" runat="server" visible="false">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td>
                                <%-- INNER TABLE --%>
                                <table cellspacing="0" cellpadding="0" width="90%">
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelAltalanosFTS" runat="server" CssClass="mrUrlapCaption" 
                                             Text="�gy/irat/k�ld.t�rgy, �gyind�t�, bek�ld� tartalmaz:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezo">
                                            <asp:TextBox ID="TextBoxAltalanosFTS" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                                        </td>                              
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                                <asp:CheckBox ID="cbKuldemenySimpleSearch" runat="server" Text="" ToolTip="Egyszer� bevitel" />
                            </td>
                            <td>
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr style="text-align:left;">
                                        <td class="mrUrlapCaptionBal">
                                            <asp:Label ID="labelKuldemenySearch" runat="server" Text="K�ldem�ny:"></asp:Label>
                                        </td>
                                        <td>
                                            <ssc:KuldemenySimpleSearchComponent ID="kuldemenySimpleSearchComponent" runat="server" />
                                            <table id="ComplexSearchKuldemeny" runat="server">
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelKuld_Erkeztetokonyv_Ev" runat="server" Text="�v:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc17:EvIntervallum_SearchFormControl ID="Kuld_EvIntervallum_SearchFormControl" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Kuld_Label2" runat="server" Text="�rkeztet�k�nyv:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc7:IraIktatoKonyvekDropDownList ID="Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList"
                                                            Mode="ErkeztetoKonyvek" EvIntervallum_SearchFormControlId="Kuld_EvIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Kuld_Label16" runat="server" Text="�rkeztet�si azonos�t�:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc3:SzamIntervallum_SearchFormControl ID="Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 100%" />
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1">
                            </td>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                                <asp:CheckBox ID="cbUgyiratSimpleSearch" runat="server" Text="" ToolTip="Egyszer� bevitel" />
                            </td>
                            <td>
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr style="text-align:left;">
                                        <td class="mrUrlapCaptionBal">
                                            <asp:Label ID="labelUgyiratSearch" runat="server" Text="�gyirat:"></asp:Label>
                                        </td>
                                        <td>
                                            <ssc:UgyiratSimpleSearchComponent ID="ugyiratSimpleSearchComponent" runat="server" />
                                            <table id="ComplexSearchUgyirat" runat="server">
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelUgyirat_IktKonyv_Ev" runat="server" Text="�v:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc17:EvIntervallum_SearchFormControl ID="Ugyirat_EvIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Ugyirat_Label11" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc7:IraIktatoKonyvekDropDownList ID="Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList"
                                                            Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="Ugyirat_EvIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Ugyirat_Label14" runat="server" Text="F�sz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc3:SzamIntervallum_SearchFormControl ID="Ugyirat_Foszam_SzamIntervallum_SearchFormControl1"
                                                            runat="server" />
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 100%" />
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="1">
                            </td>
                            <td colspan="2">
                                <hr />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                                <asp:CheckBox ID="cbPldSimpleSearch" runat="server" Text="" ToolTip="Egyszer� bevitel" />
                            </td>
                            <td>
                                <table>
                                    <%-- INNER TABLE --%>
                                    <tr style="text-align:left;">
                                        <td class="mrUrlapCaptionBal">
                                            <asp:Label ID="labelPldSearch" runat="server" Text="Iratp�ld�ny:" Enabled="false"></asp:Label>
                                        </td>
                                        <td>
                                            <ssc:IratPeldanySimpleSearchComponent ID="pldSimpleSearchComponent" runat="server" />
                                            <table id="ComplexSearchPld" runat="server">
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="labelPld_IktKonyv_Ev" runat="server" Text="�v:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc17:EvIntervallum_SearchFormControl ID="Pld_EvIntervallum_SearchFormControl" runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Pld_Label17" runat="server" Text="Iktat�k�nyv:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc7:IraIktatoKonyvekDropDownList ID="Pld_IktKonyv_IraIktatoKonyvekDropDownList"
                                                            Mode="Iktatokonyvek" EvIntervallum_SearchFormControlId="Pld_EvIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Pld_Label18" runat="server" Text="F�sz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc3:SzamIntervallum_SearchFormControl ID="Pld_Foszam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                                <tr class="urlapSor_kicsi">
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Pld_Label15" runat="server" Text="Alsz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc3:SzamIntervallum_SearchFormControl ID="Pld_Alszam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
                                                    <td class="mrUrlapCaption_short">
                                                        <asp:Label ID="Pld_Label12" runat="server" Text="Sorsz�m:"></asp:Label>
                                                    </td>
                                                    <td class="mrUrlapMezoVAlignTop">
                                                        <uc3:SzamIntervallum_SearchFormControl ID="Pld_Sorszam_SzamIntervallum_SearchFormControl"
                                                            runat="server" />
                                                    </td>
<%--                                                    <td style="width: 100%" />--%>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 100%" />
                                    </tr>
                                </table>
                                <%-- /INNER TABLE --%>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaptionBal_nowidth">
                            </td>
                            <td class="mrUrlapCaption_short">
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                &nbsp;
                            </td>
                            <td class="mrUrlapCaption_short">
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td colspan="1">
                            </td>
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
