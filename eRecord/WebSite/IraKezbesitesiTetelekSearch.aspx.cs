using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;


public partial class IraKezbesitesiTetelek : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_IraKezbesitesiTetelekSearch);
    private string Mode = "";

    #region radiobuttonlists
    private void FillRadioListObjectMode()
    {
        rblObjectMode.Items.Clear();
        rblObjectMode.Items.Add(new ListItem("K�zbes�t�si t�telek", "KT"));
        rblObjectMode.Items.Add(new ListItem("Iratkezel�si elemek", "O"));
        rblObjectMode.SelectedValue = "KT";
    }

    private void FillRadioListKezbesitesiTetelekSearchMode()
    {
        rblKezbesitesiTetelekSearchMode.Items.Clear();
        rblKezbesitesiTetelekSearchMode.Items.Add(new ListItem("Legut�bbi", EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType.LastValidIfExists.ToString()));
        rblKezbesitesiTetelekSearchMode.Items.Add(new ListItem("Mind", EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType.AllValidIfExists.ToString()));
        rblKezbesitesiTetelekSearchMode.Items.Add(new ListItem("Csak iratkezel�si elem", EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType.None.ToString()));
        rblKezbesitesiTetelekSearchMode.Items.Add(new ListItem("K�zbes�t�si t�tel n�lk�li elemek", EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType.ObjectHasNoValidItem.ToString()));
        rblKezbesitesiTetelekSearchMode.SelectedValue = EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType.LastValidIfExists.ToString();
    }

    private void SetJavaScriptToRadioListObjectMode()
    {
        string rbl_onclick_template = "var rbl = $get('" + tr_KezbesitesiTetelekSearchMode.ClientID + "'); if (rbl) {{ rbl.style.display = '{0}'; {1} }}";
        string display = "inline";
        string nodisplay = "none";
        string rblDisabledIfNotDefault = "rbl.disabled = !isDefaultWhere();";

        if (rblObjectMode.Items[0].Attributes["onclick"] == null)
        {
            rblObjectMode.Items[0].Attributes.Add("onclick", String.Format(rbl_onclick_template, nodisplay, ""));
        }
        else
        {
            rblObjectMode.Items[0].Attributes["onclick"] = String.Format(rbl_onclick_template, nodisplay, "");
        }

        if (rblObjectMode.Items[1].Attributes["onclick"] == null)
        {
            rblObjectMode.Items[1].Attributes.Add("onclick", String.Format(rbl_onclick_template, display, rblDisabledIfNotDefault));
        }
        else
        {
            rblObjectMode.Items[1].Attributes["onclick"] = String.Format(rbl_onclick_template, display, rblDisabledIfNotDefault);
        }
    }
    #endregion radiobuttonlists


    private void AddDefaultWhereJavaScript()
    {
        if (!Page.ClientScript.IsStartupScriptRegistered(Page.GetType(), "isDefaultWhereJS"))
        {
            String js = @"function isDefaultWhere()
{
    var atado=$get('" + Atado_FelhasznaloCsoportTextBox.TextBox.ClientID + @"'); if (atado && atado.value && atado.value != '') { return false; }
    var cel=$get('" + CsoportId_Cel_CsoportTextBox.TextBox.ClientID + @"'); if (cel && cel.value && cel.value != '') { return false; }
    var atvevo=$get('" + Atvevo_FelhasznaloCsoportTextBox.TextBox.ClientID + @"'); if (atvevo && atvevo.value && atvevo.value != '') { return false; }
    var atadosajat=$get('" + Ch_Atado_Sajat.ClientID + @"'); if (atadosajat && atadosajat.checked == true) { return false; }
    var atadoszervezet=$get('" + Ch_Atado_Sajat_Szervezet.ClientID + @"'); if (atadoszervezet && atadoszervezet.checked == true) { return false; }
    var cimzettsajat=$get('" + Ch_Cimzett_Sajat.ClientID + @"'); if (cimzettsajat && cimzettsajat.checked == true) { return false; }
    var cimzettszervezet=$get('" + Ch_Cimzett_Sajat_Szervezet.ClientID + @"'); if (cimzettszervezet && cimzettszervezet.checked == true) { return false; }
    var atvevosajat=$get('" + Ch_Atvevo_Sajat.ClientID + @"'); if (atvevosajat && atvevosajat.checked == true) { return false; }
    var atvevoszervezet=$get('" + Ch_Atvevo_Sajat_Szervezet.ClientID + @"'); if (atvevoszervezet && atvevoszervezet.checked == true) { return false; }
    var atnemvett=$get('" + Ch_AtNemVett.ClientID + @"'); if (atnemvett && atnemvett.checked == true) { return false; }
    var atadaskezd=$get('" + AtadasDat_DatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID + @"'); if (atadaskezd && atadaskezd.value && atadaskezd.value != '') { return false; }
    var atadasvege=$get('" + AtadasDat_DatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID + @"'); if (atadasvege && atadasvege.value && atadasvege.value != '') { return false; }
    var letrehozaskezd=$get('" + LetrehozasIdo_DatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID + @"'); if (letrehozaskezd && letrehozaskezd.value && letrehozaskezd.value != '') { return false; }
    var letrehozasvege=$get('" + LetrehozasIdo_DatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID + @"'); if (letrehozasvege && letrehozasvege.value && letrehozasvege.value != '') { return false; }
    var atvetelkezd=$get('" + AtvetelDat_DatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID + @"'); if (atvetelkezd && atvetelkezd.value && atvetelkezd.value != '') { return false; }
    var atvetelvege=$get('" + AtvetelDat_DatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID + @"'); if (atvetelvege && atvetelvege.value && atvetelvege.value != '') { return false; }
    // BUG_4233
    var nyilvantartsz=$get('" + NyilvantartSzTextBox.ClientID + @"'); if (nyilvantartsz && nyilvantartsz.value && nyilvantartsz.value != '') { return false; }
    
    return true;
}
";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "isDefaultWhereJS", js, true);
        }

        string jsBlur = "var rbl=$get('" + tr_KezbesitesiTetelekSearchMode.ClientID + "'); if (rbl) { rbl.disabled = !isDefaultWhere(); } ";

        List<TextBox> textboxes = new List<TextBox>();
        textboxes.Add(Atado_FelhasznaloCsoportTextBox.TextBox);
        textboxes.Add(CsoportId_Cel_CsoportTextBox.TextBox);
        textboxes.Add(Atvevo_FelhasznaloCsoportTextBox.TextBox);
        textboxes.Add(AtadasDat_DatumIntervallum_SearchCalendarControl.DatumKezd_TextBox);
        textboxes.Add(AtadasDat_DatumIntervallum_SearchCalendarControl.DatumVege_TextBox);
        textboxes.Add(LetrehozasIdo_DatumIntervallum_SearchCalendarControl.DatumKezd_TextBox);
        textboxes.Add(LetrehozasIdo_DatumIntervallum_SearchCalendarControl.DatumVege_TextBox);
        textboxes.Add(AtvetelDat_DatumIntervallum_SearchCalendarControl.DatumKezd_TextBox);
        textboxes.Add(AtvetelDat_DatumIntervallum_SearchCalendarControl.DatumVege_TextBox);

        foreach (TextBox tb in textboxes)
        {
            if (tb.Attributes["onblur"] == null)
            {
                tb.Attributes.Add("onblur", jsBlur);
            }
            else
            {
                tb.Attributes["onblur"] = jsBlur + tb.Attributes["onblur"];
            }
        }

        foreach (TextBox tb in textboxes)
        {
            if (tb.Attributes["onclick"] == null)
            {
                tb.Attributes.Add("onclick", jsBlur);
            }
            else
            {
                tb.Attributes["onclick"] = jsBlur + tb.Attributes["onclick"];
            }
        }

        List<CheckBox> checkboxes = new List<CheckBox>();
        checkboxes.Add(Ch_Atado_Sajat);
        checkboxes.Add(Ch_Atado_Sajat_Szervezet);
        checkboxes.Add(Ch_Atado_Sajat_Szervezet_Osszesen);
        checkboxes.Add(Ch_Cimzett_Sajat);
        checkboxes.Add(Ch_Cimzett_Sajat_Szervezet);
        checkboxes.Add(Ch_Cimzett_Sajat_Szervezet_Osszesen);
        checkboxes.Add(Ch_Atvevo_Sajat);
        checkboxes.Add(Ch_Atvevo_Sajat_Szervezet);
        checkboxes.Add(Ch_Atvevo_Sajat_Szervezet_Osszesen);
        checkboxes.Add(Ch_AtNemVett);

        foreach (CheckBox cb in checkboxes)
        {
            if (cb.Attributes["onclick"] == null)
            {
                cb.Attributes.Add("onclick", jsBlur);
            }
            else
            {
                cb.Attributes["onclick"] = jsBlur + cb.Attributes["onclick"];
            }
        }
    }

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
           new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        SearchHeader1.HeaderTitle = Resources.Search.IraKezbesitesiTetelekSearchHeaderTitle;
        SearchHeader1.TemplateObjectType = _type;

        Mode = Request.QueryString.Get(QueryStringVars.Mode);

        if (Mode == CommandName.Atadandok)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AtadandokSearch;
        }
        else if (Mode == CommandName.Atveendok)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AtveendokSearch;
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            SearchHeader1.HeaderTitle = Resources.Search.IraKezbesitesiTetelek_AltalanosKereses_SearchHeaderTitle;
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch;

            if (!IsPostBack)
            {
                FillRadioListObjectMode();
                FillRadioListKezbesitesiTetelekSearchMode();
            }

            AddDefaultWhereJavaScript();
        }
        else if (String.IsNullOrEmpty(Mode))
        {
        }

        registerJavascripts();


        int currentYear = System.DateTime.Now.Year;
        kuldemenySimpleSearchComponent.AlapertelmezettEv = currentYear;
        ugyiratSimpleSearchComponent.AlapertelmezettEv = currentYear;
        pldSimpleSearchComponent.AlapertelmezettEv = currentYear;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EREC_IraKezbesitesiTetelekSearch searchObject = null;
            if (Mode == CommandName.Atadandok
                && Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AtadandokSearch))
            {
                searchObject = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                    Page, new EREC_IraKezbesitesiTetelekSearch(), Constants.CustomSearchObjectSessionNames.AtadandokSearch);
            }
            else if (Mode == CommandName.Atveendok
                     && Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AtveendokSearch))
            {
                searchObject = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                   Page, new EREC_IraKezbesitesiTetelekSearch(), Constants.CustomSearchObjectSessionNames.AtveendokSearch);
            }
            else if (Mode == CommandName.AltalanosKereses
                     && Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch))
            {
                searchObject = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject_CustomSessionName(
                   Page, new EREC_IraKezbesitesiTetelekSearch(true), Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch);
            }
            else if (String.IsNullOrEmpty(Mode) && Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_IraKezbesitesiTetelekSearch)Search.GetSearchObject(Page, new EREC_IraKezbesitesiTetelekSearch(true));
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

        // n�h�ny komponensn�l le kell tiltani a viewstate-et, k�l�nben mindenf�le cs�nyas�got csin�l...
        //Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.EnableViewState = false;
        Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.EnableViewState = false;
        Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.EnableViewState = false;
        //Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.EnableViewState = false;
        Pld_Alszam_SzamIntervallum_SearchFormControl.EnableViewState = false;
        Pld_Foszam_SzamIntervallum_SearchFormControl.EnableViewState = false;
        //Pld_IktKonyv_IraIktatoKonyvekDropDownList.EnableViewState = false;
        Pld_Sorszam_SzamIntervallum_SearchFormControl.EnableViewState = false;

        Kuld_EvIntervallum_SearchFormControl.EnableViewState = false;
        Ugyirat_EvIntervallum_SearchFormControl.EnableViewState = false;
        Pld_EvIntervallum_SearchFormControl.EnableViewState = false;

        if (Mode == CommandName.AltalanosKereses)
        {
            SetJavaScriptToRadioListObjectMode();
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Mode == CommandName.Atadandok)
        {
            Label_Atado1.Enabled = false;
            Atado_FelhasznaloCsoportTextBox.Enabled = false;
            //Label_Atado2.Enabled = false;            
            //Ch_Atado_Sajat.Enabled = false;
            Ch_Atvevo_Sajat_Szervezet_Osszesen.Text = "Mind";
            Ch_Cimzett_Sajat_Szervezet_Osszesen.Text = "Mind";

            Ch_AtNemVett.Visible = false;
        }
        else if (Mode == CommandName.Atveendok)
        {
            Label_Cimzett1.Enabled = false;
            //Label_Cimzett2.Enabled = false;
            CsoportId_Cel_CsoportTextBox.Enabled = false;
            //Ch_Cimzett_Sajat.Enabled = false;
            Ch_Atvevo_Sajat_Szervezet_Osszesen.Text = "Mind";
            Ch_Atado_Sajat_Szervezet_Osszesen.Text = "Mind";

            Ch_AtNemVett.Visible = false;
        }
        else if (Mode == CommandName.AltalanosKereses)
        {
            rblObjectMode.Visible = true;
            tr_KezbesitesiTetelekSearchMode.Visible = true;
            tr_TargyFTS.Visible = true;
        }
        // BUG_4233    
        //a funkci�t T�K kezel� haszn�lhatja
        Nyilvantartassz_row.Visible = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);

    }

    #endregion

    /// <summary>
    /// Keres�si objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = null;
        if (searchObject != null) erec_IraKezbesitesiTetelekSearch = (EREC_IraKezbesitesiTetelekSearch)searchObject;

        if (erec_IraKezbesitesiTetelekSearch != null)
        {
            Atado_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraKezbesitesiTetelekSearch.Felhasznalo_Id_Atado_USER.Value;
            Atado_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            AtadasDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_IraKezbesitesiTetelekSearch.AtadasDat);

            // �tveend�kn�l nem t�ltj�k (elsz�llna)
            //if (Mode != CommandName.Atveendok)
            //{
            //    CsoportId_Cel_CsoportTextBox.Id_HiddenField = erec_IraKezbesitesiTetelekSearch.Csoport_Id_Cel.Value;
            //    CsoportId_Cel_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
            //}

            // csak ha t�nyleg egy Id van benne
            string Csoport_Id_Cel = erec_IraKezbesitesiTetelekSearch.Csoport_Id_Cel.Value.Replace("'", "");
            string[] separator = new string[] { "," };
            int cntIds = Csoport_Id_Cel.Split(separator, StringSplitOptions.None).Length;
            if (cntIds < 2)
            {
                CsoportId_Cel_CsoportTextBox.Id_HiddenField = Csoport_Id_Cel;
            }
            else
            {
                CsoportId_Cel_CsoportTextBox.Id_HiddenField = "";
            }
            CsoportId_Cel_CsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_IraKezbesitesiTetelekSearch.Manual_LetrehozasIdo);

            Atvevo_FelhasznaloCsoportTextBox.Id_HiddenField = erec_IraKezbesitesiTetelekSearch.Felhasznalo_Id_AtvevoUser.Value;
            Atvevo_FelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            AtvetelDat_DatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(
                erec_IraKezbesitesiTetelekSearch.AtvetelDat);

            #region Atado/Cimzett/Atvevo RadioButtons
            // Mivel nem lehet egyidej�leg t�bb gomb kiv�lasztva, el�bb mindet ki kell kapcsolni,
            // m�sk�pp hib�s m�k�d�st kapunk

            // �tad�ra vonatkoz� r�di�gombok
            Ch_Atado_Sajat.Checked = false;
            Ch_Atado_Sajat_Szervezet.Checked = false;
            Ch_Atado_Sajat_Szervezet_Osszesen.Checked = false;
            if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat.Value))
            {
                Ch_Atado_Sajat.Checked = true;
            }
            else if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat_Szervezet.Value))
            {
                Ch_Atado_Sajat_Szervezet.Checked = true;
            }
            else // if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat_Szervezet_Osszesen.Value))
            {
                Ch_Atado_Sajat_Szervezet_Osszesen.Checked = true;
            }

            // C�mzettre vonatkoz� r�di�gombok
            Ch_Cimzett_Sajat.Checked = false;
            Ch_Cimzett_Sajat_Szervezet.Checked = false;
            Ch_Cimzett_Sajat_Szervezet_Osszesen.Checked = false;
            if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat.Value))
            {
                Ch_Cimzett_Sajat.Checked = true;
            }
            else if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat_Szervezet.Value))
            {
                Ch_Cimzett_Sajat_Szervezet.Checked = true;
            }
            else // if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat_Szervezet_Osszesen.Value))
            {
                Ch_Cimzett_Sajat_Szervezet_Osszesen.Checked = true;
            }

            // �tvev�re vonatkoz� r�di�gombok
            Ch_Atvevo_Sajat.Checked = false;
            Ch_Atvevo_Sajat_Szervezet.Checked = false;
            Ch_Atvevo_Sajat_Szervezet_Osszesen.Checked = false;
            Ch_AtNemVett.Checked = false;
            if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat.Value))
            {
                Ch_Atvevo_Sajat.Checked = true;
            }
            else if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat_Szervezet.Value))
            {
                Ch_Atvevo_Sajat_Szervezet.Checked = true;
            }
            else if  // �tadand�k/�tveend�k eset�n �rtelmetlen �s nem l�tszik
                (erec_IraKezbesitesiTetelekSearch.Manual_AtNemVett.Operator == Query.Operators.isnull
                && Mode != CommandName.Atveendok && Mode != CommandName.Atadandok)
            {
                Ch_AtNemVett.Checked = true;
            }
            else // if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat_Szervezet_Osszesen.Value) || Mode == CommandName.Atveendok || Mode != CommandName.Atadandok)
            {
                Ch_Atvevo_Sajat_Szervezet_Osszesen.Checked = true;
            }
            #endregion Atado/Cimzett/Atvevo RadioButtons

            Ch_Kuldemeny.Checked = (String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.Value))
                ? false : true;

            Ch_Ugyirat.Checked = (String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.Value))
                ? false : true;

            Ch_Iratpeldany.Checked = (String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.Value))
                ? false : true;

            Ch_Dosszie.Checked = (String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Dosszie.Value))
                ? false : true;

            //Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillAndSetSelectedValue(false, false, Constants.IktatoErkezteto.Erkezteto
            //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value, true, SearchHeader1.ErrorPanel);

            bool bSimpleSearchKuldemeny = true;
            if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
            {
                bSimpleSearchKuldemeny &= kuldemenySimpleSearchComponent.FillComponentsFromSearchObject(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch, SearchHeader1.ErrorPanel);

                if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    // TODO:
                    Kuld_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                    Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                    , Kuld_EvIntervallum_SearchFormControl.EvTol, Kuld_EvIntervallum_SearchFormControl.EvIg
                    , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
                }
                else
                {
                    Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Erkezteto
                        , "", "", true, false, SearchHeader1.ErrorPanel);
                }

                Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Erkezteto_Szam);
            }
            cbKuldemenySimpleSearch.Checked = bSimpleSearchKuldemeny;
            ShowOrHideComponent(kuldemenySimpleSearchComponent.Container, bSimpleSearchKuldemeny);
            ShowOrHideComponent(ComplexSearchKuldemeny, !bSimpleSearchKuldemeny);

            //Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.FillAndSetSelectedValue(false, false, Constants.IktatoErkezteto.Iktato
            //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Value, true, SearchHeader1.ErrorPanel);
            bool bSimpleSearchUgyirat = true;
            if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch != null)
            {
                bSimpleSearchUgyirat &= ugyiratSimpleSearchComponent.FillComponentsFromSearchObject(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch, SearchHeader1.ErrorPanel);

                if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    // TODO:
                    Ugyirat_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                    Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                    , Ugyirat_EvIntervallum_SearchFormControl.EvTol, Ugyirat_EvIntervallum_SearchFormControl.EvIg
                    , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
                }
                else
                {
                    Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                        , "", "", true, false, SearchHeader1.ErrorPanel);
                }

                Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SetComponentFromSearchObjectFields(
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Foszam);
            }
            cbUgyiratSimpleSearch.Checked = bSimpleSearchUgyirat;
            ShowOrHideComponent(ugyiratSimpleSearchComponent.Container, bSimpleSearchUgyirat);
            ShowOrHideComponent(ComplexSearchUgyirat, !bSimpleSearchUgyirat);

            //Pld_IktKonyv_IraIktatoKonyvekDropDownList.FillAndSetSelectedValue(false, false, Constants.IktatoErkezteto.Iktato
            //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch
            //        .IraIktatokonyv_Id.Value, true, SearchHeader1.ErrorPanel);
            bool bSimpleSearchPld = true;
            if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch != null)
            {
                bSimpleSearchPld &= pldSimpleSearchComponent.FillComponentsFromSearchObject(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch, SearchHeader1.ErrorPanel);

                if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch != null)
                {
                    Pld_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                    Pld_IktKonyv_IraIktatoKonyvekDropDownList.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                    , Pld_EvIntervallum_SearchFormControl.EvTol, Pld_EvIntervallum_SearchFormControl.EvIg
                    , true, false, IktatoKonyvek.GetIktatokonyvValue(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch), SearchHeader1.ErrorPanel);
                }
                else
                {
                    Pld_IktKonyv_IraIktatoKonyvekDropDownList.FillDropDownList_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                        , "", "", true, false, SearchHeader1.ErrorPanel);
                }

                if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch != null)
                {
                    Pld_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);
                }

                if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null)
                {
                    Pld_Alszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam);
                }

                Pld_Sorszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Sorszam);
            }

            cbPldSimpleSearch.Checked = bSimpleSearchPld;
            ShowOrHideComponent(pldSimpleSearchComponent.Container, bSimpleSearchPld);
            ShowOrHideComponent(ComplexSearchPld, !bSimpleSearchPld);

            #region Barcode

            List<string> vonalkodList = new List<string>();
            VonalKodListBox1.ListBox.Items.Clear();

            #region EREC_KuldKuldemenyek
            if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch != null
                && !String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.BarCode.Value))
            {
                string[] vonalkodok = erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.BarCode.Value.Replace("'", "").Split(',');
                foreach (string item in vonalkodok)
                {
                    if (!vonalkodList.Contains(item))
                    {
                        vonalkodList.Add(item);
                        VonalKodListBox1.ListBox.Items.Add(item);
                    }
                }
            }
            #endregion EREC_KuldKuldemenyek

            #region EREC_PldIratPeldanyok
            if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch != null
                && !String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.BarCode.Value))
            {
                string[] vonalkodok = erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.BarCode.Value.Replace("'", "").Split(',');
                foreach (string item in vonalkodok)
                {
                    if (!vonalkodList.Contains(item))
                    {
                        vonalkodList.Add(item);
                        VonalKodListBox1.ListBox.Items.Add(item);
                    }
                }
            }
            #endregion EREC_PldIratPeldanyok

            #region EREC_UgyUgyiratok
            if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch != null
                && !String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.BARCODE.Value))
            {
                string[] vonalkodok = erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.BARCODE.Value.Replace("'", "").Split(',');
                foreach (string item in vonalkodok)
                {
                    if (!vonalkodList.Contains(item))
                    {
                        vonalkodList.Add(item);
                        VonalKodListBox1.ListBox.Items.Add(item);
                    }
                }
            }
            #endregion EREC_UgyUgyiratok

            VonalKodListBox1.VonalkodClientList = String.Join(",", vonalkodList.ToArray());

            #endregion Barcode

            // ObjectMode
            rblObjectMode.SelectedValue = (erec_IraKezbesitesiTetelekSearch.isObjectMode ? "O" : "KT");
            rblKezbesitesiTetelekSearchMode.SelectedValue = erec_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectMode.KezbesitesiTetelekSearchMode.ToString();
            ShowOrHideComponent(tr_KezbesitesiTetelekSearchMode, erec_IraKezbesitesiTetelekSearch.isObjectMode);

            bool isDefaultWhere = (erec_IraKezbesitesiTetelekSearch.isObjectMode && erec_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectMode.IsDefaultWhere);
            EnableOrDisableComponent(tr_KezbesitesiTetelekSearchMode, isDefaultWhere);

            #region fts
            TextBoxAltalanosFTS.Text = "";
            if (Ch_Kuldemeny.Checked || Ch_Ugyirat.Checked || Ch_Iratpeldany.Checked)
            {
                //if (erec_IraKezbesitesiTetelekSearch.fts_altalanos != null)
                //{
                //    TextBoxAltalanosFTS.Text = erec_IraKezbesitesiTetelekSearch.fts_altalanos.Filter;
                //}
                //else
                //{
                //    TextBoxAltalanosFTS.Text = "";
                //}

                // elvileg egyforma �rt�k lehet csak benn�k, addig megy�nk, m�g �rt�ket tal�lunk...
                if (Ch_Kuldemeny.Checked)
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch != null)
                    {
                        if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value))
                        {
                            TextBoxAltalanosFTS.Text = erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value;
                        }
                        else if (!String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.Value))
                        {
                            TextBoxAltalanosFTS.Text = erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.Value;
                        }
                    }
                }
                else if (Ch_Ugyirat.Checked)
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch != null
                        && !String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.Value))
                    {
                        TextBoxAltalanosFTS.Text = erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.Value;
                    }
                    else if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch != null
                        && !String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.NevSTR_Ugyindito.Value))
                    {
                        TextBoxAltalanosFTS.Text = erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.NevSTR_Ugyindito.Value;
                    }
                }
                else if (Ch_Iratpeldany.Checked)
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch != null
                        && erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch != null
                        && !String.IsNullOrEmpty(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Value))
                    {
                        TextBoxAltalanosFTS.Text = erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Value;
                    }
                }

            }

            #endregion fts

            // BUG_4233
            NyilvantartSzTextBox.Text = erec_IraKezbesitesiTetelekSearch.Megjegyzes.Value;
        }
    }

    // A mez�k kit�lt�tts�ge alapj�n meg�llap�tjuk, hogy van-e k�l�n felt�tel
    // a k�zbes�t�si t�telekre - csak ObjectMode-ban van jelent�s�ge
    // (ha nincs k�zi be�ll�t�s, nem adjuk �t a Where felt�telt a t�rolt elj�r�snak)
    private bool IsDefaultWhere()
    {
        bool isDefaultWhere = true;
        isDefaultWhere &= String.IsNullOrEmpty(Atado_FelhasznaloCsoportTextBox.Id_HiddenField);
        isDefaultWhere &= String.IsNullOrEmpty(CsoportId_Cel_CsoportTextBox.Id_HiddenField);
        isDefaultWhere &= String.IsNullOrEmpty(Atvevo_FelhasznaloCsoportTextBox.Id_HiddenField);

        isDefaultWhere &= (Ch_Atado_Sajat.Checked == false);
        isDefaultWhere &= (Ch_Atado_Sajat_Szervezet.Checked == false);
        isDefaultWhere &= (Ch_Cimzett_Sajat.Checked == false);
        isDefaultWhere &= (Ch_Cimzett_Sajat_Szervezet.Checked == false);
        isDefaultWhere &= (Ch_Atvevo_Sajat.Checked == false);
        isDefaultWhere &= (Ch_Atvevo_Sajat_Szervezet.Checked == false);
        isDefaultWhere &= (Ch_AtNemVett.Checked == false);

        isDefaultWhere &= String.IsNullOrEmpty(AtadasDat_DatumIntervallum_SearchCalendarControl.DatumKezd);
        isDefaultWhere &= String.IsNullOrEmpty(AtadasDat_DatumIntervallum_SearchCalendarControl.DatumVege);
        isDefaultWhere &= String.IsNullOrEmpty(LetrehozasIdo_DatumIntervallum_SearchCalendarControl.DatumKezd);
        isDefaultWhere &= String.IsNullOrEmpty(LetrehozasIdo_DatumIntervallum_SearchCalendarControl.DatumVege);
        isDefaultWhere &= String.IsNullOrEmpty(AtvetelDat_DatumIntervallum_SearchCalendarControl.DatumKezd);
        isDefaultWhere &= String.IsNullOrEmpty(AtvetelDat_DatumIntervallum_SearchCalendarControl.DatumVege);

        return isDefaultWhere;
    }

    /// <summary>
    /// Form --> Keres�si objektum
    /// </summary>
    private EREC_IraKezbesitesiTetelekSearch SetSearchObjectFromComponents()
    {
        EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch = (EREC_IraKezbesitesiTetelekSearch)SearchHeader1.TemplateObject;
        if (erec_IraKezbesitesiTetelekSearch == null)
        {
            erec_IraKezbesitesiTetelekSearch = new EREC_IraKezbesitesiTetelekSearch(true);
        }

        if (!String.IsNullOrEmpty(Atado_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_IraKezbesitesiTetelekSearch.Felhasznalo_Id_Atado_USER.Value = Atado_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_IraKezbesitesiTetelekSearch.Felhasznalo_Id_Atado_USER.Operator = Query.Operators.equals;
        }

        AtadasDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraKezbesitesiTetelekSearch.AtadasDat);

        if (!String.IsNullOrEmpty(CsoportId_Cel_CsoportTextBox.Id_HiddenField))
        {
            erec_IraKezbesitesiTetelekSearch.Csoport_Id_Cel.Value = CsoportId_Cel_CsoportTextBox.Id_HiddenField;
            erec_IraKezbesitesiTetelekSearch.Csoport_Id_Cel.Operator = Query.Operators.equals;
        }

        LetrehozasIdo_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraKezbesitesiTetelekSearch.Manual_LetrehozasIdo);

        if (!String.IsNullOrEmpty(Atvevo_FelhasznaloCsoportTextBox.Id_HiddenField))
        {
            erec_IraKezbesitesiTetelekSearch.Felhasznalo_Id_AtvevoUser.Value = Atvevo_FelhasznaloCsoportTextBox.Id_HiddenField;
            erec_IraKezbesitesiTetelekSearch.Felhasznalo_Id_AtvevoUser.Operator = Query.Operators.equals;
        }

        AtvetelDat_DatumIntervallum_SearchCalendarControl.SetSearchObjectFields(
            erec_IraKezbesitesiTetelekSearch.AtvetelDat);

        String sajatCsoportId = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
            FelhasznaloProfil.FelhasznaloId(Page));

        if (Ch_Atado_Sajat.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat.Value = sajatCsoportId;
            erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat.Operator = Query.Operators.equals;
        }

        if (Ch_Atado_Sajat_Szervezet.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat_Szervezet.Value = sajatCsoportId;
            erec_IraKezbesitesiTetelekSearch.Manual_Atado_Sajat_Szervezet.Operator = Query.Operators.notequals;
        }

        if (Ch_Cimzett_Sajat.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat.Value = sajatCsoportId;
            erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat.Operator = Query.Operators.equals;
        }

        if (Ch_Cimzett_Sajat_Szervezet.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat_Szervezet.Value = sajatCsoportId;
            erec_IraKezbesitesiTetelekSearch.Manual_Cimzett_Sajat_Szervezet.Operator = Query.Operators.notequals;
        }

        if (Ch_Atvevo_Sajat.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat.Value = sajatCsoportId;
            erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat.Operator = Query.Operators.equals;
        }

        if (Ch_Atvevo_Sajat_Szervezet.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat_Szervezet.Value = sajatCsoportId;
            erec_IraKezbesitesiTetelekSearch.Manual_Atvevo_Sajat_Szervezet.Operator = Query.Operators.notequals;
        }

        // �tadand�k/�tveend�k eset�n �rtelmetlen �s nem l�tszik
        if (Mode != CommandName.Atveendok && Mode != CommandName.Atadandok)
        {
            if (Ch_AtNemVett.Checked == true)
            {
                erec_IraKezbesitesiTetelekSearch.Manual_AtNemVett.Value = "";
                erec_IraKezbesitesiTetelekSearch.Manual_AtNemVett.Operator = Query.Operators.isnull;
            }
        }

        String Group_ObjTipusok = "456";

        if (Ch_Kuldemeny.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.Value = Constants.TableNames.EREC_KuldKuldemenyek;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.Operator = Query.Operators.equals;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.Group = Group_ObjTipusok;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Kuldemeny.GroupOperator = Query.Operators.or;
        }
        if (Ch_Ugyirat.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.Value = Constants.TableNames.EREC_UgyUgyiratok;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.Operator = Query.Operators.equals;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.Group = Group_ObjTipusok;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Ugyirat.GroupOperator = Query.Operators.or;
        }
        if (Ch_Iratpeldany.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.Value = Constants.TableNames.EREC_PldIratPeldanyok;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.Operator = Query.Operators.equals;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.Group = Group_ObjTipusok;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_IratPeldany.GroupOperator = Query.Operators.or;
        }
        if (Ch_Dosszie.Checked == true)
        {
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Dosszie.Value = Constants.TableNames.KRT_Mappak;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Dosszie.Operator = Query.Operators.equals;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Dosszie.Group = Group_ObjTipusok;
            erec_IraKezbesitesiTetelekSearch.Manual_Obj_type_Dosszie.GroupOperator = Query.Operators.or;
        }

        //if (!String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue))
        //{
        //    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value =
        //        Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue;
        //    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.IraIktatokonyv_Id.Operator =
        //        Query.Operators.equals;
        //}

        if (Ch_Kuldemeny.Checked == true)
        {
            if (cbKuldemenySimpleSearch.Checked == false)
            {
                if (!String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue)
                || !String.IsNullOrEmpty(Kuld_EvIntervallum_SearchFormControl.EvTol)
                || !String.IsNullOrEmpty(Kuld_EvIntervallum_SearchFormControl.EvIg)
                || !String.IsNullOrEmpty(Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SzamTol)
                || !String.IsNullOrEmpty(Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SzamIg))
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
                    }
                    //Search.ClearErvenyessegFilter(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvKezd
                    //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvVege);

                    if (!String.IsNullOrEmpty(Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SelectedValue))
                    {
                        Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SetSearchObject(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch);
                    }

                    Kuld_EvIntervallum_SearchFormControl.SetSearchObjectFields(erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                    Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Erkezteto_Szam);
                }
                // a ReadableWhere miatt t�r�lj�k a nem l�that� mez�ket
                #region Clear hidden input fields
                kuldemenySimpleSearchComponent.ClearInputFields();
                #endregion Clear hidden input fields
            }
            else if (cbKuldemenySimpleSearch.Checked == true)
            {
                if (!String.IsNullOrEmpty(kuldemenySimpleSearchComponent.Text))
                {
                    kuldemenySimpleSearchComponent.FillSearchObjectFromComponents(ref erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch);
                }
                // a ReadableWhere miatt t�r�lj�k a nem l�that� mez�ket
                #region Clear hidden input fields
                Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SetSelectedValue("");
                Kuld_EvIntervallum_SearchFormControl.EvTol = "";
                Kuld_EvIntervallum_SearchFormControl.EvIg = "";
                Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SzamTol = "";
                Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SzamIg = "";
                #endregion Clear hidden input fields
            }
        }
        else
        {
            // ne maradjon �rt�k a dropdownlistben
            Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.SetSelectedValue("");
            kuldemenySimpleSearchComponent.ClearInputFields();
        }

        //if (!String.IsNullOrEmpty(Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue))
        //{
        //    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Value =
        //        Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue;
        //    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.IraIktatokonyv_Id.Operator =
        //        Query.Operators.equals;
        //}

        if (Ch_Ugyirat.Checked == true)
        {
            if (cbUgyiratSimpleSearch.Checked == false)
            {
                if (!String.IsNullOrEmpty(Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue)
                       || !String.IsNullOrEmpty(Ugyirat_EvIntervallum_SearchFormControl.EvTol)
                       || !String.IsNullOrEmpty(Ugyirat_EvIntervallum_SearchFormControl.EvIg)
                       || !String.IsNullOrEmpty(Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SzamTol)
                       || !String.IsNullOrEmpty(Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SzamIg))
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
                    }
                    //Search.ClearErvenyessegFilter(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvKezd
                    //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvVege);
                    if (!String.IsNullOrEmpty(Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue))
                    {
                        Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SetSearchObject(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch);
                    }

                    Ugyirat_EvIntervallum_SearchFormControl.SetSearchObjectFields(erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                    Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SetSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Foszam);
                }
                // a ReadableWhere miatt t�r�lj�k a nem l�that� mez�ket
                #region Clear hidden input fields
                ugyiratSimpleSearchComponent.ClearInputFields();
                #endregion Clear hidden input fields
            }
            else if (cbUgyiratSimpleSearch.Checked == true)
            {
                if (!String.IsNullOrEmpty(ugyiratSimpleSearchComponent.Text))
                {
                    ugyiratSimpleSearchComponent.FillSearchObjectFromComponents(ref erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch);
                }
                // a ReadableWhere miatt t�r�lj�k a nem l�that� mez�ket
                #region Clear hidden input fields
                Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SetSelectedValue("");
                Ugyirat_EvIntervallum_SearchFormControl.EvTol = "";
                Ugyirat_EvIntervallum_SearchFormControl.EvIg = "";
                Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SzamTol = "";
                Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SzamIg = "";
                #endregion Clear hidden input fields
            }
        }
        else
        {
            // ne maradjon �rt�k a dropdownlistben
            Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.SetSelectedValue("");
            ugyiratSimpleSearchComponent.ClearInputFields();
        }


        //if (!String.IsNullOrEmpty(Pld_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue))
        //{
        //    erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch
        //        .IraIktatokonyv_Id.Value = Pld_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue;
        //    erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch
        //        .IraIktatokonyv_Id.Operator = Query.Operators.equals;
        //}
        if (Ch_Iratpeldany.Checked == true)
        {
            if (cbPldSimpleSearch.Checked == false)
            {
                if (!String.IsNullOrEmpty(Pld_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue)
                    || !String.IsNullOrEmpty(Pld_EvIntervallum_SearchFormControl.EvTol)
                    || !String.IsNullOrEmpty(Pld_EvIntervallum_SearchFormControl.EvIg)
                    || !String.IsNullOrEmpty(Pld_Foszam_SzamIntervallum_SearchFormControl.SzamTol)
                    || !String.IsNullOrEmpty(Pld_Foszam_SzamIntervallum_SearchFormControl.SzamIg)
                    || !String.IsNullOrEmpty(Pld_Alszam_SzamIntervallum_SearchFormControl.SzamTol)
                    || !String.IsNullOrEmpty(Pld_Alszam_SzamIntervallum_SearchFormControl.SzamIg)
                    || !String.IsNullOrEmpty(Pld_Sorszam_SzamIntervallum_SearchFormControl.SzamTol)
                    || !String.IsNullOrEmpty(Pld_Sorszam_SzamIntervallum_SearchFormControl.SzamIg))
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch = new EREC_IraIktatoKonyvekSearch();
                    }
                    //Search.ClearErvenyessegFilter(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvKezd
                    //    , erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.ErvVege);

                    if (!String.IsNullOrEmpty(Pld_IktKonyv_IraIktatoKonyvekDropDownList.SelectedValue))
                    {
                        Pld_IktKonyv_IraIktatoKonyvekDropDownList.SetSearchObject(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch);
                    }

                    Pld_EvIntervallum_SearchFormControl.SetSearchObjectFields(erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch.Ev);

                    Pld_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

                    Pld_Alszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Alszam);

                    Pld_Sorszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Sorszam);
                }
                // a ReadableWhere miatt t�r�lj�k a nem l�that� mez�ket
                #region Clear hidden input fields
                pldSimpleSearchComponent.ClearInputFields();
                #endregion Clear hidden input fields
            }
            else if (cbPldSimpleSearch.Checked == true)
            {
                if (!String.IsNullOrEmpty(pldSimpleSearchComponent.Text))
                {
                    pldSimpleSearchComponent.FillSearchObjectFromComponents(ref erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch);
                }
                // a ReadableWhere miatt t�r�lj�k a nem l�that� mez�ket
                #region Clear hidden input fields
                Pld_IktKonyv_IraIktatoKonyvekDropDownList.SetSelectedValue("");
                Pld_EvIntervallum_SearchFormControl.EvTol = "";
                Pld_EvIntervallum_SearchFormControl.EvIg = "";
                Pld_Foszam_SzamIntervallum_SearchFormControl.SzamTol = "";
                Pld_Foszam_SzamIntervallum_SearchFormControl.SzamIg = "";
                Pld_Alszam_SzamIntervallum_SearchFormControl.SzamTol = "";
                Pld_Alszam_SzamIntervallum_SearchFormControl.SzamIg = "";
                Pld_Sorszam_SzamIntervallum_SearchFormControl.SzamTol = "";
                Pld_Sorszam_SzamIntervallum_SearchFormControl.SzamIg = "";
                #endregion Clear hidden input fields
            }
        }
        else
        {
            // ne maradjon �rt�k a dropdownlistben
            Pld_IktKonyv_IraIktatoKonyvekDropDownList.SetSelectedValue("");
            pldSimpleSearchComponent.ClearInputFields();
        }

        // ObjectMode
        erec_IraKezbesitesiTetelekSearch.isObjectMode = (rblObjectMode.SelectedValue == "O" ? true : false);
        if (erec_IraKezbesitesiTetelekSearch.isObjectMode == true)
        {
            bool isDefaultWhere = IsDefaultWhere();
            erec_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectMode.IsDefaultWhere = isDefaultWhere;

            if (isDefaultWhere)
            {
                erec_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectMode.KezbesitesiTetelekSearchMode =
                    (EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType)Enum.Parse(typeof(EREC_IraKezbesitesiTetelekSearch.ExtendedFilterForObjectModeClass.KezbesitesiTetelekSearchModeType)
                            , rblKezbesitesiTetelekSearchMode.SelectedValue);
            }
        }
        else
        {
            // ReadableWhere miatt t�r�lj�k
            rblKezbesitesiTetelekSearchMode.SelectedIndex = -1;
        }

        #region Barcode
        // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
        if (!String.IsNullOrEmpty(VonalKodListBox1.VonalkodClientList))
        {
            string vonalkodok = "'" + VonalKodListBox1.VonalkodClientList.Replace(",", "','") + "'";

            SetBarcodeSearch(erec_IraKezbesitesiTetelekSearch, vonalkodok);

        }//BUG_13408
        else if (!String.IsNullOrEmpty(VonalKodListBox1.TextBox.Text))
        {
            string vonalkod = "'" + VonalKodListBox1.TextBox.Text.Replace(",", "','") + "'";
            SetBarcodeSearch(erec_IraKezbesitesiTetelekSearch, vonalkod);
        }
        #endregion Barcode

        #region fts
        if (Ch_Kuldemeny.Checked || Ch_Ugyirat.Checked || Ch_Iratpeldany.Checked)
        {
            if (!String.IsNullOrEmpty(TextBoxAltalanosFTS.Text))
            {
                //erec_IraKezbesitesiTetelekSearch.fts_altalanos = new FullTextSearchField();
                //erec_IraKezbesitesiTetelekSearch.fts_altalanos.Filter = TextBoxAltalanosFTS.Text;

                if (Ch_Kuldemeny.Checked)
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();
                    }

                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Group = "333";
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.GroupOperator = Query.Operators.or;
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Value = TextBoxAltalanosFTS.Text;
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.NevSTR_Bekuldo.Operator = Query.Operators.contains;

                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.Group = "333";
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.GroupOperator = Query.Operators.or;
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.Value = TextBoxAltalanosFTS.Text;
                    // BUG_3944
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.Operator = Query.Operators.contains;
                    //erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.Targy.Operator = Search.GetOperatorByLikeCharater(TextBoxAltalanosFTS.Text); 

                }

                if (Ch_Ugyirat.Checked)
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch();
                    }

                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.Group = "333";
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.GroupOperator = Query.Operators.or;
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.Value = TextBoxAltalanosFTS.Text;
                    // BUG_3944
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.Operator = Query.Operators.contains;
                    //erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.Targy.Operator = Search.GetOperatorByLikeCharater(TextBoxAltalanosFTS.Text); 

                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.NevSTR_Ugyindito.Group = "333";
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.NevSTR_Ugyindito.GroupOperator = Query.Operators.or;
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.NevSTR_Ugyindito.Value = TextBoxAltalanosFTS.Text;
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.NevSTR_Ugyindito.Operator = Query.Operators.contains;
                }

                if (Ch_Iratpeldany.Checked)
                {
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch = new EREC_PldIratPeldanyokSearch();
                    }
                    if (erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch == null)
                    {
                        erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch = new EREC_IraIratokSearch();
                    }

                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Value = TextBoxAltalanosFTS.Text;
                    // BUG_3944
                    erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Operator = Query.Operators.contains;
                    //erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch.Targy.Operator = Search.GetOperatorByLikeCharater(TextBoxAltalanosFTS.Text);

                }
            }
        }
        else
        {
            // ReadableWhere miatt t�r�lj�k
            TextBoxAltalanosFTS.Text = "";
        }
        #endregion fts

        // BUG_4233 
        if (!String.IsNullOrEmpty(NyilvantartSzTextBox.Text))
        {
            erec_IraKezbesitesiTetelekSearch.Megjegyzes.Value = NyilvantartSzTextBox.Text;
            erec_IraKezbesitesiTetelekSearch.Megjegyzes.Operator = Query.Operators.equals;
        }
        return erec_IraKezbesitesiTetelekSearch;
    }

    private void SetBarcodeSearch(EREC_IraKezbesitesiTetelekSearch erec_IraKezbesitesiTetelekSearch, string vonalkodok)
    {
        bool bNoObjectFilter = !Ch_Kuldemeny.Checked && !Ch_Ugyirat.Checked && !Ch_Iratpeldany.Checked;

        // ha megengedett objektum, vagy nincs sz�k�t�s objektum t�pusra
        if (Ch_Kuldemeny.Checked || bNoObjectFilter)
        {
            //erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.BarCode.Group = "101";
            //erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.BarCode.GroupOperator = Query.Operators.or;
            erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.BarCode.Value = vonalkodok;
            erec_IraKezbesitesiTetelekSearch.Extended_EREC_KuldKuldemenyekSearch.BarCode.Operator = Query.Operators.inner;
        }
        if (Ch_Iratpeldany.Checked || bNoObjectFilter)
        {
            //erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.BarCode.Group = "101";
            //erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.BarCode.GroupOperator = Query.Operators.or;
            erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.BarCode.Value = vonalkodok;
            erec_IraKezbesitesiTetelekSearch.Extended_EREC_PldIratPeldanyokSearch.BarCode.Operator = Query.Operators.inner;
        }
        if (Ch_Ugyirat.Checked || bNoObjectFilter)
        {
            //erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.BARCODE.Group = "101";
            //erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.BARCODE.GroupOperator = Query.Operators.or;
            erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.BARCODE.Value = vonalkodok;
            erec_IraKezbesitesiTetelekSearch.Extended_EREC_UgyUgyiratokSearch.BARCODE.Operator = Query.Operators.inner;
        }
    }



    /// <summary>
    /// FormTemplateLoader esem�nyei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IraKezbesitesiTetelekSearch searchObject = SetSearchObjectFromComponents();
            EREC_IraKezbesitesiTetelekSearch defaultSearchObject = GetDefaultSearchObject();

            if (searchObject.isObjectMode == defaultSearchObject.isObjectMode
                &&
                Search.IsIdentical(searchObject, defaultSearchObject)
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch, defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch
                              , defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIratokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch
                              , defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratdarabokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch
                              , defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_UgyUgyiratokSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch
                              , defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch.Extended_EREC_IraIktatoKonyvekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch
                              , defaultSearchObject.Extended_EREC_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch)
                && Search.IsIdentical(searchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch
                              , defaultSearchObject.Extended_EREC_PldIratPeldanyokSearch.Extended_EREC_IraIktatoKonyvekSearch)
                )
            {
                //default searchobject
                if (Mode == CommandName.Atadandok)
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(
                        Page, Constants.CustomSearchObjectSessionNames.AtadandokSearch);
                }
                else if (Mode == CommandName.Atveendok)
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(
                        Page, Constants.CustomSearchObjectSessionNames.AtveendokSearch);
                }
                else if (Mode == CommandName.AltalanosKereses)
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(
                        Page, Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession(Page, _type);
                }
            }
            else
            {
                //Keres�si felt�telek elment�se kiirathat� form�ban
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                if (Mode == CommandName.Atadandok)
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.AtadandokSearch);
                }
                else if (Mode == CommandName.Atveendok)
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.AtveendokSearch);
                }
                else if (Mode == CommandName.AltalanosKereses)
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.AltalanosKeresesSearch);
                }
                else
                {
                    Search.SetSearchObject(Page, searchObject);
                }
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IraKezbesitesiTetelekSearch GetDefaultSearchObject()
    {
        EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch(true);

        String sajatCsoportId = Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(
            FelhasznaloProfil.FelhasznaloId(Page));

        if (Mode == CommandName.Atadandok)
        {
            //search.Manual_Atado_Sajat.Value = sajatCsoportId;
            //search.Manual_Atado_Sajat.Operator = Query.Operators.equals;

            search.Felhasznalo_Id_Atado_USER.Value = sajatCsoportId;
            search.Felhasznalo_Id_Atado_USER.Operator = Query.Operators.equals;
        }
        else if (Mode == CommandName.Atveendok)
        {
            /*search.Manual_Cimzett_Sajat.Value = sajatCsoportId;
            search.Manual_Cimzett_Sajat.Operator = Query.Operators.equals;*/
        }
        else if (Mode == CommandName.AltalanosKereses)
        {

        }

        return search;
    }

    private void registerJavascripts()
    {


        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        Ch_Kuldemeny.Attributes["onclick"] =
            " enableOrDisableComponents('" + Ch_Kuldemeny.ClientID
                + "',0,['"
                + Kuld_Erkeztetokonyv_IraIktatoKonyvekDropDownList.DropDownList.ClientID
                + "','"
                + Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SzamTol_TextBox.ClientID
                + "','"
                + Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.SzamIg_TextBox.ClientID
                + "','"
                + Kuld_Erkeztetoszam_SzamIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + Kuld_EvIntervallum_SearchFormControl.EvTol_TextBox.ClientID
                + "','"
                + Kuld_EvIntervallum_SearchFormControl.EvIg_TextBox.ClientID
                + "','"
                + Kuld_EvIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + labelKuld_Erkeztetokonyv_Ev.ClientID
                + "','"
                + Kuld_Label2.ClientID
                + "','"
                + Kuld_Label16.ClientID
                // simple search input
                + "','"
                + labelKuldemenySearch.ClientID
                + "','"
                + kuldemenySimpleSearchComponent.ErkeztetoKonyvDropDownList.ClientID
                + "','"
                + kuldemenySimpleSearchComponent.ErkeztetoszamTextBox.ClientID
                + "','"
                + kuldemenySimpleSearchComponent.EvTextBox.ClientID
                + "','"
                + cbKuldemenySimpleSearch.ClientID
                + "']);";

        Ch_Ugyirat.Attributes["onclick"] =
            " enableOrDisableComponents('" + Ch_Ugyirat.ClientID
                + "',0,['"
                + Ugyirat_IktKonyv_IraIktatoKonyvekDropDownList.DropDownList.ClientID
                + "','"
                + Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SzamTol_TextBox.ClientID
                + "','"
                + Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.SzamIg_TextBox.ClientID
                + "','"
                + Ugyirat_Foszam_SzamIntervallum_SearchFormControl1.ImageButton_Masol.ClientID
                + "','"
                + Ugyirat_EvIntervallum_SearchFormControl.EvTol_TextBox.ClientID
                + "','"
                + Ugyirat_EvIntervallum_SearchFormControl.EvIg_TextBox.ClientID
                + "','"
                + Ugyirat_EvIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + labelUgyirat_IktKonyv_Ev.ClientID
                + "','"
                + Ugyirat_Label11.ClientID
                + "','"
                + Ugyirat_Label14.ClientID
                // simple search input
                + "','"
                + labelUgyiratSearch.ClientID
                + "','"
                + ugyiratSimpleSearchComponent.IktatoKonyvDropDownList.ClientID
                + "','"
                + ugyiratSimpleSearchComponent.FoszamTextBox.ClientID
                + "','"
                + ugyiratSimpleSearchComponent.EvTextBox.ClientID
                + "','"
                + cbUgyiratSimpleSearch.ClientID
                + "']);";

        Ch_Iratpeldany.Attributes["onclick"] =
            " enableOrDisableComponents('" + Ch_Iratpeldany.ClientID
                + "',0,['"
                + Pld_IktKonyv_IraIktatoKonyvekDropDownList.DropDownList.ClientID
                + "','"
                + Pld_Foszam_SzamIntervallum_SearchFormControl.SzamTol_TextBox.ClientID
                + "','"
                + Pld_Foszam_SzamIntervallum_SearchFormControl.SzamIg_TextBox.ClientID
                + "','"
                + Pld_Foszam_SzamIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + Pld_Alszam_SzamIntervallum_SearchFormControl.SzamTol_TextBox.ClientID
                + "','"
                + Pld_Alszam_SzamIntervallum_SearchFormControl.SzamIg_TextBox.ClientID
                + "','"
                + Pld_Alszam_SzamIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + Pld_Sorszam_SzamIntervallum_SearchFormControl.SzamTol_TextBox.ClientID
                + "','"
                + Pld_Sorszam_SzamIntervallum_SearchFormControl.SzamIg_TextBox.ClientID
                + "','"
                + Pld_Sorszam_SzamIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + Pld_EvIntervallum_SearchFormControl.EvTol_TextBox.ClientID
                + "','"
                + Pld_EvIntervallum_SearchFormControl.EvIg_TextBox.ClientID
                + "','"
                + Pld_EvIntervallum_SearchFormControl.ImageButton_Masol.ClientID
                + "','"
                + labelPld_IktKonyv_Ev.ClientID
                + "','"
                + Pld_Label12.ClientID
                + "','"
                + Pld_Label15.ClientID
                + "','"
                + Pld_Label17.ClientID
                + "','"
                + Pld_Label18.ClientID
                // simple search input
                + "','"
                + labelPldSearch.ClientID
                + "','"
                + pldSimpleSearchComponent.IktatoKonyvDropDownList.ClientID
                + "','"
                + pldSimpleSearchComponent.FoszamTextBox.ClientID
                + "','"
                + pldSimpleSearchComponent.AlszamTextBox.ClientID
                + "','"
                + pldSimpleSearchComponent.EvTextBox.ClientID
                + "','"
                + pldSimpleSearchComponent.SorszamTextBox.ClientID
                + "','"
                + cbPldSimpleSearch.ClientID
                + "']);";

        if (Mode == CommandName.AltalanosKereses)
        {
            Ch_Kuldemeny.Attributes["onclick"] +=
                " enableOrDisableComponentsByAllCheckBoxes(['"
                + Ch_Kuldemeny.ClientID
                + "','"
                + Ch_Ugyirat.ClientID
                + "','"
                + Ch_Iratpeldany.ClientID
                + "'],1,false,true,['"
                // FTS input
                + labelAltalanosFTS.ClientID
                + "','"
                + TextBoxAltalanosFTS.ClientID
                + "']);";

            Ch_Ugyirat.Attributes["onclick"] +=
                " enableOrDisableComponentsByAllCheckBoxes(['"
                + Ch_Kuldemeny.ClientID
                + "','"
                + Ch_Ugyirat.ClientID
                + "','"
                + Ch_Iratpeldany.ClientID
                + "'],1,false,true,['"
                // FTS input
                + labelAltalanosFTS.ClientID
                + "','"
                + TextBoxAltalanosFTS.ClientID
                + "']);";

            Ch_Iratpeldany.Attributes["onclick"] +=
                " enableOrDisableComponentsByAllCheckBoxes(['"
                + Ch_Kuldemeny.ClientID
                + "','"
                + Ch_Ugyirat.ClientID
                + "','"
                + Ch_Iratpeldany.ClientID
                + "'],1,false,true,['"
                // FTS input
                + labelAltalanosFTS.ClientID
                + "','"
                + TextBoxAltalanosFTS.ClientID
                + "']);";

        }

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "disableComponents", "<script>"
            + Ch_Kuldemeny.Attributes["onclick"]
            + Ch_Ugyirat.Attributes["onclick"]
            + Ch_Iratpeldany.Attributes["onclick"]
            + "</script>");


        #region simple/complex search input
        cbKuldemenySimpleSearch.Attributes["onclick"] =
            " showOrHideComponents('" + cbKuldemenySimpleSearch.ClientID
                + "',0,['"
                + kuldemenySimpleSearchComponent.Container.ClientID
                + "']);";

        cbKuldemenySimpleSearch.Attributes["onclick"] +=
            " showOrHideComponents('" + cbKuldemenySimpleSearch.ClientID
            + "',1,['"
            + ComplexSearchKuldemeny.ClientID
            + "']);";

        cbUgyiratSimpleSearch.Attributes["onclick"] =
            " showOrHideComponents('" + cbUgyiratSimpleSearch.ClientID
                + "',0,['"
                + ugyiratSimpleSearchComponent.Container.ClientID
                + "']);";

        cbUgyiratSimpleSearch.Attributes["onclick"] +=
            " showOrHideComponents('" + cbUgyiratSimpleSearch.ClientID
            + "',1,['"
            + ComplexSearchUgyirat.ClientID
            + "']);";

        cbPldSimpleSearch.Attributes["onclick"] =
            " showOrHideComponents('" + cbPldSimpleSearch.ClientID
                + "',0,['"
                + pldSimpleSearchComponent.Container.ClientID
                + "']);";

        cbPldSimpleSearch.Attributes["onclick"] +=
            " showOrHideComponents('" + cbPldSimpleSearch.ClientID
            + "',1,['"
            + ComplexSearchPld.ClientID
            + "']);";

        #endregion simple/complex search input

        // f�kusz a vonalk�d mez�re
        ScriptManager1.SetFocus(VonalKodListBox1.TextBox);
    }

    private void ShowOrHideComponent(HtmlControl ctrl, bool bShow)
    {
        string displayValue = (bShow ? "inline" : "none");
        if (ctrl.Style["display"] != null)
        {
            ctrl.Style["display"] = displayValue;
        }
        else
        {
            ctrl.Style.Add("display", displayValue);
        }
    }

    private void EnableOrDisableComponent(WebControl ctrl, bool bEnable)
    {
        ctrl.Enabled = bEnable;
    }

    private void EnableOrDisableComponent(HtmlControl ctrl, bool bEnable)
    {
        ctrl.Disabled = !bEnable;
    }
}
