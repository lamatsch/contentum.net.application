﻿using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;

public partial class IraPostaKonyvekForm : Contentum.eUtility.UI.PageBase
{

    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    // CR3355
    private bool isTUK = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        
        // CR3355
        isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:

                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PostaKonyv" + Command);
                break;
        }


        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                execParam.Record_Id = id;

                Result result = service.Get(execParam);
                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = (EREC_IraIktatoKonyvek)result.Record;
                    LoadComponentsFromBusinessObject(erec_IraIktatoKonyvek);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }

        SetComponentsVisibility();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //registerJavascripts();

        FormHeader1.HeaderTitle = Resources.Form.IraPostaKonyvekFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }


    private void SetComponentsVisibility()
    {
        // CR3355
        //LezarasDatuma_CalendarControl.ReadOnly = true;
        Azonosito_TextBox.ReadOnly = true;

        if (Command == CommandName.Modify)
        {
            // CR3355
            KezelesTipusa_RadioButton_E.Enabled = false;
            KezelesTipusa_RadioButton_P.Enabled = false;
        }

        if (Command == CommandName.View)
        {
            Ev_RequiredNumberBox.ReadOnly = true;
            MegkulJelzes_RequiredTextBox.ReadOnly = true;
            Nev_RequiredTextBox.ReadOnly = true;
            Vevokod_RequiredTextBox.ReadOnly = true;
            MegallapodasAzonosito_RequiredTextBox.ReadOnly = true;
            ErvenyessegCalendarControl1.ReadOnly = true;
            // CR3355
            KezelesTipusa_RadioButton_E.Enabled = false;
            KezelesTipusa_RadioButton_P.Enabled = false;
            Terjedelem_RequiredTextBox.ReadOnly = true;
            SelejtezesDatuma_CalendarControl.ReadOnly = true;
            Default_IraIrattariTetelTextBox1.ReadOnly = true;
            LezarasDatuma_CalendarControl.ReadOnly = true;

            //BLG_2549 - LZS
            cbUjranyitando.Enabled = false;
        }
        // CR3355
        SetKezelesiMod();
    }

    // CR3355
    private void SetKezelesiMod()
    {
        //if (KezelesTipusa_RadioButton_E.Checked)
        //{
        //    Terjedelem_RequiredTextBox.Enabled = false;
        //    Terjedelem_RequiredTextBox.Validate = false;
        //    SelejtezesDatuma_CalendarControl.ReadOnly = true;
        //    Default_IraIrattariTetelTextBox1.ReadOnly = true;
        //    LezarasDatuma_CalendarControl.ReadOnly = true;
        //    Terjedelem_RequiredTextBox.Text = String.Empty;
        //    SelejtezesDatuma_CalendarControl.Text = String.Empty;
        //    Default_IraIrattariTetelTextBox1.Id_HiddenField = String.Empty;
        //    Default_IraIrattariTetelTextBox1.Text = "";
        //    LezarasDatuma_CalendarControl.Text = String.Empty;


        //}
        //else
        //{

        //    Terjedelem_RequiredTextBox.Enabled = true;
        //    Terjedelem_RequiredTextBox.Validate = true;
        //    SelejtezesDatuma_CalendarControl.ReadOnly = false;
        //    Default_IraIrattariTetelTextBox1.ReadOnly = false;
        //    LezarasDatuma_CalendarControl.ReadOnly = false;
        //    //tr_terjedelem.Visible = true;
        //    //tr_selejtezes.Visible = true;
        //}
        if (isTUK)
        {
            tr_terjedelem.Visible = true;
            tr_selejtezes.Visible = true;
            tr_ITSZ.Visible = true;
            tr_KezelesTipusa.Visible = true;

            if (KezelesTipusa_RadioButton_E.Checked)
            {
                Terjedelem_RequiredTextBox.Enabled = false;
                Terjedelem_RequiredTextBox.Validate = false;
                SelejtezesDatuma_CalendarControl.ReadOnly = true;
                Default_IraIrattariTetelTextBox1.ReadOnly = true;
                LezarasDatuma_CalendarControl.ReadOnly = true;
                Terjedelem_RequiredTextBox.Text = String.Empty;
                SelejtezesDatuma_CalendarControl.Text = String.Empty;
                Default_IraIrattariTetelTextBox1.Id_HiddenField = String.Empty;
                Default_IraIrattariTetelTextBox1.Text = "";
                LezarasDatuma_CalendarControl.Text = String.Empty;


            }
            else
            {

                Terjedelem_RequiredTextBox.Enabled = true;
                Terjedelem_RequiredTextBox.Validate = true;
                SelejtezesDatuma_CalendarControl.ReadOnly = false;
                Default_IraIrattariTetelTextBox1.ReadOnly = false;
                LezarasDatuma_CalendarControl.ReadOnly = false;
                //tr_terjedelem.Visible = true;
                //tr_selejtezes.Visible = true;
            }
        }
        else
        {
            tr_terjedelem.Visible = false;
            tr_selejtezes.Visible = false;
            tr_ITSZ.Visible = false;
            tr_KezelesTipusa.Visible = false;
            
            //BLG_2549
            //LZS - Szerkeszthető kell legyen a lezárás textbox
            //LezarasDatuma_CalendarControl.ReadOnly = true;
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IraIktatoKonyvek erec_IraIktatoKonyvek)
    {
        Ev_RequiredNumberBox.Text = erec_IraIktatoKonyvek.Ev;
        MegkulJelzes_RequiredTextBox.Text = erec_IraIktatoKonyvek.MegkulJelzes;
        Nev_RequiredTextBox.Text = erec_IraIktatoKonyvek.Nev;
        Vevokod_RequiredTextBox.Text = erec_IraIktatoKonyvek.PostakonyvVevokod;
        MegallapodasAzonosito_RequiredTextBox.Text = erec_IraIktatoKonyvek.PostakonyvMegallapodasAzon;
        Azonosito_TextBox.Text = erec_IraIktatoKonyvek.Azonosito;
        
        ErvenyessegCalendarControl1.ErvKezd = erec_IraIktatoKonyvek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = erec_IraIktatoKonyvek.ErvVege;;

        // CR3355
        LezarasDatuma_CalendarControl.Text = erec_IraIktatoKonyvek.LezarasDatuma;

        //BLG_2549 - LZS -
        cbUjranyitando.Checked = (erec_IraIktatoKonyvek.Ujranyitando == "1") ? true : false;

        Default_IraIrattariTetelTextBox1.Id_HiddenField = erec_IraIktatoKonyvek.DefaultIrattariTetelszam;
        Default_IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);
        Terjedelem_RequiredTextBox.Text = erec_IraIktatoKonyvek.Terjedelem;
        SelejtezesDatuma_CalendarControl.Text = erec_IraIktatoKonyvek.SelejtezesDatuma;
        if (isTUK)
        {
            if (string.IsNullOrEmpty(erec_IraIktatoKonyvek.KezelesTipusa.ToString()))
            {
                KezelesTipusa_RadioButton_E.Checked = true;
                KezelesTipusa_RadioButton_P.Checked = false;
            }
            else
            {
                if (erec_IraIktatoKonyvek.KezelesTipusa == "E")
                {
                    KezelesTipusa_RadioButton_E.Checked = true;
                    KezelesTipusa_RadioButton_P.Checked = false;
                }
                else
                {
                    KezelesTipusa_RadioButton_E.Checked = false;
                    KezelesTipusa_RadioButton_P.Checked = true;
                }
            }
        }
        else
        {
            KezelesTipusa_RadioButton_E.Checked = true;
            KezelesTipusa_RadioButton_P.Checked = false;
        }

        FormHeader1.Record_Ver = erec_IraIktatoKonyvek.Base.Ver;

        FormPart_CreatedModified1.SetComponentValues(erec_IraIktatoKonyvek.Base);;
               
    }

    // form --> business object
    private EREC_IraIktatoKonyvek GetBusinessObjectFromComponents()
    {
        EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = new EREC_IraIktatoKonyvek();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_IraIktatoKonyvek.Updated.SetValueAll(false);
        erec_IraIktatoKonyvek.Base.Updated.SetValueAll(false);

        erec_IraIktatoKonyvek.IktatoErkezteto = Constants.IktatoErkezteto.Postakonyv;
        erec_IraIktatoKonyvek.Updated.IktatoErkezteto = true;

        erec_IraIktatoKonyvek.Ev = Ev_RequiredNumberBox.Text;
        erec_IraIktatoKonyvek.Updated.Ev = pageView.GetUpdatedByView(Ev_RequiredNumberBox);

        erec_IraIktatoKonyvek.MegkulJelzes = MegkulJelzes_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.MegkulJelzes = pageView.GetUpdatedByView(MegkulJelzes_RequiredTextBox);

        erec_IraIktatoKonyvek.Nev = Nev_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.Nev = pageView.GetUpdatedByView(Nev_RequiredTextBox);

        erec_IraIktatoKonyvek.PostakonyvVevokod = Vevokod_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.PostakonyvVevokod = pageView.GetUpdatedByView(Vevokod_RequiredTextBox);

        erec_IraIktatoKonyvek.PostakonyvMegallapodasAzon = MegallapodasAzonosito_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.PostakonyvMegallapodasAzon = pageView.GetUpdatedByView(MegallapodasAzonosito_RequiredTextBox);

        if (Command == CommandName.New)
        {
            // itt nincs értelmezve, de kötelezõ paramétere az insertnek, ezért mindig 0-ra állítjuk
            erec_IraIktatoKonyvek.UtolsoFoszam = "0";
            erec_IraIktatoKonyvek.Updated.UtolsoFoszam = true;
        }

        //erec_IraIktatoKonyvek.ErvKezd = ErvenyessegCalendarControl1.ErvKezd;
        //erec_IraIktatoKonyvek.Updated.ErvKezd = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        //erec_IraIktatoKonyvek.ErvVege = ErvenyessegCalendarControl1.ErvVege;
        //erec_IraIktatoKonyvek.Updated.ErvVege = pageView.GetUpdatedByView(ErvenyessegCalendarControl1);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_IraIktatoKonyvek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));
        // CR3355
        //BLG_2549 - LZS
        erec_IraIktatoKonyvek.LezarasDatuma = !String.IsNullOrEmpty(LezarasDatuma_CalendarControl.Text) ? (LezarasDatuma_CalendarControl.Text.Substring(0, 10)) + " 23:59" : "";
        erec_IraIktatoKonyvek.Updated.LezarasDatuma = pageView.GetUpdatedByView(LezarasDatuma_CalendarControl);

        //BLG_2549 - LZS
        erec_IraIktatoKonyvek.Ujranyitando = cbUjranyitando.Checked ? "1" : "0";
        erec_IraIktatoKonyvek.Updated.Ujranyitando = pageView.GetUpdatedByView(cbUjranyitando);

        erec_IraIktatoKonyvek.DefaultIrattariTetelszam = Default_IraIrattariTetelTextBox1.Id_HiddenField;
        erec_IraIktatoKonyvek.Updated.DefaultIrattariTetelszam = pageView.GetUpdatedByView(Default_IraIrattariTetelTextBox1);

        erec_IraIktatoKonyvek.Terjedelem = Terjedelem_RequiredTextBox.Text;
        erec_IraIktatoKonyvek.Updated.Terjedelem = pageView.GetUpdatedByView(Terjedelem_RequiredTextBox);

        erec_IraIktatoKonyvek.SelejtezesDatuma = SelejtezesDatuma_CalendarControl.Text;
        erec_IraIktatoKonyvek.Updated.SelejtezesDatuma = pageView.GetUpdatedByView(SelejtezesDatuma_CalendarControl);

        if (KezelesTipusa_RadioButton_E.Checked)
        {
            erec_IraIktatoKonyvek.KezelesTipusa = "E";
            erec_IraIktatoKonyvek.Updated.KezelesTipusa = pageView.GetUpdatedByView(KezelesTipusa_RadioButton_E);
        }
        else
        {
            erec_IraIktatoKonyvek.KezelesTipusa = "P";
            erec_IraIktatoKonyvek.Updated.KezelesTipusa = pageView.GetUpdatedByView(KezelesTipusa_RadioButton_P);
        }

        erec_IraIktatoKonyvek.Base.Ver = FormHeader1.Record_Ver;
        erec_IraIktatoKonyvek.Base.Updated.Ver = true;

        return erec_IraIktatoKonyvek;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(Ev_RequiredNumberBox);
            compSelector.Add_ComponentOnClick(MegkulJelzes_RequiredTextBox);
            compSelector.Add_ComponentOnClick(Nev_RequiredTextBox);
            compSelector.Add_ComponentOnClick(Vevokod_RequiredTextBox);
            compSelector.Add_ComponentOnClick(MegallapodasAzonosito_RequiredTextBox);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            // CR3355
            compSelector.Add_ComponentOnClick(LezarasDatuma_CalendarControl);

            compSelector.Add_ComponentOnClick(Terjedelem_RequiredTextBox);
            compSelector.Add_ComponentOnClick(SelejtezesDatuma_CalendarControl);
            compSelector.Add_ComponentOnClick(KezelesTipusa_RadioButton_E);
            compSelector.Add_ComponentOnClick(KezelesTipusa_RadioButton_P);
            compSelector.Add_ComponentOnClick(Default_IraIrattariTetelTextBox1);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                            EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_IraIktatoKonyvek);
                            
                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_RequiredTextBox.Text))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                }
                                else
                                {
                                    Contentum.eUtility.JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);

                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
                                EREC_IraIktatoKonyvek erec_IraIktatoKonyvek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_IraIktatoKonyvek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    // CR3355
    protected void KezelesTipusa_RadioButton_CheckedChanged(object sender, EventArgs e)
    {
        SetKezelesiMod();
    }

    //private void registerJavascripts()
    //{

    //    JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
    //            , "Javascripts/CheckBoxes.js");

    //}
}
