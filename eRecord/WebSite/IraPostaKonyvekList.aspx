<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" 
CodeFile="IraPostaKonyvekList.aspx.cs" Inherits="IraPostaKonyvekList" Title="Postak�nyvek lek�rdez�se" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/JogosultakSubListHeader.ascx" TagName="JogosultakSubListHeader" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/JogosultakTabPage.ascx" TagName="JogosultakTabPage" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />
    
    
<%--HiddenFields--%> 

<asp:HiddenField
    ID="HiddenField1" runat="server" />
<asp:HiddenField
    ID="MessageHiddenField" runat="server" />
<%--/HiddenFields--%> 
    
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="IraPostaKonyvekCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
    <asp:UpdatePanel ID="IraPostaKonyvekUpdatePanel" Runat="server" OnLoad="IraPostaKonyvekUpdatePanel_Load">
        <ContentTemplate>
            <ajaxToolkit:CollapsiblePanelExtender ID="IraPostaKonyvekCPE" runat="server" TargetControlID="Panel1"
                CollapsedSize="20" Collapsed="False" ExpandControlID="IraPostaKonyvekCPEButton" CollapseControlID="IraPostaKonyvekCPEButton"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="IraPostaKonyvekCPEButton"
                ExpandedSize="0" ExpandedText="K�ldem�nyek list�ja" CollapsedText="K�ldem�nyek list�ja">
            </ajaxToolkit:CollapsiblePanelExtender>
            <asp:Panel ID="Panel1" runat="server">
             <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">  
                                
                <asp:GridView ID="IraPostaKonyvekGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                    OnRowCommand="IraPostaKonyvekGridView_RowCommand" OnPreRender="IraPostaKonyvekGridView_PreRender"
                    OnSorting="IraPostaKonyvekGridView_Sorting" OnRowDataBound="IraPostaKonyvekGridView_RowDataBound"
                    AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                            <HeaderTemplate>
                                <!-- OnCommand="GridViewSelectAllCheckBox" -->
                                <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                &nbsp;&nbsp;
                                <!-- OnCommand="GridViewDeselectAllCheckBox" -->
                                <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                    AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage" HeaderStyle-CssClass="GridViewSelectRowImage" 
                                                ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>" SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                            <HeaderStyle Width="25px" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:CommandField>                        
                        <asp:BoundField DataField="Ev" HeaderText="�v" SortExpression="Ev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MegkulJelzes" HeaderText="Postak�nyv&nbsp;jele" SortExpression="MegkulJelzes">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nev" HeaderText="Megnevez�s" SortExpression="Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                       <%-- <asp:BoundField DataField="ErvKezd" HeaderText="�rv.Kezd." SortExpression="ErvKezd">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ErvVege" HeaderText="�rv.V�ge" SortExpression="ErvVege">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>--%>
                        <asp:BoundField DataField="PostakonyvVevokod" HeaderText="Vev�k�d" SortExpression="PostakonyvVevokod">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PostakonyvMegallapodasAzon" HeaderText="Meg�llapod�s azonos�t�" SortExpression="Postakonyv_MegallapodasAzon">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                                                
                        <asp:BoundField DataField="Azonosito" HeaderText="Azonos�t�" SortExpression="Azonosito">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>    
                         <%--CR3355--%>
                          <asp:BoundField DataField="KezelesTipusa" HeaderText="Kezel�s t�pusa" SortExpression="KezelesTipusa" Visible="false">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                          <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="ITSZ" SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam" Visible="false">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                         </asp:BoundField>
                           <asp:BoundField DataField="Terjedelem" HeaderText="Terjedelem" SortExpression="Terjedelem" Visible="false">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                          <asp:BoundField DataField="LezarasDatuma" HeaderText="Lez�r�s&nbsp;d�tuma" SortExpression="LezarasDatuma"> 
                             <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />                                                                                                                              
                       </asp:BoundField>   
                           <asp:BoundField DataField="SelejtezesDatuma" HeaderText="Selejtez�s d�tuma" SortExpression="SelejtezesDatuma" Visible="false">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                          
                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">                                            
                            <HeaderTemplate>
                                <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                            </ItemTemplate>
                       </asp:TemplateField>
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
                </td>
                                 </tr>
                             </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
           </td>
        </tr>
    <tr>
    <td style="text-align: left; height: 8px;" colspan="2">
    </td>
    </tr>
       <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="DetailCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                 <asp:UpdatePanel ID="UpdatePanel_Detail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left" colspan="2">
                            
                                        <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                            TargetControlID="Panel8" CollapsedSize="20" Collapsed="False" ExpandControlID="DetailCPEButton"
                                            CollapseControlID="DetailCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                                            AutoExpand="false" ScrollContents="false" CollapsedImage="images/hu/Grid/plus.gif"
                                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="DetailCPEButton">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="Panel8" runat="server">
                                            <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Width="100%"
                                            OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                            OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">
                                                <ajaxToolkit:TabPanel ID="CsoportokTabPanel" runat="server" TabIndex="0">
                                                    <HeaderTemplate>
                                                        <asp:UpdatePanel ID="LabelUpdatePanelIdeIktatok" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="Header1" runat="server" Text="Post�z�k"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <asp:UpdatePanel ID="CsoportokUpdatePanel" runat="server" OnLoad="CsoportokUpdatePanel_Load">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="CsoportokPanel" runat="server" Visible="false" Width="100%">
                                                                    <uc1:SubListHeader ID="CsoportokSubListHeader" runat="server" />
                                                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                                <ajaxToolkit:CollapsiblePanelExtender ID="CsoportokCPE" runat="server" TargetControlID="Panel2"
                                                                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                    AutoExpand="false" ExpandedSize="0">
                                                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                                                <asp:Panel ID="Panel2" runat="server">
                                                                                    <asp:GridView ID="CsoportokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                                        AllowSorting="True" AutoGenerateColumns="false" OnSorting="CsoportokGridView_Sorting"
                                                                                        OnPreRender="CsoportokGridView_PreRender" OnRowCommand="CsoportokGridView_RowCommand"
                                                                                        DataKeyNames="Id">
                                                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                                <HeaderTemplate>
                                                                                                    <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                    &nbsp;&nbsp;
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                                <HeaderStyle Width="25px" />
                                                                                                <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            </asp:CommandField>
                                                                                            <asp:BoundField DataField="Csoport_Iktathat_Nev" HeaderText="Post�z�hely&nbsp;neve" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                                SortExpression="Csoport_Iktathat_Nev" HeaderStyle-Width="250px" />
                                                                                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s&nbsp;ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                                SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                                <ajaxToolkit:TabPanel ID="JogosultakTabPanel" runat="server" TabIndex="1">
                                                    <HeaderTemplate>
                                                        <asp:UpdatePanel ID="LabelUpdatePanelJogosultak" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text="Jogosultak"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </HeaderTemplate>
                                                    <ContentTemplate>
                                                        <asp:UpdatePanel ID="JogosultakUpdatePanel" runat="server" OnLoad="JogosultakUpdatePanel_Load">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="JogosultakPanel" runat="server" Visible="false" Width="100%">
                                                                    <uc2:JogosultakSubListHeader ID="JogosultakSubListHeader" runat="server" />
                                                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                                                                <ajaxToolkit:CollapsiblePanelExtender ID="JogosultakCPE" runat="server" TargetControlID="Panel4"
                                                                                    CollapsedSize="20" Collapsed="False" ExpandDirection="Vertical" AutoCollapse="false"
                                                                                    AutoExpand="false" ExpandedSize="0">
                                                                                </ajaxToolkit:CollapsiblePanelExtender>
                                                                                <asp:Panel ID="Panel4" runat="server">
                                                                                    <asp:GridView ID="JogosultakGridView" runat="server" CellPadding="0" CellSpacing="0"
                                                                                        BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                                                                        AllowSorting="True" AutoGenerateColumns="false" OnSorting="JogosultakGridView_Sorting"
                                                                                        OnPreRender="JogosultakGridView_PreRender" OnRowCommand="JogosultakGridView_RowCommand"
                                                                                        DataKeyNames="Id">
                                                                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                                                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                                                                        <HeaderStyle CssClass="GridViewHeaderStyle" />
                                                                                        <Columns>
                                                                                            <asp:TemplateField>
                                                                                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                                                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                                                                <HeaderTemplate>
                                                                                                    <asp:ImageButton ID="SelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                                                                    &nbsp;&nbsp;
                                                                                                    <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                                                                        runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:CommandField ButtonType="Image" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"  SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                                                                <HeaderStyle Width="25px" />
                                                                                                <ItemStyle BorderWidth="1" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            </asp:CommandField>
                                                                                            <asp:BoundField DataField="Nev" HeaderText="N�v" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                                SortExpression="Nev" HeaderStyle-Width="250px" />
                                                                                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="Hozz�rendel�s&nbsp;ideje" ItemStyle-CssClass="GridViewBoundFieldItemStyle"
                                                                                                SortExpression="LetrehozasIdo" HeaderStyle-Width="200px" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </asp:Panel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                
                                            </ajaxToolkit:TabContainer>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                          </ContentTemplate>
                </asp:UpdatePanel>
                        </td>
                    </tr>
            </table>
</asp:Content>

