using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public partial class IraPostaKonyvekList : Contentum.eUtility.UI.PageBase
{      
    
    UI ui = new UI();
    // CR3355
    int kezelesModja_Index;
    private bool isTUK = false;

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "PostaKonyvekList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        // CR3355
        isTUK = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.TUK, false);
        if (isTUK)
        {
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("KezelesTipusa"))].Visible = true;
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("Terjedelem"))].Visible = true;
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"))].Visible = true;
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("SelejtezesDatuma"))].Visible = true;
        }
        else
        {
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("KezelesTipusa"))].Visible = false;
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("Terjedelem"))].Visible = false;
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"))].Visible = false;
            IraPostaKonyvekGridView.Columns[IraPostaKonyvekGridView.Columns.IndexOf(getGridViewDataColumn("SelejtezesDatuma"))].Visible = false;

            //IraPostaKonyvekGridView.Columns.Remove(getGridViewDataColumn("KezelesTipusa"));
            //IraPostaKonyvekGridView.Columns.Remove(getGridViewDataColumn("Terjedelem"));
            //IraPostaKonyvekGridView.Columns.Remove(getGridViewDataColumn("EREC_IraIrattariTetelek.IrattariTetelszam"));
            //IraPostaKonyvekGridView.Columns.Remove(getGridViewDataColumn("SelejtezesDatuma"));
            ////            IraPostaKonyvekGridView.DataBind();
            //IraPostaKonyvekGridViewBind();
        }
        kezelesModja_Index = 0;

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
        CsoportokSubListHeader.RowCount_Changed += new EventHandler(CsoportokSubListHeader_RowCount_Changed);
       
        CsoportokSubListHeader.ErvenyessegFilter_Changed += new EventHandler(CsoportokSubListHeader_ErvenyessegFilter_Changed);

        JogosultakSubListHeader.RowCount_Changed += new EventHandler(JogosultakSubListHeader_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ListHeader1.HeaderLabel = Resources.List.IraPostaKonyvekListHeaderTitle;
        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.PostaKonyvekSearch;
        ListHeader1.SearchObjectType = typeof(EREC_IraIktatoKonyvekSearch);

        // A baloldali, alapb�l invisible ikonok megjelen�t�se:
        ListHeader1.PrintVisible = true;

        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;
        ListHeader1.SSRSPrintVisible = true;

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);
        JavaScripts.RegisterActiveTabChangedClientScript(Page, TabContainer1);
        ScriptManager1.RegisterAsyncPostBackControl(TabContainer1);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IraPostaKonyvekSearch.aspx", ""
            , Defaults.PopupWidth, Defaults.PopupHeight, IraPostaKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IraPostaKonyvekForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, IraPostaKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IraPostaKonyvekGridView.ClientID);
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("PostakonyvekPrintForm.aspx");

        string column_v = "";
        for (int i = 0; i < IraPostaKonyvekGridView.Columns.Count; i++)
        {
            if (IraPostaKonyvekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraPostaKonyvekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IraPostaKonyvekGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IraPostaKonyvekGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IraPostaKonyvekGridView.ClientID);


        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IraPostaKonyvekGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IraPostaKonyvekUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IraPostaKonyvekGridView;

        CsoportokSubListHeader.ModifyVisible = false;

        CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        CsoportokSubListHeader.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(CsoportokGridView.ClientID);
        CsoportokSubListHeader.ButtonsClick += new CommandEventHandler(CsoportokSubListHeader_ButtonsClick);

        JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        JogosultakSubListHeader.DeleteOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(JogosultakGridView.ClientID);
        JogosultakSubListHeader.ButtonsClick += new CommandEventHandler(JogosultakSubListHeader_ButtonsClick);

        //selectedRecordId kezel�se
        CsoportokSubListHeader.AttachedGridView = CsoportokGridView;
        JogosultakSubListHeader.AttachedGridView = JogosultakGridView;

        ui.SetClientScriptToGridViewSelectDeSelectButton(IraPostaKonyvekGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IraIktatoKonyvekSearch(),Constants.CustomSearchObjectSessionNames.PostaKonyvekSearch);

        if (!IsPostBack) IraPostaKonyvekGridViewBind();

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.New);
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.View);
        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.Modify);
        ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.Invalidate);
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.ViewHistory);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        ListHeader1.IktatokonyvLezarasEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.Lezaras);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "PostaKonyv" + CommandName.Lock);

        CsoportokPanel.Visible = FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiList");
/**/        JogosultakPanel.Visible = true;


        CsoportokSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        CsoportokSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        CsoportokSubListHeader.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Modify);
        CsoportokSubListHeader.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);

        JogosultakSubListHeader.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.New);
        JogosultakSubListHeader.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.View);
        JogosultakSubListHeader.DeleteEnabled = FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + CommandName.Invalidate);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            if (String.IsNullOrEmpty(MasterListSelectedRowId))
            {
                ActiveTabClear();
            }
            //ActiveTabRefreshOnClientClicks();
            ActiveTabRefreshDetailList(TabContainer1.ActiveTab);
        }

        string column_v = "";
        for (int i = 0; i < IraPostaKonyvekGridView.Columns.Count; i++)
        {
            if (IraPostaKonyvekGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IraPostaKonyvekSSRS.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
    }

    #endregion

    #region Master List

    protected void IraPostaKonyvekGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IraPostaKonyvekGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IraPostaKonyvekGridView", ViewState);

        IraPostaKonyvekGridViewBind(sortExpression, sortDirection);
    }

    protected void IraPostaKonyvekGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //if (!IsPostBack)
        //{
        //    if (!Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.PostaKonyvekSearch))
        //    {
        //        search = (EREC_IraIktatoKonyvekSearch)Search.CreateSearchObjectWithDefaultFilter(typeof(EREC_IraIktatoKonyvekSearch), Page);

        //        // sessionbe ment�s
        //        Search.SetSearchObject_CustomSessionName(Page, search, Constants.CustomSearchObjectSessionNames.PostaKonyvekSearch);
        //    }
        //}

        var search =
            //(EREC_IraIktatoKonyvekSearch)Search.GetSearchObject(Page, new EREC_IraIktatoKonyvekSearch());
            (EREC_IraIktatoKonyvekSearch)Search.GetSearchObject_CustomSessionName(
                Page, new EREC_IraIktatoKonyvekSearch(), Constants.CustomSearchObjectSessionNames.PostaKonyvekSearch);

        search.IktatoErkezteto.Filter(Constants.IktatoErkezteto.Postakonyv);

        //if (!isTUK)
        //{
        //    search.KezelesTipusa.Value = "E";
        //    search.KezelesTipusa.Operator = Query.Operators.equals;
        //}

        search.OrderBy = Search.GetOrderBy("IraPostaKonyvekGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        //Result res = service.GetAllWithExtension(ExecParam, search);
        Result res = service.GetAllWithExtensionAndJogosultsag(ExecParam, search,true);

        UI.GridViewFill(IraPostaKonyvekGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void IraPostaKonyvekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        // CR3355
        //if (kezelesModja_Index == 0)
        //{
            kezelesModja_Index = GetColumnIndexByName(e.Row, "KezelesTipusa");
        //}
        if (kezelesModja_Index < e.Row.Cells.Count)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[kezelesModja_Index].Text.Equals("E"))
                {
                    e.Row.Cells[kezelesModja_Index].Text = Constants.IktatokonyKezelesTipusa.Elektronikus;
                }
                else if (e.Row.Cells[kezelesModja_Index].Text.Equals("P"))
                {
                    e.Row.Cells[kezelesModja_Index].Text = Constants.IktatokonyKezelesTipusa.Papir;
                }
            }
        }
    }

    protected int GetColumnIndexByName(GridViewRow row, string columnName)
    {
        int columnIndex = 0;
        foreach (DataControlFieldCell cell in row.Cells)
        {
            if (cell.ContainingField is BoundField)
                if (((BoundField)cell.ContainingField).DataField.Equals(columnName))
                    break;
            columnIndex++;
        }
        return columnIndex;
    }

    protected void IraPostaKonyvekGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IraPostaKonyvekGridView.PageIndex;

        IraPostaKonyvekGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IraPostaKonyvekGridView.PageCount;

        if (prev_PageIndex != IraPostaKonyvekGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IraPostaKonyvekCPE);
            IraPostaKonyvekGridViewBind();
            ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IraPostaKonyvekCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IraPostaKonyvekGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IraPostaKonyvekGridViewBind();
    }

    protected void IraPostaKonyvekGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IraPostaKonyvekGridView, selectedRowNumber, "check");

            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            if (Header1.Text.IndexOf(" (") > 0)
                Header1.Text = Header1.Text.Remove(Header1.Text.IndexOf(" ("));
            if (Label1.Text.IndexOf(" (") > 0)
                Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));

            // CR3355
            GridViewRow gvr = (sender as GridView).Rows[selectedRowNumber];
            SetDetailTabByKezelesTipus(gvr, id);
            ////if (kezelesModja_Index == 0)
            ////{
            //kezelesModja_Index = GetColumnIndexByName(gvr, "KezelesTipusa");
            ////}
            //if (kezelesModja_Index < gvr.Cells.Count)
            //{
            //    if (gvr.Cells[kezelesModja_Index].Text == Constants.IktatokonyKezelesTipusa.Papir)
            //    {
            //        TabContainer1.Visible = false;
            //        DetailCPEButton.Visible = false;
            //        UpdatePanel_Detail.Update();

            //        //tr_DetailRow.Visible = false;
            //    }
            //    else
            //    {
            //        TabContainer1.Visible = true;
            //        DetailCPEButton.Visible = true;
            //        UpdatePanel_Detail.Update();

            //        //tr_DetailRow.Visible = true;
            //        ActiveTabRefresh(TabContainer1, id);
            //    }
            //} else
            //{
            //    TabContainer1.Visible = true;
            //    DetailCPEButton.Visible = true;
            //    UpdatePanel_Detail.Update();

            //    //tr_DetailRow.Visible = true;
            //    ActiveTabRefresh(TabContainer1, id);
            //}

        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IraPostaKonyvekForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   , Defaults.PopupWidth, Defaults.PopupHeight, IraPostaKonyvekUpdatePanel.ClientID);
            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraPostaKonyvekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IraPostaKonyvekUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IraIktatoKonyvek";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IraPostaKonyvekUpdatePanel.ClientID);

            CsoportokSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraPostazohelyekForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.PostakonyvId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
            ListHeader1.PrintOnClientClick = "javascript:window.open('IraPostaKonyvekPrintForm.aspx?" + QueryStringVars.PostakonyvId + "=" + id + "')";

            JogosultakSubListHeader.NewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.IktatokonyvId + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);

        }
    }

    protected void IraPostaKonyvekUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IraPostaKonyvekGridViewBind();

                    // CR3355
                    GridViewRow gvr = IraPostaKonyvekGridView.SelectedRow;
                    SetDetailTabByKezelesTipus(gvr, UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));           
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIraPostaKonyvek();
            IraPostaKonyvekGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraPostaKonyvRecords();
                IraPostaKonyvekGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIraPostaKonyvRecords();
                IraPostaKonyvekGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIraPostaKonyvek();
                break;
        }
    }

    private void LockSelectedIraPostaKonyvRecords()
    {
        LockManager.LockSelectedGridViewRecords(IraPostaKonyvekGridView, "EREC_IraIktatoKonyvek"
            , "PostaKonyvLock", "PostaKonyvForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraPostaKonyvRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IraPostaKonyvekGridView, "EREC_IraIktatoKonyvek"
            , "PostaKonyvLock", "PostaKonyvForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    /// <summary>
    /// T�rli a IraPostaKonyvekGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedIraPostaKonyvek()
    {
        if (FunctionRights.GetFunkcioJog(Page, "PostaKonyvInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IraPostaKonyvekGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IraIktatoKonyvekService service = eRecordService.ServiceFactory.GetEREC_IraIktatoKonyvekService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraPostaKonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIraPostaKonyvek()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IraPostaKonyvekGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IraIktatoKonyvek");
        }
    }

    protected void IraPostaKonyvekGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IraPostaKonyvekGridViewBind(e.SortExpression, UI.GetSortToGridView("IraPostaKonyvekGridView", ViewState, e.SortExpression));
        ActiveTabClear();
    }

    // CR3355
    private DataControlField getGridViewDataColumn(string name)
    {
        foreach (DataControlField dcf in IraPostaKonyvekGridView.Columns)
        {
            if (dcf is BoundField)
            {
                if (dcf.SortExpression == name)
                {

                    return dcf;
                }
            }
        }
        return null;
    }

    // CR3355
    private void SetDetailTabByKezelesTipus(GridViewRow gvr, String selectedId)
    {
        if (gvr != null)
        {
            //GridViewRow gvr = IraPostaKonyvekGridView.SelectedRow;
            //if (kezelesModja_Index == 0)
            //{
            kezelesModja_Index = GetColumnIndexByName(gvr, "KezelesTipusa");
            //}
            if (kezelesModja_Index < gvr.Cells.Count)
            {
                if (gvr.Cells[kezelesModja_Index].Text == Constants.IktatokonyKezelesTipusa.Papir)
                {
                    TabContainer1.Visible = false;
                    DetailCPEButton.Visible = false;
                    UpdatePanel_Detail.Update();

                    //tr_DetailRow.Visible = false;
                }
                else
                {
                    TabContainer1.Visible = true;
                    DetailCPEButton.Visible = true;
                    UpdatePanel_Detail.Update();

                    //tr_DetailRow.Visible = true;
                    ActiveTabRefresh(TabContainer1, selectedId);
                }
            }
            else
            {
                TabContainer1.Visible = true;
                DetailCPEButton.Visible = true;
                UpdatePanel_Detail.Update();

                //tr_DetailRow.Visible = true;
                ActiveTabRefresh(TabContainer1, selectedId);
            }
        }
    }
    #endregion



    #region Detail Tab

    // az �tadott tabpanel tartalm�t friss�ti, ha az az akt�v tab
    protected void ActiveTabRefreshDetailList(AjaxControlToolkit.TabPanel tab)
    {
        if (TabContainer1.ActiveTab.Equals(tab))
        {
            ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
            ActiveTabRefreshOnClientClicks();
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        if (IraPostaKonyvekGridView.SelectedIndex == -1)
        {
            return;
        }
        string IraPostaKonyvId = UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView);
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer, IraPostaKonyvId);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender, string IraPostaKonyvId)
    {
        switch (sender.ActiveTab.TabIndex)
        {
            case 0:
                CsoportokGridViewBind(IraPostaKonyvId);
                CsoportokPanel.Visible = true;
                break;
            case 1:
                JogosultakGridViewBind(IraPostaKonyvId);
                JogosultakPanel.Visible = true;
                break;
        }
    }

    private void ActiveTabClear()
    {
        switch (TabContainer1.ActiveTabIndex)
        {
            case 0:
                ui.GridViewClear(CsoportokGridView);
                break;
            case 1:
                ui.GridViewClear(JogosultakGridView);
                break;
        }
    }

    private void SetDetailRowCountOnAllTab(string RowCount)
    {
        Session[Constants.DetailRowCount] = RowCount;

        CsoportokSubListHeader.RowCount = RowCount;
        JogosultakSubListHeader.RowCount = RowCount;
    }


    private void ActiveTabRefreshOnClientClicks()
    {
        if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
        {
            CsoportokGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(CsoportokGridView));
        }
        else if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
        {
            JogosultakGridView_RefreshOnClientClicks(UI.GetGridViewSelectedRecordId(JogosultakGridView), UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
        }
    }


    #endregion


    #region Csoportok Detail

    private void CsoportokSubListHeader_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_IdePostazok();
            CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
        }
    }

    protected void CsoportokGridViewBind(string IraPostaKonyvId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("CsoportokGridView", ViewState, "Csoport_Iktathat_Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("CsoportokGridView", ViewState);

        CsoportokGridViewBind(IraPostaKonyvId, sortExpression, sortDirection);
    }

    protected void CsoportokGridViewBind(string IraPostaKonyvId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(IraPostaKonyvId))
        {
            EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IraIktatoKonyvek IraPostaKonyv = new EREC_IraIktatoKonyvek();
            IraPostaKonyv.Id = IraPostaKonyvId;

            EREC_Irat_IktatokonyveiSearch search = new EREC_Irat_IktatokonyveiSearch();
            search.OrderBy = Search.GetOrderBy("CsoportokGridView", ViewState, SortExpression, SortDirection);

            CsoportokSubListHeader.SetErvenyessegFields(search.ErvKezd, search.ErvVege);

            Result res = service.GetAllByIktatoKonyv(ExecParam, IraPostaKonyv, search);

            if (Header1.Text.IndexOf(" (") > 0)
                Header1.Text = Header1.Text.Remove(Header1.Text.IndexOf(" ("));
            Header1.Text += " (" + res.GetCount.ToString() + ")";

            UI.GridViewFill(CsoportokGridView, res, CsoportokSubListHeader, EErrorPanel1, ErrorUpdatePanel);

            ui.SetClientScriptToGridViewSelectDeSelectButton(CsoportokGridView);
        }
        else
        {
            ui.GridViewClear(CsoportokGridView);
        }
    }


    void CsoportokSubListHeader_ErvenyessegFilter_Changed(object sender, EventArgs e)
    {
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
    }

    protected void CsoportokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(CsoportokTabPanel))
                    //{
                    //    CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
                    //}
                    ActiveTabRefreshDetailList(CsoportokTabPanel);
                    break;
            }
        }
    }

    /// <summary>
    /// T�rli a CsoportokGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelected_IdePostazok()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(CsoportokGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_Irat_IktatokonyveiService service = eRecordService.ServiceFactory.GetEREC_Irat_IktatokonyveiService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void CsoportokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(CsoportokGridView, selectedRowNumber, "check");
        }
    }

    private void CsoportokGridView_RefreshOnClientClicks(string erkeztetoKonyvId)
    {
        string id = erkeztetoKonyvId;
        if (!String.IsNullOrEmpty(id))
        {
            CsoportokSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraPostazohelyekForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID);
            CsoportokSubListHeader.ModifyOnClientClick = JavaScripts.SetOnClientClick("IraPostazohelyekForm.aspx"
                , CommandName.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, CsoportokUpdatePanel.ClientID, EventArgumentConst.refreshDetailList);
        }
    }

    protected void CsoportokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = CsoportokGridView.PageIndex;

        CsoportokGridView.PageIndex = CsoportokSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != CsoportokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, CsoportokCPE);
            CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(CsoportokSubListHeader.Scrollable, CsoportokCPE);
        }
        CsoportokSubListHeader.PageCount = CsoportokGridView.PageCount;
        CsoportokSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(CsoportokGridView);
    }


    void CsoportokSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(CsoportokSubListHeader.RowCount);
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
    }

    protected void CsoportokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        CsoportokGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView)
            , e.SortExpression, UI.GetSortToGridView("CsoportokGridView", ViewState, e.SortExpression));
    }

    #endregion

    #region Jogosultak Detail

    private void JogosultakSubListHeader_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelected_Jogosultak();
            JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
        }
    }

    protected void JogosultakGridViewBind(string IraIktatoKonyvId)
    {
        String sortExpression = Search.GetSortExpressionFromViewState("JogosultakGridView", ViewState, "Nev");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("JogosultakGridView", ViewState);

        JogosultakGridViewBind(IraIktatoKonyvId, sortExpression, sortDirection);
    }

    protected void JogosultakGridViewBind(string IraIktatoKonyvId, String SortExpression, SortDirection SortDirection)
    {
        if (!string.IsNullOrEmpty(IraIktatoKonyvId))
        {
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();

            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            Contentum.eQuery.BusinessDocuments.RowRightsCsoportSearch search = new RowRightsCsoportSearch();
            search.OrderBy = Search.GetOrderBy("JogosultakGridView", ViewState, SortExpression, SortDirection);

            Result res = service.GetAllRightedCsoportByJogtargy(ExecParam, IraIktatoKonyvId, search);

            if (Label1.Text.IndexOf(" (") > 0)
                Label1.Text = Label1.Text.Remove(Label1.Text.IndexOf(" ("));
            Label1.Text += " (" + res.Ds.Tables[0].Rows.Count.ToString() + ")";

            UI.GridViewFill(JogosultakGridView, res, JogosultakSubListHeader, EErrorPanel1, ErrorUpdatePanel);

            ui.SetClientScriptToGridViewSelectDeSelectButton(JogosultakGridView);
        }
        else
        {
            ui.GridViewClear(JogosultakGridView);
        }
    }

    private void deleteSelected_Jogosultak()
    {
        if (FunctionRights.GetFunkcioJog(Page, "Irat_IktatokonyveiInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(JogosultakGridView, EErrorPanel1, ErrorUpdatePanel);
            RightsService service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiRemoveCsoportFromJogtargyById(execParams);
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    protected void JogosultakUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshDetailList:
                    //if (TabContainer1.ActiveTab.Equals(JogosultakTabPanel))
                    //{
                    //    JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
                    //}
                    ActiveTabRefreshDetailList(JogosultakTabPanel);
                    break;
            }
        }
    }

    protected void JogosultakGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(JogosultakGridView, selectedRowNumber, "check");
        }
    }

    private void JogosultakGridView_RefreshOnClientClicks(string id, string IktatoKonyvId)
    {
        if (!String.IsNullOrEmpty(id))
        {
            JogosultakSubListHeader.ViewOnClientClick = JavaScripts.SetOnClientClick("IraIktatokonyvJogosultakForm.aspx"
                , CommandName.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                  + "&" + QueryStringVars.IktatokonyvId + "=" + IktatoKonyvId
                , Defaults.PopupWidth, Defaults.PopupHeight, JogosultakUpdatePanel.ClientID);
        }
    }

    protected void JogosultakGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = JogosultakGridView.PageIndex;

        JogosultakGridView.PageIndex = JogosultakSubListHeader.PageIndex;
        string detailRowCount = "";
        if (Session[Constants.DetailRowCount] != null)
        {
            detailRowCount = Session[Constants.DetailRowCount].ToString();
        }

        if (prev_PageIndex != JogosultakGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, JogosultakCPE);
            JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
        }
        else
        {
            UI.GridViewSetScrollable(JogosultakSubListHeader.Scrollable, JogosultakCPE);
        }
        JogosultakSubListHeader.PageCount = JogosultakGridView.PageCount;
        JogosultakSubListHeader.PagerLabel = UI.GetGridViewPagerLabel(JogosultakGridView);
    }


    void JogosultakSubListHeader_RowCount_Changed(object sender, EventArgs e)
    {
        SetDetailRowCountOnAllTab(JogosultakSubListHeader.RowCount);
        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView));
    }

    protected void JogosultakGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        JogosultakGridViewBind(UI.GetGridViewSelectedRecordId(IraPostaKonyvekGridView)
            , e.SortExpression, UI.GetSortToGridView("JogosultakGridView", ViewState, e.SortExpression));
    }

    #endregion

}
