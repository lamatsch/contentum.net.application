using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;
using System.IO;

public partial class IraPostakonyvekPrintForm : Contentum.eUtility.UI.PageBase
{
    private string postakonyvId = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        postakonyvId = Request.QueryString.Get(QueryStringVars.PostakonyvId);

        if (String.IsNullOrEmpty(postakonyvId))
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
    }

    private EREC_KuldKuldemenyekSearch GetSearchObjectFromComponents()
    {
        EREC_KuldKuldemenyekSearch erec_KuldKuldemenyekSearch = new EREC_KuldKuldemenyekSearch();

        erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Value = postakonyvId;
        erec_KuldKuldemenyekSearch.IraIktatokonyv_Id.Operator = Query.Operators.equals;
        DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_KuldKuldemenyekSearch.BeerkezesIdeje);
        
        return erec_KuldKuldemenyekSearch;
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
         if (e.CommandName == CommandName.Save)
         {
            var searchObject = GetSearchObjectFromComponents();
            Response.Redirect("PostakonyvekPrintFormSSRS.aspx?PostakonyvId="+postakonyvId+"&DatumKezd="+searchObject.BeerkezesIdeje.Value+"&DatumVege="+searchObject.BeerkezesIdeje.ValueTo);
         }
    }
}
