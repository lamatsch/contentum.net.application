﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IratAtvetelIntezkedesreTomegesForm.aspx.cs" Inherits="IratAtvetelIntezkedesreTomegesForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />
        
    <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <uc1:FormHeader ID="FormHeader1" runat="server" />

    <div> 
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
        
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                    
                  <asp:Panel ID="IratokListPanel" runat="server" Visible="false">
                            <asp:Label ID="IratokGridHeader" runat="server" Text="Iratok:" style="font-weight: bold; text-decoration: underline;"/>
                            <div style="margin-top:3px; max-height:250px;overflow-y:auto">
                                <asp:GridView ID="IraIratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                    BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                    OnRowDataBound="IraIratokGridView_RowDataBound"
                                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="GridViewBorder">
                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                    <HeaderStyle CssClass="GridViewHeaderStyle" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                            <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                                    Visible="false" OnClientClick="return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatokonyvek.Iktatohely, EREC_UgyUgyiratok.Foszam, EREC_IraIratok.Alszam">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="160px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Targy1" HeaderText="Irat&nbsp;tárgy" SortExpression="EREC_IraIratok.Targy">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ElsoIratPeldanyFelelos_Nev" HeaderText="Kezelő" SortExpression="Csoportok_Peldany_FelelosNev.Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                        </asp:BoundField>
                                        <asp:TemplateField SortExpression="Csoportok_UgyintezoNev.Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderTemplate>
                                                <asp:HyperLink ID="headerUgyintezo" runat="server" Text='<%#Contentum.eUtility.ForditasExpressionBuilder.GetForditas("BoundField_IratUgyintezo", "Ügyintéző", "~/IraIratokList.aspx") %>' />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="labelUgyintezo" runat="server" Text='<%#Eval("FelhasznaloCsoport_Id_Ugyintezo_Nev") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <ItemTemplate>
                                                <%-- Átiktatott állapot esetén (06) átiktatás utáni új iktatószám megjelenítése --%>
                                                <asp:Label ID="labelAllapotNev" runat="server" Text='<%# string.Concat(Eval("Allapot_Nev"), (Eval("Allapot") as string) == "06" && (Eval("UjIktatoSzam") as string) != null ? "<br />(" + (Eval("UjIktatoSzam") as string).Replace(" ", "&nbsp;") + ")" : "") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ElsoIratPeldanyOrzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_Peldany_OrzoNev.Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                        </asp:BoundField>
                                        <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                            <HeaderTemplate>
                                                <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Panel ID="Panel_Warning_Irat" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_Irat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:formfooter id="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upResult">
    <ContentTemplate>
    <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
       <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
       <asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
       <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
       <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
        <eui:eformpanel id="EFormPanel2" runat="server" cssclass="mrResultPanel">
                <div class="mrResultPanelText">A kijelölt tételek átvétele sikeresen végrehajtódott.</div>
                <table>
                    <tr>
                        <%--<td>
                            <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/images/hu/ovalgomb/atvetelilistanyomtatas.gif"
                                onmouseover="swapByName(this.id,'atvetelilistanyomtatas2.gif')" onmouseout="swapByName(this.id,'atvetelilistanyomtatas.gif')"
                               CommandName="Print" />
                        </td>--%>
                        <td>
                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                CommandName="Close" />
                        </td>
                    </tr>
                </table>
        </eUI:eFormPanel>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>
