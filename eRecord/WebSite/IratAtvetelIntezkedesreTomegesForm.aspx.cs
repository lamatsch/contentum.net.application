﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Web.UI;

public partial class IratAtvetelIntezkedesreTomegesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private string Command = "";
    private String IratId = "";
    private String[] IratokArray;

    private const string FunkcioKod_IratAtvetelIntezkedesre = "IratAtvetelIntezkedesre";

    public string maxTetelszam = "0";
    
    protected void Page_Init(object sender, EventArgs e)
    {
        this.FormHeader1.HeaderTitle = "Irat átvétel intézkedésre";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratId)))
        {
            if (Session[Constants.SelectedIratIds] != null)
                IratId = Session[Constants.SelectedIratIds].ToString();
        }
        else IratId = Request.QueryString.Get(QueryStringVars.IratId);

        if (!String.IsNullOrEmpty(IratId))
        {
            IratokArray = IratId.Split(',');
        }
        
        // Funkciójog-ellenõrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratAtvetelIntezkedesre);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);
        ImageClose.OnClientClick = "window.returnValue=true; window.close(); return false;";

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    private void LoadFormComponents()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            IratokListPanel.Visible = true;
            FillIratokGridView();
        }
    }

    private void FillIratokGridView()
    {
        var res = IratokGridViewBind();

        // ellenõrzés:
        if (res != null && res.GetCount != IratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        // Van-e olyan rekord, ami nem vehetõ át? 
        // CheckBoxok vizsgálatával:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(IraIratokGridView, "check");

        int count_NEMatvehetok = IratokArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_Irat.Text = String.Format(Resources.List.UI_Irat_AtvetelreKijeloltekWarning, count_NEMatvehetok);
            Panel_Warning_Irat.Visible = true;
        }
    }
  
    protected Result IratokGridViewBind()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            var search = new EREC_IraIratokSearch();
            
            search.Id.In(IratokArray);
            
            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(IraIratokGridView, res, "", FormHeader1.ErrorPanel, null);

            return res;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    public void IratokGridView_RowDataBound_CheckIratAtvetelIntezkedesre(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRow row = ((DataRowView)e.Row.DataItem).Row;

            Iratok.Statusz statusz = Iratok.GetAllapotFromDataRow(row);

            ErrorDetails errorDetails = null;
            //-Az iratnak 'Szignált', vagy 'Elintézett' állapotban kell lennie
            bool ok = (statusz.Allapot == KodTarak.IRAT_ALLAPOT.Szignalt || statusz.Allapot == KodTarak.IRAT_ALLAPOT.Elintezett);
            if (!ok) errorDetails = new ErrorDetails { Message = "Az iratnak szignált, vagy elintézett állapotban kell lennie" };// Iratok.ErrorDetailMessages.IratAllapotaNemMegfelelo
            else
            {
                // - Az irat "Intézkedo ügyintézője" (EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez) megegyezik a műveletet végző felhasználóval
                ok = row["FelhasznaloCsoport_Id_Ugyintez"].ToString().Equals(UI.SetExecParamDefault(page).Felhasznalo_Id, StringComparison.InvariantCultureIgnoreCase);
                if (!ok)
                {
                    var iratUgyintezoMezo = Contentum.eUtility.ForditasExpressionBuilder.GetForditas("BoundField_IratUgyintezo", "Ügyintéző", "~/IraIratokList.aspx");
                    if (iratUgyintezoMezo.Equals("Intézkedő ügyintéző", StringComparison.InvariantCultureIgnoreCase))
                    {
                        errorDetails = new ErrorDetails { Message = "Csak az irat intézkedő ügyintézője végezheti el a műveletet." };
                    }
                    else
                    {
                        errorDetails = new ErrorDetails { Message = "Csak a(z) '" + iratUgyintezoMezo + "' mező által megadott felhasználó végezheti el a műveletet." };
                    }
                }
                else
                {
                    // - Az iratnak van legalább 1 olyan iratpéldánya, amelynek a kezelője a műveletet végző felhasználó.
                    var execParam = UI.SetExecParamDefault(Page);
                    var service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    var search = new EREC_PldIratPeldanyokSearch();
                    search.IraIrat_Id.Filter(statusz.Id);
                    search.Csoport_Id_Felelos.Filter(Contentum.eUtility.Csoportok.GetFelhasznaloSajatCsoportId(execParam));

                    Result result = service.GetAll(execParam.Clone(), search);

                    if (result.IsError)
                    {
                        errorDetails = statusz.CreateErrorDetail(ResultError.GetErrorMessageFromResultObject(result));
                        ok = false;
                    }
                    else if (result.GetCount == 0)
                    {
                        errorDetails =  new ErrorDetails { Message = "Az iratnak nincs olyan iratpéldánya, aminek a felhasználó a kezelője." };
                        ok = false;
                    }
                }
            }

            UI.SetRowCheckboxAndInfo(ok, e, errorDetails, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, statusz.Id, page);
        }
    }

    protected void IraIratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        IratokGridView_RowDataBound_CheckIratAtvetelIntezkedesre(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
        }
    }
    
    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratAtvetelIntezkedesre))
            {
                if (String.IsNullOrEmpty(IratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    var selectedItemsList = ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    var iratIds = selectedItemsList.ToArray();
                    Result result = service.AtvetelIntezkedesre(execParam.Clone(), iratIds, "", "");
                    if (result.IsError)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(IraIratokGridView, result);
                    }
                    else
                    {
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                    }
                }
                
            } 
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }
}

