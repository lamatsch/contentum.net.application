<%@ Page Language="C#"
    MasterPageFile="~/EmptyMasterPage.master"
    AutoEventWireup="true" EnableEventValidation="false"
    Inherits="IratElintezesForm" CodeFile="IratElintezesForm.aspx.cs"
    Title="Iratok elint�z�se" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc7" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="uc11" %>
<%@ Register Src="Component/FormTemplateLoader.ascx" TagName="FormTemplateLoader" TagPrefix="uc6" %>
<%@ Register Src="~/Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc" %>
<%@ Register Src="~/Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc" %>
<%@ Register Src="~/Component/EditablePartnerTextBox.ascx" TagName="EditablePartnerTextBox" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        div.blokk {
            margin: 5px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IratokFormHeaderTitle%>" />

            <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

            <%if (Contentum.eRecord.Utility.FelhasznaloProfil.OrgIsNMHH(Page))
                {%>
            <div class="blokk">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="IratHatasaUgyintezesre_Label" runat="server" Text="Irat hat�sa az �gyint�z�sre (sakk�ra):" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:KodtarakDropDownList ID="IratHatasaUgyintezesre_KodtarakDropDownList" runat="server" CssClass="mrUrlapInput" ReadOnly="true" Enabled="false" />
                                    <asp:Label ID="Label_ErrorOnSakkoraEllenorzes" runat="server" Text="<%$Resources:Form,IratElintezesSakkoraNemMegfelelo%>" ForeColor="Red" Visible="false" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </div>
            <%}%>

            <div class="blokk">
                <uc11:KezelesiFeljegyzesPanel ID="KezelesiFeljegyzesPanel1" runat="server" />
            </div>

            <div class="blokk">
                <eUI:eFormPanel ID="HataridoKorrekciokBlokk" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHataridoKorrekcio" runat="server" Text="�zemzavar vagy m�s elh�r�tatlan esem�ny:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:CheckBox ID="cbHataridoKorrekcio" runat="server" CssClass="urlapCheckbox" AutoPostBack="true" onclick="Page_BlockSubmit = false;" />
                                </td>
                            </tr>
                            <tr id="trIdotartam" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelIdotartam" runat="server" Text="Id�tartam:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <div class="DisableWrap">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <uc:CalendarControl ID="IdotartamKezd" runat="server" ReadOnly="false" Validate="true" TimeVisible="true" />
                                                </td>
                                                <td style="width: 20px; text-align: center">
                                                    <span>-</span>
                                                </td>
                                                <td>
                                                    <uc:CalendarControl ID="IdotartamVege" runat="server" ReadOnly="false" Validate="true" TimeVisible="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trIgazoloIrat" runat="server" class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelIgazoloIrat" runat="server" Text="Igazol� irat nyilv�ntart�si sz�ma:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:RequiredTextBox ID="IgazoloIrat" runat="server" Validate="true" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </div>

            <div class="blokk">
                <eUI:eFormPanel ID="HataridoSzamitasBlokk" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Button ID="btnUgyiontezesiIdoSzamitatsa" runat="server" Text="�gyint�z�si id� sz�m�t�sa" CausesValidation="false" OnClick="btnUgyiontezesiIdoSzamitatsa_Click" />
                                </td>
                                <td class="mrUrlapMezo"></td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelEredetiHatarido" runat="server" Text="Eredeti hat�rid�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:Label ID="EredetiHatarido" runat="server" Text="-" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHataridoMeghosszabbitas" runat="server" Text="Hat�rid� meghosszabb�t�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:Label ID="HataridoMeghosszabbitas" runat="server" Text="-" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHataridoTullepes" runat="server" Text="Hat�rid�-t�ll�p�s:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <asp:Label ID="HataridoTullepes" runat="server" Text="-" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </div>

            <div class="blokk">
                <eUI:eFormPanel ID="VisszafizetesBlokk" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tbody>
                            <tr class="urlapSor" runat="server">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelVisszafizetesJogcime" runat="server" Text="Visszafizet�s jogc�me:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:KodtarakDropDownList ID="VisszafizetesJogcime" runat="server" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelVisszafizetesOsszege" runat="server" Text="Visszafizet�s �sszege (Ft):" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:RequiredNumberBox ID="VisszafizetesOsszege" runat="server" Validate="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHatarozatSzama" runat="server" Text="Hat�rozat sz�ma:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:RequiredTextBox ID="HatarozatSzama" runat="server" Validate="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelVisszafizetesCimzettje" runat="server" Text="Visszafizet�s c�mzettje:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:EditablePartnerTextBox ID="VisszafizetesCimzettje" runat="server" Validate="false" />
                                </td>
                            </tr>
                            <tr class="urlapSor">
                                <td class="mrUrlapCaption">
                                    <asp:Label ID="labelHatarido" runat="server" Text="Hat�rid�:" />
                                </td>
                                <td class="mrUrlapMezo">
                                    <uc:CalendarControl ID="Hatarido" runat="server" Validate="false" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </eUI:eFormPanel>
            </div>

            <uc2:FormFooter ID="FormFooter1" runat="server" />
            <asp:Button ID="ForceSaveButton" runat="server" Style="display: none" />

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
