using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IratElintezesForm : Contentum.eUtility.UI.PageBase
{
    const string kcs_VISSZAFIZETES_JOGCIME = "VISSZAFIZETES_JOGCIME";
    const string kcs_IRAT_HATASA_UGYINTEZESRE = "IRAT_HATASA_UGYINTEZESRE";

    #region PROPERTIES
    private string Command = "";
    Contentum.eUtility.PageView pageView = null;
    string Id;

    const string msg = "Biztos hogy k�szre jelenti az iratot a hat�rid� t�ll�p�s miatt d�j-, illet�k visszafizet�sre vonatkoz� adatok megad�sa n�lk�l?";
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        Id = Request.QueryString.Get(QueryStringVars.Id);

        if (!IsPostBack)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratElintezes");

            if (String.IsNullOrEmpty(Id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }

            try
            {
                EREC_IraIratok irat = Get();
                LoadComponentsFromBusinessObject(irat);

                bool sakkoraEllenorzes = SakkoraEllenorzes(irat);
                if (!sakkoraEllenorzes)
                    ErrorOnSakkoraEllenorzes();
            }
            catch (Exception ex)
            {
                Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }

        InitKezelesiFeljegyzes();

        InitHeaderFooter();

        InitFormTemplate();
    }

    EREC_IraIratok Get()
    {
        EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = Id;

        Result result = service.Get(execParam);
        if (String.IsNullOrEmpty(result.ErrorCode))
        {
            EREC_IraIratok irat = (EREC_IraIratok)result.Record;
            return irat;
        }
        else
        {
            throw new Contentum.eUtility.ResultException(result);
        }
    }

    private void InitHeaderFooter()
    {
        //FormHeader1.FormTemplateLoader1_Visibility = true;
        FormHeader1.FullManualHeaderTitle = Resources.Buttons.ElintezetteNyilvanitas;
    }

    private void InitKezelesiFeljegyzes()
    {
        KezelesiFeljegyzesPanel1.Command = CommandName.ElintezetteNyilvanitas;
        KezelesiFeljegyzesPanel1.ObjektumPanelVisible = false;
        KezelesiFeljegyzesPanel1.IsMemo = false;
        KezelesiFeljegyzesPanel1.IsFeladat = false;
    }

    bool UgyintezesiIdoSzamitasaClicked
    {
        get
        {
            object o = ViewState["UgyintezesiIdoSzamitasaClicked"];

            if (o != null)
                return (bool)o;
            else
                return false;
        }
        set
        {
            ViewState["UgyintezesiIdoSzamitasaClicked"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadInitErrorPanel();

        LoadInitHeaderFooter();

        LoadInitKezelesiFeljegyzes();

        if (IsPostBack)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"];
            if (eventArgument == "ForceSave")
            {
                try
                {
                    Save(true);
                }
                catch (Exception ex)
                {
                    Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }

            }
        }

    }

    private void LoadInitErrorPanel()
    {
        if (FormHeader1.ErrorPanel.Visible)
        {
            FormHeader1.ErrorPanel.Visible = false;
        }
    }
    private void LoadInitHeaderFooter()
    {
        FormHeader1.ButtonsClick += FormHeader1_ButtonsClick;
        FormFooter1.ButtonsClick += FormFooter1_ButtonsClick;
    }
    private void LoadInitKezelesiFeljegyzes()
    {
        KezelesiFeljegyzesPanel1.SetObjektumType(Constants.TableNames.EREC_IraIratok);
        KezelesiFeljegyzesPanel1.SetObjektum(Id, Constants.TableNames.EREC_IraIratok);
        KezelesiFeljegyzesPanel1.ShowOnlyFeljegyzes(5);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        trIdotartam.Visible = cbHataridoKorrekcio.Checked;
        trIgazoloIrat.Visible = cbHataridoKorrekcio.Checked;

        FormFooter1.ImageButton_Save.Enabled = UgyintezesiIdoSzamitasaClicked;

        if (!UgyintezesiIdoSzamitasaClicked)
            FormFooter1.ImageButton_Save.ToolTip = "A m�velet elv�gz�se el�tt az �gyint�z�si id� sz�m�t�sa gombra kell kattintani!";
        else
            FormFooter1.ImageButton_Save.ToolTip = "";
    }


    #region CLICK
    private void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            try
            {
                Save(false);
            }
            catch (Exception ex)
            {
                Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
        }
    }

    private void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        //TemplateHandler(e);
    }

    void Save(bool confirmed)
    {

        if (FunctionRights.GetFunkcioJog(Page, "IratElintezes"))
        {
            if (String.IsNullOrEmpty(Id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                EREC_IraIratok irat = this.Get();
                EREC_IraIratok erec_IraIratok = GetBusinessObjectFromComponents();
                erec_IraIratok.Id = Id;

                bool sakkoraEllenorzes = SakkoraEllenorzes(irat);
                if (!sakkoraEllenorzes)
                    return;

                this.SetHataridok(irat);

                if (!confirmed)
                {
                    if (IsHataridoTullepes(irat))
                    {
                        if (!VisszafizetesiAdatokKitoltve(erec_IraIratok))
                        {
                            string js = String.Format("if (confirm('{0}')) {{ __doPostBack('{1}','ForceSave'); }}", msg, ForceSaveButton.UniqueID);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
                            return;
                        }
                    }
                }

                int uzemzavar = GetUzemzavarIdotartam();

                if (uzemzavar > 0)
                {
                    if (!irat.Typed.Hatarido.IsNull)
                    {
                        erec_IraIratok.Typed.Hatarido = irat.Typed.Hatarido.Value.AddDays(uzemzavar);
                        erec_IraIratok.Updated.Hatarido = true;
                    }
                }

                EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                ExecParam execParam = UI.SetExecParamDefault(Page);
                execParam.Record_Id = Id;

                EREC_HataridosFeladatok erec_HataridosFeladatok = KezelesiFeljegyzesPanel1.GetBusinessObject();
                if (erec_HataridosFeladatok != null)
                {
                    erec_HataridosFeladatok.Tipus = KodTarak.FELADAT_TIPUS.ElintezesMegjegyzes;
                }

                Result result = service.Elintezes(execParam, erec_IraIratok, erec_HataridosFeladatok);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);

                    //Az irat statisztika nincs kit�ltve
                    if (result.ErrorCode == "80416")
                    {
                        string url = String.Format("~/IraIratokForm.aspx?Command=Modify&Id={0}&selectedTab=TabIratJellemzoPanel", Id);
                        url = this.ResolveClientUrl(url);
                        string onclick = JavaScripts.SetOnClientClick(url, null, Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, EFormPanel1.ClientID);
                        string link = "<a href=\"javascript:void(0)\" style=\"text-decoration:underline; font-weight:bold;\" onclick=\"" + onclick + ";\">" + "Irat statisztika kit�lt�se" + "</a>";
                        FormHeader1.ErrorPanel.Body += "<div style=\"padding:3px;\">" + link + "<div>";
                    }
                }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    #endregion

    #region BUSINESS OBJECT
    private void LoadComponentsFromBusinessObject(EREC_IraIratok erec_IraIratok)
    {
        FillIratHatasaDropdownlist(erec_IraIratok);
        IdotartamKezd.Text = erec_IraIratok.UzemzavarKezdete;
        IdotartamVege.Text = erec_IraIratok.UzemzavarVege;
        IgazoloIrat.Text = erec_IraIratok.UzemzavarIgazoloIratSzama;

        if (!String.IsNullOrEmpty(erec_IraIratok.UzemzavarKezdete) ||
            !String.IsNullOrEmpty(erec_IraIratok.UzemzavarVege)
            || !String.IsNullOrEmpty(erec_IraIratok.UzemzavarIgazoloIratSzama))
        {
            cbHataridoKorrekcio.Checked = true;
        }

        Iratok.Hatarido IratHatarido = new Iratok.Hatarido(Page, erec_IraIratok);
        EredetiHatarido.Text = IratHatarido.GetHataridoIdoTartam();
        SetHataridok(erec_IraIratok);

        VisszafizetesJogcime.FillAndSetSelectedValue(kcs_VISSZAFIZETES_JOGCIME, erec_IraIratok.VisszafizetesJogcime, true, FormHeader1.ErrorPanel);
        VisszafizetesOsszege.Text = erec_IraIratok.VisszafizetesOsszege;
        HatarozatSzama.Text = erec_IraIratok.VisszafizetesHatarozatSzama;
        if (!string.IsNullOrEmpty(erec_IraIratok.Partner_Id_VisszafizetesCimzet))
        {
            VisszafizetesCimzettje.Id_HiddenField = erec_IraIratok.Partner_Id_VisszafizetesCimzet;
            VisszafizetesCimzettje.SetPartnerTextBoxById(FormHeader1.ErrorPanel);
        }
        else if (!string.IsNullOrEmpty(erec_IraIratok.Partner_Nev_VisszafizetCimzett))
        {
            VisszafizetesCimzettje.Text = erec_IraIratok.Partner_Nev_VisszafizetCimzett;
        }

        Hatarido.Text = erec_IraIratok.VisszafizetesHatarido;

        FormHeader1.Record_Ver = erec_IraIratok.Base.Ver;
    }

    private EREC_IraIratok GetBusinessObjectFromComponents()
    {
        EREC_IraIratok erec_IraIratok = new EREC_IraIratok();
        erec_IraIratok.Updated.SetValueAll(false);
        erec_IraIratok.Base.Updated.SetValueAll(false);

        erec_IraIratok.IratHatasaUgyintezesre = IratHatasaUgyintezesre_KodtarakDropDownList.SelectedValue;
        erec_IraIratok.Updated.IratHatasaUgyintezesre = pageView.GetUpdatedByView(IratHatasaUgyintezesre_KodtarakDropDownList);

        erec_IraIratok.UzemzavarKezdete = IdotartamKezd.Text;
        erec_IraIratok.Updated.UzemzavarKezdete = pageView.GetUpdatedByView(IdotartamKezd);

        erec_IraIratok.UzemzavarVege = IdotartamVege.Text;
        erec_IraIratok.Updated.UzemzavarVege = pageView.GetUpdatedByView(IdotartamVege);

        erec_IraIratok.UzemzavarIgazoloIratSzama = IgazoloIrat.Text;
        erec_IraIratok.Updated.UzemzavarIgazoloIratSzama = pageView.GetUpdatedByView(IgazoloIrat);

        erec_IraIratok.VisszafizetesJogcime = VisszafizetesJogcime.SelectedValue;
        erec_IraIratok.Updated.VisszafizetesJogcime = pageView.GetUpdatedByView(VisszafizetesJogcime);

        erec_IraIratok.VisszafizetesOsszege = VisszafizetesOsszege.Text;
        erec_IraIratok.Updated.VisszafizetesOsszege = pageView.GetUpdatedByView(VisszafizetesOsszege);

        erec_IraIratok.VisszafizetesHatarozatSzama = HatarozatSzama.Text;
        erec_IraIratok.Updated.VisszafizetesHatarozatSzama = pageView.GetUpdatedByView(HatarozatSzama);

        if (!string.IsNullOrEmpty(VisszafizetesCimzettje.Id_HiddenField))
        {
            erec_IraIratok.Partner_Id_VisszafizetesCimzet = VisszafizetesCimzettje.Id_HiddenField;
            erec_IraIratok.Updated.Partner_Id_VisszafizetesCimzet = pageView.GetUpdatedByView(VisszafizetesCimzettje);
        }
        if (string.IsNullOrEmpty(VisszafizetesCimzettje.Id_HiddenField) && !string.IsNullOrEmpty(VisszafizetesCimzettje.Text))
        {
            erec_IraIratok.Partner_Nev_VisszafizetCimzett = VisszafizetesCimzettje.Text;
            erec_IraIratok.Updated.Partner_Nev_VisszafizetCimzett = pageView.GetUpdatedByView(VisszafizetesCimzettje);
        }

        erec_IraIratok.VisszafizetesHatarido = Hatarido.Text;
        erec_IraIratok.Updated.VisszafizetesHatarido = pageView.GetUpdatedByView(Hatarido);

        erec_IraIratok.Base.Ver = FormHeader1.Record_Ver;
        erec_IraIratok.Base.Updated.Ver = true;

        return erec_IraIratok;
    }
    /// <summary>
    /// FillIratHatasaDropdownlist
    /// </summary>
    /// <param name="irat"></param>
    private void FillIratHatasaDropdownlist(EREC_IraIratok irat)
    {
        if (irat == null)
            return;

        #region OBSOLETE
        //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        //Contentum.eUtility.Sakkora.EnumIratHatasaUgyintezesre actualType = Contentum.eUtility.Sakkora.EnumIratHatasaUgyintezesre.NOTHING;
        //List<string> toStates = new List<string>();

        //EREC_UgyUgyiratok obj_Ugyirat = Contentum.eUtility.Sakkora.GetUgyirat(execParam, irat.Ugyirat_Id);

        //toStates = Contentum.eUtility.Sakkora.SakkoraAllapotok(execParam, obj_Ugyirat, irat.EljarasiSzakasz, false, out actualType);
        //if (toStates != null && toStates.Count >= 0)
        //    IratHatasaUgyintezesre_KodtarakDropDownList.FillDropDownList(kcs_IRAT_HATASA_UGYINTEZESRE, toStates, true, FormHeader1.ErrorPanel);
        //else
        //IratHatasaUgyintezesre_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRAT_HATASA_UGYINTEZESRE, irat.IratHatasaUgyintezesre, true, FormHeader1.ErrorPanel);
        #endregion

        IratHatasaUgyintezesre_KodtarakDropDownList.FillAndSetSelectedValue(kcs_IRAT_HATASA_UGYINTEZESRE, irat.IratHatasaUgyintezesre, true, FormHeader1.ErrorPanel);
    }
    #endregion

    #region KISHERCEG

    #region PROPERTIES
    private Type formTemplateType = typeof(EREC_HataridosFeladatok);
    //private bool IsTemplateMode = false;
    #endregion

    private void InitFormTemplate()
    {
        //FormHeader1.TemplateObjectType = formTemplateType;
        //FormHeader1.FormTemplateLoader1_Visibility = true;
    }
    //private void TemplateHandler(CommandEventArgs e)
    //{
    //    if (e.CommandName == CommandName.LoadTemplate)
    //    {
    //        IsTemplateMode = true;
    //        if (FormHeader1.TemplateObject is EREC_HataridosFeladatok)
    //            LoadComponentsFromBusinessObject((EREC_HataridosFeladatok)FormHeader1.TemplateObject);
    //        IsTemplateMode = false;
    //    }
    //    else if (e.CommandName == CommandName.NewTemplate)
    //    {
    //        IsTemplateMode = true;
    //        EREC_HataridosFeladatok element = GetBusinessObjectFromComponents();
    //        if (element != null)
    //            FormHeader1.NewTemplate(element);
    //        IsTemplateMode = false;
    //    }
    //    else if (e.CommandName == CommandName.SaveTemplate)
    //    {
    //        IsTemplateMode = true;
    //        EREC_HataridosFeladatok element = KezelesiFeljegyzesPanel1.GetBusinessObject();
    //        if (element != null)
    //            FormHeader1.SaveCurrentTemplate(element);
    //        IsTemplateMode = false;
    //    }
    //    else if (e.CommandName == CommandName.InvalidateTemplate)
    //    {
    //        // FormTemplateLoader lekezeli           
    //    }
    //}
    #endregion

    protected void btnUgyiontezesiIdoSzamitatsa_Click(object sender, EventArgs e)
    {
        try
        {
            EREC_IraIratok irat = this.Get();
            SetHataridok(irat);

            bool sakkoraEllenorzes = SakkoraEllenorzes(irat);
            if (sakkoraEllenorzes)
                UgyintezesiIdoSzamitasaClicked = true;
            else
                UgyintezesiIdoSzamitasaClicked = false;
        }
        catch (Exception ex)
        {
            Result result = Contentum.eUtility.ResultException.GetResultFromException(ex);
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
        }
    }

    void SetHataridok(EREC_IraIratok irat)
    {
        HataridoMeghosszabbitas.Text = GetHataridoMeghosszabitasText(irat);
        HataridoTullepes.Text = GetHataridoTullepessText(irat);
    }

    bool IsHataridoTullepes(EREC_IraIratok irat)
    {
        int tullepes = GetHataridoTullepes(irat);

        return tullepes > 0;
    }

    bool VisszafizetesiAdatokKitoltve(EREC_IraIratok irat)
    {
        if (String.IsNullOrEmpty(irat.VisszafizetesJogcime))
            return false;

        if (String.IsNullOrEmpty(irat.VisszafizetesOsszege))
            return false;

        if (String.IsNullOrEmpty(irat.VisszafizetesHatarozatSzama))
            return false;

        if (string.IsNullOrEmpty(irat.Partner_Id_VisszafizetesCimzet) && string.IsNullOrEmpty(irat.Partner_Nev_VisszafizetCimzett))
            return false;

        if (String.IsNullOrEmpty(irat.VisszafizetesHatarido))
            return false;

        return true;
    }

    #region hatarido

    int GetUzemzavarIdotartam()
    {
        if (cbHataridoKorrekcio.Checked)
        {
            DateTime dtKezd = IdotartamKezd.SelectedDate;
            DateTime dtVege = IdotartamVege.SelectedDate;

            if (dtKezd != DateTime.MinValue && dtVege != DateTime.MinValue)
            {
                return (dtVege - dtKezd).Days;
            }
        }

        return 0;
    }

    int GetHataridoMeghosszabitas(EREC_IraIratok irat)
    {
        int meghosszabitas = GetUzemzavarIdotartam();

        if (!irat.Typed.FelfuggesztettNapokSzama.IsNull)
            meghosszabitas += irat.Typed.FelfuggesztettNapokSzama.Value;

        return meghosszabitas;
    }

    string GetHataridoMeghosszabitasText(EREC_IraIratok irat)
    {
        return string.Format("{0} nap", GetHataridoMeghosszabitas(irat));
    }

    int GetHataridoTullepes(EREC_IraIratok irat)
    {
        if (irat.Typed.Hatarido.IsNull)
        {
            return 0;
        }
        else
        {
            DateTime hatarido = irat.Typed.Hatarido.Value;
            int meghosszabitas = GetHataridoMeghosszabitas(irat);
            DateTime now = DateTime.Now;
            int tullepes = (now - hatarido).Days;
            tullepes -= meghosszabitas;

            return Math.Max(0, tullepes);
        }
    }

    string GetHataridoTullepessText(EREC_IraIratok irat)
    {
        return string.Format("{0} nap", GetHataridoTullepes(irat));
    }

    #endregion
    /// <summary>
    /// Sakkora ellenorzes
    /// </summary>
    /// <param name="irat"></param>
    /// <returns></returns>
    private bool SakkoraEllenorzes(EREC_IraIratok irat)
    {
        if (irat == null)
            return true;

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        bool kellSakkoraEllenorzes = Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRAT_ELINT_SAKKORA_FIGYELES, false);

        if (!kellSakkoraEllenorzes)
            return true;

        switch (irat.IratHatasaUgyintezesre)
        {
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_NONE:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_ONPROGRESS:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_PAUSE:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.NULLA_STOP:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Harom:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Het:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Nyolc:
            case Contentum.eUtility.KodTarak.IRAT_HATASA_UGYINTEZESRE_KOD.Tiz:
                return true;
            default:
                break;
        }
        return false;
    }
    /// <summary>
    /// ErrorOnSakkoraEllenorzesPostback
    /// </summary>
    private void ErrorOnSakkoraEllenorzesPostback()
    {
        string js = String.Format(" __doPostBack('{1}','ErrorOnSakkoraEllenorzes'); ", msg, ForceSaveButton.UniqueID);
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), js, js, true);
    }
    /// <summary>
    /// ErrorOnSakkoraEllenorzes
    /// </summary>
    private void ErrorOnSakkoraEllenorzes()
    {
        Label_ErrorOnSakkoraEllenorzes.Visible = true;
        //string script = "alert('" + Resources.Form.IratElintezesSakkoraNemMegfelelo + "'); return false;";
        //ClientScript.RegisterStartupScript(Page.GetType(), "IratElintezesSakkoraAlert", script, true);
    }
}

