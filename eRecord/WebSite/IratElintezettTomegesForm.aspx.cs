﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using System.Web.UI;
using System.Collections.Generic;

public partial class IratElintezettTomegesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private string Command = "";
    private String IratId = "";
    private String[] IratokArray;

    private string funkcioKod = "IratElintezes";

    public string maxTetelszam = "0";
    
    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Iratok tömeges elintézetté nyilvánítása";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratId)))
        {
            if (Session[Constants.SelectedIratIds] != null)
                IratId = Session[Constants.SelectedIratIds].ToString();
        }
        else IratId = Request.QueryString.Get(QueryStringVars.IratId);

        if (!String.IsNullOrEmpty(IratId))
        {
            IratokArray = IratId.Split(',');
        }

        // Funkciójog-ellenõrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, funkcioKod);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);
        ImageClose.OnClientClick = JavaScripts.GetOnCloseClientClickScript();
        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
            + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
    }

    private void LoadFormComponents()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            IratokListPanel.Visible = true;
            FillIratokGridView();
        }
    }

    private void FillIratokGridView()
    {
        var res = IratokGridViewBind();

        // ellenõrzés:
        if (res != null && res.GetCount != IratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        int count_ok = UI.GetGridViewSelectedCheckBoxesCount(IraIratokGridView, "check");

        int count_NEMok = IratokArray.Length - count_ok;

        if (count_NEMok > 0)
        {
            Label_Warning_Irat.Text = Resources.List.UI_NotModifiableItemsCount + count_NEMok.ToString();
            Panel_Warning_Irat.Visible = true;
        }

        Panel_Stat.Visible = count_ok > 0;
    }
  
    protected Result IratokGridViewBind()
    {
        if (IratokArray != null && IratokArray.Length > 0)
        {
            var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            var search = new EREC_IraIratokSearch();
            
            search.Id.In(IratokArray);
            
            Result res = service.GetAllWithExtension(ExecParam, search);

            if (res.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                Panel_Stat.Visible = false;
            }
            else
            {
                ui.GridViewFill(IraIratokGridView, res, "", FormHeader1.ErrorPanel, null);
            }
            return res;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    public void IratokGridView_RowDataBound_CheckElintezettTomeges(GridViewRowEventArgs e, Page page)
    {
        if (e == null || page == null) return;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRow row = ((DataRowView)e.Row.DataItem).Row;

            Iratok.Statusz statusz = Iratok.GetAllapotFromDataRow(row);

            ErrorDetails errorDetails = null;

            var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            var resultCheck = service.CheckIratElintezheto(execParam, statusz.Id);

            bool ok = !resultCheck.IsError;
            if (!ok)
            {
                // [800120]
                var errCode = 0;
                errorDetails = resultCheck.ErrorDetail ?? new ErrorDetails(Int32.TryParse(resultCheck.ErrorCode, out errCode) ? ResultError.GetErrorMessageByErrorCode(errCode) : resultCheck.ErrorMessage, Constants.TableNames.EREC_IraIratok, statusz.Id, null, null);
            }
            else // ok
            {
                ok = Iratok.CheckElintezhetoWithFunctionRight(Page, statusz.Id, out errorDetails);
                if (ok)
                {
                    var adathordozoTipusa = row["AdathordozoTipusa_Nev"].ToString();
                    ok = adathordozoTipusa.ToLower().Contains("elektronikus");
                    if (!ok)
                    {
                        errorDetails = new ErrorDetails { Message = "Csak elektronikus irat nyilvánítható elintézetté." };
                    }
                }
            }
            UI.SetRowCheckboxAndInfo(ok, e, errorDetails, Constants.ErrorDetails.ObjectTypes.EREC_IraIratok, statusz.Id, page);
        }
    }

    protected void IraIratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        IratokGridView_RowDataBound_CheckElintezettTomeges(e, Page);

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
        {
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");
        }
    }

    private List<string> GetSelectedIds()
    {
        return ui.GetGridViewSelectedRows(IraIratokGridView, FormHeader1.ErrorPanel, null);
    }
    
    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, funkcioKod))
            {
                if (String.IsNullOrEmpty(IratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    var service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    var selectedItemsList = GetSelectedIds();

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    var iratIds = selectedItemsList.ToArray();

                    var errorResults = new List<Result>();

                    var batchResult = new Result();
                    batchResult.Ds = new DataSet();
                    batchResult.Ds.Tables.Add();
                    batchResult.Ds.Tables[0].Columns.Add("Id");

                    foreach (var iratId in iratIds)
                    {
                        execParam.Record_Id = iratId;

                        var result = service.Get(execParam);
                        if (!result.IsError && result.Record is EREC_IraIratok)
                        {
                            result = service.Elintezes(execParam, result.Record as EREC_IraIratok, null);
                        }

                        if (result.IsError)
                        {
                            errorResults.Add(result);
                            batchResult.Ds.Tables[0].Rows.Add(iratId);
                        }
                    }

                    if (errorResults.Count > 0)
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, errorResults[0]);
                        UI.MarkFailedRecordsInGridView(IraIratokGridView, batchResult);
                    }
                    else
                    {
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                        ResultError.ResetErrorPanel(FormHeader1.ErrorPanel);
                    }
                }
            } 
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }
}

