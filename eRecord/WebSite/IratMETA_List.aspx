<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IratMETA_List.aspx.cs" Inherits="IratMETA_List" Title="Irat META adatainak list�ja" %>

<%@ Register Src="eRecordComponent/UgyiratDarabMasterAdatok.ascx" TagName="UgyiratDarabMasterAdatok"
    TagPrefix="uc11" %>

<%@ Register Src="eRecordComponent/IrattarDropDownList.ascx" TagName="IrattarDropDownList"
    TagPrefix="uc10" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc8" %>

<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc6" %>

<%@ Register Src="eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc10" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup" TagPrefix="uc" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc" %>
<%@ Register Src="~/Component/DateTimeCompareValidator.ascx" TagPrefix="uc" TagName="DateTimeCompareValidator" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>

    <!--Friss�t�s jelz�se-->
    <uc:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />

    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IratMETAadatFormHeaderTitle %>" />

    <asp:Panel ID="MainPanel" runat="server">
        <uc:InfoModalPopup ID="InfoModalPopup1" runat="server" />

        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td>
                    <asp:Panel ID="panelSimpleMode" runat="server" Visible="true">
                    </asp:Panel>
                    <asp:Panel ID="panelList" runat="server" Visible="true">
                        <table cellpadding="0" cellspacing="0" width="90%">
                            <tr>
                                <td>
                                    <asp:GridView ID="metaDataListGridView" runat="server" CellPadding="0" CellSpacing="0"
                                        BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                                        OnRowDataBound="metaDataListGridView_RowDataBound"
                                        AllowSorting="False" AutoGenerateColumns="False">
                                        <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                        <HeaderStyle CssClass="GridViewHeaderStyle" />

                                        <Columns>
                                            <asp:BoundField DataField="KOD" HeaderText="Irat META adat"
                                                SortExpression="KOD">
                                                <HeaderStyle CssClass="GridViewBorderHeader" HorizontalAlign="Left" Width="150px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerSettings Visible="true" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                                        <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                                        <br />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

