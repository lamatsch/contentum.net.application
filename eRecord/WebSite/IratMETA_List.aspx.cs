﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Data;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;

public partial class IratMETA_List : Contentum.eUtility.UI.PageBase
{
    private string Command = "";

    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;



    private UI ui = new UI();


    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        // Jogosultságellenõrzés:
        //switch (Command)
        //{
        //    case CommandName.DesignView:
        //        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
        //        compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
        //        FormFooter1.Controls.Add(compSelector);
        //        break;
        //    default:
        //        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratLezaras);
        //        break;
        //}



        if (!IsPostBack)
        {
            LoadFormComponents();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        FormHeader1.DisableModeLabel = true;
        FormHeader1.HeaderTitle = Resources.Form.IratMETAadatFormHeaderTitle;
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

    }



    private void LoadFormComponents()
    {
        FillmetaDataListGridView();
    }

    private void FillmetaDataListGridView()
    {
        DataSet ds = metaDataListGridViewBind();

        // ellenõrzés:
        if (ds == null || ds.Tables[0].Rows.Count == 0)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }
    }

    protected DataSet metaDataListGridViewBind()
    {
        EREC_IraIratokService iratokService = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IraIratokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());
        EREC_IraIratokSearch iratokSearch = new EREC_IraIratokSearch();
        iratokSearch.TopRow = 1;

        //Meghívjuk egy rendszerben lévő iratid-ra iratokService.GetForSablon() method-ot:
        string iratId = "";
        Result resIratId = iratokService.GetAllWithExtension(ExecParam, iratokSearch);

        if (!resIratId.IsError)
        {
            foreach (DataRow row in resIratId.Ds.Tables[0].Rows)
            {
                iratId = row["Id"].ToString();
            }
        }

        Result metaDataResult = iratokService.GetForSablon(ExecParam, iratId);

        if (!metaDataResult.IsError)
        {
            ui.GridViewFill(metaDataListGridView, metaDataResult, "", FormHeader1.ErrorPanel, null);

            DataSet ds = metaDataResult.Ds;
            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void metaDataListGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }



    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            JavaScripts.RegisterCloseWindowClientScript(Page);
        }
    }


}
