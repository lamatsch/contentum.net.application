<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IratMetaDefinicioForm.aspx.cs" Inherits="IratMetaDefinicioForm" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc9" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc6" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="~/eRecordComponent/KezelesiFeljegyzesPanel.ascx" TagName="KezelesiFeljegyzesPanel" TagPrefix="kp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelRovidNev" runat="server" Text="R�vid n�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="RovidNevTextBox" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelUgykod" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <asp:UpdatePanel ID="UgytipusKodInputType_UpdatePanel" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelUgytipusKodStar" runat="server" CssClass="ReqStar" Text="*"
                                        Visible="false"></asp:Label>
                                        <asp:Label ID="labelUgytipusKod" runat="server" Text="�gyt�pus k�d:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:RequiredTextBox ID="UgyTipusKodRequiredTextBox" runat="server" Validate="false" />
                                        <uc9:KodtarakDropDownList ID="UgytipusKodtarakDropDownList" runat="server" />
                                        <br />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo">
                                        <asp:RadioButton ID="rbManualOrAuto" GroupName="rbgUgytipusKodInputType" runat="server"
                                            Checked="true" Text="K�zi/Auto" AutoPostBack="true" OnCheckedChanged="UgytipusKodInputType_CheckedChanged" />
                                        <asp:RadioButton ID="rbPredefined" GroupName="rbgUgytipusKodInputType" runat="server"
                                            Checked="false" Text="El�re defini�lt" AutoPostBack="true" OnCheckedChanged="UgytipusKodInputType_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="szignalasTipusaUpdatePanel" runat="server">
                        <ContentTemplate>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelUgytipusStar" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                        <asp:Label ID="labelUgytipus" runat="server" Text="�gyt�pus:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:RequiredTextBox ID="UgyTipusRequiredTextBox" CssClass="mrUrlapNagyInput" runat="server" />
                                    </td>
                                </tr>
                                <tr id="tr_UgyFajta" class="urlapSor" runat="server">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelUgyFajta" runat="server" Text="�gy fajt�ja:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList ID="UgyFajtajaKodtarakDropDownList" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" runat="server" id="tr_EljarasiSzakasz">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelEljarasiSzakasz" runat="server" Text="Elj�r�si szakasz:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList ID="EljarasiSzakaszKodtarakDropDownList" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" runat="server" id="tr_Irattipus">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelIrattipus" runat="server" Text="Iratt�pus:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList ID="IratTipusKodtarakDropDownList" runat="server" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="tr_GeneraltTargy" runat="server" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelGeneraltTargy" runat="server" Text="Gener�lt t�rgy:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <asp:TextBox ID="GeneraltTargyTextBox" runat="server" Width="250"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="trIntezesiIdo" runat="server">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelIntezesiIdo" runat="server" Text="Int�z�si id�:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:RequiredNumberBox ID="IntezesiIdoTextBox" CssClass="mrUrlapInputNumber" runat="server"
                                            Validate="false" />
                                        <uc9:KodtarakDropDownList ID="IdoegysegKodtarakDropDownList" CssClass="mrUrlapInputRovid"
                                            runat="server" />
                                        <asp:CheckBox ID="cbIntezesiIdoKotott" runat="server" Text="K�t�tt" TextAlign="Right" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="tr_UgyiratHataridoKitolas" runat="server">
                                    <td class="mrUrlapCaption"></td>
                                    <td class="mrUrlapMezo">
                                        <asp:CheckBox ID="cbUgyiratHataridoKitolas" runat="server" Checked="true" Text="Irat hat�rid� az �gyirat hat�ridej�t kitolhatja"
                                            TextAlign="Right" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="tr_SzignalasTipusa" runat="server" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelSzignalasTipusa" runat="server" Text="Szign�l�s t�pusa:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc9:KodtarakDropDownList ID="SzignalasTipusaKodtarakDropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SzignalasTipusaKodtarakDropDownList_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr class="urlapSor" id="tr_JavasoltFelelos" runat="server" visible="false">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelJavasoltFelelos" runat="server" Text="Javasolt felel�s:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc5:CsoportTextBox ID="JavasoltFelelosCsoportTextBox" runat="server" Validate="false" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapMezo" colspan="2" id="tr_KezelesiFeljegyzesPanel" runat="server" visible="false">
                                            <kp:KezelesiFeljegyzesPanel runat="server" ID="FeljegyzesPanel" Validate="false" />
                                    </td>
                                </tr>
                                <tr class="urlapSor">
                                    <td class="mrUrlapCaption">
                                        <asp:Label ID="labelErvenyessegStar" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                        <asp:Label ID="labelErvenyesseg" runat="server" Text="�rv�nyess�g:"></asp:Label>
                                    </td>
                                    <td class="mrUrlapMezo">
                                        <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server"></uc6:ErvenyessegCalendarControl>
                                    </td>
                                </tr>

                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
                <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="SzignalasiJegyzekVerSion" runat="server" />
    <asp:HiddenField ID="HataridosFeladatId" runat="server" />
</asp:Content>
