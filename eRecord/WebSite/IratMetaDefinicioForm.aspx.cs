﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;

public partial class IratMetaDefinicioForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    //private string SzignalasTipusa = "";
    //private string JavasoltFelelosId = "";

    private string IrattariTetel_Id = "";
    private Contentum.eAdmin.Utility.PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private const string kcs_ELJARASISZAKASZ = "ELJARASI_SZAKASZ";
    //private const string kcs_IRATFAJTA = "IRAT_FAJTA";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_UGYTIPUS = "UGYTIPUS";
    private const string kcs_SZIGNALASTIPUSA = "SZIGNALAS_TIPUSA";
    private const string kcs_UGYFAJTAJA = "UGY_FAJTAJA";
    private const string kt_UGYFAJTAJA_DEFAULT = "0";// nem hatósági ügy
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";

    #region JavaScripts

    private string CheckIntezesiIdoJavaScript()
    {
        string js = @"var intezesiIdoTextBox = $get('" + IntezesiIdoTextBox.ClientID + @"');
var idoegysegDropDownList = $get('" + IdoegysegKodtarakDropDownList.DropDownList.ClientID + @"');
if (intezesiIdoTextBox && idoegysegDropDownList)
{
    if (intezesiIdoTextBox.value != '' && intezesiIdoTextBox.value != '0' && idoegysegDropDownList[idoegysegDropDownList.selectedIndex].value == '')
    {
        alert('" + Resources.Error.UITimeUnitMissing + @"');
        return false;
    }
}

";
        return js;
    }

    #endregion



    private void SetNewControls()
    {
        if (rbManualOrAuto.Checked)
        {
            UgyTipusKodRequiredTextBox.Visible = true;
            UgytipusKodtarakDropDownList.Visible = false;
        }
        else if (rbPredefined.Checked)
        {
            UgyTipusKodRequiredTextBox.Visible = false;
            UgytipusKodtarakDropDownList.Visible = true;
        }
    }

    private void SetViewControls()
    {
        RovidNevTextBox.ReadOnly = true;
        IraIrattariTetelTextBox1.ReadOnly = true;
        UgyTipusRequiredTextBox.ReadOnly = true;
        UgyTipusKodRequiredTextBox.ReadOnly = true;
        UgytipusKodtarakDropDownList.ReadOnly = true;
        EljarasiSzakaszKodtarakDropDownList.ReadOnly = true;
        IratTipusKodtarakDropDownList.ReadOnly = true;
        GeneraltTargyTextBox.ReadOnly = true;
        IntezesiIdoTextBox.ReadOnly = true;
        UgyFajtajaKodtarakDropDownList.ReadOnly = true;
        SzignalasTipusaKodtarakDropDownList.ReadOnly = true;
        JavasoltFelelosCsoportTextBox.ReadOnly = true;
        cbIntezesiIdoKotott.Enabled = false;
        IdoegysegKodtarakDropDownList.ReadOnly = true;
        cbUgyiratHataridoKitolas.Enabled = false;
        ErvenyessegCalendarControl1.ReadOnly = true;

        rbManualOrAuto.Enabled = false;
        rbPredefined.Enabled = false;

        // sorok megjelenítése
        tr_SzignalasTipusa.Visible = true;
        //BLG_2950
        tr_JavasoltFelelos.Visible = true;
        tr_KezelesiFeljegyzesPanel.Visible = true;
        FeljegyzesPanel.PanelEnabled = false;

        tr_GeneraltTargy.Visible = true;

        labelRovidNev.CssClass = "mrUrlapInputWaterMarked";
        labelUgykod.CssClass = "mrUrlapInputWaterMarked";
        labelUgytipusKod.CssClass = "mrUrlapInputWaterMarked";
        labelUgytipus.CssClass = "mrUrlapInputWaterMarked";
        labelUgyFajta.CssClass = "mrUrlapInputWaterMarked";
        labelEljarasiSzakasz.CssClass = "mrUrlapInputWaterMarked";
        labelIrattipus.CssClass = "mrUrlapInputWaterMarked";
        labelGeneraltTargy.CssClass = "mrUrlapInputWaterMarked";
        labelIntezesiIdo.CssClass = "mrUrlapInputWaterMarked";
        labelSzignalasTipusa.CssClass = "mrUrlapInputWaterMarked";
        labelJavasoltFelelos.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
    }

    private void SetReadOnlyControlsByTipus(string tipus)
    {
        switch (tipus)
        {
            case Constants.IratMetaDefinicioTipus.IratMetaDefinicio:
                IraIrattariTetelTextBox1.ReadOnly = true;
                EljarasiSzakaszKodtarakDropDownList.ReadOnly = true;
                IratTipusKodtarakDropDownList.ReadOnly = true;

                // eredetileg nem láthatóak, itt most megjelenítjük az egész sort
                tr_SzignalasTipusa.Visible = true;
                //BLG_2950
                tr_JavasoltFelelos.Visible = false;
                tr_GeneraltTargy.Visible = true;

                // sorok eltüntetése
                tr_EljarasiSzakasz.Visible = false;
                tr_Irattipus.Visible = false;

                break;
            case Constants.IratMetaDefinicioTipus.EljarasiSzakasz:
                IraIrattariTetelTextBox1.ReadOnly = true;
                UgyTipusRequiredTextBox.ReadOnly = true;
                UgyTipusKodRequiredTextBox.ReadOnly = true;
                IratTipusKodtarakDropDownList.ReadOnly = true;

                // itt nincs értelme
                trIntezesiIdo.Visible = false;
                IntezesiIdoTextBox.ReadOnly = true;
                IdoegysegKodtarakDropDownList.ReadOnly = true;

                // itt nincs értelme
                tr_UgyiratHataridoKitolas.Visible = false;

                UgyFajtajaKodtarakDropDownList.ReadOnly = true;

                // nem láthatóak, az egész sort eltüntetjük
                SzignalasTipusaKodtarakDropDownList.ReadOnly = true;
                JavasoltFelelosCsoportTextBox.ReadOnly = true;

                // sorok eltüntetése
                tr_Irattipus.Visible = false;

                rbManualOrAuto.Enabled = false;
                rbPredefined.Enabled = false;

                EljarasiSzakaszKodtarakDropDownList.Validate = true;
                break;
            case Constants.IratMetaDefinicioTipus.Irattipus:
                IraIrattariTetelTextBox1.ReadOnly = true;
                UgyTipusRequiredTextBox.ReadOnly = true;
                UgyTipusKodRequiredTextBox.ReadOnly = true;
                EljarasiSzakaszKodtarakDropDownList.ReadOnly = true;

                UgyFajtajaKodtarakDropDownList.ReadOnly = true;

                // nem láthatóak, az egész sort eltüntetjük
                SzignalasTipusaKodtarakDropDownList.ReadOnly = true;
                JavasoltFelelosCsoportTextBox.ReadOnly = true;

                rbManualOrAuto.Enabled = false;
                rbPredefined.Enabled = false;

                IratTipusKodtarakDropDownList.Validate = true;
                break;
            default:
                // eredetileg nem láthatóak, itt most megjelenítjük az egész sort
                tr_SzignalasTipusa.Visible = true;
                //BLG_2950
                tr_JavasoltFelelos.Visible = false;
                break;
        }

    }

    private void SetModifyControls()
    {
        //IraIrattariTetelTextBox1.ReadOnly = true;
        JavasoltFelelosCsoportTextBox.Validate = false;
        UgyTipusKodRequiredTextBox.Validate = false;
        UgytipusKodtarakDropDownList.ReadOnly = true;
        UgyTipusKodRequiredTextBox.ReadOnly = true;

        rbManualOrAuto.Enabled = false;
        rbPredefined.Enabled = false;

        if (rbManualOrAuto.Checked)
        {
            UgyTipusKodRequiredTextBox.Visible = true;
            UgytipusKodtarakDropDownList.Visible = false;
        }
        else if (rbPredefined.Checked)
        {
            UgyTipusKodRequiredTextBox.Visible = false;
            UgytipusKodtarakDropDownList.Visible = true;
        }
    }

    private void FillKodTarakDropDownListsByTipus(string tipus)
    {
        Logger.Info(String.Format("DropDown Listák feltöltése, típus: {0}", tipus));
        UgyFajtajaKodtarakDropDownList.FillAndSetSelectedValue(kcs_UGYFAJTAJA, kt_UGYFAJTAJA_DEFAULT, FormHeader1.ErrorPanel);

        switch (tipus)
        {
            case Constants.IratMetaDefinicioTipus.IratMetaDefinicio:
                UgytipusKodtarakDropDownList.FillDropDownList(kcs_UGYTIPUS, FormHeader1.ErrorPanel);
                SzignalasTipusaKodtarakDropDownList.FillAndSetEmptyValue(kcs_SZIGNALASTIPUSA, FormHeader1.ErrorPanel);

                // csak napot vagy munkanapot lehet megadni
                List<string> filterList = new List<string>();
                filterList.Add(KodTarak.IDOEGYSEG.Nap);
                filterList.Add(KodTarak.IDOEGYSEG.Munkanap);
                IdoegysegKodtarakDropDownList.FillDropDownList(kcs_IDOEGYSEG, filterList, true, FormHeader1.ErrorPanel);

                break;
            case Constants.IratMetaDefinicioTipus.EljarasiSzakasz:
                UgytipusKodtarakDropDownList.FillDropDownList(kcs_UGYTIPUS, FormHeader1.ErrorPanel);
                EljarasiSzakaszKodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASISZAKASZ, FormHeader1.ErrorPanel);
                break;
            case Constants.IratMetaDefinicioTipus.Irattipus:
                UgytipusKodtarakDropDownList.FillDropDownList(kcs_UGYTIPUS, FormHeader1.ErrorPanel);
                EljarasiSzakaszKodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASISZAKASZ, FormHeader1.ErrorPanel);
                IratTipusKodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATTIPUS, FormHeader1.ErrorPanel);

                IdoegysegKodtarakDropDownList.FillAndSetSelectedValue(kcs_IDOEGYSEG, KodTarak.IDOEGYSEG.DEFAULT, true, FormHeader1.ErrorPanel);
                break;
            default:
                UgytipusKodtarakDropDownList.FillDropDownList(kcs_UGYTIPUS, FormHeader1.ErrorPanel);
                EljarasiSzakaszKodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASISZAKASZ, FormHeader1.ErrorPanel);
                IratTipusKodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATTIPUS, FormHeader1.ErrorPanel);
                SzignalasTipusaKodtarakDropDownList.FillAndSetEmptyValue(kcs_SZIGNALASTIPUSA, FormHeader1.ErrorPanel);
                IdoegysegKodtarakDropDownList.FillAndSetSelectedValue(kcs_IDOEGYSEG, KodTarak.IDOEGYSEG.DEFAULT, true, FormHeader1.ErrorPanel);
                break;

        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);
        Logger.Info("Page_Init (Command = " + Command + ")");

        IrattariTetel_Id = Request.QueryString.Get(QueryStringVars.IrattariTetelId);

        IraIrattariTetelTextBox1.FromIratmetaDefiniciok = true;

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariTetel" + Command);
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratMetaDefinicio" + Command);
                break;
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            if (Command == CommandName.Modify)
            {
                this.SetModifyControls();
            }

            if (Command == CommandName.View)
            {
                this.SetViewControls();
            }

            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                Logger.Error("Nincs megadva id!");
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                Logger.Info("Record_Id = " + id);
                EREC_IratMetaDefinicio EREC_IratMetaDefinicio = GetEREC_IratMetaDefinicio(id);

                // ha van típus megadva: IratMetaDefinicio, EljarasiSzakasz, Irattipus
                string tipus = Request.QueryString.Get(QueryStringVars.Tipus);
                if (!String.IsNullOrEmpty(tipus))
                {
                    Logger.Info(String.Format("Tipus = {0}", tipus));

                    SetReadOnlyControlsByTipus(tipus);

                    if (Command == CommandName.Modify)
                    {
                        if (String.IsNullOrEmpty(tipus) || tipus == Constants.IratMetaDefinicioTipus.IratMetaDefinicio)
                        {
                            // nem engedjük, hogy mindkettõ egyszerre Checked állapotban legyen, mert az ellentmondás lenne:
                            // (intézési idõ kötött, de irat által kitolható)
                            cbUgyiratHataridoKitolas.Attributes.Add("onclick", "if (this.checked) { var cb = $get('" + cbIntezesiIdoKotott.ClientID + "');cb.checked = false;}");
                            cbIntezesiIdoKotott.Attributes.Add("onclick", "if (this.checked) { var cb = $get('" + cbUgyiratHataridoKitolas.ClientID + "');cb.checked = false;}");
                        }
                    }
                }
                if (!IsPostBack)
                {

                    FillKodTarakDropDownListsByTipus(tipus);

                    if (Command == CommandName.Modify)
                    {
                        FormFooter1.ImageButton_Save.OnClientClick += CheckIntezesiIdoJavaScript();
                    }
                }

                if (EREC_IratMetaDefinicio != null)
                {
                    LoadComponentsFromBusinessObject(EREC_IratMetaDefinicio);

                    #region Szignálási jegyzék
                    System.Data.DataRow dataRow_EREC_SzignalasiJegyzekek = GetEREC_SzignalasiJegyzekekByIratMetaDefinicio(id);
                    LoadComponentsFromDataRow(dataRow_EREC_SzignalasiJegyzekek);
                    #endregion Szignálási jegyzék

                    #region BLG_2950 - Határidős feladat betöltése
                    if (!IsPostBack)
                    {
                        EREC_HataridosFeladatokService hataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                        execParam.Record_Id = HataridosFeladatId.Value;

                        if (!string.IsNullOrEmpty(HataridosFeladatId.Value))
                        {
                            Result resGet = hataridosFeladatokService.Get(execParam/*new ExecParam() { Record_Id = HataridosFeladatId.Value }*/);

                            if (resGet.IsError)
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resGet);
                            }


                            if (resGet.Record != null)
                            {
                                EREC_HataridosFeladatok erec_hataridosFeladat = (EREC_HataridosFeladatok)resGet.Record;
                                FeljegyzesPanel.SetFeladatByBusinessObject(erec_hataridosFeladat);
                                if (Command == CommandName.View)
                                {
                                    FeljegyzesPanel.PanelEnabled = false;
                                }

                                if (Command == CommandName.Modify)
                                {
                                    FeljegyzesPanel.PanelEnabled = true;
                                }
                            }
                        }
                    }

                    #endregion
                }
            }
        }
        else if (Command == CommandName.New)
        {
            SetNewControls();

            if (!IsPostBack)
            {
                // szülõ irat metadefiníció szint átvétele
                string IratMetaDefinicio_Id = Request.QueryString.Get(QueryStringVars.IratMetadefinicioId);
                if (!String.IsNullOrEmpty(IratMetaDefinicio_Id))
                {
                    Logger.Info("Record_Id = " + IratMetaDefinicio_Id);

                    EREC_IratMetaDefinicio EREC_IratMetaDefinicioParent = GetEREC_IratMetaDefinicio(IratMetaDefinicio_Id);

                    if (EREC_IratMetaDefinicioParent != null)
                    {
                        string tipusByParent = String.Empty;
                        if (!String.IsNullOrEmpty(EREC_IratMetaDefinicioParent.Irattipus))
                        {
                            // irattípus alatt nincs több szint
                            SetViewControls();
                        }
                        else if (!String.IsNullOrEmpty(EREC_IratMetaDefinicioParent.EljarasiSzakasz))
                        {
                            tipusByParent = Constants.IratMetaDefinicioTipus.Irattipus;
                        }
                        else if (!String.IsNullOrEmpty(EREC_IratMetaDefinicioParent.Ugytipus))
                        {
                            tipusByParent = Constants.IratMetaDefinicioTipus.EljarasiSzakasz;
                        }
                        else if (!String.IsNullOrEmpty(EREC_IratMetaDefinicioParent.UgykorKod))
                        {
                            tipusByParent = Constants.IratMetaDefinicioTipus.IratMetaDefinicio;
                        }

                        FillKodTarakDropDownListsByTipus(tipusByParent);
                        SetReadOnlyControlsByTipus(tipusByParent);


                        LoadComponentsFromBusinessObject(EREC_IratMetaDefinicioParent);

                        if (String.IsNullOrEmpty(tipusByParent) || tipusByParent == Constants.IratMetaDefinicioTipus.IratMetaDefinicio)
                        {
                            // nem engedjük, hogy mindkettõ egyszerre Checked állapotban legyen, mert az ellentmondás lenne:
                            // (intézési idõ kötött, de irat által kitolható)
                            cbUgyiratHataridoKitolas.Attributes.Add("onclick", "if (this.checked) { var cb = $get('" + cbIntezesiIdoKotott.ClientID + "');cb.checked = false;}");
                            cbIntezesiIdoKotott.Attributes.Add("onclick", "if (this.checked) { var cb = $get('" + cbUgyiratHataridoKitolas.ClientID + "');cb.checked = false;}");
                        }
                    }
                }
                else if (!String.IsNullOrEmpty(IrattariTetel_Id))
                {
                    FillKodTarakDropDownListsByTipus(Constants.IratMetaDefinicioTipus.IratMetaDefinicio);
                    SetReadOnlyControlsByTipus(Constants.IratMetaDefinicioTipus.IratMetaDefinicio);

                    IraIrattariTetelTextBox1.Id_HiddenField = IrattariTetel_Id;
                    IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);
                }
            }   // if (!IsPostBack)

        }

        //BLG_2950
        //JavasoltFelelosCsoportTextBox.ContainerId = tr_JavasoltFelelos.ClientID;

        //JavasoltFelelosCsoportTextBox.HideIfDisabled = true;
        JavasoltFelelosCsoportTextBox.HideIfDisabled = false;

        #region BLG_2950
        //LZS
        //FeljegyzesPanel.ErrorPanel = FormHeader1.ErrorPanel;
        if (SzignalasTipusaKodtarakDropDownList.SelectedValue == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore)
        {
            tr_KezelesiFeljegyzesPanel.Visible = true;
            tr_JavasoltFelelos.Visible = true;
        }
        else
        {
            tr_KezelesiFeljegyzesPanel.Visible = false;
            tr_JavasoltFelelos.Visible = false;
        }
        #endregion
    }

    protected EREC_IratMetaDefinicio GetEREC_IratMetaDefinicio(String IratMetaDefinicio_Id)
    {
        Logger.Info(String.Format("Irat metadefiníció lekérése (Id: {0})", IratMetaDefinicio_Id));

        EREC_IratMetaDefinicio erec_IratMetaDefinicio = null;

        if (!String.IsNullOrEmpty(IratMetaDefinicio_Id))
        {
            EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = IratMetaDefinicio_Id;

            Result result = service.Get(execParam);
            if (!result.IsError)
            {
                erec_IratMetaDefinicio = (EREC_IratMetaDefinicio)result.Record;
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }

        }
        return erec_IratMetaDefinicio;
    }

    protected System.Data.DataRow GetEREC_SzignalasiJegyzekekByIratMetaDefinicio(String UgykorTargykor_Id)
    {
        Logger.Info(String.Format("Szignálási jegyzék lekérése (UgykorTagykor_Id: {0})", UgykorTargykor_Id));

        System.Data.DataRow dataRow_result = null;

        if (!String.IsNullOrEmpty(UgykorTargykor_Id))
        {
            EREC_SzignalasiJegyzekekService SzJservice = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
            EREC_SzignalasiJegyzekekSearch SzJSearch = new EREC_SzignalasiJegyzekekSearch();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            SzJSearch.UgykorTargykor_Id.Filter(UgykorTargykor_Id);

            Result resSzJ = SzJservice.GetAll(execParam, SzJSearch);

            if (!resSzJ.IsError)
            {
                if (resSzJ.GetCount == 1)
                {
                    dataRow_result = resSzJ.Ds.Tables[0].Rows[0];
                }
                else if (resSzJ.GetCount > 1)
                {
                    Logger.Warn("A szignálási jegyzék nem egyértelmû! Szignálási jegyzékek -bõl visszakapott sorok száma = " + resSzJ.Ds.Tables[0].Rows.Count);
                    ResultError.SetResultByErrorCode(resSzJ, 57001); // a keresés több találatot eredményezett! 
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resSzJ);
                }
            }
            else
            {
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resSzJ);
            }
        }

        return dataRow_result;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (page " + Resources.Form.IratMetaDefinicioFormHeaderTitle + ")");
        FormHeader1.HeaderTitle = Resources.Form.IratMetaDefinicioFormHeaderTitle;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //BLG_2950
        //JavasoltFelelosCsoportTextBox.SetFilterBySzignalasTipus(SzignalasTipusaKodtarakDropDownList.DropDownList);
    }

    protected void UgytipusKodInputType_CheckedChanged(object sender, EventArgs e)
    {

        if (rbManualOrAuto.Checked)
        {
            UgyTipusKodRequiredTextBox.Visible = true;
            UgytipusKodtarakDropDownList.Visible = false;
        }
        else if (rbPredefined.Checked)
        {
            UgyTipusKodRequiredTextBox.Visible = false;
            UgytipusKodtarakDropDownList.Visible = true;
        }
    }

    private void LoadComponentsFromBusinessObject(EREC_IratMetaDefinicio erec_IratMetaDefinicio)
    {
        Logger.Info("Komponensek feltöltése BusinessObject-bõl");
        if (erec_IratMetaDefinicio != null)
        {
            RovidNevTextBox.Text = erec_IratMetaDefinicio.Rovidnev;

            IraIrattariTetelTextBox1.Id_HiddenField = erec_IratMetaDefinicio.Ugykor_Id;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

            UgyTipusRequiredTextBox.Text = erec_IratMetaDefinicio.UgytipusNev;

            UgyTipusKodRequiredTextBox.Text = erec_IratMetaDefinicio.Ugytipus;

            UgyFajtajaKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicio.UgyFajta);

            EljarasiSzakaszKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicio.EljarasiSzakasz);

            IratTipusKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicio.Irattipus);

            GeneraltTargyTextBox.Text = erec_IratMetaDefinicio.GeneraltTargy;

            IntezesiIdoTextBox.Text = erec_IratMetaDefinicio.UgyiratIntezesiIdo;

            IdoegysegKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicio.Idoegyseg);

            cbIntezesiIdoKotott.Checked = (erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott == "1" ? true : false);

            cbUgyiratHataridoKitolas.Checked = (erec_IratMetaDefinicio.UgyiratHataridoKitolas == "1" ? true : false);

            if (UgytipusKodtarakDropDownList.DropDownList.Items.FindByValue(erec_IratMetaDefinicio.Ugytipus) != null)
            {
                rbManualOrAuto.Checked = false;
                rbPredefined.Checked = true;

                UgyTipusKodRequiredTextBox.Visible = false;
                UgytipusKodtarakDropDownList.Visible = true;

                UgytipusKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicio.Ugytipus);
            }
            else
            {
                rbManualOrAuto.Checked = true;
                rbPredefined.Checked = false;

                UgyTipusKodRequiredTextBox.Visible = true;
                UgytipusKodtarakDropDownList.Visible = false;

                UgyTipusKodRequiredTextBox.Text = erec_IratMetaDefinicio.Ugytipus;
            }

            ErvenyessegCalendarControl1.ErvKezd = erec_IratMetaDefinicio.ErvKezd;
            ErvenyessegCalendarControl1.ErvVege = erec_IratMetaDefinicio.ErvVege;

            FormHeader1.Record_Ver = erec_IratMetaDefinicio.Base.Ver;

            //A módosíytás,létrehozás form beállítása
            FormPart_CreatedModified1.SetComponentValues(erec_IratMetaDefinicio.Base);
        }
    }

    private void LoadComponentsFromDataRow(System.Data.DataRow dataRow_EREC_SzignalasiJegyzekek)
    {
        Logger.Info("Komponensek feltöltése DataRow-ból");

        if (dataRow_EREC_SzignalasiJegyzekek != null)
        {
            if (dataRow_EREC_SzignalasiJegyzekek["SzignalasTipusa"] != null)
            {
                SzignalasTipusaKodtarakDropDownList.SetSelectedValue(dataRow_EREC_SzignalasiJegyzekek["SzignalasTipusa"].ToString());
            }

            if (dataRow_EREC_SzignalasiJegyzekek["Csoport_Id_Felelos"] != null)
            {
                JavasoltFelelosCsoportTextBox.Id_HiddenField = dataRow_EREC_SzignalasiJegyzekek["Csoport_Id_Felelos"].ToString();
                JavasoltFelelosCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }

            if (dataRow_EREC_SzignalasiJegyzekek["Ver"] != null)
            {
                SzignalasiJegyzekVerSion.Value = dataRow_EREC_SzignalasiJegyzekek["Ver"].ToString();
            }

            if (dataRow_EREC_SzignalasiJegyzekek["HataridosFeladatId"] != null)
            {
                HataridosFeladatId.Value = dataRow_EREC_SzignalasiJegyzekek["HataridosFeladatId"].ToString();
            }
        }
    }

    private EREC_SzignalasiJegyzekek GetSzJBusinessObjectFromComponents(String Id)
    {
        Logger.Info("Szignálási Jegyzékek Businessobject feltöltése Componensekbõl (Id=" + Id + ")");
        EREC_SzignalasiJegyzekek erec_SzignalasiJegyzekek = new EREC_SzignalasiJegyzekek();
        erec_SzignalasiJegyzekek.Updated.SetValueAll(false);
        erec_SzignalasiJegyzekek.Base.Updated.SetValueAll(false);

        erec_SzignalasiJegyzekek.UgykorTargykor_Id = Id;
        erec_SzignalasiJegyzekek.Updated.UgykorTargykor_Id = true;

        erec_SzignalasiJegyzekek.Csoport_Id_Felelos = JavasoltFelelosCsoportTextBox.Id_HiddenField;
        erec_SzignalasiJegyzekek.Updated.Csoport_Id_Felelos = pageView.GetUpdatedByView(JavasoltFelelosCsoportTextBox);

        erec_SzignalasiJegyzekek.SzignalasTipusa = SzignalasTipusaKodtarakDropDownList.SelectedValue;
        erec_SzignalasiJegyzekek.Updated.SzignalasTipusa = pageView.GetUpdatedByView(SzignalasTipusaKodtarakDropDownList);

        erec_SzignalasiJegyzekek.Base.Ver = SzignalasiJegyzekVerSion.Value;
        erec_SzignalasiJegyzekek.Base.Updated.Ver = true;

        #region BLG_2950
        erec_SzignalasiJegyzekek.HataridosFeladatId = HataridosFeladatId.Value;
        erec_SzignalasiJegyzekek.Updated.HataridosFeladatId = true;
        #endregion

        return erec_SzignalasiJegyzekek;
    }

    private EREC_IratMetaDefinicio GetBusinessObjectFromComponents()
    {
        Logger.Info("iratMetaDefinicio BusinessObject feltöltése komponensekbõl.");
        EREC_IratMetaDefinicio erec_IratMetaDefinicio = new EREC_IratMetaDefinicio();

        erec_IratMetaDefinicio.Updated.SetValueAll(false);
        erec_IratMetaDefinicio.Base.Updated.SetValueAll(false);

        erec_IratMetaDefinicio.Rovidnev = RovidNevTextBox.Text;
        erec_IratMetaDefinicio.Updated.Rovidnev = pageView.GetUpdatedByView(RovidNevTextBox);

        erec_IratMetaDefinicio.Ugykor_Id = IraIrattariTetelTextBox1.Id_HiddenField;
        erec_IratMetaDefinicio.Updated.Ugykor_Id = pageView.GetUpdatedByView(IraIrattariTetelTextBox1);

        erec_IratMetaDefinicio.UgykorKod = IraIrattariTetelTextBox1.Text;
        erec_IratMetaDefinicio.Updated.UgykorKod = pageView.GetUpdatedByView(IraIrattariTetelTextBox1);

        if (rbManualOrAuto.Checked)
        {
            erec_IratMetaDefinicio.Ugytipus = UgyTipusKodRequiredTextBox.Text;
            erec_IratMetaDefinicio.Updated.Ugytipus = pageView.GetUpdatedByView(UgyTipusKodRequiredTextBox);
        }
        else if (rbPredefined.Checked)
        {
            erec_IratMetaDefinicio.Ugytipus = UgytipusKodtarakDropDownList.SelectedValue;
            erec_IratMetaDefinicio.Updated.Ugytipus = pageView.GetUpdatedByView(UgytipusKodtarakDropDownList);
        }

        erec_IratMetaDefinicio.UgytipusNev = UgyTipusRequiredTextBox.Text;
        erec_IratMetaDefinicio.Updated.UgytipusNev = pageView.GetUpdatedByView(UgyTipusRequiredTextBox);

        erec_IratMetaDefinicio.UgyFajta = UgyFajtajaKodtarakDropDownList.SelectedValue;
        erec_IratMetaDefinicio.Updated.UgyFajta = pageView.GetUpdatedByView(UgyFajtajaKodtarakDropDownList);

        erec_IratMetaDefinicio.EljarasiSzakasz = EljarasiSzakaszKodtarakDropDownList.SelectedValue;
        erec_IratMetaDefinicio.Updated.EljarasiSzakasz = pageView.GetUpdatedByView(EljarasiSzakaszKodtarakDropDownList);

        erec_IratMetaDefinicio.Irattipus = IratTipusKodtarakDropDownList.SelectedValue;
        erec_IratMetaDefinicio.Updated.Irattipus = pageView.GetUpdatedByView(IratTipusKodtarakDropDownList);

        erec_IratMetaDefinicio.UgyiratIntezesiIdoKotott = (cbIntezesiIdoKotott.Checked ? "1" : "0");
        erec_IratMetaDefinicio.Updated.UgyiratIntezesiIdoKotott = pageView.GetUpdatedByView(cbIntezesiIdoKotott);

        erec_IratMetaDefinicio.GeneraltTargy = GeneraltTargyTextBox.Text;
        erec_IratMetaDefinicio.Updated.GeneraltTargy = pageView.GetUpdatedByView(GeneraltTargyTextBox);

        erec_IratMetaDefinicio.UgyiratIntezesiIdo = IntezesiIdoTextBox.Text;
        erec_IratMetaDefinicio.Updated.UgyiratIntezesiIdo = pageView.GetUpdatedByView(IntezesiIdoTextBox);

        erec_IratMetaDefinicio.Idoegyseg = IdoegysegKodtarakDropDownList.SelectedValue;
        erec_IratMetaDefinicio.Updated.Idoegyseg = pageView.GetUpdatedByView(IdoegysegKodtarakDropDownList);

        erec_IratMetaDefinicio.UgyiratHataridoKitolas = (cbUgyiratHataridoKitolas.Checked ? "1" : "0");
        erec_IratMetaDefinicio.Updated.UgyiratHataridoKitolas = pageView.GetUpdatedByView(cbUgyiratHataridoKitolas);

        ErvenyessegCalendarControl1.SetErvenyessegFields(erec_IratMetaDefinicio, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        erec_IratMetaDefinicio.Base.Ver = FormHeader1.Record_Ver;
        erec_IratMetaDefinicio.Base.Updated.Ver = true;

        return erec_IratMetaDefinicio;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(RovidNevTextBox);
            compSelector.Add_ComponentOnClick(IraIrattariTetelTextBox1);
            compSelector.Add_ComponentOnClick(UgyTipusRequiredTextBox);
            compSelector.Add_ComponentOnClick(UgyTipusKodRequiredTextBox);
            compSelector.Add_ComponentOnClick(EljarasiSzakaszKodtarakDropDownList);
            compSelector.Add_ComponentOnClick(IratTipusKodtarakDropDownList);
            compSelector.Add_ComponentOnClick(GeneraltTargyTextBox);
            compSelector.Add_ComponentOnClick(IntezesiIdoTextBox);
            compSelector.Add_ComponentOnClick(SzignalasTipusaKodtarakDropDownList);
            compSelector.Add_ComponentOnClick(JavasoltFelelosCsoportTextBox);
            compSelector.Add_ComponentOnClick(rbManualOrAuto);
            compSelector.Add_ComponentOnClick(rbPredefined);
            compSelector.Add_ComponentOnClick(cbIntezesiIdoKotott);
            compSelector.Add_ComponentOnClick(cbUgyiratHataridoKitolas);
            compSelector.Add_ComponentOnClick(ErvenyessegCalendarControl1);

            FormFooter1.SaveEnabled = false;
        }
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        Logger.Info("FormFooter1ButtonsClick (CommandEventArgs=" + e.CommandName + ")");
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IrattariTetel" + Command))
            {
                Logger.Info("IratMetaDefinicio.Command = " + Command);
                switch (Command)
                {
                    case CommandName.New:
                        {
                            EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                            EREC_IratMetaDefinicio erec_IratMetaDefinicio = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            Result result = service.Insert(execParam, erec_IratMetaDefinicio);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!

                                // csak ügytípus szintig...
                                if (
                                    //String.IsNullOrEmpty(erec_IratMetaDefinicio.UgytipusNev) &&
                                    String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz)
                                    && String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus)
                                    && (pageView.GetUpdatedByView(SzignalasTipusaKodtarakDropDownList) || pageView.GetUpdatedByView(JavasoltFelelosCsoportTextBox))
                                    )
                                {

                                    //csak akkor kell insert, ha ki is van valamelyik töltve
                                    EREC_SzignalasiJegyzekekService SzJServiceInsert = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                                    EREC_SzignalasiJegyzekek erec_SzignalasiJegyzekek = GetSzJBusinessObjectFromComponents(result.Uid);

                                    #region BLG_2950
                                    if (SzignalasTipusaKodtarakDropDownList.SelectedValue == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore)
                                    {
                                        EREC_HataridosFeladatok hataridosFeladatok = FeljegyzesPanel.GetBusinessObject();
                                        EREC_HataridosFeladatokService hataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();

                                        //BLG_2950 - LZS - Még nem létezik határidős feladat a szignálási jegyzékhez, szóval INSERT
                                        Result resInsert = hataridosFeladatokService.Insert(execParam, hataridosFeladatok);

                                        if (resInsert.IsError)
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resInsert);
                                        }

                                        HataridosFeladatId.Value = resInsert.Uid;
                                        erec_SzignalasiJegyzekek.HataridosFeladatId = resInsert.Uid;
                                    }
                                    #endregion

                                    ExecParam SzJexecParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    Result resultSzJInsert = SzJServiceInsert.Insert(SzJexecParam, erec_SzignalasiJegyzekek);
                                    if (!String.IsNullOrEmpty(resultSzJInsert.ErrorCode))
                                    {
                                        Logger.Error("resultSzJInsert.ErrorMessage = " + resultSzJInsert.ErrorMessage);
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultSzJInsert);
                                    }
                                    else
                                    {
                                        if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, RovidNevTextBox.Text))
                                        {
                                            String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                            if (!String.IsNullOrEmpty(refreshCallingWindow)
                                                && refreshCallingWindow == "1"
                                                )
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                            }
                                            else
                                            {
                                                JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                            }
                                        }
                                        else
                                        {
                                            JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                        }
                                        // nem küldjük vissza a szerverre
                                        EFormPanel1.Visible = false;
                                    }
                                }
                                else
                                {
                                    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, RovidNevTextBox.Text))
                                    {
                                        String refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                                        if (!String.IsNullOrEmpty(refreshCallingWindow)
                                            && refreshCallingWindow == "1"
                                            )
                                        {
                                            JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                        }
                                        else
                                        {
                                            JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                        }
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, result.Uid);
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    // nem küldjük vissza a szerverre
                                    EFormPanel1.Visible = false;
                                }
                            }
                            else
                            {
                                Logger.Error("result.ErrorMessage = " + result.ErrorMessage);
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                            break;
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
                                EREC_IratMetaDefinicio erec_IratMetaDefinicio = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                execParam.Record_Id = recordId;

                                Result result = service.Update(execParam, erec_IratMetaDefinicio);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    // csak ügytípus szintig...
                                    if (
                                        //String.IsNullOrEmpty(erec_IratMetaDefinicio.UgytipusNev) &&
                                        String.IsNullOrEmpty(erec_IratMetaDefinicio.EljarasiSzakasz)
                                        && String.IsNullOrEmpty(erec_IratMetaDefinicio.Irattipus)
                                        && (pageView.GetUpdatedByView(SzignalasTipusaKodtarakDropDownList) || pageView.GetUpdatedByView(JavasoltFelelosCsoportTextBox))
                                        )
                                    {
                                        //Szignálási jegyzékek
                                        EREC_SzignalasiJegyzekekService SzJservice = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                                        EREC_SzignalasiJegyzekekSearch SzJSearch = new EREC_SzignalasiJegyzekekSearch();

                                        SzJSearch.UgykorTargykor_Id.Value = recordId;
                                        SzJSearch.UgykorTargykor_Id.Operator = Contentum.eQuery.Query.Operators.equals;

                                        Result resSzJ = SzJservice.GetAll(execParam, SzJSearch);
                                        System.Data.DataSet ds = resSzJ.Ds;

                                        if (String.IsNullOrEmpty(resSzJ.ErrorCode))
                                        {
                                            if (ds.Tables[0].Rows.Count == 0)
                                            {
                                                if (pageView.GetUpdatedByView(SzignalasTipusaKodtarakDropDownList) || pageView.GetUpdatedByView(JavasoltFelelosCsoportTextBox))
                                                {
                                                    //csak akkor kell insert, ha ki is van valamelyik töltve

                                                    EREC_SzignalasiJegyzekekService SzJServiceInsert = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                                                    EREC_SzignalasiJegyzekek erec_SzignalasiJegyzekek = GetSzJBusinessObjectFromComponents(recordId);

                                                    #region BLG2950
                                                    if (SzignalasTipusaKodtarakDropDownList.SelectedValue == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore)
                                                    {
                                                        EREC_HataridosFeladatok hataridosFeladatok = FeljegyzesPanel.GetBusinessObject();
                                                        EREC_HataridosFeladatokService hataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();

                                                        //BLG_2950 - LZS - Még nem létezik határidős feladat a szignálási jegyzékhez, úgyhogy INSERT
                                                        Result resInsert = hataridosFeladatokService.Insert(execParam, hataridosFeladatok);

                                                        if (resInsert.IsError)
                                                        {
                                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resInsert);
                                                        }

                                                        HataridosFeladatId.Value = resInsert.Uid;
                                                        erec_SzignalasiJegyzekek.HataridosFeladatId = resInsert.Uid;
                                                    }
                                                    #endregion

                                                    ExecParam SzJexecParam = UI.SetExecParamDefault(Page, new ExecParam());
                                                    Result resultSzJInsert = SzJServiceInsert.Insert(SzJexecParam, erec_SzignalasiJegyzekek);
                                                    if (!String.IsNullOrEmpty(resultSzJInsert.ErrorCode))
                                                    {
                                                        Logger.Error("resultSzJInsert.ErrorMessage = " + resultSzJInsert.ErrorMessage);
                                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultSzJInsert);
                                                    }
                                                    else
                                                    {
                                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                                    }
                                                }
                                                else
                                                {
                                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                                }
                                            }
                                            else
                                            {
                                                EREC_SzignalasiJegyzekekService SzJServiceUpdate = eRecordService.ServiceFactory.GetEREC_SzignalasiJegyzekekService();
                                                EREC_SzignalasiJegyzekek erec_SzignalasiJegyzekek = GetSzJBusinessObjectFromComponents(recordId);

                                                EREC_HataridosFeladatok hataridosFeladatok = FeljegyzesPanel.GetBusinessObject();
                                                EREC_HataridosFeladatokService hataridosFeladatokService = eRecordService.ServiceFactory.GetEREC_HataridosFeladatokService();

                                                //BLG_2950 - LZS - Ha még nem létezik határidős feladat a szignálási jegyzékhez, akkor INSERT
                                                if (string.IsNullOrEmpty(erec_SzignalasiJegyzekek.HataridosFeladatId))
                                                {
                                                    Result resInsert = hataridosFeladatokService.Insert(execParam, hataridosFeladatok);

                                                    if (resInsert.IsError)
                                                    {
                                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resInsert);
                                                    }

                                                    HataridosFeladatId.Value = resInsert.Uid;
                                                    //erec_SzignalasiJegyzekek.HataridosFeladatId = resInsert.Uid;
                                                    //erec_SzignalasiJegyzekek.Updated.HataridosFeladatId = true;

                                                }//BLG_2950 - LZS - Ha már létezik határidős feladat a szignálási jegyzékhez, akkor UPDATE
                                                else
                                                {
                                                    ExecParam updateExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                                                    updateExecParam.Record_Id = HataridosFeladatId.Value;

                                                    Result resUpdate = hataridosFeladatokService.Update(updateExecParam, hataridosFeladatok);

                                                    if (resUpdate.IsError)
                                                    {
                                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resUpdate);
                                                    }

                                                }



                                                ExecParam SzJexecParam = UI.SetExecParamDefault(Page, new ExecParam());
                                                SzJexecParam.Record_Id = ds.Tables[0].Rows[0]["Id"].ToString();

                                                //BLG_2950
                                                erec_SzignalasiJegyzekek.HataridosFeladatId = HataridosFeladatId.Value;
                                                erec_SzignalasiJegyzekek.Updated.HataridosFeladatId = true;

                                                Result resultSzjUpdate = SzJServiceUpdate.Update(SzJexecParam, erec_SzignalasiJegyzekek);
                                                if (!String.IsNullOrEmpty(resultSzjUpdate.ErrorCode))
                                                {
                                                    Logger.Error("resultSzjUpdate.ErrorMessage = " + resultSzjUpdate.ErrorMessage);
                                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resultSzjUpdate);
                                                }
                                                else
                                                {
                                                    JavaScripts.RegisterCloseWindowClientScript(Page);
                                                }

                                            }

                                            //SzignalasTipusa = ds.Tables[0].Rows[0]["SzignalasTipusa"].ToString();
                                            //JavasoltFelelosId = ds.Tables[0].Rows[0]["Csoport_Id_Felelos"].ToString();
                                            //JavaScripts.RegisterCloseWindowClientScript(Page);

                                            // nem küldjük vissza a szerverre
                                            EFormPanel1.Visible = false;
                                        }
                                        else
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                        }
                                    }
                                    else
                                    {
                                        JavaScripts.RegisterCloseWindowClientScript(Page);
                                        // nem küldjük vissza a szerverre
                                        EFormPanel1.Visible = false;
                                    }
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                Logger.Error("AccessDenied");
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    #region BLG_2950
    protected void SzignalasTipusaKodtarakDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (SzignalasTipusaKodtarakDropDownList.SelectedValue == KodTarak.SZIGNALAS_TIPUSA._5_AutomatikusSzignalasUgyintezore)
        {
            tr_KezelesiFeljegyzesPanel.Visible = true;
            tr_JavasoltFelelos.Visible = true;
        }
        else
        {
            tr_KezelesiFeljegyzesPanel.Visible = false;
            tr_JavasoltFelelos.Visible = false;
        }

    }
    #endregion
}