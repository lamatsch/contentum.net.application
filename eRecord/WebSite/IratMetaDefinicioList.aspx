<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IratMetaDefinicioList.aspx.cs" Inherits="IratMetaDefinicioList" Title="<%$ Resources:List,IratMetaDefiniciokListHeaderTitle %>" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:listheader id="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eui:eerrorpanel id="EErrorPanel1" runat="server">
            </eui:eerrorpanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:customupdateprogress id="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>
    <%--HiddenFields--%>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <asp:HiddenField ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>
    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="IratMetaDefiniciokCPEButton" ImageUrl="images/hu/Grid/minus.gif"
                    OnClientClick="return false;" />
            </td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="IratMetaDefiniciokUpdatePanel" runat="server" OnLoad="IratMetaDefiniciokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxtoolkit:collapsiblepanelextender id="IratMetaDefiniciokCPE" runat="server" expandedtext="Felhaszn�l�k list�ja"
                            collapsedtext="Felhaszn�l�k list�ja" imagecontrolid="IratMetaDefiniciokCPEButton"
                            expandedimage="images/hu/Grid/minus.gif" collapsedimage="images/hu/Grid/plus.gif"
                            autoexpand="false" autocollapse="false" expanddirection="Vertical" collapsecontrolid="IratMetaDefiniciokCPEButton"
                            expandcontrolid="IratMetaDefiniciokCPEButton" collapsed="False" collapsedsize="20"
                            targetcontrolid="Panel1" expandedsize="0">
                        </ajaxtoolkit:collapsiblepanelextender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                            <asp:GridView ID="IratMetaDefiniciokGridView" runat="server" OnRowDataBound="IratMetaDefiniciokGridView_RowDataBound"
                                                OnSorting="IratMetaDefiniciokGridView_Sorting" DataKeyNames="Id" AutoGenerateColumns="False"
                                                OnPreRender="IratMetaDefiniciokGridView_PreRender" AllowSorting="True" PagerSettings-Visible="false"
                                                AllowPaging="True" BorderWidth="1px" CellPadding="0" OnRowCommand="IratMetaDefiniciokGridView_RowCommand">
                                                <PagerSettings Visible="False"></PagerSettings>
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;&nbsp;
                                                            <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                                                AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </HeaderTemplate>
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle"></HeaderStyle>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:CommandField SelectImageUrl="~/images/hu/Grid/3Drafts.gif" SelectText="<%$ Resources:List,AlternateText_RowSelectButton %>"
                                                        ButtonType="Image" ShowSelectButton="True">
                                                        <HeaderStyle Width="25px" CssClass="GridViewSelectRowImage"></HeaderStyle>
                                                        <ItemStyle CssClass="GridViewSelectRowImage" HorizontalAlign="Center" VerticalAlign="Middle">
                                                        </ItemStyle>
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="UgykorKod" SortExpression="UgykorKod" HeaderText="&#220;gyk&#243;d">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle Width="80px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UgytipusNev" SortExpression="UgytipusNev" HeaderText="&#220;gyt&#237;pus">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle Width="120px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EljarasiSzakasz" SortExpression="EljarasiSzakasz" HeaderText="Elj&#225;r&#225;si szakasz">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle Width="120px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IrattipusNev" SortExpression="IrattipusNev" HeaderText="Iratt&#237;pus">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle Width="80px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UgyiratIntezesiIdo" SortExpression="UgyiratIntezesiIdo"
                                                        HeaderText="Int&#233;z&#233;si id�">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle Width="80px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IdoegysegNev" SortExpression="KRT_Kodtarak_Idoegyseg.Nev"
                                                        HeaderText="Id�egys�g">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle Width="80px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>                                                    
                                                    <asp:TemplateField HeaderText="Int�z�si id� k�t�tt" SortExpression="UgyiratIntezesiIdoKotott">
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbIntezesiIdoKotott" runat="server" Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="�gyirat hat�rid� kitol�s" SortExpression="UgyiratHataridoKitolas">
                                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="cbUgyiratHataridoKitolas" runat="server" Enabled="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>                                                    
                                                    <asp:BoundField DataField="SzignalasTipusa" SortExpression="SzignalasTipusa" HeaderText="Szign&#225;l&#225;s t&#237;pusa">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="JavasoltFelelos" SortExpression="JavasoltFelelos" HeaderText="Javasolt felel�s">
                                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="GridViewBorderHeader"></HeaderStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Image ID="Image1" runat="server" AlternateText="Z�rol�s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                        </HeaderTemplate>
                                                        <ItemStyle CssClass="GridViewLockedImage"></ItemStyle>
                                                        <HeaderStyle CssClass="GridViewLockedImage"></HeaderStyle>
                                                        <ItemTemplate>
                                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                                <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                                <HeaderStyle CssClass="GridViewHeaderStyle"></HeaderStyle>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>
