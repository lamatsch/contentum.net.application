using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IratMetaDefinicioList : Contentum.eUtility.UI.PageBase
{
    public const string Header = "$Header: IratMetaDefinicioList.aspx.cs, 18, 2010.05.03 9:55:16, Boda Eszter$";
    public const string Version = "$Revision: 18$";

    UI ui = new UI();

    private String Startup = "";

    private const string default_SearchExpression = "Rovidnev";

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratMetaDefinicioList");

        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (" + Resources.List.IratMetaDefiniciokListHeaderTitle + ")");
        ListHeader1.HeaderLabel = Resources.List.IratMetaDefiniciokListHeaderTitle;
        ListHeader1.SearchObjectType = typeof(EREC_IratMetaDefinicioSearch);

        ListHeader1.NewVisible = true;
        ListHeader1.InvalidateVisible = true;
        ListHeader1.PrintVisible = true;

        ListHeader1.SetRightFunctionButtonsVisible(false);

        //ListHeader1.BejovoIratIktatasVisible = true;
        //ListHeader1.BelsoIratIktatasVisible = true;

        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        if (EErrorPanel1.Visible)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx", QueryStringVars.Command + "=" + CommandName.New
            , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IrattariTervForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify,
                    Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.InvalidateOnClientClick = JavaScripts.SetOnClientClickDeleteConfirm(IratMetaDefiniciokGridView.ClientID);

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IratMetaDefiniciokGridView.ClientID);

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IratMetaDefiniciokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IratMetaDefiniciokGridView.ClientID);
        //   ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IratMetaDefinicioPrintForm.aspx");
        ListHeader1.PrintOnClientClick = "javascript:window.open('IratMetaDefinicioPrintFormSSRS.aspx')";


        //ListHeader1.BejovoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefiniciokForm.aspx",
        //  QueryStringVars.Command + "=" + CommandName.New
        //  , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID,EventArgumentConst.refreshMasterList);

        //ListHeader1.BelsoIratIktatasOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefiniciokForm.aspx",
        //    QueryStringVars.Command + "=" + CommandName.New + "&" + QueryStringVars.Mode + "=" + CommandName.BelsoIratIktatas
        //    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID,EventArgumentConst.refreshMasterList);



        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IratMetaDefiniciokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IratMetaDefiniciokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IratMetaDefiniciokGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IratMetaDefinicioSearch());

        //if (!IsPostBack) IratMetaDefiniciokGridViewBind();

        ui.SetClientScriptToGridViewSelectDeSelectButton(IratMetaDefiniciokGridView);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        //String Startup = Request.QueryString.Get(QueryStringVars.Startup);
        if (!IsPostBack && Startup != null && (Startup == Constants.Startup.FromSzignalasiJegyzek))
        {
            string script = "OpenNewWindow(); function OpenNewWindow() { ";
            if (Startup == Constants.Startup.FromSzignalasiJegyzek)
            {
                script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("IrattariTervForm.aspx",
                    QueryStringVars.Command + "=" + CommandName.Modify
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
                    , CustomUpdateProgress1.UpdateProgress.ClientID);
            }
            script += "}";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "IMDKarbantartas", script, true);

            popupNyitas = true;
        }

        // Ha karbantart� fa k�perny�t is kell nyitni, nem t�ltj�k fel a list�t:
        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            IratMetaDefiniciokGridViewBind();
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Startup == Constants.Startup.FromSzignalasiJegyzek)
        {
            ListHeader1.NewEnabled = false;
            ListHeader1.ModifyEnabled = false;
            ListHeader1.ViewToolTip = "Iratt�ri terv t�rk�p";
        }
        else
        {
            ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.New);
            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Modify);
        }
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.View);
        //ListHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Invalidate);
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.ViewHistory);

        //ListHeader1.BejovoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "BejovoIratIktatas");
        //ListHeader1.BelsoIratIktatasEnabled = FunctionRights.GetFunkcioJog(Page, "BelsoIratIktatas");

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IratMetaDefiniciokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
            //if (String.IsNullOrEmpty(MasterListSelectedRowId))
            //{
            //    //ActiveTabClear();
            //}
            ////ActiveTabRefreshOnClientClicks();
        }
    }


    #endregion

    #region Master List

    protected void IratMetaDefiniciokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IratMetaDefiniciokGridView", ViewState, default_SearchExpression);
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IratMetaDefiniciokGridView", ViewState);

        IratMetaDefiniciokGridViewBind(sortExpression, sortDirection);
    }

    /// <summary>
    /// Adatk�t�s
    /// </summary>
    protected void IratMetaDefiniciokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        Logger.Info("IratMetaDefiniciokGridViewBind(SortExpression = " + SortExpression + ";SortDirection = " + SortDirection + ")");
        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IratMetaDefinicioSearch search = (EREC_IratMetaDefinicioSearch)Search.GetSearchObject(Page, new EREC_IratMetaDefinicioSearch());
        search.OrderBy = Search.GetOrderBy("IratMetaDefiniciokGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        //Result res = service.GetAll.GetAllWithExtensionAndJogosultak(ExecParam, search, true);
        //Result res = service.GetAll(ExecParam, search);
        Result res = service.GetAllWithExtension(ExecParam, search);
        if (!String.IsNullOrEmpty(res.ErrorCode))
        {
            Logger.Error(res.ErrorMessage);
        }
        UI.GridViewFill(IratMetaDefiniciokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void IratMetaDefiniciokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbIntezesiIdoKotott", "UgyiratIntezesiIdoKotott");
        UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyiratHataridoKitolas", "UgyiratHataridoKitolas");
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void IratMetaDefiniciokGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IratMetaDefiniciokGridView.PageIndex;

        IratMetaDefiniciokGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IratMetaDefiniciokGridView.PageCount;

        if (prev_PageIndex != IratMetaDefiniciokGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IratMetaDefiniciokCPE);
            IratMetaDefiniciokGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IratMetaDefiniciokCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IratMetaDefiniciokGridView);

    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IratMetaDefiniciokGridViewBind();
        //ActiveTabClear();
    }

    protected void IratMetaDefiniciokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Logger.Info("IratMetaDefiniciokGridView_RowCommand (Command = "+e.CommandName+")");
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IratMetaDefiniciokGridView, selectedRowNumber, "check");
            
            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

        }
    }


    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IrattariTervForm.aspx", QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id,
                    Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
            string tableName = "EREC_IratMetaDefinicio";
            ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
                , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
                , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IratMetaDefiniciokUpdatePanel.ClientID);

            //ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            //Iratok.Statusz iratStatusz = Iratok.GetAllapotById(id, execParam, EErrorPanel1);
            

            // M�dos�t�s
            //if (!Iratok.Modosithato(iratStatusz, execParam))
            //{
            //    ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_IratMegtekintes + "')) {" +
            //        JavaScripts.SetOnClientClick("IratMetaDefiniciokForm.aspx"
            //            , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
            //            + "} else return false;";
            //}
            //else
            //{
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IratMetaDefiniciokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            //}
        }
    }

    protected void IratMetaDefiniciokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    ListHeader1.NincsFeltoltes = false;
                    IratMetaDefiniciokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(IratMetaDefiniciokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIratMetaDefinicio();
            IratMetaDefiniciokGridViewBind();
        }
    }

    private void deleteSelectedIratMetaDefinicio()
    {
        Logger.Info("deleteSelectedIratMetaDefinicio");
        if (FunctionRights.GetFunkcioJog(Page, "IratMetaDefinicio" + CommandName.Invalidate))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IratMetaDefiniciokGridView, EErrorPanel1, ErrorUpdatePanel);
            EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
                Logger.Info("execParams[" + i + "].Record_Id = " + execParams[i].Record_Id);
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                Logger.Error(result.ErrorMessage);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            Logger.Error("Access Denied!");
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }


    private void ListHeaderRightFunkcionButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("ListHeaderRightFunkcionButtonsClick (Command = " + e.CommandName + ")");
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIratMetaDefinicioRecords();
                IratMetaDefiniciokGridViewBind();
                break;

            case CommandName.Unlock:
                UnlockSelectedIratMetaDefinicioRecords();
                IratMetaDefiniciokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIratMetaDefiniciok();
                break;
        }
    }

    private void LockSelectedIratMetaDefinicioRecords()
    {
        LockManager.LockSelectedGridViewRecords(IratMetaDefiniciokGridView, "EREC_IratMetaDefiniciok"
            , "IratMetaDefinicioLock", "IratMetaDefinicioForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIratMetaDefinicioRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IratMetaDefiniciokGridView, "EREC_IratMetaDefiniciok"
            , "IratMetaDefinicioLock", "IratMetaDefinicioForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// Elkuldi emailben a IratMetaDefiniciokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIratMetaDefiniciok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IratMetaDefiniciokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IratMetaDefiniciok");
        }
    }

    protected void IratMetaDefiniciokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IratMetaDefiniciokGridViewBind(e.SortExpression, UI.GetSortToGridView("IratMetaDefiniciokGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }

    #endregion

}
