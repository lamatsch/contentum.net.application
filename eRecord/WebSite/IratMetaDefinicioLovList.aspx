<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    EnableEventValidation="True" CodeFile="IratMetaDefinicioLovList.aspx.cs" Inherits="IratMetaDefinicioLovList"
    Title="<%$Resources:LovList,IratMetaDefinicioLovListHeaderTitle%>" %>

<%@ Register Src="Component/LovListHeader.ascx" TagName="LovListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/LovListFooter.ascx" TagName="LovListFooter" TagPrefix="uc3" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="ktdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 110px;
            min-width: 110px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc2:LovListHeader ID="LovListHeader1" runat="server" HeaderTitle="<%$Resources:LovList,IratMetaDefinicioLovListHeaderTitle%>" />
    &nbsp;<br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 90%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <asp:UpdatePanel runat="server" ID="SearchUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <br />
                            <table style="width: 90%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr id="trMaxRowWarning" class="LovListWarningRow" runat="server" visible="false">
                                        <td>
                                            <asp:Label ID="labelMaxRowWarning" runat="server" Text="<%$Resources:LovList,MaxRowWarning%>">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr class="urlapSor">
                                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelKod" Text="Iratt�ri&nbsp;t�telsz�m:" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="TextBoxUgykorKod" runat="server" CssClass="mrUrlapInput" />
                                                        </td>

                                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelUgytipus" Text="�gyt�pus:" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapMezo">
                                                            <asp:TextBox ID="TextBoxUgytipus" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_Ugyiratdarab" class="urlapSor" runat="server">
                                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelEljarasiSzakasz" Text="Elj�r�si&nbsp;szakasz:" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="3">
                                                            <ktdd:KodtarakDropDownList ID="KodTarakDropDownList_EljarasiSzakasz" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_Irat" class="urlapSor" runat="server">
                                                        <td class="mrUrlapCaption">&nbsp;<asp:Label ID="labelIrattipus" Text="Iratt�pus:" runat="server" />
                                                        </td>
                                                        <td class="mrUrlapMezo" colspan="3">
                                                            <ktdd:KodtarakDropDownList ID="KodTarakDropDownList_Irattipus" runat="server" CssClass="mrUrlapInput" />
                                                        </td>
                                                    </tr>
                                                    <tr class="urlapSor">
                                                        <td colspan="4">
                                                            <asp:ImageButton ID="ButtonSearch" OnClick="ButtonSearch_Click" runat="server" ImageUrl="~/images/hu/trapezgomb/kereses_trap1.jpg"
                                                                AlternateText="Keres�s"></asp:ImageButton>
                                                            <asp:ImageButton ID="ButtonAdvancedSearch" runat="server" ImageUrl="~/images/hu/trapezgomb/reszleteskereses_trap1.jpg"
                                                                AlternateText="R�szletes keres�s"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 5px; vertical-align: top; padding-top: 5px; text-align: left">
                                            <div class="listaFulFelsoCsikKicsi">
                                                <img alt="" src="images/hu/design/spacertrans.gif" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">
                                            <ajaxToolkit:CollapsiblePanelExtender ID="IratMetaDefinicioCPE" runat="server" TargetControlID="Panel1">
                                            </ajaxToolkit:CollapsiblePanelExtender>
                                            <asp:Panel ID="Panel1" runat="server">
                                                <table style="width: 98%" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top; text-align: left" class="GridViewHeaderBackGroundStyle">
                                                                <!-- Kliens oldali kiv�laszt�s kezel�se -->
                                                                <asp:HiddenField ID="GridViewSelectedIndex" runat="server"></asp:HiddenField>
                                                                <asp:HiddenField ID="GridViewSelectedId" runat="server"></asp:HiddenField>
                                                                <asp:GridView ID="GridViewSearchResult" runat="server" CssClass="GridViewLovListStyle"
                                                                    CellPadding="0" BorderWidth="1px" AllowPaging="True" PagerSettings-Visible="false"
                                                                    AutoGenerateColumns="False" DataKeyNames="Id" OnRowDataBound="GridView_RowDataBound">
                                                                    <PagerSettings Visible="False"></PagerSettings>
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Id" SortExpression="Id">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Merge_IrattariTetelszam" SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam" HeaderText="Itsz.">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="100px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UgykorKod" SortExpression="UgykorKod" HeaderText="&#220;gyk�r k&#243;d">
                                                                            <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle"></ItemStyle>
                                                                            <HeaderStyle Width="50px" CssClass="GridViewLovListInvisibleCoulumnStyle"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UgytipusNev" SortExpression="UgytipusNev" HeaderText="&#220;gyt&#237;pus">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="120px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="EljarasiSzakasz" SortExpression="EljarasiSzakasz" HeaderText="Elj&#225;r&#225;si szakasz">
                                                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle"></ItemStyle>
                                                                            <HeaderStyle Width="120px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="IrattipusNev" SortExpression="IrattipusNev" HeaderText="Iratt&#237;pus">
                                                                            <HeaderStyle Width="100px" CssClass="GridViewBorderHeader"></HeaderStyle>
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="GridViewRowStyle" Wrap="True"></RowStyle>
                                                                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle"></SelectedRowStyle>
                                                                    <HeaderStyle CssClass="GridViewLovListHeaderStyle"></HeaderStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                            <%--
                                            <asp:ListBox ID="ListBoxSearchResult" runat="server" Rows="15" Width="95%">
                                            </asp:ListBox>
                                            --%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; text-align: left">&nbsp;
                                            <br />
                                            <asp:ImageButton ID="ImageButtonReszletesAdatok" onmouseover="swapByName(this.id,'reszletesadatok2.png')"
                                                onmouseout="swapByName(this.id,'reszletesadatok.png')" runat="server" ImageUrl="~/images/hu/ovalgomb/reszletesadatok.png"
                                                AlternateText="Kijel�lt t�tel r�szletes adatainak megtekint�se"></asp:ImageButton>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </eUI:eFormPanel>
            </td>
        </tr>
    </table>
    <br />
    <uc3:LovListFooter ID="LovListFooter1" runat="server" />
</asp:Content>
