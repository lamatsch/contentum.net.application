
using System;

using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eRecord.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;

public partial class IratMetaDefinicioLovList : Contentum.eUtility.UI.PageBase
{
    private const string kcs_ELJARASI_SZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_IRATTIPUS = "IRATTIPUS";

    private UI ui = new UI();
    private bool disable_refreshLovList = false;
    private string Filter = String.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratMetaDefinicioList");        
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        registerJavascripts();

        LovListFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(LovListFooter_ButtonsClick);
            
        // van-e sz�r�s az objektum t�pusra
        Filter = Request.QueryString.Get(QueryStringVars.Filter);
        if (!String.IsNullOrEmpty(Filter))
        {
            if (Filter == Constants.FilterType.IratMetaDefinicio.Ugyirat)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.IratMetaDefinicioLovListHeaderTitle_Filtered_Ugyirat;
            }
            else if (Filter == Constants.FilterType.IratMetaDefinicio.Ugyiratdarab)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.IratMetaDefinicioLovListHeaderTitle_Filtered_Ugyiratdarab;
            }
            else if (Filter == Constants.FilterType.IratMetaDefinicio.Irat)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.IratMetaDefinicioLovListHeaderTitle_Filtered_Irat;
            }
            else if (Filter == Constants.FilterType.IratMetaDefinicio.All)
            {
                LovListHeader1.HeaderTitle = Resources.LovList.IratMetaDefinicioLovListHeaderTitle;
            }            
        }            
                
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        LovListFooter1.OkOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow(GridViewSelectedIndex);

        ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClick_FormViewByHiddenField("IratMetaDefinicioForm.aspx", "", GridViewSelectedId.ClientID);

        ButtonAdvancedSearch.OnClientClick = JavaScripts.SetOnClientClick("IratMetaDefinicioSearch.aspx", ""
            , 800, 650, SearchUpdatePanel.ClientID, EventArgumentConst.refreshLovListByDetailSearch);

        //ImageButtonReszletesAdatok.OnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        if (!IsPostBack)
        {
            KodTarakDropDownList_EljarasiSzakasz.FillAndSetEmptyValue(kcs_ELJARASI_SZAKASZ, LovListHeader1.ErrorPanel);
            KodTarakDropDownList_Irattipus.FillAndSetEmptyValue(kcs_IRATTIPUS, LovListHeader1.ErrorPanel);
            FillGridViewSearchResult(false);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshLovListByDetailSearch:
                    if (disable_refreshLovList != true)
                    {
                        FillGridViewSearchResult(true);
                    }
                    break;
            }
        }

        // scrollozhat�s�g �ll�t�sa
        UI.SetLovListGridViewScrollable(GridViewSearchResult, IratMetaDefinicioCPE, 15);

        // felesleges keres�mez�k elt�ntet�se
        if (Filter == Constants.FilterType.IratMetaDefinicio.Ugyirat)
        {
            tr_Ugyiratdarab.Visible = false; // nincs �rtelme, hogy l�tsszon
            tr_Irat.Visible = false;
        }
        else if (Filter == Constants.FilterType.IratMetaDefinicio.Ugyiratdarab)
        {
            tr_Irat.Visible = false;
        }

    }



    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        JavaScripts.ResetScroll(Page, IratMetaDefinicioCPE);
        FillGridViewSearchResult(false);
    }

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }

    protected void FillGridViewSearchResult(bool searchObjectFromSession)
    {
        GridViewSelectedId.Value = String.Empty;
        GridViewSelectedIndex.Value = String.Empty;

        EREC_IratMetaDefinicioService service = eRecordService.ServiceFactory.GetEREC_IratMetaDefinicioService();
        EREC_IratMetaDefinicioSearch search = null;

        if (searchObjectFromSession == true)
        {
            search = (EREC_IratMetaDefinicioSearch)Search.GetSearchObject(Page, new EREC_IratMetaDefinicioSearch());
        }
        else
        {
            search = new EREC_IratMetaDefinicioSearch();


            if (!String.IsNullOrEmpty(Filter))
            {
                switch (Filter)
                {
                    case Constants.FilterType.IratMetaDefinicio.Ugyirat:
                        search.Ugytipus.Value = "";
                        search.Ugytipus.Operator = Query.Operators.notnull;

                        search.EljarasiSzakasz.Value = "";
                        search.EljarasiSzakasz.Operator = Query.Operators.isnull;

                        search.Irattipus.Value = "";
                        search.Irattipus.Operator = Query.Operators.isnull;
                        break;
                    case Constants.FilterType.IratMetaDefinicio.Ugyiratdarab:
                        search.Ugytipus.Value = "";
                        search.Ugytipus.Operator = Query.Operators.notnull;

                        search.EljarasiSzakasz.Value = "";
                        search.EljarasiSzakasz.Operator = Query.Operators.notnull;

                        search.Irattipus.Value = "";
                        search.Irattipus.Operator = Query.Operators.isnull;
                        break;
                    case Constants.FilterType.IratMetaDefinicio.Irat:
                        search.Ugytipus.Value = "";
                        search.Ugytipus.Operator = Query.Operators.notnull;

                        search.EljarasiSzakasz.Value = "";
                        search.EljarasiSzakasz.Operator = Query.Operators.notnull;

                        search.Irattipus.Value = "";
                        search.Irattipus.Operator = Query.Operators.notnull;
                        break;
                    case Constants.FilterType.IratMetaDefinicio.All:
                        break;
                    default:
                        break;
                }
            }

            if (!String.IsNullOrEmpty(TextBoxUgykorKod.Text))
            {
                search.UgykorKod.Value = TextBoxUgykorKod.Text;
                search.UgykorKod.Operator = Search.GetOperatorByLikeCharater(TextBoxUgykorKod.Text);                
            }

            if (!String.IsNullOrEmpty(TextBoxUgytipus.Text))
            {
                search.UgytipusNev.Value = TextBoxUgytipus.Text;
                search.UgytipusNev.Operator = Search.GetOperatorByLikeCharater(TextBoxUgytipus.Text);
            }

            // TODO: megnevez�sre, nem k�dra keresni
            if (!String.IsNullOrEmpty(KodTarakDropDownList_EljarasiSzakasz.SelectedValue))
            {
                search.EljarasiSzakasz.Value = KodTarakDropDownList_EljarasiSzakasz.SelectedValue;
                search.EljarasiSzakasz.Operator = Query.Operators.equals;
            }

            if (!String.IsNullOrEmpty(KodTarakDropDownList_Irattipus.SelectedValue))
            {
                search.Irattipus.Value = KodTarakDropDownList_Irattipus.SelectedValue;
                search.Irattipus.Operator = Query.Operators.equals;
            }

        }


        search.OrderBy = "UgykorKod, Ugytipus, EljarasiSzakasz, Irattipus";

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAllWithExtension(ExecParam, search);

        ui.GridViewFill(GridViewSearchResult, result, LovListHeader1.ErrorPanel, LovListHeader1.ErrorUpdatePanel);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
        
     }


    protected void LovListFooter_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToString() == CommandName.Ok)
        {
            if (!String.IsNullOrEmpty(GridViewSelectedIndex.Value) && !String.IsNullOrEmpty(GridViewSelectedId.Value))
            {
                int selectedIndex = -1;
                Int32.TryParse(GridViewSelectedIndex.Value, out selectedIndex);

                //string selectedText = ListBoxSearchResult.SelectedItem.Text;
                if (selectedIndex > -1)
                {
                    GridViewSearchResult.SelectedIndex = selectedIndex;

                    string selectedId = UI.GetGridViewSelectedRecordId(GridViewSearchResult);
                    string IrattariTetelszam = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 1);
                    string UgykorKod = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 2);
                    string Ugytipus = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 3);
                    string EljarasiSzakasz = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 4);
                    string Irattipus = UI.GetGridViewSelectedRecord_ColumnValue(GridViewSearchResult, 5);

                    string selectedText = UgykorKod;
                    if (!String.IsNullOrEmpty(Ugytipus))
                    {
                        selectedText += "/" + Ugytipus;

                        if (!String.IsNullOrEmpty(EljarasiSzakasz))
                        {
                            selectedText += "/" + EljarasiSzakasz;

                            if (!String.IsNullOrEmpty(Irattipus))
                            {
                                selectedText += "/" + Irattipus;
                            }
                        }
                    }

                    bool refreshCallingWindow = JavaScripts.SendBackResultToCallingWindow(Page, selectedId, selectedText);
                    if (refreshCallingWindow == true)
                    {
                        string paramValue_refreshCallingWindow = Request.QueryString.Get(QueryStringVars.RefreshCallingWindow);
                        if (paramValue_refreshCallingWindow != null && paramValue_refreshCallingWindow == "1") { refreshCallingWindow = true; }
                        else { refreshCallingWindow = false; }
                    }

                    JavaScripts.RegisterCloseWindowClientScript(Page, refreshCallingWindow);
                    disable_refreshLovList = true;
                }
            }
            else
            {
                GridViewSelectedId.Value = String.Empty;
                GridViewSelectedIndex.Value = String.Empty;

                ResultError.DisplayErrorOnErrorPanel(LovListHeader1.ErrorPanel
                    , Resources.Error.ErrorLabel, Resources.Error.UINoSelectedItem);
                LovListHeader1.ErrorUpdatePanel.Update();
            }
        }
    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    

    

    
    protected override void Render(HtmlTextWriter writer)
    {

        foreach (GridViewRow row in GridViewSearchResult.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow || row.RowType == DataControlRowType.EmptyDataRow)
            {
                JavaScripts.SetLovListGridViewRowScripts(row, GridViewSelectedIndex, GridViewSelectedId);
            }
        }

        base.Render(writer);

    }


}
