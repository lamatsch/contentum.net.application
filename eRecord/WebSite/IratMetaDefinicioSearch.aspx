<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IratMetaDefinicioSearch.aspx.cs" Inherits="IratMetaDefinicioSearch"
    Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/PartnerTextBox.ascx" TagName="PartnerTextBox" TagPrefix="uc5" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc17" %>
<%@ Register Src="eRecordComponent/AlszamIntervallum_SearchFormControl.ascx" TagName="AlszamIntervallum_SearchFormControl"
    TagPrefix="uc15" %>
<%@ Register Src="eRecordComponent/FoszamIntervallum_SearchFormControl.ascx" TagName="FoszamIntervallum_SearchFormControl"
    TagPrefix="uc16" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc13" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc14" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td style="width: 100%">
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor" id="tr_AutoGeneratePartner_CheckBox" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="R�vid n�v:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="RovidNevTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc6:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="�gyt�pus k�d:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="UgyTipusKodTextBox" runat="server" Width="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="�gyt�pus:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="UgyTipusTextBox" runat="server" Width="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trVegrehajto" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Elj�r�si szakasz:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="EljarasiSzakaszKodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trAtvevo" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Iratt�pus:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="IratTipusKodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="Gener�lt t�rgy:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="GeneraltTargyTextBox" runat="server" Width="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelIntezesiIdo" runat="server" Text="Int�z�si id�:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="IntezesiIdoTextBox" CssClass="mrUrlapInputNumber" runat="server" />
                                            <uc11:KodtarakDropDownList ID="IdoegysegKodtarakDropDownList" CssClass="mrUrlapInputRovid"
                                                runat="server" />
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rblIntezesiIdoKotott" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Selected="false" Text="K�t�tt" />
                                                <asp:ListItem Value="0" Selected="false" Text="Nem k�t�tt" />
                                                <asp:ListItem Value="NotSet" Selected="true" Text="�sszes" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgyiratHataridoKitolas" runat="server" Text="Irat hat�rideje �gyirat hat�ridej�t:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:RadioButtonList ID="rblUgyiratHataridoKitolas" runat="server"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Selected="false" Text="Kitolhatja" />
                                    <asp:ListItem Value="0" Selected="false" Text="Nem tolhatja ki" />
                                    <asp:ListItem Value="NotSet" Selected="true" Text="�sszes" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr1" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label12" runat="server" Text="Szign�l�s t�pusa:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="SzignalasTipusaKodtarakDropDownList" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr2" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label13" runat="server" Text="Javasolt felel�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:CsoportTextBox ID="JavasoltFelelosCsoportTextBox" runat="server" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2" rowspan="2" style="padding-bottom:30px;">
                                <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server">
                                </uc5:Ervenyesseg_SearchFormComponent>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
