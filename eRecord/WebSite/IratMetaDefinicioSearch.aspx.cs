using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;


public partial class IratMetaDefinicioSearch : Contentum.eUtility.UI.PageBase
{
    public const string Header = "$Header: IratMetaDefinicioSearch.aspx.cs, 12, 2009.07.07. 12:46:38, Boda Eszter$";
    public const string Version = "$Revision: 12$";

    //private string SzignalasTipusa = "";
    //private string JavasoltFelelosId = "";

    private Type _type = typeof(EREC_IratMetaDefinicioSearch);
    //private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";
    private const string kcs_ELJARASISZAKASZ = "ELJARASI_SZAKASZ";
    //private const string kcs_IRATFAJTA = "IRAT_FAJTA";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_SZIGNALASTIPUSA = "SZIGNALAS_TIPUSA";
    private const string kcs_IDOEGYSEG = "IDOEGYSEG";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
        IraIrattariTetelTextBox1.FromIratmetaDefiniciok = true;
        IraIrattariTetelTextBox1.Validate = false;
        JavasoltFelelosCsoportTextBox.Validate = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (" + Resources.Search.IratMetaDefinicioSearchHeaderTitle + ")");
        SearchHeader1.HeaderTitle = Resources.Search.IratMetaDefinicioSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            EREC_IratMetaDefinicioSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_IratMetaDefinicioSearch)Search.GetSearchObject(Page, new EREC_IratMetaDefinicioSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        Logger.Info("LoadComponentsFromSearchObject");
        EREC_IratMetaDefinicioSearch erec_IratMetaDefinicioSearch = null;
        if (searchObject != null) erec_IratMetaDefinicioSearch = (EREC_IratMetaDefinicioSearch)searchObject;

        if (erec_IratMetaDefinicioSearch != null)
        {

            EljarasiSzakaszKodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASISZAKASZ, SearchHeader1.ErrorPanel);
            EljarasiSzakaszKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicioSearch.EljarasiSzakasz.Value);

            //IratTipusKodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATFAJTA, SearchHeader1.ErrorPanel);
            IratTipusKodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATTIPUS, SearchHeader1.ErrorPanel);
            IratTipusKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicioSearch.Irattipus.Value);

            RovidNevTextBox.Text = erec_IratMetaDefinicioSearch.Rovidnev.Value;

            IraIrattariTetelTextBox1.Id_HiddenField = erec_IratMetaDefinicioSearch.Ugykor_Id.Value;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);

            UgyTipusTextBox.Text = erec_IratMetaDefinicioSearch.UgytipusNev.Value;

            UgyTipusKodTextBox.Text = erec_IratMetaDefinicioSearch.Ugytipus.Value;

            GeneraltTargyTextBox.Text = erec_IratMetaDefinicioSearch.GeneraltTargy.Value;

            switch (erec_IratMetaDefinicioSearch.UgyiratIntezesiIdoKotott.Value)
            {
                case "0":
                    rblIntezesiIdoKotott.SelectedValue = "0";
                    break;
                case "1":
                    rblIntezesiIdoKotott.SelectedValue = "1";
                    break;
                default:
                    rblIntezesiIdoKotott.SelectedValue = "NotSet";
                    break;
            }

            switch (erec_IratMetaDefinicioSearch.UgyiratHataridoKitolas.Value)
            {
                case "0":
                    rblUgyiratHataridoKitolas.SelectedValue = "0";
                    break;
                case "1":
                    rblUgyiratHataridoKitolas.SelectedValue = "1";
                    break;
                default:
                    rblUgyiratHataridoKitolas.SelectedValue = "NotSet";
                    break;
            }

            if (!String.IsNullOrEmpty(erec_IratMetaDefinicioSearch.UgyiratIntezesiIdo.Value))
            {
                IntezesiIdoTextBox.Text = erec_IratMetaDefinicioSearch.UgyiratIntezesiIdo.Value;
            }

            if (!String.IsNullOrEmpty(erec_IratMetaDefinicioSearch.Idoegyseg.Value))
            {
                IdoegysegKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicioSearch.Idoegyseg.Value);
            }

            IdoegysegKodtarakDropDownList.FillAndSetEmptyValue(kcs_IDOEGYSEG, SearchHeader1.ErrorPanel);
            IdoegysegKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicioSearch.Idoegyseg.Value);

            JavasoltFelelosCsoportTextBox.Id_HiddenField = erec_IratMetaDefinicioSearch.Manual_Csoport_Id_Felelos.Value;
            JavasoltFelelosCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            SzignalasTipusaKodtarakDropDownList.FillAndSetEmptyValue(kcs_SZIGNALASTIPUSA, SearchHeader1.ErrorPanel);
            SzignalasTipusaKodtarakDropDownList.SetSelectedValue(erec_IratMetaDefinicioSearch.Manual_SzignalasTipusa.Value);

            Ervenyesseg_SearchFormComponent1.SetDefault(erec_IratMetaDefinicioSearch.ErvKezd, erec_IratMetaDefinicioSearch.ErvVege);


        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IratMetaDefinicioSearch SetSearchObjectFromComponents()
    {
        Logger.Info("SetSearchObjectFromComponents");
        EREC_IratMetaDefinicioSearch erec_IratMetaDefinicioSearch = (EREC_IratMetaDefinicioSearch)SearchHeader1.TemplateObject;
        if (erec_IratMetaDefinicioSearch == null)
        {
            erec_IratMetaDefinicioSearch = new EREC_IratMetaDefinicioSearch();
        }

        if (!String.IsNullOrEmpty(EljarasiSzakaszKodtarakDropDownList.SelectedValue))
        {
            erec_IratMetaDefinicioSearch.EljarasiSzakasz.Value = EljarasiSzakaszKodtarakDropDownList.SelectedValue;
            erec_IratMetaDefinicioSearch.EljarasiSzakasz.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(IratTipusKodtarakDropDownList.SelectedValue))
        {
            erec_IratMetaDefinicioSearch.Irattipus.Value = IratTipusKodtarakDropDownList.SelectedValue;
            erec_IratMetaDefinicioSearch.Irattipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(RovidNevTextBox.Text))
        {
            erec_IratMetaDefinicioSearch.Rovidnev.Value = RovidNevTextBox.Text;
            erec_IratMetaDefinicioSearch.Rovidnev.Operator = Search.GetOperatorByLikeCharater(RovidNevTextBox.Text);
        }

        if (!String.IsNullOrEmpty(IraIrattariTetelTextBox1.Id_HiddenField))
        {
            erec_IratMetaDefinicioSearch.Ugykor_Id.Value = IraIrattariTetelTextBox1.Id_HiddenField;
            erec_IratMetaDefinicioSearch.Ugykor_Id.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(UgyTipusKodTextBox.Text))
        {
            erec_IratMetaDefinicioSearch.Ugytipus.Value = UgyTipusKodTextBox.Text;
            erec_IratMetaDefinicioSearch.Ugytipus.Operator = Search.GetOperatorByLikeCharater(UgyTipusKodTextBox.Text);
        }

        if (!String.IsNullOrEmpty(UgyTipusTextBox.Text))
        {
            erec_IratMetaDefinicioSearch.UgytipusNev.Value = UgyTipusTextBox.Text;
            erec_IratMetaDefinicioSearch.UgytipusNev.Operator = Search.GetOperatorByLikeCharater(UgyTipusTextBox.Text);
        }

        if (!String.IsNullOrEmpty(GeneraltTargyTextBox.Text))
        {
            erec_IratMetaDefinicioSearch.GeneraltTargy.Value = GeneraltTargyTextBox.Text;
            erec_IratMetaDefinicioSearch.GeneraltTargy.Operator = Search.GetOperatorByLikeCharater(GeneraltTargyTextBox.Text);
        }

        if (!String.IsNullOrEmpty(IntezesiIdoTextBox.Text))
        {
            erec_IratMetaDefinicioSearch.UgyiratIntezesiIdo.Value = IntezesiIdoTextBox.Text;
            erec_IratMetaDefinicioSearch.UgyiratIntezesiIdo.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(IdoegysegKodtarakDropDownList.SelectedValue))
        {
            erec_IratMetaDefinicioSearch.Idoegyseg.Value = IdoegysegKodtarakDropDownList.SelectedValue;
            erec_IratMetaDefinicioSearch.Idoegyseg.Operator = Query.Operators.equals;
        }

        switch (rblIntezesiIdoKotott.SelectedValue)
        {
            case "0":
                erec_IratMetaDefinicioSearch.UgyiratIntezesiIdoKotott.Value = "0";
                erec_IratMetaDefinicioSearch.UgyiratIntezesiIdoKotott.Operator = Query.Operators.equals;
                break;
            case "1":
                erec_IratMetaDefinicioSearch.UgyiratIntezesiIdoKotott.Value = "1";
                erec_IratMetaDefinicioSearch.UgyiratIntezesiIdoKotott.Operator = Query.Operators.equals;
                break;
            case "NotSet":
            default:
                break;
        }

        switch (rblUgyiratHataridoKitolas.SelectedValue)
        {
            case "0":
                erec_IratMetaDefinicioSearch.UgyiratHataridoKitolas.Value = "0";
                erec_IratMetaDefinicioSearch.UgyiratHataridoKitolas.Operator = Query.Operators.equals;
                break;
            case "1":
                erec_IratMetaDefinicioSearch.UgyiratHataridoKitolas.Value = "1";
                erec_IratMetaDefinicioSearch.UgyiratHataridoKitolas.Operator = Query.Operators.equals;
                break;
            case "NotSet":
            default:
                break;
        }

        if (!String.IsNullOrEmpty(JavasoltFelelosCsoportTextBox.Id_HiddenField))
        {
            erec_IratMetaDefinicioSearch.Manual_Csoport_Id_Felelos.Value = JavasoltFelelosCsoportTextBox.Id_HiddenField;
            erec_IratMetaDefinicioSearch.Manual_Csoport_Id_Felelos.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(SzignalasTipusaKodtarakDropDownList.SelectedValue))
        {
            erec_IratMetaDefinicioSearch.Manual_SzignalasTipusa.Value = SzignalasTipusaKodtarakDropDownList.SelectedValue;
            erec_IratMetaDefinicioSearch.Manual_SzignalasTipusa.Operator = Query.Operators.equals;
        }

        //DatumIntervallum_SearchCalendarControl1.SetSearchObjectFields(erec_IratMetaDefinicioSearch.VegrehajtasDatuma);
        //DatumIntervallum_SearchCalendarControl2.SetSearchObjectFields(erec_IratMetaDefinicioSearch.LezarasDatuma);
        ////DatumIntervallum_SearchCalendarControl3.SetSearchObjectFields(erec_IratMetaDefinicioSearch.);

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(erec_IratMetaDefinicioSearch.ErvKezd, erec_IratMetaDefinicioSearch.ErvVege);

        return erec_IratMetaDefinicioSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("SearchHeader1_ButtonsClick (Command = " + e.CommandName + ")");
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("SearchFooter1_ButtonsClick (Command = " + e.CommandName + ")");
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IratMetaDefinicioSearch searchObject = SetSearchObjectFromComponents();
            if (Search.IsIdentical(searchObject, GetDefaultSearchObject()))
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IratMetaDefinicioSearch GetDefaultSearchObject()
    {
        return new EREC_IratMetaDefinicioSearch();
    }

}
