﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IratPldUgyiratbaHelyezesForm.aspx.cs" Inherits="IratPldUgyiratbaHelyezesForm"%>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>
<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>
                
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
    
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                   
                    
                    <asp:Panel ID="IratPeldanyokListPanel" runat="server" Visible="false">
                    
                 <br />
                 <asp:Label ID="Label_IratPeldanyok" runat="server" Text="Iratpéldányok:" Visible="false" CssClass="GridViewTitle"></asp:Label>          
                 <br />  
                    <asp:GridView ID="PldIratPeldanyokGridView" runat="server"
                                            CellPadding="0" CellSpacing="0" BorderWidth="1" GridLines="Both" AllowPaging="False"
                                            PagerSettings-Visible="false" AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id" 
                                            OnRowDataBound="PldIratPeldanyokGridView_RowDataBound">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <%--<HeaderTemplate>
                                                        <asp:ImageButton ID="SelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg" AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                        &nbsp;&nbsp;
                                                        <asp:ImageButton ID="DeSelectingRowsImageButton"
                                                            runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg" AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                    </HeaderTemplate>--%>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                                       
                                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                                <ItemTemplate>                                    
                                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                        Visible="false" OnClientClick="return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                                       
                                        <asp:BoundField DataField="IktatoSzam_Merge" HeaderText="Iktatószám" 
                                            SortExpression="EREC_IraIktatokonyvek.MegkulJelzes, EREC_UgyUgyiratdarabok.Foszam, EREC_IraIratok.Alszam, EREC_PldIratPeldanyok.Sorszam">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="160px"/>
                                        </asp:BoundField>             
                                        <asp:BoundField DataField="EREC_IraIratok_Targy" HeaderText="Tárgy" SortExpression="EREC_IraIratok_Targy">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="EREC_KuldKuldemenyek_PostazasIranya" HeaderText="Irány" SortExpression="EREC_KuldKuldemenyek_PostazasIranya">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField> --%>   
                                        <asp:BoundField DataField="VisszaerkezesiHatarido" HeaderText="Határidő" SortExpression="VisszaerkezesiHatarido">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NevSTR_Cimzett" HeaderText="Címzett" SortExpression="NevSTR_Cimzett">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="150px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="KuldesMod_Nev" HeaderText="Küldésmód" SortExpression="KuldesMod_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FelhasznaloCsoport_Id_Orzo_Nev" HeaderText="Irat helye" SortExpression="FelhasznaloCsoport_Id_Orzo_Nev">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px"/>
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="EREC_KuldKuldemenyek_Bekuldok" HeaderText="Küldő/Feladó nevek" SortExpression="EREC_KuldKuldemenyek_Bekuldok">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="EREC_KuldKuldemenyek_HivatkozasiSzam" HeaderText="Hivatkozási szám" SortExpression="EREC_KuldKuldemenyek_HivatkozasiSzam">
                                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            <HeaderStyle CssClass="GridViewBorderHeader" />
                                        </asp:BoundField>--%>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Zárolás" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>                   
                    
                    </asp:Panel>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                </td>
            </tr>
            <tr>
            <td>
            <br />
                
                <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">     
                <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                <br />
                </asp:Panel>
              
            </td></tr>
            <tr>
                <td>
                
                    
                    <uc2:FormFooter ID="FormFooter1" runat="server" />                 
                </td>
            </tr>            
        </table>
     </asp:Panel>
      <asp:Panel ID="ResultPanel" runat="server" Visible="false" width="90%">
            <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
            <asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
            <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
            <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
            <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">A kijelölt iratpéldányok ügyiratba helyezése sikeresen végrehajtódott.</div>
                <table>
                    <tr>                        
                        <td>
                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                               CommandName="Close" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>      
      </asp:Panel>
</asp:Content>
