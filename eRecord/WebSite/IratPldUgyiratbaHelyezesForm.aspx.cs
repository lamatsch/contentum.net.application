﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using System.Data;

public partial class IratPldUgyiratbaHelyezesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
     
    private string IratPeldanyId = "";    
    private string[] IratPeldanyokArray;

    private const string FunkcioKod_IratpeldanyUgyiratbaHelyezes = "IratpeldanyUgyiratbaHelyezes";

    public string maxTetelszam = "0";


    protected void Page_Init(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        // FormFooternek beállítjuk a Modify üzemmódot:
        FormFooter1.Command = CommandName.Modify;

        // BLG_11293
        // outlook addInból hívva
        if (!string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratId)))
        {
 
            Contentum.eRecord.Service.EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            Contentum.eBusinessDocuments.ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            search.IraIrat_Id.Value = Request.QueryString.Get(QueryStringVars.IratId);
            search.IraIrat_Id.Operator = Query.Operators.equals;

            Contentum.eBusinessDocuments.Result resultPldGetAll = service.GetAllWithExtension(execparam, search);

            if (resultPldGetAll.IsError)
            {
                throw new Contentum.eUtility.ResultException(resultPldGetAll);
            }


            if (resultPldGetAll.Ds.Tables[0].Rows.Count > 0)
            {
                List<string> lstPldIratPeldanyok_Ids = new List<string>(); //(resultPldGetAll.Ds.Tables[0].Rows.Count);
               // int i = 0;

                foreach (DataRow row in resultPldGetAll.Ds.Tables[0].Rows)
                {
                    string currentId = row["Id"].ToString();
                    if (!String.IsNullOrEmpty(currentId))
                    {
                        lstPldIratPeldanyok_Ids.Add(currentId);
                        //lstPldIratPeldanyok_Ids[i] = currentId;
                        //i++;
                    }
                }
                IratPeldanyId = String.Join("','", lstPldIratPeldanyok_Ids.ToArray()) ;
            }
        } else
        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.IratPeldanyId)))
        {
            if (Session["SelectedIratPeldanyIds"] != null)
                IratPeldanyId = Session["SelectedIratPeldanyIds"].ToString();
        }
        else IratPeldanyId = Request.QueryString.Get(QueryStringVars.IratPeldanyId);


     if (!String.IsNullOrEmpty(IratPeldanyId))
        {
            IratPeldanyokArray = IratPeldanyId.Split(',');
        }      
        else
        {

        }

        // Jogosultságellenőrzés:
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_IratpeldanyUgyiratbaHelyezes);

        if (!IsPostBack)
        {
            LoadFormComponents();
        }


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        FormHeader1.HeaderTitle = Resources.Form.IratPldUgyiratbaHelyezeseHeaderTitle;
        
        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        ImageClose.OnClientClick = "window.returnValue='" + EventArgumentConst.refresh + "'; window.close(); return false;";


        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
            + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count).ToString();
                
    }

    private void LoadFormComponents()
    {
        
        if (String.IsNullOrEmpty(IratPeldanyId))
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            MainPanel.Visible = false;
        }
        else
        {           
            IratPeldanyokListPanel.Visible = true;

            FillIratPeldanyGridView();
        }      

    }




    private void FillIratPeldanyGridView()
    {
        DataSet ds = IratPeldanyokGridViewBind();

        // ellenőrzés:
        if (ds != null && ds.Tables[0].Rows.Count != IratPeldanyokArray.Length)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");            
        }

        // Van-e olyan rekord, ami nem adható át? 
        // CheckBoxok vizsgálatával:
        int count_ugyiratbaHelyezhetok = UI.GetGridViewSelectedCheckBoxesCount(PldIratPeldanyokGridView, "check");

        int count_ugyiratbaNEMHelyezhetok = IratPeldanyokArray.Length - count_ugyiratbaHelyezhetok;

        if (count_ugyiratbaNEMHelyezhetok > 0)
        {
            Label_Warning_IratPeldany.Text = String.Format(Resources.List.UI_IratPeldany_AthelyezesreKijeloltekWarning, count_ugyiratbaNEMHelyezhetok);            
            Panel_Warning_IratPeldany.Visible = true;
        }
    }




    protected DataSet IratPeldanyokGridViewBind()
    {
        if (IratPeldanyokArray != null && IratPeldanyokArray.Length > 0)
        {
            EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_PldIratPeldanyokSearch search = new EREC_PldIratPeldanyokSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            for (int i = 0; i < IratPeldanyokArray.Length; i++)
            {
                if (i == 0)
                {
                    search.Id.Value = "'" + IratPeldanyokArray[i] + "'";
                }
                else
                {
                    search.Id.Value += ",'" + IratPeldanyokArray[i] + "'";
                }
            }
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(PldIratPeldanyokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }


    protected void PldIratPeldanyokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckUgyiratbaHelyezes(e, Page);
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_IratpeldanyUgyiratbaHelyezes))
            {
                int selectedIds = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null).Count;

                if (selectedIds > int.Parse(maxTetelszam))
                {
                    string javaS = "alert ('{alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIds.ToString() + "'); return false; } ";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                }


                if (String.IsNullOrEmpty(IratPeldanyId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                   
                    EREC_PldIratPeldanyokService service = eRecordService.ServiceFactory.GetEREC_PldIratPeldanyokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    List<string> selectedItemsList = ui.GetGridViewSelectedRows(PldIratPeldanyokGridView, FormHeader1.ErrorPanel, null);
                    String[] IratPeldanyIds = new String[selectedItemsList.Count];
                    for (int i = 0; i < IratPeldanyIds.Length; i++)
                    {
                        IratPeldanyIds[i] = selectedItemsList[i];
                    }



                    Result result = service.UgyiratbaHelyezes_Tomeges(execParam, IratPeldanyIds);

                    if (result.IsError == false)
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, IratPeldanyId);
                        JavaScripts.RegisterCloseWindowClientScript(Page);

                        MainPanel.Visible = false;
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(PldIratPeldanyokGridView, result);
                    }

                }


            }
            else
            {
                UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
            }

        }
    }

    protected void ImagePrint_Click(object sender, ImageClickEventArgs e)
    {

    }


}
