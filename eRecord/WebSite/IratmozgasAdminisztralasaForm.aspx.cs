﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using System.Data;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;

public partial class IratmozgasAdminisztralasaForm : Contentum.eUtility.UI.PageBase
{
    #region properties
    String selectedItemIds;
    String[] selectedItemsArray;
    bool isTuk = false;
    public string maxTetelszam = "0";
    private UI ui = new UI();

    public const string kuldemeny = "Küldemény";
    public const string iratpeldany = "Iratpéldány";
    public const string ugyirat = "Ügyirat";
    public const string dosszie = "Dosszié";
    #endregion

    #region Oldal betöltése
    protected void Page_Load(object sender, EventArgs e)
    {
        //csak TÜK kezelőhasználhatja a funkciót!
        isTuk = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.TUK);

        //sessionben átadott adatok kinyerése
        selectedItemIds = Session["selectedItemIds"] == null ? String.Empty : Session["selectedItemIds"].ToString();
        if (!String.IsNullOrEmpty(selectedItemIds))
        {
            selectedItemsArray = selectedItemIds.Split(',');
        }
        SetComponentReadOnly(!isTuk);
        if (!isTuk)
            FormFooter1.Command = CommandName.Cancel;

        FormFooter1.Visible = true;
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (!IsPostBack)
        {
            FillGridView();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        FormFooter1.Command = CommandName.Modify;
        FormHeader1.DisableModeLabel = true;
        FormHeader1.HeaderTitle = Resources.Form.IratMozgasAdministralasHeaderTitle;
        IraKezbesitesiTetelekGridView.Visible = true;
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        int cntKijelolt_Lezaras = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, FormHeader1.ErrorPanel, null).Count;

        labelTetelekSzamaDb.Text = cntKijelolt_Lezaras.ToString();
    }
    #endregion


    private void SetComponentReadOnly(bool value)
    {
        AtvevoTextBox.ReadOnly = value;
        AtvetelIdopontjaCalendarControl.ReadOnly = value;
        txtNyilvantartasiSzam.ReadOnly = value;
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {

        if (e.CommandName == CommandName.Save)
        {
            //csak TÜK
            if (isTuk)
            {
                //administráció végrahajtása
                int selectedIdsInt = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, FormHeader1.ErrorPanel, null).Count;

                if (selectedIdsInt > int.Parse(maxTetelszam))
                {
                    // ha a JavaScript végén "return false;" van, nem jelenik meg az üzenet...
                    string javaS = "alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + selectedIdsInt.ToString() + "');"; // return false; } ";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "maxItemNumberExceed", javaS, true);
                    return;
                }
                if (selectedIdsInt == 0)
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }

                //átadás, átvétel
                List<string> selectedKezbesitesiTetelekList = ui.GetGridViewSelectedRows(IraKezbesitesiTetelekGridView, FormHeader1.ErrorPanel, null);
                String atadandoList = String.Join(",", selectedKezbesitesiTetelekList.ToArray());
                //foreach (string KezbesitesiTetelId in selectedKezbesitesiTetelekList)
                //{
                //    EREC_IraKezbesitesiTetelekService serviceKezbTetel = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                //    ExecParam execParamKezbTetel = UI.SetExecParamDefault(Page, new ExecParam());
                //    execParamKezbTetel.Record_Id = KezbesitesiTetelId;
                //    EREC_IraKezbesitesiTetelek tetel = (EREC_IraKezbesitesiTetelek)serviceKezbTetel.Get(execParamKezbTetel).Record;

                //    if (tetel.Allapot == KodTarak.KEZBESITESITETEL_ALLAPOT.Atadasra_kijelolt)
                //    {
                //        atadandoList += String.IsNullOrEmpty(atadandoList) ? KezbesitesiTetelId : "," + KezbesitesiTetelId;
                //        EREC_IraKezbesitesiTetelekList.Add(tetel);
                //    }
                //}

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_IraKezbesitesiTetelekService serviceAtadAtvesz = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                Result atadasRes = serviceAtadAtvesz.Atadas_Tomeges(execParam.Clone(), atadandoList.Split(','));
                if (!string.IsNullOrEmpty(atadasRes.ErrorCode))
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ResultError.GetErrorMessageByErrorCode(Convert.ToInt32(atadasRes.ErrorCode)));
                    return;
                }
                Result atvetelRes = serviceAtadAtvesz.Atvetel_Tomeges_Idegen(execParam.Clone(), atadandoList.Split(','), AtvevoTextBox.Id_HiddenField);
                if (!string.IsNullOrEmpty(atvetelRes.ErrorCode))
                {
                    ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ResultError.GetErrorMessageByErrorCode(Convert.ToInt32(atvetelRes.ErrorCode)));
                    return;
                }


                //Felülírjuk a felületen megadott értékekkel az EREC_IraKezbesitesiTetelek -t
                foreach (string KezbesitesiTetelId in selectedKezbesitesiTetelekList)
                {
                    string version = String.Empty;
                    EREC_IraKezbesitesiTetelekService serviceKezbTetel = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                    ExecParam execParamKezbTetel = UI.SetExecParamDefault(Page, new ExecParam());
                    execParamKezbTetel.Record_Id = KezbesitesiTetelId;
                    Result result_Search = serviceKezbTetel.Get(execParamKezbTetel);
                    EREC_IraKezbesitesiTetelek tetel = (EREC_IraKezbesitesiTetelek)result_Search.Record;

                    //verzió mentése, különben hibát ad
                    version = tetel.Base.Ver;

                    tetel.Updated.SetValueAll(false);
                    tetel.Base.Updated.SetValueAll(false);

                    tetel.AtadasDat = AtvetelIdopontjaCalendarControl.Text;
                    tetel.Updated.AtadasDat = true;

                    tetel.AtvetelDat = AtvetelIdopontjaCalendarControl.Text;
                    tetel.Updated.AtvetelDat = true;

                    tetel.Megjegyzes = txtNyilvantartasiSzam.Text;
                    tetel.Updated.Megjegyzes = true;

                    tetel.Felhasznalo_Id_AtvevoUser = AtvevoTextBox.Id_HiddenField;
                    tetel.Updated.Felhasznalo_Id_AtvevoUser = true;

                    //tetel.Csoport_Id_Cel

                    tetel.Base.Ver = version;
                    tetel.Base.Updated.Ver = true;

                    ExecParam execParam_TetelUpdate = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam_TetelUpdate.Record_Id = tetel.Id;

                    EREC_IraKezbesitesiTetelekService serviceKezbTetelUpdate = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
                    Result result_KezbTetelUpdate = serviceKezbTetelUpdate.Update(execParam_TetelUpdate, tetel);

                    if (!string.IsNullOrEmpty(result_KezbTetelUpdate.ErrorMessage))
                    {
                        ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ResultError.GetErrorMessageByErrorCode(Convert.ToInt32(result_KezbTetelUpdate.ErrorMessage)));
                        return;
                    }

                    //TODO címzett állítása, ha kell!
                    //EREC_PldIratPeldanyok pld = new EREC_PldIratPeldanyok();
                    //pld.Cim_id_Cimzett
                    
                }

                //sikeresen lefutott minden, bezárjuk az oldalt
                //JavaScripts.RegisterSelectedRecordIdToParent(Page, "");
                JavaScripts.RegisterCloseWindowClientScript(Page);
            }
            else
            {
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_NotTUK, "");
            }
        }
    }

    #region gridView
    private void FillGridView()
    {
        try
        {
            DataSet ds = GridViewBind();

            // ellenőrzés:
            if (ds != null && ds.Tables[0].Rows.Count != selectedItemsArray.Length)
            {
                // nem annyi rekord jött, mint amennyit vártunk:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            }
        }
        catch (Contentum.eUtility.ResultException e)
        {
            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, Contentum.eUtility.ResultException.GetResultFromException(e));
        }
    }

    protected DataSet GridViewBind()
    {
        if (selectedItemsArray != null && selectedItemsArray.Length > 0)
        {
            EREC_IraKezbesitesiTetelekService service = eRecordService.ServiceFactory.GetEREC_IraKezbesitesiTetelekService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_IraKezbesitesiTetelekSearch search = new EREC_IraKezbesitesiTetelekSearch();
            // keresési objektum beállítása: szűrés az Id-kra:
            for (int i = 0; i < selectedItemsArray.Length; i++)
            {
                if (i == 0)
                {
                    search.Id.Value = "'" + selectedItemsArray[i] + "'";
                }
                else
                {
                    search.Id.Value += ",'" + selectedItemsArray[i] + "'";
                }
            }
            search.Id.Value = Search.GetSqlInnerString(selectedItemsArray);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(IraKezbesitesiTetelekGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void IraKezbesitesiTetelekGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Típus kiírása
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label labelTipus = (Label)e.Row.FindControl("labelTipus");
            if (labelTipus != null && labelTipus.Text.Trim() != String.Empty)
            {
                switch (labelTipus.Text.Trim())
                {
                    case Constants.TableNames.EREC_KuldKuldemenyek:
                        labelTipus.Text = kuldemeny;
                        break;
                    case Constants.TableNames.EREC_PldIratPeldanyok:
                        labelTipus.Text = iratpeldany;
                        break;
                    case Constants.TableNames.EREC_UgyUgyiratok:
                        labelTipus.Text = ugyirat;
                        break;
                    case Constants.TableNames.KRT_Mappak:
                        labelTipus.Text = dosszie;
                        break;
                }
            }
        }

        //GridView_RowDataBound_SetVegyesInfo(e);
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        //IratPeldanyok.IratPeldanyokGridView_RowDataBound_CheckAtadasraKijeloles(e, Page);
        IraKezbesitesiTetelek.GridView_RowDataBound_CheckAtadasraKijeloles(e, Page);
        //BaseEREC_IraKezbesitesiTetelek.

        CheckBox cb = (CheckBox)e.Row.FindControl("check");
        if (cb != null)
            cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                "; return true;");

        //UI.GridView_RowDataBound_SetCheckBoxCheckedValue(e, Page, "cbUgyintezesModja", "UgyintezesModja");
        UI.SetFeladatInfo(e, GetObjType(e), GetObjId(e));

        GridView_RowDataBound_SetKezbesitesiTetelAllapot(e);

    }

    private void GridView_RowDataBound_SetKezbesitesiTetelAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;


            // megjelöljük a kézbesítési tételekk sorait
            bool isKezbesitesiTetel = false;
            isKezbesitesiTetel = (drw.Row.Table.Columns.Contains("Id") && !String.IsNullOrEmpty(drw["Id"].ToString()));

            if (isKezbesitesiTetel)
            {
                string allapotnev = GetKezbesitesiTetelAllapot(e);
                Label lbAllapotNev = (Label)e.Row.FindControl("labelAllapotNev");
                lbAllapotNev.Text = allapotnev;
            }
        }
    }

    private string GetKezbesitesiTetelAllapot(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string allapot = null;
                string allapot_nev = null;
                if (drw.Row.Table.Columns.Contains("Allapot"))
                {
                    allapot = drw["Allapot"].ToString();
                    if (!String.IsNullOrEmpty(allapot))
                    {
                        allapot_nev = Contentum.eUtility.KodTar_Cache.GetKodtarErtekByKodCsoportKodTar(KodTarak.KEZBESITESITETEL_ALLAPOT.KodcsoportKod, allapot, Page);
                    }
                }

                return allapot_nev;
            }
        }

        return String.Empty;
    }

    private string GetObjType(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string Obj_type = String.Empty;
                if (drw.Row.Table.Columns.Contains("Obj_type"))
                {
                    Obj_type = drw["Obj_type"].ToString();
                }

                return Obj_type;
            }
        }

        return String.Empty;
    }

    private string GetObjId(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            if (drw != null)
            {
                string Obj_Id = String.Empty;
                if (drw.Row.Table.Columns.Contains("Obj_Id"))
                {
                    Obj_Id = drw["Obj_Id"].ToString();
                }

                return Obj_Id;
            }
        }

        return String.Empty;
    }
    #endregion
}