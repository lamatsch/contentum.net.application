﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IratokByIktatokonyvAndDatePrintForm.aspx.cs" Inherits="IratokByIktatokonyvAndDatePrintForm"
    Title="Untitled Page" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList" TagPrefix="uc4" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="cc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IratokByIktatokonyvAndDatePrintFormHeaderTitle %>" />
    <br />
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapNyitoSor">
                            <td class="mrUrlapCaption_short">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapSpacer">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapCaption">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                            <td class="mrUrlapMezo">
                                <img alt=" " src="images/hu/design/spacertrans.gif" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label15" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label49" runat="server" Text="Iktatókönyv:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc4:IraIktatoKonyvekDropDownList ID="IraIktatoKonyvekDropDownList1" runat="server" Validate="true" />
                                <asp:DropDownList ID="Iktatokonyvek_DropDownLis_Ajax" runat="server" CssClass="mrUrlapInputComboBox" Visible="False" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label3" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="Label5" runat="server" Text="Dátum:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <cc:CalendarControl ID="DatumCalendarControl1" runat="server" Validate="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label1" runat="server" Text="Postázás iránya:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <asp:DropDownList ID="PostazasIranya_DropDownList" runat="server" CssClass="formLoaderComboBox" Width="155px" Visible="True"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor_kicsi">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label2" runat="server" Text="Rendezés:"></asp:Label></td>
                            <td class="mrUrlapMezo" nowrap="nowrap">
                                <asp:RadioButton ID="RendezesIktatoszamra" Text="Iktatószámra" Checked="True" GroupName="rendezes" runat="server" />
                                <br />
                                <asp:RadioButton ID="RendezesUgyintezore" Text="Ügyintézőre" GroupName="rendezes" runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <br />
                <uc2:FormFooter ID="FormFooter1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
