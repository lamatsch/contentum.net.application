﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Net;

public partial class IratokByIktatokonyvAndDatePrintForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1_ButtonsClick);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.ModeText = Resources.Form.FormHeaderPrint;
        FormFooter1.ImageButton_Save.Visible = true;
        FormFooter1.ImageButton_Close.Visible = false;
        FormFooter1.ImageButton_Back.Visible = true;
        FormFooter1.ImageButton_Save.OnClientClick = "";

        IraIktatoKonyvekDropDownList1.FillAndSetEmptyValue(true, false, Constants.IktatoErkezteto.Iktato, EErrorPanel1);

        PostazasIranya_DropDownList.Items.Clear();

        PostazasIranya_DropDownList.Items.Add("Mindkettő");
        PostazasIranya_DropDownList.Items.Add("Belső (Kimenő)");
        PostazasIranya_DropDownList.Items.Add("Bejövő");
    }

    protected void FormFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            EREC_IraIratokService service = eRecordService.ServiceFactory.GetEREC_IraIratokService();
            EREC_IraIratokSearch erec_IraIratokSearch = new EREC_IraIratokSearch();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            string where = "";
            string iktatokonyv = "";
            string date = "";

            if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                where = " EREC_UgyUgyiratdarabok.IraIktatokonyv_Id='" + IraIktatoKonyvekDropDownList1.SelectedValue + "' ";
                iktatokonyv = IraIktatoKonyvekDropDownList1.DropDownList.SelectedItem.Text;


            }

            if (!string.IsNullOrEmpty(DatumCalendarControl1.Text))
            {
                string _date = DatumCalendarControl1.Text.Replace(" ", "").Remove(10).Replace(".", "-");
                date = DatumCalendarControl1.Text;
                if (!string.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
                {
                    where = where + " and EREC_IraIratok.Letrehozasido BETWEEN '" + _date + " 00:00:00' and ' " + _date + " 23:59:59' ";
                }
                else
                {
                    where = " EREC_IraIratok.Letrehozasido BETWEEN '" + _date + " 00:00:00' and ' " + _date + " 23:59:59' ";
                }
            }

            switch (PostazasIranya_DropDownList.Text)
            {
                case "Bejövő":
                    if (string.IsNullOrEmpty(where))
                    {
                        where = " EREC_IraIratok.PostazasIranya = 1";
                    }
                    else
                    {
                        where = where + " and EREC_IraIratok.PostazasIranya = 1";
                    }
                    break;
                case "Belső (Kimenő)":
                    if (string.IsNullOrEmpty(where))
                    {
                        where = " EREC_IraIratok.PostazasIranya = 0";
                    }
                    else
                    {
                        where = where + " and EREC_IraIratok.PostazasIranya = 0";
                    }
                    break;
            }

            if (string.IsNullOrEmpty(where))
            {
                where = " EREC_IraIratok.Allapot != 06";
            }
            else
            {
                where = where + " and EREC_IraIratok.Allapot != 06";
            }

            erec_IraIratokSearch.WhereByManual = where;

            if (RendezesIktatoszamra.Checked)
            {
                erec_IraIratokSearch.OrderBy = "ORDER BY EREC_UgyUgyiratdarabok.Foszam ASC, EREC_IraIratok.Alszam ASC, EREC_IraIktatokonyvek.MegkulJelzes ASC ";
            }
            else
            {
                erec_IraIratokSearch.OrderBy = "ORDER BY ISNULL(dbo.fn_GetCsoportNev(EREC_IraIratok.FelhasznaloCsoport_Id_Ugyintez),dbo.fn_GetCsoportNev(EREC_UgyUgyiratok.FelhasznaloCsoport_Id_Ugyintez)) ASC, EREC_IraIktatokonyvek.Ev DESC, EREC_IraIktatokonyvek.Iktatohely ASC, EREC_UgyUgyiratdarabok.Foszam ASC, EREC_IraIratok.Alszam ASC, EREC_IraIktatokonyvek.MegkulJelzes ASC ";
            }

            //LZS - BUG_9177 / BUG_4747 - SSRS riportos megoldás

            //string orderby = erec_UgyUgyiratokSearch.OrderBy;
            string executor = execParam.Felhasznalo_Id;
            string szervezet = execParam.FelhasznaloSzervezet_Id;

            //Where törlése a Session-ből
            if (Session["Where"] != null)
            {
                Session["Where"] = null;
            }

            //OrderBy törlése a Session-ből
            if (Session["OrderBy"] != null)
            {
                Session["OrderBy"] = null;
            }

            Session["Where"] = where;
            Session["OrderBy"] = erec_IraIratokSearch.OrderBy;

            string queryString = "ExecutorUserId=" + executor + '&' + "FelhasznaloSzervezet_Id=" + szervezet + '&' + "Date=" + DatumCalendarControl1.Text + '&' + "Iktatokonyv=" + IraIktatoKonyvekDropDownList1.SelectedValue;

            //Meghívjuk az IratokByIktatokonyvAndDatePrintFormSSRS.aspx nyomtatóoldalt, .
            string js = "javascript:window.open('IratokByIktatokonyvAndDatePrintFormSSRS.aspx?" + queryString.Trim() + "')";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "IratokByIktatokonyvAndDatePrintFormSSRS", js, true);

        }
        if (e.CommandName == CommandName.Back)
        {
            if (Page.Session[Constants.isLogged] == null || Page.Session[Constants.isLogged].ToString() != "true")
            {
                Response.Redirect("Login.aspx", true);
            }
            if (Page.PreviousPage != null)
            {
                Page.Response.Redirect(Page.PreviousPage.Request.Url.ToString(), true);
            }
            else
            {
                Page.Response.Redirect("Default.aspx", true);
            }
        }
    }
}
