﻿using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;

public partial class IratokByIktatokonyvAndDatePrintFormSSRS : Contentum.eUtility.UI.PageBase
{
    //Query string paraméter adatok eltárolása
    private string Date = String.Empty;
    private string Where = String.Empty;
    private string OrderBy = String.Empty;
    private string IktatoKonyv = String.Empty;

    #region not used
    private string riport_path = String.Empty;
    private string executor = String.Empty;
    private string szervezet = String.Empty;
    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        //executor kiszedése a query stringből
        if (Request.QueryString["ExecutorUserId"] != null)
        {
            executor = Request.QueryString["ExecutorUserId"].ToString();
        }

        //szervezet kiszedése a query stringből
        if (Request.QueryString["FelhasznaloSzervezet_Id"] != null)
        {
            szervezet = Request.QueryString["FelhasznaloSzervezet_Id"].ToString();
        }

        //Date kiszedése a query stringből
        if (Request.QueryString["Date"] != null)
        {
            Date = Request.QueryString["Date"].ToString();
        }

        //IktatoKonyv kiszedése a query stringből
        if (Request.QueryString["IktatoKonyv"] != null)
        {
            IktatoKonyv = Request.QueryString["IktatoKonyv"].ToString();
        }

        //Where kiszedése a Session-ből
        if (Session["Where"] != null)
        {
            Where = Session["Where"].ToString();
        }

        //OrderBy kiszedése a Session-ből
        if (Session["OrderBy"] != null)
        {
            OrderBy = Session["OrderBy"].ToString();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Report server paraméterek
            ReportViewer1.Reset();
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;
            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);

            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;

            //Riport elérési út
            riport_path = ReportViewer1.ServerReport.ReportPath;
            riport_path += "/Lekerdezes/IratokNapiLista";

            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + riport_path;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            //Riport paraméterek beállítása
            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ShowParameterPrompts = false;
            ReportViewer1.ServerReport.Refresh();
        }
    }


    //Riport paraméterek beállítása
    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                ReportParameters[0] = new ReportParameter(rpis[0].Name);
                ReportParameters[0].Values.Add(executor);

                ReportParameters[1] = new ReportParameter(rpis[1].Name);
                ReportParameters[1].Values.Add(IktatoKonyv);

                ReportParameters[2] = new ReportParameter(rpis[2].Name);
                ReportParameters[2].Values.Add(szervezet);

                ReportParameters[3] = new ReportParameter(rpis[3].Name);
                ReportParameters[3].Values.Add(Where);

                ReportParameters[4] = new ReportParameter(rpis[4].Name);
                ReportParameters[4].Values.Add(Date );

                ReportParameters[5] = new ReportParameter(rpis[5].Name);
                ReportParameters[5].Values.Add(OrderBy);
            }

        }

        return ReportParameters;
    }
}

