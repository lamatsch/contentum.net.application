﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IrattarList.aspx.cs" Inherits="IrattarList" Title="Untitled Page" %>

<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />


    <%--HiddenFields--%>

    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="UgyUgyiratokCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="UgyUgyiratokUpdatePanel" runat="server" OnLoad="UgyUgyiratokUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="UgyUgyiratokCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="UgyUgyiratokCPEButton" CollapseControlID="UgyUgyiratokCPEButton"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif"
                            ExpandedImage="images/hu/Grid/minus.gif" ImageControlID="UgyUgyiratokCPEButton"
                            ExpandedSize="0" ExpandedText="Küldemények listája" CollapsedText="Küldemények listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                        <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="UgyUgyiratokGridView_RowCommand" OnPreRender="UgyUgyiratokGridView_PreRender"
                                            OnSorting="UgyUgyiratokGridView_Sorting" OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />

                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <div class="DisableWrap">
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" HeaderText="Sz" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Image ID="SzereltImage" AlternateText="Szerelés" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/szerelt.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" HeaderText="Csat." Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Image ID="CsatoltImage" AlternateText="Csatolás" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" Style="cursor: pointer"
                                                            onmouseover="Utility.UI.SetElementOpacity(this,0.7);" onmouseout="Utility.UI.SetElementOpacity(this,1);" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" HeaderText="Csny." Visible="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolmány" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" HeaderText="F." Visible="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="FeladatImage" AlternateText="Kezelési feljegyzés" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IktatoHely" HeaderText="Iktatókönyv" SortExpression="EREC_IraIktatokonyvek.Iktatohely"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Foszam" HeaderText="Fõszám" SortExpression="EREC_UgyUgyiratok.Foszam"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ev" HeaderText="Év" SortExpression="EREC_IraIktatokonyvek.Ev"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám"
                                                    SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezelõ" SortExpression="Csoportok_FelelosNev.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="Ügyindító" SortExpression="EREC_UgyUgyiratok.NevSTR_Ugyindito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyindito_Cim" HeaderText="Ügyindító címe" Visible="false" SortExpression="EREC_UgyUgyiratok.CimStr_Ugyindito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BARCODE" HeaderText="Vonalkód" Visible="false" SortExpression="EREC_UgyUgyiratok.BARCODE">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--LZS - BUG_7099 - obsolate
                                            Ügyiratok megjegyzés (EREC_UgyUgyiratok.Note) mező gridre helyezése--%>
                                                <%-- <asp:BoundField DataField="Note" HeaderText="<%$Forditas:BoundField_Megjegyzes|Megjegyzés%>" SortExpression="EREC_UgyUgyiratok.Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                                <%----LZS - BUG_11081--%>
                                                <asp:TemplateField HeaderText="<%$Forditas:BoundField_Megjegyzes|Megjegyzés%>" SortExpression="">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelNote" runat="server"
                                                            Text='<%# NoteJSONParser( Eval("Note") as string ) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Merge_IrattariTetelszam" HeaderText="Itsz." SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyintezo_Nev" HeaderText="Ügyintézõ" SortExpression="Csoportok_UgyintezoNev.Nev" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <%-- skontróban esetén (07) skontró vége megjelenítése --%>
                                                        <asp:Label ID="labelAllapotNev" runat="server" Text='<%#                                        
                string.Concat(                                          
                  Eval("Allapot_Nev")
                 , ((Eval("Allapot") as string) == "07"
                    || (Eval("Allapot") as string) == "57") ? "<br />(" + Eval("SkontroVege_Rovid") + ")" : "") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="UgyintezesModja_Nev" HeaderText="Küldemény típusa"
                                                    SortExpression="EREC_UgyUgyiratok.UgyintezesModja" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;dátum" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Hatarido" HeaderText="Határidõ" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lezárás" SortExpression="EREC_UgyUgyiratok.Lezarasdat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Elintezesdat" HeaderText="Ügyirat elintézési idõpontja" SortExpression="EREC_UgyUgyiratok.Elintezesdat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%--                        <asp:BoundField DataField="IrattarbaVetelDat" HeaderText="Irattárba&nbsp;vétel" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaVetelDat">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>--%>
                                                <%-- CR#2197: választható, alapból rejtett dátummezõk START --%>
                                                <%-- jelenleg tárolt eljárásban formázva a dátum adatok, egyébként pl. 
                            DataFormatString="{0:yyyy.MM.dd}" HtmlEncodeFormatString="true" kellene --%>
                                                <asp:BoundField DataField="SztornirozasDat" HeaderText="Sztornó dátum" Visible="false" SortExpression="EREC_UgyUgyiratok.SztornirozasDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SkontrobaDat" HeaderText="Skontró kezdete" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontrobaDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SkontroVege_Rovid" HeaderText="Skontró határideje" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontroVege">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IrattarbaKuldDatuma" HeaderText="Irattárba küldés" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaKuldDatuma">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IrattarbaVetelDat" HeaderText="Irattárba vétel" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaVetelDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KolcsonzesDatuma" HeaderText="Kölcsönzés idõpontja" Visible="false" SortExpression="EREC_IrattariKikero.KikerKezd">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KolcsonzesiHatarido" HeaderText="Kölcsönzés határideje" Visible="false" SortExpression="EREC_IrattariKikero.KikerVege">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MegorzesiIdoVege" HeaderText="Õrzési idõ vége" Visible="false" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FelulvizsgalatDat" HeaderText="Felülvizsgálat" Visible="false" SortExpression="EREC_UgyUgyiratok.FelulvizsgalatDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SelejtezesDat" HeaderText="Selejtezve / Levéltárba" Visible="false" SortExpression="EREC_UgyUgyiratok.SelejtezesDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%-- CR#2197: választható, alapból rejtett dátummezõk END --%>
                                                <%--TODO: ide majd egy kis ikon kell, ami fölé húzva megjelenik az õrzõ neve--%>
                                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort"
                                                            CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelJelleg" runat="server" Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                            ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Alszám/Db" SortExpression="EREC_UgyUgyiratok.UtolsoAlszam" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelUtolsoAlszam" runat="server" Text='<%#String.Format("{0} / {1}",Eval("UtolsoAlszam"),Eval("IratSzam"))%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- CR3205 - Az irattárban levõ ügyiratoknál az irattári hely megjelenítése --%>
                                                <asp:BoundField DataField="IrattariHely" HeaderText="Irattári hely" SortExpression="EREC_UgyUgyiratok.IrattariHely">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%-- CR3205 - CR3205 - Az irattárban levõ ügyiratoknál az irattári hely megjelenítése --%>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>


    </table>
</asp:Content>



