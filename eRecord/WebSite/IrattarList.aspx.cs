using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IrattarList : Contentum.eUtility.UI.PageBase
{

    public ScriptManager ScriptManager
    {
        get { return this.ScriptManager1; }
        set { this.ScriptManager1 = value; }
    }

    UI ui = new UI();

    private String Startup = "";

    bool kikeresJovahagyassal = false;

    #region Base Page
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);

        if (Startup != Constants.Startup.FromAtmenetiIrattar && Startup != Constants.Startup.FromKozpontiIrattar && Startup != Constants.Startup.FromSkontroIrattar
            && Startup != Constants.Startup.AtmenetiIrattarSearchForm
            && Startup != Constants.Startup.AtmenetiIrattarFastSearchForm
            && Startup != Constants.Startup.KozpontiIrattarSearchForm
            && Startup != Constants.Startup.KozpontiIrattarFastSearchForm)
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        if (Startup == Constants.Startup.FromAtmenetiIrattar
            || Startup == Constants.Startup.AtmenetiIrattarSearchForm
            || Startup == Constants.Startup.AtmenetiIrattarFastSearchForm)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AtmenetiIrattarList");
        }
        if (Startup == Constants.Startup.FromKozpontiIrattar
            || Startup == Constants.Startup.KozpontiIrattarSearchForm
            || Startup == Constants.Startup.KozpontiIrattarFastSearchForm)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KozpontiIrattarList");
        }
        if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "SkontroIrattarList");
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["IrattarListStartup"] == null)
        {
            if (Startup == Constants.Startup.AtmenetiIrattarSearchForm
                || Startup == Constants.Startup.AtmenetiIrattarFastSearchForm)
            {
                Session["IrattarListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath + "?" + QueryStringVars.Startup + "=" + Constants.Startup.FromAtmenetiIrattar);
            }

            if (Startup == Constants.Startup.KozpontiIrattarSearchForm
                || Startup == Constants.Startup.KozpontiIrattarFastSearchForm)
            {
                Session["IrattarListStartup"] = Startup;
                Response.Clear();
                Response.Redirect(Page.Request.AppRelativeCurrentExecutionFilePath + "?" + QueryStringVars.Startup + "=" + Constants.Startup.FromKozpontiIrattar);
            }
        }

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, UgyUgyiratokGridView);

        // Iratt�rba v�tel d�tuma csak k�zponti iratt�rn�l l�tsz�djon alapb�l
        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            int columnIndexIrattarbaVetel = UI.GetGridViewColumnIndex(UgyUgyiratokGridView, "IrattarbaVetelDat");
            UgyUgyiratokGridView.Columns[columnIndexIrattarbaVetel].Visible = true;
        }
        #region CR3205 - Az iratt�rban lev� �gyiratokn�l az iratt�ri hely megjelen�t�se
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            int columnIndexIrattariHely = UI.GetGridViewColumnIndex(UgyUgyiratokGridView, "IrattariHely");
            UgyUgyiratokGridView.Columns[columnIndexIrattariHely].Visible = false;
        }
        #endregion
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        kikeresJovahagyassal = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRATTAROZAS_ATMENETI_AZONOS_KEZELES, false);

        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.HeaderLabel = Resources.List.AtmenetiIrattarListHeaderTitle;
        }

        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.HeaderLabel = Resources.List.KozpontiIrattarListHeaderTitle;
        }

        if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            ListHeader1.HeaderLabel = Resources.List.SkontroIrattarListHeaderTitle;
        }


        ListHeader1.SetRightFunctionButtonsVisible(false);

        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch;
            //BLG_8081
            ListHeader1.UgyiratpotlonyomtatasVisible = true;

            ListHeader1.UgyiratpotlonyomtatasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + UgyUgyiratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                + " else { javascript:window.open('UgyiratPotloLapPrintFormSSRS.aspx?UgyiratId=" + "'+getIdsBySelectedCheckboxes('" + UgyUgyiratokGridView.ClientID + "','check')+'" + "')}";
                
            
        }
        else if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch;
            //BLG_8081
            ListHeader1.UgyiratpotlonyomtatasVisible = true;
            ListHeader1.UgyiratpotlonyomtatasOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + UgyUgyiratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
                + " else { javascript:window.open('UgyiratPotloLapPrintFormSSRS.aspx?UgyiratId=" + "'+getIdsBySelectedCheckboxes('" + UgyUgyiratokGridView.ClientID + "','check')+'" + "')}";
        }
        else if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch;
        }
        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokSearch);



        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        // baloldali gombok l�that�s�ga:
        ListHeader1.NumericSearchVisible = true;
        ListHeader1.NewVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;
        ListHeader1.UgyiratTerkepVisible = true;

        //bernat.laszlo added : Excel Export (Grid)
        ListHeader1.ExportVisible = true;
        ScriptManager.RegisterPostBackControl(ListHeader1.ExportButton);
        //bernat.laszlo eddig

        // jobboldali gombok l�that�s�ga:
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokSearch.aspx", Constants.Startup.StartupName + "=" + Startup
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Startup,
            "", Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IrattarListajaPrintForm.aspx?" + Constants.Startup.StartupName + "=" + Startup);

        string column_v = "";
        for (int i = 0; i < UgyUgyiratokGridView.Columns.Count; i++)
        {
            if (UgyUgyiratokGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarSSRSAtmenetiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarSSRSKozpontiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarSSRSSkontroIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(UgyUgyiratokGridView.ClientID);


        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.KolcsonzesVisible = true;
            ListHeader1.KolcsonzesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

            ListHeader1.KolcsonzesVisszaVisible = true;
            ListHeader1.KolcsonzesVisszaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.KolcsonzesKiadasVisible = true;
            ListHeader1.KolcsonzesKiadasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.KolcsonzesKiadas.ToolTip = Resources.Buttons.Kiadas;

            //ListHeader1.JegyzekrehelyezesVisible = true;
            //ListHeader1.JegyzekrehelyezesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            #region CR3206 - Iratt�ri strukt�ra
            ListHeader1.ImageIrattarbarendezesVisible = true;
            ListHeader1.ImageIrattarbarendezesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                    + UgyUgyiratokGridView.ClientID + "','check'); "
                    + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            #endregion
        }
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.IrattarozasraVisible = true;
            ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
            ListHeader1.AtadasraKijelolVisible = false;
            ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + UgyUgyiratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.KolcsonzesVisible = true;
            ListHeader1.KolcsonzesOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
            ListHeader1.Kolcsonzes.ToolTip = Resources.Buttons.Kikeres;

            ListHeader1.KolcsonzesKiadasVisible = true;
            ListHeader1.KolcsonzesKiadasOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
            ListHeader1.KolcsonzesKiadas.ToolTip = Resources.Buttons.Kiadas;

            //ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.KolcsonzesVisszaVisible = true;
            ListHeader1.KolcsonzesVisszaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.KolcsonzesVissza.ToolTip = Resources.Buttons.KikeresVissza;
            #region CR3206 - Iratt�ri strukt�ra
            ListHeader1.ImageIrattarbarendezesVisible = true;
            ListHeader1.ImageIrattarbarendezesOnClientClick = "var count = getSelectedCheckBoxesCount('"
                    + UgyUgyiratokGridView.ClientID + "','check'); "
                    + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            if (kikeresJovahagyassal)
            {
                ListHeader1.KolcsonzesJovahagyasVisible = true;
                ListHeader1.KolcsonzesJovahagyasOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
                ListHeader1.KolcsonzesJovahagyas.ToolTip = Resources.Buttons.KikeresJovahagyas;
            }

            #endregion
        }

        if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            ExecParam execParam = UI.SetExecParamDefault(Page);
            if (execParam.Felhasznalo_Id == KodTarak.SPEC_SZERVEK.GetSkontroIrattaros(execParam).Obj_Id)
            {
                ListHeader1.AtvetelVisible = true;

                ListHeader1.AtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
                                                + UgyUgyiratokGridView.ClientID + "','check'); "
                                                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            }

            ListHeader1.IrattarozasraVisible = false;
            ListHeader1.AtadasraKijelolVisible = false;
            ListHeader1.AtadasraKijelolOnClientClick = "var count = getSelectedCheckBoxesCount('"
                + UgyUgyiratokGridView.ClientID + "','check'); "
                + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}";

            ListHeader1.KolcsonzesVisible = true;
            ListHeader1.KolcsonzesOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID); ;
            ListHeader1.Kolcsonzes.ToolTip = Resources.Buttons.Kikeres;

            ListHeader1.KolcsonzesKiadasVisible = true;
            ListHeader1.KolcsonzesKiadasOnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
            ListHeader1.KolcsonzesKiadas.ToolTip = Resources.Buttons.Kiadas;

            ListHeader1.KolcsonzesVisszaVisible = true;
            ListHeader1.KolcsonzesVisszaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
            ListHeader1.KolcsonzesVissza.ToolTip = Resources.Buttons.KikeresVissza;
        }


        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(UgyUgyiratokGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(UgyUgyiratokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(UgyUgyiratokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = UgyUgyiratokGridView;

        /* Breaked Records */
        string CustomSessionName = null;
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            CustomSessionName = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarSearch;
        }
        else if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            CustomSessionName = Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch;
        }
        else if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            CustomSessionName = Constants.CustomSearchObjectSessionNames.SkontroIrattarSearch;
        }

        if (!String.IsNullOrEmpty(CustomSessionName))
        {
            /* Breaked Records */
            Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratokSearch(), CustomSessionName);
        }
        else
        {
            /* Breaked Records */
            Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratokSearch());
        }

        // Ha Menubol-bol nyitjak meg, akkor new popup-ot kell csinalni!
        bool popupNyitas = false;
        if (Session["IrattarListStartup"] != null)
        {
            if (Session["IrattarListStartup"].ToString() == Constants.Startup.AtmenetiIrattarSearchForm
                || Session["IrattarListStartup"].ToString() == Constants.Startup.AtmenetiIrattarFastSearchForm
                || Session["IrattarListStartup"].ToString() == Constants.Startup.KozpontiIrattarSearchForm
                || Session["IrattarListStartup"].ToString() == Constants.Startup.KozpontiIrattarFastSearchForm)
            {
                string script = "OpenNewWindow(); function OpenNewWindow() { ";
                if (Session["IrattarListStartup"].ToString() == Constants.Startup.AtmenetiIrattarSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyUgyiratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattar
               , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
               , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["IrattarListStartup"].ToString() == Constants.Startup.AtmenetiIrattarFastSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyiratokNumericSearch.aspx"
                        , Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattar, Defaults.PopupWidth, Defaults.PopupHeight
                        , UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["IrattarListStartup"].ToString() == Constants.Startup.KozpontiIrattarSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyUgyiratokSearch.aspx", Constants.Startup.StartupName + "=" + Constants.Startup.FromKozpontiIrattar
               , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList
               , CustomUpdateProgress1.UpdateProgress.ClientID);
                }
                else if (Session["IrattarListStartup"].ToString() == Constants.Startup.KozpontiIrattarFastSearchForm)
                {
                    script += JavaScripts.SetOnClientClick_DisplayUpdateProgress("UgyiratokNumericSearch.aspx"
                         , Constants.Startup.StartupName + "=" + Constants.Startup.FromKozpontiIrattar, Defaults.PopupWidth, Defaults.PopupHeight
                         , UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, CustomUpdateProgress1.UpdateProgress.ClientID);
                }

                script += "}";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "IrattarSearch", script, true);
                Session.Remove("IrattarListStartup");

                popupNyitas = true;
            }
        }

        if (popupNyitas)
        {
            ListHeader1.NincsFeltoltes = true;
        }
        else if (!IsPostBack)
        {
            UgyUgyiratokGridViewBind();
        }

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = false;
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = false;
        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + CommandName.View);
            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + CommandName.Modify);
            ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + CommandName.View);

            #region CR3206 - Iratt�ri strukt�ra
            ListHeader1.ImageIrattarbarendezesEnabled = FunctionRights.GetFunkcioJog(Page, "IrattarRendezes" + CommandName.Modify);
            #endregion

            ListHeader1.KolcsonzesEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarKolcsonzes");
            ListHeader1.KolcsonzesKiadasEnabled = FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa");

            ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarKolcsonzes")
                || FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"); // CR#2036: IrattarKolcsonzesKiadasa joggal vissza lehet vonni m�s n�vre t�rt�nt kik�r�st is!
            ListHeader1.IrattarozasraEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba");

            //bernat.laszlo added
            ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + CommandName.ExcelExport);
            //bernat.laszlo eddig

            ListHeader1.KolcsonzesJovahagyasEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarKikeresJovahagyas");
        }

        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + CommandName.View);
            ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + CommandName.Modify);
            ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + CommandName.View);

            Ugyiratok.SetKozpontiIrattarKolcsonzesFunkciogomb(null, null, ListHeader1.Kolcsonzes, null);
            ListHeader1.KolcsonzesEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarKolcsonzes");
            ListHeader1.KolcsonzesKiadasEnabled = FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa");

            #region CR3206 - Iratt�ri strukt�ra
            ListHeader1.ImageIrattarbarendezesEnabled = FunctionRights.GetFunkcioJog(Page, "IrattarRendezes" + CommandName.Modify);
            #endregion

            ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarKolcsonzes")
                || FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"); // CR#2036: IrattarKolcsonzesKiadasa joggal vissza lehet vonni m�s n�vre t�rt�nt kik�r�st is!
            //ListHeader1.JegyzekrehelyezesEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarJegyzekrehelyezes");

            //bernat.laszlo added
            ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + CommandName.ExcelExport);
            //bernat.laszlo eddig
        }

        if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            ListHeader1.KolcsonzesEnabled = ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "KikeresSkontroIrattarbol");
            ListHeader1.KolcsonzesKiadasEnabled = FunctionRights.GetFunkcioJog(Page, "KiadasSkontroIrattarbol");
        }


        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }

        string column_v = "";
        for (int i = 0; i < UgyUgyiratokGridView.Columns.Count; i++)
        {
            if (UgyUgyiratokGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarSSRSAtmenetiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarSSRSKozpontiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
        else if (Startup == Constants.Startup.FromSkontroIrattar)
        {
            ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarSSRSSkontroIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
        }
    }


    #endregion

    #region Master List

    protected void UgyUgyiratokGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("UgyUgyiratokGridView", ViewState, "EREC_UgyUgyiratok.LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("UgyUgyiratokGridView", ViewState, SortDirection.Descending);

        UgyUgyiratokGridViewBind(sortExpression, sortDirection);
    }

    protected void UgyUgyiratokGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponent.ClearDokumentElements();
        var execParam = UI.SetExecParamDefault(Page, new ExecParam());

        if (Startup == Constants.Startup.FromKozpontiIrattar) // kozponti iratt�r check
        {
            string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id.ToLower();

            if (String.IsNullOrEmpty(kozpontiIrattarId))
            {
                Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, errorResult);
                return;
            }
        }

        UI.SetPaging(execParam, ListHeader1);
        var orderBy = Search.GetOrderBy("UgyUgyiratokGridView", ViewState, SortExpression, SortDirection);

        EREC_UgyUgyiratokSearch search;
        var res = UtilityIrattar.GetIrattarList(Page, execParam, Startup, orderBy, out search);

        UI.GridViewFill(UgyUgyiratokGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

        ui.SetClientScriptToGridViewSelectDeSelectButton(UgyUgyiratokGridView);
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetSzerelesInfo(e);
        GridView_RowDataBound_SetCsatolasInfo(e);
        GridView_RowDataBound_SetVegyesInfo(e);
        UI.SetFeladatInfo(e, Constants.TableNames.EREC_UgyUgyiratok);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        //t�rolt elj�r�s viszg�lja
                        //#region BLG_577
                        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
                        //{   /*NINCS JOGA*/
                        //    CsatolmanyImage.Visible = false;
                        //    return;
                        //}
                        //#endregion

                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                        else
                        {
                            // set "link"
                            string id = "";

                            if (drw["Id"] != null)
                            {
                                id = drw["Id"].ToString();
                            }

                            string Azonosito = ""; // iktat�sz�m, a ReadableWhere fogja haszn�lni a dokumentumok list�j�n
                            if (drw.Row.Table.Columns.Contains("Foszam_Merge") && drw["Foszam_Merge"] != null)
                            {
                                Azonosito = drw["Foszam_Merge"].ToString();
                            }

                            if (!String.IsNullOrEmpty(id))
                            {
                                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolsaCount = String.Empty;

            if (drw["CsatolasCount"] != null)
            {
                csatolsaCount = drw["CsatolasCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolsaCount))
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

                if (CsatoltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolsaCount, out count);
                    if (count > 0)
                    {
                        CsatoltImage.Visible = true;
                        CsatoltImage.ToolTip = String.Format("Csatol�s: {0} darab", csatolsaCount);
                        if (drw["Id"] != null)
                        {
                            string ugyiratId = drw["Id"].ToString();
                            string onclick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + ugyiratId +
                            "&" + QueryStringVars.SelectedTab + "=" + "Csatolas"
                             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);
                            CsatoltImage.Attributes.Add("onclick", onclick);

                        }
                    }
                    else
                    {
                        CsatoltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");
                if (CsatoltImage != null)
                {
                    CsatoltImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetSzerelesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string szereltCount = String.Empty;

            if (drw["SzereltCount"] != null)
            {
                szereltCount = drw["SzereltCount"].ToString();
            }

            string elokeszitettSzerelesCount_str = String.Empty;
            if (drw["ElokeszitettSzerelesCount"] != null)
            {
                elokeszitettSzerelesCount_str = drw["ElokeszitettSzerelesCount"].ToString();
            }

            /// ha van szerel�sre el�k�sz�tett �gyirat (UgyUgyiratId_Szulo ki van t�ltve erre, de nem szerelt m�g az �llapota),
            /// akkor a piros szerelt ikont rakjuk ki, am�gy a feket�t
            /// 
            int elokeszitettSzerelesCount = 0;
            if (!String.IsNullOrEmpty(elokeszitettSzerelesCount_str))
            {
                Int32.TryParse(elokeszitettSzerelesCount_str, out elokeszitettSzerelesCount);
            }

            if (elokeszitettSzerelesCount > 0)
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    SzereltImage.Visible = true;
                    SzereltImage.ImageUrl = "~/images/hu/ikon/szerelt_piros.gif";
                    SzereltImage.ToolTip = "El�k�sz�tett szerel�s";
                    SzereltImage.AlternateText = SzereltImage.ToolTip;
                }
            }
            else if (!String.IsNullOrEmpty(szereltCount))
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(szereltCount, out count);
                    if (count > 0)
                    {
                        SzereltImage.Visible = true;
                    }
                    else
                    {
                        SzereltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");
                if (SzereltImage != null)
                {
                    SzereltImage.Visible = false;
                }

            }
        }
    }

    protected void UgyUgyiratokGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, UgyUgyiratokCPE);
        ListHeader1.RefreshPagerLabel();
    }

    protected void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        UgyUgyiratokGridViewBind();
        //ActiveTabClear();
    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, UgyUgyiratokCPE);
        UgyUgyiratokGridViewBind();
    }

    protected void UgyUgyiratokGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(UgyUgyiratokGridView, selectedRowNumber, "check");

            //string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();

            //ActiveTabRefresh(TabContainer1, id);

        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                   + "&" + Constants.Startup.StartupName + "=" + Startup
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);

            //ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
            //       , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
            //       + "&" + Constants.Startup.StartupName + "=" + Startup
            //       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);

            ErrorDetails errorDetail;
            Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotById(id, Page, EErrorPanel1);
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            // M�dos�t�s
            if (!Ugyiratok.IrattarbolModosithato(statusz, execParam, out errorDetail))
            {
                ListHeader1.ModifyOnClientClick = "if (confirm('" + Resources.Question.UIConfirmHeader_UgyiratMegtekintes
                   + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page) + "')) {" + JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                + "&" + Constants.Startup.StartupName + "=" + Startup
                , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList)
                + "} else return false;";

                // �gyiratt�rk�p View m�dban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
                       + "&" + Constants.Startup.StartupName + "=" + Startup
                       + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);

            }
            else
            {
                ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                    + "&" + Constants.Startup.StartupName + "=" + Startup
                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

                // �gyiratt�rk�p Modify m�dban
                ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                       , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                       + "&" + Constants.Startup.StartupName + "=" + Startup
                       + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
                       , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID);

            }
            //ListHeader1.AtadasraKijelolOnClientClick = JavaScripts.SetOnClientClick("AtadasForm.aspx",
            //    QueryStringVars.UgyiratId + "=" + id
            //    , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);



            if (Startup == Constants.Startup.FromAtmenetiIrattar)
            {
                //Ugyiratok.SetAtmenetiIrattarbolKikeresFunkciogomb(id, statusz, ListHeader1.Kolcsonzes, UgyUgyiratokUpdatePanel, execParam);
                Ugyiratok.SetAtmenetiIrattarKikeresVisszaFunkciogomb(id, statusz, ListHeader1.KolcsonzesVissza, UgyUgyiratokUpdatePanel);

                //if (Ugyiratok.KozpontiIrattarbaKuldheto(statusz, execParam, out errorDetail))
                //{
                //    ListHeader1.IrattarozasraOnClientClick = JavaScripts.SetOnClientClick("IrattarbaAdasForm.aspx",
                //        QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id +
                //        "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattar
                //        , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                //}
                //else
                //{
                //    ListHeader1.IrattarozasraOnClientClick = "alert('" + Resources.Error.ErrorCode_52202 
                //        + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                //        + "'); return false;";
                //}
            }

            if (Startup == Constants.Startup.FromSkontroIrattar)
            {
                //Ugyiratok.SetAtmenetiIrattarbolKikeresFunkciogomb(id, statusz, ListHeader1.Kolcsonzes, UgyUgyiratokUpdatePanel, execParam);
                Ugyiratok.SetSkontroIrattarKikeresVisszaFunkciogomb(id, statusz, ListHeader1.KolcsonzesVissza, UgyUgyiratokUpdatePanel);
            }

            if (Startup == Constants.Startup.FromKozpontiIrattar)
            {
                Ugyiratok.SetKozpontiIrattarKolcsonzesFunkciogomb(id, statusz, ListHeader1.Kolcsonzes, UgyUgyiratokUpdatePanel);

                // K�lcs�nz�s kiad�s:
                if (Ugyiratok.KiadhatoKozpontiIrattarbol(statusz, out errorDetail))
                {
                    ListHeader1.KolcsonzesKiadasOnClientClick = "if (confirm('"
                     + Resources.Question.UIConfirmHeader_KolcsonzesKiadas
                     + "')) { } else { return false; } ";
                }
                else
                {
                    ListHeader1.KolcsonzesKiadasOnClientClick = "alert('" + Resources.Error.ErrorCode_52407
                        + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                        + "'); return false; ";
                }

                Ugyiratok.SetKozpontiIrattarKolcsonzesVisszaFunkciogomb(id, statusz, ListHeader1.KolcsonzesVissza, UgyUgyiratokUpdatePanel);
                //ListHeader1.KolcsonzesVisszaOnClientClick = "if (confirm('Biztosan visszavonja a k�lcs�nz�si k�relmet?')) {} else {return false;}";
                //ListHeader1.JegyzekrehelyezesOnClientClick = "alert('TODO'); return false;";
            }

        }
    }

    protected void UgyUgyiratokUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    UgyUgyiratokGridViewBind();
                    //ActiveTabRefresh(TabContainer1, UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView));
                    break;
            }
        }

    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //if (e.CommandName == CommandName.Invalidate)
        //{
        //    deleteSelectedUgyUgyiratok();
        //    UgyUgyiratokGridViewBind();
        //}
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, UgyUgyiratokGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
        //bernat.laszlo eddig
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string js = string.Empty;
        List<string> SelectedIds = null;

        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraUgyUgyiratRecords();
                UgyUgyiratokGridViewBind();
                break;
            case CommandName.Unlock:
                UnlockSelectedIraUgyUgyiratRecords();
                UgyUgyiratokGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedUgyUgyiratok();
                break;
            //case CommandName.SkontrobolKivesz:
            //    Ugyiratok.SkontrobolKivesz(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView), Page, EErrorPanel1, ErrorUpdatePanel);
            //    UgyUgyiratokGridViewBind();
            //    break;
            case CommandName.KolcsonzesVissza:
                if (Startup == Constants.Startup.FromAtmenetiIrattar)
                {
                    EREC_IrattariKikeroService kikeroService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                    ExecParam kikeroExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    string ugyiratId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

                    Result kikeroResult = kikeroService.KikeresSztorno(kikeroExecParam, ugyiratId);
                    if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kikeroResult);
                    else
                        UgyUgyiratokGridViewBind();
                }
                else if (Startup == Constants.Startup.FromKozpontiIrattar)
                {
                    EREC_IrattariKikeroService kikeroService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                    ExecParam kikeroExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    kikeroExecParam.Record_Id = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);
                    Result kikeroResult = kikeroService.KolcsonzesSztorno(kikeroExecParam);
                    if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kikeroResult);
                    else
                        UgyUgyiratokGridViewBind();
                }
                else if (Startup == Constants.Startup.FromSkontroIrattar)
                {
                    EREC_IrattariKikeroService kikeroService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                    ExecParam kikeroExecParam = UI.SetExecParamDefault(Page, new ExecParam());
                    string ugyiratId = UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView);

                    Result kikeroResult = kikeroService.SkontroKikeresSztorno(kikeroExecParam, ugyiratId);
                    if (!string.IsNullOrEmpty(kikeroResult.ErrorCode))
                        ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, kikeroResult);
                    else
                        UgyUgyiratokGridViewBind();
                }
                break;
            case CommandName.AtadasraKijeloles:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedUgyiratIds"] = sb.ToString();

                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedBarcodeIds"] = null;
                Session["SelectedIratPeldanyIds"] = null;
                Session["SelectedDosszieIds"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("AtadasForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratokAtadas", js, true);
                break;
            case CommandName.Kolcsonzes:
                SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                if (SelectedIds != null && SelectedIds.Count > 0)
                {
                    Session[Constants.SessionNames.SelectedIds] = SelectedIds;

                    if (Startup == Constants.Startup.FromAtmenetiIrattar)
                    {
                        js = JavaScripts.SetOnClientClickWithTimeout("IrattariKikeroForm.aspx", QueryStringVars.Command + "=" + CommandName.KikeresAtmenetiIrattarbol, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    }
                    else if (Startup == Constants.Startup.FromSkontroIrattar)
                    {
                        js = JavaScripts.SetOnClientClickWithTimeout("IrattariKikeroForm.aspx", QueryStringVars.Command + "=" + CommandName.KikeresSkontroIrattarbol, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    }

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Kikeres", js, true);
                }
                else
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                    ErrorUpdatePanel.Update();
                }
                break;
            case CommandName.KolcsonzesJovahagyas:
                SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                if (SelectedIds != null && SelectedIds.Count > 0)
                {
                    Session[Constants.SessionNames.SelectedIds] = SelectedIds;

                    if (Startup == Constants.Startup.FromAtmenetiIrattar)
                    {
                        js = JavaScripts.SetOnClientClickWithTimeout("IrattariKikeroForm.aspx", QueryStringVars.Command + "=" + CommandName.KikeresAtmenetiIrattarbolJovahagyas, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    }

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "KikeresJovahagyas", js, true);
                }
                else
                {
                    ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                    ErrorUpdatePanel.Update();
                }
                break;
            case CommandName.KolcsonzesKiadas:
                if (Startup == Constants.Startup.FromKozpontiIrattar)
                {
                    // K�lcs�nz�s kiad�s k�zponti iratt�rb�l:
                    Ugyiratok.KolcsonzesKiadasKozpontiIrattarbol(UI.GetGridViewSelectedRecordId(UgyUgyiratokGridView), Page, EErrorPanel1, ErrorUpdatePanel);
                    UgyUgyiratokGridViewBind();
                    return;
                }
                else if (Startup == Constants.Startup.FromAtmenetiIrattar)
                {
                    // �tmeneti iratt�rb�l kiad�s:

                    SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (SelectedIds != null && SelectedIds.Count > 0)
                    {
                        Session[Constants.SessionNames.SelectedIds] = SelectedIds;

                        js = JavaScripts.SetOnClientClickWithTimeout("IrattariKikeroForm.aspx", QueryStringVars.Command + "=" + CommandName.KiadasAtmenetiIrattarbol, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Kiadas", js, true);
                    }
                    else
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                        ErrorUpdatePanel.Update();
                    }
                }
                else if (Startup == Constants.Startup.FromSkontroIrattar)
                {
                    // Skontr� iratt�rb�l kiad�s:

                    SelectedIds = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    if (SelectedIds != null && SelectedIds.Count > 0)
                    {
                        Session[Constants.SelectedUgyiratIds] = SelectedIds;

                        js = JavaScripts.SetOnClientClickWithTimeout("SkontroInditasForm.aspx", QueryStringVars.Command + "=" + CommandName.KiadasSkontroIrattarbol, Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Kiadas", js, true);
                    }
                    else
                    {
                        ResultError.DisplayErrorOnErrorPanel(EErrorPanel1, "Hiba!", Resources.List.UI_NoSelectedItem);
                        ErrorUpdatePanel.Update();
                    }
                }

                break;
            case CommandName.Atvetel:
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedUgyiratIds"] = sb.ToString();

                Session["SelectedBarcodeIds"] = null;
                Session["SelectedKuldemenyIds"] = null;
                Session["SelectedIratPeldanyIds"] = null;
                Session["SelectedDosszieIds"] = null;
                //ne okozzon gondot, ha kor�bban h�vtuk a t�k visszav�tel funkci�t
                Session["TUKVisszavetel"] = null;

                js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtadas", js, true);
                break;
            case CommandName.Irattarozasra:
                {
                    List<string> SelectedUgyiratIdsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel);
                    Session["SelectedUgyiratIds"] = String.Join(",", SelectedUgyiratIdsList.ToArray());

                    // BUG_8005
                    //if (SelectedUgyiratIdsList.Count == 1)
                    //{
                    //    string id = SelectedUgyiratIdsList[0];
                    //    ErrorDetails errorDetail;
                    //    Ugyiratok.Statusz statusz = Ugyiratok.GetAllapotById(id, Page, EErrorPanel1);
                    //    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                    //    if (Ugyiratok.KozpontiIrattarbaKuldheto(statusz, execParam, out errorDetail))
                    //    {
                    //        js = JavaScripts.SetOnClientClickWithTimeout("IrattarbaAdasForm.aspx",
                    //            QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id +
                    //            "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattar
                    //            , Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    //    }
                    //    else
                    //    {
                    //        js = "alert('" + Resources.Error.ErrorCode_52202
                    //            + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    //            + "');";
                    //    }
                    //}
                    //else
                    //{

                    //    js = JavaScripts.SetOnClientClickWithTimeout("IrattarbaAdasForm.aspx",
                    //        QueryStringVars.Command + "=" + CommandName.Irattarozasra +
                    //        "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattar,
                    //        Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID,
                    //        EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    //}

                    js = JavaScripts.SetOnClientClickWithTimeout("IrattarbaAdasTomegesForm.aspx",
                           QueryStringVars.Command + "=" + CommandName.Irattarozasra +
                           "&" + Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattar,
                           Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID,
                           EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "IrattarbaAdas", js, true);
                }
                break;
            case CommandName.Irattarbarendezes:
                #region CR3206 - Iratt�ri strukt�ra
                sb = new System.Text.StringBuilder();
                foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel))
                {
                    sb.Append(s + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                Session["SelectedUgyiratIds"] = sb.ToString();

                Session["SelectedBarcodeIds"] = null;


                js = JavaScripts.SetOnClientClickWithTimeout("IrattarRendezesForm.aspx", CommandName.Irattarbarendezes + "&" + QueryStringVars.Startup + "=" + Startup, Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokUpdatePanel.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIrattarbarendezes", js, true);
                #endregion
                break;
        }
    }

    private void LockSelectedIraUgyUgyiratRecords()
    {
        LockManager.LockSelectedGridViewRecords(UgyUgyiratokGridView, "EREC_UgyUgyiratok"
            , "UgyiratLock", "UgyiratForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraUgyUgyiratRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(UgyUgyiratokGridView, "EREC_UgyUgyiratok"
            , "UgyiratLock", "UgyiratForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// Elkuldi emailben a UgyUgyiratokGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedUgyUgyiratok()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel), UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_Irattar", Constants.Startup.StartupName + "=" + Startup);
        }
    }

    protected void UgyUgyiratokGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        UgyUgyiratokGridViewBind(e.SortExpression, UI.GetSortToGridView("UgyUgyiratokGridView", ViewState, e.SortExpression));
        //ActiveTabClear();
    }

    protected void GetJelleg(object oJelleg, out string JellegLabel, out string JellegTooltip)
    {
        string jelleg = (oJelleg ?? String.Empty) as string;

        switch (jelleg)
        {
            case KodTarak.UGYIRAT_JELLEG.Elektronikus:
                JellegLabel = "E";
                JellegTooltip = "Elektronikus";
                break;
            case KodTarak.UGYIRAT_JELLEG.Papir:
                JellegLabel = "P";
                JellegTooltip = "Pap�r";
                break;
            case KodTarak.UGYIRAT_JELLEG.Vegyes:
                JellegLabel = "V";
                JellegTooltip = "Vegyes";
                break;
            default:
                goto case KodTarak.UGYIRAT_JELLEG.Papir;
        }
    }

    protected string GetJellegLabel(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return label;
    }

    protected string GetJellegToolTip(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return tooltip;
    }


    #endregion

    public string NoteJSONParser(string noteJSONString)
    {
        string result = "";
        if (!string.IsNullOrEmpty(noteJSONString))
        {
            Note_JSON note;

            try
            {
                note = JsonConvert.DeserializeObject<Note_JSON>(noteJSONString);
            }
            catch (Exception)
            {
                note = null;
            }

            if (note != null)
                result = note.Megjegyzes;
        }

        return result;
    }

}



