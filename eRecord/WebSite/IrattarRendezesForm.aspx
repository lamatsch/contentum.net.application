﻿<%@ Page Language="C#"  MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IrattarRendezesForm.aspx.cs" Inherits="IrattarRendezesForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="eRecordComponent/VonalkodTextBox.ascx" TagName="VonalkodTextBox" TagPrefix="uc8" %>

<%@ Register Src="eRecordComponent/IrattariHelyLevelekDropDown.ascx" TagName="IrattariHelyLevelekDropDown" TagPrefix="uc10" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%-- CR3246 Irattári Helyek kezelésének módosítása--%>
<%@ Register Src="eRecordComponent/IrattarStrukturaTerkep.ascx" TagName="IrattarStrukturaTerkep" TagPrefix="imdt" %>   

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>
        
    <!--Frissítés jelzése-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
              
    
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
             <uc1:FormHeader ID="FormHeader1" runat="server" />
<%--                    <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
    
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                    <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                         <br />
                        <asp:Label ID="Label_Ugyiratok" runat="server" Text="Ügyiratok:" Visible="false"  CssClass="GridViewTitle"></asp:Label>          
                        <br />
                        <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                            OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                            AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle"/>  
                    
                            <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                        <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                                           
                                        <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                            <ItemTemplate>                                    
                                                <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                                    Visible="false" OnClientClick="return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                    <%-- CR3246 Irattári Helyek kezelésének módosítása--%>
                                     <asp:TemplateField HeaderText="Irattár" Visible="false">
                                       <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label_IrattarTipus" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Foszam_Merge" HeaderText="Iktatószám" 
                                        SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam" >
                                        <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                        <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Targy" HeaderText="Tárgy" SortExpression="Targy" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezelő" SortExpression="Felelos_Nev" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="Ügyindító" SortExpression="NevSTR_Ugyindito"  >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="Allapot_Nev" HeaderText="Állapot" SortExpression="Allapot_Nev"  >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;dátum" SortExpression="LetrehozasIdo" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="Hatarido" HeaderText="Határidő" SortExpression="Hatarido" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="Lezarasdat" HeaderText="Lezárás" SortExpression="Lezarasdat" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="Elintezesdat" HeaderText="Ügyirat elintézési időpontja" SortExpression="Elintezesdat" >
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>                        
                                    <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IrattariTetelszam" HeaderText="Irattári tételszám" SortExpression="IrattariTetelszam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IrattariHely" HeaderText="Irattári hely" SortExpression="IrattariHely">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemStyle CssClass="GridViewLovListInvisibleCoulumnStyle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbUgyintezesModja" runat="server" Enabled="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>                        
                                    <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                        <HeaderTemplate>
                                            <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>                    
                    
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijelölt tételek száma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                </td>
            </tr>
            <tr>
            <td>
            <br />
                <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">                
                <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                </asp:Panel>
                <br />
                <asp:Panel ID="Panel_Warning_NemAzonositottVonalkodok" runat="server" Visible="false" HorizontalAlign="Center">                    
                    <br />
                    <asp:Label ID="labelNotIdentifiedHeader" runat="server" Text="<%$Resources:Form,UI_NemAzonositottVonalkodok%>" 
                        Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                    <span style="position:relative;top:-10px; left:38%;">
                        <asp:Label ID="Label_Warning_NemAzonositottVonalkodok" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                    </span>
                </asp:Panel>
                
            </td></tr>
            <tr>
                <td>
                    <%-- CR3246 Irattári Helyek kezelésének módosítása--%>
                 <eUI:eFormPanel ID="Panel_IrattariStruktura" runat="server">
                <table cellpadding="0" cellspacing="0" style="width: 100%;" >
                    <tr class="urlapSor" runat="server"> 
                        <td  style="text-align: left; vertical-align: top;"  colspan="2">
                            <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                            <asp:Label ID="Label7" runat="server" Text="Irattári hely megadása:"  CssClass="GridViewTitle"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="mrUrlapCaption_short">
                              <asp:Label ID="Label1" runat="server" Text="Vonalkód:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <%-- CR3246 Irattári Helyek kezelésének módosítása--%>
                             <asp:UpdatePanel runat="server" ID="Vonalkod_UpdatePanel"  UpdateMode="Conditional" >
                                 <ContentTemplate>   
                            <%--<uc15:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDown" runat="server" />--%>
                               <uc8:VonalkodTextBox ID="IrattariHely_VonalkodTextBox" runat="server" />
                                     </ContentTemplate>
                             </asp:UpdatePanel>
                        </td>                   
                    </tr>
                    <tr class="urlapSor_kicsi" runat="server">
                        <td></td>
                        <td  class="mrUrlapMezo" style="border: solid 1px Silver;" >
                             <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Conditional" >
                       <ContentTemplate>        
                            <asp:Panel ID="Panel1" runat="server" Visible="true" >
                                <ajaxToolkit:CollapsiblePanelExtender ID="MetaAdatokCPE" runat="server" TargetControlID="Panel1"
                                    CollapsedSize="20" Collapsed="False" 
                                     AutoCollapse="false" AutoExpand="false"    
                                    ExpandedSize="150"
                                    ScrollContents="true">
                                </ajaxToolkit:CollapsiblePanelExtender>   
                                <table style="width: 97%;">
                                    <tr>
                                     <td style="width: 100%;">
                                         <table style="width: 100%;">
                                             <tr>
                                                 <td style="width: 100%; text-align: left; ">
                                                     <asp:Panel ID="Panel2" runat="server" Visible="true">
                                                         <imdt:IrattarStrukturaTerkep ID="IrattarTerkep1" runat="server"
                                                            OnSelectedNodeChanged = "IrattarTerkep_SelectedNodeChanged" AddEmptyItem="true"  
                                                         />                                        
                                                     </asp:Panel>     
                                                 </td>                                   
                                            </tr>                               
                                         </table>
                                     </td>

                                   </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                   </asp:UpdatePanel> 
                            
                        </td>
                    </tr>
                </table>
                         </eUI:eFormPanel>
                 <%--   <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="Label4" runat="server" Text="*" CssClass="ReqStar"></asp:Label>
                                <asp:Label ID="labelIrattar" runat="server" Text="Irattári hely:"></asp:Label>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption_short">
                                <asp:Label ID="label2" runat="server" Text="Vonalkód:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:VonalkodTextBox ID="IrattariHely_VonalkodTextBox" runat="server" />
                            </td>
                            <td class="mrUrlapMezo">
                                <uc10:IrattariHelyLevelekDropDown ID="IrattariHelyLevelekDropDown" OnSelectedIndexChanged="IrattarLevelekDropdownList_SelectedIndexChanged" runat="server" />
                            </td>                           
                        </tr>
                    </table>
                    </eUI:eFormPanel>--%>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />                 
                </td>
            </tr>            
        </table>
     </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upResult">
     <ContentTemplate>
      <asp:Panel ID="ResultPanel" runat="server" Visible="false" width="90%">
            <eUI:eFormPanel ID="EFormPanel2" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">A kijelölt tételek irattárba rendezése sikeresen végrehajtódott.</div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                               CommandName="Close" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>      
      </asp:Panel>
      </ContentTemplate>
     </asp:UpdatePanel>
</asp:Content>