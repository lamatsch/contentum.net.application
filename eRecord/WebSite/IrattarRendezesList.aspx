﻿<%@ Page Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IrattarRendezesList.aspx.cs" Inherits="IrattarRendezesList" EnableEventValidation="false" %>


<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc7" %>                 

<%@ Register Src="eRecordComponent/VonalKodListBox.ascx" TagName="VonalKodListBox" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="lh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <lh:ListHeader ID="ListHeader1" runat="server" />
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--Hiba megjelenites--%>
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional" OnLoad="ErrorUpdatePanel1_Load"
        RenderMode="Inline">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--/Hiba megjelenites--%>
    <asp:UpdatePanel ID="VonalkodUpdatePanel" runat="server" 
        onload="VonalkodUpdatePanel_Load" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Panel ID="MainPanel" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td valign="top">
                            <eUI:eFormPanel ID="EFormPanel1" runat="server">
                                <uc4:VonalKodListBox runat="server" ID="VonalKodListBox" MaxElementsNumber="500" />
                            </eUI:eFormPanel>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: center;">
                                        &nbsp;<br />
                                        <asp:ImageButton ID="IrattarRendezesButton" runat="server" CssClass="cursor_Hand"
                                            ImageUrl="~/images/hu/ovalgomb/rendben.jpg" onmouseover="swapByName(this.id,'rendben2.jpg')"
                                            onmouseout="swapByName(this.id,'rendben.jpg')" AlternateText="Mehet" ToolTip="Mehet"
                                            OnClick="AtadasButtonOnClick" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
