﻿using Contentum.eRecord.Utility;
using System;
using System.Web.UI;

public partial class IrattarRendezesList : System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattarRendezesList");

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.Form.IrattarRendezesHeaderTitle;
        
        ListHeader1.SetRightFunctionButtonsVisible(false);
        ListHeader1.SendObjectsVisible = false;
        ListHeader1.PagerVisible = false;
        #region Baloldali funkciógombok kiszedése
        ListHeader1.SearchVisible = false;
        ListHeader1.NewVisible = false;
        ListHeader1.ViewVisible = false;
        ListHeader1.ModifyVisible = false;
        ListHeader1.ExportVisible = false;
        ListHeader1.PrintVisible = false;
        ListHeader1.InvalidateVisible = false;
        ListHeader1.HistoryVisible = false;
        ListHeader1.DefaultFilterVisible = false;
        #endregion
        ListHeader1.CustomSearchObjectSessionName = "-----";

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        ListHeader1.RefreshImageButton.ToolTip = Resources.Form.UI_PageReload;
        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(VonalkodUpdatePanel.ClientID);


        registerJavascripts();

    }


    private void registerJavascripts()
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        //Url beállítása
        IrattarRendezesButton.Attributes.Add("onclick",
                @"var listBox = $get('" + VonalKodListBox.ListBox.ClientID + @"');
                  if (listBox && listBox.options.length > 0)
                  {
                  }
                  else 
                  { 
                     alert('" + Resources.Error.UINoBarCode + @"'); 
                      return false;
                  }
                ");


        // fókusz a vonalkód mezőre
        ScriptManager1.SetFocus(VonalKodListBox.TextBox);
    }

    protected void ErrorUpdatePanel1_Load(object sender, EventArgs e)
    {
    }

    public void AtadasButtonOnClick(object sender, EventArgs e)
    {
        Session["SelectedBarcodeIds"] = VonalKodListBox.VonalkodClientList;

        Session["SelectedKuldemenyIds"] = null;
        Session["SelectedUgyiratIds"] = null;
        Session["SelectedIratPeldanyIds"] = null;
        Session["SelectedDosszieIds"] = null;

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedVonalkodokAtadas",
            JavaScripts.SetOnClientClickWithTimeout("IrattarRendezesForm.aspx", "", Defaults.PopupWidth_Max, Defaults.PopupHeight, VonalkodUpdatePanel.ClientID, EventArgumentConst.refreshMasterList, 100).TrimEnd("return false;".ToCharArray()), true);
        
    }
    protected void VonalkodUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    VonalKodListBox.Reset();
                    VonalkodUpdatePanel.Update();
                    break;
            }
        }

    }
}