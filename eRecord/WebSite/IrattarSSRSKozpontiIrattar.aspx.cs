﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using Microsoft.Reporting.WebForms;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;
using System.Collections.Generic;

public partial class IrattarSSRSKozpontiIrattar : Contentum.eUtility.UI.PageBase
{
    private string vis = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        vis = Request.QueryString.Get(QueryStringVars.Filter);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
            ReportViewer1.ServerReport.ReportServerCredentials = rvc;

            Uri ReportServerUrl = new Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerUrl;
            ReportViewer1.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewer1.ServerReport.ReportPath;

            ReportViewer1.ShowRefreshButton = false;
            ReportViewer1.ShowParameterPrompts = false;

            ReportParameterInfoCollection rpis = ReportViewer1.ServerReport.GetParameters();

            if (rpis.Count > 0)
            {
                ReportParameter[] ReportParameters = GetReportParameters(rpis);

                ReportViewer1.ServerReport.SetParameters(ReportParameters);
            }

            ReportViewer1.ServerReport.Refresh();
        }
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;
        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

                EREC_UgyUgyiratokSearch search = null;

                search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                    , Constants.CustomSearchObjectSessionNames.KozpontiIrattarSearch);

                search.TopRow = 0;

                string defaultAllapotok = "";

                defaultAllapotok = "'" + KodTarak.UGYIRAT_ALLAPOT.Irattarban_orzott + "','"
                    + KodTarak.UGYIRAT_ALLAPOT.Irattarbol_elkert + "','" + KodTarak.UGYIRAT_ALLAPOT.EngedelyezettKikeronLevo + "'";
                
                search.Manual_Allapot_DefaultFilter.Value = defaultAllapotok;
                search.Manual_Allapot_DefaultFilter.Operator = Contentum.eQuery.Query.Operators.inner;
                search.Manual_Allapot_DefaultFilter.Group = "99";
                search.Manual_Allapot_DefaultFilter.GroupOperator = Query.Operators.or;

                //szereltek kezelése
                search.Manual_Szereles_Alatt.Value = defaultAllapotok;
                search.Manual_Szereles_Alatt.Operator = Contentum.eQuery.Query.Operators.inner;
                search.Manual_Szereles_Alatt.Group = "99";
                search.Manual_Szereles_Alatt.GroupOperator = Query.Operators.or;

                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id.ToLower();

                if (String.IsNullOrEmpty(kozpontiIrattarId))
                {
                    Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, errorResult);
                }
                // Központi irattárnál azokat hozzuk, ahol az irat helye (őrző) a Központi irattár:
                search.FelhasznaloCsoport_Id_Orzo.Value = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id.ToLower();
                search.FelhasznaloCsoport_Id_Orzo.Operator = Contentum.eQuery.Query.Operators.equals;

                ExecParam.Fake = true;

                //Result res = service.GetAllWithExtension(ExecParam, search);
                Result result = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "OrderBy":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@OrderBy")))
                            {
                                ReportParameters[i].Values.Add(" order by EREC_UgyUgyiratok.LetrehozasIdo DESC");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@OrderBy"));
                            }
                            break;
                        case "Where_KuldKuldemenyek":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_KuldKuldemenyek"));
                            break;
                        case "Where":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where"));
                            break;
                        case "TopRow":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@TopRow"));
                            break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ExecutorUserId"));
                            break;
                        case "Jogosultak":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Jogosultak"));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@FelhasznaloSzervezet_Id"));
                            break;
                        case "pageNumber":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageNumber")))
                            {
                                ReportParameters[i].Values.Add("0");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageNumber"));
                            }
                            break;
                        case "pageSize":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@pageSize")))
                            {
                                ReportParameters[i].Values.Add("10000");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@pageSize"));
                            }
                            break;
                        case "SelectedRowId":
                            if (string.IsNullOrEmpty(result.SqlCommand.GetParamValue("@SelectedRowId")))
                            {
                                //ReportParameters[i].Values.Add(" ");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@SelectedRowId"));
                            }
                            break;
                        case "ReadableWhere":
                            ReportParameters[i].Values.Add(search.ReadableWhere);
                            break;
                        case "Where_UgyUgyiratdarabok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_UgyUgyiratdarabok"));
                            break;
                        case "Where_IraIratok":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_IraIratok"));
                            break;
                        case "Where_IraIktatokonyvek":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_IraIktatokonyvek"));
                            break;
                        case "Where_Dosszie":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Where_Dosszie"));
                            break;
                        case "ObjektumTargyszavai_ObjIdFilter":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ObjektumTargyszavai_ObjIdFilter"));
                            break;
                        case "Altalanos_FTS":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@Altalanos_FTS"));
                            break;
                        case "ForMunkanaplo":
                            ReportParameters[i].Values.Add(result.SqlCommand.GetParamValue("@ForMunkanaplo"));
                            break;
                        case "SzVisibility":
                            if (vis.Substring(2, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "CsatVisibility":
                            if (vis.Substring(3, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "CsnyVisibility":
                            if (vis.Substring(4, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "FVisibility":
                            if (vis.Substring(5, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "IktatokonyvVisibility":
                            if (vis.Substring(6, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "FoszamVisibility":
                            if (vis.Substring(7, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "EvVisibility":
                            if (vis.Substring(8, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IktatoszamVisibility":
                            if (vis.Substring(9, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "TargyVisibility":
                            if (vis.Substring(10, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "KezeloVisibility":
                            if (vis.Substring(11, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "UgyfelUgyinditoVisibility":
                            if (vis.Substring(12, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "UgyinditoCimVisibility":
                            if (vis.Substring(13, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "VonalkodVisibility":
                            if (vis.Substring(14, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            } break;
                        case "LtszVisibility":
                            if (vis.Substring(15, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "UgyintezoVisibility":
                            if (vis.Substring(16, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AllapotVisibility":
                            if (vis.Substring(17, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AdathordozoTipusaVisibility":
                            if (vis.Substring(18, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IktDatumVisibility":
                            if (vis.Substring(19, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "HataridoVisibility":
                            if (vis.Substring(20, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "LezarasDatumVisibility":
                            if (vis.Substring(21, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ElintezesVisibility":
                            if (vis.Substring(22, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "StornoDatumVisibility":
                            if (vis.Substring(23, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "SkontroKezdeteVisibility":
                            if (vis.Substring(24, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "SkontroHataridejeVisibility":
                            if (vis.Substring(25, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IrattarbaKuldesVisibility":
                            if (vis.Substring(26, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IrattarbaVetelVisibility":
                            if (vis.Substring(27, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "KolcsonzesKezdeteVisibility":
                            if (vis.Substring(28, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "KolcsonzesHataridejeVisibility":
                            if (vis.Substring(29, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "MegorzesiIdoVegeVisibility":
                            if (vis.Substring(30, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "FelulvizsgalatVisibility":
                            if (vis.Substring(31, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "SelejtezveLeveltartbaVisibility":
                            if (vis.Substring(32, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "IraHelyeVisibility":
                            if (vis.Substring(33, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "JellegVisibility":
                            if (vis.Substring(34, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "AlszamDbVisibility":
                            if (vis.Substring(35, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                        case "ZarolasVisibility":
                            if (vis.Substring(36, 1).Equals("1"))
                            {
                                ReportParameters[i].Values.Add("true");
                            }
                            else
                            {
                                ReportParameters[i].Values.Add("false");
                            }
                            break;
                    }
                }
            }
        }
        return ReportParameters;
    }
}
