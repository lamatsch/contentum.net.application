<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IrattarbaAdasForm.aspx.cs" Inherits="IrattarbaAdasForm" Title="Untitled Page" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc9" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc7" %>
<%@ Register Src="eRecordComponent/IrattarDropDownList.ascx" TagName="IrattarDropDownList"
    TagPrefix="uc8" %>
    <%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
    
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>

<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>

<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>

<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>

<%@ Register Src="~/eRecordComponent/TeljessegEllenorzesResultPanel.ascx" TagName="TeljessegEllenorzesReultPanel" TagPrefix="uc" %>

<%@ Register Src="~/eRecordComponent/UgyiratMasterAdatok.ascx" TagName="UgyiratMasterAdatok" TagPrefix="uc" %>

<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup"
    TagPrefix="uc" %>

<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Iratt�rba k�ld�s" />

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

    <uc:CustomUpdateProgress ID="UpdateProgress" runat="server" />
    <uc:TeljessegEllenorzesReultPanel runat="server" ID="TeljessegEllenorzesResultPanel1" />
    <br />
    <table cellpadding="0" cellspacing="0" width="90%">
        <tr>
            <td>
                <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                        </eUI:eErrorPanel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <uc:UgyiratMasterAdatok ID="UgyiratAdatok" runat="server" />
                <asp:Panel ID="UgyiratokList" runat="server" Visible="false">
                    <uc:InfoModalPopup ID="InfoModalPopup1" runat="server" />
                    <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                        BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                        OnRowDataBound="UgyUgyiratokGridView_RowDataBound" AllowSorting="False" AutoGenerateColumns="False"
                        DataKeyNames="Id">
                        <rowstyle cssclass="GridViewRowStyle" wrap="True" />
                        <selectedrowstyle cssclass="GridViewSelectedRowStyle" />
                        <headerstyle cssclass="GridViewHeaderStyle" />
                        <columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                <ItemTemplate>
                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                        Visible="false" OnClientClick="return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Foszam_Merge" HeaderText="F�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                <%--TODO: ide majd egy kis ikon kell, ami f�l� h�zva megjelenik az �rz� neve--%>
                            </asp:BoundField>
                            <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                <HeaderTemplate>
                                    <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </columns>
                        <pagersettings visible="False" />
                    </asp:GridView>
                    <br />
                    <asp:Panel runat="server" ID="panelTetelekSzama" Style="height: 20px;">
                        <div>
                            <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                            <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <br />
                <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor" id="rowItsz" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Nev_Label" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server"></uc7:IraIrattariTetelTextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rowMegjegyzes" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="Megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput" TextMode="MultiLine"
                                    Rows="3">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rowVezetoiMegjegyzes" visible="false" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Vezet�i megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="VezetoiMegjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"
                                    TextMode="MultiLine" Rows="3">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label><asp:Label
                                    ID="Label2" runat="server" Text="Iratt�r jellege:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:IrattarDropDownList ID="IrattarDropDownList1" runat="server"></uc8:IrattarDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="Iratt�r:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc9:CsoportTextBox ID="CsoportTextBox1" runat="server" AjaxEnabled="false" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <br />
                <asp:UpdatePanel ID="upButtons" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td style="padding-right: 15px;">
                                    <asp:ImageButton ID="ImageApprove" runat="server" ImageUrl="~/images/hu/ovalgomb/jovahagyas.gif"
                                        onmouseover="swapByName(this.id,'jovahagyas2.gif')" onmouseout="swapByName(this.id,'jovahagyas.gif')"
                                        CommandName="Approve" Visible="False" OnClick="ImageApprove_Click" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageReject" runat="server" ImageUrl="~/images/hu/ovalgomb/elutasitas.gif"
                                        onmouseover="swapByName(this.id,'elutasitas2.gif')" onmouseout="swapByName(this.id,'elutasitas.gif')"
                                        CommandName="Reject" Visible="False" OnClick="ImageReject_Click" />
                                </td>
                                <td>
                                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
