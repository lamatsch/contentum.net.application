/* $Header: IrattarbaAdasForm.aspx.cs, 25, 2011.08.30 11:46:09, Lamatsch Andr?s$ 
 */
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IrattarbaAdasForm : Contentum.eUtility.UI.PageBase
{
    private string Command = "";
    private string Mode = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;
    private string startup;

    private bool jovahagyas = false;

    private void SetVisiblityVezetoiMegjegyzes(bool visible)
    {
        rowVezetoiMegjegyzes.Visible = visible;
        Megjegyzes_TextBox.ReadOnly = visible;
        VezetoiMegjegyzes_TextBox.Text = "";
    }

    private void SetVisiblityMegjegyzes(bool visible)
    {
        rowMegjegyzes.Visible = visible;
        rowVezetoiMegjegyzes.Visible = visible;
    }

    private string CheckMegjegyzesJavaScript(TextBox tb, string msgIfEmpty)
    {
        string js = @"
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };

var megjegyzes = $get('" + tb.ClientID + @"');
if (megjegyzes && megjegyzes.value.trim() == '')
{
    alert('" + msgIfEmpty + @"');
    return false;
}
";
        return js;
    }

    #region Template
    private bool IsTemblateEnabled
    {
        get
        {
            if (Command == CommandName.Modify &&
                startup == Constants.Startup.FromUgyirat)
            {
                return true;
            }

            return false;
        }
    }

    public class IrattarbaAdasTemplateObject
    {
        private string _megjegyzes;

        public string Megjegyzes
        {
            get { return _megjegyzes; }
            set { _megjegyzes = value; }
        }

        private string _vezetoiMegjegyzes;

        public string VezetoiMegjegyzes
        {
            get { return _vezetoiMegjegyzes; }
            set { _vezetoiMegjegyzes = value; }
        }

        private string _irattarJellege;

        public string IrattarJellege
        {
            get { return _irattarJellege; }
            set { _irattarJellege = value; }
        }

        private string _irattar;

        public string Irattar
        {
            get { return _irattar; }
            set { _irattar = value; }
        }
    }

    void FormHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromTemplate((IrattarbaAdasTemplateObject)FormHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            FormHeader1.NewTemplate(GetTemplateObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            FormHeader1.SaveCurrentTemplate(GetTemplateObjectFromComponents());
        }
    }

    private void LoadComponentsFromTemplate(IrattarbaAdasTemplateObject templateObject)
    {
        if (templateObject != null)
        {
            if (!String.IsNullOrEmpty(templateObject.Megjegyzes))
            {
                Megjegyzes_TextBox.Text = templateObject.Megjegyzes;
            }

            if (!String.IsNullOrEmpty(templateObject.VezetoiMegjegyzes))
            {
                VezetoiMegjegyzes_TextBox.Text = templateObject.VezetoiMegjegyzes;
            }

            if (!String.IsNullOrEmpty(templateObject.IrattarJellege))
            {
                IrattarDropDownList1.SetSelectedValue(templateObject.IrattarJellege);
                if (IrattarDropDownList1.SelectedValue == Constants.IrattarJellege.KozpontiIrattar)
                {
                    CsoportTextBox1.ReadOnly = true;
                }
            }

            if (!String.IsNullOrEmpty(templateObject.Irattar))
            {
                CsoportTextBox1.Id_HiddenField = templateObject.Irattar;
                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }
        }
    }

    private IrattarbaAdasTemplateObject GetTemplateObjectFromComponents()
    {
        IrattarbaAdasTemplateObject templateObject = new IrattarbaAdasTemplateObject();

        templateObject.Megjegyzes = Megjegyzes_TextBox.Text;
        templateObject.VezetoiMegjegyzes = VezetoiMegjegyzes_TextBox.Text;
        templateObject.IrattarJellege = IrattarDropDownList1.SelectedValue;
        templateObject.Irattar = CsoportTextBox1.Id_HiddenField;

        return templateObject;
    }

    #endregion

    private UI ui = new UI();

    private string[] SelectedUgyiratIds
    {
        get
        {
            object o = Session["SelectedUgyiratIds"];

            if (o != null)
                return ((string)o).Split(',');

            return null;
        }
    }

    private bool? _kotelezeoTeljessegEllenorzes = null;

    // Rendszerparam�ter szerint k�telez�-e teljesnek lennie az �gyiratnak iratt�rba ad�s el�tt?
    private bool KotelezoTeljessegEllenorzes
    {
        get
        {
            if (_kotelezeoTeljessegEllenorzes == null)
            {
                _kotelezeoTeljessegEllenorzes =
                                Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.TELJESSEGELLENORZES_IRATTARBAADASELOTT_ENABLED);
            }

            return _kotelezeoTeljessegEllenorzes.Value;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        startup = Request.QueryString.Get(Constants.Startup.StartupName);

        pageView = new PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        Mode = Request.QueryString.Get(QueryStringVars.Mode);
        jovahagyas = (Mode == CommandName.UgyiratIrattarozasJovahagyasa);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                break;
        }


        if (Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                if (!IsPostBack)
                {
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;

                    Result result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_UgyUgyiratok erec_UgyUgyiratok = (EREC_UgyUgyiratok)result.Record;
                        LoadComponentsFromBusinessObject(erec_UgyUgyiratok);
                        #region TeljessegEllenorzes
                        if (!jovahagyas)
                        {
                            if (KotelezoTeljessegEllenorzes)
                            {
                                string warningMsg;
                                bool passed = TeljessegEllenorzesResultPanel1.Ellenorzes(id, false, true, out warningMsg);
                                if (!passed)
                                {
                                    UgyiratAdatok.Visible = false;
                                    EFormPanel1.Visible = false;
                                    FormFooter1.SaveEnabled = false;
                                    FormFooter1.Visible = false;
                                    return;
                                }

                                /// BLG#2366: A sikeress�g ellen�re lehet hogy van egy�b megjegyz�s is, amit meg kell jelen�teni (ha van munkairat az �gyiratban)
                                /// A "Rendben" gombra r�tesz�nk egy megeros�to k�rd�st:
                                /// 
                                // (A figyelmezteto �zenetet az ErrorPanelbol vessz�k ki.)
                                FormFooter1.ImageButton_Save.OnClientClick = "if (confirm('" + warningMsg
                                                + "\\nBiztosan folytatja a m�veletet?') == false) { return false; }";
                            }
                            else
                            {
                                // csak figyelmeztet� �zenet, ha nem ok:
                                ASP.erecordcomponent_teljessegellenorzesresultpanel_ascx.DisplayWarningMessage(Page, erec_UgyUgyiratok, FormHeader1.ErrorPanel, null);
                            }
                        }
                        #endregion

                        //LZS - BUG_4789
                        if (startup == Constants.Startup.FromAtmenetiIrattar)
                        {
                            FormFooter1.ImageButton_SaveAndPrint.Enabled =
                            FormFooter1.ImageButton_SaveAndPrint.Visible = true;
                        }
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
        }
        else if (Command == CommandName.Irattarozasra)
        {
            if (!IsPostBack)
            {
                UgyiratAdatok.Visible = false;
                UgyiratokList.Visible = true;
                rowItsz.Visible = false;

                FormFooter1.ImageButton_Save.OnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);

                FillUgyiratokGridView();

                labelTetelekSzamaDb.Text = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString();
            }

            // Rendben gomb enged�lyez�se:
            FormFooter1.Command = CommandName.Modify;

            //LZS - BUG_4789
            FormFooter1.ImageButton_SaveAndPrint.Enabled =
            FormFooter1.ImageButton_SaveAndPrint.Visible = true;
            FormFooter1.ImageButton_SaveAndPrint.OnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);

            JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                    , "Javascripts/CheckBoxes.js");
        }

        IrattarDropDownList1.DropDownList.AutoPostBack = true;
        CsoportTextBox1.Filter = Constants.FilterType.Irattar;

        if (startup == Constants.Startup.FromUgyirat)
        {
            IrattarDropDownList1.DropDownList.SelectedIndexChanged += new EventHandler(IrattarDropDownList1_SelectedIndexChanged);
        }

        if (IsTemblateEnabled)
        {
            FormHeader1.TemplateObjectType = typeof(IrattarbaAdasTemplateObject);
            FormHeader1.FormTemplateLoader1_Visibility = true;
        }

        FormHeader1.ButtonsClick += new CommandEventHandler(FormHeader1_ButtonsClick);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
            System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (!IsPostBack)
        {
            // �tmeneti iratt�rba ad�sn�l megvizsg�ljuk, h�ny darab �tmeneti iratt�r van;
            // Ha csak egy, be�rjuk ezt.
            // (J�v�hagy�sn�l nem v�gjuk fejbe)
            if (startup == Constants.Startup.FromUgyirat && !jovahagyas)
            {
                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service_csoportok.GetAllIrattar(execParam);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
                else
                {
                    if (result.Ds != null && result.Ds.Tables[0].Rows.Count == 1)
                    {
                        // Egy iratt�r van, ez lesz a default
                        DataRow row = result.Ds.Tables[0].Rows[0];
                        string irattarId = row["Id"].ToString();
                        string irattarNev = row["Nev"].ToString();

                        CsoportTextBox1.Id_HiddenField = irattarId;
                        CsoportTextBox1.Text = irattarNev;

                        // elmentj�k ViewState-be, hogy ha k�zponti iratt�rat v�lasztanak, majd �jra �tmenetit, vissza tudjuk t�lteni
                        ViewState["AtmenetiIrattar_CsoportTextBox_Id"] = irattarId;
                    }

                    // ha nincs �tmeneti iratt�ra, a k�zpontit aj�nljuk fel:
                    if (result.Ds != null && result.Ds.Tables[0].Rows.Count == 0)
                    {
                        IrattarDropDownList1.SetKozpontiIrattar();

                        CsoportTextBox1.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower();
                        CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                        CsoportTextBox1.ReadOnly = true;
                    }
                    else
                    {
                        IrattarDropDownList1.SetAtmenetiIrattar();
                    }
                }
            }

        }

        // elutas�t�sn�l k�telez� a vezet�i megjegyz�s
        if (jovahagyas)
        {
            ImageReject.OnClientClick = CheckMegjegyzesJavaScript(VezetoiMegjegyzes_TextBox, Resources.Form.UI_ElutasitasVezetoiMegjegyzesKotelezo);
        }

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        //if (IsPostBack && !FormFooter1.ImageButton_Save.Visible)
        //{
        //    jovahagyas = true;
        //}

        if (!IsPostBack)
        {
            // figyelmeztet�s fizikai �tad�sra
            FormFooter1.ImageButton_Save.OnClientClick += " alert('" + Resources.List.UI_Atadas_AtadasFizikailag + "');";
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        //kiv�lasztott �gyirat adatainak megjelen�t�se
        UgyiratAdatok.SetUgyiratMasterAdatokByBusinessObject(erec_UgyUgyiratok, FormHeader1.ErrorPanel, null);

        //j�v�hagy�s eset�n 
        if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
        {
            //jovahagyas = true;
            IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

            this.SetVisiblityVezetoiMegjegyzes(true);

            CsoportTextBox1.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Cimzett;
            CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        }
        else
        {
            IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

            CsoportTextBox1.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Cimzett;
            CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

            IrattarDropDownList1.DropDownList.SelectedIndex = 0;
        }

        //Iratt�ri kezel�si feljegyz�s lek�r�se
        if (startup != Constants.Startup.FromAtmenetiIrattar)
        {
            //EREC_UgyKezFeljegyzesekService svc = eRecordService.ServiceFactory.GetEREC_UgyKezFeljegyzesekService();
            //ExecParam xpm = UI.SetExecParamDefault(Page, new ExecParam());

            //Result result = svc.GetLastIrattariKezelesiFeljegyzes(xpm, erec_UgyUgyiratok.Id);

            //if (String.IsNullOrEmpty(result.ErrorCode))
            //{
            //    EREC_UgyKezFeljegyzesek kezelesiFeljegyzes = (EREC_UgyKezFeljegyzesek)result.Record;

            //    if (kezelesiFeljegyzes != null)
            //    {
            //        Megjegyzes_TextBox.Text = kezelesiFeljegyzes.Leiras;
            //    }
            //    else
            //    {
            //        Megjegyzes_TextBox.Text = "";
            //    }
            //}
            //else
            //{
            //    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            //}
        }

    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(IraIrattariTetelTextBox1);
            compSelector.Add_ComponentOnClick(Megjegyzes_TextBox);
            //compSelector.Add_ComponentOnClick(Modosithato_CheckBox);
            compSelector.Add_ComponentOnClick(IrattarDropDownList1);

            FormFooter1.SaveEnabled = false;

        }
    }

    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != SelectedUgyiratIds.Length)
        {
            // nem annyi rekord j�tt, mint amennyit v�rtunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adhat� �t? 
        // CheckBoxok vizsg�lat�val:
        //if (Command == CommandName.KiadasAtmenetiIrattarbol)
        //{
        //    foreach (GridViewRow row in UgyUgyiratokGridView.Rows)
        //    {
        //        CheckBox checkBox = (CheckBox)row.FindControl("check");
        //        if (checkBox != null)
        //        {
        //            if (checkBox.Enabled)
        //            {
        //                row.Visible = false;
        //            }
        //            else
        //            {
        //                //System.Data.DataRowView drv = (System.Data.DataRowView)row.DataItem;
        //                SelectedIdsList.Remove(checkBox.Text);
        //                countKiNemAdhatok++;
        //            }
        //        }
        //    }
        //    if (countKiNemAdhatok > 0)
        //    {
        //        UgyiratokListPanel.Visible = true;
        //        labelNemKiadhatoUgyiratok.Visible = true;
        //        labelUgyiratok.Visible = false;
        //    }
        //}
        //else
        //{
        //    UgyiratokListPanel.Visible = true;
        //    int count_kikerhetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        //    int count_kiNEMkerhetok = SelectedIdsList.Count - count_kikerhetok;

        //    if (count_kiNEMkerhetok > 0)
        //    {
        //        Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_KikeresWarning, count_kiNEMkerhetok);
        //        Panel_Warning_Ugyirat.Visible = true;
        //    }
        //}
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (SelectedUgyiratIds != null && SelectedUgyiratIds.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            // keres�si objektum be�ll�t�sa: sz�r�s az Id-kra:
            search.Id.Value = Search.GetSqlInnerString(SelectedUgyiratIds);
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKozpontiIrattarbaKuldheto(e, Page);

            CheckBox cb = (CheckBox)e.Row.FindControl("check");
            if (cb != null)
                cb.Attributes.Add("onclick", "if (document.getElementById('" + cb.ClientID + "').checked)" +
                    "{document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)+1;}" +
                    "else {document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML=parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML)-1;}" +
                    "; return true;");
        }
    }

    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndPrint)
        {
            if (FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba") || FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba"))
            {
                switch (Command)
                {
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                                //EREC_UgyUgyiratok erec_UgyUgyiratok = GetBusinessObjectFromComponents();

                                // T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result;
                                if (!String.IsNullOrEmpty(startup) && startup == Constants.Startup.FromAtmenetiIrattar)
                                {
                                    result = service.AtadasKozpontiIrattarba(execParam, recordId, IraIrattariTetelTextBox1.Id_HiddenField, Megjegyzes_TextBox.Text, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);

                                    if (e.CommandName == CommandName.SaveAndPrint)
                                    {
                                        //LZS BUG_4789
                                        OpenSSRSRiport(e);
                                    }
                                }
                                else
                                {
                                    result = service.AtadasIrattarba(execParam, recordId, IraIrattariTetelTextBox1.Id_HiddenField, Megjegyzes_TextBox.Text, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);
                                }

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                            break;
                        }
                    case CommandName.Irattarozasra:
                        {
                            if (SelectedUgyiratIds == null)
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(EErrorPanel1);
                                ErrorUpdatePanel1.Update();
                            }
                            else
                            {
                                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                                // T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = new Result();

                                List<string> selectedUgyiratokList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, ErrorUpdatePanel1);

                                if (!String.IsNullOrEmpty(startup) && startup == Constants.Startup.FromAtmenetiIrattar)
                                {
                                    result = service.AtadasKozpontiIrattarba_Tomeges(execParam, selectedUgyiratokList.ToArray(), Megjegyzes_TextBox.Text, null, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);

                                    if (e.CommandName == CommandName.SaveAndPrint)
                                    {
                                        //LZS BUG_4789
                                        OpenSSRSRiport(e);
                                    }
                                }

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                    ErrorUpdatePanel1.Update();
                                }
                            }
                        }
                        break;
                }
            }
            else
            {
                Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
            }
        }
        
    }

    private void OpenSSRSRiport(CommandEventArgs e)
    {
        //LZS - BUG_4789
        if (e.CommandName == CommandName.SaveAndPrint)
        {
            string idsString = GetSelectedIdsAsSeparatedString();
            string js = null;

            if (string.IsNullOrEmpty(idsString))
            {
                idsString = Request.QueryString.Get(QueryStringVars.Id);
            }

            string url = "IrattarbaAdasSSSRS.aspx?ids=" + idsString;
            js = JavaScripts.SetOnClientClickWithTimeout(url, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokGridView.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIrattarbaAdasSSSRS", js, true);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        FormHeader1.DisableModeLabel = true;
        //�tmeneti iratt�r fixen be�ll�t�sa
        if (Command == CommandName.Modify)
        {
            if (!String.IsNullOrEmpty(startup) && startup == Constants.Startup.FromUgyirat)
            {
                // j�v�hagy�sn�l ReadOnly:
                if (jovahagyas)
                {
                    if (!IsPostBack)
                    {
                        // J�v�hagy�s: k�zponti vagy �tmeneti iratt�rba ad�s j�v�hagy�sa volt megadva?            
                        if (CsoportTextBox1.Id_HiddenField.ToLower() == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower())
                        {
                            IrattarDropDownList1.DropDownList.SelectedIndex = 1;
                            CsoportTextBox1.ReadOnly = true;
                        }
                        else
                        {
                            IrattarDropDownList1.DropDownList.SelectedIndex = 0;
                        }
                    }

                    IrattarDropDownList1.ReadOnly = true;
                }
                else
                {
                    IrattarDropDownList1.DropDownList.AutoPostBack = true;
                }
            }
        }

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(startup) && startup == Constants.Startup.FromAtmenetiIrattar)
            {
                this.SetVisiblityMegjegyzes(false);
                CsoportTextBox1.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id.ToLower();
                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                CsoportTextBox1.ReadOnly = true;
                IrattarDropDownList1.ReadOnly = true;
                IrattarDropDownList1.DropDownList.AutoPostBack = false;
                IrattarDropDownList1.SetKozpontiIrattar();
                CsoportTextBox1.ReadOnly = true;
                IraIrattariTetelTextBox1.ReadOnly = true;
            }
        }


        //Gombok be�ll�t�sa
        if (jovahagyas)
        {
            FormFooter1.ImageButton_Save.Visible = false;
            ImageApprove.Visible = true;
            ImageReject.Visible = true;
        }

        if (IsPostBack)
        {
            if (IrattarDropDownList1.SelectedValue == Constants.IrattarJellege.AtmenetiIrattar)
            {
                // �tmeneti iratt�r, lehet v�lasztani:
                CsoportTextBox1.Enabled = true;
                CsoportTextBox1.ReadOnly = false;
            }
            if (IrattarDropDownList1.SelectedValue == Constants.IrattarJellege.KozpontiIrattar)
            {
                CsoportTextBox1.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id.ToLower();
                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                //CsoportTextBox1.Enabled = false;
                CsoportTextBox1.ReadOnly = true;
            }
        }

    }


    protected void IrattarDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Ha �tmeneti iratt�rra v�ltott, �s kor�bban m�r lek�rt�k az �tmeneti iratt�rat, azt visszat�ltj�k ViewState-b�l
        if (!String.IsNullOrEmpty(startup) && startup == Constants.Startup.FromUgyirat
            && IrattarDropDownList1.DropDownList.SelectedIndex == 0)
        {
            string atmenetiIrattarId = String.Empty;
            if (ViewState["AtmenetiIrattar_CsoportTextBox_Id"] != null)
            {
                atmenetiIrattarId = ViewState["AtmenetiIrattar_CsoportTextBox_Id"].ToString();
            }
            if (!String.IsNullOrEmpty(atmenetiIrattarId))
            {
                CsoportTextBox1.Id_HiddenField = atmenetiIrattarId;
                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            }
            else
            {
                CsoportTextBox1.Id_HiddenField = String.Empty;
                CsoportTextBox1.Text = String.Empty;
            }
        }
    }


    protected void ImageApprove_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba") || FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba"))
        {
            switch (Command)
            {
                case CommandName.Modify:
                    {
                        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(recordId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(EErrorPanel1);
                            ErrorUpdatePanel1.Update();
                        }
                        else
                        {
                            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                            //// T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.AtadasIrattarbaJovahagyasa(execParam, recordId, IraIrattariTetelTextBox1.Id_HiddenField, VezetoiMegjegyzes_TextBox.Text, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }
                        break;
                    }
            }
        }
        else
        {
            Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
        }

    }
    protected void ImageReject_Click(object sender, ImageClickEventArgs e)
    {
        if (FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba") || FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba"))
        {
            switch (Command)
            {
                case CommandName.Modify:
                    {
                        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(recordId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(EErrorPanel1);
                            ErrorUpdatePanel1.Update();
                        }
                        else
                        {
                            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                            //// T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            Result result = service.AtadasIrattarbaVisszautasitasa(execParam, recordId, IraIrattariTetelTextBox1.Id_HiddenField, VezetoiMegjegyzes_TextBox.Text, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterSelectedRecordIdToParent(Page, recordId);
                                JavaScripts.RegisterCloseWindowClientScriptWithScripManager(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                                ErrorUpdatePanel1.Update();
                            }
                        }
                        break;
                    }
            }
        }
        else
        {
            Response.Redirect("Error.aspx?errorcode=UIAccessDeniedFunction", true);
        }
    }

    /// <summary>
    /// GetSelectedIdsAsSeparatedString
    /// </summary>
    /// <returns></returns>
    private string GetSelectedIdsAsSeparatedString()
    {
        StringBuilder sb = new System.Text.StringBuilder();
        foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, EErrorPanel1, null))
        {
            sb.Append(s + ",");
        }
        if (sb == null || sb.Length < 1)
            return string.Empty;

        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }
}
