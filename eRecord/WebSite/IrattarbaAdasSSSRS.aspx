<%@ Page Language="C#" MasterPageFile="~/PrintFormMasterPage.master" AutoEventWireup="true"
    CodeFile="IrattarbaAdasSSSRS.aspx.cs" Inherits="IrattarbaAdasSSSRS"
    Title="FutárJegyzék" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table cellpadding="0" cellspacing="0" width="95%">
        <tr class="urlapSor_kicsi">
            <td class="mrUrlapCaption_short">
                <rsweb:ReportViewer ID="ReportViewerIrattarbaAdas" runat="server" ProcessingMode="Remote"
                    Width="1200px" Height="1000px" EnableTelemetry="false">
                    <ServerReport ReportPath="/Irattarozas/AtadasIrattarba" />
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
</asp:Content>
