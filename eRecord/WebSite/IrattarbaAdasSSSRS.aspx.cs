﻿using Microsoft.Reporting.WebForms;
using System;
using System.Configuration;
using Contentum.eRecord.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;

public partial class IrattarbaAdasSSSRS : Contentum.eUtility.UI.PageBase
{
    string[] IdList = null;
    string ids2 = String.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        string ids = Request.QueryString["Ids"];
        ids2 = ids;

        if (String.IsNullOrEmpty(ids))
        {
            return;
        }
        IdList = ids.Split(new char[] { ',' });
        ids2 = "'" + ids2.Replace(",", "','") + "'";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitReportViewer();

        }
    }

    private void InitReportViewer()
    {
        ReportViewerCredentials rvc = new ReportViewerCredentials(ConfigurationManager.AppSettings["SSRS_SERVER_USER"], ConfigurationManager.AppSettings["SSRS_SERVER_PWD"]);
        ReportViewerIrattarbaAdas.ServerReport.ReportServerCredentials = rvc;
        System.Uri ReportServerUrl = new System.Uri(ConfigurationManager.AppSettings["SSRS_SERVER_URL"]);
        ReportViewerIrattarbaAdas.ServerReport.ReportServerUrl = ReportServerUrl;
       ReportViewerIrattarbaAdas.ServerReport.ReportPath = (string.IsNullOrEmpty(ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) ? "" : "/" + ConfigurationManager.AppSettings["SSRS_SERVER_DIR"]) + ReportViewerIrattarbaAdas.ServerReport.ReportPath;


        ReportParameterInfoCollection rpis = ReportViewerIrattarbaAdas.ServerReport.GetParameters();


        if (rpis.Count > 0)
        {
            ReportParameter[] ReportParameters = GetReportParameters(rpis);

            ReportViewerIrattarbaAdas.ServerReport.SetParameters(ReportParameters);
        }

        ReportViewerIrattarbaAdas.ShowParameterPrompts = false;
        ReportViewerIrattarbaAdas.ServerReport.Refresh();
    }

    protected ReportParameter[] GetReportParameters(ReportParameterInfoCollection rpis)
    {
        ReportParameter[] ReportParameters = null;

        if (rpis != null)
        {
            if (rpis.Count > 0)
            {
                ReportParameters = new ReportParameter[rpis.Count];

                for (int i = 0; i < rpis.Count; i++)
                {
                    ReportParameters[i] = new ReportParameter(rpis[i].Name);

                    switch (rpis[i].Name)
                    {
                        case "Where":
                            //ReportParameters[i].Values.Add(" ( Erec_PldIratPeldanyok.id in ("+ ids2 +") and getdate() between EREC_PldIratPeldanyok.ErvKezd and EREC_PldIratPeldanyok.ErvVege ) ");
                            break;
                        //case "Where_EREC_IraIratok":
                        //    ReportParameters[i].Values.Add(where_EREC_IraIratok);
                        //    break;
                        //case "OrderBy":
                        //    ReportParameters[i].Values.Add(DefaultOrderBy);
                        //    break;
                        //case "TopRow":
                        //    ReportParameters[i].Values.Add(DefaultPageSize);
                        //    break;
                        case "ExecutorUserId":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.FelhasznaloId(Page));
                            break;
                        case "FelhasznaloSzervezet_Id":
                            ReportParameters[i].Values.Add(FelhasznaloProfil.GetCurrent(Page).Felhasznalo.Org.Id);
                            break;
                        //case "IntervallumFilter":
                        //    ReportParameters[i].Values.Add(GetIntervallumFilter());
                        //    break;
                        //case "eRecordWebSiteUrl":
                        //    ReportParameters[i].Values.Add(GeteRecordWebSiteUrl());
                        //    break;

                        case "ids":
                            ReportParameters[i].Values.AddRange(IdList);
                            break;
                    }
                }
            }
        }

        return ReportParameters;
    }
}
