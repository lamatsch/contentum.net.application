<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IrattarbaAdasTomegesForm.aspx.cs" Inherits="IrattarbaAdasTomegesForm" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc5" %>


<%@ Register Src="Component/FelhasznaloTextBox.ascx" TagName="FelhasznaloTextBox" TagPrefix="uc11" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc12" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl" TagPrefix="uc9" %>

<%@ Register src="eRecordComponent/InfoModalPopup.ascx" tagname="InfoModalPopup" tagprefix="uc3" %>
<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>

<%@ Register src="eRecordComponent/IrattarDropDownList.ascx" tagname="irattardropdownlist" tagprefix="uc8" %>
<%@ Register src="eRecordComponent/IraIrattariTetelTextBox.ascx" tagname="irairattariteteltextbox" tagprefix="uc7" %>
<%@ Register src="Component/CsoportTextBox.ascx" tagname="csoporttextbox" tagprefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true" />

     <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>
        
    <!--Friss�t�s jelz�se-->
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />    
                
    <uc1:FormHeader ID="FormHeader1" runat="server" />
    
    <div> 
    <asp:Panel ID="MainPanel" runat="server">
    
        <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
        
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                    
                    <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                
                 <br />
                 <asp:Label ID="Label_Ugyiratok" runat="server" Text="�gyiratok:" Visible="false"  CssClass="GridViewTitle"></asp:Label>          
                 <br />
                 
                <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                    BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                    OnRowDataBound="UgyUgyiratokGridView_RowDataBound"
                    AllowSorting="False" AutoGenerateColumns="False" DataKeyNames="Id">
                    <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                    <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                    <HeaderStyle CssClass="GridViewHeaderStyle"/>  
                    
                    <Columns>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />                               
                                <ItemTemplate>
                                    <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                </ItemTemplate>
                            </asp:TemplateField>    
                            
                            <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">                                
                                <ItemTemplate>                                    
                                    <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png" 
                                        Visible="false" OnClientClick="return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>      
                         <asp:TemplateField HeaderText="Iratt�r" Visible="false">
                           <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            <ItemTemplate>
                                <asp:Label ID="Label_IrattarTipus" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:BoundField DataField="Foszam_Merge" HeaderText="�gyirat" 
                                SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam" >
                                <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Targy" HeaderText="Felel�s" SortExpression="Targy" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                                                
                            <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito"  >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev"  >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat" >
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>                        
                            <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                            <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                            <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                        </asp:BoundField>
                            <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                <HeaderTemplate>
                                    <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>                                        
                    <PagerSettings Visible="False" />
                </asp:GridView>
                    </asp:Panel>                   
                                    
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                    <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="Panel_Warning_Kuldemeny" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_Kuldemeny" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="Panel_Warning_IratPeldany" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_IratPeldany" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="Panel_Warning_Dosszie" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_Dosszie" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="Panel_Warning_NemAzonositottVonalkodok" runat="server" Visible="false"
                        HorizontalAlign="Center">
                        <br />
                        <asp:Label ID="labelNotIdentifiedHeader" runat="server" Text="<%$Resources:Form,UI_NemAzonositottVonalkodok%>"
                            Font-Bold="true" Font-Size="Larger" CssClass="warningHeader"></asp:Label>
                        <span style="position: relative; top: -10px; left: 38%;">
                            <asp:Label ID="Label_Warning_NemAzonositottVonalkodok" runat="server" Text="Label"
                                CssClass="warningHeader"></asp:Label>
                        </span>
                    </asp:Panel>
                    <eUI:eFormPanel ID="EFormPanel3" runat="server" style="width:70%;padding-top:20px;padding-bottom:20px;">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr class="urlapSor" id="rowItsz" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Nev_Label" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server"></uc7:IraIrattariTetelTextBox>
                            </td>
                        </tr>
                        <%--LZS - BUG_7099
                        Feljegyz�s sor EREC_UgyUgyiratok.Megjegyzes--%>
                        <tr runat="server" id="rowFeljegyzes" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelFeljegyzes" runat="server" Text="<%$Forditas:labelFeljegyzes|Feljegyz�s:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Feljegyzes_TextBox" runat="server" CssClass="mrUrlapInput" TextMode="MultiLine"
                                    Rows="3">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <%--LZS - BUG_7099
                        Megjegyz�s sor EREC_UgyUgyiratok.Note--%>
                        <tr runat="server" id="rowMegjegyzes" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelMegjegyzes" runat="server" Text="<%$Forditas:labelMegjegyzes|Megjegyz�s:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="Megjegyzes_TextBox" runat="server" CssClass="mrUrlapInput" TextMode="MultiLine"
                                    Rows="3">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="rowVezetoiMegjegyzes" visible="false" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Vezet�i megjegyz�s:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="VezetoiMegjegyzes_TextBox" runat="server" CssClass="mrUrlapInput"
                                    TextMode="MultiLine" Rows="3">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor" id="trIrattarJellege" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label><asp:Label
                                    ID="Label2" runat="server" Text="Iratt�r jellege:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc8:IrattarDropDownList ID="IrattarDropDownList1" runat="server"></uc8:IrattarDropDownList>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="Iratt�r:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc9:CsoportTextBox ID="CsoportTextBox1" runat="server" AjaxEnabled="false" />
                                <asp:Label ID="Label_ElektronikusIrattar" runat="server" Text="Elektronikus iratt�r"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                    <%--BUG_8005--%>
                     <br />
                        <table>
                            <tr>
                                <td style="padding-right: 15px;">
                                    <asp:ImageButton ID="ImageApprove" runat="server" ImageUrl="~/images/hu/ovalgomb/jovahagyas.gif"
                                        onmouseover="swapByName(this.id,'jovahagyas2.gif')" onmouseout="swapByName(this.id,'jovahagyas.gif')"
                                        CommandName="Approve" Visible="False" OnClick="ImageApprove_Click" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="ImageReject" runat="server" ImageUrl="~/images/hu/ovalgomb/elutasitas.gif"
                                        onmouseover="swapByName(this.id,'elutasitas2.gif')" onmouseout="swapByName(this.id,'elutasitas.gif')"
                                        CommandName="Reject" Visible="False" OnClick="ImageReject_Click" />
                                </td>
                                <td>
                                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                                </td>
                            </tr>
                        </table>
                </td>
            </tr>
            <%--BUG_8005--%>
 <%--           <tr>
                <td>
                    <uc2:formfooter id="FormFooter1" runat="server" />
                </td>
            </tr>--%>
        </table>
    </asp:Panel>
    <asp:UpdatePanel runat="server" ID="upResult">
    <ContentTemplate>
    <asp:Panel ID="ResultPanel" runat="server" Visible="false" Width="90%">
       <asp:HiddenField ID="hfSelectedKezbesitesiTetelek" runat="server" />
       <asp:HiddenField ID="hfSelectedUgyiratok" runat="server" />
       <asp:HiddenField ID="hfSelectedIratPldok" runat="server" />
       <asp:HiddenField ID="hfSelectedKuldemenyek" runat="server" />
        <eUI:eFormPanel id="EFormPanel2" runat="server" cssclass="mrResultPanel">
                <div class="mrResultPanelText">A kijel�lt t�telek �tad�sa sikeresen v�grehajt�dott.</div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImagePrint" runat="server" ImageUrl="~/images/hu/ovalgomb/atvetelilistanyomtatas.gif"
                                onmouseover="swapByName(this.id,'atvetelilistanyomtatas2.gif')" onmouseout="swapByName(this.id,'atvetelilistanyomtatas.gif')"
                               CommandName="Print" />
                        </td>
                        <td>
                            <asp:ImageButton ID="ImageClose" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                CommandName="Close" />
                        </td>
                    </tr>
                </table>
        </eUI:eFormPanel>
    </asp:Panel>
        <asp:Panel ID="ResultPanel_IrattarbaVetel" runat="server" Visible="false" Width="90%">
            <eUI:eFormPanel ID="EFormPanel1" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">
                    A kijel�lt t�telek iratt�rba v�tele sikeresen v�grehajt�dott.</div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageClose_IrattarbaVetelResult" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                CommandName="Close" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
        </asp:Panel>
        <%--BUG_8005--%>
         <asp:Panel ID="ResultPanel_IrattarbaVetelVisszautasitas" runat="server" Visible="false" Width="90%">
            <eUI:eFormPanel ID="EFormPanel4" runat="server" CssClass="mrResultPanel">
                <div class="mrResultPanelText">
                    A kijel�lt t�telek iratt�rba v�tel�nek elutas�t�sa sikeresen v�grehajt�dott.</div>
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="ImageClose_IrattarbaVetelVisszautasitasResult" runat="server" ImageUrl="~/images/hu/ovalgomb/bezar.jpg"
                                onmouseover="swapByName(this.id,'bezar2.jpg')" onmouseout="swapByName(this.id,'bezar.jpg')"
                                CommandName="Close" />
                        </td>
                    </tr>
                </table>
            </eUI:eFormPanel>
        </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    </div>

    <script type="text/javascript" language="javascript" src="JavaScripts/jquery-3.2.1.min.js"></script>

</asp:Content>
