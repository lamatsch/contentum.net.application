using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IrattarbaAdasTomegesForm : Contentum.eUtility.UI.PageBase
{
    private UI ui = new UI();
    private ComponentSelectControl compSelector = null;
    private string Command = "";
    private bool jovahagyas = false;
    private string startup = "";
    private String UgyiratId = "";
    private String[] UgyiratokArray;

    private const string FunkcioKod_UgyiratAtadas = "UgyiratAtadasIrattarba";

    private void SetVisiblityVezetoiMegjegyzes(bool visible)
    {
        rowVezetoiMegjegyzes.Visible = visible;
        Feljegyzes_TextBox.ReadOnly = visible;
        VezetoiMegjegyzes_TextBox.Text = "";

        // BUG_11706, j�v�hagy�skor csak a Vezetoi megjegyz�s l�tsz�djon
        rowFeljegyzes.Visible = !visible;
        rowMegjegyzes.Visible = !visible;
    }

    //LZS - BUG_7099
    //Feljegyz�s sz�vegdoboz sor�nak l�that�s�ga.
    private void SetVisiblityFeljegyzes(bool visible)
    {
        rowFeljegyzes.Visible = visible;
    }

    private string CheckMegjegyzesJavaScript(TextBox tb, string msgIfEmpty)
    {
        string js = @"
            String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };

            var megjegyzes = $get('" + tb.ClientID + @"');
            if (megjegyzes && megjegyzes.value.trim() == '')
            {
                alert('" + msgIfEmpty + @"');
                return false;
            }
            ";
        return js;
    }

    private void LoadComponentsFromBusinessObject(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        // BUG_8005
        //j�v�hagy�s eset�n 
        //if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
        //{
        //    //jovahagyas = true;
        //    IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
        //    IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

        //    this.SetVisiblityVezetoiMegjegyzes(true);

        //        CsoportTextBox1.Id_HiddenField = erec_UgyUgyiratok.Csoport_Id_Cimzett;
        //        CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        //}
        //else
        //{
        //    IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
        //    IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);
        //    CsoportTextBox1.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower();
        //    CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        //    IrattarDropDownList1.DropDownList.SelectedIndex = 1;
        //}
        if (erec_UgyUgyiratok.Allapot == KodTarak.UGYIRAT_ALLAPOT.IrattarozasJovahagyasa)
        {
            this.SetVisiblityVezetoiMegjegyzes(true);
        }

        //jovahagyas = true;
        IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratok.IraIrattariTetel_Id;
        IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(FormHeader1.ErrorPanel);

        CsoportTextBox1.Set(erec_UgyUgyiratok.Csoport_Id_Cimzett, FormHeader1.ErrorPanel);

        // BUG_4765
        bool isElektronikus = IsElektronikus(erec_UgyUgyiratok);
        trIrattarJellege.Visible = !isElektronikus;
        CsoportTextBox1.Visible = !isElektronikus;
        Label_ElektronikusIrattar.Visible = isElektronikus;
    }

    public string maxTetelszam = "0";

    private bool IsElektronikus(EREC_UgyUgyiratok erec_UgyUgyiratok)
    {
        var execParam = UI.SetExecParamDefault(Page, new ExecParam());
        return erec_UgyUgyiratok != null
            && !Contentum.eRecord.BaseUtility.Ugyiratok.IsIrattarozasElektronikusJovahagyasNelkul(execParam)
            && erec_UgyUgyiratok.Jelleg == KodTarak.UGYIRAT_JELLEG.Elektronikus;
    }

    public override void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            compSelector.Add_ComponentOnClick(IraIrattariTetelTextBox1);
            compSelector.Add_ComponentOnClick(Feljegyzes_TextBox);
            //compSelector.Add_ComponentOnClick(Modosithato_CheckBox);
            compSelector.Add_ComponentOnClick(IrattarDropDownList1);

            FormFooter1.SaveEnabled = false;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        this.FormHeader1.HeaderTitle = "�gyiratok �tad�sa iratt�rba";
        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        Command = Request.QueryString.Get(CommandName.Command);
        startup = Request.QueryString.Get(Constants.Startup.StartupName);
        jovahagyas = Request.QueryString.Get(QueryStringVars.Mode) == CommandName.UgyiratIrattarozasJovahagyasa;

        BoundField bf = UI.GetGridViewColumn(UgyUgyiratokGridView, "IrattariTetelszam");
        if (bf != null) { bf.Visible = !jovahagyas; }

        // FormFooternek be�ll�tjuk a Modify �zemm�dot:
        FormFooter1.Command = CommandName.Modify;

        if (string.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.UgyiratId)))
        {
            if (Session["SelectedUgyiratIds"] != null)
                UgyiratId = Session["SelectedUgyiratIds"].ToString();
        }
        else UgyiratId = Request.QueryString.Get(QueryStringVars.UgyiratId);

        if (!String.IsNullOrEmpty(UgyiratId))
        {
            // �gyirat�tv�tel      
            UgyiratokArray = UgyiratId.Split(',');
        }

        // Funkci�jog-ellen�rz�s:               
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, FunkcioKod_UgyiratAtadas);

        if (!IsPostBack)
        {
            LoadFormComponents();

            EREC_UgyUgyiratokService srv = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ep = UI.SetExecParamDefault(Page, new ExecParam());
            ep.Record_Id = UgyiratokArray[0].ToString();
            Result res = srv.Get(ep);
            if (res.IsError || res.Record == null)
            {
                throw new Contentum.eUtility.ResultException(res.ErrorCode);
            }
            LoadComponentsFromBusinessObject(res.Record as EREC_UgyUgyiratok);
        }

        // BUG_8005
        //LZS - BUG_4789
        if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            FormFooter1.ImageButton_SaveAndPrint.Enabled =
            FormFooter1.ImageButton_SaveAndPrint.Visible = true;
            FormFooter1.ImageButton_SaveAndPrint.OnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
        }
        else
        {
            FormFooter1.ImageButton_SaveAndPrint.Enabled =
            FormFooter1.ImageButton_SaveAndPrint.Visible = false;
        }
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                    , "Javascripts/CheckBoxes.js");
        IrattarDropDownList1.DropDownList.AutoPostBack = true;
        CsoportTextBox1.Filter = Constants.FilterType.Irattar;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        IrattarDropDownList1.DropDownList.SelectedIndexChanged += new EventHandler(IrattarDropDownList1_SelectedIndexChanged);

        FormHeader1.DisableModeLabel = true;

        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        string closeJavascript= "window.returnValue=true;" +
            "if(window.opener && window.returnValue && window.opener.__doPostBack){window.opener.__doPostBack('',window.opener.postBackArgument);}"
       + "window.close();";

        ImageClose.OnClientClick = closeJavascript;
        ImageClose_IrattarbaVetelResult.OnClientClick = closeJavascript;
        // BUG_8005
        ImageClose_IrattarbaVetelVisszautasitasResult.OnClientClick = closeJavascript;

        FormFooter1.ImageButton_Save.OnClientClick = "var count = parseInt(document.getElementById('" + labelTetelekSzamaDb.ClientID + "').innerHTML); "
                + "if (count>" + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false; } "
                + "else if (count == 0) {alert('" + Resources.Error.UINoSelectedRow + "'); return false; } ";

        labelTetelekSzamaDb.Text = (ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString());
        if (!IsPostBack)
        {
            // �tmeneti iratt�rba ad�sn�l megvizsg�ljuk, h�ny darab �tmeneti iratt�r van;
            // Ha csak egy, be�rjuk ezt.
            // (J�v�hagy�sn�l nem v�gjuk fejbe)
            if (startup == Constants.Startup.FromUgyirat && !jovahagyas)
            {
                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service_csoportok.GetAllIrattar(execParam);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
                else
                {
                    if (result.GetCount == 1)
                    {
                        // Egy iratt�r van, ez lesz a default
                        DataRow row = result.Ds.Tables[0].Rows[0];
                        string irattarId = row["Id"].ToString();
                        string irattarNev = row["Nev"].ToString();

                        CsoportTextBox1.Set(irattarId, irattarNev);

                        // elmentj�k ViewState-be, hogy ha k�zponti iratt�rat v�lasztanak, majd �jra �tmenetit, vissza tudjuk t�lteni
                        ViewState["AtmenetiIrattar_CsoportTextBox_Id"] = irattarId;
                    }

                    // ha nincs �tmeneti iratt�ra, a k�zpontit aj�nljuk fel:
                    if (result.GetCount == 0)
                    {
                        IrattarDropDownList1.SetKozpontiIrattar();

                        CsoportTextBox1.Set(KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower(), FormHeader1.ErrorPanel);
                        CsoportTextBox1.ReadOnly = true;
                    }
                    else
                    {
                        IrattarDropDownList1.SetAtmenetiIrattar();
                        if (result.GetCount > 1) // BUG_8439: ha csak egy �tmeneti iratt�r van, azt hagyjuk be�ll�tva
                        {
                            // BUG_8005
                            CsoportTextBox1.Clear();
                        }
                    }
                }
            }
        }

        // BUG_8005
        // elutas�t�sn�l k�telez� a vezet�i megjegyz�s
        if (jovahagyas)
        {
            ImageReject.OnClientClick = CheckMegjegyzesJavaScript(VezetoiMegjegyzes_TextBox, Resources.Form.UI_ElutasitasVezetoiMegjegyzesKotelezo);
        }

        if (!IsPostBack)
        {
            // Ha van valahol figyelmeztet� �zenet, azt ki kell tenni, �s ilyenkor kell egy meger�s�t� k�rd�s:
            string jsCheckWarningMsg = "var cbWithWarning = $('#" + UgyUgyiratokGridView.ClientID + " span.hasWarningMsg input:checked').first(); "
                    + " if (cbWithWarning.length > 0) { var warningMsg = cbWithWarning.parent().parent().next().find('input').attr('title'); "
                    + " if (confirm(warningMsg + '\\nBiztosan folytatja a m�veletet?') == false) {return false;} } ";

            // figyelmeztet�s fizikai �tad�sra
            FormFooter1.ImageButton_Save.OnClientClick += jsCheckWarningMsg + " alert('" + Resources.List.UI_Atadas_AtadasFizikailag + "');";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (startup == Constants.Startup.FromUgyirat)
        {
            // j�v�hagy�sn�l ReadOnly:
            if (jovahagyas)
            {
                if (!IsPostBack)
                {
                    // J�v�hagy�s: k�zponti vagy �tmeneti iratt�rba ad�s j�v�hagy�sa volt megadva?            
                    if (CsoportTextBox1.Id_HiddenField.ToLower() == KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower())
                    {
                        IrattarDropDownList1.DropDownList.SelectedIndex = 1;
                        CsoportTextBox1.ReadOnly = true;
                    }
                    else
                    {
                        IrattarDropDownList1.DropDownList.SelectedIndex = 0;
                    }
                }

                IrattarDropDownList1.ReadOnly = true;
            }
            else
            {
                IrattarDropDownList1.DropDownList.AutoPostBack = true;
            }
        }

        if (!IsPostBack)
        {
            if (startup == Constants.Startup.FromAtmenetiIrattar)
            {
                //LZS - BUG_7099
                //Feljez�s sz�vegdoboz megjelen�t�se.
                this.SetVisiblityFeljegyzes(true);

                CsoportTextBox1.Set(KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id.ToLower(), FormHeader1.ErrorPanel);
                CsoportTextBox1.ReadOnly = true;
                IrattarDropDownList1.ReadOnly = true;
                IrattarDropDownList1.DropDownList.AutoPostBack = false;
                IrattarDropDownList1.SetKozpontiIrattar();
                IraIrattariTetelTextBox1.ReadOnly = true;
            }
        }

        // BUG_8005
        //Gombok be�ll�t�sa
        if (jovahagyas)
        {
            FormFooter1.ImageButton_Save.Visible = false;
            ImageApprove.Visible = true;
            ImageReject.Visible = true;
        }
        if (IsPostBack)
        {
            if (IrattarDropDownList1.SelectedValue == Constants.IrattarJellege.AtmenetiIrattar)
            {
                // �tmeneti iratt�r, lehet v�lasztani:
                CsoportTextBox1.Enabled = true;
                CsoportTextBox1.ReadOnly = false;
            }
            if (IrattarDropDownList1.SelectedValue == Constants.IrattarJellege.KozpontiIrattar)
            {
                CsoportTextBox1.Set(KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id.ToLower(), FormHeader1.ErrorPanel);
                //CsoportTextBox1.Enabled = false;
                CsoportTextBox1.ReadOnly = true;
            }
        }
        //IraIrattariTetelTextBox1.Enabled = false;
        IraIrattariTetelTextBox1.ReadOnly = true;

        // BUG_9179
        rowItsz.Visible = !jovahagyas;
    }

    protected void IrattarDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Ha �tmeneti iratt�rra v�ltott, �s kor�bban m�r lek�rt�k az �tmeneti iratt�rat, azt visszat�ltj�k ViewState-b�l
        if (startup == Constants.Startup.FromUgyirat
            && IrattarDropDownList1.DropDownList.SelectedIndex == 0)
        {
            string atmenetiIrattarId = String.Empty;
            if (ViewState["AtmenetiIrattar_CsoportTextBox_Id"] != null)
            {
                atmenetiIrattarId = ViewState["AtmenetiIrattar_CsoportTextBox_Id"].ToString();
            }
            if (!String.IsNullOrEmpty(atmenetiIrattarId))
            {
                CsoportTextBox1.Set(atmenetiIrattarId, FormHeader1.ErrorPanel);
            }
            else
            {
                CsoportTextBox1.Clear();
            }
        }
    }

    // BUG_8005

    private void ApproveReject(bool isApprove)
    {
        if (FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba") || FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba"))
        {
            if (String.IsNullOrEmpty(UgyiratId))
            {
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                return;
            }
            else
            {
                List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                if (selectedItemsList.Count == 0)
                {
                    ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                    return;
                }
                EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

                //// T�nyleges m�dos�that�s�g-ellen�rz�s majd a WebService oldalon

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                var itsz = jovahagyas ? "" : IraIrattariTetelTextBox1.Id_HiddenField; // BUG_9179

                Result result;
                if (Label_ElektronikusIrattar.Visible) // BUG_4765
                {
                    var elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(execParam);
                    result = isApprove ?
                        service.AtadasIrattarbaJovahagyasa_Tomeges(execParam, selectedItemsList.ToArray(), itsz, VezetoiMegjegyzes_TextBox.Text, "E", elektronikusIrattarId) :
                        service.AtadasIrattarbaVisszautasitasa_Tomeges(execParam, selectedItemsList.ToArray(), itsz, VezetoiMegjegyzes_TextBox.Text, "E", elektronikusIrattarId);
                }
                else
                {
                    result = isApprove ?
                        service.AtadasIrattarbaJovahagyasa_Tomeges(execParam, selectedItemsList.ToArray(), itsz, VezetoiMegjegyzes_TextBox.Text, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField) :
                        service.AtadasIrattarbaVisszautasitasa_Tomeges(execParam, selectedItemsList.ToArray(), itsz, VezetoiMegjegyzes_TextBox.Text, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);
                }

                if (!string.IsNullOrEmpty(result.ErrorCode))
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                }
                else
                {
                    MainPanel.Visible = false;
                    ResultPanel.Visible = false;
                    ResultPanel_IrattarbaVetel.Visible = isApprove;
                    ResultPanel_IrattarbaVetelVisszautasitas.Visible = !isApprove;
                    ImagePrint.Visible = false;
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }

    protected void ImageApprove_Click(object sender, ImageClickEventArgs e)
    {
        ApproveReject(true);
    }

    //BUG_8005
    protected void ImageReject_Click(object sender, ImageClickEventArgs e)
    {
        ApproveReject(false);
    }

    private void LoadFormComponents()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            UgyiratokListPanel.Visible = true;
            Label_Ugyiratok.Visible = true;

            FillUgyiratokGridView();
        }
    }

    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellen�rz�s:
        if (ds != null && ds.Tables[0].Rows.Count != UgyiratokArray.Length)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
        }

        // Van-e olyan rekord, ami nem vehet� �t? 
        // CheckBoxok vizsg�lat�val:
        int count_atvehetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

        int count_NEMatvehetok = UgyiratokArray.Length - count_atvehetok;

        if (count_NEMatvehetok > 0)
        {
            Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_AtvetelreKijeloltekWarning, count_NEMatvehetok);
            //Label_Warning_Ugyirat.Visible = true;
            Panel_Warning_Ugyirat.Visible = true;
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (UgyiratokArray != null && UgyiratokArray.Length > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            search.Id.In(UgyiratokArray);

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            return res.Ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
        // BUG_8005
        if (startup == Constants.Startup.FromAtmenetiIrattar)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKozpontiIrattarbaKuldheto(e, Page);

        }
        else
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckIrattarbaKuldheto(e, Page);
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save || e.CommandName == CommandName.SaveAndPrint)
        {
            // BUG_8005
            //if (FunctionRights.GetFunkcioJog(Page, FunkcioKod_UgyiratAtadas))    
            if (FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba") || FunctionRights.GetFunkcioJog(Page, "UgyiratAtadasIrattarba"))
            {
                if (String.IsNullOrEmpty(UgyiratId))
                {
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    return;
                }
                else
                {
                    EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                    if (selectedItemsList.Count == 0)
                    {
                        ResultError.DisplayWarningOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.WarningLabel, Resources.Error.UINoSelectedItem);
                        return;
                    }

                    // BUG_8005
                    var itsz = IraIrattariTetelTextBox1.Id_HiddenField;
                    var ugyiratok = selectedItemsList.ToArray();
                    var megj = Megjegyzes_TextBox.Text;
                    var felj = Feljegyzes_TextBox.Text;

                    Result result;
                    if (Label_ElektronikusIrattar.Visible) // BUG_4765
                    {
                        var elektronikusIrattarId = KodTarak.SPEC_SZERVEK.GetElektronikusIrattar(execParam);
                        result = service.AtadasIrattarba_Tomeges(execParam, ugyiratok, itsz, megj, felj, "E" /* nincs haszn�lva, csak ne legyen �res */, elektronikusIrattarId);
                    }
                    else if (startup == Constants.Startup.FromAtmenetiIrattar)
                    {
                        result = service.AtadasKozpontiIrattarba_Tomeges(execParam, ugyiratok, megj, felj, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);
                        if (e.CommandName == CommandName.SaveAndPrint)
                        {
                            //LZS BUG_4789
                            OpenSSRSRiport(e);
                        }
                    }
                    else
                    {
                        result = service.AtadasIrattarba_Tomeges(execParam, ugyiratok, itsz, megj, felj, IrattarDropDownList1.SelectedValue, CsoportTextBox1.Id_HiddenField);
                    }
                    if (!string.IsNullOrEmpty(result.ErrorCode))
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                    }
                    else
                    {
                        MainPanel.Visible = false;
                        ResultPanel.Visible = true;
                        // BUG_8005
                        ResultPanel_IrattarbaVetel.Visible = false;
                        ResultPanel_IrattarbaVetelVisszautasitas.Visible = false;

                        ImagePrint.Visible = false;
                    }
                }
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
        }
    }

    // BUG_8005
    private void OpenSSRSRiport(CommandEventArgs e)
    {
        //LZS - BUG_4789
        if (e.CommandName == CommandName.SaveAndPrint)
        {
            string idsString = GetSelectedIdsAsSeparatedString();
            string js = null;

            if (string.IsNullOrEmpty(idsString))
            {
                idsString = Request.QueryString.Get(QueryStringVars.Id);
            }

            string url = "IrattarbaAdasSSSRS.aspx?ids=" + idsString;
            js = JavaScripts.SetOnClientClickWithTimeout(url, "", Defaults.PopupWidth_Max, Defaults.PopupHeight, UgyUgyiratokGridView.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedIrattarbaAdasSSSRS", js, true);
        }
    }

    /// <summary>
    /// GetSelectedIdsAsSeparatedString
    /// </summary>
    /// <returns></returns>
    private string GetSelectedIdsAsSeparatedString()
    {
        StringBuilder sb = new System.Text.StringBuilder();
        foreach (string s in ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null))
        {
            sb.Append(s + ",");
        }
        if (sb == null || sb.Length < 1)
            return string.Empty;

        sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }

}

