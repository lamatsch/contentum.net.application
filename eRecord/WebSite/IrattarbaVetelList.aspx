﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="IrattarbaVetelList.aspx.cs" Inherits="IrattarbaVetelList" Title="Untitled Page" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress" TagPrefix="uc3" %>
<%@ Register Src="Component/ListHeader.ascx" TagName="ListHeader" TagPrefix="uc2" %>
<%@ Register Src="Component/SubListHeader.ascx" TagName="SubListHeader" TagPrefix="uc1" %>
<%@ Register Src="~/eRecordComponent/DokumentumVizualizerComponent.ascx" TagName="DokumentumVizualizerComponent" TagPrefix="uc" %>
<%@ Register Src="~/eRecordComponent/VisszakuldesPopup.ascx" TagName="VisszakuldesPopup" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <uc2:ListHeader ID="ListHeader1" runat="server" />
    <asp:UpdatePanel ID="ErrorUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--Hiba megjelenites--%>
            <eUI:eErrorPanel ID="EErrorPanel1" runat="server">
            </eUI:eErrorPanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc3:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <%--/Hiba megjelenites--%>

    <%--HiddenFields--%>
    <asp:HiddenField
        ID="HiddenField1" runat="server" />
    <asp:HiddenField
        ID="MessageHiddenField" runat="server" />
    <%--/HiddenFields--%>

    <%--Tablazat / Grid--%>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 0px;">
                <asp:ImageButton runat="server" ID="IrattarbaVetelCPEButton" ImageUrl="images/hu/Grid/minus.gif" OnClientClick="return false;" /></td>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:UpdatePanel ID="IrattarbaVetelUpdatePanel" runat="server" OnLoad="IrattarbaVetelUpdatePanel_Load">
                    <ContentTemplate>
                        <ajaxToolkit:CollapsiblePanelExtender ID="IrattarbaVetelCPE" runat="server" TargetControlID="Panel1"
                            CollapsedSize="20" Collapsed="False" ExpandControlID="IrattarbaVetelCPEButton"
                            CollapseControlID="IrattarbaVetelCPEButton" ExpandDirection="Vertical" AutoCollapse="false"
                            AutoExpand="false" CollapsedImage="images/hu/Grid/plus.gif" ExpandedImage="images/hu/Grid/minus.gif"
                            ImageControlID="IrattarbaVetelCPEButton" ExpandedSize="0" ExpandedText="Irattári tételszámok listája"
                            CollapsedText="Kézbesítési tételek listája">
                        </ajaxToolkit:CollapsiblePanelExtender>
                        <asp:Panel ID="Panel1" runat="server">
                            <table style="width: 98%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                                        <uc:DokumentumVizualizerComponent runat="server" ID="DokumentumVizualizerComponent" />
                                        <uc:VisszakuldesPopup runat="server" ID="VisszakuldesPopup" />
                                        <asp:GridView ID="IrattarbaVetelGridView" runat="server" CellPadding="0" CellSpacing="0"
                                            BorderWidth="1" GridLines="Both" AllowPaging="True" PagerSettings-Visible="false"
                                            OnRowCommand="IrattarbaVetelGridView_RowCommand" OnPreRender="IrattarbaVetelGridView_PreRender"
                                            OnSorting="IrattarbaVetelGridView_Sorting" OnRowDataBound="IrattarbaVetelGridView_RowDataBound"
                                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id">
                                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                                            <HeaderStyle CssClass="GridViewHeaderStyle" />

                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                                    <HeaderTemplate>
                                                        <div class="DisableWrap">
                                                            <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                                                AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                                            &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:CommandField ButtonType="Image" ItemStyle-CssClass="GridViewSelectRowImage"
                                                    HeaderStyle-CssClass="GridViewSelectRowImage" ShowSelectButton="True" SelectText="<%$Resources:List,AlternateText_RowSelectButton%>"
                                                    SelectImageUrl="~/images/hu/Grid/3Drafts.gif">
                                                    <HeaderStyle Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:CommandField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="Sz" HeaderText="Sz" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Image ID="SzereltImage" AlternateText="Szerelés" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/szerelt.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="Csatolmany" HeaderText="Csat." Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Image ID="CsatoltImage" AlternateText="Csatolás" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolt.gif" Style="cursor: pointer"
                                                            onmouseover="Utility.UI.SetElementOpacity(this,0.7);" onmouseout="Utility.UI.SetElementOpacity(this,1);" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="Csny" HeaderText="Csny." Visible="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="CsatolmanyImage" AlternateText="Csatolmány" Height="20px" Width="25px"
                                                            runat="server" ImageUrl="~/images/hu/ikon/csatolmany.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage"
                                                    HeaderStyle-Width="25px" AccessibleHeaderText="F" HeaderText="F." Visible="false">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="FeladatImage" AlternateText="Kezelési feljegyzés" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="IktatoHely" AccessibleHeaderText="Iktatohely" HeaderText="Iktatókönyv" SortExpression="EREC_IraIktatokonyvek.Iktatohely"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Foszam" AccessibleHeaderText="Foszam" HeaderText="Főszám" SortExpression="EREC_UgyUgyiratok.Foszam"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="70px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ev" AccessibleHeaderText="Ev" HeaderText="Év" SortExpression="EREC_IraIktatokonyvek.Ev"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Foszam_Merge" AccessibleHeaderText="Foszam_Merge" HeaderText="Iktatószám" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Targy" AccessibleHeaderText="Targy" HeaderText="Tárgy" SortExpression="EREC_UgyUgyiratok.Targy">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Felelos_Nev" AccessibleHeaderText="Felelos_Nev" HeaderText="Kezelő" SortExpression="Csoportok_FelelosNev.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NevSTR_Ugyindito" AccessibleHeaderText="NevSTR_Ugyindito" HeaderText="Ügyindító" SortExpression="EREC_UgyUgyiratok.NevSTR_Ugyindito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyindito_Cim" AccessibleHeaderText="Ugyindito_Cim" HeaderText="Ügyindító címe" Visible="false" SortExpression="EREC_UgyUgyiratok.CimStr_Ugyindito">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BARCODE" Visible="false" AccessibleHeaderText="BARCODE" HeaderText="Vonalkód" SortExpression="EREC_UgyUgyiratok.BARCODE">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                 <%--LZS - BUG_7099 - obsolate
                                            Ügyiratok megjegyzés (EREC_UgyUgyiratok.Note) mező gridre helyezése--%>
                                            <%-- <asp:BoundField DataField="Note" HeaderText="<%$Forditas:BoundField_Megjegyzes|Megjegyzés%>" SortExpression="EREC_UgyUgyiratok.Note">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                            </asp:BoundField>--%>
                                            <%----LZS - BUG_11081--%>
                                            <asp:TemplateField AccessibleHeaderText="Megjegyzes" HeaderText="<%$Forditas:BoundField_Megjegyzes|Megjegyzés%>" SortExpression="">
                                                <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="labelNote" runat="server"
                                                        Text='<%# NoteJSONParser( Eval("Note") as string ) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:BoundField DataField="Merge_IrattariTetelszam" AccessibleHeaderText="ITSZ" HeaderText="Itsz." SortExpression="EREC_IraIrattariTetelek.IrattariTetelszam">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" Wrap="false" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Ugyintezo_Nev" AccessibleHeaderText="Ugyintezo_Nev" HeaderText="Ügyintéző" SortExpression="Csoportok_UgyintezoNev.Nev" Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Allapot_Nev" AccessibleHeaderText="Allapot" HeaderText="Állapot" SortExpression="AllapotKodTarak.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UgyintezesModja_Nev" AccessibleHeaderText="UgyintezesModja_Nev" HeaderText="Küldemény típusa" Visible="false" SortExpression="EREC_UgyUgyiratok.UgyintezesModja">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LetrehozasIdo" AccessibleHeaderText="LetrehozasIdo_Rovid" HeaderText="Ikt.&nbsp;dátum" SortExpression="EREC_UgyUgyiratok.LetrehozasIdo">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Hatarido" AccessibleHeaderText="Hatarido" HeaderText="Határidõ" SortExpression="EREC_UgyUgyiratok.Hatarido">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Lezarasdat" AccessibleHeaderText="LezarasDat" HeaderText="Lezárás" SortExpression="EREC_UgyUgyiratok.Lezarasdat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Elintezesdat" AccessibleHeaderText="Elintezesdat" HeaderText="Ügyirat elintézési idõpontja" SortExpression="EREC_UgyUgyiratok.Elintezesdat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <%--TODO: ide majd egy kis ikon kell, ami fölé húzva megjelenik az õrzõ neve--%>
                                                </asp:BoundField>
                                                <%-- CR#2197: választható, alapból rejtett dátummezõk START --%>
                                                <%-- jelenleg tárolt eljárásban formázva a dátum adatok, egyébként pl. 
                            DataFormatString="{0:yyyy.MM.dd}" HtmlEncodeFormatString="true" kellene --%>
                                                <asp:BoundField DataField="SztornirozasDat" AccessibleHeaderText="SztornirozasDat" HeaderText="Sztornó dátum" Visible="false" SortExpression="EREC_UgyUgyiratok.SztornirozasDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SkontrobaDat" AccessibleHeaderText="SkontrobaDat" HeaderText="Határidõbe tétel idõpontja" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontrobaDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SkontroVege_Rovid" AccessibleHeaderText="SkontroVege_Rovid" HeaderText="Határidõbe tétel lejárata" Visible="false" SortExpression="EREC_UgyUgyiratok.SkontroVege">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IrattarbaKuldDatuma" AccessibleHeaderText="IrattarbaKuldDatuma" HeaderText="Irattárba küldés" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaKuldDatuma">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IrattarbaVetelDat" AccessibleHeaderText="IrattarbaVetelDat" HeaderText="Irattárba vétel" Visible="false" SortExpression="EREC_UgyUgyiratok.IrattarbaVetelDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KolcsonzesDatuma" AccessibleHeaderText="KolcsonzesDatuma" HeaderText="Kölcsönzés kezdete" Visible="false" SortExpression="EREC_IrattariKikero.KikerKezd">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="KolcsonzesiHatarido" AccessibleHeaderText="KolcsonzesiHatarido" HeaderText="Kölcsönzés határideje" Visible="false" SortExpression="EREC_IrattariKikero.KikerVege">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MegorzesiIdoVege" AccessibleHeaderText="MegorzesiIdoVege" HeaderText="Õrzési idõ vége" Visible="false" SortExpression="EREC_UgyUgyiratok.MegorzesiIdoVege">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FelulvizsgalatDat" AccessibleHeaderText="FelulvizsgalatDat" HeaderText="Felülvizsgálat" Visible="false" SortExpression="EREC_UgyUgyiratok.FelulvizsgalatDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SelejtezesDat" AccessibleHeaderText="SelejtezesDat" HeaderText="Selejtezve / Levéltárba" Visible="false" SortExpression="EREC_UgyUgyiratok.SelejtezesDat">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <%-- CR#2197: választható, alapból rejtett dátummezõk END --%>
                                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" AccessibleHeaderText="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="Csoportok_OrzoNev.Nev">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField AccessibleHeaderText="Jelleg" HeaderText="Jelleg" SortExpression="EREC_UgyUgyiratok.Jelleg"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="linkJelleg" runat="server" Text="J." ToolTip="Jelleg" CommandName="Sort"
                                                            CommandArgument="EREC_UgyUgyiratok.Jelleg" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelJelleg" runat="server" Text='<%#GetJellegLabel(Eval("Jelleg"))%>'
                                                            ToolTip='<%#GetJellegToolTip(Eval("Jelleg"))%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField AccessibleHeaderText="AlszamDb" HeaderText="Alszám/Db" SortExpression="EREC_UgyUgyiratok.UtolsoAlszam"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="30px" />
                                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="labelUtolsoAlszam" runat="server" Text='<%#String.Format("{0} / {1}",Eval("UtolsoAlszam"),Eval("IratSzam"))%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                                    <HeaderTemplate>
                                                        <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />

</asp:Content>
