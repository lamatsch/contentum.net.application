using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IrattarbaVetelList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    private String Startup = "";

    public string maxTetelszam = "0";

    private string AllIrattarIds
    {
        get
        {
            if (ViewState["AtmenetiIrattarIds"] == null)
            {
                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                Result result = service_csoportok.GetAllKezelhetoIrattar(execParam);
                if (result.IsError)
                {
                    //// hiba:
                    //ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                    //return String.Empty;
                }
                else
                {
                    #region CR3206 - Iratt�ri strukt�ra


                    string irattarIds = String.Empty;

                    if (result.GetCount > 0)
                    {
                        List<string> lstIrattarIds = new List<string>();
                        foreach (DataRow row in result.Ds.Tables[0].Rows)
                        {
                            string irattarId = row["Id"].ToString();
                            if (!String.IsNullOrEmpty(irattarId) && !lstIrattarIds.Contains(irattarId))
                            {
                                lstIrattarIds.Add(irattarId);
                            }
                        }
                        string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam.Clone()).Obj_Id;
                        lstIrattarIds.Remove(kozpontiIrattarId);
                        irattarIds = Search.GetSqlInnerString(lstIrattarIds.ToArray());
                    }
                    // elmentj�k ViewState-be
                    ViewState["AtmenetiIrattarIds"] = irattarIds;
                    #endregion
                }
            }
            if (ViewState["AtmenetiIrattarIds"] == null)
            {
                return String.Empty;
            }
            return ViewState["AtmenetiIrattarIds"].ToString();
        }

        set { ViewState["AtmenetiIrattarIds"] = value; }
    }

    private bool IsIrattarozasHelyFelhasznaloAltal
    {
        get { return Rendszerparameterek.GetBoolean(Page, "IRATTAROZAS_HELY_FELH_ALTAL", true); }
    }

    private IEnumerable<string> GetFelhasznaloAtmenetiIrattaraiIds()
    {
        var res = new List<string>();
        var service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = service_csoportok.GetAllAtmenetiIrattar(execParam);
        if (result.IsError)
        {
            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            return res;
        }
        else
        {
            foreach (DataRow row in result.Ds.Tables[0].Rows)
            {
                var irattarId = row["Id"].ToString(); // =Partner_id_kapcsolt, set in GetAllAtmenetiIrattar
                var isSajatSzervezet = row["Id"].ToString().Equals(execParam.FelhasznaloSzervezet_Id, StringComparison.CurrentCultureIgnoreCase);
                if (!String.IsNullOrEmpty(irattarId) && isSajatSzervezet && !res.Contains(irattarId))
                {
                    res.Add(irattarId);
                }
            }

            string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam.Clone()).Obj_Id;
            res.Remove(kozpontiIrattarId);
        }
        return res;
    }

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);

        if (Startup != Constants.Startup.FromAtmenetiIrattar && Startup != Constants.Startup.FromKozpontiIrattar)
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }

        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AtmenetiIrattarAtvetel");
        }
        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KozpontiIrattarAtvetel");
        }

        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);

        //IraIktatoKonyvekSubListHeader.RowCount_Changed += new EventHandler(IraIktatoKonyvekSubListHeader_RowCount_Changed);

        //IraIktatoKonyvekSubListHeader.ErvenyessegFilter_Changed += new EventHandler(IraIktatoKonyvekSubListHeader_ErvenyessegFilter_Changed);

        ListHeader1.PagingButtonsClick += new EventHandler(ListHeader1_PagingChanged);

        HataridosFeladatok.SetFeladatJelzoColumnVisibility(Page, IrattarbaVetelGridView);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.HeaderLabel = Resources.List.AtmenetiIrattarbaVetelListHeaderTitle;
        }

        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.HeaderLabel = Resources.List.KozpontiIrattarbaVetelListHeaderTitle;
        }

        //Sorrend fontos!!! 1. Custom Session Name 2. SerachObject Type (ez a template miatt kell)        
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch;
        }
        else
        {
            ListHeader1.CustomSearchObjectSessionName = Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch;
        }

        ListHeader1.NumericSearchVisible = false;

        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokSearch);
        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.NewEnabled = false;
        ListHeader1.ModifyEnabled = false;
        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = false;
        ListHeader1.PrintVisible = true;
        ListHeader1.SSRSPrintVisible = true;
        ListHeader1.UgyiratTerkepVisible = true;

        // jobboldali gombok l�that�s�ga:
        ListHeader1.VisszakuldesVisible = Rendszerparameterek.GetBoolean(UI.SetExecParamDefault(Page), Rendszerparameterek.VISSZAKULDES_IRATTARBOL_ENABLED, false); //true;
        ListHeader1.LockVisible = true;
        ListHeader1.UnlockVisible = true;

        ListHeader1.UgyiratIrattarAtvetelVisible = true;

        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IrattarbaVetelSearch.aspx", Constants.Startup.StartupName + "=" + Startup
            , Defaults.PopupWidth, Defaults.PopupHeight, IrattarbaVetelUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        if (ListHeader1.NumericSearchVisible == true)
        {
            // sz�tv�lasztjuk, mert a NumericSearch m�shonnan is h�vhat� ezekkel a Startupokkal,
            // �s akkor m�s Sessionbe menti a Search objektumot, mint ahol keress�k...
            switch (Startup)
            {
                case Constants.Startup.FromAtmenetiIrattar:
                    ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromAtmenetiIrattarbaVetel,
                        "", Defaults.PopupWidth, Defaults.PopupHeight, IrattarbaVetelUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                    break;
                case Constants.Startup.FromKozpontiIrattar:
                    ListHeader1.NumericSearchOnClientClick = JavaScripts.SetOnClientClick("UgyiratokNumericSearch.aspx?" + Constants.Startup.StartupName + "=" + Constants.Startup.FromKozpontiIrattarbaVetel,
                        "", Defaults.PopupWidth, Defaults.PopupHeight, IrattarbaVetelUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                    break;
                default:
                    break;
            }
        }

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IrattarbaVetelGridView.ClientID);
        #region CR3206 - Iratt�ri strukt�ra
        //ListHeader1.UgyiratIrattarAtvetelOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.UgyiratIrattarAtvetelOnClientClick = "var count = getSelectedCheckBoxesCount('"
            + IrattarbaVetelGridView.ClientID + "','check'); "
            + " if (count==0) {alert('" + Resources.List.UI_NoSelectedItem + "'); return false;}"
            + " else if (count > " + maxTetelszam + ") {alert('" + Resources.List.UI_MaximumSelectedItemNumber + maxTetelszam + "\\n" + Resources.List.UI_CurrentSelectedItemNumber + "' + count); return false;}"
            + " else {" + JavaScripts.SetOnClientClickIrattarbaAtvetelConfirm("IrattarbaVetelGridView", "check") + " }";
        #endregion

        ListHeader1.PrintOnClientClick = JavaScripts.SetOnClientClickIFramePrintForm("IrattarbaVetelListajaPrintForm.aspx?" + Constants.Startup.StartupName + "=" + Startup);

        string column_v = "";
        for (int i = 0; i < IrattarbaVetelGridView.Columns.Count; i++)
        {
            if (IrattarbaVetelGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        switch (Startup)
        {
            case Constants.Startup.FromAtmenetiIrattar:
                ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarbaVetelSSRSAtmenetiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
                //bernat.laszlo added : Excel Export (Grid)
                ListHeader1.ExportVisible = true;
                ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
                ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattar" + CommandName.ExcelExport);
                //bernat.laszlo eddig
                break;
            case Constants.Startup.FromKozpontiIrattar:
                ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarbaVetelSSRSKozpontiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
                //bernat.laszlo added : Excel Export (Grid)
                ListHeader1.ExportVisible = true;
                ScriptManager1.RegisterPostBackControl(ListHeader1.ExportButton);
                ListHeader1.ExportEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattar" + CommandName.ExcelExport);
                //bernat.laszlo eddig
                break;
        }

        ListHeader1.VisszakuldesOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(IrattarbaVetelGridView.ClientID);
        ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(IrattarbaVetelGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IrattarbaVetelGridView.ClientID);
        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IrattarbaVetelUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IrattarbaVetelGridView;

        /* Breaked Records */
        string CustomSessionName = null;
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            CustomSessionName = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch;
        }
        else // KozpontiIrattar
        {
            CustomSessionName = Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch;
        }

        if (!String.IsNullOrEmpty(CustomSessionName))
        {
            /* Breaked Records */
            Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratokSearch(), CustomSessionName);
        }
        else
        {
            /* Breaked Records */
            Search.SetIdsToSearchObject(Page, "Id", new EREC_UgyUgyiratokSearch());
        }

        if (!IsPostBack) IrattarbaVetelGridViewBind();

        ui.SetClientScriptToGridViewSelectDeSelectButton(IrattarbaVetelGridView);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);

        //LZS - BUG_13860
        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IrattarbaVetelGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.UgyiratokListColumnNames().GetType()).ToCustomString();
        Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IrattarbaVetelSSRS] = visibilityState;

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);
        ListHeader1.UgyiratTerkepEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.View);

        ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);
        ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);

        ListHeader1.SSRSPrintEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratSSRSPrint");

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IrattarbaVetelGridView);
            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);

        }

        //LZS - BUG_13860
        var visibilityState = new Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState().LoadFromGrid(IrattarbaVetelGridView, new Contentum.eRecord.BaseUtility.GridViewColumns.UgyiratokListColumnNames().GetType()).ToCustomString();
        Page.Session[Contentum.eRecord.BaseUtility.GridViewColumnVisibilityState.Constants.SessionVariables.IrattarbaVetelSSRS] = visibilityState;


        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            ListHeader1.UgyiratIrattarAtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtvetel");
            ListHeader1.VisszakuldesEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtvetel"); // egyel�re az �tv�tel jog�hoz k�tj�k
        }
        if (Startup == Constants.Startup.FromKozpontiIrattar)
        {
            ListHeader1.UgyiratIrattarAtvetelEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarAtvetel");
            ListHeader1.VisszakuldesEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarAtvetel"); // egyel�re az �tv�tel jog�hoz k�tj�k
        }

        string column_v = "";
        for (int i = 0; i < IrattarbaVetelGridView.Columns.Count; i++)
        {
            if (IrattarbaVetelGridView.Columns[i].Visible)
            {
                column_v = column_v + "1";
            }
            else
            {
                column_v = column_v + "0";
            }
        }

        switch (Startup)
        {
            case Constants.Startup.FromAtmenetiIrattar:
                ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarbaVetelSSRSAtmenetiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
                break;
            case Constants.Startup.FromKozpontiIrattar:
                ListHeader1.SSRSPrintOnClientClick = "javascript:window.open('IrattarbaVetelSSRSKozpontiIrattar.aspx?" + QueryStringVars.Filter + "=" + column_v + "')";
                break;
        }

        // TODO: kell meg egy gomb: atadas kozponti irattarba, funkcio:"AtmenetiIrattarAtadasKozpontiIrattarba"
    }

    #endregion


    #region Master List

    protected void IrattarbaVetelGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IrattarbaVetelGridView", ViewState, "EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IrattarbaVetelGridView", ViewState);

        IrattarbaVetelGridViewBind(sortExpression, sortDirection);
    }

    protected void IrattarbaVetelGridViewBind(String SortExpression, SortDirection SortDirection)
    {
        DokumentumVizualizerComponent.ClearDokumentElements();

        EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        //EREC_IrattarbaVetelSearch search = (EREC_IrattarbaVetelSearch)Search.GetSearchObject(Page, new EREC_IrattarbaVetelSearch());
        //search.OrderBy = Search.GetOrderBy("IrattarbaVetelGridView", ViewState, SortExpression, SortDirection);
        EREC_UgyUgyiratokSearch search = null;
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                , Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch);
            //#region CR3206 - Iratt�ri strukt�ra
            //string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam.Clone()).Obj_Id;
            //search.Csoport_Id_Felelos.NotEquals(kozpontiIrattarId);
            //#endregion
        }
        else
        {
            search = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                , Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch);
        }
        search.OrderBy = Search.GetOrderBy("IrattarbaVetelGridView", ViewState, SortExpression, SortDirection);
        search.TopRow = UI.GetTopRow(Page);

        /*
        // Csak az irattarba kuldott teletelek lathatoak:
        search.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;
        search.Allapot.Value = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
        search.Allapot.Group = "99";
        search.Allapot.GroupOperator = Query.Operators.or;

        //szereltek kezel�se
        search.Manual_Szereles_Alatt.Value = KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott;
        search.Manual_Szereles_Alatt.Operator = Contentum.eQuery.Query.Operators.equals;
        search.Manual_Szereles_Alatt.Group = "99";
        search.Manual_Szereles_Alatt.GroupOperator = Query.Operators.or;
        */

        // nem jelen�tj�k meg a szerelteket, mert a t�meges �tad�st zavarj�k (CR#1517)
        // (a szereltek egy�tt mozognak a sz�l�j�kkel, �nmagukban nem mozgathat�k)
        search.Allapot.Filter(KodTarak.UGYIRAT_ALLAPOT.Irattarba_kuldott);

        // TODO: kezelni kell az atmeneti es kozponti irattarat:
        switch (Startup)
        {
            case Constants.Startup.FromAtmenetiIrattar:
                // atmenetinel a kivetel a kozponti irattar
                if (String.IsNullOrEmpty(search.Csoport_Id_Felelos.Value))
                {
                    // CR#2220: Csak a saj�t �tmeneti iratt�raiba k�ld�tt t�teleket veheti be az �tmeneti iratt�rba
                    if (!string.IsNullOrEmpty(AllIrattarIds))
                    {
                        search.Csoport_Id_Felelos.Value = AllIrattarIds;
                        search.Csoport_Id_Felelos.Operator = Query.Operators.inner;
                    }
                }
                break;

            case Constants.Startup.FromKozpontiIrattar:
                ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());

                string kozpontiIrattarId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(ExecParam).Obj_Id;
                if (string.IsNullOrEmpty(kozpontiIrattarId))
                {
                    Result errorResult = ResultError.CreateNewResultWithErrorCode(50206);
                    ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, errorResult);
                    return;
                }

                search.Csoport_Id_Felelos.Filter(KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id);

                //if (!Rendszerparameterek.GetBoolean(ExecParam, "IRATTAROZAS_HELY_FELH_ALTAL", true))
                //{
                //    if (!string.IsNullOrEmpty(AllIrattarIds))
                //    {
                //        search.FelhasznaloCsoport_Id_Orzo.Value = AllIrattarIds;
                //        search.FelhasznaloCsoport_Id_Orzo.Operator = Query.Operators.inner;
                //    }
                //}
                break;
        }

        // Lapoz�s be�ll�t�sa:
        UI.SetPaging(ExecParam, ListHeader1);

        //Result res = service.GetAllWithExtension(ExecParam, search);
        Result res = service.GetAllWithExtensionAndJogosultak(ExecParam, search, true);

        UI.GridViewFill(IrattarbaVetelGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);

    }

    protected void ListHeader1_PagingChanged(object sender, EventArgs e)
    {
        JavaScripts.ResetScroll(Page, IrattarbaVetelCPE);
        IrattarbaVetelGridViewBind();
    }


    protected void IrattarbaVetelGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView_RowDataBound_SetSzerelesInfo(e);
        GridView_RowDataBound_SetCsatolasInfo(e);
        GridView_RowDataBound_SetVegyesInfo(e);
        UI.SetFeladatInfo(e, Constants.TableNames.EREC_UgyUgyiratok);
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    private void GridView_RowDataBound_SetVegyesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolmanyCount = String.Empty;

            if (drw.Row.Table.Columns.Contains("CsatolmanyCount") && drw["CsatolmanyCount"] != null)
            {
                csatolmanyCount = drw["CsatolmanyCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolmanyCount))
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    int count = 0;
                    Int32.TryParse(csatolmanyCount, out count);
                    if (count > 0)
                    {
                        //t�rolt elj�r�s viszg�lja
                        //#region BLG_577
                        //if (!UtilityCsatolmanyok.CheckRights_CsatolmanyokByDokumentum(Page, drw["Dokumentum_Id"].ToString()))
                        //{   /*NINCS JOGA*/
                        //    CsatolmanyImage.Visible = false;
                        //    return;
                        //}
                        //#endregion

                        CsatolmanyImage.Visible = true;
                        CsatolmanyImage.OnClientClick = "alert('" + Resources.Form.UI_NavigationError_UndefinedObject + "');";
                        CsatolmanyImage.AlternateText = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);
                        CsatolmanyImage.ToolTip = String.Format(Resources.Form.UI_CsatolmanyImage_ToolTip_WithCount, csatolmanyCount);

                        if (count == 1 && drw.Row.Table.Columns.Contains("Dokumentum_Id") && !String.IsNullOrEmpty(drw["Dokumentum_Id"].ToString()))
                        {
                            CsatolmanyImage.OnClientClick = String.Format("window.open('GetDocumentContent.aspx?id={0}'); return false;", drw["Dokumentum_Id"]);
                            DokumentumVizualizerComponent.AddDokumentElement(CsatolmanyImage.ClientID, drw["Dokumentum_Id"].ToString());
                        }
                        else
                        {
                            // set "link"
                            string id = "";

                            if (drw["Id"] != null)
                            {
                                id = drw["Id"].ToString();
                            }

                            string Azonosito = ""; // iktat�sz�m, a ReadableWhere fogja haszn�lni a dokumentumok list�j�n
                            if (drw.Row.Table.Columns.Contains("Foszam_Merge") && drw["Foszam_Merge"] != null)
                            {
                                Azonosito = drw["Foszam_Merge"].ToString();
                            }

                            if (!String.IsNullOrEmpty(id))
                            {
                                CsatolmanyImage.OnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                                    , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                                    + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.CsatolmanyokTab
                                    , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IrattarbaVetelUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
                            }
                        }
                    }
                    else
                    {
                        CsatolmanyImage.Visible = false;
                    }
                }

            }
            else
            {
                ImageButton CsatolmanyImage = (ImageButton)e.Row.FindControl("CsatolmanyImage");
                if (CsatolmanyImage != null)
                {
                    CsatolmanyImage.OnClientClick = "";
                    CsatolmanyImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetCsatolasInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string csatolsaCount = String.Empty;

            if (drw["CsatolasCount"] != null)
            {
                csatolsaCount = drw["CsatolasCount"].ToString();
            }

            if (!String.IsNullOrEmpty(csatolsaCount))
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");

                if (CsatoltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(csatolsaCount, out count);
                    if (count > 0)
                    {
                        CsatoltImage.Visible = true;
                        CsatoltImage.ToolTip = String.Format("Csatol�s: {0} darab", csatolsaCount);
                        if (drw["Id"] != null)
                        {
                            string ugyiratId = drw["Id"].ToString();
                            string onclick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                            , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + ugyiratId +
                            "&" + QueryStringVars.SelectedTab + "=" + "Csatolas"
                             , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IrattarbaVetelUpdatePanel.ClientID);
                            CsatoltImage.Attributes.Add("onclick", onclick);

                        }
                    }
                    else
                    {
                        CsatoltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image CsatoltImage = (Image)e.Row.FindControl("CsatoltImage");
                if (CsatoltImage != null)
                {
                    CsatoltImage.Visible = false;
                }

            }
        }
    }

    private void GridView_RowDataBound_SetSzerelesInfo(GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drw = (System.Data.DataRowView)e.Row.DataItem;
            string szereltCount = String.Empty;

            if (drw["SzereltCount"] != null)
            {
                szereltCount = drw["SzereltCount"].ToString();
            }

            string elokeszitettSzerelesCount_str = String.Empty;
            if (drw["ElokeszitettSzerelesCount"] != null)
            {
                elokeszitettSzerelesCount_str = drw["ElokeszitettSzerelesCount"].ToString();
            }

            /// ha van szerel�sre el�k�sz�tett �gyirat (UgyUgyiratId_Szulo ki van t�ltve erre, de nem szerelt m�g az �llapota),
            /// akkor a piros szerelt ikont rakjuk ki, am�gy a feket�t
            /// 
            int elokeszitettSzerelesCount = 0;
            if (!String.IsNullOrEmpty(elokeszitettSzerelesCount_str))
            {
                Int32.TryParse(elokeszitettSzerelesCount_str, out elokeszitettSzerelesCount);
            }

            if (elokeszitettSzerelesCount > 0)
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    SzereltImage.Visible = true;
                    SzereltImage.ImageUrl = "~/images/hu/ikon/szerelt_piros.gif";
                    SzereltImage.ToolTip = "El�k�sz�tett szerel�s";
                    SzereltImage.AlternateText = SzereltImage.ToolTip;
                }
            }
            else if (!String.IsNullOrEmpty(szereltCount))
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");

                if (SzereltImage != null)
                {
                    int count = 0;
                    Int32.TryParse(szereltCount, out count);
                    if (count > 0)
                    {
                        SzereltImage.Visible = true;
                    }
                    else
                    {
                        SzereltImage.Visible = false;
                    }
                }

            }
            else
            {
                Image SzereltImage = (Image)e.Row.FindControl("SzereltImage");
                if (SzereltImage != null)
                {
                    SzereltImage.Visible = false;
                }

            }
        }
    }

    protected void IrattarbaVetelGridView_PreRender(object sender, EventArgs e)
    {
        UI.GridViewSetScrollable(ListHeader1.Scrollable, IrattarbaVetelCPE);
        ListHeader1.RefreshPagerLabel();
        /*
        int prev_PageIndex = IrattarbaVetelGridView.PageIndex;

        IrattarbaVetelGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IrattarbaVetelGridView.PageCount;

        if (prev_PageIndex != IrattarbaVetelGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IrattarbaVetelCPE);
            IrattarbaVetelGridViewBind();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IrattarbaVetelCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IrattarbaVetelGridView);
         * */
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IrattarbaVetelGridViewBind();
    }

    protected void IrattarbaVetelGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IrattarbaVetelGridView, selectedRowNumber, "check");

            //            string id = (sender as GridView).DataKeys[selectedRowNumber].Value.ToString();
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               + "&" + Constants.Startup.StartupName + "=" + Startup
               , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IrattarbaVetelUpdatePanel.ClientID);

            // �gyiratt�rk�p View m�dban
            ListHeader1.UgyiratTerkepOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               + "&" + Constants.Startup.StartupName + "=" + Startup
               + "&" + QueryStringVars.SelectedTab + "=" + Constants.Tabs.UgyiratTerkepTab
               , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, IrattarbaVetelUpdatePanel.ClientID);


            #region St�tuszf�gg� ellen�rz�sek
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            execParam.Record_Id = id;

            EREC_UgyUgyiratokService service_ugyiratok = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();

            Result result_ugyiratGet = service_ugyiratok.Get(execParam);
            if (result_ugyiratGet.IsError)
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result_ugyiratGet);
                ErrorUpdatePanel.Update();
            }

            EREC_UgyUgyiratok ugyiratObj = (EREC_UgyUgyiratok)result_ugyiratGet.Record;

            Ugyiratok.Statusz ugyiratStatusz = Ugyiratok.GetAllapotByBusinessDocument(ugyiratObj);
            ErrorDetails errorDetail = null;

            #region Visszakuldheto
            bool visszakuldhetoIrattarbol = false;
            switch (Startup)
            {
                case Constants.Startup.FromAtmenetiIrattar:
                    // az esetleg felesleges h�v�sok elker�l�se miatt nem v�gezz�k el a teljes ellen�rz�st
                    //visszakuldhetoIrattarbol = Ugyiratok.CheckVisszakuldhetoIrattarbolWithFunctionRight(Page, statusz, out errorDetail))
                    visszakuldhetoIrattarbol = Ugyiratok.AtmenetiIrattarbolVisszakuldheto(execParam, ugyiratStatusz, out errorDetail);
                    break;
                case Constants.Startup.FromKozpontiIrattar:
                    // az esetleg felesleges h�v�sok elker�l�se miatt nem v�gezz�k el a teljes ellen�rz�st
                    //visszakuldhetoIrattarbol = Ugyiratok.CheckVisszakuldhetoIrattarbolWithFunctionRight(Page, statusz, out errorDetail))
                    visszakuldhetoIrattarbol = Ugyiratok.KozpontiIrattarbolVisszakuldheto(execParam, ugyiratStatusz, out errorDetail);
                    break;
            }

            if (visszakuldhetoIrattarbol)
            {
                VisszakuldesPopup.SetOnclientClickShowFunction(ListHeader1.VisszakuldesButton);
            }
            else
            {
                // Nem k�ldhet� vissza:
                ListHeader1.VisszakuldesOnClientClick = "alert('" + Resources.Error.UINemVisszakuldhetoTetel
                    + ResultError.GetErrorDetailMsgForAlert(errorDetail, Page)
                    + "'); return false;";
            }
            #endregion Visszakuldheto

            #endregion St�tuszf�gg� ellen�rz�sek

            //ListHeader1.UgyiratIrattarAtvetelOnClientClick = JavaScripts.SetOnClientClickIrattarbaAtvetelConfirm("IrattarbaVetelGridView", EventArgumentConst.refreshMasterList, "check");
        }
    }

    protected void IrattarbaVetelUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IrattarbaVetelGridViewBind();
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        //bernat.laszlo added: Excel Export (Grid)
        if (e.CommandName == CommandName.ExcelExport)
        {
            string browser = "";

            //LZS - browser detection
            if (Request.Browser.Type.Contains("Firefox"))
            {
                browser = "FF";
            }
            else
            {
                browser = "IE";
            }

            Contentum.eUtility.ExcelExport ex_Export = new Contentum.eUtility.ExcelExport();
            ExecParam exParam = UI.SetExecParamDefault(Page, new ExecParam());
            ex_Export.SaveGridView_ToExcel(exParam, IrattarbaVetelGridView, ListHeader1.HeaderLabel, Constants.ExcelHeaderType.Standard, browser);
        }
        //bernat.laszlo eddig
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Lock:
                LockSelectedIraUgyUgyiratRecords();
                IrattarbaVetelGridViewBind();
                break;
            case CommandName.Unlock:
                UnlockSelectedIraUgyUgyiratRecords();
                IrattarbaVetelGridViewBind();
                break;
            case CommandName.SendObjects:
                SendMailSelectedIrattarbaVetel();
                break;
            case CommandName.UgyiratIrattarAtvetel:
                AtvetelSelectedUgyirat();
                //IrattarbaVetelGridViewBind();
                break;
            case CommandName.Visszakuldes:
                {
                    String RecordId = UI.GetGridViewSelectedRecordId(IrattarbaVetelGridView);

                    ErrorDetails errorDetail;
                    bool visszakuldheto = Ugyiratok.CheckVisszakuldhetoIrattarbolWithFunctionRight(Page, RecordId, out errorDetail);

                    if (!visszakuldheto)
                    {
                        string js = String.Format("alert('{0}{1}');", Resources.Error.UINemVisszakuldhetoTetel, ResultError.GetErrorDetailMsgForAlert(errorDetail, Page));
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "NemKuldhetoVissza", js, true);
                    }
                    else
                    {
                        Ugyiratok.VisszakuldesIrattarbol(RecordId, VisszakuldesPopup.VisszakuldesIndoka, Page, EErrorPanel1, ErrorUpdatePanel);
                        IrattarbaVetelGridViewBind();
                    }
                }
                break;
        }
    }

    //kiv�lasztott elem atvetele
    private void AtvetelSelectedUgyirat()
    {
        #region  CR3206 - Iratt�ri strukt�ra
        List<string> lstGridViewSelectedRows = ui.GetGridViewSelectedRows(IrattarbaVetelGridView, EErrorPanel1, ErrorUpdatePanel);
        // elvileg nem biztos, hogy a kiv�lasztott sor egyben a bepip�lt sor is
        string js = string.Empty;
        if (UI.GetGridViewSelectedCheckBoxesCount(IrattarbaVetelGridView, "check") > 0)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (string s in ui.GetGridViewSelectedRows(IrattarbaVetelGridView, EErrorPanel1, ErrorUpdatePanel))
            {
                sb.Append(s + ",");
            }
            sb.Remove(sb.Length - 1, 1);

            if (String.IsNullOrEmpty(sb.ToString()))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(EErrorPanel1);
                return;
            }

            Session["SelectedUgyiratIds"] = sb.ToString();

            Session["SelectedBarcodeIds"] = null;
            Session["SelectedKuldemenyIds"] = null;
            Session["SelectedIratPeldanyIds"] = null;
            Session["SelectedDosszieIds"] = null;
            //ne okozzon gondot, ha kor�bban h�vtuk a t�k visszav�tel funkci�t
            Session["TUKVisszavetel"] = null;

            js = JavaScripts.SetOnClientClickWithTimeout("AtvetelForm.aspx", QueryStringVars.Command + "=" + CommandName.UgyiratIrattarAtvetel, Defaults.PopupWidth_Max, Defaults.PopupHeight, IrattarbaVetelGridView.ClientID, EventArgumentConst.refreshMasterList).TrimEnd("return false;".ToCharArray());

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "selectedUgyiratAtadas", js, true);
        }
        #endregion CR3206 - Iratt�ri strukt�ra
    }

    private void LockSelectedIraUgyUgyiratRecords()
    {
        LockManager.LockSelectedGridViewRecords(IrattarbaVetelGridView, "EREC_UgyUgyiratok"
            , "UgyiratLock", "UgyiratForceLock"
             , Page, EErrorPanel1, ErrorUpdatePanel);
    }

    private void UnlockSelectedIraUgyUgyiratRecords()
    {
        LockManager.UnlockSelectedGridViewRecords(IrattarbaVetelGridView, "EREC_UgyUgyiratok"
            , "UgyiratLock", "UgyiratForceLock"
            , Page, EErrorPanel1, ErrorUpdatePanel);
    }


    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIrattarbaVetel()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IrattarbaVetelGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IrattarbaVetel", Constants.Startup.StartupName + "=" + Startup);
        }
    }

    protected void IrattarbaVetelGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IrattarbaVetelGridViewBind(e.SortExpression, UI.GetSortToGridView("IrattarbaVetelGridView", ViewState, e.SortExpression));
    }

    protected void GetJelleg(object oJelleg, out string JellegLabel, out string JellegTooltip)
    {
        string jelleg = (oJelleg ?? String.Empty) as string;

        switch (jelleg)
        {
            case KodTarak.UGYIRAT_JELLEG.Elektronikus:
                JellegLabel = "E";
                JellegTooltip = "Elektronikus";
                break;
            case KodTarak.UGYIRAT_JELLEG.Papir:
                JellegLabel = "P";
                JellegTooltip = "Pap�r";
                break;
            case KodTarak.UGYIRAT_JELLEG.Vegyes:
                JellegLabel = "V";
                JellegTooltip = "Vegyes";
                break;
            default:
                goto case KodTarak.UGYIRAT_JELLEG.Papir;
        }
    }

    protected string GetJellegLabel(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return label;
    }

    protected string GetJellegToolTip(object oJelleg)
    {
        string label;
        string tooltip;
        GetJelleg(oJelleg, out label, out tooltip);
        return tooltip;
    }

    #endregion

    public string NoteJSONParser(string noteJSONString)
    {
        string result = "";
        if (!string.IsNullOrEmpty(noteJSONString))
        {
            Note_JSON note;

            try
            {
                note = JsonConvert.DeserializeObject<Note_JSON>(noteJSONString);
            }
            catch (Exception)
            {
                note = null;
            }

            if (note != null)
                result = note.Megjegyzes;
        }

        return result;
    }
}
