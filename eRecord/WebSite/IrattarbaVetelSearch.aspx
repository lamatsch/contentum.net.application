﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IrattarbaVetelSearch.aspx.cs" Inherits="IrattarbaVetelSearch" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc6" %>
<%@ Register Src="Component/SzamIntervallum_SearchFormControl.ascx" TagName="SzamIntervallum_SearchFormControl"
    TagPrefix="uc14" %>
<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc5" %>
<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc3" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc13" %>
<%@ Register Src="Component/EvIntervallum_SearchFormControl.ascx" TagName="EvIntervallum_SearchFormControl"
    TagPrefix="uc3" %>    
<%@ Register Src="eRecordComponent/IraIktatoKonyvekDropDownList.ascx" TagName="IraIktatoKonyvekDropDownList"
    TagPrefix="uc7" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="~/Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="cup" %>
<%@ Register Src="~/Component/FelhasznaloTextBox.ascx" TagPrefix="cup" TagName="FelhasznaloTextBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	 <uc1:SearchHeader ID="SearchHeader1" runat="server"></uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <eUI:eFormPanel ID="EFormPanel1" runat="server" Width="95%">
                    <table cellspacing="0" cellpadding="0" width="99%">
                        <tr class="urlapSor" id="tr_AutoGeneratePartner_CheckBox" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="Irattár:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc6:CsoportTextBox ID="CsoportTextBox1" runat="server" Validate="false" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelEv" runat="server" CssClass="mrUrlapCaption" Text="Év:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:EvIntervallum_SearchFormControl ID="Iktatokonyv_Ev_EvIntervallum_SearchFormControl"
                                    runat="server" />
                            </td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Iktatókönyv:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc7:IraIktatoKonyvekDropDownList id="IraIktatoKonyvekDropDownList1" runat="server"
                                EvIntervallum_SearchFormControlId="Iktatokonyv_Ev_EvIntervallum_SearchFormControl" Mode="Iktatokonyvek" IsMultiSearchMode="true"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label14" runat="server" Text="Fõszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc14:SzamIntervallum_SearchFormControl ID="UgyDarab_Foszam_SzamIntervallum_SearchFormControl"
                                    runat="server"></uc14:SzamIntervallum_SearchFormControl>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Irattári tételszám:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <uc3:IraIrattariTetelTextBox ID="IraIrattariTetelTextBox1" runat="server" Validate="false"
                                    SearchMode="true" />
                            </td>
                        </tr>
                         <%--LZS - BUG_7099
                        Ügyirathoz tartozó megjegyzés mező. EREC_UgyUgyiratok.Note--%>
                        <tr id="trMegjegyzes" runat="server" visible="true" class="urlapSor_kicsi">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="lblMegjegyzes" runat="server" Text="<%$Forditas:lblMegjegyzes|Megjegyzés:%>"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox ID="txtMegjegyzes" runat="server" CssClass="mrUrlapInputFTS"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Ügyintézõ:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <cup:FelhasznaloTextBox ID="felhasznaloTextBoxUgyintezo" runat="server" SearchMode="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" Id="trIratHelye" runat="server" Visible="false">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="Irat helye:"></asp:Label>
                            </td>
                            <td class="mrUrlapMezo">
                                <asp:TextBox runat="server" Id="tbIratHelye" CssClass="mrUrlapInput" ReadOnly="true"/>
                                <asp:HiddenField ID="hfFelhasznaloCsoport_Id_Orzo_Value" runat="server" />
                                <asp:HiddenField ID="hfFelhasznaloCsoport_Id_Orzo_Operator" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="2">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />
                            </td>
                        </tr>
                    </table>
                </eUI:eFormPanel>
                <table cellpadding="0" cellspacing="0"">
                    <tr class="urlapSor">
                        <td colspan="2">
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
