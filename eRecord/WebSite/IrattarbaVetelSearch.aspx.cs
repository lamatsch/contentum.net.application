﻿using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

// a teljes kód kidolgozandó!!!

public partial class IrattarbaVetelSearch : Contentum.eUtility.UI.PageBase
{
    private const string kcs_IKTATOSZAM_KIEG = "IKTATOSZAM_KIEG";
    private const string kcs_UGYIRAT_ALLAPOT = "UGYIRAT_ALLAPOT";
    private const string kcs_SURGOSSEG = "SURGOSSEG";

    private String Startup = "";

    #region Base Page

    protected void Page_Init(object sender, EventArgs e)
    {
        Startup = Request.QueryString.Get(QueryStringVars.Startup);

        SearchHeader1.HeaderTitle = Resources.Search.UgyUgyiratSearchHeaderTitle;
        if (Startup == Constants.Startup.FromAtmenetiIrattar)
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch;
        }
        else
        {
            SearchHeader1.CustomTemplateTipusNev = Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch;
        }
        SearchHeader1.TemplateObjectType = typeof(EREC_UgyUgyiratokSearch);

        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);

        CsoportTextBox1.Filter = Constants.FilterType.Irattar;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick += new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick += new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {

            EREC_UgyUgyiratokSearch searchObject = null;
            if (Startup == Constants.Startup.FromAtmenetiIrattar)
            {
                if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch))
                {
                    searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                         , Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch);
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }
            else
            {
                if (Search.IsSearchObjectInSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch))
                {
                    searchObject = (EREC_UgyUgyiratokSearch)Search.GetSearchObject_CustomSessionName(Page, new EREC_UgyUgyiratokSearch(true)
                         , Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch);
                }
                else
                {
                    searchObject = GetDefaultSearchObject();
                }
            }

            LoadComponentsFromSearchObject(searchObject);

            // Ha nem lett kitöltve az irattár mezõ:
            // Átmeneti irattárba adásnál megvizsgáljuk, hány darab átmeneti irattár van;
            // Ha csak egy, beírjuk ezt.
            if (Startup == Constants.Startup.FromAtmenetiIrattar && String.IsNullOrEmpty(CsoportTextBox1.Id_HiddenField))
            {
                Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                Result result = service_csoportok.GetAllIrattar(execParam);
                if (!String.IsNullOrEmpty(result.ErrorCode))
                {
                    // hiba:
                    ResultError.DisplayResultErrorOnErrorPanel(SearchHeader1.ErrorPanel, result);
                }
                else
                {
                    if (result.Ds != null && result.Ds.Tables[0].Rows.Count == 1)
                    {
                        // Egy irattár van, ez lesz a default
                        System.Data.DataRow row = result.Ds.Tables[0].Rows[0];
                        string irattarId = row["Id"].ToString();
                        string irattarNev = row["Nev"].ToString();

                        CsoportTextBox1.Id_HiddenField = irattarId;
                        CsoportTextBox1.Text = irattarNev;
                    }
                }
            }

            /* Határidõ elõtt x nap csak akkor tölthetõ ha 'Határidõben' és 'Határidõn túl' unchecked */
            //CheckBoxListHatarido.Attributes["onclick"] = "Javascript: " +
            //    " document.getElementById('RequiredNumberBoxHataridoElott').disabled = " +
            //    " (document.getElementById('" + CheckBoxListHatarido.ClientID + "_0').checked || " +
            //    " document.getElementById('" + CheckBoxListHatarido.ClientID + "_1').checked); " +
            //    " return true;";


        }
    }
    #endregion

    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = null;

        EREC_IraIktatoKonyvekSearch schIktatatokonyv = null;

        if (searchObject != null) erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)searchObject;

        if (erec_UgyUgyiratokSearch != null)
        {
            switch (Startup)
            {
                case Constants.Startup.FromAtmenetiIrattar:
                    // Ha a listán beállítjuk, hogy NEM központi irattár a felelõs, az ne látszódjon:
                    if (erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Operator != Query.Operators.notequals)
                    {
                        CsoportTextBox1.Id_HiddenField = erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value.Trim(new char[] { '\'' });
                        CsoportTextBox1.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                    }
                    CsoportTextBox1.ReadOnly = false;
                    break;
                case Constants.Startup.FromKozpontiIrattar:
                    // TODO: kene a kozponti irattar csoport id-ja!
                    ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
                    CsoportTextBox1.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execparam).Obj_Id;
                    CsoportTextBox1.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);
                    CsoportTextBox1.ReadOnly = true;

                    //LZS - BUG_7099
                    //Megjegyzés mező kiiratása a kereső felületre.
                    if (erec_UgyUgyiratokSearch.fts_note != null)
                    {
                        txtMegjegyzes.Text = erec_UgyUgyiratokSearch.fts_note.Filter;
                    }
                    else
                    {
                        txtMegjegyzes.Text = "";
                    }
                    break;
            }


            schIktatatokonyv = erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch;

            if (schIktatatokonyv != null)
            {
                Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                                         schIktatatokonyv.Ev);

                IraIktatoKonyvekDropDownList1.FillAndSelectValue_SetMegKulJelzesToValues(Constants.IktatoErkezteto.Iktato
                     , Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvTol, Iktatokonyv_Ev_EvIntervallum_SearchFormControl.EvIg
                     , true, false, IktatoKonyvek.GetIktatokonyvValue(schIktatatokonyv), SearchHeader1.ErrorPanel);
            }

            UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetComponentFromSearchObjectFields(
                 erec_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

            IraIrattariTetelTextBox1.Id_HiddenField = erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value;
            IraIrattariTetelTextBox1.SetIraIrattariTetelTextBoxById(SearchHeader1.ErrorPanel);

            if (!IsPostBack)
            {
                //template-es szűrés
                hfFelhasznaloCsoport_Id_Orzo_Value.Value = erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value;
                hfFelhasznaloCsoport_Id_Orzo_Operator.Value = erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Operator;
                SetIratHelye();
            }

        }


    }

    private EREC_UgyUgyiratokSearch SetSearchObjectFromComponents()
    {
        EREC_UgyUgyiratokSearch erec_UgyUgyiratokSearch = (EREC_UgyUgyiratokSearch)SearchHeader1.TemplateObject;
        if (erec_UgyUgyiratokSearch == null)
        {
            erec_UgyUgyiratokSearch = new EREC_UgyUgyiratokSearch(true);
        }

        if (!String.IsNullOrEmpty(CsoportTextBox1.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Value = CsoportTextBox1.Id_HiddenField;
            erec_UgyUgyiratokSearch.Csoport_Id_Felelos.Operator = Query.Operators.equals;
        }

        EREC_IraIktatoKonyvekSearch schIktatatokonyv = erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch;

        if (schIktatatokonyv != null)
        {
            if (!String.IsNullOrEmpty(IraIktatoKonyvekDropDownList1.SelectedValue))
            {
                IraIktatoKonyvekDropDownList1.SetSearchObject(schIktatatokonyv);
            }

            Iktatokonyv_Ev_EvIntervallum_SearchFormControl.SetSearchObjectFields(schIktatatokonyv.Ev);

            erec_UgyUgyiratokSearch.Extended_EREC_IraIktatoKonyvekSearch = schIktatatokonyv;

        }

        UgyDarab_Foszam_SzamIntervallum_SearchFormControl.SetSearchObjectFields(
             erec_UgyUgyiratokSearch.Extended_EREC_UgyUgyiratdarabokSearch.Foszam);

        if (!String.IsNullOrEmpty(IraIrattariTetelTextBox1.Id_HiddenField))
        {
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Value = IraIrattariTetelTextBox1.Id_HiddenField;
            erec_UgyUgyiratokSearch.IraIrattariTetel_Id.Operator = Query.Operators.equals;
        }

        //LZS - BUG_7099
        //Megjegyzés szöveg beállítása a search object-nek.
        if (!String.IsNullOrEmpty(txtMegjegyzes.Text))
        {
            erec_UgyUgyiratokSearch.fts_note = new FullTextSearchField();
            erec_UgyUgyiratokSearch.fts_note.Filter = txtMegjegyzes.Text;
        }

        #region LZS - Ügyintézõ mezõ
        if (!String.IsNullOrEmpty(felhasznaloTextBoxUgyintezo.Text))
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Value = felhasznaloTextBoxUgyintezo.Id_HiddenField;
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Ugyintez.Operator = Query.Operators.equals;
        }
        #endregion

        //template-es szűrés
        if (!String.IsNullOrEmpty(hfFelhasznaloCsoport_Id_Orzo_Value.Value) && !String.IsNullOrEmpty(hfFelhasznaloCsoport_Id_Orzo_Operator.Value))
        {
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Value = hfFelhasznaloCsoport_Id_Orzo_Value.Value;
            erec_UgyUgyiratokSearch.FelhasznaloCsoport_Id_Orzo.Operator = hfFelhasznaloCsoport_Id_Orzo_Operator.Value;
        }

        return erec_UgyUgyiratokSearch;
    }



    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_UgyUgyiratokSearch searchObject = SetSearchObjectFromComponents();

            EREC_UgyUgyiratokSearch defaultSearchObject = GetDefaultSearchObject();

            if (Search.IsIdentical(searchObject, defaultSearchObject)
                 && Search.IsIdentical(searchObject.Extended_EREC_KuldKuldemenyekSearch, defaultSearchObject.Extended_EREC_KuldKuldemenyekSearch)
                 && Search.IsIdentical(searchObject.Extended_EREC_UgyUgyiratdarabokSearch, defaultSearchObject.Extended_EREC_UgyUgyiratdarabokSearch)
                 && Search.IsIdentical(searchObject.Extended_EREC_IraIktatoKonyvekSearch, defaultSearchObject.Extended_EREC_IraIktatoKonyvekSearch)
                 && Search.IsIdentical(searchObject.Extended_EREC_IratMetaDefinicioSearch, defaultSearchObject.Extended_EREC_IratMetaDefinicioSearch)
                 && Search.IsIdentical(searchObject.Extended_EREC_IraIratokSearch, defaultSearchObject.Extended_EREC_IraIratokSearch)
                 && Search.IsIdentical(searchObject.Extended_EREC_IrattariKikeroSearch, defaultSearchObject.Extended_EREC_IrattariKikeroSearch)
                 )
            {
                // default searchobject
                if (Startup == Constants.Startup.FromAtmenetiIrattar)
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch);
                }
                else
                {
                    Search.RemoveSearchObjectFromSession_CustomSessionName(Page, Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch);
                }
            }
            else
            {
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                if (Startup == Constants.Startup.FromAtmenetiIrattar)
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.AtmenetiIrattarbaVetelSearch);
                }
                else
                {
                    Search.SetSearchObject_CustomSessionName(Page, searchObject, Constants.CustomSearchObjectSessionNames.KozpontiIrattarbaVetelSearch);
                }
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            EFormPanel1.Visible = false;
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }

    }

    private EREC_UgyUgyiratokSearch GetDefaultSearchObject()
    {
        return new EREC_UgyUgyiratokSearch(true);
    }

    void SetIratHelye()
    {
        if (!String.IsNullOrEmpty(hfFelhasznaloCsoport_Id_Orzo_Value.Value) && !String.IsNullOrEmpty(hfFelhasznaloCsoport_Id_Orzo_Operator.Value))
        {
            trIratHelye.Visible = true;

            Contentum.eAdmin.Service.KRT_CsoportokService service = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam xpm = UI.SetExecParamDefault(Page);
            KRT_CsoportokSearch search = new KRT_CsoportokSearch();
            search.Id.Value = hfFelhasznaloCsoport_Id_Orzo_Value.Value;
            search.Id.Operator = hfFelhasznaloCsoport_Id_Orzo_Operator.Value;

            Result res = service.GetAll(xpm, search);

            if (!res.IsError)
            {
                StringBuilder sb = new StringBuilder();

                foreach (DataRow row in res.Ds.Tables[0].Rows)
                {
                    if (sb.Length > 0)
                        sb.Append(", ");

                    sb.Append(row["Nev"].ToString());
                }

                tbIratHelye.Text = sb.ToString();
            }
        }
    }

}
