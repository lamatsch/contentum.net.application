﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eBusinessDocuments;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;

public partial class IrattariJogosultak : System.Web.UI.Page
{
    private string Command = "";
    private PageView pageView = null;
    private ComponentSelectControl compSelector = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        FormHeader1.HeaderTitle = "Irattári jogosultak";
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);

        switch (Command)
        {
            case CommandName.DesignView:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.DesignView);
                compSelector = (ComponentSelectControl)LoadControl("Component/ComponentSelectControl.ascx");
                FormFooter1.Controls.Add(compSelector);
                break;
            default:
                // TODO: ? FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "Irat_Iktatokonyvei" + Command);
                break;
        }

        var id = Session[Constants.SessionKeys.SelectedIrattarNodeId].ToString();
        Label_NodePath.Text = Session[Constants.SessionKeys.SelectedIrattarNodePath].ToString();
        HiddenField_NodeId.Value = id;

        if (!IsPostBack)
        {
            FillCsoportokList();
            FillJogosultakList(id);
        }

        JogosultRow.Visible = false;
        ListUpdatePanel.Visible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }
    }

    private void Load_ComponentSelectModul()
    {
        if (compSelector == null) { return; }
        else
        {
            compSelector.Enabled = true;

            //compSelector.Add_ComponentOnClick(Iktatokonyv_IktatokonyvTextBox);
            compSelector.Add_ComponentOnClick(Csoport_Id_JogosultTextBox);

            FormFooter1.SaveEnabled = false;
        }
    }

    private void FormFooter1ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            // TODO: ? if (FunctionRights.GetFunkcioJog(Page, "Irat_Iktatokonyvei" + Command)) {
                switch (Command)
                {
                    case CommandName.Modify:
                    {
                        var irattarId = HiddenField_NodeId.Value;

                        var service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
                        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                        var csoportIds = JogosultakList.Items.Cast<ListItem>().Select(item => item.Value);
                        Result result = service.SetCsoportokToJogtargy(execParam, irattarId, csoportIds.ToArray(), 'I');

                        if (result.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                        }
                        else
                        {
                            JavaScripts.RegisterCloseWindowClientScript(Page);
                        }
                        break;
                    }
                }
            //}
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    void FillCsoportokList()
    {
        CsoportokList.Items.Clear();

        var service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page);

        var search = new KRT_CsoportokSearch();
        search.Nev.Operator = Search.GetOperatorByLikeCharater(TextBoxSearch.Text);
        search.Nev.Value = TextBoxSearch.Text;
        search.OrderBy = "Nev";
        search.TopRow = Rendszerparameterek.GetInt(Page, Rendszerparameterek.LOVLIST_MAX_ROW);

        Result result = service.GetAll(ExecParam, search);

        UI.ListBoxFill(CsoportokList, result, "Nev", FormHeader1.ErrorPanel, null);

        trMaxRowWarning.Visible = UI.IsLovListMaxRowWarning(result, search.TopRow);
    }

    void FillJogosultakList(string irattarId)
    {
        JogosultakList.Items.Clear();

        var service = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.getRightsService();
        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        RowRightsCsoportSearch search = new RowRightsCsoportSearch();
        search.OrderBy = "Nev";

        Result result = service.GetAllRightedCsoportByJogtargy(ExecParam, irattarId, search);

        UI.ListBoxFill(JogosultakList, result, "Csoport_Id_Jogalany", "Nev", FormHeader1.ErrorPanel, null);
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        var selectedItems = GetSelectedItems(CsoportokList);
        foreach (ListItem item in selectedItems)
        {
            ListItem currentItem = JogosultakList.Items.FindByValue(item.Value);
            if (currentItem == null)
            {
                JogosultakList.Items.Add(item);
            }
        }
    }

    protected void RemoveButton_Click(object sender, EventArgs e)
    {
        var selectedItems = GetSelectedItems(JogosultakList);
        foreach (ListItem item in selectedItems)
        {
            JogosultakList.Items.Remove(item);
        }
    }

    List<ListItem> GetSelectedItems(ListBox listBox)
    {
        return listBox.Items.Cast<ListItem>().Where(item => item.Selected).ToList();
    }

    private void RefreshCsoportok()
    {
        ScriptManager1.SetFocus(TextBoxSearch);
        FillCsoportokList();
    }

    protected void TextBoxSearch_TextChanged(object sender, EventArgs e)
    {
        RefreshCsoportok();
    }

    protected void ButtonSearch_Click(object sender, ImageClickEventArgs e)
    {
        RefreshCsoportok();
    }
}