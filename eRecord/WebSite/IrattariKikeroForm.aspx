<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true"
    CodeFile="IrattariKikeroForm.aspx.cs" Inherits="IrattariKikeroForm" Title="Untitled Page" %>

<%@ Register Src="Component/ReadOnlyTextBox.ascx" TagName="ReadOnlyTextBox" TagPrefix="uc13" %>
<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc12" %>
<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox"
    TagPrefix="uc8" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl"
    TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList"
    TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent"
    TagPrefix="uc10" %>
<%@ Register Src="Component/ComponentSelectControl.ascx" TagName="ComponentSelectControl"
    TagPrefix="uc23" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl"
    TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc9" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/InfoModalPopup.ascx" TagName="InfoModalPopup"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .mrUrlapCaption, .kUrlapCaption, .mrUrlapCaptionHibas, .kUrlapCaptionHibas {
            text-align: left;
            width: 110px;
            min-width: 110px;
        }
        .mrUrlapInput, .mrUrlapInputWaterMarked, .mrUrlapInputHibas, .mrUrlapInputDis, .mrUrlapInputKotelezo, .mrUrlapInputRed, .mrUrlapLabelLike, .mrUrlapInputFTS {
            width: 230px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>

     <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Scripts/jquery-1.12.4.min.js") %>"></script>
    <script type="text/javascript" language="javascript">
        function startupCheckBoxListener() {
            $("input[type=checkbox][id$='check']").click(function (e) {
                var count = $("input[type=checkbox][id$='check']:checked").length;
                $("span[id$='labelTetelekSzamaDb']").text(count.toString());
            });
        }
    </script>

    <uc1:FormHeader ID="FormHeader1" runat="server" />
    <br />
    <asp:Panel runat="server" ID="MainPanel">
        <asp:HiddenField ID="hiddenWhitoutLeaderCheck" runat="server" Visible="false" Value="0" />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="text-align: left; vertical-align: top;" class="GridViewHeaderBackGroundStyle">
                    <uc3:InfoModalPopup ID="InfoModalPopup1" runat="server" />
                    <asp:Panel ID="UgyiratokListPanel" runat="server" Visible="false">
                        <div>
                            <asp:Label ID="labelUgyiratok" Text="�gyiratok list�ja" runat="server" CssClass="GridViewTitle"></asp:Label>
                            <asp:Label ID="labelNemKiadhatoUgyiratok" Text="Nem kiadhat� �gyiratok list�ja" runat="server"
                                Visible="false" CssClass="GridViewTitle"></asp:Label>
                        </div>
                        <asp:GridView ID="UgyUgyiratokGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                            OnRowDataBound="UgyUgyiratokGridView_RowDataBound" AllowSorting="False" AutoGenerateColumns="False"
                            DataKeyNames="Id">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                    <%-- <HeaderTemplate>
                                    <div class="DisableWrap">
                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                    &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                    </div>
                                </HeaderTemplate>--%>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("Id") %>' CssClass="HideCheckBoxText" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                            Visible="false" OnClientClick="return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Foszam_Merge" HeaderText="F�sz�m" SortExpression="EREC_IraIktatokonyvek.MegkulJelzes,EREC_UgyUgyiratok.Foszam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Targy" HeaderText="T�rgy" SortExpression="Targy">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Felelos_Nev" HeaderText="Kezel�" SortExpression="Felelos_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NevSTR_Ugyindito" HeaderText="�gyind�t�" SortExpression="NevSTR_Ugyindito">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IrattariTetelszam" HeaderText="Itsz." SortExpression="IrattariTetelszam">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="60px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Allapot_Nev" HeaderText="�llapot" SortExpression="Allapot_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LetrehozasIdo" HeaderText="Ikt.&nbsp;d�tum" SortExpression="LetrehozasIdo">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Hatarido" HeaderText="Hat�rid�" SortExpression="Hatarido">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Lezarasdat" HeaderText="Lez�r�s" SortExpression="Lezarasdat">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="50px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Elintezesdat" HeaderText="�gyirat elint�z�si id�pontja" SortExpression="Elintezesdat">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                    <%--TODO: ide majd egy kis ikon kell, ami f�l� h�zva megjelenik az �rz� neve--%>
                                </asp:BoundField>
                                <asp:BoundField DataField="KRT_Csoportok_Orzo_Nev" HeaderText="Irat helye" SortExpression="KRT_Csoportok_Orzo_Nev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewLockedImage" HeaderStyle-CssClass="GridViewLockedImage">
                                    <HeaderTemplate>
                                        <asp:Image ID="Image1" runat="server" AlternateText="Z&aacute;rol&aacute;s" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="LockedImage" runat="server" ImageUrl="~/images/hu/egyeb/locked.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="KikerokListPanel" runat="server" Visible="false">
                        <asp:Label ID="labelKikerok" Text="Iratt�ri kik�r�k list�ja" runat="server" CssClass="GridViewTitle"></asp:Label>
                        <asp:GridView ID="KikerokGridView" runat="server" CellPadding="0" CellSpacing="0"
                            BorderWidth="1" GridLines="Both" AllowPaging="False" PagerSettings-Visible="false"
                            AllowSorting="False" AutoGenerateColumns="False" OnRowDataBound="KikerokGridView_RowDataBound"
                            DataKeyNames="UgyUgyirat_Id">
                            <RowStyle CssClass="GridViewRowStyle" Wrap="True" />
                            <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                            <HeaderStyle CssClass="GridViewHeaderStyle" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="GridViewCheckBoxTemplateHeaderStyle" />
                                    <ItemStyle CssClass="GridViewCheckBoxTemplateItemStyle" />
                                    <%-- <HeaderTemplate>
                                    <div class="DisableWrap">
                                    <asp:ImageButton ID="SelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kispipa.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_SelectAll%>" />
                                    &nbsp;
                                    <asp:ImageButton ID="DeSelectingRowsImageButton" runat="server" ImageUrl="images/hu/egyeb/kisiksz.jpg"
                                        AlternateText="<%$Resources:List,AlternateText_DeSelectAll%>" />
                                    </div>
                                </HeaderTemplate>--%>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="check" runat="server" Text='<%# Eval("UgyUgyirat_Id") %>' CssClass="HideCheckBoxText" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-CssClass="GridViewInfoImage" HeaderStyle-CssClass="GridViewInfoImage">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="InfoImage" runat="server" ImageUrl="~/images/hu/egyeb/info_icon.png"
                                            Visible="false" OnClientClick="return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Azonos�t�" SortExpression="Azonosito">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="130px" />
                                    <ItemStyle CssClass="GridViewHyperLinkFieldItemStyle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="labelAzonosito" ToolTip="Megtekint">
                                            <asp:HyperLink runat="server" ID="hplAzonosito" NavigateUrl="" Text='<%#Eval("Azonosito") %>'>
                                            </asp:HyperLink>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DokumentumTipusNev" HeaderText="T�pus" SortExpression="DokumentumTipusNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Keres_note" HeaderText="K�r�s&nbsp;indoka" SortExpression="Keres_note">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="200px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FelhasznalasiCelNev" HeaderText="Felhaszn�l�s&nbsp;c�lja"
                                    SortExpression="FelhasznalasiCelNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="120px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AllapotNev" HeaderText="�llapot" SortExpression="AllapotNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="100px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Kikero_IdNev" HeaderText="Ig�nyl�" SortExpression="Kikero_IdNev">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="150px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="KeresDatuma" HeaderText="K�r�s&nbsp;ideje" SortExpression="KeresDatuma">
                                    <HeaderStyle CssClass="GridViewBorderHeader" Width="80px" />
                                    <ItemStyle CssClass="GridViewBoundFieldItemStyle" />
                                </asp:BoundField>
                            </Columns>
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel runat="server" ID="panelTetelekSzama" Visible="false" Style="height: 20px;">
                        <div>
                            <asp:Label ID="labelTetelekSzama" Text="Kijel�lt t�telek sz�ma: " runat="server" />
                            <asp:Label ID="labelTetelekSzamaDb" Text="0" runat="server" />
                        </div>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_Warning_Ugyirat" runat="server" Visible="false" Style="height: 20px;">
                        <asp:Label ID="Label_Warning_Ugyirat" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="Panel_Warning_Kikero" runat="server" Visible="false">
                        <asp:Label ID="Label_Warning_Kikero" runat="server" Text="Label" CssClass="warningHeader"></asp:Label>
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                        <asp:UpdatePanel ID="KolcsonzesUpdatePanel" runat="server">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr id="Tr1" runat="server" class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label14" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label12" runat="server" Text="T�pus:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <asp:RadioButtonList ID="DokumenumTipusRadioButtonList" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged"
                                                RepeatDirection="Horizontal" Width="200px">
                                                <asp:ListItem Value="U">&#220;gyirat</asp:ListItem>
                                                <asp:ListItem Value="T">T&#233;rtivev&#233;ny</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Panel runat="server" ID="panelAzonosito">
                                                <asp:Label ID="Label13" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                                <asp:Label ID="Label16" runat="server" Text="Azonos�t�:"></asp:Label>
                                            </asp:Panel>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc12:UgyiratTextBox ID="UgyiratTextBox1" runat="server" Validate="true" Width="150px" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label2" runat="server" Text="K�r�s indoka:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop" colspan="3">
                                            <asp:TextBox ID="Keres_noteTextBox" runat="server" Width="96%" Height="50px" Rows="5"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label20" runat="server" Text="Felhaszn�l�s c�lja:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop" colspan="3">
                                            <asp:RadioButtonList ID="FelhasznalasiCelRadioButtonList" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged"
                                                RepeatDirection="Horizontal" Width="220px" AutoPostBack="true">
                                                <asp:ListItem Value="B">Betekint&#233;sre</asp:ListItem>
                                                <asp:ListItem Value="U">&#220;gyint&#233;z&#233;sre</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label_kolcsonzesIdo_cs" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label_kolcsonzesIdo" runat="server" Text="K�lcs�nz�s id�pontja:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc6:ErvenyessegCalendarControl ID="Kiker_mettolKiker_meddigErvenyessegCalendarControl1"
                                                runat="server" Validate="true" TimeVisible="true" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label7" runat="server" Text="�llapot:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc11:KodtarakDropDownList ID="AllapotKodtarakDropDownList" runat="server" ReadOnly="true"
                                                Width="150px" />
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" id="tr_Tipus_dropdown" runat="server">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label17" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="LabelIgenylo" runat="server" Text="Ig�nyl�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc8:FelhasznaloCsoportTextBox ID="KikeroFelhasznaloCsoportTextBox" runat="server"
                                                Validate="true" ReadOnly="true" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label18" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text="K�r�s ideje:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc5:CalendarControl ID="KeresDatumaCalendarControl" runat="server" Validate="true" TimeVisible="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" runat="server" id="tr_jovahagyas">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label19" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                                            <asp:Label ID="LabelJovahagyo" runat="server" Text="J�v�hagy�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc8:FelhasznaloCsoportTextBox ID="JovahagyoFelhasznaloCsoportTextBox" runat="server"
                                                Validate="true" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label6" runat="server" Text="J�v�hagy�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc5:CalendarControl ID="JovagyasIdejeCalendarControl" runat="server" Validate="false" TimeVisible="true" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" runat="server" id="tr_visszaadas">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label1" runat="server" Text="Visszaad�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc8:FelhasznaloCsoportTextBox ID="VisszaadoFelhasznaloCsoportTextBox" runat="server"
                                                Validate="false" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label10" runat="server" Text="Visszaad�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc5:CalendarControl ID="VisszaadasDatumaCalendarControl" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label9" runat="server" Text="Kiad�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc8:FelhasznaloCsoportTextBox ID="Kiado_FelhasznaloCsoportTextBox1" runat="server"
                                                Validate="false" ViewMode="true" ReadOnly="true" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="Label8" runat="server" Text="Kiad�s d�tuma:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc5:CalendarControl ID="KiadasDatumaCalendarControl" runat="server" Validate="false" />
                                        </td>
                                    </tr>
                                    <tr class="urlapSor" runat="server" id="tr_visszavetel">
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelVisszavevo" runat="server" Text="Visszavev�:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc8:FelhasznaloCsoportTextBox ID="VisszavevoFelhasznaloCsoportTextBox" runat="server"
                                                Validate="false" ViewMode="true" ReadOnly="true" />
                                        </td>
                                        <td class="mrUrlapCaption">
                                            <asp:Label ID="labelVisszavetelDatuma" runat="server" Text="Visszav�tel id�pontja:"></asp:Label>
                                        </td>
                                        <td class="mrUrlapMezoVAlignTop">
                                            <uc5:CalendarControl ID="VisszavetelDatumaCalendarControl" runat="server" Validate="false" ReadOnly="true" TimeVisible="true" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </eUI:eFormPanel>
                    <%--      <uc4:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server" OnLoad="FormPart_CreatedModified1_Load" /> --%>
                    <uc2:FormFooter ID="FormFooter1" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
