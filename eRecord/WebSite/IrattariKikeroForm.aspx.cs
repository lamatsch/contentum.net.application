﻿//using Contentum.eAdmin.Utility;
using Contentum.eAdmin.Service;
using Contentum.eBusinessDocuments;
using Contentum.eQuery;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IrattariKikeroForm : Contentum.eUtility.UI.PageBase
{
    public const string JovagyoText = "Kölcsönzést engedélyezõ:";
    public const string IgenyloText = "Kölcsönzõ:";

    private UI ui = new UI();

    private string Command = "";
    private string Startup = "";

    private Contentum.eAdmin.Utility.PageView pageView = null;
    //private ComponentSelectControl compSelector = null;

    private const string KodCsoportIrattariKikeroAllapot = "IRATTARIKIKEROALLAPOT";

    public string maxTetelszam = "0";

    private List<string> SelectedIdsList = null;
    private int countKiNemAdhatok = 0;
    private bool disableJovahagyas = false;
    private bool disableElutasitas = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new Contentum.eAdmin.Utility.PageView(Page, ViewState);

        Command = Request.QueryString.Get(CommandName.Command);

        Startup = Request.QueryString.Get(Constants.Startup.StartupName);

        switch (Command)
        {
            case CommandName.KolcsonzesJovahagyas:
                //FunctionRights.GetFunkcioJogRedirectErrorPage(Page, CommandName.KolcsonzesJovahagyas);
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KolcsonzesJovahagyas");
                break;
            case CommandName.KikeresAtmenetiIrattarbol:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AtmenetiIrattarKolcsonzes");
                break;
            case CommandName.KiadasAtmenetiIrattarbol:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattarKolcsonzesKiadasa");
                break;
            case CommandName.KikeresSkontroIrattarbol:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KikeresSkontroIrattarbol");
                break;
            case CommandName.KikeresAtmenetiIrattarbolJovahagyas:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "AtmenetiIrattarKikeresJovahagyas");
                break;
            default:
                FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KozpontiIrattarKolcsonzes");
                break;
        }

        maxTetelszam = Rendszerparameterek.GetInt(UI.SetExecParamDefault(Page, new ExecParam()), Rendszerparameterek.MAX_ITEM_NUMBER).ToString();

        if (!IsPostBack)
        {
            //LZS - BUG_11549
            //A Page_Load-ból átkerült ide, mert már itt a Page_Init-ben kell használjam az átadott Id-kat KikeresAtmenetiIrattarbol esetén.
            if (Command == CommandName.KikeresAtmenetiIrattarbol || Command == CommandName.KiadasAtmenetiIrattarbol
                   || Command == CommandName.KikeresSkontroIrattarbol || Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
            {
                if (Session[Constants.SessionNames.SelectedIds] != null && Session[Constants.SessionNames.SelectedIds] is List<string>)
                {
                    SelectedIdsList = (List<string>)Session[Constants.SessionNames.SelectedIds];
                    Session[Constants.SessionNames.SelectedIds] = null;
                }
                else
                {
                    //hiba
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                    MainPanel.Visible = false;
                    return;
                }

            }

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
            EREC_IrattariKikero EREC_IrattariKikero;
            IrattariKikero.Statusz statusz = null;

            if (Command == CommandName.View || Command == CommandName.Modify)
            {
                String id = Request.QueryString.Get(QueryStringVars.Id);

                if (String.IsNullOrEmpty(id))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                }
                else
                {
                    EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                    execParam.Record_Id = id;

                    Result result = service.GetByUgyiratId(execParam);

                    // ha nem talált rekordot, megpróbál kikérõ Id alapján keresni
                    if (!string.IsNullOrEmpty(result.ErrorCode))
                        result = service.Get(execParam);

                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_IrattariKikero = (EREC_IrattariKikero)result.Record;

                        statusz = IrattariKikero.GetAllapotByBusinessDocument(EREC_IrattariKikero);

                        if (Command == CommandName.Modify && !IrattariKikero.Modosithato(statusz, execParam))
                        {
                            Response.Redirect(Page.Request.Url.AbsolutePath + "?Command=View&Id=" + id);
                            return;
                        }

                        LoadComponentsFromBusinessObject(EREC_IrattariKikero);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
            else if (Command == CommandName.New || Command == CommandName.KikeresAtmenetiIrattarbol || Command == CommandName.KikeresSkontroIrattarbol)
            {
                ClearForm();
                RadioButtonList1_SelectedIndexChanged(FelhasznalasiCelRadioButtonList, null);

                //LZS - BUG_11549
                #region LZS - BUG_11549
                //Ha a IRATTAROZAS_JOVAHAGYO_UGYFELELOS érték 1 akkor...
                if (Rendszerparameterek.GetBoolean(execParam, Rendszerparameterek.IRATTAROZAS_JOVAHAGYO_UGYFELELOS))
                {
                    hiddenWhitoutLeaderCheck.Value = "0";
                    //LZS - BUG_11549
                    //Központi irattárból történő Kölcsönzés esetén vizsgáljuk meg, hogy az Ügyirat Szervezeti egység mezőjében lévő szervezeti egység érvényes-e, illetve a szervezethez van-e olyan felhasználó rendelve, akinek létezik AtmenetiIrattarKikeresJovahagyas (Átmenti irattárból történő kikérés) és KolcsonzesJovahagyas (Központiból történő kölcsönzés) funkciójoga.
                    //1.1.Ha igen, akkor töltsük automatikusan ezzel a felhasználóval a Kikérést/ Kölcsönzést engedélyező mezőt és ne legyen szerkeszthető. 
                    String id = Request.QueryString.Get(QueryStringVars.Id);

                    //Átmeneti irattárból kikérésnél tömegesen is lehet, ezért ott a Session-ből lehet kiszedni az id-kat.
                    //Ez megtörténik feljebb.
                    if (Command == CommandName.KikeresAtmenetiIrattarbol)
                    {
                        //Hogyha valami oknál fogva nincs átadva egy sem akkor "NA"-ra állítjuk.
                        id = SelectedIdsList.Count == 1 ? SelectedIdsList[0] : "NA";
                    }

                    if (!String.IsNullOrEmpty(id))
                    {
                        //"NA" esetén meghívjuk a JovahagyoFiedFillHandler_Default() mezőkezelést.
                        if (id == "NA")
                        {
                            JovahagyoFiedFillHandler_Default(Command);
                            return;
                        }

                        //Lekérjük az ügyiratot az id alapján, hogy meg tudjuk vizsgálni, hogy érvényes-e még az ügyiratban
                        //megadott szervezeti egység.
                        EREC_UgyUgyiratokService svc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                        ExecParam xpm = UI.SetExecParamDefault(Page);
                        xpm.Record_Id = id;
                        Result res = svc.Get(xpm);
                        if (res.IsError)
                        {
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                        }
                        else
                        {
                            //Ügyirat objektum:
                            EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;

                            if (string.IsNullOrEmpty(ugyirat.Csoport_Id_Ugyfelelos))
                            {
                                //Alapeset
                                JovahagyoFiedFillHandler_Default(Command);
                            }
                            else
                            {

                                KRT_PartnerekService kRT_PartnerekService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_PartnerekService();
                                ExecParam paramPartner = UI.SetExecParamDefault(Page);
                                paramPartner.Record_Id = ugyirat.Csoport_Id_Ugyfelelos;

                                //Lekérjük az ügyirat szervezeti egységét:
                                Result resSzervezetiEgyseg = kRT_PartnerekService.Get(paramPartner);
                                if (resSzervezetiEgyseg.IsError)
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resSzervezetiEgyseg);
                                }
                                else
                                {
                                    //Szervezeti egység
                                    KRT_Partnerek partnerSzervezetiEgyseg = resSzervezetiEgyseg.Record as KRT_Partnerek;

                                    //Hogyha a szervezeti egység érvényes, akkor beírható a mezőbe jóváhagyó felhasználó:
                                    if (partnerSzervezetiEgyseg != null && Convert.ToDateTime(partnerSzervezetiEgyseg.ErvVege) > DateTime.Now)
                                    {

                                        Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                                        ExecParam paramCsoportok = UI.SetExecParamDefault(Page, new ExecParam());
                                        KRT_Csoportok szuloCsoport = new KRT_Csoportok();
                                        szuloCsoport.Id = ugyirat.Csoport_Id_Ugyfelelos;

                                        KRT_CsoportTagokSearch search = new KRT_CsoportTagokSearch();

                                        //Csoporttagok lekérése az ugyirat.Csoport_Id_Ugyfelelos id alapján:
                                        Result resCsoportTagok = service_csoportok.GetAllBySzuloCsoport(paramCsoportok, szuloCsoport, search);

                                        if (resCsoportTagok.IsError)
                                        {
                                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resCsoportTagok);
                                        }
                                        else
                                        {
                                            //Ha megvan a csoporttagok objektum, akkor abból kinyerhető a felhasználó objektum a Csoport_Id_Jogalany mezőn keresztül: 
                                            KRT_FelhasznalokService kRT_FelhasznalokService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                                            string csoportTag_Csoport_Id_JogalanyId;
                                            string csoportTagId;

                                            if (resCsoportTagok.Ds.Tables[0] != null && resCsoportTagok.Ds.Tables[0].Rows.Count > 0)
                                            {
                                                //Végigmegyünk a csoporttagokon és lekérjük a felhasználókat.
                                                foreach (DataRow csoportTag in resCsoportTagok.Ds.Tables[0].Rows)
                                                {
                                                    //Csoport_Id_Jogalany mezőn keresztül:
                                                    csoportTag_Csoport_Id_JogalanyId = csoportTag["Csoport_Id_Jogalany"].ToString();
                                                    csoportTagId = csoportTag["Id"].ToString();

                                                    ExecParam paramFelhasznalo = UI.SetExecParamDefault(Page, new ExecParam());
                                                    paramFelhasznalo.Record_Id = csoportTag_Csoport_Id_JogalanyId;

                                                    //Aktuális felhasználó:
                                                    paramFelhasznalo.Felhasznalo_Id = csoportTag_Csoport_Id_JogalanyId;//currentFelhasznalo.Id;
                                                    paramFelhasznalo.CsoportTag_Id = csoportTagId;

                                                    //Alapeset
                                                    JovahagyoFiedFillHandler_Default(Command);

                                                    switch (Command)
                                                    {
                                                        case CommandName.New:
                                                            //Ha a felhasználónak van KolcsonzesJovahagyas jogosultsága, akkor az állítjuk be a controlnak.
                                                            if (FunctionRights.HasFunctionRight(paramFelhasznalo, "KolcsonzesJovahagyas"))
                                                            {
                                                                JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField = csoportTag_Csoport_Id_JogalanyId;
                                                                JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                                                                //Beállíthuk, hogy a kiválasztó control csak olyan dolgozókat hozzon, akiknek van KolcsonzesJovahagyas jogosultságuk.
                                                                //Bár ebben az esetben nem tud majd kiválasztani, mert kiszürkítjük:
                                                                JovahagyoFelhasznaloCsoportTextBox.CsakKolcsonzesJovahagyasJoguDolgozok = true;
                                                                JovahagyoFelhasznaloCsoportTextBox.ReadOnly = true;
                                                                JovahagyoFelhasznaloCsoportTextBox.TextBox.Enabled = true;
                                                                return;
                                                            }
                                                            break;

                                                        case CommandName.KikeresAtmenetiIrattarbol:
                                                            //Ha a felhasználónak van AtmenetiIrattarKikeresJovahagyas jogosultsága, akkor az állítjuk be a controlnak.
                                                            if (FunctionRights.HasFunctionRight(paramFelhasznalo, "AtmenetiIrattarKikeresJovahagyas"))
                                                            {
                                                                JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField = csoportTag_Csoport_Id_JogalanyId;
                                                                JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                                                                //Beállíthuk, hogy a kiválasztó control csak olyan dolgozókat hozzon, akiknek van AtmenetiIrattarKikeresJovahagyas jogosultságuk.
                                                                //Bár ebben az esetben nem tud majd kiválasztani, mert kiszürkítjük:
                                                                JovahagyoFelhasznaloCsoportTextBox.CsakAtmenetiIrattarKikeresJovahagyasJoguDolgozok = true;
                                                                JovahagyoFelhasznaloCsoportTextBox.ReadOnly = true;
                                                                JovahagyoFelhasznaloCsoportTextBox.TextBox.Enabled = true;
                                                                return;
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //Alapeset
                                                JovahagyoFiedFillHandler_Default(Command);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Alapeset
                                        JovahagyoFiedFillHandler_Default(Command);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion


            }
            else if (Command == CommandName.KolcsonzesJovahagyas)
            {
                String id = Request.QueryString.Get(QueryStringVars.Id);

                if (String.IsNullOrEmpty(id))
                {
                    // nincs Id megadva:
                    ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                }
                else
                {
                    EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                    execParam.Record_Id = id;

                    Result result = service.GetByUgyiratId(execParam);

                    if (string.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_IrattariKikero = (EREC_IrattariKikero)result.Record;

                        statusz = IrattariKikero.GetAllapotByBusinessDocument(EREC_IrattariKikero);

                        //if (Command == CommandName.Modify && !IrattariKikero.Modosithato(statusz, execParam))
                        //{
                        //    Response.Redirect(Page.Request.Url.AbsolutePath + "?Command=View&Id=" + id);
                        //    return;
                        //}

                        if (Command == CommandName.KolcsonzesJovahagyas)
                        {
                            // ha nem jóváhagyható, akkor a jóváhagyás gombot letiltjuk:
                            if (IrattariKikero.Jovahagyhato(statusz, UI.SetExecParamDefault(Page)) == false)
                            {
                                disableJovahagyas = true;
                            }

                            if (IrattariKikero.Visszautasithato(statusz, UI.SetExecParamDefault(Page)) == false)
                            {
                                disableElutasitas = true;
                            }
                        }

                        LoadComponentsFromBusinessObject(EREC_IrattariKikero);
                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
            // Kontrollok beallitasa:

            AllapotKodtarakDropDownList.ReadOnly = true;
            JovahagyoFelhasznaloCsoportTextBox.Validate = false;

            if (Command == CommandName.New)
            {
                //UgyiratTextBox1.ReadOnly = false;
                //Keres_noteTextBox.ReadOnly = false;
                //Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = false;
                //KikeroFelhasznaloCsoportTextBox.ReadOnly = true;
                KeresDatumaCalendarControl.ReadOnly = true;

                if (!JovahagyoFelhasznaloCsoportTextBox.CsakKolcsonzesJovahagyasJoguDolgozok)
                    JovahagyoFelhasznaloCsoportTextBox.ReadOnly = true;

                JovagyasIdejeCalendarControl.ReadOnly = true;
                Kiado_FelhasznaloCsoportTextBox1.ReadOnly = true;
                KiadasDatumaCalendarControl.ReadOnly = true;
                VisszaadoFelhasznaloCsoportTextBox.ReadOnly = true;
                VisszaadasDatumaCalendarControl.ReadOnly = true;

                // CR#2178: Rendszerparamétertõl függõen más nevére is kölcsönözhet
                KikeroFelhasznaloCsoportTextBox.ReadOnly = !Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.KIKERES_MAS_NEVRE_ENABLED);

            }
            else if (Command == CommandName.View)
            {
                SetViewMode(true);
            }
            #region else if (Command == CommandName.Modify)
            else if (Command == CommandName.Modify)
            {
                if (true) // ha rendelkezik irattarosi modositasi jogokkal
                {
                    DokumenumTipusRadioButtonList.Enabled = false;
                    UgyiratTextBox1.ReadOnly = true;
                    if (IrattariKikero.KikeroVagyok(statusz, execParam))
                    {
                        Keres_noteTextBox.ReadOnly = false;
                        FelhasznalasiCelRadioButtonList.Enabled = true;
                        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = false;
                    }
                    else
                    {
                        Keres_noteTextBox.ReadOnly = true;
                        FelhasznalasiCelRadioButtonList.Enabled = false;
                        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = true;
                    }
                    // ide meg kell az irattarosi szerepkor ellenorzese, mert akkor jovahagyot modosithat
                    if (IrattariKikero.JovahagyoVagyok(statusz, execParam))
                    {
                        JovahagyoFelhasznaloCsoportTextBox.ReadOnly = false;
                    }
                    else
                    {
                        JovahagyoFelhasznaloCsoportTextBox.ReadOnly = true;
                    }
                    KikeroFelhasznaloCsoportTextBox.ReadOnly = true;
                    KeresDatumaCalendarControl.ReadOnly = true;
                    Kiado_FelhasznaloCsoportTextBox1.ReadOnly = true;
                    KiadasDatumaCalendarControl.ReadOnly = true;
                    JovagyasIdejeCalendarControl.ReadOnly = true;
                    VisszaadoFelhasznaloCsoportTextBox.ReadOnly = true;
                    VisszaadasDatumaCalendarControl.ReadOnly = true;
                }
                // TODO: check rights
                //else // ha NEM rendelkezik irattarosi modositasi jogokkal
                //{
                //    DokumenumTipusRadioButtonList.Enabled = false;
                //    UgyiratTextBox1.ReadOnly = true;
                //    KikeroFelhasznaloCsoportTextBox.ReadOnly = true;
                //    KeresDatumaCalendarControl.ReadOnly = true;
                //    if (IrattariKikero.KikeroVagyok(statusz, execParam))
                //    {
                //        Keres_noteTextBox.ReadOnly = false;
                //        FelhasznalasiCelRadioButtonList.Enabled = true;
                //        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = false;
                //    }
                //    else
                //    {
                //        Keres_noteTextBox.ReadOnly = true;
                //        FelhasznalasiCelRadioButtonList.Enabled = false;
                //        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = true;
                //    }
                //    // ide meg kell az irattarosi szerepkor ellenorzese, mert akkor jovahagyot modosithat
                //    if (IrattariKikero.JovahagyoVagyok(statusz, execParam))
                //    {
                //        JovahagyoFelhasznaloCsoportTextBox.ReadOnly = false;
                //    }
                //    else
                //    {
                //        JovahagyoFelhasznaloCsoportTextBox.ReadOnly = true;
                //    }
                //    JovagyasIdejeCalendarControl.ReadOnly = true;
                //    Kiado_FelhasznaloCsoportTextBox1.ReadOnly = true;
                //    KiadasDatumaCalendarControl.ReadOnly = true;
                //    VisszaadoFelhasznaloCsoportTextBox.ReadOnly = true;
                //    VisszaadasDatumaCalendarControl.ReadOnly = true;
                //}
            }
            #endregion
        }
    }

    private void JovahagyoFiedFillHandler_Default(string Command)
    {
        //Alapestben:
        //Legyen a mező szerkeszthető.
        //Ne lehessen benne gépelni, csak kiválasztással lehessen tölteni a mezőt.
        //kiválasztás ikon legyen aktív.
        //Kiválasztás ikonra kattintva csak azon dolgozó típusú csoportok jelenjenek meg, 
        //akiknek van AtmenetiIrattarKikeresJovahagyas(Átmenti irattárból történő kikérés) / KolcsonzesJovahagyas(Központiból történő kölcsönzés) funkció.
        JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField = "";
        JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        JovahagyoFelhasznaloCsoportTextBox.ReadOnly = false;
        JovahagyoFelhasznaloCsoportTextBox.TextBox.ReadOnly = true;
        JovahagyoFelhasznaloCsoportTextBox.TextBox.CssClass = "mrUrlapInput";
        hiddenWhitoutLeaderCheck.Value = "1";

        switch (Command)
        {
            case CommandName.New:
                //Beállíthuk, hogy a kiválasztó control csak olyan dolgozókat hozzon, akiknek van KolcsonzesJovahagyas jogosultságuk.
                JovahagyoFelhasznaloCsoportTextBox.CsakKolcsonzesJovahagyasJoguDolgozok = true;
                break;

            case CommandName.KikeresAtmenetiIrattarbol:
                //Beállíthuk, hogy a kiválasztó control csak olyan dolgozókat hozzon, akiknek van AtmenetiIrattarKikeresJovahagyas jogosultságuk.
                JovahagyoFelhasznaloCsoportTextBox.CsakAtmenetiIrattarKikeresJovahagyasJoguDolgozok = true;
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page reload eseten is mukodjon a jquery-s script
        ScriptManager.RegisterStartupScript(this, this.GetType(), Guid.NewGuid().ToString("N"), "startupCheckBoxListener();", true);

        if (Command == CommandName.KikeresAtmenetiIrattarbol)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.KikeresFormHeaderTitle;
        }
        else if (Command == CommandName.KiadasAtmenetiIrattarbol)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.KiadasFormHeaderTitle;
        }
        else if (Command == CommandName.KikeresSkontroIrattarbol)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.KikeresFormSkontroIrattarHeaderTitle;
        }
        if (Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
        {
            FormHeader1.FullManualHeaderTitle = Resources.Form.KikeresJovahagyasFormHeaderTitle;
        }
        else
        {
            FormHeader1.HeaderTitle = Resources.Form.IrattariKikeroFormHeaderTitle;
        }

        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        if (Command == CommandName.DesignView)
        {
            //Load_ComponentSelectModul();
        }
        else
        {
            pageView.SetViewOnPage(Command);
        }

        if (!IsPostBack)
        {
            if (Command == CommandName.KikeresAtmenetiIrattarbol || Command == CommandName.KikeresSkontroIrattarbol)
            {
                FillUgyiratokGridView();
                FormFooter1.ImageButton_Save.OnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(UgyUgyiratokGridView.ClientID);
                labelTetelekSzamaDb.Text = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null).Count.ToString();
                panelAzonosito.Visible = false;
                UgyiratTextBox1.Visible = false;
                DokumenumTipusRadioButtonList.Enabled = false;
            }

            if (Command == CommandName.KiadasAtmenetiIrattarbol || Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
            {
                FillKikerokGridView();
                EFormPanel1.Visible = false;
                FormFooter1.ImageButton_Save.OnClientClick = JavaScripts.SetOnClientClickIsSelectedRow(KikerokGridView.ClientID);
                labelTetelekSzamaDb.Text = ui.GetGridViewSelectedRows(KikerokGridView, FormHeader1.ErrorPanel, null).Count.ToString();
            }
        }

        if (Command == CommandName.KikeresAtmenetiIrattarbol || Command == CommandName.KiadasAtmenetiIrattarbol
               || Command == CommandName.KikeresSkontroIrattarbol)
        {
            // Rendben gomb engedélyezése:
            FormFooter1.Command = CommandName.New;
            panelTetelekSzama.Visible = true;

            JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
        }

        if (Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
        {
            FormFooter1.Command = CommandName.KolcsonzesJovahagyas;
            panelTetelekSzama.Visible = true;

            JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
        }

        if (disableJovahagyas)
        {
            ViewState["disableJovahagyas"] = "1";
        }
        if (disableElutasitas)
        {
            ViewState["disableElutasitas"] = "1";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["disableJovahagyas"] != null && ViewState["disableJovahagyas"].ToString() == "1")
        {
            FormFooter1.ImageButton_Accept.Visible = false;
        }
        if (ViewState["disableElutasitas"] != null && ViewState["disableElutasitas"].ToString() == "1")
        {
            FormFooter1.ImageButton_Decline.Visible = false;
        }

        //Kölcsönzés
        if (Command == CommandName.New || Command == CommandName.View || Command == CommandName.Modify
            || Command == CommandName.KolcsonzesJovahagyas)
        {
            LabelJovahagyo.Text = JovagyoText;
            LabelIgenylo.Text = IgenyloText;
        }
    }

    private void FillUgyiratokGridView()
    {
        DataSet ds = UgyiratokGridViewBind();

        // ellenõrzés:
        if (ds != null && ds.Tables[0].Rows.Count != SelectedIdsList.Count)
        {
            // nem annyi rekord jött, mint amennyit vártunk:
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "");
            //EFormPanel1.Visible = false;
        }

        // Van-e olyan rekord, ami nem adható át? 
        // CheckBoxok vizsgálatával:
        if (Command == CommandName.KiadasAtmenetiIrattarbol || Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
        {
            foreach (GridViewRow row in UgyUgyiratokGridView.Rows)
            {
                CheckBox checkBox = (CheckBox)row.FindControl("check");
                if (checkBox != null)
                {
                    if (checkBox.Enabled)
                    {
                        row.Visible = false;
                    }
                    else
                    {
                        //System.Data.DataRowView drv = (System.Data.DataRowView)row.DataItem;
                        SelectedIdsList.Remove(checkBox.Text);
                        countKiNemAdhatok++;
                    }
                }
            }
            if (countKiNemAdhatok > 0)
            {
                UgyiratokListPanel.Visible = true;
                labelNemKiadhatoUgyiratok.Visible = true;
                labelUgyiratok.Visible = false;

                if (Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
                {
                    labelNemKiadhatoUgyiratok.Text = "Nem jóváhagyható ügyiratok listája";
                }
            }
        }
        else
        {
            UgyiratokListPanel.Visible = true;
            int count_kikerhetok = UI.GetGridViewSelectedCheckBoxesCount(UgyUgyiratokGridView, "check");

            int count_kiNEMkerhetok = SelectedIdsList.Count - count_kikerhetok;

            if (count_kiNEMkerhetok > 0)
            {
                Label_Warning_Ugyirat.Text = String.Format(Resources.List.UI_Ugyirat_KikeresWarning, count_kiNEMkerhetok);
                Panel_Warning_Ugyirat.Visible = true;
            }
        }
    }

    protected DataSet UgyiratokGridViewBind()
    {
        if (SelectedIdsList != null && SelectedIdsList.Count > 0)
        {
            EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

            EREC_UgyUgyiratokSearch search = new EREC_UgyUgyiratokSearch();
            // keresési objektum beállítása: szûrés az Id-kra:
            search.Id.Value = Search.GetSqlInnerString(SelectedIdsList.ToArray());
            search.Id.Operator = Query.Operators.inner;

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(UgyUgyiratokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void UgyUgyiratokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (Command == CommandName.KiadasAtmenetiIrattarbol)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKiadasAtmenetiIrattarbol(e, Page);
            //CheckBox checkBox = (CheckBox)e.Row.FindControl("check");
            //if (checkBox != null)
            //{
            //    if (checkBox.Enabled)
            //    {
            //        e.Row.Visible = false;
            //    }
            //    else
            //    {
            //        System.Data.DataRowView drv = (System.Data.DataRowView)e.Row.DataItem;
            //        SelectedIdsList.Remove(drv["Id"].ToString());
            //    }
            //}
        }
        else if (Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKikeresAtmenetiIrattarbolJovahagyas(e, Page);
        }
        else if (Command == CommandName.KikeresAtmenetiIrattarbol)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKikeresAtmenetiIrattarbol(e, Page);
        }
        else if (Command == CommandName.KikeresSkontroIrattarbol)
        {
            Ugyiratok.UgyiratokGridView_RowDataBound_CheckKikeresSkontroIrattarbol(e, Page);
        }
    }

    private void FillKikerokGridView()
    {
        #region Ügyiratok ellenõrzése

        FillUgyiratokGridView();

        #endregion

        if (SelectedIdsList.Count > 0)
        {
            DataSet ds = KikerokGridViewBind();

            // ellenõrzés:
            if (ds != null && ds.Tables[0].Rows.Count != SelectedIdsList.Count)
            {
                // nem annyi rekord jött, mint amennyit vártunk:
                ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.ErrorHeader_FillGridView, "Ne minden elkért ügyirathoz található irattári kikérõ!");
                EFormPanel1.Visible = false;
            }

            KikerokListPanel.Visible = true;

            // Van-e olyan rekord, ami nem adható át? 
            // CheckBoxok vizsgálatával:
            int count_kiadhatok = UI.GetGridViewSelectedCheckBoxesCount(KikerokGridView, "check");

            countKiNemAdhatok += SelectedIdsList.Count - count_kiadhatok;
        }

        if (countKiNemAdhatok > 0)
        {
            Label_Warning_Kikero.Text = String.Format(Resources.List.UI_Ugyirat_KiadasWarning, countKiNemAdhatok);
            Panel_Warning_Kikero.Visible = true;
        }
    }

    protected DataSet KikerokGridViewBind()
    {
        if (SelectedIdsList != null && SelectedIdsList.Count > 0)
        {

            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
            ExecParam ExecParam = UI.SetExecParamDefault(Page);

            EREC_IrattariKikeroSearch search = new EREC_IrattariKikeroSearch();
            // keresési objektum beállítása: szûrés az Id-kra:
            search.UgyUgyirat_Id.Value = Search.GetSqlInnerString(SelectedIdsList.ToArray());
            search.UgyUgyirat_Id.Operator = Query.Operators.inner;
            search.OrderBy = "EREC_UgyUgyiratok.LetrehozasIdo desc";

            if (Command == CommandName.KikeresAtmenetiIrattarbol)
                IrattariKikero.SetSearchFilterKiadhatoAtmenetiIrattarbol(search);
            else
                IrattariKikero.SetSearchFilterKiadhatoSkontroIrattarbol(search);

            Result res = service.GetAllWithExtension(ExecParam, search);

            ui.GridViewFill(KikerokGridView, res, "", FormHeader1.ErrorPanel, null);

            DataSet ds = res.Ds;

            return ds;
        }
        else
        {
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            return null;
        }
    }

    protected void KikerokGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //UI.GridView_RowDataBound_SetLockingInfo(e, Page);

        if (Command == CommandName.KiadasAtmenetiIrattarbol)
        {
            IrattariKikero.GridView_RowDataBound_CheckKiadasAtmenetiIrattarbol(e, Page);
        }
        else if (Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
        {
            IrattariKikero.GridView_RowDataBound_CheckKikeresAtmenetiIrattarbolJovahagyas(e, Page);
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;
            if (drv != null)
            {
                Label labelAzonosito = (Label)e.Row.FindControl("labelAzonosito");
                string UgyiratId = drv["UgyUgyirat_Id"].ToString();
                if (!String.IsNullOrEmpty(UgyiratId) && labelAzonosito != null)
                {
                    string js = JavaScripts.SetOnCLientClick_NoPostBack("UgyUgyiratokForm.aspx",
                        QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + UgyiratId,
                        Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max);

                    labelAzonosito.Attributes.Add("onclick", js);
                }
            }
        }
    }

    private void ClearForm()
    {
        if (Command == CommandName.New || Command == CommandName.KikeresAtmenetiIrattarbol || Command == CommandName.KikeresSkontroIrattarbol)
        {
            switch (Startup)
            {
                case Constants.Startup.FromUgyirat:
                    DokumenumTipusRadioButtonList.SelectedIndex = 0;
                    break;
                case Constants.Startup.FromTertiveveny:
                    DokumenumTipusRadioButtonList.SelectedIndex = 1;
                    break;
                default:
                    DokumenumTipusRadioButtonList.SelectedIndex = 0;
                    break;
            }

            FelhasznalasiCelRadioButtonList.SelectedIndex = 0;

            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (!String.IsNullOrEmpty(id))
            {
                EREC_UgyUgyiratokService svc = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                ExecParam xpm = UI.SetExecParamDefault(Page);
                xpm.Record_Id = id;
                Result res = svc.Get(xpm);
                if (res.IsError)
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, res);
                }
                else
                {
                    EREC_UgyUgyiratok ugyirat = (EREC_UgyUgyiratok)res.Record;
                    UgyiratTextBox1.SetUgyiratTextBoxByBusinessObject(ugyirat, FormHeader1.ErrorPanel, null);

                    //Fizikai ügyirat csak ügyintézésre kölcsönözhetõ,
                    //ha a FIZIKAI_UGYIRAT_KOLCSONZES_BETEKINTESRE_DISABLED rendszerparaméter be van kapcsolva
                    if (Command == CommandName.New)
                    {
                        if (!Ugyiratok.IsUgyiratKolcsonozhetoBetekintesre(xpm, ugyirat))
                        {
                            FelhasznalasiCelRadioButtonList.SelectedValue = "U"; //ügyintézésre
                            FelhasznalasiCelRadioButtonList.Enabled = false;
                        }
                    }
                }
                UgyiratTextBox1.Id_HiddenField = id;
                UgyiratTextBox1.ReadOnly = true;
                DokumenumTipusRadioButtonList.Enabled = false;
            }
            else
            {
                UgyiratTextBox1.Id_HiddenField = "";
                UgyiratTextBox1.SetUgyiratTextBoxById(FormHeader1.ErrorPanel, null);
                UgyiratTextBox1.ReadOnly = false;
                DokumenumTipusRadioButtonList.Enabled = true;
            }

            AllapotKodtarakDropDownList.FillWithOneValue(KodCsoportIrattariKikeroAllapot, KodTarak.IRATTARIKIKEROALLAPOT.Nyitott, FormHeader1.ErrorPanel);

            //UgyiratTextBox1.ReadOnly = false;
            Keres_noteTextBox.Text = "";

            //Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = false;
            Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvKezd = DateTime.Today.ToShortDateString();
            Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvKezd = DateTime.Today.AddDays(1).ToShortDateString();
            KikeroFelhasznaloCsoportTextBox.Id_HiddenField = FelhasznaloProfil.GetFelhasznaloCsoport(UI.SetExecParamDefault(Page, new ExecParam()));
            KikeroFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            KeresDatumaCalendarControl.Text = DateTime.Now.ToString();
            // TODO: kell a felhasznalo szervezetenek a vezetojenek a csoport id-ja
            JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField = "";
            JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);

            JovagyasIdejeCalendarControl.Text = "";
            JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            Kiado_FelhasznaloCsoportTextBox1.Id_HiddenField = "";
            Kiado_FelhasznaloCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            KiadasDatumaCalendarControl.Text = "";
            VisszaadoFelhasznaloCsoportTextBox.Id_HiddenField = "";
            VisszaadoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            VisszaadasDatumaCalendarControl.Text = "";
            VisszavevoFelhasznaloCsoportTextBox.Id_HiddenField = "";
            VisszavevoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            VisszavetelDatumaCalendarControl.Text = "";

            if (Command == CommandName.KikeresAtmenetiIrattarbol)
            {
                FelhasznalasiCelRadioButtonList.SelectedValue = "U";
                FelhasznalasiCelRadioButtonList.Enabled = Rendszerparameterek.GetBoolean(Page,
                    Rendszerparameterek.KIKERES_ATMENETI_IRATTARBOL_BETEKINTESRE_ENABLED); //false;

                Label_kolcsonzesIdo.Visible = false;
                Label_kolcsonzesIdo_cs.Visible = false;
                Kiker_mettolKiker_meddigErvenyessegCalendarControl1.Visible = false;

                //LZS - BUG_11549
                //Jóváhagyás sor az IRATTAROZAS_ATMENETI_AZONOS_KEZELES rendszerparamétertől függ:
                tr_jovahagyas.Visible = Rendszerparameterek.GetBoolean(Page, Rendszerparameterek.IRATTAROZAS_ATMENETI_AZONOS_KEZELES);
                tr_visszaadas.Visible = false;
                tr_visszavetel.Visible = false;

                KiadasDatumaCalendarControl.ReadOnly = true;
            }
        }
    }

    // business object --> form
    private void LoadComponentsFromBusinessObject(EREC_IrattariKikero erec_IrattariKikero)
    {

        if (Command == CommandName.View || Command == CommandName.Modify || Command == CommandName.KolcsonzesJovahagyas
            || Command == CommandName.KiadasAtmenetiIrattarbol)
        {
            //UgyiratTextBox1.ReadOnly = false;
            if (!String.IsNullOrEmpty(erec_IrattariKikero.DokumentumTipus))
            {
                DokumenumTipusRadioButtonList.SelectedIndex = erec_IrattariKikero.DokumentumTipus == "U" ? 0 : 1;
            }
            else
            {
                DokumenumTipusRadioButtonList.SelectedIndex = 0;
            }

            UgyiratTextBox1.Id_HiddenField = erec_IrattariKikero.UgyUgyirat_Id;
            UgyiratTextBox1.SetUgyiratTextBoxById(FormHeader1.ErrorPanel, null);

            Keres_noteTextBox.Text = erec_IrattariKikero.Keres_note;

            FelhasznalasiCelRadioButtonList.SelectedIndex = erec_IrattariKikero.FelhasznalasiCel == "B" ? 0 : 1;

            Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvKezd = erec_IrattariKikero.KikerKezd;
            Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvVege = erec_IrattariKikero.KikerVege;

            AllapotKodtarakDropDownList.FillWithOneValue(KodCsoportIrattariKikeroAllapot, erec_IrattariKikero.Allapot, FormHeader1.ErrorPanel);

            KikeroFelhasznaloCsoportTextBox.Id_HiddenField = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
            KikeroFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            KeresDatumaCalendarControl.Text = erec_IrattariKikero.KeresDatuma;
            JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField = erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy;
            JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            JovagyasIdejeCalendarControl.Text = erec_IrattariKikero.JovagyasDatuma;
            VisszaadoFelhasznaloCsoportTextBox.Id_HiddenField = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
            VisszaadoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            Kiado_FelhasznaloCsoportTextBox1.Id_HiddenField = erec_IrattariKikero.FelhasznaloCsoport_Id_Kiado;
            Kiado_FelhasznaloCsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            KiadasDatumaCalendarControl.Text = erec_IrattariKikero.KiadasDatuma;
            VisszaadoFelhasznaloCsoportTextBox.Id_HiddenField = erec_IrattariKikero.FelhasznaloCsoport_Id_Visszaad;
            VisszaadoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            VisszaadasDatumaCalendarControl.Text = erec_IrattariKikero.VisszaadasDatuma;
            VisszavevoFelhasznaloCsoportTextBox.Id_HiddenField = erec_IrattariKikero.FelhasznaloCsoport_Id_Visszave;
            VisszavevoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            //A visszavétel dátuma nincs letárolva, így az utolsó módosítás idejét jelenítjük meg, ha visszaadott állapotú
            if (erec_IrattariKikero.Allapot == KodTarak.IRATTARIKIKEROALLAPOT.Visszaadott)
                VisszavetelDatumaCalendarControl.Text = erec_IrattariKikero.Base.ModositasIdo;

            FormHeader1.Record_Ver = erec_IrattariKikero.Base.Ver;

            if (Command == CommandName.KolcsonzesJovahagyas)
            {
                SetViewMode(true);
            }
            else if (Command == CommandName.KiadasAtmenetiIrattarbol)
            {
                #region KiadasAtmenetiIrattarbol

                // láthatóságok állítása:

                SetViewMode(true);

                Label_kolcsonzesIdo.Visible = false;
                Label_kolcsonzesIdo_cs.Visible = false;
                Kiker_mettolKiker_meddigErvenyessegCalendarControl1.Visible = false;

                // jóváhagyás, visszaadás sorok nem kellenek:
                tr_jovahagyas.Visible = false;
                tr_visszaadas.Visible = false;
                tr_visszavetel.Visible = false;

                KiadasDatumaCalendarControl.Text = DateTime.Today.ToString();

                #endregion
            }
        }
    }

    // form --> business object
    private EREC_IrattariKikero GetBusinessObjectFromComponents()
    {
        EREC_IrattariKikero erec_IrattariKikero = new EREC_IrattariKikero();
        // összes mezõ update-elhetõségét kezdetben letiltani:
        erec_IrattariKikero.Updated.SetValueAll(false);
        erec_IrattariKikero.Base.Updated.SetValueAll(false);

        erec_IrattariKikero.DokumentumTipus = DokumenumTipusRadioButtonList.SelectedValue;
        erec_IrattariKikero.Updated.DokumentumTipus = pageView.GetUpdatedByView(DokumenumTipusRadioButtonList);

        erec_IrattariKikero.UgyUgyirat_Id = UgyiratTextBox1.Id_HiddenField;
        erec_IrattariKikero.Updated.UgyUgyirat_Id = pageView.GetUpdatedByView(UgyiratTextBox1);

        erec_IrattariKikero.Keres_note = Keres_noteTextBox.Text;
        erec_IrattariKikero.Updated.Keres_note = pageView.GetUpdatedByView(Keres_noteTextBox);

        erec_IrattariKikero.FelhasznalasiCel = FelhasznalasiCelRadioButtonList.SelectedValue;
        erec_IrattariKikero.Updated.FelhasznalasiCel = pageView.GetUpdatedByView(FelhasznalasiCelRadioButtonList);

        erec_IrattariKikero.Allapot = AllapotKodtarakDropDownList.SelectedValue;

        erec_IrattariKikero.KikerKezd = Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvKezd;
        erec_IrattariKikero.Updated.KikerKezd = pageView.GetUpdatedByView(Kiker_mettolKiker_meddigErvenyessegCalendarControl1);

        erec_IrattariKikero.KikerVege = Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvVege;
        erec_IrattariKikero.Updated.KikerVege = pageView.GetUpdatedByView(Kiker_mettolKiker_meddigErvenyessegCalendarControl1);

        erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero = KikeroFelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Kikero = pageView.GetUpdatedByView(KikeroFelhasznaloCsoportTextBox);

        erec_IrattariKikero.KeresDatuma = KeresDatumaCalendarControl.Text;
        erec_IrattariKikero.Updated.KeresDatuma = pageView.GetUpdatedByView(KeresDatumaCalendarControl);

        erec_IrattariKikero.FelhasznaloCsoport_Id_Jovahagy = JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Jovahagy = pageView.GetUpdatedByView(JovahagyoFelhasznaloCsoportTextBox);

        erec_IrattariKikero.JovagyasDatuma = JovagyasIdejeCalendarControl.Text;
        erec_IrattariKikero.Updated.JovagyasDatuma = pageView.GetUpdatedByView(JovagyasIdejeCalendarControl);

        erec_IrattariKikero.KiadasDatuma = KiadasDatumaCalendarControl.Text;
        erec_IrattariKikero.Updated.KiadasDatuma = pageView.GetUpdatedByView(KiadasDatumaCalendarControl);

        erec_IrattariKikero.FelhasznaloCsoport_Id_Visszaad = VisszaadoFelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Visszaad = pageView.GetUpdatedByView(VisszaadoFelhasznaloCsoportTextBox);

        erec_IrattariKikero.VisszaadasDatuma = VisszaadasDatumaCalendarControl.Text;
        erec_IrattariKikero.Updated.VisszaadasDatuma = pageView.GetUpdatedByView(VisszaadasDatumaCalendarControl);

        erec_IrattariKikero.FelhasznaloCsoport_Id_Visszave = VisszavevoFelhasznaloCsoportTextBox.Id_HiddenField;
        erec_IrattariKikero.Updated.FelhasznaloCsoport_Id_Visszaad = pageView.GetUpdatedByView(VisszavevoFelhasznaloCsoportTextBox);

        //erec_IrattariKikero.VisszavetelDatuma = VisszavetelDatumaCalendarControl.Text;
        //erec_IrattariKikero.Updated.VisszavetelDatuma = pageView.GetUpdatedByView(VisszavetelDatumaCalendarControl);

        erec_IrattariKikero.Base.Ver = FormHeader1.Record_Ver;
        erec_IrattariKikero.Base.Updated.Ver = true;

        return erec_IrattariKikero;
    }

    //private void Load_ComponentSelectModul()
    //{
    //    if (compSelector == null) { return; }
    //    else
    //    {
    //        compSelector.Enabled = true;

    //        compSelector.Add_ComponentOnClick(DokumenumTipusRadioButtonList);
    //        compSelector.Add_ComponentOnClick(UgyiratTextBox1);
    //        compSelector.Add_ComponentOnClick(Keres_noteTextBox);
    //        compSelector.Add_ComponentOnClick(Kiker_mettolKiker_meddigErvenyessegCalendarControl1);
    //        compSelector.Add_ComponentOnClick(AllapotKodtarakDropDownList);
    //        compSelector.Add_ComponentOnClick(KikeroFelhasznaloCsoportTextBox);
    //        compSelector.Add_ComponentOnClick(KeresDatumaCalendarControl);
    //        compSelector.Add_ComponentOnClick(JovahagyoFelhasznaloCsoportTextBox);
    //        compSelector.Add_ComponentOnClick(JovagyasIdejeCalendarControl);
    //        compSelector.Add_ComponentOnClick(KiadasDatumaCalendarControl);
    //        compSelector.Add_ComponentOnClick(VisszaadoFelhasznaloCsoportTextBox);
    //        compSelector.Add_ComponentOnClick(VisszaadasDatumaCalendarControl);

    //        FormFooter1.SaveEnabled = false;
    //    }
    //}


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (Command == CommandName.KikeresAtmenetiIrattarbolJovahagyas)
        {
            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();

            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            List<string> selectedItemsList = ui.GetGridViewSelectedRows(KikerokGridView, FormHeader1.ErrorPanel, null);

            if (e.CommandName == CommandName.KolcsonzesJovahagyas)
            {
                Result result = service.KikeresJovahagyas_Tomeges(execParam, selectedItemsList.ToArray());

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (selectedItemsList.Count > 0)
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, selectedItemsList[0]);
                    }
                    JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }

            }
            else if (e.CommandName == CommandName.KolcsonzesVisszautasitas)
            {
                Result result = service.KikeresVisszautasitas_Tomeges(execParam, selectedItemsList.ToArray());

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    if (selectedItemsList.Count > 0)
                    {
                        JavaScripts.RegisterSelectedRecordIdToParent(Page, selectedItemsList[0]);
                    }
                    JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else
        #region Kolcsonzés jóváhagyása
        if (e.CommandName == CommandName.KolcsonzesJovahagyas)
        {

            //if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + Command))
            {
                EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                EREC_IrattariKikero erec_IrattariKikero = GetBusinessObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                String ugyiratId = Request.QueryString.Get(QueryStringVars.Id);

                if (!String.IsNullOrEmpty(ugyiratId))
                {
                    execParam.Record_Id = ugyiratId;
                }
                else
                {
                    execParam.Record_Id = UgyiratTextBox1.Id_HiddenField;
                }




                Result result = service.KolcsonzesJovahagyas(execParam);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    // ha egy másik formról hívták meg, adatok visszaküldése:
                    // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                    //    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                    //    {
                    //        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    //    }
                    //    else
                    //    {
                    //        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                    //    }

                    // BUG_8724
                    // Van rá automatikus ertesítés, be kell kapcsolni
                    //#region BUG_6386 e-mail küldése

                    //List<string> addresses = new List<string>();

                    //EREC_UgyUgyiratokService ugyiratokService = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService();
                    //EREC_UgyUgyiratok ugyirat = ugyiratokService.Get(execParam).Record as EREC_UgyUgyiratok;


                    //EREC_IraIrattariTetelekService irattariTetelekService = eRecordService.ServiceFactory.GetEREC_IraIrattariTetelekService();
                    //ExecParam execParamIrattariTetelek = UI.SetExecParamDefault(Page, new ExecParam());
                    //execParamIrattariTetelek.Record_Id = ugyirat.IraIrattariTetel_Id;



                    //EREC_IraIrattariTetelek irattariTetel = irattariTetelekService.Get(execParamIrattariTetelek).Record as EREC_IraIrattariTetelek;
                    //string irattariTetelSzam = "";
                    //if (irattariTetel != null)
                    //{
                    //    irattariTetelSzam = irattariTetel.IrattariTetelszam;
                    //}


                    //KRT_CsoportokService csoportokService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
                    //KRT_CsoportokSearch csoportokSearch = new KRT_CsoportokSearch();

                    //KRT_CsoportTagokService csoporttagokService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_CsoportTagokService();
                    //KRT_CsoportTagokSearch csoporttagokSearch = new KRT_CsoportTagokSearch();

                    //KRT_FelhasznalokService felhasznalokService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetKRT_FelhasznalokService();
                    ////KRT_FelhasznalokSearch felhasznalokSearch = new KRT_FelhasznalokSearch();



                    ////Központi irattár csoport Id lekérése
                    //string csoportId = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(execParam).Obj_Id;

                    //csoporttagokSearch.Csoport_Id.Value = csoportId;
                    //csoporttagokSearch.Csoport_Id.Operator = Query.Operators.equals;
                    //ExecParam execParamCsoportTagok = UI.SetExecParamDefault(Page, new ExecParam());

                    //Result resultCsoportTagok = csoporttagokService.GetAll(execParamCsoportTagok, csoporttagokSearch);

                    //ExecParam execParamFelhasznalo = UI.SetExecParamDefault(Page, new ExecParam());
                    //string email = "";
                    //foreach (DataRow row in resultCsoportTagok.Ds.Tables[0].Rows)
                    //{
                    //    if (!string.IsNullOrEmpty(row["Csoport_Id_Jogalany"].ToString()))
                    //    {
                    //        execParamFelhasznalo.Record_Id = row["Csoport_Id_Jogalany"].ToString();
                    //        email = (felhasznalokService.Get(execParamFelhasznalo).Record as KRT_Felhasznalok).EMail;

                    //        if (!string.IsNullOrEmpty(email))
                    //        {
                    //            if (Contentum.eUtility.RegExp.CheckValidEmailAddress(email))
                    //            {
                    //                addresses.Add(email);
                    //            }
                    //        }
                    //    }
                    //}

                    //execParamFelhasznalo.Record_Id = erec_IrattariKikero.FelhasznaloCsoport_Id_Kikero;
                    //string kolcsonzo = (felhasznalokService.Get(execParamFelhasznalo).Record as KRT_Felhasznalok).Nev;

                    //Contentum.eAdmin.Service.EmailService emailService = Contentum.eAdmin.Utility.eAdminService.ServiceFactory.GetEmailService();

                    ////string uzenet = string.Format("Ügyiat száma: {0} \n Irattári tételszám: {1} \n Iratot kölcsönző ügyintéző neve:", ugyirat.Azonosito, irattariTetelSzam, kolcsonzo);
                    //string targy = "[Contentum] E-mail értesítés kölcsönzési igényről";

                    //EREC_eMailBoritekok erec_eMailBoritekok = new EREC_eMailBoritekok();
                    //erec_eMailBoritekok.Felado = Rendszerparameterek.Get(execParam, "UGYFELSZOLGALAT_EMAIL_CIM");
                    ////erec_eMailBoritekok.Uzenet = uzenet;
                    //erec_eMailBoritekok.Targy = targy;
                    //erec_eMailBoritekok.Cimzett = String.Join(", ", addresses.ToArray());

                    ////Teszt
                    ////erec_eMailBoritekok.Cimzett = "Zsolt.Lakk@4ig.hu";

                    //try
                    //{
                    //    bool ret = emailService.SendEmailToKozpontiIrattarosok(execParam, erec_eMailBoritekok, ugyirat.Azonosito, irattariTetelSzam, kolcsonzo);
                    //}
                    //catch (Exception ex)
                    //{
                    //}

                    //#endregion

                    JavaScripts.RegisterCloseWindowClientScriptforChrome(Page, true);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else
        #endregion
        #region Kölcsönzés elutasítása
        if (e.CommandName == CommandName.KolcsonzesVisszautasitas)
        {
            //if (FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + Command))
            {
                EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                EREC_IrattariKikero erec_IrattariKikero = GetBusinessObjectFromComponents();

                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                String ugyiratId = Request.QueryString.Get(QueryStringVars.Id);

                if (!String.IsNullOrEmpty(ugyiratId))
                {
                    execParam.Record_Id = ugyiratId;
                }
                else
                {
                    execParam.Record_Id = UgyiratTextBox1.Id_HiddenField;
                }

                Result result = service.KolcsonzesVisszautasitas(execParam);

                if (String.IsNullOrEmpty(result.ErrorCode))
                {
                    // ha egy másik formról hívták meg, adatok visszaküldése:
                    // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                    //    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                    //    {
                    //        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                    //    }
                    //    else
                    //    {
                    //        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                    //    }
                    JavaScripts.RegisterCloseWindowClientScriptforChrome(Page, true);
                }
                else
                {
                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                }
            }
        }
        else
        #endregion
        if (e.CommandName == CommandName.Save)
        {
            switch (Command)
            {
                case CommandName.New:
                    {
                        try
                        {
                            this.ValidateInputParameters();

                            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                            EREC_IrattariKikero erec_IrattariKikero = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            String id = Request.QueryString.Get(QueryStringVars.Id);

                            if (!String.IsNullOrEmpty(id))
                            {
                                execParam.Record_Id = id;
                            }
                            else
                            {
                                execParam.Record_Id = UgyiratTextBox1.Id_HiddenField;
                            }

                            Result result = null;

                            if (hiddenWhitoutLeaderCheck.Value == "1")
                            {
                                result = service.Kolcsonzes(execParam, erec_IrattariKikero, true);
                            }
                            else
                            {
                                result = service.Kolcsonzes(execParam, erec_IrattariKikero, false);
                            }

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                // ha egy másik formról hívták meg, adatok visszaküldése:
                                // ha sikerult visszakuldeni a hivo formra az adatokat, akkor egy masik formrol nyitottak meg es akkor nem kell postback a hivo formon!!!
                                //    if (JavaScripts.SendBackResultToCallingWindow(Page, result.Uid, Nev_TextBox.Text))
                                //    {
                                //        JavaScripts.RegisterCloseWindowClientScript(Page, false);
                                //    }
                                //    else
                                //    {
                                //        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                //    }
                                JavaScripts.RegisterCloseWindowClientScriptforChrome(Page, true);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                        }
                        catch (FormatException fe)
                        {
                            ResultError.DisplayFormatExceptionOnErrorPanel(FormHeader1.ErrorPanel, fe);
                        }
                        break;
                    }
                case CommandName.KikeresAtmenetiIrattarbol:
                    {
                        if (FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarKolcsonzes"))
                        {
                            try
                            {
                                this.ValidateInputParameters();

                                EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                                EREC_IrattariKikero erec_IrattariKikero = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                                Result result = null;
                                if (hiddenWhitoutLeaderCheck.Value == "1")
                                {
                                    result = service.KikeresAtmenetiIrattarbol_Tomeges(execParam, selectedItemsList.ToArray(), erec_IrattariKikero, true);
                                }
                                else
                                {
                                    result = service.KikeresAtmenetiIrattarbol_Tomeges(execParam, selectedItemsList.ToArray(), erec_IrattariKikero, false);
                                }

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    if (selectedItemsList.Count > 0)
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, selectedItemsList[0]);
                                    }
                                    JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                                }
                            }
                            catch (FormatException fe)
                            {
                                ResultError.DisplayFormatExceptionOnErrorPanel(FormHeader1.ErrorPanel, fe);
                            }
                        }
                        else
                        {
                            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                        }
                        break;
                    }
                case CommandName.KikeresSkontroIrattarbol:
                    {
                        if (FunctionRights.GetFunkcioJog(Page, "KikeresSkontroIrattarbol"))
                        {
                            try
                            {
                                this.ValidateInputParameters();

                                EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                                EREC_IrattariKikero erec_IrattariKikero = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                                Result result = service.KikeresSkontroIrattarbol_Tomeges(execParam, selectedItemsList.ToArray(), erec_IrattariKikero);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    if (selectedItemsList.Count > 0)
                                    {
                                        JavaScripts.RegisterSelectedRecordIdToParent(Page, selectedItemsList[0]);
                                    }
                                    JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                                }
                            }
                            catch (FormatException fe)
                            {
                                ResultError.DisplayFormatExceptionOnErrorPanel(FormHeader1.ErrorPanel, fe);
                            }
                        }
                        else
                        {
                            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                        }
                        break;
                    }
                case CommandName.KiadasAtmenetiIrattarbol:
                    {
                        if (FunctionRights.GetFunkcioJog(Page, "IrattarKolcsonzesKiadasa"))
                        {
                            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                            List<string> selectedItemsList = ui.GetGridViewSelectedRows(UgyUgyiratokGridView, FormHeader1.ErrorPanel, null);

                            Result result = service.KiadasAtmenetiIrattarbol_Tomeges(execParam, selectedItemsList.ToArray());

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                if (selectedItemsList.Count > 0)
                                {
                                    JavaScripts.RegisterSelectedRecordIdToParent(Page, selectedItemsList[0]);
                                }
                                JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                UI.MarkFailedRecordsInGridView(UgyUgyiratokGridView, result);
                            }
                        }
                        else
                        {
                            UI.DisplayDontHaveFunctionRights(FormHeader1.ErrorPanel, null);
                        }
                        break;
                    }
                case CommandName.Modify:
                    {
                        String recordId = Request.QueryString.Get(QueryStringVars.Id);
                        if (String.IsNullOrEmpty(recordId))
                        {
                            // nincs Id megadva:
                            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                        }
                        else
                        {
                            EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
                            EREC_IrattariKikero erec_IrattariKikero = GetBusinessObjectFromComponents();

                            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                            execParam.Record_Id = recordId;

                            Result result = service.KolcsonzesUpdate(execParam, erec_IrattariKikero);

                            if (String.IsNullOrEmpty(result.ErrorCode))
                            {
                                JavaScripts.RegisterCloseWindowClientScriptforChrome(Page);
                            }
                            else
                            {
                                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                            }
                        }
                        break;
                    }
            }
        }
        else
        {
            UI.RedirectToAccessDeniedErrorPage(Page);
        }
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            RadioButtonList rb = (RadioButtonList)sender;
            if (rb.ID == FelhasznalasiCelRadioButtonList.ID)
            {
                string SelectedValue = rb.SelectedValue;
                switch (SelectedValue)
                {
                    case "B":
                        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvVege = DateTime.Today.AddDays(15).ToShortDateString();
                        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.Validate = true;
                        break;
                    case "U":
                        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvVege = "";
                        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.Validate = false;
                        break;
                    default:
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            ResultError.DisplayErrorOnErrorPanel(FormHeader1.ErrorPanel, Resources.Error.DefaultErrorHeader, ex.Message);
        }
    }

    private void SetViewMode(Boolean value)
    {
        DokumenumTipusRadioButtonList.Enabled = !value;
        UgyiratTextBox1.ReadOnly = value;
        Keres_noteTextBox.ReadOnly = value;
        FelhasznalasiCelRadioButtonList.Enabled = !value;
        Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ReadOnly = value;
        KikeroFelhasznaloCsoportTextBox.ReadOnly = value;
        KeresDatumaCalendarControl.ReadOnly = value;
        JovahagyoFelhasznaloCsoportTextBox.ReadOnly = value;
        JovagyasIdejeCalendarControl.ReadOnly = value;
        KiadasDatumaCalendarControl.ReadOnly = value;
        Kiado_FelhasznaloCsoportTextBox1.ReadOnly = value;
        VisszaadoFelhasznaloCsoportTextBox.ReadOnly = value;
        VisszaadasDatumaCalendarControl.ReadOnly = value;
    }

    protected void ValidateInputParameters()
    {
        string sKolcsonzesKezdete = Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvKezd.Trim();
        string sKolcsonzesVege = Kiker_mettolKiker_meddigErvenyessegCalendarControl1.ErvVege.Trim();
        if (String.IsNullOrEmpty(sKolcsonzesKezdete))
        {
            throw new FormatException(UI.GetRequeredTimeMessage("kölcsönzés kezdete"));
        }
        if (String.IsNullOrEmpty(sKolcsonzesVege))
        {
            throw new FormatException(UI.GetRequeredTimeMessage("kölcsönzés vége"));
        }

        DateTime dtKolcsonzesKezdete = UI.GetDateTimeFromString(sKolcsonzesKezdete, "kölcsönzés kezdete");
        UI.CheckDateTimeIsFutureDate(dtKolcsonzesKezdete, "kölcsönzés kezdete");
        DateTime dtKolconzesVege = UI.GetDateTimeFromString(sKolcsonzesVege, "kölcsönzés vége");
        UI.CheckDateTimeIsFutureDate(dtKolconzesVege, "kölcsönzés vége");

        if (dtKolcsonzesKezdete > dtKolconzesVege)
        {
            throw new FormatException("A kölcsönzés vége dátum nem lehet korábbi a kölcsönzés kezdeti dátumánál!");
        }
    }


}
