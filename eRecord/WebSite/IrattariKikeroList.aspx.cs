using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
//using Contentum.eAdmin.Service;
using Contentum.eRecord.Utility;

public partial class IrattariKikeroList : Contentum.eUtility.UI.PageBase
{
    UI ui = new UI();

    private String Startup = "";

    #region Base Page

    protected void Page_PreInit(object sender, EventArgs e)
    {
        Startup = Page.Request.QueryString.Get(Constants.Startup.StartupName);

        //if (Startup != Constants.Startup.FromAtmenetiIrattar && Startup != Constants.Startup.FromKozpontiIrattar)
        //{
        //    UI.RedirectToAccessDeniedErrorPage(Page);
        //}

        //if (Startup == Constants.Startup.FromAtmenetiIrattar)
        //{
        
        // TODO:    
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "FelhasznalokList");

        //}
        //if (Startup == Constants.Startup.FromKozpontiIrattar)
        //{
        //    FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "KozpontiIrattarList");
        //}
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        ListHeader1.RowCount_Changed += new EventHandler(ListHeader1_RowCount_Changed);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ListHeader1.HeaderLabel = Resources.List.IrattariKikeroListHeaderTitle;

        ListHeader1.SetRightFunctionButtonsVisible(false);

        ListHeader1.SearchObjectType = typeof(EREC_UgyUgyiratokSearch);


        if (EErrorPanel1.Visible == true)
        {
            EErrorPanel1.Visible = false;
            ErrorUpdatePanel.Update();
        }

        JavaScripts.RegisterPopupWindowClientScript(Page);

        ListHeader1.LeftFunkcionButtonsClick += new CommandEventHandler(ListHeaderLeftFunkcionButtonsClick);
        ListHeader1.RightFunkcionButtonsClick += new CommandEventHandler(ListHeaderRightFunkcionButtonsClick);

        ListHeader1.NewOnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
            , QueryStringVars.Command + "=" + CommandName.New, Defaults.PopupWidth, Defaults.PopupHeight, IrattariKikeroUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.SearchOnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroSearch.aspx", ""
            , Defaults.PopupWidth_Max, Defaults.PopupHeight_Max, IrattariKikeroUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

        ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();
        ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.RefreshOnClientClick = JavaScripts.SetOnClientClickRefreshMasterList(IrattariKikeroGridView.ClientID);

        ListHeader1.NewVisible = true;

        ListHeader1.KolcsonzesJovahagyasVisible = true;
        ListHeader1.KolcsonzesJovahagyasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.KolcsonzesVisszautasitasVisible = true;
        ListHeader1.KolcsonzesVisszautasitasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.KolcsonzesSztornoVisible = true;
        ListHeader1.KolcsonzesSztornoOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.KolcsonzesKiadasVisible = true;
        ListHeader1.KolcsonzesKiadasOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        ListHeader1.KolcsonzesVisszaVisible = true;
        ListHeader1.KolcsonzesVisszaOnClientClick = JavaScripts.SetOnClientClickNoSelectedRow();

        //ListHeader1.LockOnClientClick = JavaScripts.SetOnClientClickLockConfirm(UgyUgyiratokGridView.ClientID);
        //ListHeader1.UnlockOnClientClick = JavaScripts.SetOnClientClickUnlockConfirm(UgyUgyiratokGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick = JavaScripts.SetOnClientClickSendObjectsConfirm(IrattariKikeroGridView.ClientID);

        ListHeader1.SendObjectsOnClientClick += JavaScripts.SetOnClientClick("FelhasznalokMultiSelect.aspx",
           QueryStringVars.HiddenFieldId + "=" + HiddenField1.ClientID + "&" + QueryStringVars.MessageHiddenFieldId + "=" + MessageHiddenField.ClientID
                    , Defaults.PopupWidth, Defaults.PopupHeight, IrattariKikeroUpdatePanel.ClientID, "", true);

        //selectedRecordId kezel�se
        ListHeader1.AttachedGridView = IrattariKikeroGridView;

        /* Breaked Records */
        Search.SetIdsToSearchObject(Page, "Id", new EREC_IrattariKikeroSearch());

        if (!IsPostBack) IrattariKikeroGridViewBind();

        ui.SetClientScriptToGridViewSelectDeSelectButton(IrattariKikeroGridView);

        //scroll �llapot�nak ment�se
        JavaScripts.RegisterScrollManagerScript(Page);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        ListHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.New);

        ListHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.View);

        ListHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify);

        ListHeader1.KolcsonzesKiadasEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify); ;
        ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify); ;
        ListHeader1.KolcsonzesJovahagyasEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify); ;
        ListHeader1.KolcsonzesVisszautasitasEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify); ;
        ListHeader1.KolcsonzesKiadasEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify); ;
        ListHeader1.KolcsonzesSztornoEnabled = FunctionRights.GetFunkcioJog(Page, "Felhasznalo" + CommandName.Modify); ;

        ListHeader1.InvalidateEnabled = false;
        ListHeader1.HistoryEnabled = false;

        //if (Startup == Constants.Startup.FromAtmenetiIrattar)
        //{
        //    Ugyiratok.SetAtmenetiIrattarKolcsonzesFunkciogomb(null, null, ListHeader1.Kolcsonzes, null);
        //    ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarKolcsonzesVisszavetel"); ;
        //    ListHeader1.IrattarozasraEnabled = FunctionRights.GetFunkcioJog(Page, "AtmenetiIrattarAtadasKozpontiIrattarba"); ;
        //}

        //if (Startup == Constants.Startup.FromKozpontiIrattar)
        //{
        //    Ugyiratok.SetKozpontiIrattarKolcsonzesFunkciogomb(null, null, ListHeader1.Kolcsonzes, null);
        //    ListHeader1.KolcsonzesEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarKolcsonzes");
        //    ListHeader1.KolcsonzesVisszaEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarKolcsonzesVisszavetel"); ;
        //    ListHeader1.JegyzekrehelyezesEnabled = FunctionRights.GetFunkcioJog(Page, "KozpontiIrattarJegyzekrehelyezes"); ;
        //}

        //ListHeader1.LockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);
        //ListHeader1.UnlockEnabled = FunctionRights.GetFunkcioJog(Page, "Ugyirat" + CommandName.Lock);

        //if (IsPostBack)
        {
            String MasterListSelectedRowId = UI.GetGridViewSelectedRecordId(IrattariKikeroGridView);

            RefreshOnClientClicksByMasterListSelectedRow(MasterListSelectedRowId);
        }
    }

    #endregion


    #region Master List

    protected void IrattariKikeroGridViewBind()
    {
        String sortExpression = Search.GetSortExpressionFromViewState("IrattariKikeroGridView", ViewState, "LetrehozasIdo");
        SortDirection sortDirection = Search.GetSortDirectionFromViewState("IrattariKikeroGridView", ViewState);

        IrattariKikeroGridViewBind(sortExpression, sortDirection);
    }

    protected void IrattariKikeroGridViewBind(String SortExpression, SortDirection SortDirection)
    {

        Contentum.eRecord.Service.EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();

        ExecParam ExecParam = UI.SetExecParamDefault(Page, new ExecParam());

        EREC_IrattariKikeroSearch search = (EREC_IrattariKikeroSearch)Search.GetSearchObject(Page, new EREC_IrattariKikeroSearch());
        search.OrderBy = Search.GetOrderBy("IrattariKikeroGridView", ViewState, SortExpression, SortDirection);

        Result res = service.GetAllWithExtension(ExecParam, search);

        UI.GridViewFill(IrattariKikeroGridView, res, ListHeader1, EErrorPanel1, ErrorUpdatePanel);
    }

    protected void IrattariKikeroGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        UI.GridView_RowDataBound_SetLockingInfo(e, Page);
    }

    protected void IrattariKikeroGridView_PreRender(object sender, EventArgs e)
    {
        int prev_PageIndex = IrattariKikeroGridView.PageIndex;

        IrattariKikeroGridView.PageIndex = ListHeader1.PageIndex;
        ListHeader1.PageCount = IrattariKikeroGridView.PageCount;

        if (prev_PageIndex != IrattariKikeroGridView.PageIndex)
        {
            //scroll �llapot�nak t�rl�se
            JavaScripts.ResetScroll(Page, IrattariKikeroCPE);
            IrattariKikeroGridViewBind();
            //ActiveTabClear();
        }
        else
        {
            UI.GridViewSetScrollable(ListHeader1.Scrollable, IrattariKikeroCPE);
        }

        ListHeader1.PagerLabel = UI.GetGridViewPagerLabel(IrattariKikeroGridView);
    }


    void ListHeader1_RowCount_Changed(object sender, EventArgs e)
    {
        IrattariKikeroGridViewBind();
    }

    protected void IrattariKikeroGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int selectedRowNumber = int.Parse(e.CommandArgument.ToString());
            UI.SetGridViewCheckBoxesToSelectedRow(IrattariKikeroGridView, selectedRowNumber, "check");
        }
    }

    private void RefreshOnClientClicksByMasterListSelectedRow(string id)
    {
        if (!String.IsNullOrEmpty(id))
        {
            ListHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
               , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + id
               , Defaults.PopupWidth, Defaults.PopupHeight, IrattariKikeroUpdatePanel.ClientID);

            ListHeader1.ModifyOnClientClick = JavaScripts.SetOnClientClick("IrattariKikeroForm.aspx"
                , QueryStringVars.Command + "=" + CommandName.Modify + "&" + QueryStringVars.Id + "=" + id
                , Defaults.PopupWidth, Defaults.PopupHeight, IrattariKikeroUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);

            //string tableName = "EREC_IrattariKikero";
            //ListHeader1.HistoryOnClientClick = JavaScripts.SetOnClientClick("RecordHistory.aspx"
            //    , QueryStringVars.Id + "=" + id + "&" + QueryStringVars.TableName + "=" + tableName
            //    , Defaults.HistoryPopupWidth, Defaults.HistoryPopupHeight, IrattariKikeroUpdatePanel.ClientID);

            // Allapot szerint beallitja a headerben a gombok viselkedeset:
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
            IrattariKikero.Statusz statusz = IrattariKikero.GetAllapotById(id, execparam, EErrorPanel1);

            // KolcsonzesJovahagyas:
            IrattariKikero.SetKolcsonzesJovahagyas(id, statusz, execparam, ListHeader1.KolcsonzesJovahagyas);

            // KolcsonzesVisszautas:
            IrattariKikero.SetKolcsonzesVisszautasitas(id, statusz, execparam, ListHeader1.KolcsonzesVisszautasitas);

            // KolcsonzesSztorno:
            IrattariKikero.SetKolcsonzesSztorno(id, statusz, execparam, ListHeader1.KolcsonzesSztorno);

            // KolcsonzesKiadas:
            IrattariKikero.SetKolcsonzesKiadas(id, statusz, execparam, ListHeader1.KolcsonzesKiadas);

            // KolcsonzesVissza:
            IrattariKikero.SetKolcsonzesVissza(id, statusz, execparam, ListHeader1.KolcsonzesVissza);
        }
    }

    protected void IrattariKikeroUpdatePanel_Load(object sender, EventArgs e)
    {
        if (Request.Params["__EVENTARGUMENT"] != null)
        {
            string eventArgument = Request.Params["__EVENTARGUMENT"].ToString();
            switch (eventArgument)
            {
                case EventArgumentConst.refreshMasterList:
                    IrattariKikeroGridViewBind();
                    break;
            }
        }
    }

    private void ListHeaderLeftFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Invalidate)
        {
            deleteSelectedIrattariKikero();
            IrattariKikeroGridViewBind();
        }
    }

    private void ListHeaderRightFunkcionButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        String Id = UI.GetGridViewSelectedRecordId(IrattariKikeroGridView);
        if (String.IsNullOrEmpty(Id))
        {
            ResultError.DisplayNoIdParamError(EErrorPanel1);
            ErrorUpdatePanel.Update();        
        }
        switch (e.CommandName)
        {
            //case CommandName.Lock:
            //    LockSelectedIrattariKikeroRecords();
            //    IrattariKikeroGridViewBind();
            //    break;
            //case CommandName.Unlock:
            //    UnlockSelectedIrattariKikeroRecords();
            //    IrattariKikeroGridViewBind();
            //    break;

            case CommandName.KolcsonzesJovahagyas:
            case CommandName.KolcsonzesVisszautasitas:
            case CommandName.KolcsonzesSztorno:
            case CommandName.KolcsonzesKiadas:
            case CommandName.KolcsonzesVissza:
                KolcsonzesFunkciokSelectedIrattariKikero(Id, e.CommandName);
                break;
            case CommandName.SendObjects:
                SendMailSelectedIrattariKikero();
                break;
        }
    }
    private void KolcsonzesFunkciokSelectedIrattariKikero(String Id, String Command)
    {
        if (FunctionRights.GetFunkcioJog(Page, "FelhasznaloNew"))
        {            
            EREC_IrattariKikeroService erec_IrattariKikeroService = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
            ExecParam execparam = UI.SetExecParamDefault(Page, new ExecParam());
            execparam.Record_Id = Id;
            Result result = new Result();
            switch (Command)
            {
                case CommandName.KolcsonzesJovahagyas:
                    result = erec_IrattariKikeroService.KolcsonzesJovahagyas(execparam);
                    break;
                case CommandName.KolcsonzesVisszautasitas:
                    result = erec_IrattariKikeroService.KolcsonzesVisszautasitas(execparam);
                    break;
                case CommandName.KolcsonzesSztorno:
                    result = erec_IrattariKikeroService.KolcsonzesSztorno(execparam);
                    break;
                case CommandName.KolcsonzesKiadas:
                    result = erec_IrattariKikeroService.KolcsonzesKiadas(execparam);
                    break;
                case CommandName.KolcsonzesVissza:
                    result = erec_IrattariKikeroService.KolcsonzesVissza(execparam);
                    break;
            }
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
            IrattariKikeroGridViewBind();
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
            ErrorUpdatePanel.Update();
        }
    }


    //private void LockSelectedIrattariKikeroRecords()
    //{
    //    LockManager.LockSelectedGridViewRecords(IrattariKikeroGridView, "EREC_IrattariKikero"
    //        , "KezbesitesiTetelLock", "KezbesitesiTetelForceLock"
    //         , Page, EErrorPanel1, ErrorUpdatePanel);
    //}

    //private void UnlockSelectedIrattariKikeroRecords()
    //{
    //    LockManager.UnlockSelectedGridViewRecords(IrattariKikeroGridView, "EREC_IrattariKikero"
    //        , "KezbesitesiTetelLock", "KezbesitesiTetelForceLock"
    //        , Page, EErrorPanel1, ErrorUpdatePanel);
    //}

    /// <summary>
    /// T�rli a IrattariKikeroGridView -ban kijel�lt elemeket
    /// </summary>
    private void deleteSelectedIrattariKikero()
    {
        if (FunctionRights.GetFunkcioJog(Page, "KezbesitesiTetelInvalidate"))
        {
            List<string> deletableItemsList = ui.GetGridViewSelectedRows(IrattariKikeroGridView, EErrorPanel1, ErrorUpdatePanel);
            Contentum.eRecord.Service.EREC_IrattariKikeroService service = eRecordService.ServiceFactory.GetEREC_IrattariKikeroService();
            ExecParam[] execParams = new ExecParam[deletableItemsList.Count];
            for (int i = 0; i < deletableItemsList.Count; i++)
            {
                execParams[i] = UI.SetExecParamDefault(Page, new ExecParam());
                execParams[i].Record_Id = deletableItemsList[i];
            }
            Result result = service.MultiInvalidate(execParams);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
                ErrorUpdatePanel.Update();
            }
        }
        else
        {
            UI.DisplayDontHaveFunctionRights(EErrorPanel1, ErrorUpdatePanel);
        }
    }

    /// <summary>
    /// Elkuldi emailben a IraErkeztetoKonyvekGridView -ban kijel�lt elemek hivatkozasait
    /// </summary>
    private void SendMailSelectedIrattariKikero()
    {
        if (!String.IsNullOrEmpty(HiddenField1.Value))
        {
            Notify.SendSelectedItemsByEmail(Page, ui.GetGridViewSelectedRows(IrattariKikeroGridView, EErrorPanel1, ErrorUpdatePanel)
                , UI.StringToListBySeparator(HiddenField1.Value, ';'), MessageHiddenField.Value, "EREC_IrattariKikero");
        }
    }

    protected void IrattariKikeroGridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        IrattariKikeroGridViewBind(e.SortExpression, UI.GetSortToGridView("IrattariKikeroGridView", ViewState, e.SortExpression));
    }

    #endregion


}
