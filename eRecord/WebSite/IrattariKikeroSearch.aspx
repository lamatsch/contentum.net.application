<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IrattariKikeroSearch.aspx.cs" Inherits="IrattariKikeroSearch" Title="Untitled Page" %>

<%@ Register Src="eRecordComponent/UgyiratTextBox.ascx" TagName="UgyiratTextBox"
    TagPrefix="uc12" %>

<%@ Register Src="Component/FelhasznaloCsoportTextBox.ascx" TagName="FelhasznaloCsoportTextBox" TagPrefix="uc8" %>
<%@ Register Src="Component/DatumIntervallum_SearchCalendarControl.ascx" TagName="DatumIntervallum_SearchCalendarControl" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc10" %>
<%@ Register Src="Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="Component/CalendarControl.ascx" TagName="CalendarControl" TagPrefix="uc5" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %> 
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent" TagPrefix="uc9" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredNumberBox.ascx" TagName="RequiredNumberBox" TagPrefix="uc4" %>
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="width: 868px">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr runat="server" class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label12" runat="server" Text="T�pus:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:RadioButtonList ID="DokumentumTipusRadioButtonList" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged"
                                    RepeatDirection="Horizontal" Width="260px">
                                    <asp:ListItem Value="0">&#220;gyirat</asp:ListItem>
                                    <asp:ListItem Value="1">T&#233;rtivev&#233;ny</asp:ListItem>
                                    <asp:ListItem Value="2">&#214;sszes</asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label16" runat="server" Text="Azonos�t�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc12:UgyiratTextBox ID="UgyiratTextBox1" runat="server" SearchMode="true" />
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="K�r�s indoka:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:TextBox ID="Keres_notTextBox" runat="server" Width="600px"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label13" runat="server" Text="Felhaszn�l�s c�lja:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:RadioButtonList ID="FelhasznalasiCelRadioButtonList" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged"
                                    RepeatDirection="Horizontal" Width="260px">
                                    <asp:ListItem Value="0">Betekint&#233;sre</asp:ListItem>
                                    <asp:ListItem Value="1">&#220;gyint&#233;z&#233;sre</asp:ListItem>
                                    <asp:ListItem Value="2">&#214;sszes</asp:ListItem>
                                </asp:RadioButtonList></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label11" runat="server" Text="K�lcs�nz�s id�pontja:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="Kiker_mettolDatumIntervallum_SearchCalendarControl"
                                    runat="server" ValidateDateFormat="true" />
                                </td>
                            <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" Text="K�lcs�nz�si hat�rid�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="Kiker_meddigDatumIntervallum_SearchCalendarControl"
                                    runat="server" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor" id="tr_Tipus_dropdown" runat="server">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label4" runat="server" Text="K�lcs�nz�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc8:FelhasznaloCsoportTextBox ID="KikeroFelhasznaloCsoportTextBox" runat="server" SearchMode="true" />
                                </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label3" runat="server" Text="K�r�s ideje:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="KeresDatumaDatumIntervallum_SearchCalendarControl"
                                    runat="server" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="J�v�hagy�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc8:FelhasznaloCsoportTextBox ID="JovahagyoFelhasznaloCsoportTextBox" runat="server" SearchMode="true" />
                                </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label6" runat="server" Text="J�v�hagy�s d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="JovagyasIdejeDatumIntervallum_SearchCalendarControl"
                                    runat="server" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="�llapot:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc11:KodtarakDropDownList ID="AllapotKodtarakDropDownList" runat="server" />
                                </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="Kiad�s d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="KiadasDatumaDatumIntervallum_SearchCalendarControl"
                                    runat="server" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label9" runat="server" Text="�tvev�:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc8:FelhasznaloCsoportTextBox ID="KaptaFelhasznaloCsoportTextBox" runat="server" SearchMode="true" />
                                </td>
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label10" runat="server" Text="Visszaad�s d�tuma:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <uc7:DatumIntervallum_SearchCalendarControl ID="VisszaadasDatumaDatumIntervallum_SearchCalendarControl"
                                    runat="server" ValidateDateFormat="true" />
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td colspan="4" >
                            </td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td colspan="4">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />
                            </td>
                        </tr>
                    </table>
                    </eUI:eFormPanel>
                    
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>                      
                      </table>

                </td>
            </tr>
        </table>
</asp:Content>

