using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;

// a teljes kód kidolgozandó!!!

public partial class IrattariKikeroSearch : Contentum.eUtility.UI.PageBase
{
    private Type _type = typeof(EREC_IrattariKikeroSearch);
    private const string KodCsoportIrattariKikeroAllapot = "IRATTARIKIKEROALLAPOT";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
            new CommandEventHandler(SearchFooter1_ButtonsClick);

        SearchHeader1.HeaderTitle = Resources.Search.KuldemenySearchHeaderTitle;
        SearchHeader1.TemplateObjectType = _type;

        //ErkeztetoszamSearch.Validate = false;
        UgyiratTextBox1.Validate = false;
        Kiker_mettolDatumIntervallum_SearchCalendarControl.Validate = false;
        Kiker_meddigDatumIntervallum_SearchCalendarControl.Validate = false;
        KikeroFelhasznaloCsoportTextBox.Validate = false;
        KeresDatumaDatumIntervallum_SearchCalendarControl.Validate = false;
        JovahagyoFelhasznaloCsoportTextBox.Validate = false;
        JovagyasIdejeDatumIntervallum_SearchCalendarControl.Validate = false;
        KiadasDatumaDatumIntervallum_SearchCalendarControl.Validate = false;
        KaptaFelhasznaloCsoportTextBox.Validate = false;
        VisszaadasDatumaDatumIntervallum_SearchCalendarControl.Validate = false;

        registerJavascripts();        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            EREC_IrattariKikeroSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (EREC_IrattariKikeroSearch)Search.GetSearchObject(Page, new EREC_IrattariKikeroSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        EREC_IrattariKikeroSearch EREC_IrattariKikeroSearch = null;
        if (searchObject != null) EREC_IrattariKikeroSearch = (EREC_IrattariKikeroSearch)searchObject;

        if (EREC_IrattariKikeroSearch != null)
        {
            DokumentumTipusRadioButtonList.SelectedValue = EREC_IrattariKikeroSearch.DokumentumTipus.Value;           
            
            // TODO: Azonosito??

            if (!String.IsNullOrEmpty(EREC_IrattariKikeroSearch.UgyUgyirat_Id.Value))
            {
                UgyiratTextBox1.Id_HiddenField = EREC_IrattariKikeroSearch.UgyUgyirat_Id.Value;
                UgyiratTextBox1.SetUgyiratTextBoxById(SearchHeader1.ErrorPanel, null);
            }

            Keres_notTextBox.Text = EREC_IrattariKikeroSearch.Keres_note.Value;

            FelhasznalasiCelRadioButtonList.SelectedValue = EREC_IrattariKikeroSearch.FelhasznalasiCel.Value;

            Kiker_mettolDatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(EREC_IrattariKikeroSearch.KikerKezd);
            Kiker_meddigDatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(EREC_IrattariKikeroSearch.KikerVege);

            KikeroFelhasznaloCsoportTextBox.Id_HiddenField = EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Kikero.Value;
            KikeroFelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            KeresDatumaDatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(EREC_IrattariKikeroSearch.KeresDatuma);

            JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField = EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Jovahagy.Value;
            JovahagyoFelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            JovagyasIdejeDatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(EREC_IrattariKikeroSearch.JovagyasDatuma);

            AllapotKodtarakDropDownList.FillAndSetSelectedValue(KodCsoportIrattariKikeroAllapot
                    , EREC_IrattariKikeroSearch.Allapot.Value, true, SearchHeader1.ErrorPanel);

            KiadasDatumaDatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(EREC_IrattariKikeroSearch.KiadasDatuma);

            KaptaFelhasznaloCsoportTextBox.Id_HiddenField = EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Kikero.Value;
            KaptaFelhasznaloCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            VisszaadasDatumaDatumIntervallum_SearchCalendarControl.SetComponentFromSearchObjectFields(EREC_IrattariKikeroSearch.VisszaadasDatuma);

            // TODO: Ervenyesseg_SearchFormComponent1 ??
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private EREC_IrattariKikeroSearch SetSearchObjectFromComponents()
    {
        EREC_IrattariKikeroSearch EREC_IrattariKikeroSearch = (EREC_IrattariKikeroSearch)SearchHeader1.TemplateObject;
        if (EREC_IrattariKikeroSearch == null)
        {
            EREC_IrattariKikeroSearch = new EREC_IrattariKikeroSearch();
        }

        switch (DokumentumTipusRadioButtonList.SelectedValue)
        { 
            case "0":
                if (!String.IsNullOrEmpty(DokumentumTipusRadioButtonList.SelectedValue))
                {
                    EREC_IrattariKikeroSearch.DokumentumTipus.Value = DokumentumTipusRadioButtonList.SelectedValue;
                    EREC_IrattariKikeroSearch.DokumentumTipus.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                
                if (!String.IsNullOrEmpty(UgyiratTextBox1.Id_HiddenField))
                {
                    EREC_IrattariKikeroSearch.UgyUgyirat_Id.Value = UgyiratTextBox1.Id_HiddenField;
                    EREC_IrattariKikeroSearch.UgyUgyirat_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                break;
            case "1":
                if (!String.IsNullOrEmpty(DokumentumTipusRadioButtonList.SelectedValue))
                {
                    EREC_IrattariKikeroSearch.DokumentumTipus.Value = DokumentumTipusRadioButtonList.SelectedValue;
                    EREC_IrattariKikeroSearch.DokumentumTipus.Operator = Contentum.eQuery.Query.Operators.equals;
                }

/*  TODO: nincsne meg tertiveveny definialva!!!
 * if (!String.IsNullOrEmpty(Tertiveveny_Id.Id_HiddenField))
                {
                    EREC_IrattariKikeroSearch.Tertiveveny_Id.Value = UgyiratTextBox1.Id_HiddenField;
                    EREC_IrattariKikeroSearch.Tertiveveny_Id.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                EREC_IrattariKikeroSearch.Tertiveveny_Id.Value = ""; //UgyiratTextBox1.Id_HiddenField;*/
                break;
        }

        if (!String.IsNullOrEmpty(Keres_notTextBox.Text))
        {
            EREC_IrattariKikeroSearch.Keres_note.Value = Keres_notTextBox.Text;
            EREC_IrattariKikeroSearch.Keres_note.Operator = Contentum.eQuery.Query.Operators.likefull;
        }

        switch (DokumentumTipusRadioButtonList.SelectedValue)
        {
            case "0":
                if (!String.IsNullOrEmpty(FelhasznalasiCelRadioButtonList.SelectedValue))
                {
                    EREC_IrattariKikeroSearch.FelhasznalasiCel.Value = FelhasznalasiCelRadioButtonList.SelectedValue;
                    EREC_IrattariKikeroSearch.Keres_note.Operator = Contentum.eQuery.Query.Operators.likefull;
                }
                break;
            case "1":
                if (!String.IsNullOrEmpty(FelhasznalasiCelRadioButtonList.SelectedValue))
                {
                    EREC_IrattariKikeroSearch.FelhasznalasiCel.Value = FelhasznalasiCelRadioButtonList.SelectedValue;
                    EREC_IrattariKikeroSearch.Keres_note.Operator = Contentum.eQuery.Query.Operators.likefull;
                }
                break;
        }

        switch (FelhasznalasiCelRadioButtonList.SelectedValue)
        {
            case "0":
                if (!String.IsNullOrEmpty(FelhasznalasiCelRadioButtonList.SelectedValue))
                {
                    EREC_IrattariKikeroSearch.FelhasznalasiCel.Value = FelhasznalasiCelRadioButtonList.SelectedValue;
                    EREC_IrattariKikeroSearch.FelhasznalasiCel.Operator = Contentum.eQuery.Query.Operators.equals;
                }

                break;
            case "1":
                if (!String.IsNullOrEmpty(FelhasznalasiCelRadioButtonList.SelectedValue))
                {
                    EREC_IrattariKikeroSearch.FelhasznalasiCel.Value = FelhasznalasiCelRadioButtonList.SelectedValue;
                    EREC_IrattariKikeroSearch.FelhasznalasiCel.Operator = Contentum.eQuery.Query.Operators.equals;
                }
                break;
        }


        Kiker_mettolDatumIntervallum_SearchCalendarControl.SetSearchObjectFields(EREC_IrattariKikeroSearch.KikerKezd);
        Kiker_meddigDatumIntervallum_SearchCalendarControl.SetSearchObjectFields(EREC_IrattariKikeroSearch.KikerVege);

        if (!String.IsNullOrEmpty(KikeroFelhasznaloCsoportTextBox.Id_HiddenField))
        {
            EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Kikero.Value = KikeroFelhasznaloCsoportTextBox.Id_HiddenField;
            EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Kikero.Operator = Contentum.eQuery.Query.Operators.equals;
        }

        KeresDatumaDatumIntervallum_SearchCalendarControl.SetSearchObjectFields(EREC_IrattariKikeroSearch.KeresDatuma);

        if (!String.IsNullOrEmpty(JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField))
        {
            EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Jovahagy.Value = JovahagyoFelhasznaloCsoportTextBox.Id_HiddenField;
            EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Jovahagy.Operator = Contentum.eQuery.Query.Operators.equals;
        }

        JovagyasIdejeDatumIntervallum_SearchCalendarControl.SetSearchObjectFields(EREC_IrattariKikeroSearch.JovagyasDatuma);

        if (!String.IsNullOrEmpty(AllapotKodtarakDropDownList.SelectedValue))
        {
        EREC_IrattariKikeroSearch.Allapot.Value = AllapotKodtarakDropDownList.SelectedValue;
        EREC_IrattariKikeroSearch.Allapot.Operator = Contentum.eQuery.Query.Operators.equals;
        }

        KiadasDatumaDatumIntervallum_SearchCalendarControl.SetSearchObjectFields(EREC_IrattariKikeroSearch.KiadasDatuma);

        if (!String.IsNullOrEmpty(KaptaFelhasznaloCsoportTextBox.Id_HiddenField))
        {
        EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Kikero.Value = KaptaFelhasznaloCsoportTextBox.Id_HiddenField;
        EREC_IrattariKikeroSearch.FelhasznaloCsoport_Id_Kikero.Operator = Contentum.eQuery.Query.Operators.equals;
    }

        VisszaadasDatumaDatumIntervallum_SearchCalendarControl.SetSearchObjectFields(EREC_IrattariKikeroSearch.VisszaadasDatuma);

        // TODO: Ervenyesseg_SearchFormComponent1 ??                         

        return EREC_IrattariKikeroSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            EREC_IrattariKikeroSearch searchObject = SetSearchObjectFromComponents();

            EREC_IrattariKikeroSearch baseSearchObject = new EREC_IrattariKikeroSearch();

            if (Search.IsIdentical(searchObject, baseSearchObject))
            {
                // default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }

            TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private EREC_IrattariKikeroSearch GetDefaultSearchObject()
    {
        EREC_IrattariKikeroSearch searchObject = new EREC_IrattariKikeroSearch();
        searchObject.DokumentumTipus.Value = "2";
        return searchObject;                
    }

    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");

        Kiker_mettolDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + Kiker_mettolDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.ClientID
                + "',1,['"
                + Kiker_mettolDatumIntervallum_SearchCalendarControl.DatumKezd_ImageButton.ClientID
                + "','"
                + Kiker_mettolDatumIntervallum_SearchCalendarControl.DatumVege_ImageButton.ClientID
                + "','"
                + Kiker_mettolDatumIntervallum_SearchCalendarControl.ImageButton_Copy.ClientID
                + "','"
                + Kiker_mettolDatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID
                + "','"
                + Kiker_mettolDatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID
                + "']);";

        Kiker_meddigDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + Kiker_meddigDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.ClientID
                + "',1,['"
                + Kiker_meddigDatumIntervallum_SearchCalendarControl.DatumKezd_ImageButton.ClientID
                + "','"
                + Kiker_meddigDatumIntervallum_SearchCalendarControl.DatumVege_ImageButton.ClientID
                + "','"
                + Kiker_meddigDatumIntervallum_SearchCalendarControl.ImageButton_Copy.ClientID
                + "','"
                + Kiker_meddigDatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID
                + "','"
                + Kiker_meddigDatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID
                + "']);";

        KeresDatumaDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + KeresDatumaDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.ClientID
                + "',1,['"
                + KeresDatumaDatumIntervallum_SearchCalendarControl.DatumKezd_ImageButton.ClientID
                + "','"
                + KeresDatumaDatumIntervallum_SearchCalendarControl.DatumVege_ImageButton.ClientID
                + "','"
                + KeresDatumaDatumIntervallum_SearchCalendarControl.ImageButton_Copy.ClientID
                + "','"
                + KeresDatumaDatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID
                + "','"
                + KeresDatumaDatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID
                + "']);";

        JovagyasIdejeDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + JovagyasIdejeDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.ClientID
                + "',1,['"
                + JovagyasIdejeDatumIntervallum_SearchCalendarControl.DatumKezd_ImageButton.ClientID
                + "','"
                + JovagyasIdejeDatumIntervallum_SearchCalendarControl.DatumVege_ImageButton.ClientID
                + "','"
                + JovagyasIdejeDatumIntervallum_SearchCalendarControl.ImageButton_Copy.ClientID
                + "','"
                + JovagyasIdejeDatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID
                + "','"
                + JovagyasIdejeDatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID
                + "']);";

        KiadasDatumaDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + KiadasDatumaDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.ClientID
                + "',1,['"
                + KiadasDatumaDatumIntervallum_SearchCalendarControl.DatumKezd_ImageButton.ClientID
                + "','"
                + KiadasDatumaDatumIntervallum_SearchCalendarControl.DatumVege_ImageButton.ClientID
                + "','"
                + KiadasDatumaDatumIntervallum_SearchCalendarControl.ImageButton_Copy.ClientID
                + "','"
                + KiadasDatumaDatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID
                + "','"
                + KiadasDatumaDatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID
                + "']);";

        VisszaadasDatumaDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.Attributes["onclick"] =
        " enableOrDisableComponents('" + VisszaadasDatumaDatumIntervallum_SearchCalendarControl.AktualisEv_CheckBox.ClientID
                + "',1,['"
                + VisszaadasDatumaDatumIntervallum_SearchCalendarControl.DatumKezd_ImageButton.ClientID
                + "','"
                + VisszaadasDatumaDatumIntervallum_SearchCalendarControl.DatumVege_ImageButton.ClientID
                + "','"
                + VisszaadasDatumaDatumIntervallum_SearchCalendarControl.ImageButton_Copy.ClientID
                + "','"
                + VisszaadasDatumaDatumIntervallum_SearchCalendarControl.DatumKezd_TextBox.ClientID
                + "','"
                + VisszaadasDatumaDatumIntervallum_SearchCalendarControl.DatumVege_TextBox.ClientID
                + "']);";

    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
