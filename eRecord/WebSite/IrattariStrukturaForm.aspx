﻿<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" CodeFile="IrattariStrukturaForm.aspx.cs" Inherits="IrattariStrukturaForm" %>

<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc3" %>
<%@ Register Src="Component/RequiredTextBox.ascx" TagName="RequiredTextBox" TagPrefix="uc5" %>
<%@ Register Src="~/eRecordComponent/VonalKodTextBox.ascx" TagName="vonalkodTextBox" TagPrefix="uc4" %>
<%@ Register Src="~/Component/ErvenyessegCalendarControl.ascx" TagName="ErvenyessegCalendarControl" TagPrefix="uc6" %>
<%@ Register Src="~/Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc7" %>
<%--CR3246 Irattári Helyek kezelésének módosítása--%>
<%@ Register Src="eRecordComponent/IrattarDropDownList.ascx" TagName="IrattarDropDownList" TagPrefix="uc8" %>
<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="<%$Resources:Form,IrattariStrukturaHeaderTitle %>" />
    <br />
    <div class="popupBody">
        <eUI:eFormPanel ID="EFormPanel1" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tbody>
                    <tr class="urlapNyitoSor">
                        <td class="mrUrlapCaption">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                        <td class="mrUrlapMezo">
                            <img alt=" " src="images/hu/design/spacertrans.gif" /></td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelNev" runat="server" Text="Név:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="TextBoxNev" runat="server" ReadOnly="true" Enabled="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label1" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErtek" runat="server" Text="Érték:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">
                            <uc5:RequiredTextBox ID="requiredTextBoxErtek" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <%-- CR3246 Irattári Helyek kezelésének módosítása--%>
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelIrattarTipus" runat="server" Text="Irattár jellege:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc8:IrattarDropDownList ID="IrattarDropDownList1" runat="server"></uc8:IrattarDropDownList>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label2" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="Label4" runat="server" Text="Irattár:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo">

                            <%--  <asp:DropDownList ID="AtmenetiIrattarak_DropDownList" runat="server" CssClass="mrUrlapInputComboBox" ></asp:DropDownList>--%>

                            <%--<uc7:KodtarakDropDownList ID="ddlKodtarakAtmenetiIrattarak" runat="server" />--%>

                            <uc9:CsoportTextBox ID="CsoportTextBox1" runat="server" AjaxEnabled="false" />
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label3" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelVonalkod" runat="server" Text="Vonalkód:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc4:vonalkodTextBox ID="requiredTextBoxVonalkod" runat="server" />
                        </td>
                    </tr>
                    <tr class="urlapSor" id="trIrattarHelyfoglalas" runat="server">
                        <td class="mrUrlapCaption_short">
                            <asp:Label ID="LabelKapacitas" runat="server" Text="Kapacitás:"></asp:Label>
                        </td>
                        <td class="mrUrlapMezo" colspan="5">
                            <asp:TextBox ID="TextBoxKapacitas" runat="server" />

                            <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ErrorMessage="Hibás számformátum." Operator="DataTypeCheck" Type="Double" ControlToValidate="TextBoxKapacitas"></asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server"
                                ControlToValidate="TextBoxKapacitas"
                                Operator="GreaterThanEqual"
                                ValueToCompare="0"
                                Type="Integer"
                                ErrorMessage="A kapacitásnak pozitív számnak kell lennie!"
                                Display="Dynamic"
                                Text="*" />
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server"
                                TargetControlID="CompareValidator1">
                                <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                        <HideAction Visible="true" />
                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                </Animations>
                            </ajaxToolkit:ValidatorCalloutExtender>
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server"
                                TargetControlID="CompareValidator2">
                                <Animations>
                                                    <OnShow>
                                                    <Sequence>
                                                        <HideAction Visible="true" />
                                                        <ScriptAction Script="ValidatorOverScript(this.get_target());"/>
                                                    </Sequence>    
                                                    </OnShow>
                                </Animations>
                            </ajaxToolkit:ValidatorCalloutExtender>

                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="labelNote" runat="server" Text="Megjegyzés:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <asp:TextBox ID="textNote" CssClass="mrUrlapInput" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="urlapSor">
                        <td class="mrUrlapCaption">
                            <asp:Label ID="Label5" runat="server" CssClass="ReqStar" Text="*"></asp:Label>
                            <asp:Label ID="labelErvenyesseg" runat="server" Text="Érvényesség:"></asp:Label></td>
                        <td class="mrUrlapMezo">
                            <uc6:ErvenyessegCalendarControl ID="ErvenyessegCalendarControl1" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </eUI:eFormPanel>
        <uc3:FormPart_CreatedModified ID="FormPart_CreatedModified1" runat="server"></uc3:FormPart_CreatedModified>
        <uc2:FormFooter ID="FormFooter1" runat="server"></uc2:FormFooter>
    </div>
</asp:Content>

