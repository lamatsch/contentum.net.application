﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Contentum.eRecord.Utility;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using System.Data;
//using Contentum.eUtility;

public partial class IrattariStrukturaForm : System.Web.UI.Page
{
    private string Command = "";
    private PageView pageView = null;

    private string SzuloId { get; set; }

    private string KozpontiIrattar { get; set; }

    /// <summary>
    /// View módban a vezérlők engedélyezése és megjelenése
    /// </summary>
    private void SetViewControls()
    {
        requiredTextBoxVonalkod.ReadOnly = true;
        requiredTextBoxErtek.ReadOnly = true;
        ErvenyessegCalendarControl1.ReadOnly = true;
        // CR3246 Irattári Helyek kezelésének módosítása
        //ddlKodtarakItartaTipus.ReadOnly = true;
        IrattarDropDownList1.ReadOnly = true;
        TextBoxKapacitas.ReadOnly = true;
        CsoportTextBox1.ReadOnly = true;

        textNote.ReadOnly = true;
        labelNev.CssClass = "mrUrlapInputWaterMarked";
        labelVonalkod.CssClass = "mrUrlapInputWaterMarked";
        labelNote.CssClass = "mrUrlapInputWaterMarked";
        labelErvenyesseg.CssClass = "mrUrlapInputWaterMarked";
        LabelKapacitas.CssClass = "mrUrlapInputWaterMarked";

    }
    /// <summary>
    /// Modify módban a vezérlő engedélyezése és megjelenése
    /// </summary>
    private void SetModifyControls()
    {
        if (Command == CommandName.Modify)
        {

            //LZS - BUG_7132 - Ha a node-nak nincsen leszármazottja, és nem tartozik hozzá egy ügyirat sem, akkor a szükséges mezőket írhatóvá tesszük
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (!Session["HasChild_Id"].ToString().Contains(id) && !Session["IsExistsUgyirat_HiddenFieldReadOnlyIds"].ToString().Contains(id))
            {
                requiredTextBoxErtek.ReadOnly = false;
                IrattarDropDownList1.Enabled = true;
                CsoportTextBox1.ReadOnly = false;
            }
            else
            {
                //if (String.IsNullOrEmpty(Request.QueryString.Get(QueryStringVars.ModifyIrattar)))
                //    {


                //    requiredTextBoxVonalkod.ReadOnly = false;
                //    // CR3246 Irattári Helyek kezelésének módosítása
                //    //ddlKodtarakItartaTipus.ReadOnly = true;

                //    IrattarDropDownList1.ReadOnly = false;
                //    //AtmenetiIrattarak_DropDownList.ReadOnly = true;
                //    CsoportTextBox1.ReadOnly = false;
                //} else
                //{
                requiredTextBoxVonalkod.ReadOnly = true;
                // CR3246 Irattári Helyek kezelésének módosítása
                //ddlKodtarakItartaTipus.ReadOnly = true;

                IrattarDropDownList1.ReadOnly = true;
                //AtmenetiIrattarak_DropDownList.ReadOnly = true;
                CsoportTextBox1.ReadOnly = true;
                //}

            }


        }
        else
        {
            requiredTextBoxVonalkod.ReadOnly = false;
            requiredTextBoxVonalkod.Validate = true;
            requiredTextBoxErtek.ReadOnly = false;
            requiredTextBoxVonalkod.Validate = true;
            requiredTextBoxVonalkod.FormatValidate = true;
            // CR3246 Irattári Helyek kezelésének módosítása
            //ddlKodtarakItartaTipus.ReadOnly = false;
            IrattarDropDownList1.ReadOnly = false;
            //AtmenetiIrattarak_DropDownList.ReadOnly = false;
            CsoportTextBox1.ReadOnly = false;
            TextBoxKapacitas.ReadOnly = false;
        }
        ErvenyessegCalendarControl1.ReadOnly = false;
        textNote.ReadOnly = false;
    }

    /// <summary>
    /// Az oldal Init eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Init(object sender, EventArgs e)
    {
        requiredTextBoxVonalkod.Validate = true;
        requiredTextBoxVonalkod.FormatValidate = true;
        requiredTextBoxVonalkod.ForceBarcodeCheck = true;
        Command = Request.QueryString.Get(CommandName.Command);

        pageView = new PageView(Page, ViewState);
        SetModifyControls();

        SzuloId = Request.QueryString.Get(QueryStringVars.SzuloId);
        KozpontiIrattar = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(Page).Obj_Id.ToLower();

        bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.IRATTARIHELY_KAPACITAS).ToUpper().Trim().Equals("1");
        TextBoxKapacitas.Visible = _visible;

        if (String.IsNullOrEmpty(SzuloId) && Command == CommandName.New)
        {
            // nincs Id megadva:
            ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
        }
        else if (!String.IsNullOrEmpty(SzuloId))
        {
            if (SzuloId.Equals("root"))
                SzuloId = "";
        }

        if (Command == CommandName.View || Command == CommandName.Modify)
        {
            String id = Request.QueryString.Get(QueryStringVars.Id);
            if (String.IsNullOrEmpty(id))
            {
                // nincs Id megadva:
                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
            }
            else
            {
                using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
                {
                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                    execParam.Record_Id = id;

                    Result result = service.Get(execParam);
                    if (String.IsNullOrEmpty(result.ErrorCode))
                    {
                        EREC_IrattariHelyek _EREC_IrattariHelyek = (EREC_IrattariHelyek)result.Record;
                        LoadComponentsFromBusinessObject(_EREC_IrattariHelyek);

                    }
                    else
                    {
                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                    }
                }
            }
        }
        if (Command == CommandName.View)
        {
            SetViewControls();
        }
        if (Command != CommandName.View)
        {

            if (Command == CommandName.New)
            {
                // CR3246 Irattári Helyek kezelésének módosítása
                //ddlKodtarakItartaTipus.FillDropDownList("IrattarTipus", true, FormHeader1.ErrorPanel);

                if (!String.IsNullOrEmpty(SzuloId))
                {
                    using (EREC_IrattariHelyekService svcIrattariHelyek = Contentum.eUtility.eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
                    {
                        ExecParam execParam = UI.SetExecParamDefault(Page);
                        execParam.Record_Id = SzuloId;
                        Result resIrattariHelyek = svcIrattariHelyek.Get(execParam);
                        if (resIrattariHelyek.IsError)
                            ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, resIrattariHelyek);
                        EREC_IrattariHelyek IrattariObj = resIrattariHelyek.Record as EREC_IrattariHelyek;
                        // CR3246 Irattári Helyek kezelésének módosítása
                        //ddlKodtarakItartaTipus.SelectedValue = IrattariObj.IrattarTipus;
                        //ddlKodtarakItartaTipus.ReadOnly = !string.IsNullOrEmpty(IrattariObj.IrattarTipus);
                        if (IrattariObj != null)
                        {
                            if (IrattariObj.Felelos_Csoport_Id == KozpontiIrattar)
                            {
                                IrattarDropDownList1.SetKozpontiIrattar();
                                IrattarDropDownList1.ReadOnly = true;
                                CsoportTextBox1.Id_HiddenField = KozpontiIrattar;
                                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                                CsoportTextBox1.ReadOnly = true;
                            }
                            else
                            {
                                IrattarDropDownList1.SetAtmenetiIrattar();
                                IrattarDropDownList1.ReadOnly = true;
                                CsoportTextBox1.Id_HiddenField = IrattariObj.Felelos_Csoport_Id;
                                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                                CsoportTextBox1.ReadOnly = true;
                            }
                        }
                    }
                }
            }
            IrattarDropDownList1.DropDownList.AutoPostBack = true;
            CsoportTextBox1.Filter = Constants.FilterType.AllAtmenetiIrattar; //******************
            IrattarDropDownList1.DropDownList.SelectedIndexChanged += new EventHandler(IrattarDropDownList1_SelectedIndexChanged);



        }

    }

    /// <summary>
    /// Oldal Load eseménykezelője
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        FormFooter1.ButtonsClick += new
           System.Web.UI.WebControls.CommandEventHandler(FormFooter1ButtonsClick);

        // CR3246 Irattári Helyek kezelésének módosítása
        if (!IsPostBack)
        {
            Contentum.eAdmin.Service.KRT_CsoportokService service_csoportok = Contentum.eUtility.eAdminService.ServiceFactory.GetKRT_CsoportokService();
            ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

            Result result = service_csoportok.GetAllAtmenetiIrattar(execParam);
            if (!String.IsNullOrEmpty(result.ErrorCode))
            {
                // hiba:
                ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
            }
            else
            {

                if (result.Ds != null && result.Ds.Tables[0].Rows.Count == 1)
                {
                    // Egy irattár van, ez lesz a default
                    DataRow row = result.Ds.Tables[0].Rows[0];
                    string irattarId = row["Id"].ToString();
                    string irattarNev = row["Nev"].ToString();


                    //ddlKodtarakAtmenetiIrattarak.FillAndSetSelectedValue()
                    CsoportTextBox1.Id_HiddenField = irattarId;
                    CsoportTextBox1.Text = irattarNev;

                    // elmentjük ViewState-be, hogy ha központi irattárat választanak, majd újra átmenetit, vissza tudjuk tölteni
                    ViewState["AtmenetiIrattar_CsoportTextBox_Id"] = irattarId;
                }

                // ha nincs átmeneti irattára, a központit ajánljuk fel:
                if (result.Ds != null && result.Ds.Tables[0].Rows.Count == 0)
                {
                    IrattarDropDownList1.SetKozpontiIrattar();


                    CsoportTextBox1.Id_HiddenField = KozpontiIrattar;
                    CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                    CsoportTextBox1.ReadOnly = true;
                }
                //else
                //{
                //    IrattarDropDownList1.SetAtmenetiIrattar();
                //}
            }
        }

        //pageView.SetViewOnPage(Command);
        //if (Command == CommandName.DesignView)
        //{
        //    Load_ComponentSelectModul();
        //}
        //else
        //{
        //    pageView.SetViewOnPage(Command);
        // }
        //   ddlKodtarakItartaTipus.FillDropDownList("IrattarTipus", FormHeader1.ErrorPanel);

    }

    /// <summary>
    /// Komponensek betöltése üzleti objektumból
    /// </summary>
    /// <param name="EREC_IrattariHelyek"></param>
    private void LoadComponentsFromBusinessObject(EREC_IrattariHelyek _EREC_IrattariHelyek)
    {
        if (Command == CommandName.Modify)
        {
            SzuloId = _EREC_IrattariHelyek.SzuloId;

        }
        TextBoxNev.Text = _EREC_IrattariHelyek.Nev;
        requiredTextBoxErtek.Text = _EREC_IrattariHelyek.Ertek;

        requiredTextBoxVonalkod.Text = _EREC_IrattariHelyek.Vonalkod;
        textNote.Text = _EREC_IrattariHelyek.Base.Note;
        ErvenyessegCalendarControl1.ErvKezd = _EREC_IrattariHelyek.ErvKezd;
        ErvenyessegCalendarControl1.ErvVege = _EREC_IrattariHelyek.ErvVege;
        // CR3246 Irattári Helyek kezelésének módosítása
        //ddlKodtarakItartaTipus.FillAndSetSelectedValue("IrattarTipus", _EREC_IrattariHelyek.IrattarTipus, FormHeader1.ErrorPanel);
        CsoportTextBox1.Id_HiddenField = _EREC_IrattariHelyek.Felelos_Csoport_Id;
        CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
        TextBoxKapacitas.Text = _EREC_IrattariHelyek.Kapacitas;

        if (_EREC_IrattariHelyek.Felelos_Csoport_Id == KozpontiIrattar)
        {
            IrattarDropDownList1.SetKozpontiIrattar();
        }
        else
            IrattarDropDownList1.SetAtmenetiIrattar();

        //aktuális verzió ektárolása
        FormHeader1.Record_Ver = _EREC_IrattariHelyek.Base.Ver;
        //A modósitás,létrehozás form beállítása
        FormPart_CreatedModified1.SetComponentValues(_EREC_IrattariHelyek.Base);
    }

    /// <summary>
    /// Üzleti objektum feltöltése komponensekből
    /// </summary>
    /// <returns></returns>
    private EREC_IrattariHelyek GetBusinessObjectFromComponents()
    {
        EREC_IrattariHelyek _EREC_IrattariHelyek = new EREC_IrattariHelyek();
        _EREC_IrattariHelyek.Updated.SetValueAll(false);
        _EREC_IrattariHelyek.Base.Updated.SetValueAll(false);

        if (Command == CommandName.Modify)
        {
            _EREC_IrattariHelyek.SzuloId = SzuloId;
            _EREC_IrattariHelyek.Updated.SzuloId = false;
        }
        if (Command == CommandName.New)
        {
            _EREC_IrattariHelyek.SzuloId = SzuloId;
            _EREC_IrattariHelyek.Updated.SzuloId = true;

        }
        _EREC_IrattariHelyek.Ertek = requiredTextBoxErtek.Text;
        _EREC_IrattariHelyek.Updated.Ertek = pageView.GetUpdatedByView(requiredTextBoxErtek);
        _EREC_IrattariHelyek.Nev = requiredTextBoxErtek.Text;
        _EREC_IrattariHelyek.Updated.Nev = pageView.GetUpdatedByView(requiredTextBoxErtek);
        requiredTextBoxVonalkod.Validate = true;
        requiredTextBoxVonalkod.RequiredValidate = true;
        requiredTextBoxVonalkod.FormatValidate = true;
        _EREC_IrattariHelyek.Vonalkod = requiredTextBoxVonalkod.Text;
        _EREC_IrattariHelyek.Updated.Vonalkod = pageView.GetUpdatedByView(requiredTextBoxVonalkod);
        _EREC_IrattariHelyek.Base.Note = textNote.Text;
        _EREC_IrattariHelyek.Base.Updated.Note = pageView.GetUpdatedByView(textNote);
        // CR3246 Irattári Helyek kezelésének módosítása
        //_EREC_IrattariHelyek.IrattarTipus = ddlKodtarakItartaTipus.SelectedValue;
        //_EREC_IrattariHelyek.Updated.IrattarTipus = true;
        _EREC_IrattariHelyek.Felelos_Csoport_Id = CsoportTextBox1.Id_HiddenField;
        _EREC_IrattariHelyek.Updated.Felelos_Csoport_Id = true;

        ErvenyessegCalendarControl1.SetErvenyessegFields(_EREC_IrattariHelyek, pageView.GetUpdatedByView(ErvenyessegCalendarControl1));

        _EREC_IrattariHelyek.Kapacitas = TextBoxKapacitas.Text;
        _EREC_IrattariHelyek.Updated.Kapacitas = pageView.GetUpdatedByView(TextBoxKapacitas);

        //a megnyitási verzió megadása ellenőrzés miatt
        _EREC_IrattariHelyek.Base.Ver = FormHeader1.Record_Ver;
        _EREC_IrattariHelyek.Base.Updated.Ver = true;

        return _EREC_IrattariHelyek;
    }


    private void FormFooter1ButtonsClick(object sender, System.Web.UI.WebControls.CommandEventArgs e)
    {
        if (e.CommandName == CommandName.Save)
        {
            if (FunctionRights.GetFunkcioJog(Page, "IrattariHelyek" + Command))
            {
                switch (Command)
                {
                    case CommandName.New:
                        {
                            using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
                            {
                                EREC_IrattariHelyek _EREC_IrattariHelyek = GetBusinessObjectFromComponents();

                                ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());

                                Result result = service.Insert(execParam, _EREC_IrattariHelyek);

                                if (String.IsNullOrEmpty(result.ErrorCode))
                                {
                                    Session["NewIrattariHelyId"] = "";
                                    Session["NewIrattariHelyId"] = result.Uid;
                                    JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                }
                                else
                                {
                                    ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                }
                                break;
                            }
                        }
                    case CommandName.Modify:
                        {
                            String recordId = Request.QueryString.Get(QueryStringVars.Id);
                            if (String.IsNullOrEmpty(recordId))
                            {
                                // nincs Id megadva:
                                ResultError.DisplayNoIdParamError(FormHeader1.ErrorPanel);
                            }
                            else
                            {
                                using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
                                {
                                    EREC_IrattariHelyek _EREC_IrattariHelyek = GetBusinessObjectFromComponents();

                                    ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
                                    execParam.Record_Id = recordId;

                                    Result result = service.Update(execParam, _EREC_IrattariHelyek);

                                    if (String.IsNullOrEmpty(result.ErrorCode))
                                    {
                                        Session["ModifyIrattariHelyId"] = "";
                                        Session["ModifyIrattariHelyId"] = recordId;
                                        JavaScripts.RegisterCloseWindowClientScript(Page, true);
                                    }
                                    else
                                    {
                                        ResultError.DisplayResultErrorOnErrorPanel(FormHeader1.ErrorPanel, result);
                                    }
                                }
                            }
                            break;
                        }
                }
            }
            else
            {
                UI.RedirectToAccessDeniedErrorPage(Page);
            }
        }
    }

    protected void IrattarDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Ha átmeneti irattárra váltott, és korábban már lekértük az átmeneti irattárat, azt visszatöltjük ViewState-ből
        if (IrattarDropDownList1.DropDownList.SelectedIndex == 0)
        {
            string atmenetiIrattarId = String.Empty;
            if (ViewState["AtmenetiIrattar_CsoportTextBox_Id"] != null)
            {
                atmenetiIrattarId = ViewState["AtmenetiIrattar_CsoportTextBox_Id"].ToString();
            }
            if (!String.IsNullOrEmpty(atmenetiIrattarId))
            {
                CsoportTextBox1.Id_HiddenField = atmenetiIrattarId;
                CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
                CsoportTextBox1.ReadOnly = false;
            }
            else
            {
                CsoportTextBox1.Id_HiddenField = String.Empty;
                CsoportTextBox1.Text = String.Empty;
                CsoportTextBox1.ReadOnly = false;
            }
        }
        else
        {
            CsoportTextBox1.Id_HiddenField = KodTarak.SPEC_SZERVEK.GetKozpontiIrattar(UI.SetExecParamDefault(Page, new ExecParam())).Obj_Id.ToLower();
            CsoportTextBox1.SetCsoportTextBoxById(FormHeader1.ErrorPanel);
            CsoportTextBox1.ReadOnly = true;
        }
    }
}