﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IrattariStrukturaList.aspx.cs" Inherits="IrattariStrukturaList" %>


<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/TreeViewHeader.ascx" TagName="TreeViewHeader" TagPrefix="tvh" %> 
 <%@ Register Src="eRecordComponent/IrattarStrukturaTerkep.ascx" TagName="IrattarStrukturaTerkep"
    TagPrefix="imdt" %>         

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" >
    </asp:ScriptManager>
    <uc1:FormHeader ID="FormHeader1" runat="server"  />
    
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table border="0" cellpadding="0" cellspacing="0" width="96%">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:Panel ID="IrattariTervFormPanel" runat="server">
                   <asp:UpdatePanel ID="ErrorUpdatePanel1" runat="server" UpdateMode="Conditional">
                       <ContentTemplate>
                           <eUI:eErrorPanel ID="EErrorPanel1" runat="server" Visible="false">
                           </eUI:eErrorPanel>
                       </ContentTemplate>
                   </asp:UpdatePanel>
    
                   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                       <ContentTemplate>
                           <tvh:TreeViewHeader ID="TreeViewHeader1" runat="server" />
                       </ContentTemplate>
                   </asp:UpdatePanel>
    
                   <%--    <cup:CustomUpdateProgress ID="CustomUpdateProgress1" Runat="server" />--%>
                   <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel" UpdateMode="Conditional" OnLoad="TreeViewUpdatePanel_Load">
                       <ContentTemplate>        
                            <asp:Panel ID="MainPanel" runat="server" Visible="true">
                                <ajaxToolkit:CollapsiblePanelExtender ID="MetaAdatokCPE" runat="server" TargetControlID="Panel1"
                                    CollapsedSize="20" Collapsed="False" 
                                     AutoCollapse="false" AutoExpand="false"    
                                    ExpandedSize="400"
                                    ScrollContents="true">
                                </ajaxToolkit:CollapsiblePanelExtender>   
                                <table style="width: 100%;">
                                    <tr>
                                     <td style="width: 100%;">
                                         <table style="width: 100%;">
                                             <tr>
                                                 <td style="width: 100%; text-align: left; border: solid 1px Silver; ">
                                                     <asp:Panel ID="Panel1" runat="server" Visible="true">
                                                         <imdt:IrattarStrukturaTerkep ID="IrattarTerkep1" runat="server"
                                                            OnSelectedNodeChanged = "IrattarTerkep_SelectedNodeChanged"   
                                                         />                                        
                                                     </asp:Panel>     
                                                 </td>                                   
                                            </tr>                               
                                         </table>
                                     </td>

                                   </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                   </asp:UpdatePanel>  
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>  
     
