﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eRecord.Utility;
using Contentum.eQuery;

// csak a rövidebb megnevezésért
using IMDterkep = eRecordComponent_IrattarStrukturaTerkep;

public partial class IrattariStrukturaList : Contentum.eUtility.UI.PageBase
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Funkciójog ellenőrzés
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IrattariHelyekList");
    }

    private PageView pageView = null;

    private string RegisterClientScriptDeleteConfirm()
    {
        string js = "if (confirm('" + Contentum.eUtility.Resources.Question.GetString("UIConfirmHeader_Delete") + " \\n\\n"
                + "Biztosan törli a kijelölt csomópontot és leszármazottait?"
                + "')) {  } else return false;";
        return js;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        pageView = new PageView(Page, ViewState);

        FormHeader1.HeaderTitle = "Irattári struktúra karbantartása";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JavaScripts.RegisterPopupWindowClientScript(Page);

        TreeViewHeader1.HeaderLabel = "Irattári struktúra";
        TreeViewHeader1.HeaderNodeButtonsLabel = "Csomópont";
        TreeViewHeader1.HeaderChildButtonsLabel = "Leszármazott";
        TreeViewHeader1.SearchObjectType = typeof(IrattariTervHierarchiaSearch);
        TreeViewHeader1.SetFilterFunctionButtonsVisible(true);
        TreeViewHeader1.SetNodeFunctionButtonsVisible(true);
        TreeViewHeader1.SetChildFunctionButtonsVisible(true);
        TreeViewHeader1.SetCustomNodeFunctionButtonsVisible(false);
        TreeViewHeader1.AttachedTreeView = IrattarTerkep1.TreeView;

        TreeViewHeader1.SelectNewChildVisible = false;
        TreeViewHeader1.SearchVisible = false;
        TreeViewHeader1.DefaultFilterVisible = false;

        bool _visible = Rendszerparameterek.Get(UI.SetExecParamDefault(Page, new ExecParam()), Contentum.eRecord.BaseUtility.Rendszerparameterek.VONALKOD_NYOMTATAS_FELULETEN).ToUpper().Trim().Equals("1");
        TreeViewHeader1.VonalkodNyomtatasaVisible = _visible;

        //TreeViewHeader1.SearchOnClientClick = SetOnClientClick("IrattariTervHierarchiaFilterSearch.aspx", "");

        TreeViewHeader1.NodeFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderNodeFunctionButtonsClick);
        TreeViewHeader1.ChildFunctionButtonsClick += new CommandEventHandler(TreeViewHeaderChildFunctionButtonsClick);
        TreeViewHeader1.FilterFunctionButtonsClick += new CommandEventHandler(FilterFunctionButtonsOnClick);
        TreeViewHeader1.NodeRightsOnClientClick = SetOnClientClick("IrattariJogosultak.aspx", QueryStringVars.Command + "=" + CommandName.Modify);
        TreeViewHeader1.NodeRightsEnabled = IrattarTerkep1.TreeView.SelectedNode != null && !String.IsNullOrEmpty(IrattarTerkep1.TreeView.SelectedNode.Value);

        if (IsPostBack)
        {
            if (EErrorPanel1.Visible)
            {
                EErrorPanel1.Visible = false;
                ErrorUpdatePanel1.Update();
            }

        }
        else
        {
            Session["UgyiratokBetoltve_HiddenFieldNodesIds"] = "";
            Session["GetAllLeafNodes_Id"] = "";
            getReadonlyIdsToSession();
            getAllLeafNodes();
            IrattarTerkep1.EErrorPanel1 = EErrorPanel1;

            //LZS - BUG_7132 - Azon node-ok tárolására, amelyeknek nincs leszármazottjuk
            Session["HasNoChild_Id"] = "";
        }
    }

    protected void Page_PreRenderComplete(object sender, EventArgs e)
    {
        if (IsPostBack || (Session["NewIrattariHelyId"] != null && !String.IsNullOrEmpty(Session["NewIrattariHelyId"].ToString())))
        {
            string s2 = @"
                function GetSelectedNode() {
                   var treeViewData = window['{0}_Data'];
                   if (treeViewData.selectedNodeID.value != '') {
                       var selectedNode = document.getElementById(treeViewData.selectedNodeID.value);
                       selectedNode.scrollIntoView(true);
                   }
                   return false;
                }
                GetSelectedNode() ;";
            ScriptManager.RegisterStartupScript(IrattarTerkep1.TreeView, this.GetType(), "setScroll", s2.Replace("{0}", IrattarTerkep1.TreeView.ClientID), true);
        }

        TreeViewHeader1.NodeRightsVisible = !Rendszerparameterek.GetBoolean(Page, "IRATTAROZAS_HELY_FELH_ALTAL", true);
    }

    private void TreeViewHeaderNodeFunctionButtonsClick(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.Invalidate:
                InvalidateSelectedNode();
                break;
            case CommandName.ExpandNode:
                IrattarTerkep1.ExpandSelectedNodeAllLevels();
                break;
            case CommandName.NodeRights:
                IrattarTerkep1.ExpandSelectedNodeAllLevels();
                break;
        }

    }
    private void TreeViewHeaderChildFunctionButtonsClick(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case CommandName.UgyiratokList:
                AddUgyiratList();
                break;
        }
    }

    protected void TreeViewUpdatePanel_Load(object sender, EventArgs e)
    {
    }
    protected void FilterFunctionButtonsOnClick(object sender, EventArgs e)
    {
        Session["UgyiratokBetoltve_HiddenFieldNodesIds"] = "";
        getReadonlyIdsToSession();
        //IrattarTerkep1.SelectedId = "";
        IrattarTerkep1.RefreshTreeView(IrattarTerkep1.TreeView, e);
    }

    private string SetOnClientClick(string url, string param)
    {
        return JavaScripts.SetOnClientClick(url, param, Defaults.PopupWidth, Defaults.PopupHeight, TreeViewUpdatePanel.ClientID, EventArgumentConst.refreshMasterList);
    }

    private string IrattariStrukturaForm(string command, string nodeId)
    {
        return SetOnClientClick("IrattariStrukturaForm.aspx", QueryStringVars.Command + "=" + command + "&" + QueryStringVars.Id + "=" + nodeId);
    }

    protected void IrattarTerkep_SelectedNodeChanged(object sender, EventArgs e)
    {
        var node = IrattarTerkep1.TreeView.SelectedNode;
        var nodeId = node == null ? "" : node.Value;
        Session[Constants.SessionKeys.SelectedIrattarNodeId] = nodeId;
        Session[Constants.SessionKeys.SelectedIrattarNodePath] = TreeViewHeader1.SelectedPath;

        if (Session["UgyiratokBetoltve_HiddenFieldNodesIds"].ToString().Contains(nodeId) && !String.IsNullOrEmpty(nodeId))
        {
            TreeViewHeader1.ModifyEnabled = false;
            TreeViewHeader1.InvalidateEnabled = false;
            TreeViewHeader1.ExpandNodeEnabled = false;
            TreeViewHeader1.UgyiratokListEnabled = false;
            TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratView");
            TreeViewHeader1.ViewOnClientClick = JavaScripts.SetOnClientClick("UgyUgyiratokForm.aspx"
                   , QueryStringVars.Command + "=" + CommandName.View + "&" + QueryStringVars.Id + "=" + nodeId
                   , Defaults.PopupWidth_MaxExtended, Defaults.PopupHeight_Max, null);
        }
        else
        {
            TreeViewHeader1.VonalkodNyomtatasaOnClientClick = SetOnClientClick("VonalkodTabPrintForm.aspx", 
                QueryStringVars.Vonalkodok + "=" + nodeId + "&tipus=Irattarihely" + "&Command=Modify");

            TreeViewHeader1.InvalidateOnClientClick = RegisterClientScriptDeleteConfirm();

            string szuloId = String.IsNullOrEmpty(nodeId) ? "root" : nodeId;
            TreeViewHeader1.NewOnClientClick = SetOnClientClick("IrattariStrukturaForm.aspx", QueryStringVars.Command + "=" + CommandName.New
                      + "&" + QueryStringVars.SzuloId + "=" + szuloId
                      + "&" + QueryStringVars.RefreshCallingWindow + "=1");
            if (String.IsNullOrEmpty(nodeId))
            {
                TreeViewHeader1.ModifyEnabled = false;
                TreeViewHeader1.InvalidateEnabled = false;
                TreeViewHeader1.ViewEnabled = false;
                TreeViewHeader1.ExpandNodeEnabled = true;
                TreeViewHeader1.VonalkodNyomtatasaEnabled = false;
                TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekNew");
            }
            else
            {
                TreeViewHeader1.ViewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekView");
                TreeViewHeader1.ExpandNodeEnabled = true;

                // TODO: SZÍVÁS!!!!!!!
                if (Session["IsExistsUgyirat_HiddenFieldReadOnlyIds"].ToString().Contains(nodeId) && !String.IsNullOrEmpty(nodeId))
                {
                    TreeViewHeader1.InvalidateEnabled = false;
                    TreeViewHeader1.ModifyEnabled = true;
                    if (Session["GetAllLeafNodes_Id"].ToString().Contains(nodeId) && !String.IsNullOrEmpty(nodeId))
                    {
                        //LZS - BUG_7132
                        TreeViewHeader1.NewEnabled = false;
                    }
                    else
                        TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekNew");
                }
                else
                {
                    TreeViewHeader1.InvalidateEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekInvalidate");
                    TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekModify");
                    TreeViewHeader1.NewEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekNew");
                }

                TreeViewHeader1.ModifyOnClientClick = IrattariStrukturaForm(CommandName.Modify, nodeId);
                TreeViewHeader1.ViewOnClientClick = IrattariStrukturaForm(CommandName.View, nodeId);
            }

            if (
                Session["IsExistsUgyirat_HiddenFieldReadOnlyIds"].ToString().Contains(nodeId)
                &&
                ((
                    node.ChildNodes.Count == 0 && node.Depth == 1 &&
                    Session["isNotExistChild"].ToString().Contains(nodeId)
                )
                ||
                (
                    node.ChildNodes.Count == 0 && node.Depth > 1)
                ))
            {
                TreeViewHeader1.UgyiratokListEnabled = FunctionRights.GetFunkcioJog(Page, "UgyiratokList");
                TreeViewHeader1.ModifyEnabled = FunctionRights.GetFunkcioJog(Page, "IrattariHelyekModify");
                TreeViewHeader1.ModifyOnClientClick = IrattariStrukturaForm(CommandName.Modify, nodeId);
            }
            else
            {
                //LZS - BUG_7132 - A root node-nál nem aktív az Ügyiratfa gomb
                if (IrattarTerkep1.TreeView.SelectedNode.Parent == null)
                {
                    TreeViewHeader1.UgyiratokListEnabled = false;
                }
                else
                {
                    TreeViewHeader1.UgyiratokListEnabled = true;
                }
            }

            //LZS - BUG_7132 - A törlés gomb nem aktív, hogyha van a node-nak leszármazottja
            if (node.ChildNodes.Count > 0)
            {
                TreeViewHeader1.InvalidateEnabled = false;
                TreeViewHeader1.VonalkodNyomtatasaEnabled = false;

                //LZS - BUG_7132 - Ha van a node-nak leszármazottja, akkor az id-ját eltároljuk.
                Session["HasChild_Id"] = nodeId;
            }
            else
            {
                TreeViewHeader1.VonalkodNyomtatasaEnabled = true;
            }
        }
    }

    /// <summary>
    /// Törli a kijelölt elemet
    /// </summary>
    private void InvalidateSelectedNode()
    {
        var nodeId = IrattarTerkep1.TreeView.SelectedNode == null ? "" : IrattarTerkep1.TreeView.SelectedNode.Value;
        if (String.IsNullOrEmpty(nodeId))
        {
            Result resError = Contentum.eUtility.ResultException.GetResultFromException(new Exception());
            resError.ErrorMessage = "Nincs kijelölve törölhető elem";

            ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, resError);
            if (ErrorUpdatePanel1 != null)
            {
                ErrorUpdatePanel1.Update();
            }
            return;
        }

        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        execParam.Record_Id = nodeId;
        Result result = null;

        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            result = service.Invalidate(execParam);
        }

        if (result != null)
        {
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
            else
            {
                IrattarTerkep1.DeleteSelectedNode();  // törli a kijelölt elemet, és gyermekeit a fából
            }
        }
    }

    private void AddUgyiratList()
    {
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        using (EREC_UgyUgyiratokService service = eRecordService.ServiceFactory.GetEREC_UgyUgyiratokService())
        {
            result = service.GetAllByIrattariHelyErtek(execParam, IrattarTerkep1.TreeView.SelectedNode.Value);
        }

        if (result != null)
        {
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
            else
            {
                if (result.GetCount >= 1)
                {
                    //IsExistsUgyirat_HiddenFieldReadOnlyIds.Value = IrattarTerkep1.SelectedNode.Value;
                    IrattarTerkep1.TreeView.SelectedNode.ChildNodes.Clear();
                    foreach (DataRow item in result.Ds.Tables[0].Rows)
                    {
                        IrattarTerkep1.TreeView.SelectedNode.ChildNodes.Add(new TreeNode(item["Azonosito"].ToString(), item["Id"].ToString()));
                        Session["UgyiratokBetoltve_HiddenFieldNodesIds"] += ";" + item["Id"].ToString().ToLower();
                    }
                }
            }
            // CR3246 Irattári Helyek kezelésének módosítása
            IrattarTerkep1.ExpandSelectedNodeAllLevels();
        }
    }

    private void getReadonlyIdsToSession()
    {
        Session["IsExistsUgyirat_HiddenFieldReadOnlyIds"] = "";
        //get readonly id-k
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            result = service.GetReadOnlyIds(execParam);
        }

        if (result != null)
        {
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
            else if (result.GetCount >= 1)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    Session["IsExistsUgyirat_HiddenFieldReadOnlyIds"] += row["IdPath"].ToString().ToLower();
                }
            }
        }
    }
    private void getAllLeafNodes()
    {
        Session["GetAllLeafNodes_Id"] = "";
        //get readonly id-k
        ExecParam execParam = UI.SetExecParamDefault(Page, new ExecParam());
        Result result = null;

        using (EREC_IrattariHelyekService service = eRecordService.ServiceFactory.GetEREC_IrattariHelyekService())
        {
            result = service.GetAllLeafs(execParam);
        }

        if (result != null)
        {
            if (result.IsError)
            {
                ResultError.DisplayResultErrorOnErrorPanel(EErrorPanel1, result);
            }
            else if (result.GetCount >= 1)
            {
                foreach (DataRow row in result.Ds.Tables[0].Rows)
                {
                    Session["GetAllLeafNodes_Id"] += row["Id"].ToString().ToLower() + " , ";
                }
            }
        }
    }
}