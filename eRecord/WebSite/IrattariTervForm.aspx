<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="IrattariTervForm.aspx.cs" Inherits="IrattariTervForm" Title="Irattári terv karbantartása" %>

<%@ Register Src="Component/CustomUpdateProgress.ascx" TagName="CustomUpdateProgress"
    TagPrefix="uc7" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified"
    TagPrefix="uc4" %>
<%@ Register Src="Component/FormFooter.ascx" TagName="FormFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/FormHeader.ascx" TagName="FormHeader" TagPrefix="uc1" %>
<%@ Register Src="eRecordComponent/IrattariTervTerkepTab.ascx" TagName="IrattariTervTerkepTab"
    TagPrefix="tp1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc1:FormHeader ID="FormHeader1" runat="server" HeaderTitle="Irattári terv karbantartása" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <uc7:CustomUpdateProgress ID="CustomUpdateProgress1" runat="server" />
    <table border="0" cellpadding="0" cellspacing="0" width="96%">
        <tr>
            <td style="text-align: left; vertical-align: top; width: 100%;">
                <asp:Panel ID="IrattariTervFormPanel" runat="server">
                    <%-- OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                              OnClientActiveTabChanged="ActiveTabChanged" --%>
                    <ajaxToolkit:TabContainer ID="IratMetaDefinicioTabContainer" runat="server" Width="100%"
                        OnActiveTabChanged="IratMetaDefinicioTabContainer_ActiveTabChanged" OnClientActiveTabChanged="ActiveTabChanged"
                        ActiveTabIndex="0">
                        <ajaxToolkit:TabPanel ID="TabMetaDefinicioPanel" runat="server">
                            <HeaderTemplate>
                                <asp:Label ID="TabIratMetaDefinicioHeader" runat="server" Text="Irattári terv karbantartása"></asp:Label>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <tp1:IrattariTervTerkepTab ID="IrattariTervTerkepTab1" runat="server" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <uc2:FormFooter ID="FormFooter1" runat="server" />
</asp:Content>
