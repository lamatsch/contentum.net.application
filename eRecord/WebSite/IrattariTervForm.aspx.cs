using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eBusinessDocuments;
using Contentum.eRecord.Service;
using Contentum.eRecord.Utility;

public partial class IrattariTervForm : Contentum.eUtility.UI.PageBase
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Funkci�jog ellen�rz�s
        FunctionRights.GetFunkcioJogRedirectErrorPage(Page, "IratMetaDefinicioList");
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        JavaScripts.RegisterActiveTabChangedClientScript(Page, IratMetaDefinicioTabContainer);
        ScriptManager1.RegisterAsyncPostBackControl(IratMetaDefinicioTabContainer);

        SetEnabledAllTabsByFunctionRights();

        // Minden tab akt�v
        //SetEnabledAllTabs(true);
        TabMetaDefinicioPanel.Enabled = true;


        // TODO: Header
        //FormHeader1.FullManualHeaderTitle = Resources.Form.IrattariTervFormHeaderTitle;
        FormHeader1.FullManualHeaderTitle = "Iratt�ri terv karbantart�sa";


       String id = Request.QueryString.Get(QueryStringVars.Id);
      

        // FormHeader be�ll�t�sa az IrattariTervTerkepTab1 -nek:
        IrattariTervTerkepTab1.FormHeader = FormHeader1;

        IrattariTervTerkepTab1.Active = true;
        // TODO: parentform
        //IrattariTervTerkepTab1.ParentForm = Constants.ParentForms.IraIrat;
        IrattariTervTerkepTab1.ParentForm = "IrattariTervForm";


    }



    protected void Page_Load(object sender, EventArgs e)
    {
        registerJavascripts();

        if (!IsPostBack)
        {
            // Default panel beallitasa
            string selectedTab = Request.QueryString.Get(QueryStringVars.SelectedTab);

            bool selectedTabFound = false;
            if (!String.IsNullOrEmpty(selectedTab))
            {
                foreach (AjaxControlToolkit.TabPanel tab in IratMetaDefinicioTabContainer.Tabs)
                {
                    if (tab.ID.ToLower().Contains(selectedTab.ToLower()))
                    {
                        selectedTabFound = true;
                        IratMetaDefinicioTabContainer.ActiveTab = tab;
                        break;
                    }
                }

                if (!selectedTabFound)
                {
                    IratMetaDefinicioTabContainer.ActiveTab = TabMetaDefinicioPanel;
                }
            }
            else
            {
                IratMetaDefinicioTabContainer.ActiveTab = TabMetaDefinicioPanel;
            }

            ActiveTabRefresh(IratMetaDefinicioTabContainer);
        }

        // Atverjuk a formfooter-t, mert ugy kell mukodni-e, mint ha nezet lenne, csak a Bezar gombra van szukseg
        FormFooter1.Command = CommandName.View;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

    }

    #region Forms Tab

    protected void IratMetaDefinicioTabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        ActiveTabRefresh(sender as AjaxControlToolkit.TabContainer);
    }

    private void ActiveTabRefresh(AjaxControlToolkit.TabContainer sender)
    {
        IrattariTervTerkepTab1.Active = false;
 
        if (IratMetaDefinicioTabContainer.ActiveTab.Equals(TabMetaDefinicioPanel))
        {
            IrattariTervTerkepTab1.Active = true;
            IrattariTervTerkepTab1.ReLoadTab();
        }

    }

    #endregion



    private void registerJavascripts()
    {
        JavaScripts.registerOuterScriptFile(Page, "CheckBoxes"
                , "Javascripts/CheckBoxes.js");
    }

    private void SetEnabledAllTabs(Boolean value)
    {
        TabMetaDefinicioPanel.Enabled = value;
    }

    private void SetEnabledAllTabsByFunctionRights()
    {
        // a default panelt nem kell ellenorizni!
        TabMetaDefinicioPanel.Enabled = true;
    }

}
