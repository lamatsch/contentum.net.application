<%@ Page Language="C#" MasterPageFile="~/EmptyMasterPage.master" AutoEventWireup="true" 
CodeFile="IrattariTervHierarchiaFilterSearch.aspx.cs" Inherits="IrattariTervHierarchiaFilterSearch" Title="Iratt�ri hierarchia keres�s" %>

<%@ Register Src="eRecordComponent/IraIrattariTetelTextBox.ascx" TagName="IraIrattariTetelTextBox"
    TagPrefix="uc6" %>

<%@ Register Src="Component/CsoportTextBox.ascx" TagName="CsoportTextBox" TagPrefix="uc3" %>
<%@ Register Src="eRecordComponent/AgazatiJelTextBox.ascx" TagName="AgazatiJelTextBox" TagPrefix="uc7" %>
<%@ Register Src="Component/KodtarakDropDownList.ascx" TagName="KodtarakDropDownList" TagPrefix="uc11" %>
<%@ Register Src="Component/TalalatokSzama_SearchFormComponent.ascx" TagName="TalalatokSzama_SearchFormComponent" TagPrefix="uc10" %>
<%@ Register Src="Component/FormPart_CreatedModified.ascx" TagName="FormPart_CreatedModified" TagPrefix="uc4" %> 
<%@ Register Src="Component/SearchHeader.ascx" TagName="SearchHeader" TagPrefix="uc1" %>
<%@ Register Src="Component/SearchFooter.ascx" TagName="SearchFooter" TagPrefix="uc2" %>
<%@ Register Src="Component/Ervenyesseg_SearchFormComponent.ascx" TagName="Ervenyesseg_SearchFormComponent"
    TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <uc1:SearchHeader id="SearchHeader1" runat="server" >
    </uc1:SearchHeader>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="True"
    EnableScriptLocalization="true" ></asp:ScriptManager>
    <br />
        <table cellpadding="0" cellspacing="0" width="90%">
            <tr>
                <td style="width: 1027px">
                    <eUI:eFormPanel ID="EFormPanel1" runat="server">
                    <table cellspacing="0" cellpadding="0" width="99%">
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAgazatiJelKod" runat="server" Text="�gazati jel k�d:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="AgazatiJelKodTextBox" runat="server" CssClass="mrUrlapInput"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelAgazatiJelNev" runat="server" Text="�gazati jel megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="AgazatiJelNevTextBox" runat="server" CssClass="mrUrlapInput"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label2" runat="server" Text="Iratt�ri t�telsz�m:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="IrattariTetelszamTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                &nbsp;&nbsp;
                                <asp:Label ID="Label16" runat="server" Text="Iratt�ri t�tel megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="Nev_TextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="labelUgytipuskod" runat="server" Text="�gyt�pus k�d:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:TextBox ID="UgyTipusKodTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label5" runat="server" Text="�gyt�pus megnevez�s:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop" colspan="3">
                                <asp:TextBox ID="UgyTipusNevTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label1" runat="server" Text="Elj�r�si szakasz:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="EljarasiSzakaszKodtarakDropDownList" runat="server" CssClass="mrUrlapInputComboBox"/>
                            </td>                            
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label7" runat="server" Text="Iratt�pus:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="IratTipusKodtarakDropDownList" runat="server" CssClass="mrUrlapInputComboBox"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption">
                                <asp:Label ID="Label8" runat="server" Text="Gener�lt t�rgy:"></asp:Label></td>
                            <td class="mrUrlapMezoVAlignTop">
                                <asp:TextBox ID="GeneraltTargyTextBox" runat="server" CssClass="mrUrlapInput"></asp:TextBox></td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                <asp:Label ID="Label12" runat="server" Text="Szign�l�s t�pusa:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc11:KodtarakDropDownList ID="SzignalasTipusaKodtarakDropDownList" runat="server" Width="350px"/>
                            </td>
                        </tr>
                        <tr class="urlapSor">
                            <td class="mrUrlapCaption" >
                                <asp:Label ID="Label13" runat="server" Text="Javasolt felel�s:"></asp:Label></td>
                            <td class="mrUrlapMezo">
                                <uc3:CsoportTextBox ID="JavasoltFelelosCsoportTextBox" runat="server" Validate="false" />
                            </td>
                        </tr>                        
                        <tr class="urlapSor">
                            <td colspan="2" rowspan="2" style="padding-bottom:30px">
                                <uc5:Ervenyesseg_SearchFormComponent ID="Ervenyesseg_SearchFormComponent1" runat="server">
                                </uc5:Ervenyesseg_SearchFormComponent>
                            </td>
                        </tr>
<%-- 
                        <tr class="urlapSor">
                            <td colspan="4">
                                <uc10:TalalatokSzama_SearchFormComponent ID="TalalatokSzama_SearchFormComponent1" runat="server" />
                            </td>
                        </tr>
                        --%>
                    </table>
                    </eUI:eFormPanel>
                    
                    <table cellpadding="0" cellspacing="0"">
                        <tr class="urlapSor">
                            <td colspan="2">
                                &nbsp;</td>
                            <td colspan="2">
                            <uc2:SearchFooter ID="SearchFooter1" runat="server" />
                            </td>
                        </tr>                      
                      </table>

                </td>
            </tr>
        </table>
</asp:Content>

