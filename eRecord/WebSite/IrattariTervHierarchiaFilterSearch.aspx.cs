using System;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Contentum.eAdmin.Utility;
using Contentum.eQuery.BusinessDocuments;
using Contentum.eQuery;
using Contentum.eBusinessDocuments;
using Contentum.eAdmin.Service;
using Contentum.eRecord.Service;


public partial class IrattariTervHierarchiaFilterSearch : Contentum.eUtility.UI.PageBase
{
    /*
    public const string Header = "$Header: IrattariTervHierarchiaFilterSearch.aspx.cs, 3, 2009.07.07. 12:40:47, Boda Eszter$";
    public const string Version = "$Revision: 3$";
    */

    private Type _type = typeof(IrattariTervHierarchiaSearch);
    private const string kcsKod_IRATTARI_JEL = "IRATTARI_JEL";
    private const string kcs_ELJARASISZAKASZ = "ELJARASI_SZAKASZ";
    private const string kcs_IRATTIPUS = "IRATTIPUS";
    private const string kcs_SZIGNALASTIPUSA = "SZIGNALAS_TIPUSA";

    protected void Page_Init(object sender, EventArgs e)
    {
        SearchHeader1.TemplateObjectType = _type;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Logger.Info("Page_Load (" + Resources.Search.IratMetaDefinicioSearchHeaderTitle + ")");
        SearchHeader1.HeaderTitle = Resources.Search.IratMetaDefinicioSearchHeaderTitle;

        SearchHeader1.ButtonsClick +=
            new CommandEventHandler(SearchHeader1_ButtonsClick);
        SearchFooter1.ButtonsClick +=
           new CommandEventHandler(SearchFooter1_ButtonsClick);

        if (!IsPostBack)
        {
            IrattariTervHierarchiaSearch searchObject = null;
            if (Search.IsSearchObjectInSession(Page, _type))
            {
                searchObject = (IrattariTervHierarchiaSearch)Search.GetSearchObject(Page, new IrattariTervHierarchiaSearch());
            }
            else
            {
                searchObject = GetDefaultSearchObject();
            }
            LoadComponentsFromSearchObject(searchObject);
        }

    }

    /// <summary>
    /// Keresési objektum --> Form
    /// </summary>
    private void LoadComponentsFromSearchObject(object searchObject)
    {
        Logger.Info("LoadComponentsFromSearchObject");
        IrattariTervHierarchiaSearch irattariTervHierarchiaSearch = null;
        if (searchObject != null) irattariTervHierarchiaSearch = (IrattariTervHierarchiaSearch)searchObject;

        if (irattariTervHierarchiaSearch != null)
        {
            AgazatiJelKodTextBox.Text = irattariTervHierarchiaSearch.EREC_AgazatiJelekSearch.Kod.Value;
            AgazatiJelNevTextBox.Text = irattariTervHierarchiaSearch.EREC_AgazatiJelekSearch.Nev.Value;

            IrattariTetelszamTextBox.Text = irattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch.IrattariTetelszam.Value;
            Nev_TextBox.Text = irattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch.Nev.Value;
                   
            EljarasiSzakaszKodtarakDropDownList.FillAndSetEmptyValue(kcs_ELJARASISZAKASZ, SearchHeader1.ErrorPanel);
            EljarasiSzakaszKodtarakDropDownList.SetSelectedValue(irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.EljarasiSzakasz.Value);

            IratTipusKodtarakDropDownList.FillAndSetEmptyValue(kcs_IRATTIPUS, SearchHeader1.ErrorPanel);
            IratTipusKodtarakDropDownList.SetSelectedValue(irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Irattipus.Value);

            UgyTipusNevTextBox.Text = irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.UgytipusNev.Value;

            UgyTipusKodTextBox.Text = irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Ugytipus.Value;

            GeneraltTargyTextBox.Text = irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.GeneraltTargy.Value;

            JavasoltFelelosCsoportTextBox.Id_HiddenField = irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Manual_Csoport_Id_Felelos.Value;
            JavasoltFelelosCsoportTextBox.SetCsoportTextBoxById(SearchHeader1.ErrorPanel);

            SzignalasTipusaKodtarakDropDownList.FillAndSetEmptyValue(kcs_SZIGNALASTIPUSA, SearchHeader1.ErrorPanel);
            SzignalasTipusaKodtarakDropDownList.SetSelectedValue(irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Manual_SzignalasTipusa.Value);

            Ervenyesseg_SearchFormComponent1.SetDefault(irattariTervHierarchiaSearch.ErvKezd, irattariTervHierarchiaSearch.ErvVege);
        }
    }

    /// <summary>
    /// Form --> Keresési objektum
    /// </summary>
    private IrattariTervHierarchiaSearch SetSearchObjectFromComponents()
    {
        Logger.Info("SetSearchObjectFromComponents");
        IrattariTervHierarchiaSearch irattariTervHierarchiaSearch = (IrattariTervHierarchiaSearch)SearchHeader1.TemplateObject;
        if (irattariTervHierarchiaSearch == null)
        {
            irattariTervHierarchiaSearch = new IrattariTervHierarchiaSearch();
        }
        
        if (!String.IsNullOrEmpty(AgazatiJelKodTextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_AgazatiJelekSearch.Kod.Value = AgazatiJelKodTextBox.Text;
            irattariTervHierarchiaSearch.EREC_AgazatiJelekSearch.Kod.Operator = Search.GetOperatorByLikeCharater(AgazatiJelKodTextBox.Text);
        }

        if (!String.IsNullOrEmpty(AgazatiJelNevTextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_AgazatiJelekSearch.Nev.Value = AgazatiJelNevTextBox.Text;
            irattariTervHierarchiaSearch.EREC_AgazatiJelekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(AgazatiJelNevTextBox.Text);
        }

        if (!String.IsNullOrEmpty(IrattariTetelszamTextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch.IrattariTetelszam.Value = IrattariTetelszamTextBox.Text;
            irattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch.IrattariTetelszam.Operator = Search.GetOperatorByLikeCharater(IrattariTetelszamTextBox.Text);
        }

        if (!String.IsNullOrEmpty(Nev_TextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch.Nev.Value = Nev_TextBox.Text;
            irattariTervHierarchiaSearch.EREC_IraIrattariTetelekSearch.Nev.Operator = Search.GetOperatorByLikeCharater(Nev_TextBox.Text);
        }


        if (!String.IsNullOrEmpty(EljarasiSzakaszKodtarakDropDownList.SelectedValue))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.EljarasiSzakasz.Value = EljarasiSzakaszKodtarakDropDownList.SelectedValue;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.EljarasiSzakasz.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(IratTipusKodtarakDropDownList.SelectedValue))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Irattipus.Value = IratTipusKodtarakDropDownList.SelectedValue;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Irattipus.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(UgyTipusKodTextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Ugytipus.Value = UgyTipusKodTextBox.Text;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Ugytipus.Operator = Search.GetOperatorByLikeCharater(UgyTipusKodTextBox.Text);
        }

        if (!String.IsNullOrEmpty(UgyTipusNevTextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.UgytipusNev.Value = UgyTipusNevTextBox.Text;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.UgytipusNev.Operator = Search.GetOperatorByLikeCharater(UgyTipusNevTextBox.Text);
        }

        if (!String.IsNullOrEmpty(GeneraltTargyTextBox.Text))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.GeneraltTargy.Value = GeneraltTargyTextBox.Text;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.GeneraltTargy.Operator = Search.GetOperatorByLikeCharater(GeneraltTargyTextBox.Text);
        }

        if (!String.IsNullOrEmpty(JavasoltFelelosCsoportTextBox.Id_HiddenField))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Manual_Csoport_Id_Felelos.Value = JavasoltFelelosCsoportTextBox.Id_HiddenField;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Manual_Csoport_Id_Felelos.Operator = Query.Operators.equals;
        }

        if (!String.IsNullOrEmpty(SzignalasTipusaKodtarakDropDownList.SelectedValue))
        {
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Manual_SzignalasTipusa.Value = SzignalasTipusaKodtarakDropDownList.SelectedValue;
            irattariTervHierarchiaSearch.EREC_IratMetaDefinicioSearch.Manual_SzignalasTipusa.Operator = Query.Operators.equals;
        }

        Ervenyesseg_SearchFormComponent1.SetSearchObjectFields(irattariTervHierarchiaSearch.ErvKezd, irattariTervHierarchiaSearch.ErvVege);

        return irattariTervHierarchiaSearch;
    }



    /// <summary>
    /// FormTemplateLoader eseményei
    /// </summary>
    void SearchHeader1_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("SearchHeader1_ButtonsClick (Command = " + e.CommandName + ")");
        if (e.CommandName == CommandName.LoadTemplate)
        {
            LoadComponentsFromSearchObject(SearchHeader1.TemplateObject);
        }
        else if (e.CommandName == CommandName.NewTemplate)
        {
            SearchHeader1.NewTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.SaveTemplate)
        {
            SearchHeader1.SaveCurrentTemplate(SetSearchObjectFromComponents());
        }
        else if (e.CommandName == CommandName.InvalidateTemplate)
        {
            // FormTemplateLoader lekezeli           
        }
    }


    void SearchFooter1_ButtonsClick(object sender, CommandEventArgs e)
    {
        Logger.Info("SearchFooter1_ButtonsClick (Command = " + e.CommandName + ")");
        if (e.CommandName == CommandName.Default)
        {
            LoadComponentsFromSearchObject(GetDefaultSearchObject());
        }
        else if (e.CommandName == CommandName.Search)
        {
            IrattariTervHierarchiaSearch searchObject = SetSearchObjectFromComponents();
            IrattariTervHierarchiaSearch defaultSearchObject = GetDefaultSearchObject();

            if (Search.IsIdentical(searchObject.EREC_AgazatiJelekSearch, defaultSearchObject.EREC_AgazatiJelekSearch)
                && Search.IsIdentical(searchObject.EREC_IraIrattariTetelekSearch, defaultSearchObject.EREC_IraIrattariTetelekSearch)
                && Search.IsIdentical(searchObject.EREC_IratMetaDefinicioSearch, defaultSearchObject.EREC_IratMetaDefinicioSearch)
                && Search.IsIdentical(searchObject, defaultSearchObject)
                )
            {
                //default searchobject
                Search.RemoveSearchObjectFromSession(Page, _type);
            }
            else
            {
                //Keresési feltételek elmentése kiiratható formában
                searchObject.ReadableWhere = Search.GetReadableWhere(Form);
                Search.SetSearchObject(Page, searchObject);
            }
            //TalalatokSzama_SearchFormComponent1.SetMasterRowCountToSession();
            JavaScripts.RegisterCloseWindowClientScript(Page, true);
        }
    }

    private IrattariTervHierarchiaSearch GetDefaultSearchObject()
    {
        IrattariTervHierarchiaSearch irattariTervHierarchiaSearch = new IrattariTervHierarchiaSearch();
        // érvényességet nem adunk át
        return irattariTervHierarchiaSearch;
    }

}
