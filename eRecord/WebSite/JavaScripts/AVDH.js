﻿// A DokumentumAlairas.aspx-en van használva ez a fájl,
// az ott lévő HiddenField-ek alapján működik, amik szerver oldalon vannak kitöltve

function AVDH_GetHiddenfieldValue(fieldID) {
    var hiddenField = document.getElementById("ctl00_ContentPlaceHolder1_AVDH1_" + fieldID);
    if (hiddenField !== null) {
        return hiddenField.getAttribute("value");
    }
    else {
        return null;
    }
};
function startLogin() {
    showUpdateProgress('Folyamatban...');
    var iratIds = AVDH_GetHiddenfieldValue("hfIratIds");
    var recordIds = AVDH_GetHiddenfieldValue("hfRecordIds");
    var csatolmanyokIds = AVDH_GetHiddenfieldValue("hfCsatolmanyokIds");
    var folyamatTetelekIds = AVDH_GetHiddenfieldValue("hfFolyamatTetelekIds");    
    var fileNames = AVDH_GetHiddenfieldValue("hfFilenames");
    var externalLinks = AVDH_GetHiddenfieldValue("hfExternalLinks");
    
    var procId = AVDH_GetHiddenfieldValue("hfProcId");
    if (externalLinks !== null) {
        var arr = externalLinks.split(';');
        var arrayLength = arr.length;
        for (var i = 0; i < arrayLength; i++) {

            window.open(arr[i]);
        }
        AVDHCallbackService.StarDownload(iratIds, recordIds, csatolmanyokIds, folyamatTetelekIds, fileNames, procId, onDownload);
    }
};
function onDownload(result) {
    hideUpdateProgress();
    if (!result.IsError) {
        window.returnValue = true;
        if (window.opener && window.returnValue && window.opener.__doPostBack) { window.opener.__doPostBack('', window.opener.postBackArgument); }        
    }
    else {
        handleError(result.ErrorMessage);
    }
};
function showUpdateProgress(text) {
    if (text) {
        $('#updateProgressText').html(text);
    }
    var progressPanel = $('#UpdateProgressPanel');
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    progressPanel[0].style.left = (width / 2).toString() + "px";
    progressPanel.show();
};
function hideUpdateProgress() {
    $('#UpdateProgressPanel').hide();
};
function handleError(message) {
    alert(message);
};
