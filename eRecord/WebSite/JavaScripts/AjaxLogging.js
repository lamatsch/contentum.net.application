﻿///<reference name="MicrosoftAjax.js" />


Type.registerNamespace('AjaxLogging');

var ERROR_CODE_UNHANDLED = 1;
var ERROR_CODE_BASE = 1000;
var ERROR_CODE_WEB_SERVICE = (ERROR_CODE_BASE + 1);
var ERROR_CODE_REGULAR = (ERROR_CODE_BASE + 2);
var ERROR_CODE_USER_DEFINED = (ERROR_CODE_BASE + 3);
var ERROR_CODE_UPDATE_PANEL = (ERROR_CODE_BASE + 4);

AjaxLogging.BaseTraceListener = function(lineSeparator)
{
    this._lineSeparator = '\n';

    if (lineSeparator)
    {
        this._lineSeparator = lineSeparator;
    }
}

AjaxLogging.BaseTraceListener.prototype =
{
    get_lineSeparator : function()
    {
        if (arguments.length !== 0) throw Error.parameterCount();

        return this._lineSeparator;
    },

    dispose : function()
    {
    },

    publishException : function(errorCode, exception, environmentInfo)
    {
        throw Error.notImplemented();
    },

    formatException : function(errorCode, exception)
    {
        var lineSeparator = this.get_lineSeparator();
        var errorInfo = new Sys.StringBuilder();

        errorInfo.append('Error Code: ' + errorCode.toString() + lineSeparator);

        if (typeof exception.get_exceptionType != 'undefined')
        {
            errorInfo.append(   'Message: ' + exception.get_message() + lineSeparator +
                                'Type: ' + exception.get_exceptionType() + lineSeparator +
                                'StackTrace: ' + exception.get_stackTrace()
                            );
        }
        else
        {
            errorInfo.append(   'Message: ' + exception.message + lineSeparator +
                                'Type: ' + exception.name + lineSeparator +
                                'Description: ' + exception.description
                            );

            //Only Update Panel has this field
            if (typeof exception.httpStatusCode != 'undefined')
            {
                errorInfo.append(lineSeparator + 'HttpStatusCode: ' + exception.httpStatusCode);
            }

            //Only Unhandled error have the following two field

            if (typeof exception.lineNumber != 'undefined')
            {
                errorInfo.append(lineSeparator + 'LineNumber: ' + exception.lineNumber);
            }

            if (typeof exception.url != 'undefined')
            {
                errorInfo.append(lineSeparator + 'Url: ' + exception.url);
            }
        }

        return errorInfo.toString();
    },
    
    getTimeString: function()
    {
        var now = new Date();
        now = now.toLocaleString();
        var timeString = "A hiba fellépésének időpontja: " + now;
        return timeString;
    }
}

AjaxLogging.BaseTraceListener.registerClass('AjaxLogging.BaseTraceListener', null, Sys.IDisposable);


AjaxLogging.SysDebugTraceListener = function(lineSeparator)
{
    AjaxLogging.SysDebugTraceListener.initializeBase(this, [lineSeparator]);
}

AjaxLogging.SysDebugTraceListener.prototype =
{
    dispose : function()
    {
        AjaxLogging.SysDebugTraceListener.callBaseMethod(this, 'dispose');
    },

    publishException : function(errorCode, exception, environmentInfo)
    {
        //Calling the base class method
        var errorInfo = AjaxLogging.SysDebugTraceListener.callBaseMethod(this, 'formatException', [errorCode, exception]);
        Sys.Debug.traceDump(errorInfo);
    }
}

AjaxLogging.SysDebugTraceListener.registerClass('AjaxLogging.SysDebugTraceListener', AjaxLogging.BaseTraceListener);


AjaxLogging.AlertTraceListener = function(lineSeparator)
{
    AjaxLogging.AlertTraceListener.initializeBase(this, [lineSeparator]);
}

AjaxLogging.AlertTraceListener.prototype =
{
    dispose : function()
    {
        AjaxLogging.AlertTraceListener.callBaseMethod(this, 'dispose');
    },

    publishException : function(errorCode, exception, environmentInfo)
    {
        //Calling the base class method
        var errorInfo = AjaxLogging.AlertTraceListener.callBaseMethod(this, 'formatException', [errorCode, exception]);
        alert(errorInfo);
    }
}

AjaxLogging.AlertTraceListener.registerClass('AjaxLogging.AlertTraceListener', AjaxLogging.BaseTraceListener);


AjaxLogging.DivTraceListener = function(lineSeparator, targetElement)
{
    if (typeof targetElement == 'string')
    {
        this._element = $get(targetElement);
    }
    else
    {
        this._element = targetElement;
    }

    AjaxLogging.DivTraceListener.initializeBase(this, [lineSeparator]);
}

AjaxLogging.DivTraceListener.prototype =
{
    dispose : function()
    {
        this._element = null;
        AjaxLogging.DivTraceListener.callBaseMethod(this, 'dispose');
    },

    publishException : function(errorCode, exception, environmentInfo)
    {
        //Calling the base class methods
        var errorInfo = AjaxLogging.DivTraceListener.callBaseMethod(this, 'formatException', [errorCode, exception]);
        var lineSeparator = AjaxLogging.DivTraceListener.callBaseMethod(this, 'get_lineSeparator')

        if (this._element.innerHTML.length > 0)
        {
            //Put some extra breaks
            this._element.innerHTML = lineSeparator + lineSeparator + this._element.innerHTML;
        }

        this._element.innerHTML = errorInfo + this._element.innerHTML;
    }
}

AjaxLogging.DivTraceListener.registerClass('AjaxLogging.DivTraceListener', AjaxLogging.BaseTraceListener);


AjaxLogging.WebServiceTraceListener = function(lineSeparator, servicePath, serviceMethod)
{
    this._servicePath = servicePath;
    this._serviceMethod = serviceMethod;
    AjaxLogging.WebServiceTraceListener.initializeBase(this, [lineSeparator]);
}

AjaxLogging.WebServiceTraceListener.prototype =
{
    dispose : function()
    {
        AjaxLogging.WebServiceTraceListener.callBaseMethod(this, 'dispose');
        this._servicePath = null;
        this._serviceMethod = null;
    },

    publishException: function(errorCode, exception, environmentInfo)
    {
        //Calling the base class method
        var errorInfo = AjaxLogging.WebServiceTraceListener.callBaseMethod(this, 'formatException', [errorCode, exception]);
        
        var params = { 'errorCode' : errorCode, 'exception': errorInfo, 'url': environmentInfo.url,
                       'referrer': environmentInfo.referrer, 'scripts': environmentInfo.scripts };
                
        // Invoke the web service
        Sys.Net.WebServiceProxy.invoke(this._servicePath, this._serviceMethod, false, params);
    }
}

AjaxLogging.WebServiceTraceListener.registerClass('AjaxLogging.WebServiceTraceListener', AjaxLogging.BaseTraceListener);

AjaxLogging.ModalPopupTraceListener = function(lineSeparator, targetPopup, popupHeader, popupError, popupDetails)
{
    if (typeof targetPopup == 'string')
    {
        this._popup = $find(targetPopup);
    }
    else
    {
        this._popup = targetPopup;
    }

    if (typeof popupHeader == 'string')
    {
        this._header = $get(popupHeader);
    }
    else
    {
        this._header = popupHeader;
    }
    
    if (typeof popupError == 'string')
    {
        this._error = $get(popupError);
    }
    else
    {
        this._error = popupError;
    }
    
    if (typeof popupDetails == 'string')
    {
        this._details = $get(popupDetails);
    }
    else
    {
        this._details = popupDetails;
    }

    AjaxLogging.ModalPopupTraceListener.initializeBase(this, [lineSeparator]);
}

AjaxLogging.ModalPopupTraceListener.prototype =
{
    dispose : function()
    {
        this._error = null;
        this._header = null;
        this._details = null;
        this._popup = null;
        AjaxLogging.ModalPopupTraceListener.callBaseMethod(this, 'dispose');
    },

    publishException : function(errorCode, exception, environmentInfo)
    {
        var lineSeparator = AjaxLogging.ModalPopupTraceListener.callBaseMethod(this, 'get_lineSeparator');
        var error = 'A Javascript kód futtatása közben általános hiba lépett fel.';
        var header = 'Javascript hiba!';
        if(errorCode == ERROR_CODE_UPDATE_PANEL)
        {
            header = 'Oldalfrissítési hiba!';
            switch(exception.name)
            {
               case 'Sys.WebForms.PageRequestManagerParserErrorException':
                error = 'A hiba a weblap újratöltésével orvosolható (F5 gomb).';
                break;
               case 'Sys.WebForms.PageRequestManagerServerErrorException':
                error = 'A kérés feldolgozás közben a szerveren hiba lépett fel.';
                break;
               case 'Sys.WebForms.PageRequestManagerTimeoutException':
                error = 'A kérésre nem érkezett válasz a megadott időintervallumon belül.';
                break;
               default:
                error = 'Általános hiba lépett fel.'; 
            }
            
            var refreshButtonId = this._popup.get_CancelControlID();
            if(refreshButtonId != '')
            {
                var refreshButton = $get(refreshButtonId);
                if(refreshButton)
                    refreshButton.style.display = '';
            }
            
            var okButtonId = this._popup.get_OkControlID();
            if(okButtonId != '')
            {
                var okButton = $get(okButtonId);
                if(okButton)
                    okButton.style.left = '20px';
            }
        }
        if(errorCode == ERROR_CODE_UNHANDLED)
        {
            error = 'A Javascript kód futtatása közben hiba lépett fel.';
        }
        
        if(errorCode == ERROR_CODE_WEB_SERVICE)
        {
            error = 'Hiba lépett fel a webszolgáltatás hívásakor.';
        }
        
        error = error + lineSeparator  + AjaxLogging.ModalPopupTraceListener.callBaseMethod(this, 'getTimeString');
        this._error.innerHTML = error;
        this._header.innerHTML = header;
        this._details.innerHTML = AjaxLogging.ModalPopupTraceListener.callBaseMethod(this, 'formatException', [errorCode, exception]);
        this._popup.show();
    }
}

AjaxLogging.ModalPopupTraceListener.registerClass('AjaxLogging.ModalPopupTraceListener', AjaxLogging.BaseTraceListener);

AjaxLogging.ExceptionManager = function()
{
    this._listeners = new Array();
    AjaxLogging.ExceptionManager.initializeBase(this);
}

AjaxLogging.ExceptionManager.prototype =
{
    initialize : function()
    {
        AjaxLogging.ExceptionManager.callBaseMethod(this, 'initialize');
    },

    dispose : function()
    {
        if (this._listeners.length > 0)
        {
            for(var i = 0; i < this._listeners.length; i++)
            {
                this._listeners[i].dispose();
            }
        }

        delete this._listeners;

        AjaxLogging.ExceptionManager.callBaseMethod(this, 'dispose');
    },

    addListener : function(listener)
    {
        var e = Function._validateParams(arguments, [{name: 'listener', type: AjaxLogging.BaseTraceListener}]);
        if (e) throw e;

        Array.add(this._listeners, listener);
    },

    removeListener : function(listener)
    {
        var e = Function._validateParams(arguments, [{name: 'listener', type: AjaxLogging.BaseTraceListener}]);
        if (e) throw e;

        listener.dispose();

        Array.remove(this._listeners, listener);
    },

    publishException : function(errorCode, exception)
    {
        var e1 = Function._validateParams(arguments, [{name: 'errorCode', type: Number}, {name: 'exception', type: Error}]);
        var e2 = Function._validateParams(arguments, [{name: 'errorCode', type: Number}, {name: 'exception', type: Sys.Net.WebServiceError}]);

        if ((e1) && (e2))
        {
            throw e1;
        }
        
        if (typeof this._listeners !== 'undefined' && this._listeners != null && this._listeners.length > 0)
        {
            var environmentInfo = this._getEnvironmentInfo();

            for(var i = 0; i < this._listeners.length; i++)
            {
                this._listeners[i].publishException(errorCode, exception, environmentInfo);
            }
        }
    },

    _getEnvironmentInfo : function()
    {
        var scriptTags = document.getElementsByTagName('script');
        var scripts = new Array(); 

        if (scriptTags)
        {
            for(var i = 0; i < scriptTags.length; i++)
            {
                var scriptTag = scriptTags[i];

                if (typeof scriptTag.src != 'undefined')
                {
                    if (scriptTag.src.length > 0)
                    {
                        Array.add(scripts, scriptTag.src + ' : ' + scriptTag.readyState);
                    }
                }
            }
        }

        var url = window.location.href;

        var referrer = '';

        if (document.referrer)
        {
            if (document.referrer.length > 0)
            {
                referrer = document.referrer;
            }
        }

        return {url:url, referrer:referrer, scripts:scripts};
    }
}

AjaxLogging.ExceptionManager.registerClass('AjaxLogging.ExceptionManager', Sys.Component);

//Making the ExceptionManager a Singleton Class, but unfortunately
//there is no way to restrict its constructor to be called.

AjaxLogging.ExceptionManager._staticInstance = $create(AjaxLogging.ExceptionManager, {'id':'exceptionManager'});

AjaxLogging.ExceptionManager.getInstance = function()
{
    return AjaxLogging.ExceptionManager._staticInstance;
}

if (typeof(Sys) != 'undefined')
{
    Sys.Application.notifyScriptLoaded();
}