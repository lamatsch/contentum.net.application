﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.PartnerAutoComplete = function (element) {
    Utility.PartnerAutoComplete.initializeBase(this, [element]);

    //Properties
    this.autoCompleteExtenderClientId = '';
    this.hiddenFieldClientId = '';
    this.cimTextBoxClientId = '';
    this.cimHiddenFieldClientId = '';
    this.clonePartnerTextBoxClientId = '';
    this.clonePartnerHiddenFieldClientId = '';
    this.cloneCimTextBoxClientId = '';
    this.cloneCimHiddenFieldClientId = '';
    this.delimeter = '';
    this.checkBoxClientId = '';
    this.createToolTipOnItems = '0';    // ha '1', tooltip generálása a legördülő elemek fölé
    this.customTextEnabled = true; // ha false, és üres a hiddenfield a mező elhagyásakor (blur), töröljük a szövegdobozt
    //Variables
    this.checkBoxState = true;
    this.checkBoxChanged = true;
    this.checkBoxCheckedText = 'Minden cím';
    this.checkBoxUnCheckedText = 'Csak levelezési cím';
    this.itemSelectedHandler = null;
    this.populatingHandler = null;
    this.populatedHandler = null;
    this.showingHandler = null;
    this.blurHandler = null;
    this.textChangedHandler = null;
    this.checkBoxChangedHandler = null;
    this.cimTextBoxChangedHandler = null;
    this.applicationLoadHandler = null;
    this.alternateRowStyle = 'GridViewAlternateRowStyle';
    this.lastPrefix = null;
    this.keyDownHandler = null;
    this.cancelShowing = false;
    this.cacheArray = new Array();
    this.itemOverHandler = null;
    this.itemOutHandler = null;
    this.shownHandler = null;
    this.hiddenValue = ''; // hidden field értéke
    this.hiddenChangedHandler = null;

    this._fixHandleScroll = null;
    this._listKeyDownHandler = null;

    this._listMaxHeight = null;
    this._windowMaxScrollPosition = null;
    this._listMaxWidth = null;
    this._fullWidthMode = false;
    this._listElementMaxWidth = -1;
    this._scrollerWidth = 20;
    this.ItemSelecting = false;
}

//statikus váltózók ás függvények
Utility.PartnerAutoComplete.forrasDelimeter = ":";
Utility.PartnerAutoComplete.needForras = true;
Utility.PartnerAutoComplete.CutForras = function (text) {
    if (Utility.PartnerAutoComplete.needForras) {
        var index = text.indexOf(Utility.PartnerAutoComplete.forrasDelimeter);
        if (index == 1) {
            text = text.substring(index + 1).trim();
        }
    }

    return text;
};

Utility.PartnerAutoComplete.getHiddenValue = function () {
    return this.hiddenValue;
};

Utility.PartnerAutoComplete.prototype = {
    initialize: function () {
        Utility.PartnerAutoComplete.callBaseMethod(this, 'initialize');

        this.itemSelectedHandler = Function.createDelegate(this, this.OnItemSelected);
        this.populatingHandler = Function.createDelegate(this, this.OnPopulating);
        this.populatedHandler = Function.createDelegate(this, this.OnPopulated);
        this.showingHandler = Function.createDelegate(this, this.OnShowing);
        this.blurHandler = Function.createDelegate(this, this.OnBlur);
        this.textChangedHandler = Function.createDelegate(this, this.OnTextChanged);
        this.checkBoxChangedHandler = Function.createDelegate(this, this.OnCheckBoxChanged);
        this.cimTextBoxChangedHandler = Function.createDelegate(this, this.OnCimTextBoxChanged);
        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);
        this.keyDownHandler = Function.createDelegate(this, this.OnKeyDown);
        this.itemOverHandler = Function.createDelegate(this, this.OnItemOver);
        this.itemOutHandler = Function.createDelegate(this, this.OnItemOut);
        this.shownHandler = Function.createDelegate(this, this.OnShown);
        this._fixHandleScroll = Function.createDelegate(this, this.FixHandleScroll);
        this._listKeyDownHandler = Function.createDelegate(this, this.OnListKeyDown);
        this.hiddenChangedHandler = Function.createDelegate(this, this.OnHiddenTextChanged);

        Sys.Application.add_load(this.applicationLoadHandler);
    },

    dispose: function () {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (autoCompleteExtender) {
            if (this.itemSelectedHandler != null) {

                autoCompleteExtender.remove_itemSelected(this.itemSelectedHandler);
            }

            if (this.populatingHandler != null) {

                autoCompleteExtender.remove_populating(this.populatingHandler);

            }

            if (this.populatedHandler != null) {
                autoCompleteExtender.remove_populated(this.populatedHandler);
            }
            if (this.showingHandler != null) {
                autoCompleteExtender.remove_showing(this.showingHandler);
            }
            if (this.blurHandler != null && !this.customTextEnabled) {
                var elt = this.get_element();
                if (elt) {
                    $removeHandler(elt, 'blur', this.blurHandler);
                }
                this.blurHandler = null;
            }
            if (this.textChangedHandler != null) {
                var elt = this.get_element();
                if (elt) {
                    $removeHandler(elt, 'change', this.textChangedHandler);
                }
                this.textChangedHandler = null;
            }
            if (this.itemOverHandler != null) {
                autoCompleteExtender.remove_itemOver(this.itemOverHandler);
            }
            if (this.itemOutHandler != null) {
                autoCompleteExtender.remove_itemOut(this.itemOutHandler);
            }
            if (this.shownHandler != null) {
                autoCompleteExtender.remove_showing(this.shownHandler);
            }

            var list = autoCompleteExtender.get_completionList();
            if (list) {
                if (this._listKeyDownHandler != null) {
                    $removeHandler(document.body, "keydown", this._listKeyDownHandler);
                }
            }

            autoCompleteExtender._handleScroll = null;
        }

        var elt = this.get_element();
        if (elt) {
            if (this.keyDownHandler != null) {
                $removeHandler(elt, "keydown", this.keyDownHandler);
            }
        }


        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
        }

        if (this.checkBoxClientId.trim() != '') {
            var checkBox = $get(this.checkBoxClientId);
            if (checkBox != null) {
                if (this.checkBoxChangedHandler != null) {
                    $removeHandler(checkBox, 'click', this.checkBoxChangedHandler);
                }
            }
        }

        if (this.cimTextBoxClientId.trim() != '') {
            var cimTextBox = $get(this.cimTextBoxClientId);
            if (cimTextBox != null) {
                if (this.cimTextBoxChangedHandler != null) {
                    $removeHandler(cimTextBox, 'change', this.cimTextBoxChangedHandler);
                }
            }
        }

        if (this.hiddenFieldClientId.trim() != '') {
            var hidden = $get(this.hiddenFieldClientId);
            if (hidden != null) {
                if (this.hiddenChangedHandler != null) {
                    $removeHandler(hidden, 'change', this.hiddenChangedHandler);
                }
            }
        }

        this.itemSelectedHandler = null;
        this.populatingHandler = null;
        this.populatedHandler = null;
        this.showingHandler = null;
        this.checkBoxChangedHandler = null;
        this.cimTextBoxChangedHandler = null;
        this.applicationLoadHandler = null;
        this.keyDownHandler = null;
        this.itemOverHandler = null;
        this.itemOutHandler = null;
        this.shownHandler = null;
        this._fixHandleScroll = null;
        this._listKeyDownHandler = null;
        this.hiddenChangedHandler = null;

        Utility.PartnerAutoComplete.callBaseMethod(this, 'dispose');
    },

    OnApplicationLoad: function () {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (autoCompleteExtender != null) {
            autoCompleteExtender.add_itemSelected(this.itemSelectedHandler);
            autoCompleteExtender.add_populating(this.populatingHandler);
            autoCompleteExtender.add_populated(this.populatedHandler);
            autoCompleteExtender.add_showing(this.showingHandler);
            autoCompleteExtender.add_itemOver(this.itemOverHandler);
            autoCompleteExtender.add_itemOut(this.itemOutHandler);
            autoCompleteExtender.add_showing(this.shownHandler);

            var list = autoCompleteExtender.get_completionList();
            if (list) {
                $addHandler(document.body, "keydown", this._listKeyDownHandler);
            }
            this.SetContextKey(autoCompleteExtender);
            autoCompleteExtender.set_firstRowSelected(false);

            autoCompleteExtender._handleScroll = this._fixHandleScroll;
            var elt = this.get_element();
            if (elt) {
                $removeHandler(elt, "focus", autoCompleteExtender._focusHandler);
                autoCompleteExtender._focusHandler = Function.createDelegate(this, this.OnTextBoxFocus);
                $addHandler(elt, "focus", autoCompleteExtender._focusHandler);
            }
        }

        var elt = this.get_element();
        if (elt) {
            $addHandler(elt, "keydown", this.keyDownHandler);

            if (!this.customTextEnabled) {
                $addHandler(elt, 'blur', this.blurHandler);
            }

            $addHandler(elt, 'change', this.textChangedHandler);
        }

        if (this.checkBoxClientId.trim() != '') {
            var checkBox = $get(this.checkBoxClientId);
            if (checkBox != null) {
                $addHandler(checkBox, 'click', this.checkBoxChangedHandler);
            }
        }

        if (this.cimTextBoxClientId.trim() != '') {
            var cimTextBox = $get(this.cimTextBoxClientId);
            if (cimTextBox != null) {
                $addHandler(cimTextBox, 'change', this.cimTextBoxChangedHandler);
            }
        }

        if (this.hiddenFieldClientId.trim() != '') {
            var hidden = $get(this.hiddenFieldClientId);
            if (hidden != null) {
                if (this.hiddenChangedHandler != null) {
                    $addHandler(hidden, 'change', this.hiddenChangedHandler);
                }
            }
        }

        this._setScrollParameters();
    },

    OnItemSelected: function (sender, args) {
        this.ItemSelecting = true;

        var text = args.get_text();
        var cimText = '';

        if (text != null) {
            var partnerText = text;
            var texts = text.split(this.delimeter);
            if (texts != null && texts.length > 1) {
                partnerText = texts[0].trim();
                cimText = texts[1].trim();
            }
            else {
                var item = args.get_item();
                if (item && item.childNodes.length == 3) {
                    cimText = item.childNodes[2].nodeValue.substring(3).trim();
                }
            }

            var elt = sender.get_element();

            if (elt != null) {
                var partnerText = texts[0].trim();
                partnerText = Utility.PartnerAutoComplete.CutForras(partnerText);
                elt.value = partnerText;

                if (this.clonePartnerTextBoxClientId.trim() != '') {
                    var clonePartnerTextBox = $get(this.clonePartnerTextBoxClientId);

                    if (clonePartnerTextBox != null) {
                        clonePartnerTextBox.value = elt.value;
                    }
                }
            }

            if (this.cimTextBoxClientId.trim() != '') {
                var cimTextBox = $get(this.cimTextBoxClientId);

                if (cimTextBox != null) {
                    cimTextBox.value = cimText;
                    var isDisabled = cimTextBox.disabled;
                    cimTextBox.disabled = false;
                    $common.tryFireEvent(cimTextBox, "change");
                    cimTextBox.disabled = isDisabled;

                    if (this.cloneCimTextBoxClientId.trim() != '') {
                        var cloneCimTextBox = $get(this.cloneCimTextBoxClientId);
                        if (cloneCimTextBox != null) {
                            cloneCimTextBox.value = cimTextBox.value;
                            //                            var isCloneDisabled = cloneCimTextBox.disabled;
                            //                            cloneCimTextBox.disabled = false;
                            //                            $common.tryFireEvent(cloneCimTextBox, "change");
                            //                            cloneCimTextBox.disabled = isCloneDisabled;
                        }
                    }
                }
            }
        }

        var hiddenField = $get(this.hiddenFieldClientId);

        if (hiddenField != null) {
            var value = args.get_value();
            var cimValue = '';

            if (value != null) {
                var values = value.split(';');

                if (values != null && values.length > 1) {
                    hiddenField.value = values[0].trim();
                    cimValue = values[1].trim();
                    this.hiddenValue = values[0].trim();
                }
                else {
                    hiddenField.value = value;
                }
                this.raiseHiddenfieldValueChanged(hiddenField.value);

                if (this.cimHiddenFieldClientId.trim() != '') {
                    var cimHiddenField = $get(this.cimHiddenFieldClientId);

                    if (cimHiddenField != null) {
                        cimHiddenField.value = cimValue;

                        if (this.cloneCimHiddenFieldClientId.trim() != '') {
                            var cloneCimHiddenField = $get(this.cloneCimHiddenFieldClientId);

                            if (cloneCimHiddenField != null) {
                                cloneCimHiddenField.value = cimHiddenField.value;
                            }
                        }
                    }
                }

                if (this.clonePartnerHiddenFieldClientId.trim() != '') {
                    var clonePartnerHiddenField = $get(this.clonePartnerHiddenFieldClientId);

                    if (clonePartnerHiddenField != null) {
                        clonePartnerHiddenField.value = hiddenField.value;
                    }
                }
            }
        }


        if (this.clonePartnerTextBoxClientId.trim() != '') {
            var clonePartnerTextBox = $get(this.clonePartnerTextBoxClientId);

            if (clonePartnerTextBox != null) {
                $common.tryFireEvent(clonePartnerTextBox, "blur");
            }
        }

        this.ItemSelecting = false;
        this.raiseItemSelected(args);
    },


    OnPopulating: function (sender, args) {
        this.SetContextKey(sender);
        this.CancelPopulating(sender, args);
    },

    CancelPopulating: function (autoCompleteExtender, args) {
        var list = autoCompleteExtender.get_completionList();
        if (list != null && list.childNodes != null && autoCompleteExtender._popupBehavior && this.lastPrefix != null) {
            if (autoCompleteExtender._popupBehavior._visible) {
                //                if(list.childNodes.length < autoCompleteExtender.get_completionSetCount() && (Utility.String.StartWith(autoCompleteExtender._currentPrefix,this.lastPrefix, true)))
                //                {
                //                    args.set_cancel(true);
                //                    autoCompleteExtender._cache[autoCompleteExtender._currentPrefix] = autoCompleteExtender._cache[this.lastPrefix];
                //                    Array.add(this.cacheArray,autoCompleteExtender._currentPrefix);
                //                    this.FindPrefix(autoCompleteExtender);
                //                    this._setScrollOnSelectedItem(autoCompleteExtender);
                //                }
                //                else
                //                {
                //                    this.lastPrefix = autoCompleteExtender._currentPrefix;
                //                }

                this.lastPrefix = autoCompleteExtender._currentPrefix;
            }
            else if (!autoCompleteExtender._popupBehavior._visible) {

                if (this._IsEmptyPreviousResult(autoCompleteExtender)) {
                    args.set_cancel(true)
                }
                else {
                    this.lastPrefix = autoCompleteExtender._currentPrefix;
                }
            }
        }
        else {
            this.lastPrefix = autoCompleteExtender._currentPrefix;
        }

    },

    _IsEmptyPreviousResult: function (autoCompleteExtender) {
        var text = autoCompleteExtender._currentPrefix;
        var cache = autoCompleteExtender._cache;
        var minLength = autoCompleteExtender.get_minimumPrefixLength();

        if (cache == null || text == null || minLength == null) {
            return false;
        }

        if (Utility.String.StartWith(text, this.lastPrefix, true)) {
            if (cache[this.lastPrefix] != undefined && cache[this.lastPrefix].length == 0) {
                cache[text] = cache[this.lastPrefix];
                return true;
            }
        }
        var currentText = Utility.String.SplitEnd(text);
        var isEmpty = false;
        var emptyText = '';
        while (currentText >= minLength) {
            if (cache[currentText] != undefined) {
                if (cache[currentText].length == 0) {
                    isEmpty = true;
                    emptyText = currentText;
                    break;
                }
                else {
                    return false;
                }
            }
            else {
                currentText = Utility.String.SplitEnd(currentText);
            }
        }

        if (isEmpty && emptyText != '' && cache[currentText] != undefined) {
            this.lastPrefix = emptyText;
            currentText = text;
            while (currentText.length > emptyText.length) {
                cache[currentText] = cache[emptyText];
                currentText = Utility.String.SplitEnd(currentText);

            }

        }
        else {
            return false;
        }
    },

    OnPopulated: function (sender, args) {
        var autoCompleteExtender = sender;
        //        if(this.IsResultInCache(autoCompleteExtender))
        //        {
        //            this.UpdateList(autoCompleteExtender);
        //            this.cancelShowing = true;
        //            this.FindPrefix(autoCompleteExtender);
        //        }
        //        else
        //        {
        //            this.FindPrefix(autoCompleteExtender);
        //        }
        this.FindPrefix(autoCompleteExtender);

    },

    UpdateList: function (autoCompleteExtender) {
        completionItems = autoCompleteExtender._cache[autoCompleteExtender._currentPrefix];
        if (completionItems && completionItems.length) {
            autoCompleteExtender._completionListElement.innerHTML = '';
            autoCompleteExtender._selectIndex = -1;
            var _firstChild = null;
            var text = null;
            var value = null;

            for (var i = 0; i < completionItems.length; i++) {
                // Create the item                
                var itemElement = null;
                if (autoCompleteExtender._completionListElementID) {
                    // the completion element has been created by the user and li won't necessarily work
                    itemElement = document.createElement('div');
                } else {
                    itemElement = document.createElement('li');
                }

                // set the first child if it is null
                if (_firstChild == null) {
                    _firstChild = itemElement;
                }

                // Get the text/value for the item
                try {
                    var pair = Sys.Serialization.JavaScriptSerializer.deserialize('(' + completionItems[i] + ')');
                    if (String.isInstanceOfType(pair)) {
                        // If the web service only returned a regular string, use it for
                        // both the text and the value
                        text = pair;
                        value = pair;
                    } else {
                        // Use the text and value pair returned from the web service
                        text = pair ? pair.First : null;
                        value = pair ? pair.Second : null;
                    }
                } catch (ex) {
                    text = completionItems[i];
                    value = completionItems[i];
                }


                // Set the text/value for the item
                itemElement.appendChild(document.createTextNode(autoCompleteExtender._getTextWithInsertedWord(text)));
                itemElement._value = value;
                itemElement.__item = '';

                if (autoCompleteExtender._completionListItemCssClass) {
                    Sys.UI.DomElement.addCssClass(itemElement, autoCompleteExtender._completionListItemCssClass);
                } else {
                    var itemElementStyle = itemElement.style;
                    itemElementStyle.padding = '0px';
                    itemElementStyle.textAlign = 'left';
                    itemElementStyle.textOverflow = 'ellipsis';
                    // workaround for safari since normal colors do not
                    // show well there.
                    if (Sys.Browser.agent === Sys.Browser.Safari) {
                        itemElementStyle.backgroundColor = 'white';
                        itemElementStyle.color = 'black';
                    } else {
                        itemElementStyle.backgroundColor = autoCompleteExtender._textBackground;
                        itemElementStyle.color = autoCompleteExtender._textColor;
                    }
                }
                autoCompleteExtender._completionListElement.appendChild(itemElement);
            }
            var elementBounds = $common.getBounds(autoCompleteExtender.get_element());
            autoCompleteExtender._completionListElement.style.width = Math.max(1, elementBounds.width - 2) + 'px';
            autoCompleteExtender._completionListElement.scrollTop = 0;
            var eventArgs = new Sys.CancelEventArgs();
            autoCompleteExtender.raiseShowing(eventArgs);
            autoCompleteExtender.showPopup();
        }
    },

    IsResultInCache: function (autoCompleteExtender) {
        var text = autoCompleteExtender._currentPrefix;
        var cache = autoCompleteExtender._cache;
        var minLength = autoCompleteExtender.get_minimumPrefixLength();
        if (Array.indexOf(this.cacheArray, text) > -1) {
            return false;
        }
        else {
            var currentText = Utility.String.SplitEnd(text);
            var prevText = text;
            var isInCache = false;
            var cacheLength = 0;
            if (cache[text] != undefined)
                cacheLength = cache[text].length;
            while (cache[currentText] != undefined && currentText.length >= minLength && cache[currentText].length < autoCompleteExtender.get_completionSetCount()
            ) {
                isInCache = true;
                prevText = currentText;
                currentText = Utility.String.SplitEnd(currentText);
            }
            if (isInCache) {
                //jó leállás
                if ((currentText.length == minLength - 1 || (cache[currentText] != undefined && cache[currentText].length == autoCompleteExtender.get_completionSetCount()))
                    && cache[prevText].length >= cacheLength) {
                    Array.add(this.cacheArray, prevText);
                    currentText = text;
                    while (currentText != prevText && currentText.length > minLength) {
                        cache[currentText] = cache[prevText];
                        Array.add(this.cacheArray, currentText);
                        currentText = currentText.substring(0, currentText.length - 1);
                    }
                    this.lastPrefix = prevText;
                    return true;
                }
            }

            return isInCache;
        }
    },



    FindPrefix: function (autoCompleteExtender) {
        var list = autoCompleteExtender.get_completionList();
        var text = autoCompleteExtender._currentPrefix;
        if (list != null && list.childNodes != null && list.childNodes.length > 0) {
            var selectedIndex = -1;
            for (var i = 0; i < list.childNodes.length; i++) {
                var listElement = list.childNodes[i];
                if (Utility.String.StartWith(listElement.firstChild.nodeValue, text)) {
                    selectedIndex = i;
                    break;
                }
            }

            autoCompleteExtender._selectIndex = selectedIndex;
            this.HighLightItem(autoCompleteExtender, selectedIndex);

        }
    },

    _setScrollOnSelectedItem: function (autoCompleteExtender) {
        var list = autoCompleteExtender.get_completionList();
        var selectedIndex = autoCompleteExtender._selectIndex;
        if (list && selectedIndex > -1) {
            var listHeight = list.offsetHeight;
            var rowHeight = list.childNodes[selectedIndex].offsetHeight;
            var scrollPos = list.scrollTop;
            var rowPos = 0;
            for (var i = 0; i < selectedIndex; i++) {
                rowPos += list.childNodes[i].offsetHeight;
            }
            var pos = rowPos - listHeight;

            list.scrollTop = pos + (listHeight / 2) + (rowHeight / 2);
        }
    },


    HighLightItem: function (autoCompleteExtender, selectedIndex) {
        var list = autoCompleteExtender.get_completionList();
        if (list != null && list.childNodes != null && list.childNodes.length > 0) {
            if (selectedIndex > -1 && selectedIndex < list.childNodes.length) {
                autoCompleteExtender._highlightItem(list.childNodes[selectedIndex]);
            }
            else {
                var children = list.childNodes;

                // Unselect any previously highlighted item
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    if (child._highlighted) {
                        if (autoCompleteExtender._completionListItemCssClass) {
                            Sys.UI.DomElement.removeCssClass(child, autoCompleteExtender._highlightedItemCssClass);
                            Sys.UI.DomElement.addCssClass(child, autoCompleteExtender._completionListItemCssClass);
                        } else {
                            if (Sys.Browser.agent === Sys.Browser.Safari) {
                                child.style.backgroundColor = 'white';
                                child.style.color = 'black';
                            } else {
                                child.style.backgroundColor = autoCompleteExtender._textBackground;
                                child.style.color = autoCompleteExtender._textColor;
                            }
                        }
                        autoCompleteExtender.raiseItemOut(new AjaxControlToolkit.AutoCompleteItemEventArgs(child, child.firstChild.nodeValue, child._value));
                    }
                }
            }
        }
    },

    OnItemOver: function (sender, args) {
        var item = args.get_item();
    },

    OnItemOut: function (sender, args) {
        var item = args.get_item();
    },

    OnShowing: function (sender, args) {
        if (this.cancelShowing) {
            args.set_cancel(true);
            this.cancelShowing = false;
        }
        else {
            // előbb kell futnia, mint a SetColor-nak, mert az esetleg <BR /> elemeket generál,
            // és elvész a cím sora
            if (this.createToolTipOnItems == '1') {
                this.SetToolTipOnItems(sender);
            }
            this._listElementMaxWidth = -1;
            this.SetColor(sender);
            this._makeListScrollable(sender);
        }
    },

    _makeListScrollable: function (autoCompleteExtender) {
        var element = autoCompleteExtender.get_element();
        var flyout = autoCompleteExtender._completionListElement;
        var elemBounds = $common.getBounds(element);
        var listBounds = $common.getBounds(flyout);
        var listHeight = elemBounds.y + elemBounds.height + listBounds.height;
        if (flyout.style.height != '') {
            listHeight = elemBounds.y + elemBounds.height + this._getListRealHeight(flyout);
        }
        var bodyHeight = this._getWindowScrollSize().height;
        var windowSize = this._getWindowSize();
        var scrollPosition = this._getWindowScrollPosition();

        this._setScrollParameters();

        if (this._listElementMaxWidth > -1) {
            flyout.style.width = Math.min(this._listElementMaxWidth, this._listMaxWidth) + 'px';
        }

        if (listHeight > bodyHeight) {
            flyout.style.overflowY = 'scroll';
            flyout.style.height = this._listMaxHeight;

            var listWidth = parseInt(flyout.style.width);
            if (!isNaN(listWidth)) flyout.style.width = Math.min(listWidth + this._scrollerWidth, this._listMaxWidth) + "px";

            if (this._windowMaxScrollPosition > 0)
                this._setWindowScrollPositionY(this._windowMaxScrollPosition);
        }
        else {
            if (flyout.style.overflowY != '' || flyout.style.height != '') {
                if (listHeight < bodyHeight) {
                    flyout.style.overflowY = '';
                    flyout.style.height = '';
                }
            }

            if (listHeight > windowSize.height) {
                if (this._windowMaxScrollPosition > 0)
                    this._setWindowScrollPositionY(this._windowMaxScrollPosition);
            }
        }

        var listWidth = $common.getBounds(flyout).width;
        if (listWidth > scrollPosition.x + windowSize.width - elemBounds.x) {
            this._setWindowScrollPositionX(elemBounds.x);
        }

        this._setScrollOnSelectedItem(autoCompleteExtender);

    },

    _getListRealHeight: function (list) {
        var height = 0;
        if (list != null && list.childNodes != null && list.childNodes.length > 0) {
            for (var i = 0; i < list.childNodes.length; i++) {
                var listElement = list.childNodes[i];
                if (listElement != null) {
                    height += listElement.offsetHeight;
                }
            }
        }

        return height;
    },

    _getWindowScrollPosition: function () {
        var scrOfX = 0;
        var scrOfY = 0;
        if (typeof (window.pageYOffset) == 'number') {
            //Netscape compliant
            scrOfY = window.pageYOffset;
            scrOfX = window.pageXOffset;
        } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
            //DOM compliant
            scrOfY = document.body.scrollTop;
            scrOfX = document.body.scrollLeft;
        } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
            //IE6 standards compliant mode
            scrOfY = document.documentElement.scrollTop;
            scrOfX = document.documentElement.scrollLeft;
        }

        return { x: scrOfX, y: scrOfY };
    },

    _setWindowScrollPosition: function (posX, posY) {
        window.scrollTo(posX, posY);
    },

    _setWindowScrollPositionY: function (posY) {
        var posX = this._getWindowScrollPosition().x;
        window.scrollTo(posX, posY);
    },

    _setWindowScrollPositionX: function (posX) {
        var posY = this._getWindowScrollPosition().y;
        window.scrollTo(posX, posY);
    },

    _getWindowSize: function () {
        var height = 0;
        var width = 0;
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE
            width = window.innerWidth;
            height = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode'
            width = document.documentElement.clientWidth;
            height = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible
            width = document.body.clientWidth;
            height = document.body.clientHeight;
        }
        return { height: height, width: width };
    },

    _getWindowScrollSize: function () {
        var height = 0;
        var width = 0;
        if (window.innerHeight && window.scrollMaxY) {
            width = document.body.scrollWidth;
            height = window.innerHeight + window.scrollMaxY;
        } else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac
            width = document.body.scrollWidth;
            height = document.body.scrollHeight;
        } else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
            width = document.body.offsetWidth;
            height = document.body.offsetHeight;
        }

        return { height: height, width: width };
    },

    _isWindowScrollable: function () {
        var rect = this._getWindowSize();
        var windowHeight = rect.height;
        var windowWidth = rect.width;
        var rect2 = this._getWindowScrollSize();
        var scrollHeight = rect2.height;
        var scrollWidth = rect2.width;

        var isX = (scrollWidth > windowWidth) ? true : false;
        var isY = (scrollHeight > windowHeight) ? true : false;

        return { x: isX, y: isY };

    },

    _setScrollParameters: function () {
        if (this._listMaxHeight == null || this._windowMaxScrollPosition == null || this._listMaxWidth == null) {
            var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);
            if (autoCompleteExtender) {
                //scrollozási a paraméterek meghatározása
                var element = autoCompleteExtender.get_element();
                var flyout = autoCompleteExtender._completionListElement;
                var elemBounds = $common.getBounds(element);
                var listBounds = $common.getBounds(flyout);
                var scrollSize = this._getWindowScrollSize();
                var windowSize = this._getWindowSize();

                if (!this._listMaxHeight || this._listMaxHeight == 0) {
                    this._listMaxHeight = scrollSize.height - (elemBounds.y + elemBounds.height);
                    if (this._listMaxHeight < 0) this._listMaxHeight = 0;
                }

                if (!this._windowMaxScrollPosition) {
                    if (this._isWindowScrollable().y) {
                        this._windowMaxScrollPosition = scrollSize.height - windowSize.height;
                        if (this._windowMaxScrollPosition < 0) this._windowMaxScrollPosition = 0;
                        if (this._windowMaxScrollPosition > listBounds.y) this._windowMaxScrollPosition = listBounds.y;
                    }
                    else {
                        this._windowMaxScrollPosition = 0;
                    }
                }

                if (!this._listMaxWidth) {
                    if (this._fullWidthMode) {
                        //max szélesség az ablak szélességen scrollozási méret megnő
                        this._listMaxWidth = windowSize.width;
                    }
                    else {
                        //max szélesség az ablak scrollozható mérete, csrollozási méret állandó
                        this._listMaxWidth = Math.min(scrollSize.width - 2 - elemBounds.x, windowSize.width);
                    }

                    if (!this._isWindowScrollable().y) {
                        this._listMaxWidth = this._listMaxWidth - this._scrollerWidth;
                    }
                    if (this._listMaxWidth < 0) this._listMaxWidth = 0;
                }
            }

        }
    },

    SetColor: function (autoCompleteExtender) {
        var list = autoCompleteExtender.get_completionList();
        var prevId = null;
        var prevText = null;
        var alternateRow = true;
        if (list != null && list.childNodes != null && list.childNodes.length > 0) {
            for (var i = 0; i < list.childNodes.length; i++) {
                var listElement = list.childNodes[i];
                if (listElement != null) {
                    var id = listElement._value.split(';')[0].trim();

                    if (id != null && prevId != null) {
                        if (id != prevId) {
                            alternateRow = !alternateRow;
                        }
                        else {
                            //üres id, küldeményekből jött
                            if (id == '') {
                                var text = (listElement && listElement.firstChild) ? listElement.firstChild.nodeValue : null;
                                if (text != null && prevText != null) {
                                    text = text.split(this.delimeter)[0].trim();
                                    if (text != prevText) {
                                        alternateRow = !alternateRow;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        alternateRow = !alternateRow;
                    }

                    if (id != null)
                        prevId = id;

                    //üres id, küldeményekből jött    
                    if (id == '') {
                        var text = (listElement && listElement.firstChild) ? listElement.firstChild.nodeValue : null;
                        if (text != null) {
                            prevText = text.split(this.delimeter)[0].trim();
                        }
                    }

                    if (alternateRow) {
                        if (autoCompleteExtender._completionListItemCssClass != null) {
                            Sys.UI.DomElement.addCssClass(listElement, autoCompleteExtender._completionListItemCssClass);
                        }
                        Sys.UI.DomElement.addCssClass(listElement, this.alternateRowStyle);
                    }

                    this.SetListElementStyle(autoCompleteExtender, list, listElement);
                }
            }
        }
    },

    SetToolTipOnItems: function (autoCompleteExtender) {
        var list = autoCompleteExtender.get_completionList();
        if (list != null && list.childNodes != null && list.childNodes.length > 0) {
            for (var i = 0; i < list.childNodes.length; i++) {
                var listElement = list.childNodes[i];
                if (listElement != null) {
                    var text = (listElement && listElement.firstChild) ? listElement.firstChild.nodeValue : '';
                    text = text.trim();
                    // line break
                    text = text.replace(this.delimeter, String.fromCharCode(13));
                    listElement.title = text;
                }
            }
        }
    },

    OnShown: function (sender, args) {
        sender._popupBehavior.addBackgroundIFrame();
    },

    SetListElementStyle: function (autoCompleteExtender, list, listElement) {
        var listWidth = parseInt(list.style.width);
        list.style.width = '';

        if (list.style.display == 'none') {
            autoCompleteExtender._popupBehavior.show();
        }

        listElement.style.whiteSpace = 'nowrap';

        var elementBounds = $common.getBounds(listElement);

        if (elementBounds.width > listWidth) {
            if (elementBounds.width > this._listElementMaxWidth)
                this._listElementMaxWidth = elementBounds.width;
        };

        listElement.style.overflow = 'hidden';
        listElement.style.textOverflow = 'ellipsis';

        list.style.width = listWidth + 'px';
    },

    FixHandleScroll: function (element, index) {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);
        if (autoCompleteExtender) {
            var flyout = autoCompleteExtender._completionListElement;
            var elemBounds = $common.getBounds(element);
            var numItems = autoCompleteExtender._completionListElement.childNodes.length;
            var rowPos = 0;
            var elementHeight = element.offsetHeight;
            for (var i = 0; i <= index; i++) {
                rowPos += flyout.childNodes[i].offsetHeight;
            }

            if (rowPos - flyout.scrollTop > flyout.clientHeight) {
                flyout.scrollTop = rowPos - flyout.clientHeight;
            }

            if (rowPos - elementHeight - flyout.scrollTop < 0) {
                flyout.scrollTop = rowPos - elementHeight;
            }

        }
    },

    OnTextBoxFocus: function (ev) {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);
        if (autoCompleteExtender) {
            autoCompleteExtender._textBoxHasFocus = true;
            autoCompleteExtender._timer.set_enabled(true);
        }
    },

    SetContextKey: function (autoCompleteExtender) {
        if (this.checkBoxClientId.trim() != '') {
            var checkBox = $get(this.checkBoxClientId);
            if (checkBox != null) {
                if (this.IsCheckBoxChanged(checkBox)) {
                    var context = autoCompleteExtender.get_contextKey();
                    var index = context.indexOf(';');
                    var felhId;
                    var more;
                    if (index > -1) {
                        felhId = context.substring(0, index);
                        more = context.substring(index);
                    }
                    else
                        felhId = context;

                    if (checkBox.checked)
                        autoCompleteExtender.set_contextKey(felhId + ';' + Utility.Constants.Database.yes  + more);
                    else
                        autoCompleteExtender.set_contextKey(felhId + ';' + Utility.Constants.Database.no  + more);
                }
            }
        }
    },

    IsCheckBoxChanged: function (checkBox) {
        if (this.checkBoxChanged) {
            this.checkBoxChanged = false;
            return true;
        }
        else {
            if (checkBox.checked == this.checkBoxState) {
                return false;
            }
            else {
                this.checkBoxState = checkBox.checked;
                return true;
            }
        }
    },

    OnKeyDown: function (ev) {
        var k = ev.keyCode ? ev.keyCode : ev.rawEvent.keyCode;
        if (k === Sys.UI.Key.enter) {
            var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);
            if (autoCompleteExtender != null) {
                if (autoCompleteExtender._selectIndex == -1) {
                    ev.preventDefault();
                }
            }
        }
    },

    OnListKeyDown: function (ev) {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);
        if (autoCompleteExtender != null && autoCompleteExtender._flyoutHasFocus && autoCompleteExtender._popupBehavior.get_visible()) {
            var elt = autoCompleteExtender.get_element();
            if (elt && ev.target !== elt) {
                autoCompleteExtender._keyDownHandler(ev);
            }
        }
    },

    OnCheckBoxChanged: function (args) {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);
        if (autoCompleteExtender != null) {
            //cache törlése
            autoCompleteExtender._cache = {};
        }

        if (this.checkBoxClientId.trim() != '') {
            var checkBox = $get(this.checkBoxClientId);
            if (checkBox != null) {
                var parentSpan = checkBox.parentNode;
                if (parentSpan != null) {
                    if (checkBox.checked)
                        parentSpan.title = this.checkBoxCheckedText;
                    else
                        parentSpan.title = this.checkBoxUnCheckedText;

                }
            }
        }


    },

    OnCimTextBoxChanged: function (args) {

        var bCloneCim = true;

        // ha van klónozott partner hiddenfield és annak tartalma nem azonos az eredeti partner hiddenfieldével,
        // nem másoljuk át a címet (hogy ne írjuk felül a másik partner már kiválasztott címét)
        if (this.clonePartnerHiddenFieldClientId.trim() != '') {
            var clonePartnerHiddenField = $get(this.clonePartnerHiddenFieldClientId);

            if (clonePartnerHiddenField && this.hiddenFieldClientId.trim() != '') {
                var hiddenField = $get(this.hiddenFieldClientId);

                if (hiddenField && hiddenField.value != clonePartnerHiddenField.value) {
                    bCloneCim = false;
                }
            }
        }

        if (bCloneCim == true) {
            if (this.cimHiddenFieldClientId.trim() != '') {
                var cimHiddenField = $get(this.cimHiddenFieldClientId);

                if (cimHiddenField != null) {
                    if (this.cloneCimHiddenFieldClientId.trim() != '') {
                        var cloneCimHiddenField = $get(this.cloneCimHiddenFieldClientId);

                        if (cloneCimHiddenField != null) {
                            cloneCimHiddenField.value = cimHiddenField.value;
                        }
                    }
                }
            }

            if (this.cimTextBoxClientId.trim() != '') {
                var cimTextBox = $get(this.cimTextBoxClientId);

                if (cimTextBox != null) {
                    if (this.cloneCimTextBoxClientId.trim() != '') {
                        var cloneCimTextBox = $get(this.cloneCimTextBoxClientId);
                        if (cloneCimTextBox != null) {
                            cloneCimTextBox.value = cimTextBox.value;
                            //                        var isCloneDisabled = cloneCimTextBox.disabled;
                            //                        cloneCimTextBox.disabled = false;
                            //                        $common.tryFireEvent(cloneCimTextBox, "change");
                            //                        cloneCimTextBox.disabled = isCloneDisabled;
                        }
                    }
                }
            }
        }
    },

    OnBlur: function (args) {
        if (!this.customTextEnabled) {
            var hiddenField = $get(this.hiddenFieldClientId);

            if (hiddenField != null) {
                var value = this.hiddenValue;
                if (hiddenField.value == '' && value != '')
                    hiddenField.value = value;

                if (hiddenField.value == '') {
                    var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

                    if (autoCompleteExtender != null) {
                        autoCompleteExtender.get_element().value = '';
                        autoCompleteExtender._currentPrefix = '';
                    }
                }
                this.raiseHiddenfieldValueChanged(hiddenField.value);
            }
        }
    },


    OnTextChanged: function (args) {
        var hiddenField = $get(this.hiddenFieldClientId);

        if (hiddenField != null) {
            this.raiseHiddenfieldValueChanged(hiddenField.value);

            if (this.clonePartnerHiddenFieldClientId.trim() != '') {
                var clonePartnerHiddenField = $get(this.clonePartnerHiddenFieldClientId);

                if (clonePartnerHiddenField != null) {
                    clonePartnerHiddenField.value = hiddenField.value;
                }
            }
        }

        if (this.clonePartnerTextBoxClientId.trim() != '') {
            var elt = this.get_element();
            if (elt != null) {
                var clonePartnerTextBox = $get(this.clonePartnerTextBoxClientId);

                if (clonePartnerTextBox != null) {
                    clonePartnerTextBox.value = elt.value;

                    $common.tryFireEvent(clonePartnerTextBox, "blur");
                }
            }
        }
    },

    OnHiddenTextChanged: function (args) {
        var hiddenField = $get(this.hiddenFieldClientId);

        if (hiddenField != null) {
            this.hiddenValue = hiddenField.value;
            this.raiseHiddenfieldValueChanged(hiddenField.value);
        }
    },

    //getter, setter methods
    get_AutoCompleteExtenderClientId: function () {
        return this.autoCompleteExtenderClientId;
    },

    set_AutoCompleteExtenderClientId: function (value) {
        if (this.autoCompleteExtenderClientId != value) {
            this.autoCompleteExtenderClientId = value;
        }
    },

    get_HiddenFieldClientId: function () {
        return this.hiddenFieldClientId;
    },

    set_HiddenFieldClientId: function (value) {
        if (this.hiddenFieldClientId != value) {
            this.hiddenFieldClientId = value;
        }
    },

    get_CimTextBoxClientId: function () {
        return this.cimTextBoxClientId;
    },

    set_CimTextBoxClientId: function (value) {
        if (this.cimTextBoxClientId != value) {
            this.cimTextBoxClientId = value;
        }
    },

    get_CimHiddenFieldClientId: function () {
        return this.cimHiddenFieldClientId;
    },

    set_CimHiddenFieldClientId: function (value) {
        if (this.cimHiddenFieldClientId != value) {
            this.cimHiddenFieldClientId = value;
        }
    },

    get_ClonePartnerTextBoxClientId: function () {
        return this.clonePartnerTextBoxClientId;
    },

    set_ClonePartnerTextBoxClientId: function (value) {
        if (this.clonePartnerTextBoxClientId != value) {
            this.clonePartnerTextBoxClientId = value;
        }
    },

    get_ClonePartnerHiddenFieldClientId: function () {
        return this.clonePartnerHiddenFieldClientId;
    },

    set_ClonePartnerHiddenFieldClientId: function (value) {
        if (this.clonePartnerHiddenFieldClientId != value) {
            this.clonePartnerHiddenFieldClientId = value;
        }
    },

    get_CloneCimTextBoxClientId: function () {
        return this.cloneCimTextBoxClientId;
    },

    set_CloneCimTextBoxClientId: function (value) {
        if (this.cloneCimTextBoxClientId != value) {
            this.cloneCimTextBoxClientId = value;
        }
    },

    get_CloneCimHiddenFieldClientId: function () {
        return this.cloneCimHiddenFieldClientId;
    },

    set_CloneCimHiddenFieldClientId: function (value) {
        if (this.cloneCimHiddenFieldClientId != value) {
            this.cloneCimHiddenFieldClientId = value;
        }
    },

    get_Delimeter: function () {
        return this.delimeter;
    },

    set_Delimeter: function (value) {
        if (this.delimeter != value) {
            this.delimeter = value;
        }
    },

    get_CheckBoxClientId: function () {
        return this.checkBoxClientId;
    },

    set_CheckBoxClientId: function (value) {
        if (this.checkBoxClientId != value) {
            this.checkBoxClientId = value;
        }
    },

    get_CreateToolTipOnItems: function () {
        return this.createToolTipOnItems;
    },

    set_CreateToolTipOnItems: function (value) {
        if (this.createToolTipOnItems != value) {
            this.createToolTipOnItems = value;
        }
    },

    get_CustomTextEnabled: function () {
        return this.customTextEnabled;
    },

    set_CustomTextEnabled: function (value) {
        if (this.customTextEnabled != value) {
            this.customTextEnabled = value;
        }
    },

    get_HiddenValue: function () {
        return this.hiddenValue;
    },

    set_HiddenValue: function (value) {
        this.hiddenValue = value;;
    },

    //
    // Events
    //

    add_hiddenfieldValueChanged: function (handler) {

        this.get_events().addHandler('hiddenfieldValueChanged', handler);
    },

    remove_hiddenfieldValueChanged: function (handler) {
        this.get_events().removeHandler('hiddenfieldValueChanged', handler);
    },

    raiseHiddenfieldValueChanged: function (eventArgs) {

        var handler = this.get_events().getHandler('hiddenfieldValueChanged');
        if (handler) {
            handler(this, eventArgs);
        }
    },

    add_itemSelected: function (handler) {

        this.get_events().addHandler('itemSelected', handler);
    },

    remove_itemSelected: function (handler) {
        this.get_events().removeHandler('itemSelected', handler);
    },

    raiseItemSelected: function (eventArgs) {

        var handler = this.get_events().getHandler('itemSelected');
        if (handler) {
            handler(this, eventArgs);
        }
    }
}


Utility.PartnerAutoComplete.registerClass('Utility.PartnerAutoComplete', Sys.UI.Behavior);

Utility.CsoportAutoComplete = function (element) {
    Utility.CsoportAutoComplete.initializeBase(this, [element]);

    //Properties
    this.autoCompleteExtenderClientId = '';
    this.hiddenFieldClientId = '';
    this.customTextEnabled = false;
    this.hiddenValue = '';
    this.parentClonetTextboxClientId = '';
    this.parentCloneHiddenFieldClientId = '';

    //Variables
    this.itemSelectedHandler = null;
    this.populatingHandler = null;
    this.showingHandler = null;
    this.blurHandler = null;
    this.applicationLoadHandler = null;
    this.hiddenChangedHandler = null;
    this.alternateRowStyle = 'GridViewAlternateRowStyle';
    this.parentTextboxChangedHandler = null;
    this.parentHiddenFieldChangedHandler = null;
    this.parentClonetTextbox = null;
    this.parentCloneHiddenField = null;
}

Utility.CsoportAutoComplete.prototype = {
    initialize: function () {
        Utility.CsoportAutoComplete.callBaseMethod(this, 'initialize');

        this.itemSelectedHandler = Function.createDelegate(this, this.OnItemSelected);
        this.populatingHandler = Function.createDelegate(this, this.OnPopulating);
        this.showingHandler = Function.createDelegate(this, this.OnShowing);
        this.blurHandler = Function.createDelegate(this, this.OnBlur);
        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);
        this.hiddenChangedHandler = Function.createDelegate(this, this.OnHiddenTextChanged);
        this.parentTextboxChangedHandler = Function.createDelegate(this, this.OnParentTextboxChanged);
        this.parentHiddenFieldChangedHandler = Function.createDelegate(this, this.OnParentHiddenFieldChanged);

        if (this.parentClonetTextboxClientId) {
            this.parentClonetTextbox = $get(this.parentClonetTextboxClientId);
        }

        if (this.parentCloneHiddenFieldClientId) {
            this.parentCloneHiddenField = $get(this.parentCloneHiddenFieldClientId);
        }

        Sys.Application.add_load(this.applicationLoadHandler);
    },
    dispose: function () {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (this.itemSelectedHandler != null) {

            if (autoCompleteExtender != null) {
                autoCompleteExtender.remove_itemSelected(this.itemSelectedHandler);
            }

            this.itemSelectedHandler = null;
        }

        if (this.populatingHandler != null) {

            if (autoCompleteExtender != null) {
                autoCompleteExtender.remove_populating(this.populatingHandler);
            }

            this.populatingHandler = null;
        }

        if (this.showingHandler != null) {
            if (autoCompleteExtender != null) {
                autoCompleteExtender.remove_showing(this.showingHandler);
            }

            this.showingHandler = null;
        }

        if (this.blurHandler != null) {
            var elt = this.get_element();
            if (elt) {
                $removeHandler(elt, 'blur', this.blurHandler);
            }
            this.blurHandler = null;
        }

        if (this.hiddenFieldClientId.trim() != '') {
            var hidden = $get(this.hiddenFieldClientId);
            if (hidden != null) {
                if (this.hiddenChangedHandler != null) {
                    $removeHandler(hidden, 'change', this.hiddenChangedHandler);
                }
            }
        }

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
            this.applicationLoadHandler = null;
        }

        if (this.parentClonetTextbox && this.parentTextboxChangedHandler) {
            $removeHandler(this.parentClonetTextbox, 'change', this.parentTextboxChangedHandler);
            this.parentTextboxChangedHandler = null;
        }

        if (this.parentCloneHiddenField && this.parentHiddenFieldChangedHandler) {
            $removeHandler(this.parentCloneHiddenField, 'change', this.parentHiddenFieldChangedHandler);
            this.parentHiddenFieldChangedHandler = null;
        }

        Utility.CsoportAutoComplete.callBaseMethod(this, 'dispose');
    },

    OnApplicationLoad: function () {
        var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

        if (autoCompleteExtender != null) {
            autoCompleteExtender.add_itemSelected(this.itemSelectedHandler);
            autoCompleteExtender.add_populating(this.populatingHandler);
            autoCompleteExtender.add_showing(this.showingHandler);
        }

        var elt = this.get_element();
        if (elt) {
            $addHandler(elt, 'blur', this.blurHandler);
        }

        if (this.hiddenFieldClientId.trim() != '') {
            var hidden = $get(this.hiddenFieldClientId);
            if (hidden != null) {
                if (this.hiddenChangedHandler != null) {
                    $addHandler(hidden, 'change', this.hiddenChangedHandler);
                }
            }
        }

        if (this.parentClonetTextbox && this.parentTextboxChangedHandler) {
            $addHandler(this.parentClonetTextbox, 'change', this.parentTextboxChangedHandler);
        }

        if (this.parentCloneHiddenField && this.parentHiddenFieldChangedHandler) {
            $addHandler(this.parentCloneHiddenField, 'change', this.parentHiddenFieldChangedHandler);
        }
    },

    OnItemSelected: function (sender, args) {
        var hiddenField = $get(this.hiddenFieldClientId);

        if (hiddenField != null) {
            var value = args.get_value();

            if (value != null) {
                hiddenField.value = value.trim();
                this.hiddenValue = value.trim();
                $common.tryFireEvent(hiddenField, "change");
            }
        }

    },

    OnPopulating: function (sender, args) {
    },

    OnShowing: function (sender, args) {
        this.SetColor(sender);
    },

    SetColor: function (autoCompleteExtender) {
        var list = autoCompleteExtender.get_completionList();
        var prevId = null;
        var alternateRow = true;
        if (list != null && list.childNodes != null && list.childNodes.length > 0) {
            for (var i = 0; i < list.childNodes.length; i++) {
                var listElement = list.childNodes[i];
                if (listElement != null) {
                    var id = listElement._value.split(';')[0].trim();

                    if (id != null && prevId != null) {
                        if (id != prevId) {
                            alternateRow = !alternateRow;
                        }
                    }
                    else {
                        alternateRow = !alternateRow;
                    }

                    if (id != null)
                        prevId = id;

                    if (alternateRow) {
                        if (autoCompleteExtender._completionListItemCssClass != null) {
                            Sys.UI.DomElement.addCssClass(listElement, autoCompleteExtender._completionListItemCssClass);
                        }
                        Sys.UI.DomElement.addCssClass(listElement, this.alternateRowStyle);
                    }

                }
            }
        }
    },

    OnBlur: function (args) {
        if (!this.customTextEnabled) {
            var hiddenField = $get(this.hiddenFieldClientId);

            if (hiddenField != null) {
                var value = this.hiddenValue;
                if (hiddenField.value == '' && value != '') hiddenField.value = value;
                if (hiddenField.value == '') {
                    var autoCompleteExtender = $find(this.autoCompleteExtenderClientId);

                    if (autoCompleteExtender != null) {
                        autoCompleteExtender.get_element().value = '';
                        autoCompleteExtender._currentPrefix = '';
                    }
                }
            }
        }
    },

    OnHiddenTextChanged: function (args) {
        var hiddenField = $get(this.hiddenFieldClientId);

        if (hiddenField != null) {
            this.hiddenValue = hiddenField.value;
        }

    },

    OnParentTextboxChanged: function (args) {
        this.CloneParent();
    },

    OnParentHiddenFieldChanged: function (args) {
        this.CloneParent();
    },

    CloneParent: function () {
        var hiddenField = $get(this.hiddenFieldClientId);
        var elt = this.get_element();
        if (hiddenField && elt && this.parentClonetTextbox && this.parentCloneHiddenField) {
            hiddenField.value = this.parentCloneHiddenField.value;
            elt.value = this.parentClonetTextbox.value;
        }
    },

    //getter, setter methods
    get_AutoCompleteExtenderClientId: function () {
        return this.autoCompleteExtenderClientId;
    },

    set_AutoCompleteExtenderClientId: function (value) {
        if (this.autoCompleteExtenderClientId != value) {
            this.autoCompleteExtenderClientId = value;
        }
    },

    get_HiddenFieldClientId: function () {
        return this.hiddenFieldClientId;
    },

    set_HiddenFieldClientId: function (value) {
        if (this.hiddenFieldClientId != value) {
            this.hiddenFieldClientId = value;
        }
    },

    get_CustomTextEnabled: function () {
        return this.customTextEnabled;
    },

    set_CustomTextEnabled: function (value) {
        if (this.customTextEnabled != value) {
            this.customTextEnabled = value;
        }
    },

    get_HiddenValue: function () {
        return this.hiddenValue;
    },

    set_HiddenValue: function (value) {
        this.hiddenValue = value;;
    },

    get_ParentClonetTextboxClientId: function () {
        return this.parentClonetTextboxClientId;
    },

    set_ParentClonetTextboxClientId: function (value) {
        if (this.parentClonetTextboxClientId != value) {
            this.parentClonetTextboxClientId = value;
        }
    },

    get_ParentCloneHiddenFieldClientId: function () {
        return this.parentCloneHiddenFieldClientId;
    },

    set_ParentCloneHiddenFieldClientId: function (value) {
        if (this.parentCloneHiddenFieldClientId != value) {
            this.parentCloneHiddenFieldClientId = value;
        }
    }
}


Utility.CsoportAutoComplete.registerClass('Utility.CsoportAutoComplete', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded(); 