﻿// JScript File

// ha a forrás CheckBox checked, a cél CheckBoxok is azok lesznek
function check(srcId,targetIds)
{
    var srcCheckBox = document.getElementById(srcId);
    if (!srcCheckBox) return;
    var i = 0;
    for (i=0; i<targetIds.length; i++)
    {
        var targetCheckBox = document.getElementById(targetIds[i]);
        if (!targetCheckBox) continue;
        if (srcCheckBox.checked == true)
        {
            targetCheckBox.checked = true;    
        }    
    }   
}

// ha a forrás CheckBox unchecked, a cél CheckBoxok is azok lesznek
function uncheck(srcId,targetIds)
{
    var srcCheckBox = document.getElementById(srcId);
    if (!srcCheckBox) return;
    var i = 0;
    for (i=0; i<targetIds.length; i++)
    {
        var targetCheckBox = document.getElementById(targetIds[i]);
        if (!targetCheckBox) continue;
        if (srcCheckBox.checked == false)
        {
            targetCheckBox.checked = false;    
        }    
    }   
}

// ha a mode==0: checkbox checked állapotban a célkomponensek enabled-be kerülnek,
//    unchecked állapotban disabled-be, mode==1 esetén fordítva
function enableOrDisableComponents(srcId,mode,targetIds)
{
    var srcCheckBox = document.getElementById(srcId);
    if (!srcCheckBox) return;
    var i = 0;
    for (i=0; i<targetIds.length; i++)
    {
        var targetComponent = document.getElementById(targetIds[i]);
        if (!targetComponent) continue;
        if ((srcCheckBox.checked == true && mode==0)
            || (srcCheckBox.checked == false && mode==1))
        {
            // komponens engedélyezés:
            targetComponent.disabled = false;    
        }    
        else
        {
            // komponens letiltás:
            targetComponent.disabled = true;    
        }
    }
}


// ha a mode==0: minden checkbox homogen állapotban van, a célkomponens enabled-be kerül,
//    inhomogén állapotban disabled-be, mode==1 esetén fordítva
// ifAllChecked == true: homogen állapot, ha mind checked
// ifAllUnChecked == true: homogen állapot, ha mind unchecked
function enableOrDisableComponentsByAllCheckBoxes(srcIds, mode, ifAllChecked, ifAllUnChecked, targetIds)
{
    var isAllChecked = true;
    var isAllUnChecked = true;

    for (var i=0; i<srcIds.length; i++)
    {
        var srcCheckBox = document.getElementById(srcIds[i]);
        if (!srcCheckBox) continue;
        
        if (srcCheckBox.checked == true)
        {
            isAllUnChecked = false;
        }
        else
        {
            isAllChecked = false;
        }
     }
     
     // ha mindkett? true, akkor nem találtunk checkboxot
     if (!(isAllChecked == true && isAllUnChecked == true))
     {
         var isDisabled = false;
         
         var isHomogen = ((ifAllChecked == true && isAllChecked == true) || (ifAllUnChecked == true && isAllUnChecked == true)) ? true : false;
         
         if ((isHomogen == true &&  mode==0)
                 || (isHomogen == false &&  mode==1))
        {
            // komponens engedélyezés:
            isDisabled = false;    
        }    
        else 
        {
            // komponens letiltás:
            isDisabled = true;    
        }

        for (var i=0; i<targetIds.length; i++)
        {
            var targetComponent = document.getElementById(targetIds[i]);
            if (!targetComponent) continue;
            if (targetComponent)
            {
                // komponens engedélyezés/tiltás:
                targetComponent.disabled = isDisabled;    
            }    
        }
    }
}

// ha a mode==0: checkbox checked állapotban a célkomponensek enabled-be kerülnek,
//    unchecked állapotban disabled-be, mode==1 esetén fordítva
function showOrHideComponents(srcId,mode,targetIds)
{
    var srcCheckBox = document.getElementById(srcId);
    if (!srcCheckBox) return;
    var i = 0;
    for (i=0; i<targetIds.length; i++)
    {
        var targetComponent = document.getElementById(targetIds[i]);
        if (!targetComponent) continue;
        if (targetComponent)
        {
            if ((srcCheckBox.checked == true && mode==0)
                || (srcCheckBox.checked == false && mode==1))
            {
                // komponens engedélyezés:
                targetComponent.style.display = 'inline';    
            }    
            else
            {
                // komponens letiltás:
                targetComponent.style.display = 'none';    
            }
        }
    }
}

// ha a mode==0: checkbox checked állapotban a célkomponensek(képek) enabled-be kerülnek,
// unchecked állapotban disabled-be és elhalványulnak, mode==1 esetén fordítva
function enableOrDisableImages(srcId,mode,targetIds)
{
    var srcCheckBox = document.getElementById(srcId);
    if(srcCheckBox)
    {
        var i = 0;
        for (i=0; i<targetIds.length; i++)
        {
            var targetComponent = document.getElementById(targetIds[i]);
            if(targetComponent)
            {
                if ((srcCheckBox.checked == true && mode==0)
                    || (srcCheckBox.checked == false && mode==1))
                {
                    // komponens engedélyezés:
                    targetComponent.disabled = false;
                    targetComponent.style.filter = '';
                    targetComponent.style.MozOpacity = '';
                    targetComponent.style.cursor= '';     
                }    
                else
                {
                    // komponens letiltás:
                    targetComponent.disabled = true;
                    targetComponent.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=40)';
                    targetComponent.style.MozOpacity = '0.4';
                    targetComponent.style.cursor = 'default';
                }
            }
        }
    }
}

// ha a mode==0: checkbox checked állapotban a validator enabled-be kerül,
//    unchecked állapotban disabled-be, mode==1 esetén fordítva
function enableOrDisableValidator(srcId,mode,validatorIds)
{
    var srcCheckBox = document.getElementById(srcId);
    if (!srcCheckBox) return;
    var i = 0;
    for (i=0; i<validatorIds.length; i++)
    {
        var myVal = document.getElementById(validatorIds[i]);
        if (!myVal) continue;
        if ((srcCheckBox.checked == true && mode==0)
            || (srcCheckBox.checked == false && mode==1))
        {
            // Validator engedélyezés:
            //ValidatorEnable(myVal, true); 
            myVal.enabled = true;
        }    
        else
        {
            // Validator letiltás:
            //ValidatorEnable(myVal, false);
            myVal.enabled = false;
        }
    }
    
}

function SetComponentValueByCheckBox(srcId,targetId,checkedValue,uncheckedValue)
{
    var srcCheckBox = document.getElementById(srcId);
    if (!srcCheckBox) return;
    var targetComponent = document.getElementById(targetId);
    if (!targetComponent) return;
    
    if (srcCheckBox.checked == true)
    {
        targetComponent.value = checkedValue;
    }    
    else
    {
        targetComponent.value = uncheckedValue;
    }
     
}

// Megadja a bejelölt (checked) checkboxok számát azok közül, amelyeknek 
// az id-ja a megadott prefixszel kezd?dik
function getSelectedCheckBoxesCount(idPrefix,checkBoxID)
{
    var count = 0;
    var prefixLength = idPrefix.length;
    
    var x=document.getElementsByTagName("input");
    
    var i = 0;
    for (i = 0;i<x.length;i++)
    {
        if (x[i].type=="checkbox" && x[i].checked==true 
                && x[i].id.substr(0,prefixLength)==idPrefix && x[i].id.indexOf(checkBoxID) != -1
                )
        {
            count++;
        }    
    }
    
    return count;
}

function SelectAllCheckBox(idPrefix,checkBoxID)
{
    var prefixLength = idPrefix.length;
    
    var x=document.getElementsByTagName("input");
    
    var i = 0;
    for (i = 0;i<x.length;i++)
    {
        if (x[i].type=="checkbox" 
                && x[i].id.substr(0,prefixLength)==idPrefix && x[i].id.indexOf(checkBoxID) != -1 && !x[i].disabled
                )
        {
            x[i].checked=true
        }    
    }
}

function DeSelectAllCheckBox(idPrefix,checkBoxID)
{
    var prefixLength = idPrefix.length;
    
    var x=document.getElementsByTagName("input");
    
    var i = 0;
    for (i = 0;i<x.length;i++)
    {
        if (x[i].type=="checkbox" 
                && x[i].id.substr(0,prefixLength)==idPrefix && x[i].id.indexOf(checkBoxID) != -1
                )
        {
            x[i].checked=false
        }    
    }
}

 function findElement(parentControl,id,tagName,type)
   {
      var control = parentControl;
      if(control.childNodes.length > 0)
        {
            for(var i = 0; i<control.childNodes.length; i++)
            {
                var child = control.childNodes[i];
                if(child.id != undefined)
                {
                    if(child.id.toLowerCase().indexOf(id.toLowerCase()) != -1)
                    {
                        if(tagName != undefined)
                        {
                            if(child.tagName != undefined && child.tagName.toLowerCase() == tagName)
                            {
                                if(type != undefined)
                                {
                                    if(child.type != undefined && child.type.toLowerCase() == type)
                                    {
                                        return child;
                                    }
                                }
                                else
                                {
                                    return child;
                                }
                            }   
                        }
                        else
                        {
                            return child;
                        }
                    }
                }
               var found = this.findElement(child,id,tagName,type);
               if(found != null)
               {
                 return found;
               }
            }
        }
        
        return null;
  }
    
 function getSelectedAndNotModifiable(gridViewClientID,selectCheckboxID,modosithatoCheckboxID)
 {
 var selectCount = 0;
 var nemModosithatoCount = 0;
 var gridView = $get(gridViewClientID);
 var i = 0;
 for(i;i<gridView.rows.length;i++)
 {
    var j = 0;
    var row = gridView.rows[i];
    var rowSelected = false;
    for(j;j<row.cells.length;j++)
    {
        var cell = row.cells[j];
        var selectCheckBox = findElement(cell,selectCheckboxID,'input','checkbox');
        
        if(selectCheckBox != null)
        {
            if(selectCheckBox.checked == true)
            {
             selectCount++;
             rowSelected = true;
            }
        } 
        if(rowSelected)
        {
            var modosithatoCheckBox = findElement(cell,modosithatoCheckboxID,'input','checkbox');
            if(modosithatoCheckBox != null)
            {
                 if(modosithatoCheckBox.checked == false)
                {
                 nemModosithatoCount++;
                }
            }
        }    
    } 
 }
return {
    selected: selectCount,
    notModifiable: nemModosithatoCount
}
}

// 
 function getSelectedAndCheckboxChecked(gridViewClientID,selectCheckboxID,ischeckedCheckboxID)
 {
 var selectCount = 0;
 var checkedCount = 0;
 var gridView = $get(gridViewClientID);
 var i = 0;
 for(i;i<gridView.rows.length;i++)
 {
    var j = 0;
    var row = gridView.rows[i];
    var rowSelected = false;
    for(j;j<row.cells.length;j++)
    {
        var cell = row.cells[j];
        var selectCheckBox = findElement(cell,selectCheckboxID,'input','checkbox');
        
        if(selectCheckBox != null)
        {
            if(selectCheckBox.checked == true)
            {
             selectCount++;
             rowSelected = true;
            }
        } 
        if(rowSelected)
        {
            var ischeckedCheckBox = findElement(cell,ischeckedCheckboxID,'input','checkbox');
            if(ischeckedCheckBox != null)
            {
                 if(ischeckedCheckBox.checked == true)
                {
                 checkedCount++;
                }
            }
        }    
    } 
 }
return {
    selected: selectCount,
    checked: checkedCount
}
}

 function getSelectedAndLocked(gridViewClientID,selectCheckboxID,lockedImageID)
 {
 var selectCount = 0;
 var lockedCount = 0;
 var gridView = $get(gridViewClientID);
 var i = 0;
 for(i;i<gridView.rows.length;i++)
 {
    var j = 0;
    var row = gridView.rows[i];
    var rowSelected = false;
    for(j;j<row.cells.length;j++)
    {
        var cell = row.cells[j];
        var selectCheckBox = findElement(cell,selectCheckboxID,'input','checkbox');
        
        if(selectCheckBox != null)
        {
            if(selectCheckBox.checked == true)
            {
             selectCount++;
             rowSelected = true;
            }
        } 
        if(rowSelected)
        {
            var lockedImage = findElement(cell,lockedImageID,'img', undefined);
            if(lockedImage != null)
            {
                 //if(lockedImage.style.visibility == 'visible')
                //{
                 lockedCount++;
                //}
            }
        }    
    } 
 }
return {
    selected: selectCount,
    locked: lockedCount
}
}

 function getSelectedAndLockedAndNotModifiable(gridViewClientID,selectCheckboxID,modosithatoCheckboxID,lockedImageID)
 {
 var selectCount = 0;
 var nemModosithatoCount = 0;
 var lockedCount = 0;
 var gridView = $get(gridViewClientID);
 var i = 0;
 for(i;i<gridView.rows.length;i++)
 {
    var j = 0;
    var row = gridView.rows[i];
    var rowSelected = false;
    for(j;j<row.cells.length;j++)
    {
        var cell = row.cells[j];
        var selectCheckBox = findElement(cell,selectCheckboxID,'input','checkbox');
        
        if(selectCheckBox != null)
        {
            if(selectCheckBox.checked == true)
            {
             selectCount++;
             rowSelected = true;
            }
        } 
        if(rowSelected)
        {
            var lockedImage = findElement(cell,lockedImageID,'img', undefined);
            if(lockedImage != null)
            {
                 lockedCount++;
            }
            else
            {
                var modosithatoCheckBox = findElement(cell,modosithatoCheckboxID,'input','checkbox');
                if(modosithatoCheckBox != null)
                {
                    if(modosithatoCheckBox.checked == false)
                    {
                        nemModosithatoCount++;
                    }
                }
            }
        }    
    } 
 }
return {
    selected: selectCount,
    notModifiable: nemModosithatoCount,
    locked: lockedCount
}
}
 
 function getIdsBySelectedCheckboxes(idPrefix,checkBoxID)
 {
 
   var strIds = new String();
   var count = 0;
 
   var prefixLength = idPrefix.length;
    
    var x=document.getElementsByTagName("input");
    
    var i = 0;
    for (i = 0;i<x.length;i++)
    {
        if (x[i].type=="checkbox" 
                && x[i].id.substr(0,prefixLength)==idPrefix && x[i].id.indexOf(checkBoxID) != -1
                )
        {
            // szomszédos elem:
            if (x[i].checked==true)
            {
                var nextLabel = x[i].nextSibling;
                //alert(nextLabel.firstChild.nodeValue);
                if (count == 0)
                {
                    strIds = nextLabel.firstChild.nodeValue;
                }
                else
                {
                    strIds = strIds + "," + nextLabel.firstChild.nodeValue;          
                }
                count++;
            }
        }    
    }
    
    //alert(strIds);
    
    return strIds;
 
}

function getIdIfOneSelectedCheckbox(idPrefix,checkBoxID)
 {
 
   var strId = '';
   var count = 0;
 
   var prefixLength = idPrefix.length;
    
   var x=document.getElementsByTagName("input");
    
   var length = x.length;
    
    var i = 0;
    for (i = 0;i<length;i++)
    {
        var checkBox = x[i];
        if (checkBox.type=="checkbox" 
            && checkBox.id.substr(0,prefixLength)==idPrefix && checkBox.id.indexOf(checkBoxID) != -1)
        {
            // szomszédos elem:
            if (checkBox.checked==true)
            {
                var nextLabel = checkBox.nextSibling;
                //alert(nextLabel.firstChild.nodeValue);
                if (count == 0)
                {
                    strId = nextLabel.firstChild.nodeValue;
                }
                else
                {
                    return '';          
                }
                count++;
            }
        }    
    }
    
    return strId;
 
}

