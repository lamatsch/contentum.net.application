﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.CimAutomatikusKitoltesBehavior = function (element) {
    Utility.CimAutomatikusKitoltesBehavior.initializeBase(this, [element]);

    //Properties
    this.felhasznaloId = null;
    this.kuldesModDropDownListClientId = null;
    this.iratPeldanyTipusaDropDownListClientId = null;
    this.cimTextBoxClientId = null;
    this.cimHiddenFieldClientId = null;
    this.partnerHiddenFieldClientId = null;
    this.iratPeldanyHiddenFieldClientId = null;
    this.ugyiratHiddenFieldClientId = null;

    //Variables
    this.servicePath = "WrappedWebService/Ajax.asmx";
    this.serviceMethodGetKRID = "GetKRID";
    this.loadAddressButton = null;
    this.loadAddressButtonClickHandler = null;
    this.applicationLoadHandler = null;
    this.kuldesModDropDownList = null;
    this.iratPeldanyTipusaDropDownList = null;
    this.kuldesModDropDownListChangedHandler = null;
    this.iratPeldanyTipusaDropDownListChangedHandler = null;
    this.cimTextBox = null;
    this.cimHiddenField = null;
    this.partnerHiddenField = null;
    this.iratPeldanyHiddenField = null;
    this.ugyiratHiddenField = null;
    this.loadingText = 'Lekérdezés folyamatban...';
    this.preLoadingText = '';
    this.readOnly = false;

}

Utility.CimAutomatikusKitoltesBehavior.prototype = {
    initialize: function () {
        Utility.CimAutomatikusKitoltesBehavior.callBaseMethod(this, 'initialize');

        this.loadAddressButton = this.get_element();

        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);
        Sys.Application.add_load(this.applicationLoadHandler);

        this.loadAddressButtonClickHandler = Function.createDelegate(this, this.onLoadAddressButtonClick);

        if (this.loadAddressButton && this.loadAddressButtonClickHandler)
            $addHandler(this.loadAddressButton, 'click', this.loadAddressButtonClickHandler);

        if (this.kuldesModDropDownListClientId)
            this.kuldesModDropDownList = $get(this.kuldesModDropDownListClientId);

        if (this.iratPeldanyTipusaDropDownListClientId)
            this.iratPeldanyTipusaDropDownList = $get(this.iratPeldanyTipusaDropDownListClientId);

        this.kuldesModDropDownListChangedHandler = Function.createDelegate(this, this.onKuldesModDropDownListChanged);

        if (this.kuldesModDropDownListChangedHandler && this.kuldesModDropDownList) {
            $addHandler(this.kuldesModDropDownList, 'change', this.kuldesModDropDownListChangedHandler);
        }

        this.iratPeldanyTipusaDropDownListChangedHandler = Function.createDelegate(this, this.oniratPeldanyTipusaDropDownListChanged);

        if (this.iratPeldanyTipusaDropDownListChangedHandler && this.iratPeldanyTipusaDropDownList) {
            $addHandler(this.iratPeldanyTipusaDropDownList, 'change', this.iratPeldanyTipusaDropDownListChangedHandler);
        }



        if (this.cimTextBoxClientId)
            this.cimTextBox = $get(this.cimTextBoxClientId);

        if (this.cimHiddenFieldClientId)
            this.cimHiddenField = $get(this.cimHiddenFieldClientId);

        if (this.partnerHiddenFieldClientId)
            this.partnerHiddenField = $get(this.partnerHiddenFieldClientId);

        if (this.iratPeldanyHiddenFieldClientId)
            this.iratPeldanyHiddenField = $get(this.iratPeldanyHiddenFieldClientId);

        if (this.ugyiratHiddenFieldClientId)
            this.ugyiratHiddenField = $get(this.ugyiratHiddenFieldClientId);

        if (this.cimTextBox)
            this.readOnly = this.cimTextBox.readOnly;

    },

    dispose: function () {

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
        }

        if (this.loadAddressButtonClickHandler && this.loadAddressButton) {
            $removeHandler(this.loadAddressButton, 'click', this.loadAddressButtonClickHandler);
            this.loadAddressButtonClickHandler = null;
        }

        if (this.kuldesModDropDownListChangedHandler && this.kuldesModDropDownList) {
            $removeHandler(this.kuldesModDropDownList, 'change', this.kuldesModDropDownListChangedHandler);
            this.kuldesModDropDownListChangedHandler = null;
        }

        if (this.iratPeldanyTipusaDropDownListChangedHandler && this.iratPeldanyTipusaDropDownList) {
            $removeHandler(this.iratPeldanyTipusaDropDownList, 'change', this.iratPeldanyTipusaDropDownListChangedHandler);
            this.iratPeldanyTipusaDropDownListChangedHandler = null;
        }

        Utility.CimAutomatikusKitoltesBehavior.callBaseMethod(this, 'dispose');
    },

    OnApplicationLoad: function () {
        this.setLoadAddressButtonVisibility();
    },

    onLoadAddressButtonClick: function (args) {
        this.loadKRID();
    },

    onKuldesModDropDownListChanged: function (args) {
        this.setLoadAddressButtonVisibility();
    },

    oniratPeldanyTipusaDropDownListChanged: function (args) {
        this.setLoadAddressButtonVisibility();
    },

    loadKRID: function () {
        if (this.partnerHiddenField) {

            if (this.partnerHiddenField.value) {
                var fajta='';
                var kuldesMod='';
                var pId='';
                var uId='';

                if (this.iratPeldanyTipusaDropDownList) {
                    fajta = this.iratPeldanyTipusaDropDownList.options[this.iratPeldanyTipusaDropDownList.selectedIndex].value;
                }
                if (this.kuldesModDropDownList) {
                    kuldesMod = this.kuldesModDropDownList.options[this.kuldesModDropDownList.selectedIndex].value;
                }
                if (this.iratPeldanyHiddenField)
                    pId = this.iratPeldanyHiddenField.value;                

                if (this.ugyiratHiddenField)
                    uId = this.ugyiratHiddenField.value;

                var params = { felhasznaloId: this.felhasznaloId, partnerId: this.partnerHiddenField.value, iratPeldanyId: pId, ugyiratId: uId, iratPeldanyFajta: fajta, expedialasModja: kuldesMod };

                this.loading(true);
                // Invoke the web service
                Sys.Net.WebServiceProxy.invoke(this.servicePath, this.serviceMethodGetKRID, false, params,
                    Function.createDelegate(this, this.onWsCallBack_GetKRIDSuccess),
                    Function.createDelegate(this, this.onWsCallBack_GetKRIDError), null);
            }
            else {
                alert('Nincs kiválasztva partner!');
            }
        }
    },

    onWsCallBack_GetKRIDSuccess: function (result) {
        this.loading(false);
        if (result) {

            var splitted = result.split('||');

            if (this.cimTextBox)
                this.cimTextBox.value = splitted[0];

            if (this.cimHiddenField)
                this.cimHiddenField.value = splitted[1];
        }
    },

    onWsCallBack_GetKRIDError: function (error) {
        this.loading(false);
        alert(error.get_message());
    },

    loading: function (value) {
    },

    setLoadAddressButtonVisibility: function () {
        if (this.loadAddressButton) {
            if (this.readOnly) {
                this.loadAddressButton.style.display = 'none';
            }
            else
                if (this.kuldesModDropDownList && this.kuldesModDropDownList.selectedIndex > -1 && this.kuldesModDropDownList.options[this.kuldesModDropDownList.selectedIndex] && this.iratPeldanyTipusaDropDownList && this.iratPeldanyTipusaDropDownList.selectedIndex > -1) {
                    var kuldesMod = this.kuldesModDropDownList.options[this.kuldesModDropDownList.selectedIndex].value;
                    var fajta = this.iratPeldanyTipusaDropDownList.options[this.iratPeldanyTipusaDropDownList.selectedIndex].value;

                    //if (fajta == '1' || this.isKuldesModElektronikus(kuldesMod))
                    this.loadAddressButton.style.display = '';
                    //else
                    //    this.loadAddressButton.style.display = 'none';
                }
        }
    },

    isKuldesModElektronikus: function (kuldesMod) {
        return (kuldesMod == '19' || kuldesMod == '190');
    },

    //getter, setter methods
    get_FelhasznaloId: function () {
        return this.felhasznaloId;
    },

    set_FelhasznaloId: function (value) {
        if (this.felhasznaloId != value) {
            this.felhasznaloId = value;
        }
    },

    get_KuldesModDropDownListClientId: function () {
        return this.kuldesModDropDownListClientId;
    },

    set_KuldesModDropDownListClientId: function (value) {
        if (this.kuldesModDropDownListClientId != value) {
            this.kuldesModDropDownListClientId = value;
        }
    },

    get_IratPeldanyTipusaDropDownListClientId: function () {
        return this.iratPeldanyTipusaDropDownListClientId;
    },

    set_IratPeldanyTipusaDropDownListClientId: function (value) {
        if (this.iratPeldanyTipusaDropDownListClientId != value) {
            this.iratPeldanyTipusaDropDownListClientId = value;
        }
    },

    get_CimTextBoxClientId: function () {
        return this.cimTextBoxClientId;
    },

    set_CimTextBoxClientId: function (value) {
        if (this.cimTextBoxClientId != value) {
            this.cimTextBoxClientId = value;
        }
    },

    get_CimHiddenFieldClientId: function () {
        return this.cimHiddenFieldClientId;
    },

    set_CimHiddenFieldClientId: function (value) {
        if (this.cimHiddenFieldClientId != value) {
            this.cimHiddenFieldClientId = value;
        }
    },

    get_PartnerHiddenFieldClientId: function () {
        return this.partnerHiddenFieldClientId;
    },

    set_PartnerHiddenFieldClientId: function (value) {
        if (this.partnerHiddenFieldClientId != value) {
            this.partnerHiddenFieldClientId = value;
        }
    },

    get_IratPeldanyHiddenFieldClientId: function () {
        return this.iratPeldanyHiddenFieldClientId;
    },

    set_IratPeldanyHiddenFieldClientId: function (value) {
        if (this.iratPeldanyHiddenFieldClientId != value) {
            this.iratPeldanyHiddenFieldClientId = value;
        }
    },

    get_UgyiratHiddenFieldClientId: function () {
        return this.ugyiratHiddenFieldClientId;
    },

    set_UgyiratHiddenFieldClientId: function (value) {
        if (this.ugyiratHiddenFieldClientId != value) {
            this.ugyiratHiddenFieldClientId = value;
        }
    }
}


Utility.CimAutomatikusKitoltesBehavior.registerClass('Utility.CimAutomatikusKitoltesBehavior', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();