﻿// JScript File
/// <reference name="MicrosoftAjax.js" />
/// <reference name="MicrosoftAjaxTimer.js" />
/// <reference name="MicrosoftAjaxWebForms.js" />
/// <reference path="Common.js" />

Type.registerNamespace("Utility");

Utility.ListItem = function () {
    this.name = null;
    this.value = null;
    this.isDefaultValue = false;
}

Utility.CimTipusBehavior = function (element) {
    Utility.CimTipusBehavior.initializeBase(this, [element]);

    //Properties
    this.felhasznaloId = null;
    this.kuldesModDropDownListClientId = null;
    this.cimTextBoxClientId = null;
    this.cimHiddenFieldClientId = null;
    this.partnerAutoCompleteBehaviorClientId = null;
    this.cimTextBoxValidatorClientId = null;
    //Variables
    this.cimTipusDropDownList = null;
    this.applicationLoadHandler = null;
    this.kuldesModDropDownList = null;
    this.kuldesModDropDownListChangedHandler = null;
    this.servicePath = "WrappedWebService/Ajax.asmx";
    this.serviceMethodGetCimTipusok = "GetCimTipusok";
    this.serviceMethodGetCimTipus = "GetCimTipus";
    this.currentKuldesMod = null;
    this.cimTextBox = null;
    this.cimHiddenField = null;
    this.cimTextChangedHandler = null;
    this.partnerAutoCompleteBehavior = null;
    this.partnerAutoCompleteExtender = null;
    this.cimTipusDropDownListChangedHandler = null;
    this.currentCimTipus = '';
    this.readOnly = false;
    this.partnerItemSelectedHandler = null;
    this.cimTextBoxValidator = null;
    this.cimTextBoxValidatorEnabled = false;
    this.partnerHiddenField = null;
    this.serviceMethodIsElosztoIv = "IsElosztoIv";
    this.currentParnterId = '';
    this.isCurrentParnterEloszotoIv = false;
    
}

Utility.CimTipusBehavior.InitObject = function () {
    this.Initialized = false;
    this.CimText = '';
    this.CimId = '';
}

Utility.CimTipusBehavior.Init = new Array();

Utility.CimTipusBehavior.prototype = {
    initialize: function () {
        Utility.CimTipusBehavior.callBaseMethod(this, 'initialize');

        this.cimTipusDropDownList = this.get_element();

        this.applicationLoadHandler = Function.createDelegate(this, this.OnApplicationLoad);
        Sys.Application.add_load(this.applicationLoadHandler);

        if (this.kuldesModDropDownListClientId)
            this.kuldesModDropDownList = $get(this.kuldesModDropDownListClientId);

        this.kuldesModDropDownListChangedHandler = Function.createDelegate(this, this.onKuldesModDropDownListChanged);

        if (this.kuldesModDropDownListChangedHandler && this.kuldesModDropDownList)
        {
            $addHandler(this.kuldesModDropDownList, 'change', this.kuldesModDropDownListChangedHandler);
        }

        if (this.cimTextBoxClientId)
            this.cimTextBox = $get(this.cimTextBoxClientId);

        if (this.cimHiddenFieldClientId)
            this.cimHiddenField = $get(this.cimHiddenFieldClientId);

        this.cimTextChangedHandler = Function.createDelegate(this, this.onCimTextChanged);

        if (this.cimTextBox && this.cimTextChangedHandler) {
            $addHandler(this.cimTextBox, 'change', this.cimTextChangedHandler);
        }

        this.cimTipusDropDownListChangedHandler = Function.createDelegate(this, this.onCimTipusDropDownListChanged);
        if (this.cimTipusDropDownList && this.cimTipusDropDownListChangedHandler) {
            $addHandler(this.cimTipusDropDownList, 'change', this.cimTipusDropDownListChangedHandler);
        }

        if (this.cimTextBox)
            this.readOnly = this.cimTextBox.readOnly;

        if (this.readOnly && this.cimTipusDropDownList)
            this.cimTipusDropDownList.disabled = true;

        this.partnerItemSelectedHandler = Function.createDelegate(this, this.OnPartnerItemSelected);

        if (this.cimTextBoxValidatorClientId)
            this.cimTextBoxValidator = $get(this.cimTextBoxValidatorClientId);

        if (this.cimTextBoxValidator)
            this.cimTextBoxValidatorEnabled = this.cimTextBoxValidator.enabled;

        this.saveInitValues();
    },

    dispose: function () {

        if (this.applicationLoadHandler != null) {
            Sys.Application.remove_load(this.applicationLoadHandler);
        }

        if (this.kuldesModDropDownListChangedHandler && this.kuldesModDropDownList) {
            $removeHandler(this.kuldesModDropDownList, 'change', this.kuldesModDropDownListChangedHandler);
            this.kuldesModDropDownListChangedHandler = null;
        }

        if (this.cimTextBox && this.cimTextChangedHandler) {
            $removeHandler(this.cimTextBox, 'change', this.cimTextChangedHandler);
            this.cimTextChangedHandler = null;
        }

        if (this.cimTipusDropDownList && this.cimTipusDropDownListChangedHandler) {
            $removeHandler(this.cimTipusDropDownList, 'change', this.cimTipusDropDownListChangedHandler);
            cimTipusDropDownListChangedHandler = null;
        }

        if(this.partnerAutoCompleteBehavior && this.partnerItemSelectedHandler)
        {
            this.partnerAutoCompleteBehavior.remove_itemSelected(this.partnerItemSelectedHandler);
            this.partnerItemSelectedHandler = null;

        }

        Utility.CimTipusBehavior.callBaseMethod(this, 'dispose');
    },

    saveInitValues: function () {
        var key = this.get_id();
        var initObject = Utility.CimTipusBehavior.Init[key];

        if (!initObject) {
            initObject = new Utility.CimTipusBehavior.InitObject();
            Utility.CimTipusBehavior.Init[key] = initObject;
        }

        if (!initObject.Initialized) {
            initObject.Initialized = true;


            if (this.cimTextBox)
                initObject.CimText = this.cimTextBox.value;

            if (this.cimHiddenField)
                initObject.CimId = this.cimHiddenField.value;
        }
    },

    getInitValue: function (prop) {
        var key = this.get_id();
        var initObject = Utility.CimTipusBehavior.Init[key];

        if (initObject && initObject[prop])
            return initObject[prop];

        return '';
    },

    OnApplicationLoad: function () {
        if (this.partnerAutoCompleteBehaviorClientId)
            this.partnerAutoCompleteBehavior = $find(this.partnerAutoCompleteBehaviorClientId);

        if (this.partnerAutoCompleteBehavior)
            this.partnerAutoCompleteExtender = $find(this.partnerAutoCompleteBehavior.autoCompleteExtenderClientId);

        if (this.partnerAutoCompleteBehavior) {
            var partnerHiddenFieldClientId = this.partnerAutoCompleteBehavior.get_HiddenFieldClientId();

            if (partnerHiddenFieldClientId)
                this.partnerHiddenField = $get(partnerHiddenFieldClientId);
        }

        if (!this.readOnly)
            this.fillCimTipusok();
        else
            this.setCimTipus();

        if (this.partnerAutoCompleteBehavior)
            this.partnerAutoCompleteBehavior.add_itemSelected(this.partnerItemSelectedHandler);
    },

    onKuldesModDropDownListChanged: function (args) {
        this.fillCimTipusok();
    },

    fillCimTipusok: function () {
        var kuldesMod = '';

        if (this.kuldesModDropDownList && this.kuldesModDropDownList.selectedIndex > -1 && this.kuldesModDropDownList.options[this.kuldesModDropDownList.selectedIndex])
            kuldesMod = this.kuldesModDropDownList.options[this.kuldesModDropDownList.selectedIndex].value;

        if (this.currentKuldesMod != kuldesMod) {
            this.currentKuldesMod = kuldesMod;
            var params = { felhasznaloId: this.felhasznaloId, kuldesMod: kuldesMod };

            this.loading(true);
            // Invoke the web service
            Sys.Net.WebServiceProxy.invoke(this.servicePath, this.serviceMethodGetCimTipusok, false, params,
                Function.createDelegate(this, this.onWsCallBack_GetCimTipusokSuccess),
                Function.createDelegate(this, this.onWsCallBack_GetCimTipusokError), null);
        }
    },

    onWsCallBack_GetCimTipusokSuccess: function (result) {
        if (result) {

            if (this.cimTipusDropDownList) {
                var selectedCimTipus = null;
                if (this.cimTipusDropDownList.selectedIndex > -1) {
                    selectedCimTipus = this.cimTipusDropDownList.options[this.cimTipusDropDownList.selectedIndex].value;
                }

                while (this.cimTipusDropDownList.options.length > 0) {
                    this.cimTipusDropDownList.remove(0);
                } 
                
                for (var i in result) {
                    var listItem = result[i];
                    var option = document.createElement("option");
                    option.text = listItem.name;
                    option.value = listItem.value;
                    if (option.value == selectedCimTipus)
                        option.selected = true;
                    this.cimTipusDropDownList.add(option); 
                }

                this.setCimTipus();

                this.setCimTextBoxValidator();
            }
        }
        this.loading(false);
    },

    onWsCallBack_GetCimTipusokError: function (error) {
        this.loading(false);
        alert(error.get_message());
    },

    onCimTextChanged: function (args) {
        if (!this.partnerAutoCompleteBehavior || !this.partnerAutoCompleteBehavior.ItemSelecting)
            this.setCimTipus();
    },

    setCimTipus: function () {
        var cimId = null;

        if (this.cimHiddenField)
            cimId = this.cimHiddenField.value;

        if (cimId) {
            var params = { felhasznaloId: this.felhasznaloId, cimId: cimId };

            this.loading(true);
            // Invoke the web service
            Sys.Net.WebServiceProxy.invoke(this.servicePath, this.serviceMethodGetCimTipus, false, params,
                Function.createDelegate(this, this.onWsCallBack_GetCimTipusSuccess),
                Function.createDelegate(this, this.onWsCallBack_GetCimTipusError), null);
        }
        else {
            this.setPartnerAutoCompleteExtenderContextKey();
            this.currentCimTipus = '';
            if (!this.readOnly) {
                var cimText = this.cimTextBox.value;

                if (cimText)
                    this.setSelectedCimTipus(this.currentCimTipus);
            }
            else {
                this.setReadOnlyCimTipus('Nincs', '');
            }
        }
    },

    onWsCallBack_GetCimTipusSuccess: function (result) {
        if (result) {
            this.currentCimTipus = result.value;

            if (!this.readOnly)
                this.setSelectedCimTipus(this.currentCimTipus);
            else
                this.setReadOnlyCimTipus(result.name, result.value);
        }

        this.setPartnerAutoCompleteExtenderContextKey();
        this.loading(false);
    },

    onWsCallBack_GetCimTipusError: function (error) {
        this.loading(false);
        alert(error.get_message());
    },

    loading: function (value) {
        if (!this.readOnly) {
            if (value) {
                this.cimTipusDropDownList.disabled = true;
            }
            else {
                this.cimTipusDropDownList.disabled = false;
            }
        }
    },

    get_selectedCimTipus: function() {

        var selectedCimTipus = '';
        if (this.cimTipusDropDownList) {
            if (this.cimTipusDropDownList.selectedIndex > -1) {
                selectedCimTipus = this.cimTipusDropDownList.options[this.cimTipusDropDownList.selectedIndex].value;
            }
        }

        return selectedCimTipus;
    },

    onCimTipusDropDownListChanged: function (args) {
        this.setPartnerAutoCompleteExtenderContextKey();

        if (!Array.contains(this.get_selectedCimTipus().split(','), this.currentCimTipus)) {
            this.clearCimCheckElosztoIv();
        }
    },

    setPartnerAutoCompleteExtenderContextKey: function () {
        if (this.partnerAutoCompleteExtender) {
            var contextKey = this.partnerAutoCompleteExtender.get_contextKey();
            var cimTipus = this.get_selectedCimTipus();

            var filter = 'CimTipus=' + cimTipus;
            var pattern = /CimTipus=[^; ]*/;

            if (pattern.test(contextKey)) {
                contextKey = contextKey.replace(pattern, filter);
            }
            else {
                if (!contextKey.endsWith(';'))
                    contextKey += ';';

                contextKey += filter;
            }

            this.partnerAutoCompleteExtender.set_contextKey(contextKey);
            this.partnerAutoCompleteExtender._cache = {};
        }
    },

    clearCim: function () {
        if (this.cimTextBox && this.cimHiddenField) {
            if (!this.isCurrentParnterEloszotoIv) {
                this.cimTextBox.value = '';
                this.cimHiddenField.value = '';
            }
        }
    },

    clearCimCheckElosztoIv: function () {
        this.isElosztoIv();
    },

    setSelectedCimTipus: function (value) {
        var validCimTipus = false;
        if (this.cimTipusDropDownList) {

            for (var i = 0; i < this.cimTipusDropDownList.length; i++) {
                var opt = this.cimTipusDropDownList.options[i];

                if (opt.value == value) {
                    this.cimTipusDropDownList.value = value;
                    validCimTipus = true;
                    break;
                }
            }
        }

        var cimText = '';
        if (this.cimTextBox)
            cimText = this.cimTextBox.value;

        var cimId = '';
        if (this.cimHiddenField)
            cimId = this.cimHiddenField.value;

        if (cimText == this.getInitValue('CimText')
            && cimId == this.getInitValue('CimId')) {
            validCimTipus = true;
        }
        
        if (!validCimTipus)
            this.clearCimCheckElosztoIv();
    },

    setReadOnlyCimTipus: function (name, value) {
        if (this.cimTipusDropDownList) {
            while (this.cimTipusDropDownList.options.length > 0) {
                this.cimTipusDropDownList.remove(0);
            }

            var option = document.createElement("option");
            option.text = name;
            option.value = value;
            option.selected = true;
            this.cimTipusDropDownList.add(option);
        }
    },

    OnPartnerItemSelected: function (sender, args) {
        this.setCimTipus();
    },

    setCimTextBoxValidator: function () {
        if (this.cimTipusDropDownList && this.cimTextBoxValidator && !this.readOnly) {

            var emptyCimEnabled = false;
            for (var i = 0; i < this.cimTipusDropDownList.length; i++) {
                var opt = this.cimTipusDropDownList.options[i];

                if (opt.value == '') {
                    emptyCimEnabled = true;
                    break;
                }
            }

            if (emptyCimEnabled)
                this.cimTextBoxValidator.enabled = this.cimTextBoxValidatorEnabled;
            else
                this.cimTextBoxValidator.enabled = true;
        }
    },

    isElosztoIv: function () {

        if (this.partnerHiddenField) {

            var partnerId = this.partnerHiddenField.value;

            if (partnerId)
            {
                if (partnerId != this.currentParnterId) {
                    this.currentParnterId = partnerId;

                    var params = { felhasznaloId: this.felhasznaloId, partnerId: partnerId };

                    // Invoke the web service
                    Sys.Net.WebServiceProxy.invoke(this.servicePath, this.serviceMethodIsElosztoIv, false, params,
                        Function.createDelegate(this, this.onWsCallBack_IsElosztoIvSuccess),
                        Function.createDelegate(this, this.onWsCallBack_IsElosztoIvError), null);
                }
                else {
                    this.clearCim();
                }
            }
            else
            {
                this.currentParnterId = '';
                this.isCurrentParnterEloszotoIv = false;
                this.clearCim();
            }

        }
    },

    onWsCallBack_IsElosztoIvSuccess: function (result) {
        if (result) {

            if (result) {
                this.isCurrentParnterEloszotoIv = true;
            }
            else {
                this.isCurrentParnterEloszotoIv = false;
            }
        }
        this.clearCim();
    },

    onWsCallBack_IsElosztoIvError: function (error) {
        this.loading(false);
        alert(error.get_message());
    },

    //getter, setter methods
    get_FelhasznaloId: function () {
        return this.felhasznaloId;
    },

    set_FelhasznaloId: function (value) {
        if (this.felhasznaloId != value) {
            this.felhasznaloId = value;
        }
    },

    get_KuldesModDropDownListClientId: function () {
        return this.kuldesModDropDownListClientId;
    },

    set_KuldesModDropDownListClientId: function (value) {
        if (this.kuldesModDropDownListClientId != value) {
            this.kuldesModDropDownListClientId = value;
        }
    },

    get_CimTextBoxClientId: function () {
        return this.cimTextBoxClientId;
    },

    set_CimTextBoxClientId: function (value) {
        if (this.cimTextBoxClientId != value) {
            this.cimTextBoxClientId = value;
        }
    },

    get_CimHiddenFieldClientId: function () {
        return this.cimHiddenFieldClientId;
    },

    set_CimHiddenFieldClientId: function (value) {
        if (this.cimHiddenFieldClientId != value) {
            this.cimHiddenFieldClientId = value;
        }
    },

    get_PartnerAutoCompleteBehaviorClientId: function () {
        return this.partnerAutoCompleteBehaviorClientId;
    },

    set_PartnerAutoCompleteBehaviorClientId: function (value) {
        if (this.partnerAutoCompleteBehaviorClientId != value) {
            this.partnerAutoCompleteBehaviorClientId = value;
        }
    },

    get_CimTextBoxValidatorClientId: function () {
        return this.cimTextBoxValidatorClientId;
    },

    set_CimTextBoxValidatorClientId: function (value) {
        if (this.cimTextBoxValidatorClientId != value) {
            this.cimTextBoxValidatorClientId = value;
        }
    }
}


Utility.CimTipusBehavior.registerClass('Utility.CimTipusBehavior', Sys.UI.Behavior);

if (typeof (Sys) !== 'undefined') Sys.Application.notifyScriptLoaded();